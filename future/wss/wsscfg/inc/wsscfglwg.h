/********************************************************************
* Copyright (C) 20132006 Aricent Inc . All Rights Reserved
* $Id: wsscfglwg.h,v 1.3.2.1 2018/03/19 14:21:09 siva Exp $
* * Description: Proto types for Low Level  Routines
*********************************************************************/


/* Proto Validate Index Instance for FsSecurityWebAuthGuestInfoTable. */
INT1 nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSecurityWebAuthGuestInfoTable  */

INT1 nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsSecurityWebAuthGuestInfoTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthType ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthUrl ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthRedirectUrl ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAddr ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebTitle ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebMessage ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebLogoFileName ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebSuccMessage ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebFailMessage ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebButtonText ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthWebLoadBalInfo ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthDisplayLang ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthColor ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWlanProfileId ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthUserLifetime ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthUserEmailId ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsSecurityWebAuthGuestInfoRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSecurityWebAuthUPassword ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthType ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthUrl ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthRedirectUrl ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAddr ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebTitle ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebMessage ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebLogoFileName ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebSuccMessage ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebFailMessage ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebButtonText ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthWebLoadBalInfo ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthDisplayLang ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthColor ARG_LIST(
     (INT4));

INT1 nmhSetFsSecurityWlanProfileId ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthUserLifetime ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsSecurityWebAuthGuestInfoRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSecurityWebAuthUPassword ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE * ,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthType ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthUrl ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthRedirectUrl ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAddr ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebTitle ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebMessage ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebLogoFileName ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebSuccMessage ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebFailMessage ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebButtonText ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthWebLoadBalInfo ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthDisplayLang ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthColor ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWlanProfileId ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthUserLifetime ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsSecurityWebAuthGuestInfoRowStatus ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSecurityWebAuthUPassword ARG_LIST(
      (UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,
       tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */
INT1 nmhDepv2FsDot11nConfigTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11nMCSDataRateTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthType ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthUrl ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthRedirectUrl ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAddr ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebTitle ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebMessage ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebLogoFileName ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebSuccMessage ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebFailMessage ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebButtonText ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthWebLoadBalInfo ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthDisplayLang ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthColor ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Proto Validate Index Instance for CapwapDot11WlanTable. */
INT1 nmhValidateIndexInstanceCapwapDot11WlanTable ARG_LIST(
     (UINT4));

/* Proto Validate Index Instance for CapwapDot11WlanBindTable. */
INT1 nmhValidateIndexInstanceCapwapDot11WlanBindTable ARG_LIST(
     (INT4,
      UINT4));

/* Proto Validate Index Instance for FsWtpImageUpgradeTable. */
INT1 nmhValidateIndexInstanceFsWtpImageUpgradeTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

INT1
nmhValidateIndexInstanceFsWlanMulticastTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWlanMulticastTable  */

INT1
nmhGetFirstIndexFsWlanMulticastTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanMulticastTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanMulticastMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsWlanMulticastSnoopTableLength ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWlanMulticastSnoopTimer ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWlanMulticastSnoopTimeout ARG_LIST((UINT4 ,UINT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanMulticastMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsWlanMulticastSnoopTableLength ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWlanMulticastSnoopTimer ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsWlanMulticastSnoopTimeout ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanMulticastMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsWlanMulticastSnoopTableLength ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWlanMulticastSnoopTimer ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsWlanMulticastSnoopTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanMulticastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWlanStatisticsTable. */
INT1
nmhValidateIndexInstanceFsWlanStatisticsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for CapwapDot11WlanTable  */

INT1 nmhGetFirstIndexCapwapDot11WlanTable ARG_LIST(
     (UINT4 *));

/* Proto Type for Low Level GET FIRST fn for CapwapDot11WlanBindTable  */

INT1 nmhGetFirstIndexCapwapDot11WlanBindTable ARG_LIST(
     (INT4 *,
      UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexCapwapDot11WlanTable ARG_LIST(
     (UINT4,
      UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexCapwapDot11WlanBindTable ARG_LIST(
     (INT4,
      INT4 *,
      UINT4,
      UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanProfileIfIndex ARG_LIST(
     (UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanMacType ARG_LIST(
     (UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanTunnelMode ARG_LIST(
     (UINT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanRowStatus ARG_LIST(
     (UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanBindWlanId ARG_LIST(
     (INT4,
      UINT4,
      UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanBindBssIfIndex ARG_LIST(
     (INT4,
      UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetCapwapDot11WlanBindRowStatus ARG_LIST(
     (INT4,
      UINT4,
      INT4 *));

#ifndef RFMGMT_WANTED
/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsRrmDcaMode ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsRrmDcaChannelSelectionMode ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsRrmTpcMode ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsRrmTpcSelectionMode ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsRrmRowStatus ARG_LIST(
     (INT4,
      INT4 *));
#endif
/* Low Level SET Routine for All Objects.  */


/* Low Level SET Routine for All Objects.  */

INT1 nmhSetCapwapDot11WlanMacType ARG_LIST(
     (UINT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetCapwapDot11WlanTunnelMode ARG_LIST(
     (UINT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetCapwapDot11WlanRowStatus ARG_LIST(
     (UINT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetCapwapDot11WlanBindRowStatus ARG_LIST(
     (INT4,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2CapwapDot11WlanMacType ARG_LIST(
     (UINT4 *,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2CapwapDot11WlanTunnelMode ARG_LIST(
     (UINT4 *,
      UINT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2CapwapDot11WlanRowStatus ARG_LIST(
     (UINT4 *,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2CapwapDot11WlanBindRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      UINT4,
      INT4));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2CapwapDot11WlanTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2CapwapDot11WlanBindTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));


#ifndef RFMGMT_WANTED
INT1 nmhTestv2FsRrmDcaMode ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsRrmDcaChannelSelectionMode ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsRrmTpcMode ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsRrmTpcSelectionMode ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsRrmRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));


/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsRrmConfigTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));
#endif
/* Proto Validate Index Instance for FsDot11StationConfigTable. */
INT1 nmhValidateIndexInstanceFsDot11StationConfigTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11CapabilityProfileTable. */
INT1 nmhValidateIndexInstanceFsDot11CapabilityProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsDot11AuthenticationProfileTable. */
INT1 nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsSecurityWebAuthGuestInfoTable. */
/* INT1
nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Proto Validate Index Instance for FsStationQosParamsTable. */
INT1 nmhValidateIndexInstanceFsStationQosParamsTable ARG_LIST(
     (INT4,
      tMacAddr));

/* Proto Validate Index Instance for FsVlanIsolationTable. */
INT1 nmhValidateIndexInstanceFsVlanIsolationTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11RadioConfigTable. */
INT1 nmhValidateIndexInstanceFsDot11RadioConfigTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11QosProfileTable. */
INT1 nmhValidateIndexInstanceFsDot11QosProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsDot11WlanCapabilityProfileTable. */
INT1 nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11WlanAuthenticationProfileTable. */
INT1 nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11WlanQosProfileTable. */
INT1 nmhValidateIndexInstanceFsDot11WlanQosProfileTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11ClientSummaryTable. */
INT1
nmhValidateIndexInstanceFsDot11ClientSummaryTable ARG_LIST((tMacAddr ));

/* Proto Validate Index Instance for FsDot11ExternalWebAuthProfileTable. */
INT1
nmhValidateIndexInstanceFsDot11ExternalWebAuthProfileTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11ExternalWebAuthProfileTable  */

INT1
nmhGetFirstIndexFsDot11ExternalWebAuthProfileTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11ExternalWebAuthProfileTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11ExternalWebAuthMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ExternalWebAuthUrl ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11ExternalWebAuthMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ExternalWebAuthUrl ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11ExternalWebAuthMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ExternalWebAuthUrl ARG_LIST((UINT4 * ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11ExternalWebAuthProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsDot11RadioQosTable. */
INT1 nmhValidateIndexInstanceFsDot11RadioQosTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11QAPTable. */
INT1 nmhValidateIndexInstanceFsDot11QAPTable ARG_LIST(
     (INT4,
      INT4));

/* Proto Validate Index Instance for FsQAPProfileTable. */
INT1 nmhValidateIndexInstanceFsQAPProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Proto Validate Index Instance for FsDot11CapabilityMappingTable. */
INT1 nmhValidateIndexInstanceFsDot11CapabilityMappingTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11AuthMappingTable. */
INT1 nmhValidateIndexInstanceFsDot11AuthMappingTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11QosMappingTable. */
INT1 nmhValidateIndexInstanceFsDot11QosMappingTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11AntennasListTable. */
INT1 nmhValidateIndexInstanceFsDot11AntennasListTable ARG_LIST(
     (INT4,
      INT4));

/* Proto Validate Index Instance for FsDot11WlanTable. */
INT1 nmhValidateIndexInstanceFsDot11WlanTable ARG_LIST(
     (UINT4));

/* Proto Validate Index Instance for FsDot11WlanBindTable. */
INT1 nmhValidateIndexInstanceFsDot11WlanBindTable ARG_LIST(
     (INT4,
      UINT4));

/* Proto Type for Low Level GET FIRST fn for FsDot11StationConfigTable  */

INT1 nmhGetFirstIndexFsDot11StationConfigTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11CapabilityProfileTable  */

INT1 nmhGetFirstIndexFsDot11CapabilityProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsDot11AuthenticationProfileTable  */

INT1 nmhGetFirstIndexFsDot11AuthenticationProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSecurityWebAuthGuestInfoTable  */

/* INT1
nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto Type for Low Level GET FIRST fn for FsStationQosParamsTable  */

INT1 nmhGetFirstIndexFsStationQosParamsTable ARG_LIST(
     (INT4 *,
      tMacAddr *));

/* Proto Type for Low Level GET FIRST fn for FsVlanIsolationTable  */

INT1 nmhGetFirstIndexFsVlanIsolationTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11RadioConfigTable  */

INT1 nmhGetFirstIndexFsDot11RadioConfigTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11QosProfileTable  */

INT1 nmhGetFirstIndexFsDot11QosProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsDot11WlanCapabilityProfileTable  */

INT1 nmhGetFirstIndexFsDot11WlanCapabilityProfileTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11WlanAuthenticationProfileTable  */

INT1 nmhGetFirstIndexFsDot11WlanAuthenticationProfileTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11WlanQosProfileTable  */

INT1 nmhGetFirstIndexFsDot11WlanQosProfileTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11ClientSummaryTable  */

INT1
nmhGetFirstIndexFsDot11ClientSummaryTable ARG_LIST((tMacAddr * ));


/* Proto Type for Low Level GET FIRST fn for FsDot11RadioQosTable  */

INT1 nmhGetFirstIndexFsDot11RadioQosTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11QAPTable  */

INT1 nmhGetFirstIndexFsDot11QAPTable ARG_LIST(
     (INT4 *,
      INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsQAPProfileTable  */

INT1 nmhGetFirstIndexFsQAPProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11CapabilityMappingTable  */

INT1 nmhGetFirstIndexFsDot11CapabilityMappingTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11AuthMappingTable  */

INT1 nmhGetFirstIndexFsDot11AuthMappingTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11QosMappingTable  */

INT1 nmhGetFirstIndexFsDot11QosMappingTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11AntennasListTable  */

INT1 nmhGetFirstIndexFsDot11AntennasListTable ARG_LIST(
     (INT4 *,
      INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11WlanTable  */

INT1 nmhGetFirstIndexFsDot11WlanTable ARG_LIST(
     (UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11WlanBindTable  */

INT1 nmhGetFirstIndexFsDot11WlanBindTable ARG_LIST(
     (INT4 *,
      UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsWtpImageUpgradeTable  */

INT1 nmhGetFirstIndexFsWtpImageUpgradeTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsWlanStatisticsTable  */

INT1
nmhGetFirstIndexFsWlanStatisticsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11StationConfigTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11CapabilityProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11AuthenticationProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for GET_NEXT Routine.  */

/* INT1
nmhGetNextIndexFsSecurityWebAuthGuestInfoTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsStationQosParamsTable ARG_LIST(
     (INT4,
      INT4 *,
      tMacAddr,
      tMacAddr *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsVlanIsolationTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11RadioConfigTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11QosProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11WlanCapabilityProfileTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11WlanAuthenticationProfileTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11WlanQosProfileTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11ClientSummaryTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11RadioQosTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11QAPTable ARG_LIST(
     (INT4,
      INT4 *,
      INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsQAPProfileTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11CapabilityMappingTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11AuthMappingTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11QosMappingTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11AntennasListTable ARG_LIST(
     (INT4,
      INT4 *,
      INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11WlanTable ARG_LIST(
     (UINT4,
      UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11WlanBindTable ARG_LIST(
     (INT4,
      INT4 *,
      UINT4,
      UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsWtpImageUpgradeTable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

INT1
nmhValidateIndexInstanceFsDot11nExtConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11nExtConfigTable  */

INT1
nmhGetFirstIndexFsDot11nExtConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11nExtConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11nAMPDUStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nAMPDUSubFrame ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nAMPDULimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nAMSDUStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nAMSDULimit ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11nAMPDUStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nAMPDUSubFrame ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nAMPDULimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nAMSDUStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nAMSDULimit ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11nAMPDUStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nAMPDUSubFrame ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nAMPDULimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nAMSDUStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nAMSDULimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11nExtConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanStatisticsTable ARG_LIST((INT4 , INT4 *));


/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11aNetworkEnable ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11bNetworkEnable ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11gSupport ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11anSupport ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11bnSupport ARG_LIST(
     (INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11ManagmentSSID ARG_LIST(
     (UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11CountryString ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthType ARG_LIST((INT4 *)); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthUrl ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthRedirectUrl ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAddr ARG_LIST((INT4 *)); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebTitle ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebLogoFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebSuccMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebFailMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebButtonText ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthWebLoadBalInfo ARG_LIST((tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthDisplayLang ARG_LIST((INT4 *)); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthColor ARG_LIST((INT4 *));  */

INT1
nmhGetFsDot11StationCount ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11SupressSSID ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11VlanId ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11BandwidthThresh ARG_LIST(
     (INT4,
      INT4 *)); 


INT1
nmhGetFsDot11WlanWebAuthRedirectFileName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11WlanLoginAuthentication ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11CFPollable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11CFPollRequest ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11PrivacyOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11ShortPreambleOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11PBCCOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11ChannelAgilityPresent ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QosOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11SpectrumManagementRequired ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11ShortSlotTimeOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11APSDOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DSSSOFDMOptionEnabled ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DelayedBlockAckOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11ImmediateBlockAckOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QAckOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QueueRequestOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11TXOPRequestOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RSNAOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RSNAPreauthenticationImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11CapabilityRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11AuthenticationAlgorithm ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11WepKeyIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WepKeyType ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WepKeyLength ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WepKey ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WebAuthentication ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11AuthenticationRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWlanProfileId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *)); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthUserLifetime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *)); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthUserEmailId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * )); */

/* Proto type for Low Level GET Routine All Objects.  */

/* INT1
nmhGetFsSecurityWebAuthGuestInfoRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *)); */

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsStaQoSPriority ARG_LIST(
     (INT4,
      tMacAddr,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsStaQoSDscp ARG_LIST(
     (INT4,
      tMacAddr,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsVlanIsolation ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RadioType ARG_LIST(
     (INT4,
      UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RadioNoOfBssIdSupported ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RadioAntennaType ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RadioFailureStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11RowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QosTraffic ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QosPassengerTrustMode ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QosRateLimit ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11UpStreamCIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11UpStreamCBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11UpStreamEIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11UpStreamEBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DownStreamCIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DownStreamCBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DownStreamEIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DownStreamEBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QosRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanCFPollable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanCFPollRequest ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanPrivacyOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanShortPreambleOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanPBCCOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanChannelAgilityPresent ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQosOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanSpectrumManagementRequired ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanShortSlotTimeOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanAPSDOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanDSSSOFDMOptionEnabled ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanDelayedBlockAckOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanImmediateBlockAckOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQAckOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQueueRequestOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanTXOPRequestOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanRSNAOptionImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanRSNAPreauthenticationImplemented ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanCapabilityRowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanAuthenticationAlgorithm ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanWepKeyIndex ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanWepKeyType ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanWepKeyLength ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanWepKey ARG_LIST(
     (INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanWebAuthentication ARG_LIST(
     (INT4,
      INT4 *));

INT1 nmhSetFsDot11CapabilityMappingProfileName(
    INT4 i4IfIndex,
    tSNMP_OCTET_STRING_TYPE *
    pSetValFsDot11CapabilityMappingProfileName);

INT1 nmhSetFsDot11AuthMappingProfileName(
    INT4 i4IfIndex,
    tSNMP_OCTET_STRING_TYPE * pSetValFsDot11AuthMappingProfileName);

INT1 nmhSetFsDot11QosMappingProfileName(
    INT4 i4IfIndex,
    tSNMP_OCTET_STRING_TYPE * pSetValFsDot11QosMappingProfileName);

#ifndef RFMGMT_WANTED
INT1 nmhSetFsRrmDcaMode(
    INT4 i4FsRrmRadioType,
    INT4 i4SetValFsRrmDcaMode);

INT1 nmhSetFsRrmDcaChannelSelectionMode(
    INT4 i4FsRrmRadioType,
    INT4 i4SetValFsRrmDcaChannelSelectionMode);

INT1 nmhSetFsRrmTpcMode(
    INT4 i4FsRrmRadioType,
    INT4 i4SetValFsRrmTpcMode);
INT1 nmhSetFsRrmTpcSelectionMode(
    INT4 i4FsRrmRadioType,
    INT4 i4SetValFsRrmTpcSelectionMode);

INT1 nmhSetFsRrmRowStatus(
    INT4 i4FsRrmRadioType,
    INT4 i4SetValFsRrmRowStatus);
#endif
INT1 nmhGetFsDot11CapabilityMappingProfileName(
    INT4 i4IfIndex,
    tSNMP_OCTET_STRING_TYPE *
    pRetValFsDot11CapabilityMappingProfileName);

INT1 nmhGetFsDot11AuthMappingProfileName(
    INT4 i4IfIndex,
    tSNMP_OCTET_STRING_TYPE * pRetValFsDot11AuthMappingProfileName);

INT1 nmhGetFsDot11QosMappingProfileName(
    INT4 i4IfIndex,
    tSNMP_OCTET_STRING_TYPE * pRetValFsDot11QosMappingProfileName);

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11WlanProfileId ARG_LIST((tMacAddr ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11WtpProfileName ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11WtpRadioId ARG_LIST((tMacAddr ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11AuthStatus ARG_LIST((tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11AssocStatus ARG_LIST((tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanAuthenticationRowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQosTraffic ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQosPassengerTrustMode ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQosRateLimit ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanUpStreamCIR ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanUpStreamCBS ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanUpStreamEIR ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanUpStreamEBS ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanDownStreamCIR ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanDownStreamCBS ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanDownStreamEIR ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanDownStreamEBS ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanQosRowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11TaggingPolicy ARG_LIST(
     (INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QueueDepth ARG_LIST(
     (INT4,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11PriorityValue ARG_LIST(
     (INT4,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11DscpValue ARG_LIST(
     (INT4,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileCWmin ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileCWmax ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileAIFSN ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileTXOPLimit ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileQueueDepth ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfilePriorityValue ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileDscpValue ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsQAPProfileRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11CapabilityMappingRowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11AuthMappingRowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11QosMappingRowStatus ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsAntennaMode ARG_LIST(
     (INT4,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsAntennaSelection ARG_LIST(
     (INT4,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanProfileIfIndex ARG_LIST(
     (UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanRowStatus ARG_LIST(
     (UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanBindWlanId ARG_LIST(
     (INT4,
      UINT4,
      UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanBindBssIfIndex ARG_LIST(
     (INT4,
      UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11WlanBindRowStatus ARG_LIST(
     (INT4,
      UINT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpImageVersion ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpUpgradeDev ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpName ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpImageName ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpAddressType ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpServerIP ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsWtpRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanBeaconsSentCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsWlanProbeReqRcvdCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsWlanProbeRespSentCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsWlanDataPktRcvdCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsWlanDataPktSentCount ARG_LIST((INT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11aNetworkEnable ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11bNetworkEnable ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11gSupport ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11anSupport ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11bnSupport ARG_LIST(
     (INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11ManagmentSSID ARG_LIST(
     (UINT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11CountryString ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthType ARG_LIST((INT4 )); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthUrl ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthRedirectUrl ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAddr ARG_LIST((INT4 )); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebTitle ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebLogoFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebSuccMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebFailMessage ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebButtonText ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthWebLoadBalInfo ARG_LIST((tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthDisplayLang ARG_LIST((INT4 )); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthColor ARG_LIST((INT4 )); */

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11SupressSSID ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11VlanId ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11BandwidthThresh ARG_LIST(
     (INT4,
      INT4 ));

INT1
nmhSetFsDot11WlanWebAuthRedirectFileName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot11WlanLoginAuthentication ARG_LIST((INT4  , INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11CFPollable ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11CFPollRequest ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11PrivacyOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11ShortPreambleOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11PBCCOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11ChannelAgilityPresent ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QosOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11SpectrumManagementRequired ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11ShortSlotTimeOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11APSDOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DSSSOFDMOptionEnabled ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DelayedBlockAckOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11ImmediateBlockAckOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QAckOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QueueRequestOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11TXOPRequestOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11RSNAOptionImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11RSNAPreauthenticationImplemented ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11CapabilityRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11AuthenticationAlgorithm ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WepKeyType ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WepKeyIndex ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WepKeyLength ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WepKey ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WebAuthentication ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11AuthenticationRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWlanProfileId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 )); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthUserLifetime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 )); */

/* Low Level SET Routine for All Objects.  */

/* INT1
nmhSetFsSecurityWebAuthGuestInfoRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 )); */

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsStaQoSPriority ARG_LIST(
     (INT4,
      tMacAddr,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsStaQoSDscp ARG_LIST(
     (INT4,
      tMacAddr,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsVlanIsolation ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11RadioType ARG_LIST(
     (INT4,
      UINT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11RowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QosTraffic ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QosPassengerTrustMode ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QosRateLimit ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11UpStreamCIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11UpStreamCBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11UpStreamEIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11UpStreamEBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DownStreamCIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DownStreamCBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DownStreamEIR ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DownStreamEBS ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QosRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanCFPollable ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanCFPollRequest ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanPrivacyOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanShortPreambleOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanPBCCOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanChannelAgilityPresent ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQosOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanSpectrumManagementRequired ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanShortSlotTimeOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanAPSDOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanDSSSOFDMOptionEnabled ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanDelayedBlockAckOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanImmediateBlockAckOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQAckOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQueueRequestOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanTXOPRequestOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanRSNAOptionImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanRSNAPreauthenticationImplemented ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanCapabilityRowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanAuthenticationAlgorithm ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanWepKeyIndex ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanWepKeyType ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanWepKeyLength ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanWepKey ARG_LIST(
     (INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanWebAuthentication ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanAuthenticationRowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQosTraffic ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQosPassengerTrustMode ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQosRateLimit ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanUpStreamCIR ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanUpStreamCBS ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanUpStreamEIR ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanUpStreamEBS ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanDownStreamCIR ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanDownStreamCBS ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanDownStreamEIR ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanDownStreamEBS ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanQosRowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11TaggingPolicy ARG_LIST(
     (INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QueueDepth ARG_LIST(
     (INT4,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11PriorityValue ARG_LIST(
     (INT4,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11DscpValue ARG_LIST(
     (INT4,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileCWmin ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileCWmax ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileAIFSN ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileTXOPLimit ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileQueueDepth ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfilePriorityValue ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileDscpValue ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsQAPProfileRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11CapabilityMappingRowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11AuthMappingRowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11QosMappingRowStatus ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsAntennaMode ARG_LIST(
     (INT4,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsAntennaSelection ARG_LIST(
     (INT4,
      INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanProfileIfIndex ARG_LIST(
     (UINT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanRowStatus ARG_LIST(
     (UINT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanBindWlanId ARG_LIST(
     (INT4,
      UINT4,
      UINT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanBindBssIfIndex ARG_LIST(
     (INT4,
      UINT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11WlanBindRowStatus ARG_LIST(
     (INT4,
      UINT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpImageVersion ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpUpgradeDev ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpName ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpImageName ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpAddressType ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpServerIP ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsWtpRowStatus ARG_LIST(
     (tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanBeaconsSentCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsWlanProbeReqRcvdCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsWlanProbeRespSentCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsWlanDataPktRcvdCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsWlanDataPktSentCount ARG_LIST((INT4  ,UINT4 ));


/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11aNetworkEnable ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11bNetworkEnable ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11gSupport ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11anSupport ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11bnSupport ARG_LIST(
     (UINT4 *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11ManagmentSSID ARG_LIST(
     (UINT4 *,
      UINT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11CountryString ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthType ARG_LIST((UINT4 * , INT4 )); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthUrl ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthRedirectUrl ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAddr ARG_LIST((UINT4 * , INT4 )); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthWebTitle ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthWebMessage ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthWebLogoFileName ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/*  INT1
nmhTestv2FsSecurityWebAuthWebSuccMessage ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthWebFailMessage ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthWebButtonText ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthWebLoadBalInfo ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE *)); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthDisplayLang ARG_LIST((UINT4 * , INT4 )); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthColor ARG_LIST((UINT4 * , INT4 )); */

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11SupressSSID ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11VlanId ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11BandwidthThresh ARG_LIST(
     (UINT4 *,
      INT4,
      INT4 ));

INT1
nmhTestv2FsDot11WlanWebAuthRedirectFileName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot11WlanLoginAuthentication ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11CFPollable ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11CFPollRequest ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11PrivacyOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11ShortPreambleOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11PBCCOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11ChannelAgilityPresent ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11SpectrumManagementRequired ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11ShortSlotTimeOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11APSDOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DSSSOFDMOptionEnabled ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DelayedBlockAckOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11ImmediateBlockAckOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QAckOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QueueRequestOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11TXOPRequestOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11RSNAOptionImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11RSNAPreauthenticationImplemented ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11CapabilityRowStatus ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11AuthenticationAlgorithm ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WepKeyType ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WepKeyIndex ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WepKeyLength ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WepKey ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WebAuthentication ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11AuthenticationRowStatus ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWlanProfileId ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 )); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthUserLifetime ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 )); */

/* Low Level TEST Routines for.  */

/* INT1
nmhTestv2FsSecurityWebAuthGuestInfoRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 )); */

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsStaQoSPriority ARG_LIST(
     (UINT4 *,
      INT4,
      tMacAddr,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsStaQoSDscp ARG_LIST(
     (UINT4 *,
      INT4,
      tMacAddr,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsVlanIsolation ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11RadioType ARG_LIST(
     (UINT4 *,
      INT4,
      UINT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11RowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosTraffic ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosPassengerTrustMode ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosRateLimit ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11UpStreamCIR ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11UpStreamCBS ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11UpStreamEIR ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11UpStreamEBS ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DownStreamCIR ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DownStreamCBS ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DownStreamEIR ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DownStreamEBS ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosRowStatus ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanCFPollable ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanCFPollRequest ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanPrivacyOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanShortPreambleOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanPBCCOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanChannelAgilityPresent ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQosOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanSpectrumManagementRequired ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanShortSlotTimeOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanAPSDOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanDSSSOFDMOptionEnabled ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanDelayedBlockAckOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanImmediateBlockAckOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQAckOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQueueRequestOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanTXOPRequestOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanRSNAOptionImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanRSNAPreauthenticationImplemented ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanCapabilityRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanAuthenticationAlgorithm ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanWepKeyIndex ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanWepKeyType ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanWepKeyLength ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanWepKey ARG_LIST(
     (UINT4 *,
      INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanWebAuthentication ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanAuthenticationRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQosTraffic ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQosPassengerTrustMode ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQosRateLimit ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanUpStreamCIR ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanUpStreamCBS ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanUpStreamEIR ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanUpStreamEBS ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanDownStreamCIR ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanDownStreamCBS ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanDownStreamEIR ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanDownStreamEBS ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanQosRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11TaggingPolicy ARG_LIST(
     (UINT4 *,
      INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QueueDepth ARG_LIST(
     (UINT4 *,
      INT4,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11PriorityValue ARG_LIST(
     (UINT4 *,
      INT4,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11DscpValue ARG_LIST(
     (UINT4 *,
      INT4,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileCWmin ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileCWmax ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileAIFSN ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileTXOPLimit ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileQueueDepth ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfilePriorityValue ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileDscpValue ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsQAPProfileRowStatus ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11CapabilityMappingProfileName ARG_LIST(
     (UINT4 *,
      INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11CapabilityMappingRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11AuthMappingProfileName ARG_LIST(
     (UINT4 *,
      INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11AuthMappingRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosMappingProfileName ARG_LIST(
     (UINT4 *,
      INT4,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11QosMappingRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsAntennaMode ARG_LIST(
     (UINT4 *,
      INT4,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsAntennaSelection ARG_LIST(
     (UINT4 *,
      INT4,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanProfileIfIndex ARG_LIST(
     (UINT4 *,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanRowStatus ARG_LIST(
     (UINT4 *,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanBindWlanId ARG_LIST(
     (UINT4 *,
      INT4,
      UINT4,
      UINT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanBindBssIfIndex ARG_LIST(
     (UINT4 *,
      INT4,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11WlanBindRowStatus ARG_LIST(
     (UINT4 *,
      INT4,
      UINT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpImageVersion ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpUpgradeDev ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpName ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpImageName ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpAddressType ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpServerIP ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsWtpRowStatus ARG_LIST(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *,
      INT4));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanBeaconsSentCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsWlanProbeReqRcvdCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsWlanProbeRespSentCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsWlanDataPktRcvdCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsWlanDataPktSentCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));


/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11aNetworkEnable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11bNetworkEnable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11gSupport ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11anSupport ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11bnSupport ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11ManagmentSSID ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11CountryString ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));
/* Proto Validate Index Instance for FsDot11SupportedCountryTable. */
INT1
nmhValidateIndexInstanceFsDot11SupportedCountryTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11SupportedCountryTable  */

INT1
nmhGetFirstIndexFsDot11SupportedCountryTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11SupportedCountryTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11CountryCode ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11CountryName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11NumericIndex ARG_LIST((UINT4 ,UINT4 *));


/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthUrl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthRedirectUrl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmh Depv2FsSecurityWebAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebTitle ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebMessage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebLogoFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebSuccMessage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebFailMessage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebButtonText ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthWebLoadBalInfo ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthDisplayLang ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsSecurityWebAuthColor ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11StationConfigTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11CapabilityProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11AuthenticationProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsSecurityWebAuthGuestInfoTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsStationQosParamsTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsVlanIsolationTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11RadioConfigTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11QosProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11WlanCapabilityProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11WlanAuthenticationProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11WlanQosProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11RadioQosTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11QAPTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsQAPProfileTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11CapabilityMappingTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11AuthMappingTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11QosMappingTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11AntennasListTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11WlanTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsDot11WlanBindTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1 nmhDepv2FsWtpImageUpgradeTable ARG_LIST(
     (UINT4 *,
      tSnmpIndexList *,
      tSNMP_VAR_BIND *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanStatisticsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot11nConfigTable. */
INT1 nmhValidateIndexInstanceFsDot11nConfigTable ARG_LIST(
     (INT4));

/* Proto Validate Index Instance for FsDot11nMCSDataRateTable. */
INT1 nmhValidateIndexInstanceFsDot11nMCSDataRateTable ARG_LIST(
     (INT4,
      INT4));

/* Proto Type for Low Level GET FIRST fn for FsDot11nConfigTable  */

INT1 nmhGetFirstIndexFsDot11nConfigTable ARG_LIST(
     (INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsDot11nMCSDataRateTable  */

INT1 nmhGetFirstIndexFsDot11nMCSDataRateTable ARG_LIST(
     (INT4 *,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11nConfigTable ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexFsDot11nMCSDataRateTable ARG_LIST(
     (INT4,
      INT4 *,
      INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11nConfigShortGIfor20MHz ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11nConfigShortGIfor40MHz ARG_LIST(
     (INT4,
      INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11nConfigChannelWidth ARG_LIST(
     (INT4,
      INT4 *));

INT1
nmhGetFsDot11nConfigAllowChannelWidth ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1 nmhGetFsDot11nMCSDataRate ARG_LIST(
     (INT4,
      INT4,
      INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11nConfigShortGIfor20MHz ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11nConfigShortGIfor40MHz ARG_LIST(
     (INT4,
      INT4));

/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11nConfigChannelWidth ARG_LIST(
     (INT4,
      INT4));
INT1
nmhSetFsDot11nConfigAllowChannelWidth ARG_LIST((INT4  ,INT4 ));


/* Low Level SET Routine for All Objects.  */

INT1 nmhSetFsDot11nMCSDataRate ARG_LIST(
     (INT4,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11nConfigShortGIfor20MHz ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11nConfigShortGIfor40MHz ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11nConfigChannelWidth ARG_LIST(
     (UINT4 *,
      INT4,
      INT4));
INT1
nmhTestv2FsDot11nConfigAllowChannelWidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1 nmhTestv2FsDot11nMCSDataRate ARG_LIST(
     (UINT4 *,
      INT4,
      INT4,
      INT4));

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsDot11nConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Low Level DEP Routines for.  */

/* INT1
nmhDepv2FsDot11nMCSDataRateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*)); */

/* Proto Validate Index Instance for FsWlanSSIDStatsTable. */
INT1
nmhValidateIndexInstanceFsWlanSSIDStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWlanSSIDStatsTable  */

INT1
nmhGetFirstIndexFsWlanSSIDStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanSSIDStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanSSIDStatsAssocClientCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsWlanSSIDStatsMaxClientCount ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanSSIDStatsMaxClientCount ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanSSIDStatsMaxClientCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanSSIDStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWlanClientStatsTable. */
INT1
nmhValidateIndexInstanceFsWlanClientStatsTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsWlanClientStatsTable  */

INT1
nmhGetFirstIndexFsWlanClientStatsTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanClientStatsTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanClientStatsIdleTimeout ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsWlanClientStatsBytesReceivedCount ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsWlanClientStatsBytesSentCount ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsWlanClientStatsAuthState ARG_LIST((tMacAddr ,INT4 *));
INT1
nmhGetFsWlanClientStatsRadiusUserName ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWlanClientStatsIpAddressType ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsWlanClientStatsIpAddress ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWlanClientStatsAssocTime ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsWlanClientStatsSessionTimeLeft ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsWlanClientStatsConnTime ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWlanClientStatsSSID ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWlanClientStatsToACStatus ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWlanClientStatsClear ARG_LIST((tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanClientStatsToACStatus ARG_LIST((tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWlanClientStatsClear ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanClientStatsToACStatus ARG_LIST((UINT4 *  ,tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWlanClientStatsClear ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanClientStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWlanRadioTable. */
INT1
nmhValidateIndexInstanceFsWlanRadioTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWlanRadioTable  */

INT1
nmhGetFirstIndexFsWlanRadioTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanRadioTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanRadioRadiobaseMACAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsWlanRadioIfInOctets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsWlanRadioIfOutOctets ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsWlanRadioRadioBand ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWlanRadioMaxClientCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsWlanRadioClearStats ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanRadioMaxClientCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsWlanRadioClearStats ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanRadioMaxClientCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsWlanRadioClearStats ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanRadioTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsApWlanStatisticsTable. */
INT1
nmhValidateIndexInstanceFsApWlanStatisticsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsApWlanStatisticsTable  */

INT1
nmhGetFirstIndexFsApWlanStatisticsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsApWlanStatisticsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsApWlanBeaconsSentCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsApWlanProbeReqRcvdCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsApWlanProbeRespSentCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsApWlanDataPktRcvdCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsApWlanDataPktSentCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsApWlanBeaconsSentCount ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsApWlanProbeReqRcvdCount ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsApWlanProbeRespSentCount ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsApWlanDataPktRcvdCount ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsApWlanDataPktSentCount ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsApWlanBeaconsSentCount ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsApWlanProbeReqRcvdCount ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsApWlanProbeRespSentCount ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsApWlanDataPktRcvdCount ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsApWlanDataPktSentCount ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsApWlanStatisticsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsWlanStationTrapStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanStationTrapStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanStationTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanStationTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot11DscpConfigTable. */
INT1
nmhValidateIndexInstanceFsDot11DscpConfigTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11DscpConfigTable  */

INT1
nmhGetFirstIndexFsDot11DscpConfigTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11DscpConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11DscpInPriority ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot11OutDscp ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsDot11DscpRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11DscpInPriority ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsDot11OutDscp ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsDot11DscpRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11DscpInPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsDot11OutDscp ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsDot11DscpRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11DscpConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#ifdef BAND_SELECT_WANTED
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBandSelectStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBandSelectStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBandSelectStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsBandSelectStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWlanBandSelectTable. */
INT1
nmhValidateIndexInstanceFsWlanBandSelectTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWlanBandSelectTable  */

INT1
nmhGetFirstIndexFsWlanBandSelectTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanBandSelectTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsBandSelectPerWlanStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsBandSelectAssocCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBandSelectAgeOutSuppTimer ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsBandSelectAssocCountFlushTimer ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsBandSelectPerWlanStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsBandSelectAssocCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsBandSelectAgeOutSuppTimer ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsBandSelectAssocCountFlushTimer ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsBandSelectPerWlanStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsBandSelectAssocCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsBandSelectAgeOutSuppTimer ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsBandSelectAssocCountFlushTimer ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanBandSelectTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWlanBandSelectClientTable. */
INT1
nmhValidateIndexInstanceFsWlanBandSelectClientTable ARG_LIST((tMacAddr  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsWlanBandSelectClientTable  */

INT1
nmhGetFirstIndexFsWlanBandSelectClientTable ARG_LIST((tMacAddr *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanBandSelectClientTable ARG_LIST((tMacAddr , tMacAddr *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanBandSelectStationActiveStatus ARG_LIST((tMacAddr  , tMacAddr ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

#endif

INT1
nmhGetFsStationDebugOption ARG_LIST((INT4 *));

INT1
nmhGetFsWlanDebugOption ARG_LIST((INT4 *));

INT1
nmhGetFsRadioDebugOption ARG_LIST((INT4 *));

INT1
nmhGetFsPmDebugOption ARG_LIST((INT4 *));

INT1
nmhGetFsWssIfDebugOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsStationDebugOption ARG_LIST((INT4 ));

INT1
nmhSetFsWlanDebugOption ARG_LIST((INT4 ));

INT1
nmhSetFsRadioDebugOption ARG_LIST((INT4 ));

INT1
nmhSetFsPmDebugOption ARG_LIST((INT4 ));

INT1
nmhSetFsWssIfDebugOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsStationDebugOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWlanDebugOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRadioDebugOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPmDebugOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWssIfDebugOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsStationDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRadioDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPmDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWssIfDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsBandSelectLegacyRate ARG_LIST((INT4 *));

INT1
nmhSetFsBandSelectLegacyRate ARG_LIST((INT4 ));

INT1
nmhTestv2FsBandSelectLegacyRate ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2FsBandSelectLegacyRate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsDot11nConfigMIMOPowerSave ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigDelayedBlockAckOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigMaxAMSDULengthConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigMaxRxAMPDUFactorConfig ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigMinimumMPDUStartSpacingConfig ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigPCOOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigTransitionTimeConfig ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigMCSFeedbackOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigHTControlFieldSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigRDResponderOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigtHTDSSCCKModein40MHz ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigtHTDSSCCKModein40MHzConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigTxRxMCSSetNotEqual ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigTxMaximumNumberSpatialStreamsSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigTxUnequalModulationSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigReceiveStaggerSoundingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigTransmitStaggerSoundingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigReceiveNDPOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigTransmitNDPOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigCalibrationOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigMinimalGroupingImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigMinimalGroupingActivated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigNumberBeamFormingCSISupportAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigChannelEstimationCapabilityImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigChannelEstimationCapabilityActivated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigCurrentPrimaryChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigCurrentSecondaryChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11nConfigSTAChannelWidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigSTAChannelWidthConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigNonGreenfieldHTSTAsPresent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigOBSSNonHTSTAsPresent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigOBSSNonHTSTAsPresentConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigDualBeacon ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigDualBeaconConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigSTBCBeacon ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigSTBCBeaconConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigPCOPhase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigPCOPhaseConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigRxSTBCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11nConfigRxSTBCOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhSetFsDot11nConfigMIMOPowerSave ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigDelayedBlockAckOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigMaxAMSDULengthConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigMaxRxAMPDUFactorConfig ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigMinimumMPDUStartSpacingConfig ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigPCOOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigTransitionTimeConfig ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigMCSFeedbackOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigHTControlFieldSupported ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigRDResponderOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigtHTDSSCCKModein40MHzConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigTxRxMCSSetNotEqual ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigTxMaximumNumberSpatialStreamsSupported ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigTxUnequalModulationSupported ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigReceiveStaggerSoundingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigTransmitStaggerSoundingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigReceiveNDPOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigTransmitNDPOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigCalibrationOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigMinimalGroupingActivated ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigNumberBeamFormingCSISupportAntenna ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigChannelEstimationCapabilityActivated ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigCurrentPrimaryChannel ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigCurrentSecondaryChannel ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11nConfigSTAChannelWidthConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigOBSSNonHTSTAsPresentConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigDualBeaconConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigSTBCBeaconConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigPCOPhaseConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11nConfigRxSTBCOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigMIMOPowerSave ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigDelayedBlockAckOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigMaxAMSDULengthConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigMaxRxAMPDUFactorConfig ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigPCOOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigTransitionTimeConfig ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigMCSFeedbackOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigHTControlFieldSupported ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigRDResponderOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigTxRxMCSSetNotEqual ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigTxMaximumNumberSpatialStreamsSupported ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigTxUnequalModulationSupported ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigReceiveNDPOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigTransmitNDPOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigCalibrationOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigMinimalGroupingActivated ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigChannelEstimationCapabilityActivated ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigCurrentPrimaryChannel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigCurrentSecondaryChannel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11nConfigSTAChannelWidthConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigDualBeaconConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigSTBCBeaconConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigPCOPhaseConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11nConfigRxSTBCOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhGetFsApGroupEnabledStatus ARG_LIST((INT4 *));

INT1
nmhSetFsApGroupEnabledStatus ARG_LIST((INT4 ));

INT1
nmhTestv2FsApGroupEnabledStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhDepv2FsApGroupEnabledStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhValidateIndexInstanceFsApGroupTable ARG_LIST((UINT4 ));

INT1
nmhGetFirstIndexFsApGroupTable ARG_LIST((UINT4 *));

INT1
nmhGetNextIndexFsApGroupTable ARG_LIST((UINT4 , UINT4 *));

INT1
nmhGetApGroupIdFromGroupName ARG_LIST((tSNMP_OCTET_STRING_TYPE *, UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsApGroupName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsApGroupNameDescription ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsApGroupRadioPolicy ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsApGroupInterfaceVlan ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsApGroupRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhSetFsApGroupName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsApGroupNameDescription ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsApGroupRadioPolicy ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsApGroupInterfaceVlan ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsApGroupRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhTestv2FsApGroupName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsApGroupNameDescription ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsApGroupRadioPolicy ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsApGroupInterfaceVlan ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsApGroupRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhDepv2FsApGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhValidateIndexInstanceFsApGroupWTPTable ARG_LIST((UINT4  , UINT4 ));

INT1
nmhGetFirstIndexFsApGroupWTPTable ARG_LIST((UINT4 * , UINT4 *));

INT1
nmhGetNextIndexFsApGroupWTPTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

INT1
nmhGetFsApGroupWTPRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhSetFsApGroupWTPRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsApGroupWTPRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhDepv2FsApGroupWTPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhValidateIndexInstanceFsApGroupWLANTable ARG_LIST((UINT4  , UINT4 ));

INT1
nmhGetFirstIndexFsApGroupWLANTable ARG_LIST((UINT4 * , UINT4 *));

INT1
nmhGetNextIndexFsApGroupWLANTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

INT1
nmhGetFsApGroupWLANRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhSetFsApGroupWLANRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsApGroupWLANRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhDepv2FsApGroupWLANTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

