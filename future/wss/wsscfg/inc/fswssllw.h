/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanDhcpServerStatus ARG_LIST((INT4 *));

INT1
nmhGetFsWlanDhcpRelayStatus ARG_LIST((INT4 *));

INT1
nmhGetFsWlanDhcpNextSrvIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetFsWlanFirewallStatus ARG_LIST((INT4 *));

INT1
nmhGetFsWlanNATStatus ARG_LIST((INT4 *));

INT1
nmhGetFsWlanDNSServerIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetFsWlanDefaultRouterIpAddr ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanDhcpServerStatus ARG_LIST((INT4 ));

INT1
nmhSetFsWlanDhcpRelayStatus ARG_LIST((INT4 ));

INT1
nmhSetFsWlanDhcpNextSrvIpAddr ARG_LIST((UINT4 ));

INT1
nmhSetFsWlanFirewallStatus ARG_LIST((INT4 ));

INT1
nmhSetFsWlanNATStatus ARG_LIST((INT4 ));

INT1
nmhSetFsWlanDNSServerIpAddr ARG_LIST((UINT4 ));

INT1
nmhSetFsWlanDefaultRouterIpAddr ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanDhcpServerStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWlanDhcpRelayStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWlanDhcpNextSrvIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsWlanFirewallStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWlanNATStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsWlanDNSServerIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsWlanDefaultRouterIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanDhcpServerStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanDhcpRelayStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanDhcpNextSrvIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanFirewallStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanNATStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanDNSServerIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsWlanDefaultRouterIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWlanRadioIfTable. */
INT1
nmhValidateIndexInstanceFsWlanRadioIfTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWlanRadioIfTable  */

INT1
nmhGetFirstIndexFsWlanRadioIfTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWlanRadioIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWlanRadioIfIpAddr ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsWlanRadioIfIpSubnetMask ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsWlanRadioIfAdminStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsWlanRadioIfRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWlanRadioIfIpAddr ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsWlanRadioIfIpSubnetMask ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsWlanRadioIfAdminStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsWlanRadioIfRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWlanRadioIfIpAddr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsWlanRadioIfIpSubnetMask ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsWlanRadioIfAdminStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsWlanRadioIfRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWlanRadioIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot11WtpDhcpSrvSubnetPoolConfigTable. */
INT1
nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11WtpDhcpSrvSubnetPoolConfigTable  */

INT1
nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11WtpDhcpSrvSubnetSubnet ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvSubnetMask ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvDefaultRouter ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvDnsServerIp ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11WtpDhcpSrvSubnetSubnet ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvSubnetMask ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvSubnetStartIpAddress ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvSubnetEndIpAddress ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvSubnetLeaseTime ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvDefaultRouter ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvDnsServerIp ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetSubnet ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetMask ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetStartIpAddress ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetEndIpAddress ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetLeaseTime ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvDefaultRouter ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvDnsServerIp ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetPoolRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11WtpDhcpSrvSubnetPoolConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpNATTable. */
INT1
nmhValidateIndexInstanceFsWtpNATTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNATTable  */

INT1
nmhGetFirstIndexFsWtpNATTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNATTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpIfMainWanType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpNATConfigRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpIfMainWanType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpNATConfigRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpIfMainWanType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpNATConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNATTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpDhcpConfigTable. */
INT1
nmhValidateIndexInstanceFsWtpDhcpConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpDhcpConfigTable  */

INT1
nmhGetFirstIndexFsWtpDhcpConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpDhcpConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpDhcpServerStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsWtpDhcpRelayStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpDhcpServerStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsWtpDhcpRelayStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpDhcpServerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsWtpDhcpRelayStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpDhcpConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpFirewallConfigTable. */
INT1
nmhValidateIndexInstanceFsWtpFirewallConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpFirewallConfigTable  */

INT1
nmhGetFirstIndexFsWtpFirewallConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpFirewallConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpFirewallStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsWtpFirewallRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpFirewallStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsWtpFirewallRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpFirewallStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsWtpFirewallRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpFirewallConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpIpRouteConfigTable. */
INT1
nmhValidateIndexInstanceFsWtpIpRouteConfigTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpIpRouteConfigTable  */

INT1
nmhGetFirstIndexFsWtpIpRouteConfigTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpIpRouteConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpIpRouteConfigRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpIpRouteConfigRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpIpRouteConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpIpRouteConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpFwlFilterTable. */
INT1
nmhValidateIndexInstanceFsWtpFwlFilterTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsWtpFwlFilterTable  */

INT1
nmhGetFirstIndexFsWtpFwlFilterTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpFwlFilterTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpFwlFilterSrcAddress ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpFwlFilterDestAddress ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpFwlFilterProtocol ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsWtpFwlFilterSrcPort ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpFwlFilterDestPort ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpFwlFilterTos ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsWtpFwlFilterAccounting ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsWtpFwlFilterHitClear ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsWtpFwlFilterHitsCount ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsWtpFwlFilterAddrType ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsWtpFwlFilterFlowId ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsWtpFwlFilterDscp ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsWtpFwlFilterRowStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpFwlFilterSrcAddress ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpFwlFilterDestAddress ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpFwlFilterProtocol ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsWtpFwlFilterSrcPort ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpFwlFilterDestPort ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpFwlFilterTos ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsWtpFwlFilterAccounting ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsWtpFwlFilterHitClear ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsWtpFwlFilterAddrType ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsWtpFwlFilterFlowId ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsWtpFwlFilterDscp ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsWtpFwlFilterRowStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpFwlFilterSrcAddress ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpFwlFilterDestAddress ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpFwlFilterProtocol ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsWtpFwlFilterSrcPort ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpFwlFilterDestPort ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpFwlFilterTos ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsWtpFwlFilterAccounting ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsWtpFwlFilterHitClear ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsWtpFwlFilterAddrType ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsWtpFwlFilterFlowId ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsWtpFwlFilterDscp ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsWtpFwlFilterRowStatus ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpFwlFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpFwlRuleTable. */
INT1
nmhValidateIndexInstanceFsWtpFwlRuleTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsWtpFwlRuleTable  */

INT1
nmhGetFirstIndexFsWtpFwlRuleTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpFwlRuleTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpFwlRuleFilterSet ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpFwlRuleRowStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpFwlRuleFilterSet ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpFwlRuleRowStatus ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpFwlRuleFilterSet ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpFwlRuleRowStatus ARG_LIST((UINT4 *  ,UINT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpFwlRuleTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpFwlAclTable. */
INT1
nmhValidateIndexInstanceFsWtpFwlAclTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpFwlAclTable  */

INT1
nmhGetFirstIndexFsWtpFwlAclTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpFwlAclTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpFwlAclAction ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsWtpFwlAclSequenceNumber ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsWtpFwlAclLogTrigger ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsWtpFwlAclFragAction ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsWtpFwlAclRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpFwlAclAction ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsWtpFwlAclSequenceNumber ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsWtpFwlAclLogTrigger ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsWtpFwlAclFragAction ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsWtpFwlAclRowStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpFwlAclAction ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpFwlAclSequenceNumber ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpFwlAclLogTrigger ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpFwlAclFragAction ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpFwlAclRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpFwlAclTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpNatDynamicTransTable. */
INT1
nmhValidateIndexInstanceFsWtpNatDynamicTransTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNatDynamicTransTable  */

INT1
nmhGetFirstIndexFsWtpNatDynamicTransTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNatDynamicTransTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNatDynamicTransTranslatedLocalIp ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsWtpNatDynamicTransTranslatedLocalPort ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpNatDynamicTransLastUseTime ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsWtpNatGlobalAddressTable. */
INT1
nmhValidateIndexInstanceFsWtpNatGlobalAddressTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNatGlobalAddressTable  */

INT1
nmhGetFirstIndexFsWtpNatGlobalAddressTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNatGlobalAddressTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNatGlobalAddressMask ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsWtpNatGlobalAddressEntryStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNatGlobalAddressMask ARG_LIST((UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsWtpNatGlobalAddressEntryStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNatGlobalAddressMask ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsWtpNatGlobalAddressEntryStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNatGlobalAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpNatLocalAddressTable. */
INT1
nmhValidateIndexInstanceFsWtpNatLocalAddressTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNatLocalAddressTable  */

INT1
nmhGetFirstIndexFsWtpNatLocalAddressTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNatLocalAddressTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNatLocalAddressMask ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsWtpNatLocalAddressEntryStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNatLocalAddressMask ARG_LIST((UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsWtpNatLocalAddressEntryStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNatLocalAddressMask ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsWtpNatLocalAddressEntryStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNatLocalAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpNatStaticTable. */
INT1
nmhValidateIndexInstanceFsWtpNatStaticTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNatStaticTable  */

INT1
nmhGetFirstIndexFsWtpNatStaticTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNatStaticTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNatStaticTranslatedLocalIp ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsWtpNatStaticEntryStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNatStaticTranslatedLocalIp ARG_LIST((UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsWtpNatStaticEntryStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNatStaticTranslatedLocalIp ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsWtpNatStaticEntryStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNatStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpNatStaticNaptTable. */
INT1
nmhValidateIndexInstanceFsWtpNatStaticNaptTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNatStaticNaptTable  */

INT1
nmhGetFirstIndexFsWtpNatStaticNaptTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNatStaticNaptTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNatStaticNaptTranslatedLocalIp ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsWtpNatStaticNaptTranslatedLocalPort ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpNatStaticNaptDescription ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpNatStaticNaptEntryStatus ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNatStaticNaptTranslatedLocalIp ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsWtpNatStaticNaptTranslatedLocalPort ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpNatStaticNaptDescription ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpNatStaticNaptEntryStatus ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNatStaticNaptTranslatedLocalIp ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsWtpNatStaticNaptTranslatedLocalPort ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpNatStaticNaptDescription ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpNatStaticNaptEntryStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNatStaticNaptTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpNatIfTable. */
INT1
nmhValidateIndexInstanceFsWtpNatIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpNatIfTable  */

INT1
nmhGetFirstIndexFsWtpNatIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpNatIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpNatIfNat ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpNatIfNapt ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpNatIfTwoWayNat ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpNatIfEntryStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpNatIfNat ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpNatIfNapt ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpNatIfTwoWayNat ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpNatIfEntryStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpNatIfNat ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpNatIfNapt ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpNatIfTwoWayNat ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpNatIfEntryStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpNatIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpVlanStaticTable. */
INT1
nmhValidateIndexInstanceFsWtpVlanStaticTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpVlanStaticTable  */

INT1
nmhGetFirstIndexFsWtpVlanStaticTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpVlanStaticTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpVlanStaticName ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWtpVlanStaticRowStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpVlanStaticName ARG_LIST((UINT4  , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsWtpVlanStaticRowStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpVlanStaticName ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsWtpVlanStaticRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpVlanStaticTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpVlanStaticPortConfigTable. */
INT1
nmhValidateIndexInstanceFsWtpVlanStaticPortConfigTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpVlanStaticPortConfigTable  */

INT1
nmhGetFirstIndexFsWtpVlanStaticPortConfigTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpVlanStaticPortConfigTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpVlanStaticPort ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpVlanStaticPort ARG_LIST((UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpVlanStaticPort ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpVlanStaticPortConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsWtpIfMainTable. */
INT1
nmhValidateIndexInstanceFsWtpIfMainTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWtpIfMainTable  */

INT1
nmhGetFirstIndexFsWtpIfMainTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWtpIfMainTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWtpIfMainType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainMtu ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainAdminStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainOperStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainEncapType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainBrgPortType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainSubType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainNetworkType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfWanType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfMainEncapDot1qVlanId ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsWtpIfIpAddr ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsWtpIfIpSubnetMask ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsWtpIfIpBroadcastAddr ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsWtpIfMainPhyPort ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWtpIfMainType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainMtu ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainAdminStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainEncapType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainBrgPortType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainSubType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainNetworkType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfWanType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfMainEncapDot1qVlanId ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsWtpIfIpAddr ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsWtpIfIpSubnetMask ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsWtpIfIpBroadcastAddr ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsWtpIfMainPhyPort ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWtpIfMainType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainMtu ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainAdminStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainEncapType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainBrgPortType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainSubType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainNetworkType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfWanType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsWtpIfMainEncapDot1qVlanId ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWtpIfMainTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhTestv2FsWtpIfIpAddr ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsWtpIfIpSubnetMask ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsWtpIfIpBroadcastAddr ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsWtpIfMainPhyPort ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

