#ifndef _WSSCFGWLAN_H_
#define _WSSCFGWLAN_H_
/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsscfgwlanproto.h,v 1.3 2017/11/24 10:37:08 siva Exp $ 
 * 
 * Description: This file contains the WSS CFG function prototypes. 
 * 
 *****************************************************************************/

#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wssifradiodb.h"
#include "utilipvx.h"

INT4 WssCfgSetStationConfigTable PROTO(
     (tWsscfgDot11StationConfigEntry * pWsscfgDot11StationConfigEntry,
      tWsscfgIsSetDot11StationConfigEntry *
      pWsscfgIsSetDot11StationConfigEntry));

INT4 WssCfgSetFsDot11RadioConfigTable(
    tWsscfgFsDot11RadioConfigEntry * pWsscfgFsDot11RadioConfigEntry,
    tWsscfgIsSetFsDot11RadioConfigEntry
    * pWsscfgIsSetFsDot11RadioConfigEntry);
INT4 WssCfgSetCapabilityProfile(
    tWsscfgFsDot11CapabilityProfileEntry *
    pWsscfgFsDot11CapabilityProfileEntry,
    tWsscfgIsSetFsDot11CapabilityProfileEntry *
    pWsscfgIsSetFsDot11CapabilityProfileEntry);


INT4 WsscfgSetQosProfile(
    tWsscfgFsDot11WlanQosProfileEntry *
    pWsscfgFsDot11WlanQosProfileEntry,
    tWsscfgIsSetFsDot11WlanQosProfileEntry *
    pWsscfgIsSetFsDot11WlanQosProfileEntry);


INT4 WssCfgSetFsSecurityWebAuthGuestInfoTable(
    tWsscfgFsSecurityWebAuthGuestInfoEntry
    * pWsscfgFsSecurityWebAuthGuestInfoEntry,
    tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
    * pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry);

INT4 WssCfgSetRadioQosTable(
    tWsscfgFsDot11RadioQosEntry * pWsscfgFsDot11RadioQosEntry,
    tWsscfgIsSetFsDot11RadioQosEntry *
    pWsscfgIsSetFsDot11RadioQosEntry);

INT4 WsscfgSetManagmentSSID(
    INT4 u4FsDot11ManagmentSSID);
INT4 WssCfgGetEDCAParams(
    tRadioIfGetDB * pRadioIfGetDB);

INT4 WssCfgGetRadioParams(
    tRadioIfGetDB * pRadioIfGetDB);


INT4 WsscfgSetFsDot11WlanAuthenticationProfile(
    tWsscfgFsDot11WlanAuthenticationProfileEntry
    * pWsscfgFsDot11WlanAuthenticationProfileEntry,
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
    * pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry);

INT4 WsscfgSetFsDot11WlanQosProfile(
    tWsscfgFsDot11WlanQosProfileEntry *
    pWsscfgFsDot11WlanQosProfileEntry,
    tWsscfgIsSetFsDot11WlanQosProfileEntry *
    pWsscfgIsSetFsDot11WlanQosProfileEntry);
INT4 WsscfgSetDot11SpectrumManagementTable(
    tWsscfgDot11SpectrumManagementEntry
    * pWsscfgDot11SpectrumManagementEntry,
    tWsscfgIsSetDot11SpectrumManagementEntry
    * pWsscfgIsSetDot11SpectrumManagementEntry);

INT4 WsscfgSetFsDot11WlanCapabilityProfile(
    tWsscfgFsDot11WlanCapabilityProfileEntry *
    pWsscfgFsDot11WlanCapabilityProfileEntry,
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
    pWsscfgIsSetFsDot11WlanCapabilityProfileEntry);


INT4 WssCfgSetDot11AntennasList(
    tWsscfgDot11AntennasListEntry * pWsscfgDot11AntennasListEntry,
    tWsscfgIsSetDot11AntennasListEntry *
    pWsscfgIsSetDot11AntennasListEntry);


INT4 WssCfgSetQAPTable(
    tWsscfgFsDot11QAPEntry * pWsscfgFsDot11QAPEntry,
    tWsscfgIsSetFsDot11QAPEntry * pWsscfgIsSetFsDot11QAPEntry);

INT4 WssCfgGetDSSSTable(
    tWsscfgDot11PhyDSSSEntry * pWsscfgGetDot11PhyDSSSEntry);
INT4 WssCfgGetOFDMTable(
    tWsscfgDot11PhyOFDMEntry * pWsscfgGetDot11PhyOFDMEntry);

INT4 WssCfgSetAuthenticationAlgorithmTable PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *
      pWsscfgDot11AuthenticationAlgorithmsEntry,
      tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *
      pWsscfgIsSetDot11AuthenticationAlgorithmsEntry));

INT4 WssCfgSetWEPDefaultKeysTable PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *
      pWsscfgDot11WEPDefaultKeysEntry,
      tWsscfgIsSetDot11WEPDefaultKeysEntry *
      pWsscfgIsSetDot11WEPDefaultKeysEntry));

INT4 WssCfgSetWEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *
      pWsscfgDot11WEPKeyMappingsEntry,
      tWsscfgIsSetDot11WEPKeyMappingsEntry *
      pWsscfgIsSetDot11WEPKeyMappingsEntry));

INT4 WssCfgSetMultiDomainCapabilityTable PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *
      pWsscfgDot11MultiDomainCapabilityEntry,
      tWsscfgIsSetDot11MultiDomainCapabilityEntry *
      pWsscfgIsSetDot11MultiDomainCapabilityEntry));

INT4 WssCfgSetFsDot11AntennasListTable PROTO(
     (tWsscfgFsDot11AntennasListEntry *
      pWsscfgFsDot11AntennasListEntry,
      tWsscfgIsSetFsDot11AntennasListEntry *
      pWsscfgIsSetFsDot11AntennasListEntry));
INT4 WssCfgSetDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry * pWsscfgDot11OperationEntry,
      tWsscfgIsSetDot11OperationEntry *
      pWsscfgIsSetDot11OperationEntry));
INT4 WssCfgSetFsDot11nFsDot11nConfigTable(
    tWsscfgFsDot11nConfigEntry * pWsscfgFsDot11nConfigEntry,
    tWsscfgIsSetFsDot11nConfigEntry *
    pWsscfgIsSetFsDot11nConfigEntry);
INT4 WssCfgSetFsDot11nMCSDataRateTable(
    tWsscfgFsDot11nMCSDataRateEntry * pWsscfgFsDot11nMCSDataRateEntry,
    tWsscfgIsSetFsDot11nMCSDataRateEntry *
    pWsscfgIsSetFsDot11nMCSDataRateEntry);

INT4 WssCfgSetEDCATable PROTO(
     (tWsscfgDot11EDCAEntry * pWsscfgDot11EDCAEntry,
      tWsscfgIsSetDot11EDCAEntry * pWsscfgIsSetDot11EDCAEntry));

INT4 WssCfgSetTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry * pWsscfgDot11PhyTxPowerEntry,
      tWsscfgIsSetDot11PhyTxPowerEntry *
      pWsscfgIsSetDot11PhyTxPowerEntry));

INT4 WssCfgSetAntennasList PROTO(
     (tWsscfgDot11AntennasListEntry * pWsscfgDot11AntennasListEntry,
      tWsscfgIsSetDot11AntennasListEntry *
      pWsscfgIsSetDot11AntennasListEntry));

INT4 WssCfgSetDSSSTable PROTO(
     (tWsscfgDot11PhyDSSSEntry * pWsscfgDot11PhyDSSSEntry,
      tWsscfgIsSetDot11PhyDSSSEntry * pWsscfgIsSetDot11PhyDSSSEntry));

INT4 WssCfgSetOFDMTable PROTO(
     (tWsscfgDot11PhyOFDMEntry * pWsscfgDot11PhyOFDMEntry,
      tWsscfgIsSetDot11PhyOFDMEntry * pWsscfgIsSetDot11PhyOFDMEntry));

INT4 WssCfgSetWlanProfile PROTO(
     (tWsscfgCapwapDot11WlanEntry * pWsscfgCapwapDot11WlanEntry,
      tWsscfgIsSetCapwapDot11WlanEntry *
      pWsscfgIsSetCapwapDot11WlanEntry));

INT4 WssCfgSetWlanBinding PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *
      pWsscfgCapwapDot11WlanBindEntry,
      tWsscfgIsSetCapwapDot11WlanBindEntry *
      pWsscfgIsSetCapwapDot11WlanBindEntry));

INT4 WssCfgSetFsDot11StationConfigTable PROTO(
     (tWsscfgFsDot11StationConfigEntry *
      pWsscfgFsDot11StationConfigEntry,
      tWsscfgIsSetFsDot11StationConfigEntry *
      pWsscfgIsSetFsDot11StationConfigEntry));

INT4 WssCfgSetFsDot11ExternalWebAuthProfileTable PROTO(
        (tWsscfgFsDot11ExternalWebAuthProfileEntry *,
         tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *));

INT4 WssCfgSetFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry * pWsscfgFsDot11QAPEntry,
      tWsscfgIsSetFsDot11QAPEntry * pWsscfgIsSetFsDot11QAPEntry));


INT4 WsscfgSetFsDot11AntennasListTable PROTO(
     (tWsscfgFsDot11AntennasListEntry *
      pWsscfgFsDot11AntennasListEntry,
      tWsscfgIsSetFsDot11AntennasListEntry *
      pWsscfgIsSetFsDot11AntennasListEntry));

INT4 WssCfgSetVlanIsolation PROTO(
     (tWsscfgFsVlanIsolationEntry * pWsscfgFsVlanIsolationEntry,
      tWsscfgIsSetFsVlanIsolationEntry
      * pWsscfgIsSetFsVlanIsolationEntry));

INT4 WssCfgNetworkStatusChange PROTO(
     (INT4 i4FsDot11bNetworkEnable,
      UINT4 u4RadioType));

VOID WssCfgGetWlanCount PROTO(
     (UINT2 *));

VOID WssCfgGetWlanProfileId PROTO(
     (UINT2 u2WlanId,
      UINT2 * pu2WlanProfileId));

UINT1 WssCfgGetWlanInternlId PROTO(
     (UINT4 u4WlanProfileId,
      UINT2 * pu2WlanInternalId));

INT4 WssCfgSetFsDot11RadioTypeTable PROTO(
     (UINT4 u4RadioType));
INT4 WssCfgSetDot11Radiotype(
    tRadioIfGetDB * pRadioIfGetDB);


UINT1 WssCfgGetWlanIfIndexfromSSID PROTO(
     (UINT1 * pu1SSID,
      UINT4 * pu4WlanIfIndex));

UINT1 WssCfgGetWtpIdfromRadio PROTO(
     (UINT4 u4RadioIfIndex,
      UINT2 * pu2WtpInternalId,
      UINT4 * pu4RadioId));

UINT1 WssCfgGetWlanProfilefromIndex PROTO(
     (UINT4 u4WlanIfIndex,
      UINT2 * pu2WlanProfileId));

INT4 WssCfgGetTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry *));

UINT4 WssCfgDeleteWlanBinding PROTO ((UINT4 u4BssIfIndex));
UINT4 WssCfgDestroyDot11WlanBindTable PROTO ((UINT4 u4RadioIfIndex,
            UINT4 u4WlanProfileId));

INT4 WsscfgUtilImageTransfer PROTO ((INT1 * pTftpStr, INT1 *pi1FileName,
            tIPvXAddr * pIpAddress));

INT4 WssCfgSetLegacyRateStatus PROTO ((UINT4 *));
#endif
