/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrrmdb.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRRMDB_H
#define _FSRRMDB_H

#ifndef RFMGMT_WANTED
UINT1 FsRrmConfigTableINDEX[] = { SNMP_DATA_TYPE_INTEGER };

UINT4 fsrrm[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 84 };
tSNMP_OID_TYPE fsrrmOID = { 9, fsrrm };


UINT4 FsRrmRadioType[] =
    { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 1, 1, 1, 1 };
UINT4 FsRrmDcaMode[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 1, 1, 1, 2 };
UINT4 FsRrmDcaChannelSelectionMode[] =
    { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 1, 1, 1, 3 };
UINT4 FsRrmTpcMode[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 1, 1, 1, 4 };
UINT4 FsRrmTpcSelectionMode[] =
    { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 1, 1, 1, 5 };
UINT4 FsRrmRowStatus[] =
    { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 1, 1, 1, 6 };




tMbDbEntry fsrrmMibEntry[] = {

    {{13, FsRrmRadioType}
     , GetNextIndexFsRrmConfigTable, NULL, NULL, NULL, NULL,
     SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRrmConfigTableINDEX, 1,
     0, 0,
     NULL}
    ,

    {{13, FsRrmDcaMode}
     , GetNextIndexFsRrmConfigTable, FsRrmDcaModeGet, FsRrmDcaModeSet,
     FsRrmDcaModeTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "3"}
    ,

    {{13, FsRrmDcaChannelSelectionMode}
     , GetNextIndexFsRrmConfigTable, FsRrmDcaChannelSelectionModeGet,
     FsRrmDcaChannelSelectionModeSet,
     FsRrmDcaChannelSelectionModeTest,
     FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsRrmConfigTableINDEX, 1, 0, 0, "3"}
    ,

    {{13, FsRrmTpcMode}
     , GetNextIndexFsRrmConfigTable, FsRrmTpcModeGet, FsRrmTpcModeSet,
     FsRrmTpcModeTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "3"}
    ,

    {{13, FsRrmTpcSelectionMode}
     , GetNextIndexFsRrmConfigTable, FsRrmTpcSelectionModeGet,
     FsRrmTpcSelectionModeSet, FsRrmTpcSelectionModeTest,
     FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsRrmConfigTableINDEX, 1, 0, 0, "3"}
    ,

    {{13, FsRrmRowStatus}
     , GetNextIndexFsRrmConfigTable, FsRrmRowStatusGet,
     FsRrmRowStatusSet,
     FsRrmRowStatusTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 1, NULL}
    ,
};
tMibData fsrrmEntry = { 6, fsrrmMibEntry };

#endif
#endif                          /* _FSRRMDB_H */
