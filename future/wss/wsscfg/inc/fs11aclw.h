/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs11aclw.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsDot11ACConfigTable. */
INT1
nmhValidateIndexInstanceFsDot11ACConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11ACConfigTable  */

INT1
nmhGetFirstIndexFsDot11ACConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11ACConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11ACMaxMPDULength ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACMaxMPDULengthConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTMaxRxAMPDUFactor ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTMaxRxAMPDUFactorConfig ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTControlFieldSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTTXOPPowerSaveOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTRxMCSMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11ACVHTRxHighestDataRateSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTTxMCSMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11ACVHTTxHighestDataRateSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTOBSSScanCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACCurrentChannelBandwidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACCurrentChannelBandwidthConfig ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0Config ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTShortGIOptionIn80Implemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTShortGIOptionIn80Activated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Implemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Activated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTLDPCCodingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTLDPCCodingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTTxSTBCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTTxSTBCOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTRxSTBCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTRxSTBCOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTMUMaxUsersImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTMUMaxNSTSPerUserImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACVHTMUMaxNSTSTotalImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11ACSuBeamFormer ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACSuBeamFormee ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACMuBeamFormer ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACMuBeamFormee ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACVHTLinkAdaption ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACRxAntennaPattern ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACTxAntennaPattern ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ACBasicMCSMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11ACVHTRxMCSMapConfig ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11ACVHTTxMCSMapConfig ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1Config ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11VHTOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11OperatingModeNotificationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11ExtendedChannelSwitchActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDot11FragmentationThreshold ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11ACMaxMPDULength ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACMaxMPDULengthConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTMaxRxAMPDUFactor ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTControlFieldSupported ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTTXOPPowerSaveOptionImplemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTRxMCSMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot11ACVHTRxHighestDataRateSupported ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTTxMCSMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot11ACVHTTxHighestDataRateSupported ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTOBSSScanCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACCurrentChannelBandwidth ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACCurrentChannelBandwidthConfig ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0 ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1 ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTShortGIOptionIn80Implemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTShortGIOptionIn80Activated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Implemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Activated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTLDPCCodingOptionImplemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTLDPCCodingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTTxSTBCOptionImplemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTTxSTBCOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTRxSTBCOptionImplemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTRxSTBCOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTMUMaxUsersImplemented ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTMUMaxNSTSPerUserImplemented ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACVHTMUMaxNSTSTotalImplemented ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11ACSuBeamFormer ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACSuBeamFormee ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACMuBeamFormer ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACMuBeamFormee ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACVHTLinkAdaption ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACRxAntennaPattern ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACTxAntennaPattern ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ACBasicMCSMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot11ACVHTRxMCSMapConfig ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot11ACVHTTxMCSMapConfig ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1Config ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11VHTOptionImplemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11OperatingModeNotificationImplemented ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11ExtendedChannelSwitchActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDot11FragmentationThreshold ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11ACMaxMPDULength ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACMaxMPDULengthConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTMaxRxAMPDUFactor ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTControlFieldSupported ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTTXOPPowerSaveOptionImplemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTRxMCSMap ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot11ACVHTRxHighestDataRateSupported ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTTxMCSMap ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot11ACVHTTxHighestDataRateSupported ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTOBSSScanCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACCurrentChannelBandwidth ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACCurrentChannelBandwidthConfig ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0 ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1 ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn80Implemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Implemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Activated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTLDPCCodingOptionImplemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTTxSTBCOptionImplemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTTxSTBCOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTRxSTBCOptionImplemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTRxSTBCOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTMUMaxUsersImplemented ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTMUMaxNSTSPerUserImplemented ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACVHTMUMaxNSTSTotalImplemented ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11ACSuBeamFormer ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACSuBeamFormee ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACMuBeamFormer ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACMuBeamFormee ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACVHTLinkAdaption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACRxAntennaPattern ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACTxAntennaPattern ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ACBasicMCSMap ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot11ACVHTRxMCSMapConfig ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot11ACVHTTxMCSMapConfig ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1Config ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11VHTOptionImplemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11OperatingModeNotificationImplemented ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11ExtendedChannelSwitchActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDot11FragmentationThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11ACConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot11VHTStationConfigTable. */
INT1
nmhValidateIndexInstanceFsDot11VHTStationConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11VHTStationConfigTable  */

INT1
nmhGetFirstIndexFsDot11VHTStationConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11VHTStationConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11VHTRxHighestDataRateConfig ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11VHTTxHighestDataRateConfig ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11VHTRxHighestDataRateConfig ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11VHTTxHighestDataRateConfig ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11VHTRxHighestDataRateConfig ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11VHTTxHighestDataRateConfig ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11VHTStationConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDot11PhyVHTTable. */
INT1
nmhValidateIndexInstanceFsDot11PhyVHTTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDot11PhyVHTTable  */

INT1
nmhGetFirstIndexFsDot11PhyVHTTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDot11PhyVHTTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDot11NumberOfSpatialStreamsImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDot11NumberOfSpatialStreamsActivated ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDot11NumberOfSpatialStreamsImplemented ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsDot11NumberOfSpatialStreamsActivated ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDot11NumberOfSpatialStreamsImplemented ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsDot11NumberOfSpatialStreamsActivated ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDot11PhyVHTTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
