/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: wsscfgextn.h,v 1.2 2017/11/24 10:37:07 siva Exp $
 * Description: This file contains extern declaration of global 
 *              variables of the wsscfg module.
 *******************************************************************/

#ifndef __WSSCFGEXTN_H__
#define __WSSCFGEXTN_H__

PUBLIC tWsscfgGlobals gWsscfgGlobals;
PUBLIC tWsscfgGlobals gWsscfgGlobal;
PUBLIC tApGroupGlobals gApGroupGlobals;
extern CHR1 ac1Command[256];
extern UINT1 au1DatabaseName[256];
#endif/*__WSSCFGEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file wsscfgextn.h                      */
/*-----------------------------------------------------------------------*/

