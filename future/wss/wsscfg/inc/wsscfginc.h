/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsscfginc.h,v 1.3 2017/11/24 10:37:07 siva Exp $
 *
 * Description: This file contains include files of wsscfg module.
 *******************************************************************/

#ifndef __WSSCFGINC_H__
#define __WSSCFGINC_H__

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "params.h"
#include "wsscf.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fscfalw.h"
#include "fs1acwrp.h"

#include "wsscfgdefn.h"
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfgtmr.h"
#include "wsscfgtrc.h"
#include "wsscfgwlanproto.h"

#include "wsscfglock.h"

#include "wsswlaninc.h"
#include "radioifinc.h"
#ifdef WLC_WANTED
#include "wssstawlcprot.h"
#endif
#include "wssifpmdb.h"
#include "wssifcapdb.h"

#ifdef __WSSCFGMAIN_C__

#include "wsscfgglob.h"
#include "wsscfgcli.h"
#include "std802wr.h"
#include "wsscfglwg.h"
#include "std802lw.h"
#include "wsscfgdefg.h"
#include "wsscfgsz.h"
#include "wsscfgwrg.h"
#include "fs11acwr.h"
#include "fs11acwr.h"
#include "fswsslwr.h"
#else

#include "wsscfgextn.h"
#include "wsscfgmibclig.h"
#include "wsscfglwg.h"
#include "std802wr.h"
#include "std802lw.h"
#include "fs11ndb.h"
#include "wsscfgdefg.h"
#include "wsscfgsz.h"
#include "wsscfgwrg.h"
#include "fswsslwr.h"

#endif                          /* __WSSCFGMAIN_C__ */
#include "wsscfgprot.h"
#include "wsscfgprotg.h"
#define WAN_TYPE_OTHERS 0
#define WAN_TYPE_PRIVATE 2
#define WAN_TYPE_PUBLIC 1
#define NAT_CONFIG_INDEX_LOWER_LIMIT 1
#define NAT_CONFIG_INDEX_UPPER_LIMIT 24
#endif                          /* __WSSCFGINC_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file wsscfginc.h                       */
/*-----------------------------------------------------------------------*/
