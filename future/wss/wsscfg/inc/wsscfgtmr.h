/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsscfgtmr.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $ 
 *
 * Description: This file contains definitions for wsscfg Timer
 *******************************************************************/

#ifndef __WSSCFGTMR_H__
#define __WSSCFGTMR_H__



/* constants for timer types */
typedef enum {
    WSSCFG_TMR1 = 0,
    WSSCFG_MAX_TMRS = 1
} enWsscfgTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _WSSCFG_TMR_DESC {
    VOID( *pTmrExpFunc) (VOID *);
    INT2 i2Offset;
    /* If this field is -1 then the fn takes no
     * parameter
     */
    UINT2 u2Pad;
    /* Included for 4-byte Alignment
     */
} tWsscfgTmrDesc;

typedef struct _WSSCFG_TIMER {
    tTmrAppTimer tmrNode;
    enWsscfgTmrId eWsscfgTmrId;
} tWsscfgTmr;




#endif                          /* __WSSCFGTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgtmr.h                      */
/*-----------------------------------------------------------------------*/
