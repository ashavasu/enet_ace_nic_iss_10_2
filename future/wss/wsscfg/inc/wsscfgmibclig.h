/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
* 
*  $Id: wsscfgmibclig.h,v 1.2 2017/11/24 10:37:08 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 Dot11StationID[0];

extern UINT4 Dot11MediumOccupancyLimit[0];

extern UINT4 Dot11CFPPeriod[0];

extern UINT4 Dot11CFPMaxDuration[0];

extern UINT4 Dot11AuthenticationResponseTimeOut[0];

extern UINT4 Dot11PowerManagementMode[0];

extern UINT4 Dot11DesiredSSID[0];

extern UINT4 Dot11DesiredBSSType[0];

extern UINT4 Dot11OperationalRateSet[0];

extern UINT4 Dot11BeaconPeriod[0];

extern UINT4 Dot11DTIMPeriod[0];

extern UINT4 Dot11AssociationResponseTimeOut[0];

extern UINT4 Dot11MultiDomainCapabilityImplemented[0];

extern UINT4 Dot11MultiDomainCapabilityActivated[0];

extern UINT4 Dot11SpectrumManagementRequired[0];

extern UINT4 Dot11OperatingClassesImplemented[0];

extern UINT4 Dot11OperatingClassesRequired[0];

extern UINT4 Dot11AssociateInNQBSS[0];

extern UINT4 Dot11DLSAllowedInQBSS[0];

extern UINT4 Dot11DLSAllowed[0];

extern UINT4 IfIndex[0];

extern UINT4 Dot11AuthenticationAlgorithmsIndex[0];

extern UINT4 Dot11AuthenticationAlgorithmsActivated[0];

extern UINT4 Dot11WEPDefaultKeyIndex[0];

extern UINT4 Dot11WEPDefaultKeyValue[0];

extern UINT4 Dot11WEPKeyMappingIndex[0];

extern UINT4 Dot11WEPKeyMappingAddress[0];

extern UINT4 Dot11WEPKeyMappingWEPOn[0];

extern UINT4 Dot11WEPKeyMappingValue[0];

extern UINT4 Dot11WEPKeyMappingStatus[0];

extern UINT4 Dot11PrivacyInvoked[0];

extern UINT4 Dot11WEPDefaultKeyID[0];

extern UINT4 Dot11WEPKeyMappingLengthImplemented[0];

extern UINT4 Dot11ExcludeUnencrypted[0];

extern UINT4 Dot11RSNAActivated[0];

extern UINT4 Dot11RSNAPreauthenticationActivated[0];

extern UINT4 Dot11MultiDomainCapabilityIndex[0];

extern UINT4 Dot11FirstChannelNumber[0];

extern UINT4 Dot11NumberofChannels[0];

extern UINT4 Dot11MaximumTransmitPowerLevel[0];

extern UINT4 Dot11SpectrumManagementIndex[0];

extern UINT4 Dot11MitigationRequirement[0];

extern UINT4 Dot11ChannelSwitchTime[0];

extern UINT4 Dot11OperatingClassesIndex[0];

extern UINT4 Dot11OperatingClass[0];

extern UINT4 Dot11CoverageClass[0];

extern UINT4 Dot11RTSThreshold[0];

extern UINT4 Dot11ShortRetryLimit[0];

extern UINT4 Dot11LongRetryLimit[0];

extern UINT4 Dot11FragmentationThreshold[0];

extern UINT4 Dot11MaxTransmitMSDULifetime[0];

extern UINT4 Dot11MaxReceiveLifetime[0];

extern UINT4 Dot11CAPLimit[0];

extern UINT4 Dot11HCCWmin[0];

extern UINT4 Dot11HCCWmax[0];

extern UINT4 Dot11HCCAIFSN[0];

extern UINT4 Dot11ADDBAResponseTimeout[0];

extern UINT4 Dot11ADDTSResponseTimeout[0];

extern UINT4 Dot11ChannelUtilizationBeaconInterval[0];

extern UINT4 Dot11ScheduleTimeout[0];

extern UINT4 Dot11DLSResponseTimeout[0];

extern UINT4 Dot11QAPMissingAckRetryLimit[0];

extern UINT4 Dot11EDCAAveragingPeriod[0];

extern UINT4 Dot11GroupAddressesIndex[0];

extern UINT4 Dot11Address[0];

extern UINT4 Dot11GroupAddressesStatus[0];

extern UINT4 Dot11EDCATableIndex[0];

extern UINT4 Dot11EDCATableCWmin[0];

extern UINT4 Dot11EDCATableCWmax[0];

extern UINT4 Dot11EDCATableAIFSN[0];

extern UINT4 Dot11EDCATableTXOPLimit[0];

extern UINT4 Dot11EDCATableMSDULifetime[0];

extern UINT4 Dot11EDCATableMandatory[0];

extern UINT4 Dot11QAPEDCATableIndex[0];

extern UINT4 Dot11QAPEDCATableCWmin[0];

extern UINT4 Dot11QAPEDCATableCWmax[0];

extern UINT4 Dot11QAPEDCATableAIFSN[0];

extern UINT4 Dot11QAPEDCATableTXOPLimit[0];

extern UINT4 Dot11QAPEDCATableMSDULifetime[0];

extern UINT4 Dot11QAPEDCATableMandatory[0];

extern UINT4 Dot11QosCountersIndex[0];

extern UINT4 Dot11CurrentRegDomain[0];

extern UINT4 Dot11CurrentTxAntenna[0];

extern UINT4 Dot11CurrentRxAntenna[0];

extern UINT4 Dot11CurrentTxPowerLevel[0];

extern UINT4 Dot11CurrentChannelNumber[0];

extern UINT4 Dot11CurrentDwellTime[0];

extern UINT4 Dot11CurrentSet[0];

extern UINT4 Dot11CurrentPattern[0];

extern UINT4 Dot11CurrentIndex[0];

extern UINT4 Dot11EHCCPrimeRadix[0];

extern UINT4 Dot11EHCCNumberofChannelsFamilyIndex[0];

extern UINT4 Dot11EHCCCapabilityImplemented[0];

extern UINT4 Dot11EHCCCapabilityActivated[0];

extern UINT4 Dot11HopAlgorithmAdopted[0];

extern UINT4 Dot11RandomTableFlag[0];

extern UINT4 Dot11HopOffset[0];

extern UINT4 Dot11CurrentChannel[0];

extern UINT4 Dot11CurrentCCAMode[0];

extern UINT4 Dot11EDThreshold[0];

extern UINT4 Dot11CCAWatchdogTimerMax[0];

extern UINT4 Dot11CCAWatchdogCountMax[0];

extern UINT4 Dot11CCAWatchdogTimerMin[0];

extern UINT4 Dot11CCAWatchdogCountMin[0];

extern UINT4 Dot11RegDomainsSupportedIndex[0];

extern UINT4 Dot11AntennaListIndex[0];

extern UINT4 Dot11TxAntennaImplemented[0];

extern UINT4 Dot11RxAntennaImplemented[0];

extern UINT4 Dot11DiversitySelectionRxImplemented[0];

extern UINT4 Dot11SupportedDataRatesTxIndex[0];

extern UINT4 Dot11SupportedDataRatesRxIndex[0];

extern UINT4 Dot11CurrentFrequency[0];

extern UINT4 Dot11TIThreshold[0];

extern UINT4 Dot11ChannelStartingFactor[0];

extern UINT4 Dot11PhyOFDMChannelWidth[0];

extern UINT4 Dot11HoppingPatternIndex[0];

extern UINT4 Dot11RandomTableFieldNumber[0];

extern UINT4 Dot11ERPBCCOptionActivated[0];

extern UINT4 Dot11DSSSOFDMOptionActivated[0];

extern UINT4 Dot11ShortSlotTimeOptionImplemented[0];

extern UINT4 Dot11ShortSlotTimeOptionActivated[0];

extern UINT4 Dot11RSNAConfigGroupCipher[0];

extern UINT4 Dot11RSNAConfigGroupRekeyMethod[0];

extern UINT4 Dot11RSNAConfigGroupRekeyTime[0];

extern UINT4 Dot11RSNAConfigGroupRekeyPackets[0];

extern UINT4 Dot11RSNAConfigGroupRekeyStrict[0];

extern UINT4 Dot11RSNAConfigPSKValue[0];

extern UINT4 Dot11RSNAConfigPSKPassPhrase[0];

extern UINT4 Dot11RSNAConfigGroupUpdateCount[0];

extern UINT4 Dot11RSNAConfigPairwiseUpdateCount[0];

extern UINT4 Dot11RSNAConfigPMKLifetime[0];

extern UINT4 Dot11RSNAConfigPMKReauthThreshold[0];

extern UINT4 Dot11RSNAConfigSATimeout[0];

extern UINT4 Dot11RSNATKIPCounterMeasuresInvoked[0];

extern UINT4 Dot11RSNA4WayHandshakeFailures[0];

extern UINT4 Dot11RSNAConfigSTKRekeyTime[0];

extern UINT4 Dot11RSNAConfigSMKUpdateCount[0];

extern UINT4 Dot11RSNAConfigSMKLifetime[0];

extern UINT4 Dot11RSNASMKHandshakeFailures[0];

extern UINT4 Dot11RSNAConfigPairwiseCipherIndex[0];

extern UINT4 Dot11RSNAConfigPairwiseCipherActivated[0];

extern UINT4 Dot11RSNAConfigAuthenticationSuiteIndex[0];

extern UINT4 Dot11RSNAConfigAuthenticationSuiteActivated[0];

extern UINT4 Dot11RSNAStatsIndex[0];


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 CapwapDot11WlanProfileId[0];

extern UINT4 CapwapDot11WlanMacType[0];

extern UINT4 CapwapDot11WlanTunnelMode[0];

extern UINT4 CapwapDot11WlanRowStatus[0];

extern UINT4 CapwapDot11WlanBindRowStatus[0];

extern UINT4 FsRrmRadioType[0];

extern UINT4 FsRrmDcaMode[0];

extern UINT4 FsRrmDcaChannelSelectionMode[0];

extern UINT4 FsRrmTpcMode[0];

extern UINT4 FsRrmTpcSelectionMode[0];

extern UINT4 FsRrmRowStatus[0];

/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsDot11aNetworkEnable[0];

extern UINT4 FsDot11bNetworkEnable[0];

extern UINT4 FsDot11gSupport[0];

extern UINT4 FsDot11anSupport[0];

extern UINT4 FsDot11bnSupport[0];

extern UINT4 FsDot11ManagmentSSID[0];

extern UINT4 FsDot11CountryString[0];

extern UINT4 FsSecurityWebAuthType[0];

extern UINT4 FsSecurityWebAuthUrl[0];

extern UINT4 FsSecurityWebAuthRedirectUrl[0];

extern UINT4 FsSecurityWebAddr[0];

extern UINT4 FsSecurityWebAuthWebTitle[0];

extern UINT4 FsSecurityWebAuthWebMessage[0];

extern UINT4 FsSecurityWebAuthWebLogoFileName[0];

extern UINT4 FsSecurityWebAuthWebFailMessage[0];

extern UINT4 FsSecurityWebAuthWebLoadBalInfo[0];

extern UINT4 FsSecurityWebAuthColor[0];

extern UINT4 FsSecurityWebAuthUName[0];

extern UINT4 FsSecurityWebAuthPasswd[0];

/* extern UINT4 FsSecurityWebAuthType[0];

extern UINT4 FsSecurityWebAuthUrl[0];

extern UINT4 FsSecurityWebAddr[0]; */

extern UINT4 FsSecurityWebAuthWebSuccMessage[0];

extern UINT4 FsSecurityWebAuthWebButtonText[0];

extern UINT4 FsSecurityWebAuthDisplayLang[0];

extern UINT4 FsSecurityWebAuthUserLifetime[0];

extern UINT4 FsSecurityWebAuthGuestInfoRowStatus[0];

extern UINT4 FsDot11SupressSSID[0];

extern UINT4 FsDot11VlanId[0];

extern UINT4 FsDot11BandwidthThresh[0];

extern UINT4 FsDot11CapabilityProfileName[0];

extern UINT4 FsDot11CFPollable[0];

extern UINT4 FsDot11CFPollRequest[0];

extern UINT4 FsDot11PrivacyOptionImplemented[0];

extern UINT4 FsDot11ShortPreambleOptionImplemented[0];

extern UINT4 FsDot11PBCCOptionImplemented[0];

extern UINT4 FsDot11ChannelAgilityPresent[0];

extern UINT4 FsDot11QosOptionImplemented[0];

extern UINT4 FsDot11SpectrumManagementRequired[0];

extern UINT4 FsDot11ShortSlotTimeOptionImplemented[0];

extern UINT4 FsDot11APSDOptionImplemented[0];

extern UINT4 FsDot11DSSSOFDMOptionEnabled[0];

extern UINT4 FsDot11DelayedBlockAckOptionImplemented[0];

extern UINT4 FsDot11ImmediateBlockAckOptionImplemented[0];

extern UINT4 FsDot11QAckOptionImplemented[0];

extern UINT4 FsDot11QueueRequestOptionImplemented[0];

extern UINT4 FsDot11TXOPRequestOptionImplemented[0];

extern UINT4 FsDot11RSNAOptionImplemented[0];

extern UINT4 FsDot11RSNAPreauthenticationImplemented[0];

extern UINT4 FsDot11CapabilityRowStatus[0];

extern UINT4 FsDot11AuthenticationProfileName[0];

extern UINT4 FsDot11AuthenticationAlgorithm[0];

extern UINT4 FsDot11WepKeyIndex[0];

extern UINT4 FsDot11WepKeyType[0];

extern UINT4 FsDot11WepKeyLength[0];

extern UINT4 FsDot11WepKey[0];

extern UINT4 FsDot11WebAuthentication[0];

extern UINT4 FsDot11AuthenticationRowStatus[0];


extern UINT4 FsSecurityWlanProfileId[0];


extern UINT4 FsStaMacAddress[0];

extern UINT4 FsStaQoSPriority[0];

extern UINT4 FsStaQoSDscp[0];

extern UINT4 FsVlanIsolation[0];

extern UINT4 FsDot11RadioType[0];

extern UINT4 FsDot11RowStatus[0];

extern UINT4 FsDot11QosProfileName[0];

extern UINT4 FsDot11QosTraffic[0];

extern UINT4 FsDot11QosPassengerTrustMode[0];

extern UINT4 FsDot11QosRateLimit[0];

extern UINT4 FsDot11UpStreamCIR[0];

extern UINT4 FsDot11UpStreamCBS[0];

extern UINT4 FsDot11UpStreamEIR[0];

extern UINT4 FsDot11UpStreamEBS[0];

extern UINT4 FsDot11DownStreamCIR[0];

extern UINT4 FsDot11DownStreamCBS[0];

extern UINT4 FsDot11DownStreamEIR[0];

extern UINT4 FsDot11DownStreamEBS[0];

extern UINT4 FsDot11QosRowStatus[0];

extern UINT4 FsDot11WlanCFPollable[0];

extern UINT4 FsDot11WlanCFPollRequest[0];

extern UINT4 FsDot11WlanPrivacyOptionImplemented[0];

extern UINT4 FsDot11WlanShortPreambleOptionImplemented[0];

extern UINT4 FsDot11WlanPBCCOptionImplemented[0];

extern UINT4 FsDot11WlanChannelAgilityPresent[0];

extern UINT4 FsDot11WlanQosOptionImplemented[0];

extern UINT4 FsDot11WlanSpectrumManagementRequired[0];

extern UINT4 FsDot11WlanShortSlotTimeOptionImplemented[0];

extern UINT4 FsDot11WlanAPSDOptionImplemented[0];

extern UINT4 FsDot11WlanDSSSOFDMOptionEnabled[0];

extern UINT4 FsDot11WlanDelayedBlockAckOptionImplemented[0];

extern UINT4 FsDot11WlanImmediateBlockAckOptionImplemented[0];

extern UINT4 FsDot11WlanQAckOptionImplemented[0];

extern UINT4 FsDot11WlanQueueRequestOptionImplemented[0];

extern UINT4 FsDot11WlanTXOPRequestOptionImplemented[0];

extern UINT4 FsDot11WlanRSNAOptionImplemented[0];

extern UINT4 FsDot11WlanRSNAPreauthenticationImplemented[0];

extern UINT4 FsDot11WlanCapabilityRowStatus[0];

extern UINT4 FsDot11WlanAuthenticationAlgorithm[0];

extern UINT4 FsDot11WlanWepKeyIndex[0];

extern UINT4 FsDot11WlanWepKeyType[0];

extern UINT4 FsDot11WlanWepKeyLength[0];

extern UINT4 FsDot11WlanWepKey[0];

extern UINT4 FsDot11WlanWebAuthentication[0];

extern UINT4 FsDot11WlanAuthenticationRowStatus[0];

extern UINT4 FsDot11WlanQosTraffic[0];

extern UINT4 FsDot11WlanQosPassengerTrustMode[0];

extern UINT4 FsDot11WlanQosRateLimit[0];

extern UINT4 FsDot11WlanUpStreamCIR[0];

extern UINT4 FsDot11WlanUpStreamCBS[0];

extern UINT4 FsDot11WlanUpStreamEIR[0];

extern UINT4 FsDot11WlanUpStreamEBS[0];

extern UINT4 FsDot11WlanDownStreamCIR[0];

extern UINT4 FsDot11WlanDownStreamCBS[0];

extern UINT4 FsDot11WlanDownStreamEIR[0];

extern UINT4 FsDot11WlanDownStreamEBS[0];

extern UINT4 FsDot11WlanQosRowStatus[0];

extern UINT4 FsDot11TaggingPolicy[0];

extern UINT4 FsDot11QueueDepth[0];

extern UINT4 FsDot11PriorityValue[0];

extern UINT4 FsDot11DscpValue[0];

extern UINT4 FsQAPProfileName[0];

extern UINT4 FsQAPProfileIndex[0];

extern UINT4 FsQAPProfileCWmin[0];

extern UINT4 FsQAPProfileCWmax[0];

extern UINT4 FsQAPProfileAIFSN[0];

extern UINT4 FsQAPProfileTXOPLimit[0];

extern UINT4 FsQAPProfileAdmissionControl[0];

extern UINT4 FsQAPProfilePriorityValue[0];

extern UINT4 FsQAPProfileDscpValue[0];

extern UINT4 FsQAPProfileRowStatus[0];

extern UINT4 FsDot11CapabilityMappingProfileName[0];

extern UINT4 FsDot11CapabilityMappingRowStatus[0];

extern UINT4 FsDot11AuthMappingProfileName[0];

extern UINT4 FsDot11AuthMappingRowStatus[0];

extern UINT4 FsDot11QosMappingProfileName[0];

extern UINT4 FsDot11QosMappingRowStatus[0];

extern UINT4 FsDot11ClientMacAddress[0];

extern UINT4 FsAntennaMode[0];

extern UINT4 FsAntennaSelection[0];

extern UINT4 FsDot11WlanProfileIfIndex[0];

extern UINT4 FsDot11WlanRowStatus[0];

extern UINT4 FsDot11WlanBindWlanId[0];

extern UINT4 FsDot11WlanBindBssIfIndex[0];

extern UINT4 FsDot11WlanBindRowStatus[0];

extern UINT4 FsWtpImageVersion[0];

extern UINT4 FsWtpUpgradeDev[0];

extern UINT4 FsWtpName[0];

extern UINT4 FsWtpImageName[0];

extern UINT4 FsWtpAddressType[0];

extern UINT4 FsWtpServerIP[0];

extern UINT4 FsWtpRowStatus[0];

extern UINT4 CapwapBaseWtpProfileWtpModelNumber[0];

extern UINT4 FsDot11nConfigShortGIfor20MHz[0];

extern UINT4 FsDot11nConfigShortGIfor40MHz[0];

extern UINT4 FsDot11nConfigChannelWidth[0];

extern UINT4 FsDot11nMCSDataRateIndex[0];

extern UINT4 FsDot11nMCSDataRate[0];
