/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: wsscfgglob.h,v 1.2 2017/11/24 10:37:07 siva Exp $
 * Description: This file contains declaration of global variables 
 *              of wsscfg module.
 *******************************************************************/

#ifndef __WSSCFGGLOBAL_H__
#define __WSSCFGGLOBAL_H__

tWsscfgGlobals gWsscfgGlobals;
tApGroupGlobals gApGroupGlobals;

CHR1 ac1Command[256];
UINT1 au1DatabaseName[256];
#endif  /* __WSSCFGGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file wsscfgglob.h                      */
/*-----------------------------------------------------------------------*/
