#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |    $Id: make.h,v 1.2 2017/05/23 14:16:54 siva Exp $                                                                 |
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

WSSCFG_BASE_DIR = ${BASE_DIR}/wss/wsscfg
WSSCFG_INC_DIR  = ${WSSCFG_BASE_DIR}/inc
WSSCFG_SRC_DIR  = ${WSSCFG_BASE_DIR}/src
WSSCFG_OBJ_DIR  = ${WSSCFG_BASE_DIR}/obj
WSSIF_INC_DIR  = ${BASE_DIR}/wss/wssif/inc
FIREWALL_INC_DIR = ${BASE_DIR}/firewall/inc/
WSSWLAN_INC_DIR  = ${BASE_DIR}/wss/wsswlan/inc
RADIOIF_INC_DIR  = ${BASE_DIR}/wss/radioif/inc
WSSMAC_INC_DIR  = ${BASE_DIR}/wss/wssmac/inc
BCNMGR_INC_DIR  = ${BASE_DIR}/wss/bcnmgr/inc
APHDLR_INC_DIR = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR = ${BASE_DIR}/wss/wlchdlr/inc
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
WSSAUTH_INC_DIR = ${BASE_DIR}/wss/wssauth/inc
WSSSTA_INC_DIR = ${BASE_DIR}/wss/wsssta/inc
WSSPM_INC_DIR = ${BASE_DIR}/wss/wsspm/inc
CFA_INC_DIR = ${BASE_DIR}/cfa2/inc
RFMGMT_INC_DIR = ${BASE_DIR}/wss/rfmgmt/inc
ISS_INC_DIR = ${BASE_DIR}/ISS/common/system/inc

CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc

WSSCFGINCL= \
            -I$(WSSCFG_BASE_DIR)\
            -I$(WSSCFG_BASE_DIR)/inc\
            -I$(WSSCFG_BASE_DIR)/src

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${CFA_INC_DIR} -I${WSSCFG_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR}  -I${WSSWLAN_INC_DIR}        -I${WSSIF_INC_DIR}    -I${FIREWALL_INC_DIR}  -I${RADIOIF_INC_DIR} -I${BCNMGR_INC_DIR}     -I${APHDLR_INC_DIR} -I${WLCHDLR_INC_DIR}    -I${CAPWAP_INC_DIR}     -I${WSSMAC_INC_DIR}     -I${WSSAUTH_INC_DIR}    -I${WSSSTA_INC_DIR} -I${WSSPM_INC_DIR}  -I${ISS_INC_DIR} -I${RFMGMT_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
