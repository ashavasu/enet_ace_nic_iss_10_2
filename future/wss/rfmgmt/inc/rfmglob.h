/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmglob.h,v 1.3 2017/05/23 14:16:52 siva Exp $
 *
 * Description: This file contains type global variables used 
 *              in rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RFMGMT_GLOB_H_
#define __RFMGMT_GLOB_H_


#ifdef ROGUEAP_WANTED
#define MAX_ROGUE_AP 128
#define MAX_ROGUE_AP_RULE 128
#define ENABLED    1
#define DISABLED   0
#define ZERO 0
#define ONE 1
#define MAX_SSID_LEN 32
#define RF_GROUP_NAME_LENGTH 19
#define DEFAULT_PROTECTION 2
#define DEFAULT_RSSI -129
#define DEFAULT_CLIENT_COUNT 76
#define DEFAULT_DURATION 3601
#define MAX_PRIORITY_COUNT 128
#define ROGUE_AP_NONE 1
#define ROGUE_AP_CONTAIN 3
#define ROGUE_AP_ALERT 2
#endif 
tRfMgmtGlobals       gRfMgmtGlobals;
UINT4        gu4ChannelSwitchMsgStatus;
UINT4                gu4TxPowerTrapStatus =
                 RFMGMT_TX_POWER_CHANGE_TRAP_DISABLE ;
UINT4              gu4ChannelChangeTrapStatus = 
                 RFMGMT_CHANNEL_CHANGE_TRAP_DISABLE;
UINT4                gu4RfMgmtTrace; 
UINT1                gu1IsRfMgmtResponseReceived;
UINT4                 gu4RfmSysLogId;

#ifdef WLC_WANTED
#ifdef ROGUEAP_WANTED
UINT1    gu1RogueRuleCount = 0;
#endif 
#endif 
UINT1                 gau1Dot11aChannel [RFMGMT_MAX_CHANNELA] =
{36, 40, 44, 48, 52, 56, 60, 64, 100, 
104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 149, 153, 157, 161, 165, 184, 188, 192, 196};

UINT1 gau1Dot11bChannel [RFMGMT_MAX_CHANNELB] =
{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};

#ifdef ROGUEAP_WANTED
UINT1          gu1RfGroupName[RF_GROUP_NAME_LENGTH];
UINT1          gu1RogueDetection = 0;
UINT1          gu1RogueTrap = 2;
UINT4          gu4RogueTimeOut = 240;
#endif 
#ifdef WLC_WANTED
#define RFMGMT_MAX_CHANNEL_TYPES 3 /* 802.11a, 802.11b, 802.11g */
tDcaAlgorithm   gaDcaAlgorithm [RFMGMT_MAX_CHANNEL_TYPES] 
                               [RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE] 
                                [RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE];
#endif
#endif
