/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmextn.h,v 1.2 2017/05/23 14:16:52 siva Exp $
 *
 * Description: This file contains type global variables used 
 *              in rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RFMGMT_EXTN_H_
#define __RFMGMT_EXTN_H_
#include "rfmconst.h"

#ifdef WLC_WANTED
#include "rfactdfs.h"
#endif 


PUBLIC tRfMgmtGlobals       gRfMgmtGlobals;
PUBLIC UINT4       gu4ChannelSwitchMsgStatus;
PUBLIC UINT4                gu4TxPowerTrapStatus;
PUBLIC UINT4             gu4ChannelChangeTrapStatus;
PUBLIC UINT4                gu4RfMgmtTrace;
PUBLIC UINT1                gu1IsRfMgmtResponseReceived;
PUBLIC UINT4                 gu4RfmSysLogId;
PUBLIC UINT1 gau1Dot11aChannel [RFMGMT_MAX_CHANNELA];
PUBLIC UINT1 gau1Dot11bChannel [RFMGMT_MAX_CHANNELB];
#ifdef ROGUEAP_WANTED
#define RF_GROUP_NAME_LENGTH 19
PUBLIC UINT1          gu1RfGroupName[RF_GROUP_NAME_LENGTH];
PUBLIC UINT1          gu1RogueDetection;
PUBLIC UINT4          gu4RogueTimeOut;
PUBLIC UINT1          gu1RogueTrap;
PUBLIC UINT1          gu1RogueRuleCount;
#endif 

#ifdef WLC_WANTED
#define RFMGMT_MAX_CHANNEL_TYPES 3 /* 802.11a, 802.11b, 802.11g */
#define MAX_RFMGMT_EXTERNAL_AP_SIZE 32
PUBLIC tDcaAlgorithm   gaDcaAlgorithm [RFMGMT_MAX_CHANNEL_TYPES] 
                            [RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE] 
                            [RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE];
#endif
#endif
