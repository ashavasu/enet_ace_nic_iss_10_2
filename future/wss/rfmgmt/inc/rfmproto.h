/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmproto.h,v 1.3 2017/11/24 10:37:04 siva Exp $
 *
 * Description: This file contains  prototypes of functions used in
 *              rfmgmt module
 *                            
 ********************************************************************/
#ifndef _RFMGMTPROTO_H
#define _RFMGMTPROTO_H

#include "fswebnm.h"

#ifdef WLC_WANTED
    #include "rfactdfs.h"
#endif

INT4 RfMgmtWlcMainTaskInit PROTO ((VOID));

VOID RfMgmtWlcMainTaskDeInit PROTO((VOID));

INT4 RfMgmtWlcModuleStart PROTO ((VOID));

VOID RfMgmtWlcMainProcessEvent PROTO ((UINT4));

VOID RfMgmtWlcModuleShutDown PROTO ((VOID));

VOID RfMgmtWlcQueMsgHandler PROTO ((VOID));

INT4 RfMgmtWlcEnquePkts PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtValidateConfigStatusReq PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtProcessConfigStatusReq PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtValidateConfigStatusRsp PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtProcessConfigStatusRsp PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtProcessConfigUpdateRsp PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtProcessConfigUpdateReq PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtValidateConfigUpdateReq PROTO ((tRfMgmtMsgStruct *));

VOID RfMgmtUpdateMeasReportInfo PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtHandleRadarEventInfo PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtProcessBindingComplete PROTO ((tRfMgmtMsgStruct *));

INT4
RfMgmtShowAPAllowedChannel PROTO((tCliHandle CliHandle,
                    INT4 i4RadioType, UINT1 *pu1CapwapBaseWtpProfileName,
                    UINT4 u4RadioId));

UINT4 RfMgmtMemInit PROTO ((VOID));

VOID RfMgmtMemClear PROTO ((VOID));

INT4 RfMgmtProcessQueMsg PROTO ((VOID));

INT4 RfMgmtTmrInit PROTO ((VOID));

VOID RfMgmtTmrExpHandler PROTO ((VOID));

INT4 RfMgmtTmrStart PROTO ((UINT1 u1TmrType, UINT4 u4TmrInterval));

VOID RfMgmtTmrInitTmrDesc PROTO ((VOID));

INT4 RfMgmtWtpMainTaskInit PROTO ((VOID));

VOID RfMgmtWtpMainTaskDeInit PROTO((VOID));

INT4 RfMgmtWtpModuleStart PROTO ((VOID));

VOID RfMgmtWtpMainProcessEvent PROTO ((UINT4));

VOID RfMgmtWtpModuleShutDown PROTO ((VOID));

VOID RfMgmtWtpQueMsgHandler PROTO ((VOID));

INT4 RfMgmtWtpEnquePkts PROTO ((tRfMgmtMsgStruct *));

PUBLIC INT4 RfMgmtWlcCreateRBTree PROTO ((VOID));

VOID RfMgmtMainReleaseQMemory PROTO ((tRfMgmtQMsg *));

INT4 RfMgmtLock PROTO ((VOID));

INT4 RfMgmtUnLock PROTO ((VOID));

VOID RfMgmtInitGlobals PROTO ((VOID));

INT4 RfMgmtWlcTmrInit PROTO ((VOID));

INT4 RfMgmtWlcTmrDeInit PROTO ((VOID));

VOID RfMgmtDcaIntervalTmrExp PROTO ((VOID *));

VOID RfMgmt11hTpcIntervalTmrExp PROTO ((VOID *));

VOID RfMgmt11hDfsIntervalTmrExp PROTO ((VOID *));

#ifdef ROGUEAP_WANTED
VOID RfMgmtDeleteExpiredRogueApIntervalTmrExp PROTO ((VOID *pArgs));
#endif 

VOID RfMgmtShaIntervalTmrExp PROTO ((VOID *));

VOID RfMgmtTpcIntervalTmrExp PROTO ((VOID *));

VOID RfMgmtDcaIntExpHandler PROTO ((VOID));

VOID RfMgmtTpcIntExpHandler PROTO ((VOID));

INT4 RfMgmtWlcTmrStop PROTO ((UINT4, UINT1));

VOID RfMgmtWlcTmrInitTmrDesc PROTO ((VOID));

INT4 RfMgmtWtpTmrInit PROTO ((VOID));

INT4 RfMgmtWtpTmrDeInit PROTO ((VOID));

VOID RfMgmtScannedMsgTmrExp PROTO ((VOID *));

VOID RfMgmtChannelScanTmrExp PROTO ((VOID *));

VOID RfMgmtTxNeighborMsgTmrExp PROTO ((VOID *));

VOID RfMgmtClientScanTmrExp PROTO ((VOID *));

VOID RfMgmtTpcRequestTmrExp PROTO ((VOID *));
VOID RfMgmtChanSwitAnnounceTmrExp PROTO ((VOID *));

VOID RfMgmtNeighScanExpHandler PROTO ((VOID));

INT4 RfMgmtWtpTmrStart PROTO ((UINT4 u4RadioIfIndex,
            UINT1 u1TmrType, UINT4 u4TmrInterval));

INT4 RfMgmtWtpTmrStop PROTO ((UINT1));

VOID RfMgmtWtpTmrInitTmrDesc PROTO ((VOID));

UINT1 RfMgmtProcessChannelUpdateReq PROTO ((tRfMgmtMsgStruct 
            *pWssMsgStruct));

UINT1 RfMgmtProcessTxPowerUpdateReq PROTO ((tRfMgmtMsgStruct 
            *pWssMsgStruct));

/* rfmgmtsz.c */
UINT1 RfMgmtAutoRfProfileDBCreate PROTO ((VOID));

UINT1 RfMgmtAPConfigDBCreate PROTO ((VOID));

UINT1 RfMgmtFailedAPNeighborListDBCreate PROTO ((VOID));

UINT1 RfMgmtNeighborScanDBCreate PROTO ((VOID));

UINT1 RfMgmtClientConfigDBCreate PROTO ((VOID));

UINT1 RfMgmtClientScanDBCreate PROTO ((VOID));

UINT1 RfMgmtBssidScanDBCreate PROTO ((VOID));

UINT1 RfMgmtScanAllowedDBCreate PROTO((VOID));

INT4
RfMgmtChannelAllowedScanDBRBCmp PROTO((tRBElem * e1, tRBElem * e2));

UINT1 RfMgmtDot11hTpcInfoDBCreate PROTO ((VOID));

INT4 RfMgmtAutoRfProfileDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtAPConfigDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtNeighborScanDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtFailedAPNeighborDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtClientConfigDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtBssidScanDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtDot11hTpcInfoDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtClientScanDBRBCmp PROTO ((tRBElem *, tRBElem *));

INT4 RfMgmtExternalAPDBCmp PROTO ((tRBElem * e1, tRBElem * e2));

UINT1 RfMgmtProcessWssIfMsg  PROTO ((UINT1 , tRfMgmtMsgStruct *));

UINT1 RfMgmtApRadioConfigRequest  PROTO ((UINT1 , tRfMgmtMsgStruct *));

UINT1 RfMgmtUpdateTpcRspInfo  PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtSetQuietIEInfo PROTO ((tRfMgmtAPConfigDB *));

UINT1 RfMgmtProcessDBMsg PROTO ((UINT1 u1Opcode, tRfMgmtDB *pRfMgmtDB));

UINT1 RfMgmtProcessNeighborMessage PROTO ((tRfMgmtMsgStruct *pWssMsgStruct));

UINT1 RfMgmtProcessClientScanMessage PROTO ((tRfMgmtMsgStruct *pWssMsgStruct));

UINT1 RfMgmtConstructNeighborMsg PROTO ((tRfMgmtMsgStruct  *pWssMsgStruct));

UINT1 RfMgmtScanForNeighborMessage PROTO ((UINT4 u4RadioIfIndex));

UINT1 RfMgmtGetNeighborInfo PROTO ((tRfMgmtDB *pRfMgmtDB, UINT1 *pu1RadioName));

UINT1 RfMgmtUpdateNeighbotApDetails PROTO ((tRfMgmtQueueReq *));

UINT1 RfMgmtUpdateClientScanDetails PROTO ((tRfMgmtQueueReq *pRfMgmtQueueReq));
UINT1 RfMgmtUpdateDFSChannelDetails PROTO ((tRfMgmtQueueReq *pRfMgmtQueueReq));
VOID RfMgmtCacTmrExp PROTO ((VOID *));
VOID RfMgmtPerformCAC PROTO ((tRfMgmtAPConfigDB * pRfMgmtAPConfigDB));
VOID RfMgmtEnableChannel PROTO((UINT4 u4RadioIfIndex ,UINT1 u1Channel));
VOID RfMgmtUpdateShadowTableForRadarStatus PROTO((UINT4 u4RadioIfIndex));
VOID RfMgmtHandleRadarEvent PROTO((tRfMgmtQueueReq * pRfMgmtQueueReq ));
VOID RfMgmtTriggerRadarEvent PROTO ((UINT4, UINT1, UINT1));
UINT4 RfMgmtPerformCacCheck PROTO((UINT4 u4RadioIfIndex, UINT1 u1Channel));
UINT1 RfMgmtUpdateDFSInfo PROTO ((tRfMgmtMsgStruct  *pWssMsgStruct));
VOID RfMgmtUpdateChannelStatus PROTO((UINT4 u4RadioIfIndex,UINT4 u4EventType));
UINT1 RfMgmtGetNewChannel PROTO ((VOID));
UINT1 RfMgmtUpdateAllowedChannelList PROTO ((tRfMgmtQueueReq *pRfMgmtQueueReq));

UINT1 RfMgmtConstructClientMsg PROTO ((tRfMgmtMsgStruct  *pWssMsgStruct));

INT4 RfMgmtWtpCreateRBTree PROTO ((VOID));

UINT1 WssIfProcessRfMgmtDBMsg PROTO ((UINT1, tRfMgmtDB *));

UINT1 RfMgmtStartNeighborAPScan 
PROTO ((tRfMgmtAPConfigDB *pRfMgmtAPConfigDB , UINT1 u1Channel));

UINT1 RfMgmtStartClientSNRScan 
PROTO ((tRfMgmtClientConfigDB *pRfMgmtClientConfigDB));

INT1 RfMgmtCliDcaChannelStatus PROTO ((tCliHandle , 
   INT4 , UINT4 , UINT4 ));

UINT1  
RfMgmtGetChannelList PROTO ((UINT2 u2ColorCount, 
                             UINT2 *pu2ColorsToUse));

UINT1 
RfMgmtDeleteNeighborDetails PROTO ((UINT4  u4RadioIfIndex, UINT1));

UINT1 
RfMgmtDeleteExternalNeighborDetails PROTO ((UINT4 u4RadioType));

UINT1
RfMgmtDeleteRadiotypeNeighborInfo PROTO ((UINT4 u4RadioType));

UINT1 
RfMgmtDeleteClientDetails PROTO ((UINT4  u4RadioIfIndex));

UINT1 
RfMgmtDeleteTpcInfo PROTO ((UINT4  u4RadioIfIndex));

UINT1 
RfMgmtDeleteClientStats PROTO ((tRfMgmtMsgStruct  *));

VOID
RfMgmtUpdateNeighborEntryStatus PROTO ((tRfMgmtNeighborScanDB *pRfMgmtNeighborScanDB));
#ifdef WLC_WANTED
UINT1
RfmgmtGetBaseRadioIfIndex PROTO ((UINT4 u4RadioIfIndex, 
            UINT4 *pu4RadioIfIndex));

INT4 RfMgmtWlcTmrStart PROTO ((UINT4 u4Dot11RadioType,
            UINT1 u1TmrType, UINT4 u4TmrInterval));
INT4
RfMgmtRunDFSAlgorithm PROTO ((tRfMgmtAutoRfProfileDB * pRfMgmtAutoRfProfileDB,
                       tRfMgmtAPConfigDB * pRfMgmtAPConfigDB));

INT4 RfMgmtRunDcaAlgorithm PROTO ((tRfMgmtAutoRfProfileDB  
            *pRfMgmtAutoRfProfileDB,  UINT1));

INT4 RfMgmtRunShaAlgorithm PROTO ((tRfMgmtAutoRfProfileDB  
            *pRfMgmtAutoRfProfileDB));

INT4 RfMgmtRunTpcAlgorithm PROTO ((tRfMgmtAutoRfProfileDB  
            *pRfMgmtAutoRfProfileDB));

INT4 RfMgmtRunDpaAlgorithm PROTO ((tRfMgmtAutoRfProfileDB  
            *pRfMgmtAutoRfProfileDB));
INT4 RfMgmtRun11hTpcAlgorithm PROTO ((VOID));

INT4
RfMgmtGetProfileIdRadioId PROTO ((UINT4, UINT2*, UINT1*));

INT4
RfMgmtGetTxPowerLevel PROTO ((UINT4, UINT2*));

UINT1 RfMgmtProcessNeighConfig PROTO ((tRfMgmtDB *));

UINT1 RfMgmtProcessTpcSpectMgmt PROTO ((tRfMgmtDB *));

UINT1 RfMgmtProcessDfsParams PROTO ((tRfMgmtDB *));

UINT1 RfMgmtProcessClientConfig PROTO ((tRfMgmtDB *));

UINT1 RfMgmtProcessBssidScanStatus PROTO ((tRfMgmtDB *));

UINT1 RfMgmtProcessChSwitchMsg PROTO ((INT4 i4CurrentIfIndex));

UINT1 RfMgmtHandleNewAPRadioEntry PROTO ((tRfMgmtMsgStruct *));

UINT1 RfMgmtIsSafe PROTO ((UINT2 , UINT2 *, UINT2 , UINT1 ));

UINT1
RfMgmtValidateScannedChannel PROTO ((tRfMgmtMsgStruct *));

UINT1 
RfMgmtGraphColoringUtil PROTO ((UINT2 *pu2ColorsToUse, 
                                UINT2  u2ColorCount, 
                                UINT2 *pu2Channel, 
                         UINT2 u2InternalApId, UINT1 u1RadioIndex));

VOID RfMgmtDcaSolution PROTO ((UINT2 *pu2Channel, UINT2 u2ReqdColor,
            UINT1 *,UINT1));
VOID RfMgmtModifygaDca PROTO ((UINT1));
VOID RfMgmtDcaSolutionForRadioA PROTO ((UINT2 *pu2Channel, UINT2 u2ReqdColor,
            UINT1 *, UINT1));
UINT1
RfMgmtGetRadioAChannnelIndex PROTO ((UINT2, UINT1 *));

UINT1
RfMgmtUpdateTxPower PROTO ((UINT4, UINT2));

VOID RfMgmtSendDcaSolution PROTO ((UINT2 *pu2Channel, UINT1, UINT2));

UINT1 
RfMgmtGraphColoring PROTO ((UINT2 *pu2ColorsToUse, 
                            UINT2  u2ColorCount,
                            UINT1  u1RadioIndex));

UINT1
RfMgmtTriggerConfigUpdate PROTO ((UINT4 u4RadioIfIndex));

PUBLIC VOID
RfmSnmpifSendTrapInCxt PROTO ((UINT1, VOID *));

INT4
RfmUtilParseSubIdNew PROTO ((UINT1 **ppu1TempPtr));

tSNMP_OID_TYPE     *
RfmMakeObjIdFromDotNew PROTO ((INT1 *textStr));

VOID RfmGuiMacToStr PROTO((UINT1 *pMacAddr, UINT1 *pu1Temp));

INT4
RfmgmtSrcScalars PROTO ((tCliHandle CliHandle));
INT4
RfmShowRunningRrmConfigTable PROTO ((tCliHandle CliHandle));
INT4
RfmShowRunningRrmAPConfigTable PROTO ((tCliHandle CliHandle));
INT4
RfmShowRunningRrmTpcConfigTable PROTO ((tCliHandle CliHandle));
INT4
RfmShowRunningRrmTpcScanTable PROTO ((tCliHandle CliHandle));

UINT1 
RfmgmtDeleteFailedAPDB PROTO ((UINT4  u4RadioIfIndex));

UINT1
RfmgmtGetPowerFromPowerLevel PROTO ((UINT4, UINT2, INT2 *));

VOID
RfmgmtMacToStr PROTO ((UINT1 *, UINT1 *));

UINT1 
RfMgmtUpdateTxPowerLevel PROTO ((tRfMgmtMsgStruct  *));

UINT1
RfmgmtGetWtpIndex PROTO((UINT2, UINT1, UINT2 *));

VOID 
RfMgmtGetCapwapProfileId PROTO ((UINT2 u2WtpInternalId, UINT2 *pu2WtpIndex));
UINT1
RfMgmtUpdategaDcaAlgorithm PROTO ((INT4 i4RadioType, INT2 i2RssiThreshold));
UINT1
RfMgmtDeleteClientDBforWlanId PROTO((UINT1 u1WlanId, UINT4 u4RadioIfIndex));

#ifdef ROGUEAP_WANTED

UINT1  RfMgmtClassifyRogueApInit PROTO ((tRfMgmtRogueApDB  *pRfMgmtRogueApDB)); 

UINT1  RfMgmtClassifyRogueAp PROTO ((void));

INT4 RogueManualCompareBssidDBRBTree PROTO ((tRBElem * e1, tRBElem * e2));

INT4 RfMgmtRogueRuleCompareBssidDBRBTree PROTO ((tRBElem * e1, tRBElem * e2));

INT4 RfMgmtRogueApCompareBssidDBRBTree PROTO ((tRBElem * e1, tRBElem * e2));

UINT1 RfMgmtRogueProcessConfig PROTO ((UINT1 *, UINT1));
 
UINT1 RfMgmtRogueManualDBCreate PROTO ((VOID));

UINT1 RfMgmtRogueRuleDBCreate PROTO ((VOID));

UINT1 RfMgmtRogueApDBCreate PROTO ((VOID));

UINT1 RfMgmtArrangeRogueRulePriority PROTO ((UINT1));

UINT1 checkRuleNameExist PROTO ((UINT1 *));

UINT1 RfMgmtReArrangeRogueRule PROTO ((INT4));

#endif 

UINT1
RfMgmtDot11hDfsInfoDBCreate PROTO((VOID));
INT4
RfMgmtDot11hDfsInfoDBRBCmp PROTO((tRBElem * e1, tRBElem * e2));
UINT1
RfMgmtDeleteDfsInfo (UINT4);
UINT1
RfMgmtGetFreeExternalAPId PROTO((UINT2 *pu2ExternalAPId, UINT1 u1RadioIndex));
UINT1
RfMgmtExternalAPDBCreate PROTO((VOID));


#endif
#endif
