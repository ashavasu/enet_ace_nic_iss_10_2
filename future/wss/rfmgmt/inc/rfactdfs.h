/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfactdfs.h,v 1.3 2017/11/24 10:37:04 siva Exp $
 *
 * Description: This file contains type definitions relating to 
 *              rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RF_AC_TDFS_H__
#define __RF_AC_TDFS_H__

/* The below data structure will be created during RF Management initialization.
 * This table contains the configuration parameters required for running DCA and
 * TPC Algorithms */
typedef struct 
{
    tRBNodeEmbd     RfMgmtDBNode; 
    UINT4           u4Dot11RadioType;
    UINT4           u4DcaLastUpdatedTime;
    UINT4           u4TpcLastUpdatedTime;
    UINT4           u4ShaLastUpdatedTime;
    UINT4     u4DfsLastUpdatedTime;
    UINT4           u4DcaIntervalUpdatedTime;
    UINT4           u4TpcIntervalUpdatedTime;
    UINT4     u4DfsIntervalUpdatedTime;
    UINT4           u4DcaInterval;
    UINT4           u4SHAInterval;
    UINT4           u4SHAExecutionCount;
    UINT4           u4DPAExecutionCount;
    UINT4           u4TpcInterval;
    UINT4         u4RfMgmt11hTpcLastRun;
    UINT4         u4RfMgmt11hTpcInterval;
    UINT4           u4DfsInterval;              
    UINT4           u4RfMgmt11hDfsLastRun;
    INT4            i4RowStatus;  
    INT2            i2RssiThreshold;
    INT2            i2PowerThreshold;
    INT2            i2SNRThreshold;
    UINT1           u1DcaMode;
    UINT1           u1ApNeighborCount;
    UINT1           u1DcaSelection;
    UINT1           au1AllowedChannelList[RADIOIF_MAX_CHANNEL_A];
    UINT1           u1DcaSensitivity;
    UINT1           u1DcaUpdateChannel;
    UINT1           u1TpcMode;
    UINT1           au1UnUsedChannelList[RADIOIF_MAX_CHANNEL_A];
    UINT1           u1TpcSelection;
    UINT1           u1TpcUpdatePower;
    UINT1           u1ClientSNRStatus;
    UINT1           u1ClientThreshold;
    UINT1           u1SHAStatus;
    UINT1           u1RfMgmt11hTpcStatus;
    UINT1           u1MinLinkThreshold;
    UINT1           u1MaxLinkThreshold;
    UINT1           u1StaCountThreshold;
    UINT1           u1RfMgmt11hDfsStatus;
    UINT1           u1ConsiderExternalAPs;
    UINT1           u1ClearNeighborDetails;
    UINT1           u1NeighborCountThreshold;
    UINT1           au1Pad[3];
} tRfMgmtAutoRfProfileDB;

typedef struct 
{
    BOOL1           bDot11RadioType;
    BOOL1           bDcaLastUpdatedTime;
    BOOL1           bTpcLastUpdatedTime;
    BOOL1           bShaLastUpdatedTime;
    BOOL1     bDfsLastUpdatedTime;
    BOOL1           bDcaIntervalUpdatedTime;
    BOOL1           bTpcIntervalUpdatedTime;
    BOOL1     bDfsIntervalUpdatedTime;
    BOOL1           bTpcInterval;
    BOOL1           bRowStatus;  
    BOOL1           bDcaInterval;
    BOOL1           bRssiThreshold;
    BOOL1           bPowerThreshold;
    BOOL1           bSHAExecutionCount;
    BOOL1           bDPAExecutionCount;
    BOOL1           bSNRThreshold;
    BOOL1           bAllowedChannelList;
    BOOL1           bUnUsedChannelList;
    BOOL1           bDcaMode;
    BOOL1           bApNeighborCount;
    BOOL1           bDcaSelection;
    BOOL1           bDcaSensitivity;
    BOOL1           bDcaUpdateChannel;
    BOOL1           bTpcMode;
    BOOL1           bTpcSelection;
    BOOL1           bTpcUpdatePower;
    BOOL1           bClientSNRStatus;
    BOOL1           bClientThreshold;
    BOOL1           bSHAStatus;
    BOOL1           bSHAInterval;
    BOOL1           bRfMgmt11hTpcStatus;
    BOOL1           bRfMgmt11hTpcLastRun;
    BOOL1           bRfMgmt11hTpcInterval;
    BOOL1           bMinLinkThreshold;
    BOOL1           bMaxLinkThreshold;
    BOOL1           bStaCountThreshold;
    BOOL1           bDfsInterval;               
    BOOL1           bRfMgmt11hDfsStatus;       
    BOOL1           bRfMgmt11hDfsLastRun;
    BOOL1           bConsiderExternalAPs;
    BOOL1           bClearNeighborDetails;
    BOOL1           bNeighborCountThreshold;
    BOOL1           au1Pad[2];
} tRfMgmtAutoRfIsSetDB;
    
typedef struct
{
    tRfMgmtAutoRfProfileDB   RfMgmtAutoRfProfileDB;
    tRfMgmtAutoRfIsSetDB     RfMgmtAutoRfIsSetDB;
}tRfMgmtAutoRfTable;

typedef struct
{

   UINT4  u4ChannelNum;
   UINT4  u4DFSFlag;
}tDFSChannelInfo;

/* The below data structure will be created during AP Profile and Radio 
 * IF is created.  * This table contains the configuration parameters 
 * required for WTP Scanning.
 * u4RadioIfIndex - Table Index */
typedef struct 
{
    tRBNodeEmbd     ApConfigDBNode; 
    tTmrBlk         RfMgmtAutoScanAPTmrBlk;
    UINT4           u4RadioIfIndex;
    UINT4           u4Dot11RadioType;
    UINT4           u4ChannelChangeTime;
    UINT2           u2NeighborAgingPeriod;
    UINT2           u2CurrentChannel;
    UINT2           u2NeighborMsgPeriod;
    UINT2           u2ChannelScanDuration;
    UINT2         u2RfMgmt11hTpcRequestInterval;
    UINT2     u2RfMgmt11hDfsQuietInterval;  
    UINT2           u2RfMgmt11hDfsQuietPeriod;                
    UINT2      u2RfMgmt11hDfsMeasurementInterval; 
    UINT2           u2RfMgmt11hDfsChannelSwitchStatus;
    tMacAddr        RadioMacAddr;
    UINT1           u1ChannelChangeCount;
    UINT1           u1ChannelAssignmentMode;
    UINT1           u1PerAPTpcSelection;
    UINT1           u1AutoScanStatus;
    UINT1           au1ScannedChannel[RADIOIF_MAX_CHANNEL_A];
    UINT1           au1AllowedChannelList[RADIOIF_MAX_CHANNEL_A];
    tDFSChannelInfo DFSChannelInfo[RADIOIF_MAX_CHANNEL_A];
} tRfMgmtAPConfigDB;

typedef struct 
{
    BOOL1           bCurrentChannel;
    BOOL1           bChannelChangeTime;
    BOOL1           bDot11RadioType;
    BOOL1           bNeighborMsgPeriod;
    BOOL1           bChannelScanDuration;
    BOOL1           bNeighborAgingPeriod;
    BOOL1           bChannelChangeCount;
    BOOL1           bRadioMacAddr;
    BOOL1           bChannelAssignmentMode;
    BOOL1           bAutoScanStatus;
    BOOL1           bPerAPTpcSelection;
    BOOL1           bScannedChannel;
    BOOL1         bRfMgmt11hTpcRequestInterval;
    BOOL1           bGetAllowedList;
    BOOL1           bDFSChannelInfoSet;
    BOOL1     bRfMgmt11hDfsQuietInterval; 
    BOOL1           bRfMgmt11hDfsQuietPeriod; 
    BOOL1     bRfMgmt11hDfsMeasurementInterval;               
    BOOL1           bRfMgmt11hDfsChannelSwitchStatus;
    UINT1           au1Pad[1];
} tRfMgmtAPConfigIsSetDB;

typedef struct
{
    tRfMgmtAPConfigDB          RfMgmtAPConfigDB;
    tRfMgmtAPConfigIsSetDB     RfMgmtAPConfigIsSetDB;
}tRfMgmtApConfigTable;

/* The below data structure will be created when AP scan for the neighbor messages 
 * and sends to AC.
 * u4RadioIfIndex, u2ScannedChannel, NeighborAPMac - Table Index */
typedef struct 
{
    tRBNodeEmbd     NeighborScanDBNode; 
    tTmrBlk         RfMgmtNeighAPTmrBlk;
    UINT4           u4RadioIfIndex;
    UINT4         u4NeighborMsgCount;
    UINT4           u4LastUpdatedTime;
    UINT2           u2ScannedChannel;
    UINT2           u2ExternalAPIndex;
    INT2            i2Rssi;
    INT2            i2SNRValue;
    tMacAddr        NeighborAPMac;
    UINT1           u1EntryStatus;
    UINT1           u1StationCount;
} tRfMgmtNeighborScanDB;

typedef struct 
{
    BOOL1           bRadioIfIndex;
    BOOL1           bRssi;
    BOOL1           bLastUpdatedTime;
    BOOL1           bNeighborMsgCount;
    BOOL1           bSNRValue;
    BOOL1           bScannedChannel;
    BOOL1           bNeighborAPMac;
    BOOL1           bEntryStatus;
    BOOL1           bExternalAPIndex;
    UINT1           au1Pad[3]; 
} tRfMgmtNeighborScanIsSetDB;

typedef struct
{
    tRfMgmtNeighborScanDB       RfMgmtNeighborScanDB;
    tRfMgmtNeighborScanIsSetDB  RfMgmtNeighborScanIsSetDB;
}tRfMgmtNeighborScanTable;

/*Structure to store External AP Details*/
typedef struct
{
    tRBNodeEmbd     ExternalAPDBNode;
    tMacAddr        APMac;
    UINT2           u2Channel;
    UINT2           u2ExternalAPId;
    INT2            i2Rssi;
}tRfMgmtExternalAPDB;

typedef struct 
{
    BOOL1           bChannel;
    BOOL1           bExternalAPId;
    BOOL1           bRssi;
    UINT1           u1Pad;
} tRfMgmtExternalAPIsSetDB;

typedef struct
{
    tRfMgmtExternalAPDB       RfMgmtExternalAPDB;
    tRfMgmtExternalAPIsSetDB  RfMgmtExternalAPIsSetDB;
}tRfMgmtExternalAPTable;

/* The below data structure will be created when AP is down 
 * u4RadioIfIndex, NeighborAPMac - Table Index */
typedef struct 
{
    tRBNodeEmbd     FailedAPNeighborListDBNode;
    UINT4           u4RadioIfIndex;
    UINT4           u4NeighRadioIfIndex;
    tMacAddr        NeighborAPMac;
    INT2            i2Rssi; 
} tRfMgmtFailedAPNeighborListDB;

typedef struct 
{
    BOOL1           bRssi;
    BOOL1           bRadioIfIndex;
    BOOL1           bNeighborAPMac;
    BOOL1           bNeighRadioIfIndex;
} tRfMgmtFailedAPNeighborListIsSetDB;

typedef struct
{
    tRfMgmtFailedAPNeighborListDB       RfMgmtFailedAPNeighborListDB;
    tRfMgmtFailedAPNeighborListIsSetDB  RfMgmtFailedAPNeighborListIsSetDB;
}tRfMgmtFailedAPNeighborTable;

/* u4RfMgmt11hRadioIfIndex, RfMgmt11hStationMacAddress - Table Index */
typedef struct
{
    tRBNodeEmbd     RfMgmt11hTpcInfoDBNode;
    tMacAddr        RfMgmt11hStationMacAddress;
    INT2            i2RfMgmt11hTxPowerLevel;
    UINT4           u4RfMgmt11hTpcReportLastReceived;
    UINT4           u4RfMgmt11hRadioIfIndex;
    INT1            i1RfMgmt11hLinkMargin;
    INT1            i1RfMgmt11hBaseLinkMargin; 
    UINT1           au1pad[2];
} tRfMgmtDot11hTpcInfoDB;

typedef struct
{
    BOOL1           bRfMgmt11hRadioIfIndex;
    BOOL1           bRfMgmt11hStationMacAddress;
    BOOL1           bRfMgmt11hTxPowerLevel;
    BOOL1           bRfMgmt11hLinkMargin;
    BOOL1           bRfMgmt11hTpcReportLastReceived;
    BOOL1           bRfMgmt11hBaseLinkMargin;
    UINT1           au1pad[2];
} tRfMgmtDot11hTpcInfoIsSetDB;

typedef struct
{
    tRfMgmtDot11hTpcInfoDB       RfMgmtDot11hTpcInfoDB;
    tRfMgmtDot11hTpcInfoIsSetDB  RfMgmtDot11hTpcInfoIsSetDB;
}tRfMgmtDot11hTpcInfoTable;

typedef struct
{
    tRBNodeEmbd     RfMgmt11hDfsInfoDBNode;
    UINT4           u4RfMgmt11hRadioIfIndex;
    UINT4           u4RfMgmt11hDfsReportLastReceived;
    tMacAddr        RfMgmt11hStationMacAddress;
    UINT1           u1ChannelNum;
    BOOL1           bIsRadarPresent;
} tRfMgmtDot11hDfsInfoDB;

typedef struct
{
    BOOL1           bRfMgmt11hRadioIfIndex;
    BOOL1           bRfMgmt11hStationMacAddress;
    BOOL1           bRfMgmt11hChannelNum;
    BOOL1           bRfMgmt11hIsRadarPresent;
    BOOL1           bRfMgmt11hDfsReportLastReceived;
    UINT1           au1pad[3];
} tRfMgmtDot11hDfsInfoIsSetDB;

typedef struct
{
    tRfMgmtDot11hDfsInfoDB       RfMgmtDot11hDfsInfoDB;
    tRfMgmtDot11hDfsInfoIsSetDB  RfMgmtDot11hDfsInfoIsSetDB;
}tRfMgmtDot11hDfsInfoTable;

/* The below data structure will be created when AP Profile and Radio IF is created
 * This table contains the configuration parameters required for Client SNR 
 * Scanning 
 * u4RadioIfIndex - Table Index */
typedef struct 
{
    tRBNodeEmbd     ClientConfigDBNode; 
    UINT4           u4RadioIfIndex;
    UINT4           u4TxPowerChangeTime;
    UINT4           u4SNRScanCount;
    UINT4           u4BelowSNRCount;
    UINT4           u4TxPowerIncreaseCount;
    UINT4           u4TxPowerDecreaseCount;
    UINT2           u2SNRScanPeriod;
    UINT2           u2ClientsConnected;
    UINT2           u2ClientsAccepted;
    UINT2           u2ClientsDiscarded;
    UINT2           u2TxPowerLevel;
    INT2            i2CurrentTxPower;
    UINT1           u1TxPowerChangeCount;
    UINT1           u1SNRScanStatus;
    UINT1           au1Pad[2];
} tRfMgmtClientConfigDB;

typedef struct 
{
    BOOL1           bRadioIfIndex;
    BOOL1           bTxPowerChangeTime;
    BOOL1           bSNRScanCount;
    BOOL1           bBelowSNRCount;
    BOOL1           bTxPowerIncreaseCount;
    BOOL1           bTxPowerDecreaseCount;
    BOOL1           bClientsConnected;
    BOOL1           bClientsAccepted;
    BOOL1           bClientsDiscarded;
    BOOL1           bSNRScanPeriod;
    BOOL1           bTxPowerChangeCount;
    BOOL1           bTxPowerLevel;
    BOOL1           bCurrentTxPower;
    BOOL1           bSNRScanStatus;
    UINT1           au1Pad[2];
} tRfMgmtClientConfigIsSetDB;

typedef struct
{
    tRfMgmtClientConfigDB       RfMgmtClientConfigDB;
    tRfMgmtClientConfigIsSetDB  RfMgmtClientConfigIsSetDB;
}tRfMgmtClientConfigTable;


/* The below data structure will be created when an AP sends Client SNR
 * information to the WLC.
 * u4RadioIfIndex, ClientMacAddr - Table Index */
typedef struct 
{
    tRBNodeEmbd     ClientScanDBNode;
    tTmrBlk         RfMgmtClientTmrBlk;
    UINT4           u4RadioIfIndex;
    UINT4           u4LastUpdatedTime;
    UINT4           u4LastClientSNRScan;
    tMacAddr        ClientMacAddress;
    INT2            i2ClientSNR;
    UINT1           u1EntryStatus;
    UINT1           au1Pad[3];

} tRfMgmtClientScanDB;

typedef struct 
{
    BOOL1           bLastClientSNRScan;
    BOOL1           bClientSNR;
    BOOL1           bEntryStatus;
    UINT1           au1Pad[1];
} tRfMgmtClientScanIsSetDB;

typedef struct
{
    tRfMgmtClientScanDB        RfMgmtClientScanDB;
    tRfMgmtClientScanIsSetDB   RfMgmtClientScanIsSetDB;
}tRfMgmtClientScanTable;


/* The below data structure will be created when an entry is enabled/disabled
 * for scanning on a specific AP WLAN 
 * u4RadioIfIndex, Wlan Id - Table Index */
typedef struct 
{
    tRBNodeEmbd     BssidScanDBNode;
    UINT4           u4RadioIfIndex;
    UINT1           u1WlanID;
    UINT1           u1AutoScanStatus;
    UINT1           u1EntryStatus;
    UINT1           u1Pad;
} tRfMgmtBssidScanDB;

typedef struct 
{
    BOOL1           bEntryStatus;
    BOOL1           bAutoScanStatus;
    UINT1           au1Pad[2];
} tRfMgmtBssidScanIsSetDB;

typedef struct
{
    tRfMgmtBssidScanDB        RfMgmtBssidScanDB;
    tRfMgmtBssidScanIsSetDB   RfMgmtBssidScanIsSetDB;
}tRfMgmtBssidScanTable;

typedef struct
{
    tRBNodeEmbd RogueApDB;             
    UINT4       u4RogueApLastReportedTime;
    UINT4       u4RogueApLastHeardTime;
    UINT4       u4RogueApClientCount;
    INT2        i2RogueApRSSI;
    UINT2       u2RogueApOperationChannel;
    INT2        i2RogueApTpc;
    UINT1       u1RogueApSSID[WSSMAC_MAX_SSID_LEN];
    UINT1       u1RogueApBSSID[WSSMAC_MAC_ADDR_LEN];
    UINT1       u1BSSIDRogueApLearntFrom[WSSMAC_MAC_ADDR_LEN];
    BOOL1       isRogueApProcetionType;
    UINT1       u1RogueApSNR;
    UINT1       u1RogueApDetectedRadio; 
    UINT1       u1RogueApDetectedBand; 
    UINT1       u1RogueApClass;
    UINT1       u1RogueApState;
    UINT1       u1RogueApClassifiedBy;
    BOOL1       isRogueApStatus;
    UINT1       au1Pad[2];
} tRfMgmtRogueApDB;

typedef struct
{
    BOOL1       bRogueApBSSID;
    BOOL1       bRogueApSSID;
    BOOL1       bRogueApProcetionType;
    BOOL1       bRogueApSNR;
    BOOL1       bRogueApRSSI;
    BOOL1       bRogueApOperationChannel;
    BOOL1       bRogueApDetectedRadio;
    BOOL1       bRogueApDetectedBand;
    BOOL1       bRogueApLastReportedTime;
    BOOL1       bRogueApLastHeardTime;
    BOOL1       bRogueApTpc;
    BOOL1       bRogueApClass;
    BOOL1       bRogueApState;
    BOOL1       bRogueApClassifiedBy;
    BOOL1       bRogueApClientCount;
    BOOL1       bBSSIDRogueApLearntFrom;
    BOOL1       bRogueApStatus;
    UINT1       au1Pad[3];
} tRfMgmtRogueApIsSetDB;

typedef struct
{
    tRBNodeEmbd RogueRuleDB;
    UINT1   u1RogueApRuleName[WSSMAC_MAX_SSID_LEN];
    UINT1   u1RogueApRuleSSID[WSSMAC_MAX_SSID_LEN];
    UINT4   u4RogueApRuleDuration;
    UINT4   u4RogueApClientCount;
    INT2    i2RogueApRSSI; 
    INT2    i2RogueApRuleTpc;
    UINT1   u1RogueApRulePriOrderNum;
    UINT1   u1RogueApRuleClass;
    UINT1   u1RogueApRuleState;
    BOOL1   isRogueApProcetionType;
    BOOL1   isRogueApStatus;
    UINT1       au1Pad[3];
} tRfMgmtRogueRuleDB;

typedef struct
{
    BOOL1   bRogueApRulePriOrderNum;
    BOOL1   bRogueApRuleName;
    BOOL1   bRogueApRuleClass;
    BOOL1   bRogueApRuleState;
    BOOL1   bRogueApRuleDuration;
    BOOL1   bRogueApProcetionType;
    BOOL1   bRogueApRSSI;
    BOOL1   bRogueApRuleTpc;
    BOOL1   bRogueApRuleSSID;
    BOOL1   bRogueApClientCount;
    BOOL1   bRogueApStatus;
    UINT1   au1Pad[1];
} tRfMgmtRogueRuleIsSetDB;

typedef struct
{
    tRBNodeEmbd RogueManualDB;
    UINT1   u1RogueApBSSID[WSSMAC_MAC_ADDR_LEN];
    UINT1       u1RogueApClass;
    UINT1       u1RogueApState;
    BOOL1   isRogueApStatus;
    UINT1       au1Pad[3];
} tRfMgmtRogueManualDB;

typedef struct
{
    BOOL1   bRogueApBSSID;
    BOOL1       bRogueApClass;
    BOOL1       bRogueApState;
    BOOL1   bRogueApStatus;
} tRfMgmtRogueManualIsSetDB;

typedef struct
{
    tRfMgmtRogueApDB          RfMgmtRogueApDB;
    tRfMgmtRogueApIsSetDB     RfMgmtRogueApIsSetDB;
    tRfMgmtRogueRuleDB        RfMgmtRogueRuleDB;
    tRfMgmtRogueRuleIsSetDB   RfMgmtRogueRuleIsSetDB;
    tRfMgmtRogueManualDB      RfMgmtRogueManualDB;
    tRfMgmtRogueManualIsSetDB RfMgmtRogueManualIsSetDB;
}tRfMgmtRogueTable;

typedef struct {
    union {
        tRfMgmtAutoRfTable              RfMgmtAutoRfTable; 
        tRfMgmtNeighborScanTable        RfMgmtNeighborScanTable;
        tRfMgmtFailedAPNeighborTable    RfMgmtFailedAPNeighborTable;
        tRfMgmtClientConfigTable        RfMgmtClientConfigTable;
        tRfMgmtClientScanTable          RfMgmtClientScanTable;
        tRfMgmtApConfigTable            RfMgmtApConfigTable;
        tRfMgmtBssidScanTable           RfMgmtBssidScanTable;
     tRfMgmtDot11hTpcInfoTable     RfMgmtDot11hTpcInfoTable;
 tRfMgmtExternalAPTable          RfMgmtExternalAPTable;
     tRfMgmtDot11hDfsInfoTable     RfMgmtDot11hDfsInfoTable;
 tRfMgmtRogueTable               RfMgmtRogueTable;
    }unRfMgmtDB;
    UINT1            u1ChSwitchStatus;
    UINT1            au1Pad[3];
}tRfMgmtDB;

typedef struct {
    UINT4            u4RadioIfIndex;
    UINT2            u2ChannelNo;
    INT2             i2Rssi;
    tMacAddr         MacAddr;
    UINT2            u2Color;
    UINT1            u1IsRunStateReached;
    UINT1            u1NeighAPCount;
    UINT1            u1IsNeighborPresent;
    UINT1            u1ApActive;
    UINT1            u1AllNeighbors;
    UINT1            u1TxIncreaseLevel;
    UINT1            u1ConsiderNeighborforAlgo;
    UINT1            u1Pad;
}tDcaAlgorithm;

typedef struct {
    UINT4           u4NeighRadioIfIndex;
    UINT2           u2WtpIndex;
    INT2            i2Rssi;
}tShaNeighDetails;

typedef struct 
{
    tRBTree          RfMgmtAutoRfDB;
    tRBTree          RfMgmtNeighborAPConfigDB;
    tRBTree          RfMgmtNeighborAPScanDB;
    tRBTree          RfMgmtClientSNRConfigDB;
    tRBTree          RfMgmtClientScanDB;
    tRBTree          RfMgmtBssidScanDB;
    tRBTree          RfMgmtFailedAPNeighborDB;
    tRBTree          RfMgmtDot11hTpcInfoDB;
    tRBTree          RfMgmtExternalAPDB; 
    tRBTree          RfMgmtDot11hDfsInfoDB;
    tRBTree          RfMgmtRogueApDB;
    tRBTree          RfMgmtRogueRuleDB;
    tRBTree          RfMgmtRogueManualDB;
} tRfMgmtGlbMib;

enum {
    RFMGMT_DCA_INTERVAL_TMR = 0,
    RFMGMT_TPC_INTERVAL_TMR,
    RFMGMT_SHA_INTERVAL_TMR,
    RFMGMT_11H_TPC_INTERVAL_TMR,
    RFMGMT_11H_DFS_INTERVAL_TMR,
    RFMGMT_ROUGE_INTERVAL_TMR,
    RFMGMT_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR,
    RFMGMT_MAX_TMR
};

enum {
    RFMGMT_RADIO_TYPE_B = 0,
    RFMGMT_RADIO_TYPE_A,
    RFMGMT_MAX_RADIO_TYPE
};

typedef struct{
    /*CAPWAP Timer List ID */
    tTimerListId        RfMgmtTmrListId;
    tTmrDesc            aRfMgmtTmrDesc[RFMGMT_MAX_TMR];
    tTmrBlk             RfMgmtDCATmrBlk;
    tTmrBlk             RfMgmtTPCTmrBlk;
    tTmrBlk             RfMgmtSHATmrBlk;
    tTmrBlk             RfMgmt11hTPCTmrBlk;
    tTmrBlk             RfMgmt11hDFSTmrBlk;
    tTmrBlk             RfMgmtDeleteRougeStationTmrBlk;
    tTmrBlk             RfMgmtDeleteExpiredRougeApTmrBlk;
}tRfMgmtTimerList;

typedef struct RFMGMT_GLOBALS {
    tRfMgmtGlbMib       RfMgmtGlbMib;
    tOsixTaskId         rfMgmtTaskId;
    tOsixQId            rfMgmtQueId;
    tOsixSemId          SemId;
    tMemPoolId          QMsgPoolId;
    UINT1               u1RfmgmtTaskInitialized;
    UINT1            au1Pad[3];
} tRfMgmtGlobals;



typedef struct{
    /* Used For sending Trap */
    INT4 i4RogueApState;
    UINT2 u2Channel;
    UINT2 u2TxPowerLevel;
    UINT1 au1RogueApBSSID[WSSMAC_MAC_ADDR_LEN];
    UINT1 au1Pad[2];
} tRfMgmtTrapInfo;
 

/* Enum for Database Operations */
typedef enum{
    RFMGMT_CREATE_RADIO_TYPE_ENTRY,
    RFMGMT_SET_AUTO_RF_ENTRY,
    RFMGMT_GET_AUTO_RF_ENTRY,
    RFMGMT_DELETE_RADIO_TYPE_ENTRY,

    RFMGMT_CREATE_RADIO_IF_INDEX_ENTRY,
    RFMGMT_DESTROY_RADIO_IF_INDEX_ENTRY,
    
    RFMGMT_SET_AP_CONFIG_ENTRY,
    RFMGMT_GET_AP_CONFIG_ENTRY,
    RFMGMT_SET_CLIENT_CONFIG_ENTRY,
    RFMGMT_GET_CLIENT_CONFIG_ENTRY,

    RFMGMT_GET_FIRST_NEIGH_ENTRY,
    RFMGMT_GET_NEXT_NEIGH_ENTRY,

    RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY,
    RFMGMT_SET_NEIGHBOR_SCAN_ENTRY,
    RFMGMT_GET_NEIGHBOR_SCAN_ENTRY,
    RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY,

    RFMGMT_CREATE_FAILED_AP_NEIGH_ENTRY,
    RFMGMT_SET_FAILED_AP_NEIGH_ENTRY,
    RFMGMT_GET_FAILED_AP_NEIGH_ENTRY,
    RFMGMT_DESTROY_FAILED_AP_NEIGH_ENTRY,

    RFMGMT_GET_FIRST_FAILED_AP_NEIGH_ENTRY,
    RFMGMT_GET_NEXT_FAILED_AP_NEIGH_ENTRY,

    RFMGMT_GET_FIRST_CLIENT_ENTRY,
    RFMGMT_GET_NEXT_CLIENT_ENTRY,

    RFMGMT_CREATE_CLIENT_SCAN_ENTRY,
    RFMGMT_SET_CLIENT_SCAN_ENTRY,
    RFMGMT_GET_CLIENT_SCAN_ENTRY,
    RFMGMT_DESTROY_CLIENT_SCAN_ENTRY,

    RFMGMT_CREATE_BSSID_SCAN_ENTRY,
    RFMGMT_SET_BSSID_SCAN_ENTRY,
    RFMGMT_GET_BSSID_SCAN_ENTRY,
    RFMGMT_DESTROY_BSSID_SCAN_ENTRY,

    RFMGMT_SET_CH_SWITCH_MSG_ENTRY,
    RFMGMT_GET_CH_SWITCH_MSG_ENTRY,

    RFMGMT_CREATE_TPC_INFO_ENTRY,
    RFMGMT_GET_TPC_INFO_ENTRY,
    RFMGMT_SET_TPC_INFO_ENTRY,
    RFMGMT_DESTROY_TPC_INFO_ENTRY,
    RFMGMT_GET_FIRST_TPC_INFO_ENTRY,
    RFMGMT_GET_NEXT_TPC_INFO_ENTRY, 

    RFMGMT_CREATE_EXTERNAL_AP_ENTRY,
    RFMGMT_SET_EXTERNAL_AP_ENTRY,
    RFMGMT_GET_EXTERNAL_AP_ENTRY,
    RFMGMT_DESTROY_EXTERNAL_AP_ENTRY,

    RFMGMT_CREATE_DFS_INFO_ENTRY,
    RFMGMT_GET_DFS_INFO_ENTRY,
    RFMGMT_SET_DFS_INFO_ENTRY,
    RFMGMT_DESTROY_DFS_INFO_ENTRY,
    RFMGMT_GET_FIRST_DFS_INFO_ENTRY,
    RFMGMT_GET_NEXT_DFS_INFO_ENTRY,
    RFMGMT_CREATE_SHOW_ROGUE_INFO,
    RFMGMT_DESTROY_SHOW_ROGUE_INFO,
    RFMGMT_SET_RULE_ROGUE_INFO,
    RFMGMT_GET_RULE_ROGUE_INFO,
    RFMGMT_GET_SHOW_ROGUE_INFO,
    RFMGMT_CREATE_RULE_ROGUE_INFO,
    RFMGMT_DESTROY_RULE_ROGUE_INFO,
    RFMGMT_CREATE_MANUAL_ROGUE_INFO,
    RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
    RFMGMT_GET_MANUAL_ROGUE_INFO,
    RFMGMT_SET_MANUAL_ROGUE_INFO,
    RFMGMT_DESTROY_RULE_ROGUE_INFO_ALL,
    RFMGMT_DESTROY_ROGUE_INFO,
    RFMGMT_SEARCH_SHOW_ROGUE_INFO,
    RFMGMT_SET_SHOW_ROGUE_INFO
}eRfMgmtDBMsgType;



enum {
    ROGUEAP_CLASS_UNCLASSIFIED = 1,
    ROGUEAP_CLASS_FRIENDLY,
    ROGUEAP_CLASS_MALICIOUS,
    ROGUEAP_MAX_CLASS
};

enum {
    ROGUEAP_STATE_NONE = 1,
    ROGUEAP_STATE_ALERT,
    ROGUEAP_CLASS_CONTAIN,
    ROGUEAP_MAX_STATE
};

enum {
    ROGUEAP_CLASSIFIEDBY_MANUALLY =0,
    ROGUEAP_CLASSIFIEDBY_AUTOMATIC,
    ROGUEAP_CLASSIFIEDBY_NONE = 129,
    ROGUEAP_MAX_CLASSIFIEDBY
};

#endif
