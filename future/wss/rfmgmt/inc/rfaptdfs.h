/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfaptdfs.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains type definitions relating to 
 *              rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RF_AP_TDFS_H__
#define __RF_AP_TDFS_H__

#include "rfminc.h"
enum eventtype{
    RADAR_DETECTED,
    RADAR_CAC_FINISHED,
    RADAR_CAC_ABORTED
};

typedef struct
{
  UINT4   u4Frequency;
  UINT4   u4ChannelNum;
  UINT4   u4DFSFlag;
  UINT4   u4DFSCacTime;
}tRfMgmtDfsChannelInfo;

/* The below data structure will be created during AP Profile and Radio 
 * IF is created.  * This table contains the configuration parameters 
 * required for WTP Scanning.
 * u4RadioIfIndex - Table Index */
typedef struct 
{
    tRBNodeEmbd     ApConfigDBNode; 
    tRfMgmtDfsChannelInfo DFSChannelStatus[RADIO_MAX_DFS_CHANNEL];
    UINT4           u4RadioIfIndex;
    UINT4           u4Dot11RadioType;
    UINT4           u4APScanUpdatedTime;
    UINT4           u4NeighMsgUpdatedTime;
    UINT4           u4ChannelChangeTime;
    UINT4           u4TpcReqLastSentTime;
    UINT2           u2NeighborAgingPeriod;
    UINT2           u2CurrentChannel;
    UINT2           u2NeighborMsgPeriod;
    UINT2           u2ChannelScanDuration;
    UINT2           u2ChannelToScan;
    UINT2         u2RfMgmt11hTpcRequestInterval;
    UINT2     u2RfMgmt11hDfsQuietInterval;  
    UINT2           u2RfMgmt11hDfsQuietPeriod;               
    UINT2           u2RfMgmt11hDfsMeasurementInterval;               
    UINT2           u2RfMgmt11hDfsChannelSwitchStatus;               
    INT2            i2RssiThreshold;
    UINT1     u1RfMgmt11hDfsStatus; 
    UINT1           u1ChannelChangeCount;
    UINT1           u1ChannelAssignmentMode;
    UINT1           u1PerAPTpcSelection;
    UINT1           u1AutoScanStatus;
    UINT1           u1ChSwitchStatus;
    UINT1           u1LastScannedChannel;
    UINT1           u1TpcRequestSlot;
    UINT1           u1LastScannedIndex;
    UINT1           u1RfMgmt11hTpcStatus;
} tRfMgmtAPConfigDB;

typedef struct 
{
    BOOL1           bCurrentChannel;
    BOOL1           bDot11RadioType;
    BOOL1           bChannelChangeTime;
    BOOL1           bNeighborMsgPeriod;
    BOOL1           bChannelScanDuration;
    BOOL1           bNeighborAgingPeriod;
    BOOL1           bChannelChangeCount;
    BOOL1           bChannelAssignmentMode;
    BOOL1           bAutoScanStatus;
    BOOL1           bRssiThreshold;   
    BOOL1           bPerAPTpcSelection;
    BOOL1           bChSwitchStatus;
    BOOL1           bRfMgmt11hTpcRequestInterval;
    BOOL1           bRfMgmt11hTpcStatus;
    BOOL1           bDFSChannelStatus;
    BOOL1           bRfMgmt11hDfsQuietInterval;  
    BOOL1           bRfMgmt11hDfsQuietPeriod;         
    BOOL1           bRfMgmt11hDfsMeasurementInterval;        
    BOOL1           bRfMgmt11hDfsChannelSwitchStatus;         
    BOOL1         bRfMgmt11hDfsStatus;
} tRfMgmtAPConfigIsSetDB;

typedef struct
{
    tRfMgmtAPConfigDB          RfMgmtAPConfigDB;
    tRfMgmtAPConfigIsSetDB     RfMgmtAPConfigIsSetDB;
}tRfMgmtApConfigTable;

/* The below data structure will be created when AP scan for the neighbor messages 
 * and sends to AC.
 * u4RadioIfIndex, u2ScannedChannel, NeighborAPMac - Table Index */
typedef struct 
{
    tRBNodeEmbd     NeighborScanDBNode; 
    UINT4           u4RadioIfIndex;
    UINT4           u4LastUpdatedTime;
    UINT4           u4RogueApLastReportedTime;
    INT2            i2Rssi;
    UINT2           u2ScannedChannel;
    tMacAddr        NeighborAPMac;
    UINT1           u1NonGFPresent;
    UINT1           u1OBSSNonGFPresent;
    UINT1           u1EntryStatus;
    UINT1     au1ssid[WSSMAC_MAX_SSID_LEN];
    tMacAddr        BSSIDRogueApLearntFrom;
    BOOL1     isRogueApStatus;
    UINT1           u1StationCount;
    UINT1     au1RfGroupID[WSS_RF_GROUP_ID_MAX_LEN];
    BOOL1           isRogueApProcetionType;
    UINT1           au1Pad[3];
} tRfMgmtNeighborScanDB;

typedef struct 
{
    BOOL1           bRadioIfIndex;
    BOOL1           bRssi;
    BOOL1           bSNRValue;
    BOOL1           bScannedChannel;
    BOOL1           bNeighborAPMac;
    BOOL1           bEntryStatus;
    BOOL1           bssid;
    BOOL1           u1RogueApLastReportedTime;
} tRfMgmtNeighborScanIsSetDB;

typedef struct
{
    tRfMgmtNeighborScanDB       RfMgmtNeighborScanDB;
    tRfMgmtNeighborScanIsSetDB  RfMgmtNeighborScanIsSetDB;
}tRfMgmtNeighborScanTable;


/* The below data structure will be created when AP Profile and Radio IF is created
 * This table contains the configuration parameters required for Client SNR 
 * Scanning 
 * u4RadioIfIndex - Table Index */
typedef struct 
{
    tRBNodeEmbd     ClientConfigDBNode; 
    UINT4           u4RadioIfIndex;
    UINT4           u4ScanPeriodUpdatedTime;
    UINT4           u4TxPowerChangeTime;
    UINT2           u2TxPowerLevel;
    UINT2           u2SNRScanPeriod;
    INT2            i2SNRThreshold;
    UINT1           u1TxPowerChangeCount;
    UINT1           u1SNRScanStatus;
    UINT1           u1BssidScanStatus[RFMGMT_MAX_BSS_PER_RADIO];
    UINT1           u1WlanId;
    UINT1           au1Pad[3];
} tRfMgmtClientConfigDB;

typedef struct 
{
    BOOL1           bRadioIfIndex;
    BOOL1           bTxPowerChangeTime;
    BOOL1           bTxPowerLevel;
    BOOL1           bSNRScanPeriod;
    BOOL1           bTxPowerChangeCount;
    BOOL1           bSNRScanStatus;
    BOOL1           bSNRThreshold;
    BOOL1           bBssidScanStatus;
    BOOL1           bWlanId;
    UINT1           au1Pad[3];
} tRfMgmtClientConfigIsSetDB;


typedef struct
{
    tRfMgmtClientConfigDB       RfMgmtClientConfigDB;
    tRfMgmtClientConfigIsSetDB  RfMgmtClientConfigIsSetDB;
}tRfMgmtClientConfigTable;


/* The below data structure will be created when an AP sends Client SNR
 * information to the WLC.
 * u4RadioIfIndex, ClientMacAddr - Table Index */
typedef struct 
{
    tRBNodeEmbd     ClientScanDBNode;
    UINT4           u4RadioIfIndex;
    UINT4           u4LastUpdatedTime;
    UINT4           u4LastClientSNRScan;
    INT2            i2ClientSNR;
    tMacAddr        ClientMacAddress;
    UINT1           u1WlanId;
    UINT1           u1EntryStatus;
    UINT1           au1Pad[2];
} tRfMgmtClientScanDB;


typedef struct 
{
    BOOL1           bLastClientSNRScan;
    BOOL1           bClientSNR;
    BOOL1           bEntryStatus;
    UINT1           au1Pad;
} tRfMgmtClientScanIsSetDB;


typedef struct
{
    tRfMgmtClientScanDB        RfMgmtClientScanDB;
    tRfMgmtClientScanIsSetDB   RfMgmtClientScanIsSetDB;
}tRfMgmtClientScanTable;
         
/* The below data structure will be created when AP Profile and Radio IF is created
 * This table contains the channel number of neighbor AP by scan
 * u4RadioIfIndex - Table Index */

typedef struct
{
    tRBNodeEmbd     ChannelAllowedDBNode;
    UINT4           u4RadioIfIndex;
    UINT4           u4RadioType;
    UINT1           u1Channel;
    UINT1           u1SecChannelOffset;
    UINT1           au1Pad[2];
}tRfMgmtChannelAllowedScanDB;


typedef struct
{
   tRfMgmtChannelAllowedScanDB RfMgmtChannelAllowedScanDB;   
}tRfMgmtChannelAllowedScanTable;

typedef struct {
    union {
        tRfMgmtApConfigTable        RfMgmtApConfigTable;
        tRfMgmtNeighborScanTable    RfMgmtNeighborScanTable;
        tRfMgmtClientConfigTable    RfMgmtClientConfigTable;
        tRfMgmtClientScanTable      RfMgmtClientScanTable;
        tRfMgmtChannelAllowedScanTable RfMgmtChannelAllowedScanTable;
    }unRfMgmtDB;
}tRfMgmtDB;

typedef struct 
{
    tRBTree          RfMgmtNeighborAPConfigDB;
    tRBTree          RfMgmtNeighborAPScanDB;
    tRBTree          RfMgmtClientSNRConfigDB;
    tRBTree          RfMgmtClientScanDB;
    tRBTree          RfMgmtChannelAllowedScanDB;
} tRfMgmtGlbMib;

enum {
    RFMGMT_NEIGH_SCAN_TMR = 0,
    RFMGMT_CHAN_SCAN_TMR,
    RFMGMT_TX_MESSAGE_EXP_TMR,
    RFMGMT_CLIENT_SNR_SCAN_TMR,
    RFMGMT_11H_TPC_REQUEST_TMR,
 RFMGMT_CHAN_SWIT_ANNOUNCE_TMR,
 RFMGMT_EX_CHAN_SWIT_ANNOUNCE_TMR,
    RFMGMT_CAC_TMR,
    RFMGMT_MAX_TMR
};

typedef struct{
    /*CAPWAP Timer List ID */
    tTimerListId        RfMgmtTmrListId;
    tTmrDesc            aRfMgmtTmrDesc[RFMGMT_MAX_TMR];
    tTmrBlk             RfMgmtNeighAPTmrBlk;
    tTmrBlk             RfMgmtChannelScanTmrBlk;
    tTmrBlk             RfMgmtTxMsgExpTmrBlk;
    tTmrBlk             RfMgmtClientTmrBlk;
    tTmrBlk             RfMgmt11hTPCRequestTmrBlk;
    tTmrBlk             RfMgmtChannelSwitTmrBlk;
    tTmrBlk             RfMgmtCacTmrBlk;
}tRfMgmtTimerList;

typedef struct RFMGMT_GLOBALS {
    tRfMgmtGlbMib       RfMgmtGlbMib;
    tOsixTaskId         rfMgmtTaskId;
    tOsixQId            rfMgmtQueId;
    tOsixSemId          SemId;
    tMemPoolId          QMsgPoolId;
    UINT1               u1RfmgmtTaskInitialized;
    UINT1            au1Pad[3];

} tRfMgmtGlobals;

/* Enum for Database Operations */
typedef enum{

    RFMGMT_CREATE_RADIO_IF_INDEX_ENTRY,
    RFMGMT_DESTROY_RADIO_IF_INDEX_ENTRY,
    
    RFMGMT_SET_AP_CONFIG_ENTRY,
    RFMGMT_GET_AP_CONFIG_ENTRY,
   
    RFMGMT_SET_CLIENT_CONFIG_ENTRY,
    RFMGMT_GET_CLIENT_CONFIG_ENTRY,

    RFMGMT_GET_FIRST_NEIGH_ENTRY,
    RFMGMT_GET_NEXT_NEIGH_ENTRY,

    RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY,
    RFMGMT_SET_NEIGHBOR_SCAN_ENTRY,
 RFMGMT_SET_NEIGHBOR_ROUGE_SCAN_ENTRY,
    RFMGMT_GET_NEIGHBOR_SCAN_ENTRY,
    RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY,

    RFMGMT_GET_FIRST_CLIENT_ENTRY,
    RFMGMT_GET_NEXT_CLIENT_ENTRY,

    RFMGMT_CREATE_CLIENT_SCAN_ENTRY,
    RFMGMT_SET_CLIENT_SCAN_ENTRY,
    RFMGMT_GET_CLIENT_SCAN_ENTRY,
    RFMGMT_DESTROY_CLIENT_SCAN_ENTRY,
         
    RFMGMT_CREATE_SCAN_ALLOWED_ENTRY,
    RFMGMT_GET_FIRST_SCAN_ALLOWED_ENTRY,
    RFMGMT_GET_NEXT_SCAN_ALLOWED_ENTRY,
    RFMGMT_DESTROY_SCAN_ALLOWED_ENTRY,
}eRfMgmtDBMsgType;


#endif
