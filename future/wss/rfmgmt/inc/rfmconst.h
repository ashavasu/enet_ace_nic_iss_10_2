/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmconst.h,v 1.4 2017/11/24 10:37:04 siva Exp $
 *
 * Description: This file contains the macro defined for rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RFMGMT_CONST_H__
#define __RFMGMT_CONST_H__

#define NEIGH_CONFIG_VENDOR_MSG_LEN             18
#define CLIENT_CONFIG_VENDOR_MSG_LEN            16
#define CH_SWITCH_STATUS_VENDOR_MSG_LEN 10
#define TPC_SPEC_MGMT_VENDOR_MSG_LEN 12
#define SPEC_MGMT_VENDOR_DFS_MSG_LEN 18                   
#define RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN      14

#define RF_NEIGH_CONFIG_VENDOR_MSG_AP_ELM_LEN   58 
#ifdef ROGUEAP_WANTED
#define RF_ROGUE_CONFIG_VENDOR_MSG_ELM_LEN      24 
#define ROGUE_AP_NONE 1
#define ROGUE_AP_ALERT 2
#define ROGUE_AP_CONTAIN 3

#define ROGUE_AP_UNCLASSIFY 1
#define ROGUE_AP_FRIENDLY 2
#define ROGUE_AP_MALICIOUS 3

#endif 
#define RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN     12
#define RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN  6
#define TPC_SPECT_MGMT_VENDOR_MSG_ELM_LEN  8
#define SPECT_MGMT_VENDOR_DFS_MSG_ELM_LEN           14       
#define RFMGMT_MAC_STRING_LEN                   18

#define RFMGMT_MAX_CHANNEL                     RADIOIF_MAX_CHANNEL_A
#define RFMGMT_MAX_PKT_LEN                     50
#define RFMGMT_BUFFER_SIZE                     256
#define MAX_RFMGMT_PKTBUF         50
#define MAX_RFMGMT_PKT_QUE_SIZE                200
#define RFMGMT_MAX_NEIGHBOR                    32
#define MAX_RFMGMT_CHANNEL_ALLOWED_SCAN_SIZE   MAX_NUM_OF_RADIOS * RFMGMT_MAX_CHANNELA 
#ifdef ROGUEAP_WANTED
#define MAX_RFMGMT_ROGUEAPSCAN_SIZE            128
#define MAX_RFMGMT_ROGUE_RULE_SIZE             128
#define MAX_RFMGMT_ROGUE_MANUAL_SIZE           128
#define RFMGMT_ROGUE_CONF_MSG_LEN              24
#endif 

#define MAX_RFMGMT_AUTORF_SIZE          3
#define MAX_RFMGMT_AUTORF          3
#define MAX_RFMGMT_APCONFIG_SIZE        MAX_NUM_OF_RADIOS
#define MAX_RFMGMT_APCONFIG             MAX_NUM_OF_RADIOS
#define MAX_RFMGMT_NEIGHSCAN_SIZE       MAX_NUM_OF_RADIOS * RFMGMT_MAX_NEIGHBOR
#define MAX_RFMGMT_NEIGHSCAN            MAX_NUM_OF_RADIOS * RFMGMT_MAX_NEIGHBOR
#define MAX_RFMGMT_CLIENTCONFIG_SIZE    MAX_NUM_OF_RADIOS
#define MAX_RFMGMT_CLIENTCONFIG         MAX_NUM_OF_RADIOS
#define MAX_RFMGMT_CLIENTSCAN_SIZE      MAX_NUM_OF_STA_PER_WLC
#define MAX_RFMGMT_TPCINFO_SIZE         MAX_NUM_OF_STA_PER_WLC
#define MAX_RFMGMT_DFSINFO_SIZE         MAX_NUM_OF_STA_PER_WLC
#define MAX_RFMGMT_CLIENTSCAN           MAX_NUM_OF_STA_PER_WLC
#define MAX_RFMGMT_BSSIDSCAN            (MAX_NUM_OF_RADIOS *  MAX_RFMGMT_WLAN_ID)
#define MAX_RFMGMT_BSSIDSCAN_SIZE       (MAX_NUM_OF_RADIOS *  MAX_RFMGMT_WLAN_ID)
#define MAX_RFMGMT_FAILED_AP_NEIGH_SIZE MAX_NUM_OF_RADIOS * RFMGMT_MAX_NEIGHBOR
#define MAX_RFMGMT_WLAN_ID               16

#define RFMGMT_SUCCESS                     OSIX_SUCCESS
#define RFMGMT_FAILURE                     OSIX_FAILURE
#define RFMGMT_NO_ENTRY_FOUND              2 
#define RFMGMT_AP_RUN_STATE_REACHED        3 
#define RFMGMT_DEFAULT_RADIOA_CHANNEL      100

#define RFMGMT_MAX_TX_POWER                19
#define RFMGMT_MAX_TXPOWER_LEVEL           1
#define RFMGMT_MIN_TXPOWER_LEVEL           8
#define RFMGMT_NUM_OF_TXPOWER_LEVEL        8
#define TX_UPPER_THRESHOLD_LIMIT           6
#define TX_LOWER_THRESHOLD_LIMIT           3

#define RFMGMT_WLC_TASK_NAME          (const UINT1 *) "RFMACT"
#define RFMGMT_WTP_TASK_NAME          (const UINT1 *) "RFMAPT"

#define RFMGMT_MIN_LINK_MARGIN              -128
#define RFMGMT_MAX_LINK_MARGIN               127   
#define RFMGMT_RECV_TASK_PRIORITY     100
#define RFMGMT_STACK_SIZE             OSIX_DEFAULT_STACK_SIZE

#define RFMGMT_WLC_SEM_NAME        (UINT1 *) "RFMACS"
#define RFMGMT_WLC_QUE_NAME           (const UINT1 *) "RFMACQ"
#define RFMGMT_WLC_QUEUE_DEPTH        30

#define RFMGMT_WTP_SEM_NAME        (UINT1 *) "RFMAPS"
#define RFMGMT_WTP_QUE_NAME           (const UINT1 *) "RFMAPQ"
#define RFMGMT_WTP_QUEUE_DEPTH        200

#define RFMGMT_QUE_MSG_EVENT          0x00000001
#define RFMGMT_SEM_CREATE_INIT_CNT    1

#define RFMGMT_NUM_TEN                10
#define RFMGMT_MAX_HEX_SINGLE_DIGIT   0xf

#define RFMGMT_LOCK                   RfMgmtLock ()
#define RFMGMT_UNLOCK                 RfMgmtUnLock ()

#define RFMGMT_DCA_INT_EXP_EVENT      0x00000002
#define RFMGMT_TPC_INT_EXP_EVENT      0x00000004

#define RFMGMT_TIMER_EXP_EVENT        0x00000002

#define RFMGMT_WLC_ALL_EVENTS  \
 ( RFMGMT_QUE_MSG_EVENT | RFMGMT_DCA_INT_EXP_EVENT | \
 RFMGMT_TPC_INT_EXP_EVENT )
#define RFMGMT_WTP_ALL_EVENTS  ( RFMGMT_NEIGH_SCAN_EXP_EVENT | RFMGMT_CHAN_SCAN_EXP_EVENT | RFMGMT_AUTO_SCAN_EXP_EVENT | RFMGMT_CLIENT_SCAN_EXP_EVENT )

#define RFMGMT_MAC_SIZE                             6


#define  RFMGMT_NEIGHBOR_MSG_TYPE                   10
#ifdef ROGUEAP_WANTED
 #define  RFMGMT_NEIGHBOR_MSG_LEN            78 
#else 
 #define  RFMGMT_NEIGHBOR_MSG_LEN            14
#endif 
#define  RFMGMT_DEFAULT_COUNT                       1

#define  RFMGMT_NEIGHBOR_SCAN_FIXED_LEN             14
#define  RFMGMT_NEIGHBOR_COUNT_FIXED_LEN            11

#define  RFMGMT_CLIENT_SCAN_MSG_TYPE                11
#define  RFMGMT_CLIENT_SCAN_MSG_LEN                 6

#define  RFMGMT_CLIENT_SCAN_FIXED_LEN               9

#define  RFMGMT_NEIGH_CONF_MSG_LEN                  14

#ifdef ROGUEAP_WANTED
#define  RFMGMT_ROGUE_CONF_MSG_LEN                  24
#endif 
#define  RFMGMT_CLIENT_CONF_MSG_LEN                 10
#define  RFMGMT_CH_SWITCH_STATUS_MSG_LEN            10
#define  RFMGMT_SPECT_MGMT_MSG_LEN                  12
#define  RFMGMT_SPECT_MGMT_DFS_MSG_LEN                  18      

#define RFMGMT_EXTERNAL_AP_DB_POOLID   \
   RFMGMTACMemPoolIds[MAX_RFMGMT_EXTERNAL_AP_SIZE_SIZING_ID]


#define RFMGMT_QUEUE_POOLID               \
    RFMGMTMemPoolIds[MAX_RFMGMT_PKT_QUE_SIZE_SIZING_ID]

#define RFMGMT_RADIO_TYPE_INDEX_POOLID   \
    RFMGMTACMemPoolIds[MAX_RFMGMT_AUTORF_SIZE_SIZING_ID]

#define RFMGMT_INDEX_DB_POOLID   \
    RFMGMTMemPoolIds[MAX_RFMGMT_APCONFIG_SIZE_SIZING_ID]

#define RFMGMT_CLIENT_INDEX_DB_POOLID   \
    RFMGMTMemPoolIds[MAX_RFMGMT_CLIENTCONFIG_SIZE_SIZING_ID]

#define RFMGMT_NEIGH_SCAN_DB_POOLID   \
    RFMGMTMemPoolIds[MAX_RFMGMT_NEIGHSCAN_SIZE_SIZING_ID]

#define RFMGMT_FAILED_AP_NEIGH_DB_POOLID   \
    RFMGMTMemPoolIds[MAX_RFMGMT_FAILED_AP_NEIGH_SIZE_SIZING_ID]

#define RFMGMT_CLIENT_SCAN_DB_POOLID   \
    RFMGMTMemPoolIds[MAX_RFMGMT_CLIENTSCAN_SIZE_SIZING_ID]

#define RFMGMT_BSSID_SCAN_DB_POOLID    \
   RFMGMTACMemPoolIds[MAX_RFMGMT_BSSIDSCAN_SIZE_SIZING_ID]

#define RFMGMT_AP_CHANNEL_ALLOWED_POOLID  \
   RFMGMTMemPoolIds[MAX_RFMGMT_CHANNEL_ALLOWED_SCAN_SIZE_SIZING_ID]
   
#define RFMGMT_TPC_INFO_DB_POOLID    \
   RFMGMTACMemPoolIds[MAX_RFMGMT_TPCINFO_SIZE_SIZING_ID]

#define RFMGMT_DFS_INFO_DB_POOLID    \
   RFMGMTACMemPoolIds[MAX_RFMGMT_DFSINFO_SIZE_SIZING_ID]
#ifdef ROGUEAP_WANTED
#define RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID    \
   RFMGMTACMemPoolIds[MAX_RFMGMT_ROGUEAPSCAN_SIZE_SIZING_ID]
#define RFMGMT_MANUAL_INFO_DB_POOLID    \
   RFMGMTACMemPoolIds[MAX_RFMGMT_ROGUE_MANUAL_SIZE_SIZING_ID]
#define RFMGMT_ROGUE_RULE_INFO_DB_POOLID    \
   RFMGMTACMemPoolIds[MAX_RFMGMT_ROGUE_RULE_SIZE_SIZING_ID]
#endif 

#define CONFIG_TIME_DELAY  1 

#define RFMGMT_NO_AP_PRESENT                         2
#define RFMGMT_RESPONSE_TIMEOUT                      3
#define RFMGMT_INVALID_RESPONSE                      8

/* Default values for configurable parameters */
#define RFMGMT_DEF_DCA_INTERVAL                  600
#define RFMGMT_DEF_TPC_INTERVAL                         600
#define RFMGMT_DEF_SHA_INTERVAL                         600
#define RFMGMT_DEF_DFS_INTERVAL                         600
#define RFMGMT_DEF_DCA_TIMER_INTERVAL                   60
#define RFMGMT_DEF_TPC_TIMER_INTERVAL                   60
#define RFMGMT_DEF_SHA_TIMER_INTERVAL                   60
#define RFMGMT_DEF_11H_TPC_TIMER_INTERVAL               60
#ifdef ROGUEAP_WANTED
#define RFMGMT_DEF_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR 20
#define RFMGMT_DEF_ROUGE_TIMER_INTERVAL                 60
#endif
#define RFMGMT_DEF_11H_DFS_TIMER_INTERVAL               60
#define RFMGMT_DEF_DCA_UPDATE_TIME                 0
#define RFMGMT_DEF_TPC_UPDATE_TIME                 0
#define RFMGMT_DEF_SHA_UPDATE_TIME                 0
#define RFMGMT_DEF_SNR_THRESHOLD                 20
#define RFMGMT_DEF_RSSI_THRESHOLD          -70
#define RFMGMT_DEF_POWER_THRESHOLD         -70
#define RFMGMT_DEF_DCA_MODE                  1
#define RFMGMT_DEF_DCA_SELECTION                 1
#define RFMGMT_DEF_TPC_STATUS      1
#define RFMGMT_DEF_11H_TPC_STATUS      1
#define RFMGMT_DEF_DCA_SENSITIVITY                 1
#define RFMGMT_DEF_DCA_UPDATE_CHANNEL                 2
#define RFMGMT_DEF_SHA_STATUS                 2
#define RFMGMT_DEF_TPC_MODE                  1
#define RFMGMT_DEF_TPC_SELECTION                 1
#define RFMGMT_DEF_CONSIDER_EXT_NEIGHBORS     1
#define RFMGMT_DEF_NEIGHBOR_COUNT_THRESHOLD   1
#define RFMGMT_DEF_TPC_UPDATE                           2
#define RFMGMT_DEF_TPC_REQUEST_INTERVAL      60
#define RFMGMT_DEF_CLIENT_THRESHOLD                 12
#define RFMGMT_DEF_SCANNED_CHANNEL                 0
#define RFMGMT_DEF_CURRENT_CHANNEL                 0
#define RFMGMT_DEF_NEIGHBOR_MSG_PERIOD             60
#define RFMGMT_DEF_CHANNEL_SCAN_DURATION         180
#define RFMGMT_DEF_NEIGHBOR_AGING_PERIOD         120
#define RFMGMT_DEF_CHANNEL_CHANGE_COUNT          0
#define RFMGMT_DEF_TX_POWER_CHANGE_TIME          0
#define RFMGMT_DEF_SNR_SCAN_PERIOD              60
#define RFMGMT_DEF_TX_POWER_CHANGE_COUNT         0
#define RFMGMT_DEF_SNR_SCAN_STATUS              1
#define RFMGMT_DEF_MIN_LINK_THRESHOLD           10
#define RFMGMT_DEF_MAX_LINK_THRESHOLD           15
#define RFMGMT_DEF_STA_COUNT_THRESHOLD          3
#define RFMGMT_DEF_11H_DFS_STATUS      2 
#define RFMGMT_DEF_DFS_QUIET_INTERVAL           25      
#define RFMGMT_DEF_DFS_QUIET_PERIOD             10            
#define RFMGMT_DEF_DFS_MEASUREMENT_INTERVAL         60      
#define RFMGMT_DEF_DFS_CHANNEL_SWITCH_STATUS           2      


#define RFMGMT_MIN_NEIGH_MSG_PERIOD                 60
#define RFMGMT_MAX_NEIGH_MSG_PERIOD                 3600
#define RFMGMT_MIN_NEIGH_AGE_PERIOD                 60
#define RFMGMT_MAX_NEIGH_AGE_PERIOD                 3600
#define RFMGMT_MIN_TPC_REQ_INTERVAL                 60
#define RFMGMT_MAX_TPC_REQ_INTERVAL                 3600
#define RFMGMT_MIN_AUTO_SCAN_PERIOD                 60
#define RFMGMT_MAX_AUTO_SCAN_PERIOD                 3600
#define RFMGMT_MIN_RSSI_THRS                        -90
#define RFMGMT_MAX_RSSI_THRS                        -60
#define RFMGMT_AUTO_SCAN_ENABLE                     1
#define RFMGMT_AUTO_SCAN_DISABLE                    2
#define RFMGMT_SNR_SCAN_ENABLE                      1
#define RFMGMT_SNR_SCAN_DISABLE                     2
#define RFMGMT_MIN_SNR_THRS                         0
#define RFMGMT_MAX_SNR_THRS                         30
#define RFMGMT_MIN_SNR_SCAN_PERIOD                  60
#define RFMGMT_MAX_SNR_SCAN_PERIOD                  3600

#define RFMGMT_DCA_CLIENT_THRESHOLD                 75
#define RFMGMT_MIN_DCA_CLIENT_THRESHOLD             1

#define RFMGMT_MIN_DCA_INTERVAL                     60
#define RFMGMT_MAX_DCA_INTERVAL                     3600

#define RFMGMT_MIN_REQUEST_INTERVAL_PERIOD          60
#define RFMGMT_MAX_REQUEST_INTERVAL_PERIOD          3600

#define RFMGMT_MIN_TPC_INTERVAL                     60
#define RFMGMT_MAX_TPC_INTERVAL                     3600

#define RFMGMT_MIN_NEIGH_COUNT_THRESHOLD            1
#define RFMGMT_MAX_NEIGH_COUNT_THRESHOLD            5


#define RFMGMT_DFS_ENABLE 1
#define RFMGMT_DFS_DISABLE 2

#define RFMGMT_CONSIDER_EXTERNAL_APS 1
#define RFMGMT_IGNORE_EXTERNAL_APS   2

#define RFMGMT_MIN_DFS_INTERVAL                     60
#define RFMGMT_MAX_DFS_INTERVAL                     3600

#define RFMGMT_MIN_DFS_QUIET_INTERVAL      10
#define RFMGMT_MAX_DFS_QUIET_INTERVAL      60

#define RFMGMT_MIN_DFS_QUIET_PERIOD               1
#define RFMGMT_MAX_DFS_QUIET_PERIOD               60

#define RFMGMT_MIN_DFS_MEASUREMENT_INTERVAL  60
#define RFMGMT_MAX_DFS_MEASUREMENT_INTERVAL  3600

#define RFMGMT_MIN_SHA_INTERVAL                     60
#define RFMGMT_MAX_SHA_INTERVAL                     3600

#define RFMGMT_MIN_STA_COUNT_THRESHOLD              1
#define RFMGMT_MAX_STA_COUNT_THRESHOLD              32

#define RFMGMT_RAD_OFFSET                           6
#define RFMGMT_SCAN_CHANNEL_OFFSET                  10
#define RFMGMT_RSSI_OFFSET                          12
#define RFMGMT_SNR_OFFSET                          10

#define RFMGMT_NEIGH_ENTRY_ADD                      1
#define RFMGMT_NEIGH_ENTRY_DELETE                   2
#define RFMGMT_NEIGH_ENTRY_NO_CHG                   3

#define RFMGMT_CLIENT_ENTRY_ADD                     1
#define RFMGMT_CLIENT_ENTRY_DELETE                  2
#define RFMGMT_CLIENT_ENTRY_NO_CHG                  3

#define RFMGMT_AGEOUT_PERIOD                        10
#define RFMGMT_AP_SCAN_PERIOD                       3
#define RFMGMT_TX_MSG_PERIOD                        10
#define RFMGMT_CLIENT_SCAN_PERIOD                   10
#define RFMGMT_TPC_REQUEST_PERIOD                   10
#define RFMGMT_CHANL_SWIT_AGEOUT_PERIOD             01
#define RFMGMT_CAC_AGEOUT_PERIOD                    01

#define RFMGMT_SHA_ENABLE                           1
#define RFMGMT_SHA_DISABLE                          2
#define RFMGMT_TX_INCREASE_TO_MAX                   8
#define RFMGMT_TX_INCREASE_ONE_LEVEL                1

#define RFMGMT_TPC_MODE_GLOBAL   1
#define RFMGMT_TPC_MODE_PER_AP  2
#define RFMGMT_PER_AP_TPC_GLOBAL  1
#define RFMGMT_PER_AP_TPC_MANUAL  2
#define RFMGMT_TPC_SELECTION_AUTO   1
#define RFMGMT_TPC_SELECTION_ONCE 2
#define RFMGMT_TPC_SELECTION_OFF 3

#define RFMGMT_RADIO_TYPEA  2
#define RFMGMT_RADIO_TYPEB  1
#define RFMGMT_RADIO_TYPEG  4
#define RFMGMT_RADIO_TYPEBG     5
#define RFMGMT_RADIO_TYPEAN     10
#define RFMGMT_RADIO_TYPEBGN    13
#define RFMGMT_RADIO_TYPEAC     0x80000000

#define RFMGMT_DCA_MODE_GLOBAL   1
#define RFMGMT_DCA_MODE_PER_AP  2
#define RFMGMT_PER_AP_DCA_GLOBAL  1
#define RFMGMT_PER_AP_DCA_MANUAL  2
#define RFMGMT_DCA_SELECTION_AUTO   1
#define RFMGMT_DCA_SELECTION_ONCE 2
#define RFMGMT_DCA_SELECTION_OFF 3


#define RFMGMT_DCA_SENSITIVITY_LOW      1
#define RFMGMT_DCA_SENSITIVITY_MEDIUM   2
#define RFMGMT_DCA_SENSITIVITY_HIGH     3

/* #define RFMGMT_MAX_CHANNELA             RADIOIF_MAX_CHANNEL_A */
#define RFMGMT_MAX_CHANNELB             14
#define RFMGMT_FIRST_CHANNEL_RADIOA     36         
#define RFMGMT_DCA_UPDATE_CHANNEL  1
#define RFMGMT_TPC_UPDATE  1

#ifdef ROGUEAP_WANTED
#define RF_GROUP_NAME_LENGTH       19
#endif 

#define RFMGMT_DCA_UPDATE_CHANNEL_FAIL  3
#define RFMGMT_TPC_UPDATE_FAIL      3

#define RFMGMT_CHANNEL_SWITCH_MSG_ENABLE 1
#define RFMGMT_CHANNEL_SWITCH_MSG_DISABLE 2

#define RFMGMT_DCA_CHANNEL_ADD  1
#define RFMGMT_DCA_CHANNEL_DELETE 2

#define RFMGMT_BSSID_SCAN_STATUS_ENABLE  1
#define RFMGMT_BSSID_SCAN_STATUS_DISABLE 2

#define RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE 1
#define RFMGMT_TX_POWER_CHANGE_TRAP_DISABLE 2

#define RFMGMT_TPC_ENABLE 1
#define RFMGMT_TPC_DISABLE 2

#define RFMGMT_DFS_ENABLE 1
#define RFMGMT_DFS_DISABLE 2

#define RFMGMT_11H_TPC_ENABLE 1
#define RFMGMT_11H_TPC_DISABLE 2

#define RFMGMT_MIN_LINK_THRESHOLD  0
#define RFMGMT_MAX_LINK_THRESHOLD  30

#define RFMGMT_CHANNEL_CHANGE_TRAP_ENABLE   1
#define RFMGMT_CHANNEL_CHANGE_TRAP_DISABLE  2

#define RFMGMT_MAX_AP             NUM_OF_AP_SUPPORTED
#define RFMGMT_MAX_RADIOS_PER_AP  MAX_NUM_OF_RADIO_PER_AP
#define RFMGMT_MAX_RADIOS         MAX_NUM_OF_RADIOS
      
#define RFMGMT_OVERLAP_CHAN             14
#define RFMGMT_NON_OVERLAP_CHAN         3
#define RFMGMT_NON_OVERLAP_CHAN_OFFSET 5
#define RFMGMT_RUN_STATE_TRIGGER        1

#define RFMGMT_MAC_STR_LEN  21
#define RFMGMT_NEIGH_DELETED    3

/* CRU BUF Macros */
#define RFMGMT_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)

#define RFMGMT_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))

#define RFMGMT_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)


#define RFMGMT_RELEASE_CRU_BUF(pBuf) \
        CRU_BUF_Release_MsgBufChain ((pBuf), 0)

#define RFMGMT_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src), u4Offset, u4Size)

#define RFMGMT_ALLOCATE_CRU_BUF(u4len) \
        CRU_BUF_Allocate_MsgBufChain (u4Len, 0);

#define RFMGMT_CHANNEL_CHANGE_TRAP   1
#define RFMGMT_TXPOWER_CHANGE_TRAP   2
#ifdef ROGUEAP_WANTED
#define RFMGMT_ROGUE_ENABLE 1
#define RFMGMT_ROGUE_TIMEOUT_MIN_VALUE 240
#define RFMGMT_ROGUE_TIMEOUT_MAX_VALUE 3600
#define RFMGMT_ROGUE_ENABLE 1
#define RFMGMT_ROGUE_STATUS_TRAP     3
#endif 


#define RFM_SNMP_V2_TRAP_OID_LEN            11
#define  RFMGMT_CHANNEL_TRAP_STATUS  gu4ChannelChangeTrapStatus

#define  RFMGMT_TXPOWER_TRAP_STATUS  gu4TxPowerTrapStatus
#ifdef ROGUEAP_WANTED
#define  RFMGMT_ROGUE_TRAP_STATUS    gu1RogueTrap
#endif 

#define RFMGMT_IS_TIME_EXCEEDED (u4CurrentTime, u4Period, u1Result) \
{\
    UINT4 u4Val2 = u4CurrentTime;\
    UINT4 u4Val1 = u4Period;\
\
    if (u4Val1 <= u4Val2) {\
        u1Result = OSIX_TRUE;\
    }\
}

#define    RFMGMT_OVERLAP_1     0
#define    RFMGMT_OVERLAP_2     1
#define    RFMGMT_OVERLAP_3     2
#define    RFMGMT_OVERLAP_4     3
#define    RFMGMT_OVERLAP_5     4

#define    RFMGMT_OVERLAP_MIN1     0
#define    RFMGMT_OVERLAP_MIN2     9
#define    RFMGMT_OVERLAP_MIN3     32
#define    RFMGMT_OVERLAP_MIN4     55
#define    RFMGMT_OVERLAP_MIN5     77
#define    RFMGMT_OVERLAP_MAX      100

#define    RFMGMT_OFFSET_ZERO  0
#define    RFMGMT_OFFSET_ONE   1
#define    RFMGMT_OFFSET_TWO   2
#define    RFMGMT_OFFSET_THREE 3
#define    RFMGMT_INIT_VALUE   100

#define RFMGMT_MAX_COLORS 14
#define RFMGMT_MAX_SCAN_PERIOD       2
#define RFMGMT_FLUSH_DB              1
#define RFMGMT_FLUSH_DB_ONLY         2
#define RFMGMT_FLUSH_IF_WLAN_DELETE  3
#define RFMGMT_MAX_DATE_LEN          30
#define RFMGMT_TPC_REQUEST_SPLIT_PERIOD 4
#define MAX_RFMGMT_EXTERNAL_AP_SIZE 32

#ifdef ROGUEAP_WANTED
#define ROGUEAP_RB_GREATER 1
#define ROGUEAP_RB_LESS -1
#define ROGUEAP_RB_EQUAL 0
#define MAX_ROGUE_AP 128
#define DEFAULT_PROTECTION 2
#define DEFAULT_RSSI -129
#define DEFAULT_CLIENT_COUNT 76
#define DEFAULT_DURATION 3601 
#define MAX_PRIORITY_COUNT 128
#define ONE 1
#define ROGUE_AP_CONTAIN 3
#define ROGUE_AP_ALERT 2
#endif 
#endif
