/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmgmtsz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains RFMGMT Sizing parameters
 *
 ********************************************************************/

enum {
    MAX_RFMGMT_APCONFIG_SIZE_SIZING_ID,
#ifdef WTP_WANTED    
    MAX_RFMGMT_CHANNEL_ALLOWED_SCAN_SIZE_SIZING_ID,
#endif    
    MAX_RFMGMT_CLIENTCONFIG_SIZE_SIZING_ID,
    MAX_RFMGMT_CLIENTSCAN_SIZE_SIZING_ID,
    MAX_RFMGMT_NEIGHSCAN_SIZE_SIZING_ID,
    MAX_RFMGMT_PKT_QUE_SIZE_SIZING_ID,
    RFMGMT_MAX_SIZING_ID
};


#ifdef  _RFMGMTSZ_C
tMemPoolId RFMGMTMemPoolIds[ RFMGMT_MAX_SIZING_ID];
INT4  RfmgmtSizingMemCreateMemPools(VOID);
VOID  RfmgmtSizingMemDeleteMemPools(VOID);
INT4  RfmgmtSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RFMGMTSZ_C  */
extern tMemPoolId RFMGMTMemPoolIds[ ];
extern INT4  RfmgmtSizingMemCreateMemPools(VOID);
extern VOID  RfmgmtSizingMemDeleteMemPools(VOID);
#endif /*  _RFMGMTSZ_C  */


#ifdef  _RFMGMTSZ_C
tFsModSizingParams FsRFMGMTSizingParams [] = {
{ "tRfMgmtAPConfigDB", "MAX_RFMGMT_APCONFIG_SIZE", sizeof(tRfMgmtAPConfigDB),MAX_RFMGMT_APCONFIG_SIZE, MAX_RFMGMT_APCONFIG_SIZE,0 },
#ifdef WTP_WANTED    
{ "tRfMgmtChannelAllowedScanDB", "MAX_RFMGMT_CHANNEL_ALLOWED_SCAN_SIZE", sizeof(tRfMgmtChannelAllowedScanDB),MAX_RFMGMT_CHANNEL_ALLOWED_SCAN_SIZE, MAX_RFMGMT_CHANNEL_ALLOWED_SCAN_SIZE,0 },
#endif 
{ "tRfMgmtClientConfigDB", "MAX_RFMGMT_CLIENTCONFIG_SIZE", sizeof(tRfMgmtClientConfigDB),MAX_RFMGMT_CLIENTCONFIG_SIZE, MAX_RFMGMT_CLIENTCONFIG_SIZE,0 },
{ "tRfMgmtClientScanDB", "MAX_RFMGMT_CLIENTSCAN_SIZE", sizeof(tRfMgmtClientScanDB),MAX_RFMGMT_CLIENTSCAN_SIZE, MAX_RFMGMT_CLIENTSCAN_SIZE,0 },
{ "tRfMgmtNeighborScanDB", "MAX_RFMGMT_NEIGHSCAN_SIZE", sizeof(tRfMgmtNeighborScanDB),MAX_RFMGMT_NEIGHSCAN_SIZE, MAX_RFMGMT_NEIGHSCAN_SIZE,0 },
{ "tRfMgmtQueueReq", "MAX_RFMGMT_PKT_QUE_SIZE", sizeof(tRfMgmtQueueReq),MAX_RFMGMT_PKT_QUE_SIZE, MAX_RFMGMT_PKT_QUE_SIZE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RFMGMTSZ_C  */
extern tFsModSizingParams FsRFMGMTSizingParams [];
#endif /*  _RFMGMTSZ_C  */


