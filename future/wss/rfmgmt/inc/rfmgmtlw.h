/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rfmgmtlw.h,v 1.2 2017/05/23 14:16:52 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmDebugOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmDebugOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmDebugOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrmConfigTable. */
INT1
nmhValidateIndexInstanceFsRrmConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrmConfigTable  */

INT1
nmhGetFirstIndexFsRrmConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmDcaMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmDcaSelectionMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmTpcMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmTpcSelectionMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmTpcUpdate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmDcaInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmDcaSensitivity ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmAllowedChannels ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRrmUnusedChannels ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRrmUpdateChannel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmLastUpdatedTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmRSSIThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmClientThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmTpcInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmSNRThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmTpcLastUpdatedTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmDcaMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmDcaSelectionMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmTpcMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmTpcSelectionMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmTpcUpdate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmDcaInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmDcaSensitivity ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmAllowedChannels ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRrmUnusedChannels ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRrmUpdateChannel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmRSSIThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmClientThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmTpcInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmSNRThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmDcaMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmDcaSelectionMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmTpcMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmTpcSelectionMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmTpcUpdate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmDcaInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmDcaSensitivity ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmAllowedChannels ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRrmUnusedChannels ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRrmUpdateChannel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmRSSIThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmClientThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmTpcInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmSNRThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmChannelSwitchMsgStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmChannelSwitchMsgStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmChannelSwitchMsgStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmChannelSwitchMsgStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrmAPConfigTable. */
INT1
nmhValidateIndexInstanceFsRrmAPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrmAPConfigTable  */

INT1
nmhGetFirstIndexFsRrmAPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmAPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmAPAutoScanStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmAPNeighborScanFreq ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmAPChannelScanDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmAPNeighborAgingPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmAPMacAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsRrmAPChannelMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmAPChannelChangeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmAPChannelChangeTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmAPAssignedChannel ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsRrm11hTPCRequestInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhSetFsRrm11hTPCRequestInterval ARG_LIST((INT4  ,UINT4 ));
INT1
nmhGetFsRrmAPChannelAllowedList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
INT1
nmhGetFsRrm11hDFSQuietInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hDFSQuietPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hDFSMeasurementInterval ARG_LIST((INT4 ,UINT4 *));

INT1 
nmhGetFsRrm11hDFSChannelSwitchStatus ARG_LIST((INT4 ,INT4 *));

INT1 
nmhGetFsRrm11hDfsLastRun ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hMeasureMapRadar ARG_LIST((INT4, tMacAddr, INT4 * ));

INT1
nmhDepv2FsRrm11hDfsInfoTable ARG_LIST((UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND * ));

INT1
nmhGetFsRrm11hChannelNumber ARG_LIST((INT4, tMacAddr, UINT1 * ));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmAPAutoScanStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmAPNeighborScanFreq ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmAPChannelScanDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmAPNeighborAgingPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmAPChannelMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrm11hDFSQuietInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hDFSQuietPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hDFSMeasurementInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hDFSChannelSwitchStatus ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhSetFsRrmAPAssignedChannel ARG_LIST((INT4  ,UINT4 ));
INT1
nmhSetFsRrmAPChannelAllowedList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmAPAutoScanStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmAPNeighborScanFreq ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmAPChannelScanDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmAPNeighborAgingPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmAPChannelMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmAPAssignedChannel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrm11hTPCRequestInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
INT1
nmhTestv2FsRrmAPChannelAllowedList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRrm11hDFSQuietInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 )); 

INT1
nmhTestv2FsRrm11hDFSQuietPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));  

INT1
nmhTestv2FsRrm11hDFSMeasurementInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));  

INT1
nmhTestv2FsRrm11hDFSChannelSwitchStatus ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmAPConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrmAPStatsTable. */
INT1
nmhValidateIndexInstanceFsRrmAPStatsTable ARG_LIST((INT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsRrmAPStatsTable  */

INT1
nmhGetFirstIndexFsRrmAPStatsTable ARG_LIST((INT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmAPStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmRSSIValue ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsRrmSNR ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsRrmLastUpdatedInterval ARG_LIST((INT4  , UINT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsRrmNeighborMsgRcvdCount ARG_LIST((INT4  , UINT4  , tMacAddr ,UINT4 *));

/* Proto Validate Index Instance for FsRrmTpcConfigTable. */
INT1
nmhValidateIndexInstanceFsRrmTpcConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrmTpcConfigTable  */

INT1
nmhGetFirstIndexFsRrmTpcConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmTpcConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmAPSNRScanStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmSNRScanFreq ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmSNRScanCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmBelowSNRThresholdCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmTxPowerChangeCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmClientsConnected ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmClientsAccepted ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmClientsDiscarded ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmAPTpcMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmAPTxPowerLevel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmTxPowerChangeTime ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmAPSNRScanStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmSNRScanFreq ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmAPTpcMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmAPTxPowerLevel ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmAPSNRScanStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmSNRScanFreq ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmAPTpcMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmAPTxPowerLevel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmTpcConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrmTpcClientTable. */
INT1
nmhValidateIndexInstanceFsRrmTpcClientTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsRrmTpcClientTable  */

INT1
nmhGetFirstIndexFsRrmTpcClientTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmTpcClientTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmClientSNR ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsRrmClientLastSNRScan ARG_LIST((INT4  , tMacAddr ,UINT4 *));

/* Proto Validate Index Instance for FsRrmTpcScanTable. */
INT1
nmhValidateIndexInstanceFsRrmTpcScanTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrmTpcScanTable  */

INT1
nmhGetFirstIndexFsRrmTpcScanTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmTpcScanTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmSSIDScanStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmSSIDScanStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmSSIDScanStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmTpcScanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmChannelChangeTrapStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRrmTxPowerChangeTrapStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmChannelChangeTrapStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRrmTxPowerChangeTrapStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmChannelChangeTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrmTxPowerChangeTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmChannelChangeTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrmTxPowerChangeTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrmExtConfigTable. */
INT1
nmhValidateIndexInstanceFsRrmExtConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrmExtConfigTable  */

INT1
nmhGetFirstIndexFsRrmExtConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmExtConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmSHAStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmSHAInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmSHAExecutionCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmDpaExecutionCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmPowerThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrm11hTpcStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrm11hTpcLastRun ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hTpcInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hMinLinkThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hMaxLinkThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hStaCountThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrm11hDfsStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrm11hDfsInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmConsiderExternalAPs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmClearNeighborInfo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrmNeighborCountThreshold ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrmSHAStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmSHAInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmPowerThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrm11hTpcStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrm11hTpcInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hMinLinkThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hMaxLinkThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hStaCountThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrm11hDfsStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrm11hDfsInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRrmConsiderExternalAPs ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmClearNeighborInfo ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrmNeighborCountThreshold ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrmSHAStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmSHAInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrmPowerThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrm11hTpcStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrm11hTpcInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrm11hMinLinkThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrm11hMaxLinkThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRrm11hStaCountThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
INT1
nmhTestv2FsRrm11hDfsStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrm11hDfsInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));


INT1
nmhTestv2FsRrmConsiderExternalAPs ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmClearNeighborInfo ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrmNeighborCountThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrmExtConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrmFailedAPStatsTable. */
INT1
nmhValidateIndexInstanceFsRrmFailedAPStatsTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsRrmFailedAPStatsTable  */

INT1
nmhGetFirstIndexFsRrmFailedAPStatsTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmFailedAPStatsTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmSHARSSIValue ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsRrmStatsTable. */
INT1
nmhValidateIndexInstanceFsRrmStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrmStatsTable  */

INT1
nmhGetFirstIndexFsRrmStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrmStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrmTxPowerIncreaseCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRrmTxPowerDecreaseCount ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsRrmTpcInfoTable. */
INT1
nmhValidateIndexInstanceFsRrm11hTpcInfoTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsRrmTpcInfoTable  */

INT1
nmhGetFirstIndexFsRrm11hTpcInfoTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhValidateIndexInstanceFsRrm11hDfsInfoTable ARG_LIST((INT4  , tMacAddr ));

INT1
nmhGetFirstIndexFsRrm11hDfsInfoTable ARG_LIST((INT4 * , tMacAddr * ));

INT1
nmhGetNextIndexFsRrm11hTpcInfoTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

INT1
nmhGetNextIndexFsRrm11hDfsInfoTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

INT1
nmhGetFsRrm11hDfsReportLastReceived ARG_LIST((INT4 , tMacAddr , UINT4 * ));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrm11hTxPowerLevel ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsRrm11hLinkMargin ARG_LIST((INT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsRrm11hTpcReportLastReceived ARG_LIST((INT4  , tMacAddr ,UINT4 *));

INT1
nmhGetFsRrm11hBaseLinkMargin ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrm11hTxPowerLevel ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsRrm11hLinkMargin ARG_LIST((INT4  , tMacAddr  ,INT4 ));

INT1
nmhSetFsRrm11hBaseLinkMargin ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrm11hTxPowerLevel ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsRrm11hLinkMargin ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

INT1
nmhTestv2FsRrm11hBaseLinkMargin ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */
INT1
nmhDepv2FsRrm11hTpcInfoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#ifdef ROGUEAP_WANTED
INT1
nmhGetFsRfGroupName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRfGroupName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRfGroupName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRfGroupName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRogueApDetecttion ARG_LIST((INT4 *));

INT1
nmhGetFsRogueApTimeout ARG_LIST((UINT4 *));

INT1
nmhGetFsRogueApMaliciousTrapStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRogueApDetecttion ARG_LIST((INT4 ));

INT1
nmhSetFsRogueApTimeout ARG_LIST((UINT4 ));

INT1
nmhSetFsRogueApMaliciousTrapStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRogueApDetecttion ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRogueApTimeout ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRogueApMaliciousTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRogueApDetecttion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRogueApTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRogueApMaliciousTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRogueApTable. */
INT1
nmhValidateIndexInstanceFsRogueApTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsRogueApTable  */

INT1
nmhGetFirstIndexFsRogueApTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRogueApTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRogueApSSID ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRogueApProcetionType ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApSNR ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApRSSI ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApOperationChannel ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApDetectedRadio ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApDetectedBand ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApLastReportedTime ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsRogueApLastHeardTime ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsRogueApTxPower ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApClass ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApState ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApClassifiedBy ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApClientCount ARG_LIST((tMacAddr ,UINT4 *));

INT1
nmhGetFsRogueApLastHeardBSSID ARG_LIST((tMacAddr ,tMacAddr * ));

INT1
nmhGetFsRogueApStatus ARG_LIST((tMacAddr ,INT4 *));

/* Proto Validate Index Instance for FsRogueRuleConfigTable. */
INT1
nmhValidateIndexInstanceFsRogueRuleConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsRogueRuleConfigTable  */

INT1
nmhGetFirstIndexFsRogueRuleConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRogueRuleConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRogueApRulePriOrderNum ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsRogueApRuleClass ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsRogueApRuleState ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsRogueApRuleDuration ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsRogueApRuleProtectionType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsRogueApRuleRSSI ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsRogueApRuleTpc ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsRogueApRuleSSID ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRogueApRuleClientCount ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsRogueApRuleStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRogueApRulePriOrderNum ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsRogueApRuleClass ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsRogueApRuleState ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsRogueApRuleDuration ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsRogueApRuleProtectionType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsRogueApRuleRSSI ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsRogueApRuleTpc ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsRogueApRuleSSID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRogueApRuleClientCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsRogueApRuleStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRogueApRulePriOrderNum ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsRogueApRuleClass ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsRogueApRuleState ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsRogueApRuleDuration ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsRogueApRuleProtectionType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsRogueApRuleRSSI ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsRogueApRuleTpc ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsRogueApRuleSSID ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRogueApRuleClientCount ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsRogueApRuleStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRogueRuleConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRogueManualConfigTable. */
INT1
nmhValidateIndexInstanceFsRogueManualConfigTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsRogueManualConfigTable  */

INT1
nmhGetFirstIndexFsRogueManualConfigTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRogueManualConfigTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRogueApManualClass ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApManualState ARG_LIST((tMacAddr ,INT4 *));

INT1
nmhGetFsRogueApManualStatus ARG_LIST((tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRogueApManualClass ARG_LIST((tMacAddr  ,INT4 ));

INT1
nmhSetFsRogueApManualState ARG_LIST((tMacAddr  ,INT4 ));

INT1
nmhSetFsRogueApManualStatus ARG_LIST((tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRogueApManualClass ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

INT1
nmhTestv2FsRogueApManualState ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

INT1
nmhTestv2FsRogueApManualStatus ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRogueManualConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif 
