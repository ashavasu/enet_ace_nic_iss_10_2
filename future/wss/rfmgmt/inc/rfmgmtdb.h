/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rfmgmtdb.h,v 1.3 2017/05/23 14:16:52 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _RFMGMTDB_H
#define _RFMGMTDB_H


#ifdef WLC_WANTED
UINT1 FsRrmConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
#endif
UINT1 FsRrmAPConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRrmAPStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsRrmTpcConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRrmTpcClientTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsRrmTpcScanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
#ifdef WLC_WANTED
UINT1 FsRrmExtConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRrmFailedAPStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsRrmStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRrm11hTpcInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsRrm11hDfsInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_UNSIGNED32};

#ifdef ROGUEAP_WANTED
UINT1 FsRogueApTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsRogueRuleConfigTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsRogueManualConfigTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
#endif
#endif

UINT4 rfmgmt [] ={1,3,6,1,4,1,29601,2,84};
tSNMP_OID_TYPE rfmgmtOID = {9, rfmgmt};


UINT4 FsRrmDebugOption [ ] ={1,3,6,1,4,1,29601,2,84,1,1};
#ifdef WLC_WANTED
UINT4 FsRrmRadioType [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,1};
UINT4 FsRrmDcaMode [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,2};
UINT4 FsRrmDcaSelectionMode [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,3};
UINT4 FsRrmTpcMode [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,4};
UINT4 FsRrmTpcSelectionMode [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,5};
UINT4 FsRrmTpcUpdate [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,6};
UINT4 FsRrmDcaInterval [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,7};
UINT4 FsRrmDcaSensitivity [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,8};
UINT4 FsRrmAllowedChannels [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,9};
UINT4 FsRrmUnusedChannels [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,10};
UINT4 FsRrmUpdateChannel [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,11};
UINT4 FsRrmLastUpdatedTime [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,12};
UINT4 FsRrmRSSIThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,13};
UINT4 FsRrmClientThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,14};
UINT4 FsRrmTpcInterval [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,15};
UINT4 FsRrmSNRThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,16};
UINT4 FsRrmTpcLastUpdatedTime [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,17};
UINT4 FsRrmRowStatus [ ] ={1,3,6,1,4,1,29601,2,84,1,2,1,18};
#endif
UINT4 FsRrmChannelSwitchMsgStatus [ ] ={1,3,6,1,4,1,29601,2,84,2,1};
UINT4 FsRrmAPAutoScanStatus [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,1};
UINT4 FsRrmAPNeighborScanFreq [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,2};
UINT4 FsRrmAPChannelScanDuration [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,3};
UINT4 FsRrmAPNeighborAgingPeriod [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,4};
UINT4 FsRrmAPMacAddress [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,5};
UINT4 FsRrmAPChannelMode [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,6};
UINT4 FsRrmAPChannelChangeCount [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,7};
UINT4 FsRrmAPChannelChangeTime [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,8};
UINT4 FsRrmAPAssignedChannel [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,9};
UINT4 FsRrm11hTPCRequestInterval [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,10};
UINT4 FsRrmAPChannelAllowedList [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,11};
UINT4 FsRrm11hDFSQuietInterval [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,12};
UINT4 FsRrm11hDFSMeasurementInterval [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,13};
UINT4 FsRrm11hDFSQuietPeriod [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,14};
UINT4 FsRrm11hDFSChannelSwitchStatus [ ] ={1,3,6,1,4,1,29601,2,84,2,2,1,15};
UINT4 FsRrmScannedChannel [ ] ={1,3,6,1,4,1,29601,2,84,3,1,1,1};
UINT4 FsRrmNeighborMacAddress [ ] ={1,3,6,1,4,1,29601,2,84,3,1,1,2};
UINT4 FsRrmRSSIValue [ ] ={1,3,6,1,4,1,29601,2,84,3,1,1,3};
UINT4 FsRrmSNR [ ] ={1,3,6,1,4,1,29601,2,84,3,1,1,4};
UINT4 FsRrmLastUpdatedInterval [ ] ={1,3,6,1,4,1,29601,2,84,3,1,1,5};
UINT4 FsRrmNeighborMsgRcvdCount [ ] ={1,3,6,1,4,1,29601,2,84,3,1,1,6};
UINT4 FsRrmAPSNRScanStatus [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,1};
UINT4 FsRrmSNRScanFreq [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,2};
UINT4 FsRrmSNRScanCount [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,3};
UINT4 FsRrmBelowSNRThresholdCount [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,4};
UINT4 FsRrmTxPowerChangeCount [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,5};
UINT4 FsRrmClientsConnected [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,6};
UINT4 FsRrmClientsAccepted [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,7};
UINT4 FsRrmClientsDiscarded [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,8};
UINT4 FsRrmAPTpcMode [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,9};
UINT4 FsRrmAPTxPowerLevel [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,10};
UINT4 FsRrmTxPowerChangeTime [ ] ={1,3,6,1,4,1,29601,2,84,4,1,1,11};
UINT4 FsRrmClientMacAddress [ ] ={1,3,6,1,4,1,29601,2,84,4,2,1,1};
UINT4 FsRrmClientSNR [ ] ={1,3,6,1,4,1,29601,2,84,4,2,1,2};
UINT4 FsRrmClientLastSNRScan [ ] ={1,3,6,1,4,1,29601,2,84,4,2,1,3};
UINT4 FsRrmWlanId [ ] ={1,3,6,1,4,1,29601,2,84,4,3,1,1};
UINT4 FsRrmSSIDScanStatus [ ] ={1,3,6,1,4,1,29601,2,84,4,3,1,2};
UINT4 FsRrmChannelChangeTrapStatus [ ] ={1,3,6,1,4,1,29601,2,84,5,1};
UINT4 FsRrmTxPowerChangeTrapStatus [ ] ={1,3,6,1,4,1,29601,2,84,5,2};
#ifdef WLC_WANTED
UINT4 FsRrmSHAStatus [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,1};
UINT4 FsRrmSHAInterval [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,2};
UINT4 FsRrmSHAExecutionCount [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,3};
UINT4 FsRrmDpaExecutionCount [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,4};
UINT4 FsRrmPowerThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,5};
UINT4 FsRrm11hTpcStatus [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,6};
UINT4 FsRrm11hTpcLastRun [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,7};
UINT4 FsRrm11hTpcInterval [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,8};
UINT4 FsRrm11hMinLinkThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,9};
UINT4 FsRrm11hMaxLinkThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,10};
UINT4 FsRrm11hStaCountThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,11};
UINT4 FsRrm11hDfsInterval [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,12};
UINT4 FsRrm11hDfsStatus [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,13};
UINT4 FsRrmConsiderExternalAPs [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,14};
UINT4 FsRrmClearNeighborInfo [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,15};
UINT4 FsRrmNeighborCountThreshold [ ] ={1,3,6,1,4,1,29601,2,84,1,3,1,16};

#ifdef ROGUEAP_WANTED
UINT4 FsRfGroupName [ ] ={1,3,6,1,4,1,29601,2,84,1,4};
#endif
UINT4 FsRrmSHANeighborMacAddress [ ] ={1,3,6,1,4,1,29601,2,84,4,4,1,1};
UINT4 FsRrmSHARSSIValue [ ] ={1,3,6,1,4,1,29601,2,84,4,4,1,2};
UINT4 FsRrmTxPowerIncreaseCount [ ] ={1,3,6,1,4,1,29601,2,84,7,1,1,1};
UINT4 FsRrmTxPowerDecreaseCount [ ] ={1,3,6,1,4,1,29601,2,84,7,1,1,2};
UINT4 FsRrm11hStationMacAddress [ ] ={1,3,6,1,4,1,29601,2,84,8,1,1,1};
UINT4 FsRrm11hTxPowerLevel [ ] ={1,3,6,1,4,1,29601,2,84,8,1,1,2};
UINT4 FsRrm11hLinkMargin [ ] ={1,3,6,1,4,1,29601,2,84,8,1,1,3};
UINT4 FsRrm11hTpcReportLastReceived [ ] ={1,3,6,1,4,1,29601,2,84,8,1,1,4};
UINT4 FsRrm11hBaseLinkMargin [ ] ={1,3,6,1,4,1,29601,2,84,8,1,1,5};
#ifdef ROGUEAP_WANTED
UINT4 FsRogueApTimeout [ ] ={1,3,6,1,4,1,29601,2,84,10,1};
UINT4 FsRogueApMaliciousTrapStatus [ ] ={1,3,6,1,4,1,29601,2,84,10,2};
UINT4 FsRogueApTrapBssid [ ] ={1,3,6,1,4,1,29601,2,84,10,0,1};
UINT4 FsRogueApBSSID [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,1};
UINT4 FsRogueApSSID [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,2};
UINT4 FsRogueApProcetionType [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,3};
UINT4 FsRogueApSNR [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,4};
UINT4 FsRogueApRSSI [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,5};
UINT4 FsRogueApOperationChannel [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,6};
UINT4 FsRogueApDetectedRadio [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,7};
UINT4 FsRogueApDetectedBand [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,8};
UINT4 FsRogueApLastReportedTime [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,9};
UINT4 FsRogueApLastHeardTime [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,10};
UINT4 FsRogueApTxPower [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,11};
UINT4 FsRogueApClass [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,12};
UINT4 FsRogueApState [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,13};
UINT4 FsRogueApClassifiedBy [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,14};
UINT4 FsRogueApClientCount [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,15};
UINT4 FsRogueApLastHeardBSSID [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,16};
UINT4 FsRogueApStatus [ ] ={1,3,6,1,4,1,29601,2,84,10,3,1,17};
UINT4 FsRogueApRuleName [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,1};
UINT4 FsRogueApRulePriOrderNum [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,2};
UINT4 FsRogueApRuleClass [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,3};
UINT4 FsRogueApRuleState [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,4};
UINT4 FsRogueApRuleDuration [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,5};
UINT4 FsRogueApRuleProtectionType [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,6};
UINT4 FsRogueApRuleRSSI [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,7};
UINT4 FsRogueApRuleTpc [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,8};
UINT4 FsRogueApRuleSSID [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,9};
UINT4 FsRogueApRuleClientCount [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,10};
UINT4 FsRogueApRuleStatus [ ] ={1,3,6,1,4,1,29601,2,84,10,4,1,11};
UINT4 FsRogueApManualBSSID [ ] ={1,3,6,1,4,1,29601,2,84,10,5,1,1};
UINT4 FsRogueApManualClass [ ] ={1,3,6,1,4,1,29601,2,84,10,5,1,2};
UINT4 FsRogueApManualState [ ] ={1,3,6,1,4,1,29601,2,84,10,5,1,3};
UINT4 FsRogueApManualStatus [ ] ={1,3,6,1,4,1,29601,2,84,10,5,1,4};
UINT4 FsRogueApDetecttion [ ] ={1,3,6,1,4,1,29601,2,84,10,6};
#endif
UINT4 FsRrm11hDfsStationMacAddress [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,1};
UINT4 FsRrm11hChannelNumber [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,2};
UINT4 FsRrm11hMeasureMapBSS [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,3};
UINT4 FsRrm11hMeasureMapOFDM [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,4};
UINT4 FsRrm11hMeasureMapUnidentified [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,5};
UINT4 FsRrm11hMeasureMapRadar [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,6};
UINT4 FsRrm11hMeasureMapUnmeasured [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,7};
UINT4 FsRrm11hDfsReportLastReceived [ ] ={1,3,6,1,4,1,29601,2,84,9,1,1,8};
#endif



tMbDbEntry rfmgmtMibEntry[]= {

{{11,FsRrmDebugOption}, NULL, FsRrmDebugOptionGet, FsRrmDebugOptionSet, FsRrmDebugOptionTest, FsRrmDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
#ifdef WLC_WANTED
{{13,FsRrmRadioType}, GetNextIndexFsRrmConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRrmConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmDcaMode}, GetNextIndexFsRrmConfigTable, FsRrmDcaModeGet, FsRrmDcaModeSet, FsRrmDcaModeTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmDcaSelectionMode}, GetNextIndexFsRrmConfigTable, FsRrmDcaSelectionModeGet, FsRrmDcaSelectionModeSet, FsRrmDcaSelectionModeTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmTpcMode}, GetNextIndexFsRrmConfigTable, FsRrmTpcModeGet, FsRrmTpcModeSet, FsRrmTpcModeTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmTpcSelectionMode}, GetNextIndexFsRrmConfigTable, FsRrmTpcSelectionModeGet, FsRrmTpcSelectionModeSet, FsRrmTpcSelectionModeTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmTpcUpdate}, GetNextIndexFsRrmConfigTable, FsRrmTpcUpdateGet, FsRrmTpcUpdateSet, FsRrmTpcUpdateTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRrmDcaInterval}, GetNextIndexFsRrmConfigTable, FsRrmDcaIntervalGet, FsRrmDcaIntervalSet, FsRrmDcaIntervalTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "600"},

{{13,FsRrmDcaSensitivity}, GetNextIndexFsRrmConfigTable, FsRrmDcaSensitivityGet, FsRrmDcaSensitivitySet, FsRrmDcaSensitivityTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmAllowedChannels}, GetNextIndexFsRrmConfigTable, FsRrmAllowedChannelsGet, FsRrmAllowedChannelsSet, FsRrmAllowedChannelsTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmUnusedChannels}, GetNextIndexFsRrmConfigTable, FsRrmUnusedChannelsGet, FsRrmUnusedChannelsSet, FsRrmUnusedChannelsTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmUpdateChannel}, GetNextIndexFsRrmConfigTable, FsRrmUpdateChannelGet, FsRrmUpdateChannelSet, FsRrmUpdateChannelTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRrmLastUpdatedTime}, GetNextIndexFsRrmConfigTable, FsRrmLastUpdatedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmRSSIThreshold}, GetNextIndexFsRrmConfigTable, FsRrmRSSIThresholdGet, FsRrmRSSIThresholdSet, FsRrmRSSIThresholdTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "-70"},

{{13,FsRrmClientThreshold}, GetNextIndexFsRrmConfigTable, FsRrmClientThresholdGet, FsRrmClientThresholdSet, FsRrmClientThresholdTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "12"},

{{13,FsRrmTpcInterval}, GetNextIndexFsRrmConfigTable, FsRrmTpcIntervalGet, FsRrmTpcIntervalSet, FsRrmTpcIntervalTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "600"},

{{13,FsRrmSNRThreshold}, GetNextIndexFsRrmConfigTable, FsRrmSNRThresholdGet, FsRrmSNRThresholdSet, FsRrmSNRThresholdTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 0, "20"},

{{13,FsRrmTpcLastUpdatedTime}, GetNextIndexFsRrmConfigTable, FsRrmTpcLastUpdatedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmRowStatus}, GetNextIndexFsRrmConfigTable, FsRrmRowStatusGet, FsRrmRowStatusSet, FsRrmRowStatusTest, FsRrmConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmConfigTableINDEX, 1, 0, 1, NULL},

{{13,FsRrmSHAStatus}, GetNextIndexFsRrmExtConfigTable, FsRrmSHAStatusGet, FsRrmSHAStatusSet, FsRrmSHAStatusTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRrmSHAInterval}, GetNextIndexFsRrmExtConfigTable, FsRrmSHAIntervalGet, FsRrmSHAIntervalSet, FsRrmSHAIntervalTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "600"},

{{13,FsRrmSHAExecutionCount}, GetNextIndexFsRrmExtConfigTable, FsRrmSHAExecutionCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRrmExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmDpaExecutionCount}, GetNextIndexFsRrmExtConfigTable, FsRrmDpaExecutionCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRrmExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmPowerThreshold}, GetNextIndexFsRrmExtConfigTable, FsRrmPowerThresholdGet, FsRrmPowerThresholdSet, FsRrmPowerThresholdTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "-70"},

{{13,FsRrm11hTpcStatus}, GetNextIndexFsRrmExtConfigTable, FsRrm11hTpcStatusGet, FsRrm11hTpcStatusSet, FsRrm11hTpcStatusTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrm11hTpcLastRun}, GetNextIndexFsRrmExtConfigTable, FsRrm11hTpcLastRunGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrm11hTpcInterval}, GetNextIndexFsRrmExtConfigTable, FsRrm11hTpcIntervalGet, FsRrm11hTpcIntervalSet, FsRrm11hTpcIntervalTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "600"},

{{13,FsRrm11hMinLinkThreshold}, GetNextIndexFsRrmExtConfigTable, FsRrm11hMinLinkThresholdGet, FsRrm11hMinLinkThresholdSet, FsRrm11hMinLinkThresholdTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "10"},

{{13,FsRrm11hMaxLinkThreshold}, GetNextIndexFsRrmExtConfigTable, FsRrm11hMaxLinkThresholdGet, FsRrm11hMaxLinkThresholdSet, FsRrm11hMaxLinkThresholdTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "15"},

{{13,FsRrm11hStaCountThreshold}, GetNextIndexFsRrmExtConfigTable, FsRrm11hStaCountThresholdGet, FsRrm11hStaCountThresholdSet, FsRrm11hStaCountThresholdTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "3"},

{{13,FsRrm11hDfsInterval}, GetNextIndexFsRrmExtConfigTable, FsRrm11hDfsIntervalGet, FsRrm11hDfsIntervalSet, FsRrm11hDfsIntervalTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "600"},

{{13,FsRrm11hDfsStatus}, GetNextIndexFsRrmExtConfigTable, FsRrm11hDfsStatusGet, FsRrm11hDfsStatusSet, FsRrm11hDfsStatusTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRrmConsiderExternalAPs}, GetNextIndexFsRrmExtConfigTable, FsRrmConsiderExternalAPsGet, FsRrmConsiderExternalAPsSet, FsRrmConsiderExternalAPsTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmClearNeighborInfo}, GetNextIndexFsRrmExtConfigTable, FsRrmClearNeighborInfoGet, FsRrmClearNeighborInfoSet, FsRrmClearNeighborInfoTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRrmNeighborCountThreshold}, GetNextIndexFsRrmExtConfigTable, FsRrmNeighborCountThresholdGet, FsRrmNeighborCountThresholdSet, FsRrmNeighborCountThresholdTest, FsRrmExtConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmExtConfigTableINDEX, 1, 0, 0, "1"},



#ifdef ROGUEAP_WANTED
{{11,FsRfGroupName}, NULL, FsRfGroupNameGet, FsRfGroupNameSet, FsRfGroupNameTest, FsRfGroupNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "Aricent123"},
#endif

#endif
{{11,FsRrmChannelSwitchMsgStatus}, NULL, FsRrmChannelSwitchMsgStatusGet, FsRrmChannelSwitchMsgStatusSet, FsRrmChannelSwitchMsgStatusTest, FsRrmChannelSwitchMsgStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsRrmAPAutoScanStatus}, GetNextIndexFsRrmAPConfigTable, FsRrmAPAutoScanStatusGet, FsRrmAPAutoScanStatusSet, FsRrmAPAutoScanStatusTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmAPNeighborScanFreq}, GetNextIndexFsRrmAPConfigTable, FsRrmAPNeighborScanFreqGet, FsRrmAPNeighborScanFreqSet, FsRrmAPNeighborScanFreqTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "60"},

{{13,FsRrmAPChannelScanDuration}, GetNextIndexFsRrmAPConfigTable, FsRrmAPChannelScanDurationGet, FsRrmAPChannelScanDurationSet, FsRrmAPChannelScanDurationTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "180"},

{{13,FsRrmAPNeighborAgingPeriod}, GetNextIndexFsRrmAPConfigTable, FsRrmAPNeighborAgingPeriodGet, FsRrmAPNeighborAgingPeriodSet, FsRrmAPNeighborAgingPeriodTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "60"},

{{13,FsRrmAPMacAddress}, GetNextIndexFsRrmAPConfigTable, FsRrmAPMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsRrmAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmAPChannelMode}, GetNextIndexFsRrmAPConfigTable, FsRrmAPChannelModeGet, FsRrmAPChannelModeSet, FsRrmAPChannelModeTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmAPChannelChangeCount}, GetNextIndexFsRrmAPConfigTable, FsRrmAPChannelChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRrmAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmAPChannelChangeTime}, GetNextIndexFsRrmAPConfigTable, FsRrmAPChannelChangeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmAPAssignedChannel}, GetNextIndexFsRrmAPConfigTable, FsRrmAPAssignedChannelGet, FsRrmAPAssignedChannelSet, FsRrmAPAssignedChannelTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrm11hTPCRequestInterval}, GetNextIndexFsRrmAPConfigTable, FsRrm11hTPCRequestIntervalGet, FsRrm11hTPCRequestIntervalSet, FsRrm11hTPCRequestIntervalTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "600"},

{{13,FsRrmAPChannelAllowedList}, GetNextIndexFsRrmAPConfigTable, FsRrmAPChannelAllowedListGet, FsRrmAPChannelAllowedListSet, FsRrmAPChannelAllowedListTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, NULL},
{{13,FsRrm11hDFSQuietInterval}, GetNextIndexFsRrmAPConfigTable, FsRrm11hDFSQuietIntervalGet, FsRrm11hDFSQuietIntervalSet, FsRrm11hDFSQuietIntervalTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "25"},

{{13,FsRrm11hDFSMeasurementInterval}, GetNextIndexFsRrmAPConfigTable, FsRrm11hDFSMeasurementIntervalGet, FsRrm11hDFSMeasurementIntervalSet, FsRrm11hDFSMeasurementIntervalTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "60"},

{{13,FsRrm11hDFSQuietPeriod}, GetNextIndexFsRrmAPConfigTable, FsRrm11hDFSQuietPeriodGet, FsRrm11hDFSQuietPeriodSet, FsRrm11hDFSQuietPeriodTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "10"},

{{13,FsRrm11hDFSChannelSwitchStatus}, GetNextIndexFsRrmAPConfigTable, FsRrm11hDFSChannelSwitchStatusGet, FsRrm11hDFSChannelSwitchStatusSet, FsRrm11hDFSChannelSwitchStatusTest, FsRrmAPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmAPConfigTableINDEX, 1, 0, 0, "2"},


{{13,FsRrmScannedChannel}, GetNextIndexFsRrmAPStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRrmAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsRrmNeighborMacAddress}, GetNextIndexFsRrmAPStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRrmAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsRrmRSSIValue}, GetNextIndexFsRrmAPStatsTable, FsRrmRSSIValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRrmAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsRrmSNR}, GetNextIndexFsRrmAPStatsTable, FsRrmSNRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRrmAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsRrmLastUpdatedInterval}, GetNextIndexFsRrmAPStatsTable, FsRrmLastUpdatedIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsRrmNeighborMsgRcvdCount}, GetNextIndexFsRrmAPStatsTable, FsRrmNeighborMsgRcvdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsRrmAPSNRScanStatus}, GetNextIndexFsRrmTpcConfigTable, FsRrmAPSNRScanStatusGet, FsRrmAPSNRScanStatusSet, FsRrmAPSNRScanStatusTest, FsRrmTpcConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmTpcConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmSNRScanFreq}, GetNextIndexFsRrmTpcConfigTable, FsRrmSNRScanFreqGet, FsRrmSNRScanFreqSet, FsRrmSNRScanFreqTest, FsRrmTpcConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmTpcConfigTableINDEX, 1, 0, 0, "60"},

{{13,FsRrmSNRScanCount}, GetNextIndexFsRrmTpcConfigTable, FsRrmSNRScanCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmBelowSNRThresholdCount}, GetNextIndexFsRrmTpcConfigTable, FsRrmBelowSNRThresholdCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmTxPowerChangeCount}, GetNextIndexFsRrmTpcConfigTable, FsRrmTxPowerChangeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmClientsConnected}, GetNextIndexFsRrmTpcConfigTable, FsRrmClientsConnectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmClientsAccepted}, GetNextIndexFsRrmTpcConfigTable, FsRrmClientsAcceptedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmClientsDiscarded}, GetNextIndexFsRrmTpcConfigTable, FsRrmClientsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmAPTpcMode}, GetNextIndexFsRrmTpcConfigTable, FsRrmAPTpcModeGet, FsRrmAPTpcModeSet, FsRrmAPTpcModeTest, FsRrmTpcConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmTpcConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRrmAPTxPowerLevel}, GetNextIndexFsRrmTpcConfigTable, FsRrmAPTxPowerLevelGet, FsRrmAPTxPowerLevelSet, FsRrmAPTxPowerLevelTest, FsRrmTpcConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmTxPowerChangeTime}, GetNextIndexFsRrmTpcConfigTable, FsRrmTxPowerChangeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmTpcConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmClientMacAddress}, GetNextIndexFsRrmTpcClientTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRrmTpcClientTableINDEX, 2, 0, 0, NULL},

{{13,FsRrmClientSNR}, GetNextIndexFsRrmTpcClientTable, FsRrmClientSNRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRrmTpcClientTableINDEX, 2, 0, 0, NULL},

{{13,FsRrmClientLastSNRScan}, GetNextIndexFsRrmTpcClientTable, FsRrmClientLastSNRScanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrmTpcClientTableINDEX, 2, 0, 0, NULL},

{{13,FsRrmWlanId}, GetNextIndexFsRrmTpcScanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRrmTpcScanTableINDEX, 2, 0, 0, NULL},

{{13,FsRrmSSIDScanStatus}, GetNextIndexFsRrmTpcScanTable, FsRrmSSIDScanStatusGet, FsRrmSSIDScanStatusSet, FsRrmSSIDScanStatusTest, FsRrmTpcScanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrmTpcScanTableINDEX, 2, 0, 0, NULL},

#ifdef WLC_WANTED
{{13,FsRrmSHANeighborMacAddress}, GetNextIndexFsRrmFailedAPStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRrmFailedAPStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsRrmSHARSSIValue}, GetNextIndexFsRrmFailedAPStatsTable, FsRrmSHARSSIValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRrmFailedAPStatsTableINDEX, 2, 0, 0, NULL},

#endif
{{11,FsRrmChannelChangeTrapStatus}, NULL, FsRrmChannelChangeTrapStatusGet, FsRrmChannelChangeTrapStatusSet, FsRrmChannelChangeTrapStatusTest, FsRrmChannelChangeTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsRrmTxPowerChangeTrapStatus}, NULL, FsRrmTxPowerChangeTrapStatusGet, FsRrmTxPowerChangeTrapStatusSet, FsRrmTxPowerChangeTrapStatusTest, FsRrmTxPowerChangeTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

#ifdef WLC_WANTED
{{13,FsRrmTxPowerIncreaseCount}, GetNextIndexFsRrmStatsTable, FsRrmTxPowerIncreaseCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRrmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsRrmTxPowerDecreaseCount}, GetNextIndexFsRrmStatsTable, FsRrmTxPowerDecreaseCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRrmStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsRrm11hStationMacAddress}, GetNextIndexFsRrm11hTpcInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRrm11hTpcInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsRrm11hTxPowerLevel}, GetNextIndexFsRrm11hTpcInfoTable, FsRrm11hTxPowerLevelGet, FsRrm11hTxPowerLevelSet, FsRrm11hTxPowerLevelTest, FsRrm11hTpcInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hTpcInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsRrm11hLinkMargin}, GetNextIndexFsRrm11hTpcInfoTable, FsRrm11hLinkMarginGet, FsRrm11hLinkMarginSet, FsRrm11hLinkMarginTest, FsRrm11hTpcInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hTpcInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsRrm11hTpcReportLastReceived}, GetNextIndexFsRrm11hTpcInfoTable, FsRrm11hTpcReportLastReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrm11hTpcInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsRrm11hBaseLinkMargin}, GetNextIndexFsRrm11hTpcInfoTable, FsRrm11hBaseLinkMarginGet, FsRrm11hBaseLinkMarginSet, FsRrm11hBaseLinkMarginTest, FsRrm11hTpcInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hTpcInfoTableINDEX, 2, 0, 0, NULL},
/*
{{13,FsRrm11hDfsStationMacAddress}, GetNextIndexFsRrm11hDfsInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hChannelNumber}, GetNextIndexFsRrm11hDfsInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hMeasureMapBSS}, GetNextIndexFsRrm11hDfsInfoTable, FsRrm11hMeasureMapBSSGet, FsRrm11hMeasureMapBSSSet, FsRrm11hMeasureMapBSSTest, FsRrm11hDfsInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hMeasureMapOFDM}, GetNextIndexFsRrm11hDfsInfoTable, FsRrm11hMeasureMapOFDMGet, FsRrm11hMeasureMapOFDMSet, FsRrm11hMeasureMapOFDMTest, FsRrm11hDfsInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hMeasureMapUnidentified}, GetNextIndexFsRrm11hDfsInfoTable, FsRrm11hMeasureMapUnidentifiedGet, FsRrm11hMeasureMapUnidentifiedSet, FsRrm11hMeasureMapUnidentifiedTest, FsRrm11hDfsInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hMeasureMapRadar}, GetNextIndexFsRrm11hDfsInfoTable, FsRrm11hMeasureMapRadarGet, FsRrm11hMeasureMapRadarSet, FsRrm11hMeasureMapRadarTest, FsRrm11hDfsInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hMeasureMapUnmeasured}, GetNextIndexFsRrm11hDfsInfoTable, FsRrm11hMeasureMapUnmeasuredGet, FsRrm11hMeasureMapUnmeasuredSet, FsRrm11hMeasureMapUnmeasuredTest, FsRrm11hDfsInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},

{{13,FsRrm11hDfsReportLastReceived}, GetNextIndexFsRrm11hDfsInfoTable, FsRrm11hDfsReportLastReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRrm11hDfsInfoTableINDEX, 3, 0, 0, NULL},
*/
#ifdef ROGUEAP_WANTED
{{12,FsRogueApTrapBssid}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsRogueApTimeout}, NULL, FsRogueApTimeoutGet, FsRogueApTimeoutSet, FsRogueApTimeoutTest, FsRogueApTimeoutDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "240"},

{{11,FsRogueApMaliciousTrapStatus}, NULL, FsRogueApMaliciousTrapStatusGet, FsRogueApMaliciousTrapStatusSet, FsRogueApMaliciousTrapStatusTest, FsRogueApMaliciousTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsRogueApBSSID}, GetNextIndexFsRogueApTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApSSID}, GetNextIndexFsRogueApTable, FsRogueApSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApProcetionType}, GetNextIndexFsRogueApTable, FsRogueApProcetionTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApSNR}, GetNextIndexFsRogueApTable, FsRogueApSNRGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApRSSI}, GetNextIndexFsRogueApTable, FsRogueApRSSIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApOperationChannel}, GetNextIndexFsRogueApTable, FsRogueApOperationChannelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApDetectedRadio}, GetNextIndexFsRogueApTable, FsRogueApDetectedRadioGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApDetectedBand}, GetNextIndexFsRogueApTable, FsRogueApDetectedBandGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApLastReportedTime}, GetNextIndexFsRogueApTable, FsRogueApLastReportedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApLastHeardTime}, GetNextIndexFsRogueApTable, FsRogueApLastHeardTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApTxPower}, GetNextIndexFsRogueApTable, FsRogueApTxPowerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApClass}, GetNextIndexFsRogueApTable, FsRogueApClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApState}, GetNextIndexFsRogueApTable, FsRogueApStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApClassifiedBy}, GetNextIndexFsRogueApTable, FsRogueApClassifiedByGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApClientCount}, GetNextIndexFsRogueApTable, FsRogueApClientCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApLastHeardBSSID}, GetNextIndexFsRogueApTable, FsRogueApLastHeardBSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApStatus}, GetNextIndexFsRogueApTable, FsRogueApStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRogueApTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApRuleName}, GetNextIndexFsRogueRuleConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRogueRuleConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApRulePriOrderNum}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRulePriOrderNumGet, FsRogueApRulePriOrderNumSet, FsRogueApRulePriOrderNumTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApRuleClass}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleClassGet, FsRogueApRuleClassSet, FsRogueApRuleClassTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRogueApRuleState}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleStateGet, FsRogueApRuleStateSet, FsRogueApRuleStateTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRogueApRuleDuration}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleDurationGet, FsRogueApRuleDurationSet, FsRogueApRuleDurationTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "3601"},

{{13,FsRogueApRuleProtectionType}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleProtectionTypeGet, FsRogueApRuleProtectionTypeSet, FsRogueApRuleProtectionTypeTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRogueApRuleRSSI}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleRSSIGet, FsRogueApRuleRSSISet, FsRogueApRuleRSSITest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "-129"},

{{13,FsRogueApRuleTpc}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleTpcGet, FsRogueApRuleTpcSet, FsRogueApRuleTpcTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsRogueApRuleSSID}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleSSIDGet, FsRogueApRuleSSIDSet, FsRogueApRuleSSIDTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApRuleClientCount}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleClientCountGet, FsRogueApRuleClientCountSet, FsRogueApRuleClientCountTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "76"},

{{13,FsRogueApRuleStatus}, GetNextIndexFsRogueRuleConfigTable, FsRogueApRuleStatusGet, FsRogueApRuleStatusSet, FsRogueApRuleStatusTest, FsRogueRuleConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueRuleConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsRogueApManualBSSID}, GetNextIndexFsRogueManualConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsRogueManualConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsRogueApManualClass}, GetNextIndexFsRogueManualConfigTable, FsRogueApManualClassGet, FsRogueApManualClassSet, FsRogueApManualClassTest, FsRogueManualConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueManualConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRogueApManualState}, GetNextIndexFsRogueManualConfigTable, FsRogueApManualStateGet, FsRogueApManualStateSet, FsRogueApManualStateTest, FsRogueManualConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueManualConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsRogueApManualStatus}, GetNextIndexFsRogueManualConfigTable, FsRogueApManualStatusGet, FsRogueApManualStatusSet, FsRogueApManualStatusTest, FsRogueManualConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRogueManualConfigTableINDEX, 1, 0, 0, "2"},

{{11,FsRogueApDetecttion}, NULL, FsRogueApDetecttionGet, FsRogueApDetecttionSet, FsRogueApDetecttionTest, FsRogueApDetecttionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
#endif

#endif
};
#ifdef WLC_WANTED
tMibData rfmgmtEntry = { 129, rfmgmtMibEntry }; 
#else
tMibData rfmgmtEntry = { 41, rfmgmtMibEntry };
#endif

#endif /* _RFMGMTDB_H */

