/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: rfmtrc.h,v 1.2 2017/11/24 10:37:04 siva Exp $
 *  
 *  Description: This file contains the trace related info for RFMGMT module.
 **********************************************************************/
#ifndef _RFMGMTTRC_H_
#define _RFMGMTTRC_H_

#define RFMGMT_MGMT_TRC                    0x00000001
#define RFMGMT_INIT_TRC                    0x00000002
#define RFMGMT_ENTRY_TRC                   0x00000004
#define RFMGMT_EXIT_TRC                    0x00000008
#define RFMGMT_FAILURE_TRC                 0x00000010
#define RFMGMT_INFO_TRC                    0x00000020
#define RFMGMT_CRITICAL_TRC                0x00000040
#define RFMGMT_DCA_TRC                     0x00000080
#define RFMGMT_TPC_TRC                     0x00000100
#define RFMGMT_SHA_TRC                     0x00000200
#define RFMGMT_DPA_TRC                     0x00000400
#define RFMGMT_11H_TPC_TRC                 0x00000800
#define RFMGMT_ALL_TRC                     0x00000FFF

#define  RFMGMT_MOD                ((const char *)"RFMGMT")

#define RFMGMT_MASK               gu4RfMgmtTrace 

/* Trace and debug flags */
#define RFMGMT_FN_ENTRY() MOD_FN_ENTRY (RFMGMT_MASK, \
        RFMGMT_ENTRY_TRC,RFMGMT_MOD)

#define RFMGMT_FN_EXIT() MOD_FN_EXIT (RFMGMT_MASK, \
        RFMGMT_EXIT_TRC,RFMGMT_MOD)

#define RFMGMT_TRC(mask,fmt)\
      MOD_TRC(RFMGMT_MASK,mask,RFMGMT_MOD,fmt)
#define RFMGMT_TRC1(mask,fmt,arg1)\
        MOD_TRC_ARG1(RFMGMT_MASK,mask,RFMGMT_MOD,fmt,arg1)
#define RFMGMT_TRC2(mask,fmt,arg1,arg2)\
        MOD_TRC_ARG2(RFMGMT_MASK,mask,RFMGMT_MOD,fmt,arg1,arg2)
#define RFMGMT_TRC3(mask,fmt,arg1,arg2,arg3)\
        MOD_TRC_ARG3(RFMGMT_MASK,mask,RFMGMT_MOD,fmt,arg1,arg2,arg3)
#define RFMGMT_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
        MOD_TRC_ARG4(RFMGMT_MASK,mask,RFMGMT_MOD,fmt,arg1,arg2,arg3,arg4)
#define RFMGMT_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
        MOD_TRC_ARG5(RFMGMT_MASK,mask,RFMGMT_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define RFMGMT_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
        MOD_TRC_ARG6(RFMGMT_MASK,mask,RFMGMT_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#endif

