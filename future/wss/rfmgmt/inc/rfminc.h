/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfminc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains common includes of rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RFMGMT_INC_H__
#define __RFMGMT_INC_H__

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "ip.h"

#include "trace.h"
#include "fssyslog.h"
#include "utilrand.h"

#include "rfmgmt.h"
#include "wssifinc.h"
#include "radioifconst.h"

#include "rfmconst.h"

#ifdef WLC_WANTED
#include "rfactdfs.h"
#include "rfmgmtacsz.h"
#else
#include "rfaptdfs.h"
#endif

#if defined (__RFAPMAIN_C__) || defined (__RFACMAIN_C__)
#include "rfmglob.h"
#else
#include "rfmextn.h"
#endif

#include "rfmgmtsz.h"
#include "rfmtrc.h"
#include "rfmproto.h"
#include "rfmgmtlw.h"
#include "rfmgmtwr.h"
#include "rfmtrc.h"
#endif
