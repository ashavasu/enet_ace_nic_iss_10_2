/* $Id: rfmgmtacsz.h,v 1.2 2017/05/23 14:16:52 siva Exp $ */
enum {
    MAX_RFMGMT_AUTORF_SIZE_SIZING_ID,
    MAX_RFMGMT_BSSIDSCAN_SIZE_SIZING_ID,
    MAX_RFMGMT_FAILED_AP_NEIGH_SIZE_SIZING_ID,
    MAX_RFMGMT_TPCINFO_SIZE_SIZING_ID,
    MAX_RFMGMT_DFSINFO_SIZE_SIZING_ID,
#ifdef ROGUEAP_WANTED
    MAX_RFMGMT_ROGUEAPSCAN_SIZE_SIZING_ID,
    MAX_RFMGMT_ROGUE_MANUAL_SIZE_SIZING_ID,
    MAX_RFMGMT_ROGUE_RULE_SIZE_SIZING_ID,
#endif
    MAX_RFMGMT_EXTERNAL_AP_SIZE_SIZING_ID,
    RFMGMTAC_MAX_SIZING_ID
};


#ifdef  _RFMGMTACSZ_C
tMemPoolId RFMGMTACMemPoolIds[ RFMGMTAC_MAX_SIZING_ID];
INT4  RfmgmtacSizingMemCreateMemPools(VOID);
VOID  RfmgmtacSizingMemDeleteMemPools(VOID);
INT4  RfmgmtacSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RFMGMTACSZ_C  */
extern tMemPoolId RFMGMTACMemPoolIds[ ];
extern INT4  RfmgmtacSizingMemCreateMemPools(VOID);
extern VOID  RfmgmtacSizingMemDeleteMemPools(VOID);
#endif /*  _RFMGMTACSZ_C  */


#ifdef  _RFMGMTACSZ_C
tFsModSizingParams FsRFMGMTACSizingParams [] = {
{ "tRfMgmtAutoRfProfileDB", "MAX_RFMGMT_AUTORF_SIZE", sizeof(tRfMgmtAutoRfProfileDB),MAX_RFMGMT_AUTORF_SIZE, MAX_RFMGMT_AUTORF_SIZE,0 },
{ "tRfMgmtBssidScanDB", "MAX_RFMGMT_BSSIDSCAN_SIZE", sizeof(tRfMgmtBssidScanDB),MAX_RFMGMT_BSSIDSCAN_SIZE, MAX_RFMGMT_BSSIDSCAN_SIZE,0 },
{ "tRfMgmtFailedAPNeighborListDB", "MAX_RFMGMT_FAILED_AP_NEIGH_SIZE", sizeof(tRfMgmtFailedAPNeighborListDB),MAX_RFMGMT_FAILED_AP_NEIGH_SIZE, MAX_RFMGMT_FAILED_AP_NEIGH_SIZE,0 },
{ "tRfMgmtDot11hTpcInfoDB", "MAX_RFMGMT_TPCINFO_SIZE", sizeof(tRfMgmtDot11hTpcInfoDB),MAX_RFMGMT_TPCINFO_SIZE, MAX_RFMGMT_TPCINFO_SIZE,0 },
{ "tRfMgmtDot11hDfsInfoDB", "MAX_RFMGMT_DFSINFO_SIZE", sizeof(tRfMgmtDot11hDfsInfoDB),MAX_RFMGMT_DFSINFO_SIZE, MAX_RFMGMT_DFSINFO_SIZE,0 },
#ifdef ROGUEAP_WANTED
{ "tRfMgmtRogueApDB", "MAX_RFMGMT_ROGUEAPSCAN_SIZE", sizeof(tRfMgmtRogueApDB),MAX_RFMGMT_ROGUEAPSCAN_SIZE, MAX_RFMGMT_ROGUEAPSCAN_SIZE,0 },
{ "tRfMgmtRogueManualDB", "MAX_RFMGMT_ROGUE_MANUAL_SIZE", sizeof(tRfMgmtRogueManualDB),MAX_RFMGMT_ROGUE_MANUAL_SIZE, MAX_RFMGMT_ROGUE_MANUAL_SIZE,0 },
{ "tRfMgmtRogueRuleDB", "MAX_RFMGMT_ROGUE_RULE_SIZE", sizeof(tRfMgmtRogueRuleDB),MAX_RFMGMT_ROGUE_RULE_SIZE, MAX_RFMGMT_ROGUE_RULE_SIZE,0 },
#endif
{ "tRfMgmtExternalAPDB", "MAX_RFMGMT_EXTERNAL_AP_SIZE", sizeof(tRfMgmtExternalAPDB),MAX_RFMGMT_EXTERNAL_AP_SIZE, MAX_RFMGMT_EXTERNAL_AP_SIZE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RFMGMTACSZ_C  */
extern tFsModSizingParams FsRFMGMTACSizingParams [];
#endif /*  _RFMGMTACSZ_C  */


