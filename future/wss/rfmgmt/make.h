# $Id: make.h,v 1.2 2017/11/24 10:37:04 siva Exp $
#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

SYSTEM_COMPILATION_SWITCHES += -DRFMGMT_TEST_WANTED

TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################
RFMGMT_TEST_BASE_DIR = ${BASE_DIR}/wss/test
RFMGMT_TEST_OBJ_DIR  = ${RFMGMT_TEST_BASE_DIR}/obj
RFMGMT_TEST_SRC_DIR  = ${RFMGMT_TEST_BASE_DIR}/src
RFMGMT_TEST_INC_DIR  = ${RFMGMT_TEST_BASE_DIR}/inc
RFMGMT_BASE_DIR = ${BASE_DIR}/wss/rfmgmt
RFMGMT_INC_DIR  = ${RFMGMT_BASE_DIR}/inc
RFMGMT_SRC_DIR  = ${RFMGMT_BASE_DIR}/src
RFMGMT_OBJ_DIR  = ${RFMGMT_BASE_DIR}/obj
WSSWLAN_INC_DIR  = ${BASE_DIR}/wss/wsswlan/inc
RADIOIF_INC_DIR  = ${BASE_DIR}/wss/radioif/inc
WSSIF_INC_DIR  = ${BASE_DIR}/wss/wssif/inc
WSSMAC_INC_DIR  = ${BASE_DIR}/wss/wssmac/inc
BCNMGR_INC_DIR  = ${BASE_DIR}/wss/bcnmgr/inc
APHDLR_INC_DIR = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR = ${BASE_DIR}/wss/wlchdlr/inc
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
WSSSTA_INC_DIR = ${BASE_DIR}/wss/wsssta/inc
WSSCFG_INC_DIR = ${BASE_DIR}/wss/wsscfg/inc
WSSPM_INC_DIR = ${BASE_DIR}/wss/wsspm/inc
ISS_INC_DIR = ${BASE_DIR}/ISS/common/system/inc
CFA_INC_DIR = ${BASE_DIR}/cfa2/inc
WEB_INC_DIR = ${BASE_DIR}/ISS/common/web/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${RFMGMT_INC_DIR}  -I${RADIOIF_INC_DIR} -I${WSSWLAN_INC_DIR} -I${WSSIF_INC_DIR} -I${WSSMAC_INC_DIR} -I${BCNMGR_INC_DIR} -I${APHDLR_INC_DIR} -I${WLCHDLR_INC_DIR} -I${CAPWAP_INC_DIR} -I${WSSSTA_INC_DIR}  -I${WSSCFG_INC_DIR} -I${WSSPM_INC_DIR} -I${ISS_INC_DIR} -I${CFA_INC_DIR} -I${WEB_INC_DIR} -I${RFMGMT_TEST_INC_DIR}

INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################


