/*$Id: rfmgmtwr.c,v 1.2 2017/05/23 14:16:53 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "rfmgmtlw.h"
# include  "rfmgmtwr.h"
# include  "rfmgmtdb.h"

VOID
RegisterRFMGMT ()
{
    SNMPRegisterMib (&rfmgmtOID, &rfmgmtEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&rfmgmtOID, (const UINT1 *) "rfmgmt");
}

VOID
UnRegisterRFMGMT ()
{
    SNMPUnRegisterMib (&rfmgmtOID, &rfmgmtEntry);
    SNMPDelSysorEntry (&rfmgmtOID, (const UINT1 *) "rfmgmt");
}

INT4
FsRrmDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrmDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrmDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrmDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsRrmDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrmDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrmDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmDebugOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifdef WLC_WANTED
INT4
GetNextIndexFsRrmConfigTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmDcaModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDcaMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmDcaSelectionModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDcaSelectionMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmTpcModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmTpcSelectionModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcSelectionMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmTpcUpdateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcUpdate (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmDcaIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDcaInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmDcaSensitivityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDcaSensitivity (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmAllowedChannelsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAllowedChannels (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsRrmUnusedChannelsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmUnusedChannels (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsRrmUpdateChannelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmUpdateChannel (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmLastUpdatedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmLastUpdatedTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmRSSIThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmRSSIThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmClientThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClientThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmTpcIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmSNRThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSNRThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmTpcLastUpdatedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcLastUpdatedTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmDcaModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmDcaMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaSelectionModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmDcaSelectionMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmTpcMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcSelectionModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmTpcSelectionMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcUpdateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmTpcUpdate (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmDcaInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsRrmDcaSensitivitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmDcaSensitivity (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRrmAllowedChannelsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAllowedChannels (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsRrmUnusedChannelsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmUnusedChannels (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsRrmUpdateChannelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmUpdateChannel (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRrmRSSIThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmRSSIThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRrmClientThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmClientThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsRrmTpcIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmTpcInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsRrmSNRThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmSNRThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrmRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmDcaMode (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaSelectionModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmDcaSelectionMode (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmTpcMode (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcSelectionModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmTpcSelectionMode (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcUpdateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmTpcUpdate (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmDcaInterval (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrmDcaSensitivityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmDcaSensitivity (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrmAllowedChannelsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAllowedChannels (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsRrmUnusedChannelsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmUnusedChannels (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsRrmUpdateChannelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmUpdateChannel (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrmRSSIThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmRSSIThreshold (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrmClientThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmClientThreshold (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsRrmTpcIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmTpcInterval (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrmSNRThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmSNRThreshold (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRrmRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrmConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmConfigTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
#endif

INT4
FsRrmChannelSwitchMsgStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrmChannelSwitchMsgStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrmChannelSwitchMsgStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrmChannelSwitchMsgStatus (pMultiData->i4_SLongValue));
}

INT4
FsRrmChannelSwitchMsgStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrmChannelSwitchMsgStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrmChannelSwitchMsgStatusDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmChannelSwitchMsgStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrmAPConfigTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmAPConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmAPConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmAPAutoScanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPAutoScanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmAPNeighborScanFreqGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPNeighborScanFreq (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPChannelScanDurationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPChannelScanDuration
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPNeighborAgingPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPNeighborAgingPeriod
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsRrmAPMacAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                     (tMacAddr *) pMultiData->pOctetStrValue->
                                     pu1_OctetList));

}

INT4
FsRrmAPChannelModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPChannelMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmAPChannelChangeCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPChannelChangeCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPChannelChangeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPChannelChangeTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPAssignedChannelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPAssignedChannel (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPChannelAllowedListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPChannelAllowedList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRrm11hTPCRequestIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hTPCRequestInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDFSQuietIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDFSQuietInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDFSMeasurementIntervalGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDFSMeasurementInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDFSQuietPeriodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDFSQuietPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDFSChannelSwitchStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDFSChannelSwitchStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmAPAutoScanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPAutoScanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrmAPNeighborScanFreqSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPNeighborScanFreq (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsRrmAPChannelScanDurationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPChannelScanDuration
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrmAPNeighborAgingPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPNeighborAgingPeriod
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrmAPChannelModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPChannelMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRrmAPAssignedChannelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPAssignedChannel (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRrmAPChannelAllowedListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPChannelAllowedList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRrm11hTPCRequestIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hTPCRequestInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSQuietIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hDFSQuietInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSMeasurementIntervalSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hDFSMeasurementInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSQuietPeriodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hDFSQuietPeriod (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSChannelSwitchStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hDFSChannelSwitchStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmAPAutoScanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPAutoScanStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRrmAPNeighborScanFreqTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPNeighborScanFreq (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsRrmAPChannelScanDurationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPChannelScanDuration (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsRrmAPNeighborAgingPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPNeighborAgingPeriod (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsRrmAPChannelModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPChannelMode (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrmAPAssignedChannelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPAssignedChannel (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsRrmAPChannelAllowedListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPChannelAllowedList (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsRrm11hTPCRequestIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hTPCRequestInterval (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSQuietIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hDFSQuietInterval (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSMeasurementIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hDFSMeasurementInterval (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsRrm11hDFSQuietPeriodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hDFSQuietPeriod (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDFSChannelSwitchStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hDFSChannelSwitchStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsRrmAPConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmAPConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrmAPStatsTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmAPStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmAPStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRrmRSSIValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmRSSIValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  (*(tMacAddr *) pMultiIndex->pIndex[2].
                                   pOctetStrValue->pu1_OctetList),
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmSNRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSNR (pMultiIndex->pIndex[0].i4_SLongValue,
                            pMultiIndex->pIndex[1].u4_ULongValue,
                            (*(tMacAddr *) pMultiIndex->pIndex[2].
                             pOctetStrValue->pu1_OctetList),
                            &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmLastUpdatedIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmLastUpdatedInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmNeighborMsgRcvdCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmNeighborMsgRcvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsRrmTpcConfigTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmTpcConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmTpcConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmAPSNRScanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPSNRScanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmSNRScanFreqGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSNRScanFreq (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmSNRScanCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSNRScanCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmBelowSNRThresholdCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmBelowSNRThresholdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmTxPowerChangeCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTxPowerChangeCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmClientsConnectedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClientsConnected (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmClientsAcceptedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClientsAccepted (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmClientsDiscardedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClientsDiscarded (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPTpcModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPTpcMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmAPTxPowerLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmAPTxPowerLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmTxPowerChangeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTxPowerChangeTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmAPSNRScanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPSNRScanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRrmSNRScanFreqSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmSNRScanFreq (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsRrmAPTpcModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPTpcMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRrmAPTxPowerLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmAPTxPowerLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrmAPSNRScanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPSNRScanStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRrmSNRScanFreqTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmSNRScanFreq (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrmAPTpcModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPTpcMode (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrmAPTxPowerLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmAPTxPowerLevel (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRrmTpcConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmTpcConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrmTpcClientTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmTpcClientTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmTpcClientTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRrmClientSNRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcClientTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClientSNR (pMultiIndex->pIndex[0].i4_SLongValue,
                                  (*(tMacAddr *) pMultiIndex->pIndex[1].
                                   pOctetStrValue->pu1_OctetList),
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmClientLastSNRScanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcClientTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClientLastSNRScan (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsRrmTpcScanTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmTpcScanTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmTpcScanTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmSSIDScanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmTpcScanTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSSIDScanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmSSIDScanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmSSIDScanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRrmSSIDScanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmSSIDScanStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcScanTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmTpcScanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrmChannelChangeTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrmChannelChangeTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrmTxPowerChangeTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrmTxPowerChangeTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrmChannelChangeTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrmChannelChangeTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsRrmTxPowerChangeTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrmTxPowerChangeTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsRrmChannelChangeTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrmChannelChangeTrapStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrmTxPowerChangeTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrmTxPowerChangeTrapStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrmChannelChangeTrapStatusDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmChannelChangeTrapStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrmTxPowerChangeTrapStatusDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmTxPowerChangeTrapStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifdef WLC_WANTED
INT4
GetNextIndexFsRrmExtConfigTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmExtConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmExtConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmSHAStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSHAStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmSHAIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSHAInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmSHAExecutionCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSHAExecutionCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmDpaExecutionCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDpaExecutionCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmPowerThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmPowerThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRrm11hTpcStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hTpcStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsRrm11hTpcLastRunGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hTpcLastRun (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hTpcIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hTpcInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hMinLinkThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hMinLinkThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hMaxLinkThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hMaxLinkThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hStaCountThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hStaCountThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDfsIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDfsInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDfsStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDfsStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmConsiderExternalAPsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmConsiderExternalAPs
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmClearNeighborInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmClearNeighborInfo (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmNeighborCountThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmNeighborCountThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmSHAStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmSHAStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRrmSHAIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmSHAInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsRrmPowerThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmPowerThreshold (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRrm11hTpcStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hTpcStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrm11hTpcIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hTpcInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrm11hMinLinkThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hMinLinkThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrm11hMaxLinkThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hMaxLinkThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrm11hStaCountThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hStaCountThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDfsIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hDfsInterval (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDfsStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hDfsStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrmConsiderExternalAPsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmConsiderExternalAPs
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmClearNeighborInfoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmClearNeighborInfo (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrmNeighborCountThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmNeighborCountThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRrmSHAStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmSHAStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRrmSHAIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmSHAInterval (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsRrmPowerThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmPowerThreshold (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrm11hTpcStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hTpcStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRrm11hTpcIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hTpcInterval (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRrm11hMinLinkThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hMinLinkThreshold (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsRrm11hMaxLinkThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hMaxLinkThreshold (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsRrm11hStaCountThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hStaCountThreshold (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDfsIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hDfsInterval (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsRrm11hDfsStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hDfsStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRrmConsiderExternalAPsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmConsiderExternalAPs (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRrmClearNeighborInfoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmClearNeighborInfo (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRrmNeighborCountThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmNeighborCountThreshold (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsRrmExtConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmExtConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrmFailedAPStatsTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmFailedAPStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmFailedAPStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRrmSHARSSIValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmFailedAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmSHARSSIValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                     (*(tMacAddr *) pMultiIndex->pIndex[1].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsRrmStatsTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmTxPowerIncreaseCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTxPowerIncreaseCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRrmTxPowerDecreaseCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTxPowerDecreaseCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsRrm11hTpcInfoTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrm11hTpcInfoTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrm11hTpcInfoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRrm11hTxPowerLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrm11hTpcInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hTxPowerLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                        (*(tMacAddr *) pMultiIndex->pIndex[1].
                                         pOctetStrValue->pu1_OctetList),
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRrm11hLinkMarginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrm11hTpcInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hLinkMargin (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRrm11hTpcReportLastReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrm11hTpcInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hTpcReportLastReceived
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hBaseLinkMarginGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrm11hTpcInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hBaseLinkMargin (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRrm11hTxPowerLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hTxPowerLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                        (*(tMacAddr *) pMultiIndex->pIndex[1].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiData->i4_SLongValue));

}

INT4
FsRrm11hLinkMarginSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hLinkMargin (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiData->i4_SLongValue));

}

INT4
FsRrm11hBaseLinkMarginSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrm11hBaseLinkMargin (pMultiIndex->pIndex[0].i4_SLongValue,
                                          (*(tMacAddr *) pMultiIndex->pIndex[1].
                                           pOctetStrValue->pu1_OctetList),
                                          pMultiData->i4_SLongValue));

}

INT4
FsRrm11hTxPowerLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hTxPowerLevel (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           (*(tMacAddr *) pMultiIndex->
                                            pIndex[1].pOctetStrValue->
                                            pu1_OctetList),
                                           pMultiData->i4_SLongValue));

}

INT4
FsRrm11hLinkMarginTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hLinkMargin (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrm11hBaseLinkMarginTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRrm11hBaseLinkMargin (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             (*(tMacAddr *) pMultiIndex->
                                              pIndex[1].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiData->i4_SLongValue));

}

INT4
FsRrm11hTpcInfoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrm11hTpcInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrm11hDfsInfoTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrm11hDfsInfoTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrm11hDfsInfoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRrm11hMeasureMapRadarGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrm11hDfsInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hMeasureMapRadar (pMultiIndex->pIndex[0].i4_SLongValue,
                                           (*(tMacAddr *) pMultiIndex->
                                            pIndex[1].pOctetStrValue->
                                            pu1_OctetList),
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRrm11hDfsReportLastReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrm11hDfsInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrm11hDfsReportLastReceived
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
FsRrm11hDfsInfoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrm11hDfsInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifdef ROGUEAP_WANTED

INT4
FsRfGroupNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRfGroupName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsRfGroupNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRfGroupName (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRogueApDetecttionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRogueApDetecttion (&(pMultiData->i4_SLongValue)));
}

INT4
FsRogueApTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRogueApTimeout (&(pMultiData->u4_ULongValue)));
}

INT4
FsRogueApMaliciousTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRogueApMaliciousTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRogueApDetecttionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRogueApDetecttion (pMultiData->i4_SLongValue));
}

INT4
FsRogueApTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRogueApTimeout (pMultiData->u4_ULongValue));
}

INT4
FsRogueApMaliciousTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRogueApMaliciousTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsRogueApDetecttionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRogueApDetecttion (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRogueApTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRogueApTimeout (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRogueApMaliciousTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRogueApMaliciousTrapStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRogueApDetecttionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRogueApDetecttion
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRogueApTimeoutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRogueApTimeout (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRogueApMaliciousTrapStatusDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRogueApMaliciousTrapStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRogueApTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRogueApTable ((tMacAddr *) pNextMultiIndex->
                                            pIndex[0].pOctetStrValue->
                                            pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRogueApTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRogueApSSIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApSSID ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                  pOctetStrValue->pu1_OctetList),
                                 pMultiData->pOctetStrValue));

}

INT4
FsRogueApProcetionTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApProcetionType ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApSNRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApSNR ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                 pOctetStrValue->pu1_OctetList),
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRSSIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRSSI ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                  pOctetStrValue->pu1_OctetList),
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApOperationChannelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApOperationChannel ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApDetectedRadioGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApDetectedRadio ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApDetectedBandGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApDetectedBand ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApLastReportedTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApLastReportedTime ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             &(pMultiData->u4_ULongValue)));

}

INT4
FsRogueApLastHeardTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApLastHeardTime ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                           pOctetStrValue->pu1_OctetList),
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsRogueApTxPowerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApTxPower ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                     pOctetStrValue->pu1_OctetList),
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApClass ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                   pOctetStrValue->pu1_OctetList),
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApState ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                   pOctetStrValue->pu1_OctetList),
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApClassifiedByGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApClassifiedBy ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApClientCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApClientCount ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsRogueApLastHeardBSSIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsRogueApLastHeardBSSID ((*(tMacAddr *) pMultiIndex->
                                            pIndex[0].pOctetStrValue->
                                            pu1_OctetList),
                                           (tMacAddr *) pMultiData->
                                           pOctetStrValue->pu1_OctetList));

}

INT4
FsRogueApStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueApTable ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                    pOctetStrValue->pu1_OctetList),
                                   &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsRogueRuleConfigTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRogueRuleConfigTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRogueRuleConfigTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRogueApRulePriOrderNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRulePriOrderNum
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRuleClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleClass (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRuleStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleState (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRuleDurationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleDuration (pMultiIndex->pIndex[0].pOctetStrValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsRogueApRuleProtectionTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleProtectionType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRuleRSSIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleRSSI (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRuleTpcGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleTpc (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRuleSSIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleSSID (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsRogueApRuleClientCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleClientCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRogueApRuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueRuleConfigTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApRuleStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApRulePriOrderNumSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRulePriOrderNum
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleClassSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleClass (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleState (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleDurationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleDuration (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsRogueApRuleProtectionTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleProtectionType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleRSSISet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleRSSI (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleTpcSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleTpc (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleSSIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleSSID (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsRogueApRuleClientCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleClientCount
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsRogueApRuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApRuleStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRogueApRulePriOrderNumTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRulePriOrderNum (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleClassTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleClass (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleState (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleDurationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleDuration (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsRogueApRuleProtectionTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleProtectionType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleRSSITest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleRSSI (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleTpcTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleTpc (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRogueApRuleSSIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleSSID (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsRogueApRuleClientCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleClientCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsRogueApRuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApRuleStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRogueRuleConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRogueRuleConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRogueManualConfigTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRogueManualConfigTable ((tMacAddr *)
                                                      pNextMultiIndex->
                                                      pIndex[0].pOctetStrValue->
                                                      pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRogueManualConfigTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsRogueApManualClassGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueManualConfigTable ((*(tMacAddr *)
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           pOctetStrValue->
                                                           pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApManualClass ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApManualStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueManualConfigTable ((*(tMacAddr *)
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           pOctetStrValue->
                                                           pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApManualState ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApManualStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRogueManualConfigTable ((*(tMacAddr *)
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           pOctetStrValue->
                                                           pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRogueApManualStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRogueApManualClassSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApManualClass ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiData->i4_SLongValue));

}

INT4
FsRogueApManualStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApManualState ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiData->i4_SLongValue));

}

INT4
FsRogueApManualStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRogueApManualStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->i4_SLongValue));

}

INT4
FsRogueApManualClassTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApManualClass (pu4Error,
                                           (*(tMacAddr *) pMultiIndex->
                                            pIndex[0].pOctetStrValue->
                                            pu1_OctetList),
                                           pMultiData->i4_SLongValue));

}

INT4
FsRogueApManualStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApManualState (pu4Error,
                                           (*(tMacAddr *) pMultiIndex->
                                            pIndex[0].pOctetStrValue->
                                            pu1_OctetList),
                                           pMultiData->i4_SLongValue));

}

INT4
FsRogueApManualStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRogueApManualStatus (pu4Error,
                                            (*(tMacAddr *) pMultiIndex->
                                             pIndex[0].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiData->i4_SLongValue));

}

INT4
FsRogueManualConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRogueManualConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRfGroupNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRfGroupName (pMultiData->pOctetStrValue));
}

INT4
FsRfGroupNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRfGroupName (pMultiData->pOctetStrValue));
}
#endif
#endif
