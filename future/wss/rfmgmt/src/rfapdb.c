/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfapdb.c,v 1.2 2017/11/24 10:37:05 siva Exp $
 *
 * Description: This file contains the DB realated information of the
 *              rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RFAPDB_C__
#define __RFAPDB_C__

#include "rfminc.h"

/*****************************************************************************/
/* Function     : RfMgmtAPConfigDBCreate                                     */
/*                                                                           */
/* Description  : This function creates the RfMgmtAPConfigDB                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtAPConfigDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtAPConfigDB, ApConfigDBNode);

    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RfMgmtAPConfigDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtAPConfigDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtAPConfigDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtNeighborScanDBCreate                                 */
/*                                                                           */
/* Description  : This function creates the RfMgmtNeighborScanDB             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtNeighborScanDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtNeighborScanDB, NeighborScanDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtNeighborScanDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtNeighborScanDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtNeighborScanDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtClientConfigDBCreate                                 */
/*                                                                           */
/* Description  : This function creates the RfMgmtClientConfigDB             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtClientConfigDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtClientConfigDB, ClientConfigDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtClientConfigDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtClientConfigDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtClientConfigDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtClientScanDBCreate                                   */
/*                                                                           */
/* Description  : This function creates the RfMgmtClientScanDB               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtClientScanDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    RFMGMT_FN_ENTRY ();

    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtClientScanDB, ClientScanDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtClientScanDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtClientScanDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtClientScanDBCreate failed "));
        return RFMGMT_FAILURE;
    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtScanAllowedDBCreate                                  */
/*                                                                           */
/* Description  : This function creates the RfMgmtAPConfigDB                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtScanAllowedDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset =
        FSAP_OFFSETOF (tRfMgmtChannelAllowedScanDB, ChannelAllowedDBNode);

    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtChannelAllowedScanDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtChannelAllowedScanDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtScanAllowedDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtScanAllowedDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RfMgmtChannelAllowedScanDBRBCmp                      */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtChannelAllowedScanDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtChannelAllowedScanDB *pNode1 = e1;
    tRfMgmtChannelAllowedScanDB *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }

    if (pNode1->u4RadioType > pNode2->u4RadioType)
    {
        return 1;
    }
    else if (pNode1->u4RadioType < pNode2->u4RadioType)
    {
        return -1;
    }

    if (pNode1->u1Channel > pNode2->u1Channel)
    {
        return 1;
    }
    else if (pNode1->u1Channel < pNode2->u1Channel)
    {
        return -1;
    }
    if (pNode1->u1SecChannelOffset > pNode2->u1SecChannelOffset)
    {
        return 1;
    }
    else if (pNode1->u1SecChannelOffset < pNode2->u1SecChannelOffset)
    {
        return -1;
    }

    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtAPConfigDBRBCmp                                */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtAPConfigDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtAPConfigDB  *pNode1 = e1;
    tRfMgmtAPConfigDB  *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtNeighborScanDBRBCmp                            */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtNeighborScanDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtNeighborScanDB *pNode1 = e1;
    tRfMgmtNeighborScanDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->NeighborAPMac, pNode2->NeighborAPMac,
                       RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtClientConfigDBRBCmp                            */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtClientConfigDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtClientConfigDB *pNode1 = e1;
    tRfMgmtClientConfigDB *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtClientScanDBRBCmp                              */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtClientScanDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtClientScanDB *pNode1 = e1;
    tRfMgmtClientScanDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->ClientMacAddress, pNode2->ClientMacAddress,
                       RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/****************************************************************************
 * *                                                                        *
 * * Function     : WssIfProcessRfMgmtDBMsg                                 *
 * *                                                                        *
 * * Description  : Calls RF MGMT module DB to process fn calls from other  *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : eMsgType - Msg opcode                                   *
 * *                pRfMgmtDB - Pointer to RF MGMT DB struct                * 
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : OSIX_SUCCESS on success                                 *
 * *                OSIX_FAILURE otherwise                                  *
 * *                                                                        *
 * **************************************************************************/

UINT1
WssIfProcessRfMgmtDBMsg (UINT1 u1OpCode, tRfMgmtDB * pRfMgmtDB)
{
    RFMGMT_LOCK;
    if (RfMgmtProcessDBMsg (u1OpCode, pRfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_UNLOCK;
        return OSIX_FAILURE;
    }

    RFMGMT_UNLOCK;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessDBMsg                                         */
/*                                                                           */
/* Description  : Used to invoke the coreesponding DB to perform set or get  */
/*                operation.                                                 */
/* Input        : Opcode and Pointer to tRfMgmtDB                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtProcessDBMsg (UINT1 u1Opcode, tRfMgmtDB * pRfMgmtDB)
{
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB   RfMgmtAPConfigDB;
    tRfMgmtNeighborScanDB RfMgmtNeighborScanDB;
    tRfMgmtNeighborScanDB *pRfMgmtNeighborScanDB = NULL;
    tRfMgmtClientConfigDB RfMgmtClientConfigDB;
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;
    tRfMgmtClientScanDB RfMgmtClientScanDB;
    tRfMgmtClientScanDB *pRfMgmtClientScanDB = NULL;
    tRfMgmtChannelAllowedScanDB *pRfMgmtChannelAllowedScanDB = NULL;
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1WlanId = 0;

    MEMSET (&RfMgmtAPConfigDB, 0, sizeof (tRfMgmtAPConfigDB));
    MEMSET (&RfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RfMgmtClientConfigDB, 0, sizeof (tRfMgmtClientConfigDB));
    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RFMGMT_FN_ENTRY ();

    switch (u1Opcode)
    {
        case RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY:
            /* Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtNeighborScanDB =
                 (tRfMgmtNeighborScanDB *) MemAllocMemBlk
                 (RFMGMT_NEIGH_SCAN_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtNeighborScanDB: "
                            "Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtNeighborScanDB: "
                              "Memory alloc failed for radio index"));
                return RFMGMT_FAILURE;
            }

            MEMSET (pRfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));

            /* Copy the received values to the DB */
            pRfMgmtNeighborScanDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex;

            pRfMgmtNeighborScanDB->i2Rssi =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.i2Rssi;

            pRfMgmtNeighborScanDB->u2ScannedChannel =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ScannedChannel;
#ifdef ROGUEAP_WANTED
            MEMSET (pRfMgmtNeighborScanDB->au1ssid, 0, WSSMAC_MAX_SSID_LEN);
            MEMCPY (pRfMgmtNeighborScanDB->au1ssid,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1ssid, WSSMAC_MAX_SSID_LEN);

            MEMCPY (pRfMgmtNeighborScanDB->au1RfGroupID,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1RfGroupID, WSS_RF_GROUP_ID_MAX_LEN);

            pRfMgmtNeighborScanDB->u1StationCount =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1StationCount;
            pRfMgmtNeighborScanDB->isRogueApStatus =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.isRogueApStatus;
            pRfMgmtNeighborScanDB->u4RogueApLastReportedTime =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RogueApLastReportedTime;
            pRfMgmtNeighborScanDB->isRogueApProcetionType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.isRogueApProcetionType;

            MEMCPY (pRfMgmtNeighborScanDB->BSSIDRogueApLearntFrom,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.BSSIDRogueApLearntFrom, MAC_ADDR_LEN);

#endif
            MEMCPY (pRfMgmtNeighborScanDB->NeighborAPMac,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.NeighborAPMac, MAC_ADDR_LEN);

            /* The following info will be required to know the status of the
             * entry. When set to add, neighbor AP info is not in sync between
             * AP and AC */
            pRfMgmtNeighborScanDB->u1EntryStatus = RFMGMT_NEIGH_ENTRY_ADD;

            /* The below time will be used to delete the inactive entries,
             * incase of exceeding the threshold */
            pRfMgmtNeighborScanDB->u4LastUpdatedTime = OsixGetSysUpTime ();

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                 (tRBElem *) pRfMgmtNeighborScanDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                    (UINT1 *) pRfMgmtNeighborScanDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Neighbor "
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Neighbor "
                              "Scan DB"));
                return RFMGMT_FAILURE;
            }

            break;

        case RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtNeighborScanTable.RfMgmtNeighborScanDB);
            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "to delete the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "to delete the neighbor AP"));
                return RFMGMT_FAILURE;
            }
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       pRfMgmtNeighborScanDB);
            MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNeighborScanDB);
            pRfMgmtNeighborScanDB = NULL;
            break;

        case RFMGMT_SET_NEIGHBOR_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtNeighborScanTable.RfMgmtNeighborScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "to set the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "to set the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            /* Compare the RSSI Value exist in DB and the received value. If the
             * value changes then set the value along with entry status as add
             * and update the last updated time */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bRssi != OSIX_FALSE)
            {
                if (pRfMgmtNeighborScanDB->i2Rssi !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.i2Rssi)
                {
                    pRfMgmtNeighborScanDB->i2Rssi =
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.i2Rssi;

                    pRfMgmtNeighborScanDB->u1EntryStatus =
                        RFMGMT_NEIGH_ENTRY_ADD;

                    pRfMgmtNeighborScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
                else
                {
                    /* Already reported the timer value to AC. Update the time
                     * alone */
                    pRfMgmtNeighborScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();

                }
                if (pRfMgmtNeighborScanDB->u2ScannedChannel !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u2ScannedChannel)
                {
                    pRfMgmtNeighborScanDB->u2ScannedChannel =
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.u2ScannedChannel;
                    pRfMgmtNeighborScanDB->u1EntryStatus =
                        RFMGMT_NEIGH_ENTRY_ADD;
                }
#ifdef ROGUEAP_WANTED
                /* copying the values into DB */
                MEMSET (pRfMgmtNeighborScanDB->au1ssid, 0, WSSMAC_MAX_SSID_LEN);
                MEMCPY (pRfMgmtNeighborScanDB->au1ssid,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.au1ssid, WSSMAC_MAX_SSID_LEN);

                pRfMgmtNeighborScanDB->u1StationCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1StationCount;
                pRfMgmtNeighborScanDB->isRogueApStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.isRogueApStatus;
                pRfMgmtNeighborScanDB->u4RogueApLastReportedTime =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4RogueApLastReportedTime;
                pRfMgmtNeighborScanDB->isRogueApProcetionType =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.isRogueApProcetionType;

                MEMCPY (pRfMgmtNeighborScanDB->BSSIDRogueApLearntFrom,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.BSSIDRogueApLearntFrom,
                        MAC_ADDR_LEN);

                MEMCPY (pRfMgmtNeighborScanDB->au1RfGroupID,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.au1RfGroupID,
                        WSS_RF_GROUP_ID_MAX_LEN);

                MEMCPY (pRfMgmtNeighborScanDB->NeighborAPMac,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.NeighborAPMac, MAC_ADDR_LEN);
#endif

                pRfMgmtNeighborScanDB->u1NonGFPresent =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1NonGFPresent;
                pRfMgmtNeighborScanDB->u1OBSSNonGFPresent =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1OBSSNonGFPresent;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bEntryStatus != OSIX_FALSE)
            {
                pRfMgmtNeighborScanDB->u1EntryStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1EntryStatus;
            }
            break;

        case RFMGMT_GET_NEIGHBOR_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtNeighborScanTable.RfMgmtNeighborScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bRssi != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.i2Rssi = pRfMgmtNeighborScanDB->i2Rssi;

                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1EntryStatus =
                    pRfMgmtNeighborScanDB->u1EntryStatus;

                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4LastUpdatedTime
                    = pRfMgmtNeighborScanDB->u4LastUpdatedTime;

                /* copying the values from DB to output structure */
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1NonGFPresent =
                    pRfMgmtNeighborScanDB->u1NonGFPresent;
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1OBSSNonGFPresent =
                    pRfMgmtNeighborScanDB->u1OBSSNonGFPresent;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bEntryStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1EntryStatus =
                    pRfMgmtNeighborScanDB->u1EntryStatus;
            }
            break;

        case RFMGMT_GET_FIRST_NEIGH_ENTRY:
            pRfMgmtNeighborScanDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                RfMgmtNeighborAPScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Neighbor entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Neighbor entry not found"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.i2Rssi = pRfMgmtNeighborScanDB->i2Rssi;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1EntryStatus =
                pRfMgmtNeighborScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex
                = pRfMgmtNeighborScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ScannedChannel
                = pRfMgmtNeighborScanDB->u2ScannedChannel;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.NeighborAPMac,
                    pRfMgmtNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

            /* copying the values from DB to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1NonGFPresent =
                pRfMgmtNeighborScanDB->u1NonGFPresent;
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1OBSSNonGFPresent =
                pRfMgmtNeighborScanDB->u1OBSSNonGFPresent;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4LastUpdatedTime
                = pRfMgmtNeighborScanDB->u4LastUpdatedTime;
#ifdef ROGUEAP_WANTED

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.isRogueApProcetionType =
                pRfMgmtNeighborScanDB->isRogueApProcetionType;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.isRogueApStatus =
                pRfMgmtNeighborScanDB->isRogueApStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1StationCount =
                pRfMgmtNeighborScanDB->u1StationCount;
            MEMSET (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1ssid, 0, WSSMAC_MAX_SSID_LEN);
            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1ssid,
                    pRfMgmtNeighborScanDB->au1ssid, WSSMAC_MAX_SSID_LEN);

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1RfGroupID,
                    pRfMgmtNeighborScanDB->au1RfGroupID,
                    WSS_RF_GROUP_ID_MAX_LEN);

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.BSSIDRogueApLearntFrom,
                    pRfMgmtNeighborScanDB->BSSIDRogueApLearntFrom,
                    MAC_ADDR_LEN);
#endif
            break;

        case RFMGMT_GET_NEXT_NEIGH_ENTRY:
            /* Pass the received input and check entry is already present */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid RadioIfIndex\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid RadioIfIndex"));
                return RFMGMT_FAILURE;
            }
            pRfMgmtNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtNeighborScanTable.RfMgmtNeighborScanDB,
                               NULL);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Neighbor entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Neighbor entry not found"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.i2Rssi = pRfMgmtNeighborScanDB->i2Rssi;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1EntryStatus =
                pRfMgmtNeighborScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex
                = pRfMgmtNeighborScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ScannedChannel
                = pRfMgmtNeighborScanDB->u2ScannedChannel;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.NeighborAPMac,
                    pRfMgmtNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4LastUpdatedTime
                = pRfMgmtNeighborScanDB->u4LastUpdatedTime;
            /* copying the values from DB to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1NonGFPresent =
                pRfMgmtNeighborScanDB->u1NonGFPresent;
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1OBSSNonGFPresent =
                pRfMgmtNeighborScanDB->u1OBSSNonGFPresent;
#ifdef ROGUEAP_WANTED
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.isRogueApProcetionType =
                pRfMgmtNeighborScanDB->isRogueApProcetionType;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1StationCount =
                pRfMgmtNeighborScanDB->u1StationCount;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.isRogueApStatus =
                pRfMgmtNeighborScanDB->isRogueApStatus;

            MEMSET (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1ssid, 0, WSSMAC_MAX_SSID_LEN);
            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1ssid,
                    pRfMgmtNeighborScanDB->au1ssid,
                    strlen ((const char *) pRfMgmtNeighborScanDB->au1ssid));

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.au1RfGroupID,
                    pRfMgmtNeighborScanDB->au1RfGroupID,
                    WSS_RF_GROUP_ID_MAX_LEN);

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.BSSIDRogueApLearntFrom,
                    pRfMgmtNeighborScanDB->BSSIDRogueApLearntFrom,
                    MAC_ADDR_LEN);
#endif
            break;

        case RFMGMT_CREATE_CLIENT_SCAN_ENTRY:
            /* Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtClientScanDB =
                 (tRfMgmtClientScanDB *) MemAllocMemBlk
                 (RFMGMT_CLIENT_SCAN_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtClientScanDB: "
                            "Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtClientScanDB: "
                              "Memory alloc failed for radio index"));
                return RFMGMT_FAILURE;
            }

            MEMSET (pRfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));

            /* Copy the received values to the DB */
            pRfMgmtClientScanDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex;

            pRfMgmtClientScanDB->i2ClientSNR =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.i2ClientSNR;

            MEMCPY (pRfMgmtClientScanDB->ClientMacAddress,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress, MAC_ADDR_LEN);

            /* The following info will be required to know the status of the
             * entry. When set to add, Client info is not in sync between
             * AP and AC */
            pRfMgmtClientScanDB->u1EntryStatus = RFMGMT_CLIENT_ENTRY_ADD;

            /* The below time will be used to delete the inactive entries,
             * incase of exceeding the threshold */
            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4LastUpdatedTime = OsixGetSysUpTime ();

            /* The below time will be used to delete the inactive entries,
             * incase of exceeding the threshold */
            pRfMgmtClientScanDB->u4LastUpdatedTime = OsixGetSysUpTime ();

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                 (tRBElem *) pRfMgmtClientScanDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientScanDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                              "Scan DB\r\n"));
                return RFMGMT_FAILURE;
            }

            break;

        case RFMGMT_DESTROY_CLIENT_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientScanTable.RfMgmtClientScanDB);
            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Client Scan in DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Client Scan in DB\r\n"));
                return RFMGMT_FAILURE;
            }
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                       pRfMgmtClientScanDB);
            MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtClientScanDB);
            pRfMgmtClientScanDB = NULL;
            break;

        case RFMGMT_SET_CLIENT_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientScanTable.RfMgmtClientScanDB);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Client Scan in DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Client Scan in DB"));
                return RFMGMT_FAILURE;
            }

            /* Compare the SNR Value exist in DB and the received value. If the
             * value changes then set the value along with entry status as add
             * and update the last updated time */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bClientSNR != OSIX_FALSE)
            {
                if (pRfMgmtClientScanDB->i2ClientSNR !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.i2ClientSNR)
                {
                    pRfMgmtClientScanDB->i2ClientSNR =
                        pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanDB.i2ClientSNR;
                    pRfMgmtClientScanDB->u1EntryStatus =
                        RFMGMT_CLIENT_ENTRY_ADD;

                    pRfMgmtClientScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
                else
                {
                    /* Already reported the timer value to AC. Update the time
                     * alone */
                    pRfMgmtClientScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bEntryStatus != OSIX_FALSE)
            {
                pRfMgmtClientScanDB->u1EntryStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus;
            }
            break;

        case RFMGMT_GET_CLIENT_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientScanTable.RfMgmtClientScanDB);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Client Scan in DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Client Scan in DB\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bClientSNR != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.i2ClientSNR =
                    pRfMgmtClientScanDB->i2ClientSNR;

                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus =
                    pRfMgmtClientScanDB->u1EntryStatus;

                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u4LastUpdatedTime
                    = pRfMgmtClientScanDB->u4LastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bEntryStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus =
                    pRfMgmtClientScanDB->u1EntryStatus;
            }
            break;

        case RFMGMT_GET_FIRST_CLIENT_ENTRY:
            pRfMgmtClientScanDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Client entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Client entry not found\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.i2ClientSNR =
                pRfMgmtClientScanDB->i2ClientSNR;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus =
                pRfMgmtClientScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex
                = pRfMgmtClientScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4LastUpdatedTime =
                pRfMgmtClientScanDB->u4LastUpdatedTime;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress,
                    pRfMgmtClientScanDB->ClientMacAddress, MAC_ADDR_LEN);

            break;

        case RFMGMT_GET_NEXT_CLIENT_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid RadioIfIndex\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid RadioIfIndex\r\n"));
                return RFMGMT_FAILURE;
            }
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtClientScanTable.RfMgmtClientScanDB, NULL);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Client entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Client entry not found\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.i2ClientSNR =
                pRfMgmtClientScanDB->i2ClientSNR;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus =
                pRfMgmtClientScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex
                = pRfMgmtClientScanDB->u4RadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress,
                    pRfMgmtClientScanDB->ClientMacAddress, MAC_ADDR_LEN);

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4LastUpdatedTime =
                pRfMgmtClientScanDB->u4LastUpdatedTime;

            break;
        case RFMGMT_SET_AP_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_SET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_SET_AP_CONFIG_ENTRY\r\n"));
                return RFMGMT_FAILURE;
            }
            RfMgmtAPConfigDB.u4Dot11RadioType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4Dot11RadioType;
            RfMgmtAPConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;

            /* Pass the received input and check entry is already present */
            pRfMgmtAPConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtApConfigTable.RfMgmtAPConfigDB);

            if (pRfMgmtAPConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP\r\n"));
                return RFMGMT_FAILURE;
            }
            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bCurrentChannel == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2CurrentChannel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2CurrentChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeTime == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u4ChannelChangeTime =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u4ChannelChangeTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborMsgPeriod == OSIX_TRUE)
            {

                if (pRfMgmtAPConfigDB->u2NeighborMsgPeriod !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborMsgPeriod)
                {
                    pRfMgmtAPConfigDB->u4NeighMsgUpdatedTime =
                        OsixGetSysUpTime ();
                }
                pRfMgmtAPConfigDB->u2NeighborMsgPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborMsgPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelScanDuration == OSIX_TRUE)
            {

                if (pRfMgmtAPConfigDB->u2ChannelScanDuration !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2ChannelScanDuration)
                {
                    pRfMgmtAPConfigDB->u4APScanUpdatedTime =
                        OsixGetSysUpTime ();
                }
                pRfMgmtAPConfigDB->u2ChannelScanDuration =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2ChannelScanDuration;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborAgingPeriod == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2NeighborAgingPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborAgingPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeCount == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1ChannelChangeCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelChangeCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelAssignmentMode == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1ChannelAssignmentMode =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelAssignmentMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bAutoScanStatus == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1AutoScanStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1AutoScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRssiThreshold == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->i2RssiThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.i2RssiThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChSwitchStatus == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1ChSwitchStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChSwitchStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcStatus == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1RfMgmt11hTpcStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1RfMgmt11hTpcStatus;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDFSChannelStatus == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtAPConfigDB->DFSChannelStatus,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.DFSChannelStatus,
                        (sizeof (tRfMgmtDfsChannelInfo) *
                         RADIO_MAX_DFS_CHANNEL));
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsMeasurementInterval
                == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsMeasurementInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsChannelSwitchStatus
                == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsChannelSwitchStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsStatus == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1RfMgmt11hDfsStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1RfMgmt11hDfsStatus;
            }
            break;
        case RFMGMT_GET_AP_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_GET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_GET_AP_CONFIG_ENTRY\r\n"));
                return RFMGMT_FAILURE;
            }
            RfMgmtAPConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;

            RfMgmtAPConfigDB.u4Dot11RadioType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4Dot11RadioType;

            /* Pass the received input and check entry is already present */
            pRfMgmtAPConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtApConfigTable.RfMgmtAPConfigDB);

            if (pRfMgmtAPConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP\r\n"));
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDot11RadioType == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u4Dot11RadioType =
                    RfMgmtAPConfigDB.u4Dot11RadioType;
            }
            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bCurrentChannel == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2CurrentChannel =
                    pRfMgmtAPConfigDB->u2CurrentChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeTime == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u4ChannelChangeTime =
                    pRfMgmtAPConfigDB->u4ChannelChangeTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborMsgPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborMsgPeriod =
                    pRfMgmtAPConfigDB->u2NeighborMsgPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelScanDuration == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2ChannelScanDuration =
                    pRfMgmtAPConfigDB->u2ChannelScanDuration;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborAgingPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborAgingPeriod =
                    pRfMgmtAPConfigDB->u2NeighborAgingPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelChangeCount =
                    pRfMgmtAPConfigDB->u1ChannelChangeCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelAssignmentMode == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelAssignmentMode =
                    pRfMgmtAPConfigDB->u1ChannelAssignmentMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bAutoScanStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1AutoScanStatus =
                    pRfMgmtAPConfigDB->u1AutoScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRssiThreshold == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.i2RssiThreshold =
                    pRfMgmtAPConfigDB->i2RssiThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChSwitchStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChSwitchStatus =
                    pRfMgmtAPConfigDB->u1ChSwitchStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval =
                    pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1RfMgmt11hTpcStatus =
                    pRfMgmtAPConfigDB->u1RfMgmt11hTpcStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsMeasurementInterval
                == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsMeasurementInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsChannelSwitchStatus
                == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsChannelSwitchStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1RfMgmt11hDfsStatus =
                    pRfMgmtAPConfigDB->u1RfMgmt11hDfsStatus;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDFSChannelStatus == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.DFSChannelStatus,
                        pRfMgmtAPConfigDB->DFSChannelStatus,
                        (sizeof (tRfMgmtDfsChannelInfo) *
                         RADIO_MAX_DFS_CHANNEL));
            }
            break;
        case RFMGMT_SET_CLIENT_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_SET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_SET_AP_CONFIG_ENTRY\r\n"));
                return RFMGMT_FAILURE;
            }
            RfMgmtClientConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex;

            /* Pass the received input and check entry is already present */
            pRfMgmtClientConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientConfigTable.RfMgmtClientConfigDB);

            if (pRfMgmtClientConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerLevel == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u2TxPowerLevel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2TxPowerLevel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeTime == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u4TxPowerChangeTime =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerChangeTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod == OSIX_TRUE)
            {
                if (pRfMgmtClientConfigDB->u2SNRScanPeriod !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2SNRScanPeriod)
                {
                    pRfMgmtClientConfigDB->u4ScanPeriodUpdatedTime =
                        OsixGetSysUpTime ();
                }
                pRfMgmtClientConfigDB->u2SNRScanPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2SNRScanPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeCount == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u1TxPowerChangeCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1TxPowerChangeCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u1SNRScanStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1SNRScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRThreshold == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->i2SNRThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.i2SNRThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bBssidScanStatus == OSIX_TRUE)
            {
                u1WlanId = (UINT1) (pRfMgmtDB->unRfMgmtDB.
                                    RfMgmtClientConfigTable.
                                    RfMgmtClientConfigDB.u1WlanId - 1);
                pRfMgmtClientConfigDB->u1BssidScanStatus[u1WlanId] =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1BssidScanStatus[u1WlanId];
            }
            break;
        case RFMGMT_GET_CLIENT_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_GET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_GET_AP_CONFIG_ENTRY\r\n"));
                return RFMGMT_FAILURE;
            }
            RfMgmtClientConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex;

            /* Pass the received input and check entry is already present */
            pRfMgmtClientConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientConfigTable.RfMgmtClientConfigDB);

            if (pRfMgmtClientConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the client\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the client\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerLevel == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2TxPowerLevel =
                    pRfMgmtClientConfigDB->u2TxPowerLevel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeTime == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerChangeTime =
                    pRfMgmtClientConfigDB->u4TxPowerChangeTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2SNRScanPeriod =
                    pRfMgmtClientConfigDB->u2SNRScanPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1TxPowerChangeCount =
                    pRfMgmtClientConfigDB->u1TxPowerChangeCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1SNRScanStatus =
                    pRfMgmtClientConfigDB->u1SNRScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRThreshold == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.i2SNRThreshold =
                    pRfMgmtClientConfigDB->i2SNRThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bBssidScanStatus == OSIX_TRUE)
            {
                u1WlanId = (UINT1) (pRfMgmtDB->unRfMgmtDB.
                                    RfMgmtClientConfigTable.
                                    RfMgmtClientConfigDB.u1WlanId - 1);
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1BssidScanStatus[u1WlanId] =
                    pRfMgmtClientConfigDB->u1BssidScanStatus[u1WlanId];
            }
            break;

        case RFMGMT_CREATE_RADIO_IF_INDEX_ENTRY:
            /*Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtAPConfigDB =
                 (tRfMgmtAPConfigDB *) MemAllocMemBlk
                 (RFMGMT_INDEX_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for radio index\n"));
                return RFMGMT_FAILURE;
            }
            if ((pRfMgmtClientConfigDB =
                 (tRfMgmtClientConfigDB *) MemAllocMemBlk
                 (RFMGMT_CLIENT_INDEX_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for radio index\n"));

                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                pRfMgmtAPConfigDB = NULL;
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtAPConfigDB, 0, sizeof (tRfMgmtAPConfigDB));
            MEMSET (pRfMgmtClientConfigDB, 0, sizeof (tRfMgmtClientConfigDB));

            pRfMgmtAPConfigDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;
            pRfMgmtAPConfigDB->u4Dot11RadioType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4Dot11RadioType;

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                 (tRBElem *) pRfMgmtAPConfigDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientConfigDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for AP config "
                            "DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for AP config "
                              "DB\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Initializing default values for the AP and Client Config Table */
            pRfMgmtAPConfigDB->u2CurrentChannel = RFMGMT_DEF_CURRENT_CHANNEL;

            pRfMgmtAPConfigDB->u1AutoScanStatus = RFMGMT_AUTO_SCAN_ENABLE;

            pRfMgmtAPConfigDB->u2NeighborMsgPeriod =
                RFMGMT_DEF_NEIGHBOR_MSG_PERIOD;

            pRfMgmtAPConfigDB->u2ChannelScanDuration =
                RFMGMT_DEF_CHANNEL_SCAN_DURATION;

            pRfMgmtAPConfigDB->u4APScanUpdatedTime = OsixGetSysUpTime ();

            pRfMgmtAPConfigDB->u2NeighborAgingPeriod =
                RFMGMT_DEF_NEIGHBOR_AGING_PERIOD;

            pRfMgmtAPConfigDB->u4NeighMsgUpdatedTime = OsixGetSysUpTime ();

            pRfMgmtAPConfigDB->u1ChannelChangeCount =
                RFMGMT_DEF_CHANNEL_CHANGE_COUNT;

            pRfMgmtAPConfigDB->u1AutoScanStatus = RFMGMT_AUTO_SCAN_ENABLE;

            pRfMgmtAPConfigDB->u1ChSwitchStatus =
                RFMGMT_CHANNEL_SWITCH_MSG_ENABLE;

            pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval =
                RFMGMT_DEF_TPC_REQUEST_INTERVAL;

            pRfMgmtAPConfigDB->u1RfMgmt11hTpcStatus = RFMGMT_DEF_11H_TPC_STATUS;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval =
                RFMGMT_DEF_DFS_QUIET_INTERVAL;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod =
                RFMGMT_DEF_DFS_QUIET_PERIOD;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsMeasurementInterval =
                RFMGMT_DEF_DFS_MEASUREMENT_INTERVAL;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsChannelSwitchStatus =
                RFMGMT_DEF_DFS_CHANNEL_SWITCH_STATUS;
            pRfMgmtAPConfigDB->u1RfMgmt11hDfsStatus = RFMGMT_DEF_11H_DFS_STATUS;

            pRfMgmtAPConfigDB->i2RssiThreshold = RFMGMT_DEF_RSSI_THRESHOLD;

            pRfMgmtClientConfigDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;

            pRfMgmtClientConfigDB->u4ScanPeriodUpdatedTime =
                OsixGetSysUpTime ();

            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                 (tRBElem *) pRfMgmtClientConfigDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientConfigDB);
                RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                           RfMgmtNeighborAPConfigDB, pRfMgmtAPConfigDB);
                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                            "config DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                              "config DB\r\n"));
                return RFMGMT_FAILURE;
            }

            pRfMgmtClientConfigDB->u4TxPowerChangeTime =
                RFMGMT_DEF_TX_POWER_CHANGE_TIME;

            pRfMgmtClientConfigDB->u2SNRScanPeriod = RFMGMT_DEF_SNR_SCAN_PERIOD;

            pRfMgmtClientConfigDB->u1TxPowerChangeCount =
                RFMGMT_DEF_TX_POWER_CHANGE_COUNT;

            pRfMgmtClientConfigDB->u1SNRScanStatus = RFMGMT_DEF_SNR_SCAN_STATUS;

            pRfMgmtClientConfigDB->u4ScanPeriodUpdatedTime =
                OsixGetSysUpTime ();

            pRfMgmtClientConfigDB->i2SNRThreshold = RFMGMT_DEF_SNR_THRESHOLD;

            pRfMgmtClientConfigDB->i2SNRThreshold = RFMGMT_DEF_SNR_THRESHOLD;

            for (u1WlanId = 0; u1WlanId < MAX_RFMGMT_WLAN_ID; u1WlanId++)
            {
                pRfMgmtClientConfigDB->u1BssidScanStatus[u1WlanId]
                    = RFMGMT_BSSID_SCAN_STATUS_ENABLE;
            }
            break;
        case RFMGMT_CREATE_SCAN_ALLOWED_ENTRY:
            /*Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtChannelAllowedScanDB =
                 (tRfMgmtChannelAllowedScanDB *) MemAllocMemBlk
                 (RFMGMT_AP_CHANNEL_ALLOWED_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n"));
                return RFMGMT_FAILURE;
            }
            MEMSET (pRfMgmtChannelAllowedScanDB, 0,
                    sizeof (tRfMgmtChannelAllowedScanDB));
            pRfMgmtChannelAllowedScanDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioIfIndex;
            pRfMgmtChannelAllowedScanDB->u4RadioType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioType;

            pRfMgmtChannelAllowedScanDB->u1Channel =
                pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u1Channel;

            pRfMgmtChannelAllowedScanDB->u1SecChannelOffset =
                pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u1SecChannelOffset;

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtChannelAllowedScanDB,
                 (tRBElem *) pRfMgmtChannelAllowedScanDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_AP_CHANNEL_ALLOWED_POOLID,
                                    (UINT1 *) pRfMgmtChannelAllowedScanDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n"));

                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for allowed channel"
                            "DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for allowed channel "
                              "DB\r\n"));
                return RFMGMT_FAILURE;
            }
            break;
        case RFMGMT_GET_FIRST_SCAN_ALLOWED_ENTRY:
            pRfMgmtChannelAllowedScanDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                RfMgmtChannelAllowedScanDB);

            if (pRfMgmtChannelAllowedScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n"));

                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Client entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Client entry not found\r\n"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioIfIndex =
                pRfMgmtChannelAllowedScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioType =
                pRfMgmtChannelAllowedScanDB->u4RadioType;

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u1Channel =
                pRfMgmtChannelAllowedScanDB->u1Channel;

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u1SecChannelOffset =
                pRfMgmtChannelAllowedScanDB->u1SecChannelOffset;
            break;
        case RFMGMT_GET_NEXT_SCAN_ALLOWED_ENTRY:
            /* Pass the received input and check entry is already present */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid RadioIfIndex\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid RadioIfIndex"));
                return RFMGMT_FAILURE;
            }
            pRfMgmtChannelAllowedScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtChannelAllowedScanDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtChannelAllowedScanTable.
                               RfMgmtChannelAllowedScanDB, NULL);

            if (pRfMgmtChannelAllowedScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Neighbor entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Neighbor entry not found"));
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n"));

                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioIfIndex =
                pRfMgmtChannelAllowedScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u4RadioType =
                pRfMgmtChannelAllowedScanDB->u4RadioType;

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u1Channel =
                pRfMgmtChannelAllowedScanDB->u1Channel;

            pRfMgmtDB->unRfMgmtDB.RfMgmtChannelAllowedScanTable.
                RfMgmtChannelAllowedScanDB.u1SecChannelOffset =
                pRfMgmtChannelAllowedScanDB->u1SecChannelOffset;
            break;

        case RFMGMT_DESTROY_SCAN_ALLOWED_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtChannelAllowedScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.
                           RfMgmtChannelAllowedScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtChannelAllowedScanTable.
                           RfMgmtChannelAllowedScanDB);

            if (pRfMgmtChannelAllowedScanDB == NULL)
            {
                /* If not present, return error */

                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtChannelAllowedScanDB: Memory alloc failed for radio index\n"));
                return RFMGMT_FAILURE;
            }
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtChannelAllowedScanDB,
                       pRfMgmtChannelAllowedScanDB);
            MemReleaseMemBlock (RFMGMT_AP_CHANNEL_ALLOWED_POOLID,
                                (UINT1 *) pRfMgmtChannelAllowedScanDB);
            pRfMgmtChannelAllowedScanDB = NULL;
            break;

        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmt: Unknown request received\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmt: Unknown request received\n"));
            return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteNeighborDetails                                */
/*                                                                           */
/* Description  : This function will destroy the neighbor AP infomarion      */
/*                for the given radio ifindex                                */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteNeighborDetails (UINT4 u4RadioIfIndex, UINT1 u1FlushAlgoTable)
{
    tRfMgmtNeighborScanDB RfMgmtNextNeighborScanDB;
    tRfMgmtNeighborScanDB *pRfMgmtNextNeighborScanDB = NULL;

    UNUSED_PARAM (u1FlushAlgoTable);

    MEMSET (&RfMgmtNextNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));

    RFMGMT_FN_ENTRY ();

    /* Get all the neighbor AP information for the radio index 
       and flush the entries in the DB */

    pRfMgmtNextNeighborScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);

    while (pRfMgmtNextNeighborScanDB != NULL)
    {
        RfMgmtNextNeighborScanDB.u4RadioIfIndex =
            pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
        RfMgmtNextNeighborScanDB.u2ScannedChannel =
            pRfMgmtNextNeighborScanDB->u2ScannedChannel;
        MEMCPY (RfMgmtNextNeighborScanDB.NeighborAPMac,
                pRfMgmtNextNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);
        if (pRfMgmtNextNeighborScanDB->u4RadioIfIndex == u4RadioIfIndex)
        {

            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtNeighborAPScanDB, pRfMgmtNextNeighborScanDB);
            MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNextNeighborScanDB);

            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);
        }
        else
        {
            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);
        }

    }

    /* If required invoke DCA Algorithm here */
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteClientDetails                                  */
/*                                                                           */
/* Description  : This function will destroy the client infomarion for the   */
/*                given radio ifindex                                        */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteClientDetails (UINT4 u4RadioIfIndex)
{
    tRfMgmtClientScanDB RfMgmtClientScanDB;
    tRfMgmtClientScanDB RfMgmtNextClientScanDB;
    tRfMgmtClientScanDB *pRfMgmtNextClientScanDB = NULL;

    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));

    RFMGMT_FN_ENTRY ();
    /* Get all the CLIENT information for the radio index
     *        and flush the entries in the DB */

    pRfMgmtNextClientScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                       (tRBElem *) & RfMgmtNextClientScanDB, NULL);

    while (pRfMgmtNextClientScanDB != NULL)
    {
        RfMgmtNextClientScanDB.u4RadioIfIndex =
            pRfMgmtNextClientScanDB->u4RadioIfIndex;
        MEMCPY (RfMgmtNextClientScanDB.ClientMacAddress,
                pRfMgmtNextClientScanDB->ClientMacAddress, MAC_ADDR_LEN);
        if (pRfMgmtNextClientScanDB->u4RadioIfIndex == u4RadioIfIndex)
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtClientScanDB, pRfMgmtNextClientScanDB);
            MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNextClientScanDB);

        }
        pRfMgmtNextClientScanDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & RfMgmtNextClientScanDB, NULL);

    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}
#endif
