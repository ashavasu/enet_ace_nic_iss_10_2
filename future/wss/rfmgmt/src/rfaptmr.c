
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfaptmr.c,v 1.5 2017/11/24 10:37:05 siva Exp $
 *
 * Description: This file contains the RFMGMT Timer related routines.
 *                            
 ********************************************************************/

#ifndef __RFAPTMR_C__
#define __RFAPTMR_C__

#include "rfminc.h"
#include "aphdlr.h"
#include "capwapconst.h"
#include "radioifextn.h"
#include "wssifstawtpprot.h"
#include "wsscfginc.h"
#include "radioifhwnpwr.h"

static tRfMgmtTimerList gRfMgmtTmrList;
extern UINT1        gu1WtpClientWalkIndex;

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtWtpTmrInit                                           *
 *                                                                           *
 * Description  : This function creates a timer list for all the timers      *
 *                          in RFMGMT module.                                *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
RfMgmtWtpTmrInit (VOID)
{
    RFMGMT_FN_ENTRY ();
    /* Timer List for State Machine Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) RFMGMT_WTP_TASK_NAME,
                            RFMGMT_TIMER_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gRfMgmtTmrList.
                                                RfMgmtTmrListId)) ==
        TMR_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfmgmtTmrInit: Failed to "
                    "create the Application Timer List \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfmgmtTmrInit: Failed to "
                      "create the Application Timer List"));
        return RFMGMT_FAILURE;
    }

    RfMgmtWtpTmrInitTmrDesc ();

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : RfMgmtWtpTmrDeInit
 *
 * Description  : This function deletes the timer list for the timers
 *                in RFMGMT module.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
INT4
RfMgmtWtpTmrDeInit (VOID)
{
    RFMGMT_FN_ENTRY ();

    if (TmrDeleteTimerList (gRfMgmtTmrList.RfMgmtTmrListId) == TMR_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtTmrInit: Failed to delete the RFMGMT " "Timer List");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtTmrInit: Failed to delete the RFMGMT "
                      "Timer List"));
        return RFMGMT_FAILURE;
    }

    MEMSET (gRfMgmtTmrList.aRfMgmtTmrDesc, 0,
            (sizeof (tTmrDesc) * RFMGMT_MAX_TMR));

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtWtpTmrInitTmrDesc                                       *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the timers in RFMGMT module.                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtWtpTmrInitTmrDesc (VOID)
{
    RFMGMT_FN_ENTRY ();

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_NEIGH_SCAN_TMR].TmrExpFn
        = RfMgmtScannedMsgTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_NEIGH_SCAN_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CHAN_SCAN_TMR].TmrExpFn
        = RfMgmtChannelScanTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CHAN_SCAN_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_TX_MESSAGE_EXP_TMR].TmrExpFn
        = RfMgmtTxNeighborMsgTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_TX_MESSAGE_EXP_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CLIENT_SNR_SCAN_TMR].TmrExpFn
        = RfMgmtClientScanTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CLIENT_SNR_SCAN_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_11H_TPC_REQUEST_TMR].TmrExpFn
        = RfMgmtTpcRequestTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_11H_TPC_REQUEST_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CHAN_SWIT_ANNOUNCE_TMR].TmrExpFn
        = RfMgmtChanSwitAnnounceTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CHAN_SWIT_ANNOUNCE_TMR].i2Offset = -1;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CAC_TMR].TmrExpFn = RfMgmtCacTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_CAC_TMR].i2Offset = -1;

    RFMGMT_FN_EXIT ();
}

/*****************************************************************************
 * Function                  : RfMgmtScannedMsgTmrExp                        *
 *                                                                           *
 * Description               : This routine handles the scanned information  *
 *                             of Neighbor AP and Client SNR entries         *
 *                                                                           *
 * Input                     : pArg - Input argument                         *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtScannedMsgTmrExp (VOID *pArg)
{
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Period = 0;
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtNextAPConfigDB = NULL;

    UNUSED_PARAM (pArg);

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    pRfMgmtNextAPConfigDB = RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                            RfMgmtNeighborAPConfigDB);

    while (pRfMgmtNextAPConfigDB != NULL)
    {
        /* Check the aging period of neighbor AP and the last updated 
         * time with the current time. If the timer expired, then set 
         * the entry status flag to delete */
        pRfMgmtAPConfigDB = pRfMgmtNextAPConfigDB;
        u4Period = pRfMgmtAPConfigDB->u2NeighborAgingPeriod;

        if (RfMgmtProcessDBMsg (RFMGMT_GET_FIRST_NEIGH_ENTRY,
                                &RfMgmtDB) == RFMGMT_SUCCESS)
        {
            do
            {
                /* Check whether the radio ifindex entry is present in 
                 * the DB. If yes then check whether the entry is active */
                if (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4RadioIfIndex ==
                    pRfMgmtAPConfigDB->u4RadioIfIndex)
                {
                    /* Get the current time */
                    u4CurrentTime = OsixGetSysUpTime ();
                    u4CurrentTime = (u4CurrentTime -
                                     RfMgmtDB.unRfMgmtDB.
                                     RfMgmtNeighborScanTable.
                                     RfMgmtNeighborScanDB.u4LastUpdatedTime);

                    /* If the entry is not updated till the aging period 
                     * occured, then set the entry status to delete. */
                    if (u4Period <= u4CurrentTime)
                    {
                        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanIsSetDB.bEntryStatus = OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanDB.u1EntryStatus =
                            RFMGMT_NEIGH_ENTRY_DELETE;

                        /* For the given radio ifindex fetch all the entries 
                         * available in the DB */
                        if (RfMgmtProcessDBMsg (RFMGMT_SET_NEIGHBOR_SCAN_ENTRY,
                                                &RfMgmtDB) != RFMGMT_SUCCESS)
                        {
                            RFMGMT_TRC (RFMGMT_INFO_TRC,
                                        "RfMgmtScannedMsgTmrExp: DB update failed\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                          "RfMgmtScannedMsgTmrExp: DB update failed"));
                            continue;
                        }
                    }
                }
            }
            while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_NEIGH_ENTRY,
                                       &RfMgmtDB) == RFMGMT_SUCCESS);
        }

        pRfMgmtNextAPConfigDB = RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                               RfMgmtNeighborAPConfigDB,
                                               (tRBElem *) pRfMgmtAPConfigDB,
                                               NULL);
    }

    if (RfMgmtProcessDBMsg (RFMGMT_GET_FIRST_CLIENT_ENTRY,
                            &RfMgmtDB) == RFMGMT_SUCCESS)
    {
        do
        {
            /* Check whether the radio ifindex entry is present in 
             * the DB. If yes then check whether the entry is active */
            if (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex ==
                pRfMgmtAPConfigDB->u4RadioIfIndex)
            {
                /* Get the current time */
                u4CurrentTime = OsixGetSysUpTime ();
                u4CurrentTime = (u4CurrentTime -
                                 RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                                 RfMgmtClientScanDB.u4LastUpdatedTime);

                /* If the entry is not updated till the aging period 
                 * occured, then set the entry status to delete. */
                if (u4Period <= u4CurrentTime)
                {
                    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanIsSetDB.bEntryStatus = OSIX_TRUE;
                    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanDB.u1EntryStatus =
                        RFMGMT_CLIENT_ENTRY_DELETE;

                    /* For the given radio ifindex fetch all the entries 
                     * available in the DB */
                    if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_SCAN_ENTRY,
                                            &RfMgmtDB) != RFMGMT_SUCCESS)
                    {
                        RFMGMT_TRC (RFMGMT_INFO_TRC,
                                    "RfMgmtScannedMsgTmrExp: DB update "
                                    "failed\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtScannedMsgTmrExp: DB update "
                                      "failed\n"));
                        continue;
                    }
                }
            }
        }
        while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_CLIENT_ENTRY,
                                   &RfMgmtDB) == RFMGMT_SUCCESS);
    }
    /* Start the timer */
    RfMgmtWtpTmrStart (u4RadioIfIndex,
                       RFMGMT_NEIGH_SCAN_TMR, RFMGMT_AGEOUT_PERIOD);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmtChannelScanTmrExp                       *
 *                                                                           *
 * Description               : This routine handles the channel duration on  *
 *                             which AP will scan for neighbor message       *
 *                                                                           *
 * Input                     : pArg - Input argument                         *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtChannelScanTmrExp (VOID *pArg)
{
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Period = 0;
    UINT4               u4Index = 0;
    UINT1               u1MaxChannelA = 0;
    UINT1               u1MaxChannelB = 0;
    UINT1               u1Channel = 0;
    UINT1               u1LastAChannel = 0;
    UINT1               u1FirstAChannel = 0;
    UINT1               u1Index1 = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtNextAPConfigDB = NULL;

    RFMGMT_FN_ENTRY ();

    UNUSED_PARAM (pArg);

    pRfMgmtNextAPConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB);
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtNextAPConfigDB->u4RadioIfIndex;
    /* GETTING CONFIGURED RADIO TYPE for the particular Radio from RADIO db */
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  (&RadioIfGetDB)) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Country String get failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Country String get failed\n"));
        return;
    }
    pRfMgmtNextAPConfigDB->u4Dot11RadioType =
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
    for (u4Index = 0; u4Index < RADIOIF_SUPPORTED_COUNTRY_LIST; u4Index++)
    {
        if (STRCMP (RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                    gWsscfgSupportedCountryList[u4Index].au1CountryCode) == 0)
        {
            /*Finding the index of country and reg domain correspondingly */
            u4Index = (UINT4) gRegDomainCountryMapping[u4Index].u2RegDomain;
            break;
        }
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_FALSE;
    u1FirstAChannel = RFMGMT_FIRST_CHANNEL_RADIOA;
    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
    {
        if (gau1Dot11aChannelCheckList[u1Channel] != 0)
        {
            u1LastAChannel = gau1Dot11aRegDomainChannelList[u1Channel];
            u1MaxChannelA++;
        }
    }

    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
    {
        if (gau1Dot11bChannelRegDomainCheckList[u4Index][u1Channel] != 0)
        {
            u1MaxChannelB++;
        }
    }
    while (pRfMgmtNextAPConfigDB != NULL)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRfMgmtNextAPConfigDB->u4RadioIfIndex;
        /* GETTING CONFIGURED RADIO TYPE for the particular Radio from RADIO db */
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Radiotype get failed\r\n");
            return;
        }
        pRfMgmtNextAPConfigDB->u4Dot11RadioType =
            RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
        pRfMgmtAPConfigDB = pRfMgmtNextAPConfigDB;
        if (pRfMgmtAPConfigDB->u1AutoScanStatus == RFMGMT_AUTO_SCAN_ENABLE)
        {
            /* Get the current time */
            u4CurrentTime = OsixGetSysUpTime ();
            u4CurrentTime = (u4CurrentTime -
                             pRfMgmtAPConfigDB->u4APScanUpdatedTime);
            if ((pRfMgmtNextAPConfigDB->u4Dot11RadioType == RADIO_TYPEA) ||
                (pRfMgmtNextAPConfigDB->u4Dot11RadioType == RADIO_TYPEAN) ||
                (pRfMgmtNextAPConfigDB->u4Dot11RadioType == RADIO_TYPEAC))
            {
                u4Period = (UINT4) (pRfMgmtAPConfigDB->u2ChannelScanDuration /
                                    u1MaxChannelA);
                if (u4Period <= u4CurrentTime)
                {
                    /*Check for first and last channel */
                    if ((pRfMgmtNextAPConfigDB->u1LastScannedChannel ==
                         u1LastAChannel)
                        || (pRfMgmtNextAPConfigDB->u1LastScannedChannel == 0))
                    {
                        pRfMgmtNextAPConfigDB->u1LastScannedChannel =
                            u1FirstAChannel;
                        /*Resetting to start next iteration */
                        pRfMgmtNextAPConfigDB->u1LastScannedIndex = 0;
                    }
                    else if (pRfMgmtNextAPConfigDB->u1LastScannedChannel != 0)
                    {
                        for (u1Index1 =
                             pRfMgmtNextAPConfigDB->u1LastScannedIndex + 1;
                             u1Index1 < RFMGMT_MAX_CHANNELA; u1Index1++)
                        {
                            if (gau1Dot11aChannelCheckList[u1Index1] != 0)
                            {
                                pRfMgmtNextAPConfigDB->u1LastScannedIndex =
                                    u1Index1;
                                pRfMgmtNextAPConfigDB->u1LastScannedChannel =
                                    gau1Dot11aRegDomainChannelList[u1Index1];
                                break;
                            }
                        }
                    }
                    RfMgmtStartNeighborAPScan (pRfMgmtAPConfigDB,
                                               pRfMgmtNextAPConfigDB->
                                               u1LastScannedChannel);
                    pRfMgmtAPConfigDB->u4APScanUpdatedTime =
                        OsixGetSysUpTime ();
                }
            }
            else
            {
                u4Period = (UINT4) (pRfMgmtAPConfigDB->u2ChannelScanDuration /
                                    u1MaxChannelB);
                if (u4Period <= u4CurrentTime)
                {
                    if (u1MaxChannelB ==
                        pRfMgmtNextAPConfigDB->u1LastScannedChannel)
                    {
                        pRfMgmtNextAPConfigDB->u1LastScannedChannel = 1;
                    }
                    else
                    {
                        pRfMgmtNextAPConfigDB->u1LastScannedChannel =
                            pRfMgmtNextAPConfigDB->u1LastScannedChannel + 1;
                    }

                    RfMgmtStartNeighborAPScan (pRfMgmtAPConfigDB,
                                               pRfMgmtNextAPConfigDB->
                                               u1LastScannedChannel);

                    pRfMgmtAPConfigDB->u4APScanUpdatedTime =
                        OsixGetSysUpTime ();
                }

            }

        }
        pRfMgmtNextAPConfigDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) pRfMgmtAPConfigDB, NULL);
    }

    /* Start the timer */
    RfMgmtWtpTmrStart (u4RadioIfIndex,
                       RFMGMT_CHAN_SCAN_TMR, RFMGMT_AP_SCAN_PERIOD);

    RFMGMT_FN_EXIT ();
    return;
}

/****************************************************************************
* Function                  : RfMgmtChanSwitAnnounceTmrExp                  *
*                                                                           *
* Description               : This routine handles the Channel Switch       *
*                             Announcement Timer Expiry.                    *
*                                                                           *
* Input                     : pSessEntry - pointer to session entry table   *
*                                                                           *
* Output                    : None                                          *
*                                                                           *
* Returns                   : None.                                         *
*****************************************************************************/
VOID
RfMgmtChanSwitAnnounceTmrExp (VOID *pArg)
{
    INT4                i4RadioIfIndex = 1;
    UINT4               u4FsCapwapWtpRadioChannelChangeCount;
    UINT4               u4RadioType = 0;
    UINT1               u1Index = 0;
    UINT1               au1RadioIfName[RADIO_INTF_NAME_LEN + 1];
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtNextAPConfigDB = NULL;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    RFMGMT_FN_ENTRY ();

    UNUSED_PARAM (pArg);

    pRfMgmtNextAPConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB);

    while (pRfMgmtNextAPConfigDB != NULL)
    {
        pRfMgmtAPConfigDB = pRfMgmtNextAPConfigDB;

        MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRfMgmtAPConfigDB->u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
            OSIX_FAILURE)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtChanSwitAnnounceTmrExp: RadioDB access failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtChanSwitAnnounceTmrExp: RadioDB access failed\n"));
            return;
        }
        u4RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
        if ((u4RadioType == RADIO_TYPEA) || (u4RadioType == RADIO_TYPEAN)
            || (u4RadioType == RADIO_TYPEAC))
        {
            if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount != 0)
            {
                for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED;
                     u1Index++)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                                  (&RadioIfGetDB)) ==
                        OSIX_SUCCESS)
                    {
                        if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                            break;
                    }
                    else
                        continue;
                }
                MEMSET (au1RadioIfName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioIfName, "%s%d", RADIO_INTF_NAME,
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId - 1);
#endif
                if (u4RadioType == RADIO_TYPE_AC)
                {

                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeElem = OSIX_TRUE;

                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1VhtOperModeNotify = OSIX_TRUE;

                    if ((WssIfProcessRadioIfDBMsg
                         (WSS_GET_RADIO_IF_DB_11AC,
                          &RadioIfGetDB)) != OSIX_SUCCESS)
                    {
                        RFMGMT_TRC (RFMGMT_INFO_TRC,
                                    "RfMgmtChanSwitAnnounceTmrExp: RadioDB access failed\r\n");
                        continue;

                    }
                    /*To remove Channel wrapper IE in beacon */
                    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
                        u1Action = OSIX_FALSE;

                    if (RadioIfSetDot11AC (&RadioIfGetDB,
                                           (UINT1 *) &au1RadioIfName) !=
                        OSIX_SUCCESS)
                    {
                        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                    "RfMgmtChanSwitAnnounceTmrExp: RadioDB access failed\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtChanSwitAnnounceTmrExp: RadioDB access failed\\n"));
                        return;
                    }
                }
                RadioIfRemoveChanlSwitIE (&RadioIfGetDB);
                if (RadioIfSetOFDMCurrentChannel
                    (&RadioIfGetDB, (UINT1 *) &au1RadioIfName) != OSIX_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtChanSwitAnnounceTmrExp: Writing to the hardware failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtChanSwitAnnounceTmrExp: Writing to the hardware failed\n"));
                    return;
                }
                if (nmhGetFsCapwapWtpRadioChannelChangeCount (i4RadioIfIndex,
                                                              &u4FsCapwapWtpRadioChannelChangeCount)
                    != SNMP_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "nmhGetFsCapwapWtpRadioConfigUpdateCount, returns failure\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "nmhGetFsCapwapWtpRadioConfigUpdateCount, returns failure\n"));
                }
                u4FsCapwapWtpRadioChannelChangeCount++;
                if (nmhSetFsCapwapWtpRadioChannelChangeCount (i4RadioIfIndex,
                                                              u4FsCapwapWtpRadioChannelChangeCount)
                    != SNMP_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "nmhSetFsCapwapWtpRadioChannelChangeCount, returns failure \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "nmhSetFsCapwapWtpRadioChannelChangeCount, returns failure \n"));
                }
#ifdef RFMGMT_WANTED
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u2Channel =
                    (UINT2) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

                if (WssIfProcessRfMgmtMsg (RFMGMT_CHANNEL_UPDATE_MSG,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                                 "Update Channel to RF module returns failure \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "Update Channel to RF module returns failure\n"));
                }
#endif
            }
        }
        pRfMgmtNextAPConfigDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) pRfMgmtAPConfigDB, NULL);
    }

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmtTxNeighborMsgTmrExp                     *
 *                                                                           *
 * Description               : This routine handles the Retransmit Timer     *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtTxNeighborMsgTmrExp (VOID *pArg)
{
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4CurrentTime = 0;
    UINT2               u2Period = 0;
    tRfMgmtMsgStruct    WssMsgStruct;
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtNextAPConfigDB = NULL;
    tRfMgmtClientConfigDB *pRfMgmtNextClientConfigDB = NULL;
    unApHdlrMsgStruct   ApHdlrMsg;

    UNUSED_PARAM (pArg);

    RFMGMT_FN_ENTRY ();

    MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WssMsgStruct, 0, sizeof (tRfMgmtMsgStruct));

    pRfMgmtNextAPConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB);

    pRfMgmtNextClientConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB);

    while (pRfMgmtNextAPConfigDB != NULL)
    {
        pRfMgmtAPConfigDB = pRfMgmtNextAPConfigDB;

        u2Period = pRfMgmtAPConfigDB->u2NeighborMsgPeriod;
        /* Get the current time */
        u4CurrentTime = OsixGetSysUpTime ();
        u4CurrentTime = (u4CurrentTime -
                         pRfMgmtAPConfigDB->u4NeighMsgUpdatedTime);

        if (u2Period <= u4CurrentTime)
        {
            /* 1. Get ieee 802.11, 2. Radio stats from radio if db */

            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pRfMgmtNextAPConfigDB->u4RadioIfIndex;
            /* GETTING CONFIGURED RADIO TYPE for the particular Radio from RADIO db */
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Radiotype get failed\r\n");
                pRfMgmtAPConfigDB->u4NeighMsgUpdatedTime = OsixGetSysUpTime ();
                pRfMgmtNextAPConfigDB =
                    RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                   RfMgmtNeighborAPConfigDB,
                                   (tRBElem *) pRfMgmtAPConfigDB, NULL);
                continue;
            }
            if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Binding Not Done yet\r\n");
                pRfMgmtAPConfigDB->u4NeighMsgUpdatedTime = OsixGetSysUpTime ();
                pRfMgmtNextAPConfigDB =
                    RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                   RfMgmtNeighborAPConfigDB,
                                   (tRBElem *) pRfMgmtAPConfigDB, NULL);
                continue;
            }
            ApHdlrMsg.ApHdlrQueueReq.u4MsgType = APHDLR_NEIGHBOUR_AP_CTRL_MSG;
            ApHdlrMsg.ApHdlrQueueReq.pRcvBuf = NULL;

            if (WssIfProcessApHdlrMsg
                (WSS_APHDLR_RF_WTP_EVENT_REQ, &ApHdlrMsg) == OSIX_FAILURE)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Failed to send the packet "
                            "to the Ap Hdlr Module \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                              gu4RfmSysLogId,
                              "Failed to send "
                              "the packet to the Ap Hdlr Module"));
            }

            if (pRfMgmtNextClientConfigDB != NULL)
            {
                ApHdlrMsg.ApHdlrQueueReq.u4MsgType =
                    APHDLR_CLIENT_SCAN_CTRL_MSG;
                ApHdlrMsg.ApHdlrQueueReq.pRcvBuf = NULL;

                if (WssIfProcessApHdlrMsg
                    (WSS_APHDLR_RF_WTP_EVENT_REQ, &ApHdlrMsg) == OSIX_FAILURE)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "Failed to send the packet "
                                "to the Ap Hdlr Module \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                  gu4RfmSysLogId,
                                  "Failed to send "
                                  "the packet to the Ap Hdlr Module"));
                }
            }

            pRfMgmtAPConfigDB->u4NeighMsgUpdatedTime = OsixGetSysUpTime ();
        }
        break;                    /* Break is added, because the timer is global for an ap */
    }

    /* Start the timer */
    RfMgmtWtpTmrStart (u4RadioIfIndex,
                       RFMGMT_TX_MESSAGE_EXP_TMR, RFMGMT_TX_MSG_PERIOD);

    RFMGMT_FN_EXIT ();

    return;
}

/*****************************************************************************
 * Function                  : RfMgmtClientScanTmrExp                        *
 *                                                                           *
 * Description               : This routine handles the Retransmit Timer     *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtClientScanTmrExp (VOID *pArg)
{
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Period = 0;
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;
    tRfMgmtClientConfigDB *pRfMgmtNextClientConfigDB = NULL;

    UNUSED_PARAM (pArg);

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    pRfMgmtNextClientConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB);

    while (pRfMgmtNextClientConfigDB != NULL)
    {
        pRfMgmtClientConfigDB = pRfMgmtNextClientConfigDB;

        if (pRfMgmtClientConfigDB->u1SNRScanStatus == RFMGMT_SNR_SCAN_ENABLE)
        {
            u4Period = (UINT4) pRfMgmtClientConfigDB->u2SNRScanPeriod;

            /* Get the current time */
            u4CurrentTime = OsixGetSysUpTime ();
            u4CurrentTime = (u4CurrentTime -
                             pRfMgmtClientConfigDB->u4ScanPeriodUpdatedTime);

            if (u4Period <= u4CurrentTime)
            {
                WssStaWtpShowClient ();
                if (gu1WtpClientWalkIndex != 0)
                {
                    RfMgmtStartClientSNRScan (pRfMgmtClientConfigDB);
                    pRfMgmtClientConfigDB->u4ScanPeriodUpdatedTime =
                        OsixGetSysUpTime ();
                }
            }
        }

        pRfMgmtNextClientConfigDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                           (tRBElem *) pRfMgmtClientConfigDB, NULL);
    }

    /* Start the timer */
    RfMgmtWtpTmrStart (u4RadioIfIndex,
                       RFMGMT_CLIENT_SNR_SCAN_TMR, RFMGMT_CLIENT_SCAN_PERIOD);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     :  RfMgmtNeighScanExpHandler                                 *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtNeighScanExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    RFMGMT_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gRfMgmtTmrList.RfMgmtTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        RFMGMT_TRC1 (RFMGMT_MGMT_TRC, "Timer to be processed %d\r\n",
                     u1TimerId);

        if (u1TimerId < RFMGMT_MAX_TMR)

        {
            i2Offset = gRfMgmtTmrList.aRfMgmtTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter. */
                (*(gRfMgmtTmrList.aRfMgmtTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gRfMgmtTmrList.aRfMgmtTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtWtpTmrStart                                          *
 *                                                                           *
 * Description  : This function used to start the rfmgmt state machiune      *
 *                specified timers.                                          *
 *                                                                           *
 * Input        : pSessEntry - pointer to session entry table                *
 *                u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
RfMgmtWtpTmrStart (UINT4 u4RadioIfIndex, UINT1 u1TmrType, UINT4 u4TmrInterval)
{
    UNUSED_PARAM (u4RadioIfIndex);

    RFMGMT_FN_ENTRY ();
    switch (u1TmrType)
    {
        case RFMGMT_NEIGH_SCAN_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtNeighAPTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            break;
        case RFMGMT_CHAN_SCAN_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtChannelScanTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));

                return OSIX_FAILURE;
            }
        }
            break;
        case RFMGMT_TX_MESSAGE_EXP_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtTxMsgExpTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            break;
        case RFMGMT_11H_TPC_REQUEST_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmt11hTPCRequestTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            break;
        case RFMGMT_CHAN_SWIT_ANNOUNCE_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtChannelSwitTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start Channel Switch Annoncement Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start Channel Switch Annoncement Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            break;

        case RFMGMT_CLIENT_SNR_SCAN_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtClientTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            break;
        case RFMGMT_CAC_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtCacTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start CAC Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start CAC Timer Type \n"));
                return OSIX_FAILURE;
            }
        }
            break;
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtTmrStart:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtTmrStart:Invalid Timer type FAILED !!!"));
            return OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : RfMgmtWtpTmrStop                              *
 *                                                                           *
 * Description               : This routine stops the given discovery timer  *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to stop     *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
RfMgmtWtpTmrStop (UINT1 u1TmrType)
{
    INT4                i4RetVal = OSIX_FAILURE;

    RFMGMT_FN_ENTRY ();

    switch (u1TmrType)
    {
        case RFMGMT_NEIGH_SCAN_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtNeighAPTmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtTmrStop: "
                            "Failure to stop DCA interval timer\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop DCA interval timer"));
            }

            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_CHAN_SCAN_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtChannelScanTmrBlk))
                != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtTmrStop: "
                            "Failure to stop TPC interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop TPC interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_TX_MESSAGE_EXP_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtTxMsgExpTmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtTmrStop: "
                            "Failure to stop TPC interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop TPC interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_CLIENT_SNR_SCAN_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtClientTmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtTmrStop: "
                            "Failure to stop TPC interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop TPC interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_CHAN_SWIT_ANNOUNCE_TMR:
        {
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtChannelSwitTmrBlk)) ==
                TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStop: "
                             "Failed to stop CSA Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStop: "
                              "Failed to stop CSA Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            break;

        case RFMGMT_11H_TPC_REQUEST_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmt11hTPCRequestTmrBlk)) !=
                TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtTmrStop: "
                            "Failure to stop TPC request interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop TPC request interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_CAC_TMR:
        {
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtCacTmrBlk)) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to stop CAC Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to stop CAC Timer Type\n"));
                return OSIX_FAILURE;
            }
        }
            i4RetVal = OSIX_SUCCESS;
            break;
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtTmrStop:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtTmrStop:Invalid Timer type FAILED !!!"));
            return OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************
 * Function                  : RfMgmtTpcRequestTmrExp                        *
 *                                                                           *
 * Description               : This routine handles the Tpc Request Timer    *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtTpcRequestTmrExp (VOID *pArg)
{
    tRfMgmtAPConfigDB  *pRfMgmtNextAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRadioIfGetDB       RadioIfDB;
    tRfMgmtDB           RfMgmtDB;
    unWssMsgStructs     WssMsgStruct;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Period = 0;
    UINT1               u1RadioId = 0;

    MEMSET (&WssMsgStruct, 0, sizeof (unWssMsgStructs));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    UNUSED_PARAM (pArg);

    RFMGMT_FN_ENTRY ();

    pRfMgmtNextAPConfigDB = RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                            RfMgmtNeighborAPConfigDB);
    while (pRfMgmtNextAPConfigDB != NULL)
    {
        pRfMgmtAPConfigDB = pRfMgmtNextAPConfigDB;
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u4RadioIfIndex = pRfMgmtNextAPConfigDB->u4RadioIfIndex;

        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
            bRfMgmt11hTpcStatus = OSIX_TRUE;

        if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY,
                                &RfMgmtDB) == RFMGMT_SUCCESS)
        {
            if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u1RfMgmt11hTpcStatus == RFMGMT_11H_TPC_ENABLE)
            {

                RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pRfMgmtAPConfigDB->u4RadioIfIndex;
                /*Get the radio type from Radio DB */
                RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfDB) != OSIX_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                " RfMgmtTpcRequestTmrExp : Radio type get failed\r\n");
                    pRfMgmtNextAPConfigDB =
                        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                       RfMgmtNeighborAPConfigDB,
                                       (tRBElem *) pRfMgmtAPConfigDB, NULL);
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  " RfMgmtTpcRequestTmrExp : Radio type get failed\n"));
                    continue;
                }

                if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEA) ||
                    (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEAN) ||
                    (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEAC))
                {
                    /* Get the current time */
                    u4CurrentTime = OsixGetSysUpTime ();
                    u4CurrentTime = (u4CurrentTime - pRfMgmtAPConfigDB->
                                     u4TpcReqLastSentTime);

                    u4Period =
                        pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval /
                        RFMGMT_TPC_REQUEST_SPLIT_PERIOD;

                    if (u4CurrentTime > u4Period)
                    {
                        u1RadioId = RadioIfDB.RadioIfGetAllDB.u1RadioId;

                        WssMsgStruct.WssSta11hTpcMsg.u1RadioId = u1RadioId;
                        pRfMgmtAPConfigDB->u1TpcRequestSlot++;
                        WssMsgStruct.WssSta11hTpcMsg.u1TpcRequestSlot
                            = pRfMgmtAPConfigDB->u1TpcRequestSlot;

                        WssStaWtpProcessWssIfMsg (WSS_CONSTRUCT_TPC_REQUEST,
                                                  &WssMsgStruct);
                        if (pRfMgmtAPConfigDB->u1TpcRequestSlot ==
                            RFMGMT_TPC_REQUEST_SPLIT_PERIOD - 1)
                        {
                            pRfMgmtAPConfigDB->u1TpcRequestSlot = 0;
                        }
                        pRfMgmtAPConfigDB->u4TpcReqLastSentTime =
                            OsixGetSysUpTime ();
                    }
                }
            }
        }
        pRfMgmtNextAPConfigDB = RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                               RfMgmtNeighborAPConfigDB,
                                               (tRBElem *) pRfMgmtAPConfigDB,
                                               NULL);
    }
    /* Start the timer */
    RfMgmtWtpTmrStart (u4RadioIfIndex,
                       RFMGMT_11H_TPC_REQUEST_TMR, RFMGMT_TPC_REQUEST_PERIOD);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmtCacTmrExp                               *
 *                                                                           *
 * Description               : This routine handles the CAC Timer            *
 Expiry.                                       *
 *                                                                           *
 * Input                     : pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *****************************************************************************/
VOID
RfMgmtCacTmrExp (VOID *pArg)
{
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtNextAPConfigDB = NULL;

    RFMGMT_FN_ENTRY ();

    UNUSED_PARAM (pArg);
    pRfMgmtNextAPConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB);
    while (pRfMgmtNextAPConfigDB != NULL)
    {
        pRfMgmtAPConfigDB = pRfMgmtNextAPConfigDB;
        RfMgmtPerformCAC (pRfMgmtAPConfigDB);
        pRfMgmtNextAPConfigDB = RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                               RfMgmtNeighborAPConfigDB,
                                               (tRBElem *) pRfMgmtAPConfigDB,
                                               NULL);
    }
    RFMGMT_FN_EXIT ();
    return;
}

#endif
