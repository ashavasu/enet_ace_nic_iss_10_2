/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmsnmp.c,v 1.3 2017/11/24 10:37:05 siva Exp $ rfmsnmp.c,v 1.0
 *
 * Description: This file contains the rfmgmt trap generation
 *              related code.
 *                            
 ********************************************************************/

#ifndef __RFMSNMP_C__
#define __RFMSNMP_C__

#include "rfminc.h"
#include "fsrrm.h"
#include "snmputil.h"

/****************************************************************************
 *                                                                          *
 * Function     : RfmSnmpifSendTrapInCxt                                    *
 *                                                                          *
 * Description  : Routine to send trap to SNMP Agent.                       *
 *                                                                          *
 * Input        : u1TrapId: Trap identifier                                 *
 *                p_trap_info : Pointer to structure having the trap info.  *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
PUBLIC VOID
RfmSnmpifSendTrapInCxt (UINT1 u1TrapId, VOID *pTrapInfo)
{
#if ((defined(FUTURE_SNMP_WANTED))||(defined(SNMP_3_WANTED))||\
                 (defined(SNMPV3_WANTED)))
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4RfmTrapOid[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 84, 6 };
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[RFMGMT_BUFFER_SIZE];
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tRfMgmtTrapInfo    *pRfMgmtTrapInfo;
    tSNMP_OCTET_STRING_TYPE *pMacString = NULL;

    pRfMgmtTrapInfo = (tRfMgmtTrapInfo *) pTrapInfo;

    RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "Trap to be generated: %d\n", u1TrapId);
    /* Allocate the Memory for RFMGMT Trap OID to form the VbList */
    pEnterpriseOid = alloc_oid (RFM_SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    MEMCPY (pEnterpriseOid->pu4_OidList, au4RfmTrapOid, sizeof (au4RfmTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4RfmTrapOid) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;
    pSnmpTrapOid = alloc_oid (RFM_SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));

    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {
        case RFMGMT_CHANNEL_CHANGE_TRAP:
            if (RFMGMT_CHANNEL_TRAP_STATUS != RFMGMT_CHANNEL_CHANGE_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            SPRINTF ((CHR1 *) au1Buf, "fsRrmTraps");
            if ((pOid = RfmMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pRfMgmtTrapInfo->
                                                         u2Channel, NULL, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case RFMGMT_TXPOWER_CHANGE_TRAP:
            if (RFMGMT_TXPOWER_TRAP_STATUS !=
                RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            SPRINTF ((CHR1 *) au1Buf, "fsRrmTraps");
            if ((pOid = RfmMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         pRfMgmtTrapInfo->
                                                         u2TxPowerLevel, NULL,
                                                         NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;
#ifdef ROGUEAP_WANTED
        case RFMGMT_ROGUE_STATUS_TRAP:
            if (RFMGMT_ROGUE_TRAP_STATUS != RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            SPRINTF ((CHR1 *) au1Buf, "fsRrmTraps");
            if ((pOid = RfmMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pMacString =
                SNMP_AGT_FormOctetString ((UINT1 *) pRfMgmtTrapInfo->
                                          au1RogueApBSSID,
                                          (INT4) (MAC_ADDR_LEN));
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pMacString,
                                                         NULL,
                                                         (tSNMP_COUNTER64_TYPE)
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;
#endif
        default:
            break;
    }
#ifdef SNMP_2_WANTED
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
#endif
#else

    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pTrapInfo);

#endif /* FUTURE_SNMP_WANTED */

#ifdef DEBUG_WANTED
    DbgNotifyTrap (u1TrapId, pTrapInfo);
#endif
    UNUSED_PARAM (pMacString);
}

/****************************************************************************
 *                                                                          *
 * Function     : RfmUtilParseSubIdNew                                      *
 *                                                                          *
 * Description  : returns the numeric value of ppu1TempPtr                  *
 *                                                                          *
 * Input        : **ppu1TempPtr                                             *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : value of ppu1TempPtr or INVALID                           *
 *                                                                          *
 ****************************************************************************/

INT4
RfmUtilParseSubIdNew (UINT1 **ppu1TempPtr)
{
    INT4                i4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        i4Value = (i4Value * RFMGMT_NUM_TEN) +
            (*pu1Tmp & RFMGMT_MAX_HEX_SINGLE_DIGIT);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4Value = 0;
    }
    *ppu1TempPtr = pu1Tmp;
    return (i4Value);
}

/****************************************************************************
 *                                                                          *
 * Function     : RfmMakeObjIdFromDotNew                                    *
 *                                                                          *
 * Description  : returns the numeric value of ppu1TempPtr                  *
 *                                                                          *
 * Input        : **ppu1TempPtr                                             *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : value of ppu1TempPtr or INVALID                           *
 *                                                                          *
 ****************************************************************************/
#define   RFMGMT_SNMP_TEMP_BUFF   280
INT1                gai1tempBuffer[RFMGMT_SNMP_TEMP_BUFF];
tSNMP_OID_TYPE     *
RfmMakeObjIdFromDotNew (INT1 *textStr)
{
    tSNMP_OID_TYPE     *poidPtr = NULL;
    INT1               *pi1tempPtr = NULL, *pi1dotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2dotCount = 0;
    UINT2               u2TmpVar = 0;
    UINT2               u2TmpVar1 = 0;
    UINT2               u2TmpVar2 = 0;
    UINT1              *pu1TmpPtr = NULL;

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*textStr) != 0)
    {
        pi1dotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (pi1dotPtr == NULL)
        {
            pi1dotPtr = textStr + STRLEN ((INT1 *) textStr);
        }
        pi1tempPtr = textStr;

        for (u2Index = 0; ((pi1tempPtr < pi1dotPtr) &&
                           (u2Index < RFMGMT_BUFFER_SIZE)); u2Index++)
        {
            if (u2Index < RFMGMT_SNMP_TEMP_BUFF)
            {
                gai1tempBuffer[u2Index] = *pi1tempPtr++;
            }
        }
        if (u2Index < RFMGMT_SNMP_TEMP_BUFF)
        {
            gai1tempBuffer[u2Index] = '\0';
        }

        u2TmpVar = (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID));
        for (u2Index = 0;
             ((u2Index < u2TmpVar) && (orig_mib_oid_table[u2Index].pName
                                       != NULL)); u2Index++)
        {
            u2TmpVar1 = ((STRLEN ((INT1 *) gai1tempBuffer)) ==
                         (STRLEN (orig_mib_oid_table[u2Index].pName)));
            u2TmpVar2 = (STRCMP (orig_mib_oid_table[u2Index].pName,
                                 (INT1 *) gai1tempBuffer) == 0);
            if ((u2TmpVar2) && (u2TmpVar1))
            {
                STRNCPY ((INT1 *) gai1tempBuffer,
                         orig_mib_oid_table[u2Index].pNumber,
                         MEM_MAX_BYTES (STRLEN
                                        (orig_mib_oid_table[u2Index].pNumber),
                                        sizeof (gai1tempBuffer)));
                break;
            }
        }

        if ((u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
            && (orig_mib_oid_table[u2Index].pName == NULL))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) gai1tempBuffer, (INT1 *) pi1dotPtr,
                 MEM_MAX_BYTES (STRLEN ((INT1 *) pi1dotPtr),
                                (sizeof (gai1tempBuffer) -
                                 STRLEN (gai1tempBuffer))));
    }
    else
    {                            /* is not alpha, so just copy into gai1tempBuffer */
        STRCPY ((INT1 *) gai1tempBuffer, (INT1 *) textStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2dotCount = 0;
    for (u2Index = 0; ((u2Index < RFMGMT_SNMP_TEMP_BUFF) &&
                       (gai1tempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (gai1tempBuffer[u2Index] == '.')
        {
            u2dotCount++;
        }
    }
    if ((poidPtr = alloc_oid ((INT4) (u2dotCount + 1))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) gai1tempBuffer;
    for (u2Index = 0; u2Index < u2dotCount + 1; u2Index++)
    {
        if ((poidPtr->pu4_OidList[u2Index] =
             ((UINT4) (RfmUtilParseSubIdNew (&pu1TmpPtr)))) == (UINT4) -1)
        {
            free_oid (poidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (poidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (poidPtr);
}
#endif
