
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfacmain.c,v 1.7 2018/01/04 09:54:33 siva Exp $
 *
 * Description: This file contains the rfmgmt task main loop
 *              and the initialisation routines for WLC.
 *                            
 ********************************************************************/

#ifndef __RFACMAIN_C__
#define __RFACMAIN_C__

#include "rfminc.h"
#include "radioifproto.h"
#include "rfmproto.h"
#ifdef ROGUEAP_WANTED
extern UINT1        RfMgmtResetRogueApClassifiedBy (void);
#endif
FS_ULONG            gaChannelOverlapPercentage[RFMGMT_MAX_CHANNELB + 1]
    [RFMGMT_MAX_CHANNELB + 1] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    ,
    {0, 100, 77, 55, 32, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    ,
    {0, 77, 100, 77, 55, 32, 9, 0, 0, 0, 0, 0, 0, 0, 0}
    ,
    {0, 55, 77, 100, 77, 55, 32, 9, 0, 0, 0, 0, 0, 0, 0}
    ,
    {0, 32, 55, 77, 100, 77, 55, 32, 9, 0, 0, 0, 0, 0, 0}
    ,
    {0, 9, 32, 55, 77, 100, 77, 55, 32, 9, 0, 0, 0, 0, 0}
    ,
    {0, 0, 9, 32, 55, 77, 100, 77, 55, 32, 9, 0, 0, 0, 0}
    ,
    {0, 0, 0, 9, 32, 55, 77, 100, 77, 55, 32, 9, 0, 0, 0}
    ,
    {0, 0, 0, 0, 9, 32, 55, 77, 100, 77, 55, 32, 9, 0, 0}
    ,
    {0, 0, 0, 0, 0, 9, 32, 55, 77, 100, 77, 55, 32, 9, 0}
    ,
    {0, 0, 0, 0, 0, 0, 9, 32, 55, 77, 100, 77, 55, 32, 9}
    ,
    {0, 0, 0, 0, 0, 0, 0, 9, 32, 55, 77, 100, 77, 55, 32}
    ,
    {0, 0, 0, 0, 0, 0, 0, 0, 9, 32, 55, 77, 100, 77, 55}
    ,
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 32, 55, 77, 100, 77}
    ,
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 32, 55, 77, 100}
};

UINT1               gu1OverLapPercent;
UINT2               gau2APChannels[RFMGMT_MAX_RADIOS];
UINT2               gau2ApNewColors[RFMGMT_MAX_RADIOS +
                                    MAX_RFMGMT_EXTERNAL_AP_SIZE];
UINT2               gau2ExternalApColors[MAX_RFMGMT_EXTERNAL_AP_SIZE];
UINT2               gau2ColorsToUse[RFMGMT_MAX_COLORS];
UINT1               gau2NoExternalAp[RFMGMT_MAX_CHANNEL_TYPES];
UINT4               gu4FailedToComDCAAlog = 0;
extern INT1         nmhGetFirstIndexCapwapBaseWtpProfileTable (UINT4 *);
extern INT1         nmhGetNextIndexCapwapBaseWtpProfileTable (UINT4, UINT4 *);

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWlcMainTask                                         *
 *                                                                          *
 * Description  : Main function of RFMGMT task.                             *
 *                                                                          *
 * Input        : pi1Arg                                                    *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtWlcMainTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;
    RFMGMT_FN_ENTRY ();

    UNUSED_PARAM (pi1Arg);

    if (RfMgmtWlcMainTaskInit () == OSIX_FAILURE)
    {
        RfMgmtWlcMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        OsixReceiveEvent (RFMGMT_QUE_MSG_EVENT | RFMGMT_TIMER_EXP_EVENT,
                          OSIX_WAIT, (UINT4) 0, &u4Events);
        RFMGMT_LOCK;
        if (u4Events & RFMGMT_QUE_MSG_EVENT)
        {
            RFMGMT_TRC (CONTROL_PLANE_TRC,
                        "RfMgmtMainProcessEvent: Recvd Q Msg Event\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtMainProcessEvent: Recvd Q Msg Event"));
            RfMgmtWlcQueMsgHandler ();
        }

        if (u4Events & RFMGMT_TIMER_EXP_EVENT)
        {
            RFMGMT_TRC (CONTROL_PLANE_TRC,
                        "RfMgmtMainProcessEvent: Recvd DCA interval exp Event\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtMainProcessEvent: Recvd DCA interval exp Event"));
            RfMgmtDcaIntExpHandler ();
        }

        RFMGMT_UNLOCK;
    }
    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtWlcMainTaskInit                                      */
/*                                                                           */
/*                                                                           */
/* Description  : RFMGMT task initialization routine. This function creates  */
/*                the semaphore, task and queues                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtWlcMainTaskInit (VOID)
{
    MEMSET (&gRfMgmtGlobals, 0, sizeof (tRfMgmtGlobals));
    RFMGMT_FN_ENTRY ();

    /* Initialize the global variables */
    RfMgmtInitGlobals ();

    /* RFMGMT Semaphore */
    if (OsixCreateSem (RFMGMT_WLC_SEM_NAME, RFMGMT_SEM_CREATE_INIT_CNT, 0,
                       &gRfMgmtGlobals.SemId) == OSIX_FAILURE)
    {
        RFMGMT_TRC (OS_RESOURCE_TRC | RFMGMT_CRITICAL_TRC,
                    "RfMgmtMainTaskInit: RFMGMT Semaphore creation FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4RfmSysLogId,
                      "RfMgmtMainTaskInit: RFMGMT Semaphore creation FAILED!!!\n"));
        return RFMGMT_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. 
     * So GiveSem is called before using the semaphore. */
    OsixSemGive (gRfMgmtGlobals.SemId);

    /* Create Task */
    if (OsixGetTaskId (SELF, RFMGMT_WLC_TASK_NAME,
                       &(gRfMgmtGlobals.rfMgmtTaskId)) == OSIX_FAILURE)
    {
        RFMGMT_TRC (OS_RESOURCE_TRC | RFMGMT_CRITICAL_TRC,
                    "RfMgmtMainTaskInit: RFMGMT Task Creation FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4RfmSysLogId,
                      "RfMgmtMainTaskInit: RFMGMT Task Creation FAILED!!!\n"));
        return RFMGMT_FAILURE;
    }

    /* Create queue */
    if (OsixCreateQ (RFMGMT_WLC_QUE_NAME, RFMGMT_WLC_QUEUE_DEPTH, 0,
                     &(gRfMgmtGlobals.rfMgmtQueId)) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtWlcMainTask: Queue Creation failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtWlcMainTask: Queue Creation failed\n"));
        lrInitComplete (OSIX_FAILURE);
        return RFMGMT_FAILURE;
    }

    /* Registering with SYSLOG */
    gu4RfmSysLogId = (UINT4) SYS_LOG_REGISTER ((UINT1 *) RFMGMT_WLC_SEM_NAME,
                                               SYSLOG_CRITICAL_LEVEL);

    /* Invoke the below function to create the mem pool and create the 
     * a related operations */
    if (RfMgmtWlcModuleStart () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (OS_RESOURCE_TRC | RFMGMT_CRITICAL_TRC,
                    "RfMgmtMainTaskInit: RFMGMT Module Start FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4RfmSysLogId,
                      "RfMgmtMainTaskInit: RFMGMT Module Start FAILED!!!\n"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWlcMainTaskDeInit                                      *
 *                                                                          *
 * Description  : This function de-inits global data structures of          *
 *                Beacon Manager.                                           *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtWlcMainTaskDeInit (VOID)
{
    RFMGMT_FN_ENTRY ();
    RfMgmtWlcModuleShutDown ();

    if (gRfMgmtGlobals.rfMgmtQueId != 0)
    {
        OsixQueDel (gRfMgmtGlobals.rfMgmtQueId);
        gRfMgmtGlobals.rfMgmtQueId = 0;
    }

    if (gRfMgmtGlobals.SemId != 0)
    {
        OsixSemDel (gRfMgmtGlobals.SemId);
        gRfMgmtGlobals.SemId = 0;
    }
    SYS_LOG_DEREGISTER (gu4RfmSysLogId);
    RFMGMT_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWlcModuleStart                                      *
 *                                                                          *
 * Description  : This function allocates memory pools and inits global     *
 *                data structures of RFMGMT        .                        *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                          *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtWlcModuleStart (VOID)
{
    RFMGMT_FN_ENTRY ();
    /* Initialize Database Mempools */
    if (RfmgmtacSizingMemCreateMemPools () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleStart: Memory Initialization FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleStart: Memory Initialization FAILED!!!\n"));
        RfmgmtacSizingMemDeleteMemPools ();
        return RFMGMT_FAILURE;
    }

    /* Initialize Database Mempools */
    if (RfmgmtSizingMemCreateMemPools () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleStart: Memory Initialization FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleStart: Memory Initialization FAILED!!!\n"));

        RfmgmtacSizingMemDeleteMemPools ();
        RfmgmtSizingMemDeleteMemPools ();
        return RFMGMT_FAILURE;
    }

    /* Timer Initialization */
    if (RfMgmtWlcTmrInit () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleStart: Timer Initialization FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleStart: Timer Initialization FAILED!!!\n"));

        RfmgmtacSizingMemDeleteMemPools ();
        RfmgmtSizingMemDeleteMemPools ();

        return RFMGMT_FAILURE;
    }

    /* RFMGMT manager Initialization */
    if (RfMgmtWlcCreateRBTree () == RFMGMT_FAILURE)
    {
        RfmgmtacSizingMemDeleteMemPools ();
        RfmgmtSizingMemDeleteMemPools ();

        return RFMGMT_FAILURE;
    }

    RegisterRFMGMT ();

    gRfMgmtGlobals.u1RfmgmtTaskInitialized = OSIX_TRUE;

    /* Start the DCA Timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_DCA_INTERVAL_TMR, RFMGMT_DEF_DCA_TIMER_INTERVAL);
    /* Start the TPC Timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_TPC_INTERVAL_TMR, RFMGMT_DEF_TPC_TIMER_INTERVAL);

    /* Start the SHA Timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_SHA_INTERVAL_TMR, RFMGMT_DEF_SHA_TIMER_INTERVAL);

    /* Start the 11h TPC Timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_11H_TPC_INTERVAL_TMR,
                       RFMGMT_DEF_11H_TPC_TIMER_INTERVAL);

    RfMgmtWlcTmrStart (0,
                       RFMGMT_11H_DFS_INTERVAL_TMR,
                       RFMGMT_DEF_11H_DFS_TIMER_INTERVAL);
#ifdef ROGUEAP_WANTED
    /* Start the Rogue Ap Timeout Timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR,
                       RFMGMT_DEF_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR);
#endif

    RFMGMT_FN_EXIT ();

    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWlcCreateRBTree                                     *
 *
 * Description  : This function creates all the RBTree required.            * 
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                          *
 *                                                                          *
 ****************************************************************************/
PUBLIC INT4
RfMgmtWlcCreateRBTree (VOID)
{
    if (RfMgmtAutoRfProfileDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtAPConfigDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtNeighborScanDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtClientConfigDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtClientScanDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtBssidScanDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtDot11hTpcInfoDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtFailedAPNeighborListDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
#ifdef ROGUEAP_WANTED
    if (RfMgmtRogueManualDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtRogueRuleDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtRogueApDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
#endif
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtInitGlobals                                         *
 *
 * Description  : This function initis all Globals.                         * 
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtInitGlobals (VOID)
{
    MEMSET (&gRfMgmtGlobals, 0, sizeof (tRfMgmtGlobals));
    MEMSET (gau2ApNewColors, 0,
            ((RFMGMT_MAX_RADIOS +
              MAX_RFMGMT_EXTERNAL_AP_SIZE) * sizeof (UINT2)));
    MEMSET (gau2ExternalApColors, 0,
            ((MAX_RFMGMT_EXTERNAL_AP_SIZE) * sizeof (UINT2)));
    MEMSET (gau2APChannels, 0, (RFMGMT_MAX_RADIOS * sizeof (UINT2)));
    MEMSET (gau2ColorsToUse, 0, (RFMGMT_MAX_COLORS * sizeof (UINT2)));

    /* Set Channel Switch Status */
    gu4ChannelSwitchMsgStatus = RFMGMT_CHANNEL_SWITCH_MSG_ENABLE;
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWlcModuleShutDown                                      *
 *                                                                          *
 * Description  : This function shuts down the RFMGMT module.               *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtWlcModuleShutDown (VOID)
{
    tRfMgmtQMsg        *pRfMgmtMsg = NULL;

    RFMGMT_FN_ENTRY ();
    RFMGMT_LOCK;

    if (gRfMgmtGlobals.u1RfmgmtTaskInitialized == OSIX_FALSE)
    {
        RFMGMT_UNLOCK;
        return;
    }

    gRfMgmtGlobals.u1RfmgmtTaskInitialized = OSIX_FALSE;

    while (OsixQueRecv (gRfMgmtGlobals.rfMgmtQueId, (UINT1 *) &pRfMgmtMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pRfMgmtMsg == NULL)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Received pointer is NULL\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Received pointer is NULL\n"));
            RFMGMT_UNLOCK;
            return;
        }

    }

    /* Deinitialize the timer function */
    if (RfMgmtWlcTmrDeInit () == OSIX_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleShutDown: Timer DeInit FAILED !!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleShutDown: Timer DeInit FAILED !!!\n"));
    }

    /* Release the allocated memory and delete the mem pools created */
    RfMgmtMainReleaseQMemory (pRfMgmtMsg);

    RfmgmtacSizingMemDeleteMemPools ();
    RfmgmtSizingMemDeleteMemPools ();

    RFMGMT_UNLOCK;

    RFMGMT_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtMainReleaseQMemory                                  *
 *                                                                          *
 * Description  : This function releases the Q memory pools.                *
 *                                                                          *
 * Input        : pRfMgmtMsg - Queue Memory                                 *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtMainReleaseQMemory (tRfMgmtQMsg * pRfMgmtMsg)
{
    RFMGMT_FN_ENTRY ();
    if (pRfMgmtMsg != NULL)
    {
        /* Release the CRU buffer of the Beacon PDU enqueued by WSS MAC */
        CRU_BUF_Release_MsgBufChain
            (pRfMgmtMsg->unMsgParam.RfMgmtQueueReq.pRcvBuf, FALSE);
    }

    MemReleaseMemBlock (RFMGMT_QUEUE_POOLID, (UINT1 *) pRfMgmtMsg);
    RFMGMT_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtLock                                                *
 *                                                                          *
 * Description  : This function is to take RfMgmt mutex semaphore.          *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtLock (VOID)
{
    RFMGMT_FN_ENTRY ();
    if (OsixSemTake (gRfMgmtGlobals.SemId) == OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtUnlock                                              *
 *                                                                          *
 * Description  : This function is to release RfMgmt mutex semaphore.       *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtUnLock (VOID)
{
    RFMGMT_FN_ENTRY ();

    if (OsixSemGive (gRfMgmtGlobals.SemId) == OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();

    return RFMGMT_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : RfMgmtWlcQueMsgHandler                                  *
 *                                                                         *
 * DESCRIPTION      : Function is used to process the RFMGMT Q msgs.       *
 *                                                                         *
 * INPUT            : NONE                                                 *
 *                                                                         *
 * OUTPUT           : NONE                                                 *
 *                                                                         *
 * RETURNS          : NONE                                                 *
 *                                                                         *
 ***************************************************************************/
VOID
RfMgmtWlcQueMsgHandler (VOID)
{
    tRfMgmtQueueReq    *pMsg = NULL;

    RFMGMT_FN_ENTRY ();

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gRfMgmtGlobals.rfMgmtQueId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pMsg == NULL)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Received pointer is NULL\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Received pointer is NULL\n"));
            return;
        }
        switch (pMsg->u4MsgType)
        {
            default:
            {
                RFMGMT_TRC (CONTROL_PLANE_TRC,
                            "RfMgmtQueMsgHandler: Unknown message type"
                            "received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtQueMsgHandler: Unknown message type"
                              "received\n"));
            }
                break;
        }

        /* Release the buffer to pool */
        MemReleaseMemBlock (RFMGMT_QUEUE_POOLID, (UINT1 *) pMsg);

    }

    RFMGMT_FN_EXIT ();

    return;
}

/*****************************************************************************/
/* Function Name      : RfMgmtHandleRadarEventInfo                           */
/*                                                                           */
/* Description        : This function handles the radar event                */
/*                                                                           */
/* Input(s)           : pWssMsgStruct - Pointer to the input structure       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RFMGMT_SUCCESS or RFMGMT_FAILURE                     */
/*****************************************************************************/
UINT1
RfMgmtHandleRadarEventInfo (tRfMgmtMsgStruct * pRfMgmtMsgStruct)
{

    UINT1               u1Index = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtUpdateDB;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.u2WtpInternalId;
    pWssIfCapwapDB->CapwapGetDB.u1RadioId =
        pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.
        DFSRadarEventInfo.u1RadioId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: Wss Capwap "
                    "DB get failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: Get Rfmgmt "
                    "AP config entry failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    for (u1Index = 0; u1Index < RADIOIF_MAX_CHANNEL_A; u1Index++)
    {
        if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            DFSChannelInfo[u1Index].u4ChannelNum ==
            pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.
            DFSRadarEventInfo.u2ChannelNum)
        {

            if (pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                DFSRadarEventInfo.isRadarFound)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelInfo[u1Index].u4DFSFlag =
                    RADIO_CHAN_DFS_UNAVAILABLE;
            }
            else
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelInfo[u1Index].u4DFSFlag =
                    RADIO_CHAN_DFS_AVAILABLE;
            }
            break;
        }
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: Set Rfmgmt "
                    "AP config entry failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEA;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bRowStatus = OSIX_TRUE;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaMode = OSIX_TRUE;
    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaSelection = OSIX_TRUE;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bAllowedChannelList = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtUpdateDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: Get Rfmgmt Auto "
                    "RF entry failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    if (RfMgmtRunDFSAlgorithm (&RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
                               RfMgmtAutoRfProfileDB,
                               &RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                               RfMgmtAPConfigDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: RunDFSAlgo returned failure\r\n");
    }
    if (RfMgmtRunDcaAlgorithm (&RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
                               RfMgmtAutoRfProfileDB, 0) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: RfMgmtRunDcaAlgorithm failed\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bCurrentChannel = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: Getting Rfmgmt "
                    "index failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel =
        (UINT1) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2CurrentChannel;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
    if (RadioIfSetOFDMTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleRadarEventInfo: RadioIfSetOFDMTable "
                    "failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RfMgmtProcessBindingComplete                         */
/*                                                                           */
/* Description        : This function initiates config update request for    */
/*                      auto scan status once binding completes              */
/* Input(s)           : pWssMsgStruct - Pointer to the input structure       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RFMGMT_SUCCESS or RFMGMT_FAILURE                     */
/*****************************************************************************/
UINT1
RfMgmtProcessBindingComplete (tRfMgmtMsgStruct * pRfMgmtMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Input Parameter Check */
    if (pRfMgmtMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessBindingComplete: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessBindingComplete: Null input received"));
        return OSIX_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bAutoScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Fetch from DB  Failed\r\n");
        return RFMGMT_FAILURE;
    }

    if (RfMgmtProcessNeighConfig (&RfMgmtDB) == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Config update request for auto-scan"
                    " status  Failed\r\n");
        return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : fMgmtUpdateMeasReportInfo                            */
/*                                                                           */
/* Description        : This function handles the measreport                 */
/*                                                                           */
/* Input(s)           : pQMsg - Received queue message                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RfMgmtUpdateMeasReportInfo (tRfMgmtMsgStruct * pRfMgmtMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1Index = 0;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

/*Start Updating the MeasInfo into the DFS Table*/

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB.
        u4RfMgmt11hRadioIfIndex = pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.
        MeasRep.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB.
        u1ChannelNum = pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.
        MeasRep.u1ChannelNum;

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB.
        bIsRadarPresent = pRfMgmtMsgStruct->unRfMgmtMsg.
        RfMgmtQueueReq.MeasRep.isRadarPresent;

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB.
            RfMgmt11hStationMacAddress, pRfMgmtMsgStruct->unRfMgmtMsg.
            RfMgmtQueueReq.MeasRep.StationMacAddr, sizeof (tMacAddr));

    /* Check whether entry is already present*
     *      * If it is already present create a entry else update it*/
    if (RfMgmtProcessDBMsg (RFMGMT_GET_DFS_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        /* Create the DB entry */
        if (RfMgmtProcessDBMsg (RFMGMT_CREATE_DFS_INFO_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtUpdateDfsRspInfo : Creating "
                        "DFS info failed\r\n");
            return;
        }
    }
    else
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
            RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hChannelNum = OSIX_TRUE;

        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
            RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hIsRadarPresent = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
            RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hStationMacAddress = OSIX_TRUE;

        if (RfMgmtProcessDBMsg (RFMGMT_SET_DFS_INFO_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtUpdateDfsRspInfo: Updating "
                        "DFS info failed\r\n");
            return;
        }

    }
/*End Updating the MeasInfo into the DFS Table*/

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.MeasRep.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtUpdateMeasReportInfo: Getting Rfmgmt "
                    "index failed\r\n");
        return;
    }

    for (u1Index = 0; u1Index < RADIOIF_MAX_CHANNEL_A; u1Index++)
    {
        if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            DFSChannelInfo[u1Index].u4ChannelNum ==
            pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.MeasRep.u1ChannelNum)
        {

            if (pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.MeasRep.
                isRadarPresent)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelInfo[u1Index].u4DFSFlag =
                    RADIO_CHAN_DFS_UNAVAILABLE;
            }
            else
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelInfo[u1Index].u4DFSFlag =
                    RADIO_CHAN_DFS_AVAILABLE;
            }
            break;
        }
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.MeasRep.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtUpdateMeasReportInfo: Getting Rfmgmt "
                    "index failed\r\n");
        return;
    }
    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : RfMgmtWlcEnquePkts                                   */
/*                                                                           */
/* Description        : This function is to enque received packets           */
/*                      to the RFMGMT task                                   */
/*                                                                           */
/* Input(s)           : pQMsg - Received queue message                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RfMgmtWlcEnquePkts (tRfMgmtMsgStruct * pMsg)
{
    INT4                i4Status = RFMGMT_SUCCESS;
    tRfMgmtQueueReq    *pRfMgmtQueueReq = NULL;

    RFMGMT_FN_ENTRY ();

    if (pMsg == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtWlcEnquePkts: "
                    "invalid  msg structure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtWlcEnquePkts: invalid msg structure\n"));
        return RFMGMT_FAILURE;
    }

    pRfMgmtQueueReq = (tRfMgmtQueueReq *) MemAllocMemBlk (RFMGMT_QUEUE_POOLID);
    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Memory Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Memory Allocation Failed\n"));
        CRU_BUF_Release_MsgBufChain (pMsg->unRfMgmtMsg.RfMgmtQueueReq.pRcvBuf,
                                     FALSE);
        return OSIX_FAILURE;
    }
    pRfMgmtQueueReq->u4MsgType = pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType;
    pRfMgmtQueueReq->pRcvBuf = pMsg->unRfMgmtMsg.RfMgmtQueueReq.pRcvBuf;

    switch (pRfMgmtQueueReq->u4MsgType)
    {
        case RFMGMT_WLC_RECV_NEIGHBOR_MSG:
            break;

        case RFMGMT_WLC_RECV_CLIENT_MSG:
            break;
        case RFMGMT_TPC_RSP_MSG:
            RfMgmtUpdateTpcRspInfo (pMsg);
            break;
        case RFMGMT_MEAS_REPORT_MSG:
            RfMgmtUpdateMeasReportInfo (pMsg);
            break;
        default:
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtWlcEnquePkts: "
                        "unknown msg queue \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtWlcEnquePkts: unknown msg queue\n"));
            break;
        }
    }

    if (OsixQueSend (gRfMgmtGlobals.rfMgmtQueId,
                     (UINT1 *) &pRfMgmtQueueReq,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "Failed to send the message to CTRL Rx queue \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Failed to send the message to CTRL Rx queue\n"));
        CRU_BUF_Release_MsgBufChain (pRfMgmtQueueReq->pRcvBuf, FALSE);
        MemReleaseMemBlock (RFMGMT_QUEUE_POOLID, (UINT1 *) pRfMgmtQueueReq);
        return OSIX_FAILURE;
    }
    /*post event to message queue */
    if (OsixEvtSend (gRfMgmtGlobals.rfMgmtTaskId,
                     RFMGMT_QUE_MSG_EVENT) == OSIX_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtWlcEnquePkts: Failed to send Event \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtWlcEnquePkts: Failed to send Event\n"));
        i4Status = OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();

    return i4Status;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateTpcRspInfo                                     */
/*                                                                           */
/* Description  : This function is used to store the information sent in     */
/*                Tpc Response in DB.                                        */
/*                                                                           */
/* Input        : tRfMgmtMsgStruct                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateTpcRspInfo (tRfMgmtMsgStruct * pRfMgmtMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /*Copy the information posted and store it in DB */
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
        u4RfMgmt11hRadioIfIndex = pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.
        TpcRspInfo.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
        i1RfMgmt11hLinkMargin = pRfMgmtMsgStruct->unRfMgmtMsg.RfMgmtQueueReq.
        TpcRspInfo.i1LinkMargin;

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
        i2RfMgmt11hTxPowerLevel = (INT2) pRfMgmtMsgStruct->unRfMgmtMsg.
        RfMgmtQueueReq.TpcRspInfo.i1TxPower;

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
            RfMgmt11hStationMacAddress, pRfMgmtMsgStruct->unRfMgmtMsg.
            RfMgmtQueueReq.TpcRspInfo.StaMac, sizeof (tMacAddr));

    /* Check whether entry is already present*
     * If it is already present create a entry else update it*/
    if (RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        /* Create the DB entry */
        if (RfMgmtProcessDBMsg (RFMGMT_CREATE_TPC_INFO_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtUpdateTpcRspInfo : Creating "
                        "TPC info failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtUpdateTpcRspInfo: Creating "
                          "TPC info failed\n"));
            return RFMGMT_FAILURE;
        }
    }
    else
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTxPowerLevel = OSIX_TRUE;

        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hLinkMargin = OSIX_TRUE;

        if (RfMgmtProcessDBMsg (RFMGMT_SET_TPC_INFO_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtUpdateTpcRspInfo: Updating "
                        "TPC info failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtUpdateTpcRspInfo: Updating " "TPCfailed\n"));
            return RFMGMT_FAILURE;
        }

    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtApRadioConfigRequest                                 */
/*                                                                           */
/* Description  : To create or delete the radio interface related DB when a  */
/*                Radio Interface is initialized                             */
/*                                                                           */
/* Input        : u1OpCode - Received OP code                                */
/*                pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtApRadioConfigRequest (UINT1 u1OpCode, tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1MsgType = 0;
    UINT1               u1BssidMsgType = 0;
    UINT1               u1WlanID = 0;
    UINT2               u2WtpInternalId = 0;

    RFMGMT_FN_ENTRY ();
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtApRadioConfigRequest : Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtApRadioConfigRequest : Null input received\n"));
        return RFMGMT_FAILURE;
    }

    /* Copy the radio ifindex and radio type if valid, else return failure */
    if (pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex != 0)
    {
        /* Assign the message type to be processed by DB based on the 
         * input received */
        if (u1OpCode == RFMGMT_CREATE_RADIO_ENTRY)
        {
            u1MsgType = RFMGMT_CREATE_RADIO_IF_INDEX_ENTRY;
            u1BssidMsgType = RFMGMT_CREATE_BSSID_SCAN_ENTRY;
        }
        else if (u1OpCode == RFMGMT_UPDATE_RADIO_MAC_ADDR)
        {
            u1MsgType = RFMGMT_SET_AP_CONFIG_ENTRY;
        }
        else
        {
            u1MsgType = RFMGMT_DESTROY_RADIO_IF_INDEX_ENTRY;
            u1BssidMsgType = RFMGMT_DESTROY_BSSID_SCAN_ENTRY;

            u2WtpInternalId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u2WtpInternalId;

            gau2ApNewColors[u2WtpInternalId] = 0;
        }

        MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u4RadioIfIndex =
            pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                RadioMacAddr, pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.
                RadioMacAddr, sizeof (tMacAddr));

        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigIsSetDB.bRadioMacAddr = OSIX_TRUE;

        /* Create the DB entry */
        if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtApRadioConfigRequest: RF MGMT DB "
                        "processing failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtApRadioConfigRequest: RF MGMT DB "
                          "processing failed\n"));
            return RFMGMT_FAILURE;
        }

        if (u1MsgType != RFMGMT_SET_AP_CONFIG_ENTRY)
        {
            MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));
            RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
                u4RadioIfIndex =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
            for (u1WlanID = 1; u1WlanID <= MAX_RFMGMT_WLAN_ID; u1WlanID++)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
                    u1WlanID = u1WlanID;
                if (RfMgmtProcessDBMsg (u1BssidMsgType, &RfMgmtDB) !=
                    RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtApRadioConfigRequest: RFMGMT BSSID DB "
                                "processing failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtApRadioConfigRequest: RFMGMT BSSID DB "
                                  "processing failed\n"));
                    return RFMGMT_FAILURE;
                }
            }
        }
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtApRadioConfigRequest: Invalid index, "
                    "return failure\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtApRadioConfigRequest: Invalid index, "
                      "return failure\n"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessNeighborMessage                               */
/*                                                                           */
/* Description  : This function parse the received neighbor packet from      */
/*                the AP and store it in the DB                              */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessNeighborMessage (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfmgmtFailedAPDB;
    tRadioIfGetDB       RadioIfDB;
    tWssWlanDB         *pWssWlanDB = NULL;
    tRfMgmtAutoRfProfileDB RfMgmtAutoRfProfileDB;
    tWssifauthDBMsgStruct *pStaMsgStruct = NULL;
    tVendorNeighborAp  *pRfMgmtNeighborMsg = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tMacAddr            MacAddr;
    tMacAddr            RadioMacAddr;
    tMacAddr            ExternalAPMac;
    INT2                i2RssiThreshold = 0;
    UINT4               u4Dot11RadioType = 0;
    UINT4               u4RadioType = 0;
    UINT2               u2Channel = 0;
    UINT2               u2Length = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2NeighWtpId = 0;
    UINT2               u2ProfileId = 0;
    UINT1               u1Count = 0;
    UINT1               u1ClientCount = 0;
    UINT1               u1ClientThreshold = 0;
    UINT1               u1MsgType = RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY;
    UINT1               u1RadioIndex = 0;
    UINT2               u2Index = 0;
    UINT1               u1AllNeighborsReported = OSIX_TRUE;
    UINT1               u1APNotNeighbor = OSIX_FALSE;
    UINT1               u1NeighborUpdateStatus = OSIX_FALSE;
    UINT1               au1String[RFMGMT_MAC_STR_LEN];
    UINT4               u4MaxChannel = 0;
    tRfMgmtScanChannel *pTempScanChannel;
    UINT1               u1IsExternalAP = 0;
    UINT1               u1ConsiderExternalAPs = 0;
    UINT1               u1IsNewExternalAP = OSIX_TRUE;
    UINT1               u1ExistingEntry = OSIX_FALSE;
#ifdef ROGUEAP_WANTED
    UINT1               au1String2[RFMGMT_MAC_STR_LEN];
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB = NULL;
    tRfMgmtRogueApIsSetDB *pRfMgmtRogueApIsSetDB = NULL;
#endif

    RFMGMT_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pStaMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pStaMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighborMessage:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighborMessage:- "
                      "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighborMessage:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighborMessage:- "
                      "UtlShMemAllocWlanDbBuf returned failure\n"));
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfmgmtFailedAPDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));
    MEMSET (pStaMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&RadioMacAddr, 0, sizeof (tMacAddr));
#ifdef ROGUEAP_WANTED
    MEMSET (&pRfMgmtRogueApIsSetDB, 0, sizeof (tRfMgmtRogueApIsSetDB));
#endif

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighborMessage: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighborMessage: Null input received\n"));
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    RfMgmtGetCapwapProfileId (pWssMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.
                              u2WtpInternalId, &u2ProfileId);
    RFMGMT_TRC1 (RFMGMT_DCA_TRC,
                 "DCA Algorithm: Request received for WTP - %d\n", u2ProfileId);

    pRfMgmtNeighborMsg = &pWssMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.
        VendorNeighborAp;

    if (pRfMgmtNeighborMsg->isOptional != OSIX_FALSE)
    {
        /* Validate the received input before storing in DB */
        if (pRfMgmtNeighborMsg->u2MsgEleType != NEIGHBOR_AP_MSG)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessNeighborMessage: Invalid message type\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessNeighborMessage: Invalid message type\n"));
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            return RFMGMT_FAILURE;
        }
        if (pRfMgmtNeighborMsg->u2MsgEleLen <= RFMGMT_NEIGHBOR_MSG_LEN)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessNeighborMessage: Invalid message length\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessNeighborMessage: Invalid message length"));
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            return RFMGMT_FAILURE;
        }

        RFMGMT_TRC1 (RFMGMT_DCA_TRC, "No of entries in message - %d \r\n",
                     ((pRfMgmtNeighborMsg->u2MsgEleLen -
                       RFMGMT_NEIGHBOR_MSG_LEN) /
                      RFMGMT_NEIGHBOR_SCAN_FIXED_LEN));
        if (pWssIfCapwapDB == NULL)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "Memory Allocation Failed:WSS_IF_DB_TEMP_MEM_ALLOC\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            return RFMGMT_FAILURE;
        }

        /* Get the radio ifindex from the input structure */
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = pRfMgmtNeighborMsg->u1RadioId;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

        u2WtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtProcessNeighborMessage :"
                        "Get radio Index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessNeighborMessage : Get radio Index failed"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            return RFMGMT_FAILURE;
        }
        RfmgmtGetWtpIndex (pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId,
                           pWssIfCapwapDB->CapwapGetDB.u1RadioId,
                           &u2WtpInternalId);

        if (pWssIfCapwapDB->CapwapGetDB.u4IfIndex == 0)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessNeighborMessage: Invalid "
                        "Ifindex received \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessNeighborMessage: Invalid "
                          "Ifindex received "));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            return RFMGMT_FAILURE;
        }
        MEMCPY (MacAddr, pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                sizeof (tMacAddr));

        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bRssiThreshold = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bClientThreshold = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bRowStatus = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bDcaInterval = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bDcaMode = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bDcaSelection = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bApNeighborCount = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bDcaSensitivity = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bAllowedChannelList = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bUnUsedChannelList = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bConsiderExternalAPs = OSIX_TRUE;

        RFMGMT_TRC1 (RFMGMT_DCA_TRC, "Radio Type - %d\n",
                     pRfMgmtNeighborMsg->u4Dot11RadioType);

        if ((pRfMgmtNeighborMsg->u4Dot11RadioType == RFMGMT_RADIO_TYPEA) ||
            (pRfMgmtNeighborMsg->u4Dot11RadioType == RFMGMT_RADIO_TYPEAN) ||
            (pRfMgmtNeighborMsg->u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
        {
            u1RadioIndex = 0;
            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            u4MaxChannel = RFMGMT_MAX_CHANNELA;
        }
        else if ((pRfMgmtNeighborMsg->u4Dot11RadioType == RFMGMT_RADIO_TYPEB) ||
                 (pRfMgmtNeighborMsg->u4Dot11RadioType == RFMGMT_RADIO_TYPEBG)
                 || (pRfMgmtNeighborMsg->u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEBGN))
        {
            u1RadioIndex = RFMGMT_OFFSET_ONE;
            u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
            u4MaxChannel = RFMGMT_MAX_CHANNELB;
        }
        else
        {
            u1RadioIndex = RFMGMT_OFFSET_TWO;
            u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
            u4MaxChannel = RFMGMT_MAX_CHANNELB;
        }

        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
            u4Dot11RadioType = u4Dot11RadioType;
        /* Store the radio type in Auto profile DB structure */
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = u4Dot11RadioType;
        /* Get the RSSI threshold for the received radio type */
        if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                                &RfMgmtDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessNeighborMessage: Getting "
                        "RSSI Threshold failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessNeighborMessage: Getting "
                          "RSSI Threshold failed"));
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return RFMGMT_FAILURE;
        }
        i2RssiThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.i2RssiThreshold;

        u1ClientThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1ClientThreshold;

        u1ConsiderExternalAPs = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1ConsiderExternalAPs;

        MEMCPY (&RfMgmtAutoRfProfileDB,
                &RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB,
                sizeof (tRfMgmtAutoRfProfileDB));

        MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
            u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        /* The below length is to check whether the length in the header field
         * and actual values matches. The below size includes fixed header of
         * radio id (1 byte) and radio type (4 bytes) */
        u2Length = RFMGMT_NEIGHBOR_MSG_LEN;

        /* Get all the parameters from the input structure */
        /* The input may contain the neighbor details for all the channels
         * available for that country. Hence loop through all the channels and
         * in each channel all the neighbor AP information and create/update
         * the DB. */
        for (u2Channel = 0; u2Channel < u4MaxChannel; u2Channel++)
        {
            u1APNotNeighbor = OSIX_FALSE;
            u1NeighborUpdateStatus = OSIX_FALSE;
            u1AllNeighborsReported = OSIX_TRUE;
            pTempScanChannel = &pRfMgmtNeighborMsg->ScanChannel[u2Channel];
            for (u1Count = 0; u1Count <
                 pTempScanChannel->u1NeighborApCount; u1Count++)
            {
                MEMSET (ExternalAPMac, 0, sizeof (tMacAddr));
                u1IsExternalAP = OSIX_FALSE;
                u1IsNewExternalAP = OSIX_TRUE;
                u2NeighWtpId = 0;
                u1MsgType = RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY;
                u1ExistingEntry = OSIX_FALSE;
#ifdef ROGUEAP_WANTED
                if (gu1RogueDetection == ENABLED)
                {
                    pRfMgmtRogueApDB =
                        &RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB;
                    MEMSET (&RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                            RfMgmtRogueApDB, 0,
                            sizeof (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                                    RfMgmtRogueApDB));
                    MEMSET (pRfMgmtRogueApDB->u1RogueApSSID, 0,
                            WSSMAC_MAX_SSID_LEN);
                    MEMCPY (pRfMgmtRogueApDB->u1RogueApSSID,
                            pTempScanChannel->au1ssid[u1Count],
                            WSSMAC_MAX_SSID_LEN);
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.u4RadioIfIndex =
                        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                }
#endif
                RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u2ScannedChannel =
                    pTempScanChannel->u2ScannedChannel;
                if (u4Dot11RadioType == RFMGMT_RADIO_TYPEA &&
                    (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                     RfMgmtNeighborScanDB.u2ScannedChannel <=
                     RFMGMT_MAX_CHANNELB))
                {
                    continue;
                }
                else if (u4Dot11RadioType == RFMGMT_RADIO_TYPEB &&
                         (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                          RfMgmtNeighborScanDB.u2ScannedChannel >
                          RFMGMT_MAX_CHANNELB))
                {
                    continue;
                }

                if (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u2ScannedChannel != 0)
                {
                    /* Add the length of the neighbor mac, rssi and channel scan
                     * related information. If there is a mismatch between the
                     * calculated length and header length then throw error. */
                    u2Length = (UINT2) (u2Length +
                                        RFMGMT_NEIGHBOR_SCAN_FIXED_LEN);

                    if (u2Length > pRfMgmtNeighborMsg->u2MsgEleLen)
                    {
                        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                    "RfMgmtProcessNeighborMessage: "
                                    "message length mismatch\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtProcessNeighborMessage: "
                                      "message length mismatch"));

                        /* Need to delete the entries which got added so far */
                        break;
                    }
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanIsSetDB.bScannedChannel = OSIX_TRUE;

                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.i2Rssi =
                        pTempScanChannel->i2Rssi[u1Count];

                    /* Query the WLAN DB and get the Radio IfIndex 
                     * corresponding to the BSSID */
                    MEMCPY (pWssWlanDB->WssWlanAttributeDB.BssId,
                            pTempScanChannel->
                            NeighborMacAddr[u1Count], sizeof (tMacAddr));
#ifdef ROGUEAP_WANTED
                    if (gu1RogueDetection == ENABLED)
                    {
                        /*RogueAp BSSID */
                        MEMCPY (pRfMgmtRogueApDB->u1RogueApBSSID,
                                pTempScanChannel->
                                NeighborMacAddr[u1Count], sizeof (tMacAddr));
                        RfmgmtMacToStr (pRfMgmtRogueApDB->u1RogueApBSSID,
                                        au1String2);

                        /* RogueAp BSSID learnt from  this AP MAC */
                        MEMCPY (pRfMgmtRogueApDB->u1BSSIDRogueApLearntFrom,
                                pTempScanChannel->
                                BSSIDRogueApLearntFrom[u1Count],
                                sizeof (tMacAddr));
                        RfmgmtMacToStr (pRfMgmtRogueApDB->
                                        u1BSSIDRogueApLearntFrom, au1String);

                        /*RogueAp SSID */
                        /*  MEMSET(pRfMgmtRogueApDB->u1RogueApSSID,0,WSSMAC_MAX_SSID_LEN);
                           MEMCPY (pRfMgmtRogueApDB->u1RogueApSSID,
                           pTempScanChannel->
                           au1ssid[u1Count], STRLEN(pTempScanChannel->au1ssid[u1Count]));
                         */
                        pRfMgmtRogueApDB->isRogueApProcetionType =
                            pTempScanChannel->isRogueApProcetionType[u1Count];
                        pRfMgmtRogueApDB->u4RogueApClientCount =
                            pTempScanChannel->u1StationCount[u1Count];
                        pRfMgmtRogueApDB->i2RogueApRSSI =
                            pTempScanChannel->i2Rssi[u1Count];
                        pRfMgmtRogueApDB->u2RogueApOperationChannel =
                            pTempScanChannel->u2ScannedChannel;
                        pRfMgmtRogueApDB->u4RogueApLastHeardTime = OsixGetSysUpTime ();    /*Heard Time */
                        pRfMgmtRogueApDB->isRogueApStatus = ENABLED;

                        pRfMgmtRogueApIsSetDB =
                            &RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                            RfMgmtRogueApIsSetDB;
                        pRfMgmtRogueApIsSetDB->bRogueApBSSID = OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bRogueApSSID = OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bRogueApProcetionType =
                            OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bRogueApRSSI = OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bRogueApOperationChannel =
                            OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bRogueApLastHeardTime =
                            OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bBSSIDRogueApLearntFrom =
                            OSIX_TRUE;
                        pRfMgmtRogueApIsSetDB->bRogueApStatus = OSIX_TRUE;
                    }

#endif
                    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
#ifdef ROGUEAP_WANTED
                        if (gu1RogueDetection == ENABLED)
                        {
                            /* Check for Heard BSSID Junk or not */
                            MEMCPY (pWssWlanDB->WssWlanAttributeDB.BssId,
                                    pTempScanChannel->
                                    BSSIDRogueApLearntFrom[u1Count],
                                    sizeof (tMacAddr));
                            pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex =
                                OSIX_TRUE;
                            if (WssIfProcessWssWlanDBMsg
                                (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                 pWssWlanDB) != OSIX_SUCCESS)
                            {
                                RfmgmtMacToStr (pRfMgmtRogueApDB->
                                                u1BSSIDRogueApLearntFrom,
                                                au1String);
                                RFMGMT_TRC1 (RFMGMT_DCA_TRC,
                                             "Wrong Heard BSS for - %s\r\n",
                                             au1String);
                                continue;

                            }

                            /* No entry found for this BSSID. which implies that 
                             * it a Rogue AP information. */

                            RfmgmtMacToStr (pWssWlanDB->WssWlanAttributeDB.
                                            BssId, au1String);
                            RFMGMT_TRC1 (RFMGMT_DCA_TRC, "No BSS for - %s\r\n",
                                         au1String);
                            u1MsgType = RFMGMT_CREATE_SHOW_ROGUE_INFO;
                            /* Create the DB entry */
                            if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB)
                                != RFMGMT_SUCCESS)
                            {
                                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                            "Rogue AP processing failed\r\n");
                                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL,
                                              gu4RfmSysLogId,
                                              "Rogue AP processing failed"));
                            }
                            u1MsgType = RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY;
                        }

#endif
                        if (u1ConsiderExternalAPs == RFMGMT_IGNORE_EXTERNAL_APS)
                        {
                            continue;
                        }

                        /* No entry found for this BSSID. which implies that 
                         * the ap may be manged by different WLC. However we 
                         * require this entry just for the neighbor AP 
                         * information. */
                        for (u2Index = RFMGMT_MAX_RADIOS;
                             u2Index <
                             RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
                             u2Index++)
                        {
                            if (MEMCMP
                                (gaDcaAlgorithm[u1RadioIndex][u2Index][u2Index].
                                 MacAddr,
                                 &pRfMgmtNeighborMsg->ScanChannel[u2Channel].
                                 NeighborMacAddr[u1Count],
                                 RFMGMT_MAC_SIZE) == 0)
                            {
                                u2NeighWtpId = u2Index;
                                u1IsNewExternalAP = OSIX_FALSE;
                                RFMGMT_TRC1 (RFMGMT_DCA_TRC,
                                             "External AP Index = %d\n",
                                             u2NeighWtpId);

                            }
                        }
                        /* AP Managed by WLC shall be maintained irrespective of
                         * configured threshold. However, external AP's
                         * information shall be stored only if less than
                         * threshold */
                        if ((i2RssiThreshold > pRfMgmtNeighborMsg->
                             ScanChannel[u2Channel].i2Rssi[u1Count])
                            && (u2NeighWtpId == 0))
                        {
                            continue;
                        }

                        MEMCPY (ExternalAPMac,
                                &pRfMgmtNeighborMsg->ScanChannel[u2Channel].
                                NeighborMacAddr[u1Count], sizeof (tMacAddr));
                        if (u2NeighWtpId == 0)
                        {
                            if (RfMgmtGetFreeExternalAPId (&u2NeighWtpId,
                                                           u1RadioIndex) ==
                                RFMGMT_FAILURE)
                            {
                                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                            "RfMgmtGetFreeExternalAPId: "
                                            "returned failure\n");
                                continue;
                            }
                            MEMCPY (gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId].
                                    MacAddr, &pRfMgmtNeighborMsg->ScanChannel
                                    [u2Channel].NeighborMacAddr[u1Count],
                                    RFMGMT_MAC_SIZE);
                            gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                [u2NeighWtpId].u1IsRunStateReached = 3;
                            RFMGMT_TRC1 (RFMGMT_DCA_TRC,
                                         "New external AP Index = %d\n",
                                         u2NeighWtpId);
                        }
                        u1IsExternalAP = OSIX_TRUE;
                        MEMCPY (RadioIfDB.RadioIfGetAllDB.MacAddr,
                                pWssWlanDB->WssWlanAttributeDB.BssId,
                                sizeof (tMacAddr));
                        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanIsSetDB.bExternalAPIndex =
                            OSIX_TRUE;
                        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanDB.u2ExternalAPIndex =
                            u2NeighWtpId;
                        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.NeighborAPMac,
                                ExternalAPMac, sizeof (tMacAddr));
                    }

                    if (u1IsExternalAP == OSIX_FALSE)
                    {
                        RadioIfDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                        RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                        RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                        RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                        RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfDB) !=
                            OSIX_SUCCESS)
                        {
                            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                        "RfMgmtProcessNeighborMessage: "
                                        "No entry found in DB \n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                          "RfMgmtProcessNeighborMessage: "
                                          "No entry found in DB "));
                            continue;
                        }

                        if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                             RFMGMT_RADIO_TYPEA) ||
                            (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                             RFMGMT_RADIO_TYPEAN) || (RadioIfDB.RadioIfGetAllDB.
                                                      u4Dot11RadioType ==
                                                      RFMGMT_RADIO_TYPEAC))
                        {
                            u4RadioType = RFMGMT_RADIO_TYPEA;
                        }
                        else if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                                  RFMGMT_RADIO_TYPEB) ||
                                 (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                                  RFMGMT_RADIO_TYPEBG) ||
                                 (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                                  RFMGMT_RADIO_TYPEBGN))
                        {
                            u4RadioType = RFMGMT_RADIO_TYPEB;
                        }
                        else
                        {
                            u4RadioType = RFMGMT_RADIO_TYPEG;
                        }

                        /* if the scanned channel/radio type does not match with the
                         * radio type for which interfereence received, then skip
                         * the entry */
                        if (u4Dot11RadioType != u4RadioType)
                        {
                            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                        "\r\nScan information received from "
                                        "different radio type. Ignoring the entry\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                          "Scan information received from "
                                          "different radio type. Ignoring the entry"));
                            continue;
                        }

                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId =
                            OSIX_TRUE;
                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress =
                            OSIX_TRUE;
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            RadioIfDB.RadioIfGetAllDB.u2WtpInternalId;
                        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId =
                            OSIX_TRUE;
                        if (WssIfProcessCapwapDBMsg
                            (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                        {
                            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                        "RfMgmtProcessNeighborMessage :"
                                        "Get radio Index failed\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                          "RfMgmtProcessNeighborMessage :"
                                          "Get radio Index failed"));
                            continue;
                        }

                        u2NeighWtpId =
                            RadioIfDB.RadioIfGetAllDB.u2WtpInternalId;
                        RfmgmtGetWtpIndex (RadioIfDB.RadioIfGetAllDB.
                                           u2WtpInternalId,
                                           RadioIfDB.RadioIfGetAllDB.u1RadioId,
                                           &u2NeighWtpId);

                        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.NeighborAPMac,
                                RadioIfDB.RadioIfGetAllDB.MacAddr,
                                sizeof (tMacAddr));
                    }
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanIsSetDB.bRssi = OSIX_TRUE;
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanIsSetDB.bNeighborAPMac = OSIX_TRUE;

                    /* Check whether entry already exist in the DB. If entry 
                     * present then update the values, else create an entry. 
                     * Also check the RSSI value is greater than threshold else
                     * delete the entry */
                    if (RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY,
                                            &RfMgmtDB) == RFMGMT_SUCCESS)
                    {
                        u1ExistingEntry = OSIX_TRUE;
                        if (pTempScanChannel->
                            u1EntryStatus[u1Count] == RFMGMT_NEIGH_ENTRY_ADD)
                        {
                            RFMGMT_TRC3 (RFMGMT_DCA_TRC,
                                         "Update for existing entry AP - %d, "
                                         "NEIGH - %d, RSSI - %d\r\n",
                                         u2WtpInternalId, u2NeighWtpId,
                                         RfMgmtDB.unRfMgmtDB.
                                         RfMgmtNeighborScanTable.
                                         RfMgmtNeighborScanDB.i2Rssi);

                            u1MsgType = RFMGMT_SET_NEIGHBOR_SCAN_ENTRY;

                            /* If the entry is already present in DB <
                             * RSSI threshold and 
                             * a. recevied RSSI > threshold then remove the 
                             *    entry from gaDcaAlgo table. 
                             * b. If the received RSSI <= threshold update both
                             *    DB and gaAlgoTable
                             * c. If existing entry in Db > thrshold and new
                             *    entry  <= threshold then add to gaDca algo
                             *    table
                             * */
                            if (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.i2Rssi >= i2RssiThreshold)
                            {
                                if (i2RssiThreshold >=
                                    pTempScanChannel->i2Rssi[u1Count])
                                {
                                    u1APNotNeighbor = OSIX_TRUE;
                                }
                                else
                                {
                                    u1NeighborUpdateStatus = OSIX_TRUE;
                                    u1APNotNeighbor = OSIX_FALSE;
                                }
                            }
                            else
                            {
                                if (i2RssiThreshold <=
                                    pTempScanChannel->i2Rssi[u1Count])
                                {
                                    u1NeighborUpdateStatus = OSIX_TRUE;
                                    u1APNotNeighbor = OSIX_FALSE;
                                }
                                else
                                {
                                    u1APNotNeighbor = OSIX_TRUE;
                                }
                            }
                            RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.i2Rssi =
                                pTempScanChannel->i2Rssi[u1Count];

                            /*If the new RSSI is below the RSSI threshold,
                             * remove the neighbor information in gaDca*/
                            if (u1APNotNeighbor == OSIX_TRUE)
                            {
                                MEMSET (&gaDcaAlgorithm[u1RadioIndex]
                                        [u2WtpInternalId][u2NeighWtpId], 0,
                                        sizeof (tDcaAlgorithm));
                                MEMSET (&gaDcaAlgorithm[u1RadioIndex]
                                        [u2NeighWtpId][u2WtpInternalId], 0,
                                        sizeof (tDcaAlgorithm));
                                if (gaDcaAlgorithm[u1RadioIndex]
                                    [u2WtpInternalId][u2WtpInternalId].
                                    u1NeighAPCount != 0)
                                {
                                    gaDcaAlgorithm[u1RadioIndex]
                                        [u2WtpInternalId][u2WtpInternalId].
                                        u1NeighAPCount--;
                                }
                                if (gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId].u1NeighAPCount != 0)
                                {
                                    gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                        [u2NeighWtpId].u1NeighAPCount--;
                                }
                                if (gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId].u1NeighAPCount == 0)
                                {
                                    gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                        [u2NeighWtpId].u1IsNeighborPresent =
                                        OSIX_FALSE;
                                }
                                if (gaDcaAlgorithm[u1RadioIndex]
                                    [u2WtpInternalId][u2WtpInternalId].
                                    u1NeighAPCount == 0)
                                {
                                    gaDcaAlgorithm[u1RadioIndex]
                                        [u2WtpInternalId][u2WtpInternalId].
                                        u1IsNeighborPresent = OSIX_FALSE;
                                }
                                if (u1IsExternalAP == OSIX_TRUE)
                                {
                                    u1MsgType =
                                        RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY;

                                    /*Find whether all the AP has reported to
                                     * delete a particular AP*/
                                    for (u2Index = 0;
                                         u2Index < RFMGMT_MAX_RADIOS; u2Index++)
                                    {
                                        if (u2Index != u2NeighWtpId)
                                        {
                                            if (gaDcaAlgorithm[u1RadioIndex]
                                                [u2Index][u2NeighWtpId].
                                                u1IsNeighborPresent ==
                                                OSIX_TRUE)
                                            {
                                                u1AllNeighborsReported =
                                                    OSIX_FALSE;
                                                break;
                                            }
                                        }
                                    }
                                    if (u1AllNeighborsReported == OSIX_TRUE)
                                    {
                                        RFMGMT_TRC (RFMGMT_DCA_TRC,
                                                    "Deleting external AP information\n");
                                        gau2NoExternalAp[u1RadioIndex]--;
                                        gau2ExternalApColors[u2NeighWtpId -
                                                             RFMGMT_MAX_RADIOS]
                                            = 0;

                                        for (u2Index = 0; u2Index <
                                             RFMGMT_MAX_RADIOS +
                                             MAX_RFMGMT_EXTERNAL_AP_SIZE;
                                             u2Index++)
                                        {
                                            MEMSET (&gaDcaAlgorithm
                                                    [u1RadioIndex][u2NeighWtpId]
                                                    [u2Index], 0,
                                                    sizeof (tDcaAlgorithm));
                                            MEMSET (&gaDcaAlgorithm
                                                    [u1RadioIndex][u2Index]
                                                    [u2NeighWtpId], 0,
                                                    sizeof (tDcaAlgorithm));
                                        }
                                    }

                                }
                            }

                        }
                        else
                        {
                            MEMSET (au1String, 0, RFMGMT_MAC_STR_LEN);
                            u1MsgType = RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY;

                            RFMGMT_TRC3 (RFMGMT_DCA_TRC,
                                         "Delete existing entry AP - %d, "
                                         "NEIGH - %d, RSSI - %d\r\n",
                                         u2WtpInternalId, u2NeighWtpId,
                                         RfMgmtDB.unRfMgmtDB.
                                         RfMgmtNeighborScanTable.
                                         RfMgmtNeighborScanDB.i2Rssi);
                            if (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.i2Rssi >= i2RssiThreshold)
                            {
                                gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                    [u2WtpInternalId].u1NeighAPCount--;

                                if (gaDcaAlgorithm[u1RadioIndex]
                                    [u2WtpInternalId][u2WtpInternalId].
                                    u1NeighAPCount == 0)
                                {
                                    gaDcaAlgorithm[u1RadioIndex]
                                        [u2WtpInternalId][u2WtpInternalId].
                                        u1IsNeighborPresent = OSIX_FALSE;
                                }

                                gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                    [u2NeighWtpId].u1IsNeighborPresent =
                                    OSIX_FALSE;

                                /*Set the Ap State when the AP goes down */
                                gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId].u1ApActive = OSIX_FALSE;

                                /*Find whether all the AP has reported to
                                 * delete a particular AP*/
                                for (u2Index = 0; u2Index < RFMGMT_MAX_RADIOS;
                                     u2Index++)
                                {
                                    if (u2Index != u2NeighWtpId)
                                    {
                                        if (gaDcaAlgorithm[u1RadioIndex]
                                            [u2Index][u2NeighWtpId].
                                            u1IsNeighborPresent == OSIX_TRUE)
                                        {
                                            u1AllNeighborsReported = OSIX_FALSE;
                                            break;
                                        }
                                    }
                                }
                                gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId].u1NeighAPCount--;

                                if (gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId].u1NeighAPCount == 0)
                                {
                                    gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                        [u2NeighWtpId].u1IsNeighborPresent =
                                        OSIX_FALSE;
                                }

                                /*If all the Aps have reported to delete a
                                 * particular AP then add to the failed AP DB*/

                                if ((u1AllNeighborsReported == OSIX_TRUE)
                                    && (u1IsExternalAP != OSIX_TRUE))
                                {
                                    MEMSET (&RfmgmtFailedAPDB, 0,
                                            sizeof (tRfMgmtDB));
                                    RfmgmtFailedAPDB.unRfMgmtDB.
                                        RfMgmtFailedAPNeighborTable.
                                        RfMgmtFailedAPNeighborListDB.
                                        u4RadioIfIndex =
                                        gaDcaAlgorithm[u1RadioIndex]
                                        [u2NeighWtpId][u2NeighWtpId].
                                        u4RadioIfIndex;

                                    for (u2Index = 0;
                                         u2Index < RFMGMT_MAX_RADIOS; u2Index++)
                                    {
                                        if (u2Index != u2NeighWtpId)
                                        {
                                            if (gaDcaAlgorithm[u1RadioIndex]
                                                [u2NeighWtpId][u2Index].
                                                u1IsNeighborPresent ==
                                                OSIX_TRUE)

                                            {
                                                RfmgmtFailedAPDB.unRfMgmtDB.
                                                    RfMgmtFailedAPNeighborTable.
                                                    RfMgmtFailedAPNeighborListDB.
                                                    u4NeighRadioIfIndex =
                                                    gaDcaAlgorithm[u1RadioIndex]
                                                    [u2Index][u2Index].
                                                    u4RadioIfIndex;

                                                MEMCPY (RfmgmtFailedAPDB.
                                                        unRfMgmtDB.
                                                        RfMgmtFailedAPNeighborTable.
                                                        RfMgmtFailedAPNeighborListDB.
                                                        NeighborAPMac,
                                                        gaDcaAlgorithm
                                                        [u1RadioIndex]
                                                        [u2NeighWtpId][u2Index].
                                                        MacAddr,
                                                        sizeof (tMacAddr));

                                                /*Populate the failed AP DB once whether the AP is 
                                                 * no more detected by the neighbor*/
                                                /*If the entry is not present create an entry */
                                                if (RfMgmtProcessDBMsg
                                                    (RFMGMT_GET_FAILED_AP_NEIGH_ENTRY,
                                                     &RfmgmtFailedAPDB) ==
                                                    RFMGMT_FAILURE)
                                                {
                                                    RfmgmtFailedAPDB.unRfMgmtDB.
                                                        RfMgmtFailedAPNeighborTable.
                                                        RfMgmtFailedAPNeighborListDB.
                                                        i2Rssi =
                                                        gaDcaAlgorithm
                                                        [u1RadioIndex]
                                                        [u2NeighWtpId][u2Index].
                                                        i2Rssi;

                                                    if (RfMgmtProcessDBMsg
                                                        (RFMGMT_CREATE_FAILED_AP_NEIGH_ENTRY,
                                                         &RfmgmtFailedAPDB) ==
                                                        RFMGMT_FAILURE)
                                                    {
                                                        RFMGMT_TRC
                                                            (RFMGMT_FAILURE_TRC,
                                                             "Failed to create entry in failed AP"
                                                             "DB\r\n");
                                                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId, "Failed to create entry in failed AP" "DB"));
                                                    }
                                                }
                                                MEMSET (&gaDcaAlgorithm
                                                        [u1RadioIndex]
                                                        [u2NeighWtpId]
                                                        [u2WtpInternalId], 0,
                                                        sizeof (tDcaAlgorithm));
                                                MEMSET (&gaDcaAlgorithm
                                                        [u1RadioIndex]
                                                        [u2WtpInternalId]
                                                        [u2NeighWtpId], 0,
                                                        sizeof (tDcaAlgorithm));
                                            }
                                        }
                                    }
                                }
                                if ((u1AllNeighborsReported == OSIX_TRUE)
                                    && (u1IsExternalAP == OSIX_TRUE))
                                {
                                    /*Reset the variables and gaDcaAlgorithm
                                     * when all APs report delete for a external
                                     * AP*/
                                    RFMGMT_TRC (RFMGMT_DCA_TRC,
                                                "Deleting external AP information\n");
                                    gau2NoExternalAp[u1RadioIndex]--;
                                    gau2ExternalApColors[u2NeighWtpId -
                                                         RFMGMT_MAX_RADIOS] = 0;

                                    for (u2Index = 0; u2Index <
                                         RFMGMT_MAX_RADIOS +
                                         MAX_RFMGMT_EXTERNAL_AP_SIZE; u2Index++)
                                    {
                                        MEMSET (&gaDcaAlgorithm[u1RadioIndex]
                                                [u2NeighWtpId][u2Index], 0,
                                                sizeof (tDcaAlgorithm));
                                        MEMSET (&gaDcaAlgorithm[u1RadioIndex]
                                                [u2Index][u2NeighWtpId], 0,
                                                sizeof (tDcaAlgorithm));
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        RFMGMT_TRC3 (RFMGMT_DCA_TRC,
                                     "New entry found for AP - %d, "
                                     "NEIGH - %d, RSSI - %d\r\n",
                                     u2WtpInternalId, u2NeighWtpId,
                                     RfMgmtDB.unRfMgmtDB.
                                     RfMgmtNeighborScanTable.
                                     RfMgmtNeighborScanDB.i2Rssi);

                        /* If the RSSI received is less than the threshold then
                         * set the flag so that neighbor count in gaDcaAlgo
                         * table is not updated */
                        if (i2RssiThreshold > pTempScanChannel->i2Rssi[u1Count])
                        {
                            u1APNotNeighbor = OSIX_TRUE;
                        }
                        if (u1IsExternalAP == OSIX_TRUE)
                        {
                            gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                [u2NeighWtpId].u1NeighAPCount++;
                        }

                    }

                    /* If entry status is add then update the neighbor details
                     * in algorithm global strcuture */
                    if (pTempScanChannel->
                        u1EntryStatus[u1Count] == RFMGMT_NEIGH_ENTRY_ADD)
                    {
                        RFMGMT_TRC3 (RFMGMT_DCA_TRC,
                                     "Add New entry for AP - %d, "
                                     "NEIGH - %d, RSSI - %d\r\n",
                                     u2WtpInternalId, u2NeighWtpId,
                                     RfMgmtDB.unRfMgmtDB.
                                     RfMgmtNeighborScanTable.
                                     RfMgmtNeighborScanDB.i2Rssi);
                        if (u1IsExternalAP != OSIX_TRUE)
                        {
                            if (RfmgmtDeleteFailedAPDB
                                (gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                 [u2NeighWtpId].u4RadioIfIndex) !=
                                RFMGMT_SUCCESS)
                            {
                                RFMGMT_TRC (RFMGMT_DCA_TRC,
                                            "Failed to clear Failed AP DB");
                                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL,
                                              gu4RfmSysLogId,
                                              "Failed to clear Failed AP DB"));
                            }
                        }
                        if (u1APNotNeighbor != OSIX_TRUE)
                        {
                            gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                [u2NeighWtpId].u1IsNeighborPresent = OSIX_TRUE;
                            gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                [u2WtpInternalId].u1IsNeighborPresent =
                                OSIX_TRUE;
                            gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                [u2WtpInternalId].u1IsNeighborPresent =
                                OSIX_TRUE;
                            gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                [u2NeighWtpId].u1IsNeighborPresent = OSIX_TRUE;
                            if (u1NeighborUpdateStatus == OSIX_FALSE)
                            {
                                gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                    [u2WtpInternalId].u1NeighAPCount++;
                            }
                            if (u1IsExternalAP != OSIX_TRUE)
                            {

                                gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                    [u2NeighWtpId].u4RadioIfIndex =
                                    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex;
                            }
                            gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                [u2NeighWtpId].u4RadioIfIndex =
                                RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex;
                            gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                [u2NeighWtpId].u2ChannelNo
                                = pTempScanChannel->u2ScannedChannel;
                            gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                                [u2NeighWtpId].i2Rssi
                                = pTempScanChannel->i2Rssi[u1Count];
                            gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                [u2WtpInternalId].i2Rssi
                                = pTempScanChannel->i2Rssi[u1Count];
                            MEMCPY (gaDcaAlgorithm[u1RadioIndex]
                                    [u2WtpInternalId][u2NeighWtpId].MacAddr,
                                    RadioIfDB.RadioIfGetAllDB.MacAddr,
                                    sizeof (tMacAddr));
                            MEMCPY (gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2WtpInternalId].MacAddr,
                                    gaDcaAlgorithm[u1RadioIndex]
                                    [u2WtpInternalId][u2WtpInternalId].MacAddr,
                                    sizeof (tMacAddr));
                        }
                    }
                    else if (u1ExistingEntry != OSIX_TRUE)
                    {
                        if (u1IsExternalAP == OSIX_TRUE)
                        {
                            MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                    [u2NeighWtpId], 0, sizeof (tDcaAlgorithm));
                        }
                        RFMGMT_TRC (RFMGMT_DCA_TRC,
                                    "Entry has already been deleted from DB, "
                                    "therefore ignoring it\r\n");
                        continue;
                    }

                    if (u1IsExternalAP != OSIX_TRUE)
                    {

                        /* Get the number of clients connected to the BSSID and 
                         * check with the client threshold value. If the client 
                         * threshold is crossed then update the rejected count 
                         * and fetch the next entry */
                        pStaMsgStruct->WssBssStnCountDB.u4BssIfIndex =
                            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

                        if (WssIfProcessWssAuthDBMsg
                            (WSS_STA_GET_CLIENT_COUNT_PER_BSSSID,
                             pStaMsgStruct) != OSIX_SUCCESS)
                        {
                        }

                        u1ClientCount = pStaMsgStruct->WssBssStnCountDB.
                            u1ClientCount;
                        if (u1ClientThreshold < u1ClientCount)
                        {

                            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                        "RfMgmtProcessNeighborMessage:  "
                                        "Exceeded the client Threshold\r\n");
                            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                          "RfMgmtProcessNeighborMessage: "
                                          "Exceeded the client Threshold"));
                            continue;
                        }
                    }
                    if ((u1IsExternalAP == OSIX_TRUE)
                        && (u1IsNewExternalAP == OSIX_TRUE)
                        && (u1MsgType == RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY))
                    {
                        gau2ExternalApColors[u2NeighWtpId - RFMGMT_MAX_RADIOS] =
                            pRfMgmtNeighborMsg->ScanChannel[u2Channel].
                            u2ScannedChannel;

                        /*Set neighbor status of external Ap as true, if the APs
                         * has different channels. This is done because we cant
                         * change the channels of external APs*/
                        for (u2Index = RFMGMT_MAX_RADIOS;
                             u2Index <
                             RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
                             u2Index++)
                        {
                            if ((gau2ExternalApColors
                                 [u2NeighWtpId - RFMGMT_MAX_RADIOS] != 0)
                                &&
                                (gau2ExternalApColors
                                 [u2Index - RFMGMT_MAX_RADIOS] != 0))
                            {
                                if (gau2ExternalApColors
                                    [u2NeighWtpId - RFMGMT_MAX_RADIOS] ==
                                    gau2ExternalApColors[u2Index -
                                                         RFMGMT_MAX_RADIOS])
                                {
                                    gaDcaAlgorithm[u1RadioIndex][u2Index]
                                        [u2NeighWtpId].u1IsNeighborPresent =
                                        OSIX_FALSE;
                                    gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                        [u2Index].u1IsNeighborPresent =
                                        OSIX_FALSE;
                                }
                                else
                                {
                                    gaDcaAlgorithm[u1RadioIndex][u2Index]
                                        [u2NeighWtpId].u1IsNeighborPresent =
                                        OSIX_TRUE;
                                    gaDcaAlgorithm[u1RadioIndex][u2NeighWtpId]
                                        [u2Index].u1IsNeighborPresent =
                                        OSIX_TRUE;
                                }
                            }
                        }
                        gau2NoExternalAp[u1RadioIndex]++;
                    }

                    /* Create the DB entry */
                    if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB)
                        != RFMGMT_SUCCESS)
                    {
                        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                    "RfMgmtProcessNeighborMessage: RF MGMT DB "
                                    "processing failed\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtProcessNeighborMessage: RF MGMT DB "
                                      "processing failed"));
                        continue;
                    }
                }
            }
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessClientScanMessage                             */
/*                                                                           */
/* Description  : This function parse the received client SNR packet from    */
/*                the AP and store it in the DB                              */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessClientScanMessage (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtUpdateDB;
    tRfMgmtDB           RfMgmtDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfDB;
    tVendorClientScan  *pRfMgmtClientScanMsg = NULL;
    tWssifauthDBMsgStruct *pStaMsgStruct = NULL;
    tWssWlanDB         *pWssWlanDB;
    UINT4               u4Dot11RadioType = 0;
    INT2                i2SNRThreshold = 0;
    UINT2               u2Length = 0;
    UINT1               u1Count = 0;
    UINT1               u1MsgType = RFMGMT_CREATE_CLIENT_SCAN_ENTRY;
    INT4                i4Status = 0;

    RFMGMT_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    pStaMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pStaMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientScanMessage:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientScanMessage:- "
                      "UtlShMemAllocWssIfAuthDbBuf returned failure"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientScanMessage:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientScanMessage:- "
                      "UtlShMemAllocWlanDbBuf returned failure"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pStaMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientScanMessage: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientScanMessage: Null input received"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
        return RFMGMT_FAILURE;
    }

    pRfMgmtClientScanMsg = &pWssMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.
        VendorClientScan;

    if (pRfMgmtClientScanMsg->isOptional != OSIX_FALSE)
    {
        /* Validate the received input before storing in DB */
        if (pRfMgmtClientScanMsg->u2MsgEleType != CLIENT_SCAN_MSG)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessClientScanMessage: Invalid message type\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessClientScanMessage: Invalid message type\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return RFMGMT_FAILURE;
        }
        if (pRfMgmtClientScanMsg->u2MsgEleLen < RFMGMT_CLIENT_SCAN_MSG_LEN)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessClientScanMessage: Invalid message length\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessClientScanMessage: Invalid message length"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return RFMGMT_FAILURE;
        }
        /* Get the radio ifindex from the input structure */
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssMsgStruct->unRfMgmtMsg.RfMgmtPmEvtStatsReq.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = pRfMgmtClientScanMsg->u1RadioId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessClientScanMessage: Getting "
                        "Radio index failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessClientScanMessage: Getting "
                          "Radio index failed"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return RFMGMT_FAILURE;
        }

        RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return RFMGMT_FAILURE;
        }

        if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEA) ||
            (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEAN) ||
            (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
        }
        else if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEB) ||
                 (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBG) ||
                 (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBGN))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
        }
        else
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
        }

        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
            u4Dot11RadioType = u4Dot11RadioType;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bSNRThreshold = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
            bClientThreshold = OSIX_TRUE;
        /* Get the RSSI threshold for the received radio type */
        if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                                &RfMgmtDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessNeighborMessage: Getting "
                        "RSSI Threshold failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessNeighborMessage: Getting "
                          "RSSI Threshold failed"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return RFMGMT_FAILURE;
        }
        i2SNRThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.i2SNRThreshold;

        /* The below length is to check whether the length in the header field
         * and actual values matches. The below size includes fixed header of
         * radio id (1 byte) and client count (1 byte) */

        /* Get all the parameters from the input structure */
        u1Count = pRfMgmtClientScanMsg->u1ClientCount;

        RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
            u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        /* For each client create the entry in the DB */
        for (u1Count = 0; u1Count < pRfMgmtClientScanMsg->u1ClientCount;
             u1Count++)
        {
            /* Add the length of the client mac and snr related 
             * information. If there is a mismatch between the
             * calculated length and header length then throw error. */
            u2Length = (UINT2) (u2Length + RFMGMT_CLIENT_SCAN_FIXED_LEN);

            if (u2Length > pRfMgmtClientScanMsg->u2MsgEleLen)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessClientScanMessage: "
                            "message length mismatch\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessClientScanMessage: "
                              "message length mismatch\n"));

                /* Need to delete the entries which got added so far */
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return RFMGMT_FAILURE;
            }

            RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
                i2ClientSNR = pRfMgmtClientScanMsg->i2ClientSNR[u1Count];

            MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress,
                    &pRfMgmtClientScanMsg->ClientMacAddress[u1Count],
                    sizeof (tMacAddr));

#ifndef RFMGMT_TEST_WANTED
            MEMCPY (pStaMsgStruct->WssIfAuthStateDB.stationMacAddress,
                    &pRfMgmtClientScanMsg->ClientMacAddress[u1Count],
                    sizeof (tMacAddr));

            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                          pStaMsgStruct) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessClientScanMessage: "
                            "Entry not found in station DB. Ignore entry\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessClientScanMessage: "
                              "Entry not found in station DB. Ignore entry"));
                continue;
            }
            /* Query the WLAN DB and get the BSSIfIndex Entry 
             * corresponding to the BSSID */
            pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex = pStaMsgStruct->
                WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;

            pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessClientScanMessage: "
                            "Not found in station DB. Ignore entry\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessClientScanMessage: "
                              "Not found in station DB. Ignore entry"));
                continue;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex !=
                RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                             "RfMgmtProcessClientScanMessage: RadioIfIndex "
                             "not matched.RFMGMT Client Updation DB failed %d\n",
                             RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                             RfMgmtClientScanDB.u4RadioIfIndex);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessClientScanMessage: RadioIfIndex "
                              "not matched.RFMGMT Client Updation DB failed "));
                continue;
            }
#endif
            /* Check whether entry already exist in the DB. If entry present then
             * update the values, else create an entry */
            if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_SCAN_ENTRY,
                                    &RfMgmtDB) == RFMGMT_SUCCESS)
            {
                if ((i2SNRThreshold > pRfMgmtClientScanMsg->
                     i2ClientSNR[u1Count]) && ((pRfMgmtClientScanMsg->
                                                u1EntryStatus[u1Count]) ==
                                               RFMGMT_CLIENT_ENTRY_ADD))
                {
                    u1MsgType = RFMGMT_SET_CLIENT_SCAN_ENTRY;
                }
                else
                {
                    u1MsgType = RFMGMT_DESTROY_CLIENT_SCAN_ENTRY;
                }
            }
            else
            {
                /* If the received SNR is less than the threshold then
                 * skip updating the entry to the DB. But update the count for
                 * value less then SNR threshold */
                if (i2SNRThreshold < pRfMgmtClientScanMsg->i2ClientSNR[u1Count])
                {
                    /*update for less SNR threshold; */
                    /*u1MsgType = RFMGMT_DESTROY_CLIENT_SCAN_ENTRY; */
                    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u4RadioIfIndex =
                        RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanDB.u4RadioIfIndex;

                    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigIsSetDB.bBelowSNRCount = OSIX_TRUE;

                    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigIsSetDB.bClientsDiscarded = OSIX_TRUE;

                    if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY,
                                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
                    {
                        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                                     "RfMgmtProcessClientScanMessage: Below SNR "
                                     "Count change to the RFMGMT DB failed %d\n",
                                     RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                                     RfMgmtClientScanDB.u4RadioIfIndex);
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtProcessClientScanMessage: Below SNR "
                                      "Count change to the RFMGMT DB failed"));
                    }

                    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
                    i4Status = 1;

                }
            }

            /* If the entry status is set to delete then delete the entry
             * from the DB */
            if (i4Status != 1)
            {
                if (pRfMgmtClientScanMsg->u1EntryStatus[u1Count] ==
                    RFMGMT_CLIENT_ENTRY_ADD)
                {
                    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u4RadioIfIndex =
                        RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanDB.u4RadioIfIndex;
                    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigIsSetDB.bClientsAccepted = OSIX_TRUE;

                    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigIsSetDB.bSNRScanCount = OSIX_TRUE;

                    if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY,
                                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
                    {
                        RFMGMT_TRC1 (RFMGMT_INFO_TRC,
                                     "RfMgmtProcessClientScanMessage: Client "
                                     "entry accepted %d\n",
                                     RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                                     RfMgmtClientScanDB.u4RadioIfIndex);
                    }

                }

                RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanIsSetDB.bClientSNR = OSIX_TRUE;

                /* Create the DB entry */
                if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtProcessClientScanMessage: RF MGMT DB "
                                "processing failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtProcessClientScanMessage: RF MGMT DB "
                                  "processing failed"));
                    continue;
                }
            }
            i4Status = 0;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessWssIfMsg                                      */
/*                                                                           */
/* Description  : To call the appropriate Func calls in RF MGMT from external*/
/*                modules                                                    */
/*                                                                           */
/* Input        : u1OpCode - Received OP code                                */
/*                pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessWssIfMsg (UINT1 u1OpCode, tRfMgmtMsgStruct * pWssMsgStruct)
{

    UINT1               u1RetStatus = 1;
    RFMGMT_FN_ENTRY ();

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessWssIfMsg: Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessWssIfMsg: Null input received"));
        return RFMGMT_FAILURE;
    }

    switch (u1OpCode)
    {
        case RFMGMT_UPDATE_RADIO_MAC_ADDR:
        case RFMGMT_CREATE_RADIO_ENTRY:
        case RFMGMT_DELETE_RADIO_ENTRY:
        {
            u1RetStatus = RfMgmtApRadioConfigRequest (u1OpCode, pWssMsgStruct);

        }
            break;

        case RFMGMT_WLC_RECV_NEIGHBOR_MSG:
        {
            u1RetStatus = RfMgmtProcessNeighborMessage (pWssMsgStruct);

        }
            break;

        case RFMGMT_WLC_RECV_CLIENT_MSG:
        {
            u1RetStatus = RfMgmtProcessClientScanMessage (pWssMsgStruct);

        }
            break;
        case RFMGMT_VALIDATE_CONFIG_STATUS_REQ:
        {
            u1RetStatus = RfMgmtValidateConfigStatusReq (pWssMsgStruct);

        }
            break;
        case RFMGMT_VALIDATE_SCANNED_CHANNEL:
        {
            u1RetStatus = RfMgmtValidateScannedChannel (pWssMsgStruct);
        }
            break;
        case RFMGMT_CONFIG_STATUS_REQ:
        {
            u1RetStatus = RfMgmtProcessConfigStatusReq (pWssMsgStruct);
        }
            break;
        case RFMGMT_CONFIG_UPDATE_RSP:
        {
            u1RetStatus = RfMgmtProcessConfigUpdateRsp (pWssMsgStruct);
        }
            break;

        case RFMGMT_WLC_RUN_STATE_INDI_MSG:
        {
            u1RetStatus = RfMgmtHandleNewAPRadioEntry (pWssMsgStruct);
        }
            break;
        case RFMGMT_DELETE_NEIGHBOR_INFO:
        {
            u1RetStatus =
                RfMgmtDeleteNeighborDetails (pWssMsgStruct->unRfMgmtMsg.
                                             RfMgmtIntfConfigReq.u4RadioIfIndex,
                                             RFMGMT_FLUSH_DB_ONLY);
            u1RetStatus =
                RfMgmtDeleteClientDetails (pWssMsgStruct->unRfMgmtMsg.
                                           RfMgmtIntfConfigReq.u4RadioIfIndex);
            u1RetStatus =
                RfMgmtDeleteTpcInfo (pWssMsgStruct->unRfMgmtMsg.
                                     RfMgmtIntfConfigReq.u4RadioIfIndex);
        }
            break;

        case RFMGMT_DELETE_CLIENT_STATS:
        {
            u1RetStatus = RfMgmtDeleteClientStats (pWssMsgStruct);
        }
            break;
        case RFMGMT_TX_POWER_UPDATE:
        {
            u1RetStatus = RfMgmtUpdateTxPowerLevel (pWssMsgStruct);
        }
            break;
        case RFMGMT_RADIO_DOWN_MSG:
        case RFMGMT_PROFILE_DISABLE_MSG:
        {
            u1RetStatus =
                RfMgmtDeleteNeighborDetails (pWssMsgStruct->unRfMgmtMsg.
                                             RfMgmtIntfConfigReq.u4RadioIfIndex,
                                             RFMGMT_FLUSH_DB_ONLY);
            u1RetStatus =
                RfMgmtDeleteTpcInfo (pWssMsgStruct->unRfMgmtMsg.
                                     RfMgmtIntfConfigReq.u4RadioIfIndex);

        }
            break;
        case RFMGMT_WLAN_DELETION_MSG:
        {
            u1RetStatus =
                RfMgmtDeleteNeighborDetails (pWssMsgStruct->unRfMgmtMsg.
                                             RfMgmtIntfConfigReq.u4RadioIfIndex,
                                             RFMGMT_FLUSH_IF_WLAN_DELETE);
            u1RetStatus =
                RfMgmtDeleteTpcInfo (pWssMsgStruct->unRfMgmtMsg.
                                     RfMgmtIntfConfigReq.u4RadioIfIndex);

        }
            break;

        case RFMGMT_DFS_INFO_UPDATE:
        {
            u1RetStatus = RfMgmtUpdateDFSInfo (pWssMsgStruct);
        }
            break;
        case RFMGMT_WLC_RECV_RADAREVENT_MSG:
        {
            u1RetStatus = RfMgmtHandleRadarEventInfo (pWssMsgStruct);
        }
            break;
        case RFMGMT_NOTIFY_BINDING_COMPLETE:
        {
            u1RetStatus = RfMgmtProcessBindingComplete (pWssMsgStruct);
        }
            break;

            /* Return failure for invalid opcode */
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessWssIfMsg: Invalid OpCode, "
                        "return failure\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessWssIfMsg: Invalid OpCode, "
                          "return failure"));
            return RFMGMT_FAILURE;
    }

    if (u1RetStatus == RFMGMT_FAILURE)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtProcessWssIfMsg:Failed in Processing Message %d\r\n",
                     u1OpCode);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessWssIfMsg:Failed in Processing Message"));

    }

    RFMGMT_FN_EXIT ();
    return u1RetStatus;
}

/*****************************************************************************/
/* Function     : RfMgmtValidateConfigStatusReq                              */
/*                                                                           */
/* Description  : This functions validates the status request message        */
/*                from WTP                                                   */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtValidateConfigStatusReq (tRfMgmtMsgStruct * pWssMsgStruct)
{
    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtValidateConfigStatusReq: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtValidateConfigStatusReq: Null input received"));
        return RFMGMT_FAILURE;
    }
    switch (pWssMsgStruct->u1Opcode)
    {
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                NeighApTableConfig.u2MsgEleLen
                != RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid msg type"
                            "recvd for Rfmgmt Neigh config msg, "
                            "Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid msg type"
                              "recvd for Rfmgmt Neigh config msg, "
                              "Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                NeighApTableConfig.u4VendorId != VENDOR_ID)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid "
                            "vendor id recvd"
                            "for rfmgmt Neigh config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid "
                              "vendor id recvd"
                              "for rfmgmt Neigh config msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2NeighborMsgPeriod
                 < RFMGMT_MIN_NEIGH_MSG_PERIOD) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2NeighborMsgPeriod
                 > RFMGMT_MAX_NEIGH_MSG_PERIOD))
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid Neighbor "
                            "Msg Period recvd for Rfmgmt "
                            "Neigh config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid Neighbor "
                              "Msg Period recvd for Rfmgmt "
                              "Neigh config msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2NeighborAgingPeriod
                 < RFMGMT_MIN_NEIGH_AGE_PERIOD) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2NeighborAgingPeriod
                 > RFMGMT_MAX_NEIGH_AGE_PERIOD))
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Neighbor aging "
                            "period recvd for Rfmgmt "
                            "Neigh config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Neighbor aging "
                              "period recvd for Rfmgmt "
                              "Neigh config msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2ChannelScanDuration
                 < RFMGMT_MIN_AUTO_SCAN_PERIOD) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2ChannelScanDuration
                 > RFMGMT_MAX_AUTO_SCAN_PERIOD))
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid scan "
                            "duration recvd for Rfmgmt "
                            "Neigh config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid scan "
                              "duration recvd for Rfmgmt "
                              "Neigh config msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                NeighApTableConfig.u1RadioId == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid radioid "
                            "recvd for Rfmgmt "
                            "Neigh config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid radioid "
                              "recvd for Rfmgmt "
                              "Neigh config msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u1AutoScanStatus
                 != RFMGMT_AUTO_SCAN_ENABLE) &&
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u1AutoScanStatus
                 != RFMGMT_AUTO_SCAN_DISABLE))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid auto scan "
                            "status recvd for Rfmgmt "
                            "Neigh config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid auto scan "
                              "status recvd for Rfmgmt "
                              "Neigh config msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
        }
            break;
        case VENDOR_CLIENT_CONFIG_TYPE:
        {
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ClientTableConfig.u2MsgEleLen
                != RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid msg "
                            "type recvd for Rfmgmt "
                            "Client config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid msg "
                              "type recvd for Rfmgmt "
                              "Client config msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ClientTableConfig.u4VendorId != VENDOR_ID)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                            "recvd for Rfmgmt "
                            "Client config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                              "recvd for Rfmgmt "
                              "Client config msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.u2SNRScanPeriod
                 < RFMGMT_MIN_SNR_SCAN_PERIOD) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.u2SNRScanPeriod
                 > RFMGMT_MAX_SNR_SCAN_PERIOD))
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid Snr Scan "
                            "Period recvd for Rfmgmt "
                            "Client config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid Snr Scan "
                              "Period recvd for Rfmgmt "
                              "Client config msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ClientTableConfig.u1RadioId == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid radioid "
                            "recvd for Rfmgmt "
                            "Client config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid radioid "
                              "recvd for Rfmgmt "
                              "Client config msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.u1SNRScanStatus
                 != RFMGMT_SNR_SCAN_ENABLE) &&
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.u1SNRScanStatus != RFMGMT_SNR_SCAN_DISABLE))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid Snr scan "
                            "status recvd for Rfmgmt "
                            "Client config msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid Snr scan "
                              "status recvd for Rfmgmt "
                              "Client config msg, Validation failed"));
                return RFMGMT_FAILURE;
            }

        }
            break;
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ChSwitchStatusTable.u2MsgEleLen
                != RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid msg type "
                            "recvd for Rfmgmt "
                            "Channel switch msg msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid msg type "
                              "recvd for Rfmgmt "
                              "Channel switch msg msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ChSwitchStatusTable.u4VendorId != VENDOR_ID)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                            "recvd for Rfmgmt "
                            "Channel switch msg msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                              "recvd for Rfmgmt "
                              "Channel switch msg msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ChSwitchStatusTable.u1RadioId == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid radioid "
                            "recvd for Rfmgmt "
                            "Channel switch msg msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid radioid "
                              "recvd for Rfmgmt "
                              "Channel switch msg msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ChSwitchStatusTable.u1ChSwitchStatus
                 != RFMGMT_CHANNEL_SWITCH_MSG_ENABLE) &&
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ChSwitchStatusTable.u1ChSwitchStatus
                 != RFMGMT_CHANNEL_SWITCH_MSG_DISABLE))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid channel "
                            "switch status recvd for Rfmgmt "
                            "Channel switch msg msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "switch status recvd for Rfmgmt "
                              "Channel switch msg msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
        }
            break;
        case VENDOR_SPECT_MGMT_TPC_TYPE:
        {
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.u2MsgEleLen
                != TPC_SPECT_MGMT_VENDOR_MSG_ELM_LEN)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid msg type "
                            "recvd for Rfmgmt "
                            "Vendor Spec Mgmt msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid msg type "
                              "recvd for Rfmgmt "
                              "Vendor Spec Mgmt msg, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.u4VendorId != VENDOR_ID)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                            "recvd for Rfmgmt "
                            "Vendor Spec Mgmt msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                              "recvd for Rfmgmt "
                              "Vendor Spec Mgmt msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.u1RadioId == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid radioid "
                            "recvd for Rfmgmt "
                            "Vendor Spec Mgmt msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid radioid "
                              "recvd for Rfmgmt "
                              "Vendor Spec Mgmt msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 TpcSpectMgmtTable.u2TpcRequestInterval
                 > RFMGMT_MAX_TPC_REQ_INTERVAL) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 TpcSpectMgmtTable.u2TpcRequestInterval
                 < RFMGMT_MIN_TPC_REQ_INTERVAL))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid TPC "
                            "Request Interval rcvd for Rfgmt "
                            "Vendor Spec Mgmt msg, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "Request Interval rcvd for Rfgmt "
                              "Vendor Spec Mgmt msg, Validation failed\n"));
                return RFMGMT_FAILURE;

            }
        }
            break;
        case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
        {
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u2MsgEleLen != SPECT_MGMT_VENDOR_DFS_MSG_ELM_LEN)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid msg type "
                            "recvd for Rfmgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid msg type "
                              "recvd for Rfmgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed"));
                return RFMGMT_FAILURE;

            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u4VendorId != VENDOR_ID)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                            "recvd for Rfmgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid vendor id "
                              "recvd for Rfmgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u1RadioId == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid radioid "
                            "recvd for Rfmgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid radioid "
                              "recvd for Rfmgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsQuietInterval
                 > RFMGMT_MAX_DFS_QUIET_INTERVAL) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsQuietInterval
                 < RFMGMT_MIN_DFS_QUIET_INTERVAL))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid TPC "
                            "Quiet Interval rcvd for Rfgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "Quiet Interval rcvd for Rfgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsQuietPeriod
                 > RFMGMT_MAX_DFS_QUIET_PERIOD) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsQuietPeriod < RFMGMT_MIN_DFS_QUIET_PERIOD))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid TPC "
                            "Quiet Period rcvd for Rfgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "Quiet Period rcvd for Rfgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsMeasurementInterval
                 > RFMGMT_MAX_DFS_MEASUREMENT_INTERVAL) ||
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsMeasurementInterval
                 < RFMGMT_MIN_DFS_MEASUREMENT_INTERVAL))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid TPC "
                            "Measurement Interval rcvd for Rfgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "Measurement Interval rcvd for Rfgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsChannelSwitchStatus
                 != RFMGMT_CHANNEL_SWITCH_MSG_ENABLE) &&
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u2DfsChannelSwitchStatus
                 != RFMGMT_CHANNEL_SWITCH_MSG_DISABLE))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid TPC "
                            "Channel Switch rcvd for Rfgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "Channel Switch rcvd for Rfgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u111hDfsStatus
                 != RFMGMT_DFS_ENABLE) &&
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 DfsParamsTable.u111hDfsStatus != RFMGMT_DFS_DISABLE))

            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtValidateConfigStatusReq: Invalid TPC "
                            "DFS Status rcvd for Rfgmt "
                            "Vendor Spec Mgmt msg DFS, Validation failed\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateConfigStatusReq: Invalid channel "
                              "DFS Status rcvd for Rfgmt "
                              "Vendor Spec Mgmt msg DFS, Validation failed\n"));
                return RFMGMT_FAILURE;
            }

        }
            break;
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalide msg recived\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Invalide msg recived"));
            return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtValidateScannedChannel                               */
/*                                                                           */
/* Description  : This functions validates the status request message        */
/*                from WTP                                                   */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtValidateScannedChannel (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1ConflictChannelIndex = 0;
    UINT1               u1Channel = 0;
    UINT1               u1RetVal = 0;
    tRadioIfGetDB       RadioGetDB;
    tSNMP_OCTET_STRING_TYPE ChannelList;
    UINT1               au1Dot11ChannelList[RFMGMT_MAX_CHANNELA];

    MEMSET (&au1Dot11ChannelList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (&ChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ChannelList.pu1_OctetList = au1Dot11ChannelList;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioGetDB, 0, sizeof (tRadioIfGetDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtScanChannelList.u2WtpInternalId;
    pWssIfCapwapDB->CapwapGetDB.u1RadioId =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtScanChannelList.u1RadioId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;;

    for (u1ConflictChannelIndex = 0;
         u1ConflictChannelIndex < RFMGMT_MAX_CHANNELA; u1ConflictChannelIndex++)
    {
        if (pWssMsgStruct->unRfMgmtMsg.
            RfMgmtScanChannelList.au1ScannedList[u1ConflictChannelIndex] == 0)
        {
            break;
        }
        else
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bGetAllowedList = OSIX_TRUE;
            u1RetVal =
                RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
            if (u1RetVal == RFMGMT_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtValidateScannedChannel :"
                            "RowStatus Set failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtValidateScannedChannel :"
                              "RowStatus Set failed"));
                return RFMGMT_FAILURE;
            }
            for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
            {

                if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    au1AllowedChannelList[u1Channel] ==
                    pWssMsgStruct->unRfMgmtMsg.RfMgmtScanChannelList.
                    au1ScannedList[u1ConflictChannelIndex])
                {
                    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                        au1AllowedChannelList[u1Channel] = 0;
                    break;
                }
            }
            u1RetVal =
                RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB);
            if (u1RetVal == RFMGMT_FAILURE)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
    }
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            au1ScannedChannel,
            pWssMsgStruct->unRfMgmtMsg.RfMgmtScanChannelList.au1ScannedList,
            RFMGMT_MAX_CHANNELA);
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bScannedChannel = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bGetAllowedList = OSIX_FALSE;
    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
        RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessConfigStatusReq : Getting "
                    "Rfmgmt index failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessConfigStatusReq : Getting "
                      "Rfmgmt index failed"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessConfigStatusReq                               */
/*                                                                           */
/* Description  : This functions process the status request message          */
/*                from WTP                                                   */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessConfigStatusReq (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtThresDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4Dot11RadioType = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtThresDB, 0, sizeof (tRfMgmtDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessConfigStatusReq : Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessConfigStatusReq : Null input received"));
        return RFMGMT_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    switch (pWssMsgStruct->u1Opcode)
    {
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
            /* Reset the flag received. This flag will be set by each attibute if
             * there is a conflict between the values stored in DB and input
             * received */
            pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                NeighApTableConfig.isOptional = OSIX_FALSE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                NeighApTableConfig.u1RadioId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "Radio index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "Radio index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }

            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "Radio type failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "Radio type failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;

            }

            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEA) ||
                (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEAN) ||
                (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEAC))
            {
                u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            }
            else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB) ||
                     (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBG) ||
                     (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBGN))
            {
                u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
            }
            else
            {
                u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
            }

            RfMgmtThresDB.unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = u4Dot11RadioType;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborMsgPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChannelScanDuration = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bAutoScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborAgingPeriod = OSIX_TRUE;
            if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "Rfmgmt index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
            RfMgmtThresDB.unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfIsSetDB.bRssiThreshold = OSIX_TRUE;
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2NeighborMsgPeriod) !=
                (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                 RfMgmtAPConfigDB.u2NeighborMsgPeriod))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    NeighApTableConfig.isOptional |= OSIX_TRUE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2NeighborAgingPeriod) !=
                (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                 RfMgmtAPConfigDB.u2NeighborAgingPeriod))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    NeighApTableConfig.isOptional |= OSIX_TRUE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u2ChannelScanDuration) !=
                (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                 RfMgmtAPConfigDB.u2ChannelScanDuration))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    NeighApTableConfig.isOptional |= OSIX_TRUE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.u1AutoScanStatus) !=
                (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                 RfMgmtAPConfigDB.u1AutoScanStatus))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    NeighApTableConfig.isOptional |= OSIX_TRUE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 NeighApTableConfig.i2RssiThreshold) !=
                (RfMgmtThresDB.unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.i2RssiThreshold))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    NeighApTableConfig.isOptional |= OSIX_TRUE;
            }
        }
            break;
        case VENDOR_CLIENT_CONFIG_TYPE:
        {
            /* Reset the flag received. This flag will be set by each attibute if
             * there is a conflict between the values stored in DB and input
             * received */
            pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ClientTableConfig.isOptional = OSIX_FALSE;

            /* To get radio if index */
            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ClientTableConfig.u1RadioId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "Radio index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "Radio index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            /* To get client config params */
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex =
                pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus = OSIX_TRUE;

            if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting Rfmgmt "
                            "index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting Rfmgmt "
                              "index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }

            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "Radio type failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "Radio type failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;

            }

            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEA) ||
                (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEAN) ||
                (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEAC))
            {
                u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            }
            else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB) ||
                     (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBG) ||
                     (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBGN))
            {
                u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
            }
            else
            {
                u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
            }
            RfMgmtThresDB.unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = u4Dot11RadioType;

            RfMgmtThresDB.unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfIsSetDB.bSNRThreshold = OSIX_TRUE;
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.u2SNRScanPeriod) !=
                (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                 RfMgmtClientConfigDB.u2SNRScanPeriod))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    ClientTableConfig.isOptional |= OSIX_TRUE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.u1SNRScanStatus) !=
                (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                 RfMgmtClientConfigDB.u1SNRScanStatus))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    ClientTableConfig.isOptional |= OSIX_TRUE;
            }
            if ((pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                 ClientTableConfig.i2SNRThreshold) !=
                (RfMgmtThresDB.unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.i2SNRThreshold))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    ClientTableConfig.isOptional |= OSIX_TRUE;
            }
        }
            break;
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
            /* Reset the flag received. This flag will be set by each attibute if
             * there is a conflict between the values stored in DB and input
             * received */
            pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ChSwitchStatusTable.isOptional = OSIX_FALSE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ChSwitchStatusTable.u1RadioId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting Radio "
                            "index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting Radio "
                              "index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                ChSwitchStatusTable.u1ChSwitchStatus
                != (gu4ChannelSwitchMsgStatus))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    ChSwitchStatusTable.isOptional |= OSIX_TRUE;
            }
        }
            break;
        case VENDOR_SPECT_MGMT_TPC_TYPE:
        {
            /* Reset the flag received. This flag will be set by each attibute if
             * there is a conflict between the values stored in DB and input
             * received */
            pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.isOptional = OSIX_FALSE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.u1RadioId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting Radio "
                            "index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting Radio "
                              "index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hTpcRequestInterval = OSIX_TRUE;

            if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "Tpc Request Interval failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "Tpc Request Interval failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }

            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.u2TpcRequestInterval
                != (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    TpcSpectMgmtTable.isOptional |= OSIX_TRUE;
            }
            MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
            RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcStatus = OSIX_TRUE;
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                TpcSpectMgmtTable.u111hTpcStatus
                != (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    TpcSpectMgmtTable.isOptional |= OSIX_TRUE;
            }
        }
            break;
        case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
        {
            /* Reset the flag received. This flag will be set by each attibute if
             * there is a conflict between the values stored in DB and input
             * received */
            pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.isOptional = OSIX_FALSE;

            pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u1RadioId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u1RadioId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting Radio "
                            "index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting Radio "
                              "index failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsQuietInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;

            if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusReq : Getting "
                            "DFS Request Parameters failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusReq : Getting "
                              "DFS Request Parameters Interval failed"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }

            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u2DfsQuietInterval
                != (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    DFSChannelTable.isOptional |= OSIX_TRUE;
            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u2DfsQuietPeriod
                != (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    DFSChannelTable.isOptional |= OSIX_TRUE;
            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u2DfsMeasurementInterval
                != (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    DFSChannelTable.isOptional |= OSIX_TRUE;
            }
            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u2DfsChannelSwitchStatus
                != (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    DFSChannelTable.isOptional |= OSIX_TRUE;
            }
            MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
            RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hDfsStatus = OSIX_TRUE;

            if (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                DfsParamsTable.u111hDfsStatus
                != (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus))
            {
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.
                    DFSChannelTable.isOptional |= OSIX_TRUE;
            }

        }
            break;

        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalide msg recived\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Invalide msg recived"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return RFMGMT_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessNeighConfig                                   */
/*                                                                           */
/* Description  : Process the config update request                          */
/*                for neig config params                                     */
/*                                                                           */
/* Input        : pRfMgmtDB - rfmgmt DB structure                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessNeighConfig (tRfMgmtDB * pRfMgmtDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RetVal = OSIX_SUCCESS;
    tRfMgmtDB           RfMgmtDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    RFMGMT_FN_ENTRY ();
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    if (pRfMgmtDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighConfig: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    gu1IsRfMgmtResponseReceived = RFMGMT_INVALID_RESPONSE;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighConfig: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;

    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bNeighborMsgPeriod = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bChannelScanDuration = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bAutoScanStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bNeighborAgingPeriod = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighConfig: Rfmgmt DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
        u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
        u2MsgEleType = NEIGH_CONFIG_VENDOR_MSG;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
        u2MsgEleLen = RF_NEIGH_CONFIG_VENDOR_MSG_ELM_LEN;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
        isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bNeighborMsgPeriod != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u2NeighborMsgPeriod = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2NeighborMsgPeriod;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u2NeighborMsgPeriod = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2NeighborMsgPeriod;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bChannelScanDuration != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u2ChannelScanDuration = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2ChannelScanDuration;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u2ChannelScanDuration = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2ChannelScanDuration;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bAutoScanStatus != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u1AutoScanStatus = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u1AutoScanStatus;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u1AutoScanStatus = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u1AutoScanStatus;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bNeighborAgingPeriod != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u2NeighborAgingPeriod = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2NeighborAgingPeriod;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.
            u2NeighborAgingPeriod = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2NeighborAgingPeriod;
    }

    u1RetVal =
        (UINT1) WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);

    if (u1RetVal == RFMGMT_NO_AP_PRESENT)
    {
        gu1IsRfMgmtResponseReceived = RFMGMT_NO_AP_PRESENT;
    }
    else if (u1RetVal != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateReq:To change Neigh config info in AP failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateReq:To change Neigh config info in AP failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

#ifdef ROGUEAP_WANTED
/*****************************************************************************/
/* Function     : RfMgmtRogueProcessConfig                                   */
/*                                                                           */
/* Description  : Process the Rogue Detection Configuratins                  */
/*                                                                           */
/*                                                                           */
/* Input        : pu1RfGroupName - RF Group name                             */
/*                u1RogueDetection - Rogue Detection Variable                */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtRogueProcessConfig (UINT1 *pu1RfGroupName, UINT1 u1RogueDetection)
{
    UINT1               u1RetVal = OSIX_SUCCESS;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT4               u4currentProfileId = 0;
    UINT4               u4nextProfileId = 0;
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RFMGMT_FN_ENTRY ();

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (nmhGetFirstIndexCapwapBaseWtpProfileTable
        (&u4nextProfileId) != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    do
    {
        u4currentProfileId = u4nextProfileId;

        /* Get the radio ifindex from the input structure */
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4currentProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {

            continue;
        }
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = 1;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtDeleteClientStats :"
                        "Get radio Index failed\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
            == RFMGMT_FAILURE)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }

        MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.
            u4VendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.
            u2MsgEleType = ROGUE_CONFIG_VENDOR_MSG;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.
            u2MsgEleLen = RF_ROGUE_CONFIG_VENDOR_MSG_ELM_LEN;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.
            u1RfDetection = u1RogueDetection;
        MEMCPY (pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.
                u1RfGroupName, pu1RfGroupName, CAPWAP_RF_GROUP_NAME_SIZE);
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.NeighApTableConfig.u1RadioId =
            1;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.RogueMgmt.isOptional =
            OSIX_TRUE;

        u1RetVal =
            (UINT1) WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
                                            pWlcHdlrMsgStruct);

        if (u1RetVal == OSIX_FAILURE)
        {
            continue;
        }

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable
           (u4currentProfileId, &u4nextProfileId) == SNMP_SUCCESS);

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;

}
#endif

/*****************************************************************************/
/* Function     : RfMgmtProcessTpcSpectMgmt                                  */
/*                                                                           */
/* Description  : Process the config update request                          */
/*                for Spectrum Mgmt params                                   */
/*                                                                           */
/* Input        : pRfMgmtDB - rfmgmt DB structure                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessTpcSpectMgmt (tRfMgmtDB * pRfMgmtDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT2               u211hTpcRequestInterval = 0;

    RFMGMT_FN_ENTRY ();
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTpcSpectMgmt:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    if (pRfMgmtDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTpcSpectMgmt: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessTpcSpectMgmt: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    gu1IsRfMgmtResponseReceived = RFMGMT_INVALID_RESPONSE;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTpcSpectMgmt: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessTpcSpectMgmt: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }
    if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType != RFMGMT_RADIO_TYPEA) &&
        (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType != RFMGMT_RADIO_TYPEAN)
        && (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType !=
            RFMGMT_RADIO_TYPEAC))
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_SUCCESS;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hTpcRequestInterval = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTpcSpectMgmt: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessTpcSpectMgmt: Rfmgmt DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }
    u211hTpcRequestInterval = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcStatus = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTpcSpectMgmt: Tpc status fetch failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessTpcSpectMgmt: Tpc Status fetch failed\r\n"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
        u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
        u2MsgEleType = VENDOR_SPECT_MGMT_TPC_MSG;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
        u2MsgEleLen = TPC_SPECT_MGMT_VENDOR_MSG_ELM_LEN;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
        isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hTpcRequestInterval != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
            u2TpcRequestInterval = u211hTpcRequestInterval;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
            u2TpcRequestInterval = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcStatus != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
            u111hTpcStatus = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.TpcSpectMgmtTable.
            u111hTpcStatus = pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus;
    }

    u1RetVal =
        (UINT1) WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);

    if (u1RetVal == RFMGMT_NO_AP_PRESENT)
    {
        gu1IsRfMgmtResponseReceived = RFMGMT_NO_AP_PRESENT;
    }
    else if (u1RetVal != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateReq:To change Spect Mgmt info in AP failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateReq:To change Spect Mgmt info in AP failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessDfsParams                     */
/*                                                                           */
/* Description  : Process the config update request                          */
/*                for DFS params                                           */
/*                                                                           */
/* Input        : pRfMgmtDB - rfmgmt DB structure                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessDfsParams (tRfMgmtDB * pRfMgmtDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RetVal = OSIX_SUCCESS;
    tRfMgmtDB           RfMgmtDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT2               u211hDfsQuietInterval;
    UINT2               u211hDfsQuietPeriod;
    UINT2               u211hDfsMeasurementInterval;
    UINT2               u211hDfsChannelSwitchStatus;

    RFMGMT_FN_ENTRY ();
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessDfsParams:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    if (pRfMgmtDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessDfsParams: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessDfsParams: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    gu1IsRfMgmtResponseReceived = RFMGMT_INVALID_RESPONSE;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessDfsParams: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessDfsParams: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;

    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsQuietInterval = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessDfsParams: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessDfsParams: Rfmgmt DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    u211hDfsQuietInterval = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval;
    u211hDfsQuietPeriod = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod;
    u211hDfsMeasurementInterval = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval;
    u211hDfsChannelSwitchStatus = RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsStatus = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTpcSpectMgmt: Tpc status fetch failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessTpcSpectMgmt: Tpc Status fetch failed\r\n"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
        u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
        u2MsgEleType = VENDOR_SPECTRUM_MGMT_DFS_MSG;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
        u2MsgEleLen = SPECT_MGMT_VENDOR_DFS_MSG_ELM_LEN;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
        isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsQuietInterval != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.u2DfsQuietInterval = u211hDfsQuietInterval;    /*RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                                                                                               RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval; */
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
            u2DfsQuietInterval = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsQuietPeriod != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.u2DfsQuietPeriod = u211hDfsQuietPeriod;    /*RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                                                                                           RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod; */
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
            u2DfsQuietPeriod = pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsMeasurementInterval != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.u2DfsMeasurementInterval = u211hDfsMeasurementInterval;    /*RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                                                                                                           RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval; */
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
            u2DfsMeasurementInterval =
            pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u2RfMgmt11hDfsMeasurementInterval;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bRfMgmt11hDfsChannelSwitchStatus != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.u2DfsChannelSwitchStatus = u211hDfsChannelSwitchStatus;    /*RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                                                                                                           RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus; */
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
            u2DfsChannelSwitchStatus =
            pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u2RfMgmt11hDfsChannelSwitchStatus;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsStatus != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
            u111hDfsStatus = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.DfsParamsTable.
            u111hDfsStatus = pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus;
    }
    u1RetVal =
        (UINT1) WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);

    if (u1RetVal == RFMGMT_NO_AP_PRESENT)
    {
        gu1IsRfMgmtResponseReceived = RFMGMT_NO_AP_PRESENT;
    }
    else if (u1RetVal != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateReq:To change DFS Params info in AP failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateReq:To change DFS Params info in AP failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessClientConfig                                  */
/*                                                                           */
/* Description  : Process the config update request                          */
/*                for client config params                                   */
/*                                                                           */
/* Input        : pRfMgmtDB - rfmgmt DB structure                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessClientConfig (tRfMgmtDB * pRfMgmtDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RetVal = OSIX_SUCCESS;
    tRfMgmtDB           RfMgmtDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    RFMGMT_FN_ENTRY ();
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientConfig:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    gu1IsRfMgmtResponseReceived = RFMGMT_INVALID_RESPONSE;

    if (pRfMgmtDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientConfig: Invalid DB\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientConfig: Invalid DB"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientConfig: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientConfig: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;

    }

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4RadioIfIndex = pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanPeriod = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientConfig: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientConfig: Rfmgmt DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u2MsgEleType = CLIENT_CONFIG_VENDOR_MSG;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u2MsgEleLen = RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanPeriod != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
            u2SNRScanPeriod = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u2SNRScanPeriod;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
            u2SNRScanPeriod = pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u2SNRScanPeriod;
    }
    if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanStatus != OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
            u1SNRScanStatus = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u1SNRScanStatus;
    }
    else
    {
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
            u1SNRScanStatus = pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u1SNRScanStatus;
    }

    u1RetVal =
        (UINT1) WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);

    if (u1RetVal == RFMGMT_NO_AP_PRESENT)
    {
        gu1IsRfMgmtResponseReceived = RFMGMT_NO_AP_PRESENT;
    }
    else if (u1RetVal != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateReq:To change Client config info in AP failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateReq:To change Client config info in AP failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessBssidScanStaus                                */
/*                                                                           */
/* Description  : Process the config update request                          */
/*                for client config params                                   */
/*                                                                           */
/* Input        : pRfMgmtDB - rfmgmt DB structure                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessBssidScanStatus (tRfMgmtDB * pRfMgmtDB)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RetVal = OSIX_SUCCESS;
    UINT1               u1WlanId = 0;
    tRfMgmtDB           RfMgmtDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    RFMGMT_FN_ENTRY ();
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessBssidScanStatus:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    gu1IsRfMgmtResponseReceived = RFMGMT_INVALID_RESPONSE;

    if (pRfMgmtDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessBssidScanStatus invalid input\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessBssidScanStatus invalid input"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
        RfMgmtBssidScanDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientConfig: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientConfig: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;

    }

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4RadioIfIndex = pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
        RfMgmtBssidScanDB.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanPeriod = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessBssidScanStatus: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessBssidScanStatus: Rfmgmt DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u2MsgEleType = CLIENT_CONFIG_VENDOR_MSG;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u2MsgEleLen = RF_CLIENT_CONFIG_VENDOR_MSG_ELM_LEN;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u1SNRScanStatus = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u1SNRScanStatus;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
        u2SNRScanPeriod = RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2SNRScanPeriod;

    if (pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanIsSetDB.
        bAutoScanStatus != OSIX_FALSE)
    {
        u1WlanId = pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
            RfMgmtBssidScanDB.u1WlanID;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
            u1WlanId = u1WlanId;
        pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ClientTableConfig.
            u1BssidScanStatus[u1WlanId - 1] = pRfMgmtDB->unRfMgmtDB.
            RfMgmtBssidScanTable.RfMgmtBssidScanDB.u1AutoScanStatus;
    }
    u1RetVal = (UINT1) WssIfProcessWlcHdlrMsg
        (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ, pWlcHdlrMsgStruct);

    if (u1RetVal == RFMGMT_NO_AP_PRESENT)
    {
        gu1IsRfMgmtResponseReceived = RFMGMT_NO_AP_PRESENT;
    }
    else if (u1RetVal != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateReq:To bssid scan status in AP failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateReq:To bssid scan status in AP failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessChSwitchMsg                                   */
/*                                                                           */
/* Description  : Process the config update request                          */
/*                for channel switch msg                                     */
/*                                                                           */
/* Input        : i4CurrenIfIndex                                            */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessChSwitchMsg (INT4 i4CurrentIfIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RetVal = OSIX_SUCCESS;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessChSwitchMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    RFMGMT_FN_ENTRY ();
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    gu1IsRfMgmtResponseReceived = RFMGMT_INVALID_RESPONSE;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4CurrentIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighConfig: RadioIf DB access failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;

    }
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ChSwitchStatusTable.
        u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ChSwitchStatusTable.
        u2MsgEleType = CH_SWITCH_STATUS_VENDOR_MSG;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ChSwitchStatusTable.
        u2MsgEleLen = RF_CH_SWITCH_STATUS_VENDOR_MSG_ELM_LEN;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ChSwitchStatusTable.
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ChSwitchStatusTable.
        isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->RfMgmtConfigUpdateReq.ChSwitchStatusTable.
        u1ChSwitchStatus = (UINT1) gu4ChannelSwitchMsgStatus;

    u1RetVal =
        (UINT1) WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RFMGMT_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);

    if (u1RetVal == RFMGMT_NO_AP_PRESENT)
    {
        gu1IsRfMgmtResponseReceived = RFMGMT_NO_AP_PRESENT;
    }
    else if (u1RetVal != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateReq:To change channel switch msg in AP failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateReq:To change channel switch msg in AP failed"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return RFMGMT_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessConfigUpdateRsp                               */
/*                                                                           */
/* Description  : This functions process the update resp message             */
/*                from WTP                                                   */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessConfigUpdateRsp (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;

    RFMGMT_FN_ENTRY ();
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateRsp: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateRsp: Null input received"));
        return OSIX_FAILURE;
    }

    gu1IsRfMgmtResponseReceived = (UINT1)
        pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateRsp.u4ResultCode;

    if ((gu1IsRfMgmtResponseReceived == OSIX_FAILURE) ||
        (gu1IsRfMgmtResponseReceived == RFMGMT_RESPONSE_TIMEOUT))
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "ConfigUpdateRsp : Configuration failed at WTP\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "ConfigUpdateRsp : Configuration failed at WTP"));
        return RFMGMT_FAILURE;
    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtHandleNewAPRadioEntry                                */
/*                                                                           */
/* Description  : This functions gets invoked when a notification comes from */
/*                CAPWAP module once an AP reached run state. If DCA is      */
/*                enabled then algorithm should be executed and a channel    */
/*                needs to be assigned to AP                                 */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtHandleNewAPRadioEntry (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtUpdateDB;
    UINT4               u4Dot11RadioType = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2NeighId = 0;
    UINT1               u1DcaMode = 0;
    UINT1               u1DcaSelection = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RadioIndex = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleNewAPRadioEntry: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtHandleNewAPRadioEntry: Null input received"));
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.b11hDfsStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleNewAPRadioEntry: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtHandleNewAPRadioEntry: RadioIf DB access failed"));
        return RFMGMT_FAILURE;
    }

    /* Pass the WTP Internal Id */
    u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    RfmgmtGetWtpIndex (RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId,
                       RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                       &u2WtpInternalId);

    for (u2NeighId = 0; u2NeighId < RFMGMT_MAX_RADIOS; u2NeighId++)
    {
        MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2NeighId],
                0, sizeof (tDcaAlgorithm));
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRadioMacAddr = OSIX_TRUE;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelAssignmentMode = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleNewAPRadioEntry : Getting Rfmgmt "
                    "index failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtHandleNewAPRadioEntry : Getting Rfmgmt "
                      "index failed"));
        return RFMGMT_FAILURE;
    }

    if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA) ||
        (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
        || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
            RFMGMT_RADIO_TYPEAC))
    {
        u1RadioIndex = RFMGMT_OFFSET_ZERO;
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
    }
    else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
              RFMGMT_RADIO_TYPEB) ||
             (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
              RFMGMT_RADIO_TYPEBG) ||
             (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
              RFMGMT_RADIO_TYPEBGN))
    {
        u1RadioIndex = RFMGMT_OFFSET_ONE;
        u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
    }
    else
    {
        u1RadioIndex = RFMGMT_OFFSET_TWO;
        u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
    }

    for (u2NeighId = 0; u2NeighId < RFMGMT_MAX_RADIOS; u2NeighId++)
    {
        MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2NeighId],
                0, sizeof (tDcaAlgorithm));
    }

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = u4Dot11RadioType;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bRowStatus = OSIX_TRUE;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaMode = OSIX_TRUE;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaSelection = OSIX_TRUE;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bAllowedChannelList = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtUpdateDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtHandleNewAPRadioEntry : RRM Config "
                    "table does not exist\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtHandleNewAPRadioEntry : RRM Config "
                      "table does not exist"));
        return RFMGMT_SUCCESS;
    }

    u1DcaMode = RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaMode;

    u1DcaSelection = RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaSelection;

    if (((u1DcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
         (u1DcaSelection == RFMGMT_DCA_SELECTION_OFF)) ||
        ((u1DcaMode == RFMGMT_DCA_MODE_PER_AP) &&
         (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
          u1ChannelAssignmentMode == RFMGMT_PER_AP_DCA_MANUAL)))
    {
        return RFMGMT_SUCCESS;
    }
    gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
        u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

    gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
        u1IsRunStateReached = OSIX_TRUE;
    MEMCPY (gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
            MacAddr, RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            RadioMacAddr, sizeof (tMacAddr));

    if (u4Dot11RadioType == RFMGMT_RADIO_TYPEA ||
        (u4Dot11RadioType == RFMGMT_RADIO_TYPEAN) ||
        (u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4RadioIfIndex =
            pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet = OSIX_TRUE;

        if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtHandleNewAPRadioEntry : Getting Rfmgmt "
                        "index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtHandleNewAPRadioEntry : Getting Rfmgmt "
                          "index failed"));
            return RFMGMT_FAILURE;
        }
        if (RfMgmtRunDFSAlgorithm (&RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
                                   RfMgmtAutoRfProfileDB,
                                   &RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                   RfMgmtAPConfigDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtHandleNewAPRadioEntry: RunDFSAlgo returned failure\r\n");
        }
    }

    if (RfMgmtRunDcaAlgorithm (&RfMgmtUpdateDB.unRfMgmtDB.RfMgmtAutoRfTable.
                               RfMgmtAutoRfProfileDB, 0) != RFMGMT_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /*The following called function handles all the parameters that are to be 
     * sent in config update request soon after run state */

    if (RfMgmtTriggerConfigUpdate (pWssMsgStruct->unRfMgmtMsg.
                                   RfMgmtIntfConfigReq.u4RadioIfIndex) !=
        RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtTriggerConfigUpdate returns failure\r\n");
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtRunTpcAlgorithm                                      */
/*                                                                           */
/* Description  : This functions checks the DB and run the tx power control  */
/*                algorithm                                                  */
/*                                                                           */
/* Input        : pRfMgmtAutoRfProfileDB - Pointer to the input structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtRunTpcAlgorithm (tRfMgmtAutoRfProfileDB * pRfMgmtAutoRfProfileDB)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtAPDB;
    tRfMgmtDB           RfMgmtUpdateDB;
    tRadioIfGetDB       RadioIfGetDB;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4Dot11RadioType = 0;
    UINT1               u1Count = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1PerAPTPCMode = 0;

    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtAPDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RFMGMT_FN_ENTRY ();

    /* Check whether the entry is present in DB for the received 
     * radio type. If no entry available then return. */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_FIRST_CLIENT_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_TPC_TRC,
                    "RfMgmtRunTpcAlgorithm: No entry found in DB. "
                    "Algorithm not executed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRunTpcAlgorithm: No entry found in DB. "
                      "Algorithm not executed"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_SUCCESS;
    }

    do
    {
        /* Check TPC selecion mode. If manual then skip the entry */
        RfMgmtAPDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u4RadioIfIndex =
            RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
            u4RadioIfIndex;
        RfMgmtAPDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
            bPerAPTpcSelection = OSIX_TRUE;

        u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtAPDB);
        if (u1RetVal == RFMGMT_FAILURE)
        {
            continue;
        }
        u1PerAPTPCMode = (INT4) RfMgmtAPDB.unRfMgmtDB.
            RfMgmtApConfigTable.RfMgmtAPConfigDB.u1PerAPTpcSelection;

        if (((pRfMgmtAutoRfProfileDB->u1TpcMode == RFMGMT_TPC_MODE_GLOBAL) &&
             (pRfMgmtAutoRfProfileDB->u1TpcSelection ==
              RFMGMT_TPC_SELECTION_OFF)) ||
            ((pRfMgmtAutoRfProfileDB->u1TpcMode == RFMGMT_TPC_MODE_PER_AP) &&
             (u1PerAPTPCMode == RFMGMT_PER_AP_TPC_MANUAL)))
        {
            continue;
        }

        /* Check whether the radio ifindex entry is present in the DB. If
         * present then get the radio type from DB */
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
            u4RadioIfIndex;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            continue;
        }

        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEA) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEAN) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEAC))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
        }
        else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEB) ||
                 (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBG) ||
                 (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBGN))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
        }
        else
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
        }

        if (u4Dot11RadioType != pRfMgmtAutoRfProfileDB->u4Dot11RadioType)
        {
            continue;
        }
        ++u1Count;

        if (u4RadioIfIndex == RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex)
        {
            RFMGMT_TRC (RFMGMT_TPC_TRC,
                        "RfMgmtRunTpcAlgorithm: Tx Power already increased for "
                        "the AP Radio\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtRunTpcAlgorithm: Tx Power already increased for "
                          "the AP Radio"));
            continue;
        }
        u4RadioIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

        RFMGMT_TRC (RFMGMT_TPC_TRC,
                    "RfMgmtRunTpcAlgorithm: Client entries available in DB. "
                    "Trigger the algorithm\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRunTpcAlgorithm: Client entries available in DB. "
                      "Trigger the algorithm"));

        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u4RadioIfIndex = RfMgmtDB.unRfMgmtDB.
            RfMgmtClientScanTable.RfMgmtClientScanDB.u4RadioIfIndex;

        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

        if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                                &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_TPC_TRC,
                        "RfMgmtRunTpcAlgorithm: No entry found in DB. "
                        "Algorithm not executed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtRunTpcAlgorithm: No entry found in DB. "
                          "Algorithm not executed"));
            continue;
        }

        /* Check the value of tx power. If max power is already reached then
         * throw error */
        if (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u2TxPowerLevel <= RFMGMT_MAX_TXPOWER_LEVEL)
        {
            RFMGMT_TRC (RFMGMT_TPC_TRC,
                        "RfMgmtRunTpcAlgorithm: MAX Tx Power reached. "
                        "Cannot run the algorithm\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtRunTpcAlgorithm: MAX Tx Power reached. "
                          "Cannot run the algorithm"));
            continue;
        }
        /* Update the Radio intf structure with algorithm assigned tx
         * power. Update the tx power in radio module and once success update
         * the same in RFMGMT DB */
        if (RfMgmtUpdateTxPower (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                                 RfMgmtClientScanDB.u4RadioIfIndex,
                                 --(RfMgmtUpdateDB.unRfMgmtDB.
                                    RfMgmtClientConfigTable.
                                    RfMgmtClientConfigDB.u2TxPowerLevel)) ==
            RFMGMT_FAILURE)
        {
            RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                         "RfMgmtRunTpcAlgorithm: Power change to the Radio DB "
                         "failed %d\n", RadioIfMsgStruct.unRadioIfMsg.
                         RadioIfDcaTpcUpdate.u4IfIndex);
            continue;
        }
        MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    }
    while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_CLIENT_ENTRY,
                               &RfMgmtDB) == RFMGMT_SUCCESS);

    if (u1Count == 0)
    {
        RFMGMT_TRC (RFMGMT_TPC_TRC,
                    "RfMgmtRunTpcAlgorithm: No entry available for the radio type. "
                    "Algorithm not executed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRunTpcAlgorithm: No entry available for the radio type."
                      "Algorithm not executed"));
    }
    RFMGMT_FN_EXIT ();

    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtRunDpaAlgorithm                                      */
/*                                                                           */
/* Description  : This functions checks the DB and runs the Dynamic power    */
/*                adjustment algorithm                                       */
/*                                                                           */
/* Input        : pRfMgmtAutoRfProfileDB - Pointer to the input structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtRunDpaAlgorithm (tRfMgmtAutoRfProfileDB * pRfMgmtAutoRfProfileDB)
{
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB  *pRfMgmtAPNextConfigDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtDB           RfMgmtUpdateDB;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tRfMgmtNeighborScanTable RfMgmtNeighborScanTable;
    tRfMgmtNeighborScanTable RfMgmtNextNeighborScanTable;
    tRfMgmtNeighborScanTable *pRfMgmtNeighborScanTable = NULL;
    tRfMgmtNeighborScanTable *pRfMgmtNextNeighborScanTable = NULL;
    UINT1               u1Count = 0;
    UINT2               u2NewTxPowerLevel = 0;
    UINT4               u4Dot11RadioType = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2Index = 0;
    UINT2               u2Index_1 = 0;
    /*As per the algorithm, we need the third highest RSSI. 
       Change this to the number according to the req. */
    UINT1               u1TargetIndex = RFMGMT_OFFSET_THREE;
    UINT1               u1NeighborCount = 0;
    UINT1               u1PerAPDpaMode = 0;
    UINT1               u1RetVal = 0;
    INT2                i2TargetRssi = 0;
    INT2                i2Tmp = 0;
    INT2                ai2TmpRssi[RFMGMT_MAX_NEIGHBOR];
    INT2                i2TransmitPower = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtNeighborScanTable, 0, sizeof (tRfMgmtNeighborScanTable));
    MEMSET (&RfMgmtNextNeighborScanTable, 0, sizeof (tRfMgmtNeighborScanTable));
    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (ai2TmpRssi, 0, RFMGMT_MAX_NEIGHBOR);

    RFMGMT_FN_ENTRY ();

    pRfMgmtAPNextConfigDB = RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                            RfMgmtNeighborAPConfigDB);

    if (pRfMgmtAPNextConfigDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_DPA_TRC,    /* Change Dpa Trace full func */
                    "RfMgmtRunDpaAlgorithm: No entry found in DB. " "Algorithm not executed\n");    /*Change trace messages - full funct */
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRunDpaAlgorithm: No entry found in DB. "
                      "Algorithm not executed"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_SUCCESS;
    }

    do
    {
        pRfMgmtAPConfigDB = pRfMgmtAPNextConfigDB;

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRfMgmtAPConfigDB->u4RadioIfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            continue;
        }

        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEA) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEAN) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEAC))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
        }
        else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEB) ||
                 (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBG) ||
                 (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBGN))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
        }
        else
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
        }

        if (u4Dot11RadioType != pRfMgmtAutoRfProfileDB->u4Dot11RadioType)
        {
            /* If the radio type is different then skip the algorithm execution
             * for the AP. */
            continue;
        }
        ++u1Count;                /* To check whether the Algorithm is executed or not */

        u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        RfmgmtGetWtpIndex (RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId,
                           RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                           &u2WtpInternalId);

        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u4RadioIfIndex = pRfMgmtAPConfigDB->u4RadioIfIndex;
        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
            bPerAPTpcSelection = OSIX_TRUE;

        u1RetVal =
            RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtUpdateDB);
        if (u1RetVal == RFMGMT_FAILURE)
        {
            continue;
        }
        u1PerAPDpaMode = (INT4) RfMgmtUpdateDB.unRfMgmtDB.
            RfMgmtApConfigTable.RfMgmtAPConfigDB.u1PerAPTpcSelection;

        if (((pRfMgmtAutoRfProfileDB->u1TpcMode == RFMGMT_TPC_MODE_GLOBAL) &&
             (pRfMgmtAutoRfProfileDB->u1TpcSelection ==
              RFMGMT_TPC_SELECTION_OFF)) ||
            ((pRfMgmtAutoRfProfileDB->u1TpcMode == RFMGMT_TPC_MODE_PER_AP) &&
             (u1PerAPDpaMode == RFMGMT_PER_AP_TPC_MANUAL)))
        {
            continue;
        }

        RfMgmtGetCapwapProfileId (RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId,
                                  &u2WtpInternalId);

        RFMGMT_TRC2 (RFMGMT_DPA_TRC,
                     "DPA Algorithm: For AP Profile %d Radio Id %d\n",
                     u2WtpInternalId, RadioIfGetDB.RadioIfGetAllDB.u1RadioId);

        /* Check the neighbor AP count in DB. If the number of neighbors are
         * < 3, then set the tx power level to 1. Else check the result of
         * equation and increase/decrease the tx power accordingly */
        RfMgmtNextNeighborScanTable.RfMgmtNeighborScanDB.u4RadioIfIndex =
            pRfMgmtAPConfigDB->u4RadioIfIndex;

        pRfMgmtNeighborScanTable = &RfMgmtNextNeighborScanTable;

        /*Get next - RSSI, 2nd index */
        while ((pRfMgmtNextNeighborScanTable =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) pRfMgmtNeighborScanTable,
                               NULL)) != NULL)
        {
            pRfMgmtNeighborScanTable = pRfMgmtNextNeighborScanTable;

            if (pRfMgmtAPConfigDB->u4RadioIfIndex !=
                pRfMgmtNeighborScanTable->RfMgmtNeighborScanDB.u4RadioIfIndex)
            {
                break;
            }

            RFMGMT_TRC2 (RFMGMT_DPA_TRC,
                         "Power Threshold - %d and neighbor msg - %d\n",
                         pRfMgmtAutoRfProfileDB->i2PowerThreshold,
                         pRfMgmtNeighborScanTable->RfMgmtNeighborScanDB.i2Rssi);
            /* If the RSSI value is lesser than the power threshold,
             * then considet the AP as neighbor. */
            if (pRfMgmtAutoRfProfileDB->i2PowerThreshold <=
                pRfMgmtNeighborScanTable->RfMgmtNeighborScanDB.i2Rssi)
            {
                ai2TmpRssi[u1NeighborCount] = pRfMgmtNeighborScanTable->
                    RfMgmtNeighborScanDB.i2Rssi;
                u1NeighborCount++;
            }
        }
        for (u2Index = 0; u2Index < u1TargetIndex; u2Index++)
        {
            for (u2Index_1 = 0; u2Index_1 < (u1NeighborCount - u2Index - 1);
                 u2Index_1++)
            {
                if (ai2TmpRssi[u2Index_1] < ai2TmpRssi[u2Index_1 + 1])
                {
                    i2Tmp = ai2TmpRssi[u2Index_1];
                    ai2TmpRssi[u2Index_1] = ai2TmpRssi[u2Index_1 + 1];
                    ai2TmpRssi[u2Index_1 + 1] = i2Tmp;
                }
            }
        }
        /*Since the RSSI of 3rd neighbor will be present in ai2TmpRssi[2] */
        i2TargetRssi = ai2TmpRssi[u1TargetIndex - 1];
        if (u1NeighborCount < u1TargetIndex)
        {
            u2NewTxPowerLevel = 1;
            RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u2TxPowerLevel = u2NewTxPowerLevel;

            RFMGMT_TRC1 (RFMGMT_DPA_TRC,
                         "Increase the Tx Power level to %d (max) \r\n",
                         u2NewTxPowerLevel);

            /*Function to update power and handle trap */
            RfMgmtUpdateTxPower (pRfMgmtAPConfigDB->u4RadioIfIndex,
                                 u2NewTxPowerLevel);
            u1NeighborCount = 0;
            continue;
        }

        u1NeighborCount = 0;
        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u4RadioIfIndex =
            pRfMgmtAPConfigDB->u4RadioIfIndex;

        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

        /*New RF DB addition - bCurrentTxPower & u2CurrentTxPower */
        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigIsSetDB.bCurrentTxPower = OSIX_TRUE;

        if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                                &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_DPA_TRC, "Failed to get Tx Power from DB\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtRunDpaAlgorithm: No entry found in DB. "
                          "Algorithm not executed"));
            continue;
        }

        /* Formula : 
         * Transmit Power = Tx_Max for given AP + (Tx power control thresh -
         RSSI of 3rd highest neighbor above the threshold) */

        i2TransmitPower =
            (INT2) (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.i2CurrentTxPower +
                    (pRfMgmtAutoRfProfileDB->i2PowerThreshold - i2TargetRssi));
        RFMGMT_TRC1 (RFMGMT_DPA_TRC, "Resultant Power = %d\n", i2TransmitPower);

        if (i2TransmitPower < TX_LOWER_THRESHOLD_LIMIT)
        {
            /* Check the value of tx power. If max power is already reached then
             * throw error */
            if (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u2TxPowerLevel == RFMGMT_MAX_TXPOWER_LEVEL)
            {
                RFMGMT_TRC (RFMGMT_DPA_TRC,
                            "MAX Tx Power reached. "
                            "Cannot run the algorithm\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRunDpaAlgorithm: MAX Tx Power reached. "
                              "Cannot run the algorithm"));
                continue;
            }
            u2NewTxPowerLevel =
                --(RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                   RfMgmtClientConfigDB.u2TxPowerLevel);
            RfMgmtUpdateTxPower (pRfMgmtAPConfigDB->u4RadioIfIndex,
                                 u2NewTxPowerLevel);

            /*Function to update power and handle trap */
            RFMGMT_TRC1 (RFMGMT_DPA_TRC,
                         "    Increase Tx Power level to %d \r\n",
                         u2NewTxPowerLevel);
        }

        if (i2TransmitPower > TX_UPPER_THRESHOLD_LIMIT)
        {
            /* Check the value of tx power. If min power is already reached then
             * throw error */
            if (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u2TxPowerLevel == RFMGMT_MIN_TXPOWER_LEVEL)
            {
                RFMGMT_TRC (RFMGMT_DPA_TRC,
                            "RfMgmtRunDpaAlgorithm: MIN Tx Power reached. "
                            "Cannot run the algorithm\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRunDpaAlgorithm: MIN Tx Power reached. "
                              "Cannot run the algorithm"));
                continue;
            }

            u2NewTxPowerLevel =
                ++(RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
                   RfMgmtClientConfigDB.u2TxPowerLevel);

            /*Function to update power and handle trap */
            RFMGMT_TRC1 (RFMGMT_DPA_TRC,
                         "Decrease Tx Power level to %d\r\n",
                         u2NewTxPowerLevel);

            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "DPA Algorithm: Change the Tx Power by 1 level"));

            RfMgmtUpdateTxPower (pRfMgmtAPConfigDB->u4RadioIfIndex,
                                 u2NewTxPowerLevel);
        }
    }
    while ((pRfMgmtAPNextConfigDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) pRfMgmtAPConfigDB, NULL)) != NULL);

    if (u1Count == 0)
    {
        RFMGMT_TRC (RFMGMT_DPA_TRC,
                    "RfMgmtRunDpaAlgorithm: No entry available for the radio type. "
                    "Algorithm not executed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRunDpaAlgorithm: No entry available for the radio type."
                      "Algorithm not executed"));
    }
    RFMGMT_FN_EXIT ();

    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmt11hTpcAlgorithm                                      */
/*                                                                           */
/* Description  : This functions checks the DB and runs the 11h Transmit Power*/
/*               Control algorithm                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtRun11hTpcAlgorithm ()
{
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2TxPowerLevel = 0;
    UINT2               u2ProfileId = 0;
    INT1                i1RelativeLinkMargin = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1StationCount = 0;
    UINT1               u1IsTxPowerIncreased = OSIX_FALSE;
    UINT1               u1AboveLinkThresholdCount = 0;
    UINT1               u1MinLinkThreshold = 0;
    UINT1               u1MaxLinkThreshold = 0;
    UINT1               u1StaCountThreshold = 0;
    UINT1               u1BelowLinkThresStaCount = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /*Get the configurations required for 11h TPC Algorithm from DB */
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bMinLinkThreshold = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bMaxLinkThreshold = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bStaCountThreshold = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                            &RfMgmtDB) == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_11H_TPC_TRC,
                    "RfMgmt11hTpcAlgorithm: Fetching configurations from "
                    "DB failed. ");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmt11hTpcAlgorithm: Fetching configurations from "
                      "DB failed. "));
        RFMGMT_FN_EXIT ();
        return RFMGMT_FAILURE;
    }

    u1MinLinkThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1MinLinkThreshold;
    u1MaxLinkThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1MaxLinkThreshold;
    u1StaCountThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1StaCountThreshold;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    /*Check whether the entries are present in TPC info DB */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_FIRST_TPC_INFO_ENTRY,
                            &RfMgmtDB) == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_11H_TPC_TRC,
                    "RfMgmt11hTpcAlgorithm: No entry found in DB. "
                    "Algorithm not executed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmt11hTpcAlgorithm: No entry found in DB. "
                      "Algorithm not executed"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_SUCCESS;
    }

    u4RadioIfIndex = RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex;

    if (RfMgmtGetProfileIdRadioId (u4RadioIfIndex, &u2ProfileId, &u1RadioId)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_11H_TPC_TRC,
                    "RfMgmtRun11hTpcAlgorithm : "
                    "Failed to get Profile Id and Radio id\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRun11hTpcAlgorithm : "
                      "Failed to get Profile Id and Radio id\n"));
        return RFMGMT_FAILURE;
    }
    if (RfMgmtGetTxPowerLevel (u4RadioIfIndex, &u2TxPowerLevel) ==
        RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_11H_TPC_TRC,
                    "RfMgmtRun11hTpcAlgorithm : "
                    "Failed to get Tx Power from DB\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRun11hTpcAlgorithm : "
                      "Failed to get Tx Power from DB\n"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_TRC2 (RFMGMT_11H_TPC_TRC,
                 "RfMgmtRun11hTpcAlgorithm: Algorithm is "
                 "triggered for AP: %d Radio Id: %d\r\n",
                 u2ProfileId, u1RadioId);

    do
    {
        /* Get the relative link margin */
        i1RelativeLinkMargin =
            (INT1) (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin -
                    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin);

        if (u4RadioIfIndex == RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex)
        {
            u1StationCount++;
            /*Skip the entries of the same radio if Tx Power is already 
             * increased*/
            if (u1IsTxPowerIncreased == OSIX_TRUE)
            {
                continue;
            }
        }
        else
        {
            /*If all the stations relative link margin is greater than 
             * max link threshold reduce the Tx Power by one level*/
            if (u1AboveLinkThresholdCount == u1StationCount)
            {
                if ((u2TxPowerLevel + RFMGMT_OFFSET_ONE) >
                    RFMGMT_MIN_TXPOWER_LEVEL)
                {
                    RFMGMT_TRC2 (RFMGMT_11H_TPC_TRC,
                                 "11hTpcAlgorithm: Can no more decrease "
                                 "Power level for AP: %d Radio Id: %d\r\n",
                                 u2ProfileId, u1RadioId);
                }
                else
                {

                    RfMgmtUpdateTxPower (u4RadioIfIndex, ++u2TxPowerLevel);
                    RFMGMT_TRC1 (RFMGMT_11H_TPC_TRC,
                                 "Decrease the Tx Power level to %d \r\n",
                                 u2TxPowerLevel);
                }
            }
            u4RadioIfIndex = RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex;
            u1StationCount = 1;
            /*Reset all the variables */
            u1AboveLinkThresholdCount = 0;
            u1BelowLinkThresStaCount = 0;
            u1IsTxPowerIncreased = OSIX_FALSE;

            if (RfMgmtGetTxPowerLevel (u4RadioIfIndex, &u2TxPowerLevel) ==
                RFMGMT_FAILURE)
            {
                RFMGMT_TRC (RFMGMT_11H_TPC_TRC,
                            "RfMgmtRun11hTpcAlgorithm : "
                            "Failed to get Tx Power from DB\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRun11hTpcAlgorithm : "
                              "Failed to get Tx Power from DB\n"));
                return RFMGMT_FAILURE;
            }
            if (RfMgmtGetProfileIdRadioId (u4RadioIfIndex, &u2ProfileId,
                                           &u1RadioId) == RFMGMT_FAILURE)
            {
                RFMGMT_TRC (RFMGMT_11H_TPC_TRC,
                            "RfMgmtRun11hTpcAlgorithm : "
                            "Failed to get Profile Id and Radio id\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRun11hTpcAlgorithm : "
                              "Failed to get Profile Id and Radio id\n"));
                return RFMGMT_FAILURE;
            }
            RFMGMT_TRC2 (RFMGMT_11H_TPC_TRC,
                         "RfMgmtRun11hTpcAlgorithm: Algorithm is "
                         "triggered for AP: %d Radio Id: %d\r\n",
                         u2ProfileId, u1RadioId);
        }
        /*Check whether the relative link margin sent by the connected stations
         *is below Minimum Link Margin. Increase the Tx-Power by one level
         if number of stations below min link threshold is more than the station
         count threshold configuration*/
        if ((i1RelativeLinkMargin > 0) &&
            (i1RelativeLinkMargin > (INT1) u1MinLinkThreshold))
        {
            u1BelowLinkThresStaCount++;

            if (u1BelowLinkThresStaCount >= u1StaCountThreshold)
            {
                if (u2TxPowerLevel == RFMGMT_MAX_TXPOWER_LEVEL)
                {
                    RFMGMT_TRC2 (RFMGMT_11H_TPC_TRC,
                                 "RfMgmtRun11hTpcAlgorithm: Can no more increase"
                                 "Power level for AP: %d Radio Id: %d\r\n",
                                 u2ProfileId, u1RadioId);

                    continue;
                }
                else
                {
                    RfMgmtUpdateTxPower (u4RadioIfIndex, --u2TxPowerLevel);
                    RFMGMT_TRC1 (RFMGMT_11H_TPC_TRC,
                                 "Increase the Tx Power level to %d \r\n",
                                 u2TxPowerLevel);
                    u1IsTxPowerIncreased = OSIX_TRUE;
                }
            }
        }
        if (((RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
              RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin >
              RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
              RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin) &&
             abs (i1RelativeLinkMargin) > (INT1) u1MaxLinkThreshold))
        {
            u1AboveLinkThresholdCount++;
        }
    }
    while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_TPC_INFO_ENTRY,
                               &RfMgmtDB) == RFMGMT_SUCCESS);
    /*If all the stations relative link margin is greater than 
     * max link threshold reduce the Tx Power by one level*/
    if (u1AboveLinkThresholdCount == u1StationCount)
    {
        if ((u2TxPowerLevel + RFMGMT_OFFSET_ONE) > RFMGMT_MIN_TXPOWER_LEVEL)
        {
            RFMGMT_TRC2 (RFMGMT_11H_TPC_TRC,
                         "11hTpcAlgorithm: Can no more decrease "
                         "Power level for AP: %d Radio Id: %d\r\n",
                         u2ProfileId, u1RadioId);
        }
        else
        {

            RfMgmtUpdateTxPower (u4RadioIfIndex, ++u2TxPowerLevel);
            RFMGMT_TRC1 (RFMGMT_11H_TPC_TRC,
                         "Decrease the Tx Power level to %d \r\n",
                         u2TxPowerLevel);
        }
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtGetTxPowerLevel                                      */
/*                                                                           */
/* Description  : This functions gets the tx-power level of the Access Points*/
/*                                                                           */
/* Input        : RadioIfIndex                                               */
/*                                                                           */
/* Output       : TxPowerLevel                                               */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtGetTxPowerLevel (UINT4 u4RadioIfIndex, UINT2 *u2TxPowerLevel)
{
    tRfMgmtDB           RfMgmtUpdateDB;

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = u4RadioIfIndex;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtGetTxPowerLevel: "
                    "Failed to get Tx Power from DB\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtGetTxPowerLevel : "
                      "Failed to get Tx Power from DB\n"));
        return RFMGMT_FAILURE;
    }
    *u2TxPowerLevel = RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2TxPowerLevel;
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtGetProfileIdRadioId                                  */
/*                                                                           */
/* Description  : This function gets the profile id and radioid              */
/*                                                                           */
/* Input        : RadioIfIndex                                               */
/*                                                                           */
/* Output       : profile id and radioid                                     */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtGetProfileIdRadioId (UINT4 u4RadioIfIndex, UINT2 *u2ProfileId,
                           UINT1 *u1RadioId)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtRun11hTpcAlgorithm: RadioIf DB access failed\r\n");
        return RFMGMT_FAILURE;
    }
    RfMgmtGetCapwapProfileId (RadioIfGetDB.RadioIfGetAllDB.
                              u2WtpInternalId, u2ProfileId);
    *u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtModifygaDca                                          */
/*                                                                           */
/* Description  : Modify the gaDca array based in the configured neighbor    */
/* count threshold                                                           */
/*                                                                           */
/* Input        : u1RadioIndex                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
RfMgmtModifygaDca (UINT1 u1RadioIndex)
{
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4Dot11RadioType = 0;
    UINT2               u2WtpId = 0;
    UINT2               u2NeighId = 0;
    UINT2               au2Channel[RFMGMT_MAX_CHANNELA];
    UINT1               u1NeighborCountThreshold = 0;
    UINT1               u1ChannelIndex = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (au2Channel, 0, sizeof (au2Channel));

    if (u1RadioIndex == 0)
    {
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
    }
    else if (u1RadioIndex == RFMGMT_OFFSET_ONE)
    {
        u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
    }
    else
    {
        u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bNeighborCountThreshold = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = u4Dot11RadioType;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtModifygaDca: Getting "
                    "Neighbor Count Threshold failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModifygaDca: Getting "
                      "Neighbor Count Threshold failed"));
        return;
    }
    u1NeighborCountThreshold = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1NeighborCountThreshold;

    /*Check for neighbors with same channel. If the count exceeds the neighbor
     * count threshold then modify the gaDca array*/
    for (u2WtpId = 0; u2WtpId < RFMGMT_MAX_RADIOS; u2WtpId++)
    {
        MEMSET (au2Channel, 0, sizeof (au2Channel));
        if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
            u1IsRunStateReached != OSIX_TRUE)
        {
            continue;
        }
        for (u2NeighId = 0; u2NeighId < RFMGMT_MAX_RADIOS +
             MAX_RFMGMT_EXTERNAL_AP_SIZE; u2NeighId++)
        {
            if (u2WtpId == u2NeighId)
            {
                continue;
            }
            gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2NeighId].
                u1ConsiderNeighborforAlgo = OSIX_FALSE;
            gaDcaAlgorithm[u1RadioIndex][u2NeighId][u2WtpId].
                u1ConsiderNeighborforAlgo = OSIX_FALSE;

            if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2NeighId].
                u1IsNeighborPresent == OSIX_TRUE)
            {
                if (u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
                {
                    RfMgmtGetRadioAChannnelIndex (gaDcaAlgorithm[u1RadioIndex]
                                                  [u2WtpId][u2NeighId].
                                                  u2ChannelNo, &u1ChannelIndex);
                }
                else
                {
                    u1ChannelIndex = (UINT1) (gaDcaAlgorithm[u1RadioIndex]
                                              [u2WtpId][u2NeighId].u2ChannelNo -
                                              1);
                }
                au2Channel[u1ChannelIndex]++;
                if (au2Channel[u1ChannelIndex] >= u1NeighborCountThreshold)
                {
                    RFMGMT_TRC2 (RFMGMT_DCA_TRC,
                                 "AP - %d and AP - %d are considered as neighbors for algorithm\r\n",
                                 u2WtpId, u2NeighId);
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "AP - %d and AP - %d are considered as neighbors for algorithm\r\n",
                                  u2WtpId, u2NeighId));
                    gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2NeighId].
                        u1ConsiderNeighborforAlgo = OSIX_TRUE;
                    gaDcaAlgorithm[u1RadioIndex][u2NeighId][u2WtpId].
                        u1ConsiderNeighborforAlgo = OSIX_TRUE;
                }
            }
        }
    }
}

/* DCA algorithm - START */

/*****************************************************************************/
/* Function     : RfMgmtRunDcaAlgorithm                                      */
/*                                                                           */
/* Description  : This functions checks the DB and run the DCA algorithm     */
/*                                                                           */
/* Input        : pRfMgmtAutoRfProfileDB - Pointer to the input structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtRunDcaAlgorithm (tRfMgmtAutoRfProfileDB * pRfMgmtAutoRfProfileDB,
                       UINT1 u1RunStateTrigger)
{
    UINT2               u2ReqdColor = 0;
    UINT2               u2ColorCount = 0;
    UINT1               u1DCASolFound = OSIX_FALSE;
    UINT1               u1RadioIndex = 0;

    UNUSED_PARAM (u1RunStateTrigger);

    RFMGMT_FN_ENTRY ();

    /* Step-1: 
     * Minor sanity validations
     */
    if (pRfMgmtAutoRfProfileDB->i4RowStatus != ACTIVE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtRunDcaAlgorithm: Table not active \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRunDcaAlgorithm: Table not active "));
        gu4FailedToComDCAAlog++;
        return RFMGMT_SUCCESS;
    }

    if (pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
    {
        u1RadioIndex = RFMGMT_OFFSET_ZERO;
    }
    else if (pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEB)
    {
        u1RadioIndex = RFMGMT_OFFSET_ONE;
    }
    else
    {
        u1RadioIndex = RFMGMT_OFFSET_TWO;
    }
    /* Step-2: 
     * Iterate until the solution is found. Each iteration
     * one color is increased to the total colors available
     * The loop will break when it identifies the solution with 
     * set of colors
     */
    /*Modify the gaDca array based in the configured neighbor count threshold */
    RfMgmtModifygaDca (u1RadioIndex);
    for (u2ColorCount = 1; u2ColorCount <= RFMGMT_MAX_COLORS; u2ColorCount++)
    {
        MEMSET (gau2ColorsToUse, 0, (RFMGMT_MAX_COLORS * sizeof (UINT2)));
        MEMSET (gau2ApNewColors, 0,
                ((RFMGMT_MAX_RADIOS +
                  MAX_RFMGMT_EXTERNAL_AP_SIZE) * sizeof (UINT2)));
        if (RfMgmtGetChannelList (u2ColorCount, &(gau2ColorsToUse[0])) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_DCA_TRC,
                        "RfMgmtRunDcaAlgorithm: Input colors - Channels to "
                        "continue DCA algorithm not available\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtRunDcaAlgorithm: Input colors - Channels to "
                          "continue DCA algorithm not available"));
            continue;
        }

        RFMGMT_TRC1 (RFMGMT_DCA_TRC,
                     "DCA alogorithm - Iteration colors  = %d\r\n",
                     u2ColorCount);

        if (RfMgmtGraphColoring (gau2ColorsToUse,
                                 u2ColorCount, u1RadioIndex) == OSIX_TRUE)
        {
            RFMGMT_TRC1 (RFMGMT_DCA_TRC,
                         "Solution Available with colors = %d\r\n",
                         u2ColorCount);
            u1DCASolFound = OSIX_TRUE;
            if (u2ReqdColor < u2ColorCount)
            {
                u2ReqdColor = u2ColorCount;
            }
            break;
        }
    }

    /* Step-3:
     * If the coloring solution is available, then do following
     * 1. Identify the channels to assign for colors
     * 2. Identify the channel changes
     * 3. Apply the channel changes to each AP
     */
    if (u1DCASolFound == OSIX_TRUE)
    {
        if (u1RadioIndex == 0)
        {
            RfMgmtDcaSolutionForRadioA (gau2ApNewColors, u2ReqdColor,
                                        pRfMgmtAutoRfProfileDB->
                                        au1AllowedChannelList, u1RadioIndex);
        }
        else
        {
            RfMgmtDcaSolution (gau2ApNewColors, u2ReqdColor,
                               pRfMgmtAutoRfProfileDB->au1AllowedChannelList,
                               u1RadioIndex);
        }

        RfMgmtSendDcaSolution (gau2APChannels, u1RadioIndex, u2ReqdColor);
        return RFMGMT_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_DCA_TRC,
                    "!!! CRITICAL: DCA FAILED to assign the colors!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "!!! CRITICAL: DCA FAILED to assign the colors!!!"));
        return RFMGMT_FAILURE;
    }

    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtGetChannelList                                       */
/*                                                                           */
/* Description  : A utility function to get the channel list for the given   */
/*                interference overlap percentage                            */
/*                                                                           */
/* Input        : u2ColorCount - Overlap percentage                         */
/*                pu2AllowedChannels - List of channels available            */
/*                                                                           */
/* Output       : pu2OverlapChannels                                         */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtGetChannelList (UINT2 u2ColorCount, UINT2 *pu2ColorsToUse)
{
    UINT2               u2Count = 0;

    RFMGMT_FN_ENTRY ();

    for (u2Count = 1; u2Count <= u2ColorCount; u2Count++)
    {
        pu2ColorsToUse[u2Count - 1] = u2Count;
    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtGraphColoring                                        */
/*                                                                           */
/* Description  : This function solves the m Coloring problem using          */
/*                Backtracking. It mainly uses graphColoringUtil() to solve  */
/*                the problem. It returns false if the m colors cannot be    */
/*                assigned, otherwise return true and prints assignments of  */
/*                colors to all vertices. Please note that there may be more */
/*                than one solutions, this function prints one of the        */
/*                feasible solutions.                                        */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtGraphColoring (UINT2 *pu2ColorsToUse,
                     UINT2 u2ColorCount, UINT1 u1RadioIndex)
{
    /* Initialize all color values as 0. This initialization is 
     * needed */
    /* correct functioning of RfMgmtIsSafe() */
    UINT1               u1RetVal = 0;

    RFMGMT_FN_ENTRY ();

    /* Call RfMgmtGraphColoringUtil() for vertex 0 */
    u1RetVal = RfMgmtGraphColoringUtil (pu2ColorsToUse,
                                        u2ColorCount,
                                        gau2ApNewColors, 0, u1RadioIndex);

    if (u1RetVal == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }

    RFMGMT_FN_EXIT ();

    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function     : RfMgmtDcaSolution                                          */
/*                                                                           */
/* Description  : A utility function to print solution                       */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
VOID
RfMgmtDcaSolution (UINT2 *pu2Channel, UINT2 u2ReqdColor,
                   UINT1 *pu1AllowedChannelList, UINT1 u1RadioIndex)
{
    UINT2               au2Channel[RFMGMT_MAX_CHANNELB];
    UINT2               u2WtpBaseId = 0;
    UINT2               u2TmpChannel = 0;
    UINT2               au2TmpChannel[RFMGMT_MAX_CHANNELB];
    UINT2               u2NeighId = 0;
    UINT2               u2Index = 0;
    UINT2               u2Index_1 = 0;
    UINT1               u1Color = 0;
    UINT1               u1BaseChannel = 1;
    UINT1               u1AllowedChannels = 0;
    UINT1               u1Count = 0;
    UINT1               u1ChannelCount = 0;
    UINT2               u2NumOfChannels = 0;
    UINT1               u1MaxChannel = 0;
    UINT1               u1ChannelList = 0;
    UINT1               au1OverlapLevel[RFMGMT_MAX_CHANNELB];
    UINT1               u1OverlapValue = 0;;
    UINT1               u1NeighborChannelCount = 0;;
    UINT1               u1BestOverlapLevel = 0;;
    UINT1               au1AllowedChannelList[RFMGMT_MAX_CHANNELB];

    RFMGMT_FN_ENTRY ();
    MEMSET (au1AllowedChannelList, 0, RFMGMT_MAX_CHANNELB);
    MEMSET (au2Channel, 0, sizeof (au2Channel));
    MEMSET (au2TmpChannel, 0, sizeof (au2TmpChannel));
    MEMSET (au1OverlapLevel, 0, RFMGMT_MAX_CHANNELB);

    gu1OverLapPercent = 0;

    for (u1ChannelCount = 0;
         u1ChannelCount < RFMGMT_OVERLAP_CHAN; u1ChannelCount++)
    {
        u1AllowedChannels = pu1AllowedChannelList[u1ChannelCount];
        au1AllowedChannelList[u1AllowedChannels - 1] = u1AllowedChannels;

        if (u1AllowedChannels != 0)
        {
            u1MaxChannel = au1AllowedChannelList[u1AllowedChannels - 1];
            u2NumOfChannels++;
        }
    }

    /* if the available channel count is less than the number of 
     * colors required then reduce the no of colors required to 
     * channel count */
    if (u2NumOfChannels == 0)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDcaSolution: No channels available to assign AP's\n");
        for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
        {
            pu2Channel[u2WtpBaseId] = 0;
        }
        return;
    }

    if (u2ReqdColor > u2NumOfChannels)
    {
        for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
        {
            if (pu2Channel[u2WtpBaseId] > u2NumOfChannels)
            {
                pu2Channel[u2WtpBaseId] =
                    (UINT2) ((pu2Channel[u2WtpBaseId] % u2NumOfChannels) + 1);
            }
        }

        u2ReqdColor = u2NumOfChannels;
    }
    while (u2ReqdColor > (UINT2) u1Count)
    {
        for (u1ChannelCount = (UINT1) (u1Count + 1);
             u1ChannelCount <= RFMGMT_MAX_CHANNELB; u1ChannelCount++)
        {
            if (au1AllowedChannelList[u1ChannelCount - 1] != 0)
            {
                u1AllowedChannels = au1AllowedChannelList[u1ChannelCount - 1];

                if ((gaChannelOverlapPercentage[u1BaseChannel]
                     [u1AllowedChannels] <= gu1OverLapPercent) ||
                    (gaChannelOverlapPercentage[u1BaseChannel]
                     [u1AllowedChannels] == RFMGMT_OVERLAP_MAX))
                {
                    /* Store the list of interfering channel */
                    au2Channel[u1Count] =
                        au1AllowedChannelList[u1ChannelCount - 1];
                    u1Count++;

                    u1BaseChannel = u1AllowedChannels;

                    if ((u2ReqdColor != u1Count) &&
                        (u1AllowedChannels >= u1MaxChannel))
                    {
                        u1Count = 0;
                        u1ChannelList++;
                        u1BaseChannel = 1;
                        u1AllowedChannels = 0;

                        if (gu1OverLapPercent == RFMGMT_OVERLAP_MAX)
                        {
                            return;
                        }
                        switch (u1ChannelList)
                        {
                            case RFMGMT_OVERLAP_1:
                                gu1OverLapPercent = RFMGMT_OVERLAP_MIN1;
                                break;
                            case RFMGMT_OVERLAP_2:
                                gu1OverLapPercent = RFMGMT_OVERLAP_MIN2;
                                break;
                            case RFMGMT_OVERLAP_3:
                                gu1OverLapPercent = RFMGMT_OVERLAP_MIN3;
                                break;
                            case RFMGMT_OVERLAP_4:
                                gu1OverLapPercent = RFMGMT_OVERLAP_MIN4;
                                break;
                            case RFMGMT_OVERLAP_5:
                                gu1OverLapPercent = RFMGMT_OVERLAP_MIN5;
                                break;
                            default:
                                gu1OverLapPercent = RFMGMT_OVERLAP_MAX;
                                break;
                        }
                    }
                    else if (u2ReqdColor <= u1Count)
                    {
                        break;
                    }
                }
                if (u2ReqdColor <= u1Count)
                {
                    break;
                }
                else if (u1AllowedChannels >= u1MaxChannel)
                {
                    u1Count = 0;
                    u1ChannelList++;
                    u1BaseChannel = 1;
                    u1AllowedChannels = 0;

                    switch (u1ChannelList)
                    {
                        case RFMGMT_OVERLAP_1:
                            gu1OverLapPercent = RFMGMT_OVERLAP_MIN1;
                            break;
                        case RFMGMT_OVERLAP_2:
                            gu1OverLapPercent = RFMGMT_OVERLAP_MIN2;
                            break;
                        case RFMGMT_OVERLAP_3:
                            gu1OverLapPercent = RFMGMT_OVERLAP_MIN3;
                            break;
                        case RFMGMT_OVERLAP_4:
                            gu1OverLapPercent = RFMGMT_OVERLAP_MIN4;
                            break;
                        case RFMGMT_OVERLAP_5:
                            gu1OverLapPercent = RFMGMT_OVERLAP_MIN5;
                            break;
                        default:
                            gu1OverLapPercent = RFMGMT_OVERLAP_MAX;
                            break;
                    }
                }
            }
        }
    }

    for (u1Color = 1; u1Color <= u2ReqdColor; u1Color++)
    {
        for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
        {
            if (pu2Channel[u2WtpBaseId] == u1Color)
            {
                if ((u2ReqdColor == 1) && (gau2APChannels[u2WtpBaseId] != 0))
                {
                    u2TmpChannel = gau2APChannels[u2WtpBaseId];
                    if (u2TmpChannel ==
                        (UINT2) au1AllowedChannelList[u2TmpChannel - 1])
                    {
                        continue;
                    }
                    else
                    {
                        gau2APChannels[u2WtpBaseId] = au2Channel[u1Color - 1];
                    }
                }
                else
                {
                    gau2APChannels[u2WtpBaseId] = au2Channel[u1Color - 1];
                }
            }
        }
    }
    if (gau2NoExternalAp[RFMGMT_OFFSET_ONE] == 0)
    {
        return;
    }
    /*Based on the channels got from the Dca algorithm, modify the u2ChannelNo in
     * gaDcaAlgorithm.u2ChannelNo for the corresponding APs. By doing so, we can
     * choose the channels accordingly after consideration of external APs*/
    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        for (u2NeighId = 0;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if (u2WtpBaseId == u2NeighId)
            {
                gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
                    u2ChannelNo = gau2APChannels[u2WtpBaseId];
                continue;
            }
            if (gaDcaAlgorithm[u1RadioIndex][u2NeighId][u2WtpBaseId].
                u2ChannelNo != 0)
            {
                gaDcaAlgorithm[u1RadioIndex][u2NeighId][u2WtpBaseId].
                    u2ChannelNo = gau2APChannels[u2WtpBaseId];
                if (u2NeighId < RFMGMT_MAX_RADIOS)
                {
                    gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2NeighId].
                        u2ChannelNo = gau2APChannels[u2NeighId];
                }
            }
        }
    }

    /*Incase of external AP into consideration the following will be executed to
     * assign the channel to the AP based on the neighbors*/
    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        u1Count = 0;
        u1OverlapValue = 0;
        u1BestOverlapLevel = 0;
        MEMSET (au1OverlapLevel, 0, RFMGMT_MAX_CHANNELB);
        MEMSET (au2Channel, 0, sizeof (au2Channel));
        if (gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
            u1IsRunStateReached != OSIX_TRUE)
        {
            continue;
        }
        /*Collect the channels of the neighor APs in the array au2Channel */
        for (u2NeighId = 0;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if ((gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2NeighId].
                 u1ConsiderNeighborforAlgo == OSIX_TRUE)
                && (u2WtpBaseId != u2NeighId))
            {
                au2Channel[gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2NeighId].
                           u2ChannelNo - 1] =
                    gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2NeighId].
                    u2ChannelNo;
            }
        }
        /*au2TmpChannel contains the list of the channels that the neighbors
         * operate on*/
        for (u2Index = 0; u2Index < RFMGMT_MAX_CHANNELB; u2Index++)
        {
            if (au2Channel[u2Index] != 0)
            {
                au2TmpChannel[u1Count] = au2Channel[u2Index];
                u1Count++;
            }
        }
        /*If there are no neighbors proceed, let the AP operate on the same
         * channel it was operating previously*/
        if (u1Count == 0)
        {
            gau2APChannels[u2WtpBaseId] = gaDcaAlgorithm[u1RadioIndex]
                [u2WtpBaseId][u2WtpBaseId].u2ChannelNo;
            continue;
        }
        u1NeighborChannelCount = u1Count;
        /*Collect the overlap level of every channels in allowed list based on
         * the neighbors are operating channels*/
        for (u2Index = 0; u2Index < RFMGMT_MAX_CHANNELB; u2Index++)
        {
            au1OverlapLevel[u2Index] = RFMGMT_OVERLAP_MAX;
            if (au1AllowedChannelList[u2Index] == 0)
            {
                continue;
            }
            for (u2Index_1 = 0; u2Index_1 < u1NeighborChannelCount; u2Index_1++)
            {
                u1OverlapValue = (UINT1) abs (au1AllowedChannelList[u2Index] -
                                              au2TmpChannel[u2Index_1]);
                if (u1OverlapValue < au1OverlapLevel[u2Index])
                {
                    if (u1OverlapValue <= RFMGMT_NON_OVERLAP_CHAN_OFFSET)
                    {
                        au1OverlapLevel[u2Index] = u1OverlapValue;
                    }
                    else
                    {
                        au1OverlapLevel[u2Index] =
                            RFMGMT_NON_OVERLAP_CHAN_OFFSET;
                    }
                }
            }
        }
        /*Choose the lease overlapping channel and assign the first channel with
         * that overlap level. Channel change not required if the overlap level 
         * of the new channel is same the overlap level of the channel that 
         * the AP was previously operating*/
        for (u2Index = 0; u2Index < RFMGMT_MAX_CHANNELB; u2Index++)
        {
            if ((au1OverlapLevel[u2Index] > u1BestOverlapLevel) &&
                (au1OverlapLevel[u2Index] != RFMGMT_OVERLAP_MAX))
            {
                u1BestOverlapLevel = au1OverlapLevel[u2Index];
            }
        }
        if (au1OverlapLevel[gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId]
                            [u2WtpBaseId].u2ChannelNo - 1] ==
            u1BestOverlapLevel)
        {
            gau2APChannels[u2WtpBaseId] = gaDcaAlgorithm[u1RadioIndex]
                [u2WtpBaseId][u2WtpBaseId].u2ChannelNo;
            continue;
        }
        for (u2Index = 0; u2Index < RFMGMT_MAX_CHANNELB; u2Index++)
        {
            if (au1OverlapLevel[u2Index] == u1BestOverlapLevel)
            {
                break;
            }
        }
        gau2APChannels[u2WtpBaseId] = u2Index + 1;
        /*Update the new channel of the AP in the gaDca, so that this channel
         * change in taken into account during calculation of channel for other
         * APs*/
        for (u2Index = 0;
             u2Index < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2Index++)
        {
            if (gaDcaAlgorithm[u1RadioIndex][u2Index][u2WtpBaseId].
                u2ChannelNo != 0)
            {
                gaDcaAlgorithm[u1RadioIndex][u2Index][u2WtpBaseId].
                    u2ChannelNo = gau2APChannels[u2WtpBaseId];
            }
        }
    }
    UNUSED_PARAM (u1RadioIndex);
    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtGetRadioAChannnelIndex                               */
/*                                                                           */
/* Description  : A utility function is used to get the index of the         */
/*                correspondint radio type a channel                         */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtGetRadioAChannnelIndex (UINT2 u2Channel, UINT1 *pu1Index)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < RADIOIF_MAX_CHANNEL_A; u1Index++)
    {
        if (gau1Dot11aRegDomainChannelList[u1Index] == u2Channel)
        {
            *pu1Index = u1Index;
            return RFMGMT_SUCCESS;
        }
    }
    return RFMGMT_FAILURE;
}

/*****************************************************************************/
/* Function     : RfMgmtIsSafe                                               */
/*                                                                           */
/* Description  : A utility function to check if the current Channel         */
/*                assignment is safe for the WTP                             */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtIsSafe (UINT2 u2InternalApId,
              UINT2 *pu2Channel, UINT2 u2Color, UINT1 u1RadioIndex)
{
    UINT2               u2NeighborApId = 0;
    UINT1               u1NeighStatus = 0;

    RFMGMT_FN_ENTRY ();
    for (u2NeighborApId = 0;
         u2NeighborApId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
         u2NeighborApId++)
    {
        u1NeighStatus =
            gaDcaAlgorithm[u1RadioIndex][u2InternalApId][u2NeighborApId].
            u1ConsiderNeighborforAlgo;

        if ((u2InternalApId != u2NeighborApId) &&
            (u1NeighStatus == OSIX_TRUE) &&
            (u2Color == pu2Channel[u2NeighborApId]))
        {
            RFMGMT_TRC3 (RFMGMT_DCA_TRC,
                         "Channel %d  interfence for AP %d and %d\r\n",
                         u2Color, u2InternalApId, u2NeighborApId);
            return OSIX_FALSE;
        }
    }

    RFMGMT_FN_EXIT ();
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function     : RfMgmtGraphColoringUtil                                    */
/*                                                                           */
/* Description  : A recursive utility function to solve m coloring problem   */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtGraphColoringUtil (UINT2 *pu2ColorsToUse,
                         UINT2 u2ColorCount,
                         UINT2 *pu2Channel,
                         UINT2 u2InternalApId, UINT1 u1RadioIndex)
{
    UINT2               u2Color = 0;
    UINT1               u1RetVal = 0;

    RFMGMT_FN_ENTRY ();

    /* Consider this vertex u2InternalApId and try different colors */
    for (u2Color = 1; u2Color <= u2ColorCount; u2Color++)
    {
        /* If all vertices are assigned a color then
         * return OSIX_TRUE 
         */
        /* IMPORTANT NOTE: the following macro should be changed to 
         * RFMGMT_MAX_RADIOS instead of NUM_OF_AP_SUPPORTED to support
         * for multi radio with RF.
         * Since this function is called in recursive mode,
         * No of times the stack allocation called is changed from 
         * NUM_OF_AP_SUPPORTED to RFMGMT_MAX_RADIOS,
         * This makes corruption and crashes in timer.
         * To avoid this crash, proper logic should be implemented here.
         */
/*        if (u2InternalApId >= NUM_OF_AP_SUPPORTED * 31) */

        if (u2InternalApId >= RFMGMT_MAX_RADIOS)
        {
            return OSIX_TRUE;
        }

        /* Check if assignment of color u2Color to 
         * u2WtpInternalId is fine*/
        u1RetVal = RfMgmtIsSafe (u2InternalApId,
                                 pu2Channel,
                                 pu2ColorsToUse[u2Color - 1], u1RadioIndex);

        if (u1RetVal == OSIX_TRUE)
        {
            pu2Channel[u2InternalApId] = pu2ColorsToUse[u2Color - 1];

            /* recur to assign colors to rest of the vertices */
            if (RfMgmtGraphColoringUtil (pu2ColorsToUse,
                                         u2ColorCount,
                                         pu2Channel,
                                         (UINT2) (u2InternalApId + 1),
                                         u1RadioIndex) == OSIX_TRUE)
            {
                return OSIX_TRUE;
            }

            /* If assigning color c doesn't lead to a solution
               then remove it */
            pu2Channel[u2InternalApId] = 0;
            return OSIX_FALSE;
        }
    }

    /* If no color can be assigned to this vertex then return false */

    RFMGMT_FN_EXIT ();
    return OSIX_FALSE;
}

/* DCA algorithm - END */

/*****************************************************************************/
/* Function     : RfMgmtSendDcaSolution                                      */
/*                                                                           */
/* Description  : A utility function to print solution                       */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
VOID
RfMgmtSendDcaSolution (UINT2 *pu2Channel, UINT1 u1RadioIndex, UINT2 u2ReqdColor)
{
    tRfMgmtDB           RfMgmtDB;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    UINT2               u2WtpInternalId = 0;
    UINT2               au2ApNewColors[RFMGMT_MAX_RADIOS];
    UINT1               u1DcaMode = 0;
    tRfMgmtTrapInfo     RfMgmtTrapInfo;

    MEMSET (au2ApNewColors, 0, sizeof (au2ApNewColors));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&RfMgmtTrapInfo, 0, sizeof (tRfMgmtTrapInfo));

    RFMGMT_FN_ENTRY ();
    if (pu2Channel == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDcaSolution : Invalid input. "
                    "No channel change triggered\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtDcaSolution : Invalid input. "
                      "No channel change triggered"));
        return;
    }
    if (u1RadioIndex == RFMGMT_OFFSET_ZERO)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
    }
    else if (u1RadioIndex == RFMGMT_OFFSET_ONE)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
            u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
    }
    else
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
            u4Dot11RadioType = RFMGMT_RADIO_TYPEG;

    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bDcaMode = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_DCA_TRC,
                    "RfMgmtSendDcaSolution: Dca Mode fetch" "failed\r\n");
        return;
    }

    u1DcaMode = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaMode;

    if (MEMCMP (pu2Channel, au2ApNewColors, sizeof (au2ApNewColors)) == 0)
    {
        return;
    }

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    for (u2WtpInternalId = 0; u2WtpInternalId < RFMGMT_MAX_RADIOS;
         u2WtpInternalId++)
    {
        if (gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
            u1IsRunStateReached == OSIX_TRUE)
        {
            if ((u2ReqdColor == 1) &&
                (gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
                 u2ChannelNo != 0))
            {
                /*    continue; */
            }

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                [u2WtpInternalId].u4RadioIfIndex;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bCurrentChannel = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChannelAssignmentMode = OSIX_TRUE;

            if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY,
                                    &RfMgmtDB) != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRunDcaSolution: Getting Channel "
                            "info failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRunDcaSolution: Getting Channel "
                              "info failed"));
                continue;
            }

            gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
                u2ChannelNo = pu2Channel[u2WtpInternalId];
            if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u2CurrentChannel == pu2Channel[u2WtpInternalId])
            {
                RFMGMT_TRC (RFMGMT_DCA_TRC,
                            "RfMgmtRunDcaSolution: Channel change "
                            "not required\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRunDcaSolution: Channel change "
                              "not required\r\n"));
                continue;
            }
            if ((RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                 u1ChannelAssignmentMode != RFMGMT_PER_AP_DCA_MANUAL)
                || (u1DcaMode == RFMGMT_DCA_MODE_GLOBAL))
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    u2CurrentChannel = pu2Channel[u2WtpInternalId];

                /* Update the Radio intf structure with algorithm assigned channel.
                 * Update the channel in radio module and once success update
                 * the same in RFMGMT DB */
                RadioIfMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.u4IfIndex =
                    gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId]
                    [u2WtpInternalId].u4RadioIfIndex;
                gaDcaAlgorithm[u1RadioIndex][u2WtpInternalId][u2WtpInternalId].
                    u1ApActive = OSIX_TRUE;

                RadioIfMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.
                    u2CurrentChannel =
                    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    u2CurrentChannel;

                if (WssIfProcessRadioIfMsg (WSS_RADIOIF_CHANNEL_UPDATE,
                                            &RadioIfMsgStruct) != OSIX_SUCCESS)
                {
                    RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                                 "RfMgmtRunDcaSolution: Channel change to the Radio DB "
                                 "failed %d\n", RadioIfMsgStruct.unRadioIfMsg.
                                 RadioIfDcaTpcUpdate.u4IfIndex);
                    continue;
                }

                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                    bChannelChangeCount = OSIX_TRUE;

                if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY,
                                        &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtRunDcaSolution: Getting Channel "
                                "info failed\r\n");
                }
                RfMgmtTrapInfo.u2Channel =
                    RadioIfMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.
                    u2CurrentChannel;
                /*RfmSnmpifSendTrapInCxt (RFMGMT_CHANNEL_CHANGE_TRAP,
                   RadioIfMsgStruct.unRadioIfMsg.
                   RadioIfDcaTpcUpdate.u2CurrentChannel, 0); */
                RfmSnmpifSendTrapInCxt (RFMGMT_CHANNEL_CHANGE_TRAP,
                                        &RfMgmtTrapInfo);
            }
        }
    }

    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteClientStats                                    */
/*                                                                           */
/* Description  : This function deletes the Client related information when  */
/*                station related information                                */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to input structure                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtDeleteClientStats (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRfMgmtDB           RfMgmtUpdateDB;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientStats: Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtDeleteClientStats: Null input received"));
        return RFMGMT_FAILURE;
    }

    RFMGMT_TRC2 (RFMGMT_INFO_TRC,
                 "Delete Station entry for WTP %d radio %d\r\n",
                 pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.u2WtpInternalId,
                 pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId);

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);

    /* Get the radio ifindex from the input structure */
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.u2WtpInternalId;
    pWssIfCapwapDB->CapwapGetDB.u1RadioId =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId;

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtDeleteClientStats :"
                    "Get radio Index failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u4IfIndex == 0)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientStats: Invalid "
                    "Ifindex received \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtDeleteClientStats: Invalid " "Ifindex received "));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
        RfMgmtClientScanDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

    MEMCPY (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
            RfMgmtClientScanDB.ClientMacAddress,
            pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.ClientMac,
            sizeof (tMacAddr));

    if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_CLIENT_SCAN_ENTRY,
                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientStats: Client " "deletion failed\n");
        /*Even if destroy of client DB fails, 
         * details in TPC info should be deleted*/
    }

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    /*Delete the entry in TPC info Db as well */
    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

    MEMCPY (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.ClientMac,
            sizeof (tMacAddr));

    if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_TPC_INFO_ENTRY,
                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientStats: Client " "deletion failed\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDcaSolutionForRadioA                                 */
/*                                                                           */
/* Description  : A utility function to print solution                       */
/*                                                                           */
/* Input        :                                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
VOID
RfMgmtDcaSolutionForRadioA (UINT2 *pu2Channel, UINT2 u2ReqdColor,
                            UINT1 *pu1AllowedChannelList, UINT1 u1RadioIndex)
{

    UINT1               au1AllowedChannelList[RFMGMT_MAX_CHANNELA];
    UINT1               u1Count = 0;
    UINT1               u1NumOfChannels = 0;
    UINT2               u2WtpBaseId = 0;
    UINT2               u2NeighId = 0;
    UINT2               au2Channel[RFMGMT_MAX_CHANNELA];
    UINT2               u2Index = 0;
    UINT1               u1Diff = 0;
    UINT1               u1Tmp = 0;
    UINT1               u1ChannelIndex = 0;
    UINT1               u1LeastNeighborChannelIndex = RFMGMT_INIT_VALUE;
    UINT1               u1PreviousChannelNeighCount = RFMGMT_INIT_VALUE;
    /*Channel index and channel neighbor count can be 0. So we use initialize 
     * the variables to unique value RFMGMT_INIT_VALUE. If the max number of 
     * neigbor supported to modified to more than 100, we have to change the 
     * value to RFMGMT_INIT_VALUE*/

    RFMGMT_FN_ENTRY ();
    MEMSET (au1AllowedChannelList, 0, RFMGMT_MAX_CHANNELA);

    /*Populate the list of allowed channels in the array au1AllowedChannelList */
    for (u1Count = 0; u1Count < RFMGMT_MAX_CHANNELA; u1Count++)
    {
        if (pu1AllowedChannelList[u1Count] != 0)
        {
            u1Diff = (UINT1) (pu1AllowedChannelList[u1Count] -
                              pu1AllowedChannelList[u1Tmp]);
            if ((u1Diff > RFMGMT_OFFSET_TWO) || (u1Count == u1Tmp))
            {
                au1AllowedChannelList[u1NumOfChannels] =
                    pu1AllowedChannelList[u1Count];
                u1NumOfChannels++;
                u1Tmp = u1Count;
            }
        }
    }

    /* if the available channel count is less than the number of 
     * colors required then reduce the no of colors required to 
     * channel count */
    if (u2ReqdColor > u1NumOfChannels)
    {
        for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
        {
            if (pu2Channel[u2WtpBaseId] > u1NumOfChannels)
            {
                pu2Channel[u2WtpBaseId] =
                    (UINT2) ((pu2Channel[u2WtpBaseId] % u1NumOfChannels) + 1);
            }
        }
    }

    /*Fills the global array gau2APChannels with the channel to be assigned to
     * APs*/
    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        for (u1Count = 1; u1Count <= u1NumOfChannels; u1Count++)
        {
            if (pu2Channel[u2WtpBaseId] == (UINT2) u1Count)
            {
                gau2APChannels[u2WtpBaseId] =
                    (UINT2) au1AllowedChannelList[u1Count - 1];
                break;
            }
        }
    }
    if (gau2NoExternalAp[0] == 0)
    {
        return;
    }
    /*Based on the channels got from the Dca algorithm, modify the u2ChannelNo in
     * gaDcaAlgorithm.u2ChannelNo for the corresponding APs. By doing so, we can
     * choose the channels accordingly after consideration of external APs*/

    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        for (u2NeighId = 0;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if (u2WtpBaseId == u2NeighId)
            {
                gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
                    u2ChannelNo = gau2APChannels[u2WtpBaseId];
                continue;
            }
            if (gaDcaAlgorithm[u1RadioIndex][u2NeighId][u2WtpBaseId].
                u2ChannelNo != 0)
            {
                gaDcaAlgorithm[u1RadioIndex][u2NeighId][u2WtpBaseId].
                    u2ChannelNo = gau2APChannels[u2WtpBaseId];
                if (u2NeighId < RFMGMT_MAX_RADIOS)
                {
                    gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2NeighId].
                        u2ChannelNo = gau2APChannels[u2NeighId];
                }
            }
        }
    }
    /*Incase of external AP into consideration the following will be executed to
     * assign the channel to the AP based on the neighbors*/
    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        u1ChannelIndex = 0;
        u1PreviousChannelNeighCount = RFMGMT_INIT_VALUE;
        MEMSET (au2Channel, 0, sizeof (au2Channel));
        if (gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
            u1IsRunStateReached != OSIX_TRUE)
        {
            continue;
        }
        /*Collect the channels and number of  neighor APs operating on it in the array au2Channel */
        for (u2NeighId = 0;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if ((gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2NeighId].
                 u1ConsiderNeighborforAlgo == OSIX_TRUE)
                && (u2WtpBaseId != u2NeighId))
            {
                RfMgmtGetRadioAChannnelIndex (gaDcaAlgorithm[u1RadioIndex]
                                              [u2WtpBaseId][u2NeighId].
                                              u2ChannelNo, &u1ChannelIndex);
                au2Channel[u1ChannelIndex]++;
            }
        }
        /*Choose the first non-used channel if present. Else the channel with least 
         *  neighbors operating on is chosen*/
        for (u1Count = 0; u1Count < RFMGMT_MAX_CHANNELA; u1Count++)
        {
            if (pu1AllowedChannelList[u1Count] != 0)
            {
                if (u1LeastNeighborChannelIndex == RFMGMT_INIT_VALUE)
                {
                    u1LeastNeighborChannelIndex = u1Count;
                }
                if (au2Channel[u1Count] < (UINT2) u1PreviousChannelNeighCount)
                {
                    u1LeastNeighborChannelIndex = u1Count;
                    if (au2Channel[u1Count] == 0)
                    {
                        break;
                    }
                }
                u1PreviousChannelNeighCount = (UINT1) au2Channel[u1Count];
            }
        }
        gau2APChannels[u2WtpBaseId] =
            gau1Dot11aRegDomainChannelList[u1LeastNeighborChannelIndex];
        /*Update the new channel of the AP in the gaDca, so that this channel
         * change in taken into account during calculation of channel for other
         * APs*/
        for (u2Index = 0; u2Index < RFMGMT_MAX_RADIOS; u2Index++)
        {
            if (gaDcaAlgorithm[u1RadioIndex][u2Index][u2WtpBaseId].
                u2ChannelNo != 0)
            {
                gaDcaAlgorithm[u1RadioIndex][u2Index][u2WtpBaseId].
                    u2ChannelNo = gau2APChannels[u2WtpBaseId];
            }
        }
    }

}

/*****************************************************************************/
/* Function     : RfMgmtRunShaAlgorithm                                      */
/*                                                                           */
/* Description  : This functions checks the DB and run the DCA algorithm     */
/*                                                                           */
/* Input        : pRfMgmtAutoRfProfileDB - Pointer to the input structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtRunShaAlgorithm (tRfMgmtAutoRfProfileDB * pRfMgmtAutoRfProfileDB)
{
    tRfMgmtFailedAPNeighborListDB *pRfMgmtFailedAPNeighborListDB = NULL;
    tRfMgmtFailedAPNeighborListDB RfMgmtFailedAPNeighborListDB;
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtGetDB;
    tRadioIfGetDB       RadioIfGetDB;
    tShaNeighDetails    ShaNeighDetails[RFMGMT_MAX_RADIOS];
    tShaNeighDetails    TmpShaNeighDetails;
    UINT4               u4Dot11RadioType = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2WtpIndex = 0;
    UINT2               u2WtpId = 0;
    UINT2               u2NeighId = 0;
    UINT2               u2NeighborCount = 0;
    UINT2               u2CompareCount = 0;
    UINT2               u2Count = 0;
    UINT2               u2Index = 0;
    UINT2               u2Index_1 = 0;
    UINT2               u2WtpBaseId = 0;
    UINT2               u2ProfileId = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1RadioIndex = 0;
    UINT1               u1Tmp = 0;
    UINT1               u1Tmp_1 = 0;

    MEMSET (&RfMgmtFailedAPNeighborListDB, 0,
            sizeof (tRfMgmtFailedAPNeighborListDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtGetDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RFMGMT_FN_ENTRY ();
    if ((pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
        || (pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
        || (pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
    {
        u1RadioIndex = RFMGMT_OFFSET_ZERO;
    }
    else if ((pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEB)
             || (pRfMgmtAutoRfProfileDB->u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEBG)
             || (pRfMgmtAutoRfProfileDB->u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEBGN))
    {
        u1RadioIndex = RFMGMT_OFFSET_ONE;
    }
    else
    {
        u1RadioIndex = RFMGMT_OFFSET_TWO;
    }

    /*Run the Sha Algorithm for all the failed APs */
    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        MEMSET (&RfMgmtFailedAPNeighborListDB, 0,
                sizeof (tRfMgmtFailedAPNeighborListDB));
        u1Tmp_1 = 0;

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = gaDcaAlgorithm
            [u1RadioIndex][u2WtpBaseId][u2WtpBaseId].u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
            == OSIX_FAILURE)
        {
            continue;
        }

        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RFMGMT_RADIO_TYPEB)
            || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                RFMGMT_RADIO_TYPEBG)
            || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                RFMGMT_RADIO_TYPEBGN))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
        }
        else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEA) ||
                 (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEAN) ||
                 (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEAC))
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
        }
        else
        {
            u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
        }

        if (pRfMgmtAutoRfProfileDB->u4Dot11RadioType != u4Dot11RadioType)
        {
            continue;
        }
        u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId = 0;
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId = 0;

        RfMgmtFailedAPNeighborListDB.u4RadioIfIndex = gaDcaAlgorithm
            [u1RadioIndex][u2WtpBaseId][u2WtpBaseId].u4RadioIfIndex;

        /*If the received radioifindex is a failed AP then populate all the
         * neighbor details in ShaNeighDetails*/
        while (1)
        {
            pRfMgmtFailedAPNeighborListDB = RBTreeGetNext (gRfMgmtGlobals.
                                                           RfMgmtGlbMib.
                                                           RfMgmtFailedAPNeighborDB,
                                                           (tRBElem *) &
                                                           RfMgmtFailedAPNeighborListDB,
                                                           NULL);

            if (pRfMgmtFailedAPNeighborListDB == NULL)
            {
                break;
            }
            if (pRfMgmtFailedAPNeighborListDB->
                u4RadioIfIndex != RfMgmtFailedAPNeighborListDB.u4RadioIfIndex)

            {
                break;
            }

            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pRfMgmtFailedAPNeighborListDB->u4NeighRadioIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRunShaAlgorithm: RadioIf DB access failed\r\n");
                return RFMGMT_FAILURE;
            }

            RfmgmtGetWtpIndex (RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId,
                               RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                               &u2WtpIndex);

            /*Set Max Power to the APs that have no neighbors */
            if (gaDcaAlgorithm[u1RadioIndex][u2WtpIndex][u2WtpIndex].
                u1IsNeighborPresent == OSIX_FALSE)
            {
                RfMgmtUpdateTxPower (gaDcaAlgorithm[u1RadioIndex]
                                     [u2WtpIndex][u2WtpIndex].u4RadioIfIndex,
                                     1);
            }

            ShaNeighDetails[u2Count].u2WtpIndex = u2WtpIndex;
            ShaNeighDetails[u2Count].u4NeighRadioIfIndex =
                pRfMgmtFailedAPNeighborListDB->u4NeighRadioIfIndex;
            ShaNeighDetails[u2Count].i2Rssi =
                pRfMgmtFailedAPNeighborListDB->i2Rssi;

            MEMCPY (RfMgmtFailedAPNeighborListDB.NeighborAPMac,
                    pRfMgmtFailedAPNeighborListDB->NeighborAPMac, MAC_ADDR_LEN);
            u2Count++;
        }

        /* If it is not a failed AP then continue */
        if (u2Count == 0)
        {
            /*Set Max Power to the APs that have no neighbors */
            if ((gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
                 u1IsRunStateReached == OSIX_TRUE) &&
                (gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
                 u1ApActive == OSIX_TRUE) &&
                (gaDcaAlgorithm[u1RadioIndex][u2WtpBaseId][u2WtpBaseId].
                 u1IsNeighborPresent == OSIX_FALSE))
            {
                RfmgmtGetWtpIndex (u2WtpInternalId, u1RadioId, &u2WtpIndex);
                RfMgmtUpdateTxPower (gaDcaAlgorithm[u1RadioIndex]
                                     [u2WtpIndex][u2WtpIndex].u4RadioIfIndex,
                                     1);
            }
            continue;
        }
        if (u2Count < RFMGMT_OFFSET_THREE)
        {
            for (u2Index = 0; u2Index < u2Count; u2Index++)
            {
                u2WtpId = ShaNeighDetails[u2Index].u2WtpIndex;
                RfMgmtGetDB.unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4RadioIfIndex =
                    gaDcaAlgorithm[u1RadioIndex]
                    [u2WtpId][u2WtpId].u4RadioIfIndex;
                RfMgmtGetDB.unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

                if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                                        &RfMgmtGetDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtRunShaAlgorithm: Client entry not "
                                "found\r\n");
                    return RFMGMT_FAILURE;
                }

                if (RfMgmtGetDB.unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2TxPowerLevel == 1)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                        [u2WtpId].u4RadioIfIndex;
                    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                    }
                    RfMgmtGetCapwapProfileId (RadioIfGetDB.RadioIfGetAllDB.
                                              u2WtpInternalId, &u2ProfileId);
                    RFMGMT_TRC2 (RFMGMT_SHA_TRC,
                                 "RfMgmtRunShaAlgorithm: Can no more increase"
                                 "Power level for AP: %d Radio Id: %d\r\n",
                                 u2ProfileId,
                                 RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                }
                else
                {
                    RfMgmtUpdateTxPower (gaDcaAlgorithm[u1RadioIndex]
                                         [u2WtpId][u2WtpId].u4RadioIfIndex,
                                         --RfMgmtGetDB.unRfMgmtDB.
                                         RfMgmtClientConfigTable.
                                         RfMgmtClientConfigDB.u2TxPowerLevel);
                    gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                        u1TxIncreaseLevel = 0;
                }

            }
            u2Count = 0;
            continue;
        }
        /*Sort the ShaNeighDetails with respect to Rssi */
        for (u2Index = 0; u2Index < u2Count - 1; u2Index++)
        {
            for (u2Index_1 = 0; u2Index_1 < u2Count - u2Index - 1; u2Index_1++)
            {
                if (ShaNeighDetails[u2Index_1].i2Rssi <
                    ShaNeighDetails[u2Index_1 + 1].i2Rssi)
                {
                    MEMCPY (&TmpShaNeighDetails, &ShaNeighDetails[u2Index_1],
                            sizeof (tShaNeighDetails));
                    MEMCPY (&ShaNeighDetails[u2Index_1],
                            &ShaNeighDetails[u2Index_1 + 1],
                            sizeof (tShaNeighDetails));
                    MEMCPY (&ShaNeighDetails[u2Index_1 + 1],
                            &TmpShaNeighDetails, sizeof (tShaNeighDetails));
                }
            }
        }
        if ((u2Count % 2) == 0)
        {
            u2Count = (UINT2) (u2Count / 2);
        }
        else
        {
            u2Count = (UINT2) ((u2Count / 2) + 1);
        }
        /*Self healing algorithm increases the tx-power for the n/2 number of AP
         * where n represent the number of neighbors of the failed APs*
         * Hence the for loop is run for u2Count*/
        for (u2NeighborCount = 0; u2NeighborCount < u2Count; u2NeighborCount++)
        {
            u1Tmp = 0;
            u2WtpId = ShaNeighDetails[u2NeighborCount].u2WtpIndex;
            if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].u1NeighAPCount
                == 0)
            {
                gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].u1TxIncreaseLevel
                    = RFMGMT_TX_INCREASE_TO_MAX;
                continue;
            }

            /*Compare  whether the AP is neighbor of the remaining APs(Failed
             * Ap Neighbors) */
            for (u2CompareCount = 0; u2CompareCount < u2Count; u2CompareCount++)
            {
                u2NeighId = ShaNeighDetails[u2CompareCount].u2WtpIndex;
                if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2NeighId].
                    u1IsNeighborPresent == OSIX_TRUE)
                {
                    gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                        u1TxIncreaseLevel = RFMGMT_TX_INCREASE_ONE_LEVEL;
                    u1Tmp++;
                }
                if (u2CompareCount == u2Count - 1)
                {
                    if (u1Tmp == u2Count)
                    {
                        gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                            u1AllNeighbors = OSIX_TRUE;
                        u1Tmp_1++;
                    }
                }
            }
        }
        if (u1Tmp_1 != u2Count)
        {
            for (u2Index = 0; u2Index < u2Count; u2Index++)
            {
                u2WtpId = ShaNeighDetails[u2Index].u2WtpIndex;
                gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                    u1TxIncreaseLevel = RFMGMT_TX_INCREASE_ONE_LEVEL;
                gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                    u1AllNeighbors = 0;
            }
        }
        else
        {
            /*Convergence has been achieved */
            u2Count = 0;
            continue;
        }
        for (u2Index = 0; u2Index < u2Count; u2Index++)
        {
            u2WtpId = ShaNeighDetails[u2Index].u2WtpIndex;
            if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                u1AllNeighbors != OSIX_TRUE)
            {
                if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                    u1TxIncreaseLevel == RFMGMT_TX_INCREASE_ONE_LEVEL)
                {
                    RfMgmtGetDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u4RadioIfIndex =
                        gaDcaAlgorithm[u1RadioIndex]
                        [u2WtpId][u2WtpId].u4RadioIfIndex;
                    RfMgmtGetDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

                    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                                            &RfMgmtGetDB) != RFMGMT_SUCCESS)
                    {
                        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                    "RfMgmtRunShaAlgorithm: No Client entry "
                                    "found\r\n");

                        return RFMGMT_FAILURE;
                    }

                    if (RfMgmtGetDB.unRfMgmtDB.RfMgmtClientConfigTable.
                        RfMgmtClientConfigDB.u2TxPowerLevel == 1)
                    {
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                            gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                            [u2WtpId].u4RadioIfIndex;
                        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId =
                            OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                      &RadioIfGetDB) !=
                            OSIX_SUCCESS)
                        {
                        }
                        RfMgmtGetCapwapProfileId (RadioIfGetDB.RadioIfGetAllDB.
                                                  u2WtpInternalId,
                                                  &u2ProfileId);
                        RFMGMT_TRC2 (RFMGMT_SHA_TRC,
                                     "RfMgmtRunShaAlgorithm: Can no more increase"
                                     "Power level for AP: %d Radio Id: %d\r\n",
                                     u2ProfileId,
                                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                    }
                    else
                    {
                        RfMgmtUpdateTxPower (gaDcaAlgorithm[u1RadioIndex]
                                             [u2WtpId][u2WtpId].u4RadioIfIndex,
                                             --RfMgmtGetDB.unRfMgmtDB.
                                             RfMgmtClientConfigTable.
                                             RfMgmtClientConfigDB.
                                             u2TxPowerLevel);

                        gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                            u1TxIncreaseLevel = 0;
                    }
                }
                if (gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                    u1TxIncreaseLevel == RFMGMT_TX_INCREASE_TO_MAX)
                {
                    RfMgmtUpdateTxPower (gaDcaAlgorithm[u1RadioIndex]
                                         [u2WtpId][u2WtpId].u4RadioIfIndex, 1);
                    gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].
                        u1TxIncreaseLevel = 0;
                }
            }
        }
        u2Count = 0;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateTxPower                                        */
/*                                                                           */
/* Description  : This functions updates the tx power in radio module        */
/*                                                                           */
/* Input        : RadioIfIndex, TxPowerLevel                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateTxPower (UINT4 u4RadioIfIndex, UINT2 u2TxPowerLevel)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    tRfMgmtDB           RfMgmtUpdateDB;
    tRfMgmtTrapInfo     RfMgmtTrapInfo;

    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtTrapInfo, 0, sizeof (tRfMgmtTrapInfo));

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = u4RadioIfIndex;

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtUpdateTxPower: No Client entry found "
                     "%d\n", u4RadioIfIndex);
        return RFMGMT_FAILURE;
    }

    if (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2TxPowerLevel == u2TxPowerLevel)
    {
        return RFMGMT_SUCCESS;
    }
    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2TxPowerLevel = u2TxPowerLevel;
    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerChangeCount = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY,
                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtUpdateTxPower: Power change to the RFMGMT DB "
                     "failed %d\n", u4RadioIfIndex);
        return RFMGMT_FAILURE;
    }

    RadioIfMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.u4IfIndex
        = u4RadioIfIndex;
    RadioIfMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.u2TxPowerLevel
        = u2TxPowerLevel;

    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_POWER_UPDATE,
                                &RadioIfMsgStruct) != OSIX_SUCCESS)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtUpdateTxPower: Power change to the Radio DB "
                     "failed %d\n", RadioIfMsgStruct.unRadioIfMsg.
                     RadioIfDcaTpcUpdate.u4IfIndex);
        return RFMGMT_FAILURE;
    }
    RfMgmtTrapInfo.u2TxPowerLevel =
        RadioIfMsgStruct.unRadioIfMsg.RadioIfDcaTpcUpdate.u2TxPowerLevel;
    RfmSnmpifSendTrapInCxt (RFMGMT_TXPOWER_CHANGE_TRAP, &RfMgmtTrapInfo);
    return RFMGMT_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtUpdateTxPowerLevel                                   */
/*                                                                           */
/* Description  : This function updates the tx power level in RF module      */
/*                                                                           */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to input structure                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateTxPowerLevel (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u2TxPowerLevel =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u2TxPower;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bTxPowerLevel = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfmgmtGetWtpIndex                                          */
/*                                                                           */
/* Description  : This utility gievs the wtp index from WtpInternalId and    */
/*                RadioId                                                    */
/*                                                                           */
/* Input        : WtpInternalId RadioId                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/

UINT1
RfmgmtGetWtpIndex (UINT2 u2WtpInternalId, UINT1 u1RadioId, UINT2 *pu2WtpIndex)
{
    /* Logic definition:
     * Consider that no of APs = 3; No of Radios per AP = 2;
     *
     *  -------------------------------------------------------------
     * | AP Id Logical  | Radio Id Logical | Wtp Int | Radio | Array |
     * | representation | representation   | id      |  Id   |  Id   |
     *  -------------------------------------------------------------
     * |       1        |        1         |  0      |  1    |   0   |
     * |       1        |        2         |  0      |  2    |   1   |
     *  -------------------------------------------------------------
     * |       2        |        1         |  1      |  1    |   2   |
     * |       2        |        2         |  1      |  2    |   3   |
     *  -------------------------------------------------------------
     * |       3        |        1         |  2      |  1    |   4   |
     * |       3        |        2         |  2      |  2    |   5   |
     *  -------------------------------------------------------------
     */
    *pu2WtpIndex = (UINT2) (((u2WtpInternalId * RFMGMT_MAX_RADIOS_PER_AP)
                             + u1RadioId) - 1);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtGetCapwapProfileId                                   */
/*                                                                           */
/* Description  : This utility gievs the wtp index from WtpInternalId and    */
/*                RadioId                                                    */
/*                                                                           */
/* Input        : WtpInternalId RadioId                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/

VOID
RfMgmtGetCapwapProfileId (UINT2 u2WtpInternalId, UINT2 *pu2WtpIndex)
{
    CapwapGetProfileIdFromInternalProfId (u2WtpInternalId, pu2WtpIndex);
    return;
}

/*****************************************************************************/
/* Function     : RfMgmtTriggerConfigUpdate                                  */
/*                                                                           */
/* Description  : This utility triggers the config update request for the    */
/*                parameters that are to be sent after run state             */
/*                                                                           */
/* Input        : WtpInternalId RadioId                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/

UINT1
RfMgmtTriggerConfigUpdate (UINT4 u4RadioIfIndex)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtBssidScanDB  RfMgmtBssidScanDB;
    UINT1               u1WlanId = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtBssidScanDB, 0, sizeof (RfMgmtBssidScanDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
        u4RadioIfIndex = u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanIsSetDB.
        bAutoScanStatus = OSIX_TRUE;

    /*Check whether the ssid snr scan status for all wlan id.
     * And if it is non-default value trigger the config update request for it*/
    for (u1WlanId = 1; u1WlanId <= MAX_RFMGMT_WLAN_ID; u1WlanId++)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
            u1WlanID = u1WlanId;
        if (RfMgmtProcessDBMsg (RFMGMT_GET_BSSID_SCAN_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            continue;
        }

        if (RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
            u1AutoScanStatus != RFMGMT_BSSID_SCAN_STATUS_ENABLE)
        {

            if (RfMgmtProcessBssidScanStatus (&RfMgmtDB) != RFMGMT_SUCCESS)
            {

                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessBssidScanStatus returns failure\r\n");
            }
        }
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdategaDcaAlgorithm                                 */
/*                                                                           */
/* Description  : This function is used to update the gaDcaAlgorithm         */
/*                when RSSI threshold is modified.                           */
/*                                                                           */
/* Input        : i2RSSIThreshold - Dca RSSI Threshold                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/

UINT1
RfMgmtUpdategaDcaAlgorithm (INT4 i4RadioType, INT2 i2RssiThreshold)
{
    tRadioIfGetDB       RadioIfDB;
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtNeighborScanDB *pRfMgmtNextNeighborScanDB = NULL;
    tRfMgmtNeighborScanDB RfMgmtNeighborScanDB;
    tMacAddr            PrevMacAddr;
    UINT4               u4PreviousRadioifIndex = 0;
    UINT2               u2NeighId = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2TmpRadioType = 0;
    UINT1               u1RadioId = 0;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&PrevMacAddr, 0, sizeof (tMacAddr));

    if ((i4RadioType == RFMGMT_RADIO_TYPEA) ||
        (i4RadioType == RFMGMT_RADIO_TYPEAN) ||
        (i4RadioType == (INT4) RFMGMT_RADIO_TYPEAC))
    {
        u1RadioId = 0;
    }
    else if ((i4RadioType == RFMGMT_RADIO_TYPEB) ||
             (i4RadioType == RFMGMT_RADIO_TYPEBG) ||
             (i4RadioType == RFMGMT_RADIO_TYPEBGN))
    {
        u1RadioId = RFMGMT_OFFSET_ONE;
    }
    else
    {
        u1RadioId = RFMGMT_OFFSET_TWO;
    }

    /*If the new RSSI is greater than the RSSI that is stored in 
     * in the gaDcaAlgorithm array then remove the entries*/
    for (u2WtpInternalId = 0; u2WtpInternalId < RFMGMT_MAX_RADIOS;
         u2WtpInternalId++)
    {
        for (u2NeighId = 0;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if ((u2WtpInternalId != u2NeighId) && (gaDcaAlgorithm[u1RadioId]
                                                   [u2WtpInternalId][u2NeighId].
                                                   u1IsNeighborPresent != 0))
            {
                if ((gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                     i2Rssi < i2RssiThreshold) && (gaDcaAlgorithm[u1RadioId]
                                                   [u2NeighId])
                    [u2WtpInternalId].i2Rssi < i2RssiThreshold)
                {
                    MEMSET (&gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                            [u2NeighId], 0, sizeof (tDcaAlgorithm));
                    gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2WtpInternalId].u1NeighAPCount--;
                    if (gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2WtpInternalId].u1NeighAPCount == 0)
                    {
                        gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                            [u2WtpInternalId].u1IsNeighborPresent = 0;
                    }
                    MEMSET (&gaDcaAlgorithm[u1RadioId][u2NeighId]
                            [u2WtpInternalId], 0, sizeof (tDcaAlgorithm));
                    gaDcaAlgorithm[u1RadioId][u2NeighId]
                        [u2NeighId].u1NeighAPCount--;
                    /*If existing external AP is more neighbor to any of the WLC 
                     * managed Access Points, then decrease the external AP count*/
                    if ((gaDcaAlgorithm[u1RadioId][u2NeighId][u2NeighId].
                         u1NeighAPCount == 0)
                        && (u2NeighId >= RFMGMT_MAX_RADIOS))
                    {
                        MEMSET (&gaDcaAlgorithm[u1RadioId][u2NeighId]
                                [u2NeighId], 0, sizeof (tDcaAlgorithm));
                        gau2NoExternalAp[u1RadioId]--;
                        gau2ExternalApColors[u2NeighId - RFMGMT_MAX_RADIOS] = 0;
                    }
                }
                else if ((gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                          i2Rssi > gaDcaAlgorithm[u1RadioId][u2NeighId]
                          [u2WtpInternalId].i2Rssi))
                {
                    gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                        i2Rssi = gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2NeighId].i2Rssi;
                }
                else
                {
                    gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                        i2Rssi = gaDcaAlgorithm[u1RadioId][u2NeighId]
                        [u2WtpInternalId].i2Rssi;
                }
            }
        }
    }
    /*If the new RSSI is lesser than the RSSI that is stored in 
     * in the neighbor DB,  then move the entries to gaDcaAlgorithm*/
    pRfMgmtNextNeighborScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       (tRBElem *) & RfMgmtNeighborScanDB, NULL);

    while (pRfMgmtNextNeighborScanDB != NULL)
    {
        RfMgmtNeighborScanDB.u4RadioIfIndex =
            pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
        RfMgmtNeighborScanDB.u2ScannedChannel =
            pRfMgmtNextNeighborScanDB->u2ScannedChannel;
        MEMCPY (RfMgmtNeighborScanDB.NeighborAPMac,
                pRfMgmtNextNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

        MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));

        RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRfMgmtNextNeighborScanDB->u4RadioIfIndex;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfDB) != OSIX_SUCCESS)
        {
            u4PreviousRadioifIndex = pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
            MEMCPY (PrevMacAddr,
                    pRfMgmtNextNeighborScanDB->NeighborAPMac,
                    sizeof (tMacAddr));
            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNeighborScanDB, NULL);
            continue;
        }

        if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
            || (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                RFMGMT_RADIO_TYPEAN)
            || (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                RFMGMT_RADIO_TYPEAC))
        {
            u2TmpRadioType = RFMGMT_RADIO_TYPEA;
        }
        else if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEB) || (RadioIfDB.RadioIfGetAllDB.
                                          u4Dot11RadioType ==
                                          RFMGMT_RADIO_TYPEBG)
                 || (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RFMGMT_RADIO_TYPEBGN))
        {
            u2TmpRadioType = RFMGMT_RADIO_TYPEB;
        }
        else
        {
            u2TmpRadioType = RFMGMT_RADIO_TYPEG;
        }

        RfmgmtGetWtpIndex (RadioIfDB.RadioIfGetAllDB.u2WtpInternalId,
                           RadioIfDB.RadioIfGetAllDB.u1RadioId,
                           &u2WtpInternalId);
        if (pRfMgmtNextNeighborScanDB->i2Rssi <= i2RssiThreshold)
        {
            u4PreviousRadioifIndex = pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
            MEMCPY (PrevMacAddr,
                    pRfMgmtNextNeighborScanDB->NeighborAPMac,
                    sizeof (tMacAddr));
            if (pRfMgmtNextNeighborScanDB->u2ExternalAPIndex != 0)
            {
                MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4RadioIfIndex =
                    pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
                MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.NeighborAPMac,
                        pRfMgmtNextNeighborScanDB->NeighborAPMac,
                        sizeof (tMacAddr));
                if (RfMgmtProcessDBMsg
                    (RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY,
                     &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtUpdategaDcaAlgorithm: Neighbor DB"
                                " Deletion failed\r\n");
                }
                RfMgmtNeighborScanDB.u4RadioIfIndex = u4PreviousRadioifIndex;
                MEMCPY (RfMgmtNeighborScanDB.NeighborAPMac,
                        PrevMacAddr, MAC_ADDR_LEN);
            }
            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNeighborScanDB, NULL);
            continue;
        }
        if (u2TmpRadioType != (UINT2) i4RadioType)
        {
            u4PreviousRadioifIndex = pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
            MEMCPY (PrevMacAddr,
                    pRfMgmtNextNeighborScanDB->NeighborAPMac,
                    sizeof (tMacAddr));
            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNeighborScanDB, NULL);
            continue;
        }

        for (u2NeighId = 0; u2NeighId < RFMGMT_MAX_RADIOS; u2NeighId++)
        {
            if (MEMCMP (gaDcaAlgorithm[u1RadioId][u2NeighId]
                        [u2NeighId].MacAddr,
                        pRfMgmtNextNeighborScanDB->NeighborAPMac,
                        RFMGMT_MAC_SIZE) != 0)
            {
                continue;
            }
            /*Update in gaDcaAlgorithm as neighbors */
            if (gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].i2Rssi
                == 0)
            {
                if ((gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                     i2Rssi > pRfMgmtNextNeighborScanDB->i2Rssi)
                    && (gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                        i2Rssi != 0))
                {
                    gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                        i2Rssi =
                        gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                        i2Rssi;
                }
                else
                {
                    gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                        i2Rssi = pRfMgmtNextNeighborScanDB->i2Rssi;
                    gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                        i2Rssi = pRfMgmtNextNeighborScanDB->i2Rssi;
                }
            }
            else
            {
                /*Ignore to update the entry if it is already updated */
                continue;
            }
            MEMCPY (gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                    MacAddr, gaDcaAlgorithm[u1RadioId][u2NeighId]
                    [u2NeighId].MacAddr, RFMGMT_MAC_SIZE);
            MEMCPY (gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                    MacAddr, gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                    [u2WtpInternalId].MacAddr, RFMGMT_MAC_SIZE);
            if (gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                u1IsNeighborPresent != OSIX_TRUE)
            {
                gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                    u1IsNeighborPresent = OSIX_TRUE;
                gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                    u1NeighAPCount++;
            }
            if (gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                u1IsNeighborPresent != OSIX_TRUE)
            {
                gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                    u1IsNeighborPresent = OSIX_TRUE;
                gaDcaAlgorithm[u1RadioId][u2NeighId][u2WtpInternalId].
                    u1NeighAPCount++;
            }
        }
        u4PreviousRadioifIndex = pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
        MEMCPY (PrevMacAddr,
                pRfMgmtNextNeighborScanDB->NeighborAPMac, sizeof (tMacAddr));

        pRfMgmtNextNeighborScanDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & RfMgmtNeighborScanDB, NULL);

    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtRunDFSAlgorithm                                      */
/*                                                                           */
/* Description  : This functions checks the DFS available channels and      */
/*                allocated it to an AP                                     */
/*                                                                           */
/* Input        : pRfMgmtAutoRfProfileDB - Pointer to the input structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtRunDFSAlgorithm (tRfMgmtAutoRfProfileDB * pRfMgmtAutoRfProfileDB,
                       tRfMgmtAPConfigDB * pRfMgmtAPConfigDB)
{
    UINT1               au1AllowedChannelList[RFMGMT_MAX_CHANNELA];
    UINT1               u1ChnlIdx = 0;
    UINT1               u1ChnlCount = 0;
    UINT1               u1RadioIndex = 0;
    MEMSET (au1AllowedChannelList, 0, RFMGMT_MAX_CHANNELB);
    UNUSED_PARAM (u1RadioIndex);

    RFMGMT_FN_ENTRY ();

    if (pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
    {
        u1RadioIndex = 0;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtRunDFSAlgorithm: Radiotype not valid \n");
    }
    for (u1ChnlIdx = 0; u1ChnlIdx < RADIOIF_MAX_CHANNEL_A; u1ChnlIdx++)
    {
        if (pRfMgmtAPConfigDB->DFSChannelInfo[u1ChnlIdx].u4ChannelNum ==
            pRfMgmtAutoRfProfileDB->au1AllowedChannelList[u1ChnlIdx])
        {

            if ((pRfMgmtAPConfigDB->DFSChannelInfo[u1ChnlIdx].u4DFSFlag &
                 RADIO_CHAN_DFS_MASK) == RADIO_CHAN_DFS_AVAILABLE)
            {
                au1AllowedChannelList[u1ChnlCount] =
                    (UINT1) pRfMgmtAPConfigDB->DFSChannelInfo[u1ChnlIdx].
                    u4ChannelNum;
                u1ChnlCount++;
            }

        }
    }
    if (u1ChnlCount)
    {
        MEMCPY (pRfMgmtAutoRfProfileDB->au1AllowedChannelList,
                au1AllowedChannelList, RADIOIF_MAX_CHANNEL_A);
    }
    return RFMGMT_SUCCESS;
    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateDFSInfo                                        */
/* Description  : This function updates the DFS info in RF module            */
/*                                                                           */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to input structure                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateDFSInfo (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    /* Get the radio ifindex from the input structure */
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.u2WtpInternalId;
    pWssIfCapwapDB->CapwapGetDB.u1RadioId =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.DFSChannelTable.
        u1RadioId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientScanMessage: Getting "
                    "Radio index failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessClientScanMessage: Getting "
                      "Radio index failed"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            DFSChannelInfo,
            pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusReq.DFSChannelTable.
            DFSChStats, RADIOIF_MAX_CHANNEL_A);
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bDFSChannelInfoSet = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation for DFS Info Failed\r\n");
        return RFMGMT_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return RFMGMT_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtGetFreeExternalAPId                                  */
/* Description  : This function updates the DFS info in RF module            */
/*                                                                           */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to input structure                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtGetFreeExternalAPId (UINT2 *pu2ExternalAPId, UINT1 u1RadioIndex)
{
    UINT2               u2Index = 0;
    for (u2Index = RFMGMT_MAX_RADIOS;
         u2Index < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE; u2Index++)
    {
        if (gaDcaAlgorithm[u1RadioIndex][u2Index][u2Index].u1IsRunStateReached
            != 3)
        {
            *pu2ExternalAPId = u2Index;
            return RFMGMT_SUCCESS;
        }
    }
    return RFMGMT_FAILURE;
}

#ifdef ROGUEAP_WANTED
/*****************************************************************************/
/* Function     : RfMgmtResetRogueApClassifiedBy                             */
/*                                                                           */
/*                                                                           */
/* Description  : Rogue AP classified based on the Rule configuraion.        */
/*                This route will get Rogue AP DB and classify each Rogue AP */
/*                when it not manually configured.                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Change the RogueAP class and state based on Rule configured*/
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtResetRogueApClassifiedBy (void)
{
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB;
    tRfMgmtRogueApDB    RfMgmtRogueApDB;

    MEMSET (NextRogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (RogueAP, ZERO, sizeof (tMacAddr));

    pRfMgmtRogueApDB =
        (tRfMgmtRogueApDB *) RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                             RfMgmtRogueApDB);
    {
        while (pRfMgmtRogueApDB != NULL)
        {
            if (pRfMgmtRogueApDB->u1RogueApClassifiedBy !=
                ROGUEAP_CLASSIFIEDBY_MANUALLY)
            {
                pRfMgmtRogueApDB->u1RogueApClassifiedBy =
                    ROGUEAP_CLASSIFIEDBY_NONE;
                pRfMgmtRogueApDB->u1RogueApClass = ROGUEAP_CLASS_UNCLASSIFIED;
                pRfMgmtRogueApDB->u1RogueApState = ROGUEAP_STATE_NONE;
            }

            MEMCPY (RfMgmtRogueApDB.u1RogueApBSSID,
                    pRfMgmtRogueApDB->u1RogueApBSSID, MAC_ADDR_LEN);
            pRfMgmtRogueApDB =
                (tRfMgmtRogueApDB *) RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                                    RfMgmtRogueApDB,
                                                    (tRBElem *) &
                                                    RfMgmtRogueApDB, NULL);
        }
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteExpiredRogueApIntervalTmrExp                   */
/*                                                                           */
/*                                                                           */
/* Description  : Whenever the Rogue AP entry timeout gets expired, then     */
/*                delete the rogue AP entries for the Rogue AP DB.           */
/*                Rogue AP expire timeout will be applied only to non-Manual */
/*                Rogue AP. ie: Rogue AP Time out will not be applied to     */
/*                Manuall Rogue AP.                                          */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Delete expired RogueAP                                     */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
RfMgmtDeleteExpiredRogueApIntervalTmrExp (void *pArg)
{

    UNUSED_PARAM (pArg);
    tOsixSysTime        u4CurrentTime = ZERO;
    UINT4               u4RogueApTimeInSec = ZERO;
    tRfMgmtRogueApDB    RfMgmtRogueApDB;
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB = &RfMgmtRogueApDB;
    UINT4               u4HeardTime = ZERO;
    UINT4               u4RogueApTimeOut = ZERO;
    INT4                i4RogueDetection = DISABLED;
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;
    UINT1               au1String[RFMGMT_MAC_STR_LEN];

    MEMSET (NextRogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (RogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (&RfMgmtRogueApDB, ZERO, sizeof (RfMgmtRogueApDB));

    /* Get the Rogue AP Detection */
    nmhGetFsRogueApDetecttion (&i4RogueDetection);
    nmhGetFsRogueApTimeout (&u4RogueApTimeOut);

    if ((u4RogueApTimeOut != ZERO) && (i4RogueDetection != DISABLED))
    {
        /* Check the Rogue AP entries  */
        if (nmhGetFirstIndexFsRogueApTable (&NextRogueAP) != SNMP_FAILURE)
        {
            while (MEMCMP (RogueAP, NextRogueAP, sizeof (tMacAddr)) != 0)
            {
                RfmgmtMacToStr (NextRogueAP, au1String);
                if (nmhGetFsRogueApLastHeardTime (NextRogueAP, &u4HeardTime) !=
                    SNMP_FAILURE)
                {
                    /*Getting the DB data */
                    MEMCPY (&RfMgmtRogueApDB.u1RogueApBSSID, NextRogueAP,
                            sizeof (tMacAddr));
                    pRfMgmtRogueApDB =
                        RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                                   (tRBElem *) & RfMgmtRogueApDB);

                    if (pRfMgmtRogueApDB->u1RogueApClassifiedBy !=
                        ROGUEAP_CLASSIFIEDBY_MANUALLY)
                    {
                        nmhGetFsRogueApLastHeardTime (NextRogueAP,
                                                      &u4HeardTime);
                        u4HeardTime =
                            (u4HeardTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                        /* Need to convert to secs from ticks recieved */
                        OsixGetSysTime (&u4CurrentTime);
                        u4CurrentTime =
                            (u4CurrentTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                        u4RogueApTimeInSec = u4CurrentTime - u4HeardTime;
                        /* Delete the Rogue AP when the configured RogueAP 
                         * timeout is lesser than RogueApHeardTime */
                        if (u4RogueApTimeInSec >= u4RogueApTimeOut)
                        {

                            if (pRfMgmtRogueApDB != NULL)
                            {
                                RfmgmtMacToStr (NextRogueAP, au1String);
                                /*Remove the corresponding node from the tree */
                                RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                                           RfMgmtRogueApDB, pRfMgmtRogueApDB);
                                MemReleaseMemBlock
                                    (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID,
                                     (UINT1 *) pRfMgmtRogueApDB);

                                pRfMgmtRogueApDB = NULL;
                            }
                        }
                    }
                    else
                    {

                        if (pRfMgmtRogueApDB->u1RogueApClassifiedBy ==
                            ROGUEAP_CLASSIFIEDBY_MANUALLY)
                        {
                            nmhGetFsRogueApLastHeardTime (NextRogueAP,
                                                          &u4HeardTime);
                            u4HeardTime =
                                (u4HeardTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                            /* Need to convert to secs from ticks recieved */
                            OsixGetSysTime (&u4CurrentTime);
                            u4CurrentTime =
                                (u4CurrentTime /
                                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                            u4RogueApTimeInSec = u4CurrentTime - u4HeardTime;
                            /* Delete the Rogue AP when the configured RogueAP 
                             * timeout is lesser than RogueApHeardTime */
                            if (u4RogueApTimeInSec >= u4RogueApTimeOut)
                            {
                                /* Pass the received input and check entry is already present */
                                if (pRfMgmtRogueApDB != NULL)
                                {
                                    RfmgmtMacToStr (NextRogueAP, au1String);
                                    pRfMgmtRogueApDB->u4RogueApLastHeardTime =
                                        0;
                                    pRfMgmtRogueApDB->u1RogueApState = 0;
                                }
                            }
                        }
                    }
                }
                MEMCPY (RogueAP, NextRogueAP, sizeof (tMacAddr));
                nmhGetNextIndexFsRogueApTable (RogueAP, &NextRogueAP);
            }                    /*While Rogue AP get DB */
        }
    }                            /*RogueDetection check */
    if (RfMgmtClassifyRogueAp () != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Rogue Classification failed\r\n");
    }
    /* Start the Rogue Ap Timeout Timer */
    RfMgmtWlcTmrStart (0, RFMGMT_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR,
                       RFMGMT_DEF_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR);

    return;

}

/*****************************************************************************/
/* Function     : arrangeRogueRulePriority*/
/*                                                                           */
/*                                                                           */
/* Description  : Rogue AP classified based on the Rule configuraion.        */
/*                This route will get Rogue AP DB and classify each Rogue AP */
/*                when it not manually configured.                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Change the RogueAP class and state based on Rule configured*/
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtArrangeRogueRulePriority (UINT1 u1Priority)
{
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE RuleName;
    UINT1               au1RogueRuleName[MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[MAX_SSID_LEN];
    UINT1               au1SsidName[MAX_SSID_LEN];
    tRfMgmtRogueRuleDB  RfMgmtRogueRuleDB;
    UINT1               u1RogueRuleCount = ZERO;
    INT4                i4RulePriority = ZERO;
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;

    MEMSET (au1RogueRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (&NextRuleName, ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SsidName, ZERO, MAX_SSID_LEN);
    MEMSET (&RfMgmtRogueRuleDB, ZERO, sizeof (tRfMgmtRogueRuleDB));
    MEMSET (NextRogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (RogueAP, ZERO, sizeof (tMacAddr));

    RuleName.pu1_OctetList = au1RogueRuleName;
    NextRuleName.pu1_OctetList = au1RogueNextRuleName;

    /*Get the Rogue Rule count */
    u1RogueRuleCount = gu1RogueRuleCount;
    if (u1Priority >= u1RogueRuleCount + 2)
    {
        return RFMGMT_FAILURE;
    }
    if (u1Priority == (gu1RogueRuleCount + 1))
    {
        return RFMGMT_SUCCESS;
    }
    else
    {
        if (nmhGetFirstIndexFsRogueRuleConfigTable (&NextRuleName)
            != SNMP_FAILURE)
        {
            while (MEMCMP (RuleName.pu1_OctetList,
                           NextRuleName.pu1_OctetList, MAX_SSID_LEN) != ZERO)
            {
                /* Check the Rogue Rule Priority same as index. 
                 * If Yes, start Rogue AP classification. */
                if ((nmhGetFsRogueApRulePriOrderNum (&NextRuleName,
                                                     &i4RulePriority) ==
                     SNMP_SUCCESS) && (i4RulePriority < u1Priority))
                {
                    MEMCPY (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                            MAX_SSID_LEN);
                    nmhGetNextIndexFsRogueRuleConfigTable (&RuleName,
                                                           &NextRuleName);
                    continue;
                }
                else
                {
                    nmhSetFsRogueApRulePriOrderNum (&NextRuleName,
                                                    ++i4RulePriority);
                }

                MEMCPY (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                        MAX_SSID_LEN);
                nmhGetNextIndexFsRogueRuleConfigTable (&RuleName,
                                                       &NextRuleName);
            }                    /*While till Rogue Rules configured */
        }
    }

    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtClassifyRogueAp                                      */
/*                                                                           */
/*                                                                           */
/* Description  : Rogue AP classified based on the Rule configuraion.        */
/*                This route will get Rogue AP DB and classify each Rogue AP */
/*                when it not manually configured.                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Change the RogueAP class and state based on Rule configured*/
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtClassifyRogueAp (void)
{
    tOsixSysTime        u4CurrentTime = ZERO;
    UINT4               u4RogueApTimeInSec = ZERO;
    UINT4               u4HeardTime = ZERO;
    tRfMgmtTrapInfo     RfMgmtTrapInfo;
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE SsidName;
    INT4                i4Class = ZERO;
    INT4                i4State = ZERO;
    INT4                i4Protection = DEFAULT_PROTECTION;
    INT4                i2Rssi = ZERO;
    INT4                i4Status = ZERO;
    UINT4               u4ClientCount = ZERO;
    UINT4               u4Duration = ZERO;
    UINT1               au1RogueRuleName[MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[MAX_SSID_LEN];
    UINT1               au1SsidName[MAX_SSID_LEN];
    tRfMgmtRogueRuleDB  RfMgmtRogueRuleDB;
    tRfMgmtRogueRuleDB *pRfMgmtRogueRuleDB = NULL;
    UINT1               u1MatchingFlag = DISABLED;
    UINT1               u1rssiFlag = DISABLED;
    UINT1               u1ProcetionTypeFlag = DISABLED;
    UINT1               u1ClientCountFlag = DISABLED;
    UINT1               u1DurationFlag = DISABLED;
    UINT1               u1SsidFlag = DISABLED;
    INT4                i4RogueDetection = DISABLED;
    UINT1               u1Timeout = DISABLED;
    UINT1               u1RogueRuleCount = ZERO;
    UINT1               u1Index = ZERO;
    INT4                i4Priority = ZERO;
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB;
    tRfMgmtRogueApDB    RfMgmtRogueApDB;

    UINT1               au1Ssid[MAX_SSID_LEN];

    MEMSET (au1RogueRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (&NextRuleName, ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RfMgmtRogueRuleDB, ZERO, sizeof (tRfMgmtRogueRuleDB));
    MEMSET (NextRogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (RogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (&RfMgmtTrapInfo, 0, sizeof (tRfMgmtTrapInfo));
    MEMSET (au1Ssid, ZERO, MAX_SSID_LEN);
    MEMSET (au1SsidName, ZERO, MAX_SSID_LEN);

    NextRuleName.pu1_OctetList = au1RogueNextRuleName;
    SsidName.pu1_OctetList = au1Ssid;

    RfMgmtResetRogueApClassifiedBy ();

    /* Get the Rogue Rule count */
    u1RogueRuleCount = gu1RogueRuleCount;

    /* Get the Rogue AP Detection */
    nmhGetFsRogueApDetecttion (&(i4RogueDetection));

    if (i4RogueDetection != DISABLED)
    {
        for (u1Index = (ZERO + 1); ((u1Index <= u1RogueRuleCount) &&
                                    (u1Index <= MAX_ROGUE_AP)); u1Index++)
        {
            pRfMgmtRogueRuleDB =
                (tRfMgmtRogueRuleDB *) RBTreeGetFirst
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB);
            while (pRfMgmtRogueRuleDB != NULL)
            {
                i4Status = pRfMgmtRogueRuleDB->isRogueApStatus;
                i4Priority = pRfMgmtRogueRuleDB->u1RogueApRulePriOrderNum;
                i4Protection = pRfMgmtRogueRuleDB->isRogueApProcetionType;
                i2Rssi = pRfMgmtRogueRuleDB->i2RogueApRSSI;
                u4ClientCount = pRfMgmtRogueRuleDB->u4RogueApClientCount;
                u4Duration = pRfMgmtRogueRuleDB->u4RogueApRuleDuration;
                i4Class = pRfMgmtRogueRuleDB->u1RogueApRuleClass;
                i4State = pRfMgmtRogueRuleDB->u1RogueApRuleState;
                MEMCPY (SsidName.pu1_OctetList,
                        pRfMgmtRogueRuleDB->u1RogueApRuleSSID,
                        strlen ((const char *) pRfMgmtRogueRuleDB->
                                u1RogueApRuleSSID));

                if (i4Status == ENABLED)
                {
                    if (i4Priority != u1Index)
                    {
                        MEMCPY (RfMgmtRogueRuleDB.u1RogueApRuleName,
                                pRfMgmtRogueRuleDB->u1RogueApRuleName,
                                WSSMAC_MAX_SSID_LEN);

                        pRfMgmtRogueRuleDB =
                            (tRfMgmtRogueRuleDB *) RBTreeGetNext
                            (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                             (tRBElem *) & RfMgmtRogueRuleDB, NULL);
                        continue;
                    }
                    /* Find the Fields configured. */
                    if (i4Protection != DEFAULT_PROTECTION)
                    {
                        u1ProcetionTypeFlag = ENABLED;
                    }
                    if (i2Rssi != DEFAULT_RSSI)
                    {
                        u1rssiFlag = ENABLED;
                    }
                    if (u4ClientCount != DEFAULT_CLIENT_COUNT)
                    {
                        u1ClientCountFlag = ENABLED;
                    }
                    if (u4Duration != DEFAULT_DURATION)
                    {
                        u1DurationFlag = ENABLED;
                    }
                    if (MEMCMP
                        (SsidName.pu1_OctetList, au1SsidName,
                         MAX_SSID_LEN) != ZERO)
                    {
                        u1SsidFlag = ENABLED;
                    }

                    /* When any flag is set do the classification process. */
                    if (u1ProcetionTypeFlag || u1rssiFlag || u1ClientCountFlag
                        || u1SsidFlag || u1DurationFlag)
                    {
                        pRfMgmtRogueApDB =
                            (tRfMgmtRogueApDB *) RBTreeGetFirst
                            (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB);
                        /*  Get the Rogue AP from RfMgmtRogueApDB */
                        while (pRfMgmtRogueApDB != NULL)
                        {
                            if (pRfMgmtRogueApDB->u1RogueApClassifiedBy >=
                                u1Index)
                            {
                                if ((i4Protection != DEFAULT_PROTECTION) &&
                                    (pRfMgmtRogueApDB->isRogueApProcetionType ==
                                     i4Protection))
                                {
                                    u1MatchingFlag = ENABLED;
                                }
                                if ((i2Rssi != DEFAULT_RSSI) &&
                                    (pRfMgmtRogueApDB->i2RogueApRSSI >= i2Rssi))
                                {
                                    u1MatchingFlag = ENABLED;
                                }
                                if ((u4ClientCount != DEFAULT_CLIENT_COUNT) &&
                                    (pRfMgmtRogueApDB->u4RogueApClientCount ==
                                     u4ClientCount))
                                {
                                    u1MatchingFlag = ENABLED;
                                }
                                u4HeardTime =
                                    pRfMgmtRogueApDB->u4RogueApLastHeardTime;
                                u4HeardTime =
                                    (u4HeardTime /
                                     SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                                OsixGetSysTime (&u4CurrentTime);
                                u4CurrentTime =
                                    (u4CurrentTime /
                                     SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                                u4RogueApTimeInSec =
                                    u4CurrentTime - u4HeardTime;
                                if ((u4Duration != DEFAULT_DURATION)
                                    && (u4RogueApTimeInSec >= u4Duration))
                                {
                                    u1MatchingFlag = ENABLED;
                                    u1Timeout = ENABLED;
                                }
                                if (!(memcmp (pRfMgmtRogueApDB->u1RogueApSSID,
                                              SsidName.pu1_OctetList,
                                              MAX_SSID_LEN)))
                                {
                                    u1MatchingFlag = ENABLED;
                                }
                                if (u1MatchingFlag == ENABLED)
                                {
                                    u1MatchingFlag = DISABLED;    /* Reset the Flag */
                                    if (u1Timeout != ENABLED)
                                    {
                                        pRfMgmtRogueApDB->u1RogueApClass =
                                            (UINT1) i4Class;
                                        pRfMgmtRogueApDB->u1RogueApState =
                                            (UINT1) i4State;
                                        pRfMgmtRogueApDB->
                                            u1RogueApClassifiedBy = u1Index;
                                        if (i4State == ROGUE_AP_ALERT
                                            && pRfMgmtRogueApDB->
                                            u4RogueApLastReportedTime == 0)
                                        {
                                            MEMCPY (RfMgmtTrapInfo.
                                                    au1RogueApBSSID,
                                                    pRfMgmtRogueApDB->
                                                    u1RogueApBSSID,
                                                    WSSMAC_MAC_ADDR_LEN);
                                            RfMgmtTrapInfo.i4RogueApState =
                                                i4State;
                                            RfmSnmpifSendTrapInCxt
                                                (RFMGMT_ROGUE_STATUS_TRAP,
                                                 &RfMgmtTrapInfo);
                                            OsixGetSysTime (&pRfMgmtRogueApDB->
                                                            u4RogueApLastReportedTime);
                                        }
                                    }
                                    else
                                    {
                                        u1Timeout = DISABLED;
                                        if (pRfMgmtRogueApDB->u1RogueApClass !=
                                            ROGUE_AP_UNCLASSIFY)
                                        {
                                            pRfMgmtRogueApDB->u1RogueApClass =
                                                (UINT1) ROGUE_AP_UNCLASSIFY;
                                            pRfMgmtRogueApDB->u1RogueApState =
                                                (UINT1) ROGUE_AP_NONE;
                                            pRfMgmtRogueApDB->
                                                u1RogueApClassifiedBy = u1Index;
                                        }
                                    }

                                }
                            }
                            MEMCPY (RfMgmtRogueApDB.u1RogueApBSSID,
                                    pRfMgmtRogueApDB->u1RogueApBSSID,
                                    MAC_ADDR_LEN);
                            pRfMgmtRogueApDB =
                                (tRfMgmtRogueApDB *) RBTreeGetNext
                                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                                 (tRBElem *) & RfMgmtRogueApDB, NULL);
                        }        /*While Rogue AP get DB */
                    }            /*If ends in first Get Rogue AP ends */
                }
                MEMCPY (RfMgmtRogueRuleDB.u1RogueApRuleName,
                        pRfMgmtRogueRuleDB->u1RogueApRuleName,
                        strlen ((const char *) pRfMgmtRogueRuleDB->
                                u1RogueApRuleName));

                pRfMgmtRogueRuleDB =
                    (tRfMgmtRogueRuleDB *) RBTreeGetNext
                    (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                     (tRBElem *) & RfMgmtRogueRuleDB, NULL);

            }                    /* While till Rogue Rules configured */
        }                        /*For ends */
    }                            /* RogueDetection check */
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtClassifyRogueApInit                                  */
/*                                                                           */
/*                                                                           */
/* Description  : Initial classification for Rogue AP at heard state.        */
/*                This route will get the rule and classify the Rogue AP.   */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Change the RogueAP class and state based on Rule configured*/
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtClassifyRogueApInit (tRfMgmtRogueApDB * pRfMgmtRogueApDB)
{
    tOsixSysTime        u4CurrentTime = ZERO;
    UINT4               u4RogueApTimeInSec = ZERO;
    UINT4               u4HeardTime = ZERO;
    tRfMgmtTrapInfo     RfMgmtTrapInfo;
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE SsidName;
    INT4                i4Class = ZERO;
    INT4                i4State = ZERO;
    INT4                i4Protection = DEFAULT_PROTECTION;
    INT4                i2Rssi = ZERO;
    INT4                i4Status = ZERO;
    UINT4               u4ClientCount = ZERO;
    UINT4               u4Duration = ZERO;
    UINT1               au1RogueRuleName[MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[MAX_SSID_LEN];
    UINT1               au1SsidName[MAX_SSID_LEN];
    tRfMgmtRogueRuleDB  RfMgmtRogueRuleDB;
    tRfMgmtRogueRuleDB *pRfMgmtRogueRuleDB = NULL;
    UINT1               u1MatchingFlag = DISABLED;
    UINT1               u1rssiFlag = DISABLED;
    UINT1               u1ProcetionTypeFlag = DISABLED;
    UINT1               u1ClientCountFlag = DISABLED;
    UINT1               u1DurationFlag = DISABLED;
    UINT1               u1SsidFlag = DISABLED;
    INT4                i4RogueDetection = DISABLED;
    UINT1               u1RogueRuleCount = ZERO;
    UINT1               u1Index = ZERO;
    INT4                i4Priority = ZERO;
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;

    UINT1               au1Ssid[MAX_SSID_LEN];

    MEMSET (au1RogueRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (au1Ssid, ZERO, MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (&NextRuleName, ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RfMgmtRogueRuleDB, ZERO, sizeof (tRfMgmtRogueRuleDB));
    MEMSET (NextRogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (RogueAP, ZERO, sizeof (tMacAddr));
    MEMSET (&RfMgmtTrapInfo, 0, sizeof (tRfMgmtTrapInfo));
    MEMSET (au1Ssid, ZERO, MAX_SSID_LEN);
    MEMSET (au1SsidName, ZERO, MAX_SSID_LEN);

    NextRuleName.pu1_OctetList = au1RogueNextRuleName;
    SsidName.pu1_OctetList = au1Ssid;

    if (pRfMgmtRogueApDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "pRfMgmtRogueApDB: Invalid Rogue Ap entry received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "pRfMgmtRogueApDB: Invalid Rogue Ap entry received\n"));
        return OSIX_FAILURE;
    }

    /* Get the Rogue Rule count */
    u1RogueRuleCount = gu1RogueRuleCount;

    /* Get the Rogue AP Detection */
    nmhGetFsRogueApDetecttion (&(i4RogueDetection));

    /* Classify the Rogue AP when Rogue Detection is Enabled */
    if (i4RogueDetection != DISABLED)
    {
        for (u1Index = (ZERO + 1); ((u1Index <= u1RogueRuleCount) &&
                                    (u1Index <= MAX_ROGUE_AP)); u1Index++)
        {
            pRfMgmtRogueRuleDB =
                (tRfMgmtRogueRuleDB *) RBTreeGetFirst
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB);
            while (pRfMgmtRogueRuleDB != NULL)

            {
                i4Status = pRfMgmtRogueRuleDB->isRogueApStatus;
                i4Priority = pRfMgmtRogueRuleDB->u1RogueApRulePriOrderNum;
                i4Protection = pRfMgmtRogueRuleDB->isRogueApProcetionType;
                i2Rssi = pRfMgmtRogueRuleDB->i2RogueApRSSI;
                u4ClientCount = pRfMgmtRogueRuleDB->u4RogueApClientCount;
                u4Duration = pRfMgmtRogueRuleDB->u4RogueApRuleDuration;
                i4Class = pRfMgmtRogueRuleDB->u1RogueApRuleClass;
                i4State = pRfMgmtRogueRuleDB->u1RogueApRuleState;
                MEMCPY (SsidName.pu1_OctetList,
                        pRfMgmtRogueRuleDB->u1RogueApRuleSSID,
                        strlen ((const char *) pRfMgmtRogueRuleDB->
                                u1RogueApRuleSSID));

                if (i4Status == ENABLED)
                {
                    if (i4Priority != u1Index)
                    {
                        MEMCPY (RfMgmtRogueRuleDB.u1RogueApRuleName,
                                pRfMgmtRogueRuleDB->u1RogueApRuleName,
                                WSSMAC_MAX_SSID_LEN);

                        pRfMgmtRogueRuleDB =
                            (tRfMgmtRogueRuleDB *) RBTreeGetNext
                            (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                             (tRBElem *) & RfMgmtRogueRuleDB, NULL);

                        continue;
                    }
                    /* Find the Fields configured. */
                    if (i4Protection != DEFAULT_PROTECTION)
                    {
                        u1ProcetionTypeFlag = ENABLED;
                    }
                    if (i2Rssi != DEFAULT_RSSI)
                    {
                        u1rssiFlag = ENABLED;
                    }
                    if (u4ClientCount != DEFAULT_CLIENT_COUNT)
                    {
                        u1ClientCountFlag = ENABLED;
                    }
                    if (u4Duration != DEFAULT_DURATION)
                    {
                        u1DurationFlag = ENABLED;
                    }
                    if (MEMCMP
                        (SsidName.pu1_OctetList, au1SsidName,
                         MAX_SSID_LEN) != ZERO)
                    {
                        u1SsidFlag = ENABLED;
                    }

                    /* When any flag is set do the classification process. */
                    if (u1ProcetionTypeFlag || u1rssiFlag || u1ClientCountFlag
                        || u1SsidFlag || u1DurationFlag)
                    {
                        if (pRfMgmtRogueApDB->u1RogueApClassifiedBy >= u1Index)
                        {
                            if ((i4Protection != DEFAULT_PROTECTION) &&
                                (pRfMgmtRogueApDB->isRogueApProcetionType ==
                                 i4Protection))
                            {
                                u1MatchingFlag = ENABLED;
                            }
                            if ((i2Rssi != DEFAULT_RSSI) &&
                                (pRfMgmtRogueApDB->i2RogueApRSSI >= i2Rssi))
                            {
                                u1MatchingFlag = ENABLED;
                            }
                            if ((u4ClientCount != DEFAULT_CLIENT_COUNT) &&
                                (pRfMgmtRogueApDB->u4RogueApClientCount ==
                                 u4ClientCount))
                            {
                                u1MatchingFlag = ENABLED;
                            }
                            u4HeardTime =
                                pRfMgmtRogueApDB->u4RogueApLastHeardTime;
                            u4HeardTime =
                                (u4HeardTime / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                            OsixGetSysTime (&u4CurrentTime);
                            u4CurrentTime =
                                (u4CurrentTime /
                                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC);

                            u4RogueApTimeInSec = u4CurrentTime - u4HeardTime;
                            if ((u4Duration != DEFAULT_DURATION) &&
                                (u4RogueApTimeInSec >= u4Duration))
                            {
                                u1MatchingFlag = ENABLED;
                            }
                            if (MEMCMP (pRfMgmtRogueApDB->u1RogueApSSID,
                                        SsidName.pu1_OctetList,
                                        MAX_SSID_LEN) == 0)
                            {
                                u1MatchingFlag = ENABLED;
                            }
                            if (u1MatchingFlag == ENABLED)
                            {
                                u1MatchingFlag = DISABLED;    /* Reset the Flag */
                                pRfMgmtRogueApDB->u1RogueApClass = i4Class;
                                pRfMgmtRogueApDB->u1RogueApState = i4State;
                                pRfMgmtRogueApDB->u1RogueApClassifiedBy =
                                    u1Index;
                                if (i4State == ROGUE_AP_ALERT
                                    && pRfMgmtRogueApDB->
                                    u4RogueApLastReportedTime == 0)
                                {
                                    MEMCPY (RfMgmtTrapInfo.au1RogueApBSSID,
                                            pRfMgmtRogueApDB->u1RogueApBSSID,
                                            WSSMAC_MAC_ADDR_LEN);
                                    RfMgmtTrapInfo.i4RogueApState = i4State;
                                    RfmSnmpifSendTrapInCxt
                                        (RFMGMT_ROGUE_STATUS_TRAP,
                                         &RfMgmtTrapInfo);
                                    OsixGetSysTime (&pRfMgmtRogueApDB->
                                                    u4RogueApLastReportedTime);
                                }

                            }
                        }
                    }            /*If ends in first Get Rogue AP ends */
                }
                MEMCPY (RfMgmtRogueRuleDB.u1RogueApRuleName,
                        pRfMgmtRogueRuleDB->u1RogueApRuleName,
                        strlen ((const char *) pRfMgmtRogueRuleDB->
                                u1RogueApRuleName));

                pRfMgmtRogueRuleDB =
                    (tRfMgmtRogueRuleDB *) RBTreeGetNext
                    (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                     (tRBElem *) & RfMgmtRogueRuleDB, NULL);

            }                    /* While till Rogue Rules configured */
        }                        /*For ends */
    }                            /* RogueDetection check */
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtReArrangeRogueRule                                   */
/*                                                                           */
/*                                                                           */
/* Description  : Re-Arrange the Rogue AP when rules get changed.            */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : Change the RogueAP class and state based on Rule configured*/
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtReArrangeRogueRule (INT4 i4deleteRulePriority)
{
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE RuleName;
    UINT1               au1RogueRuleName[MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[MAX_SSID_LEN];
    INT4                i4currentPriority = 0;
    MEMSET (au1RogueRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, ZERO, MAX_SSID_LEN);
    RuleName.pu1_OctetList = au1RogueRuleName;
    NextRuleName.pu1_OctetList = au1RogueNextRuleName;
    if (nmhGetFirstIndexFsRogueRuleConfigTable (&NextRuleName) == SNMP_SUCCESS)
    {
        while (MEMCMP (RuleName.pu1_OctetList,
                       NextRuleName.pu1_OctetList, MAX_SSID_LEN) != ZERO)
        {
            if ((nmhGetFsRogueApRulePriOrderNum
                 (&NextRuleName, &i4currentPriority) == SNMP_SUCCESS) &&
                (i4currentPriority > i4deleteRulePriority))
            {
                nmhSetFsRogueApRulePriOrderNum (&NextRuleName,
                                                --i4currentPriority);
                MEMCPY (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                        MAX_SSID_LEN);
                nmhGetNextIndexFsRogueRuleConfigTable (&RuleName,
                                                       &NextRuleName);
            }
            else
            {
                MEMCPY (RuleName.pu1_OctetList,
                        NextRuleName.pu1_OctetList, MAX_SSID_LEN);
                nmhGetNextIndexFsRogueRuleConfigTable (&RuleName,
                                                       &NextRuleName);
            }
        }
    }
    return RFMGMT_SUCCESS;
}

#endif
#endif
