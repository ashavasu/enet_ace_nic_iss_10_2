
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfapmain.c,v 1.5 2017/11/24 10:37:05 siva Exp $
 *
 * Description: This file contains the rfmgmt task main loop
 *              and the initialisation routines for WTP.
 *                            
 ********************************************************************/

#ifndef __RFAPMAIN_C__
#define __RFAPMAIN_C__

#include "rfminc.h"
#include "wssifstawtpprot.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#include "rfhwnpwr.h"
#include "rfmgmtnp.h"
#include "radioifproto.h"
#endif
#include "testwr.h"
#include "wsscfginc.h"
extern UINT1        gau1Dot11DFSChannelList[RADIO_MAX_DFS_CHANNEL];

/*INT4 WsscfgGetFsDot11bnSupport PROTO((INT4 *));*/

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWtpMainTask                                         *
 *                                                                          *
 * Description  : Main function of RFMGMT task.                             *
 *                                                                          *
 * Input        : pi1Arg                                                    *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtWtpMainTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;
    RFMGMT_FN_ENTRY ();

    UNUSED_PARAM (pi1Arg);

    /* Invoke the main task init function to create task, 
     * timer and semaphore */
    if (RfMgmtWtpMainTaskInit () == OSIX_FAILURE)
    {
        RfMgmtWtpMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        OsixReceiveEvent (RFMGMT_QUE_MSG_EVENT | RFMGMT_TIMER_EXP_EVENT,
                          OSIX_WAIT, (UINT4) 0, &u4Events);
        RFMGMT_LOCK;
        if (u4Events & RFMGMT_QUE_MSG_EVENT)
        {
            RFMGMT_TRC (CONTROL_PLANE_TRC,
                        "RfMgmtMainProcessEvent: Recvd Q Msg Event\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtMainProcessEvent: Recvd Q Msg Event"));
            RfMgmtWtpQueMsgHandler ();
        }

        if (u4Events & RFMGMT_TIMER_EXP_EVENT)
        {
            RFMGMT_TRC (CONTROL_PLANE_TRC,
                        "RfMgmtMainProcessEvent: Recvd neighbor scan exp "
                        "Event\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtMainProcessEvent: Recvd neighbor scan exp "
                          "Event"));
            RfMgmtNeighScanExpHandler ();
        }

        RFMGMT_UNLOCK;
    }

    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtWtpMainTaskInit                                      */
/*                                                                           */
/*                                                                           */
/* Description  : RFMGMT task initialization routine. This function creates  */
/*                the semaphore, task and queues                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS / RFMGMT_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtWtpMainTaskInit (VOID)
{
    RFMGMT_FN_ENTRY ();

    /* Initialize the global variables */
    RfMgmtInitGlobals ();

    /* RFMGMT Semaphore */
    if (OsixCreateSem (RFMGMT_WTP_SEM_NAME, RFMGMT_SEM_CREATE_INIT_CNT, 0,
                       &(gRfMgmtGlobals.SemId)) == OSIX_FAILURE)
    {
        RFMGMT_TRC (OS_RESOURCE_TRC | RFMGMT_CRITICAL_TRC,
                    "RfMgmtMainTaskInit: RFMGMT Semaphore creation FAILED!!!\r\n");
        return RFMGMT_FAILURE;
    }

    /* Create Task */
    if (OsixGetTaskId (SELF, RFMGMT_WTP_TASK_NAME,
                       &(gRfMgmtGlobals.rfMgmtTaskId)) == OSIX_FAILURE)
    {
        RFMGMT_TRC (OS_RESOURCE_TRC | RFMGMT_CRITICAL_TRC,
                    "RfMgmtMainTaskInit: RFMGMT Task Creation FAILED!!!\r\n");
        return RFMGMT_FAILURE;
    }

    /* Create queue */
    if (OsixCreateQ (RFMGMT_WTP_QUE_NAME, RFMGMT_WTP_QUEUE_DEPTH, 0,
                     &(gRfMgmtGlobals.rfMgmtQueId)) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtWtpMainTask: Queue Creation failed \r\n");
        return RFMGMT_FAILURE;
    }

    /* Registering with SYSLOG */
    gu4RfmSysLogId = (UINT4) (SYS_LOG_REGISTER ((UINT1 *) RFMGMT_WTP_SEM_NAME,
                                                SYSLOG_CRITICAL_LEVEL));

    /* Invoke the below function to create the mem pool and create the 
     * DB related operations */
    if (RfMgmtWtpModuleStart () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (OS_RESOURCE_TRC | RFMGMT_CRITICAL_TRC,
                    "RfMgmtMainTaskInit: RFMGMT Module Start FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtMainTaskInit: RFMGMT Module Start FAILED!!!"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWtpMainTaskDeInit                                   *
 *                                                                          *
 * Description  : This function de-inits global data structures of          *
 *                Beacon Manager.                                           *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtWtpMainTaskDeInit (VOID)
{
    RFMGMT_FN_ENTRY ();

    RfMgmtWtpModuleShutDown ();

    if (gRfMgmtGlobals.rfMgmtQueId != 0)
    {
        OsixQueDel (gRfMgmtGlobals.rfMgmtQueId);
        gRfMgmtGlobals.rfMgmtQueId = 0;
    }

    if (gRfMgmtGlobals.SemId != 0)
    {
        OsixSemDel (gRfMgmtGlobals.SemId);
        gRfMgmtGlobals.SemId = 0;
    }
    SYS_LOG_DEREGISTER (gu4RfmSysLogId);
    RFMGMT_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWtpModuleStart                                      *
 *                                                                          *
 * Description  : This function allocates memory pools and inits global     *
 *                data structures of RFMGMT and create the DB related       *
 *                structures                                                *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                          *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtWtpModuleStart (VOID)
{
    RFMGMT_FN_ENTRY ();

    /* Initialize Database Mempools */
    if (RfmgmtSizingMemCreateMemPools () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleStart: Memory Initialization FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleStart: Memory Initialization FAILED!!!"));
        RfmgmtSizingMemDeleteMemPools ();
        return RFMGMT_FAILURE;
    }

    /* Timer Initialization */
    if (RfMgmtWtpTmrInit () == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleStart: Timer Initialization FAILED!!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleStart: Timer Initialization FAILED!!!"));
        RfmgmtSizingMemDeleteMemPools ();
        return RFMGMT_FAILURE;
    }

    /* RFMGMT manager Initialization */
    if (RfMgmtWtpCreateRBTree () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }

    gRfMgmtGlobals.u1RfmgmtTaskInitialized = OSIX_TRUE;

    RegisterRFMGMT ();
#ifdef RFMGMT_TEST_WANTED
    RegisterTEST ();
#endif
    RFMGMT_FN_EXIT ();

    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWtpCreateRBTree                                     *
 *
 * Description  : This function creates all the RBTree required.            * 
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                          *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtWtpCreateRBTree (VOID)
{
    if (RfMgmtAPConfigDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtNeighborScanDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtClientConfigDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtClientScanDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RfMgmtScanAllowedDBCreate () == RFMGMT_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtInitGlobals                                         *
 *
 * Description  : This function initis all Globals.                         * 
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtInitGlobals (VOID)
{
    MEMSET (&gRfMgmtGlobals, 0, sizeof (tRfMgmtGlobals));

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtWtpModuleShutDown                                   *
 *                                                                          *
 * Description  : This function shuts down the RFMGMT module.               *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtWtpModuleShutDown (VOID)
{
    tRfMgmtQMsg        *pRfMgmtMsg = NULL;

    RFMGMT_FN_ENTRY ();
    RFMGMT_LOCK;

    if (gRfMgmtGlobals.u1RfmgmtTaskInitialized == OSIX_FALSE)
    {
        RFMGMT_UNLOCK;
        return;
    }

    gRfMgmtGlobals.u1RfmgmtTaskInitialized = OSIX_FALSE;

    while (OsixQueRecv (gRfMgmtGlobals.rfMgmtQueId, (UINT1 *) &pRfMgmtMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pRfMgmtMsg == NULL)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Received pointer is NULL\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Received pointer is NULL"));
            RFMGMT_UNLOCK;
            return;
        }
    }

    /* Deinitialize the timer function */
    if (RfMgmtWtpTmrDeInit () == OSIX_FAILURE)
    {
        RFMGMT_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "RfMgmtModuleShutDown: Timer DeInit FAILED !!!\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtModuleShutDown: Timer DeInit FAILED !!!"));
    }

    /* Release the allocated memory and delete the mem pools created */
    RfMgmtMainReleaseQMemory (pRfMgmtMsg);
    RfmgmtSizingMemDeleteMemPools ();

    RFMGMT_UNLOCK;
    RFMGMT_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtMainReleaseQMemory                                  *
 *                                                                          *
 * Description  : This function releases the Q memory pools.                *
 *                                                                          *
 * Input        : pRfMgmtMsg - Queue Memory                                 *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
RfMgmtMainReleaseQMemory (tRfMgmtQMsg * pRfMgmtMsg)
{
    RFMGMT_FN_ENTRY ();

    MemReleaseMemBlock (RFMGMT_QUEUE_POOLID, (UINT1 *) pRfMgmtMsg);
    RFMGMT_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtLock                                                *
 *                                                                          *
 * Description  : This function is to take RfMgmt mutex semaphore.          *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtLock (VOID)
{
    RFMGMT_FN_ENTRY ();
    if (OsixSemTake (gRfMgmtGlobals.SemId) == OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : RfMgmtUnlock                                              *
 *                                                                          *
 * Description  : This function is to release RfMgmt mutex semaphore.       *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
RfMgmtUnLock (VOID)
{
    RFMGMT_FN_ENTRY ();

    if (OsixSemGive (gRfMgmtGlobals.SemId) == OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();

    return RFMGMT_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : RfMgmtWtpQueMsgHandler                               *
 *                                                                         *
 * DESCRIPTION      : Function is used to process the RFMGMT Q msgs.       *
 *                                                                         *
 * INPUT            : NONE                                                 *
 *                                                                         *
 * OUTPUT           : NONE                                                 *
 *                                                                         *
 * RETURNS          : NONE                                                 *
 *                                                                         *
 ***************************************************************************/
VOID
RfMgmtWtpQueMsgHandler (VOID)
{
    tRfMgmtQueueReq    *pMsg = NULL;

    RFMGMT_FN_ENTRY ();

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gRfMgmtGlobals.rfMgmtQueId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pMsg == NULL)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Received pointer is NULL\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Received pointer is NULL"));
            return;
        }

        switch (pMsg->u4MsgType)
        {
            case RFMGMT_NEIGH_SCAN_MSG:
                RfMgmtUpdateNeighbotApDetails (pMsg);
                break;

            case RFMGMT_CLIENT_SCAN_MSG:
                RfMgmtUpdateClientScanDetails (pMsg);
                break;
            case RFMGMT_SCAN_CHANNEL_LIST:
                RfMgmtUpdateAllowedChannelList (pMsg);
                break;
            case RFMGMT_DFS_CHANNEL_INFO:
                RfMgmtUpdateDFSChannelDetails (pMsg);
                break;
            case RFMGMT_RADAR_EVENT:
                RfMgmtHandleRadarEvent (pMsg);
                break;

            default:
            {
                RFMGMT_TRC (CONTROL_PLANE_TRC,
                            "RfMgmtQueMsgHandler: Unknown message type"
                            "received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtQueMsgHandler: Unknown message type"
                              "received"));
            }
                break;
        }
        /* Release the buffer to pool */
        MemReleaseMemBlock (RFMGMT_QUEUE_POOLID, (UINT1 *) pMsg);
    }

    RFMGMT_FN_EXIT ();

    return;
}

/*****************************************************************************/
/* Function Name      : RfMgmtWtpEnquePkts                                   */
/*                                                                           */
/* Description        : This function is to enque received packets           */
/*                      to the RFMGMT task                                   */
/*                                                                           */
/* Input(s)           : pQMsg - Received queue message                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
RfMgmtWtpEnquePkts (tRfMgmtMsgStruct * pMsg)
{
    tRfMgmtQueueReq    *pRfMgmtQueueReq = NULL;
    INT4                i4Status = RFMGMT_FAILURE;

    RFMGMT_FN_ENTRY ();

    pRfMgmtQueueReq = (tRfMgmtQueueReq *) MemAllocMemBlk (RFMGMT_QUEUE_POOLID);

    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RFMGMT - Memory Allocation Failed for "
                    "pool Id = RFMGMT_QUEUE_POOLID\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RFMGMT - Memory Allocation Failed for "
                      "pool Id = RFMGMT_QUEUE_POOLID"));
        return OSIX_FAILURE;
    }
    if (pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType == RFMGMT_DFS_CHANNEL_INFO)
    {
        MEMCPY (pRfMgmtQueueReq->DFSChannelInfo,
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.DFSChannelInfo,
                (sizeof (tRfMgmtDFSChannelStatus) * RFMGMT_MAX_CHANNELA));

        pRfMgmtQueueReq->u4RadioIfIndex = pMsg->unRfMgmtMsg.RfMgmtQueueReq.
            u4RadioIfIndex;
        pRfMgmtQueueReq->u4MsgType = pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType;
    }
    else if (pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType == RFMGMT_RADAR_EVENT)
    {
        pRfMgmtQueueReq->u4RadioIfIndex = pMsg->unRfMgmtMsg.RfMgmtQueueReq.
            u4RadioIfIndex;
        pRfMgmtQueueReq->u4MsgType = pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType;
        pRfMgmtQueueReq->u4EventType =
            pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4EventType;
        pRfMgmtQueueReq->u4Frequency =
            pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4Frequency;
    }

    else
    {

        if (pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType ==
            RFMGMT_CLIENT_SCAN_MSG)
        {
            MEMCPY (pRfMgmtQueueReq->ClientMacAddress,
                    pMsg->unRfMgmtMsg.RfMgmtQueueReq.ClientMacAddress,
                    sizeof (tMacAddr));
            pRfMgmtQueueReq->i2ClientSNR =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.i2ClientSNR;
        }
        else
        {
            MEMCPY (pRfMgmtQueueReq->NeighborMacAddr,
                    pMsg->unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                    sizeof (tMacAddr));
            pRfMgmtQueueReq->i2Rssi = pMsg->unRfMgmtMsg.RfMgmtQueueReq.i2Rssi;
            pRfMgmtQueueReq->u1SecChannelOffset =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.u1SecChannelOffset;
            pRfMgmtQueueReq->u1NonGFPresent =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.u1NonGFPresent;
            pRfMgmtQueueReq->u1OBSSNonGFPresent =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.u1OBSSNonGFPresent;
#ifdef ROGUEAP_WANTED
            pRfMgmtQueueReq->u1StationCount =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.u1StationCount;
            pRfMgmtQueueReq->isRogueApStatus =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.isRogueApStatus;
            pRfMgmtQueueReq->u4RogueApLastReportedTime =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4RogueApLastReportedTime;
            pRfMgmtQueueReq->isRogueApProcetionType =
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.isRogueApProcetionType;
#endif
        }
#ifdef ROGUEAP_WANTED
        MEMCPY (pRfMgmtQueueReq->au1ssid,
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.au1ssid, WSSMAC_MAX_SSID_LEN);

        MEMCPY (pRfMgmtQueueReq->au1RfGroupID,
                pMsg->unRfMgmtMsg.RfMgmtQueueReq.au1RfGroupID,
                WSS_RF_GROUP_ID_MAX_LEN);
#endif
    }

    pRfMgmtQueueReq->u4RadioIfIndex = pMsg->unRfMgmtMsg.RfMgmtQueueReq.
        u4RadioIfIndex;
    pRfMgmtQueueReq->u2ScannedChannel = pMsg->unRfMgmtMsg.RfMgmtQueueReq.
        u2ScannedChannel;
    pRfMgmtQueueReq->u4MsgType = pMsg->unRfMgmtMsg.RfMgmtQueueReq.u4MsgType;
    if (OsixQueSend (gRfMgmtGlobals.rfMgmtQueId, (UINT1 *) &pRfMgmtQueueReq,
                     OSIX_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        OsixEvtSend (gRfMgmtGlobals.rfMgmtTaskId, RFMGMT_QUE_MSG_EVENT);
        i4Status = OSIX_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "Failed to send the message to queue \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Failed to send the message to queue "));
        MemReleaseMemBlock (RFMGMT_QUEUE_POOLID, (UINT1 *) pRfMgmtQueueReq);
        i4Status = OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();

    return i4Status;
}

/*****************************************************************************/
/* Function     : RfMgmtConstructNeighborMsg                                 */
/*                                                                           */
/* Description  : This function will construct the neighbor message to the   */
/*                output structure.                                          */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the interface structure         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtConstructNeighborMsg (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtUpdateDB;
    tVendorNeighborAp  *pRfMgmtNeighborMsg = NULL;
    tRadioIfGetDB       RadioIfDB;
    UINT4               u4RadioIfIndex = 0;
    UINT1               u1ScanChannelCount = 0;
    UINT1               u1Count = 0;
    UINT1               au1Channel[RFMGMT_MAX_CHANNEL];
    UINT1               u1ChannelFlag = 0;
    UINT1               u1ChannelUpdated = 0;
    UINT1               u1Index = 0;
    UINT4               u4MaxChannel = 0;
    UINT4               u4RadioType = 0;
    tRfMgmtScanChannel *pTempScanChannel;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (au1Channel, 0, RFMGMT_MAX_CHANNEL);

    /* Validate the received input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtConstructNeighborMsg: Invalid input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructNeighborMsg: Invalid input received"));
        return RFMGMT_FAILURE;
    }

    pRfMgmtNeighborMsg = &pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.
        VendorNeighborAp;
    u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4RadioIfIndex = u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bAutoScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighConfig: Rfmgmt DB access failed"));
        return RFMGMT_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u1AutoScanStatus != RFMGMT_AUTO_SCAN_ENABLE)
    {
        RFMGMT_TRC (RFMGMT_INFO_TRC,
                    "RfMgmtConstructNeighborMsg: No entry found\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructNeighborMsg: No entry found"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_NO_ENTRY_FOUND;
    }

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    /* For the given radio ifindex fetch all the entries avilable in the DB */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_FIRST_NEIGH_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_INFO_TRC,
                    "RfMgmtConstructNeighborMsg: No entry found\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructNeighborMsg: No entry found"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_NO_ENTRY_FOUND;
    }

    RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        /* No entry found for this BSSID. which implies that the ap
         * may be manged by different WLC. However we require this
         * entry just for the neighbor AP information. */
        return RFMGMT_FAILURE;
    }

    /* Construct Neighbor message */
    pRfMgmtNeighborMsg->u4VendorId = VENDOR_ID;
    pRfMgmtNeighborMsg->isOptional = OSIX_TRUE;
    pRfMgmtNeighborMsg->u2MsgEleType = NEIGHBOR_AP_MSG;
    pRfMgmtNeighborMsg->u2MsgEleLen = RFMGMT_NEIGHBOR_MSG_LEN;

    pRfMgmtNeighborMsg->u1RadioId = RadioIfDB.RadioIfGetAllDB.u1RadioId;

    pRfMgmtNeighborMsg->u4Dot11RadioType =
        RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType;

    if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA) ||
        (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN) ||
        (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
    {
        u4RadioType = RFMGMT_RADIO_TYPEA;
        u4MaxChannel = RFMGMT_MAX_CHANNELA;
    }
    else if (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEB ||
             RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEBGN)
    {
        u4RadioType = RFMGMT_RADIO_TYPEB;
        u4MaxChannel = RFMGMT_MAX_CHANNELB;
    }
    else
    {
        u4RadioType = RFMGMT_RADIO_TYPEG;
    }

    do
    {
        /*Validations for construction of neighbors of same radiotype */
        if ((u4RadioType == RFMGMT_RADIO_TYPEA) &&
            RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.u2ScannedChannel <= RFMGMT_MAX_CHANNELB)
        {
            continue;
        }
        if ((u4RadioType == RFMGMT_RADIO_TYPEB) &&
            RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.u2ScannedChannel > RFMGMT_MAX_CHANNELB)
        {
            continue;
        }
        if ((u4RadioType == RFMGMT_RADIO_TYPEG) &&
            RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.u2ScannedChannel >= RFMGMT_MAX_CHANNELB)
        {
            continue;
        }
        u1ChannelUpdated = 0;

        /* Check whether the radio ifindex entry is present in the DB. If
         * present then get the scanned channels and neighbor AP information and
         * store it in the message structure. */
        if (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
            u4RadioIfIndex == u4RadioIfIndex)
        {
            for (u1Index = 0; u1Index < u4MaxChannel; u1Index++)
            {
                if ((au1Channel[u1Index] != 0) && (RfMgmtDB.unRfMgmtDB.
                                                   RfMgmtNeighborScanTable.
                                                   RfMgmtNeighborScanDB.
                                                   u2ScannedChannel ==
                                                   (UINT2) au1Channel[u1Index]))
                {
                    u1ChannelUpdated = 1;
                    break;
                }
            }
            if (u1ChannelUpdated == 1)
            {
                continue;
            }
            /* Newly scanned channel. Hence update both the scanned channel
             * and other information */
            if (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1EntryStatus != RFMGMT_NEIGH_ENTRY_NO_CHG)
            {
                u1Count = 0;

                pTempScanChannel = &pRfMgmtNeighborMsg->
                    ScanChannel[u1ScanChannelCount];
                pTempScanChannel->u1EntryStatus[u1Count] = RfMgmtDB.unRfMgmtDB.
                    RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.u1EntryStatus;

                pTempScanChannel->u2ScannedChannel = RfMgmtDB.unRfMgmtDB.
                    RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                    u2ScannedChannel;
                au1Channel[u1ChannelFlag] =
                    (UINT1) pTempScanChannel->u2ScannedChannel;
                u1ChannelFlag++;

                pTempScanChannel->i2Rssi[u1Count] = RfMgmtDB.unRfMgmtDB.
                    RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.i2Rssi;

#ifdef ROGUEAP_WANTED
                pTempScanChannel->isRogueApStatus[u1Count] =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.isRogueApStatus;

                pTempScanChannel->u1StationCount[u1Count] =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1StationCount;

                pTempScanChannel->u4RogueApLastReportedTime[u1Count] =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4RogueApLastReportedTime;
                pTempScanChannel->isRogueApProcetionType[u1Count] =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.isRogueApProcetionType;
                MEMCPY (pTempScanChannel->
                        au1ssid[u1Count], RfMgmtDB.unRfMgmtDB.
                        RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.au1ssid,
                        WSSMAC_MAX_SSID_LEN);

                MEMCPY (pTempScanChannel->
                        au1RfGroupID[u1Count], RfMgmtDB.unRfMgmtDB.
                        RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                        au1RfGroupID, WSS_RF_GROUP_ID_MAX_LEN);

                MEMCPY (pTempScanChannel->
                        BSSIDRogueApLearntFrom[u1Count], RfMgmtDB.unRfMgmtDB.
                        RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                        BSSIDRogueApLearntFrom, MAC_ADDR_LEN);

#endif
                MEMCPY (pTempScanChannel->
                        NeighborMacAddr[u1Count], RfMgmtDB.unRfMgmtDB.
                        RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                        NeighborAPMac, sizeof (tMacAddr));
                /* copying the values from DB to output structure */
                pTempScanChannel->
                    u1NonGFPresent =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1NonGFPresent;

                pTempScanChannel->u1OBSSNonGFPresent =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u1OBSSNonGFPresent;

                /* Increment the Channel scan count and neighbor Count */
                u1Count = RFMGMT_DEFAULT_COUNT;

                pTempScanChannel->u1NeighborApCount = u1Count;

                ++u1ScanChannelCount;

                pRfMgmtNeighborMsg->u2MsgEleLen = (UINT2)
                    (pRfMgmtNeighborMsg->u2MsgEleLen + RFMGMT_NEIGHBOR_MSG_LEN);

                RfMgmtUpdateNeighborEntryStatus (&RfMgmtDB.unRfMgmtDB.
                                                 RfMgmtNeighborScanTable.
                                                 RfMgmtNeighborScanDB);

                RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4RadioIfIndex =
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4RadioIfIndex;

                MEMCPY (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.NeighborAPMac,
                        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.NeighborAPMac, MAC_ADDR_LEN);

                while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_NEIGH_ENTRY,
                                           &RfMgmtUpdateDB) == RFMGMT_SUCCESS)
                {
                    if ((RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                         RfMgmtNeighborScanDB.u2ScannedChannel ==
                         RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                         RfMgmtNeighborScanDB.u2ScannedChannel)
                        && (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanDB.u4RadioIfIndex ==
                            u4RadioIfIndex))
                    {
                        if (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanDB.u1EntryStatus !=
                            RFMGMT_NEIGH_ENTRY_NO_CHG)
                        {
                            /*Check for maximum number of neighbor entries for a
                             *channel particular*/
                            if (u1Count == RFMGMT_MAX_NEIGHBOR_AP)
                            {
                                continue;
                            }
                            --u1ScanChannelCount;
                            /* Already scanned channel. Hence update only the
                             * neighbor MAC and RSSI */
                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                u1EntryStatus[u1Count] =
                                RfMgmtUpdateDB.unRfMgmtDB.
                                RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                                u1EntryStatus;

                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                i2Rssi[u1Count] = RfMgmtUpdateDB.unRfMgmtDB.
                                RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                                i2Rssi;

                            MEMCPY (pRfMgmtNeighborMsg->
                                    ScanChannel[u1ScanChannelCount].
                                    NeighborMacAddr[u1Count],
                                    RfMgmtUpdateDB.unRfMgmtDB.
                                    RfMgmtNeighborScanTable.
                                    RfMgmtNeighborScanDB.NeighborAPMac,
                                    sizeof (tMacAddr));
                            /* copying the values from DB to output structure */
#ifdef ROGUEAP_WANTED
                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                isRogueApStatus[u1Count] =
                                RfMgmtUpdateDB.unRfMgmtDB.
                                RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                                isRogueApStatus;

                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                u4RogueApLastReportedTime[u1Count] =
                                RfMgmtUpdateDB.unRfMgmtDB.
                                RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                                u4RogueApLastReportedTime;
                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                isRogueApProcetionType[u1Count] =
                                RfMgmtUpdateDB.unRfMgmtDB.
                                RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                                isRogueApProcetionType;
                            MEMCPY (pRfMgmtNeighborMsg->
                                    ScanChannel[u1ScanChannelCount].
                                    au1ssid[u1Count],
                                    RfMgmtUpdateDB.unRfMgmtDB.
                                    RfMgmtNeighborScanTable.
                                    RfMgmtNeighborScanDB.au1ssid,
                                    WSSMAC_MAX_SSID_LEN);
                            MEMCPY (pRfMgmtNeighborMsg->
                                    ScanChannel[u1ScanChannelCount].
                                    au1RfGroupID[u1Count],
                                    RfMgmtUpdateDB.unRfMgmtDB.
                                    RfMgmtNeighborScanTable.
                                    RfMgmtNeighborScanDB.au1RfGroupID,
                                    WSS_RF_GROUP_ID_MAX_LEN);
                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                u1StationCount[u1Count] =
                                RfMgmtUpdateDB.unRfMgmtDB.
                                RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
                                u1StationCount;

                            MEMCPY (pRfMgmtNeighborMsg->
                                    ScanChannel[u1ScanChannelCount].
                                    BSSIDRogueApLearntFrom[u1Count],
                                    RfMgmtUpdateDB.unRfMgmtDB.
                                    RfMgmtNeighborScanTable.
                                    RfMgmtNeighborScanDB.BSSIDRogueApLearntFrom,
                                    MAC_ADDR_LEN);
#endif

                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                u1NonGFPresent =
                                RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.u1NonGFPresent;

                            pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                                u1OBSSNonGFPresent =
                                RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                                RfMgmtNeighborScanDB.u1OBSSNonGFPresent;

                            pRfMgmtNeighborMsg->u2MsgEleLen = (UINT2)
                                (pRfMgmtNeighborMsg->u2MsgEleLen +
                                 RFMGMT_NEIGHBOR_MSG_LEN);

                            /* Increment the Channel scan count and neighbor Count */
                            ++u1Count;
                            /*pRfMgmtNeighborMsg->ScanChannel[u1ScanChannelCount].
                               u1NeighborApCount = u1Count; */
                            pTempScanChannel->u1NeighborApCount = u1Count;

                            ++u1ScanChannelCount;

                            RfMgmtUpdateNeighborEntryStatus (&RfMgmtUpdateDB.
                                                             unRfMgmtDB.
                                                             RfMgmtNeighborScanTable.
                                                             RfMgmtNeighborScanDB);
                        }
                    }
                }
            }
        }
    }
    while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_NEIGH_ENTRY,
                               &RfMgmtDB) == RFMGMT_SUCCESS);

    if (pRfMgmtNeighborMsg->u2MsgEleLen == RFMGMT_NEIGHBOR_MSG_LEN)
    {
        /* No entry exist for the radio index. Hence memset the output 
         * structure */
        RFMGMT_FN_EXIT ();
        return RFMGMT_NO_ENTRY_FOUND;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtConstructClientMsg                                   */
/*                                                                           */
/* Description  : This function will construct the Client SNR message in AP  */
/*                send to the WLC.                                           */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the interface structure         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtConstructClientMsg (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtUpdateDB;
    tVendorClientScan  *pRfMgmtClientMsg = NULL;
    tRadioIfGetDB       RadioIfDB;
    UINT4               u4RadioIfIndex = 0;
    UINT1               u1OpCode = 0;
    UINT1               u1Count = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));

    /* Validate the received input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtConstructClientMsg: Invalid input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructClientMsg: Invalid input received"));
        return RFMGMT_FAILURE;
    }

    pRfMgmtClientMsg = &pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.
        VendorClientScan;
    u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

    RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        /* No entry found for this BSSID. which implies that the ap
         * may be manged by different WLC. However we require this
         * entry just for the neighbor AP information. */
        return RFMGMT_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4RadioIfIndex = u4RadioIfIndex;

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigIsSetDB.
        bSNRScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtConstructClientMsg: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructClientMsg: Rfmgmt DB access failed"));
        return RFMGMT_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u1SNRScanStatus != RFMGMT_SNR_SCAN_ENABLE)
    {
        RFMGMT_TRC (RFMGMT_INFO_TRC,
                    "RfMgmtConstructClientMsg: No entry found\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructClientMsg: No entry found"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_NO_ENTRY_FOUND;
    }

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* For the given radio ifindex fetch all the entries avilable in the DB */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_FIRST_CLIENT_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_INFO_TRC,
                    "RfMgmtConstructClientMsg: No entry found\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtConstructClientMsg: No entry found"));
        RFMGMT_FN_EXIT ();
        return RFMGMT_NO_ENTRY_FOUND;
    }

    /* Construct Client message */
    pRfMgmtClientMsg->u4VendorId = VENDOR_ID;
    pRfMgmtClientMsg->isOptional = OSIX_TRUE;

    pRfMgmtClientMsg->u2MsgEleType = CLIENT_SCAN_MSG;
    pRfMgmtClientMsg->u2MsgEleLen = RFMGMT_CLIENT_SCAN_MSG_LEN;
    pRfMgmtClientMsg->u1RadioId = RadioIfDB.RadioIfGetAllDB.u1RadioId;

    do
    {
        /* Check whether the radio ifindex entry is present in the DB. If
         * present then get the Client information and
         * store it in the message structure. */
        if (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
            u4RadioIfIndex == u4RadioIfIndex)
        {
            if (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus != RFMGMT_CLIENT_ENTRY_NO_CHG)
            {
                /* Copy all the client related information for the received
                 * radio index */
                pRfMgmtClientMsg->u1EntryStatus[u1Count] =
                    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus;

                pRfMgmtClientMsg->i2ClientSNR[u1Count] = RfMgmtDB.unRfMgmtDB.
                    RfMgmtClientScanTable.RfMgmtClientScanDB.i2ClientSNR;

                MEMCPY (pRfMgmtClientMsg->ClientMacAddress[u1Count],
                        RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanDB.ClientMacAddress, sizeof (tMacAddr));

                /* Increment the Channel scan count and neighbor Count */
                ++u1Count;

                pRfMgmtClientMsg->u1ClientCount = u1Count;
                pRfMgmtClientMsg->u2MsgEleLen = (UINT2)
                    (pRfMgmtClientMsg->u2MsgEleLen +
                     RFMGMT_CLIENT_SCAN_FIXED_LEN);
            }
            /* Update the entry status to no change since the message 
             * is already constrcuted */
            RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex =
                RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex;
            MEMCPY (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress,
                    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress, MAC_ADDR_LEN);

            /* If the entry is in add status change to no change 
             * status. Else if the entry is in delete status. 
             * Then remove the entry from the DB */
            if (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus == RFMGMT_CLIENT_ENTRY_ADD)
            {
                RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus =
                    RFMGMT_CLIENT_ENTRY_NO_CHG;

                u1OpCode = RFMGMT_SET_CLIENT_SCAN_ENTRY;

                RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanIsSetDB.bEntryStatus = OSIX_TRUE;
            }
            if (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus == RFMGMT_CLIENT_ENTRY_DELETE)
            {
                u1OpCode = RFMGMT_DESTROY_CLIENT_SCAN_ENTRY;
            }
            if (u1OpCode != 0)
            {
                if (RfMgmtProcessDBMsg (u1OpCode,
                                        &RfMgmtUpdateDB) == RFMGMT_FAILURE)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtConstructClientMsg: DB updation "
                                "failed\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtConstructClientMsg: DB updation "
                                  "failed"));
                    RFMGMT_FN_EXIT ();
                    return RFMGMT_SUCCESS;
                }
                u1OpCode = 0;
            }
        }
    }
    while (RfMgmtProcessDBMsg (RFMGMT_GET_NEXT_CLIENT_ENTRY,
                               &RfMgmtDB) == RFMGMT_SUCCESS);

    if (pRfMgmtClientMsg->u2MsgEleLen == RFMGMT_CLIENT_SCAN_MSG_LEN)
    {
        /* No entry exist for the radio index. Hence memset the output 
         * structure */
        MEMSET (pRfMgmtClientMsg, 0, sizeof (tVendorClientScan));
        RFMGMT_FN_EXIT ();
        return RFMGMT_NO_ENTRY_FOUND;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessWssIfMsg                                      */
/*                                                                           */
/* Description  : To call the appropriate Func calls in RF MGMT from external*/
/*                modules                                                    */
/*                                                                           */
/* Input        : u1OpCode - Received OP code                                */
/*                pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessWssIfMsg (UINT1 u1OpCode, tRfMgmtMsgStruct * pWssMsgStruct)
{
    UINT1               u1RetStatus = RFMGMT_FAILURE;

    RFMGMT_FN_ENTRY ();

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessWssIfMsg: Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessWssIfMsg: Null input received"));
        return RFMGMT_FAILURE;
    }

    switch (u1OpCode)
    {
        case RFMGMT_CREATE_RADIO_ENTRY:
        case RFMGMT_DELETE_RADIO_ENTRY:
            u1RetStatus = RfMgmtApRadioConfigRequest (u1OpCode, pWssMsgStruct);
            break;

        case RFMGMT_WTP_CONSTRUCT_NEIGH_MSG:
            u1RetStatus = RfMgmtConstructNeighborMsg (pWssMsgStruct);
            break;

        case RFMGMT_WTP_CONSTRUCT_CLIENT_SCAN_MSG:
            u1RetStatus = RfMgmtConstructClientMsg (pWssMsgStruct);
            break;

        case RFMGMT_CONFIG_STATUS_RSP:
            u1RetStatus = RfMgmtProcessConfigStatusRsp (pWssMsgStruct);
            break;

        case RFMGMT_CONFIG_UPDATE_REQ:
            u1RetStatus = RfMgmtProcessConfigUpdateReq (pWssMsgStruct);
            break;

        case RFMGMT_CHANNEL_UPDATE_MSG:
            u1RetStatus = RfMgmtProcessChannelUpdateReq (pWssMsgStruct);
            break;

        case RFMGMT_TXPOWER_UPDATE_MSG:
            u1RetStatus = RfMgmtProcessTxPowerUpdateReq (pWssMsgStruct);
            break;

        case RFMGMT_DELETE_CLIENT_STATS:
        {
            u1RetStatus = RfMgmtDeleteClientStats (pWssMsgStruct);
        }
            break;
        case RFMGMT_VENDOR_NEIGHBOR_AP_TYPE:
            u1RetStatus = RfMgmtConstructNeighborMsg (pWssMsgStruct);
            break;
        case RFMGMT_VENDOR_CLIENT_SCAN_TYPE:
            u1RetStatus = RfMgmtConstructClientMsg (pWssMsgStruct);
            break;
        case RFMGMT_WLAN_DELETION_MSG:
        case RFMGMT_RADIO_DOWN_MSG:
        {
            u1RetStatus = RfMgmtDeleteNeighborDetails (pWssMsgStruct->
                                                       unRfMgmtMsg.
                                                       RfMgmtIntfConfigReq.
                                                       u4RadioIfIndex,
                                                       RFMGMT_FLUSH_DB);
        }
            break;
            /* Return failure for invalid opcode */
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessWssIfMsg: Invalid OpCode, return failure\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtProcessWssIfMsg: Invalid OpCode, return failure"));
            u1RetStatus = RFMGMT_FAILURE;
    }

    if (u1RetStatus == RFMGMT_FAILURE)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtProcessWssIfMsg: Failed in processing msg = %d\r\n",
                     u1OpCode);
    }
    RFMGMT_FN_EXIT ();

    return u1RetStatus;
}

/*****************************************************************************/
/* Function     : RfMgmtStartNeighborAPScan                                  */
/*                                                                           */
/* Description  : This function will invoke the NPAPI to start scanning for  */
/*                neighbor AP messages.                                      */
/*                                                                           */
/* Input        : pRfMgmtAPConfigDB - Pointer to the input structure         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtStartNeighborAPScan (tRfMgmtAPConfigDB * pRfMgmtAPConfigDB,
                           UINT1 u1Channel)
{
#ifdef NPAPI_WANTED
    UINT1               i1RetVal = 0;
    tRfMgmtNpWrHwScanNeighInfo Entry;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1WlanId = WLAN_DEF_INTERFACE;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RadioId = 0;
    UINT1               u1Index = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    MEMSET (&Entry, 0, sizeof (tRfMgmtNpWrHwScanNeighInfo));

    Entry.RadInfo.au2WlanIfIndex[0] = u1WlanId;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtAPConfigDB->u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
    }
    else
    {
        for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) == OSIX_SUCCESS)
            {
                if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                    break;
            }
            else
                continue;
        }
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = 1;
    }
    else
    {
        for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          (&RadioIfGetDB)) == OSIX_SUCCESS)
            {
                if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                    break;
            }
            else
                continue;
        }
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus ==
        RADIOIF_ADMIN_STATUS_DISABLED)
    {
        return RFMGMT_SUCCESS;
    }
    /*No need to scan if Bssid count is 0 */
    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        return RFMGMT_SUCCESS;
    }

    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) Entry.RadInfo.au1RadioIfName,
             "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    WssWlanGetWlanIntfName (u1RadioId, u1WlanId, Entry.RadInfo.au1WlanIfname[0],
                            &Entry.RadInfo.u1WlanId);

    Entry.u1ChannelToBeScanned = u1Channel;

    i1RetVal = FsRfMgmtHwScanNeighborInfo (&Entry);

    if (i1RetVal != FNP_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtStartNeighborAPScan : Start hardware scan "
                    "failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtStartNeighborAPScan : Start hardware scan "
                      "failed"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
#endif
#ifdef NPAPI_WANTED_WRAPPER
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1WlanId = WLAN_DEF_INTERFACE;
    UINT1               u1RadioId = 0;
    RFMGMT_FN_ENTRY ();

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /* Generic NP structure */
                         NP_RFMGMT_MODULE,    /* Module ID */
                         FS_RFMGMT_SCAN_NEIGHBR_MSG,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    /* Check for invalid input */
    if (pRfMgmtAPConfigDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4RfmSysLogId,
                                  "RfMgmtStartNeighborAPScan: Null input received")));
        return RFMGMT_FAILURE;
    }

    if (pRfMgmtAPConfigDB->u4RadioIfIndex == 0)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtStartNeighborAPScan: Invalid Radio IfIndex\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4RfmSysLogId,
                      "RfMgmtStartNeighborAPScan: Invalid Radio IfIndex"));
        return RFMGMT_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtAPConfigDB->u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    u1RadioId = pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) FsHwNp.RfMgmtNpModInfo.unOpCode.sConfigScanNeighInfo.
             RadInfo.au1RadioIfName, "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    FsHwNp.RfMgmtNpModInfo.unOpCode.sConfigScanNeighInfo.
        RadInfo.au2WlanIfIndex[0] = u1WlanId;
    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME, u1WlanId - 1);
    STRCPY (FsHwNp.RfMgmtNpModInfo.unOpCode.
            sConfigScanNeighInfo.RadInfo.au1WlanIfname[0], au1Wname);
    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtStartNeighborAPScan : Values to get from "
                    "Hw failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4RfmSysLogId,
                      "RfMgmtStartNeighborAPScan : Values to get from "
                      "Hw failed"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
#else
    UNUSED_PARAM (pRfMgmtAPConfigDB);
    UNUSED_PARAM (u1Channel);
#endif
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtStartClientSNRScan                                   */
/*                                                                           */
/* Description  : This function will invoke the NPAPI to start scanning for  */
/*                neighbor AP messages.                                      */
/*                                                                           */
/* Input        : pRfMgmtClientConfigDB - Pointer to the input structure     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtStartClientSNRScan (tRfMgmtClientConfigDB * pRfMgmtClientConfigDB)
{
#ifdef NPAPI_WANTED
    UINT1               i1RetVal = 0;
    tRfMgmtNpWrHwScanClientInfo Entry;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1WlanId = WLAN_DEF_INTERFACE;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RadioId = 0;
    UINT1               u1Index = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&Entry, 0, sizeof (tRfMgmtNpWrHwScanNeighInfo));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = pRfMgmtClientConfigDB->u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }
    if (RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus ==
        RADIOIF_ADMIN_STATUS_DISABLED)
    {
        return RFMGMT_SUCCESS;
    }
    /*No need to scan if Bssid count is 0 */
    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        return RFMGMT_SUCCESS;
    }

    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) Entry.RadInfo.au1RadioIfName,
             "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    Entry.RadInfo.au2WlanIfIndex[0] = u1WlanId;

    Entry.RadInfo.u4RadioIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) Entry.RadInfo.au1RadioIfName,
             "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    for (u1Index = 1; u1Index <= RADIO_MAX_BSSID_SUPPORTED; u1Index++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1Index;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      (&RadioIfGetDB)) == OSIX_SUCCESS)
        {
            if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
            {
                WssWlanGetWlanIntfName (u1RadioId,
                                        RadioIfGetDB.RadioIfGetAllDB.u1WlanId,
                                        Entry.RadInfo.au1WlanIfname[0],
                                        &Entry.RadInfo.u1WlanId);
                i1RetVal = FsRfMgmtHwScanClientInfo (&Entry);

                if (i1RetVal != FNP_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtStartClientSNRScan: Start hardware scan "
                                "failed\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtStartClientSNRScan: Start hardware scan "
                                  "failed"));
                    return RFMGMT_FAILURE;
                }
            }
        }
        else
            continue;

    }
#endif
#ifdef NPAPI_WANTED_WRAPPER
    tFsHwNp             FsHwNp;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1WlanId = WLAN_DEF_INTERFACE;

    RFMGMT_FN_ENTRY ();

    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /* Generic NP structure */
                         NP_RFMGMT_MODULE,    /* Module ID */
                         FS_RFMGMT_SCAN_CLIENT_MSG,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    /* Check for invalid input */
    if (pRfMgmtClientConfigDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtStartClientSNRScan: Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4RfmSysLogId,
                      "RfMgmtStartClientSNRScan: Null input received"));
        return RFMGMT_FAILURE;
    }

    if (pRfMgmtClientConfigDB->u4RadioIfIndex == 0)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtStartClientSNRScan: Invalid Radio IfIndex\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4RfmSysLogId,
                      "RfMgmtStartClientSNRScan: Invalid Radio IfIndex"));
        return RFMGMT_FAILURE;
    }

    FsHwNp.RfMgmtNpModInfo.unOpCode.sConfigScanClientInfo.
        RadInfo.au2WlanIfIndex[0] = u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME, u1WlanId - 1);

    STRCPY (FsHwNp.RfMgmtNpModInfo.unOpCode.sConfigScanClientInfo.
            RadInfo.au1WlanIfname[0], au1Wname);

    /*To Set in the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtStartClientSNRScan : Values to get from "
                    "Hw failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4RfmSysLogId,
                      "RfMgmtStartClientSNRScan : Values to get from "
                      "Hw failed"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();

#else
    UNUSED_PARAM (pRfMgmtClientConfigDB);
#endif

    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateNeighbotApDetails                              */
/*                                                                           */
/* Description  : This function will get invoked whenever a neighbor ap info */
/*                is scanned. The DB should be created/updated for the       */
/*                all the entries received                                   */
/*                                                                           */
/* Input        : pRfMgmtQueueReq - Pointer to input structure               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateNeighbotApDetails (tRfMgmtQueueReq * pRfMgmtQueueReq)
{
    tRfMgmtNeighborScanDB RfMgmtNeighborScanDB;
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfDB;
    UINT1               u1MsgType = RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY;
    MEMSET (&RfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    /*        
     *  Following code is commented out as a fix  for ipv6 blocker
     *  This needs to be reverted back,   
     *  once the HT 20-40 co-existence is working fine.
     */

    /*
       UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
       MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
     */
    RFMGMT_FN_ENTRY ();
    /* Validate for invalid input */
    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "Invalid input received - Queue msg NULL\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Invalid input received - Queue msg NULL"));
        return RFMGMT_FAILURE;
    }

    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = pRfMgmtQueueReq->u4RadioIfIndex;
    RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        /* No entry found for this BSSID. which implies that the ap
         * may be manged by different WLC. However we require this
         * entry just for the neighbor AP information. */
    }

    if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC) ||
        (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN) ||
        (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEBGN))
    {
        RadioIfDB.RadioIfGetAllDB.u1HTCapEnable = DOT11_INFO_ELEM_HTCAPABILITY;
        RadioIfDB.RadioIfGetAllDB.u1HTOpeEnable = DOT11_INFO_ELEM_HTOPERATION;
        RadioIfDB.RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
        RadioIfDB.RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
        RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
            pRfMgmtQueueReq->u4RadioIfIndex;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                      &RadioIfDB) != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "WssWlanProcessConfigRequest:: RadioDBMsg\
                    failed to return MacAddr\r\n");
            return OSIX_FAILURE;
        }
    }
    if (MEMCMP (RadioIfDB.RadioIfGetAllDB.MacAddr, pRfMgmtQueueReq->
                NeighborMacAddr, sizeof (tMacAddr)) == 0)
    {
        /*Skip to update the DB when the AP scans the BSSID which 
         *is the same as its own*/
        return RFMGMT_SUCCESS;
    }
    if (((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA) ||
         (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)) &&
        (pRfMgmtQueueReq->u2ScannedChannel <= RFMGMT_MAX_CHANNELB))
    {
        RFMGMT_TRC (RFMGMT_MGMT_TRC,
                    "Updation of details of different radio type not required\r\n");
        return RFMGMT_SUCCESS;
    }
    else if (((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEB)
              || (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBG)
              || (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType ==
                  RFMGMT_RADIO_TYPEBGN))
             && (pRfMgmtQueueReq->u2ScannedChannel > RFMGMT_MAX_CHANNELB))
    {
        RFMGMT_TRC (RFMGMT_MGMT_TRC,
                    "Updation of details of different radio type not required\r\n");
        return RFMGMT_SUCCESS;
    }
    else if ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEG)
             && (pRfMgmtQueueReq->u2ScannedChannel > RFMGMT_MAX_CHANNELB))
    {
        RFMGMT_TRC (RFMGMT_MGMT_TRC,
                    "Updation of details of different radio type not required\r\n");
        return RFMGMT_SUCCESS;
    }

#ifdef ROGUEAP_WANTED
    WssWlanDB.WssWlanAttributeDB.u1RadioId =
        RadioIfDB.RadioIfGetAllDB.u1RadioId;
    WssWlanDB.WssWlanAttributeDB.u1WlanId = 1;
    WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB)
        == OSIX_SUCCESS)
    {
        MEMCPY (&RfMgmtNeighborScanDB.BSSIDRogueApLearntFrom,
                WssWlanDB.WssWlanAttributeDB.BssId, sizeof (tMacAddr));
    }
#endif

    /* Copy the neighbor ap details from the linear buffer */
    MEMCPY (&RfMgmtNeighborScanDB.NeighborAPMac,
            pRfMgmtQueueReq->NeighborMacAddr, sizeof (tMacAddr));

    RfMgmtNeighborScanDB.u4RadioIfIndex = pRfMgmtQueueReq->u4RadioIfIndex;
    RfMgmtNeighborScanDB.u2ScannedChannel = pRfMgmtQueueReq->u2ScannedChannel;
    RfMgmtNeighborScanDB.i2Rssi = pRfMgmtQueueReq->i2Rssi;
#ifdef ROGUEAP_WANTED
    MEMCPY (&RfMgmtNeighborScanDB.au1ssid, pRfMgmtQueueReq->au1ssid,
            WSSMAC_MAX_SSID_LEN);

    MEMCPY (&RfMgmtNeighborScanDB.au1RfGroupID, pRfMgmtQueueReq->au1RfGroupID,
            WSS_RF_GROUP_ID_MAX_LEN);

    RfMgmtNeighborScanDB.u1StationCount = pRfMgmtQueueReq->u1StationCount;
    RfMgmtNeighborScanDB.isRogueApStatus = pRfMgmtQueueReq->isRogueApStatus;
    RfMgmtNeighborScanDB.u4RogueApLastReportedTime =
        pRfMgmtQueueReq->u4RogueApLastReportedTime;
    RfMgmtNeighborScanDB.isRogueApProcetionType =
        pRfMgmtQueueReq->isRogueApProcetionType;
    RfMgmtNeighborScanDB.u1StationCount = pRfMgmtQueueReq->u1StationCount;
#endif

    if (pRfMgmtQueueReq->u2ScannedChannel ==
        RadioIfDB.RadioIfGetAllDB.u1CurrentChannel)
    {
        RfMgmtNeighborScanDB.u1NonGFPresent = pRfMgmtQueueReq->u1NonGFPresent;
        RfMgmtNeighborScanDB.u1OBSSNonGFPresent =
            pRfMgmtQueueReq->u1OBSSNonGFPresent;
    }
/*copying HT info to RF db*/
    MEMCPY (&RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB, &RfMgmtNeighborScanDB,
            sizeof (tRfMgmtNeighborScanDB));
#ifndef RFMGMT_TEST_WANTED
#ifdef ASKEY_WANTED
    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
        NeighborAPMac[MAC_ADDR_LEN - 1] =
        (UINT1) (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                 RfMgmtNeighborScanDB.NeighborAPMac[MAC_ADDR_LEN - 1] & 0xF0);
#endif
#endif
    /* Invoke the DB to update the neighbor entry in the hardware */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY,
                            &RfMgmtDB) == RFMGMT_SUCCESS)
    {
        u1MsgType = RFMGMT_SET_NEIGHBOR_SCAN_ENTRY;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanIsSetDB.
        bRssi = OSIX_TRUE;

    /* Create the DB entry */
    if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtGetNeighborInfo: RF MGMT DB "
                    "processing failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtGetNeighborInfo: RF MGMT DB "
                      "processing failed"));
        return RFMGMT_FAILURE;
    }
#ifdef NPAPI_WANTED
    if (((UINT1) pRfMgmtQueueReq->u2ScannedChannel ==
         RadioIfDB.RadioIfGetAllDB.u1CurrentChannel) &&
        ((pRfMgmtQueueReq->u1NonGFPresent != 0)
         || (pRfMgmtQueueReq->u1OBSSNonGFPresent != 0)))
    {
        if (RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY,
                                &RfMgmtDB) == RFMGMT_SUCCESS)
        {
            /*        
             *  Following code is commented out as a fix  for ipv6 blocker
             *  This needs to be reverted back,   
             *  once the HT 20-40 co-existence is working fine.
             */
            /*
               MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
               SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,    
               RadioIfDB.RadioIfGetAllDB.u1RadioId - 1);
             */
            RadioIfDB.RadioIfGetAllDB.Dot11NhtOperation.u1ObssNonHtStasPresent =
                RfMgmtNeighborScanDB.u1OBSSNonGFPresent;
            RadioIfDB.RadioIfGetAllDB.Dot11NhtOperation.u1IsGreenFieldPresent =
                RfMgmtNeighborScanDB.u1NonGFPresent;
            /*  
             *  Following code is commented out as a fix  for ipv6 blocker
             *  This needs to be reverted back,   
             *  once the HT 20-40 co-existence is working fine.
             */

            /*
               RadioIfSetDot11N (&RadioIfDB , au1RadioName);
             */
        }
    }
#endif
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateClientScanDetails                              */
/*                                                                           */
/* Description  : This function will get invoked whenever a Client info is   */
/*                scanned. The DB should be created/updated for all the      */
/*                entries received                                           */
/*                                                                           */
/* Input        : pRfMgmtQueueReq - Pointer to input structure               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateClientScanDetails (tRfMgmtQueueReq * pRfMgmtQueueReq)
{
    tRfMgmtClientScanDB RfMgmtClientScanDB;
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDB           RfMgmtConfigDB;
    tWssStaStateDB      WssStaStateDB;
    UINT1               u1MsgType = RFMGMT_CREATE_CLIENT_SCAN_ENTRY;

    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    MEMSET (&RfMgmtConfigDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&WssStaStateDB, 0, sizeof (tWssStaStateDB));

    /* Validate for invalid input */
    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Invalid input received"));
        return RFMGMT_FAILURE;
    }

    /* Copy the neighbor ap details from the linear buffer */

    MEMCPY (&RfMgmtClientScanDB.ClientMacAddress,
            pRfMgmtQueueReq->ClientMacAddress, sizeof (tMacAddr));
    RfMgmtClientScanDB.u4RadioIfIndex = pRfMgmtQueueReq->u4RadioIfIndex;
    RfMgmtClientScanDB.i2ClientSNR = pRfMgmtQueueReq->i2ClientSNR;
    MEMCPY (&RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB,
            &RfMgmtClientScanDB, sizeof (tRfMgmtClientScanDB));

#ifndef RFMGMT_TEST_WANTED
    MEMCPY (WssStaStateDB.stationMacAddress,
            pRfMgmtQueueReq->ClientMacAddress, MAC_ADDR_LEN);
    /*   Need not update the client scan details if the client mac address 
       is not present in the WssStaGetStationDB    */
    if (WssStaGetStationDB (&WssStaStateDB) == OSIX_FAILURE)
    {
        RFMGMT_TRC6 (RFMGMT_FAILURE_TRC,
                     "Invalid mac address:%x:%x:%x:%x:%x:%x\r\n",
                     WssStaStateDB.stationMacAddress[0],
                     WssStaStateDB.stationMacAddress[1],
                     WssStaStateDB.stationMacAddress[2],
                     WssStaStateDB.stationMacAddress[3],
                     WssStaStateDB.stationMacAddress[4],
                     WssStaStateDB.stationMacAddress[5]);
        return RFMGMT_FAILURE;
    }

    /* Changes to skip Client Scan on a particular BSSID */
    RfMgmtConfigDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = pRfMgmtQueueReq->u4RadioIfIndex;
    RfMgmtConfigDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u1WlanId = WssStaStateDB.u1WlanId;
    RfMgmtConfigDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bBssidScanStatus = OSIX_TRUE;

    /* Invoke the DB to update the client scan entry in the hardware */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY,
                            &RfMgmtConfigDB) == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid radio index received\r\n");
    }
    else
    {
        if (RfMgmtConfigDB.unRfMgmtDB.RfMgmtClientConfigTable.
            RfMgmtClientConfigDB.u1BssidScanStatus
            [WssStaStateDB.u1WlanId - 1] == RFMGMT_BSSID_SCAN_STATUS_DISABLE)
        {
            if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_SCAN_ENTRY,
                                    &RfMgmtDB) == RFMGMT_SUCCESS)
            {
                if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_CLIENT_SCAN_ENTRY,
                                        &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Destroy entry in Client DB"
                                "failed \r\n");
                    return RFMGMT_SUCCESS;

                }
            }
            RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "SNR Scanning disabled. Skipping "
                         "entry for WLAN ID %d\r\n", WssStaStateDB.u1WlanId);
            return RFMGMT_SUCCESS;
        }
    }

#endif
    /* Invoke the DB to update the client scan entry in the hardware */
    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_SCAN_ENTRY,
                            &RfMgmtDB) == RFMGMT_SUCCESS)
    {
        u1MsgType = RFMGMT_SET_CLIENT_SCAN_ENTRY;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanIsSetDB.
        bClientSNR = OSIX_TRUE;

    /* Create the DB entry */
    if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtUpdateClientScanDetails: RF MGMT DB "
                    "processing failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtUpdateClientScanDetails: RF MGMT DB "
                      "processing failed"));
        return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateAllowedChannelList                             */
/*                                                                           */
/* Description  : This function will get invoked whenever a Client info is   */
/*                scanned. The DB should be created/updated for all the      */
/*                entries received                                           */
/*                                                                           */
/* Input        : pRfMgmtQueueReq - Pointer to input structure               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateAllowedChannelList (tRfMgmtQueueReq * pRfMgmtQueueReq)
{
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    /* Validate for invalid input */
    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Invalid input received"));
        return RFMGMT_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
        RfMgmtChannelAllowedScanDB.u4RadioIfIndex =
        pRfMgmtQueueReq->u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
        RfMgmtChannelAllowedScanDB.u1Channel =
        (UINT1) pRfMgmtQueueReq->u2ScannedChannel;
    RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
        RfMgmtChannelAllowedScanDB.u1SecChannelOffset =
        pRfMgmtQueueReq->u1SecChannelOffset;

    if (pRfMgmtQueueReq->u2ScannedChannel > 14)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
            RfMgmtChannelAllowedScanDB.u4RadioType = RFMGMT_RADIO_TYPEA;
    }
    else
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtChannelAllowedScanTable.
            RfMgmtChannelAllowedScanDB.u4RadioType = RFMGMT_RADIO_TYPEB;
    }
    if (RfMgmtProcessDBMsg (RFMGMT_CREATE_SCAN_ALLOWED_ENTRY,
                            &RfMgmtDB) == RFMGMT_SUCCESS)
    {
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtApRadioConfigRequest                                 */
/*                                                                           */
/* Description  : To create or delete the radio interface related DB when a  */
/*                Radio Interface is initialized                             */
/*                                                                           */
/* Input        : u1OpCode - Received OP code                                */
/*                pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtApRadioConfigRequest (UINT1 u1OpCode, tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1MsgType = 0;
    UINT4               u4RadioIfIndex = 0;

    RFMGMT_FN_ENTRY ();
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtApRadioConfigRequest : Null input received\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtApRadioConfigRequest : Null input received"));
        return RFMGMT_FAILURE;
    }

    /* Copy the radio ifindex and radio type if valid, else return failure */
    if (pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex != 0)
    {
        /* Assign the message type to be processed by DB based on the 
         * input received */
        if (u1OpCode == RFMGMT_CREATE_RADIO_ENTRY)
        {
            u1MsgType = RFMGMT_CREATE_RADIO_IF_INDEX_ENTRY;
        }
        else
        {
            u1MsgType = RFMGMT_DESTROY_RADIO_IF_INDEX_ENTRY;

            /* Stop the neighbor msg expiry timer */
            RfMgmtWtpTmrStop (RFMGMT_NEIGH_SCAN_TMR);

            /* Stop the channel scan timer */
            RfMgmtWtpTmrStop (RFMGMT_CHAN_SCAN_TMR);

            /* Stop the periodic message to AC timer */
            RfMgmtWtpTmrStop (RFMGMT_TX_MESSAGE_EXP_TMR);

            /* Stop the client msg expiry timer */
            RfMgmtWtpTmrStop (RFMGMT_CLIENT_SNR_SCAN_TMR);

            /* Stop the tpc request  expiry timer */
            RfMgmtWtpTmrStop (RFMGMT_11H_TPC_REQUEST_TMR);

        }

        MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            u4RadioIfIndex =
            pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
        u4RadioIfIndex =
            pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

        /* Create the DB entry */
        if (RfMgmtProcessDBMsg (u1MsgType, &RfMgmtDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtApRadioConfigRequest: RF MGMT DB "
                        "processing failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtApRadioConfigRequest: RF MGMT DB "
                          "processing failed"));
            return RFMGMT_FAILURE;
        }
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtApRadioConfigRequest: Invalid index, "
                    "return failure\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtApRadioConfigRequest: Invalid index, "
                      "return failure"));
        return RFMGMT_FAILURE;
    }

    if (u1OpCode == RFMGMT_CREATE_RADIO_ENTRY)
    {
        /* Start the neighbor msg expiry timer */
        RfMgmtWtpTmrStart (u4RadioIfIndex, RFMGMT_NEIGH_SCAN_TMR,
                           RFMGMT_AGEOUT_PERIOD);

        /* Start the channel scan timer */
        RfMgmtWtpTmrStart (u4RadioIfIndex, RFMGMT_CHAN_SCAN_TMR,
                           RFMGMT_AP_SCAN_PERIOD);

        /* Start the periodic message to AC timer */
        RfMgmtWtpTmrStart (u4RadioIfIndex, RFMGMT_TX_MESSAGE_EXP_TMR,
                           RFMGMT_TX_MSG_PERIOD);

        /* Start the client msg expiry timer */
        RfMgmtWtpTmrStart (u4RadioIfIndex, RFMGMT_CLIENT_SNR_SCAN_TMR,
                           RFMGMT_CLIENT_SCAN_PERIOD);

        /* Start the client msg expiry timer */
        RfMgmtWtpTmrStart (u4RadioIfIndex, RFMGMT_11H_TPC_REQUEST_TMR,
                           RFMGMT_TPC_REQUEST_PERIOD);
    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessConfigStatusRsp                               */
/*                                                                           */
/* Description  : This functions process the status response message         */
/*                from WTP                                                   */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessConfigStatusRsp (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessConfigStatusRsp : Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessConfigStatusRsp : Null input received"));
        return RFMGMT_FAILURE;
    }

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);
    switch (pWssMsgStruct->u1Opcode)
    {
        case VENDOR_NEIGH_CONFIG_TYPE:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigStatusRsp.
                                          NeighApTableConfig.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborMsgPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChannelScanDuration = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bAutoScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborAgingPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRssiThreshold = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2NeighborMsgPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                NeighApTableConfig.u2NeighborMsgPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2NeighborAgingPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                NeighApTableConfig.u2NeighborAgingPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2ChannelScanDuration =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                NeighApTableConfig.u2ChannelScanDuration;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1AutoScanStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                NeighApTableConfig.u1AutoScanStatus;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.i2RssiThreshold =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                NeighApTableConfig.i2RssiThreshold;

            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY,
                                    &RfMgmtDB) != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;

        case VENDOR_CLIENT_CONFIG_TYPE:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigStatusRsp.
                                          ClientTableConfig.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);

            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRThreshold = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u2SNRScanPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                ClientTableConfig.u2SNRScanPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u1SNRScanStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                ClientTableConfig.u1SNRScanStatus;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u1WlanId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                ClientTableConfig.u1WlanId;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.i2SNRThreshold =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                ClientTableConfig.i2SNRThreshold;

            if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY,
                                    &RfMgmtDB) != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusRsp : Getting Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusRsp : Getting Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
        case VENDOR_CH_SWITCH_STATUS_TYPE:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigStatusRsp.
                                          ChSwitchStatusTable.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChSwitchStatus = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1ChSwitchStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                ChSwitchStatusTable.u1ChSwitchStatus;
            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
        case VENDOR_SPECT_MGMT_TPC_TYPE:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigStatusRsp.
                                          TpcSpectMgmtTable.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hTpcRequestInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hTpcStatus = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                TpcSpectMgmtTable.u2TpcRequestInterval;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1RfMgmt11hTpcStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                TpcSpectMgmtTable.u111hTpcStatus;

            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
        case VENDOR_SPECTRUM_MGMT_DFS_TYPE:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigStatusRsp.
                                          DfsParamsTable.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsQuietInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsStatus = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u2DfsQuietInterval;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u2DfsQuietPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u2DfsMeasurementInterval;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u2DfsChannelSwitchStatus;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1RfMgmt11hDfsStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u111hDfsStatus;
            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigStatusRsp:Getting Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                (UINT4) (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                         DfsParamsTable.u1RadioId +
                         SYS_DEF_MAX_ENET_INTERFACES);
            RadioIfGetDB.RadioIfIsGetAllDB.bChanlSwitStatus = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.b11hDfsStatus = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u1ChanlSwitStatus =
                (UINT1) pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u2DfsChannelSwitchStatus;
            RadioIfGetDB.RadioIfGetAllDB.u111hDfsStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.
                DfsParamsTable.u111hDfsStatus;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          &RadioIfGetDB) != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq:Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq:Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
#ifdef ROGUEAP_WANTED
        case VENDOR_ROUGE_AP:
        {
            gu1RogueDetection =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                RogueMgmt.u1RfDetection;
            MEMCPY (gu1RfGroupName,
                    pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigStatusRsp.RougeTable.
                    au1RfGroupName, CAPWAP_RF_GROUP_NAME_SIZE);
        }
            break;
#endif
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalide msg recived\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Invalide msg recived"));
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return RFMGMT_FAILURE;
    }
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtSetQuietIEInfo                                       */
/*                                                                           */
/* Description  : This function will invoke the NPAPI to set Quiet IE Config */
/*                parameters.                                                */
/*                                                                           */
/* Input        : pRfMgmtAPConfigDB - Pointer to the input structure         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtSetQuietIEInfo (tRfMgmtAPConfigDB * pRfMgmtAPConfigDB)
{
#ifdef NPAPI_WANTED
    UINT1               i1RetVal = 0;
    tRfMgmtNpWrHwQuietIEInfo Entry;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1WlanId = WLAN_DEF_INTERFACE;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1RadioId = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (au1Wname, 0, WLAN_NAME_MAX_LEN);

    MEMSET (&Entry, 0, sizeof (tRfMgmtNpWrHwScanNeighInfo));

    Entry.RadInfo.au2WlanIfIndex[0] = u1WlanId;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtAPConfigDB->u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return RFMGMT_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u1AdminStatus ==
        RADIOIF_ADMIN_STATUS_DISABLED)
    {
        return RFMGMT_SUCCESS;
    }
    /*No need to set if Bssid count is 0 */
    if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
    {
        return RFMGMT_SUCCESS;
    }

    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) Entry.RadInfo.au1RadioIfName,
             "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME, u1WlanId - 1);

    STRCPY (Entry.RadInfo.au1WlanIfname[0], au1Wname);

    Entry.u1QuietPeriod =
        (UINT1) pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval;
    Entry.u2QuietDuration = pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod;

    i1RetVal = FsRfMgmtHwSetQuietIEInfo (&Entry);

    if (i1RetVal != FNP_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtSetQuietIEInfo : Set Quiet IE Info " "failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtSetQuietIEInfo : Set Quiet IE Info " "failed"));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
#else
    UNUSED_PARAM (pRfMgmtAPConfigDB);
#endif
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessConfigUpdateReq                               */
/*                                                                           */
/* Description  : This functions process the status response message         */
/*                from WTP                                                   */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessConfigUpdateReq (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1WlanId = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessConfigUpdateReq : Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessConfigUpdateReq : Null input received"));
        return RFMGMT_FAILURE;
    }

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);
    switch (pWssMsgStruct->u1Opcode)
    {
        case NEIGH_CONFIG_VENDOR_MSG:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigUpdateReq.
                                          NeighApTableConfig.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborMsgPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChannelScanDuration = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bAutoScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bNeighborAgingPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRssiThreshold = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2NeighborMsgPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                NeighApTableConfig.u2NeighborMsgPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2NeighborAgingPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                NeighApTableConfig.u2NeighborAgingPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2ChannelScanDuration =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                NeighApTableConfig.u2ChannelScanDuration;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1AutoScanStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                NeighApTableConfig.u1AutoScanStatus;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.i2RssiThreshold =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                NeighApTableConfig.i2RssiThreshold;

            if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1AutoScanStatus == RFMGMT_AUTO_SCAN_DISABLE)
            {
                RfMgmtDeleteNeighborDetails (RfMgmtDB.unRfMgmtDB.
                                             RfMgmtApConfigTable.
                                             RfMgmtAPConfigDB.u4RadioIfIndex,
                                             RFMGMT_FLUSH_DB);
            }
            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq:Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq:Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;

        case CLIENT_CONFIG_VENDOR_MSG:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4)
                (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                 ClientTableConfig.u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);

            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRThreshold = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bBssidScanStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bWlanId = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u2SNRScanPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                ClientTableConfig.u2SNRScanPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u1SNRScanStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                ClientTableConfig.u1SNRScanStatus;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.i2SNRThreshold =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                ClientTableConfig.i2SNRThreshold;
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u1WlanId =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                ClientTableConfig.u1WlanId;
            u1WlanId = (UINT1) (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                                RfMgmtClientConfigDB.u1WlanId - 1);
            RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u1BssidScanStatus[u1WlanId] =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                ClientTableConfig.u1BssidScanStatus[u1WlanId];

            if (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u1SNRScanStatus == RFMGMT_SNR_SCAN_DISABLE)
            {
                RfMgmtDeleteClientDetails (RfMgmtDB.unRfMgmtDB.
                                           RfMgmtClientConfigTable.
                                           RfMgmtClientConfigDB.u4RadioIfIndex);
            }
            if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY,
                                    &RfMgmtDB) != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq : Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq : Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
        case CH_SWITCH_STATUS_VENDOR_MSG:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigUpdateReq.
                                          ChSwitchStatusTable.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bChSwitchStatus = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1ChSwitchStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                ChSwitchStatusTable.u1ChSwitchStatus;
            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq:Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq:Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
        case VENDOR_SPECT_MGMT_TPC_MSG:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigUpdateReq.
                                          TpcSpectMgmtTable.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hTpcRequestInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hTpcStatus = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                TpcSpectMgmtTable.u2TpcRequestInterval;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1RfMgmt11hTpcStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                TpcSpectMgmtTable.u111hTpcStatus;

            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq:Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq:Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;

        case VENDOR_SPECTRUM_MGMT_DFS_MSG:
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                u4RadioIfIndex = (UINT4) (pWssMsgStruct->unRfMgmtMsg.
                                          RfMgmtConfigUpdateReq.
                                          DfsParamsTable.u1RadioId +
                                          SYS_DEF_MAX_ENET_INTERFACES);

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsQuietInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
                bRfMgmt11hDfsStatus = OSIX_TRUE;

            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u2DfsQuietInterval;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u2DfsQuietPeriod;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u2DfsMeasurementInterval;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u2DfsChannelSwitchStatus;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u1RfMgmt11hDfsStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u111hDfsStatus;

            if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
                != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq:Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq:Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
            RfMgmtSetQuietIEInfo (&
                                  (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                                   RfMgmtAPConfigDB));
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                (UINT4) (pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                         DfsParamsTable.u1RadioId +
                         SYS_DEF_MAX_ENET_INTERFACES);
            RadioIfGetDB.RadioIfIsGetAllDB.bChanlSwitStatus = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.b11hDfsStatus = OSIX_TRUE;
            RadioIfGetDB.RadioIfGetAllDB.u1ChanlSwitStatus =
                (UINT1) pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u2DfsChannelSwitchStatus;
            RadioIfGetDB.RadioIfGetAllDB.u111hDfsStatus =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                DfsParamsTable.u111hDfsStatus;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          &RadioIfGetDB) != RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessConfigUpdateReq:Getting "
                            "Rfmgmt index failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessConfigUpdateReq:Getting "
                              "Rfmgmt index failed"));
                CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
                return RFMGMT_FAILURE;
            }
        }
            break;
#ifdef ROGUEAP_WANTED
        case ROGUE_CONFIG_VENDOR_MSG:
        {

            gu1RogueDetection =
                pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.
                RogueMgmt.u1RfDetection;
            MEMCPY (gu1RfGroupName,
                    pWssMsgStruct->unRfMgmtMsg.RfMgmtConfigUpdateReq.RogueMgmt.
                    u1RfGroupName, CAPWAP_RF_GROUP_NAME_SIZE);
        }
            break;
#endif
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalide msg recived\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Invalide msg recived"));
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return RFMGMT_FAILURE;
    }
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessChannelUpdateReq                              */
/*                                                                           */
/* Description  : This functions update the channel number received from     */
/*                radio module                                               */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessChannelUpdateReq (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfDB;
    INT1                idx;
    UINT4               configreqchnlnum = 0;
    UINT1               u111hDfsStatus = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessChannelUpdateReq: Null input received\r\n");
        return RFMGMT_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bCurrentChannel = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2CurrentChannel =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u2Channel;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessChannelUpdateReq: Getting Rfmgmt index failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessChannelUpdateReq: Getting Rfmgmt index failed"));
        return RFMGMT_FAILURE;
    }

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bDFSChannelStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessChannelUpdateReq: Rfmgmt DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessChannelUpdateReq: Rfmgmt DB access failed"));
        return RFMGMT_FAILURE;
    }
    RadioIfDB.RadioIfIsGetAllDB.bCapability = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.b11hDfsStatus = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessChannelUpdateReq: Radio DB access failed\r\n");
        return RFMGMT_FAILURE;
    }
    u111hDfsStatus = RadioIfDB.RadioIfGetAllDB.u111hDfsStatus;

    if ((u111hDfsStatus == 1) &&
        ((RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA) ||
         (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN) ||
         (RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC)))
    {
        configreqchnlnum = pWssMsgStruct->unRfMgmtMsg.
            RfMgmtIntfConfigReq.u2Channel;

        for (idx = 0; idx < RADIO_MAX_DFS_CHANNEL; idx++)
        {
            if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.DFSChannelStatus[idx].u4ChannelNum ==
                configreqchnlnum)
            {
                break;
            }
        }
        if (idx < RADIO_MAX_DFS_CHANNEL)
        {
            if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                DFSChannelStatus[idx].u4DFSFlag & RADIO_CHAN_RADAR)
            {
                if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelStatus[idx].u4DFSFlag & RADIO_CHAN_DFS_AVAILABLE)
                {
                    return RFMGMT_SUCCESS;
                }

                RfMgmtWtpTmrStart (pWssMsgStruct->unRfMgmtMsg.
                                   RfMgmtIntfConfigReq.u4RadioIfIndex,
                                   RFMGMT_CAC_TMR, RFMGMT_CAC_AGEOUT_PERIOD);
            }
        }
    }

    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessTxPowerUpdateReq                              */
/*                                                                           */
/* Description  : This functions update the Tx Power level received from     */
/*                radio module                                               */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to the input structure             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtProcessTxPowerUpdateReq (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Input Parameter Check */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTxPowerUpdateReq: Null input received\r\n");
        return RFMGMT_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2TxPowerLevel =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u2TxPower;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex =
        pWssMsgStruct->unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex;
    if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessTxPowerUpdateReq: Getting Rfmgmt index failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessTxPowerUpdateReq: Getting Rfmgmt index failed"));
        return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteClientStats                                    */
/*                                                                           */
/* Description  : This function deletes the Client related information when  */
/*                station related information                                */
/*                                                                           */
/* Input        : pWssMsgStruct - Pointer to input structure                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtDeleteClientStats (tRfMgmtMsgStruct * pWssMsgStruct)
{
    tRfMgmtDB           RfMgmtUpdateDB;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));

    /* Check for invalid input */
    if (pWssMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientStats: Null input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtDeleteClientStats: Null input received"));
        return RFMGMT_FAILURE;
    }

    RFMGMT_TRC1 (RFMGMT_INFO_TRC, "Delete Station entry for Radio %d\r\n",
                 pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId);

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
        RfMgmtClientScanDB.u4RadioIfIndex =
        (UINT4) pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.
        u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;

    MEMCPY (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtClientScanTable.
            RfMgmtClientScanDB.ClientMacAddress,
            pWssMsgStruct->unRfMgmtMsg.RfMgmtStaProcessReq.ClientMac,
            sizeof (tMacAddr));

    if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_CLIENT_SCAN_ENTRY,
                            &RfMgmtUpdateDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientStats: Client " "deletion failed\n");
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateNeighborEntryStatus                            */
/*                                                                           */
/* Description  : This function updates the entry status of every entries in */
/*                neighbor DB once the neighbor message is constructed.      */
/*                                                                           */
/* Input        : pRfMgmtNeighborScanDB                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
VOID
RfMgmtUpdateNeighborEntryStatus (tRfMgmtNeighborScanDB * pRfMgmtNeighborScanDB)
{
    tRfMgmtDB           RfMgmtUpdateDB;
    UINT1               u1OpCode = 0;

    RFMGMT_FN_ENTRY ();

    MEMSET (&RfMgmtUpdateDB, 0, sizeof (tRfMgmtDB));

    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u4RadioIfIndex =
        pRfMgmtNeighborScanDB->u4RadioIfIndex;
    RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u2ScannedChannel =
        pRfMgmtNeighborScanDB->u2ScannedChannel;

    MEMCPY (RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.NeighborAPMac,
            pRfMgmtNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

    /* If the entry is in add status change to no change 
     * status. Else if the entry is in delete status. 
     * Then remove the entry from the DB */
    if (pRfMgmtNeighborScanDB->u1EntryStatus == RFMGMT_NEIGH_ENTRY_ADD)
    {
        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.u1EntryStatus = RFMGMT_NEIGH_ENTRY_NO_CHG;

        u1OpCode = RFMGMT_SET_NEIGHBOR_SCAN_ENTRY;

        RfMgmtUpdateDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanIsSetDB.bEntryStatus = OSIX_TRUE;
    }
    if (pRfMgmtNeighborScanDB->u1EntryStatus == RFMGMT_NEIGH_ENTRY_DELETE)
    {
        u1OpCode = RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY;
    }
    if (u1OpCode != 0)
    {
        if (RfMgmtProcessDBMsg (u1OpCode, &RfMgmtUpdateDB) == RFMGMT_FAILURE)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtUpdateNeighborEntryStatus: DB updation "
                        "failed\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtUpdateNeighborEntryStatus: DB updation "
                          "failed"));
        }
    }
    RFMGMT_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : RfMgmtPerformCAC                                           */
/*                                                                           */
/* Description  : This function will invoke the NPAPI to start CAC           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
VOID
RfMgmtPerformCAC (tRfMgmtAPConfigDB * pRfMgmtAPConfigDB)
{
#ifdef NPAPI_WANTED
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfGetDB;
    tRfMgmtCacInfo      Entry;
    UINT1               i1RetVal = 0;
    UINT1               u1RadioId = 0;
    UINT1               au1Wname[WLAN_NAME_MAX_LEN];
    UINT1               u1WlanId = WLAN_DEF_INTERFACE;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pRfMgmtAPConfigDB->u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bOFDMChannelWidth = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentFrequency = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bHTCapInfo = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtPerformCAC: RadioDB access failed\r\n");
        return;
    }

    Entry.u1CurrentChannel = RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
    Entry.u1Bandwidth = (UINT1) RadioIfGetDB.RadioIfGetAllDB.u4OFDMChannelWidth;
    Entry.u2HTEnabled = RadioIfGetDB.RadioIfGetAllDB.u2HTCapInfo;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy1 = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtPerformCAC: RadioDB access failed\r\n");
        return;

    }
    if (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo)
    {

        Entry.u1Bandwidth =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth;
        Entry.u1CentreFreq0 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0;
        Entry.u1CentreFreq1 =
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1;

    }
    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

    SPRINTF ((CHR1 *) Entry.RadInfo.au1RadioIfName,
             "%s%d", RADIO_INTF_NAME, u1RadioId - 1);

    Entry.RadInfo.au2WlanIfIndex[0] = u1WlanId;

    SPRINTF ((CHR1 *) au1Wname, "%s%d", RADIO_ATH_INTF_NAME, u1WlanId - 1);

    STRCPY (Entry.RadInfo.au1WlanIfname[0], au1Wname);

    i1RetVal = FsRfMgmtStartCac (&Entry);
    if (i1RetVal != FNP_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtPerformCAC: Start hardware scan " "failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtPerformCAC: Start hardware scan " "failed"));
        return;
    }
#else
    UNUSED_PARAM (pRfMgmtAPConfigDB);
#endif
    return;
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateDFSChannelDetails                              */
/*                                                                           */
/* Description  : This function will get invoked whenever a Client info is   */
/*                scanned. The DB should be created/updated for all the      */
/*                entries received                                           */
/*                                                                           */
/* Input        : pRfMgmtQueueReq - Pointer to input structure               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
UINT1
RfMgmtUpdateDFSChannelDetails (tRfMgmtQueueReq * pRfMgmtQueueReq)
{
    tRfMgmtDB           RfMgmtDB;
    tRfMgmtDfsChannelInfo dfschannelinfo[RADIO_MAX_DFS_CHANNEL];
    UINT4               u4Index;
    UINT4               u4count = 0, u4Idx1 = 0;
    MEMSET (&dfschannelinfo, 0,
            (sizeof (tRfMgmtDfsChannelInfo) * RADIO_MAX_DFS_CHANNEL));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    /* Validate for invalid input */
    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Invalid input received"));
        return RFMGMT_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex
        = pRfMgmtQueueReq->u4RadioIfIndex;

    for (u4Index = 0;
         u4Index < RFMGMT_MAX_CHANNELA && u4Idx1 < RADIO_MAX_DFS_CHANNEL;
         u4Index++)
    {
        if (gau1Dot11DFSChannelList[u4Idx1] ==
            pRfMgmtQueueReq->DFSChannelInfo[u4Index].u4ChannelNum)
        {
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                DFSChannelStatus[u4count].u4ChannelNum =
                pRfMgmtQueueReq->DFSChannelInfo[u4Index].u4ChannelNum;
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                DFSChannelStatus[u4count].u4DFSFlag =
                pRfMgmtQueueReq->DFSChannelInfo[u4Index].u4DFSFlag;

            u4count++;
            u4Idx1++;
        }
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDFSChannelStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY,
                            &RfMgmtDB) == RFMGMT_SUCCESS)
    {
        return RFMGMT_SUCCESS;
    }
    else
        return RFMGMT_FAILURE;
}

/*****************************************************************************/
/* Function     : RfMgmtEnableChannel                                        */
/*                                                                           */
/* Description  : This function post CAC enables the channel                 */
/*                                                                           */
/* Input        : RadioIfIndex,Channel                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RfMgmtEnableChannel (UINT4 u4RadioIfIndex, UINT1 u1Channel)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    RadioIfMsgStruct.unRadioIfMsg.RadioIfDfsChnlUpdate.u4IfIndex =
        u4RadioIfIndex;
    RadioIfMsgStruct.unRadioIfMsg.RadioIfDfsChnlUpdate.u1CurrentChannel =
        u1Channel;

    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_DFS_CHANNEL_UPDATE,
                                &RadioIfMsgStruct) != OSIX_SUCCESS)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtEnableChannel: Channel change to the Radio DB "
                     "failed %d\n", RadioIfMsgStruct.unRadioIfMsg.
                     RadioIfDfsChnlUpdate.u4IfIndex);
    }

}

/*****************************************************************************/
/* Function     : RfMgmtUpdateShadowTableForRadarStatus                      */
/*                                                                           */
/* Description  : This function updates the shadow table on detecting        */
/*                radar                                                      */
/* Input        : RadioIfIndex                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RfMgmtUpdateShadowTableForRadarStatus (UINT4 u4RadioIfIndex)
{
    tRadioIfMsgStruct   RadioIfMsgStruct;
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    RadioIfMsgStruct.unRadioIfMsg.RadioIfRadarStatUpdate.u4IfIndex =
        u4RadioIfIndex;
    RadioIfMsgStruct.unRadioIfMsg.RadioIfRadarStatUpdate.u1RadarStatus =
        OSIX_TRUE;

    if (WssIfProcessRadioIfMsg (WSS_RADIOIF_DFS_RADAR_STAT_UPDATE,
                                &RadioIfMsgStruct) != OSIX_SUCCESS)
    {
        RFMGMT_TRC1 (RFMGMT_FAILURE_TRC,
                     "RfMgmtUpdateShadowTableForRadarStatus: Channel change to the Radio DB "
                     "failed %d\n", RadioIfMsgStruct.unRadioIfMsg.
                     RadioIfRadarStatUpdate.u4IfIndex);
    }

}

/*****************************************************************************/
/* Function     : RfMgmtTriggerRadarEvent                                    */
/*                                                                           */
/* Description  : This function will trigger Radar event to WLC              */
/*                                                                           */
/* Input        : RadioIfIndex, RadioID, CurrentChannel                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RfMgmtTriggerRadarEvent (UINT4 u4RadioIfIndex, UINT1 u1RadioID,
                         UINT1 u1CurrentChannel)
{
    unApHdlrMsgStruct   ApHdlrMsg;
    UNUSED_PARAM (u4RadioIfIndex);
    MEMSET (&ApHdlrMsg, 0, sizeof (unApHdlrMsgStruct));
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.isOptional = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
    ApHdlrMsg.PmWtpEventReq.vendSpec.elementId = VENDOR_DFS_RADAR_STATS;

    ApHdlrMsg.PmWtpEventReq.vendSpec.isOptional = OSIX_TRUE;

    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.u2ChannelNum = u1CurrentChannel;
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.u1RadioId = u1RadioID;
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.isRadarFound = OSIX_TRUE;
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.u4VendorId = VENDOR_ID;
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.u2MsgEleType = VENDOR_DFS_RADAR_STATUS_MSG;
    ApHdlrMsg.PmWtpEventReq.vendSpec.unVendorSpec.
        DFSRadarEventInfo.u2MsgEleLen = 4;

    if (WssIfProcessApHdlrMsg (WSS_APHDLR_PM_WTP_EVENT_REQ,
                               &ApHdlrMsg) == OSIX_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "Failed to send the packet " "to the Ap Hdlr Module \r\n");
    }
}

/*****************************************************************************/
/* Function     : RfMgmtHandleRadarEvent                                     */
/*                                                                           */
/* Description  : This function will handle te radar event                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RfMgmtHandleRadarEvent (tRfMgmtQueueReq * pRfMgmtQueueReq)
{
    unWssMsgStructs     WssMsgStruct;
    tRadioIfGetDB       RadioIfDB;
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    /* Validate for invalid input */
    if (pRfMgmtQueueReq == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid input received\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "Invalid input received"));
        return;
    }
    RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = pRfMgmtQueueReq->u4RadioIfIndex;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        /* No entry found for this BSSID. which implies that the ap
           may be manged by different WLC. However we require this
           entry just for the neighbor AP information. */
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Get Radio If DB Failed\r\n");
        return;
    }
    WssMsgStruct.WssStaStateDB.u1RadioId = RadioIfDB.RadioIfGetAllDB.u1RadioId;
    RfMgmtUpdateChannelStatus (pRfMgmtQueueReq->u4RadioIfIndex,
                               pRfMgmtQueueReq->u4EventType);

    switch (pRfMgmtQueueReq->u4EventType)
    {
        case RADAR_DETECTED:
        case RADAR_CAC_ABORTED:
            RadioIfDB.RadioIfIsGetAllDB.bRadarFound = OSIX_TRUE;
            RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                pRfMgmtQueueReq->u4RadioIfIndex;
            RadioIfDB.RadioIfGetAllDB.u1RadarFound = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          &RadioIfDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Set Radio If DB Failed\r\n");
                return;
            }
            RfMgmtUpdateShadowTableForRadarStatus (pRfMgmtQueueReq->
                                                   u4RadioIfIndex);
            RfMgmtTriggerRadarEvent (pRfMgmtQueueReq->u4RadioIfIndex,
                                     RadioIfDB.RadioIfGetAllDB.u1RadioId,
                                     RadioIfDB.RadioIfGetAllDB.
                                     u1CurrentChannel);
            if (WssStaWtpProcessWssIfMsg
                (WSS_CONSTRUCT_MEAS_REQUEST, &WssMsgStruct))
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Failed to trigger Measurement request\r\n");
            }
            break;
        case RADAR_CAC_FINISHED:
            RfMgmtEnableChannel (pRfMgmtQueueReq->u4RadioIfIndex,
                                 RadioIfDB.RadioIfGetAllDB.u1CurrentChannel);
            break;
        default:
            break;

    }
}

/*****************************************************************************/
/* Function     : RfMgmtUpdateChannelStatus                                  */
/*                                                                           */
/* Description  : This function updates the channel status                  */
/*                                                                           */
/* Input        : RadioIfIndex and EventType                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RfMgmtUpdateChannelStatus (UINT4 u4RadioIfIndex, UINT4 u4EventType)
{
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1Idx = 0;
    UINT4               u4CurrentChannel = 0;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bCurrentChannel = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bDFSChannelStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex = u4RadioIfIndex;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtUpdateChannelStatus: Getting Rfmgmt index failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtUpdateChannelStatus: Getting Rfmgmt index failed"));
        return;
    }

    u4CurrentChannel =
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2CurrentChannel;
    for (u1Idx = 0; u1Idx < RADIO_MAX_DFS_CHANNEL; u1Idx++)
    {
        if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            DFSChannelStatus[u1Idx].u4ChannelNum == u4CurrentChannel)
        {
            break;
        }
    }
    switch (u4EventType)
    {
        case RADAR_DETECTED:
        case RADAR_CAC_ABORTED:
        {
            if (u1Idx < RADIO_MAX_DFS_CHANNEL)
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelStatus[u1Idx].u4DFSFlag =
                    RADIO_CHAN_DFS_UNAVAILABLE;
        }
            break;
        case RADAR_CAC_FINISHED:
        {
            if (u1Idx < RADIO_MAX_DFS_CHANNEL)
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                    DFSChannelStatus[u1Idx].u4DFSFlag =
                    RADIO_CHAN_DFS_AVAILABLE;

        }
            break;
        default:
            break;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigIsSetDB.
        bDFSChannelStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex = u4RadioIfIndex;
    if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "setting AP Config entry failed\r\n");
    }

}

#endif
