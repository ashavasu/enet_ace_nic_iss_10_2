/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmcli.c,v 1.5 2017/12/08 10:16:25 siva Exp $
 *
 * Description: This file contains the routines for rfmgmt module 
 *         related cli commands. 
 *                            
 ********************************************************************/
#ifndef __RFMGMTCLI_C__
#define __RFMGMTCLI_C__

#include "rfminc.h"
#include "wsscfginc.h"
#include "rfmcli.h"
#include "capwapcli.h"
#include "wsscfgcli.h"
#ifdef ROGUEAP_WANTED
INT1                rogueGetFsApClassifiedBy (tMacAddr, UINT1 *);
#endif
/****************************************************************************
 * Function    :  cli_process_rfmgmt_cmd
 * Description :  This function is exported to CLI module to handle the
                  rfmgmt cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
cli_process_rfmgmt_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RFMGMT_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Inst = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1RadioId = 0;
    tMacAddr            BaseMacAddr;
    MEMSET (BaseMacAddr, 0, sizeof (tMacAddr));

    CliRegisterLock (CliHandle, RfMgmtLock, RfMgmtUnLock);
    RfMgmtLock ();

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store RFMGMT_CLI_MAX_ARGS arguements at the max. 
     */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == RFMGMT_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_RFMGMT_CREATE_RADIO:
            i4RetStatus = RfMgmtRadioCreate (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_DELETE_RADIO:
            i4RetStatus = RfMgmtRadioDelete (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_TPC_SELECTION:
            i4RetStatus = RfMgmtCliTpcMode (CliHandle, (INT4) *args[0],
                                            *args[1]);
            if ((*args[1] == CLI_TPC_MODE_GLOBAL))
            {
                i4RetStatus = RfMgmtCliTpcSelectionMode (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[2]);
                if (i4RetStatus == CLI_FAILURE)
                {
                    break;
                }
            }
            if (args[6] == NULL)
            {
                u1RadioId = 0;
            }
            else
            {
                u1RadioId = (UINT1) *args[6];
            }

            if (args[3] != NULL)
            {
                i4RetStatus = RfMgmtCliTpcPerAPMode (CliHandle, (INT4) *args[0],
                                                     (UINT1 *) args[3],
                                                     *args[4], args[5],
                                                     u1RadioId);
                if (i4RetStatus == CLI_FAILURE)
                {
                    break;
                }
            }

            break;
        case CLI_RFMGMT_DCA_SELECTION:
            i4RetStatus = RfMgmtCliDcaMode (CliHandle, (INT4) *args[0],
                                            *args[1]);
            if ((*args[1] == CLI_DCA_MODE_GLOBAL))
            {
                i4RetStatus =
                    RfMgmtCliDcaSelectionMode (CliHandle, (INT4) *args[0],
                                               *args[2]);
                if (i4RetStatus == CLI_FAILURE)
                {
                    break;
                }
            }
            if (args[6] == NULL)
            {
                u1RadioId = 0;
            }
            else
            {
                u1RadioId = (UINT1) *args[6];
            }
            if (args[3] != NULL)
            {
                i4RetStatus = RfMgmtCliDcaPerAPMode (CliHandle, (INT4) *args[0],
                                                     (UINT1 *) args[3],
                                                     *args[4], args[5],
                                                     u1RadioId);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    break;
                }
            }
            break;

        case CLI_RFMGMT_DCA_SENSITIVITY:
            i4RetStatus = RfMgmtCliDcaSensitivity (CliHandle, (INT4) *args[0],
                                                   (UINT4) *args[1]);
            break;
        case CLI_RFMGMT_DCA_INTERVAL:
            i4RetStatus = RfMgmtCliDcaInterval (CliHandle, (INT4) *args[0],
                                                *args[1]);
            break;
        case CLI_RFMGMT_TPC_INTERVAL:
            i4RetStatus = RfMgmtCliTpcInterval (CliHandle, (INT4) *args[0],
                                                *args[1]);
            break;
        case CLI_RFMGMT_SHA_INTERVAL:
            i4RetStatus = RfMgmtCliShaInterval (CliHandle, (INT4) *args[0],
                                                *args[1]);
            break;
        case CLI_RFMGMT_SHA_STATUS:
            i4RetStatus = RfMgmtCliShaStatus (CliHandle, (INT4) *args[0],
                                              *args[1]);
            break;
        case CLI_RFMGMT_11H_TPC_STATUS:
            i4RetStatus = RfMgmtCli11hTpcStatus (CliHandle, (INT4) *args[0],
                                                 *args[1]);
            break;
        case CLI_RFMGMT_11H_TPC_INTERVAL:
            i4RetStatus = RfMgmtCli11hTpcInterval (CliHandle, (INT4) *args[0],
                                                   *args[1]);
            break;
        case CLI_RFMGMT_11H_TPC_REQUEST_INTERVAL:
            i4RetStatus =
                RfMgmtCli11hTpcRequestInterval (CliHandle, (INT4) *args[0],
                                                *args[1], (UINT1 *) args[2],
                                                args[3]);
            break;
        case CLI_RFMGMT_11H_TPC_MIN_LINK_THRESHOLD:
            i4RetStatus =
                RfMgmtCli11hMinLinkThreshold (CliHandle, (INT4) *args[0],
                                              *args[1]);
            break;
        case CLI_RFMGMT_11H_TPC_MAX_LINK_THRESHOLD:
            i4RetStatus =
                RfMgmtCli11hMaxLinkThreshold (CliHandle, (INT4) *args[0],
                                              *args[1]);
            break;
        case CLI_RFMGMT_11H_TPC_STA_COUNT_THRESHOLD:
            i4RetStatus =
                RfMgmtCli11hStaCountThreshold (CliHandle, (INT4) *args[0],
                                               *args[1]);
            break;
        case CLI_RFMGMT_SHOW_11H_TPC_INFO:
            i4RetStatus = RfMgmtShow11hTpcInfo (CliHandle,
                                                (UINT1 *) args[0], *args[1]);
            break;
        case CLI_RFMGMT_11H_DFS_STATUS:
            i4RetStatus = RfMgmtCli11hDfsStatus (CliHandle, (INT4) *args[0],
                                                 *args[1]);
            break;
        case CLI_RFMGMT_SHOW_11H_DFS_INFO:
            i4RetStatus = RfMgmtShow11hDfsInfo (CliHandle,
                                                (UINT1 *) args[0], *args[1]);
            break;
        case CLI_RFMGMT_11H_DFS_INTERVAL:
            i4RetStatus = RfMgmtCli11hDfsInterval (CliHandle, (INT4) *args[0],
                                                   *args[1]);
            break;
        case CLI_RFMGMT_11H_DFS_QUIET_INTERVAL:
            i4RetStatus =
                RfMgmtCli11hDfsQuietInterval (CliHandle, (INT4) *args[0],
                                              *args[1], (UINT1 *) args[2],
                                              args[3]);
            break;
        case CLI_RFMGMT_11H_DFS_QUIET_PERIOD:
            i4RetStatus =
                RfMgmtCli11hDfsQuietPeriod (CliHandle, (INT4) *args[0],
                                            *args[1], (UINT1 *) args[2],
                                            args[3]);
            break;
        case CLI_RFMGMT_11H_DFS_MEASUREMENT_INTERVAL:
            i4RetStatus =
                RfMgmtCli11hDfsMeasurementInterval (CliHandle, (INT4) *args[0],
                                                    *args[1], (UINT1 *) args[2],
                                                    args[3]);
            break;
        case CLI_RFMGMT_DFS_CHANNEL_SWITCH_STATUS:
            i4RetStatus =
                RfMgmtCli11hDfsChannelSwitchStatus (CliHandle, (INT4) *args[0],
                                                    *args[1], (UINT1 *) args[2],
                                                    args[3]);
            break;
        case CLI_RFMGMT_SHOW_FAILED_AP_NEIGBHOR:
            i4RetStatus = RfMgmtShowFailedAPNeighbors (CliHandle,
                                                       (INT4) *args[0],
                                                       (UINT1 *) args[1]);
            break;
        case CLI_RFMGMT_DCA_CHANNEL_UPDATE:
            i4RetStatus =
                RfMgmtCliDcaChannelUpdate (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_TPC_UPDATE:
            i4RetStatus = RfMgmtCliTpcUpdate (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_DCA_CLIENT_THRESHOLD:
            i4RetStatus = RfMgmtCliClientThreshold (CliHandle, (INT4) *args[0],
                                                    *args[1]);
            break;
        case CLI_RFMGMT_BSSID_SNR_SCAN:
            i4RetStatus = RfMgmtCliBssidSNRScanStatus (CliHandle,
                                                       (INT4) *args[0],
                                                       *args[1],
                                                       (UINT1 *) args[2],
                                                       *args[3], *args[4]);
            break;
        case CLI_RFMGMT_DEBUG:
            i4RetStatus = RfMgmtCliEnableDebug (CliHandle, (*(INT4 *) args[0]));
            break;
        case CLI_RFMGMT_NO_DEBUG:
            i4RetStatus = RfMgmtCliDisableDebug (CliHandle,
                                                 (*(INT4 *) args[0]));
            break;

        case CLI_RFMGMT_TPC_SNR_THRESHOLD:
            i4RetStatus = RfMgmtCliTpcSNRThreshold (CliHandle,
                                                    (INT4) (*args[0]),
                                                    (INT4) *args[1]);
            break;
        case CLI_RFMGMT_DCA_RSSI_THRESHOLD:
            i4RetStatus = RfMgmtCliRssiThreshold (CliHandle,
                                                  (INT4) (*args[0]),
                                                  (INT4) (*args[1]));

            break;
        case CLI_RFMGMT_POWER_THRESHOLD:
            i4RetStatus = RfMgmtCliPowerThreshold (CliHandle,
                                                   (INT4) (*args[0]),
                                                   (INT4) (*args[1]));

            break;
        case CLI_RFMGMT_CHANNEL_SWITCH_STATUS:
            i4RetStatus = RfMgmtCliChannelSwitchStatus (CliHandle, *args[0]);
            break;
        case CLI_RFMGMT_DCA_CHANNEL_STATUS:
            i4RetStatus = RfMgmtCliDcaChannelStatus (CliHandle,
                                                     (INT4) *args[0], *args[1],
                                                     *args[2]);
            break;
        case CLI_RFMGMT_SHOW_AP_AUTORF:
            i4RetStatus = RfMgmtShowAPAutoRf (CliHandle, (INT4) *args[0],
                                              (UINT1 *) args[1], *args[2]);
            break;
        case CLI_RFMGMT_SHOW_AP_ALLOWEDCHANNEL:
            i4RetStatus =
                RfMgmtShowAPAllowedChannel (CliHandle, (INT4) *args[0],
                                            (UINT1 *) args[1], *args[2]);
            break;
        case CLI_RFMGMT_CLIENT_SNR_SCAN_STATUS:
            i4RetStatus = RfMgmtCliSNRScanStatus (CliHandle, (INT4) *args[0],
                                                  *args[1], (UINT1 *) args[2],
                                                  args[3]);
            break;
        case CLI_RFMGMT_CLIENT_SNR_SCAN_PERIOD:
            i4RetStatus = RfMgmtCliSNRScanPeriod (CliHandle, (INT4) *args[0],
                                                  *args[1], (UINT1 *) args[2],
                                                  args[3]);
            break;
        case CLI_RFMGMT_DCA_AUTO_SCAN_STATUS:
            i4RetStatus = RfMgmtCliAPAutoScanStatus (CliHandle, (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);
            break;
        case CLI_RFMGMT_DCA_CHANNEL_SCAN_DURATION:
            i4RetStatus = RfMgmtCliDcaChannelScanDuration (CliHandle,
                                                           (INT4) *args[0],
                                                           *args[1],
                                                           (UINT1 *) args[2],
                                                           args[3]);
            break;

        case CLI_RFMGMT_DCA_SCAN_FREQUENCY:
            i4RetStatus = RfMgmtCliDcaScanFrequency (CliHandle, (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);

            break;
        case CLI_RFMGMT_DCA_CHANNEL_NEIGHBOR_AGING:
            i4RetStatus =
                RfMgmtCliNeighborAgingPeriod (CliHandle, (INT4) *args[0],
                                              *args[1], (UINT1 *) args[2],
                                              args[3]);
            break;
        case CLI_RFMGMT_TX_POWER_TRAP_STATUS:

            i4RetStatus = RfMgmtCliTxPowerTrapStatus (CliHandle, *args[0]);
            break;
        case CLI_RFMGMT_CHANNEL_TRAP_STATUS:
            i4RetStatus = RfMgmtCliChannelChangeTrapStatus (CliHandle,
                                                            *args[0]);
            break;
        case CLI_RFMGMT_SHOW_CHANNEL:
            i4RetStatus =
                RfMgmtShowAdvancedChannel (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_SHOW_SHA:
            i4RetStatus = RfMgmtShowAdvancedSha (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_SHOW_COVERAGE:
            i4RetStatus =
                RfMgmtShowAdvancedCoverage (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_SHOW_TXPOWER:
            i4RetStatus =
                RfMgmtShowAdvancedTxPower (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_SHOW_TPC_STATS:
            i4RetStatus = RfMgmtCliTpcStats (CliHandle, *(INT4 *) args[0],
                                             (UINT1 *) args[1], args[2]);
            break;
        case CLI_RFMGMT_SHOW_NEIGHBOR_AP_CONFIG:
            i4RetStatus = RfMgmtCliNeighborAPConfig (CliHandle, (INT4) *args[0],
                                                     (UINT1 *) args[1],
                                                     args[2]);
            break;
        case CLI_RFMGMT_SHOW_CHANNEL_SWITCH_STATUS:
            i4RetStatus = RfMgmtCliShowChannelSwitchStatus (CliHandle);
            break;
        case CLI_RFMGMT_SHOW_SSID_SCAN_STATUS:
            i4RetStatus = RfMgmtCliShowBssidSNRScanStatus (CliHandle,
                                                           (INT4) *args[0],
                                                           (UINT1 *) args[1],
                                                           *args[2], *args[3]);
            break;
        case CLI_RFMGMT_SHOW_TX_POWER_TRAP_STATUS:
            i4RetStatus = RfMgmtCliShowTxPowerTrapStatus (CliHandle);
            break;
        case CLI_RFMGMT_SHOW_CHANNEL_TRAP_STATUS:
            i4RetStatus = RfMgmtCliShowChannelTrapStatus (CliHandle);
            break;
        case CLI_RFMGMT_EXTERNAL_APS_STATUS:
            i4RetStatus = RfMgmtCliExternalAPStatus (CliHandle, (INT4) *args[0],
                                                     (UINT4) *args[1]);
            break;
        case CLI_RFMGMT_CLEAR_NEIGHBOR_INFO:
            i4RetStatus =
                RfMgmtCliClearNeighborDetails (CliHandle, (INT4) *args[0]);
            break;
        case CLI_RFMGMT_NEIGHBOR_COUNT_THRESHOLD:
            i4RetStatus =
                RfMgmtCliNeighborCountThreshold (CliHandle, (INT4) *args[0],
                                                 *args[1]);
            break;

#ifdef ROGUEAP_WANTED
        case CLI_ROGUE_AP_CLASSIFY:
            if (NULL != args[2])
            {
                StrToMac ((UINT1 *) args[2], BaseMacAddr);
            }
            i4RetStatus = RogueClassifyAP (CliHandle,
                                           *args[0], *args[1], BaseMacAddr);
            break;
        case CLI_ROGUE_AP_DELETE_CLASS:
            i4RetStatus = RogueDeleteClassAP (CliHandle, *args[0], *args[1]);
            break;
        case CLI_ROGUE_AP_ADD_DELETE:
            if (NULL != args[3])
            {
                StrToMac ((UINT1 *) args[3], BaseMacAddr);
            }
            i4RetStatus = RogueAddDeleteAP (CliHandle,
                                            *args[0], *args[1], *args[2],
                                            BaseMacAddr);
            break;
        case CLI_ROGUE_RULE_ADD_AP:
            if (args[1] != NULL)
            {
                if ((UINT1) *args[2] > gu1RogueRuleCount + 1)
                {
                    CliPrintf (CliHandle, "\r\nPriority should be"
                               "less than of %d\r\n", (gu1RogueRuleCount + 2));
                    break;
                }
                else
                {
                    if (checkRuleNameExist ((UINT1 *) args[3]) ==
                        RFMGMT_SUCCESS)
                    {
                        RfMgmtArrangeRogueRulePriority (*args[2]);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\nRule Name: %s is"
                                   "already exist.\r\n", args[3]);
                    }

                    i4RetStatus = RogueAddRule (CliHandle, (UINT1) *args[0],
                                                (UINT1) *args[1],
                                                (UINT1) *args[2],
                                                (UINT1 *) args[3]);
                    if (i4RetStatus == CLI_SUCCESS)
                    {
                        RfMgmtClassifyRogueAp ();
                    }
                }
            }
            else
            {
                if ((UINT1) *args[2] > gu1RogueRuleCount + 1)
                {
                    CliPrintf (CliHandle, "\r\nPriority should be"
                               "less than of %d\r\n", (gu1RogueRuleCount + 2));
                    break;
                }
                else
                {
                    if (checkRuleNameExist ((UINT1 *) args[3]) ==
                        RFMGMT_SUCCESS)
                    {
                        RfMgmtArrangeRogueRulePriority (*args[2]);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\nRule Name: %s"
                                   "is already exist.\r\n", args[3]);
                    }
                    i4RetStatus = RogueAddRule (CliHandle,
                                                *args[0], 0, *args[2],
                                                (UINT1 *) args[3]);
                    if (i4RetStatus == CLI_SUCCESS)
                    {
                        RfMgmtClassifyRogueAp ();
                    }
                }
            }
            break;
        case CLI_ROGUE_RULE_CLASSIFY:
            i4RetStatus = RogueClassifyRule (CliHandle,
                                             *args[0], *args[1],
                                             (UINT1 *) args[2]);
            if (i4RetStatus == CLI_SUCCESS)
            {
                RfMgmtClassifyRogueAp ();
            }
            break;
        case CLI_ROGUE_RULE_FUNC:
            i4RetStatus =
                RogueRuleFunc (CliHandle, *args[0], (UINT1 *) args[1]);
            if (i4RetStatus == CLI_SUCCESS)
            {
                RfMgmtClassifyRogueAp ();
            }
            break;
        case CLI_ROGUE_RULE_CONDITION_SET:
            if (args[0] != NULL)
            {

                i4RetStatus = RogueSetRuleConditionClient
                    (CliHandle, *args[0], (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[1] != NULL)
            {
                i4RetStatus = RogueSetRuleConditionDuration
                    (CliHandle, *args[1], (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }

            }
            if (args[2] != NULL)
            {
                i4RetStatus = RogueSetRuleConditionEncrypt
                    (CliHandle, *args[2], (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[3] != NULL)
            {
                i4RetStatus = RogueSetRuleConditionRssi
                    (CliHandle, *args[3], (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[4] != NULL)
            {
                i4RetStatus = RogueSetRuleConditionTpc
                    (CliHandle, *args[4], (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[5] != NULL)
            {
                i4RetStatus = RogueSetRuleConditionSsid
                    (CliHandle, (UINT1 *) args[5], (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            break;
        case CLI_ROGUE_RULE_CONDITION_DELETE:
            if (args[0] != NULL)
            {
                i4RetStatus = RogueDeleteRuleConditionClient
                    (CliHandle, (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {

                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[1] != NULL)
            {
                i4RetStatus = RogueDeleteRuleConditionDuration
                    (CliHandle, (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[2] != NULL)
            {
                i4RetStatus = RogueDeleteRuleConditionEncrypt
                    (CliHandle, (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[3] != NULL)
            {
                i4RetStatus = RogueDeleteRuleConditionRssi
                    (CliHandle, (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[4] != NULL)
            {
                i4RetStatus = RogueDeleteRuleConditionTpc
                    (CliHandle, (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            if (args[5] != NULL)
            {
                i4RetStatus = RogueDeleteRuleConditionSsid
                    (CliHandle, (UINT1 *) args[6]);
                if (i4RetStatus == CLI_SUCCESS)
                {
                    RfMgmtClassifyRogueAp ();
                }
            }
            break;
        case CLI_ROGUE_DETECTION:
            i4RetStatus = RogueSetRogueDetection (CliHandle, (UINT1) *args[0]);
            break;
        case CLI_ROGUE_RFNAME:
            i4RetStatus = RogueSetRfGroupName (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_SHOW_ROGUE_AP:
            if (args[1] != NULL)
            {
                StrToMac ((UINT1 *) args[1], BaseMacAddr);
                i4RetStatus = RogueShowDetailedRogueAp (CliHandle, BaseMacAddr);
            }
            else if ((*args[0]) == 4)
            {
                i4RetStatus = RogueShowFullSummaryRogueAp (CliHandle);
            }
            else
            {
                i4RetStatus =
                    RogueShowSummaryRogueAp (CliHandle, (UINT1) *args[0]);
            }
            break;
        case CLI_SHOW_ROGUE_RULE:
            if (args[0] == NULL)
            {
                i4RetStatus = RogueShowSummaryRogueRule (CliHandle);
            }
            else
            {
                i4RetStatus = RogueShowDetailedRogueRule
                    (CliHandle, (UINT1 *) args[0]);
            }
            break;
        case CLI_RFMGMT_ROGUE_TRAP_STATUS:
            i4RetStatus = RogueTrapStatusSet (CliHandle, (UINT1) *args[0]);
            break;
        case CLI_SHOW_ROGUE_AP_STATUS:
            i4RetStatus = RogueShowStatus (CliHandle);
            break;
        case CLI_ROGUE_TIMEOUT:
            i4RetStatus = RogueSetTimeout (CliHandle, *args[0]);
            break;
#endif
        default:
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RFMGMT_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gapRfMgmtCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    RFMGMT_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    return i4RetStatus;
}

/*****************************************************************************/
/* Function     : RfMgmtRadioCreate                                          */
/*                                                                           */
/* Description  : Creates a new row status entry                             */
/*                                                                           */
/* Input        : RadioType                                                  */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtRadioCreate (tCliHandle CliHandle, INT4 i4GetRadioType)
{
    INT4                i4Status = 0;
    INT4                i4RowStatus = 0;

    /* Check whether entry is present already. If yes, then simply return
     * success. Else create the entry  */
    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) == SNMP_FAILURE)
    {
        i4Status = CREATE_AND_GO;
    }
    else
    {
        i4Status = ACTIVE;
    }

    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Set failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtRadioDelete                                          */
/*                                                                           */
/* Description  : Deletes a row status entry                                 */
/*                                                                           */
/* Input        : Radio Type                                                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtRadioDelete (tCliHandle CliHandle, INT4 i4GetRadioType)
{
    INT4                i4RowStatus = 0;

    /* Check whether entry is present already. If yes, then simply return
     * success. Else create the entry  */
    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       " RfMgmtRadioDelete : RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle,
               "Failed to delete the radio configurations for "
               "Radio Type = %d \r\n", i4GetRadioType);
    return CLI_FAILURE;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcMode                                           */
/*                                                                           */
/* Description  : Used to set the Tpc Mode Configuration.                    */
/*                                                                           */
/* Input        : RadioType, TpcMode                                         */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCliTpcMode (tCliHandle CliHandle, INT4 i4GetRadioType, UINT4 u4TpcMode)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRrmTpcMode (&u4ErrCode, i4GetRadioType, (INT4) u4TpcMode) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsRrmTpcMode (i4GetRadioType, (INT4) u4TpcMode) ==
            SNMP_FAILURE)
        {
            i4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcSelectionMode                                  */
/*                                                                           */
/* Description  : Used to set the Tpc Selection Mode Configuration.          */
/*                                                                           */
/* Input        : RadioType, TpcSelectionMode                                */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
RfMgmtCliTpcSelectionMode (tCliHandle CliHandle, INT4 i4GetRadioType,
                           UINT4 u4TpcSelectionMode)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetVal = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRrmTpcSelectionMode (&u4ErrCode, i4GetRadioType,
                                        (INT4) u4TpcSelectionMode) !=
        SNMP_FAILURE)
    {
        if (nmhSetFsRrmTpcSelectionMode
            (i4GetRadioType, (INT4) u4TpcSelectionMode) != SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcPerAPMode                                      */
/*                                                                           */
/* Description  : Used to set the Tpc Per AP Mode Configuration.             */
/*                                                                           */
/* Input        : RadioType, pointer to APName, PerApSelectionMode,          */
/*           pointer to TxPower                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCliTpcPerAPMode (tCliHandle CliHandle, INT4 i4RadioIfType,
                       UINT1 *pu1APName, UINT4 u4PerApSelectionMode,
                       UINT4 *pu4TxPower, UINT1 u1RadioIndex)
{
    INT4                i4RetVal = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NoOfRadio = 0;
    INT4                i4RadioIfIndex = 0;
    UINT1               au1ModelName[CLI_MAX_PROFILE_NAME];
    tSNMP_OCTET_STRING_TYPE ModelName;
    tWsscfgDot11PhyTxPowerEntry WsscfgSetDot11PhyTxPowerEntry;
    tWsscfgIsSetDot11PhyTxPowerEntry WsscfgIsSetDot11PhyTxPowerEntry;

    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WsscfgSetDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgDot11PhyTxPowerEntry));
    MEMSET (&WsscfgIsSetDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgIsSetDot11PhyTxPowerEntry));
    MEMSET (au1ModelName, 0, CLI_MAX_PROFILE_NAME);
    ModelName.pu1_OctetList = au1ModelName;

    if (pu1APName == NULL)
    {
        CliPrintf (CliHandle, "\r\n Invalid input.\r\n");
        return CLI_FAILURE;
    }
    /* When Profile name is received as input get the profile id
     * from mapping table */
    if (CapwapGetWtpProfileIdFromProfileName
        (pu1APName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "nmhGetCapwapBaseWtpProfileWtpModelNumber:Failed \n");
        return CLI_FAILURE;
    }
    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) != SNMP_SUCCESS)
    {
    }
    if (u1RadioIndex == 0)
    {
        for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio; u1RadioIndex++)
        {
            i4RetVal = RfmgmtCliUtilAssignTxPower (CliHandle, u4WtpProfileId,
                                                   u1RadioIndex, i4RadioIfIndex,
                                                   i4RadioIfType,
                                                   u4PerApSelectionMode,
                                                   pu4TxPower);
            if (i4RetVal == CLI_FAILURE)
            {
                break;
            }
        }
    }
    else
    {
        i4RetVal = RfmgmtCliUtilAssignTxPower (CliHandle, u4WtpProfileId,
                                               u1RadioIndex, i4RadioIfIndex,
                                               i4RadioIfType,
                                               u4PerApSelectionMode,
                                               pu4TxPower);

    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : RfmgmtCliUtilAssignTxPower                                 */
/*                                                                           */
/* Description  : Used to set the Txpower based on RadioIndex                */
/*                                                                           */
/* Input        : WtpProfileId, RadioType,RadioIfIndex, RadioIndex           */
/*            pointer to APName, PerApSelectionMode                */
/*           pointer to TxPower                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfmgmtCliUtilAssignTxPower (tCliHandle CliHandle, UINT4 u4WtpProfileId,
                            UINT1 u1RadioIndex, INT4 i4RadioIfIndex,
                            INT4 i4RadioIfType, UINT4 u4PerApSelectionMode,
                            UINT4 *pu4TxPower)
{
    UINT4               u4GetRadioType = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4WtpProfileId, u1RadioIndex, &i4RadioIfIndex) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4GetRadioType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Radiotype Get failed\r\n");
        return SNMP_FAILURE;
    }

    if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioIfType))
    {
        CliPrintf (CliHandle, "Radio Type conflicting\r\n");
        return CLI_SUCCESS;
    }

    if (nmhGetFsRrmRowStatus ((INT4) u4GetRadioType, &i4RowStatus) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus ((INT4) u4GetRadioType, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmAPTpcMode (&u4ErrCode, i4RadioIfIndex,
                                 (INT4) u4PerApSelectionMode) != SNMP_FAILURE)
    {
        if (nmhSetFsRrmAPTpcMode (i4RadioIfIndex, (INT4) u4PerApSelectionMode)
            != SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus ((INT4) u4GetRadioType, i4RowStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (pu4TxPower != NULL)
    {
        if (nmhTestv2FsRrmAPTxPowerLevel (&u4ErrCode, i4RadioIfIndex,
                                          *pu4TxPower) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Invalid Input\r\n");
            return CLI_SUCCESS;
        }
        nmhSetFsRrmAPTxPowerLevel (i4RadioIfIndex, *pu4TxPower);
    }
    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaMode                                           */
/*                                                                           */
/* Description  : Used to set the Dca Mode Configuration.                    */
/*                                                                           */
/* Input        : RadioType, DcaMode                                         */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCliDcaMode (tCliHandle CliHandle, INT4 i4RadioType, UINT4 u4DcaMode)
{
    INT4                i4RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4RadioType, (INT4) u4DcaMode) !=
        SNMP_FAILURE)
    {
        if (nmhSetFsRrmDcaMode (i4RadioType, (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            i4RetVal = CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaSelectionMode                                  */
/*                                                                           */
/* Description  : Used to set the Dca Selection Mode Configuration.          */
/*                                                                           */
/* Input        : RadioType, DcaSelectionMode                                */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
RfMgmtCliDcaSelectionMode (tCliHandle CliHandle, INT4 i4GetRadioType,
                           UINT4 u4DcaSelectionMode)
{
    INT4                i4RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                        (INT4) u4DcaSelectionMode) !=
        SNMP_FAILURE)
    {
        if ((nmhSetFsRrmDcaSelectionMode
             (i4GetRadioType, (INT4) u4DcaSelectionMode) != SNMP_SUCCESS))
        {
        }
    }

    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaPerAPMode                                      */
/*                                                                           */
/* Description  : Used to set the Dca Per AP Mode Configuration.             */
/*                                                                           */
/* Input        : RadioType, pointer to APName, PerApSelectionMode,          */
/*           pointer to Channel number                          */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCliDcaPerAPMode (tCliHandle CliHandle, INT4 i4RadioType,
                       UINT1 *pu1APName, UINT4 u4PerApSelectionMode,
                       UINT4 *pu4ChannelNumber, UINT1 u1RadioIndex)
{
    INT4                i4RetVal = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NoOfRadio = 0;
    UINT4               u4RadioId = 1;
    INT4                i4RadioIfIndex = 0;
    UINT1               au1ModelName[CLI_MAX_PROFILE_NAME];
    tSNMP_OCTET_STRING_TYPE ModelName;

    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ModelName, 0, CLI_MAX_PROFILE_NAME);
    ModelName.pu1_OctetList = au1ModelName;
    if (pu1APName == NULL)
    {
        CliPrintf (CliHandle, "\r\n Invalid input.\r\n");
        return CLI_FAILURE;
    }
    /* When Profile name is received as input get the profile id
     * from mapping table */

    if (CapwapGetWtpProfileIdFromProfileName
        (pu1APName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Get the radio ifindex */
    if ((nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
         (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) != SNMP_SUCCESS))
    {
    }
    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) != SNMP_SUCCESS))
    {
    }
    if (u1RadioIndex == 0)
    {
        for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio; u1RadioIndex++)
        {
            i4RetVal = RfmgmtCliUtilAssignChannel (CliHandle, u4WtpProfileId,
                                                   u1RadioIndex, i4RadioIfIndex,
                                                   i4RadioType,
                                                   u4PerApSelectionMode,
                                                   pu4ChannelNumber);
            if (i4RetVal == CLI_FAILURE)
            {
                break;
            }
        }
    }
    else
    {
        if (u1RadioIndex <= ((UINT1) u4NoOfRadio))
        {
            i4RetVal = RfmgmtCliUtilAssignChannel (CliHandle, u4WtpProfileId,
                                                   u1RadioIndex, i4RadioIfIndex,
                                                   i4RadioType,
                                                   u4PerApSelectionMode,
                                                   pu4ChannelNumber);
        }
        else
        {
            i4RetVal = CLI_FAILURE;
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : RfmgmtCliUtilAssignChannel                                       */
/*                                                                           */
/* Description  : Used to set the Channel based on RadioIndex                */
/*                                                                           */
/* Input        : WtpProfileId, RadioType,RadioIfIndex, RadioIndex           */
/*                      pointer to APName, PerApSelectionMode                */
/*           pointer to TxPower                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfmgmtCliUtilAssignChannel (tCliHandle CliHandle, UINT4 u4WtpProfileId,
                            UINT1 u1RadioIndex, INT4 i4RadioIfIndex,
                            INT4 i4RadioType, UINT4 u4PerApSelectionMode,
                            UINT4 *pu4ChannelNumber)
{
    UINT4               u4GetRadioType = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4WtpProfileId, u1RadioIndex, &i4RadioIfIndex) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                &u4GetRadioType) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "nmhGetFsDot11RadioType: Failed\n");
        return CLI_FAILURE;
    }

    if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
    {
        CliPrintf (CliHandle, "\r\n Radio Type conflicting\r\n");
        return CLI_SUCCESS;
    }
    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmAPChannelMode (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4PerApSelectionMode) !=
        SNMP_FAILURE)
    {
        if ((nmhSetFsRrmAPChannelMode
             (i4RadioIfIndex, (INT4) u4PerApSelectionMode) != SNMP_SUCCESS))
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (pu4ChannelNumber != NULL)
    {
        if (nmhTestv2FsRrmAPAssignedChannel (&u4ErrCode, i4RadioIfIndex,
                                             *pu4ChannelNumber) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Invalid Input\r\n");
            return CLI_SUCCESS;
        }

        nmhSetFsRrmAPAssignedChannel (i4RadioIfIndex, *pu4ChannelNumber);
    }
    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaSensitivity                                    */
/*                                                                           */
/* Description  : Used to set the Dca Sensitivity Level.                     */
/*                                                                           */
/* Input        : RadioType, DcaSensitivityLevel                             */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliDcaSensitivity (tCliHandle CliHandle, INT4 i4RadioType,
                         UINT4 u4DcaSensitivity)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmDcaSensitivity (&u4ErrCode, i4RadioType,
                                      (INT4) u4DcaSensitivity) != SNMP_FAILURE)
    {
        if ((nmhSetFsRrmDcaSensitivity (i4RadioType, (INT4) u4DcaSensitivity) !=
             SNMP_SUCCESS))
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaChannelUpdate                                  */
/*                                                                           */
/* Description  : Used to Update the Channel by running the DCA algorithm.   */
/*                                                                           */
/* Input        : RadioType.                                                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliDcaChannelUpdate (tCliHandle CliHandle, INT4 i4RadioType)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4UpdateChannel = RFMGMT_DCA_UPDATE_CHANNEL;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsRrmUpdateChannel (&u4ErrCode, (INT4) i4RadioType,
                                         (INT4) u4UpdateChannel) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Invalid Update value\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetFsRrmUpdateChannel
             ((INT4) i4RadioType, (INT4) u4UpdateChannel) != SNMP_SUCCESS))
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Rowstatus not active\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaInterval                                       */
/*                                                                           */
/* Description  : Used to set the Dca Anchor Time.                       */
/*                                                                           */
/* Input        : RadioType, DcaInterval.                                    */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliDcaInterval (tCliHandle CliHandle, INT4 i4RadioType,
                      UINT4 u4DcaInterval)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmDcaInterval (&u4ErrCode, i4RadioType,
                                   u4DcaInterval) != SNMP_FAILURE)
    {
        if ((nmhSetFsRrmDcaInterval (i4RadioType, u4DcaInterval) !=
             SNMP_SUCCESS))
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcInterval                                       */
/*                                                                           */
/* Description  : Used to set the Tpc Interval.                              */
/*                                                                           */
/* Input        : RadioType, TpcInterval.                                    */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliTpcInterval (tCliHandle CliHandle, INT4 i4RadioType,
                      UINT4 u4TpcInterval)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmTpcInterval (&u4ErrCode, i4RadioType,
                                   u4TpcInterval) != SNMP_FAILURE)
    {
        if (nmhSetFsRrmTpcInterval (i4RadioType, u4TpcInterval) != SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliShaInterval                                       */
/*                                                                           */
/* Description  : Used to set the Tpc Interval.                              */
/*                                                                           */
/* Input        : RadioType, ShaInterval.                                    */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliShaInterval (tCliHandle CliHandle, INT4 i4RadioType,
                      UINT4 u4ShaInterval)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmSHAInterval (&u4ErrCode, i4RadioType,
                                   u4ShaInterval) != SNMP_FAILURE)
    {
        if (nmhSetFsRrmSHAInterval (i4RadioType, u4ShaInterval) != SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (i1RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcUpdate                                         */
/*                                                                           */
/* Description  : Used to Update the Transmit Power by running the TPC       */
/*                Algorithm.                                                 */
/* Input        : RadioType.                                                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliTpcUpdate (tCliHandle CliHandle, INT4 i4RadioType)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4TpcUpdate = RFMGMT_TPC_UPDATE;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsRrmTpcUpdate (&u4ErrCode, i4RadioType,
                                     (INT4) u4TpcUpdate) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Invalid Update value\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsRrmTpcUpdate (i4RadioType, (INT4) u4TpcUpdate) !=
            SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Rowstatus not active\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliClientThreshold                                   */
/*                                                                           */
/* Description  : Used to set the Client Threshold value that a single AP    */
/*                can handle.                                                */
/* Input        : RadioType, ClientThreshold                                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCliClientThreshold (tCliHandle CliHandle, INT4 i4GetRadioType,
                          UINT4 u4ClientThreshold)
{
    INT4                i4RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRrmClientThreshold (&u4ErrCode, i4GetRadioType,
                                       u4ClientThreshold) != SNMP_FAILURE)
    {
        if ((nmhSetFsRrmClientThreshold (i4GetRadioType, u4ClientThreshold) !=
             SNMP_SUCCESS))
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliEnableDebug                                       */
/*                                                                           */
/* Description  : Used to enable the required debug options.                 */
/*                                                                           */
/* Input        : debugoption.                                               */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
RfMgmtCliEnableDebug (tCliHandle CliHandle, INT4 i4EnableDebug)
{
    UINT4               u4ErrCode = 0;
    INT4                i4OldDebugMask = 0;

    nmhGetFsRrmDebugOption (&i4OldDebugMask);

    i4EnableDebug = i4EnableDebug | i4OldDebugMask;

    if (nmhTestv2FsRrmDebugOption (&u4ErrCode, i4EnableDebug) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Inputs\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsRrmDebugOption (&i4OldDebugMask);
    i4EnableDebug = i4EnableDebug | i4OldDebugMask;
    nmhSetFsRrmDebugOption (i4EnableDebug);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDisableDebug                                      */
/*                                                                           */
/* Description  : Used to enable the required debug options.                 */
/*                                                                           */
/* Input        : debugoption.                                               */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
RfMgmtCliDisableDebug (tCliHandle CliHandle, INT4 i4DisableDebug)
{
    UINT4               u4ErrCode = 0;
    INT4                i4OldDebugMask = 0;

    nmhGetFsRrmDebugOption (&i4OldDebugMask);
    i4DisableDebug = i4DisableDebug & (~i4OldDebugMask);

    if (nmhTestv2FsRrmDebugOption (&u4ErrCode, i4DisableDebug) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Invalid Inputs");
        return CLI_FAILURE;
    }

    nmhSetFsRrmDebugOption (i4DisableDebug);
    return CLI_FAILURE;
}

/*****************************************************************************/
/* Function     : RfMgmtShowAPAllowedChannel                                 */
/*                                                                           */
/* Description  : This function will display Allowed Channel list for an AP  */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                pu1ApProfileName - AP Profile Name                         */
/*                u4RadioId - u4RadioId                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowAPAllowedChannel (tCliHandle CliHandle,
                            INT4 i4RadioType,
                            UINT1 *pu1CapwapBaseWtpProfileName, UINT4 u4RadioId)
{
    tMacAddr            RadioMacAddr;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    INT4                i4RowStatus = 0;
    UINT1               u1Channel = 0;
    UINT1               u1count = 0;
    UINT1               u1Index = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4GetRadioType = 0;
    UINT1               au1AllowedList[RFMGMT_MAX_CHANNELA];
    UINT1               au1TempAused[RFMGMT_MAX_CHANNELA];
    UINT1               au1TempBused[RFMGMT_MAX_CHANNELB];
    tSNMP_OCTET_STRING_TYPE AllowedList;
    MEMSET (au1AllowedList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (&RadioMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&AllowedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TempAused, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1TempBused, 0, RFMGMT_MAX_CHANNELB);
    AllowedList.pu1_OctetList = au1AllowedList;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nNo row found\r\n");
        return CLI_FAILURE;
    }

    /* Validate the input */
    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Not Found\r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Get the radio ifindex */
    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId, (INT4 *) &u4RadioIfIndex)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex: Failed\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsDot11RadioType ((INT4) u4RadioIfIndex,
                                &u4GetRadioType) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "nmhGetFsDot11RadioType: Failed\n");
        return CLI_FAILURE;
    }

    if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
    {
        CliPrintf (CliHandle, "\r\nRadio Type conflicting\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsRrmAPChannelAllowedList (u4RadioIfIndex, &AllowedList) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nNo entry found\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\n");
        CliPrintf (CliHandle, "\rAllowed Channels                 : ");
        if (i4RadioType == CLI_RADIO_TYPEA)
        {
            for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
            {
                if (au1AllowedList[u1Channel] != 0x0)
                {
                    au1TempAused[u1count] = au1AllowedList[u1Channel];
                    u1count++;
                }
            }
            for (u1Index = 0; u1Index < u1count; u1Index++)
            {
                if (au1TempAused[u1Index + 1] != 0)
                {
                    CliPrintf (CliHandle, "%d, ", au1TempAused[u1Index]);
                }
                else
                {
                    CliPrintf (CliHandle, "%d", au1TempAused[u1Index]);
                }
            }
        }
        else
        {
            for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
            {
                if (au1AllowedList[u1Channel] != 0x0)
                {
                    au1TempBused[u1count] = au1AllowedList[u1Channel];
                    u1count++;
                }
            }
            for (u1Index = 0; u1Index < u1count; u1Index++)
            {
                if (au1TempBused[u1Index + 1] != 0)
                {
                    CliPrintf (CliHandle, "%d, ", au1TempBused[u1Index]);
                }
                else
                {
                    CliPrintf (CliHandle, "%d", au1TempBused[u1Index]);
                }
            }
        }
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShowAPAutoRf                                         */
/*                                                                           */
/* Description  : This function will display auto RF related info for each AP*/
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                pu1ApProfileName - AP Profile Name                         */
/*                u4RadioId - u4RadioId                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowAPAutoRf (tCliHandle CliHandle,
                    INT4 i4RadioType, UINT1 *pu1CapwapBaseWtpProfileName,
                    UINT4 u4RadioId)
{
    tMacAddr            RadioMacAddr;
    tMacAddr            NeighMacAddr;
    tMacAddr            ClientMacAddr;
    tMacAddr            NextNeighMacAddr;
    tMacAddr            NextClientMacAddr;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    UINT1               au1NeighString[CLI_MAC_TO_STR_LEN];
    UINT1               au1ClientString[CLI_MAC_TO_STR_LEN];
    UINT4               u4RadioIfIndex = 0;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    INT4                i4SNR = 0;
    INT4                i4LastSNRScan = 0;
    UINT4               u4NextScannedChannel = 0;
    UINT4               u4ScannedChannel = 0;
    UINT4               u4GetRadioType = 0;
    UINT4               u4ChannelChangeCount = 0;
    UINT4               u4ChannelChangeTime = 0;
    UINT4               u4TxPowerChangeCount = 0;
    UINT4               u4TxPowerChngeTime = 0;
    UINT4               u4AssignedChannel = 0;
    UINT4               u4NeighborCount = 0;
    UINT4               u4TxPowerLevel = 0;
    CHR1                au1TimeSuccess[RFMGMT_MAX_DATE_LEN];
    CHR1                au1Time[RFMGMT_MAX_DATE_LEN];
    CHR1                au1TxPowerChange[RFMGMT_MAX_DATE_LEN];
    INT4                i4RowStatus = 0;

    MEMSET (au1TimeSuccess, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (au1Time, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (au1TxPowerChange, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (&RadioMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&ClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&au1String, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (&au1NeighString, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (&au1ClientString, 0, CLI_MAC_TO_STR_LEN);

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n No row found\r\n");
        return CLI_FAILURE;
    }

    /* Validate the input */
    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Not Found");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Get the radio ifindex */
    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId, (INT4 *) &u4RadioIfIndex)
        == SNMP_SUCCESS)
    {
    }

    if (nmhGetFsRrmAPMacAddress ((INT4) u4RadioIfIndex,
                                 &RadioMacAddr) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "nmhGetFsRrmAPMacAddress: Failed\n");
        return CLI_FAILURE;
    }
    if (nmhGetFsDot11RadioType ((INT4) u4RadioIfIndex,
                                &u4GetRadioType) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "nmhGetFsDot11RadioType: Failed\n");
        return CLI_FAILURE;
    }

    if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
    {
        CliPrintf (CliHandle, "\r\n Radio Type conflicting\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsRrmAPAssignedChannel ((INT4) u4RadioIfIndex, &u4AssignedChannel);

    nmhGetFsRrmAPChannelChangeCount ((INT4) u4RadioIfIndex,
                                     &u4ChannelChangeCount);

    nmhGetFsRrmAPChannelChangeTime ((INT4) u4RadioIfIndex,
                                    &u4ChannelChangeTime);
    UtlGetTimeStrForTicks (u4ChannelChangeTime, (CHR1 *) au1TimeSuccess);

    nmhGetFsRrmAPTxPowerLevel ((INT4) u4RadioIfIndex, &u4TxPowerLevel);

    nmhGetFsRrmTxPowerChangeCount ((INT4) u4RadioIfIndex,
                                   &u4TxPowerChangeCount);
    nmhGetFsRrmTxPowerChangeTime ((INT4) u4RadioIfIndex, &u4TxPowerChngeTime);

    CliPrintf (CliHandle, "\r\nAP Profile Name                  : %s",
               pu1CapwapBaseWtpProfileName);
    CliMacToStr (RadioMacAddr, au1String);
    CliPrintf (CliHandle, "\r\nMac Address                      : %s",
               au1String);

    if (u4GetRadioType == CLI_RADIO_TYPEA)
    {
        CliPrintf (CliHandle, "\r\nRadio Type                       : dot11a");
    }
    else if (u4GetRadioType == CLI_RADIO_TYPEB)
    {
        CliPrintf (CliHandle, "\r\nRadio Type                       : dot11b");
    }
    else if (u4GetRadioType == CLI_RADIO_TYPEG)
    {
        CliPrintf (CliHandle, "\r\nRadio Type                       : dot11g");
    }
    else if (u4GetRadioType == CLI_RADIO_TYPEBG)
    {
        CliPrintf (CliHandle, "\r\nRadio Type                       : dot11bg");
    }
    else if (u4GetRadioType == CLI_RADIO_TYPEAN)
    {
        CliPrintf (CliHandle, "\r\nRadio Type                       : dot11an");
    }
    else if (u4GetRadioType == CLI_RADIO_TYPEAC)
    {
        CliPrintf (CliHandle, "\r\nRadio Type                       : dot11ac");
    }
    else if (u4GetRadioType == CLI_RADIO_TYPEBGN)
    {
        CliPrintf (CliHandle,
                   "\r\nRadio Type                       : dot11bgn");
    }
    CliPrintf (CliHandle, "\r\nCurrent Channel                  : %d",
               u4AssignedChannel);
    CliPrintf (CliHandle, "\r\nCurrent TxPower Level            : %d",
               u4TxPowerLevel);

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "\r\nNearby RADs");

    if (nmhGetFirstIndexFsRrmAPStatsTable (&i4NextIndex,
                                           &u4NextScannedChannel,
                                           &NextNeighMacAddr) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n  No entry found");
    }
    else
    {
        do
        {
            i4Index = i4NextIndex;
            u4ScannedChannel = u4NextScannedChannel;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (i4Index == (INT4) u4RadioIfIndex)
            {
                nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                      NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1NeighString);
                CliPrintf (CliHandle, "\r\n  RAD %-18s(CHA %d) : %d dBm",
                           au1NeighString, u4ScannedChannel, i4Rssi);
                u4NeighborCount++;
            }
        }
        while (nmhGetNextIndexFsRrmAPStatsTable (i4Index, &i4NextIndex,
                                                 u4ScannedChannel,
                                                 &u4NextScannedChannel,
                                                 NeighMacAddr,
                                                 &NextNeighMacAddr) ==
               SNMP_SUCCESS);
    }

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "\r\nChannel Assignment Information");
    CliPrintf (CliHandle, "\r\n  Neighbor Count                    : %d",
               u4NeighborCount);
    CliPrintf (CliHandle, "\r\n  Channel Change Count             : %d",
               u4ChannelChangeCount);

    if (u4ChannelChangeCount != 0)
    {
        CliPrintf (CliHandle, "\r\n  Last Channel Change Time       : %-12s\n",
                   au1TimeSuccess);
        MEMSET (au1TimeSuccess, 0, RFMGMT_MAX_DATE_LEN);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Last Channel Change Time       : Not Executed\n");
    }

    /* Txpower information */

    CliPrintf (CliHandle, "\r\nConnected STAs");
    i4NextIndex = 0;
    if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                             &NextClientMacAddr) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n  No entry found");
    }
    else
    {
        do
        {
            i4Index = i4NextIndex;
            MEMCPY (&ClientMacAddr, &NextClientMacAddr, sizeof (tMacAddr));
            if (i4NextIndex == (INT4) u4RadioIfIndex)
            {

                nmhGetFsRrmClientSNR (i4Index, ClientMacAddr, &i4SNR);
                nmhGetFsRrmClientLastSNRScan (i4Index, ClientMacAddr,
                                              (UINT4 *) (&i4LastSNRScan));
                CliMacToStr (ClientMacAddr, au1ClientString);
                UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan,
                                       (CHR1 *) au1TimeSuccess);

                CliPrintf (CliHandle,
                           "\r\n  Client MAC                  : %-18s",
                           au1ClientString);
                CliPrintf (CliHandle, "\r\n  SNR                         : %d",
                           i4SNR);
            }
        }
        while (nmhGetNextIndexFsRrmTpcClientTable (i4Index, &i4NextIndex,
                                                   ClientMacAddr,
                                                   &NextClientMacAddr) ==
               SNMP_SUCCESS);
    }

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "\r\nTxpower Assignment Information");
    CliPrintf (CliHandle, "\r\n  TxPower Change Count           : %d",
               u4TxPowerChangeCount);
    if (u4TxPowerChangeCount != 0)
    {
        UtlGetTimeStrForTicks (u4TxPowerChngeTime, (CHR1 *) au1TimeSuccess);
        CliPrintf (CliHandle, "\r\n  Last Txpower Change Time       : %-12s",
                   au1TimeSuccess);
        MEMSET (au1TimeSuccess, 0, RFMGMT_MAX_DATE_LEN);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Last Txpower Change Time       : Not Executed");
    }

    CliPrintf (CliHandle, "\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaChannelStatus                                  */
/*                                                                           */
/* Description  : Used to add or delete the channel numbers that the WLC can */
/*                assign to the WTP.                                         */
/* Input        : RadioType, ChannelNumberStatus, ChannelNumber              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliDcaChannelStatus (tCliHandle CliHandle, INT4 i4GetRadioType,
                           UINT4 u4ChannelNumberStatus, UINT4 u4ChannelNumber)
{
    UINT1               au1Dot11bChannelList[RFMGMT_MAX_CHANNELA];
    UINT1               au1Dot11bChannelList1[RFMGMT_MAX_CHANNELA];
    UINT1               u1Channel = 0;
    tSNMP_OCTET_STRING_TYPE ChannelList;
    tSNMP_OCTET_STRING_TYPE TmpChannelList;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    MEMSET (&au1Dot11bChannelList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (&au1Dot11bChannelList1, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (&ChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TmpChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ChannelList.pu1_OctetList = au1Dot11bChannelList1;
    TmpChannelList.pu1_OctetList = au1Dot11bChannelList;

    if (i4GetRadioType == RFMGMT_RADIO_TYPEB)
    {
        if (u4ChannelNumber > RFMGMT_MAX_CHANNELB)
        {
            CliPrintf (CliHandle, "Invalid Channel number\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (u4ChannelNumberStatus == CLI_DCA_ADD_CHANNEL)
    {
        if (nmhGetFsRrmAllowedChannels (i4GetRadioType, &TmpChannelList)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n Allowed Channels fetch failed");
            if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, " RfMgmtCliDcaChannelStatus : "
                           "RowStatus Set failed\r\n");
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (i4GetRadioType == RFMGMT_RADIO_TYPEA)
        {
            for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
            {
                if (gau1Dot11aRegDomainChannelList[u1Channel] ==
                    u4ChannelNumber)
                {
                    TmpChannelList.pu1_OctetList[u1Channel] =
                        (UINT1) u4ChannelNumber;
                }
            }
        }
        else
        {
            TmpChannelList.pu1_OctetList[u4ChannelNumber - 1] =
                (UINT1) u4ChannelNumber;
        }

        if (nmhTestv2FsRrmAllowedChannels (&u4ErrCode, i4GetRadioType,
                                           &TmpChannelList) != SNMP_SUCCESS)
        {
            if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "RfMgmtCliDcaChannelStatus :"
                           "RowStatus Set failed\r\n");
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "Channel list validation failed\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetFsRrmAllowedChannels (i4GetRadioType, &TmpChannelList) !=
             SNMP_SUCCESS))
        {
        }
    }

    if (u4ChannelNumberStatus == CLI_DCA_DELETE_CHANNEL)
    {
        if (nmhGetFsRrmUnusedChannels (i4GetRadioType,
                                       &ChannelList) != SNMP_SUCCESS)
        {
            if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "RfMgmtCliDcaChannelStatus :"
                           "RowStatus Set failed\r\n");
                return CLI_FAILURE;
            }

            return CLI_FAILURE;
        }
        if (i4GetRadioType == RFMGMT_RADIO_TYPEA)
        {
            for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
            {
                if (gau1Dot11aRegDomainChannelList[u1Channel] ==
                    u4ChannelNumber)
                {
                    ChannelList.pu1_OctetList[u1Channel] =
                        (UINT1) u4ChannelNumber;
                }
            }
        }
        else
        {
            ChannelList.pu1_OctetList[u4ChannelNumber - 1] =
                (UINT1) u4ChannelNumber;
        }
        if (nmhTestv2FsRrmUnusedChannels (&u4ErrCode, i4GetRadioType,
                                          &ChannelList) != SNMP_SUCCESS)
        {
            if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "RfMgmtCliDcaChannelStatus : "
                           "RowStatus Set failed\r\n");
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "Channel list validation failed\r\n");
            return CLI_FAILURE;
        }
        nmhSetFsRrmUnusedChannels (i4GetRadioType, &ChannelList);
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }

    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcSNRThreshold                                   */
/*                                                                           */
/* Description  : Used to set the SNR Threshold value                        */
/*                                                                           */
/* Input        : RadioType, SNR Threshold, pointer to profile name,         */
/*                pointer to radio id.                                       */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliTpcSNRThreshold (tCliHandle CliHandle, INT4 i4GetRadioType,
                          INT4 i4SNRThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmSNRThreshold (&u4ErrCode, i4GetRadioType,
                                    i4SNRThreshold) != SNMP_FAILURE)
    {
        if (nmhSetFsRrmSNRThreshold (i4GetRadioType, i4SNRThreshold) !=
            SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliBssidSNRScanStatus                                */
/*                                                                           */
/* Description  : Used to set the SNR Scan status of particular AP           */
/*                                                                           */
/* Input        : RadioType, SNR Threshold, pointer to profile name,         */
/*                pointer to radio id.                                       */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliBssidSNRScanStatus (tCliHandle CliHandle, INT4 i4GetRadioType,
                             UINT4 u4SNRScanStatus, UINT1 *pu1ProfileName,
                             UINT4 u4RadioId, UINT4 u4WlanId)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4RadioIfIndex = 0;

    if (pu1ProfileName == NULL)
    {
        CliPrintf (CliHandle, "Invalid input\n\n");
        return CLI_FAILURE;
    }
    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u4RadioId,
                                    i4GetRadioType,
                                    &i4RadioIfIndex) == OSIX_SUCCESS)
    {
        if (nmhTestv2FsRrmSSIDScanStatus (&u4ErrCode, i4RadioIfIndex,
                                          u4WlanId,
                                          (INT4) u4SNRScanStatus) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Getting radio index failed \r\n");
            return CLI_FAILURE;
        }
        nmhSetFsRrmSSIDScanStatus (i4RadioIfIndex, u4WlanId,
                                   (INT4) u4SNRScanStatus);
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliRssiThreshold                                     */
/*                                                                           */
/* Description  : Used to set the Rssi Threshold Value.                      */
/*                                                                           */
/* Input        : RadioType, Rssi Threshold                                  */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliRssiThreshold (tCliHandle CliHandle, INT4 i4GetRadioType,
                        INT4 i4RssiThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmRSSIThreshold (&u4ErrCode, i4GetRadioType,
                                     i4RssiThreshold) != SNMP_FAILURE)
    {
        if (nmhSetFsRrmRSSIThreshold (i4GetRadioType, i4RssiThreshold) !=
            SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid RSSI Threshold value\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliPowerThreshold                                    */
/*                                                                           */
/* Description  : Used to set the Power Threshold Value.                     */
/*                                                                           */
/* Input        : RadioType, Power Threshold                                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliPowerThreshold (tCliHandle CliHandle, INT4 i4GetRadioType,
                         INT4 i4PowerThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmPowerThreshold (&u4ErrCode, i4GetRadioType,
                                      i4PowerThreshold) != SNMP_FAILURE)
    {
        if (nmhSetFsRrmPowerThreshold (i4GetRadioType, i4PowerThreshold) !=
            SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid Power Threshold value\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliChannelSwitchStatus                               */
/*                                                                           */
/* Description  : Used to set the Channel Switch Message Status.             */
/*                                                                           */
/* Input        : RadioType, Rssi Threshold                                  */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliChannelSwitchStatus (tCliHandle CliHandle, UINT4 u4ChannelSwitchStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRrmChannelSwitchMsgStatus (&u4ErrCode,
                                              (INT4) u4ChannelSwitchStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Value \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsRrmChannelSwitchMsgStatus ((INT4) u4ChannelSwitchStatus);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCli11hDfsChannelSwitchStatus                         */
/*                                                                           */
/* Description  : Used to set the Channel Switch Message Status for an AP    */
/*                                  and for a particular radio               */
/*                                                                           */
/* Input        : RadioType, Channel Switch Status                           */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCli11hDfsChannelSwitchStatus (tCliHandle CliHandle, INT4 i4RadioType,
                                    UINT4 u4ChannelSwitchStatus,
                                    UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{

    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /*If no Profile name is provided, Quiet Interval  is updated */
    /* for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSChannelSwitchStatus
                    (&u4ErrCode, i4RadioIfIndex,
                     u4ChannelSwitchStatus) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Channel Switch Status value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSChannelSwitchStatus (i4RadioIfIndex,
                                                      u4ChannelSwitchStatus);
            }
            else
            {
                continue;
            }

        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n WTP Profile %d \r\n", u4WtpProfileId);
        /*If AP name is provided and not the radio id, all radio id's of the
           AP is updated with the Quiet Interval value */
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrm11hDFSChannelSwitchStatus
                            (&u4ErrCode, i4RadioIfIndex,
                             u4ChannelSwitchStatus) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid Channel Switch Status value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrm11hDFSChannelSwitchStatus (i4RadioIfIndex,
                                                              u4ChannelSwitchStatus);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Quiet Interval is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);
            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSChannelSwitchStatus
                    (&u4ErrCode, i4RadioIfIndex,
                     u4ChannelSwitchStatus) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Channel Switch Status value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSChannelSwitchStatus (i4RadioIfIndex,
                                                      u4ChannelSwitchStatus);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliSNRScanStatus                                     */
/*                                                                           */
/* Description  : Used to set the SNR Scan status of the AP.                 */
/*                                                                           */
/* Input        : RadioType, Scan Status, Profile Name, Pointer to radio id. */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliSNRScanStatus (tCliHandle CliHandle, INT4 i4RadioType,
                        UINT4 u4SNRScanStatus, UINT1 *pu1ProfileName,
                        UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If no Profile name is provided SNR scan status is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPSNRScanStatus (&u4ErrCode, i4RadioIfIndex,
                                                   (INT4) u4SNRScanStatus) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid SNR scan Status value\r\n");
                    return CLI_FAILURE;
                }
                if (nmhSetFsRrmAPSNRScanStatus
                    (i4RadioIfIndex, (INT4) u4SNRScanStatus) != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         * AP is updated with the SNR scan status. */
        if (pu4RadioId == NULL)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrmAPSNRScanStatus
                            (&u4ErrCode, i4RadioIfIndex,
                             (INT4) u4SNRScanStatus) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid SNR Scan Status value\r\n");
                            return CLI_FAILURE;
                        }
                        if (nmhSetFsRrmAPSNRScanStatus (i4RadioIfIndex,
                                                        (INT4) u4SNRScanStatus)
                            == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Set SNR Scan Status Failed\r\n");
                            return CLI_FAILURE;
                        }

                    }

                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP SNR scan status is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPSNRScanStatus (&u4ErrCode, i4RadioIfIndex,
                                                   (INT4) u4SNRScanStatus) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid SNR scan status value\r\n");
                    return CLI_FAILURE;
                }
                if (nmhSetFsRrmAPSNRScanStatus
                    (i4RadioIfIndex, (INT4) u4SNRScanStatus) != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliSNRScanPeriod.                                    */
/*                                                                           */
/* Description  : Used to set the SNR Scan period of the AP.                 */
/*                                                                           */
/* Input        : RadioType, Scan period, Profile Name, Pointer to radio id. */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliSNRScanPeriod (tCliHandle CliHandle, INT4 i4RadioType,
                        UINT4 u4SNRScanPeriod, UINT1 *pu1ProfileName,
                        UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If no Profile name is provided SNR scan period is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmSNRScanFreq (&u4ErrCode, i4RadioIfIndex,
                                               (UINT4) u4SNRScanPeriod) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid SNR scan period value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmSNRScanFreq (i4RadioIfIndex,
                                        (UINT4) u4SNRScanPeriod);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (pu4RadioId == NULL)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrmSNRScanFreq
                            (&u4ErrCode, i4RadioIfIndex,
                             (UINT4) u4SNRScanPeriod) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid SNR Scan period value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrmSNRScanFreq (i4RadioIfIndex,
                                                (UINT4) u4SNRScanPeriod);

                    }

                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP SNR scan period is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmSNRScanFreq (&u4ErrCode, i4RadioIfIndex,
                                               (UINT4) u4SNRScanPeriod) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid SNR scan period value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmSNRScanFreq (i4RadioIfIndex,
                                        (UINT4) u4SNRScanPeriod);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliAPAutoScanStatus                                  */
/*                                                                           */
/* Description  : Used to set the SNR Scan status of the AP.                 */
/*                                                                           */
/* Input        : RadioType, Scan Status, Profile Name, Pointer to radio id. */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliAPAutoScanStatus (tCliHandle CliHandle, INT4 i4RadioType,
                           UINT4 u4APAutoScanStatus, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    /*If no Profile name is provided AP Auto scan status is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPAutoScanStatus (&u4ErrCode, i4RadioIfIndex,
                                                    (INT4) u4APAutoScanStatus)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid AP auto scan status value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPAutoScanStatus (i4RadioIfIndex,
                                             (INT4) u4APAutoScanStatus);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         * AP is updated with the AP auto scan status. */
        if (pu4RadioId == NULL)
        {
            if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrmAPAutoScanStatus
                            (&u4ErrCode, i4RadioIfIndex,
                             (INT4) u4APAutoScanStatus) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid AP auto Scan Status value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrmAPAutoScanStatus (i4RadioIfIndex,
                                                     (INT4) u4APAutoScanStatus);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, auto scan status is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPAutoScanStatus (&u4ErrCode, i4RadioIfIndex,
                                                    (INT4) u4APAutoScanStatus)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid AP auto scan status value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPAutoScanStatus (i4RadioIfIndex,
                                             (INT4) u4APAutoScanStatus);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaChannelScanDuration                            */
/*                                                                           */
/* Description  : Used to set the Channel Scan Duration of the AP            */
/*                                                                           */
/* Input        : RadioType, Scan Duration, Profile Name, Pointer to radio id*/
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliDcaChannelScanDuration (tCliHandle CliHandle, INT4 i4RadioType,
                                 UINT4 u4ScanDuration, UINT1 *pu1ProfileName,
                                 UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If no Profile name is provided channel scan is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPChannelScanDuration
                    (&u4ErrCode, i4RadioIfIndex,
                     (UINT4) u4ScanDuration) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid channel scan duration value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPChannelScanDuration (i4RadioIfIndex,
                                                  (UINT4) u4ScanDuration);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         * AP is updated with the channel scan duration. */
        if (pu4RadioId == NULL)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrmAPChannelScanDuration
                            (&u4ErrCode, i4RadioIfIndex,
                             (UINT4) u4ScanDuration) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid channel scan duration value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrmAPChannelScanDuration (i4RadioIfIndex,
                                                          (UINT4)
                                                          u4ScanDuration);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, channel scan duration */
        /*is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPChannelScanDuration
                    (&u4ErrCode, i4RadioIfIndex,
                     (UINT4) u4ScanDuration) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid channel scan duration value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPChannelScanDuration (i4RadioIfIndex,
                                                  (UINT4) u4ScanDuration);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliDcaScanFrequency                                  */
/*                                                                           */
/* Description  : Used to set frequency that AP sends database information   */
/*                to AC.                                                     */
/* Input        : RadioType, Frequency, Profile Name, Pointer to radio id    */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliDcaScanFrequency (tCliHandle CliHandle, INT4 i4RadioType,
                           UINT4 u4ScanFrequency, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If no Profile name is provided scan frequency is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPNeighborScanFreq
                    (&u4ErrCode, i4RadioIfIndex,
                     (UINT4) u4ScanFrequency) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid scan frequency value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPNeighborScanFreq (i4RadioIfIndex,
                                               (UINT4) u4ScanFrequency);
            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         * AP is updated with scan frequency */
        if (pu4RadioId == NULL)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrmAPNeighborScanFreq
                            (&u4ErrCode, i4RadioIfIndex,
                             (UINT4) u4ScanFrequency) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid scan frequency value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrmAPNeighborScanFreq (i4RadioIfIndex,
                                                       (UINT4) u4ScanFrequency);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, scan frequency is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPNeighborScanFreq
                    (&u4ErrCode, i4RadioIfIndex,
                     (UINT4) u4ScanFrequency) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid scan frequency value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPNeighborScanFreq (i4RadioIfIndex,
                                               (UINT4) u4ScanFrequency);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliNeighborAgingPeriod                               */
/*                                                                           */
/* Description  : Used to set neighbor age out period after which the AP     */
/*                deletes the neighbor from database                         */
/* Input        : RadioType, Ageout period,Profile Name,pointer to radio id. */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliNeighborAgingPeriod (tCliHandle CliHandle, INT4 i4RadioType,
                              UINT4 u4AgingPeriod, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If no Profile name is provided, neighbor aging period is updated */
    /* for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPNeighborAgingPeriod
                    (&u4ErrCode, i4RadioIfIndex,
                     (UINT4) u4AgingPeriod) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid neighbor aging period value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPNeighborAgingPeriod (i4RadioIfIndex,
                                                  (UINT4) u4AgingPeriod);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         * AP is updated with the neighbor aging period*/
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrmAPNeighborAgingPeriod
                            (&u4ErrCode, i4RadioIfIndex,
                             (UINT4) u4AgingPeriod) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid Neighbor aging period value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrmAPNeighborAgingPeriod (i4RadioIfIndex,
                                                          (UINT4)
                                                          u4AgingPeriod);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, scan frequency is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrmAPNeighborAgingPeriod
                    (&u4ErrCode, i4RadioIfIndex,
                     (UINT4) u4AgingPeriod) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Neighbor aging period value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrmAPNeighborAgingPeriod (i4RadioIfIndex,
                                                  (UINT4) u4AgingPeriod);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliTxPowerTrapStatus                                 */
/*                                                                           */
/* Description  : Used to set Trap Status of Transmit Power Change.          */
/*                                                                           */
/* Input        : Tx power trap status                                       */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliTxPowerTrapStatus (tCliHandle CliHandle, UINT4 u4TxPowerTrapStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRrmTxPowerChangeTrapStatus (&u4ErrCode,
                                               (INT4) u4TxPowerTrapStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid status \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsRrmTxPowerChangeTrapStatus ((INT4) u4TxPowerTrapStatus);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliShowTxPowerTrapStatus                             */
/*                                                                           */
/* Description  : Used to show Trap Status of Transmit Power Change.         */
/*                                                                           */
/* Input        : Tx power trap status                                       */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliShowTxPowerTrapStatus (tCliHandle CliHandle)
{
    INT4                i4TxPowerTrapStatus = 0;

    nmhGetFsRrmTxPowerChangeTrapStatus (&i4TxPowerTrapStatus);
    if (i4TxPowerTrapStatus == RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n Txpower change trap    :   ENABLE\n");
    }
    if (i4TxPowerTrapStatus == RFMGMT_TX_POWER_CHANGE_TRAP_DISABLE)
    {
        CliPrintf (CliHandle, "\r\n Txpower change trap    :   DISABLE\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliChannelChangeTrapStatus                           */
/*                                                                           */
/* Description  : Used to set Trap Status for Channel change in AP.          */
/*                                                                           */
/* Input        : Channel change trap status                                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliChannelChangeTrapStatus (tCliHandle CliHandle,
                                  UINT4 u4ChannelChangeTrapStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRrmChannelChangeTrapStatus (&u4ErrCode,
                                               (INT4) u4ChannelChangeTrapStatus)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid status \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsRrmChannelChangeTrapStatus ((INT4) u4ChannelChangeTrapStatus);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hDfsQuietInterval                               */
/*                                                                           */
/* Description  : Used to set The Quiet Interval for in AP.                  */
/*                                                                           */
/* Input        : RadioType, Quiet Interval, Profile Name, Radio ID          */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCli11hDfsQuietInterval (tCliHandle CliHandle, INT4 i4RadioType,
                              UINT4 u4QuietInterval, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /*If no Profile name is provided, Quiet Interval  is updated */
    /* for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSQuietInterval
                    (&u4ErrCode, i4RadioIfIndex,
                     u4QuietInterval) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Quiet Inteval value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSQuietInterval (i4RadioIfIndex,
                                                u4QuietInterval);
            }
            else
            {
                continue;
            }

        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n WTP Profile %d \r\n", u4WtpProfileId);
        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the Quiet Interval value*/
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrm11hDFSQuietInterval
                            (&u4ErrCode, i4RadioIfIndex,
                             u4QuietInterval) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid Quiet Interval value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrm11hDFSQuietInterval (i4RadioIfIndex,
                                                        u4QuietInterval);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Quiet Interval is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);
            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSQuietInterval
                    (&u4ErrCode, i4RadioIfIndex,
                     u4QuietInterval) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Quiet Interval value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSQuietInterval (i4RadioIfIndex,
                                                u4QuietInterval);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hDfsQuietPeriod                                 */
/*                                                                           */
/* Description  : Used to set The Quiet Period  for in AP.                   */
/*                                                                           */
/* Input        : RadioType, Quiet Period, Profile Name, Radio ID            */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCli11hDfsQuietPeriod (tCliHandle CliHandle, INT4 i4RadioType,
                            UINT4 u4QuietPeriod, UINT1 *pu1ProfileName,
                            UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /*If no Profile name is provided, Quiet Interval  is updated */
    /* for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSQuietPeriod (&u4ErrCode, i4RadioIfIndex,
                                                     u4QuietPeriod)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Quiet Period value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSQuietPeriod (i4RadioIfIndex, u4QuietPeriod);
            }
            else
            {
                continue;
            }

        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n WTP Profile %d \r\n", u4WtpProfileId);
        /*If AP name is provided and not the radio id, all radio id's of the
           AP is updated with the Quiet period value */
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrm11hDFSQuietPeriod
                            (&u4ErrCode, i4RadioIfIndex,
                             u4QuietPeriod) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid Quiet Interval value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrm11hDFSQuietPeriod (i4RadioIfIndex,
                                                      u4QuietPeriod);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Quiet Period is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);
            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSQuietPeriod (&u4ErrCode, i4RadioIfIndex,
                                                     u4QuietPeriod)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Quiet Period value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSQuietPeriod (i4RadioIfIndex, u4QuietPeriod);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hDfsMeasurementInterval                          */
/*                                                                           */
/* Description  : Used to set The Measurement Interval for in AP.             */
/*                                                                           */
/* Input        : RadioType, Measurement Interval, Profile Name, Radio ID     */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCli11hDfsMeasurementInterval (tCliHandle CliHandle, INT4 i4RadioType,
                                    UINT4 u4MeasurementInterval,
                                    UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /*If no Profile name is provided, Measurement Interval  is updated */
    /* for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSMeasurementInterval
                    (&u4ErrCode, i4RadioIfIndex,
                     u4MeasurementInterval) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Measurement Inteval value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSMeasurementInterval (i4RadioIfIndex,
                                                      u4MeasurementInterval);
            }
            else
            {
                continue;
            }

        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n WTP Profile %d \r\n", u4WtpProfileId);
        /*If AP name is provided and not the radio id, all radio id's of the
           AP is updated with the Measurement Interval value */
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrm11hDFSMeasurementInterval
                            (&u4ErrCode, i4RadioIfIndex,
                             u4MeasurementInterval) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid Measurement Interval value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrm11hDFSMeasurementInterval (i4RadioIfIndex,
                                                              u4MeasurementInterval);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Measurement Interval is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);
            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hDFSMeasurementInterval
                    (&u4ErrCode, i4RadioIfIndex,
                     u4MeasurementInterval) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Measurement Interval value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hDFSMeasurementInterval (i4RadioIfIndex,
                                                      u4MeasurementInterval);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hDfsStatus                                      */
/*                                                                           */
/* Description  : Used to set the Dfs Status Configuration.                  */
/*                                                                           */
/* Input        : RadioType, DfsStatus                                       */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCli11hDfsStatus (tCliHandle CliHandle, INT4 i4GetRadioType,
                       UINT4 u4DfsStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRrm11hDfsStatus (&u4ErrCode, i4GetRadioType,
                                    (INT4) u4DfsStatus) == SNMP_SUCCESS)
    {
        nmhSetFsRrm11hDfsStatus (i4GetRadioType, (INT4) u4DfsStatus);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShow11hDfsInfo                                       */
/*                                                                           */
/* Description  : This function will display the DFS related Info.           */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                pu1ApProfileName - AP Profile Name                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/

INT4
RfMgmtShow11hDfsInfo (tCliHandle CliHandle,
                      UINT1 *pu1CapwapBaseWtpProfileName, UINT4 u4RadioId)
{
    tMacAddr            StationMacAddr;
    tMacAddr            NextStationMacAddr;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    INT4                i4RadioIndex = 0;
    UINT4               u4LastReport = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetValFsRrm11hDfsStatus = 0;
    UINT4               u4RetValFsRrm11hDfsInterval = 0;
    UINT4               u4RetValFsRrm11hDFSQuietInterval = 0;
    UINT4               u4RetValFsRrm11hDFSQuietPeriod = 0;
    UINT4               u4RetValFsRrm11hDFSMeasurementInterval = 0;
    INT4                u4RetValFsRrm11hDFSChannelSwitchStatus = 0;
    INT4                i4RadioType = CLI_RADIO_TYPEA;
    CHR1                au1Time[RFMGMT_MAX_DATE_LEN];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    UNUSED_PARAM (u4LastReport);

    MEMSET (&StationMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextStationMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1String, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (au1Time, 0, RFMGMT_MAX_DATE_LEN);
    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n No row found\r\n");
        return CLI_FAILURE;
    }

    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Not Found");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId, &i4RadioIndex) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nradio ifIndex error");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsRrm11hDfsStatus (i4RadioType, &i4RetValFsRrm11hDfsStatus))
    {
        CliPrintf (CliHandle, "\r\nDFS Status                 : %d\n",
                   i4RetValFsRrm11hDfsStatus);
    }
    if (nmhGetFsRrm11hDfsInterval (i4RadioType, &u4RetValFsRrm11hDfsInterval))
    {
        CliPrintf (CliHandle, "\r\nDFS Interval               : %d\n",
                   u4RetValFsRrm11hDfsInterval);
    }
    if (nmhGetFsRrm11hDFSQuietInterval
        (i4RadioIndex, &u4RetValFsRrm11hDFSQuietInterval))
    {
        CliPrintf (CliHandle, "\r\nDFS Quiet Interval         : %d\n",
                   u4RetValFsRrm11hDFSQuietInterval);
    }
    if (nmhGetFsRrm11hDFSQuietPeriod
        (i4RadioIndex, &u4RetValFsRrm11hDFSQuietPeriod))
    {
        CliPrintf (CliHandle, "\r\nDFS Quiet Period           : %d\n",
                   u4RetValFsRrm11hDFSQuietPeriod);
    }
    if (nmhGetFsRrm11hDFSMeasurementInterval
        (i4RadioIndex, &u4RetValFsRrm11hDFSMeasurementInterval))
    {
        CliPrintf (CliHandle, "\r\nDFS Measurement Interval   : %d\n",
                   u4RetValFsRrm11hDFSMeasurementInterval);
    }
    if (nmhGetFsRrm11hDFSChannelSwitchStatus
        (i4RadioIndex, &u4RetValFsRrm11hDFSChannelSwitchStatus))
    {
        CliPrintf (CliHandle, "\r\nDFS Channel Switch Status  : %d\n",
                   u4RetValFsRrm11hDFSChannelSwitchStatus);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hDfsInterval                                    */
/*                                                                           */
/* Description  : Used to set the Dfs Interval.                              */
/*                                                                           */
/* Input        : RadioType, DfsInterval.                                    */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                       */
/*****************************************************************************/

INT1
RfMgmtCli11hDfsInterval (tCliHandle CliHandle, INT4 i4RadioType,
                         UINT4 u4DfsInterval)
{

    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrm11hDfsInterval (&u4ErrCode, i4RadioType,
                                      u4DfsInterval) != SNMP_FAILURE)
    {
        nmhSetFsRrm11hDfsInterval (i4RadioType, u4DfsInterval);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliShowChannelTrapStatus                             */
/*                                                                           */
/* Description  : Used to show Trap Status of channel Change.                */
/*                                                                           */
/* Input        : channel trap status                                        */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliShowChannelTrapStatus (tCliHandle CliHandle)
{
    INT4                i4ChannelTrapStatus = 0;

    nmhGetFsRrmChannelChangeTrapStatus (&i4ChannelTrapStatus);
    if (i4ChannelTrapStatus == RFMGMT_CHANNEL_CHANGE_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "Channel change trap    :   ENABLE\r\n");
    }
    if (i4ChannelTrapStatus == RFMGMT_CHANNEL_CHANGE_TRAP_DISABLE)
    {
        CliPrintf (CliHandle, "Channel change trap    :   DISABLE\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShowAdvancedChannel                                  */
/*                                                                           */
/* Description  : This function will display the advanced channel related    */
/*                configurations                                             */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                pu1ApProfileName - AP Profile Name                         */
/*                u4RadioId - u4RadioId                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowAdvancedChannel (tCliHandle CliHandle, INT4 i4GetRadioType)
{
    INT4                i4RowSatus = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelection = 0;
    INT4                i4DcaSensitivity = 0;
    INT4                i4RssiThreshold = 0;
    INT4                i4ConsiderExtAPStatus = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4LastUpdatedTime = 0;
    UINT4               u4ClientThreshold = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4DcaUpdatedTime = 0;
    UINT4               u4NeighborCountThreshold = 0;
    UINT1               u1Channel = 0;
    UINT1               u1count = 0;
    UINT1               u1Index = 0;
    UINT1               au1AllowedList[RFMGMT_MAX_CHANNELA];
    UINT1               au1UnusedList[RFMGMT_MAX_CHANNELA];
    UINT1               au1TempAused[RFMGMT_MAX_CHANNELA];
    UINT1               au1TempBused[RFMGMT_MAX_CHANNELA];
    UINT1               au1TempAunused[RFMGMT_MAX_CHANNELA];
    UINT1               au1TempBunused[RFMGMT_MAX_CHANNELA];
    tSNMP_OCTET_STRING_TYPE AllowedList;
    tSNMP_OCTET_STRING_TYPE UnusedList;

    MEMSET (au1AllowedList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1UnusedList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1TempAused, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1TempBused, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1TempAunused, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1TempBunused, 0, RFMGMT_MAX_CHANNELA);

    MEMSET (&UnusedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AllowedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    AllowedList.pu1_OctetList = au1AllowedList;
    UnusedList.pu1_OctetList = au1UnusedList;
    /* Validate the input */
    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nNo entry found\n");
        return CLI_FAILURE;
    }

    nmhGetFsRrmDcaMode (i4GetRadioType, &i4DcaMode);
    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4DcaSelection);
        if (i4DcaSelection == RFMGMT_DCA_SELECTION_OFF)
        {
            CliPrintf (CliHandle,
                       "\r\nChannel Assignment Mode          : STATIC\n");
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nChannel Assignment Mode          : AUTO");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\nChannel Assignment Mode          : PER-AP");
    }
    nmhGetFsRrmDcaInterval (i4GetRadioType, &u4DcaInterval);
    nmhGetFsRrmDcaSensitivity (i4GetRadioType, &i4DcaSensitivity);
    nmhGetFsRrmLastUpdatedTime (i4GetRadioType, &u4LastUpdatedTime);
    nmhGetFsRrmAllowedChannels (i4GetRadioType, &AllowedList);
    if (nmhGetFsRrmUnusedChannels (i4GetRadioType, &UnusedList) != SNMP_SUCCESS)
    {
    }
    nmhGetFsRrmRSSIThreshold (i4GetRadioType, &i4RssiThreshold);
    nmhGetFsRrmClientThreshold (i4GetRadioType, &u4ClientThreshold);
    nmhGetFsRrmConsiderExternalAPs (i4GetRadioType, &i4ConsiderExtAPStatus);
    nmhGetFsRrmNeighborCountThreshold (i4GetRadioType,
                                       &u4NeighborCountThreshold);

    if (u4LastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4DcaUpdatedTime = u4CurrentTime - u4LastUpdatedTime;
    }
    CliPrintf (CliHandle, "\r\nChannel Updated Interval         : %u sec",
               u4DcaInterval);

    if (u4LastUpdatedTime != 0)
    {
        CliPrintf (CliHandle,
                   "\r\nDCA Last Run                     : %u sec ago",
                   u4DcaUpdatedTime);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nDCA Last Run                     : Not Executed");
    }
    CliPrintf (CliHandle, "\r\nClient Threshold                 : %u",
               u4ClientThreshold);

    if (i4DcaSensitivity == RFMGMT_DCA_SENSITIVITY_LOW)
    {
        CliPrintf (CliHandle, "\r\nDCA Sensitivity Level            : Low");
    }

    if (i4DcaSensitivity == RFMGMT_DCA_SENSITIVITY_MEDIUM)
    {
        CliPrintf (CliHandle, "\r\nDCA Sensitivity Level            : Medium");
    }

    if (i4DcaSensitivity == RFMGMT_DCA_SENSITIVITY_HIGH)
    {
        CliPrintf (CliHandle, "\r\nDCA Sensitivity Level            : High");
    }
    if (i4ConsiderExtAPStatus == RFMGMT_CONSIDER_EXTERNAL_APS)
    {
        CliPrintf (CliHandle, "\r\nExternal AP Consideration        : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nExternal AP Consideration        : Disabled");
    }
    CliPrintf (CliHandle, "\r\nNeighbor Count Threshold         : %d",
               u4NeighborCountThreshold);

    if (i4GetRadioType == CLI_RADIO_TYPEA)
    {
        CliPrintf (CliHandle, "\r\nAuto-RF Allowed Channel List     : ");
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
        {
            if (au1AllowedList[u1Channel] != 0x0)
            {
                au1TempAused[u1count] = au1AllowedList[u1Channel];
                u1count++;
            }
        }
        for (u1Index = 0; u1Index < u1count; u1Index++)
        {
            if (au1TempAused[u1Index + 1] != 0)
            {
                CliPrintf (CliHandle, "%d, ", au1TempAused[u1Index]);
            }
            else
            {
                CliPrintf (CliHandle, "%d", au1TempAused[u1Index]);
            }
        }
        u1count = 0;
        CliPrintf (CliHandle, "\r\n\nAuto-RF Unused Channel List      : ");
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
        {
            if (au1UnusedList[u1Channel] != 0x0)
            {
                au1TempAunused[u1count] = au1UnusedList[u1Channel];
                u1count++;
            }
        }
        for (u1Index = 0; u1Index < u1count; u1Index++)
        {
            if (au1TempAunused[u1Index + 1] != 0)
            {
                CliPrintf (CliHandle, "%d, ", au1TempAunused[u1Index]);
            }
            else
            {
                CliPrintf (CliHandle, "%d", au1TempAunused[u1Index]);
            }
        }

    }
    else
    {
        CliPrintf (CliHandle, "\r\nAuto-RF Allowed Channel List     : ");
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
        {
            if (au1AllowedList[u1Channel] != 0x0)
            {
                au1TempBused[u1count] = au1AllowedList[u1Channel];
                u1count++;
            }
        }
        for (u1Index = 0; u1Index < u1count; u1Index++)
        {
            if (au1TempBused[u1Index + 1] != 0)
            {
                CliPrintf (CliHandle, "%d, ", au1TempBused[u1Index]);
            }
            else
            {
                CliPrintf (CliHandle, "%d", au1TempBused[u1Index]);
            }
        }
        u1count = 0;
        CliPrintf (CliHandle, "\r\n\nAuto-RF Unused Channel List      : ");
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
        {
            if (au1UnusedList[u1Channel] != 0x0)
            {
                au1TempBunused[u1count] = au1UnusedList[u1Channel];
                u1count++;
            }
        }
        for (u1Index = 0; u1Index < u1count; u1Index++)
        {
            if (au1TempBunused[u1Index + 1] != 0)
            {
                CliPrintf (CliHandle, "%d, ", au1TempBunused[u1Index]);
            }
            else
            {
                CliPrintf (CliHandle, "%d", au1TempBunused[u1Index]);
            }
        }
    }
    CliPrintf (CliHandle, "\r\nRSSI Threshold                     : %d dbm\r\n",
               i4RssiThreshold);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShowAdvancedSha                                      */
/*                                                                           */
/* Description  : This function will display the self healing algo related   */
/*                configurations                                             */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowAdvancedSha (tCliHandle CliHandle, INT4 i4GetRadioType)
{
    INT4                i4RowSatus = 0;
    INT4                i4SHAStatus = 0;
    UINT4               u4SHAInterval = 0;
    UINT4               u4SHAExecutionCount = 0;

    /* Validate the input */
    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nNo entry found\n");
        return CLI_FAILURE;
    }

    nmhGetFsRrmSHAInterval (i4GetRadioType, &u4SHAInterval);
    nmhGetFsRrmSHAStatus (i4GetRadioType, &i4SHAStatus);
    nmhGetFsRrmSHAExecutionCount (i4GetRadioType, &u4SHAExecutionCount);

    if (i4SHAStatus == RFMGMT_SHA_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n SHA Status                      : ENABLE");
        CliPrintf (CliHandle, "\r\n SHA Interval                    : %d secs",
                   u4SHAInterval);
        CliPrintf (CliHandle, "\r\n SHA Execution Count             : %d\r\n",
                   u4SHAExecutionCount);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n SHA Status                      : DISABLE");
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShowAdvancedCoverage                                 */
/*                                                                           */
/* Description  : This function will display the advanced power related      */
/*                configurations                                             */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                CliHandle                                                  */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowAdvancedCoverage (tCliHandle CliHandle, INT4 i4GetRadioType)
{
    INT4                i4RowSatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4TpcLastUpdatedTime = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4TpcUpdatedTime = 0;
    INT4                i4SNRThreshold = 0;

    /* Validate the input */
    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nNo entry found\n");
        return CLI_FAILURE;
    }

    nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcMode);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
    nmhGetFsRrmTpcLastUpdatedTime (i4GetRadioType, &u4TpcLastUpdatedTime);
    nmhGetFsRrmSNRThreshold (i4GetRadioType, &i4SNRThreshold);

    if (u4TpcLastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4TpcUpdatedTime = u4CurrentTime - u4TpcLastUpdatedTime;
    }
    if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
    {
        if (i4TpcSelection == RFMGMT_TPC_SELECTION_OFF)
        {
            CliPrintf (CliHandle,
                       "\r\nTransmit Power Control Mode          : OFF\n");
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTransmit Power Control Mode           : AUTO");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\nTransmit Power Control Mode : PER-AP\n");
    }
    CliPrintf (CliHandle, "\r\nTranmsit Power Change Interval         : %u sec",
               u4TpcInterval);

    if (u4TpcLastUpdatedTime != 0)
    {
        CliPrintf (CliHandle,
                   "\r\nTPC Last Run                           : %u sec ago",
                   u4TpcUpdatedTime);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nTPC Last Run                           : Not Executed");
    }
    CliPrintf (CliHandle,
               "\r\nSNR Threshold                           : %d dbm\r\n",
               i4SNRThreshold);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShowAdvancedTxPower                                  */
/*                                                                           */
/* Description  : This function will display the Tx Power related            */
/*                configurations                                             */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                CliHandle                                                  */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowAdvancedTxPower (tCliHandle CliHandle, INT4 i4GetRadioType)
{
    INT4                i4RowSatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    INT4                i4PowerThreshold = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4DpaExecutionCount = 0;

    /* Validate the input */
    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nNo entry found\n");
        return CLI_FAILURE;
    }

    nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcMode);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
    nmhGetFsRrmDpaExecutionCount (i4GetRadioType, &u4DpaExecutionCount);
    nmhGetFsRrmPowerThreshold (i4GetRadioType, &i4PowerThreshold);

    if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
    {
        if (i4TpcSelection == RFMGMT_TPC_SELECTION_OFF)
        {
            CliPrintf (CliHandle,
                       "\r\nDynamic Power Adjustment Mode          : OFF\n");
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nDynamic Power Adjustment Mode          : AUTO");
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nDynamic Power Adjustment Mode          : OFF\n");
    }
    CliPrintf (CliHandle, "\r\nDynamic Power Adjustment Interval      : %u",
               u4TpcInterval);
    CliPrintf (CliHandle, "\r\nDPA Execution Count                    : %u",
               u4DpaExecutionCount);
    CliPrintf (CliHandle,
               "\r\nPower Threshold                        : %dbm\r\n",
               i4PowerThreshold);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliTpcStats.                                         */
/*                                                                           */
/* Description  : Used to display the Tpc Statistics of the Ap.              */
/*                                                                           */
/* Input        : RadioType, Profile Name, Pointer to radio id.              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliTpcStats (tCliHandle CliHandle, INT4 i4RadioType,
                   UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4SNRScanFrequency = 0;
    UINT4               u4SNRScanCount = 0;
    UINT4               u4BelowSNRThresholdCount = 0;
    UINT4               u4TxPowerChangeCount = 0;
    UINT4               u4ClientsAccepted = 0;
    UINT4               u4ClientsDiscarded = 0;
    UINT4               u4SNRScanStatus = 0;
    UINT4               u4TpcMode = 0;
    UINT4               u4RetValFsRrmTxPowerDecreaseCount = 0;
    UINT4               u4RetValFsRrmTxPowerIncreaseCount = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelectionMode = 0;

    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If AP name is provided and not the radio id, the statistics of all the 
     * radio id is displayed.  */
    CliPrintf (CliHandle, "\r\nAP Profile Name             : %s",
               pu1ProfileName);
    if (pu4RadioId == NULL)
    {

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4WtpProfileId == u4currentProfileId)
            {

                if (RfMgmtGetDot11RadioIfIndex
                    (u4currentProfileId, u4currentBindingId, i4RadioType,
                     &i4RadioIfIndex) == OSIX_SUCCESS)
                {

                    CliPrintf (CliHandle,
                               "\r\nRadio id                    : %d",
                               u4currentBindingId);
                    nmhGetFsRrmAPSNRScanStatus (i4RadioIfIndex,
                                                (INT4 *) (&u4SNRScanStatus));
                    nmhGetFsRrmAPTpcMode (i4RadioIfIndex,
                                          (INT4 *) (&u4TpcMode));
                    nmhGetFsRrmSNRScanFreq (i4RadioIfIndex,
                                            &u4SNRScanFrequency);
                    nmhGetFsRrmSNRScanCount (i4RadioIfIndex, &u4SNRScanCount);
                    nmhGetFsRrmBelowSNRThresholdCount (i4RadioIfIndex,
                                                       &u4BelowSNRThresholdCount);
                    nmhGetFsRrmTxPowerChangeCount (i4RadioIfIndex,
                                                   &u4TxPowerChangeCount);
                    nmhGetFsRrmClientsAccepted (i4RadioIfIndex,
                                                &u4ClientsAccepted);
                    nmhGetFsRrmClientsDiscarded (i4RadioIfIndex,
                                                 &u4ClientsDiscarded);
                    nmhGetFsRrmTpcMode (i4RadioType, &i4TpcMode);
                    nmhGetFsRrmTpcSelectionMode (i4RadioType,
                                                 &i4TpcSelectionMode);
                    nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                     &u4RetValFsRrmTxPowerDecreaseCount);

                    nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                     &u4RetValFsRrmTxPowerIncreaseCount);

                    if (u4SNRScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_ENABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSNR Scan Status             : ENABLE");
                    }
                    if (u4SNRScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_DISABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSNR Scan Status             : DISABLE");
                    }
                    if (((i4TpcMode == CLI_TPC_MODE_GLOBAL) &&
                         (i4TpcSelectionMode != CLI_TPC_SELECTION_OFF)) ||
                        (i4TpcMode == CLI_DCA_MODE_PER_AP))
                    {
                        if ((i4TpcMode == CLI_TPC_MODE_GLOBAL) ||
                            (u4TpcMode == CLI_TPC_PER_AP_GLOBAL))
                        {
                            CliPrintf (CliHandle,
                                       "\r\nAP TPC Mode                 : GLOBAL");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nAP TPC Mode                 : MANUAL");
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAP TPC Mode                 : MANUAL");
                    }

                    CliPrintf (CliHandle,
                               "\r\nSNR Scan Frequency          : %u",
                               u4SNRScanFrequency);
                    CliPrintf (CliHandle,
                               "\r\nSNR Scan Count              : %u",
                               u4SNRScanCount);
                    CliPrintf (CliHandle,
                               "\r\nBelow SNR Threshold Count   : %u",
                               u4BelowSNRThresholdCount);
                    CliPrintf (CliHandle,
                               "\r\nTx Power Change Count       : %u",
                               u4TxPowerChangeCount);
                    CliPrintf (CliHandle,
                               "\r\nClients Accepted            : %u",
                               u4ClientsAccepted);
                    CliPrintf (CliHandle,
                               "\r\nClients Discarded           : %u",
                               u4ClientsDiscarded);
                    CliPrintf (CliHandle,
                               "\r\nTx Power Increase Count     : %u",
                               u4RetValFsRrmTxPowerIncreaseCount);
                    CliPrintf (CliHandle,
                               "\r\nTx Power Decrease Count     : %u\r\n",
                               u4RetValFsRrmTxPowerDecreaseCount);

                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    /*Only for the particular radio id the tpc statistics is displayed */
    else
    {
        u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                        i4RadioType,
                                        &i4RadioIfIndex) == OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nRadio id                    : %d",
                       u4currentBindingId);
            nmhGetFsRrmAPSNRScanStatus (i4RadioIfIndex,
                                        (INT4 *) (&u4SNRScanStatus));
            nmhGetFsRrmAPTpcMode (i4RadioIfIndex, (INT4 *) (&u4TpcMode));
            nmhGetFsRrmSNRScanFreq (i4RadioIfIndex, &u4SNRScanFrequency);
            nmhGetFsRrmSNRScanCount (i4RadioIfIndex, &u4SNRScanCount);
            nmhGetFsRrmBelowSNRThresholdCount (i4RadioIfIndex,
                                               &u4BelowSNRThresholdCount);
            nmhGetFsRrmTxPowerChangeCount (i4RadioIfIndex,
                                           &u4TxPowerChangeCount);
            nmhGetFsRrmClientsAccepted (i4RadioIfIndex, &u4ClientsAccepted);
            nmhGetFsRrmClientsDiscarded (i4RadioIfIndex, &u4ClientsDiscarded);
            nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                             &u4RetValFsRrmTxPowerDecreaseCount);
            nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                             &u4RetValFsRrmTxPowerIncreaseCount);

            if (u4SNRScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_ENABLE)
            {
                CliPrintf (CliHandle,
                           "\r\nSNR Scan Status             : ENABLE");
            }
            if (u4SNRScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_DISABLE)
            {
                CliPrintf (CliHandle,
                           "\r\nSNR Scan Status             : DISABLE");
            }
            if (u4TpcMode == CLI_TPC_PER_AP_GLOBAL)
            {
                CliPrintf (CliHandle,
                           "\r\nPer AP TPC Mode             : GLOBAL");
            }
            if (u4TpcMode != CLI_TPC_PER_AP_GLOBAL)
            {
                CliPrintf (CliHandle,
                           "\r\nPer AP TPC Mode             : MANUAL");
            }
            CliPrintf (CliHandle, "\r\nSNR Scan Frequency          : %u",
                       u4SNRScanFrequency);
            CliPrintf (CliHandle, "\r\nSNR Scan Count              : %u",
                       u4SNRScanCount);
            CliPrintf (CliHandle, "\r\nBelow SNR Threshold Count   : %u",
                       u4BelowSNRThresholdCount);
            CliPrintf (CliHandle, "\r\nTx Power Change Count       : %u",
                       u4TxPowerChangeCount);
            CliPrintf (CliHandle, "\r\nClients Accepted            : %u",
                       u4ClientsAccepted);
            CliPrintf (CliHandle, "\r\nClients Discarded           : %u",
                       u4ClientsDiscarded);
            CliPrintf (CliHandle, "\r\nTx Power Increase Count     : %u",
                       u4RetValFsRrmTxPowerIncreaseCount);
            CliPrintf (CliHandle, "\r\nTx Power Decrease Count     : %u\r\n",
                       u4RetValFsRrmTxPowerDecreaseCount);

        }
        else
        {
            CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                       "function fetch failed\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtCliNeighborAPConfig                                  */
/*                                                                           */
/* Description  : Used to display the Dca Configurations of the AP.          */
/*                                                                           */
/* Input        : RadioType, Profile Name, Pointer to radio id.              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliNeighborAPConfig (tCliHandle CliHandle, INT4 i4RadioType,
                           UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NeighborScanFreq = 0;
    UINT4               u4ChannelScanDuration = 0;
    UINT4               u4NeighborAgingPeriod = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4APAutoScanStatus = 0;
    UINT1               u1RadioId = 0;
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    tMacAddr            APMacAddress;
    INT4                i4APChannelMode = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;

    MEMSET (&APMacAddress, 0, sizeof (tMacAddr));
    MEMSET (&au1String, 0, CLI_MAC_TO_STR_LEN);

    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If AP name is provided and not the radio id, the statistics of all the 
     * radio id is displayed.  */
    CliPrintf (CliHandle, "\r\n AP Profile Name                          : %s",
               pu1ProfileName);
    if (pu4RadioId == NULL)
    {

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4WtpProfileId == u4currentProfileId)
            {
                if (RfMgmtGetDot11RadioIfIndex (u4currentProfileId,
                                                u4currentBindingId, i4RadioType,
                                                &i4RadioIfIndex) ==
                    OSIX_SUCCESS)
                {

                    CliPrintf (CliHandle,
                               "\r\n Radio id                                 : %d",
                               u4currentBindingId);
                    nmhGetFsRrmAPAutoScanStatus (i4RadioIfIndex,
                                                 &i4APAutoScanStatus);
                    nmhGetFsRrmAPNeighborScanFreq (i4RadioIfIndex,
                                                   &u4NeighborScanFreq);
                    nmhGetFsRrmAPChannelScanDuration (i4RadioIfIndex,
                                                      &u4ChannelScanDuration);
                    nmhGetFsRrmAPNeighborAgingPeriod (i4RadioIfIndex,
                                                      &u4NeighborAgingPeriod);
                    nmhGetFsRrmAPMacAddress (i4RadioIfIndex, &APMacAddress);
                    nmhGetFsRrmAPChannelMode (i4RadioIfIndex, &i4APChannelMode);
                    nmhGetFsRrmDcaMode (i4RadioType, &i4DcaMode);
                    nmhGetFsRrmDcaSelectionMode (i4RadioType,
                                                 &i4DcaSelectionMode);
                    if (((i4DcaMode == CLI_DCA_MODE_GLOBAL)
                         && (i4DcaSelectionMode != CLI_DCA_SELECTION_OFF))
                        || (i4DcaMode == CLI_DCA_MODE_PER_AP))
                    {
                        if ((i4DcaMode == CLI_DCA_MODE_GLOBAL) ||
                            (i4APChannelMode == CLI_DCA_PER_AP_GLOBAL))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n AP Channel mode\t\t          : GLOBAL");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n AP Channel mode\t\t          : MANUAL");
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n AP Channel mode\t\t          : MANUAL");
                    }
                    if (i4APAutoScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_ENABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n AP Auto Scan Status\t                  : ENABLE");
                    }
                    if (i4APAutoScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_DISABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n AP Auto Scan Status\t                  : DISABLE");
                    }
                    CliPrintf (CliHandle,
                               "\r\n Database share frequency                 : %u secs",
                               u4NeighborScanFreq);
                    CliPrintf (CliHandle,
                               "\r\n Channel scan frequency                   : %u secs",
                               u4ChannelScanDuration);
                    CliPrintf (CliHandle,
                               "\r\n Neighbor Aging Period                    : %u secs",
                               u4NeighborAgingPeriod);
                    CliMacToStr (APMacAddress, au1String);
                    CliPrintf (CliHandle,
                               "\r\n AP Mac Address                           : %s\r\n",
                               au1String);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    /*Only for the particular radio id of the AP configurations is displayed. */
    else
    {
        u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, *pu4RadioId,
                                        i4RadioType,
                                        &i4RadioIfIndex) == OSIX_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n Radio id                                 : %d",
                       u1RadioId);
            nmhGetFsRrmAPAutoScanStatus (i4RadioIfIndex, &i4APAutoScanStatus);
            nmhGetFsRrmAPNeighborScanFreq (i4RadioIfIndex, &u4NeighborScanFreq);
            nmhGetFsRrmAPChannelScanDuration (i4RadioIfIndex,
                                              &u4ChannelScanDuration);
            nmhGetFsRrmAPNeighborAgingPeriod (i4RadioIfIndex,
                                              &u4NeighborAgingPeriod);
            nmhGetFsRrmAPMacAddress (i4RadioIfIndex, &APMacAddress);
            nmhGetFsRrmAPChannelMode (i4RadioIfIndex, &i4APChannelMode);
            nmhGetFsRrmDcaMode (i4RadioType, &i4DcaMode);
            nmhGetFsRrmDcaSelectionMode (i4RadioType, &i4DcaSelectionMode);
            if (((i4DcaMode == CLI_DCA_MODE_GLOBAL) &&
                 (i4DcaSelectionMode != CLI_DCA_SELECTION_OFF)) ||
                (i4DcaMode == CLI_DCA_MODE_PER_AP))
            {
                if (i4APChannelMode == CLI_DCA_PER_AP_GLOBAL)
                {
                    CliPrintf (CliHandle,
                               "\r\n AP channel mode\t\t          : GLOBAL");
                }
                if (i4APChannelMode == CLI_DCA_PER_AP_MANUAL)
                {
                    CliPrintf (CliHandle,
                               "\r\n AP channel mode\t\t        : MANUAL");
                }
            }
            CliMacToStr (APMacAddress, au1String);
            if (i4APAutoScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_ENABLE)
            {
                CliPrintf (CliHandle,
                           "\r\n AP Auto Scan Status                      : ENABLE");
            }
            if (i4APAutoScanStatus == CLI_RFMGMT_SNR_AUTO_SCAN_DISABLE)
            {
                CliPrintf (CliHandle,
                           "\r\n AP Auto Scan Status                      : DISABLE");
            }
            CliPrintf (CliHandle,
                       "\r\n Database share frequency                 : %u secs",
                       u4NeighborScanFreq);
            CliPrintf (CliHandle,
                       "\r\n Channel scan frequency                   : %u secs",
                       u4ChannelScanDuration);
            CliPrintf (CliHandle,
                       "\r\n Neighbor Aging Period                    : %u secs",
                       u4NeighborAgingPeriod);
            CliPrintf (CliHandle,
                       "\r\n AP Mac Address                           : %s\r\n",
                       au1String);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                       "function fetch failed\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliShowChannelSwitchStatus                           */
/*                                                                           */
/* Description  : Used to display the channel switch status of the AP.       */
/*                                                                           */
/* Input        : NONE                                                       */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliShowChannelSwitchStatus (tCliHandle CliHandle)
{
    INT4                i4ChannelSwitchStatus = 0;
    nmhGetFsRrmChannelSwitchMsgStatus (&i4ChannelSwitchStatus);

    if (i4ChannelSwitchStatus == CLI_RFMGMT_CHANNEL_SWITCH_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n Channel switch status    :   ENABLE\r\n");
    }
    if (i4ChannelSwitchStatus == CLI_RFMGMT_CHANNEL_SWITCH_DISABLE)
    {
        CliPrintf (CliHandle, "\r\n Channel switch status    :   DISABLE\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliShowBssidSNRScanStatus                            */
/*                                                                           */
/* Description  : Used to show the SNR Scan status of particular AP          */
/*                                                                           */
/* Input        : RadioType, SNR Threshold, pointer to profile name,         */
/*                pointer to radio id.                                       */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCliShowBssidSNRScanStatus (tCliHandle CliHandle, INT4 i4RadioType,
                                 UINT1 *pu1ProfileName, UINT4 u4RadioId,
                                 UINT4 u4WlanId)
{
    UINT4               u4WtpProfileId = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4SsidScanStatus = 0;

    if (pu1ProfileName == NULL)
    {
        CliPrintf (CliHandle, "Invalid input\n\n");
        return CLI_FAILURE;
    }
    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u4RadioId,
                                    i4RadioType,
                                    &i4RadioIfIndex) == OSIX_SUCCESS)
    {
        nmhGetFsRrmSSIDScanStatus (i4RadioIfIndex, u4WlanId, &i4SsidScanStatus);
        if (i4SsidScanStatus == CLI_RFMGMT_SSID_SCAN_ENABLE)
        {
            CliPrintf (CliHandle, "\r\n SSID scanstatus   :  ENABLE\r\n");
        }
        if (i4SsidScanStatus == CLI_RFMGMT_SSID_SCAN_DISABLE)
        {
            CliPrintf (CliHandle, "\r\n SSID scanstatus   :  DISABLE\r\n");
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  RfMgmtGetDot11RadioIfIndex
 * Description :  This function is utility function to give the radio index
 *        for the and WTP Profile Id, WTP Binding ID. This function
 *       also checks the matching of the radio type
 *
 * Input       :  u4WtpProfileId - WTP  Profile ID
 *                u4WtpBindingId - WTP  Binding ID
 *                i4RadioType    - Radio Type
 *
 * Output      :  pi4RadioIfIndex -  Radio Index
 *
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
RfMgmtGetDot11RadioIfIndex (UINT4 u4WtpProfileId, UINT4 u4WtpBindingId,
                            INT4 i4RadioType, INT4 *pi4RadioIfIndex)
{
    INT4                i4RadioIfIndex = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;
    UINT4               u4GetRadioType = 0;
    INT1                i1RetStatus = 0;

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex (u4WtpProfileId,
                                                            u4WtpBindingId,
                                                            &i4RadioIfIndex) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    *pi4RadioIfIndex = i4RadioIfIndex;

    if ((i1RetStatus = nmhGetFsDot11RadioType (i4RadioIfIndex,
                                               &u4GetRadioType)) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
    {
        return OSIX_FAILURE;
    }

    return i4RetStatus;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmgmtShowRunningConfig                                */
/*                                                                           */
/*  Description     : This function displays current RFMGMT configurations   */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RfmgmtShowRunningConfig (tCliHandle CliHandle)
{
    if (RfmgmtSrcScalars (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (RfmShowRunningRrmConfigTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (RfmShowRunningRrmAPConfigTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (RfmShowRunningRrmTpcConfigTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (RfmShowRunningRrmTpcScanTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (RfmShowRunningRrmExtConfigTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
#ifdef ROGUEAP_WANTED
    if (RfmgmtShowRunningRogueApScalars (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (RfmShowRunningRogueRuleConfigTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (RfmShowRunningRogueManualConfigTable (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
#endif
    return (CLI_SUCCESS);
}

#ifdef ROGUEAP_WANTED
/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmgmtShowRunningRogueApScalars                        */
/*                                                                           */
/*  Description     : This function displays Rogue Ap scalar configurations  */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RfmgmtShowRunningRogueApScalars (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE RfName;
    UINT1               au1RogueRfName[WSSMAC_MAX_SSID_LEN];
    INT4                i4RogueApDetecttion = 0, i4TrapStatus = 0;
    UINT4               u4RogueApTimeout = 0;

    MEMSET (&au1RogueRfName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = au1RogueRfName;

    nmhGetFsRfGroupName (&RfName);
    if (strlen ((const char *) au1RogueRfName) != 0)
    {
        CliPrintf (CliHandle, "config rf-name %s\r\n", RfName.pu1_OctetList);
    }

    nmhGetFsRogueApDetecttion (&i4RogueApDetecttion);
    if (i4RogueApDetecttion == RFMGMT_ROGUE_ENABLE)
    {
        CliPrintf (CliHandle, "config rogue-detection enable\r\n");
    }

    nmhGetFsRogueApTimeout (&u4RogueApTimeout);
    if (u4RogueApTimeout != RFMGMT_ROGUE_TIMEOUT_MIN_VALUE)
    {
        CliPrintf (CliHandle, "config rogue ap time-out %d\r\n",
                   u4RogueApTimeout);
    }
    nmhGetFsRogueApMaliciousTrapStatus (&i4TrapStatus);
    if (i4TrapStatus == RFMGMT_ROGUE_ENABLE)
    {
        CliPrintf (CliHandle, "rogue ap trap enable\r\n");
    }
    CliPrintf (CliHandle, "!\n!\r\n");
    return CLI_SUCCESS;

}
#endif
/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmgmtSrcScalars                                       */
/*                                                                           */
/*  Description     : This function displays rfmgmt scalar configurations    */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RfmgmtSrcScalars (tCliHandle CliHandle)
{

    INT4                i4DebugMask = 0;
    INT4                i4RrmChannelSwitchMsgStatus = 0;
    INT4                i4RrmChannelChangeTrapStatus = 0;
    INT4                i4RrmTxPowerChangeTrapStatus = 0;

    nmhGetFsRrmDebugOption (&i4DebugMask);

    if ((i4DebugMask & RFMGMT_ENTRY_TRC) == RFMGMT_ENTRY_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt entry\r\n");
    }
    if ((i4DebugMask & RFMGMT_EXIT_TRC) == RFMGMT_EXIT_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt exit\r\n");
    }
    if ((i4DebugMask & RFMGMT_DCA_TRC) == RFMGMT_DCA_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt dca\r\n");
    }
    if ((i4DebugMask & RFMGMT_TPC_TRC) == RFMGMT_TPC_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt tpc\r\n");
    }
    if ((i4DebugMask & RFMGMT_MGMT_TRC) == RFMGMT_MGMT_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt mgmt\r\n");
    }
    if ((i4DebugMask & RFMGMT_FAILURE_TRC) == RFMGMT_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt fail\r\n");
    }
    if ((i4DebugMask & RFMGMT_INIT_TRC) == RFMGMT_INIT_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt init\r\n");
    }
    if ((i4DebugMask & RFMGMT_INFO_TRC) == RFMGMT_INFO_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt info\r\n");
    }
    if ((i4DebugMask & RFMGMT_CRITICAL_TRC) == RFMGMT_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt critical\r\n");
    }
    if ((i4DebugMask & RFMGMT_ALL_TRC) == RFMGMT_ALL_TRC)
    {
        CliPrintf (CliHandle, "debug rfmgmt all\r\n");
    }
    /*RrmChannelSwitchMsgStatus */
    nmhGetFsRrmChannelSwitchMsgStatus (&i4RrmChannelSwitchMsgStatus);
    if (i4RrmChannelSwitchMsgStatus != RFMGMT_CHANNEL_SWITCH_MSG_ENABLE)
    {
        CliPrintf (CliHandle, "advanced dot11h channel-switch disable\r\n");
    }
    /*fsRrmChannelChangeTrapStatus */
    nmhGetFsRrmChannelChangeTrapStatus (&i4RrmChannelChangeTrapStatus);
    if (i4RrmChannelChangeTrapStatus == RFMGMT_CHANNEL_CHANGE_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "channel change trap enable\r\n");
    }

    /*fsFsRrmTxPowerChangeTrapStatus */
    nmhGetFsRrmTxPowerChangeTrapStatus (&i4RrmTxPowerChangeTrapStatus);
    if (i4RrmTxPowerChangeTrapStatus == RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "tx-power change trap enable\r\n");
    }

    CliPrintf (CliHandle, "!\n!\r\n");
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRrmConfigTable                           */
/*                                                                           */
/*  Description     : This function displays RrmConfigTable configurations   */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRrmConfigTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE WtpOctetProfString = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE RrmAllowedChannels;
    tSNMP_OCTET_STRING_TYPE RrmUnusedChannels;
    INT4                i4FsRrmRadioType = 0;
    INT4                i4PreFsRrmRadioType = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelection = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    INT4                i4FsRrmTpcUpdate = 0;
    UINT4               u4RrmDcaInterval = 0;
    INT4                i4RrmDcaSensitivity = 0;
    INT4                i4RrmUpdateChannel = 0;
    INT4                i4RrmRSSIThreshold = 0;
    UINT4               u4RrmClientThreshold = 0;
    UINT4               u4RrmTpcInterval = 0;
    UINT4               u4RrmSHAInterval = 0;
/*  UINT4        u4Rrm11hDfsInterval = 0;*/
    INT4                i4RrmSNRThreshold = 0;
    INT4                i4Status = 0;
    UINT1               au1RrmAllowedChannels[RFMGMT_MAX_CHANNEL];
    UINT1               au1RrmUnusedChannels[RFMGMT_MAX_CHANNEL];
    UINT1               au1TempAused[RFMGMT_MAX_CHANNEL];
    UINT1               au1TempBused[RFMGMT_MAX_CHANNEL];
    UINT1               au1TempAunused[RFMGMT_MAX_CHANNEL];
    UINT1               au1TempBunused[RFMGMT_MAX_CHANNEL];
    UINT1               u1Channel = 0;
    UINT1               u1Index = 0;
    UINT1               u1count = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpRadioId = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT4               u4TpcMode = 0;
    UINT4               u4TxPowerLevel = 0;
    UINT4               u4PrevWtpProfileId = 0;
    UINT4               u4PrevWtpRadioId = 0;
    INT4                i4WtpRadioIfIndx = 0;
    INT4                i4WtpRadioTypeVal = 0;
    UINT1               au1WtpProfStringVal[256] = { 0 };
    UINT4               u4AssignedChannel = 0;
    INT4                i4RrmAPChannelMode = 0;

    WtpOctetProfString.pu1_OctetList = au1WtpProfStringVal;
    MEMSET (au1RrmAllowedChannels, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (au1RrmUnusedChannels, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (au1TempAused, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (au1TempBused, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (au1TempAunused, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (au1TempBunused, 0, RFMGMT_MAX_CHANNEL);

    if (nmhGetFirstIndexFsRrmConfigTable (&i4FsRrmRadioType) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4WtpProfileId, &u4WtpRadioId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        i4WtpProfileRowVal = 0;
        if (nmhGetCapwapBaseWirelessBindingType
            (u4WtpProfileId, u4WtpRadioId, &i4WtpProfileRowVal) != SNMP_SUCCESS)
        {
            continue;
        }
        i4WtpRadioIfIndx = 0;
        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (u4WtpProfileId, u4WtpRadioId, &i4WtpRadioIfIndx) != SNMP_SUCCESS)
        {
            continue;

        }
        if (i4WtpRadioIfIndx != 0)
        {
            i4WtpRadioTypeVal = 0;
            if (nmhGetFsDot11RadioType
                (i4WtpRadioIfIndx,
                 (UINT4 *) &i4WtpRadioTypeVal) != SNMP_SUCCESS)
            {
                continue;
            }
            /* In Rf module, if Radio type is "AC","AN" or "A",it is set to "A" and 
               if Radio Type is "BG","BGN" or "B",it is set to "B" */
            if ((i4WtpRadioTypeVal == CLI_RADIO_TYPEA)
                || (i4WtpRadioTypeVal == CLI_RADIO_TYPEAN)
                || ((UINT4) i4WtpRadioTypeVal == CLI_RADIO_TYPEAC))
            {
                i4WtpRadioTypeVal = CLI_RADIO_TYPEA;
            }

            if ((i4WtpRadioTypeVal == CLI_RADIO_TYPEB)
                || (i4WtpRadioTypeVal == CLI_RADIO_TYPEBG)
                || (i4WtpRadioTypeVal == CLI_RADIO_TYPEBGN))
            {
                i4WtpRadioTypeVal = CLI_RADIO_TYPEB;
            }

            if (i4WtpRadioTypeVal != 0)
            {
                MEMSET (au1WtpProfStringVal, 0, sizeof (au1WtpProfStringVal));
                WtpOctetProfString.i4_Length = 256;
                nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                &WtpOctetProfString);
            }
            if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4Status) ==
                SNMP_FAILURE)
            {
                continue;
            }

            nmhGetFsRrmDcaMode (i4WtpRadioTypeVal, &i4DcaMode);
            nmhGetFsRrmDcaSelectionMode (i4WtpRadioTypeVal, &i4DcaSelection);
            nmhGetFsRrmTpcMode (i4WtpRadioTypeVal, &i4TpcMode);
            nmhGetFsRrmTpcSelectionMode (i4WtpRadioTypeVal, &i4TpcSelection);
            nmhGetFsRrmAPTpcMode (i4WtpRadioIfIndx, (INT4 *) (&u4TpcMode));
            nmhGetFsRrmAPTxPowerLevel ((INT4) i4WtpRadioIfIndx,
                                       &u4TxPowerLevel);
            nmhGetFsRrmAPChannelMode (i4WtpRadioIfIndx, &i4RrmAPChannelMode);
            nmhGetFsRrmAPAssignedChannel (i4WtpRadioIfIndx, &u4AssignedChannel);
            if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
            {
                if (i4DcaSelection != CLI_DCA_SELECTION_OFF)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s channel global %s \r\n\r\n",
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEB) ? "b" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               (i4DcaSelection ==
                                CLI_DCA_SELECTION_AUTO) ? "auto"
                               : (i4DcaSelection ==
                                  CLI_DCA_SELECTION_ONCE) ? "once" : "");
                }
            }

            if (i4DcaMode == CLI_DCA_MODE_PER_AP)
            {
                if (i4RrmAPChannelMode == CLI_DCA_PER_AP_GLOBAL)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s channel ap %s global\r\n\r\n",
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEB) ? "b" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               au1WtpProfStringVal);
                }
                if (i4RrmAPChannelMode == CLI_DCA_PER_AP_MANUAL)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s channel ap %s %d\r\n\r\n",
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEB) ? "b" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               au1WtpProfStringVal, u4AssignedChannel);
                }
            }

            if (i4TpcMode == CLI_TPC_MODE_GLOBAL)
            {
                if (i4TpcSelection != CLI_TPC_SELECTION_OFF)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s txPower global %s \r\n\r\n",
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEB) ? "b" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               (i4TpcSelection ==
                                CLI_TPC_SELECTION_AUTO) ? "auto"
                               : (i4TpcSelection ==
                                  CLI_TPC_SELECTION_ONCE) ? "once" : "");
                }

            }
            if (i4TpcMode == CLI_TPC_MODE_PER_AP)
            {
                if (u4TpcMode == CLI_TPC_PER_AP_GLOBAL)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s txPower ap %s gloabal\r\n\r\n",
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEB) ? "b" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               au1WtpProfStringVal);
                }
                if (u4TpcMode == CLI_TPC_PER_AP_MANUAL)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s txPower ap %s %d\r\n\r\n",
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEB) ? "b" :
                               (i4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               au1WtpProfStringVal, u4TxPowerLevel);
                }

            }
            RrmAllowedChannels.pu1_OctetList = au1RrmAllowedChannels;
            RrmUnusedChannels.pu1_OctetList = au1RrmUnusedChannels;
            nmhGetFsRrmAllowedChannels (i4WtpRadioTypeVal, &RrmAllowedChannels);
            if (nmhGetFsRrmUnusedChannels
                (i4WtpRadioTypeVal, &RrmUnusedChannels) != SNMP_SUCCESS)
            {
            }
            if (i4WtpRadioTypeVal == CLI_RADIO_TYPEA)
            {
                u1count = 0;
                for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA;
                     u1Channel++)
                {
                    if (au1RrmAllowedChannels[u1Channel] != 0x0)
                    {
                        au1TempAused[u1count] =
                            au1RrmAllowedChannels[u1Channel];
                        u1count++;
                    }
                }
                for (u1Index = 0; u1Index < u1count; u1Index++)
                {
                    if (au1TempAused[u1Index] != 0)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 a channel add %d\r\n ",
                                   au1TempAused[u1Index]);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 a channel add %d\r\n",
                                   au1TempAused[u1Index]);
                    }
                }
                u1count = 0;
                for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA;
                     u1Channel++)
                {
                    if (au1RrmUnusedChannels[u1Channel] != 0x0)
                    {
                        au1TempAunused[u1count] =
                            au1RrmUnusedChannels[u1Channel];
                        u1count++;
                    }
                }
                for (u1Index = 0; u1Index < u1count; u1Index++)
                {
                    CliPrintf (CliHandle,
                               "advanced dot11 a channel delete %d\r\n ",
                               au1TempAunused[u1Index]);

                }
            }
            if (i4WtpRadioTypeVal == CLI_RADIO_TYPEB)
            {
                u1count = 0;
                for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB;
                     u1Channel++)
                {
                    if (au1RrmAllowedChannels[u1Channel] != 0x0)
                    {
                        au1TempBused[u1count] =
                            au1RrmAllowedChannels[u1Channel];
                        u1count++;
                    }
                }
                for (u1Index = 0; u1Index < u1count; u1Index++)
                {
                    if (au1TempBused[u1Index] != 0)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s channel add %d\r\n ",
                                   (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                                   (i4FsRrmRadioType ==
                                    CLI_RADIO_TYPEG) ? "g" : "",
                                   au1TempBused[u1Index]);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s channel add %d\r\n",
                                   (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                                   (i4FsRrmRadioType ==
                                    CLI_RADIO_TYPEG) ? "g" : "",
                                   au1TempBused[u1Index]);
                    }
                }
                u1count = 0;
                for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB;
                     u1Channel++)
                {
                    if (au1RrmUnusedChannels[u1Channel] != 0x0)
                    {
                        au1TempBunused[u1count] =
                            au1RrmUnusedChannels[u1Channel];
                        u1count++;
                    }
                }
                for (u1Index = 0; u1Index < u1count; u1Index++)
                {
                    if (au1TempBunused[u1Index] != 0)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s channel delete %d\r\n ",
                                   (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                                   (i4FsRrmRadioType ==
                                    CLI_RADIO_TYPEG) ? "g" : "",
                                   au1TempBunused[u1Index]);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s channel delete %d\r\n",
                                   (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                                   (i4FsRrmRadioType ==
                                    CLI_RADIO_TYPEG) ? "g" : "",
                                   au1TempBunused[u1Index]);
                    }
                }
            }

        }
        u4PrevWtpProfileId = u4WtpProfileId;
        u4PrevWtpRadioId = u4WtpRadioId;
        u4WtpProfileId = 0;
        u4WtpRadioId = 0;

    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable (u4PrevWtpProfileId,
                                                        &u4WtpProfileId,
                                                        u4PrevWtpRadioId,
                                                        &u4WtpRadioId) ==
           SNMP_SUCCESS);

    if (nmhGetFirstIndexFsRrmConfigTable (&i4FsRrmRadioType) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if ((i4FsRrmRadioType == CLI_RADIO_TYPEA)
            || (i4FsRrmRadioType == CLI_RADIO_TYPEAN)
            || ((UINT4) i4FsRrmRadioType == CLI_RADIO_TYPEAC))
        {
            i4FsRrmRadioType = CLI_RADIO_TYPEA;
        }

        if ((i4FsRrmRadioType == CLI_RADIO_TYPEB)
            || (i4FsRrmRadioType == CLI_RADIO_TYPEBG)
            || (i4FsRrmRadioType == CLI_RADIO_TYPEBGN))
        {
            i4FsRrmRadioType = CLI_RADIO_TYPEB;
        }

        if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4Status) == SNMP_FAILURE)
        {
            continue;
        }
        if (i4Status != ACTIVE)
        {
            i4PreFsRrmRadioType = i4FsRrmRadioType;
            if (nmhGetNextIndexFsRrmConfigTable (i4PreFsRrmRadioType,
                                                 &i4FsRrmRadioType) !=
                SNMP_SUCCESS)
            {
                break;
            }
            continue;
        }
        if (i4FsRrmRadioType != 0)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s dot11radio \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a"
                       : (i4FsRrmRadioType ==
                          CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "");
        }

        nmhGetFsRrmDcaSensitivity (i4FsRrmRadioType, &i4RrmDcaSensitivity);
        if (i4RrmDcaSensitivity != CLI_DCA_SENSITIVITY_LOW)
        {
            if (i4RrmDcaSensitivity == CLI_DCA_SENSITIVITY_MEDIUM)
            {
                CliPrintf (CliHandle,
                           "advanced dot11 %s channel-dca-sensitivity medium \r\n\r\n",
                           (i4FsRrmRadioType ==
                            CLI_RADIO_TYPEA) ? "a"
                           : (i4FsRrmRadioType ==
                              CLI_RADIO_TYPEB) ? "b"
                           : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "");

            }
            if (i4RrmDcaSensitivity == CLI_DCA_SENSITIVITY_HIGH)
            {
                CliPrintf (CliHandle,
                           "advanced dot11 %s channel-dca-sensitivity high \r\n\r\n",
                           (i4FsRrmRadioType ==
                            CLI_RADIO_TYPEA) ? "a"
                           : (i4FsRrmRadioType ==
                              CLI_RADIO_TYPEB) ? "b"
                           : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "");

            }

        }

        nmhGetFsRrmDcaInterval (i4FsRrmRadioType, &u4RrmDcaInterval);
        if (u4RrmDcaInterval != RFMGMT_DEF_DCA_INTERVAL)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s channel-dca-interval %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a"
                       : (i4FsRrmRadioType ==
                          CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "",
                       u4RrmDcaInterval);
        }
        nmhGetFsRrmUpdateChannel (i4FsRrmRadioType, &i4RrmUpdateChannel);
        if (i4RrmUpdateChannel != RFMGMT_DEF_DCA_UPDATE_CHANNEL)
        {
            CliPrintf (CliHandle, "advanced dot11 %s channel-update \r\n\r\n",
                       (i4FsRrmRadioType == CLI_RADIO_TYPEA) ? "a" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "");

        }
        nmhGetFsRrmTpcUpdate (i4FsRrmRadioType, &i4FsRrmTpcUpdate);
        if (i4FsRrmTpcUpdate != RFMGMT_DEF_TPC_UPDATE)
        {
            CliPrintf (CliHandle, "advanced dot11 %s tpc-update \r\n\r\n",
                       (i4FsRrmRadioType == CLI_RADIO_TYPEA) ? "a" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "");

        }
        nmhGetFsRrmClientThreshold (i4FsRrmRadioType, &u4RrmClientThreshold);
        if (u4RrmClientThreshold != RFMGMT_DEF_CLIENT_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s client-threshold %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a"
                       : (i4FsRrmRadioType ==
                          CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType ==
                          CLI_RADIO_TYPEG) ? "g" : "", u4RrmClientThreshold);
        }
        nmhGetFsRrmSNRThreshold (i4FsRrmRadioType, &i4RrmSNRThreshold);
        if (i4RrmSNRThreshold != RFMGMT_DEF_SNR_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s coverage-snr-threshold %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a"
                       : (i4FsRrmRadioType ==
                          CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "",
                       i4RrmSNRThreshold);
        }
        nmhGetFsRrmRSSIThreshold (i4FsRrmRadioType, &i4RrmRSSIThreshold);
        if (i4RrmRSSIThreshold != RFMGMT_DEF_RSSI_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s dca-rssi-threshold %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a" : (i4FsRrmRadioType ==
                                                  CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "",
                       (-1 * i4RrmRSSIThreshold));
        }
        nmhGetFsRrmTpcInterval (i4FsRrmRadioType, &u4RrmTpcInterval);
        if (u4RrmTpcInterval != RFMGMT_DEF_TPC_INTERVAL)
        {
            CliPrintf (CliHandle, "advanced dot11 %s tpc-interval %d \r\n\r\n",
                       (i4FsRrmRadioType == CLI_RADIO_TYPEA) ? "a" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "",
                       u4RrmTpcInterval);
        }
        nmhGetFsRrmSHAInterval (i4FsRrmRadioType, &u4RrmSHAInterval);
        if (u4RrmSHAInterval != RFMGMT_DEF_TPC_INTERVAL)
        {
            CliPrintf (CliHandle, "advanced dot11 %s sha-interval %d \r\n\r\n",
                       (i4FsRrmRadioType == CLI_RADIO_TYPEA) ? "a" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g" : "",
                       u4RrmSHAInterval);
        }
/*    nmhGetFsRrm11hDfsInterval (i4FsRrmRadioType, &u4Rrm11hDfsInterval);
        if (u4Rrm11hDfsInterval != RFMGMT_DEF_DFS_INTERVAL)
        {
            CliPrintf (CliHandle, "advanced dot11 %s Dfs-interval %d \r\n\r\n",
                       (i4FsRrmRadioType == CLI_RADIO_TYPEA) ? "a" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBG) ? "bg"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEAN) ? "an"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBGN) ? "bgn" : "",
                       u4Rrm11hDfsInterval);
        }
*/
        i4PreFsRrmRadioType = i4FsRrmRadioType;
    }
    while (nmhGetNextIndexFsRrmConfigTable (i4PreFsRrmRadioType,
                                            &i4FsRrmRadioType) == SNMP_SUCCESS);
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRrmAPConfigTable                         */
/*                                                                           */
/*  Description     : This function displays RrmAPConfigTable configurations */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRrmAPConfigTable (tCliHandle CliHandle)
{

    INT4                i4APAutoScanStatus = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4nextProfileId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4RrmAPChannelScanDuration = 0;
    UINT4               u4RrmAPNeighborScanFreq = 0;
    UINT4               u4RrmAPNeighborAgingPeriod = 0;
    UINT4               u4RrmTpcRequestInterval = 0;
    tMacAddr            RrmAPMacAddress;
    INT4                i4RadioIfIndex = 0;
    UINT1               au1WtpProfStringVal[256] = { 0 };
    INT4                i4WtpRadioTypeVal = 0;
    tSNMP_OCTET_STRING_TYPE WtpOctetProfString = { NULL, 0 };

    WtpOctetProfString.pu1_OctetList = au1WtpProfStringVal;

    MEMSET (&RrmAPMacAddress, 0, sizeof (tMacAddr));
    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        u4currentProfileId = u4nextProfileId;
        u4currentBindingId = u4nextBindingId;
        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (u4currentProfileId, u4currentBindingId,
             &i4RadioIfIndex) != SNMP_SUCCESS)
        {
            continue;
        }

        if (i4RadioIfIndex != 0)
        {
            i4WtpRadioTypeVal = 0;
            if (nmhGetFsDot11RadioType
                (i4RadioIfIndex, (UINT4 *) &i4WtpRadioTypeVal) != SNMP_SUCCESS)
            {
                continue;
            }
            if (i4WtpRadioTypeVal != 0)
            {
                MEMSET (au1WtpProfStringVal, 0, sizeof (au1WtpProfStringVal));
                WtpOctetProfString.i4_Length = 256;
                nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                &WtpOctetProfString);

                if (WtpOctetProfString.i4_Length != 0)
                {
                    nmhGetFsRrmAPAutoScanStatus (i4RadioIfIndex,
                                                 &i4APAutoScanStatus);
                    if (i4APAutoScanStatus != RFMGMT_AUTO_SCAN_ENABLE)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s auto-scan disable %s dot11radio %d  \r\n\r\n",
                                   (i4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (i4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (i4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBG) ? "bg"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEAN) ? "an"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBGN) ? "bgn" : "",
                                   au1WtpProfStringVal, u4currentBindingId);

                    }
                    nmhGetFsRrmAPNeighborScanFreq (i4RadioIfIndex,
                                                   &u4RrmAPNeighborScanFreq);
                    if (u4RrmAPNeighborScanFreq !=
                        RFMGMT_DEF_NEIGHBOR_MSG_PERIOD)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s  scan-frequency %d %s dot11radio %d  \r\n\r\n",
                                   (i4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (i4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (i4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBG) ? "bg"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEAN) ? "an"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBGN) ? "bgn" : "",
                                   u4RrmAPNeighborScanFreq, au1WtpProfStringVal,
                                   u4currentBindingId);
                    }

                    nmhGetFsRrmAPChannelScanDuration (i4RadioIfIndex,
                                                      &u4RrmAPChannelScanDuration);
                    if (u4RrmAPChannelScanDuration !=
                        RFMGMT_DEF_CHANNEL_SCAN_DURATION)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s channel-scan-duration %d %s dot11radio %d  \r\n\r\n",
                                   (i4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (i4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (i4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBG) ? "bg"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEAN) ? "an"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBGN) ? "bgn" : "",
                                   u4RrmAPChannelScanDuration,
                                   au1WtpProfStringVal, u4currentBindingId);

                    }
                    nmhGetFsRrmAPNeighborAgingPeriod (i4RadioIfIndex,
                                                      &u4RrmAPNeighborAgingPeriod);
                    if (u4RrmAPNeighborAgingPeriod !=
                        RFMGMT_DEF_NEIGHBOR_AGING_PERIOD)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s neighbor-aging-period %d %s dot11radio %d  \r\n\r\n",
                                   (i4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (i4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (i4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBG) ? "bg"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEAN) ? "an"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBGN) ? "bgn" : "",
                                   u4RrmAPNeighborAgingPeriod,
                                   au1WtpProfStringVal, u4currentBindingId);
                    }
                    nmhGetFsRrm11hTPCRequestInterval (i4RadioIfIndex,
                                                      &u4RrmTpcRequestInterval);
                    if (i4WtpRadioTypeVal == CLI_RADIO_TYPEA
                        && u4RrmTpcRequestInterval !=
                        RFMGMT_DEF_TPC_REQUEST_INTERVAL)
                    {
                        CliPrintf (CliHandle,
                                   "dot11h tpc-request-interval %d %s dot11radio %d  \r\n\r\n",
                                   u4RrmTpcRequestInterval,
                                   au1WtpProfStringVal, u4currentBindingId);
                    }

                }
            }
        }

    }

    while (nmhGetNextIndexFsCapwapWirelessBindingTable
           (u4currentProfileId, &u4nextProfileId,
            u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRrmTpcConfigTable                        */
/*                                                                           */
/*  Description     : This function displays rfmgmt Tpc Configuration        */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRrmTpcConfigTable (tCliHandle CliHandle)
{

    INT4                i4SNRScanStatus = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4WtpRadioTypeVal = 0;
    UINT1               au1WtpProfStringVal[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetProfString = { NULL, 0 };
    UINT4               u4SNRScanFreq = 0;

    WtpOctetProfString.pu1_OctetList = au1WtpProfStringVal;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        u4currentProfileId = u4nextProfileId;
        u4currentBindingId = u4nextBindingId;
        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (u4currentProfileId, u4currentBindingId,
             &i4RadioIfIndex) != SNMP_SUCCESS)
        {
            continue;
        }

        if (i4RadioIfIndex != 0)
        {
            i4WtpRadioTypeVal = 0;
            if (nmhGetFsDot11RadioType
                (i4RadioIfIndex, (UINT4 *) &i4WtpRadioTypeVal) != SNMP_SUCCESS)
            {
                continue;
            }

            if (i4WtpRadioTypeVal != 0)
            {
                MEMSET (au1WtpProfStringVal, 0, sizeof (au1WtpProfStringVal));
                WtpOctetProfString.i4_Length = 256;
                nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                &WtpOctetProfString);

                if (WtpOctetProfString.i4_Length != 0)
                {
                    nmhGetFsRrmAPSNRScanStatus (i4RadioIfIndex,
                                                &i4SNRScanStatus);
                    if (i4SNRScanStatus != RFMGMT_DEF_SNR_SCAN_STATUS)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s  snr-scan disable %s dot11radio %d  \r\n\r\n",
                                   (i4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (i4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (i4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBG) ? "bg"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEAN) ? "an"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBGN) ? "bgn" : "",
                                   au1WtpProfStringVal, u4currentBindingId);
                    }
                    nmhGetFsRrmSNRScanFreq ((INT4) i4RadioIfIndex,
                                            &u4SNRScanFreq);
                    if (u4SNRScanFreq != RFMGMT_DEF_SNR_SCAN_PERIOD)
                    {
                        CliPrintf (CliHandle,
                                   "advanced dot11 %s  snr-scan-period %d  %s dot11radio %d  \r\n\r\n",
                                   (i4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (i4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (i4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBG) ? "bg"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEAN) ? "an"
                                   : (i4WtpRadioTypeVal ==
                                      CLI_RADIO_TYPEBGN) ? "bgn" : "",
                                   u4SNRScanFreq, au1WtpProfStringVal,
                                   u4currentBindingId);
                    }
                }
            }
        }
    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable (u4currentProfileId,
                                                        &u4nextProfileId,
                                                        u4currentBindingId,
                                                        &u4nextBindingId) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRrmTpcScanTable                          */
/*                                                                           */
/*  Description     : This function displays rfmgmt TxPower                  */
/*                    Control Client Scan Table.                             */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRrmTpcScanTable (tCliHandle CliHandle)
{

    UINT4               u4NextRrmWlanId = 0;
    UINT4               u4RrmWlanId = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    INT4                i4RrmSSIDScanStatus = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4WtpRadioTypeVal = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (nmhGetFirstIndexFsRrmTpcScanTable (&i4NextIndex,
                                           &u4NextRrmWlanId) != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_SUCCESS;
    }
    do
    {
        i4Index = i4NextIndex;
        u4RrmWlanId = u4NextRrmWlanId;

        if (i4Index != 0)
        {
            u4WtpRadioTypeVal = 0;
            if (nmhGetFsDot11RadioType
                (i4Index, &u4WtpRadioTypeVal) != SNMP_SUCCESS)
            {
                continue;
            }

            if (u4WtpRadioTypeVal != 0)
            {

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_FAILURE)
                {
                    u4currentBindingId =
                        (UINT4) RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
                    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

                    if (WssIfProcessCapwapDBMsg
                        (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) != OSIX_SUCCESS)
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }

                nmhGetFsRrmSSIDScanStatus (i4Index, u4RrmWlanId,
                                           &i4RrmSSIDScanStatus);
                if (i4RrmSSIDScanStatus == RFMGMT_BSSID_SCAN_STATUS_DISABLE)
                {
                    CliPrintf (CliHandle,
                               "advanced dot11 %s  ssid-snr-scan disable %s dot11radio %d  wlan-id %d\r\n\r\n",
                               (u4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEA) ? "a"
                               : (u4WtpRadioTypeVal ==
                                  CLI_RADIO_TYPEB) ? "b"
                               : (u4WtpRadioTypeVal ==
                                  CLI_RADIO_TYPEG) ? "g"
                               : (u4WtpRadioTypeVal ==
                                  CLI_RADIO_TYPEBG) ? "bg"
                               : (u4WtpRadioTypeVal ==
                                  CLI_RADIO_TYPEAN) ? "an"
                               : (u4WtpRadioTypeVal ==
                                  CLI_RADIO_TYPEBGN) ? "bgn"
                               : (u4WtpRadioTypeVal ==
                                  CLI_RADIO_TYPEAC) ? "ac" : "",
                               pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                               u4currentBindingId, u4RrmWlanId);
                }

            }
        }
    }
    while (nmhGetNextIndexFsRrmTpcScanTable (i4Index, &i4NextIndex,
                                             u4RrmWlanId,
                                             &u4NextRrmWlanId) == SNMP_SUCCESS);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRrmExtConfigTable                        */
/*                                                                           */
/*  Description     : This function displays RrmExtConfigTable configurations*/
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRrmExtConfigTable (tCliHandle CliHandle)
{
    UINT4               u4RrmSHAInterval = 0;
    INT4                i4FsRrmRadioType = 0;
    INT4                i4RrmSHAStatus = 0;
    INT4                i4PowerThreshold = 0;
    INT4                i4RrmTPCStatus = 0;
    INT4                i4RrmConsiderExternalAps = 0;
    UINT4               u4RrmTPCInterval = 0;
    UINT4               u4RrmNeighborCountThreshold = 0;
    INT4                i4RrmDFSStatus = 0;
    UINT4               u4RrmDFSInterval = 0;
    INT4                i4PreFsRrmRadioType = 0;

    if (nmhGetFirstIndexFsRrmExtConfigTable (&i4FsRrmRadioType) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        nmhGetFsRrmSHAInterval (i4FsRrmRadioType, &u4RrmSHAInterval);

        if (u4RrmSHAInterval != RFMGMT_DEF_SHA_INTERVAL)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s sha-interval %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a" : (i4FsRrmRadioType ==
                                                  CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBG) ? "bg"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEAN) ? "an"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBGN) ? "bgn" : "",
                       u4RrmSHAInterval);
        }
        nmhGetFsRrmSHAStatus (i4FsRrmRadioType, &i4RrmSHAStatus);
        if (i4RrmSHAStatus != RFMGMT_DEF_SHA_STATUS)
        {
            CliPrintf (CliHandle, "advanced dot11 %s sha enable \r\n\r\n",
                       (i4FsRrmRadioType == CLI_RADIO_TYPEA) ? "a" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEB) ? "b" :
                       (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBG) ? "bg"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEAN) ? "an"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBGN) ? "bgn" : "");

        }
        nmhGetFsRrmPowerThreshold (i4FsRrmRadioType, &i4PowerThreshold);
        if (i4PowerThreshold != RFMGMT_DEF_POWER_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s tx-power-control-threshold %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a" : (i4FsRrmRadioType ==
                                                  CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBG) ? "bg"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEAN) ? "an"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBGN) ? "bgn" : "",
                       (-1 * i4PowerThreshold));
        }
        nmhGetFsRrm11hTpcStatus (i4FsRrmRadioType, &i4RrmTPCStatus);
        if (i4RrmTPCStatus != RFMGMT_DEF_TPC_STATUS)
        {
            CliPrintf (CliHandle, "dot11h tpc disable \r\n\r\n");

        }
        else
        {
            nmhGetFsRrm11hTpcInterval (i4FsRrmRadioType, &u4RrmTPCInterval);
            if (u4RrmTPCInterval != RFMGMT_DEF_TPC_INTERVAL)
            {
                CliPrintf (CliHandle,
                           "dot11h tpc-interval %d \r\n\r\n", u4RrmTPCInterval);
            }
        }
        nmhGetFsRrm11hDfsStatus (i4FsRrmRadioType, &i4RrmDFSStatus);
        if (i4RrmDFSStatus != RFMGMT_DEF_11H_DFS_STATUS)
        {
            CliPrintf (CliHandle, "dot11h dfs disable \r\n\r\n");

        }
        else
        {
            nmhGetFsRrm11hDfsInterval (i4FsRrmRadioType, &u4RrmDFSInterval);
            if (u4RrmDFSInterval != RFMGMT_DEF_DFS_INTERVAL)
            {
                CliPrintf (CliHandle,
                           "dot11h dfs-interval %d \r\n\r\n", u4RrmDFSInterval);
            }
        }
        nmhGetFsRrmConsiderExternalAPs (i4FsRrmRadioType,
                                        &i4RrmConsiderExternalAps);
        if (i4RrmConsiderExternalAps != RFMGMT_DEF_CONSIDER_EXT_NEIGHBORS)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s consider-external-aps disable\r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a" : (i4FsRrmRadioType ==
                                                  CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBG) ? "bg"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEAN) ? "an"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBGN) ? "bgn" : "");
        }
        nmhGetFsRrmNeighborCountThreshold (i4FsRrmRadioType,
                                           &u4RrmNeighborCountThreshold);
        if (u4RrmNeighborCountThreshold != RFMGMT_DEF_NEIGHBOR_COUNT_THRESHOLD)
        {
            CliPrintf (CliHandle,
                       "advanced dot11 %s neighbor-count-threshold %d \r\n\r\n",
                       (i4FsRrmRadioType ==
                        CLI_RADIO_TYPEA) ? "a" : (i4FsRrmRadioType ==
                                                  CLI_RADIO_TYPEB) ? "b"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEG) ? "g"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBG) ? "bg"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEAN) ? "an"
                       : (i4FsRrmRadioType == CLI_RADIO_TYPEBGN) ? "bgn" : "",
                       u4RrmNeighborCountThreshold);
        }
        i4PreFsRrmRadioType = i4FsRrmRadioType;
    }
    while (nmhGetNextIndexFsRrmExtConfigTable (i4PreFsRrmRadioType,
                                               &i4FsRrmRadioType) ==
           SNMP_SUCCESS);
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;

}

#ifdef ROGUEAP_WANTED
/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRogueRuleConfigTable                     */
/*                                                                           */
/*  Description     : This function displays RrmExtConfigTable configurations*/
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRogueRuleConfigTable (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE RuleName;
    tSNMP_OCTET_STRING_TYPE SsidName;
    INT4                i4Priority = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    INT4                i4Protection = 0;
    INT4                i4Rssi = 0;
    INT4                i4Tpc = 0;
    INT4                i4Status = 0;
    UINT4               u4ClientCount = 0;
    UINT4               u4Duration = 0;
    UINT1               au1RogueRuleName[WSSMAC_MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[WSSMAC_MAX_SSID_LEN];
    UINT1               au1SsidName[WSSMAC_MAX_SSID_LEN];

    MEMSET (au1RogueRuleName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (&NextRuleName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1SsidName, 0, WSSMAC_MAX_SSID_LEN);

    RuleName.pu1_OctetList = au1RogueRuleName;
    NextRuleName.pu1_OctetList = au1RogueNextRuleName;
    SsidName.pu1_OctetList = au1SsidName;

    if (nmhGetFirstIndexFsRogueRuleConfigTable (&NextRuleName) != SNMP_FAILURE)
    {
        while (MEMCMP (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                       WSSMAC_MAX_SSID_LEN) != 0)
        {
            nmhGetFsRogueApRulePriOrderNum (&NextRuleName, &i4Priority);
            nmhGetFsRogueApRuleClass (&NextRuleName, &i4Class);
            nmhGetFsRogueApRuleState (&NextRuleName, &i4State);
            if (i4Class == CLI_AP_CLASS_FRIENDLY)
            {
                CliPrintf (CliHandle,
                           "config rogue rule add ap priority %d classify friendly %s\r\n\r\n",
                           i4Priority, NextRuleName.pu1_OctetList);
            }
            if (i4Class == CLI_AP_CLASS_MALICIOUS)
            {
                if (i4State == CLI_AP_STATE_ALERT)
                {
                    CliPrintf (CliHandle,
                               "config rogue rule add ap priority %d"
                               "classify malicious state alert %s\r\n\r\n",
                               i4Priority, NextRuleName.pu1_OctetList);
                }
                if (i4State == CLI_AP_STATE_CONTAIN)
                {
                    CliPrintf (CliHandle,
                               "config rogue rule add ap priority %d"
                               "classify malicious state contain %s\r\n\r\n",
                               i4Priority, NextRuleName.pu1_OctetList);
                }
            }
            nmhGetFsRogueApRuleDuration (&NextRuleName, &u4Duration);
            nmhGetFsRogueApRuleProtectionType (&NextRuleName, &i4Protection);
            nmhGetFsRogueApRuleRSSI (&NextRuleName, &i4Rssi);
            nmhGetFsRogueApRuleTpc (&NextRuleName, &i4Tpc);
            nmhGetFsRogueApRuleSSID (&NextRuleName, &SsidName);
            nmhGetFsRogueApRuleClientCount (&NextRuleName, &u4ClientCount);
            nmhGetFsRogueApRuleStatus (&NextRuleName, &i4Status);
            if (u4Duration != CLI_DEFAULT_DURATION)
            {
                CliPrintf (CliHandle, "config rogue rule condition"
                           "ap set duration %d %s\r\n", u4Duration,
                           NextRuleName.pu1_OctetList);
            }
            if (i4Protection != CLI_DEFAULT_PROTECTION)
            {
                CliPrintf (CliHandle, "config rogue rule condition"
                           "ap set no-encryption %d %s\r\n", i4Protection,
                           NextRuleName.pu1_OctetList);
            }
            if (i4Rssi != CLI_DEFAULT_RSSI)
            {
                CliPrintf (CliHandle, "config rogue rule condition"
                           "ap set rssi %d %s\r\n", i4Rssi,
                           NextRuleName.pu1_OctetList);
            }

            if (strlen ((const char *) au1SsidName) != 0)
            {
                CliPrintf (CliHandle, "config rogue rule condition"
                           "ap set ssid %s %s\r\n", SsidName.pu1_OctetList,
                           NextRuleName.pu1_OctetList);
            }
            if (u4ClientCount != CLI_DEFAULT_CLIENT_COUNT)
            {
                CliPrintf (CliHandle, "config rogue rule condition"
                           "ap set client-count %d %s\r\n", u4ClientCount,
                           NextRuleName.pu1_OctetList);
            }
            if (i4Status == 1)
            {
                CliPrintf (CliHandle, "config rogue rule enable %s\r\n",
                           NextRuleName.pu1_OctetList);
            }
            if (i4Status == 2)
            {
                CliPrintf (CliHandle, "config rogue"
                           "rule disable %s\r\n", i4Status,
                           NextRuleName.pu1_OctetList);
            }
            MEMCPY (RuleName.pu1_OctetList,
                    NextRuleName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
            nmhGetNextIndexFsRogueRuleConfigTable (&RuleName, &NextRuleName);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RfmShowRunningRogueManualConfigTable                   */
/*                                                                           */
/*  Description     : This function displays RrmExtConfigTable configurations*/
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RfmShowRunningRogueManualConfigTable (tCliHandle CliHandle)
{
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    INT4                i4Class = 0;
    INT4                i4State = 0;

    MEMSET (au1String, 0, CLI_MAC_TO_STR_LEN);

    if (nmhGetFirstIndexFsRogueManualConfigTable (&NextRogueAP) != SNMP_FAILURE)
    {
        while (MEMCMP (RogueAP, NextRogueAP, 6) != 0)
        {
            CliMacToStr (NextRogueAP, au1String);
            nmhGetFsRogueApManualClass (NextRogueAP, &i4Class);
            nmhGetFsRogueApManualState (NextRogueAP, &i4State);
            if (i4Class == CLI_AP_CLASS_FRIENDLY)
            {
                CliPrintf (CliHandle, "config rogue"
                           "ap friendly add %s\r\n\r\n", au1String);
            }
            if (i4Class == CLI_AP_CLASS_MALICIOUS)
            {
                if (i4State == CLI_AP_STATE_ALERT)
                {
                    CliPrintf (CliHandle, "config rogue"
                               "ap malicious state alert add %s\r\n\r\n",
                               au1String);
                }
                if (i4State == CLI_AP_STATE_CONTAIN)
                {
                    CliPrintf (CliHandle, "config rogue"
                               "ap malicious state contain add %s\r\n\r\n",
                               au1String);
                }
            }
            MEMCPY (RogueAP, NextRogueAP, 6);
            nmhGetNextIndexFsRogueManualConfigTable (RogueAP, &NextRogueAP);
        }
    }
    return CLI_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function     : RfMgmtCliShaStatus                                         */
/*                                                                           */
/* Description  : Used to set the Tpc Mode Configuration.                    */
/*                                                                           */
/* Input        : RadioType, TpcMode                                         */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCliShaStatus (tCliHandle CliHandle, INT4 i4GetRadioType,
                    UINT4 u4ShaStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRrmSHAStatus (&u4ErrCode, i4GetRadioType,
                                 (INT4) u4ShaStatus) == SNMP_SUCCESS)
    {
        if (nmhSetFsRrmSHAStatus (i4GetRadioType, (INT4) u4ShaStatus) !=
            SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hTpcStatus                                      */
/*                                                                           */
/* Description  : Used to set the Tpc Status Configuration.                  */
/*                                                                           */
/* Input        : RadioType, TpcStatus                                       */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RfMgmtCli11hTpcStatus (tCliHandle CliHandle, INT4 i4GetRadioType,
                       UINT4 u4TpcStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "RowStatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsRrm11hTpcStatus (&u4ErrCode, i4GetRadioType,
                                    (INT4) u4TpcStatus) == SNMP_SUCCESS)
    {
        nmhSetFsRrm11hTpcStatus (i4GetRadioType, (INT4) u4TpcStatus);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i4RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "RowStatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i4RetVal == CLI_FAILURE)
    {
        return i4RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hTpcInterval                                    */
/*                                                                           */
/* Description  : Used to set the 11h Tpc Interval.                          */
/*                                                                           */
/* Input        : RadioType, TpcInterval.                                    */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCli11hTpcInterval (tCliHandle CliHandle, INT4 i4RadioType,
                         UINT4 u4TpcInterval)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrm11hTpcInterval (&u4ErrCode, i4RadioType,
                                      u4TpcInterval) != SNMP_FAILURE)
    {
        nmhSetFsRrm11hTpcInterval (i4RadioType, u4TpcInterval);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (i1RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hMinLinkThreshold                               */
/*                                                                           */
/* Description  : Used to set the 11h minimim link threshold                 */
/*                                                                           */
/* Input        : RadioType, link threshold                                  */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCli11hMinLinkThreshold (tCliHandle CliHandle, INT4 i4RadioType,
                              UINT4 u4MinLinkThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrm11hMinLinkThreshold (&u4ErrCode, i4RadioType,
                                           u4MinLinkThreshold) != SNMP_FAILURE)
    {
        nmhSetFsRrm11hMinLinkThreshold (i4RadioType, u4MinLinkThreshold);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (i1RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hMaxLinkThreshold                               */
/*                                                                           */
/* Description  : Used to set the 11h maximim link threshold                 */
/*                                                                           */
/* Input        : RadioType, link threshold                                  */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCli11hMaxLinkThreshold (tCliHandle CliHandle, INT4 i4RadioType,
                              UINT4 u4MaxLinkThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrm11hMaxLinkThreshold (&u4ErrCode, i4RadioType,
                                           u4MaxLinkThreshold) != SNMP_FAILURE)
    {
        nmhSetFsRrm11hMaxLinkThreshold (i4RadioType, u4MaxLinkThreshold);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (i1RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hStaCountThreshold                              */
/*                                                                           */
/* Description  : Used to set the 11h Station count threshold                */
/*                                                                           */
/* Input        : RadioType, station count threshold                         */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCli11hStaCountThreshold (tCliHandle CliHandle, INT4 i4RadioType,
                               UINT4 u4StaCountThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return CLI_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrm11hStaCountThreshold (&u4ErrCode, i4RadioType,
                                            u4StaCountThreshold) !=
        SNMP_FAILURE)
    {
        nmhSetFsRrm11hStaCountThreshold (i4RadioType, u4StaCountThreshold);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (i1RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCli11hTpcRequestInterval                             */
/*                                                                           */
/* Description  : Used to set Request Interval period.                       */
/*                                                                           */
/* Input        : RadioType,Request Interval ,Profile Name,                  */
/*                                      pointer to radio id.                 */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT1
RfMgmtCli11hTpcRequestInterval (tCliHandle CliHandle, INT4 i4RadioType,
                                UINT4 u4RequestInterval, UINT1 *pu1ProfileName,
                                UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*If no Profile name is provided, Request Interval  is updated */
    /* for all Ap's */
    if (pu1ProfileName == NULL)
    {
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (RfMgmtGetDot11RadioIfIndex
                (u4currentProfileId, u4currentBindingId, i4RadioType,
                 &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hTPCRequestInterval
                    (&u4ErrCode, i4RadioIfIndex,
                     u4RequestInterval) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Request Interval  value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hTPCRequestInterval (i4RadioIfIndex,
                                                  u4RequestInterval);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         * AP is updated with the Request Interval value*/
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (RfMgmtGetDot11RadioIfIndex
                        (u4currentProfileId, u4currentBindingId, i4RadioType,
                         &i4RadioIfIndex) == OSIX_SUCCESS)
                    {
                        if (nmhTestv2FsRrm11hTPCRequestInterval
                            (&u4ErrCode, i4RadioIfIndex,
                             u4RequestInterval) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Invalid Request Interval value\r\n");
                            return CLI_FAILURE;
                        }
                        nmhSetFsRrm11hTPCRequestInterval (i4RadioIfIndex,
                                                          u4RequestInterval);

                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Request Interval is updated. */
        else
        {
            u1RadioId = (UINT1) CLI_PTR_TO_U4 (*pu4RadioId);

            if (RfMgmtGetDot11RadioIfIndex (u4WtpProfileId, u1RadioId,
                                            i4RadioType,
                                            &i4RadioIfIndex) == OSIX_SUCCESS)
            {
                if (nmhTestv2FsRrm11hTPCRequestInterval
                    (&u4ErrCode, i4RadioIfIndex,
                     u4RequestInterval) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid Request Interval value\r\n");
                    return CLI_FAILURE;
                }
                nmhSetFsRrm11hTPCRequestInterval (i4RadioIfIndex,
                                                  u4RequestInterval);

            }
            else
            {
                CliPrintf (CliHandle, "\r\n RfMgmtGetDot11RadioIfIndex "
                           "function fetch failed\r\n");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtShow11hTpcInfo                                       */
/*                                                                           */
/* Description  : This function will display the TPC related Info.           */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                pu1ApProfileName - AP Profile Name                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShow11hTpcInfo (tCliHandle CliHandle,
                      UINT1 *pu1CapwapBaseWtpProfileName, UINT4 u4RadioId)
{
    tMacAddr            StationMacAddr;
    tMacAddr            NextStationMacAddr;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4TpcLastRun = 0;
    INT4                i4Index = 0;
    INT4                i4RadioIndex = 0;
    INT4                i4NextIndex = 0;
    INT4                i4TxPowerLevel = 0;
    INT4                i4LinkMargin = 0;
    INT4                i4BaseLinkMargin = 0;
    UINT4               u4LastReport = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetValFsRrm11hTpcStatus = 0;
    UINT4               u4RetValFsRrm11hTpcInterval = 0;
    UINT4               u4RetValFsRrm11hTPCRequestInterval = 0;
    UINT4               u4RetValFsRrm11hMaxLinkThreshold = 0;
    UINT4               u4RetValFsRrm11hMinLinkThreshold = 0;
    UINT4               u4RetValFsRrm11hStaCountThreshold = 0;
    INT4                i4RadioType = CLI_RADIO_TYPEA;
    CHR1                au1Time[RFMGMT_MAX_DATE_LEN];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];

    MEMSET (&StationMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextStationMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1String, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (au1Time, 0, RFMGMT_MAX_DATE_LEN);

    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Not Found");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId, &i4RadioIndex) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nradio ifIndex error");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsDot11RadioType (i4RadioIndex, (UINT4 *) &i4RadioType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        return CLI_FAILURE;
    }
    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Entry not found\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsRrm11hTpcStatus (i4RadioType, &i4RetValFsRrm11hTpcStatus))
    {
        CliPrintf (CliHandle, "\r\nTPC Status                 : %d",
                   i4RetValFsRrm11hTpcStatus);
    }
    if (nmhGetFsRrm11hTpcInterval (i4RadioType, &u4RetValFsRrm11hTpcInterval))
    {
        CliPrintf (CliHandle, "\r\nTPC Interval               : %d",
                   u4RetValFsRrm11hTpcInterval);
    }
    if (nmhGetFsRrm11hMinLinkThreshold (i4RadioType,
                                        &u4RetValFsRrm11hMinLinkThreshold))
    {
        CliPrintf (CliHandle, "\r\nMin Link Threshold         : %d",
                   u4RetValFsRrm11hMinLinkThreshold);
    }
    if (nmhGetFsRrm11hMaxLinkThreshold (i4RadioType,
                                        &u4RetValFsRrm11hMaxLinkThreshold))
    {
        CliPrintf (CliHandle, "\r\nMax Link Threshold         : %d",
                   u4RetValFsRrm11hMaxLinkThreshold);
    }
    if (nmhGetFsRrm11hStaCountThreshold (i4RadioType,
                                         &u4RetValFsRrm11hStaCountThreshold))
    {
        CliPrintf (CliHandle, "\r\nSta Count Threshold        : %d",
                   u4RetValFsRrm11hStaCountThreshold);
    }
    if (nmhGetFsRrm11hTPCRequestInterval
        (i4RadioIndex, &u4RetValFsRrm11hTPCRequestInterval))
    {
        CliPrintf (CliHandle, "\r\nTPC Request Interval       : %d\n",
                   u4RetValFsRrm11hTPCRequestInterval);
    }
    if (nmhGetFsRrm11hTpcLastRun (CLI_RADIO_TYPEA, &u4TpcLastRun))
    {
        UtlGetTimeStrForTicks (u4TpcLastRun, (CHR1 *) au1Time);
        CliPrintf (CliHandle, "\r\nTPC Last Run               : %-12s\n",
                   au1Time);
    }
    if (nmhGetFirstIndexFsRrm11hTpcInfoTable (&i4NextIndex,
                                              &NextStationMacAddr) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n  No entry found");
    }
    else
    {
        do
        {
            if (i4NextIndex == i4RadioIndex)
            {
                MEMSET (au1Time, 0, RFMGMT_MAX_DATE_LEN);

                CliMacToStr (NextStationMacAddr, au1String);
                CliPrintf (CliHandle, "\r\nStation  MAC               : %s",
                           au1String);
                if (nmhGetFsRrm11hBaseLinkMargin
                    (i4NextIndex, NextStationMacAddr,
                     &i4BaseLinkMargin) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nBase Link Margin           : %u",
                               i4BaseLinkMargin);
                }
                if (nmhGetFsRrm11hLinkMargin (i4NextIndex, NextStationMacAddr,
                                              &i4LinkMargin) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nRelative Link Margin       : %u",
                               i4LinkMargin);
                }
                if (nmhGetFsRrm11hTxPowerLevel (i4NextIndex, NextStationMacAddr,
                                                &i4TxPowerLevel) ==
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nTx Power Level             : %u",
                               i4TxPowerLevel);
                }

                if (nmhGetFsRrm11hTpcReportLastReceived
                    (i4NextIndex, NextStationMacAddr,
                     &u4LastReport) == SNMP_SUCCESS)
                {
                    UtlGetTimeStrForTicks (u4LastReport, (CHR1 *) au1Time);
                    CliPrintf (CliHandle,
                               "\r\nLast Report Received  Time : %-12s\n",
                               au1Time);
                }
            }
            i4Index = i4NextIndex;
            MEMCPY (StationMacAddr, NextStationMacAddr, sizeof (tMacAddr));
            i4NextIndex = 0;
            MEMSET (NextStationMacAddr, 0, sizeof (tMacAddr));

        }
        while (nmhGetNextIndexFsRrm11hTpcInfoTable (i4Index, &i4NextIndex,
                                                    StationMacAddr,
                                                    &NextStationMacAddr) ==
               SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtShowFailedAPNeighbors                                */
/*                                                                           */
/* Description  : This function will display the neighbors of the failed Aps */
/*                                                                           */
/* Input        : u4FsRadioType - Radio Type                                 */
/*                pu1ApProfileName - AP Profile Name                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS, is processing succeeds                     */
/*                RFMGMT_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
RfMgmtShowFailedAPNeighbors (tCliHandle CliHandle,
                             INT4 i4RadioType,
                             UINT1 *pu1CapwapBaseWtpProfileName)
{
    tMacAddr            RadioMacAddr;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    UINT4               u4NoOfRadio = 0;
    UINT4               u4RadioId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT1               au1ModelNumber[256];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    UINT1               au1NeighString[CLI_MAC_TO_STR_LEN];
    UINT4               u4RadioIfIndex = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4Rssi = 0;
    UINT4               u4GetRadioType = 0;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    UINT1               au1WtpProfStringVal[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetProfString = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE ModelNumber;

    WtpOctetProfString.pu1_OctetList = au1WtpProfStringVal;
    MEMSET (&RadioMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&au1ModelNumber, 0, sizeof (au1ModelNumber));
    MEMSET (&au1String, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (&au1NeighString, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Validate the input */
    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Not Found");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        ModelNumber.pu1_OctetList = au1ModelNumber;
        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (u4CapwapBaseWtpProfileId, &ModelNumber) == SNMP_SUCCESS)
        {
            if (nmhGetFsNoOfRadio (&ModelNumber, &u4NoOfRadio) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n Radio Type not configured\r\n");
                return CLI_FAILURE;
            }
            for (u4RadioId = 1; u4RadioId <= u4NoOfRadio; u4RadioId++)
            {
                /* Get the radio ifindex */
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId, u4RadioId,
                     (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                {
                }

                if (nmhGetFsRrmAPMacAddress ((INT4) u4RadioIfIndex,
                                             &RadioMacAddr) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "nmhGetFsRrmAPMacAddress: Failed\n");
                    return CLI_FAILURE;
                }
                if (nmhGetFsDot11RadioType ((INT4) u4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }
                if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
                {
                    continue;
                }

                CliPrintf (CliHandle, "\r\nAP Profile Name                : %s",
                           pu1CapwapBaseWtpProfileName);
                CliPrintf (CliHandle, "\r\nRadio ID                       : %d",
                           u4RadioId);
                CliMacToStr (RadioMacAddr, au1String);
                CliPrintf (CliHandle, "\r\nFailed AP Radio MAC            : %s",
                           au1String);

                if (u4GetRadioType == CLI_RADIO_TYPEA)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11a");
                }
                else if (u4GetRadioType == CLI_RADIO_TYPEB)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11b");
                }
                else if (u4GetRadioType == CLI_RADIO_TYPEG)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11g");
                }
                else if (u4GetRadioType == CLI_RADIO_TYPEBG)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11bg");
                }
                else if (u4GetRadioType == CLI_RADIO_TYPEAN)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11an");
                }
                else if (u4GetRadioType == CLI_RADIO_TYPEBGN)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11bgn");
                }
                else if (u4GetRadioType == CLI_RADIO_TYPEAC)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type                     : dot11ac");
                }

                CliPrintf (CliHandle, "\n");
                CliPrintf (CliHandle, "\r\nNearby RADs");

                if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                             &NextNeighMacAddr)
                    != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n  No entry found");
                    CliPrintf (CliHandle, "\n");
                }
                else
                {
                    do
                    {
                        i4Index = i4NextIndex;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == (INT4) u4RadioIfIndex)
                        {
                            nmhGetFsRrmSHARSSIValue (i4Index, NeighMacAddr,
                                                     &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1NeighString);
                            CliPrintf (CliHandle, "\r\n     RAD %-18s : %d dBm",
                                       au1NeighString, i4Rssi);
                        }
                    }
                    while (nmhGetNextIndexFsRrmFailedAPStatsTable
                           (i4Index, &i4NextIndex, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);
                }
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n  No entry found");
            CliPrintf (CliHandle, "\n");
            return CLI_SUCCESS;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                continue;
            }

            if (i4RadioIfIndex != 0)
            {
                u4GetRadioType = 0;

                if (nmhGetFsRrmAPMacAddress (i4RadioIfIndex,
                                             &RadioMacAddr) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }
                if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
                {
                    continue;
                }
                i4Index = i4RadioIfIndex;
                if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                             &NextNeighMacAddr)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n  No entry found");
                    CliPrintf (CliHandle, "\n");
                    return CLI_SUCCESS;
                }
                do
                {
                    i4Index = i4NextIndex;
                    MEMCPY (NeighMacAddr, NextNeighMacAddr, sizeof (tMacAddr));

                    if (u4GetRadioType != 0)
                    {
                        MEMSET (au1WtpProfStringVal, 0,
                                sizeof (au1WtpProfStringVal));
                        WtpOctetProfString.i4_Length = 256;
                        if (nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                            &WtpOctetProfString)
                            == SNMP_FAILURE)
                        {
                        }
                        if (WtpOctetProfString.i4_Length != 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nAP Profile Name                : %s",
                                       WtpOctetProfString.pu1_OctetList);
                            CliPrintf (CliHandle,
                                       "\r\nRadio ID                       : %d",
                                       u4currentBindingId);
                            CliMacToStr (RadioMacAddr, au1String);
                            CliPrintf (CliHandle,
                                       "\r\nFailed AP Radio MAC            : %s",
                                       au1String);
                            if (u4GetRadioType == CLI_RADIO_TYPEA)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nRadio Type                     : dot11a");
                            }
                            else if (u4GetRadioType == CLI_RADIO_TYPEB)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nRadio Type                     : dot11b");
                            }
                            else if (u4GetRadioType == CLI_RADIO_TYPEG)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nRadio Type                     : dot11g");
                            }
                            else if (u4GetRadioType == CLI_RADIO_TYPEBG)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nRadio Type                     : dot11bg");
                            }
                            else if (u4GetRadioType == CLI_RADIO_TYPEAN)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nRadio Type                     : dot11an");
                            }
                            else if (u4GetRadioType == CLI_RADIO_TYPEBGN)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nRadio Type                     : dot11bgn");
                            }

                            CliPrintf (CliHandle, "\n");
                            CliPrintf (CliHandle, "\r\nNearby RADs");

                        }
                    }

                    if (i4NextIndex == i4RadioIfIndex)
                    {

                        nmhGetFsRrmSHARSSIValue (i4Index, NeighMacAddr,
                                                 &i4Rssi);

                        CliMacToStr (NeighMacAddr, au1NeighString);
                        CliPrintf (CliHandle, "\r\n     RAD %-18s : %d dBm",
                                   au1NeighString, i4Rssi);

                    }
                }
                while (nmhGetNextIndexFsRrmFailedAPStatsTable
                       (i4Index, &i4NextIndex, NeighMacAddr,
                        &NextNeighMacAddr) == SNMP_SUCCESS);
            }
            CliPrintf (CliHandle, "\n");
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable (u4currentProfileId,
                                                            &u4nextProfileId,
                                                            u4currentBindingId,
                                                            &u4nextBindingId) ==
               SNMP_SUCCESS);
    }

    CliPrintf (CliHandle, "\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliExternalAPStatus                                  */
/*                                                                           */
/* Description  : Used to set the external AP consideration status           */
/*                                                                           */
/* Input        : RadioType, External AP consideration status                */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
RfMgmtCliExternalAPStatus (tCliHandle CliHandle, INT4 i4RadioType,
                           UINT4 u4ExtAPsConsiderationStatus)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmConsiderExternalAPs (&u4ErrCode, i4RadioType,
                                           (INT4) u4ExtAPsConsiderationStatus)
        != SNMP_FAILURE)
    {
        if ((nmhSetFsRrmConsiderExternalAPs
             (i4RadioType, (INT4) u4ExtAPsConsiderationStatus) != SNMP_SUCCESS))
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliClearNeighborDetails                              */
/*                                                                           */
/* Description  : Used to set the external AP consideration status           */
/*                                                                           */
/* Input        : RadioType, External AP consideration status                */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
RfMgmtCliClearNeighborDetails (tCliHandle CliHandle, INT4 i4RadioType)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmClearNeighborInfo (&u4ErrCode, i4RadioType,
                                         (INT4) OSIX_TRUE) != SNMP_FAILURE)
    {
        nmhSetFsRrmClearNeighborInfo (i4RadioType, (INT4) OSIX_TRUE);
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }

    if (i1RetVal == CLI_FAILURE)
    {
        return i1RetVal;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtCliNeighborCountThreshold                            */
/*                                                                           */
/* Description  : Used to set the Neighbor count threshold.                  */
/*                                                                           */
/* Input        : RadioType, Neighbor count threshold                        */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT1
RfMgmtCliNeighborCountThreshold (tCliHandle CliHandle, INT4 i4RadioType,
                                 UINT4 u4NeighborCountThreshold)
{
    INT1                i1RetVal = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "RowStatus Get failed\r\n");
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsRrmNeighborCountThreshold (&u4ErrCode, i4RadioType,
                                              u4NeighborCountThreshold) !=
        SNMP_FAILURE)
    {
        if (nmhSetFsRrmNeighborCountThreshold
            (i4RadioType, u4NeighborCountThreshold) != SNMP_SUCCESS)
        {
        }
    }
    else
    {
        CliPrintf (CliHandle, "Invalid configuration\r\n");
        i1RetVal = CLI_FAILURE;
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4RadioType, i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rowstatus Set failed\r\n");
        return CLI_FAILURE;
    }
    if (i1RetVal == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef ROGUEAP_WANTED
/*****************************************************************************/
/* Function     : RogueClassifyAP                                            */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueClassifyAP (tCliHandle CliHandle,
                 UINT1 u1ApClass, UINT1 u1ApState, tMacAddr RogueMacAddr)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsRogueApManualClass (&u4ErrorCode,
                                       RogueMacAddr,
                                       (INT4) u1ApClass) == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApManualClass
            (RogueMacAddr, (INT4) u1ApClass) == SNMP_SUCCESS)
        {
            if (u1ApState != 0)
            {
                if (nmhTestv2FsRogueApManualState (&u4ErrorCode,
                                                   RogueMacAddr,
                                                   (INT4) u1ApState) ==
                    SNMP_SUCCESS)
                {
                    if (nmhSetFsRogueApManualState
                        (RogueMacAddr, (INT4) u1ApState) == SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "Failed in nmhSetFsRogueApManualState");
                        return CLI_FAILURE;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Failed in nmhTestv2FsRogueApManualState");
                    return CLI_FAILURE;
                }
            }
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApManualClass");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApManualClass");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueDeleteClassAP                                         */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteClassAP (tCliHandle CliHandle, UINT1 u1ApClass, UINT1 u1ApState)
{
    tMacAddr            FsRogueApBSSID;
    tMacAddr            FsNextRogueApBSSID;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    if (nmhGetFirstIndexFsRogueApTable (&FsNextRogueApBSSID) != SNMP_FAILURE)
    {
        while (MEMCMP (FsRogueApBSSID, FsNextRogueApBSSID, MAC_ADDR_LEN) != 0)
        {
            nmhGetFsRogueApClass (FsNextRogueApBSSID, &i4Class);
            nmhGetFsRogueApState (FsNextRogueApBSSID, &i4State);
            if (u1ApClass == i4Class)
            {
                if (u1ApClass == ROGUEAP_CLASS_MALICIOUS)
                {
                    if (u1ApState != i4State)
                    {
                        /*CliPrintf(CliHandle,"The AP State given is wrong"); */
                        MEMCPY (&FsRogueApBSSID, &FsNextRogueApBSSID,
                                MAC_ADDR_LEN);
                        nmhGetNextIndexFsRogueApTable (FsRogueApBSSID,
                                                       &FsNextRogueApBSSID);
                        continue;
                    }
                }
                MEMCPY (&RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1RogueApBSSID,
                        &FsNextRogueApBSSID, MAC_ADDR_LEN);
                MEMCPY (&RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueManualDB.u1RogueApBSSID,
                        &FsNextRogueApBSSID, MAC_ADDR_LEN);
                if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
                                        &RfMgmtDB) == RFMGMT_FAILURE)
                {
                    CliPrintf (CliHandle, "Deleting Failed");
                    return CLI_FAILURE;
                }
            }
            MEMCPY (&FsRogueApBSSID, &FsNextRogueApBSSID, MAC_ADDR_LEN);
            nmhGetNextIndexFsRogueApTable (FsRogueApBSSID, &FsNextRogueApBSSID);
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhGetFirstIndexFsRogueApTable");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueAddDeleteAP                                           */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueAddDeleteAP (tCliHandle CliHandle, UINT1 u1ApClass,
                  UINT1 u1ApState, UINT1 u1ApFunc, tMacAddr RogueMacAddr)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4DelApClass = 0;
    INT4                i4DelApState = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID, RogueMacAddr, MAC_ADDR_LEN);

    if (u1ApFunc == 1)
    {
        if (nmhTestv2FsRogueApManualClass (&u4ErrorCode,
                                           RogueMacAddr,
                                           (INT4) u1ApClass) != SNMP_FAILURE)
        {
            if (u1ApClass == 3)
            {
                if (nmhTestv2FsRogueApManualState (&u4ErrorCode,
                                                   RogueMacAddr,
                                                   (INT4) u1ApState) !=
                    SNMP_FAILURE)
                {
                    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueManualDB.u1RogueApState = u1ApState;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Failed in nmhTestv2FsRogueApManualState");
                    return CLI_FAILURE;
                }
            }
            else
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApState = u1ApState;
            }
            RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualDB.u1RogueApClass = u1ApClass;
            if (RfMgmtProcessDBMsg (RFMGMT_CREATE_MANUAL_ROGUE_INFO,
                                    &RfMgmtDB) != RFMGMT_FAILURE)
            {
                return CLI_SUCCESS;
            }
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApManualClass");
            return CLI_FAILURE;
        }
    }
    if (u1ApFunc == 2)
    {
        if (nmhGetFsRogueApManualClass (RogueMacAddr,
                                        &i4DelApClass) == SNMP_SUCCESS)
        {
            if (i4DelApClass == u1ApClass)
            {
                if (u1ApClass == 3)
                {
                    if (nmhGetFsRogueApManualState (RogueMacAddr,
                                                    &i4DelApState) ==
                        SNMP_SUCCESS)
                    {
                        if (u1ApState != i4DelApState)
                        {
                            CliPrintf (CliHandle,
                                       "The AP State given is wrong");
                            return CLI_FAILURE;
                        }
                    }
                }
                if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
                                        &RfMgmtDB) != RFMGMT_FAILURE)
                {
                    return CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle, "Failed in Deletion");
                    return CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle, "The AP Class given is wrong");
                return CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhGetFsRogueApManualClass");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueAddRule                                               */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueAddRule (tCliHandle CliHandle, UINT1 u1ApClass,
              UINT1 u1ApState, UINT1 u1Priority, UINT1 *pu1RfName)
{
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pu1RfName, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.u1RogueApRulePriOrderNum = u1Priority;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.u1RogueApRuleClass = u1ApClass;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.u1RogueApRuleState = u1ApState;
    if (RfMgmtProcessDBMsg
        (RFMGMT_CREATE_RULE_ROGUE_INFO, &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        CliPrintf (CliHandle, "Failed in Creation");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueClassifyRule                                          */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueClassifyRule (tCliHandle CliHandle, UINT1 u1ApClass,
                   UINT1 u1ApState, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    if (nmhTestv2FsRogueApRuleClass
        (&u4ErrorCode, &RfName, (INT4) u1ApClass) == SNMP_SUCCESS)
    {
        if (u1ApClass == 3)
        {
            if (nmhTestv2FsRogueApRuleState
                (&u4ErrorCode, &RfName, (INT4) u1ApState) == SNMP_SUCCESS)
            {
                nmhSetFsRogueApRuleState (&RfName, (INT4) u1ApState);
            }
            else
            {
                CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleClass");
                return CLI_FAILURE;
            }
        }
        if (nmhSetFsRogueApRuleClass (&RfName, (INT4) u1ApClass) ==
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleClass");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleClass");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueRuleFunc                                              */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueRuleFunc (tCliHandle CliHandle, UINT1 u1ApFunc, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4deleteRulePriority = 0;
    tSNMP_OCTET_STRING_TYPE RfName;
    tRfMgmtDB           RfMgmtDB;
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE RuleName;
    tSNMP_OCTET_STRING_TYPE RuleName2;
    UINT1               au1RogueRuleName[MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[MAX_SSID_LEN];

    MEMSET (au1RogueRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, ZERO, MAX_SSID_LEN);
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RuleName2.pu1_OctetList = au1RogueRuleName;
    NextRuleName.pu1_OctetList = au1RogueNextRuleName;

    RuleName.pu1_OctetList = pu1RfName;
    if (pu1RfName != NULL)        /*RFMGMT_DESTROY_RULE_ROGUE_INFO_ALL */
    {
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleDB.u1RogueApRuleName,
                pu1RfName, WSSMAC_MAX_SSID_LEN);

        MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        RfName.pu1_OctetList = pu1RfName;
        if (u1ApFunc != 2)
        {
            if (nmhTestv2FsRogueApRuleStatus
                (&u4ErrorCode, &RfName, (INT4) u1ApFunc) == SNMP_SUCCESS)
            {
                if (nmhSetFsRogueApRuleStatus (&RfName,
                                               (INT4) u1ApFunc) == SNMP_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Failed in nmhSetFsRogueApRuleStatus");
                    return CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleStatus");
                return CLI_FAILURE;
            }
        }
        else
        {

            if ((nmhGetFsRogueApRulePriOrderNum
                 (&RuleName, &i4deleteRulePriority) == SNMP_FAILURE))
            {
                CliPrintf (CliHandle,
                           "\r\n Error in getting priority of rule to be deleted \r\n");
                return RFMGMT_FAILURE;
            }
            if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_RULE_ROGUE_INFO, &RfMgmtDB)
                == RFMGMT_SUCCESS)
            {
                RfMgmtReArrangeRogueRule (i4deleteRulePriority);
                return CLI_SUCCESS;
            }
            else
            {
                CliPrintf (CliHandle, "Failed in Deletion");
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (u1ApFunc != 2)
        {
            if (nmhGetFirstIndexFsRogueRuleConfigTable (&NextRuleName) !=
                SNMP_FAILURE)
            {
                while (MEMCMP
                       (RuleName2.pu1_OctetList, NextRuleName.pu1_OctetList,
                        MAX_SSID_LEN) != ZERO)
                {
                    if (nmhTestv2FsRogueApRuleStatus (&u4ErrorCode,
                                                      (tSNMP_OCTET_STRING_TYPE
                                                       *) & NextRuleName.
                                                      pu1_OctetList,
                                                      (INT4) u1ApFunc) ==
                        SNMP_SUCCESS)
                    {
                        if (nmhSetFsRogueApRuleStatus
                            ((tSNMP_OCTET_STRING_TYPE *) & NextRuleName.
                             pu1_OctetList, (INT4) u1ApFunc) == SNMP_SUCCESS)
                        {
                            MEMCPY (RuleName2.pu1_OctetList,
                                    NextRuleName.pu1_OctetList, MAX_SSID_LEN);
                            nmhGetNextIndexFsRogueRuleConfigTable (&RuleName2,
                                                                   &NextRuleName);
                            continue;
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "Failed in nmhSetFsRogueApRuleStatus");
                            return CLI_FAILURE;
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "Failed in nmhTestv2FsRogueApRuleStatus");
                        return CLI_FAILURE;
                    }
                    MEMCPY (RuleName2.pu1_OctetList,
                            NextRuleName.pu1_OctetList, MAX_SSID_LEN);
                    nmhGetNextIndexFsRogueRuleConfigTable (&RuleName2,
                                                           &NextRuleName);
                }
            }
        }
        else
        {
            if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_RULE_ROGUE_INFO_ALL,
                                    &RfMgmtDB) == RFMGMT_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            else
            {
                CliPrintf (CliHandle, "Failed in Deletion");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueSetRuleConditionClient                                */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRuleConditionClient (tCliHandle CliHandle,
                             UINT4 u4ClientCount, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;

    if (nmhTestv2FsRogueApRuleClientCount (&u4ErrorCode,
                                           &RfName,
                                           u4ClientCount) == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApRuleClientCount (&RfName, u4ClientCount) ==
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleClientCount");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleClientCount");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueSetRuleConditionDuration                              */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRuleConditionDuration (tCliHandle CliHandle,
                               UINT4 u4Duration, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;

    if (nmhTestv2FsRogueApRuleDuration (&u4ErrorCode,
                                        &RfName, u4Duration) == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApRuleDuration (&RfName, u4Duration) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleDuration");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleDuration");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueSetRuleConditionEncrypt                               */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRuleConditionEncrypt (tCliHandle CliHandle,
                              UINT1 u1NoEncrypt, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;

    if (nmhTestv2FsRogueApRuleProtectionType
        (&u4ErrorCode, &RfName, (INT4) u1NoEncrypt) == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApRuleProtectionType
            (&RfName, (INT4) u1NoEncrypt) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle,
                       "Failed in nmhSetFsRogueApRuleProtectionType");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleProtectionType");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueSetRuleConditionRssi                                  */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRuleConditionRssi (tCliHandle CliHandle, INT1 i1Rssi, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;

    if (nmhTestv2FsRogueApRuleRSSI (&u4ErrorCode,
                                    &RfName, (INT4) i1Rssi) == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApRuleRSSI (&RfName, (INT4) i1Rssi) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleRSSI");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleRSSI");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueSetRuleConditionTpc                                   */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRuleConditionTpc (tCliHandle CliHandle, UINT1 u1tpc, UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;

    if (nmhTestv2FsRogueApRuleTpc (&u4ErrorCode, &RfName, (INT4) u1tpc)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApRuleTpc (&RfName, (INT4) u1tpc) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleTpc");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleTpc");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueSetRuleCondition                                      */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRuleConditionSsid (tCliHandle CliHandle, UINT1 *pu1ssid,
                           UINT1 *pu1RfName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;
    tSNMP_OCTET_STRING_TYPE SsidName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    RfName.i4_Length = STRLEN (pu1RfName);

    MEMSET (&SsidName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SsidName.pu1_OctetList = pu1ssid;
    SsidName.i4_Length = STRLEN (pu1ssid);

    if (nmhTestv2FsRogueApRuleSSID (&u4ErrorCode, &RfName, &SsidName)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApRuleSSID (&RfName, &SsidName) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleSSID");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Failed in nmhTestv2FsRogueApRuleSSID");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : RogueDeleteRuleConditionClient                             */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteRuleConditionClient (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    UINT4               u4ClientCount = DEFAULT_CLIENT_COUNT;
    tSNMP_OCTET_STRING_TYPE RfName;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            RfName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApClientCount = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.u4RogueApClientCount = u4ClientCount;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        CliPrintf (CliHandle, "Failed in deleting client Rule condition");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueDeleteRuleConditionDuration                           */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteRuleConditionDuration (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    UINT4               u4Duration = CLI_DEFAULT_DURATION;
    tSNMP_OCTET_STRING_TYPE RfName;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            RfName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleDuration = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.u4RogueApRuleDuration = u4Duration;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        CliPrintf (CliHandle, "Failed in deleting Rule condition Duration");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueDeleteRuleConditionEncrypt                            */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteRuleConditionEncrypt (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    INT4                i4Encrypt = CLI_ENCRYPTION_DELETE;    /* Default value for delete */
    tSNMP_OCTET_STRING_TYPE RfName;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            RfName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApProcetionType = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.isRogueApProcetionType = (BOOL1) i4Encrypt;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        CliPrintf (CliHandle, "Failed in deleting Rule condition Encryption");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueDeleteRuleConditionRssi                               */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteRuleConditionRssi (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    INT4                i4Rssi = CLI_DEFAULT_RSSI;
    tSNMP_OCTET_STRING_TYPE RfName;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            RfName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRSSI = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.i2RogueApRSSI = (INT2) i4Rssi;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        CliPrintf (CliHandle, "Failed in deleting Rule condition Rssi");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueDeleteRuleConditionTpc                                */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteRuleConditionTpc (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    tSNMP_OCTET_STRING_TYPE RfName;
    tSNMP_OCTET_STRING_TYPE SsidName;
    UINT4               u4Duration = CLI_DEFAULT_DURATION;
    INT4                i4Encrypt = 2;
    INT4                i4Rssi = CLI_DEFAULT_RSSI;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1Ssid[WSSMAC_MAX_SSID_LEN];
    SsidName.pu1_OctetList = u1Ssid;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (u1Ssid, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (SsidName.pu1_OctetList, 0, WSSMAC_MAX_SSID_LEN);
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            RfName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleDuration = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.u4RogueApRuleDuration = u4Duration;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApProcetionType = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.isRogueApProcetionType = (BOOL1) i4Encrypt;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRSSI = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleDB.i2RogueApRSSI = (INT2) i4Rssi;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleSSID = OSIX_TRUE;

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleSSID,
            SsidName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        CliPrintf (CliHandle, "Failed in deleting all Rule conditions ");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueDeleteRuleConditionSsid                               */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueDeleteRuleConditionSsid (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    UINT1               u1Ssid[WSSMAC_MAX_SSID_LEN];
    MEMSET (u1Ssid, 0, WSSMAC_MAX_SSID_LEN);

    tSNMP_OCTET_STRING_TYPE RfName;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    tSNMP_OCTET_STRING_TYPE SsidName;
    SsidName.pu1_OctetList = u1Ssid;
    MEMSET (SsidName.pu1_OctetList, 0, WSSMAC_MAX_SSID_LEN);
    if (nmhSetFsRogueApRuleSSID (&RfName, &SsidName) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Failed in nmhSetFsRogueApRuleSSID");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function     : RogueSetRfGroupName                                        */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRfGroupName (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE RfName;

    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = pu1RfName;
    RfName.i4_Length = STRLEN (pu1RfName);

    if (nmhTestv2FsRfGroupName (&u4ErrCode, &RfName) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsRfGroupName (&RfName);
    return CLI_SUCCESS;
    UNUSED_PARAM (CliHandle);
}

/*****************************************************************************/
/* Function     : RogueSetRogueDetection                                     */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetRogueDetection (tCliHandle CliHandle, UINT1 u1Detection)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsRogueApDetecttion (&u4ErrorCode, (INT4) u1Detection)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRogueApDetecttion ((INT4) u1Detection) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
}

/*****************************************************************************/
/* Function     : RogueShowDetailedRogueAp                                   */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueShowDetailedRogueAp (tCliHandle CliHandle, tMacAddr RogueMacAddr)
{

    INT4                i4Protection = 0;
    tSNMP_OCTET_STRING_TYPE SsidName;
    tMacAddr            ApLearntFrom;
    UINT1               ssid[WSSMAC_MAX_SSID_LEN];
    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];
    CHR1                au1ReportTime[RFMGMT_MAX_DATE_LEN];
    INT4                i4Rssi = 0;
    INT4                i4Channel = 0;
    INT4                i4Radio = 0;
    INT4                i4Band = 0;
    UINT4               u4ReportTime = 0;
    UINT4               u4HeardTime = 0;
    UINT1               u1RogueApClassifiedBy = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    UINT4               u4ClientCount = 0;
    INT4                i4Status = 0;
    UINT1               au1String[CLI_MAC_TO_STR_LEN];

    MEMSET (au1String, 0, CLI_MAC_TO_STR_LEN);
    MEMSET (au1TimeLastHeard, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (au1ReportTime, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (&ssid, 0, WSSMAC_MAX_SSID_LEN);
    SsidName.pu1_OctetList = ssid;
    UINT1               au1Status[][10] = { "Not Heard", "Heard" };

    CliMacToStr (RogueMacAddr, au1String);
    CliPrintf (CliHandle, "\r\nBSSID of AP            : %s", au1String);

    if (nmhGetFsRogueApClass (RogueMacAddr, &i4Class) != SNMP_FAILURE)
    {
        if (1 == i4Class)
            CliPrintf (CliHandle, "\r\nClass                  : Unclassified");
        else if (2 == i4Class)
            CliPrintf (CliHandle, "\r\nClass                  : Friendly");
        else if (3 == i4Class)
            CliPrintf (CliHandle, "\r\nClass                  : Malicious");
    }
    if (nmhGetFsRogueApState (RogueMacAddr, &i4State) != SNMP_FAILURE)
    {
        if (1 == i4State)
            CliPrintf (CliHandle, "\r\nState                  : None");
        else if (2 == i4State)
            CliPrintf (CliHandle, "\r\nState                  : Alert");
        else if (3 == i4State)
            CliPrintf (CliHandle, "\r\nState                  : Contain");
    }
    if (rogueGetFsApClassifiedBy (RogueMacAddr,
                                  &u1RogueApClassifiedBy) != SNMP_FAILURE)
    {
        if (u1RogueApClassifiedBy == 0)
            CliPrintf (CliHandle, "\r\nClassified By          : Admin/Manual");
        else if (u1RogueApClassifiedBy == 129)
            CliPrintf (CliHandle, "\r\nClassified By          : None");
        else
            CliPrintf (CliHandle, "\r\nClassified By Rule of Priority : %d",
                       u1RogueApClassifiedBy);
    }
    if (nmhGetFsRogueApClientCount (RogueMacAddr, &u4ClientCount) !=
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nClient Count           : %d", u4ClientCount);
    }
    if (nmhGetFsRogueApStatus (RogueMacAddr, &i4Status) != SNMP_FAILURE)
    {
        if (i4Status != 0)
            CliPrintf (CliHandle, "\r\nStatus                 : %s",
                       au1Status[1]);
        else
            CliPrintf (CliHandle, "\r\nStatus                 : %s",
                       au1Status[0]);
    }
    if (i4Status != 0)
    {
        if (nmhGetFsRogueApSSID (RogueMacAddr, &SsidName) != SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nSSID Name              : %s",
                       SsidName.pu1_OctetList);
        }
        if (nmhGetFsRogueApProcetionType (RogueMacAddr,
                                          &i4Protection) != SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nProtection             : %d",
                       i4Protection);
        }
        if ((nmhGetFsRogueApLastHeardBSSID (RogueMacAddr,
                                            &ApLearntFrom) != SNMP_FAILURE))
        {
            CliMacToStr (ApLearntFrom, au1String);
            CliPrintf (CliHandle, "\r\nLearntFromBSSID of AP  : %s", au1String);
        }
        if (nmhGetFsRogueApRSSI (RogueMacAddr, &i4Rssi) != SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nRSSI                   : %d dBm",
                       i4Rssi);
        }
        if (nmhGetFsRogueApOperationChannel (RogueMacAddr, &i4Channel) !=
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nOperation Channel      : %d", i4Channel);
        }
        if (nmhGetFsRogueApDetectedRadio (RogueMacAddr, &i4Radio) !=
            SNMP_FAILURE)
        {
            if (i4Radio == 1)
            {
                CliPrintf (CliHandle, "\r\nDetected Radio         : B");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nDetected Radio         : A");
            }
        }
        if (nmhGetFsRogueApDetectedBand (RogueMacAddr, &i4Band) != SNMP_FAILURE)
        {
            if (i4Radio == 1)
            {
                CliPrintf (CliHandle, "\r\nDetected Band          : 2.4GHz");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nDetected Band          : 5GHz");
            }
        }
        if (nmhGetFsRogueApLastReportedTime (RogueMacAddr,
                                             &u4ReportTime) != SNMP_FAILURE)
        {
            if (u4ReportTime != 0)
            {
                UtlGetTimeStrForTicks (u4ReportTime, (CHR1 *) au1ReportTime);
                CliPrintf (CliHandle, "\r\nReport Time            : %s",
                           au1ReportTime);
                MEMSET (au1ReportTime, 0, RFMGMT_MAX_DATE_LEN);
            }
            else
            {
                CliPrintf (CliHandle, "\r\nReport Time            : Not Heard");
            }

        }
        if (nmhGetFsRogueApLastHeardTime (RogueMacAddr, &u4HeardTime) !=
            SNMP_FAILURE)
        {
            if (u4HeardTime != 0)
            {
                UtlGetTimeStrForTicks (u4HeardTime, (CHR1 *) au1TimeLastHeard);
                CliPrintf (CliHandle, "\r\nHeard Time             : %s",
                           au1TimeLastHeard);
                MEMSET (au1TimeLastHeard, 0, RFMGMT_MAX_DATE_LEN);
            }
            else
            {
                CliPrintf (CliHandle, "\r\nHeard Time             : Not Heard");
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueShowSummaryRogueAp                                    */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueShowSummaryRogueAp (tCliHandle CliHandle, UINT1 u1Class)
{
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;
    UINT4               u4HeardTime = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    INT4                i4Status = 0;
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];
    UINT1               au1header[][12] =
        { "BSSID", "Class", "State", "Last Heard" };
    UINT1               au1class[][13] =
        { "Unclassified", "Friendly", "Malicious" };
    UINT1               au1state[][8] = { "None", "Alert", "Contain" };
    UINT1               au1HeardTime[][10] = { "Not Heard", "Error" };
    MEMSET (au1TimeLastHeard, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (au1String, 0, CLI_MAC_TO_STR_LEN);

    if (nmhGetFirstIndexFsRogueApTable (&NextRogueAP) != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n%-19s %-12s %-7s %-10s\n",
                   au1header[0], au1header[1], au1header[2], au1header[3]);
        while (MEMCMP (RogueAP, NextRogueAP, 6) != 0)
        {
            nmhGetFsRogueApClass (NextRogueAP, &i4Class);
            if (i4Class == u1Class)
            {
                CliMacToStr (NextRogueAP, au1String);
                CliPrintf (CliHandle, "\n%s", au1String);
                if (nmhGetFsRogueApClass (NextRogueAP, &i4Class) !=
                    SNMP_FAILURE)
                {
                    if (1 == i4Class)
                        CliPrintf (CliHandle, "%-13s", au1class[0]);
                    else if (2 == i4Class)
                        CliPrintf (CliHandle, "%-13s", au1class[1]);
                    else if (3 == i4Class)
                        CliPrintf (CliHandle, "%-13s", au1class[2]);
                }
                if (nmhGetFsRogueApState (NextRogueAP, &i4State) !=
                    SNMP_FAILURE)
                {
                    if (1 == i4State)
                        CliPrintf (CliHandle, "%-9s", au1state[0]);
                    else if (2 == i4State)
                        CliPrintf (CliHandle, "%-9s", au1state[1]);
                    else if (3 == i4State)
                        CliPrintf (CliHandle, "%-9s", au1state[2]);

                }
                if (nmhGetFsRogueApLastHeardTime (NextRogueAP, &u4HeardTime) !=
                    SNMP_FAILURE)
                {
                    if (nmhGetFsRogueApStatus (NextRogueAP, &i4Status) !=
                        SNMP_FAILURE)
                    {
                        if (1 == i4Status)
                        {
                            UtlGetTimeStrForTicks (u4HeardTime,
                                                   (CHR1 *) au1TimeLastHeard);
                            CliPrintf (CliHandle, "%-10s", au1TimeLastHeard);
                            MEMSET (au1TimeLastHeard, 0, RFMGMT_MAX_DATE_LEN);
                        }
                        else
                            CliPrintf (CliHandle, "%-10s", au1HeardTime[0]);
                    }
                    else
                        CliPrintf (CliHandle, "%-10s", au1HeardTime[1]);
                }
            }
            MEMCPY (RogueAP, NextRogueAP, 6);
            nmhGetNextIndexFsRogueApTable (RogueAP, &NextRogueAP);
        }
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueShowFullSummaryRogueAp                                */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueShowFullSummaryRogueAp (tCliHandle CliHandle)
{
    tMacAddr            NextRogueAP;
    tMacAddr            RogueAP;
    tMacAddr            DefaultMac;
    UINT4               u4HeardTime = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    INT4                i4Status = 0;
    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];
    UINT1               au1header[][11] =
        { "BSSID", "Class", "State", "Last Heard" };
    UINT1               au1class[][13] =
        { "Unclassified", "Friendly", "Malicious" };
    UINT1               au1state[][8] = { "None", "Alert", "Contain" };
    UINT1               au1HeardTime[][10] = { "Not Heard", "Error" };

    MEMSET (au1TimeLastHeard, 0, RFMGMT_MAX_DATE_LEN);
    MEMSET (DefaultMac, 0, sizeof (tMacAddr));
    MEMSET (au1String, 0, CLI_MAC_TO_STR_LEN);

    if (nmhGetFirstIndexFsRogueApTable (&NextRogueAP) != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n%-19s %-12s %-7s %-5s\n",
                   au1header[0], au1header[1], au1header[2], au1header[3]);
        while (MEMCMP (RogueAP, NextRogueAP, 6) != 0)
        {
            CliMacToStr (NextRogueAP, au1String);
            CliPrintf (CliHandle, "\n%s", au1String);
            if (nmhGetFsRogueApClass (NextRogueAP, &i4Class) != SNMP_FAILURE)
            {
                if (1 == i4Class)
                    CliPrintf (CliHandle, "%-13s", au1class[0]);
                else if (2 == i4Class)
                    CliPrintf (CliHandle, "%-13s", au1class[1]);
                else if (3 == i4Class)
                    CliPrintf (CliHandle, "%-13s", au1class[2]);
            }
            if (nmhGetFsRogueApState (NextRogueAP, &i4State) != SNMP_FAILURE)
            {
                if (1 == i4State)
                    CliPrintf (CliHandle, "%-9s", au1state[0]);
                else if (2 == i4State)
                    CliPrintf (CliHandle, "%-9s", au1state[1]);
                else if (3 == i4State)
                    CliPrintf (CliHandle, "%-9s", au1state[2]);

            }
            if (nmhGetFsRogueApLastHeardTime (NextRogueAP, &u4HeardTime) !=
                SNMP_FAILURE)
            {
                if (nmhGetFsRogueApStatus (NextRogueAP, &i4Status) !=
                    SNMP_FAILURE)
                {
                    if (1 == i4Status)
                    {
                        UtlGetTimeStrForTicks (u4HeardTime,
                                               (CHR1 *) au1TimeLastHeard);
                        CliPrintf (CliHandle, "%-10s", au1TimeLastHeard);
                        MEMSET (au1TimeLastHeard, 0, RFMGMT_MAX_DATE_LEN);
                    }
                    else
                        CliPrintf (CliHandle, "%-10s", au1HeardTime[0]);
                }
                else
                    CliPrintf (CliHandle, "%-10s", au1HeardTime[1]);
            }
            MEMCPY (RogueAP, NextRogueAP, 6);
            nmhGetNextIndexFsRogueApTable (RogueAP, &NextRogueAP);
        }
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueShowSummaryRogueRule                                  */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCESS or CLI_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
RogueShowSummaryRogueRule (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE RuleName;
    INT4                i4Priority = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    INT4                i4Status = 0;
    UINT1               au1RogueRuleName[WSSMAC_MAX_SSID_LEN];
    UINT1               au1RogueNextRuleName[WSSMAC_MAX_SSID_LEN];
    INT4                i4Count = 1;
    UINT1               u1TotalRuleCount = 0;
    UINT1               au1header[][11] =
        { "Priority", "Rule Name", "Status", "Class", "State" };
    UINT1               au1class[][13] =
        { "Unclassified", "Friendly", "Malicious" };
    UINT1               au1state[][8] = { "None", "Alert", "Contain" };
    UINT1               au1status[][9] = { "Enabled", "Disabled" };

    MEMSET (au1RogueRuleName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (au1RogueNextRuleName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (&NextRuleName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RuleName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RuleName.pu1_OctetList = au1RogueRuleName;
    NextRuleName.pu1_OctetList = au1RogueNextRuleName;

    CliPrintf (CliHandle, "\n%-10s%-32s%-10s%-15s%s\n",
               au1header[0], au1header[1], au1header[2], au1header[3],
               au1header[4]);
    while (u1TotalRuleCount <= gu1RogueRuleCount)
    {
        if (nmhGetFirstIndexFsRogueRuleConfigTable (&NextRuleName) !=
            SNMP_FAILURE)
        {
            while (MEMCMP (RuleName.pu1_OctetList,
                           NextRuleName.pu1_OctetList,
                           WSSMAC_MAX_SSID_LEN) != 0)
            {
                if ((nmhGetFsRogueApRulePriOrderNum (&NextRuleName,
                                                     &i4Priority) ==
                     SNMP_SUCCESS) && (i4Priority == i4Count))
                {
                    CliPrintf (CliHandle, "\n%-10d", i4Priority);
                    CliPrintf (CliHandle, "%-32s", NextRuleName.pu1_OctetList);
                    if (nmhGetFsRogueApRuleStatus (&NextRuleName, &i4Status))
                    {
                        if (0 == i4Status)
                            CliPrintf (CliHandle, "%-10s", au1status[1]);
                        else
                            CliPrintf (CliHandle, "%-10s", au1status[0]);
                    }
                    if (nmhGetFsRogueApRuleClass (&NextRuleName, &i4Class))
                    {
                        if (1 == i4Class)
                            CliPrintf (CliHandle, "%-15s", au1class[0]);
                        else if (2 == i4Class)
                            CliPrintf (CliHandle, "%-15s", au1class[1]);
                        else if (3 == i4Class)
                            CliPrintf (CliHandle, "%-15s", au1class[2]);
                    }
                    if (nmhGetFsRogueApRuleState (&NextRuleName, &i4State))
                    {
                        if (1 == i4State)
                            CliPrintf (CliHandle, "%s", au1state[0]);
                        else if (2 == i4State)
                            CliPrintf (CliHandle, "%s", au1state[1]);
                        else if (3 == i4State)
                            CliPrintf (CliHandle, "%s", au1state[2]);
                    }
                    MEMCPY (RuleName.pu1_OctetList,
                            NextRuleName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
                    nmhGetNextIndexFsRogueRuleConfigTable (&RuleName,
                                                           &NextRuleName);
                    i4Count++;
                }
                else
                {
                    MEMCPY (RuleName.pu1_OctetList,
                            NextRuleName.pu1_OctetList, WSSMAC_MAX_SSID_LEN);
                    nmhGetNextIndexFsRogueRuleConfigTable (&RuleName,
                                                           &NextRuleName);
                    continue;
                }

            }
            CliPrintf (CliHandle, "\n");
            u1TotalRuleCount++;
        }
        else
        {
            u1TotalRuleCount++;
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueShowDetailedRogueRule                                 */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCCESS or CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RogueShowDetailedRogueRule (tCliHandle CliHandle, UINT1 *pu1RfName)
{
    tSNMP_OCTET_STRING_TYPE RfName;
    tSNMP_OCTET_STRING_TYPE SsidName;
    INT4                i4Priority = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    UINT4               u4Duration = 0;
    INT4                i4Protection = 0;
    INT4                i4Rssi = 0;
    UINT4               u4ClientCount = 0;
    INT4                i4Status = 0;
    UINT1               au1RogueRuleName[WSSMAC_MAX_SSID_LEN];
    UINT1               au1SsidName[WSSMAC_MAX_SSID_LEN];

    MEMSET (&au1RogueRuleName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (&au1SsidName, 0, WSSMAC_MAX_SSID_LEN);
    SsidName.pu1_OctetList = au1SsidName;
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = au1RogueRuleName;
    MEMCPY (RfName.pu1_OctetList, pu1RfName, WSSMAC_MAX_SSID_LEN);
    CliPrintf (CliHandle, "\r\nRF Name      : %s", pu1RfName);

    if (nmhGetFsRogueApRulePriOrderNum (&RfName, &i4Priority) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nPriority     : %d", i4Priority);
    }
    if (nmhGetFsRogueApRuleClass (&RfName, &i4Class) == SNMP_SUCCESS)
    {
        if (1 == i4Class)
            CliPrintf (CliHandle, "\r\nClass        : Unclassified");
        else if (2 == i4Class)
            CliPrintf (CliHandle, "\r\nClass        : Friendly");
        else if (3 == i4Class)
            CliPrintf (CliHandle, "\r\nClass        : Malicious");
    }
    if (nmhGetFsRogueApRuleState (&RfName, &i4State) == SNMP_SUCCESS)
    {
        if (1 == i4State)
            CliPrintf (CliHandle, "\r\nState        : None");
        else if (2 == i4State)
            CliPrintf (CliHandle, "\r\nState        : Alert");
        else if (3 == i4State)
            CliPrintf (CliHandle, "\r\nState        : Contain");
    }
    if (nmhGetFsRogueApRuleDuration (&RfName, &u4Duration) == SNMP_SUCCESS)
    {
        if (u4Duration != DEFAULT_DURATION)
            CliPrintf (CliHandle, "\r\nDuration     : %d secs", u4Duration);
    }
    if (nmhGetFsRogueApRuleProtectionType (&RfName, &i4Protection) ==
        SNMP_SUCCESS)
    {
        if (i4Protection != DEFAULT_PROTECTION)
        {
            if (i4Protection == CLI_ENCRYPTION_OPEN)
                CliPrintf (CliHandle, "\r\nEncryption   : Disabled");
            else if (i4Protection == CLI_ENCRYPTION_WPA)
                CliPrintf (CliHandle, "\r\nEncryption   : Enabled");
        }
    }
    if (nmhGetFsRogueApRuleSSID (&RfName, &SsidName) == SNMP_SUCCESS)
    {
        if (STRLEN (SsidName.pu1_OctetList) >= 1)
            CliPrintf (CliHandle, "\r\nSSID Name    : %s",
                       SsidName.pu1_OctetList);
    }
    if (nmhGetFsRogueApRuleRSSI (&RfName, &i4Rssi) == SNMP_SUCCESS)
    {
        if (i4Rssi != DEFAULT_RSSI)
            CliPrintf (CliHandle, "\r\nRSSI         : %d dBm", i4Rssi);
    }
    if (nmhGetFsRogueApRuleClientCount (&RfName, &u4ClientCount) ==
        SNMP_SUCCESS)
    {
        if (u4ClientCount != DEFAULT_CLIENT_COUNT)
            CliPrintf (CliHandle, "\r\nClient Count : %d", u4ClientCount);
    }

    if (nmhGetFsRogueApRuleStatus (&RfName, &i4Status) == SNMP_SUCCESS)
    {
        if (0 == i4Status)
            CliPrintf (CliHandle, "\r\nStatus       : Disabled");
        else
            CliPrintf (CliHandle, "\r\nStatus       : Enabled");
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueTrapStatusSet                                         */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCCESS or CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RogueTrapStatusSet (tCliHandle CliHandle, UINT1 u1TrapStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRogueApMaliciousTrapStatus (&u4ErrCode,
                                               (INT4) u1TrapStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid status \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsRogueApMaliciousTrapStatus ((INT4) u1TrapStatus);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueShowStatus                                            */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCCESS or CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RogueShowStatus (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RfName;
    UINT1               au1RogueRfName[WSSMAC_MAX_SSID_LEN];
    INT4                i4RogueApDetecttion = 0;
    UINT4               u4RogueApTimeout = 0;
    INT4                i4TrapStatus = 0;

    MEMSET (&au1RogueRfName, 0, WSSMAC_MAX_SSID_LEN);
    MEMSET (&RfName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RfName.pu1_OctetList = au1RogueRfName;

    if (nmhGetFsRfGroupName (&RfName) != SNMP_FAILURE)
    {
        if (strlen ((const char *) au1RogueRfName) == 0)
        {
            MEMCPY (au1RogueRfName, "Aricent123", strlen ("Aricent123"));
        }
        CliPrintf (CliHandle, "RF Group Name      : %s\r\n",
                   RfName.pu1_OctetList);

    }
    if (nmhGetFsRogueApDetecttion (&i4RogueApDetecttion) != SNMP_FAILURE)
    {
        if (i4RogueApDetecttion == RFMGMT_ROGUE_ENABLE)
        {
            CliPrintf (CliHandle, "Rogue AP Detection : Enabled\r\n",
                       i4RogueApDetecttion);
        }
        else
        {
            CliPrintf (CliHandle, "Rogue AP Detection : Disabled\r\n",
                       i4RogueApDetecttion);
        }
    }
    if (nmhGetFsRogueApTimeout (&u4RogueApTimeout) != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Rogue AP Timeout   : %d secs\r\n",
                   u4RogueApTimeout);
    }
    if (nmhGetFsRogueApMaliciousTrapStatus (&i4TrapStatus) != SNMP_FAILURE)
    {
        if (i4TrapStatus == RFMGMT_ROGUE_ENABLE)
        {
            CliPrintf (CliHandle, "Rogue Trap Status  : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Rogue Trap Status  : Disabled\r\n");
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function     : RogueSetTimeout                                            */
/*                                                                           */
/* Description  : Used to set the RF Group Name                              */
/*                                                                           */
/* Input        : RF Group Name                                              */
/*                                                                           */
/* Output       : CliHandle - Contains error messages                        */
/*                                                                           */
/* Returns      : CLI_SUCCESS or CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RogueSetTimeout (tCliHandle CliHandle, UINT4 u4RogueTimeout)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsRogueApTimeout (&u4ErrCode, u4RogueTimeout) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Timeout Value \r\n");
        return CLI_FAILURE;
    }
    nmhSetFsRogueApTimeout (u4RogueTimeout);

    return CLI_SUCCESS;
}

INT1
rogueGetFsApClassifiedBy (tMacAddr FsRogueApBSSID,
                          UINT1 *pu1RetValFsRogueApClassifiedBy)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApClassifiedBy = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu1RetValFsRogueApClassifiedBy =
        (UINT1) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApDB.u1RogueApClassifiedBy;

    return SNMP_SUCCESS;
}

UINT1
checkRuleNameExist (UINT1 *pu1RuleName)
{
    tRfMgmtRogueRuleDB  RfMgmtRogueRuleDB;
    tRfMgmtRogueRuleDB *pRfMgmtRogueRuleDB = &RfMgmtRogueRuleDB;

    MEMSET (pRfMgmtRogueRuleDB, 0, sizeof (tRfMgmtRogueRuleDB));
    MEMCPY (pRfMgmtRogueRuleDB->u1RogueApRuleName, pu1RuleName,
            WSSMAC_MAX_SSID_LEN);

    pRfMgmtRogueRuleDB =
        RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                   (tRBElem *) & RfMgmtRogueRuleDB);

    if (pRfMgmtRogueRuleDB != NULL)
    {
        return RFMGMT_FAILURE;
    }
    return RFMGMT_SUCCESS;

}
#endif

#endif
