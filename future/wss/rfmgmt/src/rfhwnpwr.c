/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfhwnpwr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the wrapper for Hardware API's 
 *              w.r.t rfmgmt module 
 *                            
 ********************************************************************/

#include "rfminc.h"
#include "rfmgmtnp.h"
#include "rfhwnpwr.h"
#include "nputil.h"

#ifdef  NPAPI_WANTED

/*****************************************************************************
 * Function Name      : RfMgmtNpWrHwProgram                                  *
 *                                                                           *
 * Description        : This function takes care of calling appropriate NP   *
 *                      call using the tRfMgmtNpModInfo                      *
 *                                                                           *
 * Input(s)           : pFsHwNp - pointer the input structure                *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : FNP_SUCCESS, if initialization succeeds              *
 *                      FNP_FAILURE, otherwise                               *
 *****************************************************************************/

PUBLIC UINT1
RfMgmtNpWrHwProgram  (tFsHwNp   * pFsHwNp)
{
    tRfMgmtNpModInfo    *pRfMgmtNpModInfo = NULL;
    UINT1               u1RetVal = FNP_FAILURE;
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               u4Opcode = 0;
    INT4                *pi4ErrVal = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pRfMgmtNpModInfo = &(pFsHwNp->RfMgmtNpModInfo);

    if (NULL == pRfMgmtNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_RFMGMT_SCAN_NEIGHBR_MSG:
            {
                tRfMgmtNpWrHwScanNeighInfo   *pEntry = NULL;
                pEntry = 
                    &pRfMgmtNpModInfo->RadioIfNpWrHwScanNeighInfo;

                if (NULL == pEntry)
                {
                    u1RetVal = FNP_FAILURE;
                    break;
                }

                u1RetVal = FsRfMgmtHwScanNeighborInfo (pEntry);
                break;
            }
        case FS_RFMGMT_GET_NEIGHBOR_INFO:
            {
                tRfMgmtNpWrHwGetNeighInfo   *pEntry = NULL;
                pEntry = 
                    &pRfMgmtNpModInfo->RadioIfNpWrHwGetNeighInfo;

                if (NULL == pEntry)
                {
                    u1RetVal = FNP_FAILURE;
                    break;
                }

                u1RetVal = FsRfMgmtHwGetNeighborInfo (pEntry);
                break;
            }
        case FS_RFMGMT_SCAN_CLIENT_MSG:
            {
                tRfMgmtNpWrHwScanClientInfo   *pEntry = NULL;
                pEntry = 
                    &pRfMgmtNpModInfo->RadioIfNpWrHwScanClientInfo;

                if (NULL == pEntry)
                {
                    u1RetVal = FNP_FAILURE;
                    break;
                }

                u1RetVal = FsRfMgmtHwScanClientInfo (pEntry);
                break;
            }
        case FS_RFMGMT_GET_CLIENT_INFO:
            {
                tRfMgmtNpWrHwScanClientInfo   *pEntry = NULL;
                pEntry = 
                    &pRfMgmtNpModInfo->RadioIfNpWrHwScanClientInfo;

                if (NULL == pEntry)
                {
                    u1RetVal = FNP_FAILURE;
                    break;
                }

                u1RetVal = FsRfMgmtHwGetClientInfo (pEntry);
                break;
            }
    }

    return (u1RetVal);
}

#endif 
