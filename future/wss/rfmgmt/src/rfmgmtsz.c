/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmgmtsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains RFMGMT Sizing parameters
 *
 ********************************************************************/

#define _RFMGMTSZ_C
#include "rfminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RfmgmtSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RFMGMT_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsRFMGMTSizingParams[i4SizingId].u4StructSize,
                              FsRFMGMTSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(RFMGMTMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RfmgmtSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RfmgmtSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRFMGMTSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RFMGMTMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RfmgmtSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RFMGMT_MAX_SIZING_ID; i4SizingId++)
    {
        if (RFMGMTMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RFMGMTMemPoolIds[i4SizingId]);
            RFMGMTMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
