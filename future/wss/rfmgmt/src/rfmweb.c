/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *  $Id: rfmweb.c,v 1.5 2017/12/08 10:16:25 siva Exp $  *                                    
 *
 * Description: Routines for RF MANAGEMENT WEB Module
 *******************************************************************/

#ifndef _RFMGMT_WEB_C_
#define _RFMGMT_WEB_C_

#include "webiss.h"
#include "isshttp.h"
#include "webinc.h"
#include "issmacro.h"
#include "snmputil.h"
#include "utilcli.h"

#include "capwapcliinc.h"
#include "capwapclidefg.h"
#include "wssifcapdb.h"
#include "wssifradiodb.h"
#include "wssifradioproto.h"
#include "wssifcapproto.h"
#include "wsscfgwlanproto.h"
#include "wsscfgprotg.h"
#include "wsscfgprot.h"

#include "capwapcli.h"
#include "rfmcli.h"
#include "rfminc.h"
#include "wsscfgcli.h"
#include "wsscfglwg.h"
#include "std802lw.h"
#include "rfmconst.h"
#include "radioifextn.h"
#define WSS_WEB_TRACE_ENABLE      0
#define WSS_WEB_TRC               if(WSS_WEB_TRACE_ENABLE)  {printf}
#define RFMGMT_AP_ALL 3
#define RFMGMT_AP_PROFILE 2
#define RADIO_ENABLE 1
#define RADIO_DISABLE 2

INT4                gi4ApOption;
UINT1               gau1ApName[OCTETSTR_SIZE + 1];

/***************************************************************************
*                                                                         *
*     Function Name : RfmGuiMacToStr                                         *
*                                                                         *
*     Description   : This function converts the given mac address        *
*                     in to a string of form aa:aa:aa:aa:aa:aa            *
*                                                                         *
*     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
*                     pu1Temp    : Pointer to the converted mac address   *
*                                  string.(The string must be of length   *
*                                  21 bytes minimum)                      *
*                                                                         *
*     Output(s)     : NULL                                                *
*                                                                         *
*     Returns       : NULL                                                *
*                                                                         *
***************************************************************************/

VOID
RfmGuiMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), "%s", "");

}

/*********************************************************************
 *  Function Name : IssRfmRadioANTpcPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN TPC Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANTpcPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANTpcPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANTpcPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANTpcPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN TPC page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANTpcPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    INT4                i4SNRThreshold = 0;
    UINT4               u4TpcLastUpdatedTime = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4TpcUpdatedTime = 0;
    STRCPY (pHttp->au1KeyString, "TpcInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "plam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "POWER_THRESHOLD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmSNRThreshold (i4GetRadioType, &i4SNRThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNRThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LPLA_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcLastUpdatedTime (i4GetRadioType, &u4TpcLastUpdatedTime);
    if (u4TpcLastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4TpcUpdatedTime = u4CurrentTime - u4TpcLastUpdatedTime;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcUpdatedTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANTpcPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN TPC page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANTpcPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4TpcMode = 1;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    UINT4               u4TpcSelectionMode = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4TpcUpdate = RFMGMT_TPC_UPDATE;
    UINT4               u4SNRThreshold = 0;
    INT4                i4AssignmentMethod = 0;
    INT4                i4RowStatus = 0;

    STRCPY (pHttp->au1Name, "PLAM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AssignmentMethod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_AUTO)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_AUTO;
    }
    else if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_ONCE)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_ONCE;
    }
    else if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_OFF)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_OFF;
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    if (nmhTestv2FsRrmTpcMode (&u4ErrCode, i4GetRadioType, (INT4) u4TpcMode) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
        return;

    }

    if (nmhSetFsRrmTpcMode (i4GetRadioType, (INT4) u4TpcMode) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to configure Tpc Mode ");
        return;

    }

    if (nmhTestv2FsRrmTpcSelectionMode (&u4ErrCode, i4GetRadioType,
                                        (INT4) u4TpcSelectionMode) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
        return;
    }

    if (nmhSetFsRrmTpcSelectionMode (i4GetRadioType, (INT4) u4TpcSelectionMode)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Tpc Selection Mode ");
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Update") == 0)
    {

        if (nmhTestv2FsRrmTpcUpdate (&u4ErrCode, i4GetRadioType,
                                     (INT4) u4TpcUpdate) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcUpdate (i4GetRadioType, (INT4) u4TpcUpdate)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Update ");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "TPC_INTERVAL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TpcInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "POWER_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SNRThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        if (u4TpcSelectionMode == RFMGMT_TPC_SELECTION_AUTO)
        {

            if (nmhTestv2FsRrmTpcInterval (&u4ErrCode, i4GetRadioType,
                                           u4TpcInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }

            if (nmhSetFsRrmTpcInterval (i4GetRadioType,
                                        u4TpcInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Tpc Interval ");
                return;
            }
        }

        if (u4TpcSelectionMode != RFMGMT_TPC_SELECTION_OFF)
        {
            if (nmhTestv2FsRrmSNRThreshold (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4SNRThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmSNRThreshold (i4GetRadioType, (INT4) u4SNRThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure SNR Threshold ");
                return;
            }
        }
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }

    IssRfmRadioANTpcPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANDcaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN DCA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANDcaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANDcaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANDcaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANDcaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN DCA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANDcaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4RssiThreshold = 0;
    INT4                i4DcaSelection = 0;
    INT4                i4DcaSensitivity = 0;
    INT4                i4ConsiderExternalAPs = 0;
    UINT4               u4ClientThreshold = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4LastUpdatedTime = 0;
    UINT4               u4NeighborCountThreshold = 0;
    UINT1               au1AllowedList[RFMGMT_MAX_CHANNELA];
    tSNMP_OCTET_STRING_TYPE AllowedList;
    UINT1               u1Channel = 0;
    UINT1               au1ChannelList[RFMGMT_MAX_CHANNELA + 1];
    UINT4               u4CurrentTime = 0;
    UINT4               u4DcaUpdatedTime = 0;

    MEMSET (au1AllowedList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (au1ChannelList, 0, RFMGMT_MAX_CHANNELA + 1);
    MEMSET (&AllowedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    AllowedList.pu1_OctetList = au1AllowedList;

    STRCPY (pHttp->au1KeyString, "DcaInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaInterval (i4GetRadioType, &u4DcaInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DcaInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "cam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4DcaSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LCAT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmLastUpdatedTime (i4GetRadioType, &u4LastUpdatedTime);
    if (u4LastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4DcaUpdatedTime = u4CurrentTime - u4LastUpdatedTime;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DcaUpdatedTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ChannelSensitivity_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaSensitivity (i4GetRadioType, &i4DcaSensitivity);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaSensitivity);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "RSSI_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmRSSIThreshold (i4GetRadioType, &i4RssiThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RssiThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClientThreshold_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmClientThreshold (i4GetRadioType, &u4ClientThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ClientThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ConsiderExtAps_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmConsiderExternalAPs (i4GetRadioType, &i4ConsiderExternalAPs);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ConsiderExternalAPs);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "NeighCountThreshold_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmNeighborCountThreshold (i4GetRadioType,
                                       &u4NeighborCountThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NeighborCountThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "allowedchannels_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmAllowedChannels (i4GetRadioType, &AllowedList);

    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
    {
        if (au1AllowedList[u1Channel] != 0x0)
        {
            au1ChannelList[u1Channel] = 'a';
        }
        else
        {
            au1ChannelList[u1Channel] = 'b';
        }
    }

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1ChannelList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANDcaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN DCA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANDcaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4UpdateChannel = RFMGMT_DCA_UPDATE_CHANNEL;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    UINT4               u4DcaMode = 0;
    UINT4               u4DcaSelectionMode = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4DcaSensitivity = 0;
    INT4                i4AssignmentMethod = 0;
    INT4                i4RssiThreshold = 0;
    INT4                i4ConsiderExtAps = 0;
    UINT4               u4ClientThreshold = 0;
    UINT4               u4NeighborCountThreshold = 0;
    INT4                i4RowStatus = 0;

    UINT4               u4ChannelNumber = 0;
    UINT1               au1ChannelList[RFMGMT_MAX_CHANNELA + 2];
    UINT1               au1Dot11AllowedChannelList[RFMGMT_MAX_CHANNELA + 1];
    UINT1               au1Dot11UnusedChannelList[RFMGMT_MAX_CHANNELA + 1];
    tSNMP_OCTET_STRING_TYPE AllowedChannelList;
    tSNMP_OCTET_STRING_TYPE UnusedChannelList;

    MEMSET (&AllowedChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UnusedChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Dot11AllowedChannelList, 0, RFMGMT_MAX_CHANNELA + 1);
    MEMSET (&au1Dot11UnusedChannelList, 0, RFMGMT_MAX_CHANNELA + 1);
    MEMSET (au1ChannelList, 0, RFMGMT_MAX_CHANNELA + 2);
    AllowedChannelList.pu1_OctetList = au1Dot11AllowedChannelList;
    UnusedChannelList.pu1_OctetList = au1Dot11UnusedChannelList;

    STRCPY (pHttp->au1Name, "CAM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AssignmentMethod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_AUTO)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_AUTO;
    }
    else if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_ONCE)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_ONCE;
    }
    else if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_OFF)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_OFF;
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Update") == 0)
    {
        if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }
        if (nmhSetFsRrmDcaMode (i4GetRadioType, (INT4) u4DcaMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Dca Mode ");
            return;
        }

        if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaSelectionMode (i4GetRadioType,
                                         (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Dca Selection Mode ");
            return;
        }

        if (nmhTestv2FsRrmUpdateChannel (&u4ErrCode, (INT4) i4GetRadioType,
                                         (INT4) u4UpdateChannel) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }
        if (nmhSetFsRrmRowStatus (i4GetRadioType, ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }

        if (nmhSetFsRrmUpdateChannel ((INT4) i4GetRadioType,
                                      (INT4) u4UpdateChannel) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to update channel");
            return;
        }
    }

    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "DCA_INTERVAL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DcaInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CHANNEL_SENSITIVITY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DcaSensitivity = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "RSSI");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RssiThreshold = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CLIENT_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ClientThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CONSIDER_EXT_APS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ConsiderExtAps = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "NEIGHBOR_COUNT_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4NeighborCountThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "Allowedchannels");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ChannelList, pHttp->au1Value, RFMGMT_MAX_CHANNELA + 1);
        au1ChannelList[RFMGMT_MAX_CHANNELA + 1] = '\0';

        if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaMode (i4GetRadioType, (INT4) u4DcaMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Dca Mode ");
            return;
        }

        if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaSelectionMode (i4GetRadioType,
                                         (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Dca Selection Mode ");
            return;
        }

        if (u4DcaSelectionMode == RFMGMT_DCA_SELECTION_AUTO)
        {

            if (nmhTestv2FsRrmDcaInterval (&u4ErrCode, i4GetRadioType,
                                           u4DcaInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmDcaInterval (i4GetRadioType, u4DcaInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure DCA Interval ");
                return;
            }
        }
        if (u4DcaSelectionMode != RFMGMT_DCA_SELECTION_OFF)
        {
            if (nmhTestv2FsRrmDcaSensitivity (&u4ErrCode, i4GetRadioType,
                                              (INT4) u4DcaSensitivity) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }

            if (nmhSetFsRrmDcaSensitivity
                (i4GetRadioType, (INT4) u4DcaSensitivity) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure DCA sensitivity ");
                return;
            }

            if (nmhTestv2FsRrmRSSIThreshold (&u4ErrCode, i4GetRadioType,
                                             i4RssiThreshold) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }

            if (nmhSetFsRrmRSSIThreshold (i4GetRadioType, i4RssiThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure RSSI Threshold ");
                return;
            }

            if (nmhTestv2FsRrmClientThreshold (&u4ErrCode, i4GetRadioType,
                                               u4ClientThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }

            if (nmhSetFsRrmClientThreshold (i4GetRadioType, u4ClientThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Client Threshold");
                return;
            }
            if (nmhTestv2FsRrmConsiderExternalAPs (&u4ErrCode, i4GetRadioType,
                                                   i4ConsiderExtAps) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmConsiderExternalAPs
                (i4GetRadioType, i4ConsiderExtAps) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Consider External APs");
                return;
            }
            if (nmhTestv2FsRrmNeighborCountThreshold
                (&u4ErrCode, i4GetRadioType,
                 u4NeighborCountThreshold) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmNeighborCountThreshold (i4GetRadioType,
                                                   u4NeighborCountThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Neighbor Count Threshold");
                return;
            }

            for (u4ChannelNumber = 0; u4ChannelNumber < RFMGMT_MAX_CHANNELA;
                 u4ChannelNumber++)
            {
                if ((au1ChannelList[u4ChannelNumber + 1]) == 'b')
                {
                    AllowedChannelList.pu1_OctetList[u4ChannelNumber] = 0;
                    UnusedChannelList.pu1_OctetList[u4ChannelNumber] =
                        (UINT1) gau1Dot11aRegDomainChannelList[u4ChannelNumber];
                }
                else if ((au1ChannelList[u4ChannelNumber + 1]) == 'a')
                {
                    AllowedChannelList.pu1_OctetList[u4ChannelNumber] =
                        (UINT1) gau1Dot11aRegDomainChannelList[u4ChannelNumber];
                    UnusedChannelList.pu1_OctetList[u4ChannelNumber] = 0;
                }
            }
            if (nmhTestv2FsRrmAllowedChannels (&u4ErrCode, i4GetRadioType,
                                               &AllowedChannelList) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmAllowedChannels (i4GetRadioType, &AllowedChannelList)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure allowed channels ");
                return;
            }
            if (nmhTestv2FsRrmUnusedChannels (&u4ErrCode, i4GetRadioType,
                                              &UnusedChannelList) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmUnusedChannels (i4GetRadioType, &UnusedChannelList)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure unused channels ");
                return;
            }
        }
    }
    else if (STRCMP (pHttp->au1Array, "ClearNeigh") == 0)
    {
        nmhSetFsRrmClearNeighborInfo (i4GetRadioType, OSIX_TRUE);
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioANDcaPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANDpaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN DPA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANDpaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANDpaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANDpaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANDpaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN DPA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANDpaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4RowSatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4DpaExecutionCount = 0;
    INT4                i4PowerThreshold = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) == SNMP_SUCCESS)
    {
        nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcMode);
        nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);

        if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
        {

            if (i4TpcSelection == RFMGMT_TPC_SELECTION_OFF)
            {

                STRCPY (pHttp->au1KeyString, "plam_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
            else
            {
                STRCPY (pHttp->au1KeyString, "plam_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "AUTO");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "plam_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

        STRCPY (pHttp->au1KeyString, "DPA_INTERVAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "DPA_EXECUTION_COUNT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmDpaExecutionCount (i4GetRadioType, &u4DpaExecutionCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DpaExecutionCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "POWER_THRESHOLD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmPowerThreshold (i4GetRadioType, &i4PowerThreshold);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PowerThreshold);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) ("No entry found\n"));
        return;
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioANDpaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN DPA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANDpaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4PowerThreshold = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "POWER_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PowerThreshold = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmPowerThreshold (&u4ErrCode, i4GetRadioType,
                                          i4PowerThreshold) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmPowerThreshold (i4GetRadioType, i4PowerThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Power Threshold ");
                return;
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioANDpaPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioANShaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN SHA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANShaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANShaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANShaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANShaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN SHA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANShaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4RowSatus = 0;
    INT4                i4SHAStatus = 0;
    UINT4               u4SHAInterval = 0;
    UINT4               u4SHAExecutionCount = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "sha_status_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAStatus (i4GetRadioType, &i4SHAStatus);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SHAStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "sha_interval_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAInterval (i4GetRadioType, &u4SHAInterval);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SHAInterval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "sha_execution_count_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAExecutionCount (i4GetRadioType, &u4SHAExecutionCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SHAExecutionCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) ("No entry found\n"));
        return;
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANShaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN SHA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANShaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4ShaStatus = 0;
    UINT4               u4ShaInterval = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "sha_status");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ShaStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmSHAStatus (&u4ErrCode, i4GetRadioType,
                                     i4ShaStatus) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmSHAStatus (i4GetRadioType, i4ShaStatus)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to configure Sha Status ");
                return;
            }
        }

        STRCPY (pHttp->au1Name, "sha_interval");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ShaInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmSHAInterval (&u4ErrCode, i4GetRadioType,
                                       u4ShaInterval) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmSHAInterval (i4GetRadioType, u4ShaInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Sha Interval ");
                return;
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioANShaPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNTpcPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN TPC Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNTpcPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNTpcPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNTpcPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNTpcPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN TPC page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNTpcPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    INT4                i4SNRThreshold = 0;
    UINT4               u4TpcLastUpdatedTime = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4TpcUpdatedTime = 0;

    STRCPY (pHttp->au1KeyString, "TpcInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "plam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "POWER_THRESHOLD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmSNRThreshold (i4GetRadioType, &i4SNRThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNRThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LPLA_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcLastUpdatedTime (i4GetRadioType, &u4TpcLastUpdatedTime);
    if (u4TpcLastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4TpcUpdatedTime = u4CurrentTime - u4TpcLastUpdatedTime;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcUpdatedTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "plam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNTpcPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN TPC page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNTpcPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4TpcMode = 1;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    UINT4               u4TpcSelectionMode = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4TpcUpdate = RFMGMT_TPC_UPDATE;
    UINT4               u4SNRThreshold = 0;
    INT4                i4AssignmentMethod = 0;
    INT4                i4RowStatus = 0;

    STRCPY (pHttp->au1Name, "PLAM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AssignmentMethod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_AUTO)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_AUTO;
    }
    else if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_ONCE)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_ONCE;
    }
    else if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_OFF)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_OFF;
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Update") == 0)
    {
        if (nmhTestv2FsRrmTpcMode (&u4ErrCode, i4GetRadioType, (INT4) u4TpcMode)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }
        if (nmhSetFsRrmTpcMode (i4GetRadioType, (INT4) u4TpcMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Mode ");
            return;

        }

        if (nmhTestv2FsRrmTpcSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcSelectionMode (i4GetRadioType,
                                         (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Tpc Selection Mode ");
            return;
        }

        if (nmhTestv2FsRrmTpcUpdate (&u4ErrCode, i4GetRadioType,
                                     (INT4) u4TpcUpdate) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcUpdate (i4GetRadioType, (INT4) u4TpcUpdate)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Update ");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "TPC_INTERVAL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TpcInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "POWER_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SNRThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        if (nmhTestv2FsRrmTpcMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4TpcMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }

        if (nmhSetFsRrmTpcMode (i4GetRadioType, (INT4) u4TpcMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Mode ");
            return;

        }

        if (nmhTestv2FsRrmTpcSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcSelectionMode (i4GetRadioType,
                                         (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Tpc Selection Mode ");
            return;
        }

        if (u4TpcSelectionMode == RFMGMT_TPC_SELECTION_AUTO)
        {
            if (nmhTestv2FsRrmTpcInterval (&u4ErrCode, i4GetRadioType,
                                           u4TpcInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }

            if (nmhSetFsRrmTpcInterval (i4GetRadioType,
                                        u4TpcInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Tpc Interval ");
                return;
            }
        }
        if (u4TpcSelectionMode != RFMGMT_TPC_SELECTION_OFF)
        {
            if (nmhTestv2FsRrmSNRThreshold (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4SNRThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmSNRThreshold (i4GetRadioType, (INT4) u4SNRThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure SNR Threshold ");
                return;
            }

        }

    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioBGNTpcPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNDcaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN DCA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNDcaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNDcaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNDcaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNDcaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN DCA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNDcaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4DcaSelection = 0;
    INT4                i4DcaSensitivity = 0;
    INT4                i4ConsiderExternalAPs = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4LastUpdatedTime = 0;
    UINT4               u4NeighborCountThreshold = 0;
    INT4                i4RssiThreshold = 0;
    UINT4               u4ClientThreshold = 0;
    UINT1               au1AllowedList[RFMGMT_MAX_CHANNELB];
    tSNMP_OCTET_STRING_TYPE AllowedList;
    UINT1               u1Channel = 0;
    UINT1               au1ChannelList[RFMGMT_MAX_CHANNELB + 1];
    UINT4               u4CurrentTime = 0;
    UINT4               u4DcaUpdatedTime = 0;

    MEMSET (au1AllowedList, 0, RFMGMT_MAX_CHANNELB);
    MEMSET (au1ChannelList, 0, RFMGMT_MAX_CHANNELB + 1);
    MEMSET (&AllowedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    AllowedList.pu1_OctetList = au1AllowedList;

    STRCPY (pHttp->au1KeyString, "DcaInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaInterval (i4GetRadioType, &u4DcaInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DcaInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "cam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4DcaSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LCAT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmLastUpdatedTime (i4GetRadioType, &u4LastUpdatedTime);
    if (u4LastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4DcaUpdatedTime = u4CurrentTime - u4LastUpdatedTime;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DcaUpdatedTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ChannelSensitivity_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaSensitivity (i4GetRadioType, &i4DcaSensitivity);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaSensitivity);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "RSSI_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmRSSIThreshold (i4GetRadioType, &i4RssiThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RssiThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClientThreshold_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmClientThreshold (i4GetRadioType, &u4ClientThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ClientThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ConsiderExtAps_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmConsiderExternalAPs (i4GetRadioType, &i4ConsiderExternalAPs);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ConsiderExternalAPs);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "NeighCountThreshold_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmNeighborCountThreshold (i4GetRadioType,
                                       &u4NeighborCountThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NeighborCountThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "allowedchannels_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmAllowedChannels (i4GetRadioType, &AllowedList);

    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
    {
        if (au1AllowedList[u1Channel] != 0x0)
        {
            au1ChannelList[u1Channel] = 'a';
        }
        else
        {
            au1ChannelList[u1Channel] = 'b';
        }
    }

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1ChannelList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNDcaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN DCA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNDcaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4UpdateChannel = RFMGMT_DCA_UPDATE_CHANNEL;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    UINT4               u4DcaMode = 0;
    UINT4               u4DcaSelectionMode = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4DcaSensitivity = 0;
    INT4                i4AssignmentMethod = 0;
    INT4                i4RssiThreshold = 0;
    INT4                i4ConsiderExtAps = 0;
    UINT4               u4ClientThreshold = 0;
    UINT4               u4NeighborCountThreshold = 0;
    INT4                i4RowStatus = 0;

    UINT4               u4ChannelNumber = 0;
    UINT1               au1ChannelList[RFMGMT_MAX_CHANNELB + 2];
    UINT1               au1Dot11AllowedChannelList[RFMGMT_MAX_CHANNELB + 1];
    UINT1               au1Dot11UnusedChannelList[RFMGMT_MAX_CHANNELB + 1];
    tSNMP_OCTET_STRING_TYPE AllowedChannelList;
    tSNMP_OCTET_STRING_TYPE UnusedChannelList;

    MEMSET (&AllowedChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UnusedChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Dot11AllowedChannelList, 0, RFMGMT_MAX_CHANNELB + 1);
    MEMSET (&au1Dot11UnusedChannelList, 0, RFMGMT_MAX_CHANNELB + 1);
    MEMSET (au1ChannelList, 0, RFMGMT_MAX_CHANNELB + 2);
    AllowedChannelList.pu1_OctetList = au1Dot11AllowedChannelList;
    UnusedChannelList.pu1_OctetList = au1Dot11UnusedChannelList;

    STRCPY (pHttp->au1Name, "CAM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AssignmentMethod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_AUTO)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_AUTO;
    }
    else if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_ONCE)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_ONCE;
    }
    else if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_OFF)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_OFF;
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Update") == 0)
    {
        if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaMode (i4GetRadioType, (INT4) u4DcaMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Dca Mode ");
            return;
        }

        if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaSelectionMode (i4GetRadioType,
                                         (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Dca Selection Mode ");
            return;
        }

        if (nmhTestv2FsRrmUpdateChannel (&u4ErrCode, (INT4) i4GetRadioType,
                                         (INT4) u4UpdateChannel) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }
        if (nmhSetFsRrmRowStatus (i4GetRadioType, ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }

        if (nmhSetFsRrmUpdateChannel ((INT4) i4GetRadioType,
                                      (INT4) u4UpdateChannel) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to update channel");
            return;
        }
    }

    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "DCA_INTERVAL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DcaInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CHANNEL_SENSITIVITY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DcaSensitivity = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "RSSI");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RssiThreshold = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CLIENT_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ClientThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CONSIDER_EXT_APS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ConsiderExtAps = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "NEIGHBOR_COUNT_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4NeighborCountThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "Allowedchannels");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ChannelList, pHttp->au1Value, RFMGMT_MAX_CHANNELB + 1);
        au1ChannelList[RFMGMT_MAX_CHANNELB + 1] = '\0';

        if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaMode (i4GetRadioType, (INT4) u4DcaMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Dca Mode ");
            return;
        }

        if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaSelectionMode
            (i4GetRadioType, (INT4) u4DcaSelectionMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Dca Selection Mode ");
            return;
        }

        if (u4DcaSelectionMode == RFMGMT_DCA_SELECTION_AUTO)
        {
            if (nmhTestv2FsRrmDcaInterval (&u4ErrCode, i4GetRadioType,
                                           u4DcaInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmDcaInterval (i4GetRadioType, u4DcaInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure DCA Interval ");
                return;
            }
        }
        if (u4DcaSelectionMode != RFMGMT_DCA_SELECTION_OFF)
        {
            if (nmhTestv2FsRrmDcaSensitivity (&u4ErrCode, i4GetRadioType,
                                              (INT4) u4DcaSensitivity) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmDcaSensitivity
                (i4GetRadioType, (INT4) u4DcaSensitivity) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure DCA sensitivity ");
                return;
            }
            if (nmhTestv2FsRrmRSSIThreshold (&u4ErrCode, i4GetRadioType,
                                             i4RssiThreshold) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmRSSIThreshold (i4GetRadioType, i4RssiThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure RSSI Threshold ");
                return;
            }
            if (nmhTestv2FsRrmClientThreshold (&u4ErrCode, i4GetRadioType,
                                               u4ClientThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmClientThreshold (i4GetRadioType, u4ClientThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Client Threshold");
                return;
            }
            if (nmhTestv2FsRrmConsiderExternalAPs (&u4ErrCode, i4GetRadioType,
                                                   i4ConsiderExtAps) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmConsiderExternalAPs
                (i4GetRadioType, i4ConsiderExtAps) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Consider External APs");
                return;
            }
            if (nmhTestv2FsRrmNeighborCountThreshold
                (&u4ErrCode, i4GetRadioType,
                 u4NeighborCountThreshold) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmNeighborCountThreshold (i4GetRadioType,
                                                   u4NeighborCountThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Neighbor Count Threshold");
                return;
            }

            for (u4ChannelNumber = 0; u4ChannelNumber < RFMGMT_MAX_CHANNELB;
                 u4ChannelNumber++)
            {
                if ((au1ChannelList[u4ChannelNumber + 1]) == 'b')
                {
                    AllowedChannelList.pu1_OctetList[u4ChannelNumber] = 0;
                    UnusedChannelList.pu1_OctetList[u4ChannelNumber] =
                        (UINT1) (u4ChannelNumber + 1);
                }
                else if ((au1ChannelList[u4ChannelNumber + 1]) == 'a')
                {
                    AllowedChannelList.pu1_OctetList[u4ChannelNumber] =
                        (UINT1) (u4ChannelNumber + 1);
                    UnusedChannelList.pu1_OctetList[u4ChannelNumber] = 0;
                }
            }
            if (nmhTestv2FsRrmAllowedChannels (&u4ErrCode, i4GetRadioType,
                                               &AllowedChannelList) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmAllowedChannels (i4GetRadioType, &AllowedChannelList)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure allowed channels ");
                return;
            }
            if (nmhTestv2FsRrmUnusedChannels (&u4ErrCode, i4GetRadioType,
                                              &UnusedChannelList) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmUnusedChannels (i4GetRadioType, &UnusedChannelList)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure unused channels ");
                return;
            }
        }

    }
    else if (STRCMP (pHttp->au1Array, "ClearNeigh") == 0)
    {
        nmhSetFsRrmClearNeighborInfo (i4GetRadioType, OSIX_TRUE);
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioBGNDcaPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNDpaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN DPA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNDpaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNDpaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNDpaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNDpaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN DPA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNDpaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4RowSatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4DpaExecutionCount = 0;
    INT4                i4PowerThreshold = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) == SNMP_SUCCESS)
    {
        nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcMode);
        nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);

        if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
        {

            if (i4TpcSelection == RFMGMT_TPC_SELECTION_OFF)
            {

                STRCPY (pHttp->au1KeyString, "plam_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
            else
            {
                STRCPY (pHttp->au1KeyString, "plam_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "AUTO");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "plam_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

        STRCPY (pHttp->au1KeyString, "DPA_INTERVAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "DPA_EXECUTION_COUNT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmDpaExecutionCount (i4GetRadioType, &u4DpaExecutionCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DpaExecutionCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "POWER_THRESHOLD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmPowerThreshold (i4GetRadioType, &i4PowerThreshold);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PowerThreshold);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) ("No entry found\n"));
        return;
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNDpaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN DPA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNDpaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4PowerThreshold = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "POWER_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PowerThreshold = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmPowerThreshold (&u4ErrCode, i4GetRadioType,
                                          i4PowerThreshold) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmPowerThreshold (i4GetRadioType, i4PowerThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Power Threshold ");
                return;
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioBGNDpaPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNShaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN SHA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNShaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNShaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNShaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNShaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN SHA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNShaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4RowSatus = 0;
    INT4                i4SHAStatus = 0;
    UINT4               u4SHAInterval = 0;
    UINT4               u4SHAExecutionCount = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "sha_status_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAStatus (i4GetRadioType, &i4SHAStatus);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SHAStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "sha_interval_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAInterval (i4GetRadioType, &u4SHAInterval);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SHAInterval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "sha_execution_count_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAExecutionCount (i4GetRadioType, &u4SHAExecutionCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SHAExecutionCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) ("No entry found\n"));
        return;
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNShaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN SHA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNShaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4ShaStatus = 0;
    UINT4               u4ShaInterval = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "sha_status");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ShaStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmSHAStatus (&u4ErrCode, i4GetRadioType,
                                     i4ShaStatus) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmSHAStatus (i4GetRadioType, i4ShaStatus)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to configure Sha Status ");
                return;
            }
        }

        STRCPY (pHttp->au1Name, "sha_interval");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ShaInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmSHAInterval (&u4ErrCode, i4GetRadioType,
                                       u4ShaInterval) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmSHAInterval (i4GetRadioType, u4ShaInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Sha Interval ");
                return;
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioBGNShaPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioGTpcPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G TPC Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGTpcPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGTpcPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGTpcPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGTpcPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G TPC page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGTpcPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    INT4                i4SNRThreshold = 0;
    UINT4               u4TpcLastUpdatedTime = 0;
    UINT4               u4CurrentTime = 0;
    UINT4               u4TpcUpdatedTime = 0;
    STRCPY (pHttp->au1KeyString, "TpcInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "plam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "POWER_THRESHOLD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmSNRThreshold (i4GetRadioType, &i4SNRThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNRThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LPLA_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcLastUpdatedTime (i4GetRadioType, &u4TpcLastUpdatedTime);
    if (u4TpcLastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4TpcUpdatedTime = u4CurrentTime - u4TpcLastUpdatedTime;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcUpdatedTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "plam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGTpcPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G TPC page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGTpcPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4TpcMode = 1;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    UINT4               u4TpcSelectionMode = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4TpcUpdate = RFMGMT_TPC_UPDATE;
    UINT4               u4SNRThreshold = 0;
    INT4                i4AssignmentMethod = 0;
    INT4                i4RowStatus = 0;

    STRCPY (pHttp->au1Name, "PLAM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AssignmentMethod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_AUTO)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_AUTO;
    }
    else if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_ONCE)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_ONCE;
    }
    else if (i4AssignmentMethod == RFMGMT_TPC_SELECTION_OFF)
    {
        u4TpcMode = RFMGMT_TPC_MODE_GLOBAL;
        u4TpcSelectionMode = RFMGMT_TPC_SELECTION_OFF;
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Update") == 0)
    {
        if (nmhTestv2FsRrmTpcMode (&u4ErrCode, i4GetRadioType, (INT4) u4TpcMode)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }
        if (nmhSetFsRrmTpcMode (i4GetRadioType, (INT4) u4TpcMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Mode ");
            return;

        }

        if (nmhTestv2FsRrmTpcSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcSelectionMode (i4GetRadioType,
                                         (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Tpc Selection Mode ");
            return;
        }

        if (nmhTestv2FsRrmTpcUpdate (&u4ErrCode, i4GetRadioType,
                                     (INT4) u4TpcUpdate) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcUpdate (i4GetRadioType, (INT4) u4TpcUpdate)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Update ");
            return;
        }
    }
    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "TPC_INTERVAL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TpcInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "POWER_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SNRThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        if (nmhTestv2FsRrmTpcMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4TpcMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }

        if (nmhSetFsRrmTpcMode (i4GetRadioType, (INT4) u4TpcMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Tpc Mode ");
            return;

        }

        if (nmhTestv2FsRrmTpcSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmTpcSelectionMode (i4GetRadioType,
                                         (INT4) u4TpcSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Tpc Selection Mode ");
            return;
        }

        if (u4TpcSelectionMode == RFMGMT_TPC_SELECTION_AUTO)
        {
            if (nmhTestv2FsRrmTpcInterval (&u4ErrCode, i4GetRadioType,
                                           u4TpcInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }

            if (nmhSetFsRrmTpcInterval (i4GetRadioType,
                                        u4TpcInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Tpc Interval ");
                return;
            }
        }
        if (u4TpcSelectionMode != RFMGMT_TPC_SELECTION_OFF)
        {
            if (nmhTestv2FsRrmSNRThreshold (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4SNRThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmSNRThreshold (i4GetRadioType, (INT4) u4SNRThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure SNR Threshold ");
                return;
            }

        }

    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioGTpcPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGDcaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G DCA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGDcaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGDcaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGDcaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGDcaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G DCA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGDcaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4DcaSelection = 0;
    INT4                i4DcaSensitivity = 0;
    INT4                i4ConsiderExternalAPs = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4LastUpdatedTime = 0;
    UINT4               u4NeighborCountThreshold = 0;
    INT4                i4RssiThreshold = 0;
    UINT4               u4ClientThreshold = 0;
    UINT1               au1AllowedList[RFMGMT_MAX_CHANNELB];
    tSNMP_OCTET_STRING_TYPE AllowedList;
    UINT1               u1Channel = 0;
    UINT1               au1ChannelList[RFMGMT_MAX_CHANNELB + 1];
    UINT4               u4CurrentTime = 0;
    UINT4               u4DcaUpdatedTime = 0;

    MEMSET (au1AllowedList, 0, RFMGMT_MAX_CHANNELB);
    MEMSET (au1ChannelList, 0, RFMGMT_MAX_CHANNELB + 1);
    MEMSET (&AllowedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    AllowedList.pu1_OctetList = au1AllowedList;

    STRCPY (pHttp->au1KeyString, "DcaInterval_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaInterval (i4GetRadioType, &u4DcaInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DcaInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "cam_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4DcaSelection);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaSelection);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "LCAT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmLastUpdatedTime (i4GetRadioType, &u4LastUpdatedTime);
    if (u4LastUpdatedTime != 0)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4DcaUpdatedTime = u4CurrentTime - u4LastUpdatedTime;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DcaUpdatedTime);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ChannelSensitivity_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmDcaSensitivity (i4GetRadioType, &i4DcaSensitivity);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaSensitivity);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "RSSI_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmRSSIThreshold (i4GetRadioType, &i4RssiThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RssiThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ClientThreshold_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmClientThreshold (i4GetRadioType, &u4ClientThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ClientThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ConsiderExtAps_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmConsiderExternalAPs (i4GetRadioType, &i4ConsiderExternalAPs);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ConsiderExternalAPs);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "NeighCountThreshold_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmNeighborCountThreshold (i4GetRadioType,
                                       &u4NeighborCountThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4NeighborCountThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "allowedchannels_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrmAllowedChannels (i4GetRadioType, &AllowedList);

    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
    {
        if (au1AllowedList[u1Channel] != 0x0)
        {
            au1ChannelList[u1Channel] = 'a';
        }
        else
        {
            au1ChannelList[u1Channel] = 'b';
        }
    }

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1ChannelList);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGDcaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G DCA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGDcaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4UpdateChannel = RFMGMT_DCA_UPDATE_CHANNEL;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    UINT4               u4DcaMode = 0;
    UINT4               u4DcaSelectionMode = 0;
    UINT4               u4DcaInterval = 0;
    UINT4               u4DcaSensitivity = 0;
    UINT4               u4NeighborCountThreshold = 0;
    INT4                i4AssignmentMethod = 0;
    INT4                i4ConsiderExtAps = 0;
    INT4                i4RssiThreshold = 0;
    UINT4               u4ClientThreshold = 0;
    INT4                i4RowStatus = 0;

    UINT4               u4ChannelNumber = 0;
    UINT1               au1ChannelList[RFMGMT_MAX_CHANNELB + 2];
    UINT1               au1Dot11AllowedChannelList[RFMGMT_MAX_CHANNELB + 1];
    UINT1               au1Dot11UnusedChannelList[RFMGMT_MAX_CHANNELB + 1];
    tSNMP_OCTET_STRING_TYPE AllowedChannelList;
    tSNMP_OCTET_STRING_TYPE UnusedChannelList;

    MEMSET (&AllowedChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UnusedChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Dot11AllowedChannelList, 0, RFMGMT_MAX_CHANNELB + 1);
    MEMSET (&au1Dot11UnusedChannelList, 0, RFMGMT_MAX_CHANNELB + 1);
    MEMSET (au1ChannelList, 0, RFMGMT_MAX_CHANNELB + 2);
    AllowedChannelList.pu1_OctetList = au1Dot11AllowedChannelList;
    UnusedChannelList.pu1_OctetList = au1Dot11UnusedChannelList;

    STRCPY (pHttp->au1Name, "CAM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AssignmentMethod = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_AUTO)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_AUTO;
    }
    else if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_ONCE)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_ONCE;
    }
    else if (i4AssignmentMethod == RFMGMT_DCA_SELECTION_OFF)
    {
        u4DcaMode = RFMGMT_DCA_MODE_GLOBAL;
        u4DcaSelectionMode = RFMGMT_DCA_SELECTION_OFF;
    }

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Update") == 0)
    {
        if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaMode (i4GetRadioType, (INT4) u4DcaMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Dca Mode ");
            return;
        }

        if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaSelectionMode (i4GetRadioType,
                                         (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Dca Selection Mode ");
            return;
        }

        if (nmhTestv2FsRrmUpdateChannel (&u4ErrCode, (INT4) i4GetRadioType,
                                         (INT4) u4UpdateChannel) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;

        }
        if (nmhSetFsRrmRowStatus (i4GetRadioType, ACTIVE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }

        if (nmhSetFsRrmUpdateChannel ((INT4) i4GetRadioType,
                                      (INT4) u4UpdateChannel) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to update channel");
            return;
        }
    }

    else if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "DCA_INTERVAL");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DcaInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CHANNEL_SENSITIVITY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DcaSensitivity = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "RSSI");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RssiThreshold = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CLIENT_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ClientThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CONSIDER_EXT_APS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ConsiderExtAps = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "NEIGHBOR_COUNT_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4NeighborCountThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "Allowedchannels");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ChannelList, pHttp->au1Value, RFMGMT_MAX_CHANNELB + 1);
        au1ChannelList[RFMGMT_MAX_CHANNELB + 1] = '\0';

        if (nmhTestv2FsRrmDcaMode (&u4ErrCode, i4GetRadioType,
                                   (INT4) u4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaMode (i4GetRadioType, (INT4) u4DcaMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure Dca Mode ");
            return;
        }

        if (nmhTestv2FsRrmDcaSelectionMode (&u4ErrCode, i4GetRadioType,
                                            (INT4) u4DcaSelectionMode) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
            return;
        }

        if (nmhSetFsRrmDcaSelectionMode
            (i4GetRadioType, (INT4) u4DcaSelectionMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure Dca Selection Mode ");
            return;
        }

        if (u4DcaSelectionMode == RFMGMT_DCA_SELECTION_AUTO)
        {
            if (nmhTestv2FsRrmDcaInterval (&u4ErrCode, i4GetRadioType,
                                           u4DcaInterval) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmDcaInterval (i4GetRadioType, u4DcaInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure DCA Interval ");
                return;
            }
        }
        if (u4DcaSelectionMode != RFMGMT_DCA_SELECTION_OFF)
        {
            if (nmhTestv2FsRrmDcaSensitivity (&u4ErrCode, i4GetRadioType,
                                              (INT4) u4DcaSensitivity) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmDcaSensitivity
                (i4GetRadioType, (INT4) u4DcaSensitivity) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure DCA sensitivity ");
                return;
            }
            if (nmhTestv2FsRrmRSSIThreshold (&u4ErrCode, i4GetRadioType,
                                             i4RssiThreshold) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmRSSIThreshold (i4GetRadioType, i4RssiThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure RSSI Threshold ");
                return;
            }
            if (nmhTestv2FsRrmClientThreshold (&u4ErrCode, i4GetRadioType,
                                               u4ClientThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmClientThreshold (i4GetRadioType, u4ClientThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Client Threshold");
                return;
            }
            if (nmhTestv2FsRrmConsiderExternalAPs (&u4ErrCode, i4GetRadioType,
                                                   i4ConsiderExtAps) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmConsiderExternalAPs
                (i4GetRadioType, i4ConsiderExtAps) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Consider External APs");
                return;
            }
            if (nmhTestv2FsRrmNeighborCountThreshold
                (&u4ErrCode, i4GetRadioType,
                 u4NeighborCountThreshold) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmNeighborCountThreshold (i4GetRadioType,
                                                   u4NeighborCountThreshold) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Neighbor Count Threshold");
                return;
            }

            for (u4ChannelNumber = 0; u4ChannelNumber < RFMGMT_MAX_CHANNELB;
                 u4ChannelNumber++)
            {
                if ((au1ChannelList[u4ChannelNumber + 1]) == 'b')
                {
                    AllowedChannelList.pu1_OctetList[u4ChannelNumber] = 0;
                    UnusedChannelList.pu1_OctetList[u4ChannelNumber] =
                        (UINT1) (u4ChannelNumber + 1);
                }
                else if ((au1ChannelList[u4ChannelNumber + 1]) == 'a')
                {
                    AllowedChannelList.pu1_OctetList[u4ChannelNumber] =
                        (UINT1) (u4ChannelNumber + 1);
                    UnusedChannelList.pu1_OctetList[u4ChannelNumber] = 0;
                }
            }
            if (nmhTestv2FsRrmAllowedChannels (&u4ErrCode, i4GetRadioType,
                                               &AllowedChannelList) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmAllowedChannels (i4GetRadioType, &AllowedChannelList)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure allowed channels ");
                return;
            }
            if (nmhTestv2FsRrmUnusedChannels (&u4ErrCode, i4GetRadioType,
                                              &UnusedChannelList) !=
                SNMP_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs ");
                return;
            }
            if (nmhSetFsRrmUnusedChannels (i4GetRadioType, &UnusedChannelList)
                != SNMP_SUCCESS)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure unused channels ");
                return;
            }
        }

    }
    else if (STRCMP (pHttp->au1Array, "ClearNeigh") == 0)
    {
        nmhSetFsRrmClearNeighborInfo (i4GetRadioType, OSIX_TRUE);
    }

    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioGDcaPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGDpaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G DPA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGDpaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGDpaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGDpaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGDpaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G DPA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGDpaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4RowSatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4DpaExecutionCount = 0;
    INT4                i4PowerThreshold = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) == SNMP_SUCCESS)
    {
        nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcMode);
        nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4TpcSelection);

        if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
        {

            if (i4TpcSelection == RFMGMT_TPC_SELECTION_OFF)
            {

                STRCPY (pHttp->au1KeyString, "plam_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
            else
            {
                STRCPY (pHttp->au1KeyString, "plam_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "AUTO");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
            }
        }
        else
        {
            STRCPY (pHttp->au1KeyString, "plam_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
        }

        STRCPY (pHttp->au1KeyString, "DPA_INTERVAL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmTpcInterval (i4GetRadioType, &u4TpcInterval);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "DPA_EXECUTION_COUNT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmDpaExecutionCount (i4GetRadioType, &u4DpaExecutionCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DpaExecutionCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "POWER_THRESHOLD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmPowerThreshold (i4GetRadioType, &i4PowerThreshold);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PowerThreshold);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) ("No entry found\n"));
        return;
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioGDpaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G DPA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGDpaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4PowerThreshold = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "POWER_THRESHOLD");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4PowerThreshold = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmPowerThreshold (&u4ErrCode, i4GetRadioType,
                                          i4PowerThreshold) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmPowerThreshold (i4GetRadioType, i4PowerThreshold)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Power Threshold ");
                return;
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioGDpaPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioGShaPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G SHA Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGShaPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGShaPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGShaPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGShaPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G SHA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGShaPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4RowSatus = 0;
    INT4                i4SHAStatus = 0;
    UINT4               u4SHAInterval = 0;
    UINT4               u4SHAExecutionCount = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowSatus) == SNMP_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "sha_status_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAStatus (i4GetRadioType, &i4SHAStatus);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SHAStatus);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "sha_interval_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAInterval (i4GetRadioType, &u4SHAInterval);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SHAInterval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        STRCPY (pHttp->au1KeyString, "sha_execution_count_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRrmSHAExecutionCount (i4GetRadioType, &u4SHAExecutionCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SHAExecutionCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }
    else
    {
        IssSendError (pHttp, (CONST INT1 *) ("No entry found\n"));
        return;
    }
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGShaPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN SHA page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGShaPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4ShaStatus = 0;
    UINT4               u4ShaInterval = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "sha_status");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ShaStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmSHAStatus (&u4ErrCode, i4GetRadioType,
                                     i4ShaStatus) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmSHAStatus (i4GetRadioType, i4ShaStatus)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to configure Sha Status ");
                return;
            }
        }

        STRCPY (pHttp->au1Name, "sha_interval");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ShaInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
        if (nmhTestv2FsRrmSHAInterval (&u4ErrCode, i4GetRadioType,
                                       u4ShaInterval) != SNMP_FAILURE)
        {
            if (nmhSetFsRrmSHAInterval (i4GetRadioType, u4ShaInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unable to configure Sha Interval ");
                return;
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }
    IssRfmRadioGShaPageGet (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : IssRfmRadioANNbrAPDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN Neighbour AP details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANNbrAPDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANNbrAPDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANNbrAPDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANNbrAPDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN Neighbour AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANNbrAPDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4NextScannedChannel = 0;
    UINT4               u4ScannedChannel = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmAPStatsTable
                        (&i4NextIndex, &u4NextScannedChannel,
                         &NextNeighMacAddr) != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        u4ScannedChannel = u4NextScannedChannel;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                                  NeighMacAddr, &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) (HTTP_STRLEN
                                                    (pHttp->au1DataString)));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4Rssi);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "Operating_Channel_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ScannedChannel);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmAPStatsTable
                           (i4Index, &i4NextIndex, u4ScannedChannel,
                            &u4NextScannedChannel, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        if (nmhGetFirstIndexFsRrmAPStatsTable (&i4NextIndex,
                                               &u4NextScannedChannel,
                                               &NextNeighMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            i4Index = i4NextIndex;
            u4ScannedChannel = u4NextScannedChannel;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                      NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Operating_Channel_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ScannedChannel);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPStatsTable (i4Index, &i4NextIndex,
                                                 u4ScannedChannel,
                                                 &u4NextScannedChannel,
                                                 NeighMacAddr,
                                                 &NextNeighMacAddr) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANNbrAPDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN Neighbour AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANNbrAPDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioANNbrAPDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANFldAPDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN Failed AP details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANFldAPDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANFldAPDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANFldAPDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANFldAPDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN Failed AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANFldAPDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                                 &NextNeighMacAddr)
                        != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmSHARSSIValue (i4Index,
                                                     NeighMacAddr, &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) (HTTP_STRLEN
                                                    (pHttp->au1DataString)));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4Rssi);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmFailedAPStatsTable
                           (i4Index, &i4NextIndex, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                     &NextNeighMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            i4Index = i4NextIndex;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmSHARSSIValue (i4Index, NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmFailedAPStatsTable (i4Index, &i4NextIndex,
                                                       NeighMacAddr,
                                                       &NextNeighMacAddr) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANFldAPDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN Failed AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANFldAPDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioANFldAPDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANCltScanDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN Client Scan details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANCltScanDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANCltScanDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANCltScanDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANCltScanDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN Client Scan details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANCltScanDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Type = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RadioIfIndex = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tMacAddr            ClientMacAddr;
    tMacAddr            NextClientMacAddr;
    INT4                i4SNR = 0;
    INT4                i4LastSNRScan = 0;
    UINT1               au1Time[TIME_STR_BUF_SIZE];

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (&ClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1Time, 0, TIME_STR_BUF_SIZE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) (pHttp->i4Write);
        i4Index = 0;
        u4currentProfileId = 0;
        u4currentBindingId = 0;
        u4nextProfileId = 0;
        u4nextBindingId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4currentProfileId, u4currentBindingId,
                     (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                {
                }
                if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                                         &NextClientMacAddr) !=
                    SNMP_SUCCESS)
                {
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    continue;
                }
                do
                {
                    pHttp->i4Write = (INT4) u4Temp;
                    i4Index = i4NextIndex;
                    MEMCPY (&ClientMacAddr, &NextClientMacAddr,
                            sizeof (tMacAddr));
                    if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                        SNMP_FAILURE)
                    {
                        continue;
                    }

                    if ((i4Type == RFMGMT_RADIO_TYPEA)
                        || (i4Type == RFMGMT_RADIO_TYPEAN))
                    {

                        if (i4Index == (INT4) u4RadioIfIndex)
                        {
                            nmhGetFsRrmClientSNR (i4Index, ClientMacAddr,
                                                  &i4SNR);
                            nmhGetFsRrmClientLastSNRScan (i4Index,
                                                          ClientMacAddr,
                                                          (UINT4
                                                           *) (&i4LastSNRScan));
                            CliMacToStr (ClientMacAddr, au1String);
                            UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan,
                                                   (CHR1 *) au1Time);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "SNR_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", i4SNR);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "Last_SNR_scan_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1Time);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }

                    }

                }
                while (nmhGetNextIndexFsRrmTpcClientTable (i4Index,
                                                           &i4NextIndex,
                                                           ClientMacAddr,
                                                           &NextClientMacAddr)
                       == SNMP_SUCCESS);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) (pHttp->i4Write);
        if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                                 &NextClientMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            MEMCPY (&ClientMacAddr, &NextClientMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Radio DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\n Capwap DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrmClientSNR (i4Index, ClientMacAddr, &i4SNR);
                nmhGetFsRrmClientLastSNRScan (i4Index, ClientMacAddr,
                                              (UINT4 *) (&i4LastSNRScan));
                CliMacToStr (ClientMacAddr, au1String);
                UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan, (CHR1 *) au1Time);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "SNR_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNR);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Last_SNR_scan_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Time);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmTpcClientTable (i4Index, &i4NextIndex,
                                                   ClientMacAddr,
                                                   &NextClientMacAddr) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANCltScanDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN Client scan details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANCltScanDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioANCltScanDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANClientInfoPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN Client information Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANClientInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANClientInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANClientInfoPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANClientInfoPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN Client information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANClientInfoPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4Temp = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4SNRScanCount = 0;
    UINT4               u4BelowSNRThresholdCount = 0;
    UINT4               u4TxPowerChangeCount = 0;
    UINT4               u4ClientsConnected = 0;
    UINT4               u4ClientsAccepted = 0;
    UINT4               u4ClientsDiscarded = 0;
    UINT4               u4TxPowerIncreaseCount = 0;
    UINT4               u4TxPowerDecreaseCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        if (u4currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                       &ProfileName);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_NAME");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        i4Index = 0;
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        u4currentBindingId = 0;
        u4nextBindingId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmTpcConfigTable (&i4NextIndex) !=
                        SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmSNRScanCount (i4Index, &u4SNRScanCount);
                            nmhGetFsRrmBelowSNRThresholdCount (i4Index,
                                                               &u4BelowSNRThresholdCount);
                            nmhGetFsRrmTxPowerChangeCount (i4Index,
                                                           &u4TxPowerChangeCount);
                            nmhGetFsRrmClientsConnected (i4Index,
                                                         &u4ClientsConnected);
                            nmhGetFsRrmClientsAccepted (i4Index,
                                                        &u4ClientsAccepted);
                            nmhGetFsRrmClientsDiscarded (i4Index,
                                                         &u4ClientsDiscarded);
                            nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                             &u4TxPowerDecreaseCount);

                            nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                             &u4TxPowerIncreaseCount);

                            STRCPY (pHttp->au1KeyString, "ap_name_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "radio_id_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "snr_scan_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4SNRScanCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            STRCPY (pHttp->au1KeyString,
                                    "snr_thresh_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4BelowSNRThresholdCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "power_change_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerChangeCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_connected_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsConnected);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_accepted_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsAccepted);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_discarded_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsDiscarded);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            STRCPY (pHttp->au1KeyString,
                                    "tx_power_incr_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerIncreaseCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            STRCPY (pHttp->au1KeyString,
                                    "tx_power_decr_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerDecreaseCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmTpcConfigTable (i4Index,
                                                               &i4NextIndex) ==
                           SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmTpcConfigTable (&i4NextIndex) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\n Radio DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n  Capwap DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrmSNRScanCount (i4Index, &u4SNRScanCount);
                nmhGetFsRrmBelowSNRThresholdCount (i4Index,
                                                   &u4BelowSNRThresholdCount);
                nmhGetFsRrmTxPowerChangeCount (i4Index, &u4TxPowerChangeCount);
                nmhGetFsRrmClientsConnected (i4Index, &u4ClientsConnected);
                nmhGetFsRrmClientsAccepted (i4Index, &u4ClientsAccepted);
                nmhGetFsRrmClientsDiscarded (i4Index, &u4ClientsDiscarded);
                nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                 &u4TxPowerDecreaseCount);

                nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                 &u4TxPowerIncreaseCount);

                STRCPY (pHttp->au1KeyString, "ap_name_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "radio_id_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "snr_scan_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SNRScanCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "snr_thresh_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4BelowSNRThresholdCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "power_change_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerChangeCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_connected_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsConnected);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_accepted_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsAccepted);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_discarded_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsDiscarded);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "tx_power_incr_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerIncreaseCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "tx_power_decr_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerDecreaseCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmTpcConfigTable (i4Index,
                                                   &i4NextIndex) ==
               SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANClientInfoPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN Client Information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANClientInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioANClientInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANAPScanInfoPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_AN AP scan information Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANAPScanInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANAPScanInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANAPScanInfoPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioANAPScanInfoPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_AN AP Scan information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANAPScanInfoPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    UINT1               au1TimeSuccess[TIME_STR_BUF_SIZE];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4ChannelChangeCount = 0;
    UINT4               u4ChannelChangeTime = 0;
    INT4                i4CurrentFrequency = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    MEMSET (au1TimeSuccess, 0, TIME_STR_BUF_SIZE);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        if (u4currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                       &ProfileName);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_NAME");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));
    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        i4Index = 0;
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) !=
                        SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        if (i4Index == i4RadioIfIndex)
                        {

                            i1RetVal = nmhGetFsRrmAPChannelChangeCount (i4Index,
                                                                        &u4ChannelChangeCount);
                            if (u4ChannelChangeCount != 0)
                            {
                                i1RetVal =
                                    nmhGetFsRrmAPChannelChangeTime (i4Index,
                                                                    &u4ChannelChangeTime);

                                UtlGetTimeStrForTicks (u4ChannelChangeTime,
                                                       (CHR1 *) au1TimeSuccess);
                            }

                            i1RetVal = nmhGetDot11CurrentFrequency (i4Index,
                                                                    (UINT4 *)
                                                                    &i4CurrentFrequency);

                            STRCPY (pHttp->au1KeyString, "ap_name_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "radio_id_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "change_count_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ChannelChangeCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "change_time_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1TimeSuccess);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "assigned_channel_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4CurrentFrequency);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                              &i4NextIndex) ==
                           SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;

            i1RetVal = nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type);

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\n Radio DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Capwap DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                i1RetVal = nmhGetFsRrmAPChannelChangeCount (i4Index,
                                                            &u4ChannelChangeCount);
                if (u4ChannelChangeCount != 0)
                {
                    i1RetVal = nmhGetFsRrmAPChannelChangeTime (i4Index,
                                                               &u4ChannelChangeTime);

                    UtlGetTimeStrForTicks (u4ChannelChangeTime,
                                           (CHR1 *) au1TimeSuccess);
                }

                i1RetVal =
                    nmhGetDot11CurrentFrequency (i4Index,
                                                 (UINT4 *) &i4CurrentFrequency);

                STRCPY (pHttp->au1KeyString, "ap_name_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "radio_id_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "change_count_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ChannelChangeCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "change_time_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeSuccess);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "assigned_channel_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4CurrentFrequency);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }
            MEMSET (au1TimeSuccess, 0, TIME_STR_BUF_SIZE);

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (i1RetVal);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioANAPScanInfoPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_AN AP scan information  page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioANAPScanInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioANAPScanInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioANAPScanCfgPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AN AP Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioANAPScanCfgPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANAPScanCfgPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANAPScanCfgPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRadioANAPScanCfgPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AN AP Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioANAPScanCfgPageGet (tHttp * pHttp)
{
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4AutoScanStatus = 0;
    UINT4               u4NeighborScanFreq = 0;
    UINT4               u4ChannelScanDuration = 0;
    UINT4               u4NeighborAgingPeriod = 0;

    UINT4               u4Temp = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4ChannelMode = 0;
    INT4                i4Type = 0;

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Radio DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    continue;

                }
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Capwap DB Access Failed\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                if (CapwapGetWtpProfileIdFromProfileName
                    (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId,
                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                     (INT4 *) &u4RadioIfIndex);

                STRCPY (pHttp->au1KeyString, "Ap_Name_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Radio_Id_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Auto_Scan_Status_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPAutoScanStatus ((INT4) u4RadioIfIndex,
                                             &i4AutoScanStatus);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AutoScanStatus);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "per_ap_dca_mode_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPChannelMode ((INT4) u4RadioIfIndex,
                                          &i4ChannelMode);
                nmhGetFsRrmDcaMode (i4GetRadioType, &i4DcaMode);
                nmhGetFsRrmDcaSelectionMode (i4GetRadioType,
                                             &i4DcaSelectionMode);

                if (((i4DcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
                     (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)) ||
                    (i4DcaMode == RFMGMT_DCA_MODE_PER_AP))
                {
                    i4DcaMode = i4ChannelMode;
                }
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaMode);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Database_Share_Frequency_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPNeighborScanFreq ((INT4) u4RadioIfIndex,
                                               &u4NeighborScanFreq);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4NeighborScanFreq);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Channel_Share_Frequency_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPChannelScanDuration ((INT4) u4RadioIfIndex,
                                                  &u4ChannelScanDuration);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ChannelScanDuration);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Neighbour_Ageing_Period_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPNeighborAgingPeriod ((INT4) u4RadioIfIndex,
                                                  &u4NeighborAgingPeriod);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4NeighborAgingPeriod);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioANAPScanCfgPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AN AP Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioANAPScanCfgPageSet (tHttp * pHttp)
{

    UINT4               u4RadioId = 0;
    INT4                i4AutoScanStatus = 0;
    UINT4               u4DBShareFreq = 0;
    UINT4               u4ChannelShareFreq = 0;
    UINT4               u4NeighbourAgeingPeriod = 0;
    UINT1               au1ApName[ISS_MAX_ADDR_BUFFER];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4DcaMode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4GetDcaSelection = 0;
    INT4                i4GetDcaMode = 0;

    MEMSET (&au1ApName, 0, ISS_MAX_ADDR_BUFFER);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Edit") == 0)
    {
        /* STRCPY (pHttp->au1Name, "Ap_Name"); */
        STRCPY (pHttp->au1Name, "AP_NAME_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

        STRCPY (pHttp->au1Name, "RADIO_ID_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4RadioId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "AUTO_SCAN_STATUS_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AutoScanStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DCA_MODE_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4DcaMode = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DB_SHARE_FREQ_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DBShareFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CHAN_SHARE_FREQ_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ChannelShareFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "NBR_AGE_PRD_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4NeighbourAgeingPeriod = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    }

    if (CapwapGetWtpProfileIdFromProfileName (au1ApName,
                                              &u4CapwapBaseWtpProfileId) !=
        OSIX_SUCCESS)
    {
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
    {
    }

    if (nmhTestv2FsRrmAPAutoScanStatus (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                        i4AutoScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPNeighborScanFreq (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                          u4DBShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPNeighborAgingPeriod
        (&u4ErrorCode, (INT4) u4RadioIfIndex,
         u4NeighbourAgeingPeriod) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPChannelScanDuration
        (&u4ErrorCode, (INT4) u4RadioIfIndex,
         u4ChannelShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }

    nmhGetFsRrmDcaMode (i4GetRadioType, &i4GetDcaMode);
    nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4GetDcaSelection);

    if ((i4GetDcaMode == RFMGMT_DCA_MODE_PER_AP) ||
        ((i4GetDcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
         (i4GetDcaSelection == RFMGMT_DCA_SELECTION_OFF)))
    {
        if (nmhSetFsRrmDcaMode (i4GetRadioType,
                                RFMGMT_DCA_MODE_PER_AP) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to set channel mode");
            return;
        }

        if (nmhTestv2FsRrmAPChannelMode (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                         i4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
            return;
        }
        if (nmhSetFsRrmAPChannelMode ((INT4) u4RadioIfIndex,
                                      i4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to configure DCA mode");
            return;
        }
    }

    if (nmhSetFsRrmAPAutoScanStatus ((INT4) u4RadioIfIndex,
                                     i4AutoScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Auto Scan Status");
        return;
    }

    if (nmhSetFsRrmAPNeighborScanFreq ((INT4) u4RadioIfIndex,
                                       u4DBShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Neighbour Scan Frequency");
        return;
    }

    if (nmhSetFsRrmAPNeighborAgingPeriod ((INT4) u4RadioIfIndex,
                                          u4NeighbourAgeingPeriod) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Neighbour Aging Period");
        return;
    }

    if (nmhSetFsRrmAPChannelScanDuration ((INT4) u4RadioIfIndex,
                                          u4ChannelShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Channel Scan Duration");
        return;
    }
    IssRfmRadioANAPScanCfgPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioANSNRCfgPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AN SNR Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioANSNRCfgPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioANSNRCfgPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioANSNRCfgPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRadioANSNRCfgPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AN SNR Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioANSNRCfgPageGet (tHttp * pHttp)
{
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT4               u4Temp = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;

    INT4                i4SNRScanStatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcModeCheck = 0;
    INT4                i4TpcSelectionMode = 0;
    UINT4               u4SNRScanFreq = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Type = 0;

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;

                }
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;

                }

                if (CapwapGetWtpProfileIdFromProfileName
                    (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;
                }

                nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId,
                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                     (INT4 *) &u4RadioIfIndex);

                STRCPY (pHttp->au1KeyString, "Ap_Name1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Radio_Id1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "snr_Scan_Status1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPSNRScanStatus ((INT4) u4RadioIfIndex,
                                            &i4SNRScanStatus);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNRScanStatus);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "per_ap_tpc_mode1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, &i4TpcMode);
                nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcModeCheck);
                nmhGetFsRrmTpcSelectionMode (i4GetRadioType,
                                             &i4TpcSelectionMode);

                if (((i4TpcModeCheck == RFMGMT_TPC_MODE_GLOBAL) &&
                     (i4TpcSelectionMode == RFMGMT_TPC_SELECTION_OFF)) ||
                    (i4TpcModeCheck == RFMGMT_TPC_MODE_PER_AP))
                {
                    i4TpcModeCheck = i4TpcMode;
                }
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcModeCheck);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "snr_scan_frequency1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmSNRScanFreq ((INT4) u4RadioIfIndex, &u4SNRScanFreq);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SNRScanFreq);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRadioANSNRCfgPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AN SNR Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioANSNRCfgPageSet (tHttp * pHttp)
{

    UINT1               au1ApName[ISS_MAX_ADDR_BUFFER];
    UINT4               u4RadioId = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4APSNRScanStatus = 0;
    INT4                i4TpcMode = 0;
    UINT4               u4SNRScanFreq = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4GetTpcMode = 0;
    INT4                i4GetTpcSelection = 0;

    MEMSET (&au1ApName, 0, ISS_MAX_ADDR_BUFFER);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Edit") == 0)
    {
        STRCPY (pHttp->au1Name, "AP_NAME_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

        STRCPY (pHttp->au1Name, "RADIO_ID_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4RadioId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SNR_SCAN_STATUS_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4APSNRScanStatus = ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "PER_AP_TPC_MODE_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TpcMode = ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SNR_SCAN_FREQ_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SNRScanFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    }

    if (CapwapGetWtpProfileIdFromProfileName (au1ApName,
                                              &u4CapwapBaseWtpProfileId) !=
        OSIX_SUCCESS)
    {
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
    {
    }

    if (nmhTestv2FsRrmAPSNRScanStatus (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                       i4APSNRScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }

    if (nmhTestv2FsRrmSNRScanFreq (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                   u4SNRScanFreq) == SNMP_FAILURE)
    {
        if ((i4APSNRScanStatus != RFMGMT_SNR_SCAN_ENABLE) &&
            (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
            return;
        }
    }

    nmhGetFsRrmTpcMode (i4GetRadioType, &i4GetTpcMode);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4GetTpcSelection);

    if ((i4GetTpcMode == RFMGMT_TPC_MODE_PER_AP) ||
        ((i4GetTpcMode == RFMGMT_TPC_MODE_GLOBAL) &&
         (i4GetTpcSelection == RFMGMT_TPC_SELECTION_OFF)))
    {
        if (nmhSetFsRrmTpcMode (i4GetRadioType,
                                RFMGMT_TPC_MODE_PER_AP) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to set txpower mode");
            return;
        }

        if (nmhTestv2FsRrmAPTpcMode (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                     i4TpcMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "TPC mode Invalid");
            return;
        }

        if (nmhSetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, i4TpcMode)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure per ap TPC mode");
            return;
        }
    }

    if (nmhSetFsRrmAPSNRScanStatus ((INT4) u4RadioIfIndex,
                                    i4APSNRScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure SNR Scan Status");
        return;
    }

    if (nmhSetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, i4TpcMode) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure per ap TPC mode");
        return;
    }

    if (i4APSNRScanStatus == RFMGMT_SNR_SCAN_ENABLE)
    {
        if (nmhSetFsRrmSNRScanFreq ((INT4) u4RadioIfIndex,
                                    u4SNRScanFreq) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure SNR Scan Frequency");
            return;
        }
    }
    IssRfmRadioANSNRCfgPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNNbrAPDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN Neighbour AP details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNNbrAPDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNNbrAPDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNNbrAPDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNNbrAPDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN Neighbour AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNNbrAPDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4Type = 0;
    INT4                i4RadioIfIndex = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4NextScannedChannel = 0;
    UINT4               u4ScannedChannel = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmAPStatsTable
                        (&i4NextIndex, &u4NextScannedChannel,
                         &NextNeighMacAddr) != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        u4ScannedChannel = u4NextScannedChannel;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                                  NeighMacAddr, &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4Rssi);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "Operating_Channel_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ScannedChannel);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmAPStatsTable
                           (i4Index, &i4NextIndex, u4ScannedChannel,
                            &u4NextScannedChannel, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);

                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) (pHttp->i4Write);

        if (nmhGetFirstIndexFsRrmAPStatsTable (&i4NextIndex,
                                               &u4NextScannedChannel,
                                               &NextNeighMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            u4ScannedChannel = u4NextScannedChannel;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBG)
                || (i4Type == RFMGMT_RADIO_TYPEBGN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                      NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Operating_Channel_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ScannedChannel);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPStatsTable (i4Index, &i4NextIndex,
                                                 u4ScannedChannel,
                                                 &u4NextScannedChannel,
                                                 NeighMacAddr,
                                                 &NextNeighMacAddr) ==
               SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNNbrAPDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN Neighbour AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNNbrAPDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioBGNNbrAPDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNFldAPDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN Failed AP details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNFldAPDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNFldAPDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNFldAPDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNFldAPDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN Failed AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNFldAPDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                                 &NextNeighMacAddr)
                        != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {
                            nmhGetFsRrmSHARSSIValue (i4Index,
                                                     NeighMacAddr, &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) (HTTP_STRLEN
                                                    (pHttp->au1DataString)));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4Rssi);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmFailedAPStatsTable
                           (i4Index, &i4NextIndex, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                     &NextNeighMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            i4Index = i4NextIndex;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBGN)
                || (i4Type == RFMGMT_RADIO_TYPEBG))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmSHARSSIValue (i4Index, NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmFailedAPStatsTable (i4Index, &i4NextIndex,
                                                       NeighMacAddr,
                                                       &NextNeighMacAddr) ==
               SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNFldAPDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN Failed AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNFldAPDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioBGNFldAPDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNCltScanDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN Client Scan details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNCltScanDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNCltScanDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNCltScanDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNCltScanDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN Client Scan details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNCltScanDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Type = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RadioIfIndex = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tMacAddr            ClientMacAddr;
    tMacAddr            NextClientMacAddr;
    INT4                i4SNR = 0;
    INT4                i4LastSNRScan = 0;
    UINT1               au1Time[TIME_STR_BUF_SIZE];
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (&ClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1Time, 0, TIME_STR_BUF_SIZE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) (pHttp->i4Write);
        i4Index = 0;
        u4currentProfileId = 0;
        u4currentBindingId = 0;
        u4nextProfileId = 0;
        u4nextBindingId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4currentProfileId, u4currentBindingId,
                     (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                {
                }
                if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                                         &NextClientMacAddr) !=
                    SNMP_SUCCESS)
                {

                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    continue;
                }
                do
                {
                    pHttp->i4Write = (INT4) u4Temp;
                    i4Index = i4NextIndex;
                    MEMCPY (&ClientMacAddr, &NextClientMacAddr,
                            sizeof (tMacAddr));
                    if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                        SNMP_FAILURE)
                    {
                        continue;
                    }

                    if ((i4Type == RFMGMT_RADIO_TYPEB)
                        || (i4Type == RFMGMT_RADIO_TYPEBG)
                        || (i4Type == RFMGMT_RADIO_TYPEBGN))
                    {

                        if (i4Index == (INT4) u4RadioIfIndex)
                        {
                            nmhGetFsRrmClientSNR (i4Index,
                                                  ClientMacAddr, &i4SNR);
                            nmhGetFsRrmClientLastSNRScan (i4Index,
                                                          ClientMacAddr,
                                                          (UINT4
                                                           *) (&i4LastSNRScan));
                            CliMacToStr (ClientMacAddr, au1String);
                            UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan,
                                                   (CHR1 *) au1Time);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "SNR_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", i4SNR);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "Last_SNR_scan_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1Time);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }

                    }

                }
                while (nmhGetNextIndexFsRrmTpcClientTable (i4Index,
                                                           &i4NextIndex,
                                                           ClientMacAddr,
                                                           &NextClientMacAddr)
                       == SNMP_SUCCESS);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                                 &NextClientMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            MEMCPY (&ClientMacAddr, &NextClientMacAddr, sizeof (tMacAddr));
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBG)
                || (i4Type == RFMGMT_RADIO_TYPEBGN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Radio DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\n Capwap DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrmClientSNR (i4Index, ClientMacAddr, &i4SNR);
                nmhGetFsRrmClientLastSNRScan (i4Index, ClientMacAddr,
                                              (UINT4 *) (&i4LastSNRScan));
                CliMacToStr (ClientMacAddr, au1String);
                UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan, (CHR1 *) au1Time);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "SNR_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNR);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Last_SNR_scan_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Time);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmTpcClientTable (i4Index, &i4NextIndex,
                                                   ClientMacAddr,
                                                   &NextClientMacAddr) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNCltScanDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN Client scan details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNCltScanDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioBGNCltScanDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNClientInfoPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN Client information Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNClientInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNClientInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNClientInfoPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNClientInfoPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN Client information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNClientInfoPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4Temp = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4SNRScanCount = 0;
    UINT4               u4BelowSNRThresholdCount = 0;
    UINT4               u4TxPowerChangeCount = 0;
    UINT4               u4ClientsConnected = 0;
    UINT4               u4ClientsAccepted = 0;
    UINT4               u4ClientsDiscarded = 0;
    UINT4               u4TxPowerIncreaseCount = 0;
    UINT4               u4TxPowerDecreaseCount = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        if (u4currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                       &ProfileName);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_NAME");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        i4Index = 0;
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        u4currentBindingId = 0;
        u4nextBindingId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId, i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmTpcConfigTable (&i4NextIndex) !=
                        SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmSNRScanCount (i4Index, &u4SNRScanCount);
                            nmhGetFsRrmBelowSNRThresholdCount (i4Index,
                                                               &u4BelowSNRThresholdCount);
                            nmhGetFsRrmTxPowerChangeCount (i4Index,
                                                           &u4TxPowerChangeCount);
                            nmhGetFsRrmClientsConnected (i4Index,
                                                         &u4ClientsConnected);
                            nmhGetFsRrmClientsAccepted (i4Index,
                                                        &u4ClientsAccepted);
                            nmhGetFsRrmClientsDiscarded (i4Index,
                                                         &u4ClientsDiscarded);
                            nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                             &u4TxPowerDecreaseCount);

                            nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                             &u4TxPowerIncreaseCount);

                            STRCPY (pHttp->au1KeyString, "ap_name_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "radio_id_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "snr_scan_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4SNRScanCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            STRCPY (pHttp->au1KeyString,
                                    "snr_thresh_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4BelowSNRThresholdCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "power_change_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerChangeCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_connected_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsConnected);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_accepted_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsAccepted);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_discarded_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsDiscarded);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            STRCPY (pHttp->au1KeyString,
                                    "tx_power_incr_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerIncreaseCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            STRCPY (pHttp->au1KeyString,
                                    "tx_power_decr_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerDecreaseCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmTpcConfigTable (i4Index,
                                                               &i4NextIndex) ==
                           SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmTpcConfigTable (&i4NextIndex) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBG)
                || (i4Type == RFMGMT_RADIO_TYPEBGN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\n Radio DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n  Capwap DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrmSNRScanCount (i4Index, &u4SNRScanCount);
                nmhGetFsRrmBelowSNRThresholdCount (i4Index,
                                                   &u4BelowSNRThresholdCount);
                nmhGetFsRrmTxPowerChangeCount (i4Index, &u4TxPowerChangeCount);
                nmhGetFsRrmClientsConnected (i4Index, &u4ClientsConnected);
                nmhGetFsRrmClientsAccepted (i4Index, &u4ClientsAccepted);
                nmhGetFsRrmClientsDiscarded (i4Index, &u4ClientsDiscarded);
                nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                 &u4TxPowerDecreaseCount);

                nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                 &u4TxPowerIncreaseCount);

                STRCPY (pHttp->au1KeyString, "ap_name_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "radio_id_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "snr_scan_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SNRScanCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "snr_thresh_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4BelowSNRThresholdCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "power_change_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerChangeCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_connected_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsConnected);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_accepted_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsAccepted);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_discarded_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsDiscarded);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "tx_power_incr_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerIncreaseCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "tx_power_decr_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerDecreaseCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmTpcConfigTable (i4Index,
                                                   &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNClientInfoPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN Client Information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNClientInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioBGNClientInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNAPScanInfoPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_BGN AP scan information Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNAPScanInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNAPScanInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNAPScanInfoPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNAPScanInfoPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_BGN AP Scan information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNAPScanInfoPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    UINT1               au1TimeSuccess[TIME_STR_BUF_SIZE];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4ChannelChangeCount = 0;
    UINT4               u4ChannelChangeTime = 0;
    INT4                i4CurrentFrequency = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    MEMSET (au1TimeSuccess, 0, TIME_STR_BUF_SIZE);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        if (u4currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                       &ProfileName);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_NAME");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));
    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        i4Index = 0;
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        else
        {

            do
            {
                pHttp->i4Write = (INT4) u4Temp;
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4CapwapBaseWtpProfileId)
                {
                    if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                               u4currentBindingId,
                                               i4RadioType,
                                               &i4Index) == OSIX_SUCCESS)
                    {
                        i4RadioIfIndex = i4Index;
                        if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) !=
                            SNMP_SUCCESS)
                        {
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                            WebnmSockWrite (pHttp,
                                            (UINT1 *) (pHttp->pi1Html +
                                                       pHttp->i4Write),
                                            (pHttp->i4HtmlSize -
                                             pHttp->i4Write));
                            continue;
                        }
                        do
                        {
                            pHttp->i4Write = (INT4) u4Temp;
                            i4Index = i4NextIndex;
                            if (i4Index == i4RadioIfIndex)
                            {

                                nmhGetFsRrmAPChannelChangeCount (i4Index,
                                                                 &u4ChannelChangeCount);

                                if (u4ChannelChangeCount != 0)
                                {
                                    nmhGetFsRrmAPChannelChangeTime (i4Index,
                                                                    &u4ChannelChangeTime);

                                    UtlGetTimeStrForTicks (u4ChannelChangeTime,
                                                           (CHR1 *)
                                                           au1TimeSuccess);
                                }
                                if (nmhGetDot11CurrentFrequency (i4Index,
                                                                 (UINT4 *)
                                                                 &i4CurrentFrequency)
                                    != SNMP_SUCCESS)
                                {
                                    continue;
                                }

                                STRCPY (pHttp->au1KeyString, "ap_name_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString,
                                         "%s", gau1ApName);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString, "radio_id_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                         u4currentBindingId);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString,
                                        "change_count_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                         u4ChannelChangeCount);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString, "change_time_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                         au1TimeSuccess);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString,
                                        "assigned_channel_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                         i4CurrentFrequency);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                            }
                        }
                        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                                  &i4NextIndex)
                               == SNMP_SUCCESS);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBG)
                || (i4Type == RFMGMT_RADIO_TYPEBGN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\n Radio DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Capwap DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmAPChannelChangeCount (i4Index,
                                                 &u4ChannelChangeCount);
                if (u4ChannelChangeCount != 0)
                {
                    nmhGetFsRrmAPChannelChangeTime (i4Index,
                                                    &u4ChannelChangeTime);

                    UtlGetTimeStrForTicks (u4ChannelChangeTime,
                                           (CHR1 *) au1TimeSuccess);
                }

                if (nmhGetDot11CurrentFrequency
                    (i4Index, (UINT4 *) &i4CurrentFrequency) != SNMP_SUCCESS)
                {
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                STRCPY (pHttp->au1KeyString, "ap_name_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "radio_id_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "change_count_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ChannelChangeCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "change_time_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeSuccess);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "assigned_channel_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4CurrentFrequency);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }
            MEMSET (au1TimeSuccess, 0, TIME_STR_BUF_SIZE);

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioBGNAPScanInfoPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_BGN AP scan information  page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioBGNAPScanInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioBGNAPScanInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioBGNAPScanCfgPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AN BGN Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioBGNAPScanCfgPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNAPScanCfgPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNAPScanCfgPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRadioBGNAPScanCfgPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AN BGN Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioBGNAPScanCfgPageGet (tHttp * pHttp)
{
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4AutoScanStatus = 0;
    UINT4               u4NeighborScanFreq = 0;
    UINT4               u4ChannelScanDuration = 0;
    UINT4               u4NeighborAgingPeriod = 0;

    UINT4               u4Temp = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4ChannelMode = 0;
    INT4                i4Type = 0;

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBG)
                || (i4Type == RFMGMT_RADIO_TYPEBGN))
            {
                WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Radio DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    continue;

                }
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Capwap DB Access Failed\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                if (CapwapGetWtpProfileIdFromProfileName
                    (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId,
                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                     (INT4 *) &u4RadioIfIndex);

                STRCPY (pHttp->au1KeyString, "Ap_Name_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Radio_Id_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Auto_Scan_Status_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPAutoScanStatus ((INT4) u4RadioIfIndex,
                                             &i4AutoScanStatus);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AutoScanStatus);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "per_ap_dca_mode_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPChannelMode ((INT4) u4RadioIfIndex,
                                          &i4ChannelMode);
                nmhGetFsRrmDcaMode (i4GetRadioType, &i4DcaMode);
                nmhGetFsRrmDcaSelectionMode (i4GetRadioType,
                                             &i4DcaSelectionMode);

                if (((i4DcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
                     (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)) ||
                    (i4DcaMode == RFMGMT_DCA_MODE_PER_AP))
                {
                    i4DcaMode = i4ChannelMode;
                }
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaMode);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Database_Share_Frequency_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPNeighborScanFreq ((INT4) u4RadioIfIndex,
                                               &u4NeighborScanFreq);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4NeighborScanFreq);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Channel_Share_Frequency_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPChannelScanDuration ((INT4) u4RadioIfIndex,
                                                  &u4ChannelScanDuration);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ChannelScanDuration);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Neighbour_Ageing_Period_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPNeighborAgingPeriod ((INT4) u4RadioIfIndex,
                                                  &u4NeighborAgingPeriod);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4NeighborAgingPeriod);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioBGNAPScanCfgPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AN BGN Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioBGNAPScanCfgPageSet (tHttp * pHttp)
{

    UINT4               u4RadioId = 0;
    INT4                i4AutoScanStatus = 0;
    UINT4               u4DBShareFreq = 0;
    UINT4               u4ChannelShareFreq = 0;
    UINT4               u4NeighbourAgeingPeriod = 0;
    UINT1               au1ApName[ISS_MAX_ADDR_BUFFER];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4DcaMode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4GetDcaSelection = 0;
    INT4                i4GetDcaMode = 0;

    MEMSET (&au1ApName, 0, ISS_MAX_ADDR_BUFFER);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Edit") == 0)
    {
        /* STRCPY (pHttp->au1Name, "Ap_Name"); */
        STRCPY (pHttp->au1Name, "AP_NAME_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

        STRCPY (pHttp->au1Name, "RADIO_ID_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4RadioId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "AUTO_SCAN_STATUS_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AutoScanStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DCA_MODE_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4DcaMode = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DB_SHARE_FREQ_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DBShareFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CHAN_SHARE_FREQ_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ChannelShareFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "NBR_AGE_PRD_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4NeighbourAgeingPeriod = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    }

    if (CapwapGetWtpProfileIdFromProfileName (au1ApName,
                                              &u4CapwapBaseWtpProfileId) !=
        OSIX_SUCCESS)
    {
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
    {
    }

    if (nmhTestv2FsRrmAPAutoScanStatus (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                        i4AutoScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPNeighborScanFreq (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                          u4DBShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPNeighborAgingPeriod
        (&u4ErrorCode, (INT4) u4RadioIfIndex,
         u4NeighbourAgeingPeriod) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPChannelScanDuration
        (&u4ErrorCode, (INT4) u4RadioIfIndex,
         u4ChannelShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }

    nmhGetFsRrmDcaMode (i4GetRadioType, &i4GetDcaMode);
    nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4GetDcaSelection);

    if ((i4GetDcaMode == RFMGMT_DCA_MODE_PER_AP) ||
        ((i4GetDcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
         (i4GetDcaSelection == RFMGMT_DCA_SELECTION_OFF)))
    {
        if (nmhSetFsRrmDcaMode (i4GetRadioType,
                                RFMGMT_DCA_MODE_PER_AP) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to set channel mode");
            return;
        }

        if (nmhTestv2FsRrmAPChannelMode (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                         i4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
            return;
        }
        if (nmhSetFsRrmAPChannelMode ((INT4) u4RadioIfIndex,
                                      i4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to configure DCA mode");
            return;
        }
    }

    if (nmhSetFsRrmAPAutoScanStatus ((INT4) u4RadioIfIndex,
                                     i4AutoScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Auto Scan Status");
        return;
    }

    if (nmhSetFsRrmAPNeighborScanFreq ((INT4) u4RadioIfIndex,
                                       u4DBShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Neighbour Scan Frequency");
        return;
    }

    if (nmhSetFsRrmAPNeighborAgingPeriod ((INT4) u4RadioIfIndex,
                                          u4NeighbourAgeingPeriod) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Neighbour Aging Period");
        return;
    }

    if (nmhSetFsRrmAPChannelScanDuration ((INT4) u4RadioIfIndex,
                                          u4ChannelShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Channel Scan Duration");
        return;
    }
    IssRfmRadioBGNAPScanCfgPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioBGNSNRCfgPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_BGN SNR Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioBGNSNRCfgPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioBGNSNRCfgPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioBGNSNRCfgPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRadioBGNSNRCfgPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_BGN SNR Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioBGNSNRCfgPageGet (tHttp * pHttp)
{
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT4               u4Temp = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;

    INT4                i4SNRScanStatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcModeCheck = 0;
    INT4                i4TpcSelectionMode = 0;
    UINT4               u4SNRScanFreq = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4Type = 0;

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEB)
                || (i4Type == RFMGMT_RADIO_TYPEBG)
                || (i4Type == RFMGMT_RADIO_TYPEBGN))
            {
                WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;

                }
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;

                }

                if (CapwapGetWtpProfileIdFromProfileName
                    (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;
                }

                nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId,
                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                     (INT4 *) &u4RadioIfIndex);

                STRCPY (pHttp->au1KeyString, "Ap_Name1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Radio_Id1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "snr_Scan_Status1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPSNRScanStatus ((INT4) u4RadioIfIndex,
                                            &i4SNRScanStatus);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNRScanStatus);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "per_ap_tpc_mode1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, &i4TpcMode);
                nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcModeCheck);
                nmhGetFsRrmTpcSelectionMode (i4GetRadioType,
                                             &i4TpcSelectionMode);

                if (((i4TpcModeCheck == RFMGMT_TPC_MODE_GLOBAL) &&
                     (i4TpcSelectionMode == RFMGMT_TPC_SELECTION_OFF)) ||
                    (i4TpcModeCheck == RFMGMT_TPC_MODE_PER_AP))
                {
                    i4TpcModeCheck = i4TpcMode;
                }
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcModeCheck);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "snr_scan_frequency1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmSNRScanFreq ((INT4) u4RadioIfIndex, &u4SNRScanFreq);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SNRScanFreq);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRadioBGNSNRCfgPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_BGN SNR Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioBGNSNRCfgPageSet (tHttp * pHttp)
{

    UINT1               au1ApName[ISS_MAX_ADDR_BUFFER];
    UINT4               u4RadioId = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4APSNRScanStatus = 0;
    INT4                i4TpcMode = 0;
    UINT4               u4SNRScanFreq = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEB;
    INT4                i4GetTpcMode = 0;
    INT4                i4GetTpcSelection = 0;

    MEMSET (&au1ApName, 0, ISS_MAX_ADDR_BUFFER);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Edit") == 0)
    {
        STRCPY (pHttp->au1Name, "AP_NAME_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

        STRCPY (pHttp->au1Name, "RADIO_ID_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4RadioId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SNR_SCAN_STATUS_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4APSNRScanStatus = ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "PER_AP_TPC_MODE_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TpcMode = ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SNR_SCAN_FREQ_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SNRScanFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    }

    if (CapwapGetWtpProfileIdFromProfileName (au1ApName,
                                              &u4CapwapBaseWtpProfileId) !=
        OSIX_SUCCESS)
    {
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
    {
    }

    if (nmhTestv2FsRrmAPSNRScanStatus (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                       i4APSNRScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }

    if (nmhTestv2FsRrmSNRScanFreq (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                   u4SNRScanFreq) == SNMP_FAILURE)
    {
        if ((i4APSNRScanStatus != RFMGMT_SNR_SCAN_ENABLE) &&
            (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
            return;
        }
    }

    nmhGetFsRrmTpcMode (i4GetRadioType, &i4GetTpcMode);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4GetTpcSelection);

    if ((i4GetTpcMode == RFMGMT_TPC_MODE_PER_AP) ||
        ((i4GetTpcMode == RFMGMT_TPC_MODE_GLOBAL) &&
         (i4GetTpcSelection == RFMGMT_TPC_SELECTION_OFF)))
    {
        if (nmhSetFsRrmTpcMode (i4GetRadioType,
                                RFMGMT_TPC_MODE_PER_AP) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to set txpower mode");
            return;
        }

        if (nmhTestv2FsRrmAPTpcMode (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                     i4TpcMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "TPC mode Invalid");
            return;
        }

        if (nmhSetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, i4TpcMode)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure per ap TPC mode");
            return;
        }
    }

    if (nmhSetFsRrmAPSNRScanStatus ((INT4) u4RadioIfIndex,
                                    i4APSNRScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure SNR Scan Status");
        return;
    }

    if (nmhSetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, i4TpcMode) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure per ap TPC mode");
        return;
    }

    if (i4APSNRScanStatus == RFMGMT_SNR_SCAN_ENABLE)
    {
        if (nmhSetFsRrmSNRScanFreq ((INT4) u4RadioIfIndex,
                                    u4SNRScanFreq) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure SNR Scan Frequency");
            return;
        }
    }
    IssRfmRadioBGNSNRCfgPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGNbrAPDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G Neighbour AP details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGNbrAPDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGNbrAPDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGNbrAPDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGNbrAPDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G Neighbour AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGNbrAPDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4Type = 0;
    INT4                i4RadioIfIndex = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4NextScannedChannel = 0;
    UINT4               u4ScannedChannel = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmAPStatsTable
                        (&i4NextIndex, &u4NextScannedChannel,
                         &NextNeighMacAddr) != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        u4ScannedChannel = u4NextScannedChannel;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                                  NeighMacAddr, &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4Rssi);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "Operating_Channel_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ScannedChannel);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmAPStatsTable
                           (i4Index, &i4NextIndex, u4ScannedChannel,
                            &u4NextScannedChannel, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);

                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) (pHttp->i4Write);

        if (nmhGetFirstIndexFsRrmAPStatsTable (&i4NextIndex,
                                               &u4NextScannedChannel,
                                               &NextNeighMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            u4ScannedChannel = u4NextScannedChannel;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmRSSIValue (i4Index, u4ScannedChannel,
                                      NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Operating_Channel_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ScannedChannel);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPStatsTable (i4Index, &i4NextIndex,
                                                 u4ScannedChannel,
                                                 &u4NextScannedChannel,
                                                 NeighMacAddr,
                                                 &NextNeighMacAddr) ==
               SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGNbrAPDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G Neighbour AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGNbrAPDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioGNbrAPDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGFldAPDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G Failed AP details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGFldAPDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGFldAPDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGFldAPDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGFldAPDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G Failed AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGFldAPDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            NeighMacAddr;
    tMacAddr            NextNeighMacAddr;
    INT4                i4Rssi = 0;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&NeighMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextNeighMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                                 &NextNeighMacAddr)
                        != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        MEMCPY (&NeighMacAddr, &NextNeighMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {
                            nmhGetFsRrmSHARSSIValue (i4Index,
                                                     NeighMacAddr, &i4Rssi);

                            CliMacToStr (NeighMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) (HTTP_STRLEN
                                                    (pHttp->au1DataString)));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4Rssi);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmFailedAPStatsTable
                           (i4Index, &i4NextIndex, NeighMacAddr,
                            &NextNeighMacAddr) == SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        if (nmhGetFirstIndexFsRrmFailedAPStatsTable (&i4NextIndex,
                                                     &NextNeighMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            i4Index = i4NextIndex;
            MEMCPY (&NeighMacAddr, &NextNeighMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmSHARSSIValue (i4Index, NeighMacAddr, &i4Rssi);

                CliMacToStr (NeighMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RSSI_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmFailedAPStatsTable (i4Index, &i4NextIndex,
                                                       NeighMacAddr,
                                                       &NextNeighMacAddr) ==
               SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGFldAPDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G Failed AP details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGFldAPDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioGFldAPDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGCltScanDtlPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G Client Scan details Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGCltScanDtlPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGCltScanDtlPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGCltScanDtlPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGCltScanDtlPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G Client Scan details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGCltScanDtlPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Type = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4Temp = 0;
    UINT4               u4RadioIfIndex = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tMacAddr            ClientMacAddr;
    tMacAddr            NextClientMacAddr;
    INT4                i4SNR = 0;
    INT4                i4LastSNRScan = 0;
    UINT1               au1Time[TIME_STR_BUF_SIZE];
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (&ClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextClientMacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1Time, 0, TIME_STR_BUF_SIZE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) (pHttp->i4Write);
        i4Index = 0;
        u4currentProfileId = 0;
        u4currentBindingId = 0;
        u4nextProfileId = 0;
        u4nextBindingId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4currentProfileId, u4currentBindingId,
                     (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                {
                }
                if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                                         &NextClientMacAddr) !=
                    SNMP_SUCCESS)
                {

                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    continue;
                }
                do
                {
                    pHttp->i4Write = (INT4) u4Temp;
                    i4Index = i4NextIndex;
                    MEMCPY (&ClientMacAddr, &NextClientMacAddr,
                            sizeof (tMacAddr));
                    if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type))
                    {
                        continue;
                    }

                    if (i4Type == RFMGMT_RADIO_TYPEG)
                    {

                        if (i4Index == (INT4) u4RadioIfIndex)
                        {
                            nmhGetFsRrmClientSNR (i4Index,
                                                  ClientMacAddr, &i4SNR);
                            nmhGetFsRrmClientLastSNRScan (i4Index,
                                                          ClientMacAddr,
                                                          (UINT4
                                                           *) (&i4LastSNRScan));
                            CliMacToStr (ClientMacAddr, au1String);
                            UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan,
                                                   (CHR1 *) au1Time);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "SNR_Thresh_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%d", i4SNR);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "Last_SNR_scan_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", au1Time);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }

                    }

                }
                while (nmhGetNextIndexFsRrmTpcClientTable (i4Index,
                                                           &i4NextIndex,
                                                           ClientMacAddr,
                                                           &NextClientMacAddr)
                       == SNMP_SUCCESS);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmTpcClientTable (&i4NextIndex,
                                                 &NextClientMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            MEMCPY (&ClientMacAddr, &NextClientMacAddr, sizeof (tMacAddr));
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Radio DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {

                    IssSendError (pHttp, (CONST INT1 *)
                                  "\n Capwap DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrmClientSNR (i4Index, ClientMacAddr, &i4SNR);
                nmhGetFsRrmClientLastSNRScan (i4Index, ClientMacAddr,
                                              (UINT4 *) (&i4LastSNRScan));
                CliMacToStr (ClientMacAddr, au1String);
                UtlGetTimeStrForTicks ((UINT4) i4LastSNRScan, (CHR1 *) au1Time);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "SNR_Thresh_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNR);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Last_SNR_scan_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1Time);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmTpcClientTable (i4Index, &i4NextIndex,
                                                   ClientMacAddr,
                                                   &NextClientMacAddr) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGCltScanDtlPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G Client scan details page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGCltScanDtlPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioGCltScanDtlPageGet (pHttp);

    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGClientInfoPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G Client information Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGClientInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGClientInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGClientInfoPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGClientInfoPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G Client information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGClientInfoPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4Temp = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4SNRScanCount = 0;
    UINT4               u4BelowSNRThresholdCount = 0;
    UINT4               u4TxPowerChangeCount = 0;
    UINT4               u4ClientsConnected = 0;
    UINT4               u4ClientsAccepted = 0;
    UINT4               u4ClientsDiscarded = 0;
    UINT4               u4TxPowerIncreaseCount = 0;
    UINT4               u4TxPowerDecreaseCount = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        if (u4currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                       &ProfileName);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_NAME");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        i4Index = 0;
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        u4currentBindingId = 0;
        u4nextBindingId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId, i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrmTpcConfigTable (&i4NextIndex) !=
                        SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrmSNRScanCount (i4Index, &u4SNRScanCount);
                            nmhGetFsRrmBelowSNRThresholdCount (i4Index,
                                                               &u4BelowSNRThresholdCount);
                            nmhGetFsRrmTxPowerChangeCount (i4Index,
                                                           &u4TxPowerChangeCount);
                            nmhGetFsRrmClientsConnected (i4Index,
                                                         &u4ClientsConnected);
                            nmhGetFsRrmClientsAccepted (i4Index,
                                                        &u4ClientsAccepted);
                            nmhGetFsRrmClientsDiscarded (i4Index,
                                                         &u4ClientsDiscarded);
                            nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                             &u4TxPowerDecreaseCount);

                            nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                             &u4TxPowerIncreaseCount);

                            STRCPY (pHttp->au1KeyString, "ap_name_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "radio_id_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "snr_scan_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4SNRScanCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            STRCPY (pHttp->au1KeyString,
                                    "snr_thresh_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4BelowSNRThresholdCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "power_change_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerChangeCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_connected_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsConnected);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_accepted_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsAccepted);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "clients_discarded_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ClientsDiscarded);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            STRCPY (pHttp->au1KeyString,
                                    "tx_power_incr_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerIncreaseCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            STRCPY (pHttp->au1KeyString,
                                    "tx_power_decr_count_Key");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerDecreaseCount);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrmTpcConfigTable (i4Index,
                                                               &i4NextIndex) ==
                           SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmTpcConfigTable (&i4NextIndex) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\n Radio DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n  Capwap DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrmSNRScanCount (i4Index, &u4SNRScanCount);
                nmhGetFsRrmBelowSNRThresholdCount (i4Index,
                                                   &u4BelowSNRThresholdCount);
                nmhGetFsRrmTxPowerChangeCount (i4Index, &u4TxPowerChangeCount);
                nmhGetFsRrmClientsConnected (i4Index, &u4ClientsConnected);
                nmhGetFsRrmClientsAccepted (i4Index, &u4ClientsAccepted);
                nmhGetFsRrmClientsDiscarded (i4Index, &u4ClientsDiscarded);
                nmhGetFsRrmTxPowerDecreaseCount (i4RadioType,
                                                 &u4TxPowerDecreaseCount);

                nmhGetFsRrmTxPowerIncreaseCount (i4RadioType,
                                                 &u4TxPowerIncreaseCount);

                STRCPY (pHttp->au1KeyString, "ap_name_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "radio_id_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "snr_scan_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SNRScanCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "snr_thresh_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4BelowSNRThresholdCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "power_change_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerChangeCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_connected_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsConnected);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_accepted_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsAccepted);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "clients_discarded_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ClientsDiscarded);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "tx_power_incr_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerIncreaseCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "tx_power_decr_count_Key");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4TxPowerDecreaseCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmTpcConfigTable (i4Index,
                                                   &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGClientInfoPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G Client Information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGClientInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioGClientInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGAPScanInfoPage
 *  Description   : This function processes the request coming for the
 *                  RF Radio_G AP scan information Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGAPScanInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGAPScanInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGAPScanInfoPageSet (pHttp);
    }

}

/*********************************************************************
 *  Function Name : IssRfmRadioGAPScanInfoPageGet
 *  Description   : This function processes the get request coming for the
 *                  RF Radio_G AP Scan information page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGAPScanInfoPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    UINT1               au1TimeSuccess[TIME_STR_BUF_SIZE];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4ChannelChangeCount = 0;
    UINT4               u4ChannelChangeTime = 0;
    INT4                i4CurrentFrequency = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    MEMSET (au1TimeSuccess, 0, TIME_STR_BUF_SIZE);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;

    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        if (u4currentProfileId != 0)
        {
            MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
            MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ProfileName.pu1_OctetList = au1ProfileName;

            i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                       &ProfileName);
            if (SNMP_FAILURE == i4RetVal)
            {
                continue;
            }

            STRCPY (pHttp->au1Name, "AP_NAME");
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "<option value = \"%s\" selected>%s \n",
                     ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));
    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        i4Index = 0;
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        else
        {

            do
            {
                pHttp->i4Write = (INT4) u4Temp;
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4CapwapBaseWtpProfileId)
                {
                    if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                               u4currentBindingId,
                                               i4RadioType,
                                               &i4Index) == OSIX_SUCCESS)
                    {
                        i4RadioIfIndex = i4Index;
                        if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) !=
                            SNMP_SUCCESS)
                        {
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                            WebnmSockWrite (pHttp,
                                            (UINT1 *) (pHttp->pi1Html +
                                                       pHttp->i4Write),
                                            (pHttp->i4HtmlSize -
                                             pHttp->i4Write));
                            continue;
                        }
                        do
                        {
                            pHttp->i4Write = (INT4) u4Temp;
                            i4Index = i4NextIndex;
                            if (i4Index == i4RadioIfIndex)
                            {

                                nmhGetFsRrmAPChannelChangeCount (i4Index,
                                                                 &u4ChannelChangeCount);
                                if (u4ChannelChangeCount != 0)
                                {
                                    nmhGetFsRrmAPChannelChangeTime (i4Index,
                                                                    &u4ChannelChangeTime);

                                    UtlGetTimeStrForTicks (u4ChannelChangeTime,
                                                           (CHR1 *)
                                                           au1TimeSuccess);
                                }
                                if (nmhGetDot11CurrentFrequency (i4Index,
                                                                 (UINT4 *)
                                                                 &i4CurrentFrequency)
                                    != SNMP_SUCCESS)
                                {
                                    continue;
                                }

                                STRCPY (pHttp->au1KeyString, "ap_name_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString,
                                         "%s", gau1ApName);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString, "radio_id_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                         u4currentBindingId);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString,
                                        "change_count_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                         u4ChannelChangeCount);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString, "change_time_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                         au1TimeSuccess);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);

                                STRCPY (pHttp->au1KeyString,
                                        "assigned_channel_KEY");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                         i4CurrentFrequency);
                                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                                (INT4) HTTP_STRLEN (pHttp->
                                                                    au1DataString));
                                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                                WebnmSendString (pHttp, pHttp->au1KeyString);
                                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                            }
                        }
                        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                                  &i4NextIndex)
                               == SNMP_SUCCESS);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        }

    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;
        if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "\n Radio DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Capwap DB Access Failed\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }

                nmhGetFsRrmAPChannelChangeCount (i4Index,
                                                 &u4ChannelChangeCount);
                if (u4ChannelChangeCount != 0)
                {
                    nmhGetFsRrmAPChannelChangeTime (i4Index,
                                                    &u4ChannelChangeTime);

                    UtlGetTimeStrForTicks (u4ChannelChangeTime,
                                           (CHR1 *) au1TimeSuccess);
                }
                if (nmhGetDot11CurrentFrequency
                    (i4Index, (UINT4 *) &i4CurrentFrequency) != SNMP_SUCCESS)
                {
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                STRCPY (pHttp->au1KeyString, "ap_name_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "radio_id_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "change_count_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ChannelChangeCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "change_time_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeSuccess);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "assigned_channel_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4CurrentFrequency);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }
            MEMSET (au1TimeSuccess, 0, TIME_STR_BUF_SIZE);

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }
    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 *  Function Name : IssRfmRadioGAPScanInfoPageSet
 *  Description   : This function processes the set request coming for the
 *                  RF Radio_G AP scan information  page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssRfmRadioGAPScanInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmRadioGAPScanInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioGAPScanCfgPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_G BGN Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioGAPScanCfgPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGAPScanCfgPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGAPScanCfgPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRadioGAPScanCfgPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AN BGN Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioGAPScanCfgPageGet (tHttp * pHttp)
{
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    tSNMP_OCTET_STRING_TYPE ProfileName;
    INT4                i4AutoScanStatus = 0;
    UINT4               u4NeighborScanFreq = 0;
    UINT4               u4ChannelScanDuration = 0;
    UINT4               u4NeighborAgingPeriod = 0;
    UINT4               u4Temp = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4ChannelMode = 0;
    INT4                i4Type = 0;

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Radio DB Access Failed \n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    continue;

                }
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "\n Capwap DB Access Failed\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                if (CapwapGetWtpProfileIdFromProfileName
                    (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId,
                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                     (INT4 *) &u4RadioIfIndex);

                STRCPY (pHttp->au1KeyString, "Ap_Name_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Radio_Id_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Auto_Scan_Status_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPAutoScanStatus ((INT4) u4RadioIfIndex,
                                             &i4AutoScanStatus);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AutoScanStatus);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "per_ap_dca_mode_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPChannelMode ((INT4) u4RadioIfIndex,
                                          &i4ChannelMode);
                nmhGetFsRrmDcaMode (i4GetRadioType, &i4DcaMode);
                nmhGetFsRrmDcaSelectionMode (i4GetRadioType,
                                             &i4DcaSelectionMode);

                if (((i4DcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
                     (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)) ||
                    (i4DcaMode == RFMGMT_DCA_MODE_PER_AP))
                {
                    i4DcaMode = i4ChannelMode;
                }
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DcaMode);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Database_Share_Frequency_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPNeighborScanFreq ((INT4) u4RadioIfIndex,
                                               &u4NeighborScanFreq);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4NeighborScanFreq);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Channel_Share_Frequency_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPChannelScanDuration ((INT4) u4RadioIfIndex,
                                                  &u4ChannelScanDuration);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ChannelScanDuration);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "Neighbour_Ageing_Period_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPNeighborAgingPeriod ((INT4) u4RadioIfIndex,
                                                  &u4NeighborAgingPeriod);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4NeighborAgingPeriod);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioGAPScanCfgPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_G Scan Configuration Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioGAPScanCfgPageSet (tHttp * pHttp)
{

    UINT4               u4RadioId = 0;
    INT4                i4AutoScanStatus = 0;
    UINT4               u4DBShareFreq = 0;
    UINT4               u4ChannelShareFreq = 0;
    UINT4               u4NeighbourAgeingPeriod = 0;
    UINT1               au1ApName[ISS_MAX_ADDR_BUFFER];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4DcaMode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4GetDcaSelection = 0;
    INT4                i4GetDcaMode = 0;

    MEMSET (&au1ApName, 0, ISS_MAX_ADDR_BUFFER);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Edit") == 0)
    {
        /* STRCPY (pHttp->au1Name, "Ap_Name"); */
        STRCPY (pHttp->au1Name, "AP_NAME_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

        STRCPY (pHttp->au1Name, "RADIO_ID_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4RadioId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "AUTO_SCAN_STATUS_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AutoScanStatus = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DCA_MODE_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4DcaMode = (INT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DB_SHARE_FREQ_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DBShareFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "CHAN_SHARE_FREQ_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ChannelShareFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "NBR_AGE_PRD_2");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4NeighbourAgeingPeriod = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    }

    if (CapwapGetWtpProfileIdFromProfileName (au1ApName,
                                              &u4CapwapBaseWtpProfileId) !=
        OSIX_SUCCESS)
    {
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
    {
    }

    if (nmhTestv2FsRrmAPAutoScanStatus (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                        i4AutoScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPNeighborScanFreq (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                          u4DBShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPNeighborAgingPeriod
        (&u4ErrorCode, (INT4) u4RadioIfIndex,
         u4NeighbourAgeingPeriod) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }
    if (nmhTestv2FsRrmAPChannelScanDuration
        (&u4ErrorCode, (INT4) u4RadioIfIndex,
         u4ChannelShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }

    nmhGetFsRrmDcaMode (i4GetRadioType, &i4GetDcaMode);
    nmhGetFsRrmDcaSelectionMode (i4GetRadioType, &i4GetDcaSelection);

    if ((i4GetDcaMode == RFMGMT_DCA_MODE_PER_AP) ||
        ((i4GetDcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
         (i4GetDcaSelection == RFMGMT_DCA_SELECTION_OFF)))
    {
        if (nmhSetFsRrmDcaMode (i4GetRadioType,
                                RFMGMT_DCA_MODE_PER_AP) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to set channel mode");
            return;
        }

        if (nmhTestv2FsRrmAPChannelMode (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                         i4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
            return;
        }
        if (nmhSetFsRrmAPChannelMode ((INT4) u4RadioIfIndex,
                                      i4DcaMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to configure DCA mode");
            return;
        }
    }

    if (nmhSetFsRrmAPAutoScanStatus ((INT4) u4RadioIfIndex,
                                     i4AutoScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Auto Scan Status");
        return;
    }

    if (nmhSetFsRrmAPNeighborScanFreq ((INT4) u4RadioIfIndex,
                                       u4DBShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Neighbour Scan Frequency");
        return;
    }

    if (nmhSetFsRrmAPNeighborAgingPeriod ((INT4) u4RadioIfIndex,
                                          u4NeighbourAgeingPeriod) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Neighbour Aging Period");
        return;
    }

    if (nmhSetFsRrmAPChannelScanDuration ((INT4) u4RadioIfIndex,
                                          u4ChannelShareFreq) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Unable to configure Channel Scan Duration");
        return;
    }
    IssRfmRadioGAPScanCfgPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRadioGSNRCfgPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_G SNR Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioGSNRCfgPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRadioGSNRCfgPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRadioGSNRCfgPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRadioGSNRCfgPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_G SNR Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioGSNRCfgPageGet (tHttp * pHttp)
{
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT4               u4Temp = 0;
    INT4                i4NextIndex = 0;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;

    INT4                i4SNRScanStatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcModeCheck = 0;
    INT4                i4TpcSelectionMode = 0;
    UINT4               u4SNRScanFreq = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4Type = 0;

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    if (nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIndex) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if (i4Type == RFMGMT_RADIO_TYPEG)
            {
                WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB);
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;

                }
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;

                }

                if (CapwapGetWtpProfileIdFromProfileName
                    (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                     &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
                {
                    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                    WebnmSockWrite (pHttp,
                                    (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    return;
                }

                nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4CapwapBaseWtpProfileId,
                     RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                     (INT4 *) &u4RadioIfIndex);

                STRCPY (pHttp->au1KeyString, "Ap_Name1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "Radio_Id1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "snr_Scan_Status1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPSNRScanStatus ((INT4) u4RadioIfIndex,
                                            &i4SNRScanStatus);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SNRScanStatus);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "per_ap_tpc_mode1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, &i4TpcMode);
                nmhGetFsRrmTpcMode (i4GetRadioType, &i4TpcModeCheck);
                nmhGetFsRrmTpcSelectionMode (i4GetRadioType,
                                             &i4TpcSelectionMode);

                if (((i4TpcModeCheck == RFMGMT_TPC_MODE_GLOBAL) &&
                     (i4TpcSelectionMode == RFMGMT_TPC_SELECTION_OFF)) ||
                    (i4TpcModeCheck == RFMGMT_TPC_MODE_PER_AP))
                {
                    i4TpcModeCheck = i4TpcMode;
                }
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TpcModeCheck);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");

                STRCPY (pHttp->au1KeyString, "snr_scan_frequency1_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRrmSNRScanFreq ((INT4) u4RadioIfIndex, &u4SNRScanFreq);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4SNRScanFreq);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (STRLEN (pHttp->au1DataString)));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrmAPConfigTable (i4Index,
                                                  &i4NextIndex) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

    }

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRadioGSNRCfgPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_G SNR Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRadioGSNRCfgPageSet (tHttp * pHttp)
{

    UINT1               au1ApName[ISS_MAX_ADDR_BUFFER];
    UINT4               u4RadioId = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4APSNRScanStatus = 0;
    INT4                i4TpcMode = 0;
    UINT4               u4SNRScanFreq = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEG;
    INT4                i4GetTpcMode = 0;
    INT4                i4GetTpcSelection = 0;

    MEMSET (&au1ApName, 0, ISS_MAX_ADDR_BUFFER);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Array, "Edit") == 0)
    {
        STRCPY (pHttp->au1Name, "AP_NAME_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRNCPY (au1ApName, pHttp->au1Value, (sizeof (au1ApName) - 1));

        STRCPY (pHttp->au1Name, "RADIO_ID_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4RadioId = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SNR_SCAN_STATUS_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4APSNRScanStatus = ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "PER_AP_TPC_MODE_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TpcMode = ATOI ((INT1 *) pHttp->au1Value);

        STRCPY (pHttp->au1Name, "SNR_SCAN_FREQ_1");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SNRScanFreq = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    }

    if (CapwapGetWtpProfileIdFromProfileName (au1ApName,
                                              &u4CapwapBaseWtpProfileId) !=
        OSIX_SUCCESS)
    {
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
    {
    }

    if (nmhTestv2FsRrmAPSNRScanStatus (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                       i4APSNRScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
        return;
    }

    if (nmhTestv2FsRrmSNRScanFreq (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                   u4SNRScanFreq) == SNMP_FAILURE)
    {
        if ((i4APSNRScanStatus != RFMGMT_SNR_SCAN_ENABLE) &&
            (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE))
        {
            IssSendError (pHttp, (CONST INT1 *) "Invalid Inputs");
            return;
        }
    }

    nmhGetFsRrmTpcMode (i4GetRadioType, &i4GetTpcMode);
    nmhGetFsRrmTpcSelectionMode (i4GetRadioType, &i4GetTpcSelection);

    if ((i4GetTpcMode == RFMGMT_TPC_MODE_PER_AP) ||
        ((i4GetTpcMode == RFMGMT_TPC_MODE_GLOBAL) &&
         (i4GetTpcSelection == RFMGMT_TPC_SELECTION_OFF)))
    {
        if (nmhSetFsRrmTpcMode (i4GetRadioType,
                                RFMGMT_TPC_MODE_PER_AP) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Unable to set txpower mode");
            return;
        }

        if (nmhTestv2FsRrmAPTpcMode (&u4ErrorCode, (INT4) u4RadioIfIndex,
                                     i4TpcMode) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "TPC mode Invalid");
            return;
        }

        if (nmhSetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, i4TpcMode)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to configure per ap TPC mode");
            return;
        }
    }

    if (nmhSetFsRrmAPSNRScanStatus ((INT4) u4RadioIfIndex,
                                    i4APSNRScanStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure SNR Scan Status");
        return;
    }

    if (nmhSetFsRrmAPTpcMode ((INT4) u4RadioIfIndex, i4TpcMode) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure per ap TPC mode");
        return;
    }

    if (i4APSNRScanStatus == RFMGMT_SNR_SCAN_ENABLE)
    {
        if (nmhSetFsRrmSNRScanFreq ((INT4) u4RadioIfIndex,
                                    u4SNRScanFreq) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to configure SNR Scan Frequency");
            return;
        }
    }
    IssRfmRadioGSNRCfgPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmGlobalPage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmGlobalPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmGlobalPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmGlobalPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmGlobalPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmGlobalPageGet (tHttp * pHttp)
{

    INT4                i4AStatus = 0;
    INT4                i4BStatus = 0;
    INT4                i4GStatus = 0;
    INT4                i4RowStatus = 0;

    STRCPY (pHttp->au1KeyString, "dot11a_status_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsRrmRowStatus (RFMGMT_RADIO_TYPEA, &i4RowStatus) == SNMP_SUCCESS)
    {
        i4AStatus = RADIO_ENABLE;
    }
    else
    {
        i4AStatus = RADIO_DISABLE;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "dot11b_status_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsRrmRowStatus (RFMGMT_RADIO_TYPEB, &i4RowStatus) == SNMP_SUCCESS)
    {
        i4BStatus = RADIO_ENABLE;
    }
    else
    {
        i4BStatus = RADIO_DISABLE;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    STRCPY (pHttp->au1KeyString, "dot11g_status_Key");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetFsRrmRowStatus (RFMGMT_RADIO_TYPEG, &i4RowStatus) == SNMP_SUCCESS)
    {
        i4GStatus = RADIO_ENABLE;
    }
    else
    {
        i4GStatus = RADIO_DISABLE;
    }
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) (STRLEN (pHttp->au1DataString)));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmGlobalPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmGlobalPageSet (tHttp * pHttp)
{
    UINT4               u4AStatus = 0;
    UINT4               u4BStatus = 0;
    UINT4               u4GStatus = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Status = 0;
    INT4                i4RetVal = 0;

    STRCPY (pHttp->au1Name, "dot11a_status");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4AStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "dot11b_status");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4BStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "dot11g_status");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4GStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    i4RetVal = nmhGetFsRrmRowStatus (RFMGMT_RADIO_TYPEA, &i4RowStatus);

    if (u4AStatus == RADIO_DISABLE)
    {
        if (i4RetVal == SNMP_SUCCESS)
        {
            if (nmhSetFsRrmRowStatus (RFMGMT_RADIO_TYPEA, DESTROY)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to disable dot11a radio");
                return;
            }
        }
    }
    else
    {
        if (i4RetVal == SNMP_FAILURE)
        {
            i4Status = CREATE_AND_GO;
        }
        else
        {
            i4Status = ACTIVE;
        }
        if (nmhSetFsRrmRowStatus (RFMGMT_RADIO_TYPEA, i4Status) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to enable dot11a radio");
            return;
        }

    }

    i4RetVal = nmhGetFsRrmRowStatus (RFMGMT_RADIO_TYPEB, &i4RowStatus);

    if (u4BStatus == RADIO_DISABLE)
    {
        if (i4RetVal == SNMP_SUCCESS)
        {
            if (nmhSetFsRrmRowStatus (RFMGMT_RADIO_TYPEB, DESTROY)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to disable dot11b radio");
                return;
            }
        }
    }
    else
    {
        if (i4RetVal == SNMP_FAILURE)
        {
            i4Status = CREATE_AND_GO;
        }
        else
        {
            i4Status = ACTIVE;
        }
        if (nmhSetFsRrmRowStatus (RFMGMT_RADIO_TYPEB, i4Status) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to enable dot11b radio");
            return;
        }

    }

    i4RetVal = nmhGetFsRrmRowStatus (RFMGMT_RADIO_TYPEG, &i4RowStatus);

    if (u4GStatus == RADIO_DISABLE)
    {
        if (i4RetVal == SNMP_SUCCESS)
        {
            if (nmhSetFsRrmRowStatus (RFMGMT_RADIO_TYPEG, DESTROY)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to disable dot11g radio");
                return;
            }
        }
    }
    else
    {
        if (i4RetVal == SNMP_FAILURE)
        {
            i4Status = CREATE_AND_GO;
        }
        else
        {
            i4Status = ACTIVE;
        }
        if (nmhSetFsRrmRowStatus (RFMGMT_RADIO_TYPEG, i4Status) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to enable dot11g radio");
            return;
        }

    }
    IssRfmGlobalPageGet (pHttp);
    return;
}

#ifdef ROGUEAP_WANTED
/*********************************************************************
 * *  Function Name : IssRfmRogueBasicPage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueBasicPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRogueBasicPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRogueBasicPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRogueBasicPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueBasicPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4Detection = 0;
    UINT4               u4RogueApTimeout = 0;
    INT4                i4Status = 0;
    INT4                i4TrapStatus = 0;
    UINT1               u1aRfName[19];
    tSNMP_OCTET_STRING_TYPE RfName;

    RfName.pu1_OctetList = u1aRfName;
    MEMSET (u1aRfName, 0, sizeof (u1aRfName));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1KeyString, "RF_GROUP_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRfGroupName (&RfName);
    if (STRCMP (RfName.pu1_OctetList, "") != 0)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", RfName.pu1_OctetList);
    }
    else
    {
        RfName.pu1_OctetList = (UINT1 *) "Aricent123";
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", RfName.pu1_OctetList);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "ROGUE_AP_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRogueApDetecttion ((INT4 *) &u4Detection);

    if (u4Detection == 1)
    {
        i4Status = RADIO_ENABLE;
    }
    else
    {
        i4Status = RADIO_DISABLE;
    }

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "ROGUE_TIMEOUT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRogueApTimeout (&u4RogueApTimeout);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RogueApTimeout);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "ROGUE_TRAP_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRogueApMaliciousTrapStatus (&i4TrapStatus);

    if (i4TrapStatus == 1)
    {
        i4Status = RFMGMT_ROGUE_ENABLE;
    }
    else
    {
        i4Status = 0;
    }

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) HTTP_STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*********************************************************************
 * *  Function Name : IssRfmRogueBasicPageSet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueBasicPageSet (tHttp * pHttp)
{
    INT4                i4Detection = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    INT4                i4Unclassify = 0;
    INT4                i4TrapStatus = 0;
    UINT4               u4Timeout = 0;

    tRfMgmtDB           RfMgmtDB;

    UINT1               u1aRfName[256];
    tSNMP_OCTET_STRING_TYPE RfName;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfName.pu1_OctetList = u1aRfName;
    MEMSET (u1aRfName, 0, sizeof (u1aRfName));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "RF_GROUP_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        RfName.i4_Length = STRLEN (pHttp->au1Value);
        MEMCPY (u1aRfName, pHttp->au1Value, RfName.i4_Length);

        if (nmhSetFsRfGroupName (&RfName) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) " Unable to set the Rf Group Name");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_AP_DETECTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4Detection = (INT4) (ATOI (pHttp->au1Value));

        if (nmhSetFsRogueApDetecttion (i4Detection) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set ap detection");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_AP_TIMEOUT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Timeout = (UINT4) (ATOI (pHttp->au1Value));

        if (nmhSetFsRogueApTimeout (u4Timeout) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set ap timeout");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_AP_TRAP");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TrapStatus = (INT4) (ATOI (pHttp->au1Value));

        if (nmhSetFsRogueApMaliciousTrapStatus (i4TrapStatus) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set trap status");
            return;
        }

    }

    if ((STRCMP (pHttp->au1Value, "Add") == 0)
        || (STRCMP (pHttp->au1Value, "Delete") == 0))
    {
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            i4Unclassify = 1;
        }

        STRCPY (pHttp->au1Name, "CLASS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        i4Class = (INT4) (ATOI (pHttp->au1Value));

        if ((i4Class == ROGUE_AP_UNCLASSIFY) && (i4Unclassify == 0))
        {
            /*STRCPY (pHttp->au1Name, "STATE");
               HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
               i4State = (UINT4)(ATOI(pHttp->au1Value));

               if(i4State == 0 )
               {
               IssRfmRogueUnclassifyPageSet(pHttp);
               }
               else
               {
               IssSendError(pHttp,(CONST INT1*) "Unclassified ap cannot have alert/contain state");
               return ;
               } */
            IssRfmRogueUnclassifyPageSet (pHttp);
        }
        if ((i4Class == ROGUE_AP_UNCLASSIFY) && (i4Unclassify == 1))
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unclassified ap cannot be added manually. ");
            return;
        }

        STRCPY (pHttp->au1Name, "STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4State = (INT4) (ATOI (pHttp->au1Value));

        if (i4Class == ROGUE_AP_FRIENDLY)
        {

            if (i4State == ROGUE_AP_NONE)
            {
                IssRfmRogueFriendPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Friendly ap cannot have alert/contain state");
                return;
            }
        }

        if (i4Class == ROGUE_AP_MALICIOUS)
        {
            if (i4State != ROGUE_AP_NONE)
            {
                IssRfmRogueMaliciousPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Malicious ap cannot have none state");
                return;
            }
        }

    }
    if ((STRCMP (pHttp->au1Value, "DeleteAll") == 0))
    {
        STRCPY (pHttp->au1Name, "AP_CLASS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);

        i4Class = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "AP_STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4State = (INT4) (ATOI (pHttp->au1Value));

        if (i4Class == ROGUE_AP_UNCLASSIFY)
        {
            if (i4State == ROGUE_AP_NONE)
            {
                IssRfmRogueUnclassifyPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Unclassified ap cannot have alert/contain state");
                return;
            }
        }

        if (i4Class == ROGUE_AP_FRIENDLY)
        {
            if (i4State == ROGUE_AP_NONE)
            {
                IssRfmRogueFriendPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Friendly ap cannot have alert/contain state");
                return;
            }
        }

        if (i4Class == ROGUE_AP_MALICIOUS)
        {
            if (i4State != ROGUE_AP_NONE)
            {
                IssRfmRogueMaliciousPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Malicious ap cannot have none state");
                return;
            }
        }

    }

    IssRfmRogueBasicPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRogueRulePage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueRulePage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRogueRulePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRogueRulePageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRogueRulePageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueRulePageGet (tHttp * pHttp)
{
    INT4                i4Type = 0;
    INT4                i4State = 0;
    INT4                i4Status = 0;
    INT4                i4Encrypt = 0;
    INT4                i4OutCome = 0;
    UINT4               u4Duration = 0;
    INT4                i4Rssi = 0;
    INT4                i4Pri = 0;
    UINT4               u4ClientCount = 0;
    UINT4               u4Temp = 0;

    UINT1               u1aRfName[32];
    UINT1               u1aRuleName[32];
    UINT1               u1aSsid[32];
    tSNMP_OCTET_STRING_TYPE NextRuleName;
    tSNMP_OCTET_STRING_TYPE RuleName;
    tSNMP_OCTET_STRING_TYPE Ssid;
    tRfMgmtDB           RfMgmtDB;

    NextRuleName.pu1_OctetList = u1aRfName;
    RuleName.pu1_OctetList = u1aRuleName;
    Ssid.pu1_OctetList = u1aSsid;
    MEMSET (u1aRfName, 0, sizeof (u1aRfName));
    MEMSET (u1aSsid, 0, sizeof (u1aSsid));
    MEMSET (u1aRuleName, 0, sizeof (u1aRuleName));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexFsRogueRuleConfigTable (&NextRuleName);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            STRCPY (pHttp->au1KeyString, "RULE_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                     NextRuleName.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ROGUE_RULE_PRIORITY_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRulePriOrderNum (&NextRuleName, &i4Pri);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Pri);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "TYPE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleClass (&NextRuleName, &i4Type);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Type);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "STATE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleState (&NextRuleName, &i4State);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4State);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleStatus (&NextRuleName, &i4Status);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "NO_ENCRYPTION_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleProtectionType (&NextRuleName, &i4Encrypt);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Encrypt);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DURATION_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleDuration (&NextRuleName, &u4Duration);
            if (u4Duration <= 3600)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Duration);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                u4Duration = 0;
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Duration);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            STRCPY (pHttp->au1KeyString, "RSSI_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleRSSI (&NextRuleName, &i4Rssi);
            if (i4Rssi != -129)
            {

                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                i4Rssi = 0;
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            STRCPY (pHttp->au1KeyString, "SSID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleSSID (&NextRuleName, &Ssid);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Ssid.pu1_OctetList);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "CLIENT_COUNT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRogueApRuleClientCount (&NextRuleName, &u4ClientCount);
            if (u4ClientCount <= 10)
            {

                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ClientCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                u4ClientCount = 0;
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ClientCount);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            MEMCPY (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                    WSSMAC_MAX_SSID_LEN);
            nmhGetNextIndexFsRogueRuleConfigTable (&RuleName, &NextRuleName);
        }
        while (MEMCMP
               (RuleName.pu1_OctetList, NextRuleName.pu1_OctetList,
                WSSMAC_MAX_SSID_LEN) != 0);
    }

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueRulePageSet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueRulePageSet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4ApDur = 0;
    UINT4               u4ApClient = 0;
    INT4                i4ApState = 0;
    INT4                i4ApStatus = 0;
    INT4                i4ApClass = 0;
    INT4                i4ApPri = 0;
    INT4                i4ApEncryp = 0;
    INT4                i4ApRssi = 0;

    UINT1               u1aRfName[32];
    UINT1               u1aApSsid[32];

    tRfMgmtDB           RfMgmtDB;

    tSNMP_OCTET_STRING_TYPE RfName;
    tSNMP_OCTET_STRING_TYPE ApSsid;

    RfName.pu1_OctetList = u1aRfName;
    ApSsid.pu1_OctetList = u1aApSsid;

    MEMSET (u1aRfName, 0, sizeof (u1aRfName));
    MEMSET (u1aApSsid, 0, sizeof (u1aApSsid));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {

        STRCPY (pHttp->au1Name, "ROGUE_RULE_PRIORITY");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApPri = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "ROGUE_TYPE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApClass = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "ROGUE_STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApState = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "ROGUE_RULE_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRCPY (u1aRfName, pHttp->au1Value);

        if (i4ApClass == ROGUE_AP_FRIENDLY)
        {
            if (i4ApState == ROGUE_AP_NONE)
            {
                IssRfmRogueFriendPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Friendly ap cannot have alert/contain state");
                return;
            }
        }

        if (i4ApClass == ROGUE_AP_MALICIOUS)
        {
            if (i4ApState != ROGUE_AP_NONE)
            {
                IssRfmRogueMaliciousPageSet (pHttp);
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Malicious ap cannot have none state");
                return;
            }
        }
        RfMgmtArrangeRogueRulePriority (i4ApPri);

        if ((i4ApClass == ROGUE_AP_MALICIOUS &&
             (i4ApState == ROGUE_AP_ALERT || i4ApState == ROGUE_AP_CONTAIN))
            || ((i4ApClass == ROGUE_AP_FRIENDLY)
                && (i4ApState == ROGUE_AP_NONE)))
        {
            if ((i4ApPri != 0) && (i4ApPri <= gu1RogueRuleCount + 1)
                && (STRCMP (u1aRfName, " ") != 0))
            {
                gu1RogueRuleCount++;

                MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueRuleDB.u1RogueApRuleName,
                        u1aRfName, WSSMAC_MAX_SSID_LEN);
                if (RfMgmtProcessDBMsg
                    (RFMGMT_CREATE_RULE_ROGUE_INFO,
                     &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    IssSendError (pHttp, (CONST INT1 *) "Failed in creation");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_TYPE");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                i4ApClass = (INT4) (ATOI (pHttp->au1Value));
                if (nmhSetFsRogueApRuleClass (&RfName, i4ApClass) ==
                    SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set ap classification");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_STATE");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                i4ApState = (INT4) (ATOI (pHttp->au1Value));
                if (nmhSetFsRogueApRuleState (&RfName, i4ApState) ==
                    SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set ap State");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_STATUS");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                i4ApStatus = (INT4) (ATOI (pHttp->au1Value));
                if (i4ApStatus != 2)
                {
                    if (nmhSetFsRogueApRuleStatus (&RfName, i4ApStatus) ==
                        SNMP_FAILURE)
                    {
                        IssSendError (pHttp,
                                      (CONST INT1 *) "Failed to set ap Status");
                        return;
                    }
                }
                else
                {
                    IssSendError (pHttp, (CONST INT1 *) "Status can be 1 or 0");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_RULE_PRIORITY");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                i4ApPri = (INT4) (ATOI (pHttp->au1Value));
                if (nmhSetFsRogueApRulePriOrderNum (&RfName, i4ApPri) ==
                    SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set ap Priority");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_ENCRYPTION");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                i4ApEncryp = (INT4) (ATOI (pHttp->au1Value));
                if (nmhSetFsRogueApRuleProtectionType (&RfName, i4ApEncryp) ==
                    SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set ap Encryption");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_DURATION");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                u4ApDur = (UINT4) (ATOI (pHttp->au1Value));
                if (nmhSetFsRogueApRuleDuration (&RfName, u4ApDur) ==
                    SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set ap Duration");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_RSSI");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                i4ApRssi = -(INT4) (ATOI (pHttp->au1Value));

                if (nmhSetFsRogueApRuleRSSI (&RfName, i4ApRssi) == SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set ap RSSI");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_SSID");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                ApSsid.pu1_OctetList = pHttp->au1Value;
                if (nmhSetFsRogueApRuleSSID (&RfName, &ApSsid) == SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set ap SSID");
                    return;
                }

                STRCPY (pHttp->au1Name, "ROGUE_CLIENTCOUNT");
                HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                    pHttp->au1PostQuery);
                u4ApClient = (UINT4) (ATOI (pHttp->au1Value));

                /* Client count is not mandatory parameter and hence value can 
                 * possible 0. In that case skip setting the value */
                if (u4ApClient != 0)
                {
                    if (nmhSetFsRogueApRuleClientCount (&RfName, u4ApClient) ==
                        SNMP_FAILURE)
                    {
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Failed to set ap Client Count");
                        return;
                    }
                }
            }
            else
            {
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Rule name and ap priority is mandatory. Priority can't be more than current rule count ");

                return;

            }

        }
        else
        {
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "State can be alert/contain for malicious only ");

            return;

        }

        gu1RogueRuleCount++;
        RfMgmtClassifyRogueAp ();

    }
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        STRCPY (pHttp->au1Name, "ROGUE_RULE_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRCPY (u1aRfName, pHttp->au1Value);
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleDB.u1RogueApRuleName,
                u1aRfName, WSSMAC_MAX_SSID_LEN);

        STRCPY (pHttp->au1KeyString, "ROGUE_RULE_PRIORITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApRulePriOrderNum (&RfName, &i4ApPri);

        if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_RULE_ROGUE_INFO, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to delete");
            return;

        }
        else
        {
            RfMgmtReArrangeRogueRule (i4ApPri);
            RfMgmtClassifyRogueAp ();
        }

    }

    if (STRCMP (pHttp->au1Value, "DeleteAll") == 0)
    {
        STRCPY (pHttp->au1Name, "ROGUE_RULE_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRCPY (u1aRfName, pHttp->au1Value);
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleDB.u1RogueApRuleName,
                u1aRfName, WSSMAC_MAX_SSID_LEN);

        if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_RULE_ROGUE_INFO_ALL, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to delete all rule");
            return;
        }
        RfMgmtClassifyRogueAp ();

    }
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "ROGUE_RULE_NAME");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        STRCPY (u1aRfName, pHttp->au1Value);
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleDB.u1RogueApRuleName,
                u1aRfName, WSSMAC_MAX_SSID_LEN);

        STRCPY (pHttp->au1Name, "ROGUE_STATUS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApStatus = (INT4) (ATOI (pHttp->au1Value));
        if (i4ApStatus != 2)
        {
            if (nmhSetFsRogueApRuleStatus (&RfName, i4ApStatus) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *) "Failed to set ap Status");
                return;
            }
        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *) "Status can be 1 or 0");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_ENCRYPTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApEncryp = (INT4) (ATOI (pHttp->au1Value));
        if (nmhSetFsRogueApRuleProtectionType (&RfName, i4ApEncryp) ==
            SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set ap Encryption");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_DURATION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ApDur = (UINT4) (ATOI (pHttp->au1Value));
        if (nmhSetFsRogueApRuleDuration (&RfName, u4ApDur) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set ap Duration");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_RSSI");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApRssi = -(INT4) (ATOI (pHttp->au1Value));
        if (nmhSetFsRogueApRuleRSSI (&RfName, i4ApRssi) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set ap RSSI");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_SSID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        ApSsid.pu1_OctetList = pHttp->au1Value;
        if (nmhSetFsRogueApRuleSSID (&RfName, &ApSsid) == SNMP_FAILURE)
        {
            IssSendError (pHttp, (CONST INT1 *) "Failed to set ap SSID");
            return;
        }

        STRCPY (pHttp->au1Name, "ROGUE_CLIENTCOUNT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4ApClient = (UINT4) (ATOI (pHttp->au1Value));

        /* Client count is not mandatory parameter and hence value can 
         * possible 0. In that case skip setting the value */
        if (u4ApClient != 0)
        {
            if (nmhSetFsRogueApRuleClientCount (&RfName, u4ApClient) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to set ap Client Count");
                return;
            }
        }

    }

    IssRfmRogueRulePageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmRogueFriendPage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueFriendPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRogueFriendPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRogueFriendPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRogueFriendPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueFriendPageGet (tHttp * pHttp)
{

    UINT4               u4LastHeard = 0;
    INT4                i4Class = 0;
    INT4                i4ApState = 0;
    UINT4               u4Temp = 0;

    INT4                i4OutCome = 0;
    UINT1               u1aSsid[32];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];

    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];

    tMacAddr            NextApMac;
    tMacAddr            ApMac;

    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (u1aSsid, 0, sizeof (u1aSsid));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexFsRogueApTable (&NextApMac);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {

            pHttp->i4Write = (INT4) u4Temp;

            nmhGetFsRogueApClass (NextApMac, &i4Class);
            if (i4Class == ROGUE_AP_FRIENDLY)
            {
                RfmGuiMacToStr (NextApMac, au1String);
                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "CLASS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Class);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRogueApState (NextApMac, &i4ApState);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ApState);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "LAST_HEARD_TIME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRogueApLastHeardTime (NextApMac, &u4LastHeard);
                UtlGetTimeStrForTicks (u4LastHeard, (CHR1 *) au1TimeLastHeard);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeLastHeard);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            }

            else
            {
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            }

            MEMCPY (ApMac, NextApMac, sizeof (tMacAddr));
            nmhGetNextIndexFsRogueApTable (ApMac, &NextApMac);
        }
        while (MEMCMP (ApMac, NextApMac, sizeof (tMacAddr)) != 0);

    }
    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueFriendPageSet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueFriendPageSet (tHttp * pHttp)
{
    UINT1               u1ApState = 0;
    UINT1               u1ApClass = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4Class = 0;
    INT4                i4ApState = 0;
    INT4                i4ApClass = 0;

    tMacAddr            RogueMacAddr;
    tRfMgmtDB           RfMgmtDB;
    tMacAddr            NextApMac;
    tMacAddr            ApMac;

    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (RogueMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "Add") == 0)
        || (STRCMP (pHttp->au1Value, "Delete") == 0))
    {

        STRCPY (pHttp->au1Name, "BSSID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac ((UINT1 *) pHttp->au1Value, RogueMacAddr);

        /* MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
           RfMgmtRogueApDB.u1RogueApBSSID,
           RogueMacAddr,
           MAC_ADDR_LEN); */
/*Raki*/
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualDB.u1RogueApBSSID, RogueMacAddr, MAC_ADDR_LEN);

        STRCPY (pHttp->au1Name, "CLASS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u1ApClass = (UINT1) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u1ApState = (UINT1) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "ACTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {
            MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApBSSID,
                    RogueMacAddr, MAC_ADDR_LEN);

            if (nmhTestv2FsRogueApManualClass
                (&u4ErrorCode, RogueMacAddr, (INT4) u1ApClass) != SNMP_FAILURE)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
                    u1RogueApState = (UINT1) u1ApState;
                RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
                    u1RogueApClass = (UINT1) u1ApClass;
                if (RfMgmtProcessDBMsg
                    (RFMGMT_CREATE_MANUAL_ROGUE_INFO,
                     &RfMgmtDB) == RFMGMT_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set friendly ap");
                    return;

                }

            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_MANUAL_ROGUE_INFO, &RfMgmtDB)
                == RFMGMT_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Failed to delete friendly ap");
                return;
            }

        }

    }

    if (STRCMP (pHttp->au1Value, "DeleteAll") == 0)
    {

        if (nmhGetFirstIndexFsRogueApTable (&NextApMac) != SNMP_FAILURE)
        {
            while (MEMCMP (ApMac, NextApMac, sizeof (tMacAddr)) != 0)
            {
                nmhGetFsRogueApClass (NextApMac, &i4Class);
                if (i4Class == ROGUE_AP_FRIENDLY)
                {
                    /* MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                       RfMgmtRogueApDB.u1RogueApBSSID,
                       NextApMac,
                       MAC_ADDR_LEN); */
                    /* Raki */
                    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                            RfMgmtRogueManualDB.u1RogueApBSSID,
                            NextApMac, MAC_ADDR_LEN);

                    if (RfMgmtProcessDBMsg
                        (RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
                         &RfMgmtDB) == RFMGMT_FAILURE)
                    {
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Failed to delete friendly ap");
                        return;
                    }
                }
                MEMCPY (ApMac, NextApMac, sizeof (tMacAddr));
                nmhGetNextIndexFsRogueApTable (ApMac, &NextApMac);

            }

        }
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "AP_MAC");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac ((UINT1 *) pHttp->au1Value, RogueMacAddr);

        /*          MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
           RfMgmtRogueManualDB.u1RogueApBSSID,
           RogueMacAddr,
           MAC_ADDR_LEN);
         */
        STRCPY (pHttp->au1Name, "STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApState = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "CLASS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApClass = (INT4) (ATOI (pHttp->au1Value));

/*            RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.u1RogueApState = (UINT1)i4ApState;
 *             if(RfMgmtProcessDBMsg (RFMGMT_SET_MANUAL_ROGUE_INFO, &RfMgmtDB) == RFMGMT_FAILURE)
 *                         */
        if (nmhTestv2FsRogueApManualClass
            (&u4ErrorCode, RogueMacAddr, i4ApClass) == SNMP_SUCCESS)
        {
            if (nmhSetFsRogueApManualClass (RogueMacAddr, i4ApClass) ==
                SNMP_SUCCESS)
            {
                if (i4ApState == ROGUE_AP_NONE)
                {

                    if (nmhTestv2FsRogueApManualState
                        (&u4ErrorCode, RogueMacAddr, i4ApState) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsRogueApManualState (RogueMacAddr, i4ApState)
                            != SNMP_SUCCESS)
                        {

                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Failed to set state of friendly ap");
                            return;
                        }
                    }
                }
            }
        }
        IssRfmRogueFriendPageGet (pHttp);
    }
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueMaliciousPage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueMaliciousPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRogueMaliciousPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRogueMaliciousPageSet (pHttp);
    }

}

/*****************************************************************
 * *  Function Name : IssRfmRogueMaliciousPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueMaliciousPageGet (tHttp * pHttp)
{
    INT4                i4ApState = 0;
    UINT4               u4LastHeard = 0;
    INT4                i4Class = 0;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0;
    UINT1               u1aSsid[32];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];

    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];

    tMacAddr            NextApMac;
    tMacAddr            ApMac;

    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (u1aSsid, 0, sizeof (u1aSsid));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

/*   STRCPY (pHttp->au1Name, "ACTION");
   HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
   if ((STRCMP (pHttp->au1Value, "Add"))== 0)
   {
        IssRfmRogueMaliciousPageSet (pHttp);
   }               
    */
    i4OutCome = nmhGetFirstIndexFsRogueApTable (&NextApMac);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {

            pHttp->i4Write = (INT4) u4Temp;

            nmhGetFsRogueApClass (NextApMac, &i4Class);
            if (i4Class == ROGUE_AP_MALICIOUS)
            {

                RfmGuiMacToStr (NextApMac, au1String);
                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "CLASS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Class);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "STATE_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRogueApState (NextApMac, &i4ApState);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ApState);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "LAST_HEARD_TIME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRogueApLastHeardTime (NextApMac, &u4LastHeard);
                UtlGetTimeStrForTicks (u4LastHeard, (CHR1 *) au1TimeLastHeard);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeLastHeard);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            }

            else
            {
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            }

            MEMCPY (ApMac, NextApMac, sizeof (tMacAddr));
            nmhGetNextIndexFsRogueApTable (ApMac, &NextApMac);

        }
        while (MEMCMP (ApMac, NextApMac, sizeof (tMacAddr)) != 0);
    }

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueMaliciousPageSet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueMaliciousPageSet (tHttp * pHttp)
{
    INT4                i4ApState = 0;
    INT4                i4ApClass = 0;
    INT4                i4DelApState = 0;
    INT4                i4Class = 0;
    UINT4               u4ErrorCode = 0;

    tMacAddr            RogueMacAddr;
    tRfMgmtDB           RfMgmtDB;
    tMacAddr            NextApMac;
    tMacAddr            ApMac;

    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (RogueMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if ((STRCMP (pHttp->au1Value, "Add") == 0)
        || (STRCMP (pHttp->au1Value, "Delete") == 0))
    {

        STRCPY (pHttp->au1Name, "BSSID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac ((UINT1 *) pHttp->au1Value, RogueMacAddr);

        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualDB.u1RogueApBSSID, RogueMacAddr, MAC_ADDR_LEN);

        STRCPY (pHttp->au1Name, "CLASS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApClass = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApState = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "ACTION");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "Add") == 0)
        {

            if (nmhTestv2FsRogueApManualClass
                (&u4ErrorCode, RogueMacAddr, i4ApClass) != SNMP_FAILURE)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
                    u1RogueApState = (UINT1) i4ApState;
                RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
                    u1RogueApClass = (UINT1) i4ApClass;
                if (RfMgmtProcessDBMsg
                    (RFMGMT_CREATE_MANUAL_ROGUE_INFO,
                     &RfMgmtDB) == RFMGMT_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) "Failed to set malicious ap");
                    return;

                }
            }
        }
        if (STRCMP (pHttp->au1Value, "Delete") == 0)
        {
            if ((nmhGetFsRogueApManualState (RogueMacAddr, &i4DelApState) ==
                 SNMP_SUCCESS) && (i4ApState == i4DelApState))
            {
                if (RfMgmtProcessDBMsg
                    (RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
                     &RfMgmtDB) == RFMGMT_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to delete malicious ap");
                    return;
                }
            }
            else
            {
                IssSendError (pHttp, (CONST INT1 *) "Ap State is wrong");
                return;
            }
        }
    }
    if (STRCMP (pHttp->au1Value, "DeleteAll") == 0)
    {

        STRCPY (pHttp->au1Name, "AP_STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApState = (INT4) (ATOI (pHttp->au1Value));

        if (nmhGetFirstIndexFsRogueApTable (&RogueMacAddr) != SNMP_FAILURE)
        {
            do
            {
                nmhGetFsRogueApClass (RogueMacAddr, &i4Class);
                if (i4Class == ROGUE_AP_MALICIOUS)
                {

                    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                            RfMgmtRogueManualDB.u1RogueApBSSID,
                            RogueMacAddr, MAC_ADDR_LEN);

                    if ((nmhGetFsRogueApManualState
                         (RogueMacAddr, &i4DelApState) == SNMP_SUCCESS)
                        && (i4ApState == i4DelApState))
                    {
                        if (RfMgmtProcessDBMsg
                            (RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
                             &RfMgmtDB) == RFMGMT_FAILURE)
                        {
                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Failed to delete malicious ap");
                            return;
                        }
                    }
                    /*else
                       {
                       MEMCPY(ApMac,RogueMacAddr,sizeof(tMacAddr));
                       nmhGetNextIndexFsRogueApTable(ApMac, &RogueMacAddr);  

                       } */

                }
                MEMCPY (ApMac, RogueMacAddr, sizeof (tMacAddr));
                nmhGetNextIndexFsRogueApTable (ApMac, &RogueMacAddr);

            }
            while (MEMCMP (ApMac, RogueMacAddr, sizeof (tMacAddr)) != 0);

        }
    }
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "AP_MAC");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac ((UINT1 *) pHttp->au1Value, RogueMacAddr);

        /*  MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
           RfMgmtRogueManualDB.u1RogueApBSSID,
           RogueMacAddr,
           MAC_ADDR_LEN);
         */
        STRCPY (pHttp->au1Name, "CLASS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApClass = (INT4) (ATOI (pHttp->au1Value));

        STRCPY (pHttp->au1Name, "STATE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4ApState = (INT4) (ATOI (pHttp->au1Value));
        /* 
           RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.u1RogueApClass = (UINT1)i4ApClass;
           RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.u1RogueApState = (UINT1)i4ApState;
           if(RfMgmtProcessDBMsg (RFMGMT_SET_MANUAL_ROGUE_INFO, &RfMgmtDB) == RFMGMT_FAILURE)
           {
           IssSendError(pHttp,(CONST INT1*) "Failed to set malicious ap");
           return ;
           }
         */
        if (nmhTestv2FsRogueApManualClass
            (&u4ErrorCode, RogueMacAddr, i4ApClass) == SNMP_SUCCESS)
        {
            if (nmhSetFsRogueApManualClass (RogueMacAddr, i4ApClass) ==
                SNMP_SUCCESS)
            {
                if (i4ApState == ROGUE_AP_ALERT
                    || i4ApState == ROGUE_AP_CONTAIN)
                {
                    if (nmhTestv2FsRogueApManualState
                        (&u4ErrorCode, RogueMacAddr, i4ApState) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsRogueApManualState (RogueMacAddr, i4ApState)
                            != SNMP_SUCCESS)
                        {

                            IssSendError (pHttp,
                                          (CONST INT1 *)
                                          "Failed to set malicious ap");
                            return;
                        }
                    }
                }
                else
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Failed to set malicious ap, state cannot be none");
                    return;
                }

            }
        }
        IssRfmRogueMaliciousPageGet (pHttp);
    }
/*    IssRfmRogueMaliciousPageGet(pHttp);*/
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueUnclassifyPage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueUnclassifyPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRogueUnclassifyPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRogueUnclassifyPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRogueUnclassifyPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueUnclassifyPageGet (tHttp * pHttp)
{
    UINT4               u4LastHeard = 0;
    INT4                i4Class = 0;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0;

    UINT1               u1aSsid[32];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];

    tMacAddr            NextApMac;
    tMacAddr            ApMac;

    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];

    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (u1aSsid, 0, sizeof (u1aSsid));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    i4OutCome = nmhGetFirstIndexFsRogueApTable (&NextApMac);
    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {

            pHttp->i4Write = (INT4) u4Temp;

            nmhGetFsRogueApClass (NextApMac, &i4Class);
            if ((i4Class != ROGUE_AP_FRIENDLY)
                && (i4Class != ROGUE_AP_MALICIOUS))
            {
                RfmGuiMacToStr (NextApMac, au1String);
                STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "LAST_HEARD_TIME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFsRogueApLastHeardTime (NextApMac, &u4LastHeard);
                UtlGetTimeStrForTicks (u4LastHeard, (CHR1 *) au1TimeLastHeard);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeLastHeard);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "CLASS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Class);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            }
            else
            {
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            }

            MEMCPY (ApMac, NextApMac, sizeof (tMacAddr));
            nmhGetNextIndexFsRogueApTable (ApMac, &NextApMac);
        }
        while (MEMCMP (ApMac, NextApMac, sizeof (tMacAddr)) != 0);
    }

    WebnmUnRegisterLock (pHttp);
    WebnmUnRegisterContext (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueUnclassifyPageSet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueUnclassifyPageSet (tHttp * pHttp)
{
    INT4                i4Class = 0;

    tMacAddr            RogueMacAddr;
    tRfMgmtDB           RfMgmtDB;
    tMacAddr            NextApMac;
    tMacAddr            ApMac;

    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (RogueMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "DeleteAll") == 0)
    {

        if (nmhGetFirstIndexFsRogueApTable (&NextApMac) != SNMP_FAILURE)
        {
            while (MEMCMP (ApMac, NextApMac, sizeof (tMacAddr)) != 0)
            {
                nmhGetFsRogueApClass (NextApMac, &i4Class);
                if (i4Class == ROGUE_AP_UNCLASSIFY)
                {
                    /* MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                     *               RfMgmtRogueApDB.u1RogueApBSSID,
                     *                               NextApMac,
                     *                                             MAC_ADDR_LEN);*/
                    /* Raki */
                    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
                            RfMgmtRogueManualDB.u1RogueApBSSID,
                            NextApMac, MAC_ADDR_LEN);

                    if (RfMgmtProcessDBMsg
                        (RFMGMT_DESTROY_MANUAL_ROGUE_INFO,
                         &RfMgmtDB) == RFMGMT_FAILURE)
                    {
                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "Failed to delete unclassify ap");
                        return;
                    }
                }
                MEMCPY (ApMac, NextApMac, sizeof (tMacAddr));
                nmhGetNextIndexFsRogueApTable (ApMac, &NextApMac);

            }

        }
    }
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueDetailedSummaryPage
 * *  Description   : This function processes the request coming for the
 * *                  RF global Configurations Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueDetailedSummaryPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmRogueDetailedSummaryPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmRogueDetailedSummaryPageSet (pHttp);
    }

}

/*********************************************************************
 * *  Function Name : IssRfmRogueDetailedSummaryPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueDetailedSummaryPageGet (tHttp * pHttp)
{
    INT4                i4Protect = 0;
    INT4                i4Rssi = 0;
    INT4                i4Channel = 0;
    INT4                i4DetectRadio = 0;
    INT4                i4Band = 0;
    INT4                i4Status = 0;
    UINT4               u4ClientCount = 0;
    UINT4               u4LastHeard = 0;
    UINT4               u4LastReport = 0;
    INT4                i4Class = 0;
    INT4                i4State = 0;
    UINT4               u4Temp = 0;

    UINT1               u1aSsid[32];
    UINT1               au1String[CLI_MAC_TO_STR_LEN];

    CHR1                au1TimeLastHeard[RFMGMT_MAX_DATE_LEN];
    CHR1                au1ReportTime[RFMGMT_MAX_DATE_LEN];

    tMacAddr            NextApMac;
    tMacAddr            ApMac;
    tSNMP_OCTET_STRING_TYPE Ssid;

    Ssid.pu1_OctetList = u1aSsid;
    MEMSET (NextApMac, 0, sizeof (tMacAddr));
    MEMSET (ApMac, 0, sizeof (tMacAddr));
    MEMSET (u1aSsid, 0, sizeof (u1aSsid));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    pHttp->i4Write = (INT4) u4Temp;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Show") == 0)
    {
        STRCPY (pHttp->au1Name, "BSSID");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        StrToMac ((UINT1 *) pHttp->au1Value, NextApMac);

        CliMacToStr (NextApMac, au1String);
        STRCPY (pHttp->au1KeyString, "AP_MAC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SSID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApSSID (NextApMac, &Ssid);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", Ssid.pu1_OctetList);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTECT_TYPE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApProcetionType (NextApMac, &i4Protect);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protect);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* STRCPY (pHttp->au1KeyString, "SNR_KEY");
           WebnmSendString (pHttp, pHttp->au1KeyString);
           nmhGetFsRogueApSNR(NextApMac, &i4Snr);
           SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Snr);
           WebnmSockWrite (pHttp, pHttp->au1DataString,
           (INT4) HTTP_STRLEN (pHttp->au1DataString));
           WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG); */

        STRCPY (pHttp->au1KeyString, "RSSI_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApRSSI (NextApMac, &i4Rssi);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Rssi);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "CHANNEL_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApOperationChannel (NextApMac, &i4Channel);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Channel);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DETECTING_RADIO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApDetectedRadio (NextApMac, &i4DetectRadio);
        if (i4DetectRadio == 1)
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "B");
        else
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "A");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DETECTING_BAND_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApDetectedBand (NextApMac, &i4Band);
        if (i4DetectRadio == 1)
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "2.4GHz");
        else
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "5GHz");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LAST_REPORT_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApLastReportedTime (NextApMac, &u4LastReport);
        UtlGetTimeStrForTicks (u4LastReport, (CHR1 *) au1ReportTime);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1ReportTime);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LAST_HEARD_TIME_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApLastHeardTime (NextApMac, &u4LastHeard);
        UtlGetTimeStrForTicks (u4LastHeard, (CHR1 *) au1TimeLastHeard);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeLastHeard);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApState (NextApMac, &i4State);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4State);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "CLASS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApClass (NextApMac, &i4Class);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Class);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApStatus (NextApMac, &i4Status);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Status);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "CLIENT_COUNT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetFsRogueApClientCount (NextApMac, &u4ClientCount);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ClientCount);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmUnRegisterLock (pHttp);
        WebnmUnRegisterContext (pHttp);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));

    }
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmRogueDetailedSummaryPageSet
 * *  Description   : This function processes the get request coming for the
 * *                  RF global Configurations Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmRogueDetailedSummaryPageSet (tHttp * pHttp)
{
    /* 

       tMacAddr RogueMacAddr;
       tRfMgmtDB                RfMgmtDB;

       MEMSET(RogueMacAddr, 0 ,sizeof(tMacAddr));
       MEMSET(&RfMgmtDB, 0 ,sizeof(tRfMgmtDB));

       STRCPY(pHttp->au1Name,"BSSID");
       HttpGetValuebyName(pHttp->au1Name,pHttp->au1Value,pHttp->au1PostQuery);
       issDecodeSpecialChar (pHttp->au1Value);
       StrToMac ((UINT1 *) pHttp->au1Value, RogueMacAddr);

       MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
       RfMgmtRogueManualDB.u1RogueApBSSID,
       RogueMacAddr,
       MAC_ADDR_LEN);
     */
    IssRfmRogueDetailedSummaryPageGet (pHttp);
    return;

}
#endif

/*********************************************************************
 * *  Function Name : IssRfmTpcInfoPage
 * *  Description   : This function processes the request coming for the
 * *                  Tpc Info Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmTpcInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmTpcInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmTpcInfoPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmTpcInfoPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  Tpc Info Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmTpcInfoPageGet (tHttp * pHttp)
{

    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    INT4                i4OutCome = 0, i4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Type = 0;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tMacAddr            StationMacAddr;
    tMacAddr            NextStationMacAddr;
    INT4                i4Index = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIndex = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    INT4                i4LinkMargin = 0;
    INT4                i4BaseLinkMargin = 0;
    UINT4               u4TxPowerLevel = 0;
    UINT4               u4ReportLastReceived = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&StationMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextStationMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
/*    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);*/
/*    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);*/
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));

    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable (u4currentProfileId,
                                                     &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);
        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                           u4currentBindingId,
                                           i4RadioType,
                                           &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrm11hTpcInfoTable
                        (&i4NextIndex, &NextStationMacAddr) != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        MEMCPY (&StationMacAddr, &NextStationMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {

                            nmhGetFsRrm11hLinkMargin (i4Index, StationMacAddr,
                                                      &i4LinkMargin);
                            nmhGetFsRrmAPTxPowerLevel (i4Index,
                                                       &u4TxPowerLevel);
                            nmhGetFsRrm11hTpcReportLastReceived (i4Index,
                                                                 StationMacAddr,
                                                                 &u4ReportLastReceived);
                            nmhGetFsRrm11hBaseLinkMargin (i4Index,
                                                          StationMacAddr,
                                                          &i4BaseLinkMargin);

                            CliMacToStr (StationMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString,
                                     "%s", gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) (HTTP_STRLEN
                                                    (pHttp->au1DataString)));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "BASE_LINK_MARGIN_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4BaseLinkMargin);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "LINK_MARGIN_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     i4LinkMargin);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "TX_POWER_LEVEL_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4TxPowerLevel);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString,
                                    "LAST_REPORT_RECEIVED");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ReportLastReceived);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrm11hTpcInfoTable
                           (i4Index, &i4NextIndex, StationMacAddr,
                            &NextStationMacAddr) == SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        if (nmhGetFirstIndexFsRrm11hTpcInfoTable (&i4NextIndex,
                                                  &NextStationMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            i4Index = i4NextIndex;
            MEMCPY (&StationMacAddr, &NextStationMacAddr, sizeof (tMacAddr));

            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;

                }
                nmhGetFsRrm11hLinkMargin (i4Index, StationMacAddr,
                                          &i4LinkMargin);
                nmhGetFsRrmAPTxPowerLevel (i4Index, &u4TxPowerLevel);
                nmhGetFsRrm11hTpcReportLastReceived (i4Index, StationMacAddr,
                                                     &u4ReportLastReceived);
                nmhGetFsRrm11hBaseLinkMargin (i4Index, StationMacAddr,
                                              &i4BaseLinkMargin);

                CliMacToStr (StationMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "BASE_LINK_MARGIN_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BaseLinkMargin);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "LINK_MARGIN_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4LinkMargin);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "TX_POWER_LEVEL_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TxPowerLevel);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "LAST_REPORT_RECEIVED");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ReportLastReceived);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }

        }
        while (nmhGetNextIndexFsRrm11hTpcInfoTable (i4Index, &i4NextIndex,
                                                    StationMacAddr,
                                                    &NextStationMacAddr) ==
               SNMP_SUCCESS);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmTpcInfoPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  Tpc Info Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmTpcInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmTpcInfoPageGet (pHttp);

    return;

}

/*********************************************************************
 * *  Function Name : IssRfmTpcConfigPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AH TPC Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmTpcConfigPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmTpcConfigPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmTpcConfigPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmTpcConfigPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AH TPC page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmTpcConfigPageGet (tHttp * pHttp)
{
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i411hTpcStatus = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4TpcLastRun = 0;
    UINT4               u4MinLintThreshold = 0;
    UINT4               u4MaxLintThreshold = 0;
    UINT4               u4StaCountThreshold = 0;

    STRCPY (pHttp->au1KeyString, "TPC_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hTpcStatus (i4GetRadioType, &i411hTpcStatus);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i411hTpcStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TPC_INTERVAL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hTpcInterval (i4GetRadioType, &u4TpcInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "MIN_LINK_THRESHOLD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hMinLinkThreshold (i4GetRadioType, &u4MinLintThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MinLintThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "MAX_LINK_THRESHOLD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hMaxLinkThreshold (i4GetRadioType, &u4MaxLintThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4MaxLintThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "STA_COUNT_THRESHOLD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hStaCountThreshold (i4GetRadioType, &u4StaCountThreshold);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4StaCountThreshold);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "TPC_LASTRUN_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hTpcLastRun (i4GetRadioType, &u4TpcLastRun);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TpcLastRun);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmTpcConfigPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AH TPC page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmTpcConfigPageSet (tHttp * pHttp)
{
    UINT4               u4ErrCode = 0;
    INT4                i4GetRadioType = RFMGMT_RADIO_TYPEA;
    INT4                i411hTpcStatus = 0;
    UINT4               u4TpcInterval = 0;
    UINT4               u4MinLinkThreshold = 0;
    UINT4               u4MaxLinkThreshold = 0;
    UINT4               u4StaCountThreshold = 0;
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4GetRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }
    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4GetRadioType, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {

        STRCPY (pHttp->au1Name, "TPC_STATUS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i411hTpcStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        if (nmhTestv2FsRrm11hTpcStatus (&u4ErrCode, i4GetRadioType,
                                        i411hTpcStatus) != SNMP_FAILURE)
        {
            if (nmhSetFsRrm11hTpcStatus (i4GetRadioType,
                                         i411hTpcStatus) == SNMP_FAILURE)
            {
                IssSendError (pHttp,
                              (CONST INT1 *) "Unable to configure Tpc Status ");
                return;
            }
        }
        if (i411hTpcStatus == 1)
        {
            STRCPY (pHttp->au1Name, "TPC_INTERVAL");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4TpcInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

            if (nmhTestv2FsRrm11hTpcInterval (&u4ErrCode, i4GetRadioType,
                                              u4TpcInterval) != SNMP_FAILURE)
            {
                if (nmhSetFsRrm11hTpcInterval (i4GetRadioType, u4TpcInterval)
                    == SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to configure TPC Interval ");
                    return;
                }
            }

            STRCPY (pHttp->au1Name, "MIN_LINK_THRESHOLD");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4MinLinkThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

            if (nmhTestv2FsRrm11hMinLinkThreshold (&u4ErrCode, i4GetRadioType,
                                                   u4MinLinkThreshold) !=
                SNMP_FAILURE)
            {
                if (nmhSetFsRrm11hMinLinkThreshold
                    (i4GetRadioType, u4MinLinkThreshold) == SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to configure Minimum Link Threshold ");
                    return;
                }
            }

            STRCPY (pHttp->au1Name, "MAX_LINK_THRESHOLD");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4MaxLinkThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

            if (nmhTestv2FsRrm11hMaxLinkThreshold (&u4ErrCode, i4GetRadioType,
                                                   u4MaxLinkThreshold) !=
                SNMP_FAILURE)
            {
                if (nmhSetFsRrm11hMaxLinkThreshold
                    (i4GetRadioType, u4MaxLinkThreshold) == SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to configure Maximum Link Threshold ");
                    return;
                }
            }

            STRCPY (pHttp->au1Name, "STA_COUNT_THRESHOLD");
            HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery);
            u4StaCountThreshold = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

            if (nmhTestv2FsRrm11hStaCountThreshold (&u4ErrCode, i4GetRadioType,
                                                    u4StaCountThreshold) !=
                SNMP_FAILURE)
            {
                if (nmhSetFsRrm11hStaCountThreshold
                    (i4GetRadioType, u4StaCountThreshold) == SNMP_FAILURE)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "Unable to configure Station Count Threshold ");
                    return;
                }
            }
        }
    }
    if (i4RowStatus == NOT_READY)
    {
        i4RowStatus = NOT_IN_SERVICE;
    }
    if (nmhSetFsRrmRowStatus (i4GetRadioType, i4RowStatus) == SNMP_FAILURE)
    {
        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to configure Rrm Row status ");
        return;
    }

    IssRfmTpcConfigPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmTpcWirelessPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AH TPC Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmTpcWirelessPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmTpcWirelessPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmTpcWirelessPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmTpcWirelessPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AH TPC page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmTpcWirelessPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0, nextWlanProfileId =
        0, currentWlanProfileId = 0;
    INT4                i4OutCome = 0, i4IfIndex = 0;
    UINT1               WLANSSID[OCTETSTR_SIZE];
    INT4                i4PowerConstraint = 0;
    INT4                i4Spectrum = 0;

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        wsscfgFsDot11WlanAuthenticationProfileEntry;
    tWsscfgDot11SpectrumManagementEntry wsscfgDot11SpectrumManagementEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;

    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&wsscfgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&wsscfgDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgDot11SpectrumManagementEntry));
    MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&nextWlanProfileId);

    if (i4OutCome == SNMP_FAILURE)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }
    else
    {
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            MEMSET (WLANSSID, 0, OCTETSTR_SIZE);

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (nextWlanProfileId, &i4IfIndex) != SNMP_SUCCESS)
            {
            }
            wsscfgDot11SpectrumManagementEntry.MibObject.i4IfIndex = i4IfIndex;
            wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.i4IfIndex =
                i4IfIndex;

            WssCfGetDot11DesiredSSID ((UINT4) i4IfIndex, WLANSSID);
            STRCPY (pHttp->au1KeyString, "SSID_KEY");
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", WLANSSID);
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            if (WsscfgGetAllDot11SpectrumManagementTable
                (&wsscfgDot11SpectrumManagementEntry) != OSIX_FAILURE)
            {
                i4PowerConstraint =
                    wsscfgDot11SpectrumManagementEntry.MibObject.
                    i4Dot11MitigationRequirement;
                STRCPY (pHttp->au1KeyString, "POWER_CONSTRAINT_KEY");
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         i4PowerConstraint);

                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (WsscfgGetAllFsDot11WlanCapabilityProfileTable
                (&wsscfgFsDot11WlanCapabilityProfileEntry) == OSIX_SUCCESS)
            {
                i4Spectrum =
                    wsscfgFsDot11WlanCapabilityProfileEntry.MibObject.
                    i4FsDot11WlanSpectrumManagementRequired;

                STRCPY (pHttp->au1KeyString, "SPECTRUM_KEY");

                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Spectrum);
            }
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

            /* Copy next index to current index */
            currentWlanProfileId = nextWlanProfileId;
        }
        while (nmhGetNextIndexCapwapDot11WlanTable
               (currentWlanProfileId, &nextWlanProfileId) == SNMP_SUCCESS);

        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
}

/*********************************************************************
 * *  Function Name : IssRfmTpcWirelessPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AH TPC page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmTpcWirelessPageSet (tHttp * pHttp)
{
    INT4                i4Spectrum = 0;
    INT4                i4PowerConstraint = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfIndex = 0;
    UINT1               au1DesiredSSID[32];
    UINT4               u4WlanIfIndex = 0;

    tWsscfgDot11SpectrumManagementEntry wsscfgDot11SpectrumManagementEntry;
    tWsscfgIsSetDot11SpectrumManagementEntry
        wsscfgIsSetDot11SpectrumManagementEntry;
    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        wsscfgFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        WsscfgIsSetFsDot11CapabilityProfileEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        WsscfgIsSetFsDot11WlanCapabilityProfileEntry;

    MEMSET (au1DesiredSSID, 0, 32);
    MEMSET (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&wsscfgDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgDot11SpectrumManagementEntry));
    MEMSET (&wsscfgIsSetDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgIsSetDot11SpectrumManagementEntry));
    MEMSET (&wsscfgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    STRCPY (pHttp->au1Name, "SSID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    MEMCPY (au1DesiredSSID, pHttp->au1Value, sizeof (au1DesiredSSID));
    au1DesiredSSID[(sizeof (au1DesiredSSID) - 1)] = '\0';

    if (WssCfgGetWlanIfIndexfromSSID (au1DesiredSSID, &u4WlanIfIndex) !=
        OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unknown index received");
        return;
    }

    i4IfIndex = (INT4) u4WlanIfIndex;

    WebnmRegisterLock (pHttp, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    STRCPY (pHttp->au1Name, "Power_Constraint");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PowerConstraint = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    wsscfgDot11SpectrumManagementEntry.MibObject.
        i4Dot11MitigationRequirement = i4PowerConstraint;

    wsscfgDot11SpectrumManagementEntry.MibObject.i4IfIndex = i4IfIndex;
    wsscfgDot11SpectrumManagementEntry.MibObject.i4Dot11MitigationRequirement =
        i4PowerConstraint;

    wsscfgIsSetDot11SpectrumManagementEntry.bDot11MitigationRequirement =
        OSIX_TRUE;
    wsscfgIsSetDot11SpectrumManagementEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllDot11SpectrumManagementTable
        (&u4ErrorCode, &wsscfgDot11SpectrumManagementEntry,
         &wsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WSSCFG_UNLOCK;
        return;
    }
    if (WsscfgSetAllDot11SpectrumManagementTable
        (&wsscfgDot11SpectrumManagementEntry,
         &wsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WSSCFG_UNLOCK;
        return;
    }
    nmhSetDot11MitigationRequirement (i4IfIndex, 0, i4PowerConstraint);

    STRCPY (pHttp->au1Name, "Spectrum_management");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Spectrum = (INT4) ATOI ((INT1 *) pHttp->au1Value);

    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanSpectrumManagementRequired = i4Spectrum;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.
        i4FsDot11WlanCapabilityRowStatus = ACTIVE;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.i4IfIndex = i4IfIndex;

    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanSpectrumManagementRequired = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgTestAllFsDot11WlanCapabilityProfileTable
        (&u4ErrorCode, &WsscfgSetFsDot11WlanCapabilityProfileEntry,
         &WsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WSSCFG_UNLOCK;
        return;
    }

    if (WsscfgSetAllFsDot11WlanCapabilityProfileTable
        (&WsscfgSetFsDot11WlanCapabilityProfileEntry,
         &WsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Configuration\n");
        WSSCFG_UNLOCK;
        return;
    }
    WSSCFG_UNLOCK;

    IssRfmTpcWirelessPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmTpcRequestPage
 * *  Description   : This function processes the request coming for the
 * *                  Tpc Info Page.
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmTpcRequestPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmTpcRequestPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmTpcRequestPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmTpcRequestPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  Tpc Info Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmTpcRequestPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Index = 0;
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4RequestInterval = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) (pHttp->i4Write);

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        u4currentProfileId = u4nextProfileId;
        u4currentBindingId = u4nextBindingId;

        if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                   u4currentBindingId,
                                   i4RadioType, &i4Index) == OSIX_SUCCESS)
        {
            MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) ("DB Access failed\r\n"));
                continue;
            }

            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) ("DB Access failed\r\n"));
                continue;
            }

            nmhGetFsRrm11hTPCRequestInterval (i4Index, &u4RequestInterval);

            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "%s", pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4currentBindingId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "TPC_REQUEST_INTERVAL_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RequestInterval);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        }

    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable
           (u4currentProfileId, &u4nextProfileId,
            u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;

}

/*********************************************************************
 * *  Function Name : IssRfmTpcRequestPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  Tpc Info Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 * *********************************************************************/

VOID
IssRfmTpcRequestPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE tProfileName;
    INT4                u1RadioId = 0;
    UINT4               u4TpcRequestInterval = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;

    MEMSET (&tProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    STRCPY (pHttp->au1Name, "Radio_id");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1RadioId = ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Tpc_request_interval");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TpcRequestInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Profile_name");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    tProfileName.pu1_OctetList = pHttp->au1Value;
    tProfileName.i4_Length = (INT4) STRLEN (pHttp->au1Value);

    if (CapwapGetWtpProfileIdFromProfileName
        (tProfileName.pu1_OctetList, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "No profile found");
        return;
    }

    if (RfMgmtGetRadioIfIndex (u4WtpProfileId, (UINT4) u1RadioId,
                               i4RadioType, &i4IfIndex) == OSIX_SUCCESS)
    {

        if (nmhTestv2FsRrm11hTPCRequestInterval (&u4ErrorCode, i4IfIndex,
                                                 u4TpcRequestInterval) !=
            SNMP_SUCCESS)
        {

            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "Test Failed");
            return;

        }
        if (nmhSetFsRrm11hTPCRequestInterval (i4IfIndex,
                                              u4TpcRequestInterval) !=
            SNMP_SUCCESS)
        {
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Configuration - Tpc Request Interval\n");
            return;
        }
    }
    IssRfmTpcRequestPageGet (pHttp);

    return;
}

/*********************************************************************
 * *  Function Name : IssRfmDfsConfigGlobalPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AH DFS Global Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsConfigGlobalPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmDfsConfigGlobalPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmDfsConfigGlobalPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmDfsConfigGlobalPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AH DFS Global page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsConfigGlobalPageGet (tHttp * pHttp)
{
    INT4                i411hDfsStatus = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    UINT4               u4DfsInterval = 0;

    STRCPY (pHttp->au1KeyString, "DFS_INTERVAL_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hDfsInterval (i4RadioType, &u4DfsInterval);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DfsInterval);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "DFS_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    nmhGetFsRrm11hDfsStatus (i4RadioType, &i411hDfsStatus);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i411hDfsStatus);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmDfsConfigGlobalPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AH DFS Global page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsConfigGlobalPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i411hDfsStatus = 0;
    UINT4               u4DfsInterval = 0;

    if (nmhGetFsRrmRowStatus (i4RadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Unable to get Rrm Row Status ");
        return;
    }

    if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsRrmRowStatus (i4RadioType, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set Rrm Row status ");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Array, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Array, "Apply") == 0)
    {
        STRCPY (pHttp->au1Name, "DFS_STATUS");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i411hDfsStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        nmhSetFsRrm11hDfsStatus (i4RadioType, i411hDfsStatus);

        STRCPY (pHttp->au1Name, "DFS_INT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DfsInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

        nmhSetFsRrm11hDfsInterval (i4RadioType, u4DfsInterval);
    }

    IssRfmDfsConfigGlobalPageGet (pHttp);

    return;
}

/*********************************************************************
 * *  Function Name : IssRfmDfsConfigPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AH DFS Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsConfigPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmDfsConfigPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmDfsConfigPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmDfsConfigPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AH DFS page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsConfigPageGet (tHttp * pHttp)
{
    UINT4               u4Temp = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i4Index = 0;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    UINT4               u4DfsQuietPeriod = 0;
    UINT4               u4DfsMeasurementInterval = 0;
    UINT4               u4DfsQuietInterval = 0;
    INT4                i411hDfsChannelSwitchStatus = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) (pHttp->i4Write);

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        u4currentProfileId = u4nextProfileId;
        u4currentBindingId = u4nextBindingId;

        if (RfMgmtGetRadioIfIndex (u4currentProfileId,
                                   u4currentBindingId, i4RadioType,
                                   &i4Index) == OSIX_SUCCESS)
        {
            MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          (&RadioIfGetDB)) != OSIX_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) ("DB Access failed\r\n"));
                continue;
            }

            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                IssSendError (pHttp, (CONST INT1 *) ("DB Access failed\r\n"));
                continue;
            }

            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString,
                     "%s", pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) (HTTP_STRLEN (pHttp->au1DataString)));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4currentBindingId);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) HTTP_STRLEN (pHttp->au1DataString));
            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
            WebnmSendString (pHttp, pHttp->au1KeyString);

            STRCPY (pHttp->au1KeyString, "DFS_QUIET_INT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRrm11hDFSQuietInterval (i4Index, &u4DfsQuietInterval);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DfsQuietInterval);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DFS_MEAS_INT_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRrm11hDFSMeasurementInterval (i4Index,
                                                  &u4DfsMeasurementInterval);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     u4DfsMeasurementInterval);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DFS_QUIET_PERIOD_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRrm11hDFSQuietPeriod (i4Index, &u4DfsQuietPeriod);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4DfsQuietPeriod);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DFS_CHAN_SW_STATUS_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetFsRrm11hDFSChannelSwitchStatus (i4Index,
                                                  &i411hDfsChannelSwitchStatus);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                     i411hDfsChannelSwitchStatus);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }
    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable
           (u4currentProfileId, &u4nextProfileId,
            u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmDfsConfigPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AH DFS page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsConfigPageSet (tHttp * pHttp)
{
    INT4                u1RadioId = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    INT4                i411hDfsChannelSwitchStatus = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4DfsQuietInterval = 0;
    UINT4               u4DfsMeasurementInterval = 0;
    UINT4               u4DfsQuietPeriod = 0;
    tSNMP_OCTET_STRING_TYPE tProfileName;

    MEMSET (&tProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    STRCPY (pHttp->au1Name, "Radio_id");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u1RadioId = ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Dfs_quiet_interval");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4DfsQuietInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Dfs_meas_interval");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4DfsMeasurementInterval = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Dfs_quiet_period");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4DfsQuietPeriod = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Dfs_chan_sw_status");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i411hDfsChannelSwitchStatus = (UINT4) ATOI ((INT1 *) pHttp->au1Value);

    STRCPY (pHttp->au1Name, "Profile_name");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    tProfileName.pu1_OctetList = pHttp->au1Value;
    tProfileName.i4_Length = (INT4) STRLEN (pHttp->au1Value);

    if (CapwapGetWtpProfileIdFromProfileName
        (tProfileName.pu1_OctetList, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "No profile found");
        return;
    }

    if (RfMgmtGetRadioIfIndex (u4WtpProfileId, (UINT4) u1RadioId,
                               i4RadioType, &i4IfIndex) == OSIX_SUCCESS)
    {
        if (nmhTestv2FsRrm11hDFSQuietInterval (&u4ErrorCode, i4IfIndex,
                                               u4DfsQuietInterval) !=
            SNMP_FAILURE)
        {
            if (nmhSetFsRrm11hDFSQuietInterval (i4IfIndex,
                                                u4DfsQuietInterval) ==
                SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure DFS Quiet Interval ");
                return;
            }
        }

        if (nmhTestv2FsRrm11hDFSMeasurementInterval (&u4ErrorCode,
                                                     i4IfIndex,
                                                     u4DfsMeasurementInterval)
            != SNMP_FAILURE)
        {
            if (nmhSetFsRrm11hDFSMeasurementInterval (i4IfIndex,
                                                      u4DfsMeasurementInterval)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure DFS Measurement Interval ");
                return;
            }
        }

        if (nmhTestv2FsRrm11hDFSQuietPeriod (&u4ErrorCode,
                                             i4IfIndex,
                                             u4DfsQuietPeriod) != SNMP_FAILURE)
        {
            if (nmhSetFsRrm11hDFSQuietPeriod (i4IfIndex,
                                              u4DfsQuietPeriod) == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure DFS Quiet Period ");
                return;
            }
        }

        if (nmhTestv2FsRrm11hDFSChannelSwitchStatus (&u4ErrorCode,
                                                     i4IfIndex,
                                                     i411hDfsChannelSwitchStatus)
            != SNMP_FAILURE)
        {
            if (nmhSetFsRrm11hDFSChannelSwitchStatus (i4IfIndex,
                                                      i411hDfsChannelSwitchStatus)
                == SNMP_FAILURE)
            {
                IssSendError (pHttp, (CONST INT1 *)
                              "Unable to configure Dfs Status ");
                return;
            }
        }
    }

    IssRfmDfsConfigPageGet (pHttp);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmDfsInfoPage
 * *  Description   : This function processes the request coming for the
 * *                  RF Radio_AH TPC Page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsInfoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssRfmDfsInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssRfmDfsInfoPageSet (pHttp);
    }
}

/*********************************************************************
 * *  Function Name : IssRfmDfsInfoPageGet
 * *  Description   : This function processes the get request coming for the
 * *                  RF Radio_AH TPC page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/

VOID
IssRfmDfsInfoPageGet (tHttp * pHttp)
{
    INT4                i4Type = 0;
    INT4                i4Index = 0;
    INT4                i4NextIndex = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4MeasureMapRadar = 0;
    INT4                i4OutCome = 0, i4RetVal = 0;
    INT4                i4RadioType = RFMGMT_RADIO_TYPEA;
    UINT1               u1Temp = 0;
    UINT1               u1ChannelNum = 0;
    UINT1               u1MeasureMapRadar = 0;
    UINT1               au1ProfileName[ISS_MAX_ADDR_BUFFER];
    UINT1               au1String[RFMGMT_MAC_STRING_LEN];
    UINT4               u4Temp = 0;
    UINT4               u4ReportLastReceived = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT4               u4currentProfileId = 0, u4nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    tMacAddr            StationMacAddr;
    tMacAddr            NextStationMacAddr;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tSNMP_OCTET_STRING_TYPE ProfileName;

    WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN (pWssIfCapwapDB)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1String, 0, RFMGMT_MAC_STRING_LEN);
    MEMSET (&StationMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&NextStationMacAddr, 0, sizeof (tMacAddr));

    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
    ProfileName.pu1_OctetList = au1ProfileName;

    i4OutCome = nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return;
    }

    STRCPY (pHttp->au1KeyString, "<! AP_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    do
    {
        u4currentProfileId = u4nextProfileId;

        MEMSET (au1ProfileName, 0, ISS_MAX_ADDR_BUFFER);
        MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        ProfileName.pu1_OctetList = au1ProfileName;

        i4RetVal = nmhGetCapwapBaseWtpProfileName (u4currentProfileId,
                                                   &ProfileName);
        if (SNMP_FAILURE == i4RetVal)
        {
            continue;
        }

        STRCPY (pHttp->au1Name, "AP_NAME");
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%s\" selected>%s \n",
                 ProfileName.pu1_OctetList, ProfileName.pu1_OctetList);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) (STRLEN (pHttp->au1DataString)));
    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable
           (u4currentProfileId, &u4nextProfileId));

    if (gi4ApOption == RFMGMT_AP_PROFILE)
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4currentProfileId = 0;
        u4nextProfileId = 0;
        i4Index = 0;
        u4Temp = (UINT4) (pHttp->i4Write);

        if (CapwapGetWtpProfileIdFromProfileName
            (gau1ApName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }

        do
        {
            pHttp->i4Write = (INT4) u4Temp;

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (u4currentProfileId == u4CapwapBaseWtpProfileId)
            {
                if (RfMgmtGetRadioIfIndex
                    (u4currentProfileId, u4currentBindingId, i4RadioType,
                     &i4Index) == OSIX_SUCCESS)
                {
                    i4RadioIfIndex = i4Index;
                    if (nmhGetFirstIndexFsRrm11hDfsInfoTable
                        (&i4NextIndex, &NextStationMacAddr) != SNMP_SUCCESS)
                    {
                        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        WebnmSockWrite (pHttp,
                                        (UINT1 *) (pHttp->pi1Html +
                                                   pHttp->i4Write),
                                        (pHttp->i4HtmlSize - pHttp->i4Write));
                        continue;
                    }
                    do
                    {
                        pHttp->i4Write = (INT4) u4Temp;
                        i4Index = i4NextIndex;
                        MEMCPY (&StationMacAddr, &NextStationMacAddr,
                                sizeof (tMacAddr));

                        if (i4Index == i4RadioIfIndex)
                        {
                            nmhGetFsRrm11hChannelNumber (i4Index,
                                                         StationMacAddr,
                                                         &u1ChannelNum);
                            nmhGetFsRrm11hMeasureMapRadar (i4Index,
                                                           StationMacAddr,
                                                           &i4MeasureMapRadar);
                            nmhGetFsRrm11hDfsLastRun (i4RadioType,
                                                      &u4ReportLastReceived);

                            CliMacToStr (StationMacAddr, au1String);
                            STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     gau1ApName);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) (HTTP_STRLEN
                                                    (pHttp->au1DataString)));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4currentBindingId);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                                     au1String);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "CH_NUM_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u1ChannelNum);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "MEAS_RADAR_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u1MeasureMapRadar);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "REPORT_RECVD_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u4ReportLastReceived);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);

                            STRCPY (pHttp->au1KeyString, "MEAS_BSS_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u1Temp);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                            STRCPY (pHttp->au1KeyString, "MEAS_OFDM_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u1Temp);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                            STRCPY (pHttp->au1KeyString, "MEAS_UNMEASURED_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u1Temp);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                            STRCPY (pHttp->au1KeyString,
                                    "MEAS_UNIDENTIFIED_KEY");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                                     u1Temp);
                            WebnmSockWrite (pHttp, pHttp->au1DataString,
                                            (INT4) HTTP_STRLEN (pHttp->
                                                                au1DataString));
                            STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                            WebnmSendString (pHttp, pHttp->au1KeyString);
                            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
                        }
                    }
                    while (nmhGetNextIndexFsRrm11hDfsInfoTable
                           (i4Index, &i4NextIndex, StationMacAddr,
                            &NextStationMacAddr) == SNMP_SUCCESS);
                }
                else
                {
                    continue;
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId, u4currentBindingId,
                &u4nextBindingId) == SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    else
    {
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        u4Temp = (UINT4) pHttp->i4Write;

        if (nmhGetFirstIndexFsRrm11hDfsInfoTable (&i4NextIndex,
                                                  &NextStationMacAddr) !=
            SNMP_SUCCESS)
        {
            IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return;
        }
        do
        {
            pHttp->i4Write = (INT4) u4Temp;
            i4Index = i4NextIndex;
            MEMCPY (&StationMacAddr, &NextStationMacAddr, sizeof (tMacAddr));
            if (nmhGetFsDot11RadioType (i4Index, (UINT4 *) &i4Type) ==
                SNMP_FAILURE)
            {
                continue;
            }

            if ((i4Type == RFMGMT_RADIO_TYPEA)
                || (i4Type == RFMGMT_RADIO_TYPEAN))
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();
                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4Index;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                    != OSIX_SUCCESS)
                {
                    IssSendError (pHttp,
                                  (CONST INT1 *) ("DB Access failed\r\n"));
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                nmhGetFsRrm11hChannelNumber (i4Index, StationMacAddr,
                                             &u1ChannelNum);
                nmhGetFsRrm11hMeasureMapRadar (i4Index, StationMacAddr,
                                               &i4MeasureMapRadar);
                nmhGetFsRrm11hDfsLastRun (i4RadioType, &u4ReportLastReceived);

                CliMacToStr (StationMacAddr, au1String);

                STRCPY (pHttp->au1KeyString, "AP_NAME_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "RADIO_ID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         RadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "STATION_MAC_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "CH_NUM_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1ChannelNum);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "MEAS_RADAR_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u1MeasureMapRadar);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "REPORT_RECVD_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         u4ReportLastReceived);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                STRCPY (pHttp->au1KeyString, "MEAS_BSS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Temp);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "MEAS_OFDM_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Temp);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "MEAS_UNMEASURED_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Temp);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "MEAS_UNIDENTIFIED_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1Temp);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) HTTP_STRLEN (pHttp->au1DataString));
                STRCPY (pHttp->au1KeyString, "<! PARAM STOP>");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }
        }
        while (nmhGetNextIndexFsRrm11hDfsInfoTable (i4Index, &i4NextIndex,
                                                    StationMacAddr,
                                                    &NextStationMacAddr)
               == SNMP_SUCCESS);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    gi4ApOption = RFMGMT_AP_ALL;
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return;
}

/*********************************************************************
 * *  Function Name : IssRfmDfsInfoPageSet
 * *  Description   : This function processes the set request coming for the
 * *                  RF Radio_AH DFS page.
 * *
 * *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 * *  Output(s)     : None.
 * *  Return Values : None
 **********************************************************************/
VOID
IssRfmDfsInfoPageSet (tHttp * pHttp)
{
    UINT1               au1ApName[OCTETSTR_SIZE + 1];
    UINT4               u4ApOption = 0;

    MEMSET (&au1ApName, 0, OCTETSTR_SIZE + 1);

    STRCPY (pHttp->au1Name, "SelectAp");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4ApOption = (UINT4) ATOI ((INT1 *) pHttp->au1Value);
    gi4ApOption = (INT4) u4ApOption;

    STRCPY (pHttp->au1Name, "AP_NAME");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    STRNCPY (au1ApName, pHttp->au1Value, OCTETSTR_SIZE);

    STRNCPY (gau1ApName, au1ApName, MEM_MAX_BYTES (STRLEN (au1ApName),
                                                   OCTETSTR_SIZE));
    gau1ApName[MEM_MAX_BYTES (STRLEN (au1ApName), OCTETSTR_SIZE)] = '\0';

    IssRfmDfsInfoPageGet (pHttp);

    return;
}

/****************************************************************************
 * Function    :  RfMgmtGetRadioIfIndex
 * Description :  This function is utility function to give the radio index
 *        for the and WTP Profile Id, WTP Binding ID. This function
 *       also checks the matching of the radio type
 *
 * Input       :  u4WtpProfileId - WTP  Profile ID
 *                u4WtpBindingId - WTP  Binding ID
 *                i4RadioType    - Radio Type
 *
 * Output      :  pi4RadioIfIndex -  Radio Index
 *
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
RfMgmtGetRadioIfIndex (UINT4 u4WtpProfileId, UINT4 u4WtpBindingId,
                       INT4 i4RadioType, INT4 *pi4RadioIfIndex)
{
    INT4                i4RadioIfIndex = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;
    UINT4               u4GetRadioType = 0;
    INT1                i1RetStatus = 0;

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex (u4WtpProfileId,
                                                            u4WtpBindingId,
                                                            &i4RadioIfIndex) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    *pi4RadioIfIndex = i4RadioIfIndex;

    if ((i1RetStatus = nmhGetFsDot11RadioType (i4RadioIfIndex,
                                               &u4GetRadioType)) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (RFMGMT_RADIO_TYPE_COMP ((UINT4) i4RadioType))
    {
        return OSIX_FAILURE;
    }

    return i4RetStatus;

}
#endif
