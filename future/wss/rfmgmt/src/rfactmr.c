
/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfactmr.c,v 1.2 2017/05/23 14:16:52 siva Exp $
 *
 * Description: This file contains the RFMGMT Timer related routines.
 *                            
 ********************************************************************/

#ifndef __RFACTMR_C__
#define __RFACTMR_C__

#include "rfminc.h"
#ifdef ROGUEAP_WANTED
#include "wssstawlcinc.h"
#endif
static tRfMgmtTimerList gRfMgmtTmrList;

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtWlcTmrInit                                           *
 *                                                                           *
 * Description  : This function creates a timer list for all the timers      *
 *                          in RFMGMT module.                                *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
RfMgmtWlcTmrInit (VOID)
{
    RFMGMT_FN_ENTRY ();
    /* Timer List for State Machine Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) RFMGMT_WLC_TASK_NAME,
                            RFMGMT_TIMER_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gRfMgmtTmrList.
                                                RfMgmtTmrListId)) ==
        TMR_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfmgmtTmrInit: Failed to "
                    "create the Application Timer List \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfmgmtTmrInit: Failed to "
                      "create the Application Timer List "));
        return RFMGMT_FAILURE;
    }

    RfMgmtWlcTmrInitTmrDesc ();

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : RfMgmtWlcTmrDeInit
 *
 * Description  : This function deletes the timer list for the timers
 *                in RFMGMT module.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
INT4
RfMgmtWlcTmrDeInit (VOID)
{
    RFMGMT_FN_ENTRY ();

    if (TmrDeleteTimerList (gRfMgmtTmrList.RfMgmtTmrListId) == TMR_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtTmrInit: Failed to delete the RFMGMT " "Timer List");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtTmrInit: Failed to delete the RFMGMT "
                      "Timer List"));
        return RFMGMT_FAILURE;
    }

    MEMSET (gRfMgmtTmrList.aRfMgmtTmrDesc, 0,
            (sizeof (tTmrDesc) * RFMGMT_MAX_TMR));

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtWlcTmrInitTmrDesc                                    *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the timers in RFMGMT module.                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtWlcTmrInitTmrDesc (VOID)
{
    RFMGMT_FN_ENTRY ();

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_DCA_INTERVAL_TMR].TmrExpFn
        = RfMgmtDcaIntervalTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_DCA_INTERVAL_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_TPC_INTERVAL_TMR].TmrExpFn
        = RfMgmtTpcIntervalTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_TPC_INTERVAL_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_SHA_INTERVAL_TMR].TmrExpFn
        = RfMgmtShaIntervalTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_SHA_INTERVAL_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_11H_TPC_INTERVAL_TMR].TmrExpFn
        = RfMgmt11hTpcIntervalTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_11H_TPC_INTERVAL_TMR].i2Offset = -1;

    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_11H_DFS_INTERVAL_TMR].TmrExpFn
        = RfMgmt11hDfsIntervalTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_11H_DFS_INTERVAL_TMR].i2Offset = -1;
#ifdef ROGUEAP_WANTED
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR].
        TmrExpFn = RfMgmtDeleteExpiredRogueApIntervalTmrExp;
    gRfMgmtTmrList.aRfMgmtTmrDesc[RFMGMT_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR].
        i2Offset = -1;
#endif
    RFMGMT_FN_EXIT ();
}

/*****************************************************************************
 * Function                  : RfMgmt11hDfsIntervalTmrExp                    *
 *                                                                           *
 * Description               : This routine handles the DFS algorithm timer  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pArg - pointer to Auto RF Table               *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 **************************************************************************/
VOID
RfMgmt11hDfsIntervalTmrExp (VOID *pArg)
{

    tRfMgmtDB           RfMgmtDB;
    UINT4               u4CurrentTime = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pArg);
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RFMGMT_FN_ENTRY ();

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsLastRun = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bDfsInterval = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmt11hDfsIntervalTmrExp: "
                    "Failed to get 11h DFS status\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmt11hDfsIntervalTmrExp: "
                      "Failed to get 11h DFS status\n"));
        /* Start the timer */
        RfMgmtWlcTmrStart (0, RFMGMT_11H_DFS_INTERVAL_TMR,
                           RFMGMT_DEF_11H_DFS_TIMER_INTERVAL);
        return;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hDfsStatus == RFMGMT_DFS_ENABLE)
    {
        /* Get the current time */
        u4CurrentTime = OsixGetSysUpTime ();
        u4CurrentTime = (u4CurrentTime - RfMgmtDB.unRfMgmtDB.
                         RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                         u4RfMgmt11hDfsLastRun);

        if (u4CurrentTime >= RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u4DfsInterval)
        {
            /* i4Status = RfMgmtRunDFSAlgorithm(); */

            if (i4Status == RFMGMT_FAILURE)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmt11hDfsIntervalTmrExp: "
                            "Failed to run the 11h DFS algorithm\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmt11hDfsIntervalTmrExp: "
                              "Failed to run the 11h DFS algorithm\n"));
            }
            else
            {

                MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                    u4Dot11RadioType = RFMGMT_RADIO_TYPEA;

                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                    u4RfMgmt11hDfsLastRun = OsixGetSysUpTime ();

                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                    bRfMgmt11hDfsLastRun = OSIX_TRUE;
                if (RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY,
                                        &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmt11hDfsIntervalTmrExp: "
                                "Failed to get 11h DFS status\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmt11hDfsIntervalTmrExp: "
                                  "Failed to get 11h DFS status\n"));
                }

            }
        }
    }
    /* Start the timer */
    RfMgmtWlcTmrStart (0, RFMGMT_11H_DFS_INTERVAL_TMR,
                       RFMGMT_DEF_11H_DFS_TIMER_INTERVAL);
    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmtDcaIntervalTmrExp                       *
 *                                                                           *
 * Description               : This routine handles the DCA algorithm timer  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pArg - pointer to Auto RF Table               *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtDcaIntervalTmrExp (VOID *pArg)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfConfigDB = NULL;
    UINT4               u4CurrentTime = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pArg);

    RFMGMT_FN_ENTRY ();

    pRfMgmtAutoRfProfileDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB);

    while (pRfMgmtAutoRfProfileDB != NULL)
    {
        pRfMgmtAutoRfConfigDB = pRfMgmtAutoRfProfileDB;

        if (((pRfMgmtAutoRfConfigDB->u1DcaMode == RFMGMT_DCA_MODE_GLOBAL)
             && (pRfMgmtAutoRfConfigDB->u1DcaSelection ==
                 RFMGMT_DCA_SELECTION_AUTO)) ||
            (pRfMgmtAutoRfConfigDB->u1DcaMode == RFMGMT_DCA_MODE_PER_AP))
        {
            /* Get the current time */
            u4CurrentTime = OsixGetSysUpTime ();
            u4CurrentTime = (u4CurrentTime -
                             pRfMgmtAutoRfConfigDB->u4DcaLastUpdatedTime);

            if (u4CurrentTime >= pRfMgmtAutoRfConfigDB->u4DcaInterval)
            {
                i4Status = RfMgmtRunDcaAlgorithm (pRfMgmtAutoRfConfigDB, 0);

                if (i4Status == RFMGMT_FAILURE)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtDcaIntervalTmrExp: "
                                "Failed to run the DCA algorithm\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtDcaIntervalTmrExp: "
                                  "Failed to run the DCA algorithm"));
                }
                else
                {
                    pRfMgmtAutoRfConfigDB->u4DcaLastUpdatedTime =
                        OsixGetSysUpTime ();
                }
            }
        }
        pRfMgmtAutoRfProfileDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                           (tRBElem *) pRfMgmtAutoRfConfigDB, NULL);
    }

    /* Start the timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_DCA_INTERVAL_TMR, RFMGMT_DEF_DCA_TIMER_INTERVAL);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmtShaIntervalTmrExp                       *
 *                                                                           *
 * Description               : This routine handles the DCA algorithm timer  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pArg - pointer to Auto RF Table               *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtShaIntervalTmrExp (VOID *pArg)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfConfigDB = NULL;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4CurrentTime = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pArg);
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RFMGMT_FN_ENTRY ();

    pRfMgmtAutoRfProfileDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB);

    while (pRfMgmtAutoRfProfileDB != NULL)
    {
        pRfMgmtAutoRfConfigDB = pRfMgmtAutoRfProfileDB;

        if (pRfMgmtAutoRfConfigDB->u1SHAStatus == RFMGMT_SHA_ENABLE)
        {
            /* Get the current time */
            u4CurrentTime = OsixGetSysUpTime ();
            u4CurrentTime = (u4CurrentTime -
                             pRfMgmtAutoRfConfigDB->u4ShaLastUpdatedTime);

            if (u4CurrentTime >= pRfMgmtAutoRfConfigDB->u4SHAInterval)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                    u4Dot11RadioType = pRfMgmtAutoRfConfigDB->u4Dot11RadioType;
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                    bSHAExecutionCount = OSIX_TRUE;

                if (RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY,
                                        &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtShaIntervalTmrExp: "
                                "Failed to set SHA execution count\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtShaIntervalTmrExp: "
                                  "Failed to set SHA execution count\n"));
                }

                i4Status = RfMgmtRunShaAlgorithm (pRfMgmtAutoRfConfigDB);

                if (i4Status == RFMGMT_FAILURE)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtShaIntervalTmrExp: "
                                "Failed to run the SHA algorithm\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtShaIntervalTmrExp: "
                                  "Failed to run the SHA algorithm"));
                }
                else
                {
                    pRfMgmtAutoRfConfigDB->u4ShaLastUpdatedTime =
                        OsixGetSysUpTime ();
                }
            }
        }
        pRfMgmtAutoRfProfileDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                           (tRBElem *) pRfMgmtAutoRfConfigDB, NULL);
    }

    /* Start the timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_SHA_INTERVAL_TMR, RFMGMT_DEF_SHA_TIMER_INTERVAL);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmtTpcIntervalTmrExp                       *
 *                                                                           *
 * Description               : This routine handles the DCA algorithm timer  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pArg - pointer to Auto RF Table               *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtTpcIntervalTmrExp (VOID *pArg)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfConfigDB = NULL;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4CurrentTime = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pArg);
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RFMGMT_FN_ENTRY ();

    pRfMgmtAutoRfProfileDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB);

    while (pRfMgmtAutoRfProfileDB != NULL)
    {
        pRfMgmtAutoRfConfigDB = pRfMgmtAutoRfProfileDB;

        if (((pRfMgmtAutoRfConfigDB->u1TpcMode == RFMGMT_TPC_MODE_GLOBAL)
             && (pRfMgmtAutoRfConfigDB->u1TpcSelection ==
                 RFMGMT_TPC_SELECTION_AUTO)) ||
            (pRfMgmtAutoRfConfigDB->u1TpcMode == RFMGMT_TPC_MODE_PER_AP))
        {
            /* Get the current time */
            u4CurrentTime = OsixGetSysUpTime ();
            u4CurrentTime = (u4CurrentTime -
                             pRfMgmtAutoRfConfigDB->u4TpcLastUpdatedTime);

            if (u4CurrentTime >= pRfMgmtAutoRfConfigDB->u4TpcInterval)
            {
                i4Status = RfMgmtRunTpcAlgorithm (pRfMgmtAutoRfConfigDB);

                if (i4Status == RFMGMT_FAILURE)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtTpcIntervalTmrExp: "
                                "Failed to run the TPC algorithm\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtTpcIntervalTmrExp: "
                                  "Failed to run the TPC algorithm"));
                }
                else
                {
                    pRfMgmtAutoRfConfigDB->u4TpcLastUpdatedTime =
                        OsixGetSysUpTime ();
                }

                if ((pRfMgmtAutoRfConfigDB->u1TpcMode ==
                     RFMGMT_TPC_MODE_GLOBAL) &&
                    (pRfMgmtAutoRfConfigDB->u1TpcSelection ==
                     RFMGMT_TPC_SELECTION_AUTO))
                {
                    i4Status = RfMgmtRunDpaAlgorithm (pRfMgmtAutoRfConfigDB);
                    if (i4Status == RFMGMT_FAILURE)
                    {
                        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                    "RfMgmtDpaIntervalTmrExp: "
                                    "Failed to run the DPC algorithm\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtDpaIntervalTmrExp: "
                                      "Failed to run the DPC algorithm"));
                    }
                    else
                    {
                        pRfMgmtAutoRfConfigDB->u4TpcLastUpdatedTime =
                            OsixGetSysUpTime ();
                    }

                    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                        RfMgmtAutoRfProfileDB.u4Dot11RadioType
                        = pRfMgmtAutoRfConfigDB->u4Dot11RadioType;
                    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                        RfMgmtAutoRfIsSetDB.bDPAExecutionCount = OSIX_TRUE;
                    if (RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY,
                                            &RfMgmtDB) != RFMGMT_SUCCESS)
                    {
                        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                    "RfMgmtTpcIntervalTmrExp: "
                                    "Failed to set DPA execution count\r\n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                      "RfMgmtTpcIntervalTmrExp: "
                                      "Failed to set DPA execution count\n"));
                    }
                }
            }
        }
        pRfMgmtAutoRfProfileDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                           (tRBElem *) pRfMgmtAutoRfConfigDB, NULL);
    }

    /* Start the timer */
    RfMgmtWlcTmrStart (0,
                       RFMGMT_TPC_INTERVAL_TMR, RFMGMT_DEF_TPC_TIMER_INTERVAL);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 * Function                  : RfMgmt11hTpcIntervalTmrExp                    *
 *                                                                           *
 * Description               : This routine handles the DCA algorithm timer  *
 *                             Expiry.                                       *
 *                                                                           *
 * Input                     : pArg - pointer to Auto RF Table               *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmt11hTpcIntervalTmrExp (VOID *pArg)
{
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4CurrentTime = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pArg);
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RFMGMT_FN_ENTRY ();

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = RFMGMT_RADIO_TYPEA;

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcLastRun = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcInterval = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY,
                            &RfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmt11hTpcIntervalTmrExp: "
                    "Failed to get 11h TPC status\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmt11hTpcIntervalTmrExp: "
                      "Failed to get 11h TPC status\n"));
        /* Start the timer */
        RfMgmtWlcTmrStart (0, RFMGMT_11H_TPC_INTERVAL_TMR,
                           RFMGMT_DEF_11H_TPC_TIMER_INTERVAL);
        return;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hTpcStatus == RFMGMT_11H_TPC_ENABLE)
    {
        /* Get the current time */
        u4CurrentTime = OsixGetSysUpTime ();
        u4CurrentTime = (u4CurrentTime - RfMgmtDB.unRfMgmtDB.
                         RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                         u4RfMgmt11hTpcLastRun);

        if (u4CurrentTime >= RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u4RfMgmt11hTpcInterval)
        {
            i4Status = RfMgmtRun11hTpcAlgorithm ();
            if (i4Status == RFMGMT_FAILURE)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmt11hTpcIntervalTmrExp: "
                            "Failed to run the 11h TPC algorithm\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmt11hTpcIntervalTmrExp: "
                              "Failed to run the 11h TPC algorithm"));
            }
            else
            {

                MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                    u4Dot11RadioType = RFMGMT_RADIO_TYPEA;

                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                    u4RfMgmt11hTpcLastRun = OsixGetSysUpTime ();

                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                    bRfMgmt11hTpcLastRun = OSIX_TRUE;
                if (RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY,
                                        &RfMgmtDB) != RFMGMT_SUCCESS)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmt11hTpcIntervalTmrExp: "
                                "Failed to get 11h TPC status\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmt11hTpcIntervalTmrExp: "
                                  "Failed to get 11h TPC status\n"));
                }

            }
        }
    }
    /* Start the timer */
    RfMgmtWlcTmrStart (0, RFMGMT_11H_TPC_INTERVAL_TMR,
                       RFMGMT_DEF_11H_TPC_TIMER_INTERVAL);

    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtDcaIntExpHandler                                       *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
RfMgmtDcaIntExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    RFMGMT_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gRfMgmtTmrList.RfMgmtTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        RFMGMT_TRC1 (RFMGMT_MGMT_TRC, "Timer to be processed %d\r\n",
                     u1TimerId);

        if (u1TimerId < RFMGMT_MAX_TMR)

        {
            i2Offset = gRfMgmtTmrList.aRfMgmtTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter. */
                (*(gRfMgmtTmrList.aRfMgmtTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gRfMgmtTmrList.aRfMgmtTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    RFMGMT_FN_EXIT ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : RfMgmtWlcTmrStart                                          *
 *                                                                           *
 * Description  : This function used to start the rfmgmt state machiune      *
 *                specified timers.                                          *
 *                                                                           *
 * Input        : pRfMgmtAutoRfProfileDB - Pointer to radio type DB          *
 *                u4TmrInterval - Time interval for which timer must run     *
 *               (in seconds)                                                *
 *                u1TmrType - Indicates which timer to start.                *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
RfMgmtWlcTmrStart (UINT4 u4Dot11RadioType, UINT1 u1TmrType, UINT4 u4TmrInterval)
{
    UNUSED_PARAM (u4Dot11RadioType);

    RFMGMT_FN_ENTRY ();
    switch (u1TmrType)
    {
        case RFMGMT_DCA_INTERVAL_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtDCATmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart:"
                              "Failed to start the Timer Type\n"));
                return RFMGMT_FAILURE;
            }
        }
            break;
        case RFMGMT_TPC_INTERVAL_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtTPCTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart:"
                              "Failed to start the Timer Type\n"));
                return RFMGMT_FAILURE;
            }
        }
            break;
        case RFMGMT_11H_TPC_INTERVAL_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmt11hTPCTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return RFMGMT_FAILURE;
            }
        }
            break;
        case RFMGMT_SHA_INTERVAL_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtSHATmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return RFMGMT_FAILURE;
            }
        }
            break;

        case RFMGMT_11H_DFS_INTERVAL_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmt11hDFSTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtTmrStart: "
                              "Failed to start the Timer Type\n"));
                return RFMGMT_FAILURE;
            }
        }
            break;
#ifdef ROGUEAP_WANTED
        case RFMGMT_DELETE_EXPIRED_ROUGE_AP_INTERVAL_TMR:
        {
            if (TmrStart (gRfMgmtTmrList.RfMgmtTmrListId,
                          &(gRfMgmtTmrList.RfMgmtDeleteExpiredRougeApTmrBlk),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                RFMGMT_TRC1 (RFMGMT_FAILURE_TRC, "RfMgmtTmrStart: "
                             "Failed to start the Timer Type = %d \r\n",
                             u1TmrType);
                return RFMGMT_FAILURE;
            }
        }
            break;
#endif
        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtTmrStart:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtTmrStart:Invalid Timer type FAILED !!!"));
            return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************
 * Function                  : RfMgmtWlcTmrStop                              *
 *                                                                           *
 * Description               : This routine stops the given discovery timer  *
 *                                                                           *
 * Input                     : u1TmrType - Indicates which timer to stop     *
 *                             pSessEntry - pointer to session entry table   *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Returns                   : None.                                         *
 *                                                                           *
 *****************************************************************************/
INT4
RfMgmtWlcTmrStop (UINT4 u4Dot11RadioType, UINT1 u1TmrType)
{
    INT4                i4RetVal = OSIX_FAILURE;

    UNUSED_PARAM (u4Dot11RadioType);

    RFMGMT_FN_ENTRY ();

    switch (u1TmrType)
    {
        case RFMGMT_DCA_INTERVAL_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtDCATmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfmgmtTmrStop: "
                            "Failure to stop DCA interval timer\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop DCA interval timer"));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_TPC_INTERVAL_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtTPCTmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfmgmtTmrStop: "
                            "Failure to stop TPC interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop TPC interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_11H_TPC_INTERVAL_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmt11hTPCTmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfmgmtTmrStop: "
                            "Failure to stop SHA interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop SHA interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_SHA_INTERVAL_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmtSHATmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfmgmtTmrStop: "
                            "Failure to stop SHA interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop SHA interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;
        case RFMGMT_11H_DFS_INTERVAL_TMR:
            if (TmrStop (gRfMgmtTmrList.RfMgmtTmrListId,
                         &(gRfMgmtTmrList.RfMgmt11hDFSTmrBlk)) != TMR_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfmgmtTmrStop: "
                            "Failure to stop DFS interval timer \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfmgmtTmrStop: "
                              "Failure to stop DFS interval timer "));
            }
            i4RetVal = OSIX_SUCCESS;

            break;

        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtTmrStop:Invalid Timer type FAILED !!!\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmtTmrStop:Invalid Timer type FAILED !!!"));
            return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return i4RetVal;
}

#endif
