/* $Id: rfmgmtacsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ */
#define _RFMGMTACSZ_C
#include "rfminc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  RfmgmtacSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < RFMGMTAC_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4) MemCreateMemPool( 
                          FsRFMGMTACSizingParams[i4SizingId].u4StructSize,
                          FsRFMGMTACSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(RFMGMTACMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            RfmgmtacSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   RfmgmtacSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsRFMGMTACSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, RFMGMTACMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  RfmgmtacSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < RFMGMTAC_MAX_SIZING_ID; i4SizingId++) {
        if(RFMGMTACMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( RFMGMTACMemPoolIds[ i4SizingId] );
            RFMGMTACMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
