/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rfacdb.c,v 1.4 2017/11/24 10:37:05 siva Exp $
 *
 * Description: This file contains the DB realated information of the
 *              rfmgmt module 
 *                            
 ********************************************************************/
#ifndef __RFACDB_C__
#define __RFACDB_C__

#include "rfminc.h"
#include "radioifextn.h"
#include "wsscfginc.h"
extern UINT2        gau2NoExternalAp;
extern UINT2        gau2ExternalApColors[MAX_RFMGMT_EXTERNAL_AP_SIZE];
/*****************************************************************************/
/* Function     :  RfMgmtExternalAPDBCreate                                  */
/*                                                                           */
/* Description  : This function creates the  RfMgmtExgternalAPDBCreate       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtExternalAPDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtExternalAPDB, ExternalAPDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtExternalAPDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RfMgmtExternalAPDBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    " RfMgmtExternalAPDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      " RfMgmtExternalAPDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtAutoRfProfileDBCreate                                */
/*                                                                           */
/* Description  : This function creates the RfMgmtAutoRfProfileDB            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtAutoRfProfileDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtAutoRfProfileDB, RfMgmtDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtAutoRfProfileDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtAutoRfProfileDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtAutoRfProfileDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtAPConfigDBCreate                                     */
/*                                                                           */
/* Description  : This function creates the RfMgmtAPConfigDB                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtAPConfigDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtAPConfigDB, ApConfigDBNode);

    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RfMgmtAPConfigDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtAPConfigDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtAPConfigDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtNeighborScanDBCreate                                 */
/*                                                                           */
/* Description  : This function creates the RfMgmtNeighborScanDB             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtNeighborScanDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtNeighborScanDB, NeighborScanDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtNeighborScanDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtNeighborScanDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtNeighborScanDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtFailedAPNeighborListDBCreate                         */
/*                                                                           */
/* Description  : This function creates the RfMgmtNeighborScanDB             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtFailedAPNeighborListDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtFailedAPNeighborListDB,
                                    FailedAPNeighborListDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtFailedAPNeighborDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtFailedAPNeighborListDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtFailedAPNeighborListDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtClientConfigDBCreate                                 */
/*                                                                           */
/* Description  : This function creates the RfMgmtClientConfigDB             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtClientConfigDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtClientConfigDB, ClientConfigDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtClientConfigDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtClientConfigDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtClientConfigDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtClientScanDBCreate                                   */
/*                                                                           */
/* Description  : This function creates the RfMgmtClientScanDB               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtClientScanDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtClientScanDB, ClientScanDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtClientScanDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtClientScanDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtClientScanDBCreate failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtBssidScanDBCreate                                    */
/*                                                                           */
/* Description  : This function creates the RfMgmtBssidScanDBCreate          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
RfMgmtBssidScanDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtBssidScanDB, BssidScanDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RfMgmtBssidScanDBRBCmp)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtNeighborScanDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtNeighborScanDBCreate failed "));
        return OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDot11hTpcInfoDBCreate                                */
/*                                                                           */
/* Description  : This function creates the RfMgmtDot11hTpcInfoTableCreate   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
RfMgmtDot11hTpcInfoDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtDot11hTpcInfoDB,
                                    RfMgmt11hTpcInfoDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RfMgmtDot11hTpcInfoDBRBCmp))
        == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDot11hTpcInfoDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtDot11hTpcInfoDBCreate failed "));
        return OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDot11hDfsInfoDBCreate                                */
/*                                                                           */
/* Description  : This function creates the RfMgmtDot11hDfsInfoTableCreate   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
RfMgmtDot11hDfsInfoDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtDot11hDfsInfoDB,
                                    RfMgmt11hDfsInfoDBNode);
    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RfMgmtDot11hDfsInfoDBRBCmp))
        == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDot11hDfsInfoDBCreate failed \r\n");
        return OSIX_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RfMgmtAutoRfProfileDBRBCmp                           */
/*                                                                           */
/* Description        : This function compares the two Radio Type            */
/*                       RB Trees.                                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtAutoRfProfileDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtAutoRfProfileDB *pNode1 = e1;
    tRfMgmtAutoRfProfileDB *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4Dot11RadioType > pNode2->u4Dot11RadioType)
    {
        return 1;
    }
    else if (pNode1->u4Dot11RadioType < pNode2->u4Dot11RadioType)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtNeighborScanDBRBCmp                            */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtNeighborScanDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtNeighborScanDB *pNode1 = e1;
    tRfMgmtNeighborScanDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->NeighborAPMac, pNode2->NeighborAPMac,
                       RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtFailedAPNeighborDBRBCmp                        */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtFailedAPNeighborDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtFailedAPNeighborListDB *pNode1 = e1;
    tRfMgmtFailedAPNeighborListDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->NeighborAPMac, pNode2->NeighborAPMac,
                       RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtBssidScanDBRBCmp                               */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtBssidScanDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtBssidScanDB *pNode1 = e1;
    tRfMgmtBssidScanDB *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    if (pNode1->u1WlanID > pNode2->u1WlanID)
    {
        return 1;
    }
    else if (pNode1->u1WlanID < pNode2->u1WlanID)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtDot11hTpcInfoDBRBCmp                           */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtDot11hTpcInfoDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtDot11hTpcInfoDB *pNode1 = e1;
    tRfMgmtDot11hTpcInfoDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RfMgmt11hRadioIfIndex > pNode2->u4RfMgmt11hRadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RfMgmt11hRadioIfIndex < pNode2->u4RfMgmt11hRadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->RfMgmt11hStationMacAddress,
                       pNode2->RfMgmt11hStationMacAddress, RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtDot11hDfsInfoDBRBCmp                           */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtDot11hDfsInfoDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtDot11hDfsInfoDB *pNode1 = e1;
    tRfMgmtDot11hDfsInfoDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RfMgmt11hRadioIfIndex > pNode2->u4RfMgmt11hRadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RfMgmt11hRadioIfIndex < pNode2->u4RfMgmt11hRadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->RfMgmt11hStationMacAddress,
                       pNode2->RfMgmt11hStationMacAddress, RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtExternalAPDBCmp                                */
/*                                                                           */
/* Description        : This function compares the two External AP nodes     */
/*                       RB Trees.                                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtExternalAPDBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtExternalAPDB *pNode1 = e1;
    tRfMgmtExternalAPDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();
    i4RetVal = MEMCMP (pNode1->APMac, pNode2->APMac, RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtAPConfigDBRBCmp                                */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtAPConfigDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtAPConfigDB  *pNode1 = e1;
    tRfMgmtAPConfigDB  *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtClientConfigDBRBCmp                            */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtClientConfigDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtClientConfigDB *pNode1 = e1;
    tRfMgmtClientConfigDB *pNode2 = e2;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : RfMgmtClientScanDBRBCmp                              */
/*                                                                           */
/* Description        : This function compares the two Radio Interface Index */
/*                       RB Trees                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
RfMgmtClientScanDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtClientScanDB *pNode1 = e1;
    tRfMgmtClientScanDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    RFMGMT_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    i4RetVal = MEMCMP (pNode1->ClientMacAddress, pNode2->ClientMacAddress,
                       RFMGMT_MAC_SIZE);
    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    RFMGMT_FN_EXIT ();
    return 0;
}

/****************************************************************************
 * *                                                                        *
 * * Function     : WssIfProcessRfMgmtDBMsg                                 *
 * *                                                                        *
 * * Description  : Calls RF MGMT module DB to process fn calls from other  *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : eMsgType - Msg opcode                                   *
 * *                pRfMgmtDB - Pointer to RF MGMT DB struct                * 
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : OSIX_SUCCESS on success                                 *
 * *                OSIX_FAILURE otherwise                                  *
 * *                                                                        *
 * **************************************************************************/

UINT1
WssIfProcessRfMgmtDBMsg (UINT1 u1OpCode, tRfMgmtDB * pRfMgmtDB)
{
    RFMGMT_LOCK;
    if (RfMgmtProcessDBMsg (u1OpCode, pRfMgmtDB) != RFMGMT_SUCCESS)
    {
        RFMGMT_UNLOCK;
        return RFMGMT_FAILURE;
    }

    RFMGMT_UNLOCK;
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtProcessDBMsg                                         */
/*                                                                           */
/* Description  : Used to invoke the coreesponding DB to perform set or get  */
/*                operation.                                                 */
/* Input        : Opcode and Pointer to tRfMgmtDB                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtProcessDBMsg (UINT1 u1Opcode, tRfMgmtDB * pRfMgmtDB)
{
    tRfMgmtAutoRfProfileDB RfMgmtAutoRfProfileDB;
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;
    tRfMgmtExternalAPDB *pRfMgmtExternalAPDB = NULL;
    tRfMgmtAPConfigDB   RfMgmtAPConfigDB;
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtNeighborScanDB RfMgmtNeighborScanDB;
    tRfMgmtNeighborScanDB RfMgmtNextNeighborScanDB;
    tRfMgmtNeighborScanDB *pRfMgmtNeighborScanDB = NULL;
    tRfMgmtNeighborScanDB *pRfMgmtNextNeighborScanDB = NULL;
    tRfMgmtFailedAPNeighborListDB *pRfMgmtFailedAPNeighborListDB = NULL;
    tRfMgmtClientConfigDB RfMgmtClientConfigDB;
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;
    tRfMgmtClientScanDB RfMgmtClientScanDB;
    tRfMgmtClientScanDB *pRfMgmtClientScanDB = NULL;
    tRfMgmtClientScanDB *pRfMgmtNextClientScanDB = NULL;
    tRfMgmtBssidScanDB  RfMgmtBssidScanDB;
    tRfMgmtBssidScanDB *pRfMgmtBssidScanDB = NULL;
    tRfMgmtDot11hTpcInfoDB RfMgmtDot11hTpcInfoDB;
    tRfMgmtDot11hTpcInfoDB *pRfMgmtDot11hTpcInfoDB = NULL;
    tRfMgmtDot11hDfsInfoDB RfMgmtDot11hDfsInfoDB;
    tRfMgmtDot11hDfsInfoDB *pRfMgmtDot11hDfsInfoDB = NULL;
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfDB;
    UINT1               u1Channel = 0;
    UINT1               u1Count = 0;
    UINT1               au1String[RFMGMT_MAC_STR_LEN];
    UINT1               au1FsDot11CountryString[RADIO_COUNTRY_LENGTH];
    UINT1               au1AllowedChannels[RFMGMT_MAX_CHANNELB]
        = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0 };
    UINT1               au1Dot11aAllowedChannels[RADIOIF_MAX_CHANNEL_A] =
        { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
0, 0, 0, 0, 0 };
    UINT4               u4ChannelToFill = 0;
    UINT4               u4MaxChannels = 0;
    UINT4               u4Index = 0;
    UINT4               u4CurrentTime = 0;
    UINT2               u2WtpInternalId = 0;
    INT2                i2CurrentTxPower = 0;
    CHR1                au1CurrentTime[RFMGMT_MAX_DATE_LEN];
    tOsixSysTime        u4sysTime = 0;
    tMacAddr            testMac = { 0, 0, 0, 0, 0, 0 };
#ifdef ROGUEAP_WANTED
    tRfMgmtRogueApDB    RfMgmtRogueApDB;
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB = NULL;
    tRfMgmtRogueApDB   *pRfMgmtRogueApDBget = NULL;
    tRfMgmtRogueRuleDB  RfMgmtRogueRuleDB;
    tRfMgmtRogueRuleDB *pRfMgmtRogueRuleDB = NULL;
    tRfMgmtRogueRuleDB *pRfMgmtNextRogueRuleDB = NULL;
    tRfMgmtRogueManualDB RfMgmtRogueManualDB;
    tRfMgmtRogueManualDB *pRfMgmtRogueManualDB = NULL;
    tRfMgmtTrapInfo     RfMgmtTrapInfo;
    tMacAddr            BcastBssId = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    tMacAddr            BssIdZero = { 0, 0, 0, 0, 0, 0 };
#endif

    MEMSET (&RfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));
    MEMSET (&RfMgmtAPConfigDB, 0, sizeof (tRfMgmtAPConfigDB));
    MEMSET (&RfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RfMgmtNextNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RfMgmtClientConfigDB, 0, sizeof (tRfMgmtClientConfigDB));
    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    MEMSET (&RfMgmtBssidScanDB, 0, sizeof (tRfMgmtBssidScanDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
#ifdef ROGUEAP_WANTED
    MEMSET (&RfMgmtRogueApDB, 0, sizeof (tRfMgmtRogueApDB));
    MEMSET (&RfMgmtRogueRuleDB, 0, sizeof (tRfMgmtRogueRuleDB));
    MEMSET (&RfMgmtRogueManualDB, 0, sizeof (tRfMgmtRogueManualDB));
    MEMSET (&RfMgmtTrapInfo, 0, sizeof (tRfMgmtTrapInfo));
#endif

    RFMGMT_FN_ENTRY ();

    switch (u1Opcode)
    {
        case RFMGMT_CREATE_RADIO_TYPE_ENTRY:
            /*Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtAutoRfProfileDB =
                 (tRfMgmtAutoRfProfileDB *) MemAllocMemBlk
                 (RFMGMT_RADIO_TYPE_INDEX_POOLID)) == NULL)
            {

                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAutoRfProfileDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Memory alloc "
                              "failed for radio index"));

                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));

            pRfMgmtAutoRfProfileDB->u4Dot11RadioType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.u4Dot11RadioType;
            pRfMgmtAutoRfProfileDB->i4RowStatus =
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.i4RowStatus;

            /* Initializing default values for the radio type */
            pRfMgmtAutoRfProfileDB->u4DcaInterval = RFMGMT_DEF_DCA_INTERVAL;

            pRfMgmtAutoRfProfileDB->u4DcaIntervalUpdatedTime =
                OsixGetSysUpTime ();

            pRfMgmtAutoRfProfileDB->u4TpcInterval = RFMGMT_DEF_TPC_INTERVAL;

            pRfMgmtAutoRfProfileDB->u4SHAInterval = RFMGMT_DEF_SHA_INTERVAL;
            pRfMgmtAutoRfProfileDB->u1RfMgmt11hDfsStatus =
                RFMGMT_DEF_11H_DFS_STATUS;
            pRfMgmtAutoRfProfileDB->u4DfsInterval = RFMGMT_DEF_DFS_INTERVAL;

            pRfMgmtAutoRfProfileDB->u4TpcIntervalUpdatedTime =
                OsixGetSysUpTime ();

            pRfMgmtAutoRfProfileDB->u4DcaLastUpdatedTime =
                RFMGMT_DEF_DCA_UPDATE_TIME;

            pRfMgmtAutoRfProfileDB->u4TpcLastUpdatedTime =
                RFMGMT_DEF_TPC_UPDATE_TIME;

            pRfMgmtAutoRfProfileDB->u4ShaLastUpdatedTime =
                RFMGMT_DEF_SHA_UPDATE_TIME;

            pRfMgmtAutoRfProfileDB->i2RssiThreshold = RFMGMT_DEF_RSSI_THRESHOLD;

            pRfMgmtAutoRfProfileDB->i2PowerThreshold =
                RFMGMT_DEF_POWER_THRESHOLD;

            pRfMgmtAutoRfProfileDB->i2SNRThreshold = RFMGMT_DEF_SNR_THRESHOLD;

            pRfMgmtAutoRfProfileDB->u1DcaMode = RFMGMT_DEF_DCA_MODE;

            pRfMgmtAutoRfProfileDB->u1DcaSelection = RFMGMT_DEF_DCA_SELECTION;

            pRfMgmtAutoRfProfileDB->u1DcaSensitivity =
                RFMGMT_DEF_DCA_SENSITIVITY;

            pRfMgmtAutoRfProfileDB->u1DcaUpdateChannel =
                RFMGMT_DEF_DCA_UPDATE_CHANNEL;

            pRfMgmtAutoRfProfileDB->u1TpcMode = RFMGMT_DEF_TPC_MODE;

            pRfMgmtAutoRfProfileDB->u1SHAStatus = RFMGMT_DEF_SHA_STATUS;

            pRfMgmtAutoRfProfileDB->u1TpcSelection = RFMGMT_DEF_TPC_SELECTION;

            pRfMgmtAutoRfProfileDB->u1TpcUpdatePower = RFMGMT_DEF_TPC_UPDATE;

            pRfMgmtAutoRfProfileDB->u1ConsiderExternalAPs =
                RFMGMT_DEF_CONSIDER_EXT_NEIGHBORS;

            pRfMgmtAutoRfProfileDB->u1ClearNeighborDetails = OSIX_FALSE;

            pRfMgmtAutoRfProfileDB->u1NeighborCountThreshold =
                RFMGMT_DEF_NEIGHBOR_COUNT_THRESHOLD;

            if (pRfMgmtAutoRfProfileDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
            {
                pRfMgmtAutoRfProfileDB->u1RfMgmt11hTpcStatus = 0;
                pRfMgmtAutoRfProfileDB->u4RfMgmt11hTpcInterval =
                    RFMGMT_DEF_TPC_INTERVAL;
                pRfMgmtAutoRfProfileDB->u1MinLinkThreshold =
                    RFMGMT_DEF_MIN_LINK_THRESHOLD;
                pRfMgmtAutoRfProfileDB->u1MaxLinkThreshold =
                    RFMGMT_DEF_MAX_LINK_THRESHOLD;
                pRfMgmtAutoRfProfileDB->u1StaCountThreshold =
                    RFMGMT_DEF_STA_COUNT_THRESHOLD;

            }
            else
            {
                pRfMgmtAutoRfProfileDB->u1RfMgmt11hTpcStatus = 0;
                pRfMgmtAutoRfProfileDB->u4RfMgmt11hTpcInterval = 0;
                pRfMgmtAutoRfProfileDB->u1MinLinkThreshold = 0;
                pRfMgmtAutoRfProfileDB->u1MaxLinkThreshold = 0;
                pRfMgmtAutoRfProfileDB->u1StaCountThreshold = 0;
            }

            if (WsscfgGetFsDot11CountryString (au1FsDot11CountryString) !=
                OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Country String get failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Country String get failed\n"));
                return SNMP_FAILURE;
            }

            for (u4Index = 0; u4Index < RADIOIF_SUPPORTED_COUNTRY_LIST;
                 u4Index++)
            {
                if (STRCMP (au1FsDot11CountryString,
                            gWsscfgSupportedCountryList[u4Index].
                            au1CountryCode) == 0)
                {
                    u4Index =
                        (UINT4) gRegDomainCountryMapping[u4Index].u2RegDomain;
                    break;
                }
            }
            if (u4Index == RADIOIF_SUPPORTED_COUNTRY_LIST)
            {
                u4Index = 0;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
            {
                if (u4Index != RADIOIF_SUPPORTED_COUNTRY_LIST)
                {
                    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA;
                         u1Channel++)
                    {
                        if (au1Dot11aAllowedChannels[u1Channel] != 0)
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1AllowedChannelList[u1Channel] =
                                gau1Dot11aRegDomainChannelList[u1Channel];
                        }
                        else
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1UnUsedChannelList[u1Channel] =
                                gau1Dot11aRegDomainChannelList[u1Channel];
                        }
                    }
                }
            }
            else
            {
                if (u4Index != RADIOIF_SUPPORTED_COUNTRY_LIST)
                {
                    u1Count = RFMGMT_MAX_CHANNELB;
                    /*Since for country Japan channel 14 is not allowed in
                     * radiotype g*/
                    if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                        RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                        RFMGMT_RADIO_TYPEG)
                    {
                        u1Count = RFMGMT_MAX_CHANNELB - 1;
                        pRfMgmtAutoRfProfileDB->
                            au1UnUsedChannelList[RFMGMT_MAX_CHANNELB - 1]
                            = RFMGMT_MAX_CHANNELB;
                    }
                    for (u1Channel = 0; u1Channel < u1Count; u1Channel++)
                    {
                        if (au1AllowedChannels[u1Channel] != 0)
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1AllowedChannelList[u1Channel] =
                                gau1Dot11bRegDomainChannelList[u1Channel];
                        }
                        else
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1UnUsedChannelList[u1Channel] =
                                gau1Dot11bRegDomainChannelList[u1Channel];
                        }
                    }
                }

            }

            pRfMgmtAutoRfProfileDB->u1ClientThreshold =
                RFMGMT_DEF_CLIENT_THRESHOLD;
            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                 (tRBElem *) pRfMgmtAutoRfProfileDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_RADIO_TYPE_INDEX_POOLID,
                                    (UINT1 *) pRfMgmtAutoRfProfileDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Auto"
                            "RF DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Auto"
                              "RF DB"));
                return RFMGMT_FAILURE;
            }
            break;

        case RFMGMT_SET_AUTO_RF_ENTRY:
            MEMSET (&RfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.u4Dot11RadioType == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid Radio type received "
                            "for RFMGMT_SET_AUTO_RF_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid Radio type received "
                              "for RFMGMT_SET_AUTO_RF_ENTRY"));
                return RFMGMT_FAILURE;
            }

            if ((pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
                || (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEAN)
                || (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEAC))
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            }
            else if ((pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB) || (pRfMgmtDB->unRfMgmtDB.
                                              RfMgmtAutoRfTable.
                                              RfMgmtAutoRfProfileDB.
                                              u4Dot11RadioType ==
                                              RFMGMT_RADIO_TYPEBG)
                     || (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                         RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                         RFMGMT_RADIO_TYPEBGN))
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
            }
            else
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
            }

            pRfMgmtAutoRfProfileDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                           (tRBElem *) & RfMgmtAutoRfProfileDB);

            if (pRfMgmtAutoRfProfileDB == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAutoRfProfileDB: RBTreeGet failed "
                            "in RFMGMT_SET_AUTO_RF_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAutoRfProfileDB: RBTreeGet failed "
                              "in RFMGMT_SET_AUTO_RF_ENTRY"));
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRowStatus != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->i4RowStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i4RowStatus;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaLastUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4DcaLastUpdatedTime =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DcaLastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcLastUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4TpcLastUpdatedTime =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4TpcLastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bShaLastUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4ShaLastUpdatedTime =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4ShaLastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaInterval != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4DcaInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DcaInterval;

                pRfMgmtAutoRfProfileDB->u4DcaIntervalUpdatedTime =
                    OsixGetSysUpTime ();
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcInterval != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4TpcInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4TpcInterval;

                pRfMgmtAutoRfProfileDB->u4TpcIntervalUpdatedTime =
                    OsixGetSysUpTime ();
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hDfsStatus != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1RfMgmt11hDfsStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDfsInterval != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4DfsInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DfsInterval;

                pRfMgmtAutoRfProfileDB->u4DfsIntervalUpdatedTime =
                    OsixGetSysUpTime ();
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSHAInterval != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4SHAInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4SHAInterval;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSHAExecutionCount != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4SHAExecutionCount =
                    pRfMgmtAutoRfProfileDB->u4SHAExecutionCount + 1;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDPAExecutionCount != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4DPAExecutionCount =
                    pRfMgmtAutoRfProfileDB->u4DPAExecutionCount + 1;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRssiThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->i2RssiThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i2RssiThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bPowerThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->i2PowerThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i2PowerThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSNRThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->i2SNRThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i2SNRThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaMode != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1DcaMode =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bApNeighborCount != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1ApNeighborCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ApNeighborCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaSelection != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1DcaSelection =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaSelection;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaSensitivity != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1DcaSensitivity =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaSensitivity;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaUpdateChannel != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1DcaUpdateChannel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaUpdateChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcMode != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1TpcMode =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1TpcMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSHAStatus != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1SHAStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1SHAStatus;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcStatus != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1RfMgmt11hTpcStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcInterval != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4RfMgmt11hTpcInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4RfMgmt11hTpcInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcLastRun != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u4RfMgmt11hTpcLastRun =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4RfMgmt11hTpcLastRun;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcSelection != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1TpcSelection =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1TpcSelection;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcUpdatePower != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1TpcUpdatePower =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1TpcUpdatePower;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bClientSNRStatus != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1ClientSNRStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ClientSNRStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bClientThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1ClientThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ClientThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bMinLinkThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1MinLinkThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1MinLinkThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bMaxLinkThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1MaxLinkThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1MaxLinkThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bStaCountThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1StaCountThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1StaCountThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bConsiderExternalAPs != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1ConsiderExternalAPs =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ConsiderExternalAPs;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bClearNeighborDetails != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1ClearNeighborDetails =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ClearNeighborDetails;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bNeighborCountThreshold != OSIX_FALSE)
            {
                pRfMgmtAutoRfProfileDB->u1NeighborCountThreshold =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1NeighborCountThreshold;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bAllowedChannelList != OSIX_FALSE)
            {
                if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEA)
                {
                    MEMCPY (pRfMgmtAutoRfProfileDB->au1AllowedChannelList,
                            pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1AllowedChannelList,
                            RFMGMT_MAX_CHANNELA);

                    u4MaxChannels = RFMGMT_MAX_CHANNELA;
                    for (u1Channel = 0; u1Channel < u4MaxChannels; u1Channel++)
                    {
                        u4ChannelToFill = pRfMgmtDB->unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            au1AllowedChannelList[u1Channel];
                        if (u4ChannelToFill != 0)
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1UnUsedChannelList[u1Channel] = 0;
                        }
                        else
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1UnUsedChannelList[u1Channel]
                                = gau1Dot11aRegDomainChannelList[u1Channel];
                        }
                    }
                }
                else
                {
                    MEMCPY (pRfMgmtAutoRfProfileDB->au1AllowedChannelList,
                            pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1AllowedChannelList,
                            RFMGMT_MAX_CHANNELB);
                    u4MaxChannels = RFMGMT_MAX_CHANNELB;
                    for (u1Channel = 0; u1Channel < u4MaxChannels; u1Channel++)
                    {
                        u4ChannelToFill = pRfMgmtDB->unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            au1AllowedChannelList[u1Channel];
                        if (u4ChannelToFill != 0)
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1UnUsedChannelList[u1Channel] = 0;
                        }
                        else
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1UnUsedChannelList[u1Channel]
                                = (UINT1) (u1Channel + 1);
                        }
                    }
                }
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bUnUsedChannelList != OSIX_FALSE)
            {
                if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEA)
                {
                    MEMCPY (pRfMgmtAutoRfProfileDB->au1UnUsedChannelList,
                            pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1UnUsedChannelList,
                            RFMGMT_MAX_CHANNELA);

                    u4MaxChannels = RFMGMT_MAX_CHANNELA;
                    for (u1Channel = 0; u1Channel < u4MaxChannels; u1Channel++)
                    {
                        u4ChannelToFill = pRfMgmtDB->unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            au1UnUsedChannelList[u1Channel];
                        if (u4ChannelToFill != 0)
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1AllowedChannelList[u1Channel] = 0;
                        }
                        else
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1AllowedChannelList[u1Channel]
                                = gau1Dot11aRegDomainChannelList[u1Channel];
                        }
                    }
                }
                else
                {
                    MEMCPY (pRfMgmtAutoRfProfileDB->au1UnUsedChannelList,
                            pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1UnUsedChannelList,
                            RFMGMT_MAX_CHANNELB);
                    u4MaxChannels = RFMGMT_MAX_CHANNELB;
                    for (u1Channel = 0; u1Channel < u4MaxChannels; u1Channel++)
                    {
                        u4ChannelToFill = pRfMgmtDB->unRfMgmtDB.
                            RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                            au1UnUsedChannelList[u1Channel];
                        if (u4ChannelToFill != 0)
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1AllowedChannelList[u1Channel] = 0;
                        }
                        else
                        {
                            pRfMgmtAutoRfProfileDB->
                                au1AllowedChannelList[u1Channel]
                                = (UINT1) (u1Channel + 1);
                        }
                    }
                }
            }

            break;
        case RFMGMT_GET_AUTO_RF_ENTRY:
            MEMSET (&RfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));

            if ((pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEA) ||
                (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEAN) ||
                (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            }
            else if ((pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB) ||
                     (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBG) ||
                     (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBGN))
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
            }
            else
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
            }

            pRfMgmtAutoRfProfileDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                           (tRBElem *) & RfMgmtAutoRfProfileDB);

            if (pRfMgmtAutoRfProfileDB == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAutoRfProfileDB: RBTreeGet failed "
                            "in RFMGMT_GET_AUTO_RF_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAutoRfProfileDB: RBTreeGet failed "
                              "in RFMGMT_GET_AUTO_RF_ENTRY"));
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRowStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i4RowStatus =
                    pRfMgmtAutoRfProfileDB->i4RowStatus;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaLastUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DcaLastUpdatedTime =
                    pRfMgmtAutoRfProfileDB->u4DcaLastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcLastUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4TpcLastUpdatedTime =
                    pRfMgmtAutoRfProfileDB->u4TpcLastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bShaLastUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4ShaLastUpdatedTime =
                    pRfMgmtAutoRfProfileDB->u4ShaLastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaInterval != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DcaInterval =
                    pRfMgmtAutoRfProfileDB->u4DcaInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaIntervalUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DcaIntervalUpdatedTime =
                    pRfMgmtAutoRfProfileDB->u4DcaIntervalUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcInterval != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4TpcInterval =
                    pRfMgmtAutoRfProfileDB->u4TpcInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hDfsStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus =
                    pRfMgmtAutoRfProfileDB->u1RfMgmt11hDfsStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDfsInterval != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DfsInterval =
                    pRfMgmtAutoRfProfileDB->u4DfsInterval;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSHAInterval != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4SHAInterval =
                    pRfMgmtAutoRfProfileDB->u4SHAInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSHAExecutionCount != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4SHAExecutionCount =
                    pRfMgmtAutoRfProfileDB->u4SHAExecutionCount;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDPAExecutionCount != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4DPAExecutionCount =
                    pRfMgmtAutoRfProfileDB->u4DPAExecutionCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSHAStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1SHAStatus =
                    pRfMgmtAutoRfProfileDB->u1SHAStatus;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus =
                    pRfMgmtAutoRfProfileDB->u1RfMgmt11hTpcStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcInterval != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4RfMgmt11hTpcInterval =
                    pRfMgmtAutoRfProfileDB->u4RfMgmt11hTpcInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRfMgmt11hTpcLastRun != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4RfMgmt11hTpcLastRun =
                    pRfMgmtAutoRfProfileDB->u4RfMgmt11hTpcLastRun;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcIntervalUpdatedTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4TpcIntervalUpdatedTime =
                    pRfMgmtAutoRfProfileDB->u4TpcIntervalUpdatedTime;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bRssiThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i2RssiThreshold =
                    pRfMgmtAutoRfProfileDB->i2RssiThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bPowerThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i2PowerThreshold =
                    pRfMgmtAutoRfProfileDB->i2PowerThreshold;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bSNRThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.i2SNRThreshold =
                    pRfMgmtAutoRfProfileDB->i2SNRThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaMode != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaMode =
                    pRfMgmtAutoRfProfileDB->u1DcaMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bApNeighborCount != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ApNeighborCount =
                    pRfMgmtAutoRfProfileDB->u1ApNeighborCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaSelection != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaSelection =
                    pRfMgmtAutoRfProfileDB->u1DcaSelection;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaSensitivity != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaSensitivity =
                    pRfMgmtAutoRfProfileDB->u1DcaSensitivity;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bDcaUpdateChannel != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1DcaUpdateChannel =
                    pRfMgmtAutoRfProfileDB->u1DcaUpdateChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcMode != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1TpcMode =
                    pRfMgmtAutoRfProfileDB->u1TpcMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcSelection != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1TpcSelection =
                    pRfMgmtAutoRfProfileDB->u1TpcSelection;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bTpcUpdatePower != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1TpcUpdatePower =
                    pRfMgmtAutoRfProfileDB->u1TpcUpdatePower;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bClientSNRStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ClientSNRStatus =
                    pRfMgmtAutoRfProfileDB->u1ClientSNRStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bClientThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ClientThreshold =
                    pRfMgmtAutoRfProfileDB->u1ClientThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bMinLinkThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1MinLinkThreshold =
                    pRfMgmtAutoRfProfileDB->u1MinLinkThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bMaxLinkThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1MaxLinkThreshold =
                    pRfMgmtAutoRfProfileDB->u1MaxLinkThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bStaCountThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1StaCountThreshold =
                    pRfMgmtAutoRfProfileDB->u1StaCountThreshold;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bConsiderExternalAPs != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ConsiderExternalAPs =
                    pRfMgmtAutoRfProfileDB->u1ConsiderExternalAPs;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bClearNeighborDetails != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1ClearNeighborDetails =
                    pRfMgmtAutoRfProfileDB->u1ClearNeighborDetails;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bNeighborCountThreshold != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u1NeighborCountThreshold =
                    pRfMgmtAutoRfProfileDB->u1NeighborCountThreshold;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bAllowedChannelList != OSIX_FALSE)
            {
                if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEA)
                {
                    MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1AllowedChannelList,
                            pRfMgmtAutoRfProfileDB->au1AllowedChannelList,
                            RFMGMT_MAX_CHANNELA);
                }
                else
                {
                    MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1AllowedChannelList,
                            pRfMgmtAutoRfProfileDB->au1AllowedChannelList,
                            RFMGMT_MAX_CHANNELB);
                }
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
                bUnUsedChannelList != OSIX_FALSE)
            {
                if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                    RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEA)
                {
                    MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1UnUsedChannelList,
                            pRfMgmtAutoRfProfileDB->au1UnUsedChannelList,
                            RFMGMT_MAX_CHANNELA);
                }
                else
                {
                    MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                            RfMgmtAutoRfProfileDB.au1UnUsedChannelList,
                            pRfMgmtAutoRfProfileDB->au1UnUsedChannelList,
                            RFMGMT_MAX_CHANNELB);
                }
            }

            break;

        case RFMGMT_DELETE_RADIO_TYPE_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.u4Dot11RadioType == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAutoRfProfileDB: Entry is empty binding "
                            "deletion failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAutoRfProfileDB: Entry is empty binding "
                              "deletion failed"));
                return RFMGMT_FAILURE;
            }

            if ((pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEA) ||
                (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEAN) ||
                (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                 RfMgmtAutoRfProfileDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEA;
            }
            else if ((pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB) ||
                     (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBG) ||
                     (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                      RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEBGN))
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEB;
            }
            else
            {
                RfMgmtAutoRfProfileDB.u4Dot11RadioType = RFMGMT_RADIO_TYPEG;
            }

            pRfMgmtAutoRfProfileDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                           (tRBElem *) & RfMgmtAutoRfProfileDB);

            if (pRfMgmtAutoRfProfileDB == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAutoRfProfileDB: RBTreeGet failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAutoRfProfileDB: RBTreeGet failed"));
                return RFMGMT_FAILURE;
            }

            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                       pRfMgmtAutoRfProfileDB);
            MemReleaseMemBlock (RFMGMT_RADIO_TYPE_INDEX_POOLID,
                                (UINT1 *) pRfMgmtAutoRfProfileDB);
            /*pRfMgmtAutoRfProfileDB = NULL; */

            break;

        case RFMGMT_CREATE_NEIGHBOR_SCAN_ENTRY:
            /* Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtNeighborScanDB =
                 (tRfMgmtNeighborScanDB *) MemAllocMemBlk
                 (RFMGMT_NEIGH_SCAN_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtNeighborScanDB: "
                            "Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtNeighborScanDB: "
                              "Memory alloc failed for radio index\n"));
                return RFMGMT_FAILURE;
            }

            MEMSET (pRfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));

            /* Copy the received values to the DB */
            pRfMgmtNeighborScanDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex;

            pRfMgmtNeighborScanDB->i2Rssi =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.i2Rssi;

            pRfMgmtNeighborScanDB->u2ScannedChannel =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ScannedChannel;

            pRfMgmtNeighborScanDB->u2ExternalAPIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ExternalAPIndex;

            MEMCPY (pRfMgmtNeighborScanDB->NeighborAPMac,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.NeighborAPMac, MAC_ADDR_LEN);

            /* The below time will be used to delete the inactive entries,
             * incase of exceeding the threshold */
            pRfMgmtNeighborScanDB->u4LastUpdatedTime = OsixGetSysUpTime ();

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                 (tRBElem *) pRfMgmtNeighborScanDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                    (UINT1 *) pRfMgmtNeighborScanDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Neighbor "
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Neighbor "
                              "Scan DB"));
                return RFMGMT_FAILURE;
            }

            RfmgmtMacToStr (pRfMgmtNeighborScanDB->NeighborAPMac, au1String);

            RFMGMT_TRC2 (RFMGMT_DCA_TRC, "Add Mac %s in neighbor AP DB "
                         "for Radio index - %d\r\n", au1String,
                         pRfMgmtNeighborScanDB->u4RadioIfIndex);
            break;

        case RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtNeighborScanTable.RfMgmtNeighborScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }
            RfmgmtMacToStr (pRfMgmtNeighborScanDB->NeighborAPMac, au1String);

            RFMGMT_TRC2 (RFMGMT_DCA_TRC, "Remove Mac %s in neighbor AP DB "
                         "for Radio index - %d\r\n", au1String,
                         pRfMgmtNeighborScanDB->u4RadioIfIndex);
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       pRfMgmtNeighborScanDB);
            MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNeighborScanDB);
            /*pRfMgmtNeighborScanDB = NULL; */
            break;

        case RFMGMT_SET_NEIGHBOR_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtNeighborScanTable.RfMgmtNeighborScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            /* Compare the RSSI Value exist in DB and the received value. If the
             * value changes then set the value along with entry status as add
             * and update the last updated time */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bRssi != OSIX_FALSE)
            {
                if (pRfMgmtNeighborScanDB->i2Rssi !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.i2Rssi)
                {
                    pRfMgmtNeighborScanDB->i2Rssi =
                        pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.i2Rssi;

                    pRfMgmtNeighborScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
                else
                {
                    /* Already reported the timer value to AC. Update the time
                     * alone */
                    pRfMgmtNeighborScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
                pRfMgmtNeighborScanDB->u2ScannedChannel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u2ScannedChannel;
            }

            break;

        case RFMGMT_GET_NEIGHBOR_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtNeighborScanTable.RfMgmtNeighborScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bRssi != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.i2Rssi = pRfMgmtNeighborScanDB->i2Rssi;

                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u4LastUpdatedTime
                    = pRfMgmtNeighborScanDB->u4LastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanIsSetDB.bExternalAPIndex != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.u2ExternalAPIndex
                    = pRfMgmtNeighborScanDB->u2ExternalAPIndex;
            }

            break;

        case RFMGMT_CREATE_CLIENT_SCAN_ENTRY:
            /*Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtClientScanDB =
                 (tRfMgmtClientScanDB *) MemAllocMemBlk
                 (RFMGMT_CLIENT_SCAN_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtClientScanDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtClientScanDB: Memory alloc failed for radio index"));
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));

            /* Copy the received values to the DB */
            pRfMgmtClientScanDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex;

            pRfMgmtClientScanDB->i2ClientSNR =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.i2ClientSNR;

            MEMCPY (pRfMgmtClientScanDB->ClientMacAddress,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress, MAC_ADDR_LEN);

            /* The below time will be used to delete the inactive entries,
             * incase of exceeding the threshold */
            pRfMgmtClientScanDB->u4LastUpdatedTime = OsixGetSysUpTime ();

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                 (tRBElem *) pRfMgmtClientScanDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientScanDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                              "Scan DB"));
                return RFMGMT_FAILURE;
            }

            break;

        case RFMGMT_DESTROY_CLIENT_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientScanTable.RfMgmtClientScanDB);
            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Client Scan in DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Client Scan in DB"));
                return RFMGMT_FAILURE;
            }
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                       pRfMgmtClientScanDB);
            MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtClientScanDB);
            /*pRfMgmtClientScanDB = NULL; */
            break;

        case RFMGMT_SET_CLIENT_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientScanTable.RfMgmtClientScanDB);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Client Scan in DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Client Scan in DB"));
                return RFMGMT_FAILURE;
            }

            /* Compare the SNR Value exist in DB and the received value. If the
             * value changes then set the value along with entry status as add
             * and update the last updated time */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bClientSNR != OSIX_FALSE)
            {
                if (pRfMgmtClientScanDB->i2ClientSNR !=
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.i2ClientSNR)
                {
                    pRfMgmtClientScanDB->i2ClientSNR =
                        pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                        RfMgmtClientScanDB.i2ClientSNR;
                    pRfMgmtClientScanDB->u1EntryStatus =
                        RFMGMT_CLIENT_ENTRY_ADD;

                    pRfMgmtClientScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
                else
                {
                    /* Already reported the timer value to AC. Update the time
                     * alone */
                    pRfMgmtClientScanDB->u4LastUpdatedTime =
                        OsixGetSysUpTime ();
                }
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bEntryStatus != OSIX_FALSE)
            {
                pRfMgmtClientScanDB->u1EntryStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus;
            }
            break;

        case RFMGMT_GET_CLIENT_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientScanTable.RfMgmtClientScanDB);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Client Scan in DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Client Scan in DB"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bClientSNR != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.i2ClientSNR =
                    pRfMgmtClientScanDB->i2ClientSNR;

                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus =
                    pRfMgmtClientScanDB->u1EntryStatus;

                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u4LastUpdatedTime
                    = pRfMgmtClientScanDB->u4LastUpdatedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanIsSetDB.bEntryStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.u1EntryStatus =
                    pRfMgmtClientScanDB->u1EntryStatus;
            }
            break;

        case RFMGMT_GET_FIRST_CLIENT_ENTRY:
            pRfMgmtClientScanDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Client entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Client entry not found"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.i2ClientSNR =
                pRfMgmtClientScanDB->i2ClientSNR;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus =
                pRfMgmtClientScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex
                = pRfMgmtClientScanDB->u4RadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress,
                    pRfMgmtClientScanDB->ClientMacAddress, MAC_ADDR_LEN);

            break;

        case RFMGMT_GET_NEXT_CLIENT_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtClientScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtClientScanTable.RfMgmtClientScanDB, NULL);

            if (pRfMgmtClientScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Client entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Client entry not found"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.i2ClientSNR =
                pRfMgmtClientScanDB->i2ClientSNR;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u1EntryStatus =
                pRfMgmtClientScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                RfMgmtClientScanDB.u4RadioIfIndex
                = pRfMgmtClientScanDB->u4RadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtClientScanTable.
                    RfMgmtClientScanDB.ClientMacAddress,
                    pRfMgmtClientScanDB->ClientMacAddress, MAC_ADDR_LEN);

            break;
        case RFMGMT_CREATE_RADIO_IF_INDEX_ENTRY:
            /*Memory Allocation for the node to be added in the tree */

            MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
            if ((pRfMgmtAPConfigDB =
                 (tRfMgmtAPConfigDB *) MemAllocMemBlk
                 (RFMGMT_INDEX_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for radio index"));
                return RFMGMT_FAILURE;
            }
            if ((pRfMgmtClientConfigDB =
                 (tRfMgmtClientConfigDB *) MemAllocMemBlk
                 (RFMGMT_CLIENT_INDEX_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for radio index"));

                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                /*pRfMgmtAPConfigDB = NULL; */
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtAPConfigDB, 0, sizeof (tRfMgmtAPConfigDB));
            MEMSET (pRfMgmtClientConfigDB, 0, sizeof (tRfMgmtClientConfigDB));

            pRfMgmtAPConfigDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;
            pRfMgmtAPConfigDB->u4Dot11RadioType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4Dot11RadioType;
            RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                pRfMgmtAPConfigDB->u4RadioIfIndex;
            RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfDB) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientConfigDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "WssIfProcessRadioIfDBMsg: RBTreeGet failed  "
                            "DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "WssIfProcessRadioIfDBMsg: RBTreeGet failed  "
                              "DB\n"));
                return RFMGMT_FAILURE;
            }
            pRfMgmtAPConfigDB->u4Dot11RadioType =
                RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType;

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                 (tRBElem *) pRfMgmtAPConfigDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientConfigDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for AP config "
                            "DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for AP config "
                              "DB"));
                return RFMGMT_FAILURE;
            }

            /* Initializing default values for the AP and Client Config Table */
            pRfMgmtAPConfigDB->u2CurrentChannel = RFMGMT_DEF_CURRENT_CHANNEL;

            pRfMgmtAPConfigDB->u2NeighborMsgPeriod =
                RFMGMT_DEF_NEIGHBOR_MSG_PERIOD;

            pRfMgmtAPConfigDB->u2ChannelScanDuration =
                RFMGMT_DEF_CHANNEL_SCAN_DURATION;

            pRfMgmtAPConfigDB->u2NeighborAgingPeriod =
                RFMGMT_DEF_NEIGHBOR_AGING_PERIOD;

            pRfMgmtAPConfigDB->u1ChannelChangeCount =
                RFMGMT_DEF_CHANNEL_CHANGE_COUNT;

            if ((pRfMgmtAPConfigDB->u4Dot11RadioType
                 == RFMGMT_RADIO_TYPEA) ||
                (pRfMgmtAPConfigDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
                || (pRfMgmtAPConfigDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval =
                    RFMGMT_DEF_TPC_REQUEST_INTERVAL;
            }
            else
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval = 0;
            }
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval =
                RFMGMT_DEF_DFS_QUIET_INTERVAL;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod =
                RFMGMT_DEF_DFS_QUIET_PERIOD;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsMeasurementInterval =
                RFMGMT_DEF_DFS_MEASUREMENT_INTERVAL;
            pRfMgmtAPConfigDB->u2RfMgmt11hDfsChannelSwitchStatus =
                RFMGMT_DEF_DFS_CHANNEL_SWITCH_STATUS;

            pRfMgmtAPConfigDB->u1AutoScanStatus = RFMGMT_AUTO_SCAN_ENABLE;

            pRfMgmtAPConfigDB->u1ChannelAssignmentMode = RFMGMT_DEF_DCA_MODE;

            pRfMgmtClientConfigDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;

            MEMCPY (pRfMgmtAPConfigDB->RadioMacAddr,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.RadioMacAddr, sizeof (tMacAddr));
            if (WsscfgGetFsDot11CountryString (au1FsDot11CountryString) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientConfigDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Country String get failed\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Country String get failed\n"));
                return OSIX_FAILURE;
            }

            for (u4Index = 0; u4Index < RADIOIF_SUPPORTED_COUNTRY_LIST;
                 u4Index++)
            {
                if (STRCMP (au1FsDot11CountryString,
                            gWsscfgSupportedCountryList[u4Index].
                            au1CountryCode) == 0)
                {
                    u4Index =
                        (UINT4) gRegDomainCountryMapping[u4Index].u2RegDomain;
                    break;
                }
            }
            if (u4Index == RADIOIF_SUPPORTED_COUNTRY_LIST)
            {
                u4Index = 0;
            }

            pRfMgmtAPConfigDB->u4Dot11RadioType =
                RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType;
            if ((pRfMgmtAPConfigDB->u4Dot11RadioType
                 == RFMGMT_RADIO_TYPEA) ||
                (pRfMgmtAPConfigDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
                || (pRfMgmtAPConfigDB->u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
            {
                if (u4Index != RADIOIF_SUPPORTED_COUNTRY_LIST)
                {
                    for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA;
                         u1Channel++)
                    {
                        if (gau1Dot11aChannelCheckList[u1Channel] != 0)
                        {
                            pRfMgmtAPConfigDB->
                                au1AllowedChannelList[u1Channel] =
                                gau1Dot11aRegDomainChannelList[u1Channel];
                        }
                    }
                }
            }
            else
            {
                if (u4Index != RADIOIF_SUPPORTED_COUNTRY_LIST)
                {
                    u1Count = RFMGMT_MAX_CHANNELB;
                    /*Since for country Japan channel 14 is not allowed in
                     * radiotype g*/
                    if (pRfMgmtDB->unRfMgmtDB.RfMgmtAutoRfTable.
                        RfMgmtAutoRfProfileDB.u4Dot11RadioType ==
                        RFMGMT_RADIO_TYPEG)
                    {
                        u1Count = RFMGMT_MAX_CHANNELB - 1;
                        pRfMgmtAPConfigDB->
                            au1AllowedChannelList[RFMGMT_MAX_CHANNELB - 1] = 0;
                    }
                    for (u1Channel = 0; u1Channel < u1Count; u1Channel++)
                    {
                        if (gau1Dot11bChannelRegDomainCheckList[u4Index]
                            [u1Channel] != 0)
                        {
                            pRfMgmtAPConfigDB->
                                au1AllowedChannelList[u1Channel] =
                                gau1Dot11bRegDomainChannelList[u1Channel];
                        }
                    }
                }

            }

            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                 (tRBElem *) pRfMgmtClientConfigDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtClientConfigDB);
                RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                           RfMgmtNeighborAPConfigDB, pRfMgmtAPConfigDB);
                MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                    (UINT1 *) pRfMgmtAPConfigDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                            "config DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Client "
                              "config DB"));
                return RFMGMT_FAILURE;
            }

            pRfMgmtClientConfigDB->u4TxPowerChangeTime =
                RFMGMT_DEF_TX_POWER_CHANGE_TIME;

            pRfMgmtClientConfigDB->u2SNRScanPeriod = RFMGMT_DEF_SNR_SCAN_PERIOD;

            pRfMgmtClientConfigDB->u1TxPowerChangeCount =
                RFMGMT_DEF_TX_POWER_CHANGE_COUNT;

            pRfMgmtClientConfigDB->u1SNRScanStatus = RFMGMT_SNR_SCAN_ENABLE;

            break;

        case RFMGMT_SET_AP_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Invalid IfIndex type received "
                            "RFMGMT_SET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_SET_AP_CONFIG_ENTRY"));
                return RFMGMT_FAILURE;
            }

            /* Pass the received input and check entry is already present */
            pRfMgmtAPConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtApConfigTable.RfMgmtAPConfigDB);

            if (pRfMgmtAPConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDot11RadioType == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u4Dot11RadioType =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u4Dot11RadioType;
            }
            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bCurrentChannel == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2CurrentChannel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2CurrentChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborMsgPeriod == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2NeighborMsgPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborMsgPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelScanDuration == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2ChannelScanDuration =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2ChannelScanDuration;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborAgingPeriod == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2NeighborAgingPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborAgingPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeCount == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1ChannelChangeCount++;
                OsixGetSysTime (&u4sysTime);
                pRfMgmtAPConfigDB->u4ChannelChangeTime = u4sysTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelAssignmentMode == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1ChannelAssignmentMode =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelAssignmentMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bAutoScanStatus == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1AutoScanStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1AutoScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bPerAPTpcSelection == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u1PerAPTpcSelection =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1PerAPTpcSelection;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRadioMacAddr == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtAPConfigDB->RadioMacAddr,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.RadioMacAddr, sizeof (tMacAddr));
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bScannedChannel == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtAPConfigDB->au1ScannedChannel,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.au1ScannedChannel,
                        RFMGMT_MAX_CHANNELA);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod == OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsMeasurementInterval ==
                OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsMeasurementInterval =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsChannelSwitchStatus ==
                OSIX_TRUE)
            {
                pRfMgmtAPConfigDB->u2RfMgmt11hDfsChannelSwitchStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bGetAllowedList == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtAPConfigDB->au1AllowedChannelList,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.au1AllowedChannelList,
                        RFMGMT_MAX_CHANNELA);
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtAPConfigDB->DFSChannelInfo,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.DFSChannelInfo,
                        (sizeof (tDFSChannelInfo)) * (RADIOIF_MAX_CHANNEL_A));
            }
            break;

        case RFMGMT_GET_AP_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_GET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_GET_AP_CONFIG_ENTRY"));
                return RFMGMT_FAILURE;
            }
            RfMgmtAPConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;

            /* Pass the received input and check entry is already present */
            pRfMgmtAPConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtApConfigTable.RfMgmtAPConfigDB);

            if (pRfMgmtAPConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDot11RadioType == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u4Dot11RadioType =
                    pRfMgmtAPConfigDB->u4Dot11RadioType;
            }
            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bCurrentChannel == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2CurrentChannel =
                    pRfMgmtAPConfigDB->u2CurrentChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeTime == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u4ChannelChangeTime =
                    pRfMgmtAPConfigDB->u4ChannelChangeTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborMsgPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborMsgPeriod =
                    pRfMgmtAPConfigDB->u2NeighborMsgPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelScanDuration == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2ChannelScanDuration =
                    pRfMgmtAPConfigDB->u2ChannelScanDuration;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bNeighborAgingPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2NeighborAgingPeriod =
                    pRfMgmtAPConfigDB->u2NeighborAgingPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelChangeCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelChangeCount =
                    pRfMgmtAPConfigDB->u1ChannelChangeCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bChannelAssignmentMode == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1ChannelAssignmentMode =
                    pRfMgmtAPConfigDB->u1ChannelAssignmentMode;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bAutoScanStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1AutoScanStatus =
                    pRfMgmtAPConfigDB->u1AutoScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bPerAPTpcSelection == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u1PerAPTpcSelection =
                    pRfMgmtAPConfigDB->u1PerAPTpcSelection;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRadioMacAddr == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.RadioMacAddr,
                        pRfMgmtAPConfigDB->RadioMacAddr, sizeof (tMacAddr));
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval =
                    pRfMgmtAPConfigDB->u2RfMgmt11hTpcRequestInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsQuietPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsMeasurementInterval ==
                OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsMeasurementInterval;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsChannelSwitchStatus ==
                OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                    RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus =
                    pRfMgmtAPConfigDB->u2RfMgmt11hDfsChannelSwitchStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bScannedChannel == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.au1ScannedChannel,
                        pRfMgmtAPConfigDB->au1ScannedChannel,
                        RFMGMT_MAX_CHANNELA);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bGetAllowedList == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.au1AllowedChannelList,
                        pRfMgmtAPConfigDB->au1AllowedChannelList,
                        RFMGMT_MAX_CHANNELA);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigIsSetDB.bDFSChannelInfoSet == OSIX_TRUE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                        RfMgmtAPConfigDB.DFSChannelInfo,
                        pRfMgmtAPConfigDB->DFSChannelInfo,
                        (sizeof (tDFSChannelInfo)) * (RADIOIF_MAX_CHANNEL_A));
            }
            break;

        case RFMGMT_SET_CLIENT_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_SET_CLIENT_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_SET_CLIENT_CONFIG_ENTRY"));
                return RFMGMT_FAILURE;
            }
            MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));

            RfMgmtClientConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex;

            /* Pass the received input and check entry is already present */
            pRfMgmtClientConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientConfigTable.RfMgmtClientConfigDB);

            if (pRfMgmtClientConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerLevel == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u2TxPowerLevel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2TxPowerLevel;

                RfmgmtGetPowerFromPowerLevel (RfMgmtClientConfigDB.
                                              u4RadioIfIndex,
                                              pRfMgmtClientConfigDB->
                                              u2TxPowerLevel,
                                              &i2CurrentTxPower);
                pRfMgmtClientConfigDB->i2CurrentTxPower = i2CurrentTxPower;

                RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                    RfMgmtClientConfigDB.u4RadioIfIndex;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfDB) != OSIX_SUCCESS)
                {
                    return RFMGMT_FAILURE;
                }
                OsixGetSysTime (&u4CurrentTime);
                UtlGetTimeStrForTicks (u4CurrentTime, (CHR1 *) au1CurrentTime);

                RfMgmtGetCapwapProfileId (RadioIfDB.RadioIfGetAllDB.
                                          u2WtpInternalId, &u2WtpInternalId);
                RFMGMT_TRC1 (RFMGMT_SHA_TRC, "At Time %s: ", au1CurrentTime);
                RFMGMT_TRC3 (RFMGMT_SHA_TRC, "Tx Power level %d is set "
                             "to AP: %d Radio Id: %d\r\n",
                             pRfMgmtClientConfigDB->u2TxPowerLevel,
                             u2WtpInternalId,
                             RadioIfDB.RadioIfGetAllDB.u1RadioId);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u2SNRScanPeriod =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2SNRScanPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeCount == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u1TxPowerChangeCount++;
                OsixGetSysTime (&u4sysTime);
                pRfMgmtClientConfigDB->u4TxPowerChangeTime = u4sysTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u1SNRScanStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1SNRScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerIncreaseCount == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u4TxPowerIncreaseCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerIncreaseCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerDecreaseCount == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u4TxPowerDecreaseCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerDecreaseCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bBelowSNRCount == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u4BelowSNRCount++;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bClientsDiscarded == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u2ClientsDiscarded++;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanCount == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u4SNRScanCount++;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bClientsAccepted == OSIX_TRUE)
            {
                pRfMgmtClientConfigDB->u2ClientsAccepted++;
            }
            break;

        case RFMGMT_GET_CLIENT_CONFIG_ENTRY:
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigDB.u4RadioIfIndex == 0)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Invalid IfIndex type received "
                            "RFMGMT_GET_AP_CONFIG_ENTRY\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "Invalid IfIndex type received "
                              "RFMGMT_GET_AP_CONFIG_ENTRY"));
                return RFMGMT_FAILURE;
            }
            /*RfMgmtClientConfigDB.u4RadioIfIndex =
               pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
               RfMgmtClientConfigDB.u4RadioIfIndex; */

            /* Pass the received input and check entry is already present */
            pRfMgmtClientConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtClientConfigTable.RfMgmtClientConfigDB);

            if (pRfMgmtClientConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerLevel == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2TxPowerLevel =
                    pRfMgmtClientConfigDB->u2TxPowerLevel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bCurrentTxPower == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.i2CurrentTxPower =
                    pRfMgmtClientConfigDB->i2CurrentTxPower;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeTime == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerChangeTime =
                    pRfMgmtClientConfigDB->u4TxPowerChangeTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanPeriod == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2SNRScanPeriod =
                    pRfMgmtClientConfigDB->u2SNRScanPeriod;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerChangeCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1TxPowerChangeCount =
                    pRfMgmtClientConfigDB->u1TxPowerChangeCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanStatus == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u1SNRScanStatus =
                    pRfMgmtClientConfigDB->u1SNRScanStatus;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerIncreaseCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerIncreaseCount =
                    pRfMgmtClientConfigDB->u4TxPowerIncreaseCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bTxPowerDecreaseCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4TxPowerDecreaseCount =
                    pRfMgmtClientConfigDB->u4TxPowerDecreaseCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bBelowSNRCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4BelowSNRCount =
                    pRfMgmtClientConfigDB->u4BelowSNRCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bClientsDiscarded == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2ClientsDiscarded =
                    pRfMgmtClientConfigDB->u2ClientsDiscarded;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bSNRScanCount == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u4SNRScanCount =
                    pRfMgmtClientConfigDB->u4SNRScanCount;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                RfMgmtClientConfigIsSetDB.bClientsAccepted == OSIX_TRUE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtClientConfigTable.
                    RfMgmtClientConfigDB.u2ClientsAccepted =
                    pRfMgmtClientConfigDB->u2ClientsAccepted;
            }
            break;

        case RFMGMT_GET_FIRST_NEIGH_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                RfMgmtNeighborAPScanDB);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: No neighbor entry present\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: No neighbor entry present"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.i2Rssi = pRfMgmtNeighborScanDB->i2Rssi;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex
                = pRfMgmtNeighborScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ScannedChannel
                = pRfMgmtNeighborScanDB->u2ScannedChannel;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ExternalAPIndex
                = pRfMgmtNeighborScanDB->u2ExternalAPIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.NeighborAPMac,
                    pRfMgmtNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

            break;

        case RFMGMT_GET_NEXT_NEIGH_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtNeighborScanTable.RfMgmtNeighborScanDB,
                               NULL);

            if (pRfMgmtNeighborScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Neighbor entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Neighbor entry not found"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.i2Rssi = pRfMgmtNeighborScanDB->i2Rssi;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u1EntryStatus =
                pRfMgmtNeighborScanDB->u1EntryStatus;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u4RadioIfIndex
                = pRfMgmtNeighborScanDB->u4RadioIfIndex;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ScannedChannel
                = pRfMgmtNeighborScanDB->u2ScannedChannel;

            pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                RfMgmtNeighborScanDB.u2ExternalAPIndex
                = pRfMgmtNeighborScanDB->u2ExternalAPIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtNeighborScanTable.
                    RfMgmtNeighborScanDB.NeighborAPMac,
                    pRfMgmtNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

            break;

        case RFMGMT_DESTROY_RADIO_IF_INDEX_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtAPConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtApConfigTable.RfMgmtAPConfigDB);

            /*Delete the failed AP DB correponding to the deleted interface */
            RfmgmtDeleteFailedAPDB (pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                                    RfMgmtAPConfigDB.u4RadioIfIndex);

            RfMgmtDeleteNeighborDetails (pRfMgmtDB->unRfMgmtDB.
                                         RfMgmtApConfigTable.RfMgmtAPConfigDB.
                                         u4RadioIfIndex, RFMGMT_FLUSH_DB);
            if (pRfMgmtAPConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                       pRfMgmtAPConfigDB);
            MemReleaseMemBlock (RFMGMT_INDEX_DB_POOLID,
                                (UINT1 *) pRfMgmtAPConfigDB);
            /*pRfMgmtAPConfigDB = NULL; */

            RfMgmtClientConfigDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtApConfigTable.
                RfMgmtAPConfigDB.u4RadioIfIndex;

            /* Pass the received input and check entry is already present */
            pRfMgmtClientConfigDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                           (tRBElem *) & RfMgmtClientConfigDB);

            if (pRfMgmtClientConfigDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the neighbor AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the neighbor AP"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                       pRfMgmtClientConfigDB);
            MemReleaseMemBlock (RFMGMT_CLIENT_INDEX_DB_POOLID,
                                (UINT1 *) pRfMgmtClientConfigDB);
            /*pRfMgmtClientConfigDB = NULL; */

            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);

            while (pRfMgmtNextNeighborScanDB != NULL)
            {
                RfMgmtNextNeighborScanDB.u4RadioIfIndex =
                    pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
                RfMgmtNextNeighborScanDB.u2ScannedChannel =
                    pRfMgmtNextNeighborScanDB->u2ScannedChannel;
                MEMCPY (RfMgmtNextNeighborScanDB.NeighborAPMac,
                        pRfMgmtNextNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);
                if (pRfMgmtNextNeighborScanDB->u4RadioIfIndex ==
                    RfMgmtClientConfigDB.u4RadioIfIndex)
                {
                    RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               pRfMgmtNextNeighborScanDB);
                    MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                        (UINT1 *) pRfMgmtNextNeighborScanDB);

                    pRfMgmtNextNeighborScanDB =
                        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                       RfMgmtNeighborAPScanDB,
                                       (tRBElem *) & RfMgmtNextNeighborScanDB,
                                       NULL);
                }
                else
                {
                    pRfMgmtNextNeighborScanDB =
                        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                       RfMgmtNeighborAPScanDB,
                                       (tRBElem *) & RfMgmtNextNeighborScanDB,
                                       NULL);
                }

            }

            pRfMgmtClientScanDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB);
            if (pRfMgmtClientScanDB != NULL)
            {
                RfMgmtClientScanDB.u4RadioIfIndex =
                    pRfMgmtClientScanDB->u4RadioIfIndex;

                MEMCPY (RfMgmtClientScanDB.ClientMacAddress,
                        pRfMgmtClientScanDB->ClientMacAddress, MAC_ADDR_LEN);

                while (pRfMgmtClientScanDB != NULL)
                {
                    if (pRfMgmtClientScanDB->u4RadioIfIndex
                        == RfMgmtClientConfigDB.u4RadioIfIndex)
                    {
                        pRfMgmtNextClientScanDB =
                            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                           RfMgmtClientScanDB,
                                           (tRBElem *) & RfMgmtClientScanDB,
                                           NULL);

                        if (pRfMgmtNextClientScanDB != NULL)
                        {
                            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                                       RfMgmtClientScanDB,
                                       pRfMgmtNextClientScanDB);
                            MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                                (UINT1 *)
                                                pRfMgmtNextClientScanDB);
                            pRfMgmtNextClientScanDB = NULL;
                        }
                        else
                        {
                            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                                       RfMgmtClientScanDB, pRfMgmtClientScanDB);
                            MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                                (UINT1 *) pRfMgmtClientScanDB);
                            pRfMgmtClientScanDB = NULL;
                        }
                    }
                    else
                    {
                        RfMgmtClientScanDB.u4RadioIfIndex =
                            pRfMgmtClientScanDB->u4RadioIfIndex;

                        MEMCPY (RfMgmtClientScanDB.ClientMacAddress,
                                pRfMgmtClientScanDB->ClientMacAddress,
                                MAC_ADDR_LEN);

                        pRfMgmtClientScanDB =
                            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                           RfMgmtClientScanDB,
                                           (tRBElem *) & RfMgmtClientScanDB,
                                           NULL);

                    }
                }
            }
            break;

        case RFMGMT_CREATE_BSSID_SCAN_ENTRY:
            /*Memory Allocation for the node to be added in the tree */

#if 0                            /* RAJA */
            printf ("RFMGMT_CREATE_BSSID_SCAN_ENTRY   %d \n",
                    RFMGMT_BSSID_SCAN_DB_POOLID);
#endif
            if ((pRfMgmtBssidScanDB =
                 (tRfMgmtBssidScanDB *) MemAllocMemBlk
                 (RFMGMT_BSSID_SCAN_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for BSSID\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for BSSID"));
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtBssidScanDB, 0, sizeof (tRfMgmtBssidScanDB));

            pRfMgmtBssidScanDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
                RfMgmtBssidScanDB.u4RadioIfIndex;
            pRfMgmtBssidScanDB->u1AutoScanStatus =
                RFMGMT_BSSID_SCAN_STATUS_ENABLE;
            pRfMgmtBssidScanDB->u1WlanID =
                pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
                RfMgmtBssidScanDB.u1WlanID;
            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB,
                 (tRBElem *) pRfMgmtBssidScanDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_BSSID_SCAN_DB_POOLID,
                                    (UINT1 *) pRfMgmtBssidScanDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for BSSID"
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for BSSID "
                              "Scan DB"));
                return RFMGMT_FAILURE;
            }
            break;

        case RFMGMT_DESTROY_BSSID_SCAN_ENTRY:

            /* Pass the received input and check entry is already present */
            pRfMgmtBssidScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtBssidScanTable.RfMgmtBssidScanDB);

            if (pRfMgmtBssidScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the BSSID\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the BSSID"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB,
                       pRfMgmtBssidScanDB);
            MemReleaseMemBlock (RFMGMT_BSSID_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtBssidScanDB);
            /*pRfMgmtBssidScanDB = NULL; */
            break;

        case RFMGMT_SET_BSSID_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtBssidScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtBssidScanTable.RfMgmtBssidScanDB);

            if (pRfMgmtBssidScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the BSSID\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the BSSID"));
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
                RfMgmtBssidScanIsSetDB.bAutoScanStatus != OSIX_FALSE)
            {
                pRfMgmtBssidScanDB->u1AutoScanStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
                    RfMgmtBssidScanDB.u1AutoScanStatus;
            }
            break;

        case RFMGMT_GET_BSSID_SCAN_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtBssidScanDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtBssidScanTable.RfMgmtBssidScanDB);

            if (pRfMgmtBssidScanDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the BSSID\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the BSSID"));
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
                RfMgmtBssidScanIsSetDB.bAutoScanStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtBssidScanTable.
                    RfMgmtBssidScanDB.u1AutoScanStatus =
                    pRfMgmtBssidScanDB->u1AutoScanStatus;
            }

            break;

        case RFMGMT_CREATE_FAILED_AP_NEIGH_ENTRY:
            /* Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtFailedAPNeighborListDB =
                 (tRfMgmtFailedAPNeighborListDB *) MemAllocMemBlk
                 (RFMGMT_FAILED_AP_NEIGH_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtFailedAPNeighborListDB: "
                            "Memory alloc failed for radio index\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtFailedAPNeighborListDB: "
                              "Memory alloc failed for radio index\n"));
                return RFMGMT_FAILURE;
            }

            MEMSET (pRfMgmtFailedAPNeighborListDB, 0,
                    sizeof (tRfMgmtFailedAPNeighborListDB));
            MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));

            /* Copy the received values to the DB */
            pRfMgmtFailedAPNeighborListDB->i2Rssi =
                pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.i2Rssi;

            pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4RadioIfIndex;

            pRfMgmtFailedAPNeighborListDB->u4NeighRadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4NeighRadioIfIndex;

            MEMCPY (pRfMgmtFailedAPNeighborListDB->NeighborAPMac,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.NeighborAPMac, MAC_ADDR_LEN);
            if (MEMCMP (pRfMgmtFailedAPNeighborListDB->NeighborAPMac,
                        testMac, MAC_ADDR_LEN) != 0)
            {
                /* Add the node to the RBTree */
                if (RBTreeAdd
                    (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                     (tRBElem *) pRfMgmtFailedAPNeighborListDB) != RB_SUCCESS)
                {
                    MemReleaseMemBlock (RFMGMT_FAILED_AP_NEIGH_DB_POOLID,
                                        (UINT1 *)
                                        pRfMgmtFailedAPNeighborListDB);
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtFailedAPNeighborListDB: RBTreeAdd failed for Neighbor "
                                "DB of Failed AP\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtFailedAPNeighborListDB: RBTreeAdd failed for Neighbor "
                                  "DB of Failed AP"));
                    return RFMGMT_FAILURE;
                }
                RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.u4RadioIfIndex;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfDB) != OSIX_SUCCESS)
                {
                    return RFMGMT_FAILURE;
                }

                RfmgmtMacToStr (pRfMgmtDB->unRfMgmtDB.
                                RfMgmtFailedAPNeighborTable.
                                RfMgmtFailedAPNeighborListDB.NeighborAPMac,
                                au1String);
                OsixGetSysTime (&u4CurrentTime);
                UtlGetTimeStrForTicks (u4CurrentTime, (CHR1 *) au1CurrentTime);

                RfMgmtGetCapwapProfileId (RadioIfDB.RadioIfGetAllDB.
                                          u2WtpInternalId, &u2WtpInternalId);

                RFMGMT_TRC1 (RFMGMT_SHA_TRC, "At Time %s: ", au1CurrentTime);
                RFMGMT_TRC3 (RFMGMT_SHA_TRC, "Add Mac %s in failed AP DB "
                             "for AP: %d Radio ID: %d\r\n", au1String,
                             u2WtpInternalId,
                             RadioIfDB.RadioIfGetAllDB.u1RadioId);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4RfmSysLogId,
                              "Add Mac in failed AP DB\n"));
            }

            break;

        case RFMGMT_GET_FIRST_FAILED_AP_NEIGH_ENTRY:
            pRfMgmtFailedAPNeighborListDB = RBTreeGetFirst (gRfMgmtGlobals.
                                                            RfMgmtGlbMib.
                                                            RfMgmtFailedAPNeighborDB);

            if (pRfMgmtFailedAPNeighborListDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg:Failed AP DB empty\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Failed AP DB empty"));
                return RFMGMT_FAILURE;
            }

            /*Copy the values from DB */
            pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.i2Rssi =
                pRfMgmtFailedAPNeighborListDB->i2Rssi;
            pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4RadioIfIndex =
                pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex;
            pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4NeighRadioIfIndex =
                pRfMgmtFailedAPNeighborListDB->u4NeighRadioIfIndex;
            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.NeighborAPMac,
                    pRfMgmtFailedAPNeighborListDB->NeighborAPMac, MAC_ADDR_LEN);
            break;

        case RFMGMT_GET_NEXT_FAILED_AP_NEIGH_ENTRY:
            pRfMgmtFailedAPNeighborListDB = RBTreeGetNext (gRfMgmtGlobals.
                                                           RfMgmtGlbMib.
                                                           RfMgmtFailedAPNeighborDB,
                                                           (tRBElem *) &
                                                           pRfMgmtDB->
                                                           unRfMgmtDB.
                                                           RfMgmtFailedAPNeighborTable.
                                                           RfMgmtFailedAPNeighborListDB,
                                                           NULL);
            if (pRfMgmtFailedAPNeighborListDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg:Next entry empty in Failed AP DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg:Next entry empty in Failed AP DB\r\n"));
                return RFMGMT_FAILURE;
            }

            /*Copy the values from DB */
            pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.i2Rssi =
                pRfMgmtFailedAPNeighborListDB->i2Rssi;
            pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4RadioIfIndex =
                pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex;
            pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4NeighRadioIfIndex =
                pRfMgmtFailedAPNeighborListDB->u4NeighRadioIfIndex;
            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.NeighborAPMac,
                    pRfMgmtFailedAPNeighborListDB->NeighborAPMac, MAC_ADDR_LEN);
            break;

        case RFMGMT_SET_FAILED_AP_NEIGH_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtFailedAPNeighborListDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtFailedAPNeighborTable.
                           RfMgmtFailedAPNeighborListDB);
            if (pRfMgmtFailedAPNeighborListDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for failed AP Neighbor\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for failed AP Neighbor"));
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListIsSetDB.bRssi != OSIX_FALSE)
            {
                pRfMgmtFailedAPNeighborListDB->i2Rssi =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.i2Rssi;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListIsSetDB.bNeighRadioIfIndex
                != OSIX_FALSE)
            {
                pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.u4RadioIfIndex;
            }
            break;

        case RFMGMT_GET_FAILED_AP_NEIGH_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtFailedAPNeighborListDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtFailedAPNeighborTable.
                           RfMgmtFailedAPNeighborListDB);
            if (pRfMgmtFailedAPNeighborListDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for failed AP Neighbor\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for failed AP Neighbor"));
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListIsSetDB.bRssi != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.i2Rssi =
                    pRfMgmtFailedAPNeighborListDB->i2Rssi;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListIsSetDB.bNeighRadioIfIndex
                != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.u4NeighRadioIfIndex =
                    pRfMgmtFailedAPNeighborListDB->u4NeighRadioIfIndex;
            }
            break;

        case RFMGMT_DESTROY_FAILED_AP_NEIGH_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtFailedAPNeighborListDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtFailedAPNeighborTable.
                           RfMgmtFailedAPNeighborListDB);

            MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
            if (pRfMgmtFailedAPNeighborListDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for failed AP Neighbor\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for failed AP Neighbor"));
                return RFMGMT_FAILURE;
            }
            RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                RfMgmtFailedAPNeighborListDB.u4RadioIfIndex;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfDB) != OSIX_SUCCESS)
            {
                return RFMGMT_FAILURE;
            }

            RfmgmtMacToStr (pRfMgmtFailedAPNeighborListDB->NeighborAPMac,
                            au1String);
            OsixGetSysTime (&u4CurrentTime);
            UtlGetTimeStrForTicks (u4CurrentTime, (CHR1 *) au1CurrentTime);

            RFMGMT_TRC1 (RFMGMT_SHA_TRC, "At Time %s: ", au1CurrentTime);
            RFMGMT_TRC3 (RFMGMT_SHA_TRC, "Delete Mac %s "
                         "in failed AP DB for AP: %d Radio ID: %d\r\n",
                         au1String, RadioIfDB.RadioIfGetAllDB.u2WtpInternalId,
                         RadioIfDB.RadioIfGetAllDB.u1RadioId);
            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4RfmSysLogId,
                          "Delete Mac" "in failed AP DB for AP:Radio\n"));

            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                       pRfMgmtFailedAPNeighborListDB);
            MemReleaseMemBlock (RFMGMT_FAILED_AP_NEIGH_DB_POOLID,
                                (UINT1 *) pRfMgmtFailedAPNeighborListDB);
            /*pRfMgmtFailedAPNeighborListDB = NULL; */
            break;

        case RFMGMT_CREATE_TPC_INFO_ENTRY:
            /*Memory Allocation for the node to be added in the tree */

            if ((pRfMgmtDot11hTpcInfoDB =
                 (tRfMgmtDot11hTpcInfoDB *) MemAllocMemBlk
                 (RFMGMT_TPC_INFO_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for TPC Info\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for TPC Info"));
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtDot11hTpcInfoDB, 0, sizeof (RfMgmtDot11hTpcInfoDB));

            pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex;

            MEMCPY (pRfMgmtDot11hTpcInfoDB->RfMgmt11hStationMacAddress,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
                    MAC_ADDR_LEN);

            pRfMgmtDot11hTpcInfoDB->i2RfMgmt11hTxPowerLevel =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel;

            pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hLinkMargin =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin;

            pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hBaseLinkMargin =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin;

            OsixGetSysTime (&(pRfMgmtDot11hTpcInfoDB->
                              u4RfMgmt11hTpcReportLastReceived));
            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                 (tRBElem *) pRfMgmtDot11hTpcInfoDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_TPC_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtDot11hTpcInfoDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for TPC "
                            "Info DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for TPC "
                              "Info DB"));
                return RFMGMT_FAILURE;
            }
            break;

        case RFMGMT_GET_TPC_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hTpcInfoDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB);

            if (pRfMgmtDot11hTpcInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found"
                            "for the TPC Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found"
                              "for the TPC Info"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTxPowerLevel != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel =
                    pRfMgmtDot11hTpcInfoDB->i2RfMgmt11hTxPowerLevel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hLinkMargin != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin =
                    pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hLinkMargin;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hBaseLinkMargin !=
                OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin =
                    pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hBaseLinkMargin;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.
                bRfMgmt11hTpcReportLastReceived != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.u4RfMgmt11hTpcReportLastReceived =
                    pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hTpcReportLastReceived;
            }
            break;

        case RFMGMT_SET_TPC_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hTpcInfoDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB);

            if (pRfMgmtDot11hTpcInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found"
                            "for the TPC Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found"
                              "for the TPC Info"));
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTxPowerLevel != OSIX_FALSE)
            {
                pRfMgmtDot11hTpcInfoDB->i2RfMgmt11hTxPowerLevel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hLinkMargin != OSIX_FALSE)
            {
                pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hLinkMargin =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin;

            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hBaseLinkMargin !=
                OSIX_FALSE)
            {
                pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hBaseLinkMargin =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin;

            }

            OsixGetSysTime (&(pRfMgmtDot11hTpcInfoDB->
                              u4RfMgmt11hTpcReportLastReceived));
            break;

        case RFMGMT_DESTROY_TPC_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hTpcInfoDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB);

            if (pRfMgmtDot11hTpcInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the TPC Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the TPC Info"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                       pRfMgmtDot11hTpcInfoDB);
            MemReleaseMemBlock (RFMGMT_TPC_INFO_DB_POOLID,
                                (UINT1 *) pRfMgmtDot11hTpcInfoDB);
            /*pRfMgmtDot11hTpcInfoDB = NULL; */
            break;

        case RFMGMT_GET_FIRST_TPC_INFO_ENTRY:
            pRfMgmtDot11hTpcInfoDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                RfMgmtDot11hTpcInfoDB);
            if (pRfMgmtDot11hTpcInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Tpc info entry not found\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Tpfc info entry not found"));
                return RFMGMT_FAILURE;
            }
            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex =
                pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
                    pRfMgmtDot11hTpcInfoDB->RfMgmt11hStationMacAddress,
                    MAC_ADDR_LEN);

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel =
                pRfMgmtDot11hTpcInfoDB->i2RfMgmt11hTxPowerLevel;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin =
                pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hLinkMargin;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin =
                pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hBaseLinkMargin;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.u4RfMgmt11hTpcReportLastReceived =
                pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hTpcReportLastReceived;
            break;

        case RFMGMT_GET_NEXT_TPC_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hTpcInfoDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtDot11hTpcInfoDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB,
                               NULL);

            if (pRfMgmtDot11hTpcInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the TPC Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the TPC Info"));
                return RFMGMT_FAILURE;
            }

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex =
                pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                    RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
                    pRfMgmtDot11hTpcInfoDB->RfMgmt11hStationMacAddress,
                    MAC_ADDR_LEN);

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel =
                pRfMgmtDot11hTpcInfoDB->i2RfMgmt11hTxPowerLevel;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin =
                pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hLinkMargin;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin =
                pRfMgmtDot11hTpcInfoDB->i1RfMgmt11hBaseLinkMargin;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
                RfMgmtDot11hTpcInfoDB.u4RfMgmt11hTpcReportLastReceived =
                pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hTpcReportLastReceived;
            break;

        case RFMGMT_CREATE_EXTERNAL_AP_ENTRY:
            /* Memory Allocation for the node to be added in the tree */
            if ((pRfMgmtExternalAPDB =
                 (tRfMgmtExternalAPDB *) MemAllocMemBlk
                 (RFMGMT_EXTERNAL_AP_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtExternalAPDB: "
                            "Memory alloc failed for External AP\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtExternalAPDB: "
                              "Memory alloc failed for External AP\n"));
                return RFMGMT_FAILURE;
            }

            MEMSET (pRfMgmtExternalAPDB, 0, sizeof (tRfMgmtExternalAPDB));

            /* Copy the received values to the DB */
            pRfMgmtExternalAPDB->u2Channel =
                pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                RfMgmtExternalAPDB.u2Channel;

            pRfMgmtExternalAPDB->u2ExternalAPId =
                pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                RfMgmtExternalAPDB.u2ExternalAPId;

            pRfMgmtExternalAPDB->i2Rssi =
                pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                RfMgmtExternalAPDB.i2Rssi;

            MEMCPY (pRfMgmtExternalAPDB->APMac,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                    RfMgmtExternalAPDB.APMac, MAC_ADDR_LEN);

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtExternalAPDB,
                 (tRBElem *) pRfMgmtExternalAPDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_EXTERNAL_AP_DB_POOLID,
                                    (UINT1 *) pRfMgmtExternalAPDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for External AP"
                            "DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for External AP"
                              "DB"));
                return RFMGMT_FAILURE;
            }

            break;

        case RFMGMT_SET_EXTERNAL_AP_ENTRY:
            /* Pass the received input and check entry is already present */

            pRfMgmtExternalAPDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtExternalAPDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtExternalAPTable.RfMgmtExternalAPDB);

            if (pRfMgmtExternalAPDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found"
                            "for the External AP\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found"
                              "for the External AP"));
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                RfMgmtExternalAPIsSetDB.bExternalAPId != OSIX_FALSE)
            {
                pRfMgmtExternalAPDB->u2ExternalAPId =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                    RfMgmtExternalAPDB.u2ExternalAPId;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                RfMgmtExternalAPIsSetDB.bChannel != OSIX_FALSE)
            {
                pRfMgmtExternalAPDB->u2Channel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                    RfMgmtExternalAPDB.u2Channel;

            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                RfMgmtExternalAPIsSetDB.bRssi != OSIX_FALSE)
            {
                pRfMgmtExternalAPDB->i2Rssi =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtExternalAPTable.
                    RfMgmtExternalAPDB.i2Rssi;
            }

            break;

        case RFMGMT_GET_EXTERNAL_AP_ENTRY:

            pRfMgmtExternalAPDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtExternalAPDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtExternalAPTable.RfMgmtExternalAPDB);

            if (pRfMgmtExternalAPDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the BSSID\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the BSSID"));
                return RFMGMT_FAILURE;
            }
            break;

        case RFMGMT_DESTROY_EXTERNAL_AP_ENTRY:
            pRfMgmtExternalAPDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtExternalAPDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtExternalAPTable.RfMgmtExternalAPDB);

            if (pRfMgmtExternalAPDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the BSSID\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the BSSID"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtExternalAPDB,
                       pRfMgmtExternalAPDB);
            MemReleaseMemBlock (RFMGMT_EXTERNAL_AP_DB_POOLID,
                                (UINT1 *) pRfMgmtExternalAPDB);
            /*pRfMgmtExternalAPDB = NULL; */
            break;

        case RFMGMT_CREATE_DFS_INFO_ENTRY:

            if ((pRfMgmtDot11hDfsInfoDB =
                 (tRfMgmtDot11hDfsInfoDB *) MemAllocMemBlk
                 (RFMGMT_DFS_INFO_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for DFS Info\n");
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtDot11hDfsInfoDB, 0, sizeof (RfMgmtDot11hDfsInfoDB));

            pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex;

            MEMCPY (pRfMgmtDot11hDfsInfoDB->RfMgmt11hStationMacAddress,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
                    MAC_ADDR_LEN);

            pRfMgmtDot11hDfsInfoDB->u1ChannelNum =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u1ChannelNum;

            pRfMgmtDot11hDfsInfoDB->bIsRadarPresent =
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.bIsRadarPresent;

            OsixGetSysTime (&(pRfMgmtDot11hDfsInfoDB->
                              u4RfMgmt11hDfsReportLastReceived));
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                 (tRBElem *) pRfMgmtDot11hDfsInfoDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_DFS_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtDot11hDfsInfoDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for DFS"
                            "Info DB\r\n");
                return RFMGMT_FAILURE;
            }
            break;

        case RFMGMT_GET_DFS_INFO_ENTRY:
            pRfMgmtDot11hDfsInfoDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB);

            if (pRfMgmtDot11hDfsInfoDB == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found"
                            "for the DFS Info\r\n");
                return RFMGMT_FAILURE;
            }

            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hChannelNum != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.u1ChannelNum =
                    pRfMgmtDot11hDfsInfoDB->u1ChannelNum;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hIsRadarPresent !=
                OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.bIsRadarPresent =
                    pRfMgmtDot11hDfsInfoDB->bIsRadarPresent;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoIsSetDB.
                bRfMgmt11hDfsReportLastReceived != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.u4RfMgmt11hDfsReportLastReceived =
                    pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hDfsReportLastReceived;
            }
            break;

        case RFMGMT_SET_DFS_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hDfsInfoDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB);

            if (pRfMgmtDot11hDfsInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found"
                            "for the DFS Info\r\n");
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hChannelNum != OSIX_FALSE)
            {
                pRfMgmtDot11hDfsInfoDB->u1ChannelNum =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.u1ChannelNum;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hIsRadarPresent !=
                OSIX_FALSE)
            {
                pRfMgmtDot11hDfsInfoDB->bIsRadarPresent =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.bIsRadarPresent;

            }

            OsixGetSysTime (&(pRfMgmtDot11hDfsInfoDB->
                              u4RfMgmt11hDfsReportLastReceived));
            break;
        case RFMGMT_DESTROY_DFS_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hDfsInfoDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB);

            if (pRfMgmtDot11hDfsInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the DFS Info\r\n");
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                       pRfMgmtDot11hDfsInfoDB);
            MemReleaseMemBlock (RFMGMT_DFS_INFO_DB_POOLID,
                                (UINT1 *) pRfMgmtDot11hDfsInfoDB);
            /*pRfMgmtDot11hDfsInfoDB = NULL; */
            break;

        case RFMGMT_GET_FIRST_DFS_INFO_ENTRY:

            pRfMgmtDot11hDfsInfoDB =
                RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                RfMgmtDot11hDfsInfoDB);
            if (pRfMgmtDot11hDfsInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: DFS info entry not found\r\n");
                return RFMGMT_FAILURE;
            }
            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex =
                pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
                    pRfMgmtDot11hDfsInfoDB->RfMgmt11hStationMacAddress,
                    MAC_ADDR_LEN);

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u1ChannelNum =
                pRfMgmtDot11hDfsInfoDB->u1ChannelNum;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.bIsRadarPresent =
                pRfMgmtDot11hDfsInfoDB->bIsRadarPresent;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u4RfMgmt11hDfsReportLastReceived =
                pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hDfsReportLastReceived;

            break;

        case RFMGMT_GET_NEXT_DFS_INFO_ENTRY:
            /* Pass the received input and check entry is already present */
            pRfMgmtDot11hDfsInfoDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtDot11hDfsInfoDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB,
                               NULL);

            if (pRfMgmtDot11hDfsInfoDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the DFS Info\r\n");
                return RFMGMT_FAILURE;
            }
            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex =
                pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex;

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                    RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
                    pRfMgmtDot11hDfsInfoDB->RfMgmt11hStationMacAddress,
                    MAC_ADDR_LEN);

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u1ChannelNum =
                pRfMgmtDot11hDfsInfoDB->u1ChannelNum;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.bIsRadarPresent =
                pRfMgmtDot11hDfsInfoDB->bIsRadarPresent;

            pRfMgmtDB->unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
                RfMgmtDot11hDfsInfoDB.u4RfMgmt11hDfsReportLastReceived =
                pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hDfsReportLastReceived;
            break;
#ifdef ROGUEAP_WANTED
        case RFMGMT_SEARCH_SHOW_ROGUE_INFO:
            pRfMgmtRogueApDBget = RBTreeGet
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                 (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                 RfMgmtRogueTable.RfMgmtRogueApDB);
            if (pRfMgmtRogueApDBget == NULL)
            {
                return RFMGMT_FAILURE;
            }
            break;
        case RFMGMT_SET_SHOW_ROGUE_INFO:
            pRfMgmtRogueApDBget = RBTreeGet
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                 (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                 RfMgmtRogueTable.RfMgmtRogueApDB);
            /* Update the Rogue Ap DB */
            if (pRfMgmtRogueApDBget != NULL)
            {
                pRfMgmtRogueApDBget->isRogueApStatus = ENABLED;
                MEMCPY (pRfMgmtRogueApDBget->u1BSSIDRogueApLearntFrom,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1BSSIDRogueApLearntFrom,
                        WSSMAC_MAC_ADDR_LEN);
                /* For Hidden SSID, SSID will be NULL always  */
                if (STRLEN
                    (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
                     u1RogueApSSID) == 0)
                {
                    MEMSET (pRfMgmtRogueApDBget->u1RogueApSSID, 0,
                            WSSMAC_MAX_SSID_LEN);
                    MEMCPY (pRfMgmtRogueApDBget->u1RogueApSSID, "NULL",
                            strlen ("NULL"));
                }
                else
                {
                    MEMSET (pRfMgmtRogueApDBget->u1RogueApSSID, 0,
                            WSSMAC_MAX_SSID_LEN);
                    MEMCPY (pRfMgmtRogueApDBget->u1RogueApSSID,
                            pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                            RfMgmtRogueApDB.u1RogueApSSID,
                            STRLEN (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                                    RfMgmtRogueApDB.u1RogueApSSID));
                }
                pRfMgmtRogueApDBget->i2RogueApRSSI =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.i2RogueApRSSI;
                pRfMgmtRogueApDBget->u2RogueApOperationChannel =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u2RogueApOperationChannel;
                if (pRfMgmtRogueApDBget->u2RogueApOperationChannel <
                    RADIOIF_MAX_CHANNEL_B)
                {
                    pRfMgmtRogueApDBget->u1RogueApDetectedRadio = RADIO_TYPE_B;
                    pRfMgmtRogueApDBget->u1RogueApDetectedBand = RADIO_BAND_B;
                }
                else
                {
                    pRfMgmtRogueApDBget->u1RogueApDetectedRadio = ROGUE_RADIO_A;
                    pRfMgmtRogueApDBget->u1RogueApDetectedBand = RADIO_BAND_A;
                }
                pRfMgmtRogueApDBget->u1RogueApDetectedRadio =
                    pRfMgmtRogueApDBget->u4RogueApClientCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u4RogueApClientCount;

                OsixGetSysTime (&pRfMgmtRogueApDBget->u4RogueApLastHeardTime);

                if ((pRfMgmtRogueApDBget->u1RogueApState == ROGUEAP_STATE_ALERT)
                    && (pRfMgmtRogueApDBget->u4RogueApLastReportedTime == 0))
                {
                    MEMCPY (&RfMgmtTrapInfo.au1RogueApBSSID,
                            pRfMgmtRogueApDBget->u1RogueApBSSID,
                            WSSMAC_MAC_ADDR_LEN);
                    RfMgmtTrapInfo.i4RogueApState =
                        pRfMgmtRogueApDBget->u1RogueApState;
                    RfmSnmpifSendTrapInCxt (RFMGMT_ROGUE_STATUS_TRAP,
                                            &RfMgmtTrapInfo);
                    OsixGetSysTime (&pRfMgmtRogueApDBget->
                                    u4RogueApLastReportedTime);
                }
                OsixGetSysTime (&pRfMgmtRogueApDBget->u4RogueApLastHeardTime);
            }
            break;

        case RFMGMT_CREATE_SHOW_ROGUE_INFO:
            if ((pRfMgmtRogueApDB =
                 (tRfMgmtRogueApDB *) MemAllocMemBlk
                 (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRogueAP_DB: Memory alloc failed for Rogue AP Info\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRogueAP_DB: Memory alloc failed for Rogue AP Info"));
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtRogueApDB, 0, sizeof (tRfMgmtRogueApDB));

            MEMCPY (pRfMgmtRogueApDB->u1RogueApBSSID,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
            MEMCPY (pRfMgmtRogueApDB->u1BSSIDRogueApLearntFrom,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1BSSIDRogueApLearntFrom,
                    WSSMAC_MAC_ADDR_LEN);
            MEMSET (pRfMgmtRogueApDB->u1RogueApSSID, 0, WSSMAC_MAX_SSID_LEN);
            /* For Hidden SSID, SSID will be NULL always  */
            if (STRLEN
                (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
                 u1RogueApSSID) == 0)
            {
                MEMCPY (pRfMgmtRogueApDB->u1RogueApSSID, "NULL",
                        strlen ("NULL"));
            }
            else
            {
                MEMCPY (pRfMgmtRogueApDB->u1RogueApSSID,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1RogueApSSID,
                        STRLEN (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                                RfMgmtRogueApDB.u1RogueApSSID));
            }
            pRfMgmtRogueApDB->i2RogueApRSSI =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApDB.i2RogueApRSSI;
            pRfMgmtRogueApDB->u2RogueApOperationChannel =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApDB.u2RogueApOperationChannel;
            pRfMgmtRogueApDB->u4RogueApClientCount =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApDB.u4RogueApClientCount;
            pRfMgmtRogueApDB->isRogueApProcetionType =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApDB.isRogueApProcetionType;

            if (pRfMgmtRogueApDB->u2RogueApOperationChannel < RADIOIF_MAX_CHANNEL_B)    /*CHANNEL for B */
            {
                pRfMgmtRogueApDB->u1RogueApDetectedRadio = RADIO_TYPE_B;    /*2GHz (B) */
                pRfMgmtRogueApDB->u1RogueApDetectedBand = RADIO_BAND_B;
            }
            else
            {
                pRfMgmtRogueApDB->u1RogueApDetectedRadio = ROGUE_RADIO_A;    /*5GHz (A) */
                pRfMgmtRogueApDB->u1RogueApDetectedBand = RADIO_BAND_A;
            }

            pRfMgmtRogueApDB->u1RogueApClass = ROGUEAP_CLASS_UNCLASSIFIED;
            pRfMgmtRogueApDB->u1RogueApClassifiedBy = ROGUEAP_CLASSIFIEDBY_NONE;
            pRfMgmtRogueApDB->u1RogueApState = ROGUEAP_STATE_NONE;

            pRfMgmtRogueApDB->isRogueApStatus = ENABLED;    /*Heard */

            OsixGetSysTime (&pRfMgmtRogueApDB->u4RogueApLastHeardTime);

            if ((MEMCMP (BcastBssId,
                         pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                         RfMgmtRogueApDB.u1RogueApBSSID,
                         sizeof (tMacAddr)) == 0) ||
                (MEMCMP (BssIdZero,
                         pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                         RfMgmtRogueApDB.u1RogueApBSSID,
                         sizeof (tMacAddr)) == 0))
            {
                MemReleaseMemBlock (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtRogueApDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Rogue AP"
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Rogue AP"
                              "DB"));
                return RFMGMT_FAILURE;
            }

            pRfMgmtRogueApDB->u4RogueApLastReportedTime = 0;
            /* Add the BSSID to the RogueAp */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                 (tRBElem *) pRfMgmtRogueApDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtRogueApDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Rogue AP"
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Rogue AP"
                              "DB"));
                return RFMGMT_FAILURE;
            }
            RfMgmtClassifyRogueApInit (pRfMgmtRogueApDB);
            break;
        case RFMGMT_GET_SHOW_ROGUE_INFO:

            /* Pass the received input and check entry is already present */
            pRfMgmtRogueApDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueApDB);

            if (pRfMgmtRogueApDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRogueApDB: Entry not found"
                            "for the AP Rogue Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRogueApDB: Entry not found"
                              "for the AP Rogue Info"));
                return RFMGMT_FAILURE;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApSSID != OSIX_FALSE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1RogueApSSID,
                        pRfMgmtRogueApDB->u1RogueApSSID,
                        STRLEN (pRfMgmtRogueApDB->u1RogueApSSID));
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bBSSIDRogueApLearntFrom != OSIX_FALSE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1BSSIDRogueApLearntFrom,
                        pRfMgmtRogueApDB->u1BSSIDRogueApLearntFrom,
                        WSSMAC_MAC_ADDR_LEN);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApProcetionType != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.isRogueApProcetionType =
                    pRfMgmtRogueApDB->isRogueApProcetionType;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApSNR != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApSNR =
                    pRfMgmtRogueApDB->u1RogueApSNR;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApRSSI != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.i2RogueApRSSI =
                    pRfMgmtRogueApDB->i2RogueApRSSI;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApOperationChannel != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u2RogueApOperationChannel =
                    pRfMgmtRogueApDB->u2RogueApOperationChannel;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApDetectedRadio != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApDetectedRadio =
                    pRfMgmtRogueApDB->u1RogueApDetectedRadio;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApDetectedBand != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApDetectedBand =
                    pRfMgmtRogueApDB->u1RogueApDetectedBand;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApLastReportedTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u4RogueApLastReportedTime =
                    pRfMgmtRogueApDB->u4RogueApLastReportedTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApLastHeardTime != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u4RogueApLastHeardTime =
                    pRfMgmtRogueApDB->u4RogueApLastHeardTime;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApTpc != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.i2RogueApTpc =
                    pRfMgmtRogueApDB->i2RogueApTpc;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApClass != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApClass =
                    pRfMgmtRogueApDB->u1RogueApClass;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApState != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApState =
                    pRfMgmtRogueApDB->u1RogueApState;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApClassifiedBy != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApClassifiedBy =
                    pRfMgmtRogueApDB->u1RogueApClassifiedBy;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApClientCount != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u4RogueApClientCount =
                    pRfMgmtRogueApDB->u4RogueApClientCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApIsSetDB.bRogueApStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.isRogueApStatus =
                    pRfMgmtRogueApDB->isRogueApStatus;
            }
            break;

        case RFMGMT_GET_RULE_ROGUE_INFO:

            /* Pass the received input and check entry is already present */

            pRfMgmtRogueRuleDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueRuleDB);

            if (pRfMgmtRogueRuleDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRogueRuleDB: Entry not found"
                            "for the Rogue Rule Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRogueRuleDB: Entry not found"
                              "for the Rogue Rule Info"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRulePriOrderNum != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRulePriOrderNum =
                    pRfMgmtRogueRuleDB->u1RogueApRulePriOrderNum;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleClass != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRuleClass =
                    pRfMgmtRogueRuleDB->u1RogueApRuleClass;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleState != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRuleState =
                    pRfMgmtRogueRuleDB->u1RogueApRuleState;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleDuration != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u4RogueApRuleDuration =
                    pRfMgmtRogueRuleDB->u4RogueApRuleDuration;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApProcetionType != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.isRogueApProcetionType =
                    pRfMgmtRogueRuleDB->isRogueApProcetionType;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRSSI != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.i2RogueApRSSI =
                    pRfMgmtRogueRuleDB->i2RogueApRSSI;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleTpc != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.i2RogueApRuleTpc =
                    pRfMgmtRogueRuleDB->i2RogueApRuleTpc;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleSSID != OSIX_FALSE)
            {
                MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueRuleDB.u1RogueApRuleSSID,
                        pRfMgmtRogueRuleDB->u1RogueApRuleSSID,
                        WSSMAC_MAX_SSID_LEN);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApClientCount != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u4RogueApClientCount =
                    pRfMgmtRogueRuleDB->u4RogueApClientCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.isRogueApStatus =
                    pRfMgmtRogueRuleDB->isRogueApStatus;
            }
            break;

        case RFMGMT_SET_RULE_ROGUE_INFO:

            /* Pass the received input and check entry is already present */
            pRfMgmtRogueRuleDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueRuleDB);

            if (pRfMgmtRogueRuleDB == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRogueRuleDB: Entry not found"
                            "for the Rogue Rule Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRogueRuleDB: Entry not found"
                              "for the Rogue Rule Info"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRulePriOrderNum != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->u1RogueApRulePriOrderNum =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRulePriOrderNum;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleClass != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->u1RogueApRuleClass =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRuleClass;
                if (pRfMgmtRogueRuleDB->u1RogueApRuleClass ==
                    ROGUEAP_CLASS_FRIENDLY)
                {
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueRuleIsSetDB.bRogueApRuleState = TRUE;
                    pRfMgmtRogueRuleDB->u1RogueApRuleState = ROGUEAP_STATE_NONE;
                }

            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleState != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->u1RogueApRuleState =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRuleState;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleDuration != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->u4RogueApRuleDuration =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u4RogueApRuleDuration;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApProcetionType != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->isRogueApProcetionType =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.isRogueApProcetionType;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRSSI != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->i2RogueApRSSI =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.i2RogueApRSSI;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleTpc != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->i2RogueApRuleTpc =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.i2RogueApRuleTpc;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApRuleSSID != OSIX_FALSE)
            {
                MEMCPY (pRfMgmtRogueRuleDB->u1RogueApRuleSSID,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueRuleDB.u1RogueApRuleSSID,
                        WSSMAC_MAX_SSID_LEN);
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApClientCount != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->u4RogueApClientCount =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u4RogueApClientCount;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleIsSetDB.bRogueApStatus != OSIX_FALSE)
            {
                pRfMgmtRogueRuleDB->isRogueApStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.isRogueApStatus;
            }
            break;

        case RFMGMT_CREATE_RULE_ROGUE_INFO:
            pRfMgmtRogueRuleDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueRuleDB);

            if (pRfMgmtRogueRuleDB != NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed for Rogue Rule Info"
                            "Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed for Rogue Rule Info"
                              "DB"));
                return RFMGMT_SUCCESS;
            }

            if ((pRfMgmtRogueRuleDB =
                 (tRfMgmtRogueRuleDB *) MemAllocMemBlk
                 (RFMGMT_ROGUE_RULE_INFO_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc failed for Rogue Rule Info\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc failed for Rogue Rule Info"));
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtRogueRuleDB, 0, sizeof (tRfMgmtRogueRuleDB));
            MEMCPY (pRfMgmtRogueRuleDB->u1RogueApRuleName,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRuleName, WSSMAC_MAX_SSID_LEN);
            pRfMgmtRogueRuleDB->u1RogueApRulePriOrderNum =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleDB.u1RogueApRulePriOrderNum;
            pRfMgmtRogueRuleDB->u1RogueApRuleClass =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueRuleDB.u1RogueApRuleClass;
            if (pRfMgmtRogueRuleDB->u1RogueApRuleClass ==
                ROGUEAP_CLASS_FRIENDLY)
            {
                pRfMgmtRogueRuleDB->u1RogueApRuleState = ROGUEAP_STATE_NONE;
            }
            else
            {
                pRfMgmtRogueRuleDB->u1RogueApRuleState =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueRuleDB.u1RogueApRuleState;
            }
            pRfMgmtRogueRuleDB->u4RogueApRuleDuration = DEFAULT_DURATION;
            pRfMgmtRogueRuleDB->isRogueApProcetionType = DEFAULT_PROTECTION;
            pRfMgmtRogueRuleDB->i2RogueApRSSI = DEFAULT_RSSI;
            pRfMgmtRogueRuleDB->i2RogueApRuleTpc = 0;
            pRfMgmtRogueRuleDB->u4RogueApClientCount = DEFAULT_CLIENT_COUNT;
            pRfMgmtRogueRuleDB->isRogueApStatus = 0;

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                 (tRBElem *) pRfMgmtRogueRuleDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_ROGUE_RULE_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtRogueRuleDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd failed "
                            "for Rogue Rule Info Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed "
                              "for Rogue Rule Info DB"));
                return RFMGMT_FAILURE;
            }
            gu1RogueRuleCount++;

            break;

        case RFMGMT_DESTROY_RULE_ROGUE_INFO:
            /* Pass the received input and check entry is already present */
            pRfMgmtRogueRuleDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueRuleDB);

            if (pRfMgmtRogueRuleDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Rogue Rule Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Rogue Rule Info"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                       pRfMgmtRogueRuleDB);
            MemReleaseMemBlock (RFMGMT_ROGUE_RULE_INFO_DB_POOLID,
                                (UINT1 *) pRfMgmtRogueRuleDB);

            pRfMgmtRogueRuleDB = NULL;
            gu1RogueRuleCount--;
            break;
        case RFMGMT_DESTROY_RULE_ROGUE_INFO_ALL:
            pRfMgmtRogueRuleDB = RBTreeGetFirst
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB);
            if (pRfMgmtRogueRuleDB == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Rogue Rule Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Rogue Rule Info"));
                return RFMGMT_FAILURE;
            }
            while (pRfMgmtRogueRuleDB != NULL)
            {

                pRfMgmtNextRogueRuleDB =
                    (tRfMgmtRogueRuleDB *) RBTreeGetNext
                    (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                     (tRBElem *) pRfMgmtRogueRuleDB, NULL);

                /*Remove the corresponding node from the tree */
                RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB,
                           pRfMgmtRogueRuleDB);
                MemReleaseMemBlock (RFMGMT_ROGUE_RULE_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtRogueRuleDB);

                pRfMgmtRogueRuleDB = pRfMgmtNextRogueRuleDB;
                gu1RogueRuleCount--;
            }
            break;

        case RFMGMT_GET_MANUAL_ROGUE_INFO:
            /* Pass the received input and check entry is already present */
            pRfMgmtRogueManualDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueManualDB);

            if (pRfMgmtRogueManualDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRogueApDB: Entry not found"
                            "for the Manual Rogue Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRogueApDB: Entry not found"
                              "for the Manual Info"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualIsSetDB.bRogueApClass != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApClass =
                    pRfMgmtRogueManualDB->u1RogueApClass;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualIsSetDB.bRogueApState != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApState =
                    pRfMgmtRogueManualDB->u1RogueApState;
            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualIsSetDB.bRogueApStatus != OSIX_FALSE)
            {
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.isRogueApStatus =
                    pRfMgmtRogueManualDB->isRogueApStatus;
            }
            break;

        case RFMGMT_SET_MANUAL_ROGUE_INFO:
            /* Pass the received input and check entry is already present */
            pRfMgmtRogueManualDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueManualDB);

            if (pRfMgmtRogueManualDB == NULL)
            {
                /* If not present, return error */
                RfMgmtProcessDBMsg (RFMGMT_CREATE_MANUAL_ROGUE_INFO, pRfMgmtDB);
                pRfMgmtRogueManualDB =
                    RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB,
                               (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                               RfMgmtRogueTable.RfMgmtRogueManualDB);
            }

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApBSSID,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);

            pRfMgmtRogueApDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueApDB);
            if (pRfMgmtRogueApDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtRogueApDB: Entry not found"
                            "for the TPC Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtRogueApDB: Entry not found"
                              "for the TPC Info"));
                return RFMGMT_FAILURE;
            }

            /* Copy the values from the DB strcuture to output structure */
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualIsSetDB.bRogueApClass != OSIX_FALSE)
            {
                pRfMgmtRogueManualDB->u1RogueApClass =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApClass;
                pRfMgmtRogueApDB->u1RogueApClass =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApClass;
                if (pRfMgmtRogueApDB->u1RogueApClass == ROGUEAP_CLASS_FRIENDLY)
                {
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueManualIsSetDB.bRogueApState = TRUE;
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueManualDB.u1RogueApState = ROGUEAP_STATE_NONE;
                }

            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualIsSetDB.bRogueApState != OSIX_FALSE)
            {
                pRfMgmtRogueManualDB->u1RogueApState =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApState;
                pRfMgmtRogueApDB->u1RogueApState =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApState;
                if ((pRfMgmtRogueApDB->u1RogueApState == ROGUEAP_STATE_ALERT) &&
                    (pRfMgmtRogueApDB->isRogueApStatus == ENABLED) &&
                    (pRfMgmtRogueApDB->u4RogueApLastReportedTime == 0))
                {
                    MEMCPY (&RfMgmtTrapInfo.au1RogueApBSSID,
                            pRfMgmtRogueApDB->u1RogueApBSSID,
                            WSSMAC_MAC_ADDR_LEN);
                    RfMgmtTrapInfo.i4RogueApState =
                        pRfMgmtRogueApDB->u1RogueApState;
                    RfmSnmpifSendTrapInCxt (RFMGMT_ROGUE_STATUS_TRAP,
                                            &RfMgmtTrapInfo);
                    OsixGetSysTime (&pRfMgmtRogueApDB->
                                    u4RogueApLastReportedTime);
                }

            }
            if (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualIsSetDB.bRogueApStatus != OSIX_FALSE)
            {
                pRfMgmtRogueManualDB->isRogueApStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.isRogueApStatus;
                pRfMgmtRogueApDB->isRogueApStatus =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.isRogueApStatus;
            }
            pRfMgmtRogueApDB->u1RogueApClassifiedBy =
                ROGUEAP_CLASSIFIEDBY_MANUALLY;
            break;
        case RFMGMT_CREATE_MANUAL_ROGUE_INFO:
            if ((pRfMgmtRogueManualDB =
                 (tRfMgmtRogueManualDB *) MemAllocMemBlk
                 (RFMGMT_MANUAL_INFO_DB_POOLID)) == NULL)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtAPConfigDB: Memory alloc"
                            "failed for MANUAL ROGUE INFO\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtAPConfigDB: Memory alloc "
                              "failed for MANUAL ROGUE INFO"));
                return RFMGMT_FAILURE;
            }

            /*Initialisation */
            MEMSET (pRfMgmtRogueManualDB, 0, sizeof (tRfMgmtRogueManualDB));
            MEMCPY (pRfMgmtRogueManualDB->u1RogueApBSSID,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
            pRfMgmtRogueManualDB->u1RogueApClass =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualDB.u1RogueApClass;
            if (pRfMgmtRogueManualDB->u1RogueApClass == ROGUEAP_CLASS_FRIENDLY)
            {
                pRfMgmtRogueManualDB->u1RogueApState = ROGUEAP_STATE_NONE;
            }
            else
            {
                pRfMgmtRogueManualDB->u1RogueApState =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApState;
            }
            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB,
                 (tRBElem *) pRfMgmtRogueManualDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RFMGMT_MANUAL_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtRogueManualDB);
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: RBTreeAdd"
                            "failed for MANUAL ROGUE INFO Scan DB\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: RBTreeAdd failed"
                              "for MANUAL ROGUE INFO DB"));
            }

            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApBSSID,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
            pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApDB.u1RogueApClass =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualDB.u1RogueApClass;
            pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueApDB.u1RogueApState =
                pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                RfMgmtRogueManualDB.u1RogueApState;
            pRfMgmtRogueApDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueApDB);
            if (pRfMgmtRogueApDB == NULL)
            {
                if ((pRfMgmtRogueApDB =
                     (tRfMgmtRogueApDB *) MemAllocMemBlk
                     (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID)) == NULL)
                {
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtAPConfigDB: Memory alloc"
                                "failed for Rogue AP Info\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtAPConfigDB: Memory alloc"
                                  "failed for Rogue AP Info\n"));
                    return RFMGMT_FAILURE;
                }

                /*Initialisation */
                MEMSET (pRfMgmtRogueApDB, 0, sizeof (tRfMgmtRogueApDB));
                MEMCPY (pRfMgmtRogueApDB->u1RogueApBSSID,
                        pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
                pRfMgmtRogueApDB->u1RogueApClass =
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApClass;
                if (pRfMgmtRogueApDB->u1RogueApClass == ROGUEAP_CLASS_FRIENDLY)
                {
                    pRfMgmtRogueApDB->u1RogueApState = ROGUEAP_STATE_NONE;
                }
                else
                {
                    pRfMgmtRogueApDB->u1RogueApState =
                        pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                        RfMgmtRogueApDB.u1RogueApState;
                }
                pRfMgmtRogueApDB->u4RogueApLastHeardTime = 0;
                pRfMgmtRogueApDB->u4RogueApLastReportedTime = 0;
                pRfMgmtRogueApDB->u1RogueApClassifiedBy =
                    ROGUEAP_CLASSIFIEDBY_MANUALLY;
                /* Add the node to the RBTree */
                if (RBTreeAdd
                    (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                     (tRBElem *) pRfMgmtRogueApDB) != RB_SUCCESS)
                {
                    MemReleaseMemBlock (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID,
                                        (UINT1 *) pRfMgmtRogueApDB);
                    RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                                "RfMgmtProcessDBMsg: RBTreeAdd"
                                "failed for Rogue AP Info Scan DB\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                                  "RfMgmtProcessDBMsg: RBTreeAdd failed"
                                  "for Rogue AP Info DB"));
                    return RFMGMT_FAILURE;
                }
            }
            break;
        case RFMGMT_DESTROY_MANUAL_ROGUE_INFO:
            /* Pass the received input and check 
             * entry is already present */

            pRfMgmtRogueManualDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueManualDB);

            if (pRfMgmtRogueManualDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Manual Rogue Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Manual Rogue Info"));
            }
            if (pRfMgmtRogueManualDB != NULL)
            {
                /*Remove the corresponding node from the tree */
                RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB,
                           pRfMgmtRogueManualDB);
                MemReleaseMemBlock (RFMGMT_MANUAL_INFO_DB_POOLID,
                                    (UINT1 *) pRfMgmtRogueManualDB);

                pRfMgmtRogueManualDB = NULL;
            }

            /* Pass the received input and check entry is already present */
            MEMCPY (pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueApDB.u1RogueApBSSID,
                    pRfMgmtDB->unRfMgmtDB.RfMgmtRogueTable.
                    RfMgmtRogueManualDB.u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
            pRfMgmtRogueApDB =
                RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                           (tRBElem *) & pRfMgmtDB->unRfMgmtDB.
                           RfMgmtRogueTable.RfMgmtRogueApDB);

            if (pRfMgmtRogueApDB == NULL)
            {
                /* If not present, return error */
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessDBMsg: Entry not found "
                            "for the Rogue AP Info\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                              "RfMgmtProcessDBMsg: Entry not found "
                              "for the Rogue AP Info"));
                return RFMGMT_FAILURE;
            }
            /*Remove the corresponding node from the tree */
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB,
                       pRfMgmtRogueApDB);
            MemReleaseMemBlock (RFMGMT_ROGUEAPSCAN_INFO_DB_POOLID,
                                (UINT1 *) pRfMgmtRogueApDB);

            pRfMgmtRogueApDB = NULL;
            break;

#endif

        default:
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmt: Unknown request received\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "RfMgmt: Unknown request received"));
            return RFMGMT_FAILURE;
    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteExternalNeighborDetails                        */
/*                                                                           */
/* Description  : This function will destroy the information of external     */
/*                neighbors for the given radiotype                          */
/*                                                                           */
/* Input        : radiotype                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteExternalNeighborDetails (UINT4 u4RadioType)
{
    tRfMgmtNeighborScanDB RfMgmtNextNeighborScanDB;
    tRfMgmtNeighborScanDB *pRfMgmtNextNeighborScanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4PrevRadioIfIndex = 0;
    UINT2               u2WtpId = 0;
    UINT2               u2NeighId = 0;
    UINT1               u1RadioIndex = 0;

    MEMSET (&RfMgmtNextNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    pRfMgmtNextNeighborScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);

    while (pRfMgmtNextNeighborScanDB != NULL)
    {
        RfMgmtNextNeighborScanDB.u4RadioIfIndex =
            pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
        RfMgmtNextNeighborScanDB.u2ScannedChannel =
            pRfMgmtNextNeighborScanDB->u2ScannedChannel;
        RfMgmtNextNeighborScanDB.u2ExternalAPIndex =
            pRfMgmtNextNeighborScanDB->u2ExternalAPIndex;
        MEMCPY (RfMgmtNextNeighborScanDB.NeighborAPMac,
                pRfMgmtNextNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

        if (RfMgmtNextNeighborScanDB.u4RadioIfIndex != u4PrevRadioIfIndex)
        {

            MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                RfMgmtNextNeighborScanDB.u4RadioIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtDeleteExternalNeighborDetails: "
                            "No entry found in DB \n");
                return RFMGMT_FAILURE;
            }

            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEA)
                || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEAN)
                || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEAC))
            {
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    RFMGMT_RADIO_TYPEA;
            }
            else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB)
                     || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                         RFMGMT_RADIO_TYPEBG)
                     || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                         RFMGMT_RADIO_TYPEBGN))
            {
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    RFMGMT_RADIO_TYPEB;
            }
            else
            {
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    RFMGMT_RADIO_TYPEG;
            }
        }
        if ((RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex == u4RadioType) &&
            (RfMgmtNextNeighborScanDB.u2ExternalAPIndex != 0))
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtNeighborAPScanDB, pRfMgmtNextNeighborScanDB);
            MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNextNeighborScanDB);
        }
        u4PrevRadioIfIndex = RfMgmtNextNeighborScanDB.u4RadioIfIndex;
        pRfMgmtNextNeighborScanDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                           RfMgmtNeighborAPScanDB,
                           (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);
    }

    if (u4RadioType == RFMGMT_RADIO_TYPEA)
    {
        u1RadioIndex = 0;
    }
    else if (u4RadioType == RFMGMT_RADIO_TYPEB)
    {
        u1RadioIndex = RFMGMT_OFFSET_ONE;
    }
    else
    {
        u1RadioIndex = RFMGMT_OFFSET_TWO;
    }

    for (u2WtpId = 0; u2WtpId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
         u2WtpId++)
    {
        for (u2NeighId = RFMGMT_MAX_RADIOS;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if ((gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                 [u2NeighId].u1IsNeighborPresent == OSIX_TRUE)
                && (u2WtpId < RFMGMT_MAX_RADIOS))
            {
                gaDcaAlgorithm[u1RadioIndex][u2WtpId][u2WtpId].u1NeighAPCount--;
                if (gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                    [u2WtpId].u1NeighAPCount == 0)
                {
                    gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                        [u2WtpId].u1IsNeighborPresent = 0;
                }
            }
            MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                    [u2NeighId], 0, sizeof (tDcaAlgorithm));
            MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2NeighId]
                    [u2WtpId], 0, sizeof (tDcaAlgorithm));
        }
    }
    gau2NoExternalAp = 0;
    MEMSET (&gau2ExternalApColors, 0, MAX_RFMGMT_EXTERNAL_AP_SIZE);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteRadiotypeNeighborInfo                          */
/*                                                                           */
/* Description  : This function will destroy the information of              */
/*                neighbors for the given radiotype                          */
/*                                                                           */
/* Input        : radiotype                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteRadiotypeNeighborInfo (UINT4 u4RadioType)
{
    tRfMgmtNeighborScanDB RfMgmtNextNeighborScanDB;
    tRfMgmtNeighborScanDB *pRfMgmtNextNeighborScanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4PrevRadioIfIndex = 0;
    UINT2               u2WtpId = 0;
    UINT2               u2NeighId = 0;
    UINT1               u1RadioIndex = 0;

    MEMSET (&RfMgmtNextNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    pRfMgmtNextNeighborScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);

    while (pRfMgmtNextNeighborScanDB != NULL)
    {
        RfMgmtNextNeighborScanDB.u4RadioIfIndex =
            pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
        RfMgmtNextNeighborScanDB.u2ScannedChannel =
            pRfMgmtNextNeighborScanDB->u2ScannedChannel;
        RfMgmtNextNeighborScanDB.u2ExternalAPIndex =
            pRfMgmtNextNeighborScanDB->u2ExternalAPIndex;
        MEMCPY (RfMgmtNextNeighborScanDB.NeighborAPMac,
                pRfMgmtNextNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);

        if (RfMgmtNextNeighborScanDB.u4RadioIfIndex != u4PrevRadioIfIndex)
        {

            MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                RfMgmtNextNeighborScanDB.u4RadioIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtDeleteExternalNeighborDetails: "
                            "No entry found in DB \n");
                return RFMGMT_FAILURE;
            }

            if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RFMGMT_RADIO_TYPEA)
                || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEAN)
                || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RFMGMT_RADIO_TYPEAC))
            {
                RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
                    RFMGMT_RADIO_TYPEA;
            }
            else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                      RFMGMT_RADIO_TYPEB)
                     || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                         RFMGMT_RADIO_TYPEBG)
                     || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                         RFMGMT_RADIO_TYPEBGN))
            {
                RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
                    RFMGMT_RADIO_TYPEB;
            }
            else
            {
                RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
                    RFMGMT_RADIO_TYPEG;
            }
            u4PrevRadioIfIndex = RfMgmtNextNeighborScanDB.u4RadioIfIndex;
        }
        if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == u4RadioType)
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtNeighborAPScanDB, pRfMgmtNextNeighborScanDB);
            MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNextNeighborScanDB);
        }
        pRfMgmtNextNeighborScanDB =
            RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                           RfMgmtNeighborAPScanDB,
                           (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);
    }

    if (u4RadioType == RFMGMT_RADIO_TYPEA)
    {
        u1RadioIndex = 0;
    }
    else if (u4RadioType == RFMGMT_RADIO_TYPEB)
    {
        u1RadioIndex = RFMGMT_OFFSET_ONE;
    }
    else
    {
        u1RadioIndex = RFMGMT_OFFSET_TWO;
    }

    for (u2WtpId = 0; u2WtpId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
         u2WtpId++)
    {
        for (u2NeighId = 0;
             u2NeighId < RFMGMT_MAX_RADIOS + MAX_RFMGMT_EXTERNAL_AP_SIZE;
             u2NeighId++)
        {
            if (u2WtpId != u2NeighId)
            {
                MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                        [u2NeighId], 0, sizeof (tDcaAlgorithm));
            }
            else
            {
                gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                    [u2WtpId].u1NeighAPCount = 0;
                gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                    [u2WtpId].u1IsNeighborPresent = 0;
                if ((u2WtpId >= RFMGMT_MAX_RADIOS) &&
                    (u2NeighId >= RFMGMT_MAX_RADIOS))
                {
                    MEMSET (&gaDcaAlgorithm[u1RadioIndex][u2WtpId]
                            [u2NeighId], 0, sizeof (tDcaAlgorithm));
                }
            }
        }
    }
    gau2NoExternalAp = 0;
    MEMSET (&gau2ExternalApColors, 0, MAX_RFMGMT_EXTERNAL_AP_SIZE);
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteNeighborDetails                                */
/*                                                                           */
/* Description  : This function will destroy the neighbor AP infomarion      */
/*                for the given radio ifindex                                */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteNeighborDetails (UINT4 u4RadioIfIndex, UINT1 u1FlushAlgoTable)
{
    tRfMgmtNeighborScanDB RfMgmtNeighborScanDB;
    tRfMgmtNeighborScanDB RfMgmtNextNeighborScanDB;

    tRfMgmtNeighborScanDB *pRfMgmtNextNeighborScanDB = NULL;
    tRadioIfGetDB       RadioIfDB;
    tRfMgmtDB           RfMgmtDB;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2NeighId = 0;
    UINT2               u2WtpBaseId = 0;
    UINT4               u4RadioType = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1ShaStatus = 0;

    MEMSET (&RfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RfMgmtNextNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RFMGMT_FN_ENTRY ();

    /* Get all the neighbor AP information for the radio index 
       and flush the entries in the DB */

    RadioIfDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        return RFMGMT_FAILURE;
    }
    /*Add the neighbors to failed AP DB only if BSSID count in zero */
    if ((RadioIfDB.RadioIfGetAllDB.u1BssIdCount != 0)
        && (u1FlushAlgoTable == RFMGMT_FLUSH_IF_WLAN_DELETE))
    {
        return RFMGMT_SUCCESS;
    }
    u2WtpInternalId = RadioIfDB.RadioIfGetAllDB.u2WtpInternalId;

    RfmgmtGetWtpIndex (RadioIfDB.RadioIfGetAllDB.u2WtpInternalId,
                       RadioIfDB.RadioIfGetAllDB.u1RadioId, &u2WtpInternalId);

    u4RadioType = RadioIfDB.RadioIfGetAllDB.u4Dot11RadioType;

    if ((u4RadioType == RFMGMT_RADIO_TYPEA) ||
        (u4RadioType == RFMGMT_RADIO_TYPEAN) ||
        (u4RadioType == RFMGMT_RADIO_TYPEAC))
    {
        u1RadioId = 0;
    }
    else if ((u4RadioType == RFMGMT_RADIO_TYPEB) ||
             (u4RadioType == RFMGMT_RADIO_TYPEBG) ||
             (u4RadioType == RFMGMT_RADIO_TYPEBGN))
    {
        u1RadioId = RFMGMT_OFFSET_ONE;
    }
    else
    {
        u1RadioId = RFMGMT_OFFSET_TWO;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4Dot11RadioType = u4RadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bSHAStatus = OSIX_TRUE;

    if ((RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB))
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteNeighborDetails: RF MGMT DB "
                    "processing failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtDeleteNeighborDetails: RF MGMT DB "
                      "processing failed"));
        return RFMGMT_FAILURE;

    }
    u1ShaStatus = RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1SHAStatus;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    for (u2NeighId = 0; u2NeighId < RFMGMT_MAX_RADIOS; u2NeighId++)
    {
        if (gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
            u1IsNeighborPresent != 0)
        {
            if (u2WtpInternalId != u2NeighId)
            {
                if ((u1FlushAlgoTable == RFMGMT_FLUSH_DB_ONLY)
                    || (u1FlushAlgoTable == RFMGMT_FLUSH_IF_WLAN_DELETE))
                {
                    RfMgmtDB.unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                        RfMgmtFailedAPNeighborListDB.u4RadioIfIndex =
                        gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2WtpInternalId].u4RadioIfIndex;

                    MEMCPY (RfMgmtDB.unRfMgmtDB.
                            RfMgmtFailedAPNeighborTable.
                            RfMgmtFailedAPNeighborListDB.NeighborAPMac,
                            gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                            [u2NeighId].MacAddr, MAC_ADDR_LEN);

                    RfMgmtDB.unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                        RfMgmtFailedAPNeighborListDB.u4NeighRadioIfIndex =
                        gaDcaAlgorithm[u1RadioId][u2NeighId]
                        [u2NeighId].u4RadioIfIndex;

                    RfMgmtDB.unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                        RfMgmtFailedAPNeighborListDB.i2Rssi =
                        gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2NeighId].i2Rssi;

                    if (RfMgmtProcessDBMsg (RFMGMT_CREATE_FAILED_AP_NEIGH_ENTRY,
                                            &RfMgmtDB) == RFMGMT_FAILURE)
                    {
                    }
                    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                        RfMgmtNeighborScanDB.u4RadioIfIndex =
                        gaDcaAlgorithm[u1RadioId][u2NeighId]
                        [u2NeighId].u4RadioIfIndex;

                    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
                            RfMgmtNeighborScanDB.NeighborAPMac,
                            gaDcaAlgorithm[u1RadioId][u2NeighId]
                            [u2WtpInternalId].MacAddr, sizeof (tMacAddr));

                    if (RfMgmtProcessDBMsg (RFMGMT_DESTROY_NEIGHBOR_SCAN_ENTRY,
                                            &RfMgmtDB) == RFMGMT_FAILURE)
                    {
                    }

                }
                MEMSET (&gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2NeighId], 0, sizeof (tDcaAlgorithm));
            }
        }
    }

    gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
        [u2WtpInternalId].u1ApActive = OSIX_FALSE;

    gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
        [u2WtpInternalId].u1NeighAPCount = 0;

    gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
        [u2WtpInternalId].u1IsNeighborPresent = OSIX_FALSE;

    pRfMgmtNextNeighborScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);

    while (pRfMgmtNextNeighborScanDB != NULL)
    {
        RfMgmtNextNeighborScanDB.u4RadioIfIndex =
            pRfMgmtNextNeighborScanDB->u4RadioIfIndex;
        RfMgmtNextNeighborScanDB.u2ScannedChannel =
            pRfMgmtNextNeighborScanDB->u2ScannedChannel;
        MEMCPY (RfMgmtNextNeighborScanDB.NeighborAPMac,
                pRfMgmtNextNeighborScanDB->NeighborAPMac, MAC_ADDR_LEN);
        if (pRfMgmtNextNeighborScanDB->u4RadioIfIndex == u4RadioIfIndex)
        {

            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtNeighborAPScanDB, pRfMgmtNextNeighborScanDB);
            MemReleaseMemBlock (RFMGMT_NEIGH_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNextNeighborScanDB);

            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);
        }
        else
        {
            pRfMgmtNextNeighborScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtNeighborAPScanDB,
                               (tRBElem *) & RfMgmtNextNeighborScanDB, NULL);
        }

    }

    for (u2NeighId = 0; u2NeighId < RFMGMT_MAX_RADIOS; u2NeighId++)
    {
        if (u1FlushAlgoTable == 0)
        {
            gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId].
                u1IsNeighborPresent = OSIX_FALSE;
        }
        else
        {
            if (u1ShaStatus == RFMGMT_SHA_ENABLE)
            {
                RfMgmtDB.unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                    RfMgmtFailedAPNeighborListDB.u4RadioIfIndex =
                    gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                    [u2WtpInternalId].u4RadioIfIndex;

                MEMCPY (RfMgmtDB.unRfMgmtDB.
                        RfMgmtFailedAPNeighborTable.
                        RfMgmtFailedAPNeighborListDB.NeighborAPMac,
                        gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                        [u2NeighId].MacAddr, MAC_ADDR_LEN);
                if ((gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                     [u2NeighId].u1IsNeighborPresent == OSIX_TRUE)
                    && (u2WtpInternalId != u2NeighId))
                {
                    if (RfMgmtProcessDBMsg (RFMGMT_GET_FAILED_AP_NEIGH_ENTRY,
                                            &RfMgmtDB) == RFMGMT_FAILURE)
                    {
                        RfMgmtDB.unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                            RfMgmtFailedAPNeighborListDB.u4NeighRadioIfIndex =
                            gaDcaAlgorithm[u1RadioId][u2NeighId]
                            [u2NeighId].u4RadioIfIndex;

                        RfMgmtDB.unRfMgmtDB.RfMgmtFailedAPNeighborTable.
                            RfMgmtFailedAPNeighborListDB.i2Rssi =
                            gaDcaAlgorithm[u1RadioId][u2WtpInternalId]
                            [u2NeighId].i2Rssi;

                        if (RfMgmtProcessDBMsg
                            (RFMGMT_CREATE_FAILED_AP_NEIGH_ENTRY,
                             &RfMgmtDB) == RFMGMT_FAILURE)
                        {
                        }
                    }
                }

            }
            if (u2WtpInternalId != u2NeighId)
            {
                MEMSET (&gaDcaAlgorithm[u1RadioId][u2WtpInternalId][u2NeighId],
                        0, sizeof (tDcaAlgorithm));
            }
        }
    }

    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        if (u2WtpInternalId != u2WtpBaseId)
        {
            MEMSET (&gaDcaAlgorithm[u1RadioId][u2WtpBaseId]
                    [u2WtpInternalId], 0, sizeof (tDcaAlgorithm));
        }
    }

    for (u2WtpBaseId = 0; u2WtpBaseId < RFMGMT_MAX_RADIOS; u2WtpBaseId++)
    {
        gaDcaAlgorithm[u1RadioId][u2WtpBaseId][u2WtpInternalId].
            u1IsNeighborPresent = OSIX_FALSE;
    }

    /* If required invoke DCA Algorithm here */
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;

}

/*****************************************************************************/
/* Function     : RfMgmtDeleteClientDetails                                  */
/*                                                                           */
/* Description  : This function will destroy the client infomarion for the   */
/*                given radio ifindex                                        */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteClientDetails (UINT4 u4RadioIfIndex)
{
    tRfMgmtClientScanDB RfMgmtClientScanDB;
    tRfMgmtClientScanDB RfMgmtNextClientScanDB;

    tRfMgmtClientScanDB *pRfMgmtNextClientScanDB = NULL;

    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    MEMSET (&RfMgmtNextClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    RFMGMT_FN_ENTRY ();

    /* Get all the CLIENT information for the radio index 
       and flush the entries in the DB */

    pRfMgmtNextClientScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                       (tRBElem *) & RfMgmtNextClientScanDB, NULL);

    while (pRfMgmtNextClientScanDB != NULL)
    {
        RfMgmtNextClientScanDB.u4RadioIfIndex =
            pRfMgmtNextClientScanDB->u4RadioIfIndex;
        MEMCPY (RfMgmtNextClientScanDB.ClientMacAddress,
                pRfMgmtNextClientScanDB->ClientMacAddress, MAC_ADDR_LEN);
        if (pRfMgmtNextClientScanDB->u4RadioIfIndex == u4RadioIfIndex)
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtClientScanDB, pRfMgmtNextClientScanDB);
            MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                (UINT1 *) pRfMgmtNextClientScanDB);

            pRfMgmtNextClientScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                               (tRBElem *) & RfMgmtNextClientScanDB, NULL);
        }
        else
        {
            pRfMgmtNextClientScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                               (tRBElem *) & RfMgmtNextClientScanDB, NULL);
        }

    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteTpcInfo                                        */
/*                                                                           */
/* Description  : This function will destroy the TPC   information for the   */
/*                given radio ifindex                                        */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteTpcInfo (UINT4 u4RadioIfIndex)
{
    tRfMgmtDot11hTpcInfoDB RfMgmtDot11hTpcInfoDB;
    tRfMgmtDot11hTpcInfoDB RfMgmtNextDot11hTpcInfoDB;
    tRfMgmtDot11hTpcInfoDB *pRfMgmtNextDot11hTpcInfoDB = NULL;

    MEMSET (&RfMgmtDot11hTpcInfoDB, 0, sizeof (tRfMgmtDot11hTpcInfoDB));
    MEMSET (&RfMgmtNextDot11hTpcInfoDB, 0, sizeof (tRfMgmtDot11hTpcInfoDB));
    RFMGMT_FN_ENTRY ();

    /* Get all the CLIENT information for the radio index 
       and flush the entries in the DB */

    pRfMgmtNextDot11hTpcInfoDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                       (tRBElem *) & RfMgmtNextDot11hTpcInfoDB, NULL);

    while (pRfMgmtNextDot11hTpcInfoDB != NULL)
    {
        RfMgmtNextDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex =
            pRfMgmtNextDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex;
        MEMCPY (RfMgmtNextDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
                pRfMgmtNextDot11hTpcInfoDB->RfMgmt11hStationMacAddress,
                MAC_ADDR_LEN);
        if (pRfMgmtNextDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex ==
            u4RadioIfIndex)
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtDot11hTpcInfoDB, pRfMgmtNextDot11hTpcInfoDB);
            MemReleaseMemBlock (RFMGMT_TPC_INFO_DB_POOLID,
                                (UINT1 *) pRfMgmtNextDot11hTpcInfoDB);

            pRfMgmtNextDot11hTpcInfoDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtDot11hTpcInfoDB,
                               (tRBElem *) & RfMgmtNextDot11hTpcInfoDB, NULL);
        }
        else
        {
            pRfMgmtNextDot11hTpcInfoDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtDot11hTpcInfoDB,
                               (tRBElem *) & RfMgmtNextDot11hTpcInfoDB, NULL);
        }

    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteDfsInfo                                        */
/*                                                                           */
/* Description  : This function will destroy the DFS information for the   */
/*                given radio ifindex                                        */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteDfsInfo (UINT4 u4RadioIfIndex)
{
    tRfMgmtDot11hDfsInfoDB RfMgmtDot11hDfsInfoDB;
    tRfMgmtDot11hDfsInfoDB RfMgmtNextDot11hDfsInfoDB;
    tRfMgmtDot11hDfsInfoDB *pRfMgmtNextDot11hDfsInfoDB = NULL;

    MEMSET (&RfMgmtDot11hDfsInfoDB, 0, sizeof (tRfMgmtDot11hDfsInfoDB));
    MEMSET (&RfMgmtNextDot11hDfsInfoDB, 0, sizeof (tRfMgmtDot11hDfsInfoDB));
    RFMGMT_FN_ENTRY ();
    pRfMgmtNextDot11hDfsInfoDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                       (tRBElem *) & RfMgmtNextDot11hDfsInfoDB, NULL);

    while (pRfMgmtNextDot11hDfsInfoDB != NULL)
    {
        RfMgmtNextDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex =
            pRfMgmtNextDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex;
        MEMCPY (RfMgmtNextDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
                pRfMgmtNextDot11hDfsInfoDB->RfMgmt11hStationMacAddress,
                MAC_ADDR_LEN);
        if (pRfMgmtNextDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex ==
            u4RadioIfIndex)
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtDot11hDfsInfoDB, pRfMgmtNextDot11hDfsInfoDB);
            MemReleaseMemBlock (RFMGMT_DFS_INFO_DB_POOLID,
                                (UINT1 *) pRfMgmtNextDot11hDfsInfoDB);

            pRfMgmtNextDot11hDfsInfoDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtDot11hDfsInfoDB,
                               (tRBElem *) & RfMgmtNextDot11hDfsInfoDB, NULL);
        }
        else
        {
            pRfMgmtNextDot11hDfsInfoDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtDot11hDfsInfoDB,
                               (tRBElem *) & RfMgmtNextDot11hDfsInfoDB, NULL);
        }

    }

    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfMgmtDeleteClientDBforWlanId                              */
/*                                                                           */
/* Description  : This function will destroy the client information for the  */
/*                snr scan for a wlan id is diabled                          */
/*                                                                           */
/* Input        : WlanId                                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfMgmtDeleteClientDBforWlanId (UINT1 u1WlanId, UINT4 u4RadioIfIndex)
{
    tRfMgmtClientScanDB RfMgmtClientScanDB;
    tRfMgmtClientScanDB RfMgmtNextClientScanDB;
    tWssifauthDBMsgStruct *pStaMsgStruct = NULL;
    tWssWlanDB         *pWssWlanDB;

    tRfMgmtClientScanDB *pRfMgmtNextClientScanDB = NULL;

    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    MEMSET (&RfMgmtNextClientScanDB, 0, sizeof (tRfMgmtClientScanDB));
    RFMGMT_FN_ENTRY ();

    pStaMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pStaMsgStruct == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtDeleteClientDBforWlanId:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return RFMGMT_FAILURE;
    }

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessClientScanMessage:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
        return OSIX_FAILURE;
    }
    /* Get all the CLIENT information for the radio index 
       and flush the entries in the DB */

    pRfMgmtNextClientScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                       (tRBElem *) & RfMgmtNextClientScanDB, NULL);

    while (pRfMgmtNextClientScanDB != NULL)
    {
        RfMgmtNextClientScanDB.u4RadioIfIndex =
            pRfMgmtNextClientScanDB->u4RadioIfIndex;
        MEMCPY (RfMgmtNextClientScanDB.ClientMacAddress,
                pRfMgmtNextClientScanDB->ClientMacAddress, MAC_ADDR_LEN);
        if (pRfMgmtNextClientScanDB->u4RadioIfIndex == u4RadioIfIndex)
        {
            MEMCPY (pStaMsgStruct->WssIfAuthStateDB.stationMacAddress,
                    pRfMgmtNextClientScanDB->ClientMacAddress, MAC_ADDR_LEN);

            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                          pStaMsgStruct) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtDeleteClientDBforWlanId "
                            "Entry not found in station DB. Ignore entry\n");
                pRfMgmtNextClientScanDB =
                    RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                   RfMgmtClientScanDB,
                                   (tRBElem *) & RfMgmtNextClientScanDB, NULL);
                continue;
            }

            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex = pStaMsgStruct->
                WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "RfMgmtProcessClientScanMessage: "
                            "Not found in station DB. Ignore entry\n");
                pRfMgmtNextClientScanDB =
                    RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                   RfMgmtClientScanDB,
                                   (tRBElem *) & RfMgmtNextClientScanDB, NULL);
                continue;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u1WlanId == u1WlanId)
            {
                RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                           RfMgmtClientScanDB, pRfMgmtNextClientScanDB);
                MemReleaseMemBlock (RFMGMT_CLIENT_SCAN_DB_POOLID,
                                    (UINT1 *) pRfMgmtNextClientScanDB);
            }

            pRfMgmtNextClientScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                               (tRBElem *) & RfMgmtNextClientScanDB, NULL);
        }
        else
        {
            pRfMgmtNextClientScanDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                               (tRBElem *) & RfMgmtNextClientScanDB, NULL);
        }

    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pStaMsgStruct);
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfmgmtDeleteFailedAPDB                                     */
/*                                                                           */
/* Description  : This function will destroy the client infomarion for the   */
/*                given radio ifindex                                        */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfmgmtDeleteFailedAPDB (UINT4 u4RadioIfIndex)
{
    tRfMgmtFailedAPNeighborListDB RfMgmtFailedAPNeighborListDB;
    tRfMgmtFailedAPNeighborListDB RfMgmtNextFailedAPNeighborListDB;

    tRfMgmtFailedAPNeighborListDB *pRfMgmtFailedAPNeighborListDB = NULL;

    MEMSET (&RfMgmtFailedAPNeighborListDB, 0,
            sizeof (tRfMgmtFailedAPNeighborListDB));
    MEMSET (&RfMgmtNextFailedAPNeighborListDB, 0,
            sizeof (tRfMgmtFailedAPNeighborListDB));

    /* Get all the Failed AP information for the radio index 
       and flush the entries in the DB */
    pRfMgmtFailedAPNeighborListDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                       (tRBElem *) & RfMgmtNextFailedAPNeighborListDB, NULL);
#if 0
    pRfMgmtFailedAPNeighborListDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB);
#endif
    while (pRfMgmtFailedAPNeighborListDB != NULL)
    {
        RfMgmtNextFailedAPNeighborListDB.u4RadioIfIndex =
            pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex;
        MEMCPY (RfMgmtNextFailedAPNeighborListDB.NeighborAPMac,
                pRfMgmtFailedAPNeighborListDB->NeighborAPMac, MAC_ADDR_LEN);
        if (pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex == u4RadioIfIndex)
        {
            RBTreeRem (gRfMgmtGlobals.RfMgmtGlbMib.
                       RfMgmtFailedAPNeighborDB, pRfMgmtFailedAPNeighborListDB);
            MemReleaseMemBlock (RFMGMT_FAILED_AP_NEIGH_DB_POOLID,
                                (UINT1 *) pRfMgmtFailedAPNeighborListDB);

            pRfMgmtFailedAPNeighborListDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtFailedAPNeighborDB,
                               (tRBElem *) & RfMgmtNextFailedAPNeighborListDB,
                               NULL);
        }
        else
        {
            pRfMgmtFailedAPNeighborListDB =
                RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                               RfMgmtFailedAPNeighborDB,
                               (tRBElem *) & RfMgmtNextFailedAPNeighborListDB,
                               NULL);
        }

    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/*****************************************************************************/
/* Function     : RfmgmtGetPowerFromPowerLevel                               */
/*                                                                           */
/* Description  : This function returns the power from power level           */
/*                                                                           */
/*                                                                           */
/* Input        : u4RadioIfIndex Radio IF Index                              */
/*                PowerLevel                                                 */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
RfmgmtGetPowerFromPowerLevel (UINT4 u4RadioIfIndex, UINT2 u2TxPowerLevel,
                              INT2 *pi2CurrentTxPower)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2MaxTxPowerLevel = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMultiDomainInfo = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtProcessNeighConfig: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtProcessNeighConfig: RadioIf DB access failed"));
        return RFMGMT_FAILURE;
    }

    u2MaxTxPowerLevel = RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;

    *pi2CurrentTxPower = (INT2) (((u2MaxTxPowerLevel -
                                   (RADIOIF_POWER_MULTIPLIER *
                                    u2TxPowerLevel)) +
                                  RADIOIF_POWER_MULTIPLIER));

    return RFMGMT_SUCCESS;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : RfmgmtMacToStr                                      *
 *                                                                         *
 *     Description   : This function converts the given mac address        *
 *                     in to a string of form aa:aa:aa:aa:aa:aa            *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.(The string must be of length   *
 *                                  21 bytes minimum)                      *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
RfmgmtMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if (!(pMacAddr) || !(pu1Temp))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), "   ");

}

#ifdef ROGUEAP_WANTED
/*****************************************************************************/
/* Function     : RfMgmtRogueManualDBCreate                                  */
/*                                                                           */
/* Description  : This function creates RfMgmtRogueApDB                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtRogueManualDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();

    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtRogueManualDB, RogueManualDB);

    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueManualDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RogueManualCompareBssidDBRBTree)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfMgmtRogueApScanDBCreate  failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRogueApScanDBCreate  failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/********************************************************************************
 *  Function Name   : RogueManualCompareBssidDBRBTree                           *
 *                                                                              *
 *  Description     : This function is used to Compare the BSSID before insert  *
 *                a new entry to RB tree                       *
 *                  :                                                           * 
 *  Input(s)        : tRBElem *                                                 *
 *                                                                              *
 *  Output(s)       : None                                                      *
 *                  :                                                           *
 *  Returns         : OSIX_FAILURE on failure                                   * 
 *                    OSIX_SUCCESS on success                                   *
 ********************************************************************************/

INT4
RogueManualCompareBssidDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtRogueManualDB *pNode1 = e1;
    tRfMgmtRogueManualDB *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->u1RogueApBSSID,
                       &pNode2->u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
    if (i4CmpVal > 0)
    {
        return ROGUEAP_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return ROGUEAP_RB_LESS;
    }
    return ROGUEAP_RB_EQUAL;
}

/*****************************************************************************/
/* Function     : RfMgmtRogueRuleDBCreate                                    */
/*                                                                           */
/* Description  : This function creates RfMgmtRogueApDB                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtRogueRuleDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();

    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtRogueRuleDB, RogueRuleDB);

    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueRuleDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtRogueRuleCompareBssidDBRBTree)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtRogueRuleDB  failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRogueRuleDB  failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/****************************************************************************
 *  Function Name   : RfMgmtRogueRuleCompareBssidDBRBTree                   *
 *                                                                          *
 *  Description     : This function is used to Compare the BSSID before     * 
 *                insert a new entry to RB tree                         *
 *                  :                                                       *
 *  Input(s)        : tRBElem *                                             *                            
 *                                                                          *                            
 *  Output(s)       : None                                                  *                            
 *                  :                                                       *                            
 *  Returns         : OSIX_FAILURE on failure                               *                            
 *                    OSIX_SUCCESS on success                               *                            
 ***************************************************************************/

INT4
RfMgmtRogueRuleCompareBssidDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtRogueRuleDB *pNode1 = e1;
    tRfMgmtRogueRuleDB *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->u1RogueApRuleName,
                       &pNode2->u1RogueApRuleName, WSSMAC_MAX_SSID_LEN);
    if (i4CmpVal > 0)
    {
        return ROGUEAP_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return ROGUEAP_RB_LESS;
    }
    return ROGUEAP_RB_EQUAL;
}

/*****************************************************************************/
/* Function     : RfMgmtRogueApDBCreate                                      */
/*                                                                           */
/* Description  : This function creates RfMgmtRogueApDB                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

UINT1
RfMgmtRogueApDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    RFMGMT_FN_ENTRY ();

    u4RBNodeOffset = FSAP_OFFSETOF (tRfMgmtRogueApDB, RogueApDB);

    if ((gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtRogueApDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               RfMgmtRogueApCompareBssidDBRBTree)) == NULL)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtRogueApDB  failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfMgmtRogueApDB  failed "));
        return RFMGMT_FAILURE;
    }
    RFMGMT_FN_EXIT ();
    return RFMGMT_SUCCESS;
}

/******************************************************************************
 *  Function Name   : RfMgmtRogueApCompareBssidDBRBTree                       *
 *                                                                            *
 *  Description     : This function is used to Compare the BSSID before       * 
 *                    insert a new entry to RB tree                           *
 *                  :                                                         *
 *  Input(s)        : tRBElem *                                               *                          
 *                                                                            *                          
 *  Output(s)       : None                                                    *                          
 *                  :                                                         *                          
 *  Returns         : OSIX_FAILURE on failure                                 *                          
 *                    OSIX_SUCCESS on success                                 *                          
 ******************************************************************************/

INT4
RfMgmtRogueApCompareBssidDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tRfMgmtRogueApDB   *pNode1 = e1;
    tRfMgmtRogueApDB   *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->u1RogueApBSSID,
                       &pNode2->u1RogueApBSSID, WSSMAC_MAC_ADDR_LEN);
    if (i4CmpVal > 0)
    {
        return ROGUEAP_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return ROGUEAP_RB_LESS;
    }
    return ROGUEAP_RB_EQUAL;
}

#endif

/***************************************************************************
 *                                                                         *
 *     Function Name : RfmgmtGetBaseRadioIfIndex                           *
 *                                                                         *
 *     Description   : This function returns the base radio ifindex for    *
 *                     the given radio ifindex of the same WTP             *
 *                                                                         *
 *     Input(s)      : u4RadioIfIndex : Input radio ifindex                *
 *                                                                         *
 *     Output(s)     : pu4RadioIfIndex : Base radio ifindex pointer        *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/
UINT1
RfmgmtGetBaseRadioIfIndex (UINT4 u4RadioIfIndex, UINT4 *pu4RadioIfIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "RfmgmtGetBaseRadioIfIndex: RadioIf DB access failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "RfmgmtGetBaseRadioIfIndex: RadioIf DB access failed\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return RFMGMT_FAILURE;
    }

    /* If the input index is the base index the copy the input to output
     * variable */
    if (RadioIfGetDB.RadioIfGetAllDB.u1RadioId == 1)
    {
        *pu4RadioIfIndex = u4RadioIfIndex;
    }
    else
    {
        /* Get base radio ifindex from CAPWAP module */
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId = 1;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfMgmtProcessNeighborMessage :"
                        "Get radio Index failed\r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                          "Get radio Index failed\n"));
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return RFMGMT_FAILURE;
        }

        *pu4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return RFMGMT_SUCCESS;
}
#endif
