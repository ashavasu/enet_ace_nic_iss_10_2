/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rfmgmtlw.c,v 1.5 2017/12/12 13:28:31 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "rfminc.h"
#include  "rfmcli.h"
#include  "wsscfglwg.h"
#include  "radioifextn.h"
#include  "radioifproto.h"

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmDebugOption
Input       :  The Indices
The Object 
retValFsRrmDebugOption
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmDebugOption (INT4 *pi4RetValFsRrmDebugOption)
{
    *pi4RetValFsRrmDebugOption = (INT4) gu4RfMgmtTrace;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmDebugOption
Input       :  The Indices
The Object 
setValFsRrmDebugOption
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmDebugOption (INT4 i4SetValFsRrmDebugOption)
{
    gu4RfMgmtTrace = (UINT4) i4SetValFsRrmDebugOption;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsRrmDebugOption
Input       :  The Indices
The Object 
testValFsRrmDebugOption
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmDebugOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsRrmDebugOption)
{
    INT4                i4TempDebug = 0;

    if (i4TestValFsRrmDebugOption != RFMGMT_ALL_TRC)
    {
        i4TempDebug = RFMGMT_MGMT_TRC | RFMGMT_INIT_TRC |
            RFMGMT_ENTRY_TRC | RFMGMT_EXIT_TRC | RFMGMT_FAILURE_TRC |
            RFMGMT_DCA_TRC | RFMGMT_TPC_TRC | RFMGMT_INFO_TRC |
            RFMGMT_SHA_TRC | RFMGMT_DPA_TRC | RFMGMT_CRITICAL_TRC
            | RFMGMT_11H_TPC_TRC;
        if ((i4TempDebug & i4TestValFsRrmDebugOption) !=
            i4TestValFsRrmDebugOption)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_INVALID_VALUE);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhDepv2FsRrmDebugOption
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmDebugOption (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrmConfigTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmConfigTable
Input       :  The Indices
FsRrmRadioType
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmConfigTable (INT4 i4FsRrmRadioType)
{
    UNUSED_PARAM (i4FsRrmRadioType);
    return SNMP_SUCCESS;
}

#ifdef WLC_WANTED
/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmConfigTable
Input       :  The Indices
FsRrmRadioType
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrmConfigTable (INT4 *pi4FsRrmRadioType)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;

    pRfMgmtAutoRfProfileDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB);

    if (pRfMgmtAutoRfProfileDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsRrmRadioType = (INT4) pRfMgmtAutoRfProfileDB->u4Dot11RadioType;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmConfigTable
Input       :  The Indices
FsRrmRadioType
nextFsRrmRadioType
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmConfigTable (INT4 i4FsRrmRadioType,
                                 INT4 *pi4NextFsRrmRadioType)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;
    tRfMgmtAutoRfProfileDB RfMgmtAutoRfProfileDB;

    MEMSET (&RfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));

    RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;

    pRfMgmtAutoRfProfileDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                       &RfMgmtAutoRfProfileDB, NULL);

    if (pRfMgmtAutoRfProfileDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsRrmRadioType = (INT4) pRfMgmtAutoRfProfileDB->u4Dot11RadioType;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmDcaMode
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmDcaMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmDcaMode (INT4 i4FsRrmRadioType, INT4 *pi4RetValFsRrmDcaMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaMode =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmDcaMode =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1DcaMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmDcaSelectionMode
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmDcaSelectionMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmDcaSelectionMode (INT4 i4FsRrmRadioType,
                             INT4 *pi4RetValFsRrmDcaSelectionMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaSelection =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmDcaSelectionMode =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1DcaSelection;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTpcMode
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmTpcMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTpcMode (INT4 i4FsRrmRadioType, INT4 *pi4RetValFsRrmTpcMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcMode =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmTpcMode =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1TpcMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTpcSelectionMode
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmTpcSelectionMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTpcSelectionMode (INT4 i4FsRrmRadioType,
                             INT4 *pi4RetValFsRrmTpcSelectionMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcSelection =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmTpcSelectionMode =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1TpcSelection;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTpcUpdate
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmTpcUpdate
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTpcUpdate (INT4 i4FsRrmRadioType, INT4 *pi4RetValFsRrmTpcUpdate)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcUpdatePower =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmTpcUpdate =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1TpcUpdatePower;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmDcaInterval
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmDcaInterval
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmDcaInterval (INT4 i4FsRrmRadioType, UINT4 *pu4RetValFsRrmDcaInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaInterval =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmDcaInterval =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4DcaInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmDcaSensitivity
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmDcaSensitivity
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmDcaSensitivity (INT4 i4FsRrmRadioType,
                           INT4 *pi4RetValFsRrmDcaSensitivity)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaSensitivity =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmDcaSensitivity =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1DcaSensitivity;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAllowedChannels
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmAllowedChannels
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAllowedChannels (INT4 i4FsRrmRadioType,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsRrmAllowedChannels)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bAllowedChannelList = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4FsRrmRadioType == RFMGMT_RADIO_TYPEA)
    {
        MEMCPY (pRetValFsRrmAllowedChannels->pu1_OctetList,
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1AllowedChannelList, RFMGMT_MAX_CHANNELA);
        pRetValFsRrmAllowedChannels->i4_Length = RFMGMT_MAX_CHANNELA;
    }
    else
    {
        MEMCPY (pRetValFsRrmAllowedChannels->pu1_OctetList,
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1AllowedChannelList, RFMGMT_MAX_CHANNELB);
        pRetValFsRrmAllowedChannels->i4_Length = RFMGMT_MAX_CHANNELB;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmUnusedChannels
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmUnusedChannels
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmUnusedChannels (INT4 i4FsRrmRadioType,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsRrmUnusedChannels)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bUnUsedChannelList = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4FsRrmRadioType == RFMGMT_RADIO_TYPEA)
    {
        MEMCPY (pRetValFsRrmUnusedChannels->pu1_OctetList,
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1UnUsedChannelList, RFMGMT_MAX_CHANNELA);
        pRetValFsRrmUnusedChannels->i4_Length = RFMGMT_MAX_CHANNELA;
    }
    else
    {
        MEMCPY (pRetValFsRrmUnusedChannels->pu1_OctetList,
                RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1UnUsedChannelList, RFMGMT_MAX_CHANNELB);
        pRetValFsRrmUnusedChannels->i4_Length = RFMGMT_MAX_CHANNELB;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmUpdateChannel
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmUpdateChannel
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmUpdateChannel (INT4 i4FsRrmRadioType,
                          INT4 *pi4RetValFsRrmUpdateChannel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaUpdateChannel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmUpdateChannel =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1DcaUpdateChannel;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmLastUpdatedTime
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmLastUpdatedTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmLastUpdatedTime (INT4 i4FsRrmRadioType,
                            UINT4 *pu4RetValFsRrmLastUpdatedTime)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaLastUpdatedTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmLastUpdatedTime =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4DcaLastUpdatedTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmRSSIThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmRSSIThreshold
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmRSSIThreshold (INT4 i4FsRrmRadioType,
                          INT4 *pi4RetValFsRrmRSSIThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRssiThreshold =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmRSSIThreshold =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        i2RssiThreshold;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsRrmPowerThreshold
Input       :  The Indices
FsRrmRadioType

The Object
retValFsRrmPowerThreshold
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmPowerThreshold (INT4 i4FsRrmRadioType,
                           INT4 *pi4RetValFsRrmPowerThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bPowerThreshold =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmPowerThreshold =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        i2PowerThreshold;

    return SNMP_SUCCESS;

}

 /****************************************************************************
  * Function    :  nmhGetFsRrm11hTpcStatus
  * Input       :  The Indices
  *                FsRrmRadioType
  *                 The Object
  *                 retValFsRrm11hTpcStatus
  * Output      :  The Get Low Lev Routine Take the Indices &
  *                store the Value requested in the Return val.
  * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhGetFsRrm11hTpcStatus (INT4 i4FsRrmRadioType,
                         INT4 *pi4RetValFsRrm11hTpcStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hTpcStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hTpcStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhGetFsRrm11hTpcLastRun
 * Input       :  The Indices
 *                  FsRrmRadioType
 *                  The Object
 *                  retValFsRrm11hTpcLastRun
 *
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrm11hTpcLastRun (INT4 i4FsRrmRadioType,
                          UINT4 *pu4RetValFsRrm11hTpcLastRun)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcLastRun = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hTpcLastRun =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4RfMgmt11hTpcLastRun;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhGetFsRrm11hDfsLastRun
 * Input       :  The Indices
 *                  FsRrmRadioType
 *                  The Object
 *                  retValFsRrm11hDfsLastRun
 *
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrm11hDfsLastRun (INT4 i4FsRrmRadioType,
                          UINT4 *pu4RetValFsRrm11hDfsLastRun)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsLastRun = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hDfsLastRun =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4RfMgmt11hDfsLastRun;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hTpcInterval
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                retValFsRrm11hTpcInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hTpcInterval (INT4 i4FsRrmRadioType,
                           UINT4 *pu4RetValFsRrm11hTpcInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcInterval = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hTpcInterval =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4RfMgmt11hTpcInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hMinLinkThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                retValFsRrm11hMinLinkThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hMinLinkThreshold (INT4 i4FsRrmRadioType,
                                UINT4 *pu4RetValFsRrm11hMinLinkThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bMinLinkThreshold = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hMinLinkThreshold =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1MinLinkThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hMaxLinkThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                retValFsRrm11hMaxLinkThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hMaxLinkThreshold (INT4 i4FsRrmRadioType,
                                UINT4 *pu4RetValFsRrm11hMaxLinkThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bMaxLinkThreshold = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hMaxLinkThreshold =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1MaxLinkThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hStaCountThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                retValFsRrm11hStaCountThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hStaCountThreshold (INT4 i4FsRrmRadioType,
                                 UINT4 *pu4RetValFsRrm11hStaCountThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bStaCountThreshold = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hStaCountThreshold =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1StaCountThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmClientThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmClientThreshold
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmClientThreshold (INT4 i4FsRrmRadioType,
                            UINT4 *pu4RetValFsRrmClientThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bClientThreshold =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmClientThreshold =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1ClientThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTpcInterval
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmTpcInterval
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTpcInterval (INT4 i4FsRrmRadioType, UINT4 *pu4RetValFsRrmTpcInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcInterval =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmTpcInterval =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4TpcInterval;

    return SNMP_SUCCESS;
}

 /****************************************************************************
 * Function    :  nmhGetFsRrm11hDfsStatus
 * Input       :  The Indices
 *                FsRrmRadioType
 *                The Object
 *                retValFsRrm11hDfsStatus
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsRrm11hDfsStatus (INT4 i4FsRrmRadioType,
                         INT4 *pi4RetValFsRrm11hDfsStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hDfsStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hDfsStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrmConsiderExternalAPs
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                retValFsRrmConsiderExternalAPs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrmConsiderExternalAPs (INT4 i4FsRrmRadioType,
                                INT4 *pi4RetValFsRrmConsiderExternalAPs)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bConsiderExternalAPs = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmConsiderExternalAPs =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1ConsiderExternalAPs;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrmClearNeighborInfo
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                retValFsRrmClearNeighborInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrmClearNeighborInfo (INT4 i4FsRrmRadioType,
                              INT4 *pi4RetValFsRrmClearNeighborInfo)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bClearNeighborDetails = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmClearNeighborInfo =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1ClearNeighborDetails;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsRrmNeighborCountThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmNeighborCountThreshold
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmNeighborCountThreshold (INT4 i4FsRrmRadioType,
                                   UINT4 *pu4RetValFsRrmNeighborCountThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bNeighborCountThreshold = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmNeighborCountThreshold =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1NeighborCountThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhGetFsRrm11hDfsInterval
 * Input       :  The Indices
 * FsRrmRadioType
 *
 * The Object
 * retValFsRrmDfsInterval
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *  ****************************************************************************/
INT1
nmhGetFsRrm11hDfsInterval (INT4 i4FsRrmRadioType,
                           UINT4 *pu4RetValFsRrmDfsInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDfsInterval =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmDfsInterval =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4DfsInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSNRThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmSNRThreshold
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSNRThreshold (INT4 i4FsRrmRadioType,
                         INT4 *pi4RetValFsRrmSNRThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bSNRThreshold =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmSNRThreshold =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        i2SNRThreshold;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTpcLastUpdatedTime
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmTpcLastUpdatedTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTpcLastUpdatedTime (INT4 i4FsRrmRadioType,
                               UINT4 *pu4RetValFsRrmTpcLastUpdatedTime)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bTpcLastUpdatedTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmTpcLastUpdatedTime =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4TpcLastUpdatedTime;

    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
Function    :  nmhGetFsRrmChannelSwitchMsgStatus
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmChannelSwitchMsgStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmChannelSwitchMsgStatus (INT4 *pi4RetValFsRrmChannelSwitchMsgStatus)
{
#ifdef WLC_WANTED
    *pi4RetValFsRrmChannelSwitchMsgStatus = (INT4) gu4ChannelSwitchMsgStatus;
#else
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        SYS_DEF_MAX_ENET_INTERFACES + 1;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChSwitchStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmChannelSwitchMsgStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u1ChSwitchStatus;
#endif
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhDepv2FsRrmChannelSwitchMsgStatus
Input       :  The Indices
FsRrmRadioType
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmChannelSwitchMsgStatus (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef WLC_WANTED
/****************************************************************************
Function    :  nmhGetFsRrmRowStatus
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmRowStatus (INT4 i4FsRrmRadioType, INT4 *pi4RetValFsRrmRowStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRowStatus =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmRowStatus =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.i4RowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmDcaMode
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmDcaMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmDcaMode (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrmDcaMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaMode =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "In nmhSetFsRrmDcaMode :"
                    "RfMgmtProcessDBMsg :DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1DcaMode
        == (UINT1) i4SetValFsRrmDcaMode)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1DcaMode =
        (UINT1) i4SetValFsRrmDcaMode;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmDcaSelectionMode
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmDcaSelectionMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmDcaSelectionMode (INT4 i4FsRrmRadioType,
                             INT4 i4SetValFsRrmDcaSelectionMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaSelection =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "In nmhSetFsRrmDcaSelectionMode :"
                    "RfMgmtProcessDBMsg :DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaSelection ==
        (UINT1) i4SetValFsRrmDcaSelectionMode)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1DcaSelection =
        (UINT1) i4SetValFsRrmDcaSelectionMode;

    if (i4SetValFsRrmDcaSelectionMode == CLI_DCA_SELECTION_ONCE)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1DcaUpdateChannel =
            RFMGMT_DEF_DCA_UPDATE_CHANNEL;
    }
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmTpcMode
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmTpcMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmTpcMode (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrmTpcMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcMode =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1TpcMode ==
        (UINT1) i4SetValFsRrmTpcMode)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1TpcMode =
        (UINT1) i4SetValFsRrmTpcMode;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmTpcSelectionMode
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmTpcSelectionMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmTpcSelectionMode (INT4 i4FsRrmRadioType,
                             INT4 i4SetValFsRrmTpcSelectionMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcSelection =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1TpcSelection ==
        (UINT1) i4SetValFsRrmTpcSelectionMode)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1TpcSelection =
        (UINT1) i4SetValFsRrmTpcSelectionMode;

    if (i4SetValFsRrmTpcSelectionMode == CLI_TPC_SELECTION_ONCE)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
            RfMgmtAutoRfProfileDB.u1DcaUpdateChannel = RFMGMT_DEF_TPC_UPDATE;
    }
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmTpcUpdate
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmTpcUpdate
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmTpcUpdate (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrmTpcUpdate)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRowStatus =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcMode =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcSelection =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcUpdatePower =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1TpcUpdatePower = (UINT1) i4SetValFsRrmTpcUpdate;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RfMgmtRunTpcAlgorithm (&RfMgmtDB.unRfMgmtDB.
                           RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB);

    MEMSET (&RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB, 0,
            sizeof (tRfMgmtAutoRfIsSetDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcUpdatePower =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bTpcLastUpdatedTime = OSIX_TRUE;

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1TpcUpdatePower = RFMGMT_DEF_TPC_UPDATE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4TpcLastUpdatedTime = OsixGetSysUpTime ();

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmDcaInterval
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmDcaInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmDcaInterval (INT4 i4FsRrmRadioType, UINT4 u4SetValFsRrmDcaInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaInterval =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4DcaInterval == u4SetValFsRrmDcaInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u4DcaInterval =
        u4SetValFsRrmDcaInterval;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmDcaSensitivity
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmDcaSensitivity
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmDcaSensitivity (INT4 i4FsRrmRadioType,
                           INT4 i4SetValFsRrmDcaSensitivity)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaSensitivity =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaSensitivity ==
        (UINT1) i4SetValFsRrmDcaSensitivity)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaSensitivity =
        (UINT1) i4SetValFsRrmDcaSensitivity;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmAllowedChannels
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmAllowedChannels
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAllowedChannels (INT4 i4FsRrmRadioType,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsRrmAllowedChannels)
{
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ChannelToFill = 0, u4MaxChannels = 0;
    UINT1               u1Channel = 0;
    UINT1               u1RetVal = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bAllowedChannelList = OSIX_TRUE;

    if (i4FsRrmRadioType == RFMGMT_RADIO_TYPEA)
    {
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1AllowedChannelList,
                pSetValFsRrmAllowedChannels->pu1_OctetList,
                RFMGMT_MAX_CHANNELA);
    }
    else
    {
        u4MaxChannels = RFMGMT_MAX_CHANNELB;
        MEMSET (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
                RfMgmtAutoRfProfileDB.au1AllowedChannelList, 0, u4MaxChannels);

        for (u1Channel = 0; u1Channel < u4MaxChannels; u1Channel++)
        {
            u4ChannelToFill =
                pSetValFsRrmAllowedChannels->pu1_OctetList[u1Channel];
            if (u4ChannelToFill != 0)
            {
                RfMgmtDB.unRfMgmtDB.
                    RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                    au1AllowedChannelList[u4ChannelToFill - 1] =
                    (UINT1) u4ChannelToFill;
            }
        }
    }

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmUnusedChannels
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmUnusedChannels
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmUnusedChannels (INT4 i4FsRrmRadioType,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsRrmUnusedChannels)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bUnUsedChannelList = OSIX_TRUE;

    if (i4FsRrmRadioType == RFMGMT_RADIO_TYPEA)
    {
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1UnUsedChannelList, pSetValFsRrmUnusedChannels->pu1_OctetList,
                RFMGMT_MAX_CHANNELA);
    }
    else
    {
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
                au1UnUsedChannelList, pSetValFsRrmUnusedChannels->pu1_OctetList,
                RFMGMT_MAX_CHANNELB);
    }

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmUpdateChannel
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmUpdateChannel
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmUpdateChannel (INT4 i4FsRrmRadioType,
                          INT4 i4SetValFsRrmUpdateChannel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaUpdateChannel = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRssiThreshold =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bClientThreshold =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRowStatus =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaMode =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaSelection =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bApNeighborCount =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDcaSensitivity =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bAllowedChannelList = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bUnUsedChannelList = OSIX_TRUE;

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaUpdateChannel =
        (UINT1) i4SetValFsRrmUpdateChannel;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RfMgmtRunDcaAlgorithm (&RfMgmtDB.unRfMgmtDB.
                               RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB,
                               0) != RFMGMT_SUCCESS)
    {
    }

    MEMSET (&RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB, 0,
            sizeof (tRfMgmtAutoRfIsSetDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaUpdateChannel = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDcaLastUpdatedTime = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1DcaUpdateChannel =
        RFMGMT_DEF_DCA_UPDATE_CHANNEL;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4DcaLastUpdatedTime = OsixGetSysUpTime ();

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmRSSIThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmRSSIThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmRSSIThreshold (INT4 i4FsRrmRadioType,
                          INT4 i4SetValFsRrmRSSIThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRssiThreshold =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.i2RssiThreshold ==
        (INT2) i4SetValFsRrmRSSIThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.i2RssiThreshold =
        (INT2) i4SetValFsRrmRSSIThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (RfMgmtUpdategaDcaAlgorithm (i4FsRrmRadioType,
                                    (INT2) i4SetValFsRrmRSSIThreshold) ==
        RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmPowerThreshold
Input       :  The Indices
FsRrmRadioType

The Object
setValFsRrmPowerThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmPowerThreshold (INT4 i4FsRrmRadioType,
                           INT4 i4SetValFsRrmPowerThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bPowerThreshold =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.i2PowerThreshold ==
        (INT2) i4SetValFsRrmPowerThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.i2PowerThreshold =
        (INT2) i4SetValFsRrmPowerThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 * Function    :  nmhSetFsRrm11hTpcStatus
 * Input       :  The Indices
 *                FsRrmRadioType
 *                The Object
 *                setValFsRrm11hTpcStatus
 * Output      :  The Set Low Lev Routine Take the Indices &
 *                Sets the Value accordingly.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsRrm11hTpcStatus (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrm11hTpcStatus)
{
    UINT1               u1RetVal = 0;
    INT4                i4CurrentIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Fetch from DB failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1RfMgmt11hTpcStatus ==
        (UINT1) i4SetValFsRrm11hTpcStatus)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hTpcStatus = (UINT1) i4SetValFsRrm11hTpcStatus;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB) !=
        RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    u1RetVal = (UINT1) nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIfIndex);
    if (u1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    do
    {
        i4CurrentIfIndex = i4NextIfIndex;
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4RadioIfIndex = (UINT4) i4CurrentIfIndex;

        u1RetVal = RfMgmtProcessTpcSpectMgmt (&RfMgmtDB);
        if (u1RetVal == RFMGMT_FAILURE)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "RfMgmtProcessTpcSpectMgmt returns failure \r\n");
        }
    }
    while (nmhGetNextIndexFsRrmAPConfigTable (i4CurrentIfIndex, &i4NextIfIndex)
           == SNMP_SUCCESS);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hTpcInterval
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrm11hTpcInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hTpcInterval (INT4 i4FsRrmRadioType,
                           UINT4 u4SetValFsRrm11hTpcInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcInterval = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4RfMgmt11hTpcInterval ==
        u4SetValFsRrm11hTpcInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4RfMgmt11hTpcInterval = u4SetValFsRrm11hTpcInterval;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hMinLinkThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrm11hMinLinkThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hMinLinkThreshold (INT4 i4FsRrmRadioType,
                                UINT4 u4SetValFsRrm11hMinLinkThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bMinLinkThreshold = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB Fetch Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1MinLinkThreshold ==
        (UINT1) u4SetValFsRrm11hMinLinkThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1MinLinkThreshold = (UINT1) u4SetValFsRrm11hMinLinkThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRrm11hMaxLinkThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrm11hMaxLinkThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hMaxLinkThreshold (INT4 i4FsRrmRadioType,
                                UINT4 u4SetValFsRrm11hMaxLinkThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bMaxLinkThreshold = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB Fetch Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1MaxLinkThreshold ==
        (UINT1) u4SetValFsRrm11hMaxLinkThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1MaxLinkThreshold = (UINT1) u4SetValFsRrm11hMaxLinkThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hStaCountThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrm11hStaCountThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hStaCountThreshold (INT4 i4FsRrmRadioType,
                                 UINT4 u4SetValFsRrm11hStaCountThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bStaCountThreshold = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB Fetch Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1StaCountThreshold ==
        (UINT1) u4SetValFsRrm11hStaCountThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1StaCountThreshold = (UINT1) u4SetValFsRrm11hStaCountThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsRrmClientThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmClientThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmClientThreshold (INT4 i4FsRrmRadioType,
                            UINT4 u4SetValFsRrmClientThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bClientThreshold =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1ClientThreshold ==
        (UINT1) u4SetValFsRrmClientThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1ClientThreshold =
        (UINT1) u4SetValFsRrmClientThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmTpcInterval
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmTpcInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmTpcInterval (INT4 i4FsRrmRadioType, UINT4 u4SetValFsRrmTpcInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bTpcInterval =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4TpcInterval == u4SetValFsRrmTpcInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u4TpcInterval =
        u4SetValFsRrmTpcInterval;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhSetFsRrm11hDfsStatus
 * Input       :  The Indices
 *                FsRrmRadioType
 *                The Object
 *                setValFsRrm11hDfsStatus
 * Output      :  The Set Low Lev Routine Take the Indices &
 *                Sets the Value accordingly.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsRrm11hDfsStatus (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrm11hDfsStatus)
{
    UINT1               u1RetVal = 0;
    INT4                i4CurrentIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hDfsStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Fetch from DB failed\r\n");
        return SNMP_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1RfMgmt11hDfsStatus ==
        (UINT1) i4SetValFsRrm11hDfsStatus)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hDfsStatus = (UINT1) i4SetValFsRrm11hDfsStatus;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB) !=
        RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    u1RetVal = (UINT1) nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIfIndex);
    if (u1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    do
    {
        i4CurrentIfIndex = i4NextIfIndex;
        RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4RadioIfIndex = (UINT4) i4CurrentIfIndex;

        u1RetVal = RfMgmtProcessDfsParams (&RfMgmtDB);
        if (u1RetVal == RFMGMT_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    while (nmhGetNextIndexFsRrmAPConfigTable (i4CurrentIfIndex, &i4NextIfIndex)
           == SNMP_SUCCESS);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrmConsiderExternalAPs
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrmConsiderExternalAPs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrmConsiderExternalAPs (INT4 i4FsRrmRadioType,
                                INT4 i4SetValFsRrmConsiderExternalAPs)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bConsiderExternalAPs = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "DB updation Failed\n"));
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1ConsiderExternalAPs ==
        (UINT1) i4SetValFsRrmConsiderExternalAPs)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1ConsiderExternalAPs = (UINT1) i4SetValFsRrmConsiderExternalAPs;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u1RetVal = RfMgmtDeleteExternalNeighborDetails ((UINT4) i4FsRrmRadioType);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "External APs neighbor deletion failed\r\n");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrmClearNeighborInfo
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrmClearNeighborInfo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrmClearNeighborInfo (INT4 i4FsRrmRadioType,
                              INT4 i4SetValFsRrmClearNeighborInfo)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bClearNeighborDetails = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      "DB updation Failed\n"));
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1ClearNeighborDetails ==
        (UINT1) i4SetValFsRrmClearNeighborInfo)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1ClearNeighborDetails = (UINT1) i4SetValFsRrmClearNeighborInfo;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u1RetVal = RfMgmtDeleteRadiotypeNeighborInfo ((UINT4) i4FsRrmRadioType);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "APs neighbor deletion failed\r\n");
        return SNMP_FAILURE;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1ClearNeighborDetails = OSIX_FALSE;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrmNeighborCountThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                setValFsRrmNeighborCountThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrmNeighborCountThreshold (INT4 i4FsRrmRadioType,
                                   UINT4 u4SetValFsRrmNeighborCountThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bNeighborCountThreshold = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Get DB  Failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId, "Get DB Failed\n"));
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1NeighborCountThreshold ==
        (UINT1) u4SetValFsRrmNeighborCountThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1NeighborCountThreshold = (UINT1) u4SetValFsRrmNeighborCountThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*************************************************************************
Function    :  nmhSetFsRrm11hDfsInterval
Input       :  The Indices
FsRrmRadioType

The Object
setValFsRrmDfsInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**************************************************************************/
INT1
nmhSetFsRrm11hDfsInterval (INT4 i4FsRrmRadioType,
                           UINT4 u4SetValFsRrmDfsInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bDfsInterval =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4DfsInterval == u4SetValFsRrmDfsInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u4DfsInterval =
        u4SetValFsRrmDfsInterval;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmSNRThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmSNRThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmSNRThreshold (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrmSNRThreshold)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bSNRThreshold =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.i2SNRThreshold ==
        (INT2) i4SetValFsRrmSNRThreshold)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.i2SNRThreshold =
        (INT2) i4SetValFsRrmSNRThreshold;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
Function    :  nmhSetFsRrmChannelSwitchMsgStatus
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmChannelSwitchMsgStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmChannelSwitchMsgStatus (INT4 i4SetValFsRrmChannelSwitchMsgStatus)
{
#ifdef WLC_WANTED
    INT4                i4CurrentIfIndex = 0, i4NextIfIndex = 0;
    UINT1               u1RetVal = 1;

    if (gu4ChannelSwitchMsgStatus ==
        (UINT4) i4SetValFsRrmChannelSwitchMsgStatus)
    {
        return SNMP_SUCCESS;
    }

    gu4ChannelSwitchMsgStatus = (UINT4) i4SetValFsRrmChannelSwitchMsgStatus;

    u1RetVal = (UINT1) nmhGetFirstIndexFsRrmAPConfigTable (&i4NextIfIndex);
    if (u1RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    do
    {
        i4CurrentIfIndex = i4NextIfIndex;

        u1RetVal = RfMgmtProcessChSwitchMsg (i4CurrentIfIndex);
        if (u1RetVal == RFMGMT_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    while (nmhGetNextIndexFsRrmAPConfigTable (i4CurrentIfIndex, &i4NextIfIndex)
           == SNMP_SUCCESS);
#endif
    UNUSED_PARAM (i4SetValFsRrmChannelSwitchMsgStatus);
    return SNMP_SUCCESS;
}

#ifdef WLC_WANTED
/****************************************************************************
Function    :  nmhSetFsRrmRowStatus
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmRowStatus (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrmRowStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT1               u1OpCode = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    /* Check for the existing status. If already set return success */

    switch (i4SetValFsRrmRowStatus)
    {
        case CREATE_AND_WAIT:
            i4SetValFsRrmRowStatus = NOT_READY;
            u1OpCode = RFMGMT_CREATE_RADIO_TYPE_ENTRY;
            break;

        case CREATE_AND_GO:
            i4SetValFsRrmRowStatus = ACTIVE;
            u1OpCode = RFMGMT_CREATE_RADIO_TYPE_ENTRY;
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            u1OpCode = RFMGMT_SET_AUTO_RF_ENTRY;
            break;

        case DESTROY:
            u1OpCode = RFMGMT_DELETE_RADIO_TYPE_ENTRY;
            break;

        default:
            return SNMP_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bRowStatus =
        OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.i4RowStatus =
        i4SetValFsRrmRowStatus;

    u1RetVal = RfMgmtProcessDBMsg (u1OpCode, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsRrmDcaMode
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmDcaMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmDcaMode (UINT4 *pu4ErrorCode,
                       INT4 i4FsRrmRadioType, INT4 i4TestValFsRrmDcaMode)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmDcaMode != RFMGMT_DCA_MODE_GLOBAL) &&
        (i4TestValFsRrmDcaMode != RFMGMT_DCA_MODE_PER_AP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmDcaSelectionMode
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmDcaSelectionMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmDcaSelectionMode (UINT4 *pu4ErrorCode, INT4 i4FsRrmRadioType,
                                INT4 i4TestValFsRrmDcaSelectionMode)
{
    INT4                i4RowStatus = 0;
    INT4                i4DcaMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if (i4DcaMode != RFMGMT_DCA_MODE_GLOBAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmDcaSelectionMode != RFMGMT_DCA_SELECTION_AUTO) &&
        (i4TestValFsRrmDcaSelectionMode != RFMGMT_DCA_SELECTION_ONCE) &&
        (i4TestValFsRrmDcaSelectionMode != RFMGMT_DCA_SELECTION_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmTpcMode
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmTpcMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmTpcMode (UINT4 *pu4ErrorCode, INT4 i4FsRrmRadioType,
                       INT4 i4TestValFsRrmTpcMode)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmTpcMode != RFMGMT_TPC_MODE_GLOBAL) &&
        (i4TestValFsRrmTpcMode != RFMGMT_TPC_MODE_PER_AP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmTpcSelectionMode
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmTpcSelectionMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmTpcSelectionMode (UINT4 *pu4ErrorCode, INT4 i4FsRrmRadioType,
                                INT4 i4TestValFsRrmTpcSelectionMode)
{
    INT4                i4RowStatus = 0;
    INT4                i4TpcMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmTpcMode (i4FsRrmRadioType, &i4TpcMode);
    if (i4TpcMode != RFMGMT_TPC_MODE_GLOBAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmTpcSelectionMode != RFMGMT_TPC_SELECTION_AUTO) &&
        (i4TestValFsRrmTpcSelectionMode != RFMGMT_TPC_SELECTION_ONCE) &&
        (i4TestValFsRrmTpcSelectionMode != RFMGMT_TPC_SELECTION_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmTpcUpdate
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmTpcUpdate
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmTpcUpdate (UINT4 *pu4ErrorCode,
                         INT4 i4FsRrmRadioType, INT4 i4TestValFsRrmTpcUpdate)
{
    INT4                i4RowStatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelectionMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmTpcMode (i4FsRrmRadioType, &i4TpcMode);
    if ((i4TpcMode == RFMGMT_TPC_MODE_GLOBAL) ||
        (i4TpcMode == RFMGMT_TPC_MODE_PER_AP))
    {
        if (i4TpcMode != RFMGMT_TPC_MODE_PER_AP)
        {
            nmhGetFsRrmTpcSelectionMode (i4FsRrmRadioType, &i4TpcSelectionMode);
            if (i4TpcSelectionMode != RFMGMT_TPC_SELECTION_ONCE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    if (i4TestValFsRrmTpcUpdate != RFMGMT_TPC_UPDATE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmDcaInterval
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmDcaInterval
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmDcaInterval (UINT4 *pu4ErrorCode,
                           INT4 i4FsRrmRadioType,
                           UINT4 u4TestValFsRrmDcaInterval)
{
    INT4                i4RowStatus = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelection = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelection);
        if (i4DcaSelection != RFMGMT_DCA_SELECTION_AUTO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if (((INT4) u4TestValFsRrmDcaInterval < RFMGMT_MIN_DCA_INTERVAL) ||
        (u4TestValFsRrmDcaInterval > RFMGMT_MAX_DCA_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmDcaSensitivity
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmDcaSensitivity
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmDcaSensitivity (UINT4 *pu4ErrorCode,
                              INT4 i4FsRrmRadioType,
                              INT4 i4TestValFsRrmDcaSensitivity)
{
    INT4                i4RowStatus = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelection = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for DCA mode-global and DCA Selection Mode - off */
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelection);
        if (i4DcaSelection == RFMGMT_DCA_SELECTION_OFF)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsRrmDcaSensitivity != CLI_DCA_SENSITIVITY_LOW) &&
        (i4TestValFsRrmDcaSensitivity != CLI_DCA_SENSITIVITY_MEDIUM) &&
        (i4TestValFsRrmDcaSensitivity != CLI_DCA_SENSITIVITY_HIGH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmAllowedChannels
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmAllowedChannels
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAllowedChannels (UINT4 *pu4ErrorCode,
                               INT4 i4FsRrmRadioType,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsRrmAllowedChannels)
{
    INT4                i4RowStatus = 0;
    UINT4               u4Index = 0;
    UINT1               u1Channel = 0;
    UINT1               u1Flag = OSIX_FALSE;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;
    UINT1               au1FsDot11CountryString[RADIO_COUNTRY_LENGTH];
    tSNMP_OCTET_STRING_TYPE CountryString;

    MEMSET (&CountryString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsDot11CountryString, 0, RADIO_COUNTRY_LENGTH);

    CountryString.pu1_OctetList = au1FsDot11CountryString;
    CountryString.i4_Length = RADIO_COUNTRY_LENGTH;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for DCA mode-global and DCA Selection Mode - off */
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelectionMode);
        if (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if (i4FsRrmRadioType == RFMGMT_RADIO_TYPEA)
    {
        nmhGetFsDot11CountryString (&CountryString);
        if (STRCMP (CountryString.pu1_OctetList,
                    RADIOIF_COUNTRY_CODE_JAPAN) == 0)
        {
            u1Flag = OSIX_TRUE;
        }
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
        {
            if (((gau1Dot11aChannelCheckList[u1Channel] == 0) &&
                 (pTestValFsRrmAllowedChannels->pu1_OctetList[u1Channel] !=
                  0) && (u1Flag == OSIX_FALSE)) ||
                ((gau1Dot11aChannelCheckList[u1Channel] != 0) &&
                 (pTestValFsRrmAllowedChannels->pu1_OctetList[u1Channel]
                  != gau1Dot11aRegDomainChannelList[u1Channel]) &&
                 (pTestValFsRrmAllowedChannels->pu1_OctetList[u1Channel] != 0)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        nmhGetFsDot11CountryString (&CountryString);
        for (u4Index = 0; u4Index < RADIOIF_SUPPORTED_COUNTRY_LIST; u4Index++)
        {
            if (STRCMP (CountryString.pu1_OctetList,
                        gWsscfgSupportedCountryList[u4Index].au1CountryCode) ==
                0)
            {
                u4Index = (UINT4) gRegDomainCountryMapping[u4Index].u2RegDomain;
                break;
            }
        }
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
        {
            if (pTestValFsRrmAllowedChannels->pu1_OctetList[u1Channel]
                > RFMGMT_MAX_CHANNELB
                || ((i4FsRrmRadioType == RFMGMT_RADIO_TYPEG)
                    && (u1Channel > (RFMGMT_MAX_CHANNELB - 1))))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
        if (u4Index != RADIOIF_SUPPORTED_COUNTRY_LIST)
        {
            for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
            {
                if ((gau1Dot11bChannelRegDomainCheckList[u4Index][u1Channel] ==
                     0)
                    && (pTestValFsRrmAllowedChannels->
                        pu1_OctetList[u1Channel] != 0))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_INVALID_VALUE);
                    return SNMP_FAILURE;
                }
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmUnusedChannels
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmUnusedChannels
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmUnusedChannels (UINT4 *pu4ErrorCode,
                              INT4 i4FsRrmRadioType,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsRrmUnusedChannels)
{
    INT4                i4RowStatus = 0;
    UINT1               u1Channel = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for DCA mode-global and DCA Selection Mode - off */
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelectionMode);
        if (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if (i4FsRrmRadioType == RFMGMT_RADIO_TYPEA)
    {
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
        {

            if (((gau1Dot11aChannelCheckList[u1Channel] != 0) &&
                 (pTestValFsRrmUnusedChannels->pu1_OctetList[u1Channel]
                  != gau1Dot11aRegDomainChannelList[u1Channel]) &&
                 (pTestValFsRrmUnusedChannels->pu1_OctetList[u1Channel] != 0)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
        {
            if (pTestValFsRrmUnusedChannels->pu1_OctetList[u1Channel]
                > RFMGMT_MAX_CHANNELB)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmUpdateChannel
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmUpdateChannel
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmUpdateChannel (UINT4 *pu4ErrorCode,
                             INT4 i4FsRrmRadioType,
                             INT4 i4TestValFsRrmUpdateChannel)
{
    INT4                i4RowStatus = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    /*Check for DCA mode-global and DCA Selection Mode - off */
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if ((i4DcaMode == RFMGMT_DCA_MODE_GLOBAL) ||
        (i4DcaMode == RFMGMT_DCA_MODE_PER_AP))
    {
        if (i4DcaMode != RFMGMT_DCA_MODE_PER_AP)
        {
            nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelectionMode);
            if (i4DcaSelectionMode != RFMGMT_DCA_SELECTION_ONCE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    if (i4TestValFsRrmUpdateChannel != RFMGMT_DCA_UPDATE_CHANNEL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmRSSIThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmRSSIThreshold
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmRSSIThreshold (UINT4 *pu4ErrorCode,
                             INT4 i4FsRrmRadioType,
                             INT4 i4TestValFsRrmRSSIThreshold)
{
    INT4                i4RowStatus = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelectionMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for DCA mode-global and DCA Selection Mode - off */
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);

    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelectionMode);
        if (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsRrmRSSIThreshold < RFMGMT_MIN_RSSI_THRS) ||
        (i4TestValFsRrmRSSIThreshold > RFMGMT_MAX_RSSI_THRS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmPowerThreshold
Input       :  The Indices
FsRrmRadioType

The Object
testValFsRrmPowerThreshold
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmPowerThreshold (UINT4 *pu4ErrorCode, INT4 i4FsRrmRadioType,
                              INT4 i4TestValFsRrmPowerThreshold)
{
    INT4                i4RowStatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for TPC mode-global and TPC Selection Mode - Auto */
    nmhGetFsRrmTpcMode (i4FsRrmRadioType, &i4TpcMode);
    if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
    {
        nmhGetFsRrmTpcSelectionMode (i4FsRrmRadioType, &i4TpcSelection);
        if (i4TpcSelection != RFMGMT_TPC_SELECTION_AUTO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsRrmPowerThreshold < RFMGMT_MIN_RSSI_THRS) ||
        (i4TestValFsRrmPowerThreshold > RFMGMT_MAX_RSSI_THRS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

 /****************************************************************************
  * Function    :  nmhTestv2FsRrm11hTpcStatus
  * Input       :  The Indices
  *                 FsRrmRadioType
  * 
  *                 The Object
  *                 testValFsRrm11hTpcStatus
  * Output      :  The Test Low Lev Routine Take the Indices &
  *                Test whether that Value is Valid Input for Set.
  *                 Stores the value of error code in the Return val
  * Error Codes :  The following error codes are to be returned
  *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
  *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
  *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
  *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
  *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
  * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  *****************************************************************************/
INT1
nmhTestv2FsRrm11hTpcStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsRrmRadioType,
                            INT4 i4TestValFsRrm11hTpcStatus)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrm11hTpcStatus != RFMGMT_TPC_ENABLE) &&
        (i4TestValFsRrm11hTpcStatus != RFMGMT_TPC_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hTpcInterval
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                testValFsRrm11hTpcInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hTpcInterval (UINT4 *pu4ErrorCode,
                              INT4 i4FsRrmRadioType,
                              UINT4 u4TestValFsRrm11hTpcInterval)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if (u4TestValFsRrm11hTpcInterval > RFMGMT_MAX_TPC_INTERVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hMinLinkThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                testValFsRrm11hMinLinkThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hMinLinkThreshold (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRrmRadioType,
                                   UINT4 u4TestValFsRrm11hMinLinkThreshold)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if (u4TestValFsRrm11hMinLinkThreshold > RFMGMT_MAX_LINK_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hMaxLinkThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                testValFsRrm11hMaxLinkThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hMaxLinkThreshold (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRrmRadioType,
                                   UINT4 u4TestValFsRrm11hMaxLinkThreshold)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if (u4TestValFsRrm11hMaxLinkThreshold > RFMGMT_MAX_LINK_THRESHOLD)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hStaCountThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                testValFsRrm11hStaCountThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hStaCountThreshold (UINT4 *pu4ErrorCode,
                                    INT4 i4FsRrmRadioType,
                                    UINT4 u4TestValFsRrm11hStaCountThreshold)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsRrm11hStaCountThreshold < RFMGMT_MIN_STA_COUNT_THRESHOLD) ||
        (u4TestValFsRrm11hStaCountThreshold > RFMGMT_MAX_STA_COUNT_THRESHOLD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmClientThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmClientThreshold
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmClientThreshold (UINT4 *pu4ErrorCode,
                               INT4 i4FsRrmRadioType,
                               UINT4 u4TestValFsRrmClientThreshold)
{
    INT4                i4RowStatus = 0;
    INT4                i4DcaSelectionMode = 0;
    INT4                i4DcaMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for DCA mode-global and DCA Selection Mode - off */
    nmhGetFsRrmDcaMode (i4FsRrmRadioType, &i4DcaMode);
    if (i4DcaMode == RFMGMT_DCA_MODE_GLOBAL)
    {
        nmhGetFsRrmDcaSelectionMode (i4FsRrmRadioType, &i4DcaSelectionMode);
        if (i4DcaSelectionMode == RFMGMT_DCA_SELECTION_OFF)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if ((u4TestValFsRrmClientThreshold < RFMGMT_MIN_DCA_CLIENT_THRESHOLD) ||
        (u4TestValFsRrmClientThreshold > RFMGMT_DCA_CLIENT_THRESHOLD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmTpcInterval
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmTpcInterval
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmTpcInterval (UINT4 *pu4ErrorCode,
                           INT4 i4FsRrmRadioType,
                           UINT4 u4TestValFsRrmTpcInterval)
{
    INT4                i4RowStatus = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for TPC mode-global and TPC Selection Mode - Auto */
    nmhGetFsRrmTpcMode (i4FsRrmRadioType, &i4TpcMode);
    if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
    {
        nmhGetFsRrmTpcSelectionMode (i4FsRrmRadioType, &i4TpcSelection);
        if (i4TpcSelection != RFMGMT_TPC_SELECTION_AUTO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }

    if (((INT4) u4TestValFsRrmTpcInterval < RFMGMT_MIN_TPC_INTERVAL) ||
        (u4TestValFsRrmTpcInterval > RFMGMT_MAX_TPC_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#endif
/****************************************************************************
Function    :  nmhTestv2FsRrmChannelSwitchMsgStatus
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmChannelSwitchMsgStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmChannelSwitchMsgStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsRrmChannelSwitchMsgStatus)
{
    if ((i4TestValFsRrmChannelSwitchMsgStatus !=
         RFMGMT_CHANNEL_SWITCH_MSG_ENABLE) &&
        (i4TestValFsRrmChannelSwitchMsgStatus !=
         RFMGMT_CHANNEL_SWITCH_MSG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#ifdef WLC_WANTED
/****************************************************************************
Function    :  nmhTestv2FsRrmSNRThreshold
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmSNRThreshold
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmSNRThreshold (UINT4 *pu4ErrorCode,
                            INT4 i4FsRrmRadioType,
                            INT4 i4TestValFsRrmSNRThreshold)
{
    INT4                i4RowStatus = 0;
    INT4                i4TpcSelectionMode = 0;
    INT4                i4TpcMode = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    /*Check for TPC mode-global and TPC Selection Mode - Auto */
    nmhGetFsRrmTpcMode (i4FsRrmRadioType, &i4TpcMode);
    if (i4TpcMode == RFMGMT_TPC_MODE_GLOBAL)
    {
        nmhGetFsRrmTpcSelectionMode (i4FsRrmRadioType, &i4TpcSelectionMode);
        if (i4TpcSelectionMode == RFMGMT_TPC_SELECTION_OFF)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsRrmSNRThreshold < RFMGMT_MIN_SNR_THRS) ||
        (i4TestValFsRrmSNRThreshold > RFMGMT_MAX_SNR_THRS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmRowStatus
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmRowStatus (UINT4 *pu4ErrorCode,
                         INT4 i4FsRrmRadioType, INT4 i4TestValFsRrmRowStatus)
{

    switch (i4TestValFsRrmRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_INVALID_VALUE);
            return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4FsRrmRadioType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhTestv2FsRrm11hDfsStatus
 * Input       :  The Indices
 *                FsRrmRadioType
 * 
 *                The Object
 *                testValFsRrm11hDfsStatus
 * Output      :  The Test Low Lev Routine Take the Indices &
 *                Test whether that Value is Valid Input for Set.
 *                Stores the value of error code in the Return val
 * Error Codes :  The following error codes are to be returned
 *                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsRrm11hDfsStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsRrmRadioType,
                            INT4 i4TestValFsRrm11hDfsStatus)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrm11hDfsStatus != RFMGMT_DFS_ENABLE) &&
        (i4TestValFsRrm11hDfsStatus != RFMGMT_DFS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhTestv2FsRrmConsiderExternalAPs
 * Input       :  The Indices
 *                FsRrmRadioType
 * 
 *                The Object
 *                testValFsRrm11hDfsStatus
 * Output      :  The Test Low Lev Routine Take the Indices &
 *                Test whether that Value is Valid Input for Set.
 *                Stores the value of error code in the Return val
 * Error Codes :  The following error codes are to be returned
 *                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT1
nmhTestv2FsRrmConsiderExternalAPs (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRrmRadioType,
                                   INT4 i4TestValFsRrmConsiderExternalAPs)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if (((UINT4) i4TestValFsRrmConsiderExternalAPs
         != RFMGMT_CONSIDER_EXTERNAL_APS) &&
        ((UINT4) i4TestValFsRrmConsiderExternalAPs
         != RFMGMT_IGNORE_EXTERNAL_APS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrmClearNeighborInfo
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                testValFsRrmClearNeighborInfo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrmClearNeighborInfo (UINT4 *pu4ErrorCode,
                                 INT4 i4FsRrmRadioType,
                                 INT4 i4TestValFsRrmClearNeighborInfo)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if (((UINT4) i4TestValFsRrmClearNeighborInfo
         != OSIX_TRUE) &&
        ((UINT4) i4TestValFsRrmClearNeighborInfo != OSIX_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrmNeighborCountThreshold
 Input       :  The Indices
                FsRrmRadioType

                The Object 
                testValFsRrmNeighborCountThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrmNeighborCountThreshold (UINT4 *pu4ErrorCode,
                                      INT4 i4FsRrmRadioType,
                                      UINT4
                                      u4TestValFsRrmNeighborCountThreshold)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if ((u4TestValFsRrmNeighborCountThreshold >
         RFMGMT_MAX_NEIGH_COUNT_THRESHOLD)
        || (u4TestValFsRrmNeighborCountThreshold <
            RFMGMT_MIN_NEIGH_COUNT_THRESHOLD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/**************************************************************
Function    :  nmhTestv2FsRrm11hDfsInterval
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmDfsInterval
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**************************************************************/

INT1
nmhTestv2FsRrm11hDfsInterval (UINT4 *pu4ErrorCode,
                              INT4 i4FsRrmRadioType,
                              UINT4 u4TestValFsRrmDfsInterval)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if (((INT4) u4TestValFsRrmDfsInterval < RFMGMT_MIN_DFS_INTERVAL) ||
        (u4TestValFsRrmDfsInterval > RFMGMT_MAX_DFS_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhDepv2FsRrmConfigTable
Input       :  The Indices
FsRrmRadioType
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmConfigTable (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif

/* LOW LEVEL Routines for Table : FsRrmAPConfigTable. */
/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmAPConfigTable
Input       :  The Indices
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmAPConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmAPConfigTable
Input       :  The Indices
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrmAPConfigTable (INT4 *pi4IfIndex)
{
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;

    pRfMgmtAPConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB);

    if (pRfMgmtAPConfigDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtAPConfigDB->u4RadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmAPConfigTable
Input       :  The Indices
IfIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmAPConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRfMgmtAPConfigDB  *pRfMgmtAPConfigDB = NULL;
    tRfMgmtAPConfigDB   RfMgmtAPConfigDB;

    MEMSET (&RfMgmtAPConfigDB, 0, sizeof (tRfMgmtAPConfigDB));

    RfMgmtAPConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    pRfMgmtAPConfigDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPConfigDB,
                       &RfMgmtAPConfigDB, NULL);

    if (pRfMgmtAPConfigDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtAPConfigDB->u4RadioIfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmAPAutoScanStatus
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPAutoScanStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPAutoScanStatus (INT4 i4IfIndex,
                             INT4 *pi4RetValFsRrmAPAutoScanStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bAutoScanStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmAPAutoScanStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u1AutoScanStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPNeighborScanFreq
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPNeighborScanFreq
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPNeighborScanFreq (INT4 i4IfIndex,
                               UINT4 *pu4RetValFsRrmAPNeighborScanFreq)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
#ifdef WLC_WANTED
    if (RfmgmtGetBaseRadioIfIndex
        ((UINT4) i4IfIndex,
         &RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
         u4RadioIfIndex) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtGetBaseRadioIfIndex "
                    "returned failure\r\n");
        return SNMP_FAILURE;
    }
#else
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        SYS_DEF_MAX_ENET_INTERFACES + 1;
#endif

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bNeighborMsgPeriod = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPNeighborScanFreq =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2NeighborMsgPeriod;
#ifdef WTP_WANTED
    UNUSED_PARAM (i4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPChannelScanDuration
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPChannelScanDuration
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPChannelScanDuration (INT4 i4IfIndex,
                                  UINT4 *pu4RetValFsRrmAPChannelScanDuration)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelScanDuration = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPChannelScanDuration =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2ChannelScanDuration;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPNeighborAgingPeriod
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPNeighborAgingPeriod
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPNeighborAgingPeriod (INT4 i4IfIndex,
                                  UINT4 *pu4RetValFsRrmAPNeighborAgingPeriod)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bNeighborAgingPeriod = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPNeighborAgingPeriod =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2NeighborAgingPeriod;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPMacAddress
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPMacAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPMacAddress (INT4 i4IfIndex, tMacAddr * pRetValFsRrmAPMacAddress)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    tMacAddr            MacAddr;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRadioMacAddr = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return SNMP_FAILURE;
    }

    if (MEMCMP
        (MacAddr,
         RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.RadioMacAddr,
         sizeof (tMacAddr)) == 0)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

        /* Fill boolean Radio Id     */
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "nmhGetFsRrmAPMacAddress: RadioIf DB access failed. \n");
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_FAILURE;
        }

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                        "nmhGetFsRrmAPMacAddress : Capwap DB access failed.\n");
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_FAILURE;
        }

        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                RadioMacAddr, pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                sizeof (tMacAddr));

        u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB);
        if (u1RetVal == RFMGMT_FAILURE)
        {
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_FAILURE;
        }
    }
    MEMCPY (pRetValFsRrmAPMacAddress,
            RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
            RadioMacAddr, sizeof (tMacAddr));
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRrmAPMacAddress);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPChannelMode
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPChannelMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPChannelMode (INT4 i4IfIndex, INT4 *pi4RetValFsRrmAPChannelMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelAssignmentMode = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmAPChannelMode =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u1ChannelAssignmentMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPChannelChangeCount
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPChannelChangeCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPChannelChangeCount (INT4 i4IfIndex,
                                 UINT4 *pu4RetValFsRrmAPChannelChangeCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelChangeCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPChannelChangeCount =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u1ChannelChangeCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPAssignedChannel
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPAssignedChannel
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPAssignedChannel (INT4 i4IfIndex,
                              UINT4 *pu4RetValFsRrmAPAssignedChannel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bCurrentChannel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPAssignedChannel =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2CurrentChannel;

    return SNMP_SUCCESS;
}

 /****************************************************************************
  * Function    :  nmhGetFsRrm11hTPCRequestInterval
  * Input       :  The Indices
  *                 IfIndex
  *                  The Object
  *                 retValFsRrm11hTPCRequestInterval
  * Output      :  The Get Low Lev Routine Take the Indices &
  *                store the Value requested in the Return val.
  * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  * *************************************************************************/
INT1
nmhGetFsRrm11hTPCRequestInterval (INT4 i4IfIndex,
                                  UINT4 *pu4RetValFsRrm11hTPCRequestInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hTPCRequestInterval =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2RfMgmt11hTpcRequestInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hDFSQuietInterval
 Input       :  The Indices
                IfIndex
                The Object
                retValFsRrm11hDFSQuietInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hDFSQuietInterval (INT4 i4IfIndex,
                                UINT4 *pu4RetValFsRrm11hDFSQuietInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hDFSQuietInterval =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2RfMgmt11hDfsQuietInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hDFSQuietPeriod
 Input       :  The Indices
                IfIndex
                The Object
                retValFsRrm11hDFSQuietPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hDFSQuietPeriod (INT4 i4IfIndex,
                              UINT4 *pu4RetValFsRrm11hDFSQuietPeriod)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hDFSQuietPeriod =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2RfMgmt11hDfsQuietPeriod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hDFSMeasurementInterval
 Input       :  The Indices
                IfIndex
                The Object
                retValFsRrm11hDFSMeasurementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hDFSMeasurementInterval (INT4 i4IfIndex,
                                      UINT4
                                      *pu4RetValFsRrm11hDFSMeasurementInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hDFSMeasurementInterval =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2RfMgmt11hDfsMeasurementInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hDFSChannelSwitchStatus 
 Input       :  The Indices
                IfIndex
                The Object
                retValFsRrm11hDFSMeasurementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hDFSChannelSwitchStatus (INT4 i4IfIndex,
                                      INT4
                                      *pu4RetValFsRrm11hDFSChannelSwitchStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hDFSChannelSwitchStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u2RfMgmt11hDfsChannelSwitchStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPChannelChangeTime
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPChannelChangeTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPChannelChangeTime (INT4 i4IfIndex,
                                UINT4 *pu4RetValFsRrmAPChannelChangeTime)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelChangeTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPChannelChangeTime =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u4ChannelChangeTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmAPTxPowerLevel
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPTxPowerLevel
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPTxPowerLevel (INT4 i4IfIndex, UINT4 *pu4RetValFsRrmAPTxPowerLevel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmAPTxPowerLevel =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u2TxPowerLevel;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTxPowerChangeTime
Input       :  The Indices
IfIndex

The Object
retValFsRrmTxPowerChangeTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTxPowerChangeTime (INT4 i4IfIndex,
                              UINT4 *pu4RetValFsRrmTxPowerChangeTime)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerChangeTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsRrmTxPowerChangeTime =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4TxPowerChangeTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrmAPChannelAllowedList
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRrmAPChannelAllowedList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrmAPChannelAllowedList (INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsRrmAPChannelAllowedList)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bGetAllowedList = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDot11RadioType = OSIX_TRUE;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (((INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
         RfMgmtAPConfigDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
        || ((INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
        || (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
    {
        MEMCPY (pRetValFsRrmAPChannelAllowedList->pu1_OctetList,
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                au1AllowedChannelList, RFMGMT_MAX_CHANNELA);
        pRetValFsRrmAPChannelAllowedList->i4_Length = RFMGMT_MAX_CHANNELA;
    }
    else
    {
        MEMCPY (pRetValFsRrmAPChannelAllowedList->pu1_OctetList,
                RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                au1AllowedChannelList, RFMGMT_MAX_CHANNELB);
        pRetValFsRrmAPChannelAllowedList->i4_Length = RFMGMT_MAX_CHANNELB;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRrmAPChannelAllowedList);
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmAPAutoScanStatus
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPAutoScanStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPAutoScanStatus (INT4 i4IfIndex, INT4 i4SetValFsRrmAPAutoScanStatus)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bAutoScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        == RFMGMT_FAILURE)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u1AutoScanStatus ==
        (UINT1) i4SetValFsRrmAPAutoScanStatus)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u1AutoScanStatus =
        (UINT1) i4SetValFsRrmAPAutoScanStatus;

    u1RetVal = RfMgmtProcessNeighConfig (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        /* If auto scan is disabled, then delete the neighbor entries 
         * from DB */
        if (i4SetValFsRrmAPAutoScanStatus == RFMGMT_AUTO_SCAN_DISABLE)
        {
            RfMgmtDeleteNeighborDetails ((UINT4) i4IfIndex, 0);
        }
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                    "Processing Neighbor config failed\r\n");
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsRrmAPAutoScanStatus);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsRrmAPNeighborScanFreq
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPNeighborScanFreq
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPNeighborScanFreq (INT4 i4IfIndex,
                               UINT4 u4SetValFsRrmAPNeighborScanFreq)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    if (RfmgmtGetBaseRadioIfIndex
        ((UINT4) i4IfIndex,
         &RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
         u4RadioIfIndex) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtGetBaseRadioIfIndex "
                    "returned failure\r\n");
        return SNMP_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bNeighborMsgPeriod = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2NeighborMsgPeriod ==
        (UINT2) u4SetValFsRrmAPNeighborScanFreq)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2NeighborMsgPeriod =
        (UINT2) u4SetValFsRrmAPNeighborScanFreq;

    u1RetVal = RfMgmtProcessNeighConfig (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;

    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrmAPNeighborScanFreq);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsRrmAPChannelScanDuration
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPChannelScanDuration
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPChannelScanDuration (INT4 i4IfIndex,
                                  UINT4 u4SetValFsRrmAPChannelScanDuration)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelScanDuration = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2ChannelScanDuration ==
        (UINT2) u4SetValFsRrmAPChannelScanDuration)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2ChannelScanDuration =
        (UINT2) u4SetValFsRrmAPChannelScanDuration;

    u1RetVal = RfMgmtProcessNeighConfig (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrmAPChannelScanDuration);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsRrmAPNeighborAgingPeriod
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPNeighborAgingPeriod
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPNeighborAgingPeriod (INT4 i4IfIndex,
                                  UINT4 u4SetValFsRrmAPNeighborAgingPeriod)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bNeighborAgingPeriod = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2NeighborAgingPeriod ==
        (UINT2) u4SetValFsRrmAPNeighborAgingPeriod)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2NeighborAgingPeriod =
        (UINT2) u4SetValFsRrmAPNeighborAgingPeriod;

    u1RetVal = RfMgmtProcessNeighConfig (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrmAPNeighborAgingPeriod);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsRrmAPChannelMode
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPChannelMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPChannelMode (INT4 i4IfIndex, INT4 i4SetValFsRrmAPChannelMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelAssignmentMode = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u1ChannelAssignmentMode ==
        (UINT1) i4SetValFsRrmAPChannelMode)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u1ChannelAssignmentMode =
        (UINT1) i4SetValFsRrmAPChannelMode;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmAPAssignedChannel
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPAssignedChannel
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPAssignedChannel (INT4 i4IfIndex,
                              UINT4 u4SetValFsRrmAPAssignedChannel)
{
#ifdef WLC_WANTED
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfDB;
    UINT4               u4RadioIfType = 0;
    UINT1               u1RetVal = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bCurrentChannel = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2CurrentChannel ==
        (UINT2) u4SetValFsRrmAPAssignedChannel)
    {
        return SNMP_SUCCESS;
    }

    RadioIfDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();
    if (RadioIfDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioIfType) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    RadioIfDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u1CurrentChannel =
        (UINT1) u4SetValFsRrmAPAssignedChannel;
    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    if ((u4RadioIfType == RADIO_TYPE_A) || (u4RadioIfType == RADIO_TYPE_AN)
        || (u4RadioIfType == RADIO_TYPE_AC))
    {
        if (RadioIfSetOFDMTable (&RadioIfDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (RadioIfDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (RadioIfSetDSSSTable (&RadioIfDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (RadioIfDB.
                                             RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return SNMP_FAILURE;
        }
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u2CurrentChannel =
        (UINT2) u4SetValFsRrmAPAssignedChannel;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bChannelChangeCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrmAPAssignedChannel);
    return SNMP_SUCCESS;
#endif
}

 /****************************************************************************
  * Function    :  nmhSetFsRrm11hTPCRequestInterval
  * Input       :  The Indices
  *                IfIndex
  *                  The Object
  *                setValFsRrm11hTPCRequestInterval
  * Output      :  The Set Low Lev Routine Take the Indices &
  *                Sets the Value accordingly.
  * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
  * 
  *****************************************************************************/
INT1
nmhSetFsRrm11hTPCRequestInterval (INT4 i4IfIndex,
                                  UINT4 u4SetValFsRrm11hTPCRequestInterval)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval = OSIX_TRUE;

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hTpcRequestInterval = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval ==
        (UINT2) u4SetValFsRrm11hTPCRequestInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hTpcRequestInterval =
        (UINT2) u4SetValFsRrm11hTPCRequestInterval;

    u1RetVal = RfMgmtProcessTpcSpectMgmt (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Failed to send config update\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrm11hTPCRequestInterval);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRrmAPChannelAllowedList
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRrmAPChannelAllowedList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrmAPChannelAllowedList (INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsRrmAPChannelAllowedList)
{
#ifdef WLC_WANTED
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ChannelToFill = 0, u4MaxChannels = 0;
    UINT1               u1Channel = 0;
    UINT1               u1RetVal = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bGetAllowedList = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bDot11RadioType = OSIX_TRUE;
    if (((INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
         RfMgmtAPConfigDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEA)
        || ((INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAN)
        || (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
            RfMgmtAPConfigDB.u4Dot11RadioType == RFMGMT_RADIO_TYPEAC))
    {
        MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
                au1AllowedChannelList,
                pSetValFsRrmAPChannelAllowedList->pu1_OctetList,
                RFMGMT_MAX_CHANNELA);
    }
    else
    {
        u4MaxChannels = RFMGMT_MAX_CHANNELB;
        for (u1Channel = 0; u1Channel < u4MaxChannels; u1Channel++)
        {
            u4ChannelToFill =
                pSetValFsRrmAPChannelAllowedList->pu1_OctetList[u1Channel];
            if (u4ChannelToFill != 0)
            {
                RfMgmtDB.unRfMgmtDB.
                    RfMgmtApConfigTable.RfMgmtAPConfigDB.au1AllowedChannelList
                    [u1Channel] = (UINT1) u4ChannelToFill;
            }
        }
    }

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValFsRrmAPChannelAllowedList);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hDFSQuietInterval
 Input       :  The Indices
                IfIndex
     
        The Object
                setValFsRrm11hDFSQuietInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hDFSQuietInterval (INT4 i4IfIndex,
                                UINT4 u4SetValFsRrm11hDFSQuietInterval)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietInterval = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval ==
        (UINT2) u4SetValFsRrm11hDFSQuietInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietInterval =
        (UINT2) u4SetValFsRrm11hDFSQuietInterval;

    u1RetVal = RfMgmtProcessDfsParams (&RfMgmtDB);
    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrm11hDFSQuietInterval);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hDFSQuietPeriod
 Input       :  The Indices
                IfIndex
                The Object
                setValFsRrm11hDFSQuietPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hDFSQuietPeriod (INT4 i4IfIndex,
                              UINT4 u4SetValFsRrm11hDFSQuietPeriod)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsQuietPeriod = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod ==
        (UINT2) u4SetValFsRrm11hDFSQuietPeriod)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsQuietPeriod =
        (UINT2) u4SetValFsRrm11hDFSQuietPeriod;
    u1RetVal = RfMgmtProcessDfsParams (&RfMgmtDB);
    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrm11hDFSQuietPeriod);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hDFSMeasurementInterval
 Input       :  The Indices
                IfIndex
                The Object
                setValFsRrm11hDFSMeasurementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhSetFsRrm11hDFSMeasurementInterval (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValFsRrm11hDFSMeasurementInterval)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsMeasurementInterval = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval ==
        (UINT2) u4SetValFsRrm11hDFSMeasurementInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsMeasurementInterval =
        (UINT2) u4SetValFsRrm11hDFSMeasurementInterval;

    u1RetVal = RfMgmtProcessDfsParams (&RfMgmtDB);
    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrm11hDFSMeasurementInterval);
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hDFSChannelSwitchStatus
 Input       :  The Indices
                IfIndex
                The Object
                setValFsRrm11hDFSChannelSwitchStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsRrm11hDFSChannelSwitchStatus (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValFsRrm11hDFSChannelSwitchStatus)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bRfMgmt11hDfsChannelSwitchStatus = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus ==
        (UINT2) u4SetValFsRrm11hDFSChannelSwitchStatus)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u2RfMgmt11hDfsChannelSwitchStatus =
        (UINT2) u4SetValFsRrm11hDFSChannelSwitchStatus;

    u1RetVal = RfMgmtProcessDfsParams (&RfMgmtDB);
    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrm11hDFSChannelSwitchStatus);
#endif
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsRrmAPAutoScanStatus
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPAutoScanStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPAutoScanStatus (UINT4 *pu4ErrorCode,
                                INT4 i4IfIndex,
                                INT4 i4TestValFsRrmAPAutoScanStatus)
{
    if ((i4TestValFsRrmAPAutoScanStatus != RFMGMT_AUTO_SCAN_ENABLE) &&
        (i4TestValFsRrmAPAutoScanStatus != RFMGMT_AUTO_SCAN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmAPNeighborScanFreq
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPNeighborScanFreq
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPNeighborScanFreq (UINT4 *pu4ErrorCode,
                                  INT4 i4IfIndex,
                                  UINT4 u4TestValFsRrmAPNeighborScanFreq)
{
    if ((u4TestValFsRrmAPNeighborScanFreq > RFMGMT_MAX_NEIGH_MSG_PERIOD) ||
        (u4TestValFsRrmAPNeighborScanFreq < RFMGMT_MIN_NEIGH_MSG_PERIOD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmAPChannelScanDuration
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPChannelScanDuration
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPChannelScanDuration (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     UINT4 u4TestValFsRrmAPChannelScanDuration)
{
    if ((u4TestValFsRrmAPChannelScanDuration < RFMGMT_MIN_AUTO_SCAN_PERIOD) ||
        (u4TestValFsRrmAPChannelScanDuration > RFMGMT_MAX_AUTO_SCAN_PERIOD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmAPNeighborAgingPeriod
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPNeighborAgingPeriod
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPNeighborAgingPeriod (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     UINT4 u4TestValFsRrmAPNeighborAgingPeriod)
{
    if ((u4TestValFsRrmAPNeighborAgingPeriod < RFMGMT_MIN_NEIGH_AGE_PERIOD)
        || (u4TestValFsRrmAPNeighborAgingPeriod > RFMGMT_MAX_NEIGH_AGE_PERIOD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmAPChannelMode
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPChannelMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPChannelMode (UINT4 *pu4ErrorCode,
                             INT4 i4IfIndex, INT4 i4TestValFsRrmAPChannelMode)
{
#ifdef WLC_WANTED
    INT4                i4RadioType = 0;
    INT4                i4DcaMode = 0;

    if (nmhGetFsDot11RadioType (i4IfIndex, ((UINT4 *) &i4RadioType)) !=
        SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }

    if ((i4RadioType == RFMGMT_RADIO_TYPEAN) ||
        ((UINT4) i4RadioType == RFMGMT_RADIO_TYPEAC))
    {
        i4RadioType = RFMGMT_RADIO_TYPEA;
    }
    else if ((i4RadioType == RFMGMT_RADIO_TYPEBG) ||
             (i4RadioType == RFMGMT_RADIO_TYPEBGN))
    {
        i4RadioType = RFMGMT_RADIO_TYPEB;
    }

    nmhGetFsRrmDcaMode (i4RadioType, &i4DcaMode);
    if (i4DcaMode != RFMGMT_DCA_MODE_PER_AP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmAPChannelMode != RFMGMT_PER_AP_DCA_GLOBAL) &&
        (i4TestValFsRrmAPChannelMode != RFMGMT_PER_AP_DCA_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
#endif
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsRrmAPChannelMode);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsRrmAPAssignedChannel
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPAssignedChannel
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPAssignedChannel (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4TestValFsRrmAPAssignedChannel)
{
#ifdef WLC_WANTED
    INT4                i4RadioType = 0;
    INT4                i4DcaMode = 0;
    INT4                i4APChannelMode = 0;
    tSNMP_OCTET_STRING_TYPE AllowedList;
    UINT1               au1AllowedList[RFMGMT_MAX_CHANNELA];
    UINT1               au1AllowedChannelList[RFMGMT_MAX_CHANNELA];
    UINT1               u1Flag = 0;
    UINT1               u1Channel = 0;

    MEMSET (au1AllowedList, 0, RFMGMT_MAX_CHANNELA);
    MEMSET (&AllowedList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1AllowedChannelList, 0, RFMGMT_MAX_CHANNELA);

    AllowedList.pu1_OctetList = au1AllowedList;

    if (nmhGetFsDot11RadioType (i4IfIndex, (UINT4 *) &i4RadioType) !=
        SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }

    if ((i4RadioType == RFMGMT_RADIO_TYPEAN) ||
        ((UINT4) i4RadioType == RFMGMT_RADIO_TYPEAC))
    {
        i4RadioType = RFMGMT_RADIO_TYPEA;
    }
    else if ((i4RadioType == RFMGMT_RADIO_TYPEBG) ||
             (i4RadioType == RFMGMT_RADIO_TYPEBGN))
    {
        i4RadioType = RFMGMT_RADIO_TYPEB;
    }

    nmhGetFsRrmDcaMode (i4RadioType, &i4DcaMode);
    if (i4DcaMode != RFMGMT_DCA_MODE_PER_AP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmAPChannelMode (i4IfIndex, &i4APChannelMode);
    if (i4APChannelMode != RFMGMT_PER_AP_DCA_MANUAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmAllowedChannels (i4RadioType, &AllowedList);

    if (i4RadioType == CLI_RADIO_TYPEA)
    {
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELA; u1Channel++)
        {
            if (u4TestValFsRrmAPAssignedChannel == au1AllowedList[u1Channel])
            {
                u1Flag++;
                break;
            }
        }
    }
    else
    {
        for (u1Channel = 0; u1Channel < RFMGMT_MAX_CHANNELB; u1Channel++)
        {
            if (u4TestValFsRrmAPAssignedChannel == au1AllowedList[u1Channel])
            {
                u1Flag++;
                break;
            }
        }
    }
    if (u1Flag == 0)
    {
        return SNMP_FAILURE;
    }

    if (RadioIfValidateAllowedChannels ((UINT4) i4IfIndex,
                                        (UINT1) u4TestValFsRrmAPAssignedChannel,
                                        au1AllowedChannelList) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
#endif
    UNUSED_PARAM (u4TestValFsRrmAPAssignedChannel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhTestv2FsRrm11hTPCRequestInterval
 * Input       :  The Indices
 *                 IfIndex
 *                 The Object
 *                 testValFsRrm11hTPCRequestInterval
 * Output      :  The Test Low Lev Routine Take the Indices &
 *               Test whether that Value is Valid Input for Set.
 *                Stores the value of error code in the Return val
 * Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsRrm11hTPCRequestInterval (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     UINT4 u4TestValFsRrm11hTPCRequestInterval)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }

    u4RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    if ((u4RadioType == RFMGMT_RADIO_TYPEAN) ||
        (u4RadioType == RFMGMT_RADIO_TYPEA) ||
        ((UINT4) u4RadioType == RFMGMT_RADIO_TYPEAC))
    {
        if ((u4TestValFsRrm11hTPCRequestInterval <
             RFMGMT_MIN_REQUEST_INTERVAL_PERIOD) ||
            (u4TestValFsRrm11hTPCRequestInterval >
             RFMGMT_MAX_REQUEST_INTERVAL_PERIOD))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_INVALID_VALUE);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrmAPChannelAllowedList
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRrmAPChannelAllowedList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrmAPChannelAllowedList (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsRrmAPChannelAllowedList)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pTestValFsRrmAPChannelAllowedList);
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hDFSQuietInterval
 Input       :  The Indices
                IfIndex
 
                The Object
                testValFsRrm11hDFSQuietInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hDFSQuietInterval (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   UINT4 u4TestValFsRrm11hDFSQuietInterval)
{
    UNUSED_PARAM (i4IfIndex);
    if ((u4TestValFsRrm11hDFSQuietInterval < RFMGMT_MIN_DFS_QUIET_INTERVAL) ||
        (u4TestValFsRrm11hDFSQuietInterval > RFMGMT_MAX_DFS_QUIET_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hDFSQuietPeriod
 Input       :  The Indices
                IfIndex
                The Object
                testValFsRrm11hDFSQuietPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hDFSQuietPeriod (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4TestValFsRrm11hDFSQuietPeriod)
{
    UNUSED_PARAM (i4IfIndex);
    if ((u4TestValFsRrm11hDFSQuietPeriod < RFMGMT_MIN_DFS_QUIET_PERIOD) ||
        (u4TestValFsRrm11hDFSQuietPeriod > RFMGMT_MAX_DFS_QUIET_PERIOD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hDFSMeasurementInterval
 Input       :  The Indices
                IfIndex

                The Object
                testValFsRrm11hDFSMeasurementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhTestv2FsRrm11hDFSMeasurementInterval (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4
                                         u4TestValFsRrm11hDFSMeasurementInterval)
{
    UNUSED_PARAM (i4IfIndex);
    if ((u4TestValFsRrm11hDFSMeasurementInterval <
         RFMGMT_MIN_DFS_MEASUREMENT_INTERVAL)
        || (u4TestValFsRrm11hDFSMeasurementInterval >
            RFMGMT_MAX_DFS_MEASUREMENT_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hDFSChannelSwitchStatus
 Input       :  The Indices
                IfIndex

                The Object
                testValFsRrm11hDFSChannelSwitchStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hDFSChannelSwitchStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4
                                         u4TestValFsRrm11hDFSChannelSwitchStatus)
{
    UNUSED_PARAM (i4IfIndex);
    if ((u4TestValFsRrm11hDFSChannelSwitchStatus !=
         RFMGMT_CHANNEL_SWITCH_MSG_ENABLE)
        && (u4TestValFsRrm11hDFSChannelSwitchStatus !=
            RFMGMT_CHANNEL_SWITCH_MSG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsRrmAPConfigTable
Input       :  The Indices
IfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmAPConfigTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrmAPStatsTable. */
/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmAPStatsTable
Input       :  The Indices
IfIndex
FsRrmScannedChannel
FsRrmNeighborMacAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsRrmAPStatsTable (INT4 i4IfIndex,
                                           UINT4 u4FsRrmScannedChannel,
                                           tMacAddr FsRrmNeighborMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmScannedChannel);
    UNUSED_PARAM (FsRrmNeighborMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmAPStatsTable
Input       :  The Indices
IfIndex
FsRrmScannedChannel
FsRrmNeighborMacAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrmAPStatsTable (INT4 *pi4IfIndex,
                                   UINT4 *pu4FsRrmScannedChannel,
                                   tMacAddr * pFsRrmNeighborMacAddress)
{
    tRfMgmtNeighborScanDB *pRfMgmtNeighborScanDB = NULL;

    pRfMgmtNeighborScanDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB);

    if (pRfMgmtNeighborScanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtNeighborScanDB->u4RadioIfIndex;
    *pu4FsRrmScannedChannel = (UINT4) pRfMgmtNeighborScanDB->u2ScannedChannel;
    MEMCPY (pFsRrmNeighborMacAddress, pRfMgmtNeighborScanDB->NeighborAPMac,
            MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmAPStatsTable
Input       :  The Indices
IfIndex
nextIfIndex
FsRrmScannedChannel
nextFsRrmScannedChannel
FsRrmNeighborMacAddress
nextFsRrmNeighborMacAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmAPStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                  UINT4 u4FsRrmScannedChannel,
                                  UINT4 *pu4NextFsRrmScannedChannel,
                                  tMacAddr FsRrmNeighborMacAddress,
                                  tMacAddr * pNextFsRrmNeighborMacAddress)
{
    tRfMgmtNeighborScanDB *pRfMgmtNeighborScanDB = NULL;
    tRfMgmtNeighborScanDB RfMgmtNeighborScanDB;

    MEMSET (&RfMgmtNeighborScanDB, 0, sizeof (tRfMgmtNeighborScanDB));

    RfMgmtNeighborScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtNeighborScanDB.u2ScannedChannel = (UINT2) u4FsRrmScannedChannel;
    MEMCPY (RfMgmtNeighborScanDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    pRfMgmtNeighborScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtNeighborAPScanDB,
                       &RfMgmtNeighborScanDB, NULL);

    if (pRfMgmtNeighborScanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtNeighborScanDB->u4RadioIfIndex;
    *pu4NextFsRrmScannedChannel =
        (UINT4) pRfMgmtNeighborScanDB->u2ScannedChannel;
    MEMCPY (pNextFsRrmNeighborMacAddress, pRfMgmtNeighborScanDB->NeighborAPMac,
            MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmRSSIValue
Input       :  The Indices
IfIndex
FsRrmScannedChannel
FsRrmNeighborMacAddress

The Object 
retValFsRrmRSSIValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmRSSIValue (INT4 i4IfIndex, UINT4 u4FsRrmScannedChannel,
                      tMacAddr FsRrmNeighborMacAddress,
                      INT4 *pi4RetValFsRrmRSSIValue)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u2ScannedChannel = (UINT2) u4FsRrmScannedChannel;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanIsSetDB.bRssi = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmRSSIValue =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
        i2Rssi;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSNR
Input       :  The Indices
IfIndex
FsRrmScannedChannel
FsRrmNeighborMacAddress

The Object 
retValFsRrmSNR
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSNR (INT4 i4IfIndex, UINT4 u4FsRrmScannedChannel,
                tMacAddr FsRrmNeighborMacAddress, INT4 *pi4RetValFsRrmSNR)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u2ScannedChannel = (UINT2) u4FsRrmScannedChannel;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanIsSetDB.bSNRValue = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmSNR =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
        i2SNRValue;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmScannedChannel);
    UNUSED_PARAM (FsRrmNeighborMacAddress);
    UNUSED_PARAM (pi4RetValFsRrmSNR);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmLastUpdatedInterval
Input       :  The Indices
IfIndex
FsRrmScannedChannel
FsRrmNeighborMacAddress

The Object 
retValFsRrmLastUpdatedInterval
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmLastUpdatedInterval (INT4 i4IfIndex, UINT4 u4FsRrmScannedChannel,
                                tMacAddr FsRrmNeighborMacAddress,
                                UINT4 *pu4RetValFsRrmLastUpdatedInterval)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u2ScannedChannel = (UINT2) u4FsRrmScannedChannel;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanIsSetDB.bLastUpdatedTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmLastUpdatedInterval =
        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
        u4LastUpdatedTime;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmScannedChannel);
    UNUSED_PARAM (FsRrmNeighborMacAddress);
    UNUSED_PARAM (pu4RetValFsRrmLastUpdatedInterval);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmNeighborMsgRcvdCount
Input       :  The Indices
IfIndex
FsRrmScannedChannel
FsRrmNeighborMacAddress

The Object 
retValFsRrmNeighborMsgRcvdCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmNeighborMsgRcvdCount (INT4 i4IfIndex,
                                 UINT4 u4FsRrmScannedChannel,
                                 tMacAddr FsRrmNeighborMacAddress,
                                 UINT4 *pu4RetValFsRrmNeighborMsgRcvdCount)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanDB.u2ScannedChannel = (UINT2) u4FsRrmScannedChannel;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
            RfMgmtNeighborScanDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.
        RfMgmtNeighborScanIsSetDB.bNeighborMsgCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_NEIGHBOR_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmNeighborMsgRcvdCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtNeighborScanTable.RfMgmtNeighborScanDB.
        u4NeighborMsgCount;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmScannedChannel);
    UNUSED_PARAM (FsRrmNeighborMacAddress);
    UNUSED_PARAM (pu4RetValFsRrmNeighborMsgRcvdCount);
#endif

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrmTpcConfigTable. */
/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmTpcConfigTable
Input       :  The Indices
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmTpcConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmTpcConfigTable
Input       :  The Indices
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrmTpcConfigTable (INT4 *pi4IfIndex)
{
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;

    pRfMgmtClientConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB);

    if (pRfMgmtClientConfigDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtClientConfigDB->u4RadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmTpcConfigTable
Input       :  The Indices
IfIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmTpcConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;
    tRfMgmtClientConfigDB RfMgmtClientConfigDB;

    MEMSET (&RfMgmtClientConfigDB, 0, sizeof (tRfMgmtClientConfigDB));

    RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    pRfMgmtClientConfigDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                       &RfMgmtClientConfigDB, NULL);

    if (pRfMgmtClientConfigDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtClientConfigDB->u4RadioIfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmAPSNRScanStatus
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPSNRScanStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPSNRScanStatus (INT4 i4IfIndex, INT4 *pi4RetValFsRrmAPSNRScanStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bSNRScanStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmAPSNRScanStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u1SNRScanStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSNRScanFreq
Input       :  The Indices
IfIndex
The Object 
retValFsRrmSNRScanFreq
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSNRScanFreq (INT4 i4IfIndex, UINT4 *pu4RetValFsRrmSNRScanFreq)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bSNRScanPeriod = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmSNRScanFreq =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u2SNRScanPeriod;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSNRScanCount
Input       :  The Indices
IfIndex
The Object 
retValFsRrmSNRScanCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSNRScanCount (INT4 i4IfIndex, UINT4 *pu4RetValFsRrmSNRScanCount)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bSNRScanCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmSNRScanCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4SNRScanCount;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRrmSNRScanCount);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmBelowSNRThresholdCount
Input       :  The Indices
IfIndex
The Object 
retValFsRrmBelowSNRThresholdCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmBelowSNRThresholdCount (INT4 i4IfIndex,
                                   UINT4 *pu4RetValFsRrmBelowSNRThresholdCount)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bBelowSNRCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmBelowSNRThresholdCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4BelowSNRCount;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRrmBelowSNRThresholdCount);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTxPowerChangeCount
Input       :  The Indices
IfIndex
The Object 
retValFsRrmTxPowerChangeCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTxPowerChangeCount (INT4 i4IfIndex,
                               UINT4 *pu4RetValFsRrmTxPowerChangeCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerChangeCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmTxPowerChangeCount =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u1TxPowerChangeCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmClientsConnected
Input       :  The Indices
IfIndex
The Object 
retValFsRrmClientsConnected
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmClientsConnected (INT4 i4IfIndex,
                             UINT4 *pu4RetValFsRrmClientsConnected)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bClientsConnected = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmClientsConnected =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2ClientsConnected;

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRrmClientsConnected);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmClientsAccepted
Input       :  The Indices
IfIndex
The Object 
retValFsRrmClientsAccepted
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmClientsAccepted (INT4 i4IfIndex,
                            UINT4 *pu4RetValFsRrmClientsAccepted)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bClientsAccepted = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmClientsAccepted =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2ClientsAccepted;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRrmClientsAccepted);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmClientsDiscarded
Input       :  The Indices
IfIndex
The Object 
retValFsRrmClientsDiscarded
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmClientsDiscarded (INT4 i4IfIndex,
                             UINT4 *pu4RetValFsRrmClientsDiscarded)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bClientsDiscarded = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmClientsDiscarded =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2ClientsDiscarded;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRrmClientsDiscarded);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsRrmAPTpcMode
Input       :  The Indices
IfIndex
The Object 
retValFsRrmAPTpcMode
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmAPTpcMode (INT4 i4IfIndex, INT4 *pi4RetValFsRrmAPTpcMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bPerAPTpcSelection = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmAPTpcMode =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.
        u1PerAPTpcSelection;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmAPSNRScanStatus
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPSNRScanStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPSNRScanStatus (INT4 i4IfIndex, INT4 i4SetValFsRrmAPSNRScanStatus)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bSNRScanStatus = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u1SNRScanStatus ==
        (UINT1) i4SetValFsRrmAPSNRScanStatus)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u1SNRScanStatus =
        (UINT1) i4SetValFsRrmAPSNRScanStatus;

    u1RetVal = RfMgmtProcessClientConfig (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        /* If auto scan is disabled, then delete the client entries 
         * from DB */
        if (i4SetValFsRrmAPSNRScanStatus == RFMGMT_SNR_SCAN_DISABLE)
        {
            RfMgmtDeleteClientDetails ((UINT4) i4IfIndex);
        }
        if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
            != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "Processing Client config failed\r\n");
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsRrmAPSNRScanStatus);
    return SNMP_SUCCESS;
#endif
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsRrmSNRScanFreq
Input       :  The Indices
IfIndex

The Object 
setValFsRrmSNRScanFreq
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmSNRScanFreq (INT4 i4IfIndex, UINT4 u4SetValFsRrmSNRScanFreq)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bSNRScanPeriod = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2SNRScanPeriod ==
        (UINT4) u4SetValFsRrmSNRScanFreq)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2SNRScanPeriod = (UINT2) u4SetValFsRrmSNRScanFreq;

    u1RetVal = RfMgmtProcessClientConfig (&RfMgmtDB);

    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY, &RfMgmtDB) !=
            RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRrmSNRScanFreq);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmAPTpcMode
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPTpcMode
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPTpcMode (INT4 i4IfIndex, INT4 i4SetValFsRrmAPTpcMode)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.RfMgmtAPConfigDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigIsSetDB.bPerAPTpcSelection = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AP_CONFIG_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u1PerAPTpcSelection == (UINT1) i4SetValFsRrmAPTpcMode)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtApConfigTable.
        RfMgmtAPConfigDB.u1PerAPTpcSelection = (UINT1) i4SetValFsRrmAPTpcMode;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AP_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmAPTxPowerLevel
Input       :  The Indices
IfIndex

The Object 
setValFsRrmAPTxPowerLevel
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmAPTxPowerLevel (INT4 i4IfIndex, UINT4 u4SetValFsRrmAPTxPowerLevel)
{
    tRfMgmtDB           RfMgmtDB;
    tRadioIfGetDB       RadioIfDB;
    UINT1               u1RetVal = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerLevel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2TxPowerLevel ==
        (UINT1) u4SetValFsRrmAPTxPowerLevel)
    {
        return SNMP_SUCCESS;
    }
    RadioIfDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        return SNMP_FAILURE;
    }

    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    RadioIfDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.u2CurrentTxPowerLevel =
        (UINT2) u4SetValFsRrmAPTxPowerLevel;

    if (RadioIfSetTxPowerTable (&RadioIfDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return SNMP_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u2TxPowerLevel =
        (UINT1) u4SetValFsRrmAPTxPowerLevel;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerChangeCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsRrmAPSNRScanStatus
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPSNRScanStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPSNRScanStatus (UINT4 *pu4ErrorCode,
                               INT4 i4IfIndex,
                               INT4 i4TestValFsRrmAPSNRScanStatus)
{
    if ((i4TestValFsRrmAPSNRScanStatus != RFMGMT_SNR_SCAN_ENABLE) &&
        (i4TestValFsRrmAPSNRScanStatus != RFMGMT_SNR_SCAN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmSNRScanFreq
Input       :  The Indices
IfIndex

The Object 
testValFsRrmSNRScanFreq
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmSNRScanFreq (UINT4 *pu4ErrorCode,
                           INT4 i4IfIndex, UINT4 u4TestValFsRrmSNRScanFreq)
{
    if ((u4TestValFsRrmSNRScanFreq < RFMGMT_MIN_SNR_SCAN_PERIOD) ||
        (u4TestValFsRrmSNRScanFreq > RFMGMT_MAX_SNR_SCAN_PERIOD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmAPTpcMode
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPTpcMode
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPTpcMode (UINT4 *pu4ErrorCode,
                         INT4 i4IfIndex, INT4 i4TestValFsRrmAPTpcMode)
{
#ifdef WLC_WANTED
    INT4                i4TpcMode = 0;
    INT4                i4FsRrmRadioType = 0;

    if (nmhGetFsDot11RadioType (i4IfIndex, (UINT4 *) &i4FsRrmRadioType) ==
        SNMP_SUCCESS)
    {
        nmhGetFsRrmTpcMode (i4FsRrmRadioType, &i4TpcMode);
        if (i4TpcMode != RFMGMT_TPC_MODE_PER_AP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmAPTpcMode != RFMGMT_PER_AP_TPC_GLOBAL) &&
        (i4TestValFsRrmAPTpcMode != RFMGMT_PER_AP_TPC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
#endif
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsRrmAPTpcMode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsRrmAPTxPowerLevel
Input       :  The Indices
IfIndex

The Object 
testValFsRrmAPTxPowerLevel
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmAPTxPowerLevel (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4TestValFsRrmAPTxPowerLevel)
{
#ifdef WLC_WANTED
    INT4                i4RadioType = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TxPower = 0;

    if (nmhGetFsDot11RadioType (i4IfIndex, (UINT4 *) &i4RadioType) !=
        SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }

    nmhGetFsRrmTpcMode (i4RadioType, &i4TpcMode);
    if (i4TpcMode != RFMGMT_TPC_MODE_PER_AP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    nmhGetFsRrmAPTpcMode (i4IfIndex, &i4TxPower);
    if (i4TxPower != RFMGMT_PER_AP_TPC_MANUAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
#endif
    UNUSED_PARAM (u4TestValFsRrmAPTxPowerLevel);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsRrmTpcConfigTable
Input       :  The Indices
IfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmTpcConfigTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrmTpcClientTable. */
/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmTpcClientTable
Input       :  The Indices
IfIndex
FsRrmClientMacAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsRrmTpcClientTable (INT4 i4IfIndex,
                                             tMacAddr FsRrmClientMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrmClientMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmTpcClientTable
Input       :  The Indices
IfIndex
FsRrmClientMacAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrmTpcClientTable (INT4 *pi4IfIndex,
                                     tMacAddr * pFsRrmClientMacAddress)
{
    tRfMgmtClientScanDB *pRfMgmtClientScanDB = NULL;

    pRfMgmtClientScanDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB);

    if (pRfMgmtClientScanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtClientScanDB->u4RadioIfIndex;
    MEMCPY (pFsRrmClientMacAddress, pRfMgmtClientScanDB->ClientMacAddress,
            MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmTpcClientTable
Input       :  The Indices
IfIndex
nextIfIndex
FsRrmClientMacAddress
nextFsRrmClientMacAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmTpcClientTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    tMacAddr FsRrmClientMacAddress,
                                    tMacAddr * pNextFsRrmClientMacAddress)
{
    tRfMgmtClientScanDB *pRfMgmtClientScanDB = NULL;
    tRfMgmtClientScanDB RfMgmtClientScanDB;

    MEMSET (&RfMgmtClientScanDB, 0, sizeof (tRfMgmtClientScanDB));

    RfMgmtClientScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtClientScanDB.ClientMacAddress, FsRrmClientMacAddress,
            MAC_ADDR_LEN);

    pRfMgmtClientScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientScanDB,
                       &RfMgmtClientScanDB, NULL);

    if (pRfMgmtClientScanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtClientScanDB->u4RadioIfIndex;
    MEMCPY (pNextFsRrmClientMacAddress, pRfMgmtClientScanDB->ClientMacAddress,
            MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmClientSNR
Input       :  The Indices
IfIndex
FsRrmClientMacAddress

The Object 
retValFsRrmClientSNR
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmClientSNR (INT4 i4IfIndex,
                      tMacAddr FsRrmClientMacAddress,
                      INT4 *pi4RetValFsRrmClientSNR)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
        RfMgmtClientScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
            RfMgmtClientScanDB.ClientMacAddress, FsRrmClientMacAddress,
            MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
        RfMgmtClientScanIsSetDB.bClientSNR = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmClientSNR =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
        i2ClientSNR;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmClientLastSNRScan
Input       :  The Indices
IfIndex
FsRrmClientMacAddress

The Object 
retValFsRrmClientLastSNRScan
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmClientLastSNRScan (INT4 i4IfIndex,
                              tMacAddr FsRrmClientMacAddress,
                              UINT4 *pu4RetValFsRrmClientLastSNRScan)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
        RfMgmtClientScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
            RfMgmtClientScanDB.ClientMacAddress, FsRrmClientMacAddress,
            MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.
        RfMgmtClientScanIsSetDB.bLastClientSNRScan = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmClientLastSNRScan =
        RfMgmtDB.unRfMgmtDB.RfMgmtClientScanTable.RfMgmtClientScanDB.
        u4LastUpdatedTime;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsRrmTpcScanTable. */
/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmTpcScanTable
Input       :  The Indices
IfIndex
FsRrmWlanId
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsRrmTpcScanTable (INT4 i4IfIndex, UINT4 u4FsRrmWlanId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmWlanId);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmTpcScanTable
Input       :  The Indices
IfIndex
FsRrmWlanId
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrmTpcScanTable (INT4 *pi4IfIndex, UINT4 *pu4FsRrmWlanId)
{
#ifdef WLC_WANTED
    tRfMgmtBssidScanDB *pRfMgmtBssidScanDB = NULL;

    pRfMgmtBssidScanDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB);

    if (pRfMgmtBssidScanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtBssidScanDB->u4RadioIfIndex;
    *pu4FsRrmWlanId = pRfMgmtBssidScanDB->u1WlanID;
#else
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pu4FsRrmWlanId);
    *pi4IfIndex = SYS_DEF_MAX_ENET_INTERFACES + 1;
    *pu4FsRrmWlanId = 1;
    return SNMP_SUCCESS;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmTpcScanTable
Input       :  The Indices
IfIndex
nextIfIndex
FsRrmWlanId
nextFsRrmWlanId
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmTpcScanTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                  UINT4 u4FsRrmWlanId,
                                  UINT4 *pu4NextFsRrmWlanId)
{
#ifdef WLC_WANTED
    tRfMgmtBssidScanDB *pRfMgmtBssidScanDB = NULL;
    tRfMgmtBssidScanDB  RfMgmtBssidScanDB;

    MEMSET (&RfMgmtBssidScanDB, 0, sizeof (tRfMgmtBssidScanDB));

    RfMgmtBssidScanDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtBssidScanDB.u1WlanID = (UINT1) u4FsRrmWlanId;
    pRfMgmtBssidScanDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtBssidScanDB,
                       &RfMgmtBssidScanDB, NULL);

    if (pRfMgmtBssidScanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtBssidScanDB->u4RadioIfIndex;
    *pu4NextFsRrmWlanId = pRfMgmtBssidScanDB->u1WlanID;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (u4FsRrmWlanId);
    UNUSED_PARAM (pu4NextFsRrmWlanId);
    *pi4NextIfIndex = SYS_DEF_MAX_ENET_INTERFACES + 1;

    if (u4FsRrmWlanId < MAX_RFMGMT_WLAN_ID)
    {
        *pu4NextFsRrmWlanId = ++u4FsRrmWlanId;
    }
    else
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmSSIDScanStatus
Input       :  The Indices
IfIndex
FsRrmWlanId

The Object 
retValFsRrmSSIDScanStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSSIDScanStatus (INT4 i4IfIndex, UINT4 u4FsRrmWlanId,
                           INT4 *pi4RetValFsRrmSSIDScanStatus)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
        RfMgmtBssidScanIsSetDB.bAutoScanStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.u1WlanID =
        (UINT1) u4FsRrmWlanId;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_BSSID_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmSSIDScanStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
        u1AutoScanStatus;

#else
    UINT1               u1RetVal = 0;
    UINT1               u1WlanId = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bBssidScanStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.u1WlanId =
        (UINT1) u4FsRrmWlanId;
    u1WlanId = (UINT1) u4FsRrmWlanId;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmSSIDScanStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u1BssidScanStatus[u1WlanId - 1];

#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmSSIDScanStatus
Input       :  The Indices
IfIndex
FsRrmWlanId

The Object 
setValFsRrmSSIDScanStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmSSIDScanStatus (INT4 i4IfIndex, UINT4 u4FsRrmWlanId,
                           INT4 i4SetValFsRrmSSIDScanStatus)
{
#ifdef WLC_WANTED
    UINT1               u1RetVal = 0;
    UINT1               u1Opcode = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.u4RadioIfIndex =
        (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
        RfMgmtBssidScanIsSetDB.bAutoScanStatus = OSIX_TRUE;
    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.u1WlanID =
        (UINT1) u4FsRrmWlanId;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_BSSID_SCAN_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        u1Opcode = RFMGMT_CREATE_BSSID_SCAN_ENTRY;
    }
    else
    {
        if (RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.RfMgmtBssidScanDB.
            u1AutoScanStatus == (UINT1) i4SetValFsRrmSSIDScanStatus)
        {
            return RFMGMT_SUCCESS;
        }
        u1Opcode = RFMGMT_SET_BSSID_SCAN_ENTRY;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtBssidScanTable.
        RfMgmtBssidScanDB.u1AutoScanStatus =
        (UINT1) i4SetValFsRrmSSIDScanStatus;

    u1RetVal = RfMgmtProcessBssidScanStatus (&RfMgmtDB);
    if (u1RetVal == RFMGMT_SUCCESS)
    {
        if (RfMgmtProcessDBMsg (u1Opcode, &RfMgmtDB) != RFMGMT_SUCCESS)
        {
            RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
            return SNMP_FAILURE;
        }
        if (i4SetValFsRrmSSIDScanStatus == RFMGMT_BSSID_SCAN_STATUS_DISABLE)
        {
            if (RfMgmtDeleteClientDBforWlanId ((UINT1) u4FsRrmWlanId,
                                               (UINT4) i4IfIndex) !=
                RFMGMT_SUCCESS)
            {
                RFMGMT_TRC (RFMGMT_FAILURE_TRC,
                            "Client DB deletion Failed\r\n");
            }
        }
        return SNMP_SUCCESS;
    }
    else
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmWlanId);
    UNUSED_PARAM (i4SetValFsRrmSSIDScanStatus);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsRrmSSIDScanStatus
Input       :  The Indices
IfIndex
FsRrmWlanId

The Object 
testValFsRrmSSIDScanStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmSSIDScanStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4FsRrmWlanId,
                              INT4 i4TestValFsRrmSSIDScanStatus)
{
    if ((i4TestValFsRrmSSIDScanStatus != RFMGMT_SNR_SCAN_ENABLE) &&
        (i4TestValFsRrmSSIDScanStatus != RFMGMT_SNR_SCAN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRrmWlanId);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhDepv2FsRrmTpcScanTable
Input       :  The Indices
IfIndex
FsRrmWlanId
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmTpcScanTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetFsRrmChannelChangeTrapStatus
Input       :  The Indices

The Object 
retValFsRrmChannelChangeTrapStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmChannelChangeTrapStatus (INT4 *pi4RetValFsRrmChannelChangeTrapStatus)
{
    *pi4RetValFsRrmChannelChangeTrapStatus = (INT4) gu4ChannelChangeTrapStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTxPowerChangeTrapStatus
Input       :  The Indices

The Object 
retValFsRrmTxPowerChangeTrapStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTxPowerChangeTrapStatus (INT4 *pi4RetValFsRrmTxPowerChangeTrapStatus)
{
    *pi4RetValFsRrmTxPowerChangeTrapStatus = (INT4) gu4TxPowerTrapStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
Function    :  nmhSetFsRrmChannelChangeTrapStatus
Input       :  The Indices

The Object 
setValFsRrmChannelChangeTrapStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmChannelChangeTrapStatus (INT4 i4SetValFsRrmChannelChangeTrapStatus)
{
    if (gu4ChannelChangeTrapStatus ==
        (UINT4) i4SetValFsRrmChannelChangeTrapStatus)
    {
        return SNMP_SUCCESS;
    }
    gu4ChannelChangeTrapStatus = (UINT4) i4SetValFsRrmChannelChangeTrapStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmTxPowerChangeTrapStatus
Input       :  The Indices

The Object 
setValFsRrmTxPowerChangeTrapStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmTxPowerChangeTrapStatus (INT4 i4SetValFsRrmTxPowerChangeTrapStatus)
{
    if (gu4TxPowerTrapStatus == (UINT4) i4SetValFsRrmTxPowerChangeTrapStatus)
    {
        return SNMP_SUCCESS;
    }
    gu4TxPowerTrapStatus = (UINT4) i4SetValFsRrmTxPowerChangeTrapStatus;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsRrmChannelChangeTrapStatus
Input       :  The Indices

The Object 
testValFsRrmChannelChangeTrapStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmChannelChangeTrapStatus (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsRrmChannelChangeTrapStatus)
{
    if ((i4TestValFsRrmChannelChangeTrapStatus !=
         RFMGMT_CHANNEL_CHANGE_TRAP_ENABLE) &&
        (i4TestValFsRrmChannelChangeTrapStatus !=
         RFMGMT_CHANNEL_CHANGE_TRAP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmTxPowerChangeTrapStatus
Input       :  The Indices

The Object 
testValFsRrmTxPowerChangeTrapStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmTxPowerChangeTrapStatus (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsRrmTxPowerChangeTrapStatus)
{
    if ((i4TestValFsRrmTxPowerChangeTrapStatus !=
         RFMGMT_TX_POWER_CHANGE_TRAP_ENABLE) &&
        (i4TestValFsRrmTxPowerChangeTrapStatus !=
         RFMGMT_TX_POWER_CHANGE_TRAP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
Function    :  nmhDepv2FsRrmChannelChangeTrapStatus
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmChannelChangeTrapStatus (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsRrmTxPowerChangeTrapStatus
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmTxPowerChangeTrapStatus (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmExtConfigTable
Input       :  The Indices
FsRrmRadioType
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmExtConfigTable (INT4 i4FsRrmRadioType)
{
    UNUSED_PARAM (i4FsRrmRadioType);
    return SNMP_SUCCESS;
}

#ifdef WLC_WANTED
/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmExtConfigTable
Input       :  The Indices
FsRrmRadioType
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRrmExtConfigTable (INT4 *pi4FsRrmRadioType)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;

    pRfMgmtAutoRfProfileDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB);

    if (pRfMgmtAutoRfProfileDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FsRrmRadioType = (INT4) pRfMgmtAutoRfProfileDB->u4Dot11RadioType;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmExtConfigTable
Input       :  The Indices
FsRrmRadioType
nextFsRrmRadioType
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmExtConfigTable (INT4 i4FsRrmRadioType,
                                    INT4 *pi4NextFsRrmRadioType)
{
    tRfMgmtAutoRfProfileDB *pRfMgmtAutoRfProfileDB = NULL;
    tRfMgmtAutoRfProfileDB RfMgmtAutoRfProfileDB;

    MEMSET (&RfMgmtAutoRfProfileDB, 0, sizeof (tRfMgmtAutoRfProfileDB));

    RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;

    pRfMgmtAutoRfProfileDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtAutoRfDB,
                       &RfMgmtAutoRfProfileDB, NULL);

    if (pRfMgmtAutoRfProfileDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsRrmRadioType = (INT4) pRfMgmtAutoRfProfileDB->u4Dot11RadioType;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhDepv2FsRrmExtConfigTable
Input       :  The Indices
FsRrmRadioType
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsRrmExtConfigTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrmFailedAPStatsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmFailedAPStatsTable
Input       :  The Indices
IfIndex
FsRrmNeighborMacAddress
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmFailedAPStatsTable (INT4 i4IfIndex,
                                                 tMacAddr
                                                 FsRrmNeighborMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrmNeighborMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmFailedAPStatsTable
Input       :  The Indices
IfIndex
FsRrmNeighborMacAddress
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRrmFailedAPStatsTable (INT4 *pi4IfIndex,
                                         tMacAddr * pFsRrmNeighborMacAddress)
{
    tRfMgmtFailedAPNeighborListDB *pRfMgmtFailedAPNeighborListDB = NULL;

    pRfMgmtFailedAPNeighborListDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB);
    if (pRfMgmtFailedAPNeighborListDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex;

    MEMCPY (pFsRrmNeighborMacAddress,
            pRfMgmtFailedAPNeighborListDB->NeighborAPMac, MAC_ADDR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmFailedAPStatsTable
Input       :  The Indices
IfIndex
nextIfIndex
FsRrmNeighborMacAddress
nextFsRrmNeighborMacAddress
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmFailedAPStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                        tMacAddr FsRrmNeighborMacAddress,
                                        tMacAddr * pNextFsRrmNeighborMacAddress)
{
    tRfMgmtFailedAPNeighborListDB *pRfMgmtFailedAPNeighborListDB = NULL;
    tRfMgmtFailedAPNeighborListDB RfMgmtFailedAPNeighborListDB;

    MEMSET (&RfMgmtFailedAPNeighborListDB, 0,
            sizeof (tRfMgmtFailedAPNeighborListDB));
    RfMgmtFailedAPNeighborListDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtFailedAPNeighborListDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    pRfMgmtFailedAPNeighborListDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                       &RfMgmtFailedAPNeighborListDB, NULL);

    if (pRfMgmtFailedAPNeighborListDB == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIfIndex = (INT4) pRfMgmtFailedAPNeighborListDB->u4RadioIfIndex;
    MEMCPY (pNextFsRrmNeighborMacAddress,
            pRfMgmtFailedAPNeighborListDB->NeighborAPMac, MAC_ADDR_LEN);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsRrmSHARSSIValue
Input       :  The Indices
IfIndex
FsRrmNeighborMacAddress

The Object 
retValFsRrmSHARSSIValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSHARSSIValue (INT4 i4IfIndex, tMacAddr FsRrmNeighborMacAddress,
                         INT4 *pi4RetValFsRrmSHARSSIValue)
{
    tRfMgmtFailedAPNeighborListDB *pRfMgmtFailedAPNeighborListDB = NULL;
    tRfMgmtFailedAPNeighborListDB RfMgmtFailedAPNeighborListDB;

    MEMSET (&RfMgmtFailedAPNeighborListDB, 0,
            sizeof (tRfMgmtFailedAPNeighborListDB));

    RfMgmtFailedAPNeighborListDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtFailedAPNeighborListDB.NeighborAPMac, FsRrmNeighborMacAddress,
            MAC_ADDR_LEN);

    pRfMgmtFailedAPNeighborListDB =
        RBTreeGet (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtFailedAPNeighborDB,
                   (tRBElem *) & RfMgmtFailedAPNeighborListDB);

    if (pRfMgmtFailedAPNeighborListDB == NULL)
    {
        return SNMP_SUCCESS;
    }

    *pi4RetValFsRrmSHARSSIValue = (INT4) pRfMgmtFailedAPNeighborListDB->i2Rssi;

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsRrmStatsTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsRrmStatsTable
Input       :  The Indices
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmStatsTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsRrmStatsTable
Input       :  The Indices
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRrmStatsTable (INT4 *pi4IfIndex)
{
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;

    pRfMgmtClientConfigDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB);

    if (pRfMgmtClientConfigDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtClientConfigDB->u4RadioIfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsRrmStatsTable
Input       :  The Indices
IfIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrmStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRfMgmtClientConfigDB *pRfMgmtClientConfigDB = NULL;
    tRfMgmtClientConfigDB RfMgmtClientConfigDB;

    MEMSET (&RfMgmtClientConfigDB, 0, sizeof (tRfMgmtClientConfigDB));

    RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    pRfMgmtClientConfigDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtClientSNRConfigDB,
                       &RfMgmtClientConfigDB, NULL);

    if (pRfMgmtClientConfigDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtClientConfigDB->u4RadioIfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsRrmTxPowerIncreaseCount
Input       :  The Indices
IfIndex

The Object 
retValFsRrmTxPowerIncreaseCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTxPowerIncreaseCount (INT4 i4IfIndex,
                                 UINT4 *pu4RetValFsRrmTxPowerIncreaseCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerIncreaseCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmTxPowerIncreaseCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4TxPowerIncreaseCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 * Function    :  nmhValidateIndexInstanceFsRrm11hDfsInfoTable
 * Input       :  The Indices
 *                   IfIndex
 *                    FsRrm11hStationMacAddress
 * Output      :  The Routines Validates the Given Indices.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrm11hDfsInfoTable (INT4 i4IfIndex,
                                              tMacAddr
                                              FsRrm11hStationMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFirstIndexFsRrm11hTpcInfoTable
 *   Input       :  The Indices
 *   IfIndex
 *  FsRrm11hStationMacAddress
 * Output      :  The Get First Routines gets the Lexicographicaly
 *                First Entry from the Table.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrm11hDfsInfoTable (INT4 *pi4IfIndex,
                                      tMacAddr * pFsRrm11hStationMacAddress)
{
#ifdef WLC_WANTED
    tRfMgmtDot11hDfsInfoDB *pRfMgmtDot11hDfsInfoDB = NULL;

    pRfMgmtDot11hDfsInfoDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB);

    if (pRfMgmtDot11hDfsInfoDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex;
    MEMCPY (pFsRrm11hStationMacAddress,
            pRfMgmtDot11hDfsInfoDB->RfMgmt11hStationMacAddress, MAC_ADDR_LEN);
#else

    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pFsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 * Function    :  nmhGetNextIndexFsRrm11hDfsInfoTable
 * Input       :  The Indices
 *                   IfIndex
 *   nextIfIndex    FsRrm11hStationMacAddress
 *   nextFsRrm11hStationMacAddress
 * Output      :  The Get Next function gets the Next Index for
 *                the Index Value given in the Index Values. The
 *               Indices are stored in the next_varname variables.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetNextIndexFsRrm11hDfsInfoTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     tMacAddr FsRrm11hStationMacAddress,
                                     tMacAddr * pNextFsRrm11hStationMacAddress)
{
#ifdef WLC_WANTED
    tRfMgmtDot11hDfsInfoDB *pRfMgmtDot11hDfsInfoDB = NULL;
    tRfMgmtDot11hDfsInfoDB RfMgmtDot11hDfsInfoDB;

    MEMSET (&RfMgmtDot11hDfsInfoDB, 0, sizeof (tRfMgmtDot11hDfsInfoDB));

    RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;

    MEMCPY (RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    pRfMgmtDot11hDfsInfoDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hDfsInfoDB,
                       &RfMgmtDot11hDfsInfoDB, NULL);

    if (pRfMgmtDot11hDfsInfoDB == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIfIndex = (INT4) pRfMgmtDot11hDfsInfoDB->u4RfMgmt11hRadioIfIndex;

    MEMCPY (pNextFsRrm11hStationMacAddress,
            pRfMgmtDot11hDfsInfoDB->RfMgmt11hStationMacAddress, MAC_ADDR_LEN);
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    UNUSED_PARAM (pNextFsRrm11hStationMacAddress);

    return SNMP_SUCCESS;
#endif
    return SNMP_SUCCESS;
}

INT1
nmhGetFsRrm11hDfsReportLastReceived (INT4 i4IfIndex,
                                     tMacAddr FsRrm11hStationMacAddress,
                                     UINT4
                                     *pu4RetValFsRrm11hDfsReportLastReceived)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
            RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hDfsReportLastReceived = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_DFS_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hDfsReportLastReceived =
        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.RfMgmtDot11hDfsInfoDB.
        u4RfMgmt11hDfsReportLastReceived;
    return SNMP_SUCCESS;
}

INT1
nmhGetFsRrm11hMeasureMapRadar (INT4 i4IfIndex,
                               tMacAddr FsRrm11hStationMacAddress,
                               INT4 *pi4RetValFsRrm11hIsRadarPresent)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
            RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hIsRadarPresent = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_DFS_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hIsRadarPresent =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoDB.bIsRadarPresent;
    return SNMP_SUCCESS;
}

INT1
nmhGetFsRrm11hChannelNumber (INT4 i4IfIndex,
                             tMacAddr FsRrm11hStationMacAddress,
                             UINT1 *pi4RetValFsRrm11hIsChannelPresent)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
            RfMgmtDot11hDfsInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoIsSetDB.bRfMgmt11hChannelNum = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_DFS_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hIsChannelPresent =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtDot11hDfsInfoTable.
        RfMgmtDot11hDfsInfoDB.u1ChannelNum;
    return SNMP_SUCCESS;
}

INT1
nmhDepv2FsRrm11hDfsInfoTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#if 0
/****************************************************************************
 *  Function    :  nmhGetFsRrm11hTxPowerLevel
 *   Input       :  The Indices
 *                  IfIndex
 *                  FsRrm11hStationMacAddress
 *                    The Object
 *                  retValFsRrm11hTxPowerLevel
 * Output      :  The Get Low Lev Routine Take the Indices &
 * store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/

INT1
nmhGetFsRrm11hTxPowerLevel (INT4 i4IfIndex, tMacAddr FsRrm11hStationMacAddress,
                            INT4 *pi4RetValFsRrm11hTxPowerLevel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTxPowerLevel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hTxPowerLevel =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel;

    return SNMP_SUCCESS;
}
#endif
/* LOW LEVEL Routines for Table : FsRrm11hTpcInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRrm11hTpcInfoTable
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrm11hTpcInfoTable (INT4 i4IfIndex,
                                              tMacAddr
                                              FsRrm11hStationMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRrm11hTpcInfoTable
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsRrm11hTpcInfoTable (INT4 *pi4IfIndex,
                                      tMacAddr * pFsRrm11hStationMacAddress)
{
#ifdef WLC_WANTED
    tRfMgmtDot11hTpcInfoDB *pRfMgmtDot11hTpcInfoDB = NULL;

    pRfMgmtDot11hTpcInfoDB =
        RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB);

    if (pRfMgmtDot11hTpcInfoDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex;
    MEMCPY (pFsRrm11hStationMacAddress,
            pRfMgmtDot11hTpcInfoDB->RfMgmt11hStationMacAddress, MAC_ADDR_LEN);
#else
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pFsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRrm11hTpcInfoTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsRrm11hStationMacAddress
                nextFsRrm11hStationMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRrm11hTpcInfoTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     tMacAddr FsRrm11hStationMacAddress,
                                     tMacAddr * pNextFsRrm11hStationMacAddress)
{
#ifdef WLC_WANTED
    tRfMgmtDot11hTpcInfoDB *pRfMgmtDot11hTpcInfoDB = NULL;
    tRfMgmtDot11hTpcInfoDB RfMgmtDot11hTpcInfoDB;

    MEMSET (&RfMgmtDot11hTpcInfoDB, 0, sizeof (tRfMgmtDot11hTpcInfoDB));

    RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;

    MEMCPY (RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    pRfMgmtDot11hTpcInfoDB =
        RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.RfMgmtDot11hTpcInfoDB,
                       &RfMgmtDot11hTpcInfoDB, NULL);

    if (pRfMgmtDot11hTpcInfoDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pRfMgmtDot11hTpcInfoDB->u4RfMgmt11hRadioIfIndex;

    MEMCPY (pNextFsRrm11hStationMacAddress,
            pRfMgmtDot11hTpcInfoDB->RfMgmt11hStationMacAddress, MAC_ADDR_LEN);
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    UNUSED_PARAM (pNextFsRrm11hStationMacAddress);

    return SNMP_SUCCESS;
#endif
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRrm11hTxPowerLevel
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                retValFsRrm11hTxPowerLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hTxPowerLevel (INT4 i4IfIndex, tMacAddr FsRrm11hStationMacAddress,
                            INT4 *pi4RetValFsRrm11hTxPowerLevel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTxPowerLevel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hTxPowerLevel =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrmLinkMargin
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                retValFsRrmLinkMargin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hLinkMargin (INT4 i4IfIndex, tMacAddr FsRrm11hStationMacAddress,
                          INT4 *pi4RetValFsRrmLinkMargin)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hLinkMargin = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmLinkMargin =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrm11hTpcReportLastReceived
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                retValFsRrm11hTpcReportLastReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hTpcReportLastReceived (INT4 i4IfIndex,
                                     tMacAddr FsRrm11hStationMacAddress,
                                     UINT4
                                     *pu4RetValFsRrm11hTpcReportLastReceived)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTpcReportLastReceived = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrm11hTpcReportLastReceived =
        RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
        u4RfMgmt11hTpcReportLastReceived;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRrm11hBaseLinkMargin
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                retValFsRrm11hBaseLinkMargin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRrm11hBaseLinkMargin (INT4 i4IfIndex,
                              tMacAddr FsRrm11hStationMacAddress,
                              INT4 *pi4RetValFsRrm11hBaseLinkMargin)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (RfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.u4RfMgmt11hRadioIfIndex = (UINT4) i4IfIndex;
    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hBaseLinkMargin = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrm11hBaseLinkMargin =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRrm11hTxPowerLevel
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                setValFsRrm11hTxPowerLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hTxPowerLevel (INT4 i4IfIndex,
                            tMacAddr FsRrm11hStationMacAddress,
                            INT4 i4SetValFsRrm11hTxPowerLevel)
{
#ifdef WLC_WANTED
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);

    if (RfmgmtGetBaseRadioIfIndex
        ((UINT4) i4IfIndex,
         &RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
         u4RfMgmt11hRadioIfIndex) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtGetBaseRadioIfIndex "
                    "returned failure\r\n");
        return SNMP_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hTxPowerLevel = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel ==
        i4SetValFsRrm11hTxPowerLevel)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i2RfMgmt11hTxPowerLevel =
        (INT2) i4SetValFsRrm11hTxPowerLevel;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    UNUSED_PARAM (i4SetValFsRrm11hTxPowerLevel);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrm11hLinkMargin
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                setValFsRrm11hLinkMargin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hLinkMargin (INT4 i4IfIndex,
                          tMacAddr FsRrm11hStationMacAddress,
                          INT4 i4SetValFsRrm11hLinkMargin)
{
#ifdef WLC_WANTED
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);
    if (RfmgmtGetBaseRadioIfIndex
        ((UINT4) i4IfIndex,
         &RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
         u4RfMgmt11hRadioIfIndex) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtGetBaseRadioIfIndex "
                    "returned failure\r\n");
        return SNMP_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hLinkMargin = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin ==
        i4SetValFsRrm11hLinkMargin)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i1RfMgmt11hLinkMargin =
        (INT1) i4SetValFsRrm11hLinkMargin;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    UNUSED_PARAM (i4SetValFsRrm11hLinkMargin);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRrm11hBaseLinkMargin
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                setValFsRrm11hBaseLinkMargin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRrm11hBaseLinkMargin (INT4 i4IfIndex,
                              tMacAddr FsRrm11hStationMacAddress,
                              INT4 i4SetValFsRrm11hBaseLinkMargin)
{
#ifdef WLC_WANTED
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
            RfMgmtDot11hTpcInfoDB.RfMgmt11hStationMacAddress,
            FsRrm11hStationMacAddress, MAC_ADDR_LEN);
    if (RfmgmtGetBaseRadioIfIndex
        ((UINT4) i4IfIndex,
         &RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.RfMgmtDot11hTpcInfoDB.
         u4RfMgmt11hRadioIfIndex) != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "RfmgmtGetBaseRadioIfIndex "
                    "returned failure\r\n");
        return SNMP_FAILURE;
    }

    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoIsSetDB.bRfMgmt11hBaseLinkMargin = OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin ==
        i4SetValFsRrm11hBaseLinkMargin)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtDot11hTpcInfoTable.
        RfMgmtDot11hTpcInfoDB.i1RfMgmt11hBaseLinkMargin =
        (INT1) i4SetValFsRrm11hBaseLinkMargin;

    if (RfMgmtProcessDBMsg (RFMGMT_SET_TPC_INFO_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    UNUSED_PARAM (i4SetValFsRrm11hBaseLinkMargin);
#endif
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hTxPowerLevel
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                testValFsRrm11hTxPowerLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hTxPowerLevel (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               tMacAddr FsRrm11hStationMacAddress,
                               INT4 i4TestValFsRrm11hTxPowerLevel)
{
    UINT4               u4MaxPower = 0;
    UINT4               u4MinPower = 0;
    INT4                i4RetVal = 0;

    i4RetVal = WssIfGetMaxpower ((UINT4) i4IfIndex, OSIX_TRUE, &u4MaxPower);
    if (i4RetVal != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    /*RFMGMT_OFFSET_THREE - 1 level decrease in Tx power level will result in
     * decrease in 3dbm*/
    u4MinPower = u4MaxPower - (RFMGMT_NUM_OF_TXPOWER_LEVEL *
                               RFMGMT_OFFSET_THREE);
    if ((i4TestValFsRrm11hTxPowerLevel < (INT4) u4MinPower) ||
        (i4TestValFsRrm11hTxPowerLevel > (INT4) u4MaxPower))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hLinkMargin
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                testValFsRrm11hLinkMargin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hLinkMargin (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             tMacAddr FsRrm11hStationMacAddress,
                             INT4 i4TestValFsRrm11hLinkMargin)
{
    if ((i4TestValFsRrm11hLinkMargin < RFMGMT_MIN_LINK_MARGIN) ||
        (i4TestValFsRrm11hLinkMargin > RFMGMT_MAX_LINK_MARGIN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrm11hBaseLinkMargin
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress

                The Object 
                testValFsRrm11hBaseLinkMargin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRrm11hBaseLinkMargin (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 tMacAddr FsRrm11hStationMacAddress,
                                 INT4 i4TestValFsRrm11hBaseLinkMargin)
{
    if ((i4TestValFsRrm11hBaseLinkMargin < RFMGMT_MIN_LINK_MARGIN) ||
        (i4TestValFsRrm11hBaseLinkMargin > RFMGMT_MAX_LINK_MARGIN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsRrm11hStationMacAddress);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRrm11hTpcInfoTable
 Input       :  The Indices
                IfIndex
                FsRrm11hStationMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrm11hTpcInfoTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmTxPowerDecreaseCount
Input       :  The Indices
IfIndex

The Object 
retValFsRrmTxPowerDecreaseCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmTxPowerDecreaseCount (INT4 i4IfIndex,
                                 UINT4 *pu4RetValFsRrmTxPowerDecreaseCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.
        RfMgmtClientConfigIsSetDB.bTxPowerDecreaseCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_CLIENT_CONFIG_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmTxPowerDecreaseCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtClientConfigTable.RfMgmtClientConfigDB.
        u4TxPowerDecreaseCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsRrmSHAInterval
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmTpcInterval
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmSHAInterval (INT4 i4FsRrmRadioType, UINT4 u4SetValFsRrmSHAInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bSHAInterval =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4SHAInterval == u4SetValFsRrmSHAInterval)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u4SHAInterval =
        u4SetValFsRrmSHAInterval;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSHAInterval
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmSHAInterval
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSHAInterval (INT4 i4FsRrmRadioType, UINT4 *pu4RetValFsRrmSHAInterval)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bSHAInterval =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmSHAInterval =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4SHAInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmSHAInterval
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmSHAInterval
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmSHAInterval (UINT4 *pu4ErrorCode,
                           INT4 i4FsRrmRadioType,
                           UINT4 u4TestValFsRrmSHAInterval)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if (((INT4) u4TestValFsRrmSHAInterval < RFMGMT_MIN_SHA_INTERVAL) ||
        (u4TestValFsRrmSHAInterval > RFMGMT_MAX_SHA_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsRrmSHAStatus
Input       :  The Indices
FsRrmRadioType

The Object 
testValFsRrmSHAStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsRrmSHAStatus (UINT4 *pu4ErrorCode, INT4 i4FsRrmRadioType,
                         INT4 i4TestValFsRrmSHAStatus)
{
    INT4                i4RowStatus = 0;

    if (nmhGetFsRrmRowStatus (i4FsRrmRadioType, &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ROW_CREATION_FAIL);
        return SNMP_FAILURE;
    }
    if (i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrmSHAStatus != RFMGMT_SHA_ENABLE) &&
        (i4TestValFsRrmSHAStatus != RFMGMT_SHA_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_INVALID_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsRrmSHAStatus
Input       :  The Indices
FsRrmRadioType

The Object 
setValFsRrmSHAStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsRrmSHAStatus (INT4 i4FsRrmRadioType, INT4 i4SetValFsRrmSHAStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bSHAStatus =
        OSIX_TRUE;

    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB)
        != RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
        return SNMP_FAILURE;
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u1SHAStatus == (UINT1) i4SetValFsRrmSHAStatus)
    {
        return SNMP_SUCCESS;
    }
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.u1SHAStatus =
        (UINT1) i4SetValFsRrmSHAStatus;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSHAStatus
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmSHAStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSHAStatus (INT4 i4FsRrmRadioType, INT4 *pi4RetValFsRrmSHAStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.bSHAStatus =
        OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrmSHAStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1SHAStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsRrmSHAExecutionCount
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmSHAExecutionCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmSHAExecutionCount (INT4 i4FsRrmRadioType,
                              UINT4 *pu4RetValFsRrmSHAExecutionCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bSHAExecutionCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmSHAExecutionCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4SHAExecutionCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsRrmDpaExecutionCount
Input       :  The Indices
FsRrmRadioType

The Object 
retValFsRrmDpaExecutionCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsRrmDpaExecutionCount (INT4 i4FsRrmRadioType,
                              UINT4 *pu4RetValFsRrmDpaExecutionCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType = (UINT4) i4FsRrmRadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfIsSetDB.bDPAExecutionCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRrmDpaExecutionCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u4DPAExecutionCount;

    return SNMP_SUCCESS;

}

#ifdef ROGUEAP_WANTED
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRfGroupName
 Input       :  The Indices

                The Object 
                retValFsRfGroupName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRfGroupName (tSNMP_OCTET_STRING_TYPE * pRetValFsRfGroupName)
{
    MEMCPY (pRetValFsRfGroupName->pu1_OctetList, gu1RfGroupName,
            RF_GROUP_NAME_LENGTH);
    pRetValFsRfGroupName->i4_Length = STRLEN (gu1RfGroupName);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRfGroupName
 Input       :  The Indices

                The Object 
                setValFsRfGroupName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRfGroupName (tSNMP_OCTET_STRING_TYPE * pSetValFsRfGroupName)
{
    UINT1               u1RetVal = 0;
    UINT1               u1RfGroupName[19];

    MEMSET (u1RfGroupName, 0, sizeof (u1RfGroupName));
    MEMCPY (u1RfGroupName, pSetValFsRfGroupName->pu1_OctetList,
            pSetValFsRfGroupName->i4_Length);
    u1RetVal = RfMgmtRogueProcessConfig (u1RfGroupName, gu1RogueDetection);

    if (u1RetVal != RFMGMT_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        MEMSET (gu1RfGroupName, 0, sizeof (gu1RfGroupName));
        MEMCPY (gu1RfGroupName, u1RfGroupName, sizeof (gu1RfGroupName));
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRfGroupName
 Input       :  The Indices

                The Object 
                testValFsRfGroupName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRfGroupName (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pTestValFsRfGroupName)
{
    if (pTestValFsRfGroupName == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRfGroupName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRfGroupName (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRogueApDetecttion
 Input       :  The Indices
 n

                The Object 
                retValFsRogueApDetecttion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApDetecttion (INT4 *pi4RetValFsRogueApDetecttion)
{
    INT4                i4RetValFsRogueApDetecttion = 0;
    i4RetValFsRogueApDetecttion = (INT4) gu1RogueDetection;
    *pi4RetValFsRogueApDetecttion = i4RetValFsRogueApDetecttion;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApTimeout
 Input       :  The Indices

                The Object 
                retValFsRogueApTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApTimeout (UINT4 *pu4RetValFsRogueApTimeout)
{
    UINT4               u4RetValFsRogueApTimeout = 0;
    u4RetValFsRogueApTimeout = gu4RogueTimeOut;
    *pu4RetValFsRogueApTimeout = u4RetValFsRogueApTimeout;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApMaliciousTrapStatus
 Input       :  The Indices

                The Object 
                retValFsRogueApMaliciousTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApMaliciousTrapStatus (INT4 *pi4RetValFsRogueApMaliciousTrapStatus)
{
    INT4                i4RetValFsRogueApMaliciousTrapStatus = 0;
    i4RetValFsRogueApMaliciousTrapStatus = (INT4) gu1RogueTrap;
    *pi4RetValFsRogueApMaliciousTrapStatus =
        i4RetValFsRogueApMaliciousTrapStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRogueApDetecttion
 Input       :  The Indices

                The Object 
                setValFsRogueApDetecttion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApDetecttion (INT4 i4SetValFsRogueApDetecttion)
{
    UINT1               u1RetVal = 0;
    UINT1               u1RfGroupName[19];
    UINT4               u4ErrorCode;

    MEMSET (u1RfGroupName, 0, sizeof (u1RfGroupName));

    u1RetVal =
        RfMgmtRogueProcessConfig (u1RfGroupName,
                                  (UINT1) i4SetValFsRogueApDetecttion);
    if (nmhTestv2FsRogueApDetecttion (&u4ErrorCode, i4SetValFsRogueApDetecttion)
        == SNMP_FAILURE)
        return SNMP_FAILURE;

    if (u1RetVal != RFMGMT_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    else
    {
        gu1RogueDetection = (UINT1) i4SetValFsRogueApDetecttion;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetFsRogueApTimeout
 Input       :  The Indices

                The Object 
                setValFsRogueApTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApTimeout (UINT4 u4SetValFsRogueApTimeout)
{
    UINT4               u4ErrorCode;
    if (nmhTestv2FsRogueApTimeout (&u4ErrorCode, u4SetValFsRogueApTimeout) ==
        SNMP_FAILURE)
        return SNMP_FAILURE;
    gu4RogueTimeOut = u4SetValFsRogueApTimeout;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRogueApMaliciousTrapStatus
 Input       :  The Indices

                The Object 
                setValFsRogueApMaliciousTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApMaliciousTrapStatus (INT4 i4SetValFsRogueApMaliciousTrapStatus)
{
    UINT4               u4ErrorCode;
    if (nmhTestv2FsRogueApMaliciousTrapStatus
        (&u4ErrorCode, i4SetValFsRogueApMaliciousTrapStatus) == SNMP_FAILURE)
        return SNMP_FAILURE;
    else
    {
        gu1RogueTrap = (UINT1) i4SetValFsRogueApMaliciousTrapStatus;
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRogueApDetecttion
 Input       :  The Indices

                The Object 
                testValFsRogueApDetecttion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApDetecttion (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsRogueApDetecttion)
{
    if ((i4TestValFsRogueApDetecttion == 1)
        || (i4TestValFsRogueApDetecttion == 2))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApTimeout
 Input       :  The Indices

                The Object 
                testValFsRogueApTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApTimeout (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRogueApTimeout)
{
    if ((u4TestValFsRogueApTimeout < 240) || (u4TestValFsRogueApTimeout > 3600))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApMaliciousTrapStatus
 Input       :  The Indices

                The Object 
                testValFsRogueApMaliciousTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApMaliciousTrapStatus (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsRogueApMaliciousTrapStatus)
{
    if ((i4TestValFsRogueApMaliciousTrapStatus != 1)
        && (i4TestValFsRogueApMaliciousTrapStatus != 2))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRogueApDetecttion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRogueApDetecttion (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRogueApTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRogueApTimeout (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRogueApMaliciousTrapStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRogueApMaliciousTrapStatus (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRogueApTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRogueApTable
 Input       :  The Indices
                FsRogueApBSSID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRogueApTable (tMacAddr FsRogueApBSSID)
{
    UNUSED_PARAM (FsRogueApBSSID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRogueApTable
 Input       :  The Indices
                FsRogueApBSSID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRogueApTable (tMacAddr * pFsRogueApBSSID)
{
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB;

    pRfMgmtRogueApDB =
        (tRfMgmtRogueApDB *) RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                             RfMgmtRogueApDB);

    if (pRfMgmtRogueApDB != NULL)
    {
        MEMCPY (pFsRogueApBSSID, pRfMgmtRogueApDB->u1RogueApBSSID,
                MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRogueApTable
 Input       :  The Indices
                FsRogueApBSSID
                nextFsRogueApBSSID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRogueApTable (tMacAddr FsRogueApBSSID,
                               tMacAddr * pNextFsRogueApBSSID)
{
    tRfMgmtRogueApDB    RfMgmtRogueApDB;
    tRfMgmtRogueApDB   *pRfMgmtRogueApDB = NULL;

    MEMCPY (RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);

    pRfMgmtRogueApDB =
        (tRfMgmtRogueApDB *) RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                            RfMgmtRogueApDB,
                                            (tRBElem *) & RfMgmtRogueApDB,
                                            NULL);
    if (pRfMgmtRogueApDB != NULL)
    {
        MEMCPY (pNextFsRogueApBSSID, pRfMgmtRogueApDB->u1RogueApBSSID,
                MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    UNUSED_PARAM (FsRogueApBSSID);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRogueApSSID
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApSSID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApLearntFrom (tMacAddr FsRogueApBSSID,
                           tMacAddr * pRetValFsRogueApLearntFrom)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bBSSIDRogueApLearntFrom = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsRogueApLearntFrom,
            RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
            u1BSSIDRogueApLearntFrom, MAC_ADDR_LEN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRogueApSSID
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApSSID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApSSID (tMacAddr FsRogueApBSSID,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsRogueApSSID)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApSSID = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsRogueApSSID->pu1_OctetList,
            RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.u1RogueApSSID,
            WSSMAC_MAX_SSID_LEN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRogueApProcetionType
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApProcetionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApProcetionType (tMacAddr FsRogueApBSSID,
                              INT4 *pi4RetValFsRogueApProcetionType)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApProcetionType = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApProcetionType =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        isRogueApProcetionType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApSNR
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApSNR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApSNR (tMacAddr FsRogueApBSSID, INT4 *pi4RetValFsRogueApSNR)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApSNR = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApSNR =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u1RogueApSNR;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRSSI
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApRSSI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRSSI (tMacAddr FsRogueApBSSID, INT4 *pi4RetValFsRogueApRSSI)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApRSSI = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRSSI =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        i2RogueApRSSI;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApOperationChannel
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApOperationChannel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApOperationChannel (tMacAddr FsRogueApBSSID,
                                 INT4 *pi4RetValFsRogueApOperationChannel)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApOperationChannel = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApOperationChannel =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u2RogueApOperationChannel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApDetectedRadio
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApDetectedRadio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApDetectedRadio (tMacAddr FsRogueApBSSID,
                              INT4 *pi4RetValFsRogueApDetectedRadio)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApDetectedRadio = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApDetectedRadio =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u1RogueApDetectedRadio;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApDetectedBand
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApDetectedBand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApDetectedBand (tMacAddr FsRogueApBSSID,
                             INT4 *pi4RetValFsRogueApDetectedBand)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApDetectedBand = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApDetectedBand =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u1RogueApDetectedBand;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApLastReportedTime
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApLastReportedTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApLastReportedTime (tMacAddr FsRogueApBSSID,
                                 UINT4 *pu4RetValFsRogueApLastReportedTime)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApLastReportedTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRogueApLastReportedTime =
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u4RogueApLastReportedTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApLastHeardTime
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApLastHeardTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApLastHeardTime (tMacAddr FsRogueApBSSID,
                              UINT4 *pu4RetValFsRogueApLastHeardTime)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApLastHeardTime = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRogueApLastHeardTime =
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u4RogueApLastHeardTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApTxPower
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApTxPower
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApTxPower (tMacAddr FsRogueApBSSID,
                        INT4 *pi4RetValFsRogueApTxPower)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApTpc = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApTxPower =
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.i2RogueApTpc;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApClass
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApClass (tMacAddr FsRogueApBSSID, INT4 *pi4RetValFsRogueApClass)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApClass = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApClass =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u1RogueApClass;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApClassifiedBy
 Input       :  The Indices
                FsRogueApBSSID

                The Object
                retValFsRogueApClassifiedBy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApClassifiedBy (tMacAddr FsRogueApBSSID,
                             INT4 *pi4RetValFsRogueApClassifiedBy)
{
    UNUSED_PARAM (pi4RetValFsRogueApClassifiedBy);
    UNUSED_PARAM (FsRogueApBSSID);
    return 0;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApLastHeardBSSID
 Input       :  The Indices
                FsRogueApBSSID

                The Object
                retValFsRogueApLastHeardBSSID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApLastHeardBSSID (tMacAddr FsRogueApBSSID,
                               tMacAddr * pRetValFsRogueApLastHeardBSSID)
{

    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bBSSIDRogueApLearntFrom = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsRogueApLastHeardBSSID,
            RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
            u1BSSIDRogueApLearntFrom, MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApState
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApState (tMacAddr FsRogueApBSSID, INT4 *pi4RetValFsRogueApState)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApState = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApState =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u1RogueApState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApClientCount
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApClientCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApClientCount (tMacAddr FsRogueApBSSID,
                            UINT4 *pu4RetValFsRogueApClientCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApClientCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRogueApClientCount =
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        u4RogueApClientCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApStatus
 Input       :  The Indices
                FsRogueApBSSID

                The Object 
                retValFsRogueApStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApStatus (tMacAddr FsRogueApBSSID, INT4 *pi4RetValFsRogueApStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueApDB.u1RogueApBSSID, FsRogueApBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueApIsSetDB.bRogueApStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_SHOW_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueApDB.
        isRogueApStatus;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRogueRuleConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRogueRuleConfigTable
 Input       :  The Indices
                FsRogueApRuleName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRogueRuleConfigTable (tSNMP_OCTET_STRING_TYPE *
                                                pFsRogueApRuleName)
{
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRogueRuleConfigTable
 Input       :  The Indices
                FsRogueApRuleName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRogueRuleConfigTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsRogueApRuleName)
{
    tRfMgmtRogueRuleDB *pRfMgmtRogueRuleDB;

    pRfMgmtRogueRuleDB =
        (tRfMgmtRogueRuleDB *) RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                               RfMgmtRogueRuleDB);

    if (pRfMgmtRogueRuleDB != NULL)
    {
        MEMCPY (pFsRogueApRuleName->pu1_OctetList,
                pRfMgmtRogueRuleDB->u1RogueApRuleName, WSSMAC_MAX_SSID_LEN);
        pFsRogueApRuleName->i4_Length =
            STRLEN (pFsRogueApRuleName->pu1_OctetList);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRogueRuleConfigTable
 Input       :  The Indices
                FsRogueApRuleName
                nextFsRogueApRuleName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRogueRuleConfigTable (tSNMP_OCTET_STRING_TYPE *
                                       pFsRogueApRuleName,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsRogueApRuleName)
{
    tRfMgmtRogueRuleDB  RfMgmtRogueRuleDB;
    tRfMgmtRogueRuleDB *pRfMgmtRogueRuleDB = NULL;

    MEMCPY (RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);

    pRfMgmtRogueRuleDB =
        (tRfMgmtRogueRuleDB *) RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                              RfMgmtRogueRuleDB,
                                              (tRBElem *) & RfMgmtRogueRuleDB,
                                              NULL);
    if (pRfMgmtRogueRuleDB != NULL)
    {
        MEMCPY (pNextFsRogueApRuleName->pu1_OctetList,
                pRfMgmtRogueRuleDB->u1RogueApRuleName, WSSMAC_MAX_SSID_LEN);
        pNextFsRogueApRuleName->i4_Length =
            STRLEN (pNextFsRogueApRuleName->pu1_OctetList);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRogueApRulePriOrderNum
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRulePriOrderNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRulePriOrderNum (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                INT4 *pi4RetValFsRogueApRulePriOrderNum)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
            u1RogueApRuleName, pFsRogueApRuleName->pu1_OctetList,
            WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleIsSetDB.
        bRogueApRulePriOrderNum = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRulePriOrderNum =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u1RogueApRulePriOrderNum;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleClass
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleClass (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                          INT4 *pi4RetValFsRogueApRuleClass)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleClass = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRuleClass =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u1RogueApRuleClass;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleState
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleState (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                          INT4 *pi4RetValFsRogueApRuleState)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleState = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRuleState =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u1RogueApRuleState;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleDuration
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleDuration (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                             UINT4 *pu4RetValFsRogueApRuleDuration)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleDuration = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRogueApRuleDuration =
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u4RogueApRuleDuration;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleProtectionType
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleProtectionType (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                   INT4 *pi4RetValFsRogueApRuleProtectionType)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApProcetionType = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRuleProtectionType =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        isRogueApProcetionType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleRSSI
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleRSSI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleRSSI (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                         INT4 *pi4RetValFsRogueApRuleRSSI)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRSSI = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRuleRSSI =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        i2RogueApRSSI;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleTpc
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleTpc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleTpc (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                        INT4 *pi4RetValFsRogueApRuleTpc)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleTpc = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRuleTpc =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        i2RogueApRuleTpc;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleSSID
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleSSID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleSSID (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsRogueApRuleSSID)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleSSID = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFsRogueApRuleSSID->pu1_OctetList,
            &RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
            u1RogueApRuleSSID, WSSMAC_MAX_SSID_LEN);
    pRetValFsRogueApRuleSSID->i4_Length =
        STRLEN (pRetValFsRogueApRuleSSID->pu1_OctetList);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleClientCount
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleClientCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleClientCount (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                UINT4 *pu4RetValFsRogueApRuleClientCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApClientCount = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsRogueApRuleClientCount =
        (UINT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u4RogueApClientCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApRuleStatus
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                retValFsRogueApRuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApRuleStatus (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                           INT4 *pi4RetValFsRogueApRuleStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApRuleStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        isRogueApStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRogueApRulePriOrderNum
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRulePriOrderNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRulePriOrderNum (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                INT4 i4SetValFsRogueApRulePriOrderNum)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    UINT1               u1Opcode = 0;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRulePriOrderNum = OSIX_TRUE;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        u1Opcode = RFMGMT_CREATE_RULE_ROGUE_INFO;
    }
    else
    {
        u1Opcode = RFMGMT_SET_RULE_ROGUE_INFO;
    }
    if (nmhTestv2FsRogueApRulePriOrderNum
        (&u4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRulePriOrderNum) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u1RogueApRulePriOrderNum = (UINT1) i4SetValFsRogueApRulePriOrderNum;

    u1RetVal = RfMgmtProcessDBMsg (u1Opcode, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleClass
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleClass (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                          INT4 i4SetValFsRogueApRuleClass)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleClass = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleClass
        (&u4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRuleClass) == SNMP_FAILURE)
        return SNMP_FAILURE;

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.u1RogueApRuleClass =
        (UINT1) i4SetValFsRogueApRuleClass;
    if (ROGUEAP_CLASS_FRIENDLY == i4SetValFsRogueApRuleClass)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleIsSetDB.bRogueApRuleState = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
            u1RogueApRuleState = (UINT1) ROGUEAP_STATE_NONE;
    }

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);

    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleState
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleState (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                          INT4 i4SetValFsRogueApRuleState)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               pu4ErrorCode;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleState = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleState
        (&pu4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRuleState) == SNMP_FAILURE)
        return SNMP_FAILURE;

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.u1RogueApRuleState =
        (UINT1) i4SetValFsRogueApRuleState;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleDuration
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleDuration (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                             UINT4 u4SetValFsRogueApRuleDuration)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               pu4ErrorCode;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleDuration = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleDuration
        (&pu4ErrorCode, pFsRogueApRuleName,
         u4SetValFsRogueApRuleDuration) == SNMP_FAILURE)
        return SNMP_FAILURE;

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u4RogueApRuleDuration = u4SetValFsRogueApRuleDuration;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleProtectionType
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleProtectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleProtectionType (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                   INT4 i4SetValFsRogueApRuleProtectionType)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApProcetionType = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleProtectionType
        (&u4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRuleProtectionType) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        isRogueApProcetionType = (BOOL1) i4SetValFsRogueApRuleProtectionType;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleRSSI
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleRSSI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleRSSI (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                         INT4 i4SetValFsRogueApRuleRSSI)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRSSI = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleRSSI
        (&u4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRuleRSSI) == SNMP_FAILURE)
        return SNMP_FAILURE;

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.i2RogueApRSSI =
        (INT2) i4SetValFsRogueApRuleRSSI;
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleTpc
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleTpc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleTpc (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                        INT4 i4SetValFsRogueApRuleTpc)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleTpc = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleTpc
        (&u4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRuleTpc) == SNMP_FAILURE)
        return SNMP_FAILURE;

    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.i2RogueApRuleTpc =
        (INT2) i4SetValFsRogueApRuleTpc;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleSSID
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleSSID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleSSID (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsRogueApRuleSSID)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApRuleSSID = OSIX_TRUE;
    MEMSET (&RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
            u1RogueApRuleSSID, 0, WSSMAC_MAX_SSID_LEN);

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
            u1RogueApRuleSSID, pSetValFsRogueApRuleSSID->pu1_OctetList,
            WSSMAC_MAX_SSID_LEN);

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleClientCount
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                setValFsRogueApRuleClientCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleClientCount (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                UINT4 u4SetValFsRogueApRuleClientCount)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApClientCount = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleClientCount
        (&u4ErrorCode, pFsRogueApRuleName,
         u4SetValFsRogueApRuleClientCount) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.
        u4RogueApClientCount = u4SetValFsRogueApRuleClientCount;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApRuleStatus
 Input       :  The Indices
                FsRogueApRuleNamSe

                The Object 
                setValFsRogueApRuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApRuleStatus (tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                           INT4 i4SetValFsRogueApRuleStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (&RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueRuleDB.u1RogueApRuleName,
            pFsRogueApRuleName->pu1_OctetList, WSSMAC_MAX_SSID_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueRuleIsSetDB.bRogueApStatus = OSIX_TRUE;
    if (nmhTestv2FsRogueApRuleStatus
        (&u4ErrorCode, pFsRogueApRuleName,
         i4SetValFsRogueApRuleStatus) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueRuleDB.isRogueApStatus =
        (BOOL1) i4SetValFsRogueApRuleStatus;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_RULE_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRulePriOrderNum
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRulePriOrderNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRulePriOrderNum (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                   INT4 i4TestValFsRogueApRulePriOrderNum)
{
    if ((i4TestValFsRogueApRulePriOrderNum < ONE) ||
        (i4TestValFsRogueApRulePriOrderNum > MAX_PRIORITY_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleClass
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleClass (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                             INT4 i4TestValFsRogueApRuleClass)
{
    if ((i4TestValFsRogueApRuleClass != ROGUEAP_CLASS_UNCLASSIFIED) &&
        (i4TestValFsRogueApRuleClass != ROGUEAP_CLASS_FRIENDLY) &&
        (i4TestValFsRogueApRuleClass != ROGUEAP_CLASS_MALICIOUS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleState
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleState (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                             INT4 i4TestValFsRogueApRuleState)
{
    if ((i4TestValFsRogueApRuleState != ROGUEAP_STATE_NONE) &&
        (i4TestValFsRogueApRuleState != ROGUEAP_STATE_ALERT) &&
        (i4TestValFsRogueApRuleState != ROGUEAP_CLASS_CONTAIN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleDuration
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleDuration (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                UINT4 u4TestValFsRogueApRuleDuration)
{
    if (u4TestValFsRogueApRuleDuration > (DEFAULT_DURATION - ONE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleProtectionType
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleProtectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleProtectionType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsRogueApRuleName,
                                      INT4 i4TestValFsRogueApRuleProtectionType)
{

    if ((i4TestValFsRogueApRuleProtectionType != CLI_ENCRYPTION_OPEN) &&
        (i4TestValFsRogueApRuleProtectionType != CLI_ENCRYPTION_WPA))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleRSSI
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleRSSI
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleRSSI (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                            INT4 i4TestValFsRogueApRuleRSSI)
{
    if ((i4TestValFsRogueApRuleRSSI < DEFAULT_RSSI + 1)
        || (i4TestValFsRogueApRuleRSSI > 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleTpc
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleTpc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleTpc (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                           INT4 i4TestValFsRogueApRuleTpc)
{
    if ((i4TestValFsRogueApRuleTpc < 0) || (i4TestValFsRogueApRuleTpc > 30))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleSSID
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleSSID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleSSID (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsRogueApRuleSSID)
{
    INT4                u1Ssid[WSSMAC_MAX_SSID_LEN];
    MEMSET (u1Ssid, 0, WSSMAC_MAX_SSID_LEN);

    if (MEMCMP (pTestValFsRogueApRuleSSID, u1Ssid, WSSMAC_MAX_SSID_LEN) == 0)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleClientCount
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleClientCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleClientCount (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                                   UINT4 u4TestValFsRogueApRuleClientCount)
{
    if ((u4TestValFsRogueApRuleClientCount > DEFAULT_CLIENT_COUNT - ONE) ||
        (u4TestValFsRogueApRuleClientCount < ONE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApRuleStatus
 Input       :  The Indices
                FsRogueApRuleName

                The Object 
                testValFsRogueApRuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApRuleStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsRogueApRuleName,
                              INT4 i4TestValFsRogueApRuleStatus)
{
    if ((i4TestValFsRogueApRuleStatus != 0)
        && (i4TestValFsRogueApRuleStatus != 1))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsRogueApRuleName);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRogueRuleConfigTable
 Input       :  The Indices
                FsRogueApRuleName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRogueRuleConfigTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRogueManualConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRogueManualConfigTable
 Input       :  The Indices
                FsRogueApManualBSSID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRogueManualConfigTable (tMacAddr FsRogueApManualBSSID)
{
    UNUSED_PARAM (FsRogueApManualBSSID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRogueManualConfigTable
 Input       :  The Indices
                FsRogueApManualBSSID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRogueManualConfigTable (tMacAddr * pFsRogueApManualBSSID)
{
    tRfMgmtRogueManualDB *pRfMgmtRogueManualDB;

    pRfMgmtRogueManualDB =
        (tRfMgmtRogueManualDB *) RBTreeGetFirst (gRfMgmtGlobals.RfMgmtGlbMib.
                                                 RfMgmtRogueManualDB);

    if (pRfMgmtRogueManualDB != NULL)
    {
        MEMCPY (pFsRogueApManualBSSID, pRfMgmtRogueManualDB->u1RogueApBSSID,
                MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRogueManualConfigTable
 Input       :  The Indices
                FsRogueApManualBSSID
                nextFsRogueApManualBSSID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRogueManualConfigTable (tMacAddr FsRogueApManualBSSID,
                                         tMacAddr * pNextFsRogueApManualBSSID)
{
    tRfMgmtRogueManualDB RfMgmtRogueManualDB;
    tRfMgmtRogueManualDB *pRfMgmtRogueManualDB = NULL;

    MEMCPY (RfMgmtRogueManualDB.u1RogueApBSSID, FsRogueApManualBSSID,
            MAC_ADDR_LEN);

    pRfMgmtRogueManualDB =
        (tRfMgmtRogueManualDB *) RBTreeGetNext (gRfMgmtGlobals.RfMgmtGlbMib.
                                                RfMgmtRogueManualDB,
                                                (tRBElem *) &
                                                RfMgmtRogueManualDB, NULL);
    if (pRfMgmtRogueManualDB != NULL)
    {
        MEMCPY (pNextFsRogueApManualBSSID, pRfMgmtRogueManualDB->u1RogueApBSSID,
                MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRogueApManualClass
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                retValFsRogueApManualClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApManualClass (tMacAddr FsRogueApManualBSSID,
                            INT4 *pi4RetValFsRogueApManualClass)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID,
            FsRogueApManualBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueManualIsSetDB.bRogueApClass = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_MANUAL_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApManualClass =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
        u1RogueApClass;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApManualState
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                retValFsRogueApManualState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApManualState (tMacAddr FsRogueApManualBSSID,
                            INT4 *pi4RetValFsRogueApManualState)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID,
            FsRogueApManualBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueManualIsSetDB.bRogueApState = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_MANUAL_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApManualState =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
        u1RogueApState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRogueApManualStatus
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                retValFsRogueApManualStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRogueApManualStatus (tMacAddr FsRogueApManualBSSID,
                             INT4 *pi4RetValFsRogueApManualStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID,
            FsRogueApManualBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueManualIsSetDB.bRogueApStatus = OSIX_TRUE;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_GET_MANUAL_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRogueApManualStatus =
        (INT4) RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
        isRogueApStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRogueApManualClass
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                setValFsRogueApManualClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApManualClass (tMacAddr FsRogueApManualBSSID,
                            INT4 i4SetValFsRogueApManualClass)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;

    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID,
            FsRogueApManualBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueManualIsSetDB.bRogueApClass = OSIX_TRUE;
    if (nmhTestv2FsRogueApManualClass
        (&u4ErrorCode, FsRogueApManualBSSID,
         i4SetValFsRogueApManualClass) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.u1RogueApClass =
        (UINT1) i4SetValFsRogueApManualClass;
    if (ROGUEAP_CLASS_FRIENDLY == i4SetValFsRogueApManualClass)
    {
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualIsSetDB.bRogueApState = OSIX_TRUE;
        RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.
            u1RogueApState = (UINT1) ROGUEAP_STATE_NONE;
    }
    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_MANUAL_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApManualState
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                setValFsRogueApManualState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApManualState (tMacAddr FsRogueApManualBSSID,
                            INT4 i4SetValFsRogueApManualState)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID,
            FsRogueApManualBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueManualIsSetDB.bRogueApState = OSIX_TRUE;
    if (nmhTestv2FsRogueApManualState
        (&u4ErrorCode, FsRogueApManualBSSID,
         i4SetValFsRogueApManualState) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.u1RogueApState =
        (UINT1) i4SetValFsRogueApManualState;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_MANUAL_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRogueApManualStatus
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                setValFsRogueApManualStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRogueApManualStatus (tMacAddr FsRogueApManualBSSID,
                             INT4 i4SetValFsRogueApManualStatus)
{
    UINT1               u1RetVal = 0;
    tRfMgmtDB           RfMgmtDB;
    UINT4               u4ErrorCode;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    MEMCPY (RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
            RfMgmtRogueManualDB.u1RogueApBSSID,
            FsRogueApManualBSSID, MAC_ADDR_LEN);
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.
        RfMgmtRogueManualIsSetDB.bRogueApStatus = OSIX_TRUE;
    if (nmhTestv2FsRogueApManualStatus
        (&u4ErrorCode, FsRogueApManualBSSID,
         i4SetValFsRogueApManualStatus) == SNMP_FAILURE)
        return SNMP_FAILURE;
    RfMgmtDB.unRfMgmtDB.RfMgmtRogueTable.RfMgmtRogueManualDB.isRogueApStatus =
        (BOOL1) i4SetValFsRogueApManualStatus;

    u1RetVal = RfMgmtProcessDBMsg (RFMGMT_SET_MANUAL_ROGUE_INFO, &RfMgmtDB);
    if (u1RetVal == RFMGMT_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRogueApManualClass
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                testValFsRogueApManualClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApManualClass (UINT4 *pu4ErrorCode,
                               tMacAddr FsRogueApManualBSSID,
                               INT4 i4TestValFsRogueApManualClass)
{
    if ((i4TestValFsRogueApManualClass != ROGUEAP_CLASS_UNCLASSIFIED) &&
        (i4TestValFsRogueApManualClass != ROGUEAP_CLASS_FRIENDLY) &&
        (i4TestValFsRogueApManualClass != ROGUEAP_CLASS_MALICIOUS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (FsRogueApManualBSSID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApManualState
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                testValFsRogueApManualState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApManualState (UINT4 *pu4ErrorCode,
                               tMacAddr FsRogueApManualBSSID,
                               INT4 i4TestValFsRogueApManualState)
{
    if ((i4TestValFsRogueApManualState != ROGUEAP_STATE_NONE) &&
        (i4TestValFsRogueApManualState != ROGUEAP_STATE_ALERT) &&
        (i4TestValFsRogueApManualState != ROGUEAP_CLASS_CONTAIN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (FsRogueApManualBSSID);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRogueApManualStatus
 Input       :  The Indices
                FsRogueApManualBSSID

                The Object 
                testValFsRogueApManualStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRogueApManualStatus (UINT4 *pu4ErrorCode,
                                tMacAddr FsRogueApManualBSSID,
                                INT4 i4TestValFsRogueApManualStatus)
{
    if ((i4TestValFsRogueApManualStatus != 0)
        && (i4TestValFsRogueApManualStatus != 1))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (FsRogueApManualBSSID);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRogueManualConfigTable
 Input       :  The Indices
                FsRogueApManualBSSID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRogueManualConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif
#endif
