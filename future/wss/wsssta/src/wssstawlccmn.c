/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *                                                                           *
 *  $Id: wssstawlccmn.c,v 1.2 2017/05/23 14:16:59 siva Exp $                                                                         *
 *  Description: This file contains the Interface api for WSSSTA module      *
 *                                                                           *
 *****************************************************************************/
#ifndef __WSSSTA__WLC_CMN_C__
#define __WSSSTA__WLC_CMN_C__

#include "wssstawlcinc.h"

/****************************************************************************
 * *                                                                        *
 * * Function     : WssStaProcessWssIfMsg                                   *
 * *                                                                        *
 * * Description  : Calls WSSSTA module API to process fn calls from other  *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : MsgType  - Msg opcode                                   *
 * *                tWssStaMsgStruct - Pointer to WSSSTA  msg struct        *
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : OSIX_SUCCESS on success                                 *
 * *                OSIX_FAILURE otherwise                                  *
 * *                                                                        *
 * **************************************************************************/
UINT4
WssStaProcessWssIfMsg (UINT4 MacMsgType, tWssStaMsgStruct * pWssStaInfo)
{
    UINT4               u4RetVal = 0;
    switch (MacMsgType)
    {
        case WSS_STA_INIT:
            /* Called from WLC HDLR */
            u4RetVal = WssStaInit ();
            break;
        case WSS_MAC_MGMT_AUTH_MSG:
            /* Called from WSS MAC module */
            u4RetVal = WssStaProcessAuthFrames (pWssStaInfo);
            break;
        case WSS_MAC_MGMT_ASSOC_REQ:
            /* Called from WSS MAC module */
            u4RetVal = WssStaProcessAssocFrames (pWssStaInfo);
            break;
        case WSS_MAC_MGMT_DEAUTH_MSG:
            /* Called from WSS MAC module */
            u4RetVal = WssStaProcessDeAuthFrames (pWssStaInfo);
            break;
        case WSS_MAC_MGMT_REASSOC_REQ:
            /* Called from WSS MAC module */
            u4RetVal = WssStaProcessReAssocFrames (pWssStaInfo);
            break;
        case WSS_MAC_MGMT_DISASSOC_MSG:
            u4RetVal = WssStaProcessDisAssocFrames (pWssStaInfo);
            break;
        case WSS_STA_WLAN_DELETE:
            /* Called from WLAN module */
        case WSS_STA_WTP_DELETE:
            /* Called from RadioIf module */
            u4RetVal =
                WssIfStaDelStation (pWssStaInfo->unAuthMsg.AuthDelProfile);
            break;
        case WSS_STA_IDLE_TIMEOUT:
            /* Called by WLC HDLR when AP Station Idle timeout happens */
            u4RetVal =
                WssStaDeAuthStation (pWssStaInfo->unAuthMsg.AuthDelStation);
            break;
        case WSS_STA_TIMER_EXPIRY:
            u4RetVal = WssStaTmrExpHandler ();
            break;
        case WSS_STA_STACONFIG_RESP:
            u4RetVal =
                (UINT4) (WssStaConfigResp
                         (&(pWssStaInfo->unAuthMsg.WssStaConfigReqInfo)));
            break;
        case WSS_STA_DISCOVERY_MODE:
            u4RetVal = WssStaAclRule (&pWssStaInfo->unAuthMsg.AuthDelStation);
            break;
        default:
            u4RetVal = OSIX_FAILURE;
            break;
    }

    return u4RetVal;
}

/*********************************************************************
 * Function Name : WssStaGetClientMac
 *
 * Description   : Get the MAC Address of the STA from the IP Address.
 *
 * Input(s)      : pRBElem -  Pointer to RBTree element.
 *                 visit   -  Order of visting the tree nodes.
 *                 u4Level -  Level of the tree.
 *                 pArg    -  IP Address to be searched in the RBTRee.
 *                 pOut    -  MAC Address corresponding to the IP Address.
 *
 * Output(s)     : Client MAC address
 *
 * Return Values : None.
 *********************************************************************/

UINT4
WssStaGetClientMac (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                    void *pArg, void *pOut)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    UNUSED_PARAM (u4Level);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pWssStaWepProcessDB = (tWssStaWepProcessDB *) pRBElem;

            if (pWssStaWepProcessDB->u4StationIpAddress == *((UINT4 *) pArg))
            {
                MEMCPY ((UINT1 *) pOut, pWssStaWepProcessDB->stationMacAddress,
                        sizeof (tMacAddr));
            }
        }
    }

    return RB_WALK_CONT;
}

#endif
