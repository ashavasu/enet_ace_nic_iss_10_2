/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssstawebsz.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $                                                                         *
 *  Description: This file contains the Init and Memory related api          *
 *                                                                           *
 *****************************************************************************/

#define _WSSSTAWEBSZ_C
#include "wssstawlcinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssstawebSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSSTAWEB_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4) MemCreateMemPool( 
                          FsWSSSTAWEBSizingParams[i4SizingId].u4StructSize,
                          FsWSSSTAWEBSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSSTAWEBMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssstawebSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssstawebSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSSTAWEBSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSSTAWEBMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssstawebSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSSTAWEB_MAX_SIZING_ID; i4SizingId++) {
        if(WSSSTAWEBMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSSTAWEBMemPoolIds[ i4SizingId] );
            WSSSTAWEBMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
