/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *
 *  $Id: wssstawlcprocess.c,v 1.8.2.1 2018/03/19 14:21:10 siva Exp $                                                                         *
 *  Description: This file contains the station Authentication  related api  *
 *                                                                           *
 *****************************************************************************/

#ifndef __WSSSTA_WLC_PROCESS__C
#define __WSSSTA_WLC_PROCESS__C

#include "wssstawlcinc.h"
#include "wssstawlcglob.h"
#include "wssifpmdb.h"
#include "wsscfglwg.h"
#include "wssmacconst.h"
#include "capwapcliinc.h"
#include "arp.h"
#ifdef BAND_SELECT_WANTED
#include "wsscfgcli.h"
#include "wlchdlrproto.h"
#endif
#ifdef WPS_WANTED
#include "wps.h"
#endif
#include "capwapglob.h"
#include "wlchdlrtrc.h"

extern tRBTree      gWssStaWepProcessDB;
extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;
static UINT4 ValidateSpectrumBit PROTO ((UINT2, UINT2));
#ifdef BAND_SELECT_WANTED
extern UINT4        gu4StaDebugMask;
extern tRBTree      gWssStaBandSteerDB;
#endif
extern UINT4        gu4WssStaWebAuthDebugMask;
extern UINT4        gu4WlcHdlrSysLogId;
#ifdef WSSUSER_WANTED
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif
#endif
#ifdef WPS_WANTED
tMacAddr            gau1DeAuthWpsstaMacAddr;
#endif
/*****************************************************************************
 *  Function Name   : WssStaProcessAuthFrames                               *
 *                                                                           *
 *  Description     : This function is used to Staenticate a station.       *
 *  Input(s)        : Sta Mac Frame                                         *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaProcessAuthFrames (tWssStaMsgStruct * pMsgStruct)
{

    UINT2               u2AlgoNum = 0;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssMacMsgStruct   *pwssMsgRsp = NULL;
    tMacAddr            staMacAddr;
    UINT4               u4BssIfIndex = 0;
    UINT1               dot11PrivacyOptionImplemented = 1;
    tDot11AuthMacFrame  authMacFrame;
    tWssWlanDB          wssWlanDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    BOOL1               Flag = FALSE;
    tWssifauthDBMsgStruct wssStaDB;

    MEMCPY (&authMacFrame, &pMsgStruct->unAuthMsg.WssMacAuthMacFrame,
            sizeof (tDot11AuthMacFrame));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAuthFrames:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssMsgRsp = (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssMsgRsp == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAuthFrames:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssMsgRsp, 0, sizeof (tWssMacMsgStruct));
    MEMCPY (staMacAddr, authMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));

    MEMSET (&(pwssMsgRsp->unMacMsg.AuthMacFrame), 0,
            sizeof (tDot11AuthMacFrame));
    u2AlgoNum = authMacFrame.AuthAlgoNum.u2MacFrameAuthAlgoNum;

    pwssMsgRsp->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl =
        authMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl;

    MEMCPY (pwssMsgRsp->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1DA,
            authMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
    MEMCPY (pwssMsgRsp->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1SA,
            authMacFrame.MacMgmtFrmHdr.u1DA, sizeof (tMacAddr));
    MEMCPY (pwssMsgRsp->unMacMsg.AuthMacFrame.MacMgmtFrmHdr.u1BssId,
            authMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
            authMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve auth data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        return OSIX_FAILURE;
    }
    u4BssIfIndex = wssWlanDB.WssWlanAttributeDB.u4BssIfIndex;

    wssWlanDB.WssWlanIsPresentDB.bPrivacyOptionImplemented = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve privacy data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        return OSIX_FAILURE;
    }
    dot11PrivacyOptionImplemented =
        wssWlanDB.WssWlanAttributeDB.u1PrivacyOptionImplemented;

    if (((wssWlanDB.WssWlanAttributeDB.u1AuthMethod == WSS_STA_AUTH_ALGO_OPEN)
         && (u2AlgoNum != WSS_RX_AUTH_ALGO_OPEN)) ||
        ((wssWlanDB.WssWlanAttributeDB.u1AuthMethod ==
          WSS_STA_AUTH_ALGO_SHARED) && (u2AlgoNum != WSS_RX_AUTH_ALGO_SHARED)))
    {
        pwssMsgRsp->unMacMsg.AuthMacFrame.StatCode.u2MacFrameStatusCode =
            WSSSTA_AUTH_ALG0_UNSUP;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x05;
        if (WssIfProcessWlcHdlrMsg
            (WSS_WLCHDLR_MAC_AUTH_MSG, pWlcHdlrMsgStruct))
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            return OSIX_FAILURE;
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            return OSIX_SUCCESS;
        }
    }
    switch (u2AlgoNum)
    {
        case OPEN_AUTHENTICATION:
            if (authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum ==
                WSSSTA_AUTH_SEQ1)
            {
                pwssMsgRsp->unMacMsg.AuthMacFrame.
                    AuthTransSeqNum.u2MacFrameAuthTransSeqNum =
                    WSSSTA_AUTH_SEQ2;

                pwssMsgRsp->unMacMsg.AuthMacFrame.u2SessId =
                    authMacFrame.u2SessId;
                pwssMsgRsp->unMacMsg.AuthMacFrame.
                    AuthAlgoNum.u2MacFrameAuthAlgoNum = OPEN_AUTHENTICATION;

                pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
                if (pWssStaWepProcessDB == NULL)
                {
                    pWssStaWepProcessDB =
                        (tWssStaWepProcessDB *)
                        MemAllocMemBlk (WSSSTA_AUTHDB_POOLID);
                    if (pWssStaWepProcessDB == NULL)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessAuthFrames: Memory Allocation"
                                    " Failed For WssStaWepProcessDB");
                        pwssMsgRsp->unMacMsg.AuthMacFrame.
                            StatCode.u2MacFrameStatusCode = WSSSTA_UNSPECIFIED;
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                        return OSIX_FAILURE;
                    }
                    if (OSIX_SUCCESS !=
                        WssStaUtilMaxClientCount (u4BssIfIndex, staMacAddr))
                    {
                        WLCHDLR_TRC (WLCHDLR_FAILURE_TRC,
                                     "WssStaProcessAuthFrames: Max Client count "
                                     " exceeded\n");
                        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                            (UINT1 *) pWssStaWepProcessDB);
                        pwssMsgRsp->unMacMsg.AuthMacFrame.
                            StatCode.u2MacFrameStatusCode = WSSSTA_UNSPECIFIED;
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                        return OSIX_FAILURE;
                    }
                    /* MEMSET the DB, to prevent using the old values in the
                     * memory */
                    MEMSET (pWssStaWepProcessDB, 0,
                            ((sizeof (tWssStaWepProcessDB)) -
                             (sizeof (tTmrBlk)) - (sizeof (tRBNodeEmbd))));
                    pWssStaWepProcessDB->u4BssIfIndex = 0;
                    Flag = TRUE;
                    pWssStaWepProcessDB->bRoamStatus = FALSE;
                }
                /* Update the WssStaWepProcessDB */
                /*Delete the entry in client stats DB in rfmgmt if the */
                /*client associates with other ssid */
#ifdef RFMGMT_WANTED
                if ((u4BssIfIndex != pWssStaWepProcessDB->u4BssIfIndex) &&
                    (Flag == FALSE))

                {
                    if (WssStaRFClientDBDelete
                        (pWssStaWepProcessDB->u4BssIfIndex,
                         staMacAddr) != OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessAuthFrames: RF Client "
                                    "DB Delete Fail");
                    }
                    /*Incase of roaming with other AP, new Assoc Id is to be
                     * given for the station*/
                    WssFreeAssocId (pWssStaWepProcessDB);
                    pWssStaWepProcessDB->u2AssociationID = 0;
                    pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
                    WssStaGetAssocId (pWssStaWepProcessDB);
                    pWssStaWepProcessDB->u2AssociationID =
                        pWssStaWepProcessDB->u2AssociationID | WSSSTA_AID_MASK;
                    pWssStaWepProcessDB->u2AssociationID =
                        (pWssStaWepProcessDB->
                         u2AssociationID << CAP_SHIFT_BIT8) |
                        (pWssStaWepProcessDB->
                         u2AssociationID >> CAP_SHIFT_BIT8);

                }
#endif
                MEMCPY (wssStaDB.WssIfAuthStateDB.stationMacAddress, staMacAddr,
                        sizeof (tMacAddr));

                if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                              &wssStaDB) == OSIX_SUCCESS)
                {
                    if (wssStaDB.WssIfAuthStateDB.u1StaAuthFinished == 1)
                    {
                        pWssStaWepProcessDB->bRoamStatus = TRUE;
                    }
                    else
                    {
                        pWssStaWepProcessDB->bRoamStatus = FALSE;
                    }
                }
                pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
                pWssStaWepProcessDB->u2LastSeqNum =
                    authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum;
                MEMCPY (pWssStaWepProcessDB->stationMacAddress,
                        staMacAddr, sizeof (tMacAddr));
                MEMCPY (pWssStaWepProcessDB->BssIdMacAddr,
                        authMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
                pWssStaWepProcessDB->stationState = AUTH_UNASSOC;
                if (Flag && (WssStaUpdateWepProcessDB (WSSSTA_ADD_PROCESS_DB,
                                                       pWssStaWepProcessDB) !=
                             OSIX_SUCCESS))
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaProcessAuthFrames: "
                                "Failed to add an entry to the RB Tree \r\n");
                    MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                        (UINT1 *) pWssStaWepProcessDB);
                    pwssMsgRsp->unMacMsg.AuthMacFrame.
                        StatCode.u2MacFrameStatusCode = WSSSTA_UNSPECIFIED;
                    break;
                }
                if (pWssStaWepProcessDB->u2AssociationID == 0)
                {
                    WssStaGetAssocId (pWssStaWepProcessDB);
                    pWssStaWepProcessDB->u2AssociationID =
                        pWssStaWepProcessDB->u2AssociationID | WSSSTA_AID_MASK;
                    pWssStaWepProcessDB->u2AssociationID =
                        (pWssStaWepProcessDB->
                         u2AssociationID << CAP_SHIFT_BIT8) |
                        (pWssStaWepProcessDB->
                         u2AssociationID >> CAP_SHIFT_BIT8);
                }
                pwssMsgRsp->unMacMsg.AuthMacFrame.StatCode.
                    u2MacFrameStatusCode = WSSSTA_SUCCESSFUL;
            }
            else
            {
                pwssMsgRsp->unMacMsg.AuthMacFrame.
                    StatCode.u2MacFrameStatusCode = WSSSTA_AUTH_OUT_OF_SEQ;
            }
            pwssMsgRsp->unMacMsg.AuthMacFrame.u2SessId = authMacFrame.u2SessId;
            pwssMsgRsp->unMacMsg.AuthMacFrame.
                AuthAlgoNum.u2MacFrameAuthAlgoNum = OPEN_AUTHENTICATION;
            break;

        case SHARED_KEY_AUTHENTICATION:
            if (authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum ==
                WSSSTA_AUTH_SEQ1)
            {
                if (dot11PrivacyOptionImplemented)
                {
                    /* First chk the station entry in process DB */
                    pwssMsgRsp->unMacMsg.AuthMacFrame.
                        AuthTransSeqNum.u2MacFrameAuthTransSeqNum =
                        WSSSTA_AUTH_SEQ2;
                    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
                    if (pWssStaWepProcessDB == NULL)
                    {
                        pWssStaWepProcessDB =
                            (tWssStaWepProcessDB *) MemAllocMemBlk
                            (WSSSTA_AUTHDB_POOLID);
                        if (pWssStaWepProcessDB == NULL)
                        {
                            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                        "WssStaProcessAuthFrames: Memory "
                                        "Allocation Failed For "
                                        "WssStaWepProcessDB");
                            pwssMsgRsp->unMacMsg.AuthMacFrame.
                                StatCode.u2MacFrameStatusCode =
                                WSSSTA_UNSPECIFIED;
                            break;
                        }
                        /* MEMSET the DB, to prevent using the old values in the
                         * memory */
                        MEMSET (pWssStaWepProcessDB, 0,
                                ((sizeof (tWssStaWepProcessDB)) -
                                 (sizeof (tTmrBlk)) - (sizeof (tRBNodeEmbd))));
                        Flag = TRUE;
                    }
                    if ((u4BssIfIndex != pWssStaWepProcessDB->u4BssIfIndex)
                        && (Flag == FALSE))
                    {
                        /*Incase of roaming with other AP, new Assoc Id is to be
                         * given for the station*/
                        WssFreeAssocId (pWssStaWepProcessDB);
                        pWssStaWepProcessDB->u2AssociationID = 0;
                        pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
                        WssStaGetAssocId (pWssStaWepProcessDB);
                        pWssStaWepProcessDB->u2AssociationID =
                            pWssStaWepProcessDB->
                            u2AssociationID | WSSSTA_AID_MASK;
                        pWssStaWepProcessDB->u2AssociationID =
                            (pWssStaWepProcessDB->
                             u2AssociationID << CAP_SHIFT_BIT8) |
                            (pWssStaWepProcessDB->
                             u2AssociationID >> CAP_SHIFT_BIT8);
                    }

                    /* Update the WssStaWepProcessDB */
                    pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
                    pWssStaWepProcessDB->u2LastSeqNum =
                        authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum;
                    MEMCPY (pWssStaWepProcessDB->stationMacAddress,
                            staMacAddr, sizeof (tMacAddr));
                    MEMCPY (pWssStaWepProcessDB->BssIdMacAddr,
                            authMacFrame.MacMgmtFrmHdr.u1BssId,
                            sizeof (tMacAddr));

                    pwssMsgRsp->unMacMsg.AuthMacFrame.
                        ChalngTxt.u1MacFrameElemId =
                        WSSMAC_CHALNG_TXT_ELEMENT_ID;
                    pwssMsgRsp->unMacMsg.AuthMacFrame.
                        ChalngTxt.u1MacFrameElemLen = WSSSTA_CHALLENGE_TXT_LEN;
                    WssStaRc4Prng (pwssMsgRsp->unMacMsg.AuthMacFrame.
                                   ChalngTxt.au1MacFrameChalngTxt);
                    pWssStaWepProcessDB->u4WepChallengeTextLen =
                        WSSSTA_CHALLENGE_TXT_LEN;
                    MEMCPY (pWssStaWepProcessDB->au1ChallengeText,
                            &(pwssMsgRsp->unMacMsg.AuthMacFrame.
                              ChalngTxt.au1MacFrameChalngTxt),
                            WSSSTA_CHALLENGE_TXT_LEN);
                    pWssStaWepProcessDB->u2LastSeqNum =
                        authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum;

                    if (Flag
                        &&
                        (WssStaUpdateWepProcessDB
                         (WSSSTA_ADD_PROCESS_DB,
                          pWssStaWepProcessDB) != OSIX_SUCCESS))
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessAuthFrames: "
                                    "Failed to add an entry to the RB Tree \r\n");
                        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                            (UINT1 *) pWssStaWepProcessDB);
                        pwssMsgRsp->unMacMsg.AuthMacFrame.
                            StatCode.u2MacFrameStatusCode = WSSSTA_UNSPECIFIED;
                        break;
                    }
                    if (pWssStaWepProcessDB->u2AssociationID == 0)
                    {
                        WssStaGetAssocId (pWssStaWepProcessDB);
                        pWssStaWepProcessDB->u2AssociationID =
                            pWssStaWepProcessDB->
                            u2AssociationID | WSSSTA_AID_MASK;
                        pWssStaWepProcessDB->u2AssociationID =
                            (pWssStaWepProcessDB->
                             u2AssociationID << CAP_SHIFT_BIT8) |
                            (pWssStaWepProcessDB->
                             u2AssociationID >> CAP_SHIFT_BIT8);
                    }

                }
                else
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaProcessAuthFrames: "
                                "dot11PrivacyOptionImplemented is FALSE \r\n");
                    pwssMsgRsp->unMacMsg.AuthMacFrame.
                        StatCode.u2MacFrameStatusCode = WSSSTA_AUTH_ALG0_UNSUP;
                    break;
                }
                pwssMsgRsp->unMacMsg.AuthMacFrame.
                    StatCode.u2MacFrameStatusCode = WSSSTA_SUCCESSFUL;
            }
            else if (authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum ==
                     WSSSTA_AUTH_SEQ3)
            {
                pwssMsgRsp->unMacMsg.AuthMacFrame.
                    AuthTransSeqNum.u2MacFrameAuthTransSeqNum =
                    WSSSTA_AUTH_SEQ4;
                pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
                if (pWssStaWepProcessDB == NULL)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Previous Authentication no longer valid \r\n");
                    pwssMsgRsp->unMacMsg.AuthMacFrame.
                        StatCode.u2MacFrameStatusCode = WSSSTA_UNSPECIFIED;
                    break;
                }
                else
                {
                    if (pWssStaWepProcessDB->u2LastSeqNum ==
                        authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum)
                    {
                        if ((WssStaUpdateWepProcessDB (WSSSTA_DELETE_PROCESS_DB,
                                                       pWssStaWepProcessDB)) !=
                            OSIX_SUCCESS)
                        {
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                            return OSIX_FAILURE;
                        }
                        pwssMsgRsp->unMacMsg.AuthMacFrame.
                            StatCode.u2MacFrameStatusCode =
                            WSSSTA_AUTH_OUT_OF_SEQ;
                        break;
                    }
                    pWssStaWepProcessDB->u2LastSeqNum =
                        authMacFrame.AuthTransSeqNum.u2MacFrameAuthTransSeqNum;
                    if ((!MEMCMP (pWssStaWepProcessDB->au1ChallengeText,
                                  authMacFrame.ChalngTxt.au1MacFrameChalngTxt,
                                  WSSSTA_CHALLENGE_TXT_LEN)))
                    {
                        pWssStaWepProcessDB->stationState = AUTH_UNASSOC;
                        pwssMsgRsp->unMacMsg.AuthMacFrame.
                            StatCode.u2MacFrameStatusCode = WSSSTA_SUCCESSFUL;
                    }
                    else
                    {
                        pwssMsgRsp->unMacMsg.AuthMacFrame.
                            StatCode.u2MacFrameStatusCode =
                            WSSSTA_AUTH_CHALLENGE_FAIL;
                        break;
                    }
                }
            }
            else
            {
                pwssMsgRsp->unMacMsg.AuthMacFrame.
                    StatCode.u2MacFrameStatusCode = WSSSTA_AUTH_OUT_OF_SEQ;
            }
            pwssMsgRsp->unMacMsg.AuthMacFrame.u2SessId = authMacFrame.u2SessId;
            pwssMsgRsp->unMacMsg.AuthMacFrame.
                AuthAlgoNum.u2MacFrameAuthAlgoNum = SHARED_KEY_AUTHENTICATION;
            break;
        default:
            break;
    }
    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
            sizeof (tWssMacMsgStruct));
    /*  Free the CRU Buffer TBD */
    pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x05;
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_AUTH_MSG, pWlcHdlrMsgStruct) ==
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        return OSIX_SUCCESS;
    }
    else
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        return OSIX_FAILURE;
    }
}

/*****************************************************************************
 *  Function Name   : WssStaUtilMaxClientCount                               *
 *                                                                           *
 *  Description     : This Function is use to get the max client count  for  *
 *                  : wlan and radio                                         *
 *  Input(s)        :                                                        *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

UINT4
WssStaUtilMaxClientCount (UINT4 u4BssIndex, tMacAddr staMacAddr)
{
    tWssWlanDB         *pwssWlanDB = NULL;
    tRadioIfGetDB       radioIfgetDB;
    UINT4               u4StaCount = 0;
    UINT4               u4StaCount1 = 0;
    UINT4               u4WlanIndex = 0;
    UINT4               u4RadioIfIndex = 0;
    INT1                i1RetValValidateStaDiscovery = 0;
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIndex;
    pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS_WLAN_GET_BSS_IFINDEX_ENTRY "
                    "Failed \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    u4WlanIndex = pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
    u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    radioIfgetDB.RadioIfIsGetAllDB.bMaxClientCount = OSIX_TRUE;
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    i1RetValValidateStaDiscovery = WssStaUtilValidateDiscoveryMode (staMacAddr);
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB) ==
        OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS_GET_RADIO_IF_DB " "Failed \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }

    WssStaVerifyMaxClientCount (u4WlanIndex, &u4StaCount, staMacAddr);
    WssStaVerifyRadioMaxClientCount (u4RadioIfIndex, &u4StaCount1, staMacAddr);
    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
    pwssWlanDB->WssWlanIsPresentDB.bMaxClientCount = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS_WLAN_GET_IFINDEX_ENTRY"
                    "Failed \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    /* Get the Assoc count */
    WssStaShowClient ();
    if ((gu4ClientWalkIndex >= MAX_STA_SUPP_PER_WLC) ||
        ((radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount > 0) &&
         (u4StaCount1 > radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount))
        || ((pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount > 0)
            && (u4StaCount >
                pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount)) ||
        (i1RetValValidateStaDiscovery == OSIX_FAILURE))
    {
        if (i1RetValValidateStaDiscovery == OSIX_FAILURE)
        {
            WSSSTA_TRC2 (WSSSTA_FAILURE_TRC,
                         "STA DISCOVERY MODE FAILED ..... "
                         "Rejecting Station Association\r\n", NULL, NULL);
        }
        else
        {
            WSSSTA_TRC2 (WSSSTA_FAILURE_TRC,
                         "MAX CLIENT COUNT (Radio - %d/Wlan - %d) REACHED....."
                         "Rejecting Station at WssStaUtilMaxClientCount\r\n",
                         radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount,
                         pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaRc4Prng                                          *
 *                                                                           *
 *  Description     : This Function is use to generate 128 byte of challenge *
 *                  : text                                                   *
 *  Input(s)        :                                                        *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

VOID
WssStaRc4Prng (UINT1 *pPA)
{

    UINT4               u1j, u1k, u1n;

    static UINT1        u1i;

    for (u1n = 0; u1n < WSSSTA_CHALLENGE_TXT_LEN; u1n++)
    {
        pPA[u1n] = (UINT1) u1n;
    }

    u1j = 0;
    for (u1k = 0, u1j = 0; u1k < WSSSTA_CHALLENGE_TXT_LEN; u1k++)
    {
        u1i = (UINT1) ((u1i + 1) & 0xff);
        u1j = (u1j + pPA[u1i]) & 0xff;
        pPA[u1i] = pPA[u1i] ^ pPA[u1j];
        pPA[u1j] = pPA[u1i] ^ pPA[u1j];
        pPA[u1i] = pPA[u1i] ^ pPA[u1j];
    }
}

/*****************************************************************************
 *  Function Name   : ValidateSpectrumBit                                    *
 *                                                                           *
 *  Description     : This function is used to validate Spectrum             *
 *                    management Bit.                                        *
 *  Input(s)        : assocCapability, u2Capability                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 ******************************************************************************/
static UINT4
ValidateSpectrumBit (UINT2 assocCapability, UINT2 u2Capability)
{
    if (assocCapability != u2Capability)
    {
        if ((assocCapability & (0x0001 << WSSSTA_SHIFT_8)) !=
            (u2Capability & (0x0001 << WSSSTA_SHIFT_8)))
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : ValidateAssocFrame                                     *
 *                                                                           *
 *  Description     : This function is used to validate the Assoc parameter. *
 *  Input(s)        : AssocReqMacFrame, WlanIndex, AssocRspMacFrame          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
ValidateAssocFrame (tDot11AssocReqMacFrame * passocReqMacFrame,
                    tDot11AssocRspMacFrame * pAssocRspMacFrame,
                    tRadioIfGetDB * pRadioIfGetDB, UINT2 u2Capability)
{
    UINT2               u2Index = 0;
    UINT1               u1StaSuppChannelWidth = 0;
    UINT2               assocCapability =
        passocReqMacFrame->Capability.u2MacFrameCapability;
    UINT1               ChanlNum = 0, NumOfChanl = 0;
    UINT2               u2Count = 0, u2SupportedChannelSet = 0;
    UINT4               u4RadioType = 0;
    UNUSED_PARAM (pAssocRspMacFrame);
    UNUSED_PARAM (u2Index);

#ifdef RFMGMT_WANTED
    tRfMgmtDB           RfMgmtDB;
    MEMSET (&RfMgmtDB, 0, sizeof (tRfMgmtDB));

    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.
        RfMgmtAutoRfProfileDB.u4Dot11RadioType =
        pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
    RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfIsSetDB.
        bRfMgmt11hTpcStatus = OSIX_TRUE;
    if (RfMgmtProcessDBMsg (RFMGMT_GET_AUTO_RF_ENTRY, &RfMgmtDB) !=
        RFMGMT_SUCCESS)
    {
        RFMGMT_TRC (RFMGMT_FAILURE_TRC, "DB updation Failed\r\n");
    }

    if (RfMgmtDB.unRfMgmtDB.RfMgmtAutoRfTable.RfMgmtAutoRfProfileDB.
        u1RfMgmt11hTpcStatus == OSIX_TRUE)
    {
        if ((UINT1) pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel <
            passocReqMacFrame->PowCapability.u1MacFramePowCap_MaxTxPow)
        {
            return WSSSTA_POW_CAPABILITY_UNSUP;
        }
    }
#endif

    u4RadioType = pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
    if ((((u2Capability & 0x0100) >> WSSSTA_SHIFT_8) == 1)
        && ((u4RadioType == RADIO_TYPE_A) || (u4RadioType == RADIO_TYPE_AN)
            || (u4RadioType == RADIO_TYPE_AC)))
    {
        if (ValidateSpectrumBit (assocCapability, u2Capability) != OSIX_SUCCESS)
        {
            return WSSSTA_SPECTRUMMGMT_UNSUP;
        }
        u2SupportedChannelSet =
            (passocReqMacFrame->SuppChanls.u1MacFrameElemLen) / 2;
        while (u2Count < u2SupportedChannelSet)
        {
            ChanlNum = passocReqMacFrame->SuppChanls.
                SubBandDes[u2Count].u1MacFrameSuppChanl_FirChanlNum;
            NumOfChanl = passocReqMacFrame->SuppChanls.
                SubBandDes[u2Count].u1MacFrameSuppChanl_NumOfChanl;
            while (NumOfChanl)
            {
                if (ChanlNum != pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel)
                {
                    ChanlNum = ChanlNum + (UINT1) 4;
                    NumOfChanl--;
                    continue;
                }
                break;
            }
            if (NumOfChanl == 0)
            {
                u2Count++;
                continue;
            }
            break;
        }
        if (u2Count == u2SupportedChannelSet)
        {
            return WSSSTA_CHANNELS_UNSUP;
        }
    }
    if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
         RADIO_TYPE_BGN) ||
        (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
         RADIO_TYPE_AN) ||
        (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_NG)
        || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC))
    {
        if (passocReqMacFrame->HTCapabilities.u1MacFrameElemId
            == WSSMAC_HT_CAPAB_ELEMENT_ID)
        {
            /* Reading the HT Cap value from the RadioDB */
            WssStaGetDo111nValues (pRadioIfGetDB);

            /*If  ALLOWED channel width is set to operational */
            if (pRadioIfGetDB->RadioIfGetAllDB.u1AllowChannelWidth == 2)
            {
                if (((passocReqMacFrame->HTCapabilities.u2HTCapInfo >>
                      RADIO_SHIFT_BIT9) & RADIO_MASK_BIT1) == 1)
                {
                    u1StaSuppChannelWidth = 1;
                }
                else
                {
                    u1StaSuppChannelWidth = 0;
                }
                /*If STA channel width  and AP configured channel width are not same 
                 *drop the assoc req frame here */
                if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                    u1ChannelWidth != u1StaSuppChannelWidth)
                {
                    return WSSSTA_UNSPECIFIED;
                }
            }
        }
    }

#ifdef WSSSTA_AUTH
    if (assocCapability == u2Capability)
    {
        pAssocRspMacFrame->Capability.u2MacFrameCapability = u2Capability;
        UNUSED_PARAM (pRadioIfGetDB);
        if (!strcmp (passocReqMacFrame->SuppRate.au1MacFrameSuppRate,
                     pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate))
        {
            pAssocRspMacFrame->SuppRate.u1MacFrameElemId =
                WSSMAC_SUPP_RATE_ELEMENT_ID;
            pAssocRspMacFrame->SuppRate.u1MacFrameElemLen =
                WSSSTA_SUPP_RATE_LEN;
            MEMCPY (pAssocRspMacFrame->SuppRate.au1MacFrameSuppRate,
                    pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate,
                    strlen (pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate));
        }
        else
        {
            return WSSSTA_DATARATE_UNSUP;
        }
    }
    else
    {
        for (u2Index = 0; u2Index <= MAX_CAPABILITY; u2Index++)
        {
            if ((assocCapability & 1 << u2Index) !=
                (u2Capability & 1 << u2Index))
            {
                if (u2Index == WSSSTA_OFDM_CAPABILITY)
                {
                    return WSSSTA_DSSS_OFDM_UNSUP;
                }
                else if (u2Index == 5)
                {
                    return WSSSTA_SHORTSLOTTIME_UNSUP;
                }
                else if (u2Index == 7)
                {
                    return WSSSTA_SPECTRUMMGMT_UNSUP;
                }
                else if (u2Index == 8)
                {
                    return WSSSTA_CHANNELAGILITY_UNSUP;
                }
                else if (u2Index == 9)
                {
                    return WSSSTA_PBCC_UNSUP;
                }
                else if (u2Index == 10)
                {
                    return WSSSTA_SHORTPREAMBLE_UNSUP;
                }
                else
                {
                    return WSSSTA_UNSUP_CAP;
                }
            }
        }
    }
#endif

    return WSSSTA_SUCCESSFUL;
}

/*****************************************************************************
 *  Function Name   : WssStaGetDo111nValues                                  *
 *                                                                           *
 *  Description     : This function is used to get the 802.11n values        *
 *                     from the radioDB                                      *
 *  Input(s)                                                                 *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaGetDo111nValues (tRadioIfGetDB * pRadioIfgetDB)
{
    pRadioIfgetDB->RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;
    /*HT Ext Capabilities */
    pRadioIfgetDB->RadioIfIsGetAllDB.bPco = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
    /* Beam Forming */
    pRadioIfgetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bRecStagSoundCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bTransStagSoundCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bImpTranBeamCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bExplCompSteCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
    pRadioIfgetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, pRadioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaProcessAssocFrames                               *
 *                                                                           *
 *  Description     : This function is used to Associate a station.          *
 *                  : Assoc Mac Frame                                        *
 *  Input(s)                                                                 *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaProcessAssocFrames (tWssStaMsgStruct * pMsgStruct)
{
    tMacAddr            staMacAddr;
    UINT2               u2WtpInternalId = 0;
    tWssMacMsgStruct   *pwssMsgRsp = NULL;
    tWssifauthDBMsgStruct *passocWssStaDB = NULL;
    tWssifauthDBMsgStruct *ploadWssStaDB = NULL;
    tWssMacMsgStruct   *pwssDeauthMsg = NULL;
    UINT4               u4BssIfIndex = 0;
    tDot11AssocReqMacFrame assocReqMacFrame;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT2               u2WtpId = 0;
    tWssStaParams       wssStaParams;
    UINT2               u2Aid;
    INT1                i1RetValValidateStaDiscovery = 0;
    UINT4               u4Result = 0;
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    UINT1               u1RadioId;
    tRadioIfGetDB       radioIfgetDB;
    UINT2               u2Capability;
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    tWssMacMsgStruct   *pwssDisassocMacFrame = NULL;
    UINT4               u4StaCount = 0;
    INT4                i4LoginAuth = 0;
    UINT4               u4StaCount1 = 0;
    UINT1               u1McsFlag = 0;
    UINT1               u1McsIndex = 0;
    tWlanSTATrapInfo    sStationTrapInfo;
    UINT4               u4ProfileIndex = 0;
#ifdef WPS_WANTED
    tWssWpsNotifyParams WssWpsNotifyParams;
#endif
#ifdef RSNA_WANTED
    tWssRSNANotifyParams WssRSNANotifyParams;
    BOOL1               bSessionDelete = OSIX_FALSE;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;
#endif
#ifdef WPA_WANTED
    UINT1               au1WpaOUI[] = { 0x00, 0x50, 0xf2, 0x01 };
#endif
    UINT1               u1WlanID = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT2               u2SessId = 0;
    UINT4               u4RadioIfIndex = 0;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT2               u2VlanId = 0;
    UINT1               u1AuthAlgo = 0;
    UINT1               au1WepKey[MAX_WEP_KEY_LENGTH] = "\0";
    UINT4               u4WlanIndex = 0;
#ifdef PMF_WANTED
    BOOL1               bPMFSessionExist = OSIX_FALSE;
#endif
    UINT1               u1LegacyRate = 0;
    UINT1               u1length = 0;
    UINT1               u1iter = 0;
#ifdef BAND_SELECT_WANTED
    UINT1               u1BandSelectGlobStatus = 0;
    tWssWlanDB         *pwssBandWlanDB = NULL;
    tWssStaBandSteerDB  WssStaBandSteerDB;
    tWssStaBandSteerDB *pWssStaBandSteerDB = NULL;

    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
#endif
#ifdef NPAPI_WANTED
    tWlanClientParams   WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpWrAdd  WlanClientNpWrAdd;
    tWlanClientNpWrDelete WlanClientNpWrDelete;
    tWlanParams         WlanParams;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&WlanParams, 0, sizeof (tWlanParams));
    MEMSET (&WlanClientNpWrAdd, 0, sizeof (tWlanClientNpWrAdd));
    MEMSET (&WlanClientNpWrDelete, 0, sizeof (tWlanClientNpWrDelete));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
#endif
    UINT1               u1OUI[3] = { 0x00, 0x50, 0xF2 };
    UINT2               u2QosCapab = 0;
    UINT1               u1QosIndex = 0;
    UINT1               u1AIFS_BE = 0;
    UINT1               u1ECW_BE = 0;
    UINT1               u1AIFS_BG = 0;
    UINT1               u1ECW_BG = 0;
    UINT1               u1AIFS_VI = 0;
    UINT1               u1ECW_VI = 0;
    UINT1               u1AIFS_VO = 0;
    UINT1               u1ECW_VO = 0;
    UINT1               u1Loop;
    INT4                i4AuthSuiteStatus = RSNA_DISABLED;
    UINT1               u1WebAuthStatus = WSSWLAN_DISABLE;
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";

#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;

    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));
#endif
    BOOL1               bUserRoleStatus = OSIX_FAILURE;
#ifdef WPS_WANTED
    MEMSET (&WssWpsNotifyParams, 0, sizeof (tWssWpsNotifyParams));
#endif
#ifdef RSNA_WANTED
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
#endif

    /*assocReqMacFrame = pMsgStruct->unAuthMsg.WssMacAssocReqMacFrame; */
    MEMSET (&assocReqMacFrame, 0, sizeof (tDot11AssocReqMacFrame));
    MEMCPY (&assocReqMacFrame,
            &(pMsgStruct->unAuthMsg.WssMacAssocReqMacFrame),
            sizeof (tDot11AssocReqMacFrame));
    u2SessId = assocReqMacFrame.u2SessId;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocFrames:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssDeauthMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssDeauthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocFrames:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    passocWssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (passocWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        return OSIX_FAILURE;
    }

    ploadWssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (ploadWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        return OSIX_FAILURE;
    }
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        return OSIX_FAILURE;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }

    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssDeauthMsg, 0, sizeof (tWssMacMsgStruct));
    MEMCPY (staMacAddr, assocReqMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
    MEMSET (au1WepKey, 0, MAX_WEP_KEY_LENGTH);
    MEMSET (&sStationTrapInfo, 0, sizeof (tWlanSTATrapInfo));

    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWepKey = OSIX_TRUE;

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            assocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));

    WssUtilGetTimeStr (ac1TimeStr);
    CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                 "%s : ASSOC Req recvd from %02x:%02x:%02x:%02x:%02x:%02x\n",
                 ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                 staMacAddr[3], staMacAddr[4], staMacAddr[5]);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                  "%s : ASSOC Req recvd from %02x:%02x:%02x:%02x:%02x:%02x",
                  ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                  staMacAddr[3], staMacAddr[4], staMacAddr[5]));

    i1RetValValidateStaDiscovery = WssStaUtilValidateDiscoveryMode (staMacAddr);

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u4BssIfIndex = pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
    u2WtpId = pwssWlanDB->WssWlanAttributeDB.u2WtpInternalId;
    u2Capability = pwssWlanDB->WssWlanAttributeDB.u2Capability;
    u1WlanID = pwssWlanDB->WssWlanAttributeDB.u1WlanId;
    u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    u1AuthAlgo = pwssWlanDB->WssWlanAttributeDB.u1AuthMethod;
    u4WlanIndex = pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
    u2WtpInternalId = pwssWlanDB->WssWlanAttributeDB.u2WtpInternalId;
    u1WebAuthStatus = pwssWlanDB->WssWlanAttributeDB.u1WebAuthStatus;

    if (u1AuthAlgo == WSS_STA_AUTH_ALGO_SHARED)
    {
        MEMCPY (au1WepKey, pwssWlanDB->WssWlanAttributeDB.au1WepKey,
                STRLEN (pwssWlanDB->WssWlanAttributeDB.au1WepKey));
        au1WepKey[STRLEN (pwssWlanDB->WssWlanAttributeDB.au1WepKey)] = '\0';
    }

    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS_WLAN_GET_BSS_IFINDEX_ENTRY "
                    "Failed \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId;

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
    /*radioIfgetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE; */
    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bMaxClientCount = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bSuppMCSSet = OSIX_TRUE;
#ifdef BAND_SELECT_WANTED
    radioIfgetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
#endif
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB)
        != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;
    pwssMsgRsp = (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssMsgRsp == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocFrames:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }
    MEMSET (pwssMsgRsp, 0, sizeof (tWssMacMsgStruct));

#ifdef BAND_SELECT_WANTED
    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
    if (WssIfGetBandSelectStatus (&u1BandSelectGlobStatus) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to get status of bandsteer \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    if (u1BandSelectGlobStatus != WLAN_DISABLE)
    {
        pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
        pwssWlanDB->WssWlanIsPresentDB.bBandSelectStatus = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAssocRejectCount = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAssocResetTime = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames:- "
                        "WSS_WLAN_GET_IFINDEX_ENTRY FAILED to get BandSteer Values\r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;

        }
        if (pwssWlanDB->WssWlanAttributeDB.u1BandSelectStatus
            == BAND_SELECT_ENABLED)
        {
            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC, "Band - select enabled ...\r\n");
            /* Check whether the request is received on 2.4 GHz frequency. If yes, then 
             * check the station DB for probe request received on 5 GHz frequency. If yes,
             * send Assoc response as rejected. */
            if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_BGN)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_B)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_G))
            {
                WSSSTA_TRC6 (WSSSTA_BANDSELECT_TRC,
                             "Association request received from 2.4 ghz...in bssid : %x:%x:%x:%x:%x:%x ",
                             assocReqMacFrame.MacMgmtFrmHdr.u1BssId[0],
                             assocReqMacFrame.MacMgmtFrmHdr.u1BssId[1],
                             assocReqMacFrame.MacMgmtFrmHdr.u1BssId[2],
                             assocReqMacFrame.MacMgmtFrmHdr.u1BssId[3],
                             assocReqMacFrame.MacMgmtFrmHdr.u1BssId[4],
                             assocReqMacFrame.MacMgmtFrmHdr.u1BssId[5]);

                WSSSTA_TRC6 (WSSSTA_BANDSELECT_TRC,
                             "from STA MAC : %x:%x:%x:%x:%x:%x \r\n",
                             staMacAddr[0], staMacAddr[1], staMacAddr[2],
                             staMacAddr[3], staMacAddr[4], staMacAddr[5]);

                MEMCPY (WssStaBandSteerDB.stationMacAddress, staMacAddr,
                        MAC_ADDR_LEN);
                MEMCPY (WssStaBandSteerDB.BssIdMacAddr, "\0\0\0\0\0\0",
                        MAC_ADDR_LEN);

                while ((pWssStaBandSteerDB = RBTreeGetNext (gWssStaBandSteerDB,
                                                            (tRBElem *) &
                                                            WssStaBandSteerDB,
                                                            NULL)) != NULL)
                {
                    if (MEMCMP (pWssStaBandSteerDB->BssIdMacAddr,
                                assocReqMacFrame.MacMgmtFrmHdr.u1BssId,
                                sizeof (tMacAddr)) != 0)
                    {
                        WSSSTA_TRC6 (WSSSTA_BANDSELECT_TRC,
                                     "Band steer entry in DB from bssid : %x:%x:%x:%x:%x:%x \r\n",
                                     pWssStaBandSteerDB->BssIdMacAddr[0],
                                     pWssStaBandSteerDB->BssIdMacAddr[1],
                                     pWssStaBandSteerDB->BssIdMacAddr[2],
                                     pWssStaBandSteerDB->BssIdMacAddr[3],
                                     pWssStaBandSteerDB->BssIdMacAddr[4],
                                     pWssStaBandSteerDB->BssIdMacAddr[5]);

                        pwssBandWlanDB =
                            (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

                        if (pwssBandWlanDB == NULL)
                        {
                            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                        "WssStaProcessAssocFrames:- "
                                        "Unable to allocate memory from UtlShMemAllocWlanDbBuf\r\n");
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                         pwssDeauthMsg);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        passocWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        ploadWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                            return OSIX_FAILURE;
                        }

                        MEMSET (pwssBandWlanDB, 0, sizeof (tWssWlanDB));

                        MEMCPY (pwssBandWlanDB->WssWlanAttributeDB.BssId,
                                pWssStaBandSteerDB->BssIdMacAddr,
                                sizeof (tMacAddr));

                        pwssBandWlanDB->WssWlanIsPresentDB.bWtpInternalId =
                            OSIX_TRUE;
                        pwssBandWlanDB->WssWlanIsPresentDB.bBssIfIndex =
                            OSIX_TRUE;

                        if (WssIfProcessWssWlanDBMsg
                            (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssBandWlanDB))
                        {
                            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                        "WssStaProcessAssocFrames: "
                                        "Failed to retrieve data from WssWlanDB\r\n");
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                         pwssDeauthMsg);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        passocWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        ploadWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssBandWlanDB);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                            return OSIX_FAILURE;
                        }

                        WSSSTA_TRC2 (WSSSTA_BANDSELECT_TRC,
                                     "Band steer entry created with internal id = %d, association request received on internal id = %d \r\n",
                                     pwssBandWlanDB->WssWlanAttributeDB.
                                     u2WtpInternalId, u2WtpInternalId);

                        if (pwssBandWlanDB->WssWlanAttributeDB.
                            u2WtpInternalId == u2WtpInternalId)
                        {
                            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                        "STA entry available in band steer DB ...!\r\n");
                            WSSSTA_TRC6 (WSSSTA_BANDSELECT_TRC,
                                         "WssStaProcessAssocFrames:"
                                         " Station Entry present for %2x:%2x:%2x:%2x:%2x:%2x\r\n",
                                         staMacAddr[0], staMacAddr[1],
                                         staMacAddr[2], staMacAddr[3],
                                         staMacAddr[4], staMacAddr[5]);

                            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                        "WssStaProcessAssocFrames:"
                                        " Sending Assoc Reject for the station \r\n");
                            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gi4StaSysLogId,
                                          "Sending Assoc Reject for the station!!!"));
                            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gi4StaSysLogId,
                                          "Reason: Bandsteer enabled and req recvd in 2.4GHz Freq!!!"));
                            if (pWssStaBandSteerDB->u1StaAssocCount == 0)
                            {
                                /* Stop the already running timer, if any */
                                WlcHdlrStaAssocTmrStop (pWssStaBandSteerDB);
                                /* Start Assoc Timer.  */
                                WlcHdlrStaAssocCountTmrStart
                                    (pWssStaBandSteerDB,
                                     pwssWlanDB->WssWlanAttributeDB.
                                     u1AssocResetTime);
                            }
                            pWssStaBandSteerDB->u1StaAssocCount++;
                            if (pWssStaBandSteerDB->u1StaAssocCount ==
                                pwssWlanDB->WssWlanAttributeDB.
                                u1AssocRejectCount)
                            {
                                WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                            "Deleting the station entry from "
                                            "Band Steer DB since assoc reset count reached ... \r\n");
                                /* Delete the BAND Steer DB and Stop both Timers */
                                WlcHdlrProbeTmrStop (pWssStaBandSteerDB);
                                if (WssStaUpdateBandSteerProcessDB
                                    (WSSSTA_DESTROY_BAND_STEER_DB,
                                     pWssStaBandSteerDB) == OSIX_FAILURE)
                                {
                                    WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                                "WssStaProcessAssocFrames: "
                                                "Failed to Delete the entry in bandSteerDB \r\n");
                                    UtlShMemFreeWlcBuf ((UINT1 *)
                                                        pWlcHdlrMsgStruct);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssMsgRsp);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssDeauthMsg);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                passocWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                ploadWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                pwssStaDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssWlanDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssBandWlanDB);
                                    return OSIX_FAILURE;
                                }
                            }
                            else
                            {
                                if (WssStaUpdateBandSteerProcessDB
                                    (WSSSTA_SET_BAND_STEER_DB,
                                     pWssStaBandSteerDB) == OSIX_FAILURE)
                                {
                                    WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                                "WssStaProcessAssocFrames: "
                                                "Failed to set Band steer DB\r\n");
                                    UtlShMemFreeWlcBuf ((UINT1 *)
                                                        pWlcHdlrMsgStruct);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssMsgRsp);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssDeauthMsg);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                passocWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                ploadWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                pwssStaDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssWlanDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssBandWlanDB);
                                    return OSIX_FAILURE;
                                }
                            }
                            pwssMsgRsp->unMacMsg.AssocRspMacFrame.StatCode.
                                u2MacFrameStatusCode = WSSSTA_BANDSELECT;
                            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                    MacMgmtFrmHdr.u1DA,
                                    assocReqMacFrame.MacMgmtFrmHdr.u1SA,
                                    sizeof (tMacAddr));
                            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                    MacMgmtFrmHdr.u1SA,
                                    assocReqMacFrame.MacMgmtFrmHdr.u1DA,
                                    sizeof (tMacAddr));
                            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                    MacMgmtFrmHdr.u1BssId,
                                    assocReqMacFrame.MacMgmtFrmHdr.u1BssId,
                                    sizeof (tMacAddr));
                            pwssMsgRsp->unMacMsg.AssocRspMacFrame.u2SessId =
                                assocReqMacFrame.u2SessId;

                            MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct),
                                    pwssMsgRsp, sizeof (tWssMacMsgStruct));

                            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                        "Sending Assoc Response with band steer reject code... !!!\n");

                            if (!WssIfProcessWlcHdlrMsg
                                (WSS_WLCHDLR_MAC_ASSOC_RSP, pWlcHdlrMsgStruct))
                            {
                                UtlShMemFreeWlcBuf ((UINT1 *)
                                                    pWlcHdlrMsgStruct);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssMsgRsp);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssDeauthMsg);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            passocWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            ploadWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pwssStaDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                       pwssBandWlanDB);
                                return OSIX_SUCCESS;
                            }
                            else
                            {
                                UtlShMemFreeWlcBuf ((UINT1 *)
                                                    pWlcHdlrMsgStruct);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssMsgRsp);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssDeauthMsg);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            passocWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            ploadWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pwssStaDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                       pwssBandWlanDB);
                                return OSIX_FAILURE;
                            }
                        }

                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssBandWlanDB);
                    }

                }

            }
        }
    }
#endif
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;
    WssStaVerifyMaxClientCount (u4WlanIndex, &u4StaCount, staMacAddr);
    WssStaVerifyRadioMaxClientCount (u4RadioIfIndex, &u4StaCount1, staMacAddr);
    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
    pwssWlanDB->WssWlanIsPresentDB.bMaxClientCount = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames:- "
                    "WSS_WLAN_GET_IFINDEX_ENTRY FAILED to get MaxClientCount\r\n");
    }
    /* Construct the Assoc Response msg */
    /* Kloc Fix Start */
    MEMSET (pwssMsgRsp, 0, sizeof (tWssMacMsgStruct));
    /* Kloc Fix Ends */
    MEMSET (&(pwssMsgRsp->unMacMsg.AssocRspMacFrame), 0,
            sizeof (tDot11AssocRspMacFrame));
    MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u1DA,
            assocReqMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
    MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u1SA,
            assocReqMacFrame.MacMgmtFrmHdr.u1DA, sizeof (tMacAddr));
    MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.MacMgmtFrmHdr.u1BssId,
            assocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    pwssMsgRsp->unMacMsg.AssocRspMacFrame.u2SessId = assocReqMacFrame.u2SessId;

    /* Construct Deauth Frame */
    MEMSET (&(pwssDeauthMsg->unMacMsg.DeauthMacFrame), 0,
            sizeof (tDot11DeauthMacFrame));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA,
            assocReqMacFrame.MacMgmtFrmHdr.u1DA, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA,
            assocReqMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId,
            assocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    pwssDeauthMsg->unMacMsg.DeauthMacFrame.u2SessId = assocReqMacFrame.u2SessId;

    MEMSET (&(passocWssStaDB->WssStaWtpAssocCountInfo), 0,
            sizeof (tWssIfAuthWtpAssocCountInfo));
    MEMSET (&(ploadWssStaDB->WssStaLoadBalanceDB), 0,
            sizeof (tWssIfAuthLoadBalanceDB));

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
    if (pWssStaWepProcessDB != NULL)
    {
#if 0
        /* Commenting the below code to not send DeAuth when the station fame recieved
         * with non class 2 option */
        if (pWssStaWepProcessDB->stationState !=
            AUTH_UNASSOC && pWssStaWepProcessDB->stationState != AUTH_ASSOC)
        {
            CAPWAP_TRC (CAPWAP_STATION_TRC,
                        "WssStaProcessAssocFrames :: STATION State not AUTH_ASSOC. Sending DEAUTH \r\n");
            pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.
                u2MacFrameReasonCode = WSSSTA_NONAUTH_CLASS2;
            MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
                    sizeof (tWssMacMsgStruct));
            pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;
            pWssStaWepProcessDB->bStationAssocFlag = OSIX_TRUE;
            if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG,
                                         pWlcHdlrMsgStruct))
            {
                CAPWAP_TRC (CAPWAP_STATION_TRC,
                            "WssStaProcessAssocFrames :: DEAUTH Sent \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_SUCCESS;
            }
            else
            {
                CAPWAP_TRC (CAPWAP_STATION_TRC,
                            "WssStaProcessAssocFrames :: Failed to send DEAUTH  \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
        }
#endif
    }
    else
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames:"
                    " Station not found \r\n");
        pwssMsgRsp->unMacMsg.AssocRspMacFrame.StatCode.u2MacFrameStatusCode =
            WSSSTA_ASS_DENIED_UNKNOWN;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_ASSOC_RSP,
                                     pWlcHdlrMsgStruct))
        {
            CAPWAP_TRC (CAPWAP_STATION_TRC,
                        "WssStaProcessAssocFrames :: ASSOC Response Sent \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_SUCCESS;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_STATION_TRC,
                        "WssStaProcessAssocFrames :: Failed to send Assoc Response \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }

    /* Get the Assoc count */
    WssStaShowClient ();
    passocWssStaDB->WssStaWtpAssocCountInfo.u2WtpId = u2WtpId;
    if ((WssIfProcessWssAuthDBMsg
         (WSS_AUTH_GET_WTP_ASSOC_COUNT_DB, passocWssStaDB)) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    if ((passocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount >
         (MAX_STA_SUPP_PER_WLC - 2)) &&
        (passocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount <=
         MAX_STA_SUPP_PER_WLC))
    {
        MEMCPY (ploadWssStaDB->WssStaLoadBalanceDB.stationMacAddress,
                staMacAddr, sizeof (tMacAddr));
        ploadWssStaDB->WssStaLoadBalanceDB.u4BssIfIndex = u4BssIfIndex;
        if (!WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STA_ASSOC_REJECT_COUNT_DB,
                                       ploadWssStaDB))
        {
            if (ploadWssStaDB->WssStaLoadBalanceDB.u4AssocRejectionCount <
                MAX_WSSSTA_ASSOC_REJECTCOUNT)
            {
                ploadWssStaDB->WssStaLoadBalanceDB.u4AssocRejectionCount++;
                if (WssIfProcessWssAuthDBMsg
                    (WSS_AUTH_SET_ASSOC_REJECT_COUNT_DB,
                     ploadWssStaDB) != OSIX_SUCCESS)
                {
                }

#ifdef WSSSTA_AUTH
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.StatCode.
                    u2MacFrameStatusCode = WSSSTA_ASS_TOO_MANY_MS;
                MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                        sizeof (tWssMacMsgStruct));
                if (!WssIfProcessWlcHdlrMsg
                    (WSS_WLCHDLR_MAC_ASSOC_RSP, pWlcHdlrMsgStruct))
                {
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    return OSIX_SUCCESS;
                }
                else
                {
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    return OSIX_FAILURE;
                }
#endif
            }
        }

    }

    if ((gu4ClientWalkIndex >= MAX_STA_SUPP_PER_WLC) ||
        ((radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount > 0) &&
         (u4StaCount1 > radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount))
        || ((pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount > 0)
            && (u4StaCount >
                pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount)) ||
        (i1RetValValidateStaDiscovery == OSIX_FAILURE))
    {
        if (i1RetValValidateStaDiscovery == OSIX_FAILURE)
        {
            WSSSTA_TRC2 (WSSSTA_FAILURE_TRC,
                         "STA DISCOVERY MODE FAILED ..... "
                         "Rejecting Station Association\r\n", NULL, NULL);
        }
        else
        {
            WSSSTA_TRC2 (WSSSTA_FAILURE_TRC,
                         "MAX CLIENT COUNT (Radio - %d/Wlan - %d) REACHED....."
                         "Rejecting Station Association\r\n",
                         radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount,
                         pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount);
        }
        pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode =
            WSSSTA_DEAUTH_MS_LEAVING;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
                sizeof (tWssMacMsgStruct));
        pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG,
                                     pWlcHdlrMsgStruct))
        {
            /*MAX STATION EXCEEDED TRAP */
            MEMCPY (sStationTrapInfo.au1BaseWtpId, staMacAddr, MAC_ADDR_LEN);
            sStationTrapInfo.u4ClientCount =
                radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount;
            /* Retrivie the profile Index from the local database */
            if (WssIfGetProfileIfIndex (u4BssIfIndex,
                                        &u4ProfileIndex) == OSIX_FAILURE)
            {
            }

            sStationTrapInfo.u4ifIndex = u4ProfileIndex;

            WlanSnmpifSendTrap (WSS_WLAN_MAX_USER_PER_WTP, &sStationTrapInfo);

            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }

    /* 
     * Get the validation u4Result and update the u4Result to the status code of
     * association response structure.
     */
    u4Result = ValidateAssocFrame (&assocReqMacFrame,
                                   &(pwssMsgRsp->unMacMsg.AssocRspMacFrame),
                                   &radioIfgetDB, u2Capability);
    /*Association Id is set as 0xFFFF, if  association id has depleted. Hence
     * error code is sent in association response*/
    if (pWssStaWepProcessDB->u2AssociationID == WSSSTA_ASSOC_ID_DEPLETED)
    {
        u4Result = WSSSTA_ASS_TOO_MANY_MS;
    }

    if (u4Result == OSIX_SUCCESS)
    {
        u2Aid = pWssStaWepProcessDB->u2AssociationID;
        pwssMsgRsp->unMacMsg.AssocRspMacFrame.Aid.u2MacFrameAID = u2Aid;

        pwssMsgRsp->unMacMsg.AssocRspMacFrame.StatCode.u2MacFrameStatusCode =
            (UINT2) u4Result;

        /* Fill the Assoc resp Capability and Supported Rates with the values
         * received in the Assoc request */
        pwssMsgRsp->unMacMsg.AssocRspMacFrame.Capability.u2MacFrameCapability =
            assocReqMacFrame.Capability.u2MacFrameCapability;
        pwssMsgRsp->unMacMsg.AssocRspMacFrame.Capability.u2MacFrameCapability |=
            WSSSTA_CAPABILITY_MASK;

        if ((STRLEN (assocReqMacFrame.SuppRate.au1MacFrameSuppRate)) != 0)
        {
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.SuppRate.u1MacFrameElemId =
                WSSMAC_SUPP_RATE_ELEMENT_ID;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.SuppRate.u1MacFrameElemLen =
                assocReqMacFrame.SuppRate.u1MacFrameElemLen;
            if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_BGN)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_B)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BG))
            {
                WssIfGetLegacyRateStatus (&u1LegacyRate);
                if (u1LegacyRate == LEGACY_RATE_ENABLED)
                {
                    u1length = 0;
                    for (u1iter = 0; u1iter <
                         assocReqMacFrame.SuppRate.u1MacFrameElemLen; u1iter++)
                    {
                        if ((assocReqMacFrame.SuppRate.
                             au1MacFrameSuppRate[u1iter] == CLI_OPER_RATEB_1MB)
                            || (assocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_OPER_RATEB_2MB)
                            || (assocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_OPER_RATEB_5MB)
                            || (assocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_BSS_RATEB_1MB)
                            || (assocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_BSS_RATEB_2MB)
                            || (assocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_BSS_RATEB_5MB))
                        {
                            continue;
                        }
                        else
                        {
                            pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                SuppRate.au1MacFrameSuppRate[u1length] =
                                assocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter];
                            u1length++;
                        }
                    }
                    pwssMsgRsp->unMacMsg.AssocRspMacFrame.SuppRate.
                        u1MacFrameElemLen = u1length;
                }
                else
                {
                    MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                            SuppRate.au1MacFrameSuppRate,
                            assocReqMacFrame.SuppRate.au1MacFrameSuppRate,
                            STRLEN (assocReqMacFrame.SuppRate.
                                    au1MacFrameSuppRate));
                }
            }
        }

        if ((STRLEN (assocReqMacFrame.ExtSuppRates.au1MacFrameExtSuppRates)) !=
            0)
        {
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                ExtSuppRates.u1MacFrameElemId = WSSMAC_EXT_SUPP_RATE_ELEMENTID;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                ExtSuppRates.u1MacFrameElemLen =
                assocReqMacFrame.ExtSuppRates.u1MacFrameElemLen;
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                    ExtSuppRates.au1MacFrameExtSuppRates,
                    assocReqMacFrame.ExtSuppRates.au1MacFrameExtSuppRates,
                    STRLEN (assocReqMacFrame.
                            ExtSuppRates.au1MacFrameExtSuppRates));
        }
/* Check whether STA contains WMM tag */
        if (assocReqMacFrame.WMMParam.u1MacFrameElemId ==
            WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            u2QosCapab = (UINT2) ((u2Capability & 0x0200) >> SHIFT_VALUE_9);
            /* Fill EDCA elements */

/* Check whether AP is QOS configured or not */
            if (u2QosCapab == 1)
            {
                for (u1QosIndex = 1; u1QosIndex <= WSSIF_RADIOIF_QOS_CONFIG_LEN;
                     u1QosIndex++)
                {
                    radioIfgetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
                    radioIfgetDB.RadioIfGetAllDB.u1QosIndex = u1QosIndex;
                    radioIfgetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bAdmissionControl =
                        OSIX_TRUE;
                    radioIfgetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB,
                                                  &radioIfgetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                                    "Failed to retrieve data from RadioIfDB\r\n");
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                        return OSIX_FAILURE;

                    }

                }
                radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[BEST_EFFORT_ACI]
                    = WSSIF_RADIOIF_DEF_TXOP_BE;
                if (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_B
                    || radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BG
                    || radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BGN)
                {
                    radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI]
                        = WSSIF_RADIOIF_DEF_TXOP_VI_B;
                }
                else
                {
                    radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI]
                        = WSSIF_RADIOIF_DEF_TXOP_VI;
                }
                radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[VOICE_ACI]
                    = WSSIF_RADIOIF_DEF_TXOP_VO;
                radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[BACKGROUND_ACI]
                    = WSSIF_RADIOIF_DEF_TXOP_BG;
                radioIfgetDB.RadioIfGetAllDB.
                    au1AdmissionControl[BEST_EFFORT_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_BE;
                radioIfgetDB.RadioIfGetAllDB.au1AdmissionControl[VIDEO_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_VI;
                radioIfgetDB.RadioIfGetAllDB.au1AdmissionControl[VOICE_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_VO;
                radioIfgetDB.RadioIfGetAllDB.
                    au1AdmissionControl[BACKGROUND_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_BG;
                /*STA-DB set -  WMM ENABLED STA */
                pWssStaWepProcessDB->u1StationWmmEnabled = 1;
                /*updating WMM parameter elements */
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.
                    u1MacFrameElemId = WSSMAC_VENDOR_INFO_ELEMENTID;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.
                    u1MacFrameElemLen = WSSSTA_WMM_FRAME_LENGTH;
                MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.au1OUI,
                        u1OUI, sizeof (u1OUI));
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.u1OUIType =
                    WSSSTA_WMM_OUI_TYPE;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.u1OUISubType =
                    WSSSTA_WMM_OUI_SUB_TYPE;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.u1WMMVersion =
                    WSSSTA_WMM_VERSION;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.u1QOSInfo = 0x00;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.WMMParam.u1Rsvrd =
                    WSSSTA_WMM_RESERVED;

                /*adding Best Effort Edca params */
                u1AIFS_BE = u1AIFS_BE | (BEST_EFFORT_ACI << SHIFT_VALUE_5);
                u1AIFS_BE =
                    (UINT1) (u1AIFS_BE |
                             (radioIfgetDB.
                              RadioIfGetAllDB.
                              au1AdmissionControl[BEST_EFFORT_ACI] <<
                              SHIFT_VALUE_4));
                u1AIFS_BE =
                    u1AIFS_BE | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[BEST_EFFORT_ACI];
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_BE;
                u1ECW_BE =
                    (UINT1) (u1ECW_BE |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[BEST_EFFORT_ACI] << SHIFT_VALUE_4));
                u1ECW_BE =
                    (UINT1) (u1ECW_BE | radioIfgetDB.RadioIfGetAllDB.
                             au2CwMin[BEST_EFFORT_ACI]);
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ECWMin_Max = u1ECW_BE;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BE.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) (radioIfgetDB.RadioIfGetAllDB.
                             au2TxOpLimit[BEST_EFFORT_ACI] << SHIFT_VALUE_8);
                /*adding Background Edca params */
                u1AIFS_BG = u1AIFS_BG | (VIDEO_ACI << SHIFT_VALUE_5);
                u1AIFS_BG =
                    (UINT1) (u1AIFS_BG |
                             (radioIfgetDB.
                              RadioIfGetAllDB.
                              au1AdmissionControl[BACKGROUND_ACI] <<
                              SHIFT_VALUE_4));
                u1AIFS_BG =
                    u1AIFS_BG | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[BACKGROUND_ACI];
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_BG;
                u1ECW_BG =
                    (UINT1) (u1ECW_BG |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[BACKGROUND_ACI] << SHIFT_VALUE_4));
                u1ECW_BG =
                    (UINT1) (u1ECW_BG |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMin[BACKGROUND_ACI]));
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ECWMin_Max = u1ECW_BG;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BK.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) (radioIfgetDB.RadioIfGetAllDB.
                             au2TxOpLimit[BACKGROUND_ACI] << SHIFT_VALUE_8);
                /*adding Video Edca params */

                u1AIFS_VI = u1AIFS_VI | (VOICE_ACI << SHIFT_VALUE_5);
                u1AIFS_VI = (UINT1)
                    (u1AIFS_VI |
                     (radioIfgetDB.
                      RadioIfGetAllDB.au1AdmissionControl[VIDEO_ACI] <<
                      SHIFT_VALUE_4));
                u1AIFS_VI =
                    u1AIFS_VI | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[VIDEO_ACI];
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_VI;
                u1ECW_VI =
                    (UINT1) (u1ECW_VI |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[VIDEO_ACI] << SHIFT_VALUE_4));
                u1ECW_VI =
                    (UINT1) (u1ECW_VI |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMin[VIDEO_ACI]));

                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ECWMin_Max = u1ECW_VI;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VI.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) ((radioIfgetDB.RadioIfGetAllDB.
                              au2TxOpLimit[VIDEO_ACI]) << SHIFT_VALUE_8);
                /*adding Voice Edca params */
                u1AIFS_VO =
                    (UINT1) (u1AIFS_VO | (BACKGROUND_ACI << SHIFT_VALUE_5));
                u1AIFS_VO =
                    (UINT1) (u1AIFS_VO |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au1AdmissionControl[VIDEO_ACI] << SHIFT_VALUE_4));
                u1AIFS_VO =
                    u1AIFS_VO | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[VOICE_ACI];
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_VO;
                u1ECW_VO =
                    (UINT1) (u1ECW_VO |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[VOICE_ACI] << SHIFT_VALUE_4));
                u1ECW_VO =
                    (UINT1) (u1ECW_VO | radioIfgetDB.RadioIfGetAllDB.
                             au2CwMin[VOICE_ACI]);
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ECWMin_Max = u1ECW_VO;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VO.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) ((radioIfgetDB.RadioIfGetAllDB.
                              au2TxOpLimit[VOICE_ACI]) << SHIFT_VALUE_8);
            }
            else
            {
                /*STA-DB set -  WMM DISABLED AP */
                pWssStaWepProcessDB->u1StationWmmEnabled = 0;
            }
        }
        else
        {
            /*STA-DB set -  WMM DISABLED STA */
            pWssStaWepProcessDB->u1StationWmmEnabled = 0;
        }
#ifdef WPS_WANTED
        pWssStaWepProcessDB->bDeAuthDueToWps = OSIX_FALSE;
        if (assocReqMacFrame.WpsCapability.elemId ==
            WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.elemId =
                WSSMAC_VENDOR_INFO_ELEMENTID;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.Len =
                assocReqMacFrame.WpsCapability.Len;
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.Oui,
                    assocReqMacFrame.WpsCapability.Oui, 4);
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.Version,
                    assocReqMacFrame.WpsCapability.Version, 5);
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.ReqTypeTag[0] =
                0x10;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.ReqTypeTag[1] =
                0x3B;
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.
                    ReqTypeLen, assocReqMacFrame.WpsCapability.ReqTypeLen, 2);
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.ReqType = 0x03;
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.extVal,
                    assocReqMacFrame.WpsCapability.extVal,
                    assocReqMacFrame.WpsCapability.extLen);
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.WpsCapability.extLen =
                assocReqMacFrame.WpsCapability.extLen;
            WssWpsNotifyParams.WPSASSOCIND.bWpsPresent = OSIX_TRUE;
            pWssStaWepProcessDB->bDeAuthDueToWps = OSIX_TRUE;
        }
        WssWpsNotifyParams.eWssWpsNotifyType = WSSWPS_ASSOC_IND;
        WssWpsNotifyParams.WPSASSOCIND.radioType =
            radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType;
        MEMCPY (WssWpsNotifyParams.WPSASSOCIND.au1StaMac,
                assocReqMacFrame.MacMgmtFrmHdr.u1SA, WSSMAC_MAC_ADDR_LEN);
        MEMCPY (WssWpsNotifyParams.WPSASSOCIND.au1ApMac,
                assocReqMacFrame.MacMgmtFrmHdr.u1DA, WSSMAC_MAC_ADDR_LEN);
        WssWpsNotifyParams.WPSASSOCIND.u2SessId = assocReqMacFrame.u2SessId;
        WssWpsNotifyParams.u4IfIndex = u4BssIfIndex;
        WssWpsNotifyParams.WPSASSOCIND.u1RadioId = u1RadioId;
        WssWpsNotifyParams.WPSASSOCIND.u2WtpId = u2WtpId;
        if (assocReqMacFrame.RSNInfo.u1MacFrameElemId == WSSMAC_RSN_ELEMENT_ID)
        {
            WssWpsNotifyParams.WPSASSOCIND.bRsnaPresent = OSIX_TRUE;
        }
        /* Send Notification msg to Wps */
        if (WpsProcessWssNotification (&WssWpsNotifyParams) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessAssocReqFrames:- WpsProcessWssNotification"
                        "returned Failure !!!! \n");
            return (OSIX_FAILURE);
        }
#endif
        if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RADIO_TYPE_BGN) ||
            (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RADIO_TYPE_AN) ||
            (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_NG)
            || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC))
        {
            if (assocReqMacFrame.HTCapabilities.u1MacFrameElemId
                == WSSMAC_HT_CAPAB_ELEMENT_ID)
            {
                /* Reading the HT Cap value from the RadioDB */
                WssStaGetDo111nValues (&radioIfgetDB);
                /* Assemble HT Ext Capabilities */
                if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_EXTCAP_INFO,
                                              &radioIfgetDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                                "Failed to retrieve data from RadioIfDB\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    return OSIX_FAILURE;

                }
                /* Assemble Beamforming */
                if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_BEAMFORMING_INFO,
                                              &radioIfgetDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                                "Failed to retrieve data from RadioIfDB\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    return OSIX_FAILURE;
                }
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                    HTCapabilities.u1MacFrameElemId =
                    WSSMAC_HT_CAPAB_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                    HTCapabilities.u1MacFrameElemLen =
                    WSSMAC_HT_CAPAB_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                    HTCapabilities.u2HTCapInfo =
                    ((assocReqMacFrame.HTCapabilities.u2HTCapInfo) &
                     OSIX_HTONS (radioIfgetDB.RadioIfGetAllDB.
                                 Dot11NCapaParams.u2HtCapInfo));

                MEMSET (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                        HTCapabilities.au1SuppMCSSet, VALUE_0,
                        WSSMAC_SUPP_MCS_SET);
                if ((assocReqMacFrame.HTCapabilities.au1SuppMCSSet[0] !=
                     VALUE_0)
                    && (radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[0] != VALUE_0))
                {
                    u1McsFlag++;
                }
                if ((assocReqMacFrame.HTCapabilities.au1SuppMCSSet[1] !=
                     VALUE_0)
                    && (radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[1] != VALUE_0))
                {
                    u1McsFlag++;
                }
                if ((assocReqMacFrame.HTCapabilities.au1SuppMCSSet[2] !=
                     VALUE_0)
                    && (radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[2] != VALUE_0))
                {
                    u1McsFlag++;
                }
                if (u1McsFlag)
                {
                    for (u1McsIndex = VALUE_0; u1McsIndex < u1McsFlag;
                         u1McsIndex++)
                    {
                        pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTCapabilities.
                            au1SuppMCSSet[u1McsIndex] =
                            radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                            au1HtCapaMcs[u1McsIndex];
                    }
                }
                MEMCPY (&pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTCapabilities.
                        au1SuppMCSSet[3],
                        &radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[3], (DOT11N_HT_CAP_MCS_LEN - VALUE_3));
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTCapabilities.
                    u1AMPDUParam =
                    ((assocReqMacFrame.HTCapabilities.
                      u1AMPDUParam) & (radioIfgetDB.RadioIfGetAllDB.
                                       Dot11NampduParams.u1AmpduParam));
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTCapabilities.
                    u2HTExtCap =
                    ((assocReqMacFrame.HTCapabilities.
                      u2HTExtCap) & (OSIX_HTONS (radioIfgetDB.RadioIfGetAllDB.
                                                 Dot11NhtExtCapParams.
                                                 u2HtExtCap)));
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTCapabilities.
                    u4TranBeamformCap =
                    ((assocReqMacFrame.HTCapabilities.
                      u4TranBeamformCap) & (OSIX_HTONL (radioIfgetDB.
                                                        RadioIfGetAllDB.
                                                        Dot11NhtTxBeamCapParams.
                                                        u4TxBeamCapParam)));
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTCapabilities.u1ASELCap =
                    assocReqMacFrame.HTCapabilities.u1ASELCap;
                /* HT Operation for 802.11n */
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                    HTOperation.u1MacFrameElemId = WSSMAC_HT_OPE_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                    HTOperation.u1MacFrameElemLen = WSSMAC_HT_OPE_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTOperation.u1PrimaryCh =
                    radioIfgetDB.RadioIfGetAllDB.u1CurrentChannel;
                MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                        HTOperation.au1HTOpeInfo,
                        radioIfgetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1HTOpeInfo, WSSMAC_HTOPE_INFO);
                MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.HTOperation.
                        au1BasicMCSSet,
                        radioIfgetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);
            }
        }
        if (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
        {
            radioIfgetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
            if ((WssIfProcessRadioIfDBMsg
                 (WSS_GET_RADIO_IF_DB, &radioIfgetDB)) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                            "Failed to retrieve 11AC data from RadioIfDB\r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                return OSIX_FAILURE;
            }
            radioIfgetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_FALSE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_FALSE;

            radioIfgetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperModeElem = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
            if ((WssIfProcessRadioIfDBMsg
                 (WSS_GET_RADIO_IF_DB_11AC, &radioIfgetDB)) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                            "Failed to retrieve 11AC data from RadioIfDB\r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                return OSIX_FAILURE;
            }

            if (assocReqMacFrame.VHTCapabilities.u1ElemId ==
                WSSMAC_VHT_CAPAB_ELEMENT_ID)
            {
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTCapabilities.u1ElemId =
                    WSSMAC_VHT_CAPAB_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTCapabilities.
                    u1ElemLen = WSSMAC_VHT_CAPAB_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTCapabilities.
                    u4VhtCapaInfo =
                    ((assocReqMacFrame.VHTCapabilities.u4VhtCapaInfo) &
                     (OSIX_HTONL (radioIfgetDB.RadioIfGetAllDB.
                                  Dot11AcCapaParams.u4VhtCapInfo)));

                for (u1Loop = 0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
                {
                    if (radioIfgetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs[u1Loop] == RADIO_11AC_VHT_MCS_DISABLE)
                    {
                        pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTCapabilities.
                            au1SuppMCS[u1Loop] = radioIfgetDB.RadioIfGetAllDB.
                            Dot11AcCapaParams.au1VhtCapaMcs[u1Loop];
                    }
                    else
                    {
                        pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTCapabilities.
                            au1SuppMCS[u1Loop] =
                            ((assocReqMacFrame.VHTCapabilities.
                              au1SuppMCS[u1Loop]) | (radioIfgetDB.
                                                     RadioIfGetAllDB.
                                                     Dot11AcCapaParams.
                                                     au1VhtCapaMcs[u1Loop]));
                    }
                }
/*For Highest data rate computation and updation in assoc response*/
                MEMCPY (radioIfgetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs, pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                        VHTCapabilities.au1SuppMCS, sizeof (radioIfgetDB.
                                                            RadioIfGetAllDB.
                                                            Dot11AcCapaParams.
                                                            au1VhtCapaMcs));
                WssIfVhtHighestSuppDataRate (&radioIfgetDB);
                MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                        VHTCapabilities.au1SuppMCS,
                        radioIfgetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs, sizeof (radioIfgetDB.RadioIfGetAllDB.
                                               Dot11AcCapaParams.
                                               au1VhtCapaMcs));
                radioIfgetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
            }

            pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTOperation.u1ElemId =
                WSSMAC_VHT_OPE_ELEMENT_ID;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTOperation.u1ElemLen =
                WSSMAC_VHT_OPE_ELEMET_LEN;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTOperation.u2BasicMCS =
                (OSIX_HTONS (radioIfgetDB.RadioIfGetAllDB.Dot11AcOperParams.
                             u2VhtOperMcsSet));
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.VHTOperation.
                    au1VhtOperInfo, radioIfgetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.au1VhtOperInfo,
                    sizeof (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                            VHTOperation.au1VhtOperInfo));

            if ((assocReqMacFrame.OperModeNotify.u1ElemId ==
                 WSSMAC_VHT_OPERATING_MODE_NOTIFICATION) &&
                (radioIfgetDB.RadioIfGetAllDB.Dot11AcOperParams.
                 u1VhtOperModeNotify == OSIX_TRUE))
            {
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.OperModeNotify.
                    u1ElemId = WSSMAC_VHT_OPERATING_MODE_NOTIFICATION;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.OperModeNotify.
                    u1ElemLen = WSSMAC_OPER_NOTIFY_ELEM_LEN;
                WssIfAssembleOperModeNotify (&radioIfgetDB);
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.OperModeNotify.
                    u1VhtOperModeElem = ((assocReqMacFrame.OperModeNotify.
                                          u1VhtOperModeElem) & (radioIfgetDB.
                                                                RadioIfGetAllDB.
                                                                Dot11AcOperParams.
                                                                u1VhtOperModeElem));
            }
        }

        if (assocReqMacFrame.ExtCapabilities.u1MacFrameElemId ==
            WSSMAC_EXT_CAPAB_ELEMENT_ID)
        {
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.ExtCapabilities.
                u1MacFrameElemId = WSSMAC_EXT_CAPAB_ELEMENT_ID;
            pwssMsgRsp->unMacMsg.AssocRspMacFrame.ExtCapabilities.
                u1MacFrameElemLen =
                assocReqMacFrame.ExtCapabilities.u1MacFrameElemLen;
            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.ExtCapabilities.
                    au1Capabilities,
                    assocReqMacFrame.ExtCapabilities.au1Capabilities,
                    sizeof (assocReqMacFrame.ExtCapabilities.au1Capabilities));
            if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
                && (radioIfgetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeNotify == OSIX_TRUE))
            {
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.ExtCapabilities.
                    au1Capabilities[RADIO_VALUE_7] = (pwssMsgRsp->unMacMsg.
                                                      AssocRspMacFrame.
                                                      ExtCapabilities.
                                                      au1Capabilities
                                                      [RADIO_VALUE_7] |
                                                      RADIO_VALUE_4);
            }
        }
        if (assocReqMacFrame.RSNInfo.u1MacFrameElemId != WSSMAC_RSN_ELEMENT_ID)
        {
#ifdef WSSUSER_WANTED
            if (WssUserGetSessionEntry (staMacAddr) == OSIX_SUCCESS)
            {
                MEMCPY (pwssStaDB->WssIfAuthStateDB.stationMacAddress,
                        staMacAddr, sizeof (tMacAddr));

                if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                              pwssStaDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames "
                                "Failed to get station DB details\r\n");
                }

                pwssStaDB->WssIfAuthStateDB.bStaRedirectionURL = OSIX_TRUE;
                pwssStaDB->WssIfAuthStateDB.u1StaAuthFinished = OSIX_FALSE;
                if (CapwapSendWebAuthStatus (staMacAddr,
                                             pwssStaDB->WssIfAuthStateDB.
                                             u1StaAuthFinished,
                                             pwssStaDB->WssIfAuthStateDB.
                                             aWssStaBSS[0].u2SessId) !=
                    OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames "
                                " WEB-AUTH Status processing failed\r\n");
                }

                if (WssIfProcessWssAuthDBMsg (WSS_AUTH_DB_UPDATE_RULENUM,
                                              pwssStaDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames "
                                "WssIfProcessWssAuthDBMsg returned failure\r\n");
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    return OSIX_FAILURE;
                }
#ifdef KERNEL_CAPWAP_WANTED
                /* Update the Kernel module */
                if (CapwapUpdateKernelStaTable (staMacAddr) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames "
                                " Station not found in kernel DB \r\n");
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    return OSIX_FAILURE;
                }
#endif

                MEMCPY (WssUserNotifyParams.staMac, staMacAddr, MAC_ADDR_LEN);
                WssUserNotifyParams.eWssUserNotifyType = WSSUSER_SESSION_DELETE;
                WssUserNotifyParams.u4TerminateCause = CALLBACK;
                if (WssUserProcessNotification (&WssUserNotifyParams)
                    == OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames "
                                "WssUserProcessNotification returned failure\r\n");
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    return OSIX_FAILURE;
                }
            }
#endif
        }
#ifdef RSNA_WANTED
        else if (assocReqMacFrame.RSNInfo.u1MacFrameElemId ==
                 WSSMAC_RSN_ELEMENT_ID)
        {
            /* 802.1x Authentication has to start only after recieving the Station
             * Configuration Response Message.
             * Therefore storing the required RSNA fields in the station database
             * and a trigger to RSNA will be sent, once the Station Config Response
             * is recieved*/
            /* This function will send the delete station message,
             * which will delete the exisiting RSNA session key*/
#ifdef PMF_WANTED
            if (RsnaApiIsStaPMFCapable (staMacAddr, u4BssIfIndex) !=
                OSIX_SUCCESS)
#endif
            {
                RsnaHandleExistingStationAssocRequest (staMacAddr, u4BssIfIndex,
                                                       &bSessionDelete);
                if (bSessionDelete == OSIX_TRUE)
                {
#ifdef WSSUSER_WANTED
                    MEMCPY (WssUserNotifyParams.staMac, staMacAddr,
                            MAC_ADDR_LEN);
                    WssUserNotifyParams.eWssUserNotifyType =
                        WSSUSER_SESSION_DELETE;
                    WssUserNotifyParams.u4TerminateCause = CALLBACK;
                    if (WssUserProcessNotification (&WssUserNotifyParams)
                        == OSIX_FAILURE)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessDeAuthFrames "
                                    "WssUserProcessNotification returned failure\r\n");
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        return OSIX_FAILURE;
                    }
#endif
                }
            }
#ifdef PMF_WANTED
            else
            {
                bPMFSessionExist = OSIX_TRUE;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.TimeoutInterval.
                    u1MacFrameElemId = WSSMAC_TIMEOUT_INTERVAL_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.TimeoutInterval.
                    u1MacFrameElemLen = WSSMAC_TIMEOUT_INTERVAL_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.TimeoutInterval.
                    u1TimeoutIntervalType = WSS_MAC_ASSOC_COMEBACK_TIME;
                pwssMsgRsp->unMacMsg.AssocRspMacFrame.TimeoutInterval.
                    u4TimeoutIntervalValue = 2000;

            }
#endif
            if (pWssStaWepProcessDB != NULL)
            {
                pWssStaWepProcessDB->u4RsnIeLength = (UINT4)
                    (assocReqMacFrame.RSNInfo.u1MacFrameElemLen + 2);
                /* 2 denotes RSN_VERSION_OFF used in Parsing rsn ie at rsnaproc.c */
                MEMCPY (pWssStaWepProcessDB->au1RsnIE,
                        &assocReqMacFrame.RSNInfo,
                        sizeof (assocReqMacFrame.RSNInfo));
                pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
                pWssStaWepProcessDB->u1AssocMode = WSSSTA_WPA2_MODE;
            }
        }
#endif

#ifdef WPA_WANTED
        if (assocReqMacFrame.WpaInfo.elemId == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            if (pWssStaWepProcessDB != NULL)
            {
                if (MEMCMP (assocReqMacFrame.WpaInfo.Oui, au1WpaOUI, 4) == 0)
                {
                    RsnaHandleExistingStationAssocRequest (staMacAddr,
                                                           u4BssIfIndex,
                                                           &bSessionDelete);
                    MEMCPY (pWssStaWepProcessDB->au1WpaIE,
                            &assocReqMacFrame.WpaInfo,
                            assocReqMacFrame.WpaInfo.Len + 2);
                    pWssStaWepProcessDB->u1AssocMode = WSSSTA_WPA1_MODE;
                }
                pWssStaWepProcessDB->u4WpaIeLength = (UINT4)
                    (assocReqMacFrame.WpaInfo.Len + 2);
                pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
            }
        }
#endif

        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_ASSOC_RSP,
                                    pWlcHdlrMsgStruct) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                        "WSS_WLCHDLR_MAC_ASSOC_RSP returned FAILURE\r\n");
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

#ifdef PMF_WANTED
        if (bPMFSessionExist == OSIX_TRUE)
        {
            if ((RsnaApiSendSAQueryReq (staMacAddr, u4BssIfIndex, u2SessId)) !=
                OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                            "RSNA SA Query Send returned FAILURE\r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
        }
#endif

        pWssStaWepProcessDB->u2AssociationID = u2Aid;
        pWssStaWepProcessDB->u2Sessid = u2SessId;
        pWssStaWepProcessDB->WssstaParams.u2CapabilityInfo = u2Capability;

        wssStaParams.u2CapabilityInfo = u2Capability;
        passocWssStaDB->WssStaWtpAssocCountInfo.u2WtpId = u2WtpId;
        passocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount++;
        if (WssIfProcessWssAuthDBMsg (WSS_AUTH_SET_WTP_ASSOC_COUNT_DB,
                                      passocWssStaDB) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                        "WSS_AUTH_SET_WTP_ASSOC_COUNT_DB returned FAILURE\r\n");

        }
        pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
        if (pwlcHdlrMsg == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessAssocFrames:- "
                        "UtlShMemAllocWlcBuf returned failure\n");
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId =
            assocReqMacFrame.u2SessId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageType =
            ADD_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageLength =
            WSSSTA_ADDSTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1MacAddrLen =
            sizeof (tMacAddr);
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.StaMacAddress,
                staMacAddr, sizeof (tMacAddr));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2VlanId = u2VlanId;

        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2MessageType =
            IEEE_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2MessageLength =
            WSSSTA_IEEESTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2AssocId = u2Aid;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1Flags = TRUE;
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                StaMsg.stationMacAddress, staMacAddr, sizeof (tMacAddr));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2Capab = u2WtpId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1WlanId = u1WlanID;
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.au1SuppRates,
                radioIfgetDB.RadioIfGetAllDB.au1SupportedRate,
                STRLEN (radioIfgetDB.RadioIfGetAllDB.au1SupportedRate));

        /*    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex; */
        /* radioIfgetDB.RadioIfGetAllDB.u4BssIfIndex = u4PresentBssIfIndex; */
#ifdef RSNA_WANTED
        if (u1AuthAlgo == WSS_STA_AUTH_ALGO_SHARED)
        {
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.isPresent =
                TRUE;
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2MessageType =
                IEEE_STATION_SESSION_KEY;
            pwlcHdlrMsg->StationConfReq.wssStaConfig.
                StaSessKey.u2MessageLength = (UINT2) (20 + STRLEN (au1WepKey));
            MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.stationMacAddress, staMacAddr,
                    sizeof (tMacAddr));
#ifdef PMF_WANTED
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2Flags |=
                (bPMFSessionExist << RSNA_C_FLAG_MASK);
#else
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2Flags = 0;
#endif

            MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.au1PairwiseTSC, 0, 6);
            MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.au1PairwiseRSC, 0, 6);
            MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.au1Key,
                    au1WepKey, STRLEN (au1WepKey));
        }
#endif
/*STA Configuration Request*/
        pwlcHdlrMsg->StationConfReq.vendSpec.isOptional = TRUE;
        pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pwlcHdlrMsg->StationConfReq.vendSpec.elementId = VENDOR_WMM_TYPE;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            isOptional = TRUE;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2MsgEleType = VENDOR_WMM_MSG;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2MsgEleLen = 17;
        pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleLen =
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2MsgEleLen + 4;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u4VendorId = VENDOR_ID;
        MEMCPY (pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WMMStaVendor.StaMacAddr, staMacAddr, sizeof (tMacAddr));
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u1WmmStaFlag = pWssStaWepProcessDB->u1StationWmmEnabled;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2TcpPort = (UINT2) HttpGetSourcePort ();

        /* Check whether user role is enabled and it is a dot1x/web authentication
         * with radius authentication. If so, dont create TC filters with bandwidth
         * threshold. The TC filters will be created with the USER ROLE 
         * configured bandwidth */
        nmhGetFsDot11WlanLoginAuthentication ((INT4) u4WlanIndex, &i4LoginAuth);
#ifdef RSNA_WANTED
        RsnaGetDot11RSNAEnabled ((INT4) u4WlanIndex, &i4Dot11RSNAEnabled);
        if (i4Dot11RSNAEnabled == RSNA_ENABLED)
        {
            RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled ((INT4)
                                                              u4WlanIndex,
                                                              RSNA_AUTHSUITE_8021X,
                                                              &i4AuthSuiteStatus);
        }
#endif
        /* Need to check if Radius User Role DB is NULL */
#ifdef WSSUSER_WANTED
        bUserRoleStatus = WssUserRoleModuleStatus ();
#else
        bUserRoleStatus = OSIX_SUCCESS;
#endif
        if ((bUserRoleStatus == OSIX_SUCCESS) &&
            (((i4LoginAuth == REMOTE_LOGIN_RADIUS) &&
              (u1WebAuthStatus == WSSWLAN_ENABLE))
#ifdef RSNA_WANTED
             || (i4AuthSuiteStatus == RSNA_ENABLED))
#else
             && (i4AuthSuiteStatus == RSNA_DISABLED))
#endif
            )
        {
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
                u4BandwidthThresh = 0;
        }
        else
        {
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
                u4BandwidthThresh =
                pwssWlanDB->WssWlanAttributeDB.u4BandwidthThresh;
        }

        CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                     "ASSOC:: Configuring Bandwidth Threshold %d for station:%02x:%02x:%02x:%02x:%02x:%02x\n",
                     pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                     WMMStaVendor.u4BandwidthThresh, staMacAddr[0],
                     staMacAddr[1], staMacAddr[2], staMacAddr[3], staMacAddr[4],
                     staMacAddr[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "ASSOC:: Configuring Bandwidth Threshold %d for station:%02x:%02x:%02x:%02x:%02x:%02x\n",
                      pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                      WMMStaVendor.u4BandwidthThresh, staMacAddr[0],
                      staMacAddr[1], staMacAddr[2], staMacAddr[3],
                      staMacAddr[4], staMacAddr[5]));

        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg)
            == OSIX_FAILURE)
        {
            pwssDisassocMacFrame =
                (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
            if (pwssDisassocMacFrame == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaProcessAssocFrames:-"
                            "UtlShMemAllocMacMsgStructBuf returned failure\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
            CAPWAP_TRC (CAPWAP_STATION_TRC,
                        "WssStaProcessAssocFrames : WSS_WLCHDLR_STATION_CONF_REQ Failed . SendingDISASOC with RC: WSSSTA_DISASS_TOO_MANY_MSG ");

            /* Kloc Fix Start */
            MEMSET (pwssDisassocMacFrame, 0, sizeof (tWssMacMsgStruct));
            /* Kloc Fix Ends */
            MEMSET (&(pwssDisassocMacFrame->unMacMsg.DisassocMacFrame), 0,
                    sizeof (tDot11DisassocMacFrame));
            MEMCPY (pwssDisassocMacFrame->unMacMsg.
                    DisassocMacFrame.MacMgmtFrmHdr.u1DA,
                    assocReqMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
            MEMCPY (pwssDisassocMacFrame->unMacMsg.
                    DisassocMacFrame.MacMgmtFrmHdr.u1SA,
                    assocReqMacFrame.MacMgmtFrmHdr.u1DA, sizeof (tMacAddr));
            MEMCPY (pwssDisassocMacFrame->unMacMsg.
                    DisassocMacFrame.MacMgmtFrmHdr.u1BssId,
                    assocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
            pwssDisassocMacFrame->unMacMsg.DisassocMacFrame.u2SessId =
                assocReqMacFrame.u2SessId;
            pwssDisassocMacFrame->unMacMsg.DisassocMacFrame.ReasonCode.
                u2MacFrameReasonCode = WSSSTA_DISASS_TOO_MANY_MS;
            MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDisassocMacFrame,
                    sizeof (tWssMacMsgStruct));
            pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x07;
            if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DISASSOC_MSG,
                                         pWlcHdlrMsgStruct))
            {
                CAPWAP_TRC (CAPWAP_STATION_TRC,
                            "WssStaProcessAssocFrames :  DISASSOC Sent \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDisassocMacFrame);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_SUCCESS;
            }
            else
            {
                CAPWAP_TRC (CAPWAP_STATION_TRC,
                            "WssStaProcessAssocFrames :  Sending Disassoc Failed \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDisassocMacFrame);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
        }
        wssStaParams.u2CapabilityInfo = u2Capability;
        WssStaConstructDBMsg (WSS_STA_ASSOCIATED, u4BssIfIndex, staMacAddr,
                              u2Aid, u2SessId, &wssStaParams,
                              &(pwssStaDB->WssStaDB));
        if (WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB) ==
            OSIX_SUCCESS)
        {

        }
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
    }
    else
    {
        pwssMsgRsp->unMacMsg.AssocRspMacFrame.StatCode.u2MacFrameStatusCode =
            (UINT2) u4Result;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        CAPWAP_TRC (CAPWAP_STATION_TRC,
                    "WssStaProcessAssocFrames :: Assoc validation Failed \r\n");
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_ASSOC_RSP,
                                     pWlcHdlrMsgStruct))
        {
            CAPWAP_TRC (CAPWAP_STATION_TRC,
                        "WssStaProcessAssocFrames :: ASSOC Response Sent\r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_SUCCESS;
        }
        else
        {
            CAPWAP_TRC (CAPWAP_STATION_TRC,
                        "WssStaProcessAssocFrames :: Failed to send ASSOC Response \r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) passocWssStaDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    UNUSED_PARAM (u2WtpInternalId);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : ValidateReAssocFrame                                   *
 *                                                                           *
 *  Description     : This function is used to validate the ReAssoc params.  *
 *  Input(s)        : ReAssocReqMacFrame, WlanIndex, ReAssocRspMacFrame      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
ValidateReAssocFrame (tDot11ReassocReqMacFrame * preassocReqMacFrame,
                      tDot11ReassocRspMacFrame * pReassocRspMacFrame,
                      tRadioIfGetDB * pRadioIfGetDB, UINT2 u2Capability)
{
    UINT2               u2ReassocCapability =
        preassocReqMacFrame->Capability.u2MacFrameCapability;
    UINT2               u2Index = 0;
    UINT1               ChanlNum = 0, NumOfChanl = 0;
    UINT2               u2Count = 0, u2SupportedChannelSet = 0;
    UINT4               u4RadioType = 0;
    UNUSED_PARAM (pReassocRspMacFrame);
    UNUSED_PARAM (u2Index);

    if ((UINT1) pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel <
        preassocReqMacFrame->PowCapability.u1MacFramePowCap_MaxTxPow)
    {
        return WSSSTA_POW_CAPABILITY_UNSUP;
    }

    u4RadioType = pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
    if ((((u2Capability & 0x0100) >> WSSSTA_SHIFT_8) == 1)
        && ((u4RadioType == RADIO_TYPE_A) || (u4RadioType == RADIO_TYPE_AN)
            || (u4RadioType == RADIO_TYPE_AC)))
    {
        if (ValidateSpectrumBit (u2ReassocCapability, u2Capability) !=
            OSIX_SUCCESS)
        {
            return WSSSTA_SPECTRUMMGMT_UNSUP;
        }
        u2SupportedChannelSet =
            (preassocReqMacFrame->SuppChanls.u1MacFrameElemLen) / 2;
        while (u2Count < u2SupportedChannelSet)
        {
            ChanlNum =
                preassocReqMacFrame->SuppChanls.SubBandDes[u2Count].
                u1MacFrameSuppChanl_FirChanlNum;
            NumOfChanl =
                preassocReqMacFrame->SuppChanls.SubBandDes[u2Count].
                u1MacFrameSuppChanl_NumOfChanl;
            while (NumOfChanl)
            {
                if (ChanlNum != pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel)
                {
                    ChanlNum = ChanlNum + (UINT1) 4;
                    NumOfChanl--;
                    continue;
                }
                break;
            }
            if (NumOfChanl == 0)
            {
                u2Count++;
                continue;
            }
            break;
        }
        if (u2Count == u2SupportedChannelSet)
            return WSSSTA_CHANNELS_UNSUP;
    }

#ifdef WSSSTA_AUTH
    if (u2ReassocCapability == u2Capability)
    {
        pReassocRspMacFrame->Capability.u2MacFrameCapability = u2Capability;
        if (!strncmp (preassocReqMacFrame->SuppRate.au1MacFrameSuppRate,
                      pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate,
                      WSSMAC_MAX_SUPP_RATE_LEN))
        {
            pReassocRspMacFrame->SuppRate.u1MacFrameElemId =
                WSSMAC_SUPP_RATE_ELEMENT_ID;
            pReassocRspMacFrame->SuppRate.u1MacFrameElemLen = 4;
            MEMCPY (pReassocRspMacFrame->SuppRate.au1MacFrameSuppRate,
                    pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate,
                    strlen (pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate));
            return WSSSTA_SUCCESSFUL;
        }
        else
        {
            return WSSSTA_DATARATE_UNSUP;
        }

    }
    else
    {
        for (u2Index = 0; u2Index <= MAX_CAPABILITY; u2Index++)
        {
            if ((u2ReassocCapability & 1 << u2Index) !=
                (u2Capability & 1 << u2Index))
            {
                if (u2Index == WSSSTA_OFDM_CAPABILITY)
                {
                    return WSSSTA_DSSS_OFDM_UNSUP;
                }
                else if (u2Index == WSSSTA_SHORTSLOTTIME_CAPABILITY)
                {
                    return WSSSTA_SHORTSLOTTIME_UNSUP;
                }
                else if (u2Index == WSSSTA_SPECTRUMMGMT_CAPABILITY)
                {
                    return WSSSTA_SPECTRUMMGMT_UNSUP;
                }
                else if (u2Index == WSSSTA_CHANNELAGILITY_CAPABILITY)
                {
                    return WSSSTA_CHANNELAGILITY_UNSUP;
                }
                else if (u2Index == WSSSTA_PBCC_CAPABILITY)
                {
                    return WSSSTA_PBCC_UNSUP;
                }
                else if (u2Index == WSSSTA_SHORTPREAMBLE_CAPABILITY)
                {
                    return WSSSTA_SHORTPREAMBLE_UNSUP;
                }
                else
                {
                    return WSSSTA_UNSUP_CAP;
                }
            }
        }
    }
#endif
    /* TBD construct tStationConfigInfo structure and call to capwap module */

    return WSSSTA_SUCCESSFUL;
}

/*****************************************************************************
 *  Function Name   : WssStaProcessDisAssocFrames                           *
 *                                                                           *
 *  Description     : This function is used to Disassociate a station.       *
 *  Input(s)        : DisAssoc Mac Management Frame                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaProcessDisAssocFrames (tWssStaMsgStruct * pMsgStruct)
{

    tMacAddr            staMacAddr;
    UINT4               u4BssIfIndex = 0;
    tDot11DisassocMacFrame macDisassocFrame;
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    UINT1               u1RadioId = 0;
    tWssIfPMDB          WssIfPMDB;
    tRadioIfGetDB       radioIfgetDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4ProfileIndex = 0;
    tWlanSTATrapInfo    stationTrap;
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";
#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;
#endif
#ifdef WPS_WANTED
    tWssWpsNotifyParams WssWpsNotifyParams;
    MEMSET (&WssWpsNotifyParams, 0, sizeof (tWssWpsNotifyParams));
#endif

#ifdef RSNA_WANTED
    tWssRSNANotifyParams WssRSNANotifyParams;
    UINT4               u4WlanIfIndex = 0;
    INT4                i4RetValDot11RSNAEnabled = RSNA_DISABLED;

    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
#endif
    MEMSET (&stationTrap, 0, sizeof (tWlanSTATrapInfo));
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessDisAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }
    macDisassocFrame = pMsgStruct->unAuthMsg.WssMacDisassocMacFrame;

    MEMCPY (staMacAddr, macDisassocFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));

    WssUtilGetTimeStr (ac1TimeStr);
    CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                 "%s : DISASSOC Req from %02x:%02x:%02x:%02x:%02x:%02x\n",
                 ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                 staMacAddr[3], staMacAddr[4], staMacAddr[5]);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                  "%s : DISASSOC Req recvd from %02x:%02x:%02x:%02x:%02x:%02x",
                  ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                  staMacAddr[3], staMacAddr[4], staMacAddr[5]));

    MEMCPY (pwssStaDB->WssIfAuthStateDB.stationMacAddress, staMacAddr,
            sizeof (tMacAddr));

    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            macDisassocFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u4BssIfIndex = pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDisAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));

    radioIfgetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDisAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;

    WssStaConstructDBMsg (WSS_STA_DIASSOCIATED, u4BssIfIndex, staMacAddr,
                          0, 0, NULL, &(pwssStaDB->WssStaDB));

    /*Reset all PM stats to 0 once station is disconnected */
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.tWtpPmAttState.bWssPmVSPClientStatsSet = OSIX_TRUE;
    MEMCPY (&WssIfPMDB.FsWlanClientStatsMACAddress, staMacAddr, MAC_ADDR_LEN);
    if (WssIfProcessPMDBMsg
        (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET FAILURE\r\n");
    }
    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
    if (pWssStaWepProcessDB != NULL)
    {
        u4IpAddress = pWssStaWepProcessDB->u4StationIpAddress;
        ArpGetIntfForAddr (u4ContextId, u4IpAddress, &u4IfIndex);
        if (ArpDeleteArpCacheWithIndex (u4IfIndex, u4IpAddress) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDisAssocFrames: "
                        "ArpDeleteArpCacheWithIndex FAILURE\r\n");
        }
    }
#ifdef WPS_WANTED
    /* MLME disassoc indication */
    MEMSET (WssWpsNotifyParams.WPSDISSOCIND.au1StaMac, 0, 6);
    MEMCPY (WssWpsNotifyParams.WPSDISSOCIND.au1StaMac, staMacAddr, 6);
    WssWpsNotifyParams.u4IfIndex = u4BssIfIndex;
    WssWpsNotifyParams.eWssWpsNotifyType = WSSWPS_DISSOC_IND;

    /* Send Notification msg to Wps */
    if (WpsProcessWssNotification (&WssWpsNotifyParams) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessAssocReqFrames:- WpsProcessWssNotification"
                    "returned Failure !!!! \n");
        return (OSIX_FAILURE);
    }
#endif
#ifdef RSNA_WANTED
    /* MLME disassoc indication */

    if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4WlanIfIndex) == OSIX_FAILURE)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return (OSIX_FAILURE);
    }
    if (RsnaGetDot11RSNAEnabled ((INT4) u4WlanIfIndex,
                                 &i4RetValDot11RSNAEnabled) != SNMP_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                    "WssStaProcessDisAssocFrames:- "
                    "Failed to retrive Rsna status\n");
    }
#ifdef WPS_WANTED
    if (MEMCMP (gau1DeAuthWpsstaMacAddr, staMacAddr, 6) != 0)
    {
#endif
        if (i4RetValDot11RSNAEnabled == RSNA_ENABLED)
        {

            MEMSET (WssRSNANotifyParams.MLMEDISSOCIND.au1StaMac, 0, 6);
            MEMCPY (WssRSNANotifyParams.MLMEDISSOCIND.au1StaMac, staMacAddr, 6);
            WssRSNANotifyParams.u4IfIndex = u4BssIfIndex;
            WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_DISSOC_IND;

            /* Send Notification msg to Rsna */
            if (RsnaProcessWssNotification (&WssRSNANotifyParams) ==
                OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaProcessDisAssocFrames:- RsnaProcessWssNotification"
                            "returned Failure !!!! \n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return (OSIX_FAILURE);
            }
        }
#ifdef WPS_WANTED
    }
    else
    {
        MEMSET (gau1DeAuthWpsstaMacAddr, 0, 6);
    }
#endif
#endif

#ifdef WSSUSER_WANTED

    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));

    MEMCPY (WssUserNotifyParams.staMac, staMacAddr, MAC_ADDR_LEN);
    WssUserNotifyParams.eWssUserNotifyType = WSSUSER_SESSION_DELETE;
    WssUserNotifyParams.u4TerminateCause = USER_REQUEST;
    if (WssUserProcessNotification (&WssUserNotifyParams) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDisAssocFrames "
                    "WssUserProcessNotification returned failure\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
#endif

#ifdef RFMGMT_WANTED
    if (WssStaRFClientDBDelete (u4BssIfIndex, staMacAddr) != OSIX_SUCCESS)
    {

        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessDisAssocFrames: RF Client " "DB Delete Fail");
    }
#endif

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfProcessWssAuthDBMsg failed\r\n");
    }
    else
    {
        /*STATION DISCONNECTION TRAP */
        MEMCPY (stationTrap.au1CliMac, staMacAddr, MAC_ADDR_LEN);
        stationTrap.u4AssocStatus = WSS_WLAN_STA_DISCONNECTED;

        /* Retrivie the profile Index from the local database */
        if (WssIfGetProfileIfIndex (u4BssIfIndex,
                                    &u4ProfileIndex) == OSIX_FAILURE)
        {
        }

        stationTrap.u4ifIndex = u4ProfileIndex;

        WlanSnmpifSendTrap (WSS_WLAN_CONNECTION_STATUS, &stationTrap);

        pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
        if (pwlcHdlrMsg == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessDisAssocFrames:- "
                        "UtlShMemAllocWlcBuf returned failure\n");
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageType =
            DELETE_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageLength =
            WSSSTA_DELSTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId =
            macDisassocFrame.u2SessId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1MacAddrLen = 6;
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.StaMacAddr,
                staMacAddr, sizeof (tMacAddr));
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg))
        {
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaProcessDeStaFrames                             *
 *                                                                           *
 *  Description     : This function is used to Deauthenticate a station.     *
 *  Input(s)        : Deauth Mac Management Frame                            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaProcessDeAuthFrames (tWssStaMsgStruct * pMsgStruct)
{

    tMacAddr            staMacAddr;
    tMacAddr            bssIfMacAddr;
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    UINT4               u4BssIfIndex = 0;
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    UINT1               u1RadioId;
    tDot11DeauthMacFrame macDeauthFrame;
    tWssWlanDB         *pwssWlanDB = NULL;
    tRadioIfGetDB       radioIfgetDB;
    tWssIfPMDB          WssIfPMDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4ProfileIndex = 0;
    tWlanSTATrapInfo    stationTrap;
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";
#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;

#endif

#ifdef RSNA_WANTED
    tWssRSNANotifyParams WssRSNANotifyParams;
    UINT4               u4WlanIfIndex = 0;
    INT4                i4RetValDot11RSNAEnabled = RSNA_DISABLED;

    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
#endif
    MEMSET (&stationTrap, 0, sizeof (tWlanSTATrapInfo));
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessDeAuthFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    macDeauthFrame = pMsgStruct->unAuthMsg.WssMacDeauthMacFrame;

    MEMCPY (staMacAddr, macDeauthFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
    MEMCPY (bssIfMacAddr, macDeauthFrame.MacMgmtFrmHdr.u1BssId,
            sizeof (tMacAddr));
    MEMCPY (pwssStaDB->WssIfAuthStateDB.stationMacAddress, staMacAddr,
            sizeof (tMacAddr));

    WssUtilGetTimeStr (ac1TimeStr);
    CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                 "%s : DEAUTH Req from %02x:%02x:%02x:%02x:%02x:%02x\n",
                 ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                 staMacAddr[3], staMacAddr[4], staMacAddr[5]);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                  "%s : DEAUTH Req from %02x:%02x:%02x:%02x:%02x:%02x",
                  ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                  staMacAddr[3], staMacAddr[4], staMacAddr[5]));

    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            macDeauthFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve data1 from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u4BssIfIndex = pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve data2 from WssWlanDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));

    radioIfgetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;
    WssStaConstructDBMsg (WSS_STA_DEAUTHENTICATED, u4BssIfIndex, staMacAddr, 0,
                          0, NULL, &(pwssStaDB->WssStaDB));

    /*Reset all PM stats to 0 once station is disconnected */
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));
    WssIfPMDB.tWtpPmAttState.bWssPmVSPClientStatsSet = OSIX_TRUE;
    MEMCPY (&WssIfPMDB.FsWlanClientStatsMACAddress, staMacAddr, MAC_ADDR_LEN);
    if (WssIfProcessPMDBMsg
        (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET, &WssIfPMDB) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeStaFrames: "
                    "WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET FAILURE\r\n");
    }

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
    if (pWssStaWepProcessDB != NULL)
    {
        u4IpAddress = pWssStaWepProcessDB->u4StationIpAddress;
        if (ArpGetIntfForAddr (u4ContextId, u4IpAddress, &u4IfIndex) ==
            ARP_SUCCESS)
        {
            if (ArpDeleteArpCacheWithIndex (u4IfIndex, u4IpAddress) ==
                OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames: "
                            "ArpDeleteArpCacheWithIndex FAILURE\r\n");
            }
        }
    }

#ifdef RSNA_WANTED
    /* MLME deauth indication */
    if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4WlanIfIndex) == OSIX_FAILURE)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return (OSIX_FAILURE);
    }

    if (RsnaGetDot11RSNAEnabled ((INT4) u4WlanIfIndex,
                                 &i4RetValDot11RSNAEnabled) != SNMP_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                    "WssStaProcessDeAuthFrames:- "
                    "Failed to retrive Rsna status\n");
    }
    if (i4RetValDot11RSNAEnabled == RSNA_ENABLED)
    {
        MEMSET (WssRSNANotifyParams.MLMEDEAUTHIND.au1StaMac,
                0, WSSMAC_MAC_ADDR_LEN);
        MEMCPY (WssRSNANotifyParams.MLMEDEAUTHIND.au1StaMac,
                staMacAddr, WSSMAC_MAC_ADDR_LEN);
        WssRSNANotifyParams.u4IfIndex = u4BssIfIndex;
        WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_DEAUTH_IND;
        WSSSTA_TRC (WSSSTA_MGMT_TRC,
                    "DeAuthentication received from station\n");
        /* Send Notification msg to Rsna */
        if (RsnaProcessWssNotification (&WssRSNANotifyParams) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessDeAuthFrames:-"
                        " RsnaProcessWssNotification"
                        "returned Failure !!!! \n");
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return (OSIX_FAILURE);
        }
    }
#endif

#ifdef RFMGMT_WANTED
    if (WssStaRFClientDBDelete (u4BssIfIndex, staMacAddr) != OSIX_SUCCESS)
    {

        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessDeAuthFrames: RF Client " "DB Delete Fail");
    }
#endif

#ifdef WSSUSER_WANTED
    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));

    MEMCPY (WssUserNotifyParams.staMac, staMacAddr, MAC_ADDR_LEN);
    WssUserNotifyParams.eWssUserNotifyType = WSSUSER_SESSION_DELETE;
    WssUserNotifyParams.u4TerminateCause = USER_REQUEST;
    if (WssUserProcessNotification (&WssUserNotifyParams) == OSIX_FAILURE)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessDeAuthFrames "
                    "WssUserProcessNotification returned failure\r\n");
        return OSIX_FAILURE;
    }
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
    WssUserSetBandWidth (staMacAddr, 1, 0, 0, OSIX_TRUE);
#endif
#endif

    if (!WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB))
    {
        /*STATION DISCONNECTION TRAP */
        MEMCPY (stationTrap.au1CliMac, staMacAddr, MAC_ADDR_LEN);
        stationTrap.u4AssocStatus = WSS_WLAN_STA_DISCONNECTED;

        /* Retrivie the profile Index from the local database */
        if (WssIfGetProfileIfIndex (u4BssIfIndex,
                                    &u4ProfileIndex) == OSIX_FAILURE)
        {
        }

        stationTrap.u4ifIndex = u4ProfileIndex;

        WlanSnmpifSendTrap (WSS_WLAN_CONNECTION_STATUS, &stationTrap);

        pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
        if (pwlcHdlrMsg == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessDeAuthFrames:- "
                        "UtlShMemAllocWlcBuf returned failure\n");
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageType =
            DELETE_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageLength =
            WSSSTA_DELSTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId =
            macDeauthFrame.u2SessId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1MacAddrLen = 6;
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.StaMacAddr,
                staMacAddr, sizeof (tMacAddr));

        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg))
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssStaProcessReAssocFrames                            *
 *                                                                           *
 *  Description     : This function is used to ReAssociate a station.        *
 *  Input(s)        : ReAssoc Mac Frame                                      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaProcessReAssocFrames (tWssStaMsgStruct * pMsgStruct)
{

    tWssMacMsgStruct   *pwssMsgRsp = NULL;
    tWssMacMsgStruct   *pwssDeauthMsg = NULL;
    tMacAddr            staMacAddr;
    tMacAddr            presentBssAddr;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4PresentBssIfIndex = 0;
    UINT4               u4WlanIndex = 0;
    UINT4               u4StaCount = 0;
    UINT4               u4StaCount1 = 0;
    UINT4               u4ProfileIndex = 0;
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    tWssifauthDBMsgStruct *pcurrentAssocWssStaDB = NULL;
    tWssifauthDBMsgStruct *preassocWssStaDB = NULL;
    tWssifauthDBMsgStruct *ploadWssStaDB = NULL;
    tDot11ReassocReqMacFrame reassocReqMacFrame;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT2               u2WtpId = 0;
    UINT2               u2CurrentWtpId = 0;
    UINT2               u2WtpInternalId = 0;
    tWssStaParams       wssStaParams;
    UINT2               u2Aid;
    UINT4               u4Result = 0;
    UINT1               u1RadioId;
    INT1                i1RetValValidateStaDiscovery = 0;
    tRadioIfGetDB       radioIfgetDB;
    UINT2               u2Capability;
    UINT1               u1Index;
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    UINT1               u1IndexToGetStaDB = 0;
    UINT1               u1WlanID = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT1               u1McsFlag = 0;
    UINT1               u1McsIndex = 0;
    UINT1               u1Loop;
#ifdef PMF_WANTED
    BOOL1               bPMFSessionExist = OSIX_FALSE;
#endif
    UINT1               u1iter = 0;
    UINT1               u1LegacyRate = 0;
    UINT1               u1length = 0;
#ifdef BAND_SELECT_WANTED
    UINT1               u1BandSelectGlobStatus = 0;
    tWssWlanDB         *pwssWlanBandDB = NULL;
    tWssStaBandSteerDB  WssStaBandSteerDB;
    tWssStaBandSteerDB *pWssStaBandSteerDB = NULL;

    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));

#endif
    tWlanSTATrapInfo    sStationTrapInfo;
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";
#ifdef RSNA_WANTED
    tWssRSNANotifyParams WssRSNANotifyParams;
    BOOL1               bSessionDelete = OSIX_FALSE;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;
#endif
#ifdef WPA_WANTED
    UINT1               au1WpaOUI[] = { 0x00, 0x50, 0xf2, 0x01 };
#endif
    tWssMacMsgStruct   *pwssDisassocMacFrame = NULL;
    UINT2               u2SessId = 0;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4RadioIfIndex;
    UINT2               u2VlanId = 0;
    UINT1               u1AuthAlgo = 0;
    UINT1               au1WepKey[MAX_WEP_KEY_LENGTH] = "\0";
    UINT1               u1Flag = 1;
    INT4                i4LoginAuth = 0;
    INT4                i4AuthSuiteStatus = RSNA_DISABLED;
    UINT1               u1WebAuthStatus = WSSWLAN_DISABLE;

#ifdef NPAPI_WANTED
    tWlanClientParams   WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpWrAdd  WlanClientNpWrAdd;
    tWlanClientNpWrDelete WlanClientNpWrDelete;
    tWlanParams         WlanParams;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&WlanParams, 0, sizeof (tWlanParams));
    MEMSET (&WlanClientNpWrAdd, 0, sizeof (tWlanClientNpWrAdd));
    MEMSET (&WlanClientNpWrDelete, 0, sizeof (tWlanClientNpWrDelete));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
#endif
    UINT2               u2QosCapab = 0;
    UINT1               u1QosIndex = 0;
    UINT1               u1OUI[3] = { 0x00, 0x50, 0xF2 };
    UINT1               u1AIFS_BE = 0;
    UINT1               u1ECW_BE = 0;
    UINT1               u1AIFS_BG = 0;
    UINT1               u1ECW_BG = 0;
    UINT1               u1AIFS_VI = 0;
    UINT1               u1ECW_VI = 0;
    UINT1               u1AIFS_VO = 0;
    UINT1               u1ECW_VO = 0;
#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;

    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));
#endif
    BOOL1               bUserRoleStatus = OSIX_FAILURE;
#ifdef RSNA_WANTED
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
#endif
    MEMSET (&sStationTrapInfo, 0, sizeof (tWlanSTATrapInfo));
    pwssMsgRsp = (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssMsgRsp == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessReAssocFrames:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        return OSIX_FAILURE;
    }
    MEMSET (pwssMsgRsp, 0, sizeof (tWssMacMsgStruct));
    UNUSED_PARAM (pwssDisassocMacFrame);
    UNUSED_PARAM (u1Index);
    MEMSET (au1WepKey, 0, MAX_WEP_KEY_LENGTH);

    /* reassocReqMacFrame= pMsgStruct->unAuthMsg.WssMacReassocReqMacFrame; */
    MEMSET (&reassocReqMacFrame, 0, sizeof (tDot11ReassocReqMacFrame));
    MEMCPY (&reassocReqMacFrame,
            &(pMsgStruct->unAuthMsg.WssMacReassocReqMacFrame),
            sizeof (tDot11ReassocReqMacFrame));
    u2SessId = reassocReqMacFrame.u2SessId;

    MEMCPY (staMacAddr,
            reassocReqMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));

    WssUtilGetTimeStr (ac1TimeStr);
    CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                 "%s : REASSOC Req recvd from %02x:%02x:%02x:%02x:%02x:%02x\n",
                 ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                 staMacAddr[3], staMacAddr[4], staMacAddr[5]);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                  "%s : REASSOC Req recvd from %02x:%02x:%02x:%02x:%02x:%02x",
                  ac1TimeStr, staMacAddr[0], staMacAddr[1], staMacAddr[2],
                  staMacAddr[3], staMacAddr[4], staMacAddr[5]));

    i1RetValValidateStaDiscovery = WssStaUtilValidateDiscoveryMode (staMacAddr);

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessReAssocFrames:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    pwssDeauthMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssDeauthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessReAssocFrames:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    pcurrentAssocWssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pcurrentAssocWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessReAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    preassocWssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (preassocWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessReAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    /* Construct Reassoc Response Msg */
    MEMSET (&(pwssMsgRsp->unMacMsg.ReassocRspMacFrame), 0,
            sizeof (tDot11ReassocRspMacFrame));
    MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u1DA,
            staMacAddr, sizeof (tMacAddr));
    MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u1SA,
            reassocReqMacFrame.MacMgmtFrmHdr.u1DA, sizeof (tMacAddr));
    MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.MacMgmtFrmHdr.u1BssId,
            reassocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    pwssMsgRsp->unMacMsg.ReassocRspMacFrame.u2SessId =
        reassocReqMacFrame.u2SessId;

    /* Construct Deauth Msg */
    MEMSET (&(pwssDeauthMsg->unMacMsg.DeauthMacFrame), 0,
            sizeof (tDot11DeauthMacFrame));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA,
            reassocReqMacFrame.MacMgmtFrmHdr.u1DA, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA,
            reassocReqMacFrame.MacMgmtFrmHdr.u1SA, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId,
            reassocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    pwssDeauthMsg->unMacMsg.DeauthMacFrame.u2SessId =
        reassocReqMacFrame.u2SessId;

    /* Get the BssifIndex of PA for reassociation */
    MEMCPY (presentBssAddr, reassocReqMacFrame.CurrAPaddr.au1MacFrameCurrAPAddr,
            sizeof (tMacAddr));
    MEMSET (&(pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo), 0,
            sizeof (tWssIfAuthWtpAssocCountInfo));
    MEMSET (&(preassocWssStaDB->WssStaWtpAssocCountInfo), 0,
            sizeof (tWssIfAuthWtpAssocCountInfo));
    preassocWssStaDB->WssStaWtpAssocCountInfo.u2WtpId = u2WtpId;
    pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo.u2WtpId = u2WtpId;

    /* Get the require data from Wlan DB */
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            reassocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u4BssIfIndex = pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
    u2WtpId = pwssWlanDB->WssWlanAttributeDB.u2WtpInternalId;

    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bMaxClientCount = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWepKey = OSIX_TRUE;

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            reassocReqMacFrame.MacMgmtFrmHdr.u1BssId, sizeof (tMacAddr));

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u4PresentBssIfIndex = pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
    u2CurrentWtpId = pwssWlanDB->WssWlanAttributeDB.u2WtpInternalId;
    u2Capability = pwssWlanDB->WssWlanAttributeDB.u2Capability;
    u1WlanID = pwssWlanDB->WssWlanAttributeDB.u1WlanId;
    u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    u1AuthAlgo = pwssWlanDB->WssWlanAttributeDB.u1AuthMethod;
    u4WlanIndex = pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
    u1WebAuthStatus = pwssWlanDB->WssWlanAttributeDB.u1WebAuthStatus;

    if (u1AuthAlgo == WSS_STA_AUTH_ALGO_SHARED)
    {
        MEMCPY (au1WepKey, pwssWlanDB->WssWlanAttributeDB.au1WepKey,
                STRLEN (pwssWlanDB->WssWlanAttributeDB.au1WepKey));
    }

    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4PresentBssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;

    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
    pwssWlanDB->WssWlanIsPresentDB.bMaxClientCount = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessReAssocFrames:- "
                    "WSS_WLAN_GET_IFINDEX_ENTRY FAILED to get MaxClientCount\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssWlanProcessWssWlanDBMsg: \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId;
    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));

    /* Get the require data from Radio DB */
    /* radioIfgetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE; */
    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bManMCSSet = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bMaxClientCount = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
#ifdef BAND_SELECT_WANTED
    radioIfgetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
#endif
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    /* radioIfgetDB.RadioIfGetAllDB.u4BssIfIndex = u4PresentBssIfIndex; */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

#ifdef BAND_SELECT_WANTED
    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
    if (WssIfGetBandSelectStatus (&u1BandSelectGlobStatus) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to get status of bandsteer \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    if (u1BandSelectGlobStatus != WLAN_DISABLE)
    {
        pwssWlanDB->WssWlanIsPresentDB.bBandSelectStatus = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAssocRejectCount = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAssocResetTime = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
        {
            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC, "WssStaProcessReAssocFrames:- "
                        "WSS_WLAN_GET_IFINDEX_ENTRY FAILED to get BandSteer Values\r\n");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1BandSelectStatus
            == BAND_SELECT_ENABLED)
        {
            /* Check whether the request is received on 2.4 GHz frequency. If yes, then 
             * check the station DB for probe request received on 5 GHz frequency. If yes,
             * send Assoc response as rejected. */
            if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_BGN)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_B)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_G))
            {
                MEMCPY (WssStaBandSteerDB.stationMacAddress, staMacAddr,
                        MAC_ADDR_LEN);
                MEMCPY (WssStaBandSteerDB.BssIdMacAddr, "\0\0\0\0\0\0",
                        MAC_ADDR_LEN);

                while ((pWssStaBandSteerDB = RBTreeGetNext (gWssStaBandSteerDB,
                                                            (tRBElem *) &
                                                            WssStaBandSteerDB,
                                                            NULL)) != NULL)
                {
                    if (MEMCMP (pWssStaBandSteerDB->BssIdMacAddr,
                                reassocReqMacFrame.MacMgmtFrmHdr.u1BssId,
                                sizeof (tMacAddr)) != 0)
                    {
                        WSSSTA_TRC6 (WSSSTA_BANDSELECT_TRC,
                                     "Band steer entry in DB from bssid : %x:%x:%x:%x:%x:%x \r\n",
                                     pWssStaBandSteerDB->BssIdMacAddr[0],
                                     pWssStaBandSteerDB->BssIdMacAddr[1],
                                     pWssStaBandSteerDB->BssIdMacAddr[2],
                                     pWssStaBandSteerDB->BssIdMacAddr[3],
                                     pWssStaBandSteerDB->BssIdMacAddr[4],
                                     pWssStaBandSteerDB->BssIdMacAddr[5]);

                        pwssWlanBandDB =
                            (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

                        if (pwssWlanBandDB == NULL)
                        {
                            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                        "WssStaProcessReAssocFrames:- "
                                        "Unable to allocate memory from UtlShMemAllocWlanDbBuf\r\n");
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                         pwssDeauthMsg);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        preassocWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        ploadWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                            return OSIX_FAILURE;
                        }

                        MEMSET (pwssWlanBandDB, 0, sizeof (tWssWlanDB));

                        MEMCPY (pwssWlanBandDB->WssWlanAttributeDB.BssId,
                                pWssStaBandSteerDB->BssIdMacAddr,
                                sizeof (tMacAddr));

                        pwssWlanBandDB->WssWlanIsPresentDB.bWtpInternalId =
                            OSIX_TRUE;
                        pwssWlanBandDB->WssWlanIsPresentDB.bBssIfIndex =
                            OSIX_TRUE;

                        if (WssIfProcessWssWlanDBMsg
                            (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanBandDB))
                        {
                            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                        "WssStaProcessReAssocFrames: "
                                        "Failed to retrieve data from WssWlanDB\r\n");
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                         pwssDeauthMsg);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        preassocWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                        ploadWssStaDB);
                            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanBandDB);
                            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                            return OSIX_FAILURE;
                        }

                        WSSSTA_TRC2 (WSSSTA_BANDSELECT_TRC,
                                     "Band steer entry created with internal id = %d, "
                                     "ReAssociation request received on internal id = %d \r\n",
                                     pwssWlanBandDB->WssWlanAttributeDB.
                                     u2WtpInternalId, u2WtpInternalId);

                        if (pwssWlanBandDB->WssWlanAttributeDB.
                            u2WtpInternalId == u2WtpInternalId)
                        {
                            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                        "STA entry available in band steer DB ...!\r\n");
                            WSSSTA_TRC6 (WSSSTA_BANDSELECT_TRC,
                                         "WssStaProcessReAssocFrames:"
                                         " Station Entry present for %2x:%2x:%2x:%2x:%2x:%2x\r\n",
                                         staMacAddr[0], staMacAddr[1],
                                         staMacAddr[2], staMacAddr[3],
                                         staMacAddr[4], staMacAddr[5]);

                            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                        "WssStaProcessReAssocFrames:"
                                        " Sending Re-Assoc Reject for the station \r\n");
                            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL,
                                          (UINT4) gi4StaSysLogId,
                                          "Sending Re-Assoc Reject for the station!!!"));
                            SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL,
                                          (UINT4) gi4StaSysLogId,
                                          "Reason: Bandsteer enabled and req recvd in 2.4GHz Freq!!!"));

                            if (pWssStaBandSteerDB->u1StaAssocCount == 0)
                            {
                                /* Stop the already running timer, if any */
                                WlcHdlrStaAssocTmrStop (pWssStaBandSteerDB);
                                /* Start Assoc Timer.  */
                                WlcHdlrStaAssocCountTmrStart
                                    (pWssStaBandSteerDB,
                                     pwssWlanDB->WssWlanAttributeDB.
                                     u1AssocResetTime);
                            }

                            if (pWssStaBandSteerDB->u1StaAssocCount ==
                                pwssWlanDB->WssWlanAttributeDB.
                                u1AssocRejectCount)
                            {
                                WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                            "Deleting the station entry from "
                                            "Band Steer DB since assoc reset count reached ... \r\n");
                                /* Delete the BAND Steer DB and Stop both Timers */
                                WlcHdlrProbeTmrStop (pWssStaBandSteerDB);
                                if (WssStaUpdateBandSteerProcessDB
                                    (WSSSTA_DESTROY_BAND_STEER_DB,
                                     pWssStaBandSteerDB) == OSIX_FAILURE)
                                {
                                    WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                                "WssStaProcessReAssocFrames: "
                                                "Failed to Delete the entry in bandSteerDB \r\n");
                                    UtlShMemFreeWlcBuf ((UINT1 *)
                                                        pWlcHdlrMsgStruct);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssMsgRsp);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssDeauthMsg);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                preassocWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                ploadWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                pwssStaDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssWlanBandDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssWlanDB);
                                    return OSIX_FAILURE;
                                }

                            }
                            else
                            {
                                pWssStaBandSteerDB->u1StaAssocCount++;
                                if (WssStaUpdateBandSteerProcessDB
                                    (WSSSTA_SET_BAND_STEER_DB,
                                     pWssStaBandSteerDB) == OSIX_FAILURE)
                                {
                                    WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                                "WssStaProcessReAssocFrames: "
                                                "Failed to set Band steer DB\r\n");
                                    UtlShMemFreeWlcBuf ((UINT1 *)
                                                        pWlcHdlrMsgStruct);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssMsgRsp);
                                    UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                                 pwssDeauthMsg);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                preassocWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                ploadWssStaDB);
                                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                                pwssStaDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssWlanBandDB);
                                    UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                           pwssWlanDB);
                                    return OSIX_FAILURE;
                                }
                            }
                            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.StatCode.
                                u2MacFrameStatusCode = WSSSTA_BANDSELECT;
                            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                    MacMgmtFrmHdr.u1DA,
                                    reassocReqMacFrame.MacMgmtFrmHdr.u1SA,
                                    sizeof (tMacAddr));
                            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                    MacMgmtFrmHdr.u1SA,
                                    reassocReqMacFrame.MacMgmtFrmHdr.u1DA,
                                    sizeof (tMacAddr));
                            MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                    MacMgmtFrmHdr.u1BssId,
                                    reassocReqMacFrame.MacMgmtFrmHdr.u1BssId,
                                    sizeof (tMacAddr));
                            pwssMsgRsp->unMacMsg.AssocRspMacFrame.u2SessId =
                                reassocReqMacFrame.u2SessId;

                            MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct),
                                    pwssMsgRsp, sizeof (tWssMacMsgStruct));

                            WSSSTA_TRC (WSSSTA_BANDSELECT_TRC,
                                        "Sending Re-Assoc Response with band steer reject code... !!!\n");

                            if (!WssIfProcessWlcHdlrMsg
                                (WSS_WLCHDLR_MAC_REASSOC_RSP,
                                 pWlcHdlrMsgStruct))
                            {
                                UtlShMemFreeWlcBuf ((UINT1 *)
                                                    pWlcHdlrMsgStruct);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssMsgRsp);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssDeauthMsg);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            preassocWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            ploadWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pwssStaDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                       pwssWlanBandDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                                return OSIX_SUCCESS;
                            }
                            else
                            {
                                UtlShMemFreeWlcBuf ((UINT1 *)
                                                    pWlcHdlrMsgStruct);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssMsgRsp);
                                UtlShMemFreeMacMsgStructBuf ((UINT1 *)
                                                             pwssDeauthMsg);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            preassocWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            ploadWssStaDB);
                                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                            pwssStaDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *)
                                                       pwssWlanBandDB);
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                                return OSIX_FAILURE;
                            }
                        }

                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanBandDB);

                    }
                }

            }
        }
    }
#endif

    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;

    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaProcessReAssocFrames:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    WssStaVerifyMaxClientCount (u4WlanIndex, &u4StaCount, staMacAddr);
    WssStaVerifyRadioMaxClientCount (u4RadioIfIndex, &u4StaCount1, staMacAddr);

    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIndex;
    pwssWlanDB->WssWlanIsPresentDB.bBandwidthThresh = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
    {
        CAPWAP_TRC (CAPWAP_STATION_TRC, "WssStaProcessReAssocFrames: "
                    "WSS_WLAN_GET_IFINDEX_ENTRY FAILED to get MaxClientCount\r\n");
    }
    CAPWAP_TRC3 (CAPWAP_STATION_TRC,
                 "REASSOC WLAN : %d, WebAuth : %d, Bandwidth Thresh : %d\n",
                 u4WlanIndex, u1WebAuthStatus,
                 pwssWlanDB->WssWlanAttributeDB.u4BandwidthThresh);

    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            staMacAddr, sizeof (tMacAddr));
    pWssifauthDBMsgStruct->WssIfAuthStateDB.aWssStaBSS[u1IndexToGetStaDB].
        u4BssIfIndex = u4BssIfIndex;

#if 0
    /* Commenting the bewlo code to not return failure when reassoc is recieved and
     * station db is not available */
    if (WssIfProcessWssAuthDBMsg
        (WSS_AUTH_GET_STATION_DB, pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessReAssocFrames:"
                    " Station not found in the DB \r\n");
        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.StatCode.u2MacFrameStatusCode =
            WSSSTA_REASS_DENIED;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_REASSOC_RSP,
                                     pWlcHdlrMsgStruct))
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }
#endif
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
    if (pWssStaWepProcessDB == NULL)
    {
        u1Flag = 0;
        pWssStaWepProcessDB =
            (tWssStaWepProcessDB *) MemAllocMemBlk (WSSSTA_AUTHDB_POOLID);
        if (pWssStaWepProcessDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessReAssocFrames:"
                        "Memory Allocation Failed For WssStaWepProcessDB");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }

    preassocWssStaDB->WssStaWtpAssocCountInfo.u2WtpId = u2WtpId;
    pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo.u2WtpId = u2CurrentWtpId;

    if ((WssIfProcessWssAuthDBMsg
         (WSS_AUTH_GET_WTP_ASSOC_COUNT_DB, preassocWssStaDB)) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWssAuthDBMsg:"
                    "Access to Get assoc count DB failed\r\n");
        if (u1Flag == 1)
        {
            WssFreeAssocId (pWssStaWepProcessDB);
            WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->stationMacAddress);
            RBTreeRem (gWssStaWepProcessDB, (tRBElem *) pWssStaWepProcessDB);
        }
        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                            (UINT1 *) pWssStaWepProcessDB);
        return OSIX_FAILURE;
    }
    if ((WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_WTP_ASSOC_COUNT_DB,
                                   pcurrentAssocWssStaDB)) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWssAuthDBMsg:"
                    "Access to Get assoc count DB failed\r\n");
        if (u1Flag == 1)
        {
            WssFreeAssocId (pWssStaWepProcessDB);
            WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->stationMacAddress);
            RBTreeRem (gWssStaWepProcessDB, (tRBElem *) pWssStaWepProcessDB);
        }
        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                            (UINT1 *) pWssStaWepProcessDB);
        return OSIX_FAILURE;
    }

    if ((pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount >
         (MAX_STA_SUPP_PER_WLC - 2)) &&
        (pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount <=
         MAX_STA_SUPP_PER_WLC))
    {
        ploadWssStaDB =
            (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
        if (ploadWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                        "WssStaProcessReAssocFrames:- "
                        "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_FAILURE;
        }
        MEMSET (&(ploadWssStaDB->WssStaLoadBalanceDB), 0,
                sizeof (tWssIfAuthLoadBalanceDB));

        MEMCPY (ploadWssStaDB->WssStaLoadBalanceDB.stationMacAddress,
                staMacAddr, sizeof (tMacAddr));
        ploadWssStaDB->WssStaLoadBalanceDB.u4BssIfIndex = u4BssIfIndex;
        if (!WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STA_ASSOC_REJECT_COUNT_DB,
                                       ploadWssStaDB))
        {
            if (ploadWssStaDB->WssStaLoadBalanceDB.u4AssocRejectionCount <
                MAX_WSSSTA_ASSOC_REJECTCOUNT)
            {
                ploadWssStaDB->WssStaLoadBalanceDB.u4AssocRejectionCount++;
                if (WssIfProcessWssAuthDBMsg
                    (WSS_AUTH_SET_ASSOC_REJECT_COUNT_DB,
                     ploadWssStaDB) != OSIX_SUCCESS)
                {
                }

                /*
                   pwssMsgRsp->unMacMsg.ReassocRspMacFrame.StatCode.u2MacFrameStatusCode = WSSSTA_ASS_TOO_MANY_MS;
                   MEMCPY(&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp, sizeof(tWssMacMsgStruct));
                   if (!WssIfProcessWlcHdlrMsg(WSS_WLCHDLR_MAC_REASSOC_RSP, pWlcHdlrMsgStruct))
                   {
                   UtlShMemFreeWlcBuf ((UINT1 *)pWlcHdlrMsgStruct);
                   UtlShMemFreeMacMsgStructBuf ((UINT1 *)pwssMsgRsp);
                   UtlShMemFreeMacMsgStructBuf ((UINT1 *)pwssDeauthMsg);
                   UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)pcurrentAssocWssStaDB);
                   UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)preassocWssStaDB);
                   UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)ploadWssStaDB);
                   return OSIX_SUCCESS;
                   }
                   else
                   {
                   UtlShMemFreeWlcBuf ((UINT1 *)pWlcHdlrMsgStruct);
                   UtlShMemFreeMacMsgStructBuf ((UINT1 *)pwssMsgRsp);
                   UtlShMemFreeMacMsgStructBuf ((UINT1 *)pwssDeauthMsg);
                   UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)pcurrentAssocWssStaDB);
                   UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)preassocWssStaDB);
                   UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)ploadWssStaDB);
                   return OSIX_FAILURE;
                   }
                 */
            }
        }
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
    }
    else if (pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount >
             MAX_STA_SUPP_PER_WLC)
    {
#ifdef WSSSTA_AUTH
        pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode =
            WSSSTA_DEAUTH_MS_LEAVING;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
                sizeof (tWssMacMsgStruct));
        pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG,
                                     pWlcHdlrMsgStruct))
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                        "Process of Deauth Msg is success\r\n");
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                        "failed to process the Deauth Msg\r\n");
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_FAILURE;
        }
#endif
    }

    /* Get the Assoc count */
    WssStaShowClient ();
    if ((gu4ClientWalkIndex >= MAX_STA_SUPP_PER_WLC) ||
        ((radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount > 0) &&
         (u4StaCount1 > radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount))
        || ((pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount > 0)
            && (u4StaCount >
                pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount)) ||
        (i1RetValValidateStaDiscovery == OSIX_FAILURE))
    {
        if (i1RetValValidateStaDiscovery == OSIX_FAILURE)
        {
            WSSSTA_TRC2 (WSSSTA_FAILURE_TRC,
                         "STA DISCOVERY MODE FAILED ..... "
                         "Rejecting Station Association\r\n", NULL, NULL);
        }
        else
        {
            WSSSTA_TRC2 (WSSSTA_FAILURE_TRC,
                         "MAX CLIENT COUNT (Radio - %d/Wlan - %d) REACHED....."
                         "Rejecting Station Association\r\n",
                         radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount,
                         pwssWlanDB->WssWlanAttributeDB.u4MaxClientCount);
        }
        pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode =
            WSSSTA_DEAUTH_MS_LEAVING;
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
                sizeof (tWssMacMsgStruct));
        pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG,
                                     pWlcHdlrMsgStruct))
        {
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            /*MAX STATION EXCEEDED TRAP */
            MEMCPY (sStationTrapInfo.au1BaseWtpId, staMacAddr, MAC_ADDR_LEN);
            sStationTrapInfo.u4ClientCount =
                radioIfgetDB.RadioIfGetAllDB.u4MaxClientCount;
            /* Retrivie the profile Index from the local database */
            if (WssIfGetProfileIfIndex (u4BssIfIndex,
                                        &u4ProfileIndex) == OSIX_FAILURE)
            {
            }

            sStationTrapInfo.u4ifIndex = u4ProfileIndex;

            WlanSnmpifSendTrap (WSS_WLAN_MAX_USER_PER_WTP, &sStationTrapInfo);

            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_FAILURE;
        }
    }

    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
    /*
     * Get the validation u4Result and update the u4Result to the status code of
     * association response structure.
     */
    u4Result = ValidateReAssocFrame (&reassocReqMacFrame,
                                     &(pwssMsgRsp->unMacMsg.ReassocRspMacFrame),
                                     &radioIfgetDB, u2Capability);
    if (u4Result == OSIX_SUCCESS)
    {
        u2Aid = pWssStaWepProcessDB->u2AssociationID;
        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.Aid.u2MacFrameAID = u2Aid;
        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.StatCode.u2MacFrameStatusCode =
            (UINT2) u4Result;

        /* Fill the Reassoc resp Capability and Supported Rates with the values
         * received in the Reassoc request */
        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
            Capability.u2MacFrameCapability =
            reassocReqMacFrame.Capability.u2MacFrameCapability;
        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
            Capability.u2MacFrameCapability |= WSSSTA_CAPABILITY_MASK;

        if ((STRLEN (reassocReqMacFrame.SuppRate.au1MacFrameSuppRate)) != 0)
        {
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.SuppRate.u1MacFrameElemId =
                WSSMAC_SUPP_RATE_ELEMENT_ID;
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.SuppRate.u1MacFrameElemLen =
                reassocReqMacFrame.SuppRate.u1MacFrameElemLen;
            if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_BGN)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_B)
                || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BG))
            {
                WssIfGetLegacyRateStatus (&u1LegacyRate);
                if (u1LegacyRate == LEGACY_RATE_ENABLED)
                {
                    u1length = 0;
                    for (u1iter = 0; u1iter <
                         reassocReqMacFrame.SuppRate.u1MacFrameElemLen;
                         u1iter++)
                    {
                        if ((reassocReqMacFrame.SuppRate.
                             au1MacFrameSuppRate[u1iter] == CLI_OPER_RATEB_1MB)
                            || (reassocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_OPER_RATEB_2MB)
                            || (reassocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_OPER_RATEB_5MB)
                            || (reassocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_BSS_RATEB_1MB)
                            || (reassocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_BSS_RATEB_2MB)
                            || (reassocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter] ==
                                CLI_BSS_RATEB_5MB))
                        {
                            continue;
                        }
                        else
                        {
                            pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                                SuppRate.au1MacFrameSuppRate[u1length] =
                                reassocReqMacFrame.SuppRate.
                                au1MacFrameSuppRate[u1iter];
                            u1length++;
                        }
                    }
                    pwssMsgRsp->unMacMsg.AssocRspMacFrame.SuppRate.
                        u1MacFrameElemLen = u1length;
                }
                else
                {
                    MEMCPY (pwssMsgRsp->unMacMsg.AssocRspMacFrame.
                            SuppRate.au1MacFrameSuppRate,
                            reassocReqMacFrame.SuppRate.au1MacFrameSuppRate,
                            STRLEN (reassocReqMacFrame.SuppRate.
                                    au1MacFrameSuppRate));
                }
            }
        }

        if ((STRLEN (reassocReqMacFrame.ExtSuppRates.au1MacFrameExtSuppRates))
            != 0)
        {
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtSuppRates.
                u1MacFrameElemId = WSSMAC_EXT_SUPP_RATE_ELEMENTID;
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtSuppRates.
                u1MacFrameElemLen =
                reassocReqMacFrame.ExtSuppRates.u1MacFrameElemLen;
            MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtSuppRates.
                    au1MacFrameExtSuppRates,
                    reassocReqMacFrame.ExtSuppRates.au1MacFrameExtSuppRates,
                    STRLEN (reassocReqMacFrame.
                            ExtSuppRates.au1MacFrameExtSuppRates));
        }
/* Check whether STA contains WMM tag */
        if (reassocReqMacFrame.WMMParam.u1MacFrameElemId ==
            WSSMAC_VENDOR_INFO_ELEMENTID)
        {

            u2QosCapab = (UINT2) ((u2Capability & 0x0200) >> SHIFT_VALUE_9);
            /* Fill EDCA elements */
/* Check whether AP is QOS configured or not */
            if (u2QosCapab == 1)
            {
                for (u1QosIndex = 1; u1QosIndex <= WSSIF_RADIOIF_QOS_CONFIG_LEN;
                     u1QosIndex++)
                {
                    radioIfgetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
                    radioIfgetDB.RadioIfGetAllDB.u1QosIndex = u1QosIndex;
                    radioIfgetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
                    radioIfgetDB.RadioIfIsGetAllDB.bAdmissionControl =
                        OSIX_TRUE;
                    radioIfgetDB.RadioIfGetAllDB.u1RadioId = u1RadioId;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB,
                                                  &radioIfgetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_DEBUG_TRC, "ValidateAssocFrame: "
                                    "Failed to retrieve data from RadioIfDB\r\n");
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pcurrentAssocWssStaDB);
                        if (u1Flag == 1)
                        {
                            WssFreeAssocId (pWssStaWepProcessDB);
                            WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                        stationMacAddress);
                            RBTreeRem (gWssStaWepProcessDB,
                                       (tRBElem *) pWssStaWepProcessDB);
                        }
                        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                            (UINT1 *) pWssStaWepProcessDB);

                        return OSIX_FAILURE;

                    }

                }
                radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[BEST_EFFORT_ACI]
                    = WSSIF_RADIOIF_DEF_TXOP_BE;
                if (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_B
                    || radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BG
                    || radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_BGN)
                {
                    radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI]
                        = WSSIF_RADIOIF_DEF_TXOP_VI_B;
                }
                else
                {
                    radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI]
                        = WSSIF_RADIOIF_DEF_TXOP_VI;
                }
                radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[VOICE_ACI]
                    = WSSIF_RADIOIF_DEF_TXOP_VO;
                radioIfgetDB.RadioIfGetAllDB.au2TxOpLimit[BACKGROUND_ACI]
                    = WSSIF_RADIOIF_DEF_TXOP_BG;
                radioIfgetDB.RadioIfGetAllDB.
                    au1AdmissionControl[BEST_EFFORT_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_BE;
                radioIfgetDB.RadioIfGetAllDB.au1AdmissionControl[VIDEO_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_VI;
                radioIfgetDB.RadioIfGetAllDB.au1AdmissionControl[VOICE_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_VO;
                radioIfgetDB.RadioIfGetAllDB.
                    au1AdmissionControl[BACKGROUND_ACI] =
                    WSSIF_RADIOIF_DEF_ADMISSION_BG;
                /*STA is WMM Enabled */
                pWssStaWepProcessDB->u1StationWmmEnabled = 1;

                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.
                    u1MacFrameElemId = WSSMAC_VENDOR_INFO_ELEMENTID;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.
                    u1MacFrameElemLen = WSSSTA_WMM_FRAME_LENGTH;
                MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.au1OUI,
                        u1OUI, sizeof (u1OUI));
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.u1OUIType =
                    WSSSTA_WMM_OUI_TYPE;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.u1OUISubType =
                    WSSSTA_WMM_OUI_SUB_TYPE;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.u1WMMVersion =
                    WSSSTA_WMM_VERSION;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.u1QOSInfo =
                    0x00;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.WMMParam.u1Rsvrd =
                    WSSSTA_WMM_RESERVED;

                /*adding best effort edca parameters */
                u1AIFS_BE = u1AIFS_BE | (BEST_EFFORT_ACI << SHIFT_VALUE_5);
                u1AIFS_BE =
                    (UINT1) (u1AIFS_BE |
                             (radioIfgetDB.
                              RadioIfGetAllDB.
                              au1AdmissionControl[BEST_EFFORT_ACI] <<
                              SHIFT_VALUE_4));
                u1AIFS_BE =
                    u1AIFS_BE | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[BEST_EFFORT_ACI];
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_BE;
                u1ECW_BE =
                    (UINT1) (u1ECW_BE |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[BEST_EFFORT_ACI] << SHIFT_VALUE_4));
                u1ECW_BE =
                    (UINT1) (u1ECW_BE | radioIfgetDB.RadioIfGetAllDB.
                             au2CwMin[BEST_EFFORT_ACI]);
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ECWMin_Max = u1ECW_BE;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_BE.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) (radioIfgetDB.RadioIfGetAllDB.
                             au2TxOpLimit[BEST_EFFORT_ACI] << SHIFT_VALUE_8);

                /*adding back ground edca parameters */
                u1AIFS_BG = u1AIFS_BG | (VIDEO_ACI << SHIFT_VALUE_5);
                u1AIFS_BG = (UINT1)
                    (u1AIFS_BG |
                     (radioIfgetDB.
                      RadioIfGetAllDB.au1AdmissionControl[BACKGROUND_ACI] <<
                      SHIFT_VALUE_4));
                u1AIFS_BG =
                    u1AIFS_BG | radioIfgetDB.
                    RadioIfGetAllDB.au1Aifsn[BACKGROUND_ACI];
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    EDCAParam.MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ACI_AFSN =
                    u1AIFS_BG;
                u1ECW_BG =
                    (UINT1) (u1ECW_BG |
                             (radioIfgetDB.
                              RadioIfGetAllDB.au2CwMax[BACKGROUND_ACI] <<
                              SHIFT_VALUE_4));
                u1ECW_BG =
                    (UINT1) (u1ECW_BG | radioIfgetDB.
                             RadioIfGetAllDB.au2CwMin[BACKGROUND_ACI]);
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    EDCAParam.MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ECWMin_Max =
                    u1ECW_BG;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    EDCAParam.MacFrameEDCA_AC_BK.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) (radioIfgetDB.
                             RadioIfGetAllDB.au2TxOpLimit[BACKGROUND_ACI] <<
                             SHIFT_VALUE_8);

                /*adding video edca parameters */

                u1AIFS_VI = u1AIFS_VI | (VOICE_ACI << SHIFT_VALUE_5);
                u1AIFS_VI =
                    (UINT1) (u1AIFS_VI |
                             (radioIfgetDB.
                              RadioIfGetAllDB.au1AdmissionControl[VIDEO_ACI] <<
                              SHIFT_VALUE_4));
                u1AIFS_VI =
                    u1AIFS_VI | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[VIDEO_ACI];
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_VI;
                u1ECW_VI =
                    (UINT1) (u1ECW_VI |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[VIDEO_ACI] << SHIFT_VALUE_4));
                u1ECW_VI =
                    (UINT1) (u1ECW_VI | radioIfgetDB.RadioIfGetAllDB.
                             au2CwMin[VIDEO_ACI]);

                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    EDCAParam.MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ECWMin_Max =
                    u1ECW_VI;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    EDCAParam.MacFrameEDCA_AC_VI.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) (radioIfgetDB.
                             RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI] <<
                             SHIFT_VALUE_8);

                /*adding voice edca parameters */

                u1AIFS_VO = u1AIFS_VO | (BACKGROUND_ACI << SHIFT_VALUE_5);
                u1AIFS_VO = (UINT1)
                    (u1AIFS_VO |
                     (radioIfgetDB.
                      RadioIfGetAllDB.au1AdmissionControl[VIDEO_ACI] <<
                      SHIFT_VALUE_4));
                u1AIFS_VO =
                    u1AIFS_VO | radioIfgetDB.RadioIfGetAllDB.
                    au1Aifsn[VOICE_ACI];
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ACI_AFSN = u1AIFS_VO;
                u1ECW_VO =
                    (UINT1) (u1ECW_VO |
                             (radioIfgetDB.RadioIfGetAllDB.
                              au2CwMax[VOICE_ACI] << SHIFT_VALUE_4));
                u1ECW_VO =
                    (UINT1) (u1ECW_VO | radioIfgetDB.RadioIfGetAllDB.
                             au2CwMin[VOICE_ACI]);
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ECWMin_Max = u1ECW_VO;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.EDCAParam.
                    MacFrameEDCA_AC_VO.u2MacFrameEDCA_AC_TxOpLimit =
                    (UINT2) (radioIfgetDB.RadioIfGetAllDB.
                             au2TxOpLimit[VOICE_ACI] << SHIFT_VALUE_8);

            }
            else
            {
                /*STA-DB set -  WMM DISABLED AP */
                pWssStaWepProcessDB->u1StationWmmEnabled = 0;
            }

        }
        else
        {
            /*STA-DB set -  WMM DISABLED STA */
            pWssStaWepProcessDB->u1StationWmmEnabled = 0;
        }
        if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RADIO_TYPE_BGN) ||
            (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RADIO_TYPE_AN) ||
            (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_NG)
            || (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC))
        {
            if (reassocReqMacFrame.HTCapabilities.u1MacFrameElemId
                == WSSMAC_HT_CAPAB_ELEMENT_ID)
            {

                /* Reading the HT Cap value from the RadioDB */
                WssStaGetDo111nValues (&radioIfgetDB);

                if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_EXTCAP_INFO,
                                              &radioIfgetDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                                "Failed to retrieve data from RadioIfDB\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                pcurrentAssocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                    WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                                "Process of Deauth Msg is success\r\n");

                    if (u1Flag == 1)
                    {
                        WssFreeAssocId (pWssStaWepProcessDB);
                        WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                    stationMacAddress);
                        RBTreeRem (gWssStaWepProcessDB,
                                   (tRBElem *) pWssStaWepProcessDB);
                    }
                    MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                        (UINT1 *) pWssStaWepProcessDB);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessRadioIfDBMsg (WSS_ASSEMBLE_11N_BEAMFORMING_INFO,
                                              &radioIfgetDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                                "Failed to retrieve data from RadioIfDB\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                pcurrentAssocWssStaDB);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                    WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                                "Process of Deauth Msg is success\r\n");
                    if (u1Flag == 1)
                    {
                        WssFreeAssocId (pWssStaWepProcessDB);
                        WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                    stationMacAddress);
                        RBTreeRem (gWssStaWepProcessDB,
                                   (tRBElem *) pWssStaWepProcessDB);
                    }
                    MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                        (UINT1 *) pWssStaWepProcessDB);
                    return OSIX_FAILURE;
                }

                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTCapabilities.u1MacFrameElemId =
                    WSSMAC_HT_CAPAB_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTCapabilities.u1MacFrameElemLen =
                    WSSMAC_HT_CAPAB_ELEMENT_LEN;

                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTCapabilities.u2HTCapInfo =
                    ((reassocReqMacFrame.HTCapabilities.u2HTCapInfo) &
                     OSIX_HTONS (radioIfgetDB.RadioIfGetAllDB.
                                 Dot11NCapaParams.u2HtCapInfo));

                /*For 11AC Admission control should be disabled */
                MEMSET (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                        HTCapabilities.au1SuppMCSSet, VALUE_0,
                        WSSMAC_SUPP_MCS_SET);
                if ((reassocReqMacFrame.HTCapabilities.au1SuppMCSSet[0] !=
                     VALUE_0)
                    && (radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[0] != VALUE_0))
                {
                    u1McsFlag++;
                }
                if ((reassocReqMacFrame.HTCapabilities.au1SuppMCSSet[1] !=
                     VALUE_0)
                    && (radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[1] != VALUE_0))
                {
                    u1McsFlag++;
                }
                if ((reassocReqMacFrame.HTCapabilities.au1SuppMCSSet[2] !=
                     VALUE_0)
                    && (radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[2] != VALUE_0))
                {
                    u1McsFlag++;
                }
                if (u1McsFlag)
                {
                    for (u1McsIndex = VALUE_0; u1McsIndex < u1McsFlag;
                         u1McsIndex++)
                    {
                        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.HTCapabilities.
                            au1SuppMCSSet[u1McsIndex] =
                            radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                            au1HtCapaMcs[u1McsIndex];
                    }
                }
                MEMCPY (&pwssMsgRsp->unMacMsg.ReassocRspMacFrame.HTCapabilities.
                        au1SuppMCSSet[3],
                        &radioIfgetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
                        au1HtCapaMcs[3], (DOT11N_HT_CAP_MCS_LEN - VALUE_3));

                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTCapabilities.u1AMPDUParam =
                    ((reassocReqMacFrame.HTCapabilities.u1AMPDUParam) &
                     (radioIfgetDB.RadioIfGetAllDB.Dot11NampduParams.
                      u1AmpduParam));
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTCapabilities.u2HTExtCap =
                    ((reassocReqMacFrame.HTCapabilities.u2HTExtCap) &
                     (OSIX_HTONS (radioIfgetDB.RadioIfGetAllDB.
                                  Dot11NhtExtCapParams.u2HtExtCap)));
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTCapabilities.u4TranBeamformCap =
                    ((reassocReqMacFrame.HTCapabilities.u4TranBeamformCap) &
                     (OSIX_HTONL (radioIfgetDB.RadioIfGetAllDB.
                                  Dot11NhtTxBeamCapParams.u4TxBeamCapParam)));
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.HTCapabilities.
                    u1ASELCap = reassocReqMacFrame.HTCapabilities.u1ASELCap;
                /* HT Operation for 802.11n */
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTOperation.u1MacFrameElemId = WSSMAC_HT_OPE_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                    HTOperation.u1MacFrameElemLen = WSSMAC_HT_OPE_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.HTOperation.
                    u1PrimaryCh = radioIfgetDB.RadioIfGetAllDB.u1CurrentChannel;
                MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                        HTOperation.au1HTOpeInfo,
                        radioIfgetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1HTOpeInfo, WSSMAC_HTOPE_INFO);

                MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                        HTOperation.au1BasicMCSSet,
                        radioIfgetDB.RadioIfGetAllDB.Dot11NhtOperation.
                        au1BasicMCSSet, WSSMAC_BASIC_MCS_SET);
            }
        }
        if (radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
        {
            radioIfgetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
            if ((WssIfProcessRadioIfDBMsg
                 (WSS_GET_RADIO_IF_DB, &radioIfgetDB)) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ReAssocFrame: "
                            "Failed to retrieve 11AC data from RadioIfDB\r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                if (u1Flag == 1)
                {
                    WssFreeAssocId (pWssStaWepProcessDB);
                    WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                stationMacAddress);
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                }
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);

                return OSIX_FAILURE;
            }
            radioIfgetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_FALSE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_FALSE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_FALSE;

            radioIfgetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtOperModeElem = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
            if ((WssIfProcessRadioIfDBMsg
                 (WSS_GET_RADIO_IF_DB_11AC, &radioIfgetDB)) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ReAssocFrame: "
                            "Failed to retrieve 11AC data from RadioIfDB\r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                if (u1Flag == 1)
                {
                    WssFreeAssocId (pWssStaWepProcessDB);
                    WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                stationMacAddress);
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                }
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);

                return OSIX_FAILURE;
            }

            if (reassocReqMacFrame.VHTCapabilities.u1ElemId ==
                WSSMAC_VHT_CAPAB_ELEMENT_ID)
            {
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTCapabilities.
                    u1ElemId = WSSMAC_VHT_CAPAB_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTCapabilities.
                    u1ElemLen = WSSMAC_VHT_CAPAB_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTCapabilities.
                    u4VhtCapaInfo = ((reassocReqMacFrame.VHTCapabilities.
                                      u4VhtCapaInfo) &
                                     (OSIX_HTONL
                                      (radioIfgetDB.RadioIfGetAllDB.
                                       Dot11AcCapaParams.u4VhtCapInfo)));

                for (u1Loop = 0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
                {
                    if (radioIfgetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs[u1Loop] == RADIO_11AC_VHT_MCS_DISABLE)
                    {
                        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTCapabilities.
                            au1SuppMCS[u1Loop] = radioIfgetDB.RadioIfGetAllDB.
                            Dot11AcCapaParams.au1VhtCapaMcs[u1Loop];
                    }
                    else
                    {
                        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTCapabilities.
                            au1SuppMCS[u1Loop] =
                            ((reassocReqMacFrame.VHTCapabilities.
                              au1SuppMCS[u1Loop]) | (radioIfgetDB.
                                                     RadioIfGetAllDB.
                                                     Dot11AcCapaParams.
                                                     au1VhtCapaMcs[u1Loop]));
                    }
                }
                MEMCPY (radioIfgetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs, pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                        VHTCapabilities.au1SuppMCS, sizeof (radioIfgetDB.
                                                            RadioIfGetAllDB.
                                                            Dot11AcCapaParams.
                                                            au1VhtCapaMcs));
                WssIfVhtHighestSuppDataRate (&radioIfgetDB);
                MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                        VHTCapabilities.au1SuppMCS,
                        radioIfgetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                        au1VhtCapaMcs, sizeof (radioIfgetDB.RadioIfGetAllDB.
                                               Dot11AcCapaParams.
                                               au1VhtCapaMcs));
                radioIfgetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;

            }
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTOperation.u1ElemId =
                WSSMAC_VHT_OPE_ELEMENT_ID;
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTOperation.u1ElemLen =
                WSSMAC_VHT_OPE_ELEMET_LEN;
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTOperation.u2BasicMCS =
                (OSIX_HTONS (radioIfgetDB.RadioIfGetAllDB.Dot11AcOperParams.
                             u2VhtOperMcsSet));
            MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.VHTOperation.
                    au1VhtOperInfo, radioIfgetDB.RadioIfGetAllDB.
                    Dot11AcOperParams.au1VhtOperInfo,
                    sizeof (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.
                            VHTOperation.au1VhtOperInfo));
            if ((reassocReqMacFrame.OperModeNotify.u1ElemId ==
                 WSSMAC_VHT_OPERATING_MODE_NOTIFICATION) &&
                (radioIfgetDB.RadioIfGetAllDB.Dot11AcOperParams.
                 u1VhtOperModeNotify == OSIX_TRUE))
            {
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.OperModeNotify.
                    u1ElemId = WSSMAC_VHT_OPERATING_MODE_NOTIFICATION;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.OperModeNotify.
                    u1ElemLen = WSSMAC_OPER_NOTIFY_ELEM_LEN;
                WssIfAssembleOperModeNotify (&radioIfgetDB);
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.OperModeNotify.
                    u1VhtOperModeElem = ((reassocReqMacFrame.OperModeNotify.
                                          u1VhtOperModeElem) & (radioIfgetDB.
                                                                RadioIfGetAllDB.
                                                                Dot11AcOperParams.
                                                                u1VhtOperModeElem));
            }
        }

        if (reassocReqMacFrame.ExtCapabilities.u1MacFrameElemId ==
            WSSMAC_EXT_CAPAB_ELEMENT_ID)
        {
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtCapabilities.
                u1MacFrameElemId = WSSMAC_EXT_CAPAB_ELEMENT_ID;
            pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtCapabilities.
                u1MacFrameElemLen =
                reassocReqMacFrame.ExtCapabilities.u1MacFrameElemLen;
            MEMCPY (pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtCapabilities.
                    au1Capabilities,
                    reassocReqMacFrame.ExtCapabilities.au1Capabilities,
                    sizeof (reassocReqMacFrame.ExtCapabilities.
                            au1Capabilities));
            if ((radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC)
                && (radioIfgetDB.RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeNotify == OSIX_TRUE))
            {
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.ExtCapabilities.
                    au1Capabilities[RADIO_VALUE_7] = (pwssMsgRsp->unMacMsg.
                                                      ReassocRspMacFrame.
                                                      ExtCapabilities.
                                                      au1Capabilities
                                                      [RADIO_VALUE_7] |
                                                      RADIO_VALUE_4);
            }

        }

#ifdef RSNA_WANTED
        /* MLME reassoc indication */

        /*REASSOCIATION is handled in same manner as ASSOCIATION */

        if (reassocReqMacFrame.RSNInfo.u1MacFrameElemId ==
            WSSMAC_RSN_ELEMENT_ID)
        {
#ifdef PMF_WANTED
            if (RsnaApiIsStaPMFCapable (staMacAddr, u4BssIfIndex) !=
                OSIX_SUCCESS)
#endif
            {
                RsnaHandleExistingStationAssocRequest (staMacAddr,
                                                       u4PresentBssIfIndex,
                                                       &bSessionDelete);
                if (bSessionDelete == OSIX_TRUE)
                {
#ifdef WSSUSER_WANTED
                    MEMCPY (WssUserNotifyParams.staMac, staMacAddr,
                            MAC_ADDR_LEN);
                    WssUserNotifyParams.eWssUserNotifyType =
                        WSSUSER_SESSION_DELETE;
                    WssUserNotifyParams.u4TerminateCause = CALLBACK;
                    if (WssUserProcessNotification (&WssUserNotifyParams)
                        == OSIX_FAILURE)
                    {
                        WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                                    "WssStaProcessDeAuthFrames "
                                    "WssUserProcessNotification returned failure\r\n");
                        if (u1Flag == 1)
                        {
                            WssFreeAssocId (pWssStaWepProcessDB);
                            WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                        stationMacAddress);
                            RBTreeRem (gWssStaWepProcessDB,
                                       (tRBElem *) pWssStaWepProcessDB);
                        }
                        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                            (UINT1 *) pWssStaWepProcessDB);
                        /* Kloc Fix Ends */
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pcurrentAssocWssStaDB);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);

                        return OSIX_FAILURE;
                    }
#endif

                }
            }
#ifdef PMF_WANTED
            else
            {
                bPMFSessionExist = OSIX_TRUE;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.TimeoutInterval.
                    u1MacFrameElemId = WSSMAC_TIMEOUT_INTERVAL_ELEMENT_ID;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.TimeoutInterval.
                    u1MacFrameElemLen = WSSMAC_TIMEOUT_INTERVAL_ELEMENT_LEN;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.TimeoutInterval.
                    u1TimeoutIntervalType = WSS_MAC_ASSOC_COMEBACK_TIME;
                pwssMsgRsp->unMacMsg.ReassocRspMacFrame.TimeoutInterval.
                    u4TimeoutIntervalValue = 2000;

            }
#endif
            if (pWssStaWepProcessDB != NULL)
            {

                pWssStaWepProcessDB->u4RsnIeLength = (UINT4)
                    (reassocReqMacFrame.RSNInfo.u1MacFrameElemLen + 2);
                /* 2 denotes RSN_VERSION_OFF used in Parsing rsn ie at rsnaproc.c */
                MEMCPY (pWssStaWepProcessDB->au1RsnIE,
                        &reassocReqMacFrame.RSNInfo,
                        sizeof (reassocReqMacFrame.RSNInfo));
                pWssStaWepProcessDB->u1AssocMode = WSSSTA_WPA2_MODE;
            }
        }
#endif
#ifdef WPA_WANTED
        if (reassocReqMacFrame.WpaInfo.elemId == WSSMAC_VENDOR_INFO_ELEMENTID)
        {
            if (pWssStaWepProcessDB != NULL)
            {
                if (MEMCMP (reassocReqMacFrame.WpaInfo.Oui, au1WpaOUI, 4) == 0)
                {
                    RsnaHandleExistingStationAssocRequest (staMacAddr,
                                                           u4BssIfIndex,
                                                           &bSessionDelete);
                    MEMCPY (pWssStaWepProcessDB->au1WpaIE,
                            &reassocReqMacFrame.WpaInfo,
                            reassocReqMacFrame.WpaInfo.Len + 2);
                    pWssStaWepProcessDB->u1AssocMode = WSSSTA_WPA1_MODE;
                }
                pWssStaWepProcessDB->u4WpaIeLength = (UINT4)
                    (reassocReqMacFrame.WpaInfo.Len + 2);
                pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
            }
        }
#endif

#ifdef PMF_WANTED
        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_REASSOC_RSP,
                                    pWlcHdlrMsgStruct) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssStaProcessReAssocFrames: "
                        "WSS_WLCHDLR_MAC_REASSOC_RSP returned FAILURE\r\n");
            /* Kloc Fix Start */
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            /* Kloc Fix Ends */
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            return OSIX_FAILURE;
        }

        /*PMF_WANTED */
        if (bPMFSessionExist == OSIX_TRUE)
        {
            if ((RsnaApiSendSAQueryReq (staMacAddr, u4BssIfIndex, u2SessId)) !=
                OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                            "RSNA SA Query Send returned FAILURE\r\n");
                if (u1Flag == 1)
                {
                    WssFreeAssocId (pWssStaWepProcessDB);
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                }
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) ploadWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                return OSIX_FAILURE;
            }
        }
#endif
        pWssStaWepProcessDB->u2AssociationID = u2Aid;
        pWssStaWepProcessDB->WssstaParams.u2CapabilityInfo = u2Capability;
        pWssStaWepProcessDB->u4BssIfIndex = u4BssIfIndex;
        pWssStaWepProcessDB->u2Sessid = u2SessId;

        pwssStaDB =
            (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
        if (pwssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                        "WssStaProcessReAssocFrames:- "
                        "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_FAILURE;
        }

        MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
        preassocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount--;
        if (WssIfProcessWssAuthDBMsg (WSS_AUTH_SET_WTP_ASSOC_COUNT_DB,
                                      preassocWssStaDB) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaProcessReAssocFrames:- "
                        "WSS_AUTH_SET_WTP_ASSOC_COUNT_DB returned failure\n");
        }

        wssStaParams.u2CapabilityInfo = u2Capability;

        pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
        if (pwlcHdlrMsg == NULL)
        {
            WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                        "WssStaProcessReAssocFrames:- "
                        "UtlShMemAllocWlcBuf returned failure\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_FAILURE;
        }

        MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId =
            reassocReqMacFrame.u2SessId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageType =
            ADD_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageLength =
            WSSSTA_ADDSTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1MacAddrLen =
            sizeof (tMacAddr);
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.StaMacAddress,
                staMacAddr, sizeof (tMacAddr));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2VlanId = u2VlanId;

        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2MessageType =
            IEEE_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2MessageLength =
            WSSSTA_IEEESTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2AssocId = u2Aid;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1Flags = TRUE;
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                StaMsg.stationMacAddress, staMacAddr, sizeof (tMacAddr));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2Capab = u2Capability;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1WlanId = u1WlanID;
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.au1SuppRates,
                radioIfgetDB.RadioIfGetAllDB.au1SupportedRate,
                STRLEN (radioIfgetDB.RadioIfGetAllDB.au1SupportedRate));
        /*    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex; */
        /* radioIfgetDB.RadioIfGetAllDB.u4BssIfIndex = u4PresentBssIfIndex; */
        if (u1AuthAlgo == WSS_STA_AUTH_ALGO_SHARED)
        {
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.isPresent =
                TRUE;
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2MessageType =
                IEEE_STATION_SESSION_KEY;
            pwlcHdlrMsg->StationConfReq.wssStaConfig.
                StaSessKey.u2MessageLength = (UINT2) (20 + STRLEN (au1WepKey));
            MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.stationMacAddress, staMacAddr,
                    sizeof (tMacAddr));
#ifdef PMF_WANTED
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2Flags |=
                (bPMFSessionExist << RSNA_C_FLAG_MASK);
#else
            pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2Flags = 0;
#endif
            MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.
                    au1PairwiseTSC, 0, 6);
            MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.
                    au1PairwiseRSC, 0, 6);
            MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.au1Key,
                    au1WepKey, STRLEN (au1WepKey));
        }

        /*STA Configuration Request WMM STATION Vendor MSG */
        pwlcHdlrMsg->StationConfReq.vendSpec.isOptional = TRUE;
        pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pwlcHdlrMsg->StationConfReq.vendSpec.elementId = VENDOR_WMM_TYPE;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            isOptional = TRUE;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2MsgEleType = VENDOR_WMM_MSG;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2MsgEleLen = 17;
        pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleLen =
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2MsgEleLen + 4;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u4VendorId = VENDOR_ID;
        MEMCPY (pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WMMStaVendor.StaMacAddr, staMacAddr, sizeof (tMacAddr));
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u1WmmStaFlag = pWssStaWepProcessDB->u1StationWmmEnabled;
        pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
            u2TcpPort = (UINT2) HttpGetSourcePort ();
        /* Check whether user role is enabled and it is a dot1x/web authentication
         * with radius authentication. If so, dont create TC filters with bandwidth
         * threshold. The TC filters will be created with the USER ROLE 
         * configured bandwidth */
        nmhGetFsDot11WlanLoginAuthentication ((INT4) u4WlanIndex, &i4LoginAuth);
#ifdef RSNA_WANTED
        RsnaGetDot11RSNAEnabled ((INT4) u4WlanIndex, &i4Dot11RSNAEnabled);

        if (i4Dot11RSNAEnabled == RSNA_ENABLED)
        {
            RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled ((INT4)
                                                              u4WlanIndex,
                                                              RSNA_AUTHSUITE_8021X,
                                                              &i4AuthSuiteStatus);
        }
#endif
        /* Need to check if Radius User Role DB is NULL */
#ifdef WSSUSER_WANTED
        bUserRoleStatus = WssUserRoleModuleStatus ();
#else
        bUserRoleStatus = OSIX_SUCCESS;
#endif
        if ((bUserRoleStatus == OSIX_SUCCESS) &&
            (((i4LoginAuth == REMOTE_LOGIN_RADIUS) &&
              (u1WebAuthStatus == WSSWLAN_ENABLE))
#ifdef RSNA_WANTED
             || (i4AuthSuiteStatus == RSNA_ENABLED))
#else
             && (i4AuthSuiteStatus == RSNA_DISABLED))
#endif
            )
        {
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
                u4BandwidthThresh = 0;
        }
        else
        {
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WMMStaVendor.
                u4BandwidthThresh =
                pwssWlanDB->WssWlanAttributeDB.u4BandwidthThresh;
        }
        CAPWAP_TRC7 (CAPWAP_STATION_TRC,
                     "REASSOC Configuring Bandwidth Threshold %d for station:%02x:%02x:%02x:%02x:%02x:%02x\n",
                     pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                     WMMStaVendor.u4BandwidthThresh, staMacAddr[0],
                     staMacAddr[1], staMacAddr[2], staMacAddr[3], staMacAddr[4],
                     staMacAddr[5]);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4WlcHdlrSysLogId,
                      "REASSOC Configuring Bandwidth Threshold %d for station:%02x:%02x:%02x:%02x:%02x:%02x\n",
                      pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                      WMMStaVendor.u4BandwidthThresh, staMacAddr[0],
                      staMacAddr[1], staMacAddr[2], staMacAddr[3],
                      staMacAddr[4], staMacAddr[5]));

        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg)
            == OSIX_SUCCESS)
        {
            WssStaConstructDBMsg (WSS_STA_ASSOCIATED, u4PresentBssIfIndex,
                                  staMacAddr, u2Aid,
                                  u2SessId, &wssStaParams,
                                  &(pwssStaDB->WssStaDB));
            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB) ==
                OSIX_SUCCESS)
            {
                pcurrentAssocWssStaDB->WssStaWtpAssocCountInfo.u4AssoCount++;
                if (WssIfProcessWssAuthDBMsg (WSS_AUTH_SET_WTP_ASSOC_COUNT_DB,
                                              pcurrentAssocWssStaDB) !=
                    OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaProcessReAssocFrames:- "
                                "UtlShMemAllocWlcBuf returned failure\n");
                }
            }
        }
        else
        {
            /* Kloc Fix Start */
            pwssDisassocMacFrame =
                (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
            if (pwssDisassocMacFrame == NULL)
            {
                WSSSTA_TRC (WSSSTA_DEBUG_TRC,
                            "WssStaProcessReAssocFrames:-"
                            "UtlShMemAllocMacMsgStructBuf returned failure\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                if (u1Flag == 1)
                {
                    WssFreeAssocId (pWssStaWepProcessDB);
                    WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                stationMacAddress);
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                }
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);
                return OSIX_FAILURE;
            }
            MEMSET (pwssDisassocMacFrame, 0, sizeof (tWssMacMsgStruct));
            /* Kloc Fix Ends */
            MEMSET (&(pwssDisassocMacFrame->unMacMsg.DisassocMacFrame), 0,
                    sizeof (tDot11DisassocMacFrame));
            MEMCPY (pwssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                    MacMgmtFrmHdr.u1DA, reassocReqMacFrame.MacMgmtFrmHdr.u1SA,
                    sizeof (tMacAddr));
            MEMCPY (pwssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                    MacMgmtFrmHdr.u1SA, reassocReqMacFrame.MacMgmtFrmHdr.u1DA,
                    sizeof (tMacAddr));
            MEMCPY (pwssDisassocMacFrame->unMacMsg.
                    DisassocMacFrame.MacMgmtFrmHdr.u1BssId,
                    reassocReqMacFrame.MacMgmtFrmHdr.u1BssId,
                    sizeof (tMacAddr));
            pwssDisassocMacFrame->unMacMsg.DisassocMacFrame.u2SessId =
                reassocReqMacFrame.u2SessId;
            pwssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                ReasonCode.u2MacFrameReasonCode = WSSSTA_DISASS_TOO_MANY_MS;
            MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDisassocMacFrame,
                    sizeof (tWssMacMsgStruct));
            pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x07;
            if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DISASSOC_MSG,
                                         pWlcHdlrMsgStruct))
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDisassocMacFrame);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                            "Process of DISASSOC Msg is success\r\n");
                if (u1Flag == 1)
                {
                    WssFreeAssocId (pWssStaWepProcessDB);
                    WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                stationMacAddress);
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                }
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);
                return OSIX_SUCCESS;
            }
            else
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDisassocMacFrame);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
                WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                            "Failed to process the DISASSOC Msg\r\n");
                if (u1Flag == 1)
                {
                    WssFreeAssocId (pWssStaWepProcessDB);
                    WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                                stationMacAddress);
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                }
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);
                return OSIX_FAILURE;
            }
        }
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    }
    else
    {
        pwssMsgRsp->unMacMsg.ReassocRspMacFrame.StatCode.u2MacFrameStatusCode =
            (UINT2) u4Result;

        MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssMsgRsp,
                sizeof (tWssMacMsgStruct));
        if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_REASSOC_RSP,
                                     pWlcHdlrMsgStruct))
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                        "Process of REASSOC Rsp Msg is success\r\n");
            return OSIX_SUCCESS;
        }
        else
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
            WSSSTA_TRC (WSSSTA_DEBUG_TRC, "WssIfProcessWlcHdlrMsg:"
                        "Failed to process the REASSOC Rsp Msg\r\n");
            if (u1Flag == 1)
            {
                WssFreeAssocId (pWssStaWepProcessDB);
                WssStaUtilStaDbDeleteEntry (pWssStaWepProcessDB->
                                            stationMacAddress);
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
            }
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                (UINT1 *) pWssStaWepProcessDB);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssMsgRsp);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pcurrentAssocWssStaDB);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) preassocWssStaDB);
    UNUSED_PARAM (u2WtpInternalId);
    UNUSED_PARAM (u4WlanIndex);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssStaConstructDBMsg                                  *
 *                                                                           *
 *  Description     : This function is used to construct DB msg to update    *
 *                    the station DB.                                        *
 *  Input(s)        : ReAssoc Mac Frame                                      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
VOID
WssStaConstructDBMsg (eWssStaUpdateAction action,
                      UINT4 u4BssIfIndex,
                      tMacAddr staMacAddr,
                      UINT2 u2Aid,
                      UINT2 u2SessId,
                      tWssStaParams * pWssStaParams, tWssStaDB * pWssStaDB)
{

    UNUSED_PARAM (pWssStaParams);

    pWssStaDB->action = action;
    pWssStaDB->u4BssIfIndex = u4BssIfIndex;
    MEMCPY (pWssStaDB->staMacAddr, staMacAddr, sizeof (tMacAddr));

    if (action == WSS_STA_ASSOCIATED)
    {
        /* Fill the pWssStaParams element and send for update */

        pWssStaDB->u2Aid = u2Aid;
        pWssStaDB->u2SessId = u2SessId;
        pWssStaDB->WssStaParams.u2CapabilityInfo =
            pWssStaParams->u2CapabilityInfo;
    }
}

INT4
WssStaConfigResp (tWssStaConfigReqInfo * pWssStaConfigReqInfo)
{
    tWssWlanDB          wssWlanDB;
    tMacAddr            staMacAddr;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    tWssStaParams       wssStaParams;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4ProfileIndex = 0;
    UINT2               u2Aid = 0;
    UINT2               u2Sessid = 0;
    tWlanSTATrapInfo    sStationTrapInfo;
#ifdef RSNA_WANTED
    UINT4               u4WlanIfIndex = 0;
    tWssRSNANotifyParams WssRSNANotifyParams;
    INT4                i4RetValDot11RSNAEnabled = RSNA_DISABLED;
#endif
#ifdef NPAPI_WANTED
    tWlanClientParams   WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpWrAdd  WlanClientNpWrAdd;
    tWlanClientNpWrDelete WlanClientNpWrDelete;
    tWlanParams         WlanParams;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&WlanParams, 0, sizeof (tWlanParams));
    MEMSET (&WlanClientNpWrAdd, 0, sizeof (tWlanClientNpWrAdd));
    MEMSET (&WlanClientNpWrDelete, 0, sizeof (tWlanClientNpWrDelete));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
#ifdef RSNA_WANTED
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
#endif
    MEMSET (&sStationTrapInfo, 0, sizeof (tWlanSTATrapInfo));
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaConfigResp:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    MEMSET (&wssStaParams, 0, sizeof (tWssStaParams));
    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    /* For Association */
    if (pWssStaConfigReqInfo->AddSta.isPresent == TRUE)
    {

        MEMCPY (staMacAddr, pWssStaConfigReqInfo->AddSta.StaMacAddress,
                sizeof (tMacAddr));
        pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
        if (pWssStaWepProcessDB != NULL)
        {
            MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
                    pWssStaWepProcessDB->BssIdMacAddr, MAC_ADDR_LEN);
            wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                          &wssWlanDB) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:"
                            "WssAuth DB Failed to get Auth Status for"
                            "Station Mac Address\r\n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                return OSIX_FAILURE;
            }

            wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB))
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAuthFrames: "
                            "Failed to retrieve privacy data from WssWlanDB\r\n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                return OSIX_FAILURE;
            }
            if (wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus == WSSWLAN_ENABLE)
            {
                if (pWssStaWepProcessDB->bRoamStatus == TRUE)
                {
                    MEMCPY (pwssStaDB->WssIfAuthStateDB.
                            stationMacAddress, staMacAddr, sizeof (tMacAddr));
                    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                                  pwssStaDB) != OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WlcHdlrVerifyStaPkts:"
                                    "WssAuth DB Failed to get Auth Status for"
                                    "Station Mac Address\r\n");
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                        return OSIX_FAILURE;
                    }

                    WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                                         "Send Web auth status to STA %02x:%02x:%02x:%02x:02x:%02x ",
                                         staMacAddr[0], staMacAddr[1],
                                         staMacAddr[2], staMacAddr[3],
                                         staMacAddr[4], staMacAddr[5]);
                    WSSSTA_WEBAUTH_TRC2 (WSSSTAWEBAUTH_MASK, "%d on AP %d\n",
                                         pwssStaDB->WssIfAuthStateDB.
                                         u1StaAuthFinished,
                                         pWssStaWepProcessDB->u2Sessid);

                    if (CapwapSendWebAuthStatus (staMacAddr,
                                                 pwssStaDB->WssIfAuthStateDB.
                                                 u1StaAuthFinished,
                                                 pWssStaWepProcessDB->
                                                 u2Sessid != OSIX_SUCCESS))
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessDeAuthFrames "
                                    " WEB-AUTH Status processing failed\r\n");
                    }
                }
            }

            u2Aid = pWssStaWepProcessDB->u2AssociationID;
            u4BssIfIndex = pWssStaWepProcessDB->u4BssIfIndex;
            u2Sessid = pWssStaWepProcessDB->u2Sessid;
            wssStaParams.u2CapabilityInfo =
                pWssStaWepProcessDB->WssstaParams.u2CapabilityInfo;
            WssStaConstructDBMsg (WSS_STA_ASSOCIATED, u4BssIfIndex, staMacAddr,
                                  u2Aid, u2Sessid, &wssStaParams,
                                  &(pwssStaDB->WssStaDB));
            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB) ==
                OSIX_SUCCESS)
            {
                if (WssGetIsRsnaAuthentication (u4BssIfIndex) == OSIX_FAILURE)
                {
                    /*STATION CONNECTION TRAP */
                    sStationTrapInfo.u4AssocStatus = WSS_WLAN_STA_CONNECTED;
                    MEMCPY (sStationTrapInfo.au1CliMac, staMacAddr,
                            MAC_ADDR_LEN);

                    /* Retrivie the profile Index from the local database */
                    if (WssIfGetProfileIfIndex (u4BssIfIndex,
                                                &u4ProfileIndex) ==
                        OSIX_FAILURE)
                    {
                    }

                    sStationTrapInfo.u4ifIndex = u4ProfileIndex;

                    WlanSnmpifSendTrap (WSS_WLAN_CONNECTION_STATUS,
                                        &sStationTrapInfo);
                }
#ifdef NPAPI_WANTED
                /* Call NPAPI */
                NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                     NP_WLAN_CLIENT_MODULE,    /* Module ID */
                                     WLAN_CLIENT_ADD,    /* Function/OpCode */
                                     0,    /* IfIndex value if applicable */
                                     0,    /* No. of Port Params */
                                     0);

                MEMCPY (WlanParams.bssIdMacAddr,
                        pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
                WlanClientParams.pWlanNpParams = &WlanParams;

                MEMCPY (WlanClientParams.clientMacAddr, staMacAddr,
                        sizeof (tMacAddr));
                MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
                wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
                wssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              &wssWlanDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssWlanProcessWssWlanDBMsg: \r\n");
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }

                WlanClientParams.clientVlanId =
                    wssWlanDB.WssWlanAttributeDB.u2VlanId;

                if (FNP_FAILURE == capwapNpWlanClientAdd (&WlanClientParams))
                {
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }

                UINT4               retVal = FNP_FAILURE;

                MEMSET (&WlanParams, 0, sizeof (tWlanParams));
                WlanClientParams.pWlanNpParams = &WlanParams;

                MEMCPY (WlanClientParams.clientMacAddr, staMacAddr,
                        sizeof (tMacAddr));
                WlanClientParams.clientVlanId =
                    wssWlanDB.WssWlanAttributeDB.u2VlanId;

                retVal = capwapNpWlanClientGet (&WlanClientParams);
                if (retVal == FNP_FAILURE)
                {
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }

#endif

#ifdef RSNA_WANTED
                /* Verify if the received WLAN IfIndex  - Has RSNA Enabled.
                   Only then Indicate to RSNA Module */
                if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4WlanIfIndex) ==
                    OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConfigResp:- "
                                "WssIfGetProfileIfIndex() returned failure\n");
                }

                if (RsnaGetDot11RSNAEnabled ((INT4) u4WlanIfIndex,
                                             &i4RetValDot11RSNAEnabled) !=
                    SNMP_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConfigResp:- "
                                "RsnaGetDot11RSNAEnabled() returned failure\n");
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }

#ifdef PMF_DEBUG_WANTED
                if (pWssStaConfigReqInfo->VendSpec.PMFStaVendor.isOptional ==
                    TRUE)
                {
                    WssRSNANotifyParams.PMFVendorInfo.RsnaIGTKSeqInfo[0].
                        u1KeyIndex =
                        pWssStaConfigReqInfo->VendSpec.PMFStaVendor.
                        RsnaIGTKSeqInfo[0].u1KeyIndex;
                    MEMCPY (WssRSNANotifyParams.PMFVendorInfo.
                            RsnaIGTKSeqInfo[0].au1IGTKPktNo,
                            pWssStaConfigReqInfo->VendSpec.PMFStaVendor.
                            RsnaIGTKSeqInfo[0].au1IGTKPktNo, 6);

                    WssRSNANotifyParams.PMFVendorInfo.RsnaIGTKSeqInfo[1].
                        u1KeyIndex =
                        pWssStaConfigReqInfo->VendSpec.PMFStaVendor.
                        RsnaIGTKSeqInfo[1].u1KeyIndex;
                    MEMCPY (WssRSNANotifyParams.PMFVendorInfo.
                            RsnaIGTKSeqInfo[1].au1IGTKPktNo,
                            pWssStaConfigReqInfo->VendSpec.PMFStaVendor.
                            RsnaIGTKSeqInfo[1].au1IGTKPktNo, 6);
                }
#endif
                if (((i4RetValDot11RSNAEnabled == RSNA_ENABLED) &&
                     (RsnaGetIsPtkInDoneState (staMacAddr) != OSIX_SUCCESS)) &&
                    (pWssStaWepProcessDB->u1AssocMode == WSSSTA_WPA2_MODE) &&
                    (pWssStaWepProcessDB->bDeAuthDueToWps == OSIX_FALSE))
                {
                    /* Send the notification to RSNA, only when the PTK is not in done state
                     * If the four way handshake is completed, this notifciation should not be sent 
                     * again. It should be sent only once, when open authentication is completed*/
                    /* make a MLME indication */

                    MEMSET (WssRSNANotifyParams.MLMEASSOCIND.au1StaMac, 0, 6);
                    MEMCPY (WssRSNANotifyParams.MLMEASSOCIND.au1StaMac,
                            staMacAddr, 6);
                    MEMSET (WssRSNANotifyParams.MLMEASSOCIND.au1RsnIe, 0,
                            RSNA_IE_LEN);
                    MEMCPY (WssRSNANotifyParams.MLMEASSOCIND.au1RsnIe,
                            pWssStaWepProcessDB->au1RsnIE, RSNA_IE_LEN);
                    WssRSNANotifyParams.MLMEASSOCIND.u1RsnaIELen = (UINT1)
                        (pWssStaWepProcessDB->u4RsnIeLength);
                    WssRSNANotifyParams.u4IfIndex =
                        pWssStaWepProcessDB->u4BssIfIndex;
                    WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_ASSOC_IND;

                    /* Send Notification msg to Rsna */
                    if (RsnaProcessWssNotification (&WssRSNANotifyParams)
                        == OSIX_FAILURE)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessAssocFrames:- RsnaProcessWssNotification"
                                    "returned Failure !!!! \n");
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                        return (OSIX_FAILURE);
                    }
                }
#endif
#ifdef WPA_WANTED
                if (WpaGetFsRSNAEnabled ((INT4) u4WlanIfIndex,
                                         &i4RetValDot11RSNAEnabled) !=
                    SNMP_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConfigResp:- "
                                "RsnaGetDot11RSNAEnabled() returned failure\n");
                }
                if (((i4RetValDot11RSNAEnabled == RSNA_ENABLED) &&
                     (RsnaGetIsPtkInDoneState (staMacAddr) != OSIX_SUCCESS)) &&
                    pWssStaWepProcessDB->u1AssocMode == WSSSTA_WPA1_MODE)
                {
                    /* Send the notification to RSNA, only when the PTK is not in done state
                     *                      * If the four way handshake is completed, this notifciation should not be sent
                     *                                           * again. It should be sent only once, when open authentication is completed*/
                    /* make a MLME indication */

                    MEMSET (WssRSNANotifyParams.MLMEASSOCIND.au1StaMac, 0, 6);
                    MEMCPY (WssRSNANotifyParams.MLMEASSOCIND.au1StaMac,
                            staMacAddr, 6);
                    MEMSET (WssRSNANotifyParams.MLMEASSOCIND.au1RsnIe, 0,
                            RSNA_IE_LEN);
                    MEMCPY (WssRSNANotifyParams.MLMEASSOCIND.au1RsnIe,
                            pWssStaWepProcessDB->au1WpaIE, RSNA_IE_LEN);
                    WssRSNANotifyParams.MLMEASSOCIND.u1RsnaIELen = (UINT1)
                        (pWssStaWepProcessDB->u4WpaIeLength);
                    WssRSNANotifyParams.u4IfIndex =
                        pWssStaWepProcessDB->u4BssIfIndex;
                    WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_ASSOC_IND;

                    /* Send Notification msg to Rsna */
                    if (RsnaProcessWssNotification (&WssRSNANotifyParams)
                        == OSIX_FAILURE)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessAssocFrames:- RsnaProcessWssNotification"
                                    "returned Failure !!!! \n");
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                        return (OSIX_FAILURE);
                    }
                }

#endif

            }
        }
        else
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConfigResp:"
                        " Station entry not found in the WEP process DB \r\n");
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            return OSIX_FAILURE;
        }

    }
    /* For Deauthentication */
    if (pWssStaConfigReqInfo->DelSta.isPresent == TRUE)
    {
        MEMCPY (staMacAddr, pWssStaConfigReqInfo->DelSta.StaMacAddr,
                sizeof (tMacAddr));
        pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);
        if (pWssStaWepProcessDB != NULL)
        {
            u2Aid = pWssStaWepProcessDB->u2AssociationID;
            u4BssIfIndex = pWssStaWepProcessDB->u4BssIfIndex;

#ifdef WSSUSER_WANTED
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
            /*  To Remove the configured bandwidth value 
             *  in the case of user-role enabled */
            if (WssUserRoleModuleStatus () == OSIX_SUCCESS)
            {
                WssUserSetBandWidth (staMacAddr,
                                     pWssStaWepProcessDB->u4BandWidth,
                                     pWssStaWepProcessDB->u4DLBandWidth,
                                     pWssStaWepProcessDB->u4ULBandWidth,
                                     OSIX_TRUE);
            }
#endif
#endif
            u2Sessid = pWssStaWepProcessDB->u2Sessid;
            wssStaParams.u2CapabilityInfo =
                pWssStaWepProcessDB->WssstaParams.u2CapabilityInfo;
            WssStaConstructDBMsg (WSS_STA_DEAUTHENTICATED, u4BssIfIndex,
                                  staMacAddr, u2Aid, u2Sessid, &wssStaParams,
                                  &(pwssStaDB->WssStaDB));
            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, pwssStaDB) !=
                OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WSS_AUTH_UPDATE_DB for DEL-STA Failed\r\n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                return OSIX_FAILURE;
            }
            else
            {
#ifdef NPAPI_WANTED
                /* Call NPAPI */
                NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                                     NP_WLAN_CLIENT_MODULE,    /* Module ID */
                                     WLAN_CLIENT_DELETE,    /* Function/OpCode */
                                     0,    /* IfIndex value if applicable */
                                     0,    /* No. of Port Params */
                                     0);

                MEMCPY (WlanParams.bssIdMacAddr,
                        pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
                WlanClientParams.pWlanNpParams = &WlanParams;

                MEMCPY (WlanClientParams.clientMacAddr, staMacAddr,
                        sizeof (tMacAddr));
                MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
                wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
                wssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              &wssWlanDB) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssWlanProcessWssWlanDBMsg: \r\n");
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }
                WlanClientParams.clientVlanId =
                    wssWlanDB.WssWlanAttributeDB.u2VlanId;
                if (FNP_FAILURE == capwapNpWlanClientDelete (&WlanClientParams))
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WLAN_CLIENT_DELETE Failed \r\n");
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }
#endif
#ifdef RFMGMT_WANTED
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u2WtpInternalId
                    = u2Sessid;
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId
                    = pWssStaConfigReqInfo->DelSta.u1RadioId;
                MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.
                        RfMgmtStaProcessReq.ClientMac, staMacAddr,
                        sizeof (tMacAddr));

                if (WssIfProcessRfMgmtMsg (RFMGMT_DELETE_CLIENT_STATS,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaProcessReAssocFrames: "
                                "RF module processing failed\r\n");
                }
#endif

                if (WssStaUpdateWepProcessDB (WSSSTA_DELETE_PROCESS_DB,
                                              pWssStaWepProcessDB) !=
                    OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WSSSTA_DELETE_PROCESS_DB Failed \r\n");
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                    return OSIX_FAILURE;
                }
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                return OSIX_SUCCESS;
            }
        }
        else                    /*added error message if  pWssStaWepProcessDB  is NULL */
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaConfigResp: pWssStaWepProcessDB is NULL \r\n");
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            return OSIX_SUCCESS;
        }
    }
#ifdef WSSUSER_WANTED
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
    /* For BandWidth Update */
    /*  This Vendor Specific payload is TRUE in the case of user-role enabled 
     *  and it is used to configure the Bandwidth value for the specific station 
     *  in the Hardware*/
    if ((pWssStaWepProcessDB != NULL) &&
        (pWssStaConfigReqInfo->VendSpec.isOptional == TRUE))
    {
        WssUserSetBandWidth (staMacAddr, pWssStaWepProcessDB->u4BandWidth,
                             pWssStaWepProcessDB->u4DLBandWidth,
                             pWssStaWepProcessDB->u4ULBandWidth, OSIX_FALSE);
    }
#endif
#endif
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStationRateLimitCb                                  *
 *                                                                           *
 *  Description     : This function is a callback to set the ratelimit for   *
 *                    the station IP.                                        *
 *  Input(s)        : macAddr - Station Mac Address.                          *
 *                    ipAddr - Station IP Address.
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

VOID
WssStationRateLimitCb (tMacAddr macAddr, UINT4 *u4IpAddr)
{
    tWssWlanDB          wssWlanDB;
#ifdef NPAPI_WANTED
    tWlanHostIpv4Key    wlanClientKey;
    tWlanSSIDRateParam  wlanClientRateParam;
    UINT4               u4Result = 0;
    UINT4               u4MeterId = 0;
#endif
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;

    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStationRateLimitCb:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return;
    }

    UNUSED_PARAM (u4IpAddr);

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
#ifdef NPAPI_WANTED
    MEMSET (&wlanClientKey, 0, sizeof (tWlanHostIpv4Key));
    MEMSET (&wlanClientRateParam, 0, sizeof (tWlanSSIDRateParam));
#endif
    MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));

    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            macAddr, sizeof (tMacAddr));

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStationRateLimitCb "
                    "Failed to retrieve data from Station DB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return;
    }

    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
        pWssifauthDBMsgStruct->WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;

    wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB) ==
        OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStationRateLimitCb "
                    "Failed to retrieve data from BSSID IfIndex Mapping DB\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return;
    }

#ifdef NPAPI_WANTED
    /* Get the SSID rate for Upstream */
    wlanClientKey.u4VrfId = 0;
    wlanClientKey.wlcIpAddr.ipAddr.u4_addr[0] = u4IpAddr;

    wlanClientRateParam.u1SipRateLimit =
        wssWlanDB.WssWlanAttributeDB.u1QosRateLimit;

    wlanClientRateParam.u4SipCIR =
        wssWlanDB.WssWlanAttributeDB.u4QosUpStreamCIR;
    wlanClientRateParam.u4SipCBS =
        wssWlanDB.WssWlanAttributeDB.u4QosUpStreamCBS;
    wlanClientRateParam.u4SipEIR =
        wssWlanDB.WssWlanAttributeDB.u4QosUpStreamEIR;
    wlanClientRateParam.u4SipEBS =
        wssWlanDB.WssWlanAttributeDB.u4QosUpStreamEBS;

    u4Result = capwapNpWlanSSIDSipRateSet (&wlanClientKey,
                                           &wlanClientRateParam, &u4MeterId);
    if (u4Result == FNP_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStationRateLimitCb: "
                    "capwapNpWlanSSIDSipRateSet failed.\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return;
    }

    /* Get the SSID rate for Downstream */
    wlanClientRateParam.u1DipRateLimit = FNP_TRUE;
    wlanClientRateParam.u4DipCIR =
        wssWlanDB.WssWlanAttributeDB.u4QosDownStreamCIR;
    wlanClientRateParam.u4DipCBS =
        wssWlanDB.WssWlanAttributeDB.u4QosDownStreamCBS;
    wlanClientRateParam.u4DipEIR =
        wssWlanDB.WssWlanAttributeDB.u4QosDownStreamEIR;
    wlanClientRateParam.u4DipEBS =
        wssWlanDB.WssWlanAttributeDB.u4QosDownStreamEBS;

    u4Result = capwapNpWlanSSIDDipRateSet (&wlanClientKey,
                                           &wlanClientRateParam, u4MeterId + 1);
    if (u4Result == FNP_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStationRateLimitCb: "
                    "capwapNpWlanSSIDDipRateSet failed.\r\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return;
    }
#endif
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
    return;
}

/*****************************************************************************
 *  Function Name   : WssStaSendRsnaSessionKey                               *
 *                                                                           *
 *  Description     : This function is to send the RSN session key           *
 *                                                                           *
 *  Input(s)        : macAddr - Station Mac Address.                         *
 *                    ipAddr - Station IP Address.
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

UINT4
WssStaSendRsnaSessionKey (tMacAddr staMacAddr, UINT4 u4BssIfIndex,
                          UINT1 u1KeyType)
{
#ifdef RSNA_WANTED
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tRadioIfGetDB       radioIfgetDB;
    tWssWlanDB         *pwssWlanDB = NULL;
    tRsnaIEElements     RsnaIEElements;
    UINT1               u1RadioId;
    UINT2               u2WtpId = 0;
    UINT1               u1WlanID = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4WlanIfIndex = 0;
    UINT2               u2Aid;
    UINT2               u2SessId = 0;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;
    UINT1               au1RsnaKey[RSNA_MAX_PTK_KEY_LEN];
    UINT1               u1KeyLength;
    UINT1               u1CipherIndex;
    UINT1               u1AkmIndex;
    UINT4               u4RsnIeLength = 0;
    UINT2               u2VlanId = 0;
#ifdef WPA_WANTED
    tWpaIEElements      WpaIEElements;
    INT4                i4FsRSNAEnabled = WPA_DISABLED;
    UINT1               au1WpaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x50, 0xF2, 2 };
    MEMSET (&WpaIEElements, 0, sizeof (tWpaIEElements));
#endif

    UINT1               au1RsnaAuthKeyMgmt8021X[] = { 0x00, 0x0F, 0xAC, 1 };
    UINT1               au1RsnaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x0F, 0xAC, 2 };
#ifdef PMF_WANTED
    UINT1               au1RsnaAuthKeyMgmt8021XSHA256[] =
        { 0x00, 0x0F, 0xAC, 5 };
    UINT1               au1RsnaAuthKeyMgmtPskSHA256Over8021X[] =
        { 0x00, 0x0F, 0xAC, 6 };
#endif
#ifdef WSSUSER_WANTED
    UINT4               u4BandWidth = 0;
    UINT4               u4DLBandWidth = 0;
    UINT4               u4ULBandWidth = 0;
    UINT4               u4UsedVolume = 0;
    UINT1               au1UserName[MAX_USER_NAME_LENGTH];

    MEMSET (au1UserName, 0, MAX_USER_NAME_LENGTH);
#endif
    pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pwlcHdlrMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        return OSIX_FAILURE;
    }

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pwlcHdlrMsg, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RsnaIEElements, 0, sizeof (tRsnaIEElements));

    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId;

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pwssWlanDB->WssWlanAttributeDB.BssId, sizeof (tMacAddr));

    u2WtpId = pwssWlanDB->WssWlanAttributeDB.u2WtpInternalId;
    u1WlanID = pwssWlanDB->WssWlanAttributeDB.u1WlanId;
    u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);

    if (pWssStaWepProcessDB != NULL)
    {
        u2Aid = pWssStaWepProcessDB->u2AssociationID;
        u2SessId = pWssStaWepProcessDB->u2Sessid;
    }
    else
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessReAssocFrames:"
                    "Retreiving Failed For WssStaWepProcessDB");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
    pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId = u2SessId;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.isPresent = TRUE;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageType = ADD_STATION;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageLength =
        WSSSTA_ADDSTA_LEN;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1RadioId = u1RadioId;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1MacAddrLen =
        sizeof (tMacAddr);
    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.StaMacAddress,
            staMacAddr, sizeof (tMacAddr));
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2VlanId = u2VlanId;

    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.isPresent = TRUE;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2MessageType =
        IEEE_STATION;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2MessageLength =
        WSSSTA_IEEESTA_LEN;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1RadioId = u1RadioId;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2AssocId = u2Aid;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1Flags = TRUE;
    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.stationMacAddress,
            staMacAddr, sizeof (tMacAddr));
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u2Capab = u2WtpId;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.u1WlanId = u1WlanID;
    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaMsg.au1SuppRates,
            radioIfgetDB.RadioIfGetAllDB.au1SupportedRate,
            STRLEN (radioIfgetDB.RadioIfGetAllDB.au1SupportedRate));

    if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4WlanIfIndex) == OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return (OSIX_FAILURE);
    }

    RsnaGetDot11RSNAEnabled ((INT4) u4WlanIfIndex, &i4Dot11RSNAEnabled);
#ifdef WPA_WANTED
    WpaGetFsRSNAEnabled ((INT4) u4WlanIfIndex, &i4FsRSNAEnabled);
    if (i4FsRSNAEnabled == WPA_ENABLED && u1KeyType == WPA_IE_ID)
    {
        if (WpaGetRsnIEParams (u4WlanIfIndex, &WpaIEElements) == OSIX_SUCCESS)
        {
            if (RsnaGetRsnSessionKey
                (staMacAddr, au1RsnaKey, &u1KeyLength, &u1CipherIndex,
                 &u1AkmIndex) == OSIX_SUCCESS)
            {

                u4RsnIeLength = 0;    /*Resetting the variable to fill the packet
                                     *length*/
                pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.isPresent =
                    TRUE;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.
                    u2MessageType = IEEE_STATION_SESSION_KEY;

                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.
                        stationMacAddress, staMacAddr, sizeof (tMacAddr));

                pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2Flags = 0;

                MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.au1PairwiseTSC, 0, 6);

                MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.au1PairwiseRSC, 0, 6);

                /* Fill the WPA IE params */
                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u1ElemId = WpaIEElements.u1ElemId;
                u4RsnIeLength++;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u1Len = WpaIEElements.u1Len;
                u4RsnIeLength++;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2Ver = WpaIEElements.u2Ver;
                u4RsnIeLength = u4RsnIeLength + 2;

                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.RsnaIEElements.au1GroupCipherSuite,
                        WpaIEElements.au1GroupCipherSuite,
                        RSNA_CIPHER_SUITE_LEN);
                u4RsnIeLength = u4RsnIeLength + RSNA_CIPHER_SUITE_LEN;

                /* Regarding the Pairwise Cipher Suite in RSN Element of Station
                   Config Request Message, although WLAN might have multiple pairwise
                   cipher suite been supported, we will send only the selected
                   pairwise cipher suite for that station.  This is because the
                   selected pairwise cipher suite detail is required at WTP side
                   to identify the Pairwise Cipher and key Length */

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2PairwiseCipherSuiteCount = 1;

                u4RsnIeLength = u4RsnIeLength + RSNA_PAIRWISE_CIPHER_COUNT;

                if (RsnaIEElements.u2PairwiseCipherSuiteCount ==
                    RSNA_PAIRWISE_CIPHER_COUNT)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaPwCipherDB[0].au1PairwiseCipher,
                            WpaIEElements.
                            aWpaPwCipherDB[u1CipherIndex].au1PairwiseCipher,
                            RSNA_CIPHER_SUITE_LEN);
                }
                else
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaPwCipherDB[0].au1PairwiseCipher,
                            WpaIEElements.aWpaPwCipherDB[0].au1PairwiseCipher,
                            RSNA_CIPHER_SUITE_LEN);

                }

                u4RsnIeLength = u4RsnIeLength + RSNA_CIPHER_SUITE_LEN;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2AKMSuiteCount = 1;
                u4RsnIeLength = u4RsnIeLength + 2;
                if (MEMCMP
                    (WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].
                     au1AKMSuite, au1WpaAuthKeyMgmtPskOver8021X,
                     RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK -
                                                    1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                }
                else if (MEMCMP
                         (WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                          au1AKMSuite, au1RsnaAuthKeyMgmt8021X,
                          RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X -
                                                    1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                }
                u4RsnIeLength = u4RsnIeLength + RSNA_AKM_SELECTOR_LEN;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2PmkidCount =
                    WpaIEElements.u2PmkidCount;

                if (RsnaIEElements.u2PmkidCount != 0)
                {
                    u4RsnIeLength = u4RsnIeLength + 2;

                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.aPmkidList,
                            WpaIEElements.aPmkidList, RSNA_PMKID_LEN);
                    u4RsnIeLength = u4RsnIeLength + RSNA_PMKID_LEN;
                }

                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.au1Key, au1RsnaKey, u1KeyLength);

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u1Len = u4RsnIeLength;
                /* Fill the message length based on the RSN IE params filled */
                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.u2MessageLength =
                    (UINT2) ((UINT4) (20 + u1KeyLength) + u4RsnIeLength);
            }
        }
    }
#endif

    if (i4Dot11RSNAEnabled == RSNA_ENABLED && u1KeyType == RSNA_IE_ID)
    {
        if (RsnaGetRsnIEParams (u4WlanIfIndex, &RsnaIEElements) == OSIX_SUCCESS)
        {
            if (RsnaGetRsnSessionKey
                (staMacAddr, au1RsnaKey, &u1KeyLength, &u1CipherIndex,
                 &u1AkmIndex) == OSIX_SUCCESS)
            {

                u4RsnIeLength = 0;    /*Resetting the variable to fill the packet
                                     *length*/
                pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.isPresent =
                    TRUE;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.
                    u2MessageType = IEEE_STATION_SESSION_KEY;

                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.
                        stationMacAddress, staMacAddr, sizeof (tMacAddr));

                pwlcHdlrMsg->StationConfReq.wssStaConfig.StaSessKey.u2Flags = 0;

                MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.au1PairwiseTSC, 0, 6);

                MEMSET (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.au1PairwiseRSC, 0, 6);

                /* Fill the RSN IE params */
                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u1ElemId =
                    RsnaIEElements.u1ElemId;
                u4RsnIeLength++;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u1Len = RsnaIEElements.u1Len;
                u4RsnIeLength++;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2Ver = RsnaIEElements.u2Ver;
                u4RsnIeLength = u4RsnIeLength + 2;

                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.RsnaIEElements.au1GroupCipherSuite,
                        RsnaIEElements.au1GroupCipherSuite,
                        RSNA_CIPHER_SUITE_LEN);
                u4RsnIeLength = u4RsnIeLength + RSNA_CIPHER_SUITE_LEN;

                /* Regarding the Pairwise Cipher Suite in RSN Element of Station
                   Config Request Message, although WLAN might have multiple pairwise
                   cipher suite been supported, we will send only the selected
                   pairwise cipher suite for that station.  This is because the
                   selected pairwise cipher suite detail is required at WTP side
                   to identify the Pairwise Cipher and key Length */

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2PairwiseCipherSuiteCount = 1;

                u4RsnIeLength = u4RsnIeLength + RSNA_PAIRWISE_CIPHER_COUNT;

                if (RsnaIEElements.u2PairwiseCipherSuiteCount ==
                    RSNA_PAIRWISE_CIPHER_COUNT)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaPwCipherDB[0].au1PairwiseCipher,
                            RsnaIEElements.
                            aRsnaPwCipherDB[u1CipherIndex].au1PairwiseCipher,
                            RSNA_CIPHER_SUITE_LEN);
                }
                else
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaPwCipherDB[0].au1PairwiseCipher,
                            RsnaIEElements.aRsnaPwCipherDB[0].au1PairwiseCipher,
                            RSNA_CIPHER_SUITE_LEN);

                }

                u4RsnIeLength = u4RsnIeLength + RSNA_CIPHER_SUITE_LEN;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2AKMSuiteCount = 1;
                u4RsnIeLength = u4RsnIeLength + 2;
                if (MEMCMP
                    (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].
                     au1AKMSuite, au1RsnaAuthKeyMgmtPskOver8021X,
                     RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK -
                                                      1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                }
                else if (MEMCMP
                         (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                          au1AKMSuite, au1RsnaAuthKeyMgmt8021X,
                          RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X -
                                                      1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                }
#ifdef PMF_WANTED
                else if (MEMCMP
                         (RsnaIEElements.
                          aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].au1AKMSuite,
                          au1RsnaAuthKeyMgmtPskSHA256Over8021X,
                          RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 -
                                       1].au1AKMSuite, RSNA_AKM_SELECTOR_LEN);
                }
                else if (MEMCMP
                         (RsnaIEElements.
                          aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 -
                                     1].au1AKMSuite,
                          au1RsnaAuthKeyMgmt8021XSHA256,
                          RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            RsnaIEElements.
                            aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 -
                                       1].au1AKMSuite, RSNA_AKM_SELECTOR_LEN);
                }
#endif
#ifdef DEBUG_WANTED
                if (RsnaIEElements.u2AKMSuiteCount == RSNA_AKM_SUITE_COUNT)
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.aRsnaAkmDB[0].au1AKMSuite,
                            RsnaIEElements.aRsnaAkmDB[u1AkmIndex].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                }
                else
                {
                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.aRsnaAkmDB[0].au1AKMSuite,
                            RsnaIEElements.aRsnaAkmDB[0].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                }
#endif
                u4RsnIeLength = u4RsnIeLength + RSNA_AKM_SELECTOR_LEN;

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2RsnCapab =
                    RsnaIEElements.u2RsnCapab;
                if (RsnaIEElements.u2RsnCapab != 0)
                {
                    u4RsnIeLength = u4RsnIeLength + 2;
                }

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u2PmkidCount =
                    RsnaIEElements.u2PmkidCount;

                if (RsnaIEElements.u2PmkidCount != 0)
                {
                    u4RsnIeLength = u4RsnIeLength + 2;

                    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                            StaSessKey.RsnaIEElements.aPmkidList,
                            RsnaIEElements.aPmkidList, RSNA_PMKID_LEN);
                    u4RsnIeLength = u4RsnIeLength + RSNA_PMKID_LEN;
                }

                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.
                        StaSessKey.au1Key, au1RsnaKey, u1KeyLength);

                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.RsnaIEElements.u1Len = u4RsnIeLength;
                /* Fill the message length based on the RSN IE params filled */
                pwlcHdlrMsg->StationConfReq.wssStaConfig.
                    StaSessKey.u2MessageLength =
                    (UINT2) ((UINT4) (20 + u1KeyLength) + u4RsnIeLength);
            }
        }
    }
#ifdef WSSUSER_WANTED

    if (MEMCMP
        (RsnaIEElements.aRsnaAkmDB[u1AkmIndex].au1AKMSuite,
         au1RsnaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN) == 0)
    {
        if (WssUserRoleModuleStatus () == OSIX_SUCCESS)
        {
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.isOptional = TRUE;
            pwlcHdlrMsg->StationConfReq.vendSpec.isOptional = TRUE;
            pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleType =
                VENDOR_SPECIFIC_PAYLOAD;
            pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleLen = 26;
            pwlcHdlrMsg->StationConfReq.vendSpec.elementId =
                VENDOR_USERROLE_TYPE;
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.u2MsgEleType = VENDOR_USERROLE_MSG;
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.u2MsgEleLen = 22;
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.u4VendorId = VENDOR_ID;
            MEMCPY (pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                    WssStaVendSpecPayload.StaMacAddr, staMacAddr,
                    sizeof (tMacAddr));
            if (WssUserGetSessionAttributes
                (staMacAddr, au1UserName, &u4UsedVolume, &u4BandWidth,
                 &u4DLBandWidth, &u4ULBandWidth) == OSIX_FAILURE)
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
            pWssStaWepProcessDB->u4BandWidth = u4BandWidth;
            pWssStaWepProcessDB->u4DLBandWidth = u4DLBandWidth;
            pWssStaWepProcessDB->u4ULBandWidth = u4ULBandWidth;

            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.u4BandWidth = u4BandWidth;
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.u4DLBandWidth = u4DLBandWidth;
            pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
                WssStaVendSpecPayload.u4ULBandWidth = u4ULBandWidth;

        }
    }
#endif
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg) ==
        OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
#else
    UNUSED_PARAM (staMacAddr);
    UNUSED_PARAM (u4BssIfIndex);
    UNUSED_PARAM (u1KeyType);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaDeleteRsnaSessionKey                             *
 *                                                                           *
 *  Description     : This function is to delete the RSN session key         *
 *                                                                           *
 *  Input(s)        : macAddr - Station Mac Address.                         *
 *                    u4BssIfIndex - BSS if index                            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/
UINT4
WssStaDeleteRsnaSessionKey (tMacAddr staMacAddr, UINT4 u4BssIfIndex)
{
#ifdef RSNA_WANTED
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    tWssifauthDBMsgStruct wssStaDB;
    UINT1               u1RadioId;
    tRadioIfGetDB       radioIfgetDB;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2SessId = 0;

    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pwlcHdlrMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaDeleteRsnaSessionKey:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaDeleteRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        return OSIX_FAILURE;
    }
    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pwssWlanDB->WssWlanAttributeDB.BssId, sizeof (tMacAddr));

    u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);

    if (pWssStaWepProcessDB != NULL)
    {
        u2SessId = pWssStaWepProcessDB->u2Sessid;
    }
    else
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessReAssocFrames:"
                    "Retreiving Failed For WssStaWepProcessDB");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    WssStaConstructDBMsg (WSS_STA_DEAUTHENTICATED, u4BssIfIndex, staMacAddr,
                          0, 0, NULL, &wssStaDB.WssStaDB);

    if (!WssIfProcessWssAuthDBMsg (WSS_AUTH_UPDATE_DB, &wssStaDB))
    {

        MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
        pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId = u2SessId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.isPresent = TRUE;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageType =
            DELETE_STATION;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageLength =
            WSSSTA_DELSTA_LEN;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1RadioId = u1RadioId;
        pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1MacAddrLen =
            sizeof (tMacAddr);
        MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.StaMacAddr,
                staMacAddr, sizeof (tMacAddr));
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg)
            == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
#else
    UNUSED_PARAM (staMacAddr);
    UNUSED_PARAM (u4BssIfIndex);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaSendBandWidth                                    *
 *                                                                           *
 *  Description     : This function is to send the User-Role Bandwidth Value *
 *                    and the station mac to AP                              *
 *                                                                           *
 *  Input(s)        : staMacAddr - Station Mac Address.                      *
 *                    u4BandWidth- BandWidth Value                           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

UINT4
WssStaSendBandWidth (tMacAddr staMacAddr, UINT4 u4BandWidth,
                     UINT4 u4DLBandWidth, UINT4 u4ULBandWidth)
{
#ifdef WSSUSER_WANTED
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tRadioIfGetDB       radioIfgetDB;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2SessId = 0;
    UINT2               u2VlanId = 0;
    UINT1               u1RadioId;

    pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pwlcHdlrMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        return OSIX_FAILURE;
    }

    pWssStaWepProcessDB = WssStaProcessEntryGet (staMacAddr);

    if (pWssStaWepProcessDB == NULL)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pwlcHdlrMsg, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
        pWssStaWepProcessDB->u4BssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId;

    u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;

    radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    radioIfgetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "ValidateAssocFrame: "
                    "Failed to retrieve data from RadioIfDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;
    u2SessId = pWssStaWepProcessDB->u2Sessid;
    MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
    pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId = u2SessId;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.isPresent = TRUE;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageType = ADD_STATION;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2MessageLength =
        WSSSTA_ADDSTA_LEN;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1RadioId = u1RadioId;
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u1MacAddrLen =
        sizeof (tMacAddr);
    MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.StaMacAddress,
            staMacAddr, sizeof (tMacAddr));
    pwlcHdlrMsg->StationConfReq.wssStaConfig.AddSta.u2VlanId = u2VlanId;

    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        isOptional = TRUE;
    pwlcHdlrMsg->StationConfReq.vendSpec.isOptional = TRUE;
    pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
    pwlcHdlrMsg->StationConfReq.vendSpec.u2MsgEleLen =
        VENDOR_SPEC_USERROLE_MSGELE_LEN + VENDOR_USERROLE_TYPE;
    pwlcHdlrMsg->StationConfReq.vendSpec.elementId = VENDOR_USERROLE_TYPE;
    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        u2MsgEleType = VENDOR_USERROLE_MSG;
    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        u2MsgEleLen = VENDOR_SPEC_USERROLE_MSGELE_LEN;
    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        u4VendorId = VENDOR_ID;
    MEMCPY (pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.
            WssStaVendSpecPayload.StaMacAddr, staMacAddr, sizeof (tMacAddr));

    pWssStaWepProcessDB->u4BandWidth = u4BandWidth;
    pWssStaWepProcessDB->u4DLBandWidth = u4DLBandWidth;
    pWssStaWepProcessDB->u4ULBandWidth = u4ULBandWidth;

    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        u4BandWidth = u4BandWidth;
    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        u4DLBandWidth = u4DLBandWidth;
    pwlcHdlrMsg->StationConfReq.vendSpec.unVendorSpec.WssStaVendSpecPayload.
        u4ULBandWidth = u4ULBandWidth;

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pwlcHdlrMsg) ==
        OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
#else
    UNUSED_PARAM (staMacAddr);
    UNUSED_PARAM (u4BandWidth);
    UNUSED_PARAM (u4DLBandWidth);
    UNUSED_PARAM (u4ULBandWidth);
    return OSIX_SUCCESS;
#endif
}

#ifdef WPS_WANTED
/*****************************************************************************
 *  Function Name   : WssWpsSendDeAuthMsg                                   *
 *                                                                           *
 *  Description     : This function is used to send the Deauth Msg           *
 *                                                                           *
 *  Input(s)        : macAddr - Station Mac Address.                         *
 *                    u4BssIfIndex - BSS if index                            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

INT4
WssWpsSendDeAuthMsg (UINT1 *u1BssId, UINT1 *u1StaMac, UINT2 u2SessId)
{
    tWssMacMsgStruct   *pwssDeauthMsg = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    pWssStaWepProcessDB = WssStaProcessEntryGet (u1StaMac);
    if (pWssStaWepProcessDB != NULL)
    {
        pWssStaWepProcessDB->bDeAuthDueToWps = OSIX_TRUE;
    }
    MEMCPY (gau1DeAuthWpsstaMacAddr, u1StaMac, 6);

    pwssDeauthMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssDeauthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssWpsSendDeAuthMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssWpsSendDeAuthMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");

        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        return OSIX_FAILURE;
    }

    /* Construct Deauth Frame */
    pwssDeauthMsg->msgType = 0;
    MEMSET (&(pwssDeauthMsg->unMacMsg.DeauthMacFrame), 0,
            sizeof (tDot11DeauthMacFrame));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA,
            u1BssId, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA,
            u1StaMac, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId,
            u1BssId, sizeof (tMacAddr));
    pwssDeauthMsg->unMacMsg.DeauthMacFrame.u2SessId = u2SessId;

    pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode =
        WSSSTA_DEAUTH_MS_LEAVING;
    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
            sizeof (tWssMacMsgStruct));
    pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;

    if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG, pWlcHdlrMsgStruct))
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);

        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);

    return OSIX_SUCCESS;
}
#endif
/*****************************************************************************
 *  Function Name   : WssRsnaSendDeAuthMsg                                   *
 *                                                                           *
 *  Description     : This function is used to send the Deauth Msg           *
 *                                                                           *
 *  Input(s)        : macAddr - Station Mac Address.                         *
 *                    u4BssIfIndex - BSS if index                            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

INT4
WssRsnaSendDeAuthMsg (UINT1 *u1BssId, UINT1 *u1StaMac)
{
    tWssMacMsgStruct   *pwssDeauthMsg = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    pwssDeauthMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssDeauthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssRsnaSendDeAuthMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssRsnaSendDeAuthMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");

        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        return OSIX_FAILURE;
    }

    pWssStaWepProcessDB = WssStaProcessEntryGet (u1StaMac);

    if (pWssStaWepProcessDB == NULL)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        return OSIX_FAILURE;
    }

    /* Construct Deauth Frame */
    pwssDeauthMsg->msgType = 0;
    MEMSET (&(pwssDeauthMsg->unMacMsg.DeauthMacFrame), 0,
            sizeof (tDot11DeauthMacFrame));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA,
            u1BssId, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA,
            u1StaMac, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId,
            u1BssId, sizeof (tMacAddr));
    pwssDeauthMsg->unMacMsg.DeauthMacFrame.u2SessId =
        pWssStaWepProcessDB->u2Sessid;

    pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode =
        WSSSTA_PREV_AUTH_EXPIRED;
    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
            sizeof (tWssMacMsgStruct));
    pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;

    CAPWAP_TRC (CAPWAP_STATION_TRC,
                "WssRsnaSendDeAuthMsg :::: Sending DEAUTH .. AUTH Timer Expired \r\n");
    if (!WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG, pWlcHdlrMsgStruct))
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);

        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaVerifyMaxClientCount                             *
 *                                                                           *
 *  Description     : This function is used to get number of clients         *
 *                                                                           *
 *  Input(s)        : u4WlanIndex1                                    *
 *                    pu4RetValClientCount                                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/

INT4
WssStaVerifyMaxClientCount (UINT4 u4WlanIndex, UINT4 *pu4RetValClientCount,
                            tMacAddr staMacAddr)
{
    tWssWlanDB         *pwssWlanDB = NULL;
    tWssWlanDB          wssWlanDB;
    UINT4               u4StaCount = 0;
    UINT4               u4WlanIfIndex = 0;
    UINT1               u1ReconnectFlag = 0;
    UINT4               u4Index = 0;
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    WssStaShowClient ();

    for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
    {
        pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            gaWssClientSummary[u4Index].u4BssIfIndex;
        pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        u4WlanIfIndex = pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
        if (u4WlanIndex == u4WlanIfIndex)
        {
            if (MEMCMP (gaWssClientSummary[u4Index].staMacAddr,
                        staMacAddr, sizeof (tMacAddr)) != 0)
            {
                u4StaCount++;
            }
            else
            {
                u1ReconnectFlag = 1;
                u4StaCount++;
            }
        }
    }
    if (u1ReconnectFlag == 0)
    {
        u4StaCount++;
    }
    *pu4RetValClientCount = u4StaCount;
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaVerifyRadioMaxClientCount                        *
 *                                                                           *
 *  Description     : This function is used to get number of clients         *
 *                                                                           *
 *  Input(s)        : u4RadioIfIndex                                         *
 *                    pu4RetValClientCount                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/

INT4
WssStaVerifyRadioMaxClientCount (UINT4 u4RadioIfIndex,
                                 UINT4 *pu4RetValClientCount,
                                 tMacAddr staMacAddr)
{
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT4               u4StaCount = 0;
    UINT4               u4RadioIfIndex1 = 0;
    UINT4               u4Index = 0;
    UINT1               u1ReconnectFlag = 0;

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    WssStaShowClient ();

    for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
    {
        pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            gaWssClientSummary[u4Index].u4BssIfIndex;
        pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        u4RadioIfIndex1 = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
        if (u4RadioIfIndex == u4RadioIfIndex1)
        {
            if (MEMCMP (gaWssClientSummary[u4Index].staMacAddr,
                        staMacAddr, sizeof (tMacAddr)) != 0)
            {
                u4StaCount++;
            }
            else
            {
                u1ReconnectFlag = 1;
                u4StaCount++;
            }
        }
    }
    if (u1ReconnectFlag == 0)
    {
        u4StaCount++;
    }
    *pu4RetValClientCount = u4StaCount;
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

#ifdef RFMGMT_WANTED
/*****************************************************************************
 *  Function Name   : WssStaRFClientDBDelete                                 *
 *                                                                           *
 *  Description     : This function is used delete the client DB in RF       *
 *                                                                           *
 *  Input(s)        : macAddr - Station Mac Address.                         *
 *                    u4BssIfIndex - BSS if index                            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/

INT4
WssStaRFClientDBDelete (UINT4 u4BssIfIndex, tMacAddr staMacAddr)
{
    tRfMgmtMsgStruct    WssMsgStruct;
    tWssWlanDB          WssWlanDB;
    tRadioIfGetDB       RadioIfgetDB;
    UINT1               i1RetVal = 0;

    MEMSET (&WssMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfgetDB, 0, sizeof (tRadioIfGetDB));

    /*Get the radioif index from the available BssIfIndex */
    WssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    WssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    i1RetVal =
        WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &WssWlanDB);
    if (i1RetVal == OSIX_SUCCESS)
    {

        /*Get the radio id and Wtp Internal id from RadioIfDB */
        RadioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfgetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex =
            WssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;

        i1RetVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                             &RadioIfgetDB);
        if (i1RetVal == OSIX_SUCCESS)
        {

            /*Delete the station from the RF client stats DB */
            WssMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u2WtpInternalId =
                RadioIfgetDB.RadioIfGetAllDB.u2WtpInternalId;
            WssMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId =
                RadioIfgetDB.RadioIfGetAllDB.u1RadioId;
            MEMCPY (WssMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.ClientMac,
                    staMacAddr, sizeof (tMacAddr));
            i1RetVal = RfMgmtProcessWssIfMsg (RFMGMT_DELETE_CLIENT_STATS,
                                              &WssMsgStruct);
            if (i1RetVal == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Failed WssStaRFClientDBDelete:"
                            "Failed to delete entry from client stats DB");
                return OSIX_FAILURE;
            }

        }
        else
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Failed WssStaRFClientDBDelete:"
                        "Failed to get Wtp Internal Id and radio id");
            return OSIX_FAILURE;
        }
    }
    else
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Failed WssStaRFClientDBDelete:"
                    "Failed to Get Radioif index");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif

/*****************************************************************************
 *  Function Name   : WssUtilConvertBytesToVolumeUsed                        *
 *                                                                           *
 *  Description     : This function is used to convert bytes into kb/mb/gb   *
 *                                                                           *
 *  Input(s)        : i4UsedBytes - Volume used in bytes                     *
 *                    pu1VolumeUsed - Converted volume                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/
VOID
WssUtilConvertBytesToVolumeUsed (INT4 i4UsedBytes, UINT1 *pu1VolumeUsed)
{
    if (i4UsedBytes >= WSSSTA_GB)
    {
        if (i4UsedBytes % WSSSTA_GB == 0)
            SPRINTF ((CHR1 *) pu1VolumeUsed, "%d WSSSTA_GB",
                     i4UsedBytes / WSSSTA_GB);
        else
            SPRINTF ((CHR1 *) pu1VolumeUsed, "%.1f WSSSTA_GB",
                     ((DBL8) (i4UsedBytes / WSSSTA_GB)));
    }
    else if (i4UsedBytes >= WSSSTA_MB)
    {
        if (i4UsedBytes % WSSSTA_MB == 0)
            SPRINTF ((CHR1 *) pu1VolumeUsed, "%d WSSSTA_MB",
                     i4UsedBytes / WSSSTA_MB);
        else
            SPRINTF ((CHR1 *) pu1VolumeUsed, "%.1f WSSSTA_MB",
                     ((DBL8) (i4UsedBytes / WSSSTA_MB)));
    }
    else
    {
        if (i4UsedBytes == 0)
        {

            pu1VolumeUsed[0] = '0';

            pu1VolumeUsed[1] = '\0';
        }
        else if (i4UsedBytes >= WSSSTA_KB)
        {
            if (i4UsedBytes % WSSSTA_KB == 0)
                SPRINTF ((CHR1 *) pu1VolumeUsed, "%d WSSSTA_KB",
                         i4UsedBytes / WSSSTA_KB);
            else
                SPRINTF ((CHR1 *) pu1VolumeUsed, "%.1f WSSSTA_KB",
                         ((DBL8) (i4UsedBytes / WSSSTA_KB)));
        }
        else
        {

            SPRINTF ((CHR1 *) pu1VolumeUsed, "%d Bytes", i4UsedBytes);

        }
    }
}

/*****************************************************************************/
/*  Function Name   : WssUtilGetTimeStr                                      */
/*  Description     : This function returns the current local time in a      */
/*                    format required by firewall logging functionality      */
/*  Input(s)        : ac1TimeStr - Buffer of length atleast 21 bytes         */
/*                                                                           */
/*  Output(s)       : None                                                   */
/*                                                                           */
/*  Returns         : None                                                   */
/*                                                                           */
/****************************************************************************/

VOID
WssUtilGetTimeStr (CHR1 * pc1TimeStr)
{
    tUtlTm              tm;

    UtlGetTime (&tm);

    /* Micro seconds is not updated in FSAP, so micro secs is '0'  */
    SNPRINTF (pc1TimeStr, MAX_TIME_LEN, "%02d/%02d-%02d:%02d:%02d.%06u ",
              tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, 0);
    return;
}

/*****************************************************************************
 *  Function Name   : WssStaGetAssocId                                       *
 *                                                                           *
 *  Description     : This function is used to get the first free Assoc ID   *
 *                                                                           *
 *  Input(s)        : u2Aid                                                  *
 *                                                                           *
 *  Output(s)       : u2Aid                                                  *
 *                                                                           *
 *  Returns         : NONE                                                   *
 *****************************************************************************/
UINT4
WssStaGetAssocId (tWssStaWepProcessDB * pWssStaWepProcessDB)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2WtpInternalId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssStaWtpProfileIdfromBssIfIndex (pWssStaWepProcessDB,
                                          &u2WtpInternalId) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaGetAssocId:" "Failed to get WtpInternalId \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bFreeAssocId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaGetAssocId:" "Unable to get association ID \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssStaWepProcessDB->u2AssociationID =
        (UINT2) pWssIfCapwapDB->CapwapGetDB.u4FreeAssocId;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssFreeAssocId                                         *
 *                                                                           *
 *  Description     : This function is used to free the  Assoc ID            *
 *                                                                           *
 *  Input(s)        : u2Aid                                                  *
 *                                                                           *
 *  Output(s)       : u2Aid                                                  *
 *                                                                           *
 *  Returns         : NONE                                                   *
 *****************************************************************************/
UINT4
WssFreeAssocId (tWssStaWepProcessDB * pWssStaWepProcessDB)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2UsedAid = pWssStaWepProcessDB->u2AssociationID;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssStaWtpProfileIdfromBssIfIndex (pWssStaWepProcessDB,
                                          &u2WtpInternalId) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaGetAssocId:" "Failed to get WtpInternalId \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /*As per standard  2 most significant bit of association identifier is set
     * to 1. Hence derive the Aid from the value and set that Aid as free in
     * u4AssocIdMapping*/
    u2UsedAid = ((u2UsedAid >> 8) | (u2UsedAid << 8));
    pWssIfCapwapDB->CapwapGetDB.u4FreeAssocId = (UINT4) (u2UsedAid & 0x3FFF);

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bFreeAssocId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaGetAssocId:" "Unable to get association ID \n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssStaWtpProfileIdfromBssIfIndex                       *
 *                                                                           *
 *  Description     : Utility to get WtpInternalId from BssifIndex           *
 *                                                                           *
 *  Input(s)        : tWssStaWepProcessDB                                    *
 *                                                                           *
 *  Output(s)       : WtpInternalId                                          *
 *                                                                           *
 *  Returns         : NONE                                                   *
 *****************************************************************************/
UINT4
WssStaWtpProfileIdfromBssIfIndex (tWssStaWepProcessDB * pWssStaWepProcessDB,
                                  UINT2 *pu2WtpInternalId)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {

        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaGetAssocId:"
                    "UtlShMemAllocWssIfAuthDbBuf returned failure");
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex = pWssStaWepProcessDB->
        u4BssIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaGetAssocId:" "RadioIfindex fetch failed\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaGetAssocId:" "WtpInternalId fetch failed\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    *pu2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

#ifdef PMF_WANTED

/*****************************************************************************
 *  Function Name   : WssStaSendSAQueryResponseMsg                           *
 *                                                                           *
 *  Description     : This function is to send the SA Query response to the  *
 *                    STA.                                                   *
 *                                                                           *
 *  Input(s)        : staMacAddr - Station Mac Address.                      *
 *                    BssIfIndex - Station IP Address.                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

UINT4
WssStaSendSAQueryResponseMsg (tMacAddr staMacAddr, UINT4 u4BssIfIndex,
                              UINT2 u2TransactionId, UINT2 u2SessId)
{

    tWssMacMsgStruct   *pwssStaQueryMsg = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg"
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pwssStaQueryMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();

    if (pwssStaQueryMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pwssStaQueryMsg, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&(pwssStaQueryMsg->unMacMsg.StationQueryActionFrame), 0,
            sizeof (tDot11ActionStationQueryFrame));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

    if ((WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                   pwssWlanDB)) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg"
                    "WssIfProcessWssWlanDBMsg returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssStaQueryMsg);
        return WSSMAC_FAILURE;
    }

    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.
        u2MacFrameCtrl = WSSMAC_SET_ACTION_FRAME_CTRL;

    /* Address1 field - Destination MAC */
    MEMCPY (pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.
            MacMgmtFrmHdr.u1DA, staMacAddr, WSSMAC_MAC_ADDR_LEN);
    /* Address2 field - Source MAC */
    MEMCPY (pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.
            MacMgmtFrmHdr.u1SA, pwssWlanDB->WssWlanAttributeDB.BssId,
            WSSMAC_MAC_ADDR_LEN);
    /* Addr3 field - BSSID */
    MEMCPY (pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.
            MacMgmtFrmHdr.u1BssId, pwssWlanDB->WssWlanAttributeDB.BssId,
            WSSMAC_MAC_ADDR_LEN);

    /* Category */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u1CategoryCode
        = DOT11_SA_QUERY;
    /* Action Field */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u1ActionField
        = DOT11_SA_QUERY_RESP;
    /*Transaction Identifier */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u2TransactionId
        = u2TransactionId;

    /* Session Id */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u2SessId = u2SessId;

    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssStaQueryMsg,
            sizeof (tWssMacMsgStruct));
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_SAQUERY_ACTION_RSP,
                                pWlcHdlrMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "WSS_WLCHDLR_MAC_ASSOC_RSP returned FAILURE\r\n");

        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssStaQueryMsg);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssStaQueryMsg);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaSendSAQueryReqMsg                                *
 *                                                                           *
 *  Description     : This function is to send the SA Query request to the   *
 *                    STA.                                                   *
 *                                                                           *
 *  Input(s)        : staMacAddr - Station Mac Address.                      *
 *                    BssIfIndex - Station IP Address.                       *
 *                    u2TransactionId - Transaction identifier for the STA   *
 *                    u2SessId - Session Id, which identifies the AP         *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

UINT4
WssStaSendSAQueryReqMsg (tMacAddr staMacAddr, UINT4 u4BssIfIndex,
                         UINT2 u2TransactionId, UINT2 u2SessId)
{

    tWssMacMsgStruct   *pwssStaQueryMsg = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg"
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pwssStaQueryMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();

    if (pwssStaQueryMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pwssStaQueryMsg, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&(pwssStaQueryMsg->unMacMsg.StationQueryActionFrame), 0,
            sizeof (tDot11ActionStationQueryFrame));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

    if ((WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                   pwssWlanDB)) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendSAQueryResponseMsg"
                    "WssIfProcessWssWlanDBMsg returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssStaQueryMsg);
        return WSSMAC_FAILURE;
    }

    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.MacMgmtFrmHdr.
        u2MacFrameCtrl = WSSMAC_SET_ACTION_FRAME_CTRL;

    /* Address1 field - Destination MAC */
    MEMCPY (pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.
            MacMgmtFrmHdr.u1DA, staMacAddr, WSSMAC_MAC_ADDR_LEN);
    /* Address2 field - Source MAC */
    MEMCPY (pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.
            MacMgmtFrmHdr.u1SA, pwssWlanDB->WssWlanAttributeDB.BssId,
            WSSMAC_MAC_ADDR_LEN);
    /* Addr3 field - BSSID */
    MEMCPY (pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.
            MacMgmtFrmHdr.u1BssId, pwssWlanDB->WssWlanAttributeDB.BssId,
            WSSMAC_MAC_ADDR_LEN);

    /* Category */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u1CategoryCode
        = DOT11_SA_QUERY;
    /* Action Field */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u1ActionField
        = DOT11_SA_QUERY_REQ;
    /*Transaction Identifier */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u2TransactionId
        = u2TransactionId;

    /* Session Id */
    pwssStaQueryMsg->unMacMsg.StationQueryActionFrame.u2SessId = u2SessId;

    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssStaQueryMsg,
            sizeof (tWssMacMsgStruct));
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_SAQUERY_ACTION_REQ,
                                pWlcHdlrMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "WSS_WLCHDLR_MAC_ASSOC_RSP returned FAILURE\r\n");

        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssStaQueryMsg);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssStaQueryMsg);

    return OSIX_SUCCESS;
}

#endif

/*****************************************************************************
 *  Function Name   : WssStaUtilStaDbDeleteEntry                             *
 *                                                                           *
 *  Description     : This utility is used to Delete the station entry from  *
 *                    Station DB                                             *
 *                                                                           *
 *  Input(s)        : staMacAddr  - Station MAC Address                      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : None                                                   *
 *****************************************************************************/
VOID
WssStaUtilStaDbDeleteEntry (tMacAddr staMacAddr)
{
    tWssIfAuthStateDB  *pStaDB = NULL;
    pStaDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB, staMacAddr));
    if (pStaDB != NULL)
    {
        RBTreeRem (gWssStaStateDB, (tRBElem *) pStaDB);
        MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID, (UINT1 *) pStaDB);
    }
    return;
}

/*****************************************************************************
 *  Function Name   : WssStaUtilValidateDiscoveryMode                        *
 *                                                                           *
 *  Description     : This utility is used to Validate the discovery mode of *
 *                    the stations                                           *  
 *                                                                           *
 *  Input(s)        : MacAddr  - Station MAC Address                         *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX/FAILURE                              *
 *****************************************************************************/
INT1
WssStaUtilValidateDiscoveryMode (tMacAddr MacAddr)
{
    INT4                i4RetValStationType = 0;
    tWssIfCapDB        *pWssIfCapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMCPY (pWssIfCapDB->BaseMacAddress, MacAddr, MAC_ADDR_LEN);

    if (CapwapGetFsStationType (&i4RetValStationType) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    if (i4RetValStationType == 1)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_STA_BLACKLIST_ENTRY,
                                     pWssIfCapDB) == OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }
    else if (i4RetValStationType == 2)
    {
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_STA_WHITELIST_ENTRY,
                                     pWssIfCapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
            return OSIX_FAILURE;
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }
    else
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }
}

/*****************************************************************************
 *  Function Name   : WssStaSendDeAuthMsg                                   *
 *                                                                           *
 *  Description     : This function is used to send the Deauth Msg           *
 *                                                                           *
 *  Input(s)        : macAddr - Station Mac Address.                         *
 *                    u4BssIfIndex - BSS if index                            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NONE                                                   *
 *****************************************************************************/

UINT1
WssStaSendDeAuthMsg (tMacAddr StaMacAddr)
{
    tWssMacMsgStruct   *pwssDeauthMsg = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    pWssStaWepProcessDB = WssStaProcessEntryGet (StaMacAddr);
    if (pWssStaWepProcessDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssSendDeAuthMsg:-"
                    "Fetch from WepProcessDB returned failure\n");
        return OSIX_FAILURE;
    }

    pwssDeauthMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssDeauthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssSendDeAuthMsg:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssSendDeAuthMsg:- "
                    "UtlShMemAllocWlcBuf returned failure\n");

        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        return OSIX_FAILURE;
    }
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaSendRsnaSessionKey:- "
                    "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    /* Construct Deauth Frame */
    pwssDeauthMsg->msgType = 0;
    MEMSET (&(pwssDeauthMsg->unMacMsg.DeauthMacFrame), 0,
            sizeof (tDot11DeauthMacFrame));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1SA,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1DA,
            pWssStaWepProcessDB->stationMacAddress, sizeof (tMacAddr));
    MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.u1BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    pwssDeauthMsg->unMacMsg.DeauthMacFrame.u2SessId =
        pWssStaWepProcessDB->u2Sessid;

    pwssDeauthMsg->unMacMsg.DeauthMacFrame.ReasonCode.u2MacFrameReasonCode =
        WSSSTA_DEAUTH_MS_LEAVING;
    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pwssDeauthMsg,
            sizeof (tWssMacMsgStruct));
    pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x06;

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DEAUTH_MSG, pWlcHdlrMsgStruct)
        == OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssSendDeAuthMsg "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssSendDeAuthMsg "
                    "Failed to retrieve data from WssIfProcessRadioIfDBMsg\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.u2SessId =
        pWssStaWepProcessDB->u2Sessid;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.isPresent = TRUE;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u2MessageType =
        DELETE_STATION;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u2MessageLength =
        WSSSTA_DELSTA_LEN;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u1MacAddrLen =
        sizeof (tMacAddr);
    MEMCPY (pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.StaMacAddr,
            StaMacAddr, sizeof (tMacAddr));
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pWlcHdlrMsgStruct)
        == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssSendDeAuthMsg "
                    "Failed to send station config request\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaAclRule                                          *
 *                                                                           *
 *  Description     : This utility is used to check whether the station is in*
 *                    whitelist or blacklist and send deAuth                 *  
 *                                                                           *
 *  Input(s)        : MacAddr  - Station MAC Address                         *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX/FAILURE                              *
 *****************************************************************************/
INT1
WssStaAclRule (tWssStaDeleteStation * pWssStaDeleteStation)
{
    UINT1               u1Index = 0;
    UINT1               u1DeAuthFlag = 0;
    UINT1               u1RetVal = 0;
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    if (pWssStaDeleteStation == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaAclRule:- Invalid input received\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }
    if (MEMCMP (pWssStaDeleteStation->StaMacAddr, "\0\0\0\0\0\0",
                sizeof (tMacAddr)) != 0)
    {
        u1RetVal =
            (UINT1) WssStaUtilValidateDiscoveryMode
            (pWssStaDeleteStation->StaMacAddr);
        if (u1RetVal == OSIX_FAILURE)
        {
            u1RetVal = WssStaSendDeAuthMsg (pWssStaDeleteStation->StaMacAddr);
            if (u1RetVal == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaAclRule:-"
                            "WssStaSendDeAuthMsg returned failure\n");
            }

        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }
    WssStaShowClient ();
    for (u1Index = 0; u1Index < gu4ClientWalkIndex; u1Index++)
    {
        u1DeAuthFlag = 0;
        MEMCPY (pWssIfCapDB->BaseMacAddress,
                gaWssClientSummary[u1Index].staMacAddr, MAC_ADDR_LEN);
        if (pWssStaDeleteStation->u1StationType == WSSIF_CAPWAP_STA_WHITELIST)
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_STA_WHITELIST_ENTRY,
                                         pWssIfCapDB) != OSIX_SUCCESS)
            {
                u1DeAuthFlag = 1;
            }
        }
        else
        {
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_STA_BLACKLIST_ENTRY,
                                         pWssIfCapDB) == OSIX_SUCCESS)
            {
                u1DeAuthFlag = 1;
            }
        }
        /*Send Deauth if the STA MAC is not present in the correct Access List */
        if (u1DeAuthFlag == 1)
        {
            u1RetVal =
                WssStaSendDeAuthMsg (gaWssClientSummary[u1Index].staMacAddr);
            if (u1RetVal == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaAclRule:-"
                            "WssStaSendDeAuthMsg returned failure\n");
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}
#endif
