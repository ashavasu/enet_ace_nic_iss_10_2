/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *                                                                            *
 *  $Id: wssstahwnpwr.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $                                                                     *
 *  Description: This file contains the NPAPI related API                     *
 *                                                                            *
 ******************************************************************************/

#include "wssstahwnpwr.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : WlanClientNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tWlanClientNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
WlanClientNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_FAILURE;
    UINT4               u4Opcode = 0;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pWlanClientNpModInfo = &(pFsHwNp->WlanClientNpModInfo);

    if (NULL == pWlanClientNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case WLAN_CLIENT_ADD:
        {
            tWlanClientNpWrAdd *pEntry = NULL;
            pEntry = &pWlanClientNpModInfo->WlanClientNpAdd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpWlanClientAdd (pEntry->pWlanClientNpParams);
            break;
        }
        case WLAN_CLIENT_GET:
        {
            tWlanClientNpWrGet *pEntry = NULL;
            pEntry = &pWlanClientNpModInfo->WlanClientNpGet;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpWlanClientGet (pEntry->pWlanClientNpParams);
            break;
        }

        case WLAN_CLIENT_DELETE:
        {
            tWlanClientNpWrDelete *pEntry = NULL;
            pEntry = &pWlanClientNpModInfo->WlanClientNpDelete;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpWlanClientDelete (pEntry->pWlanClientNpParams);
            break;
        }
        case WLAN_CLIENT_RULE_ADD:
        {
            tWlanClientNpWrRule *pEntry = NULL;
            pEntry = &pWlanClientNpModInfo->WlanClientNpRuleAdd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpCreateClientRule(pEntry->pWlanClientRuleParams);
            break;
        }
        case WLAN_CLIENT_RULE_DEL:
        {
            tWlanClientNpWrRule *pEntry = NULL;
            pEntry = &pWlanClientNpModInfo->WlanClientNpRuleAdd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpDeleteClientRule(pEntry->pWlanClientRuleParams);
            break;
        }

        default:
            break;

    }                            /* switch */

    return (u1RetVal);
}
