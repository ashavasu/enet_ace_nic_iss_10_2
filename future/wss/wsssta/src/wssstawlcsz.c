/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssstawlcsz.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $                                                                         *
 *  Description: This file contains the Init and Memory related api          *
 *                                                                           *
 *****************************************************************************/

#define _WSSSTAWLCSZ_C
#include "wssstawlcinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
WssstawlcSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSSTAWLC_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =(INT4)
            MemCreateMemPool (FsWSSSTAWLCSizingParams[i4SizingId].u4StructSize,
                              FsWSSSTAWLCSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(WSSSTAWLCMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WssstawlcSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
WssstawlcSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWSSSTAWLCSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, WSSSTAWLCMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
WssstawlcSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSSTAWLC_MAX_SIZING_ID; i4SizingId++)
    {
        if (WSSSTAWLCMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WSSSTAWLCMemPoolIds[i4SizingId]);
            WSSSTAWLCMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
