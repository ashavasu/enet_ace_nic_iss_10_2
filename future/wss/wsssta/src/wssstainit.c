/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssstainit.c,v 1.4 2017/11/24 10:37:11 siva Exp $                                                                         *
 *  Description: This file contains the Init and Memory related api          *
 *                                                                           *
 *****************************************************************************/
#ifndef _WSSSTAINIT_C_
#define _WSSSTAINIT_C_

#include "wssstawlcinc.h"
#include "sizereg.h"
#include "wlchdlrproto.h"

tRBTree             gWssStaWepProcessDB;
tWssStaWepProcessDBWalk gaWssStaWepProcessDB[MAX_STA_SUPP_PER_WLC];
static INT1         gDBWalkCnt;
UINT4               gu4WssStaWebAuthDebugMask = 0;

#ifdef BAND_SELECT_WANTED
tRBTree             gWssStaBandSteerDB;
UINT4               gu4StaDebugMask;

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaCompareWlcBandSteerDBRBTree                          *
 *                                                                           *
 * Description  :  This function creates the tWssStaBandSteerDB              *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaCompareWlcBandSteerDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssStaBandSteerDB *pNode1 = (tWssStaBandSteerDB *) e1;
    tWssStaBandSteerDB *pNode2 = (tWssStaBandSteerDB *) e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (pNode1->stationMacAddress,
                       pNode2->stationMacAddress, sizeof (tMacAddress));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    i4CmpVal = MEMCMP (pNode1->BssIdMacAddr,
                       pNode2->BssIdMacAddr, sizeof (tMacAddress));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}
#endif

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaInit                                                 *
 *                                                                           *
 * Description  : Memory creation and initialization required for            *
 *                WssSta module                                              *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
WssStaInit (VOID)
{

    WssIfAuthInit ();

    if (wssauthTmrInit () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaInit: Timer init Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaInit: Timer init Failed \r\n"));
        return OSIX_FAILURE;
    }

    if (WssstawlcSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssstawlcSizingMemCreateMemPools: "
                    "Mem Pool Creation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssstawlcSizingMemCreateMemPools: "
                      "Mem Pool Creation Failed \r\n"));
        return OSIX_FAILURE;
    }

    gWssStaWepProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssStaWepProcessDB, nextWssStaWepProcessDB)),
                              WssStaCompareWepProcessDBRBTree);

    if (gWssStaWepProcessDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssstaInit: gWssStaWepProcessDB RB Tree Creation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssstaInit: gWssStaWepProcessDB RB Tree Creation Failed \r\n"));
        return OSIX_FAILURE;
    }

    gWssStaTCFilterHandleDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssStaTCFilterHandle,
                                nextWssStaTCFilterHandle)),
                              WssStaCompareTCFilterHandleRBTree);

    if (gWssStaTCFilterHandleDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssstaInit: gWssStaTCFilterHandleDB RB Tree Creation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssstaInit: gWssStaTCFilterHandleDB RB Tree Creation Failed \r\n"));
        return OSIX_FAILURE;
    }
#ifdef BAND_SELECT_WANTED
    gWssStaBandSteerDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssStaBandSteerDB, StaBandSteerDBNode)),
                              WssStaCompareWlcBandSteerDBRBTree);

    if (gWssStaBandSteerDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssstaInit:  gWssStaBandSteerDB RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gi4StaSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "WSTA", SYSLOG_CRITICAL_LEVEL);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaCompareTCFilterHandleRBTree                          *
 *                                                                           *
 * Description  : This function is use during the adding of a new station to *
 *                the  Rb tree                                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaCompareTCFilterHandleRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssStaTCFilterHandle *pNode1 = (tWssStaTCFilterHandle *) e1;
    tWssStaTCFilterHandle *pNode2 = (tWssStaTCFilterHandle *) e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->StationMacAddr,
                       &pNode2->StationMacAddr, sizeof (tMacAddress));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaCompareWepProcessDBRBTree                            *
 *                                                                           *
 * Description  : This function is use during the adding TC Filter Handle in *
 *                the  Rb tree                                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : WSSSTA_RB_GREATER,WSSSTA_RB_LESS                           *
 *                and WSSSTA_RB_EQUAL                                        *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaCompareWepProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssStaWepProcessDB *pNode1 = e1;
    tWssStaWepProcessDB *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->stationMacAddress,
                       &pNode2->stationMacAddress, sizeof (tMacAddress));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaRecvMemClear                                         *
 *                                                                           *
 * Description  : This function is use to delete the Mem Pool                *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : VOID                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
WssStaRecvMemClear (VOID)
{

    WssstawlcSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaSizingMemCreateMemPools                              *
 *                                                                           *
 * Description  : This function is use to create a Mem Pool                  *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
#if 0                            /* ABI */
INT4
WssStaSizingMemCreateMemPools (VOID)
{

    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSSTA_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsWSSSTASizingParams[i4SizingId].u4StructSize,
                              FsWSSSTASizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(WSSSTAMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WssStaSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaSzRegisterModuleSizingParams                         *
 *                                                                           *
 * Description  :                                                            *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{

    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWSSSTASizingParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaSizingMemDeleteMemPools                              *
 *                                                                           *
 * Description  :                                                            *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : VOID                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
WssStaSizingMemDeleteMemPools (VOID)
{

    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSSTA_MAX_SIZING_ID; i4SizingId++)
    {
        if (WSSSTAMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WSSSTAMemPoolIds[i4SizingId]);
            WSSSTAMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
#endif
/*****************************************************************************
 *                                                                           *
 * Function     : WssStaProcessEntryGet                                      *
 *                                                                           *
 * Description  : This function is use to get the pointer for process        *
 *                entry during Authentication.                               *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : tWssStaWepProcessDB *                                      *
 *                                                                           *
 *****************************************************************************/
tWssStaWepProcessDB *
WssStaProcessEntryGet (tMacAddr staMacAddr)
{

    tWssStaWepProcessDB wepProcessDB;
    tWssStaWepProcessDB *pwepProcessDB;

    MEMSET (&wepProcessDB, 0, sizeof (tWssStaWepProcessDB));
    MEMCPY (wepProcessDB.stationMacAddress, staMacAddr, sizeof (tMacAddr));
    pwepProcessDB = ((tWssStaWepProcessDB *) RBTreeGet (gWssStaWepProcessDB,
                                                        (tRBElem *) &
                                                        wepProcessDB));
    if (pwepProcessDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC, "Entry is not present in Process DB \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                      "Entry is not present in Process DB"));
    }
    return pwepProcessDB;
}

UINT4
WssStaUpdateWepProcessDB (eProcessDB action,
                          tWssStaWepProcessDB * pWssStaWepProcessDB)
{
    tWssStaWepProcessDB *pWepProcessDB = NULL;
    switch (action)
    {
        case WSSSTA_ADD_PROCESS_DB:
            /*Get a free association id and as per standard 2 most significant
             * bits is set to 1*/
            WssStaGetAssocId (pWssStaWepProcessDB);
            pWssStaWepProcessDB->u2AssociationID =
                pWssStaWepProcessDB->u2AssociationID | WSSSTA_AID_MASK;
            pWssStaWepProcessDB->u2AssociationID =
                (pWssStaWepProcessDB->u2AssociationID << 8) |
                (pWssStaWepProcessDB->u2AssociationID >> 8);

            if (RBTreeAdd (gWssStaWepProcessDB, pWssStaWepProcessDB) !=
                RB_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaUpdateWepProcessDB: "
                            "Failed to add an entry to the Wep Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                              "WssStaUpdateWepProcessDB: "
                              "Failed to add an entry to the Wep Process DB"));
                return OSIX_FAILURE;
            }
            break;

        case WSSSTA_DELETE_PROCESS_DB:
            pWepProcessDB = ((tWssStaWepProcessDB *)
                             RBTreeGet (gWssStaWepProcessDB,
                                        (tRBElem *) pWssStaWepProcessDB));
            if (pWepProcessDB == NULL)
            {

                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in the Wep Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                              "Entry is not present in the Wep Process DB"));
                return OSIX_FAILURE;
            }
            WssFreeAssocId (pWepProcessDB);
            RBTreeRem (gWssStaWepProcessDB, (tRBElem *) pWepProcessDB);
            MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID, (UINT1 *) pWepProcessDB);
            break;

        case WSSSTA_CREATE_BAND_STEER_DB:
            break;
        case WSSSTA_GET_BAND_STEER_DB:
            break;
        case WSSSTA_SET_BAND_STEER_DB:
            break;
        case WSSSTA_DESTROY_BAND_STEER_DB:
            break;
        case WSSSTA_GET_FIRST_BAND_STEER_DB:
            break;
        case WSSSTA_GET_NEXT_BAND_STEER_DB:
            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

#ifdef BAND_SELECT_WANTED

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaUpdateBandSteerProcessDB                             *
 *                                                                           *
 * Description  : This function process BandSteer RBTree operations          *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
WssStaUpdateBandSteerProcessDB (eProcessDB action,
                                tWssStaBandSteerDB * pWssStaBandSteerDB)
{
    tWssStaBandSteerDB *pBandSteerDB = NULL;
    switch (action)
    {
        case WSSSTA_CREATE_BAND_STEER_DB:
            /*Memory Allocation for the node to be added in the tree */
            if ((pBandSteerDB =
                 (tWssStaBandSteerDB *) MemAllocMemBlk
                 (WSSSTA_BAND_STEER_POOLID)) == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "WssStaUpdateBandSteerProcessDB Entry is not present in the BandSteer Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT1) gi4StaSysLogId,
                              "Memory Alloc failed in the BandSteer DB!!!"));
                return OSIX_FAILURE;

            }
            MEMSET (pBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
            MEMCPY (pBandSteerDB->stationMacAddress,
                    pWssStaBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pBandSteerDB->BssIdMacAddr,
                    pWssStaBandSteerDB->BssIdMacAddr, MAC_ADDR_LEN);
            pBandSteerDB->u1StaStatus = pWssStaBandSteerDB->u1StaStatus;
            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gWssStaBandSteerDB, (tRBElem *) pBandSteerDB) != RB_SUCCESS)
            {
                WSSSTA_TRC6 (WSSSTA_MGMT_TRC,
                             "RBTreeAdd failed for adding %02x:%02x:%02x:%02x:%02x:%02x in gWssStaBandSteerDB\n",
                             pBandSteerDB->stationMacAddress[0],
                             pBandSteerDB->stationMacAddress[1],
                             pBandSteerDB->stationMacAddress[2],
                             pBandSteerDB->stationMacAddress[3],
                             pBandSteerDB->stationMacAddress[4],
                             pBandSteerDB->stationMacAddress[5]);
                MemReleaseMemBlock (WSSSTA_BAND_STEER_POOLID,
                                    (UINT1 *) pBandSteerDB);
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeAdd failed in the BandSteer Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT1) gi4StaSysLogId,
                              "RBTreeAdd failed in BandSteer DB!!!"));
                return OSIX_FAILURE;
            }
            WSSSTA_TRC6 (WSSSTA_MGMT_TRC,
                         "RBTreeAdd Success for adding %02x:%02x:%02x:%02x:%02x:%02x in gWssStaBandSteerDB\n",
                         pBandSteerDB->stationMacAddress[0],
                         pBandSteerDB->stationMacAddress[1],
                         pBandSteerDB->stationMacAddress[2],
                         pBandSteerDB->stationMacAddress[3],
                         pBandSteerDB->stationMacAddress[4],
                         pBandSteerDB->stationMacAddress[5]);

            break;

        case WSSSTA_GET_BAND_STEER_DB:

            pBandSteerDB =
                RBTreeGet (gWssStaBandSteerDB, (tRBElem *) pWssStaBandSteerDB);
            if (pBandSteerDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGet failed in the BandSteer Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT1) gi4StaSysLogId,
                              "Get failed in BandSteer DB!!!"));
                return OSIX_FAILURE;

            }
            MEMCPY (pWssStaBandSteerDB->stationMacAddress,
                    pBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pWssStaBandSteerDB->BssIdMacAddr,
                    pBandSteerDB->BssIdMacAddr, MAC_ADDR_LEN);
            pWssStaBandSteerDB->u1StaStatus = pBandSteerDB->u1StaStatus;

            break;
        case WSSSTA_SET_BAND_STEER_DB:
            pBandSteerDB =
                RBTreeGet (gWssStaBandSteerDB, (tRBElem *) pWssStaBandSteerDB);
            if (pBandSteerDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeSet failed in the BandSteer Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT1) gi4StaSysLogId,
                              "Set failed in BandSteer DB!!!"));
                return OSIX_FAILURE;

            }
            MEMCPY (pBandSteerDB->stationMacAddress,
                    pWssStaBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pBandSteerDB->BssIdMacAddr,
                    pWssStaBandSteerDB->BssIdMacAddr, MAC_ADDR_LEN);
            pBandSteerDB->u1StaStatus = pWssStaBandSteerDB->u1StaStatus;
            pBandSteerDB->u1StaAssocCount = pWssStaBandSteerDB->u1StaAssocCount;

            break;
        case WSSSTA_DESTROY_BAND_STEER_DB:
            pBandSteerDB =
                RBTreeGet (gWssStaBandSteerDB, (tRBElem *) pWssStaBandSteerDB);
            WSSSTA_TRC6 (WSSSTA_MGMT_TRC,
                         "Going to Destroy %02x:%02x:%02x:%02x:%02x:%02x from gWssStaBandSteerDB\n",
                         pBandSteerDB->stationMacAddress[0],
                         pBandSteerDB->stationMacAddress[1],
                         pBandSteerDB->stationMacAddress[2],
                         pBandSteerDB->stationMacAddress[3],
                         pBandSteerDB->stationMacAddress[4],
                         pBandSteerDB->stationMacAddress[5]);

            if (pBandSteerDB == NULL)
            {
                WSSSTA_TRC6 (WSSSTA_MGMT_TRC,
                             "RBTreeDestroy failure for %02x:%02x:%02x:%02x:%02x:%02x in gWssStaBandSteerDB\n",
                             pBandSteerDB->stationMacAddress[0],
                             pBandSteerDB->stationMacAddress[1],
                             pBandSteerDB->stationMacAddress[2],
                             pBandSteerDB->stationMacAddress[3],
                             pBandSteerDB->stationMacAddress[4],
                             pBandSteerDB->stationMacAddress[5]);
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeDelete failed in the BandSteer Process DB \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT1) gi4StaSysLogId,
                              "Delete failed in BandSteer DB!!!"));
                return OSIX_FAILURE;
            }
            WlcHdlrStaAssocTmrStop (pBandSteerDB);
            RBTreeRem (gWssStaBandSteerDB, (tRBElem *) pBandSteerDB);
            MemReleaseMemBlock (WSSSTA_BAND_STEER_POOLID,
                                (UINT1 *) pBandSteerDB);
            pBandSteerDB = NULL;

            break;
        case WSSSTA_GET_FIRST_BAND_STEER_DB:

            pBandSteerDB = RBTreeGetFirst (gWssStaBandSteerDB);

            if (pBandSteerDB == NULL)
            {
                /* If not present, return error */
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGetFirst failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            /* Copy the values from the DB strcuture to output structure */
            MEMCPY (pWssStaBandSteerDB->stationMacAddress,
                    pBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pWssStaBandSteerDB->BssIdMacAddr,
                    pBandSteerDB->BssIdMacAddr, MAC_ADDR_LEN);
            pWssStaBandSteerDB->u1StaStatus = pBandSteerDB->u1StaStatus;

            pWssStaBandSteerDB->u1StaAssocCount = pBandSteerDB->u1StaAssocCount;

            break;
        case WSSSTA_GET_NEXT_BAND_STEER_DB:
            /* Pass the received input and check entry is already present */
            if (MEMCMP (pWssStaBandSteerDB->stationMacAddress, "\0\0\0\0\0\0",
                        MAC_ADDR_LEN) == 0)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGetNext failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            pBandSteerDB =
                RBTreeGetNext (gWssStaBandSteerDB,
                               (tRBElem *) pWssStaBandSteerDB, NULL);
            if (pBandSteerDB == NULL)
            {
                /* If not present, return error */
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGetNext entry not found in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            /* Copy the values from the DB strcuture to output structure */
            MEMCPY (pWssStaBandSteerDB->stationMacAddress,
                    pBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pWssStaBandSteerDB->BssIdMacAddr,
                    pBandSteerDB->BssIdMacAddr, MAC_ADDR_LEN);
            pWssStaBandSteerDB->u1StaStatus = pBandSteerDB->u1StaStatus;
            pWssStaBandSteerDB->u1StaAssocCount = pBandSteerDB->u1StaAssocCount;
            break;
        case WSSSTA_ADD_PROCESS_DB:
            break;
        case WSSSTA_DELETE_PROCESS_DB:
            break;
        default:
            WSSSTA_TRC (WSSSTA_MGMT_TRC,
                        "RBTree Invalid operation in  BandSteer Process DB \r\n");
            return OSIX_FAILURE;
            break;
    }
    return OSIX_SUCCESS;
}
#endif

INT4
WepProcessDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                    void *pArg, void *pOut)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pWssStaWepProcessDB = (tWssStaWepProcessDB *) pRBElem;
            if (((pWssStaWepProcessDB->stationState == UNAUTH_UNASSOC) &&
                 (pWssStaWepProcessDB->u1ThreshCount >=
                  WSSSTA_AUTH_THRESHOLD))
                || ((pWssStaWepProcessDB->stationState == AUTH_UNASSOC) &&
                    (pWssStaWepProcessDB->u1ThreshCount >=
                     WSSSTA_ASSOC_THRESHOLD)))
            {
                MEMCPY (gaWssStaWepProcessDB[gDBWalkCnt].staMacAddr,
                        pWssStaWepProcessDB->stationMacAddress,
                        sizeof (tMacAddr));
                gDBWalkCnt++;
                WSSSTA_TRC1 (WSSSTA_MGMT_TRC,
                             "WepProcessDBWalkFn - gDBWalkCnt = %u\r\n",
                             gDBWalkCnt);
                SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4StaSysLogId,
                              "WepProcessDBWalkFn - gDBWalkCnt = %u\r\n",
                              gDBWalkCnt));
            }
            else
            {
                pWssStaWepProcessDB->u1ThreshCount++;
            }
        }
    }
    return RB_WALK_CONT;
}

UINT4
WssstaDelEntryFromWepDb (VOID)
{

    tWssStaWepProcessDB WssStaWepProcessDB;

    while (gDBWalkCnt >= 0)
    {
        MEMSET (&WssStaWepProcessDB, 0, sizeof (tWssStaWepProcessDB));
        MEMCPY (WssStaWepProcessDB.stationMacAddress,
                gaWssStaWepProcessDB[gDBWalkCnt].staMacAddr, sizeof (tMacAddr));
        if (WssStaUpdateWepProcessDB (WSSSTA_DELETE_PROCESS_DB,
                                      &WssStaWepProcessDB) != OSIX_SUCCESS)
        {
        }
        gDBWalkCnt--;
    }
    gDBWalkCnt = 0;
    return OSIX_SUCCESS;
}

#endif
