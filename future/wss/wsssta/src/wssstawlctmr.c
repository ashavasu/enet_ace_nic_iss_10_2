/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: wssstawlctmr.c,v 1.2 2017/05/23 14:16:59 siva Exp $                                                                          *
 * Description: This file contains the WSSSTA Timer related API               *
 ******************************************************************************/
#ifndef __WSSSTA_WLC_TMR_C__
#define __WSSSTA_WLC_TMR_C__
#define _WSSSTA_GLOBAL_VAR
#include "wssstawlcinc.h"

static tWssStaTimerList gWssStaTmrList;
extern tRBTree      gWssStaWepProcessDB;
tTmrBlk             wepProcessDBTmr;

/*****************************************************************************
 *                                                                           *
 * Function     : wssauthTmrInit                                              *
 *                                                                           *
 * Description  :  This function creates a timer list for all the timers     *
 *                          in WSSSTA module.                                *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
wssauthTmrInit (VOID)
{

    if (TmrCreateTimerList ((CONST UINT1 *) WLCHDLR_TASK_NAME,
                            WLCHDLR_STATMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *) & (gWssStaTmrList.WssStaTmrListId))
        == TMR_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "wssauthTmrInit: "
                    "Failed to creat the Application Timer List");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "wssauthTmrInit: "
                      "Failed to creat the Application Timer List"));
        return OSIX_FAILURE;
    }

    wssauthTmrInitTmrDesc ();

    /* Start the ONESEC Timer */
    WssStaRespStartTimer (WSSSTA_WEP_PROCESS_DB_TMR, WSSSTA_WEP_TIMEOUT);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : wssauthTmrInitTmrDesc                                       *
 *                                                                           *
 * Description  : This function intializes the timer desc for all            *
 *                the timers in WSSSTA module.                               *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
wssauthTmrInitTmrDesc (VOID)
{
    /* Discovery Timers */
    gWssStaTmrList.aWssStaTmrDesc[WSSSTA_WEP_PROCESS_DB_TMR].TmrExpFn
        = WssStaTmrExp;
    gWssStaTmrList.aWssStaTmrDesc[WSSSTA_WEP_PROCESS_DB_TMR].i2Offset = -1;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaTmrExpHandler                                        *
 *                                                                           *
 * Description  :  This function is called whenever a timer expiry           *
 *                 message is received by Service task. Different timer      *
 *                 expiry handlers are called based on the timer type.       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
UINT4
WssStaTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gWssStaTmrList.WssStaTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        if (u1TimerId < WSSSTA_MAX_TMRS)

        {
            i2Offset = gWssStaTmrList.aWssStaTmrDesc[u1TimerId].i2Offset;

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter. */
                (*(gWssStaTmrList.aWssStaTmrDesc[u1TimerId].TmrExpFn)) (NULL);
            }
            else
            {
                (*(gWssStaTmrList.aWssStaTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaTmrExp                                               *
 *                                                                           *
 * Description  :  This function is called whenever a process DB timer       *
 *                 expires during Authentication                             *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
WssStaTmrExp (VOID *pArg)
{

    UNUSED_PARAM (pArg);

    RBTreeWalk (gWssStaWepProcessDB, (tRBWalkFn) WepProcessDBWalkFn, 0, 0);
    WssstaDelEntryFromWepDb ();
    WssStaRespStartTimer (WSSSTA_WEP_PROCESS_DB_TMR, WSSSTA_WEP_TIMEOUT);
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaRespStartTimer                                       *
 *                                                                           *
 * Description  :  This function is used to start AuthProcess timer          *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaRespStartTimer (UINT1 u1TmrType, UINT4 u4TmrInterval)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    tTmrBlk            *pWssStaTmr = NULL;

    UNUSED_PARAM (pWssStaTmr);

    if (TmrStart (gWssStaTmrList.WssStaTmrListId,
                  &wepProcessDBTmr, u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
    {
        WSSSTA_TRC1 (WSSSTA_FAILURE_TRC,
                     "WssStaRespStartTimer:Failed to start the Timer Type ="
                     " %d \r\n", u1TmrType);
        SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4StaSysLogId,
                      "WssStaRespStartTimer:Failed to start the Timer Type ="
                      " %d \r\n", u1TmrType));
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

#endif
