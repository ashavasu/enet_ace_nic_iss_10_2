/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wssstawebauthmain.c,v 1.5.2.1 2018/03/07 12:52:36 siva Exp $
 * Description: This file contains the BCNMGR Main Task Functions.
 *
 ******************************************************************************/
#ifndef __WSSSTA_WEBAUTH_C__
#define __WSSSTA_WEBAUTH_C__
#include "wssstawlcinc.h"
#include "wssifstaproto.h"
#include "fssocket.h"
#include "capwap.h"
#if defined (IP_WANTED)
#if defined (OS_PTHREADS) || defined (OS_CPSS_MAIN_OS)
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#endif
#endif

#define WSSSTA_TCP_PORT    8000
#define PORT_NAME "vlan1"
#define ZERO_BYTES 0
#define TCP_CKSUM 1
extern INT1         nmhGetIfIpAddr (INT4, UINT4 *);
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
extern UINT4        gu4WssStaWebAuthDebugMask;
static INT4         i4WebAuthSock4Id = -1;
static tWssStaRecvTaskGlobals gWssStaGlobals;
static tWssStaWebAuthTimerList gWssStaWebAuthTmrList;
tTmrBlk             WssStaWebAuthTmr;
extern UINT4        gu4Stups;
static UINT2        u2IpIdentifier = 0;
static UINT4        u4StaRuleNumber = 1;
UINT1               HTTP_VER_STRING[] =
    "HTTP/1.1 302 Temporarily Moved\r\nDate:";
UINT1               HTTP_POST_RESP_VER_STRING[] = "HTTP/1.1 200 OK\r\nDate:";
UINT1               HTTP_POST_RESP_HDR_STRING[] =
    "\r\ncontent-type:text/html; Charset=ISO-8859-1\r\nConnnection:Close\r\nTransfer-Encoding: chunked\r\n\r\n";
UINT1               HTTP_POST_RESP_SUCCESS_STRING[] =
    "\n <HTML><BODY OnLoad=\"document.nameform.Login.focus();\"> <H2 ALIGN=center>Web Login</H2><BR> <FORM name=\"nameform\" method=\"POST\" ACTION=\"/wmi/page/homepage.html\">\n <CENTER><STRONG> <BLINK>Profile Creation Successfull</BLINK></STRONG> </center></CENTER></FORM>\n </BODY></HTML>";
UINT1               HTTP_POST_RESP_FAILURE_STRING[] =
    "\n <HTML><BODY OnLoad=\"document.nameform.Login.focus();\"> <H2 ALIGN=center>Web Login</H2><BR> <FORM name=\"nameform\" method=\"POST\" ACTION=\"/wmi/page/homepage.html\">\n <CENTER><STRONG> <BLINK>Profile Creation Failed </BLINK></STRONG> </center></CENTER></FORM>\n </BODY></HTML>";
UINT1               WSSSTA_HTTP_GET_METHOD[] = "GET";
UINT1               WSSSTA_HTTP_POST_METHOD[] = "POST";
UINT1               HTTP_CMN_DELIMITER[] = "\r\nContent-type:text/html\r\n";
UINT1               HTTP_DELIMITER[] = "\r\n";

UINT1               HTTP_CNTENT[] =
    "Server: \r\nLocation: http://255.255.255.255/iss/specific/wlan_web_auth_login.html\r\nContent-length: 0\r\nConnection: close\r\nContent-Type: text/html\r\n\r\n";

UINT1               HTTP_CNTENT1[] = "Server: \r\nLocation: http://";

UINT1               HTTP_PORTCHANGED[] = ":";
UINT1               HTTP_CNTENT2[] = "/iss/specific/";
UINT1               HTTP_EXT_CNTENT1[] = "Server: \r\nLocation: ";
UINT1               HTTP_EXT_CNTENT2[] =
    "\r\nContent-length: 0\r\nConnection: close\r\nContent-Type: text/html\r\n\r\n";
UINT1               HTTP_CNTENT4[] =
    "\r\nContent-length: 0\r\nConnection: close\r\nContent-Type: text/html\r\n\r\n";

UINT1               gWebAuthNextAck = 0;
extern INT4         WsscfgGetApGroupEnable (UINT4 *);
extern INT1         nmhGetIfMainSubType (INT4, INT4 *);
extern INT4         ArpGetIntfForAddr (UINT4, UINT4, UINT4 *);

INT4
WssStaWebAuthRecvMemInit (VOID)
{
    if (WssstawebSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name   : WssStaWebAuthPacketOnSocket
 * Description     : Call back function from SelAddFd(), when packet is
 *                   received
 * Inputs          : None
 * Output          : Sends an event to WssSta receiver task
 * Returns         : None.
 ******************************************************************************/
VOID
WssStaWebAuthPacketOnSocket (INT4 i4SockFd)
{
    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Entering WssStaWebAuthPacketOnSocket\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "Entering WssStaWebAuthPacketOnSocket\r\n"));
    if (i4SockFd == i4WebAuthSock4Id)
    {
        if (OsixEvtSend (gWssStaGlobals.WssStaRecvTaskId,
                         WSSSTA_TCPDATA_PACKET_ON_CPU) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaWebAuthPacketOnSocket:Event send Failed \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          "WssStaWebAuthPacketOnSocket:Event send Failed !!!"));
        }
    }
    return;
}

/*****************************************************************************
 * Function     : WssStaWebAuthSockInit
 * Description  : This routine initialises the socket related parameters.
 * Input        : None
 * Output       : None
 * Returns      : OSIX_SUCCESS  or OSIX_FAILURE
 *****************************************************************************/
INT4
WssStaWebAuthSockInit (VOID)
{
    INT4                i4Flags = 0;
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));
    WSSSTA_FN_ENTRY ();
    if (i4WebAuthSock4Id != -1)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaWebAuthSocket already created \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthSocket already created !1"));
        return OSIX_SUCCESS;
    }
    /* Create the socket in which the TCP,HTTP packets lifted to the CPU
     * would be received */
    i4WebAuthSock4Id = socket (PF_PACKET, SOCK_RAW, OSIX_HTONS (ETH_P_ALL));
    if (i4WebAuthSock4Id < 0)
    {
        perror ("WssStaWebAuthSockInit: Unable to open stream socket");
        return OSIX_FAILURE;
    }
    sprintf (ifr.ifr_name, "%s", PORT_NAME);
    if (ioctl (i4WebAuthSock4Id, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("WssStaWebAuthSockInit: IOCTL Failed");
        close (i4WebAuthSock4Id);
        return OSIX_FAILURE;
    }
    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    if (bind (i4WebAuthSock4Id, (struct sockaddr *) &Enet, sizeof (Enet)) < 0)
    {
        perror ("Bind error\n");
        close (i4WebAuthSock4Id);
        return OSIX_FAILURE;
    }

    if ((i4Flags = fcntl (i4WebAuthSock4Id, F_GETFL, 0)) < 0)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Acquiring the flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Acquiring the flags for the socket failed !!!"));
        close (i4WebAuthSock4Id);
        return (OSIX_FAILURE);
    }

    i4Flags |= O_NONBLOCK;
    if (fcntl (i4WebAuthSock4Id, F_SETFL, i4Flags) < 0)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Set flags for the socket failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Set flags for the socket failed !!!"));
        close (i4WebAuthSock4Id);
        return (OSIX_FAILURE);
    }

    if (SelAddFd (i4WebAuthSock4Id, WssStaWebAuthPacketOnSocket) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "FD add Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId, "FD add Failed !!!"));
        close (i4WebAuthSock4Id);
        return OSIX_FAILURE;
    }
    WSSSTA_TRC1 (WSSSTA_MGMT_TRC,
                 "Printing socket ID = %d \r\n", i4WebAuthSock4Id);
    SYS_LOG_MSG ((SYSLOG_DEBUG_LEVEL, gu4StaSysLogId,
                  "Printing socket ID = %d \r\n", i4WebAuthSock4Id));
    WSSSTA_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaWebAuthRecvMemClear
 *
 * Description  : Deletes all the memory pools in WssSta
 *                                                                           
 * Input        : None
 *                                                                           
 * Output       : None
 *                                                                           
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                           
 *****************************************************************************/
VOID
WssStaWebAuthRecvMemClear (VOID)
{
    WssstawebSizingMemDeleteMemPools ();
    return;
}

/****************************************************************************
 * Function     : WssStaConfigTCPLiftRule
 * Description  : Main function to Configure the BSSID + port 80 lift rule
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaConfigTCPLiftRule (tMacAddr BssID, UINT2 u2RuleId)
{
#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule = NULL;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u1RuleType = WSSSTA_TCP_CPU_RULE;
    WlanClientParams.u2RuleId = u2RuleId;
    WlanClientParams.u2RuleNum = WSSSTA_TCP_LIFT_RULE_NUM;
    WlanClientParams.u1RulePrio = WSSSTA_TCP_LIFT_RULE_PRIO;
    /* Copy the BSSID */
    MEMCPY (WlanClientParams.BssId, BssID, sizeof (tMacAddr));

    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (BssID);
    UNUSED_PARAM (u2RuleId);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssStaConfigDefaultRule
 * Description  : Main function to Configure the default rules to drop the
 *                packets.
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaConfigDefaultRule (tMacAddr BssID)
{
#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule = NULL;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u1RuleType = WSSSTA_DEFAULT_DROP;
    WlanClientParams.u2RuleId = WSSSTA_DEFAULT_RULE_ID;
    WlanClientParams.u2RuleNum = WSSSTA_DEFAULT_RULE_NUM;
    WlanClientParams.u1RulePrio = WSSSTA_DEFAULT_RULE_PRIO;
    /* Copy the BSSID */
    MEMCPY (WlanClientParams.BssId, BssID, sizeof (tMacAddr));

    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (BssID);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssStaInitiatePool
 * Description  : Main function to Initiate the Web Auth Rule pool ID
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaInitiatePool (VOID)
{
#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule = NULL;

    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Web Auth Pool Initiate \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "Web Auth Pool Initiate !!!"));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u1RuleType = WSSSTA_INITIATE_POOL;
    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Web Auth Pool Initiate \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "Web Auth Pool Initiate !!!"));
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return OSIX_FAILURE;
    }
    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Web Auth Pool Initiated\r\n");
    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                  "Web Auth Pool Initiated!!!"));
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssStaConfigDNSFwdRule
 * Description  : Main function to Configure the DNS/DHCP rules
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaConfigDNSFwdRule (VOID)
{
#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u1RuleType = WSSSTA_DNS_FWD_RULE;
    WlanClientParams.u2RuleId = WSSSTA_DNS_FWD_RULE_ID;
    WlanClientParams.u2RuleNum = WSSSTA_DNS_FWD_RULE_NUM;
    WlanClientParams.u1RulePrio = WSSSTA_DNS_FWD_RULE_PRIO;

    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssStaConfigStaFwdRule
 * Description  : Main function to Configure the DNS/DHCP rules
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaConfigStaFwdRule (tMacAddr StaMacAddr, UINT1 *pu1Uname)
{
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    pWssifauthDBMsgStruct =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssifauthDBMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC,
                    "WssStaConfigStaFwdRule:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                      "WssStaConfigStaFwdRule:- "
                      "UtlShMemAllocWssIfAuthDbBuf returned failure !!!"));
        return OSIX_FAILURE;
    }
    if (pu1Uname == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaConfigStaFwdRule:- "
                    "Logged username is empty, returning failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaConfignnaFwdRule:- "
                      "Logged username is empty, returning failure !!!"));
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
            StaMacAddr, sizeof (tMacAddr));
    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                  pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConfigStaFwdRule:"
                    "Failed to get station DB details\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Failed to get station DB details !!!"));
    }

    pWssifauthDBMsgStruct->WssIfAuthStateDB.u4RuleNumber = u4StaRuleNumber;
    pWssifauthDBMsgStruct->WssIfAuthStateDB.u1StaAuthFinished = OSIX_TRUE;
    if (STRLEN (pu1Uname) > WSS_STA_WEBUSER_LEN_MAX)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaConfigStaFwdRule:- "
                    "Logged username is longer than limit, returning failure\n");
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }
    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.au1LoggedUserName,
            pu1Uname, STRLEN (pu1Uname));
    /* Update Station webauth lifespan as in user lifetime */
    pWssifauthDBMsgStruct->WssIfAuthStateDB.u4LastUpdatedTime =
        OsixGetSysUpTime ();

#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule = NULL;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u1RuleType = WSSSTA_STA_MAC_FWD_RULE;
    WlanClientParams.u2RuleId = u4StaRuleNumber++;
    WlanClientParams.u2RuleNum = WSSSTA_FWD_RULE_NUM;
    WlanClientParams.u1RulePrio = WSSSTA_FWD_RULE_PRIO;
    /* Copy the Sta Mac address */
    MEMCPY (WlanClientParams.StaMacAddr, StaMacAddr, sizeof (tMacAddr));

    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }
#endif

    if (CapwapSendWebAuthStatus (StaMacAddr,
                                 pWssifauthDBMsgStruct->WssIfAuthStateDB.
                                 u1StaAuthFinished,
                                 pWssifauthDBMsgStruct->WssIfAuthStateDB.
                                 aWssStaBSS[0].u2SessId) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConfigStaFwdRule:"
                    " WEB-AUTH Status processing failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaConfigStaFwdRule: WEB-AUTH Status processing failed!!1"));
    }

    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_DB_UPDATE_RULENUM,
                                  pWssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConfigStaFwdRule:"
                    " Station not found in the DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaConfigStaFwdRule: Station not found in the DB !!!"));
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }

    WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                         "WEB AUTH completed for STA %02x:%02x:%02x:%02x:%02x:%02x",
                         pWssifauthDBMsgStruct->WssIfAuthStateDB.
                         stationMacAddress[0],
                         pWssifauthDBMsgStruct->WssIfAuthStateDB.
                         stationMacAddress[1],
                         pWssifauthDBMsgStruct->WssIfAuthStateDB.
                         stationMacAddress[2],
                         pWssifauthDBMsgStruct->WssIfAuthStateDB.
                         stationMacAddress[3],
                         pWssifauthDBMsgStruct->WssIfAuthStateDB.
                         stationMacAddress[4],
                         pWssifauthDBMsgStruct->WssIfAuthStateDB.
                         stationMacAddress[5]);

#ifdef KERNEL_CAPWAP_WANTED
    /* Update the Kernel module */
    if (CapwapUpdateKernelStaTable (StaMacAddr) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConfigStaFwdRule:"
                    " Station not found in kernel DB \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaConfigStaFwdRule Station not found in kernel DB !!!"));
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        return OSIX_FAILURE;
    }
#endif

    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssStaConfigDHCPFwdRule
 * Description  : Main function to Configure the DNS/DHCP rules
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaConfigDHCPFwdRule (VOID)
{
#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u1RuleType = WSSSTA_DHCP_FWD_RULE;
    WlanClientParams.u2RuleId = WSSSTA_DHCP_FWD_RULE_ID;
    WlanClientParams.u2RuleNum = WSSSTA_DHCP_FWD_RULE_NUM;
    WlanClientParams.u1RulePrio = WSSSTA_DHCP_FWD_RULE_PRIO;

    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssStaDeleteClientRule
 * Description  : Function to delete the client rule.
 * Input        : None
 * Output       : None
 * Returns      : VOID
 *****************************************************************************/

INT4
WssStaDeleteClientRule (UINT4 u4RuleId, UINT4 u4PolicyListId)
{
#ifdef NPAPI_WANTED
    tWlanClientRuleParams WlanClientParams;
    tFsHwNp             FsHwNp;
    tWlanClientNpModInfo *pWlanClientNpModInfo = NULL;
    tWlanClientNpWrRule *pWlanClientNpWrRule = NULL;

    MEMSET (&WlanClientParams, 0, sizeof (tWlanClientParams));
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    /* Call NPAPI */
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_WLAN_CLIENT_MODULE,    /* Module ID */
                         WLAN_CLIENT_RULE_DEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);
    WlanClientParams.u2RuleId = u4RuleId;
    WlanClientParams.u2RuleNum = WSSSTA_DELETE_CLIENT_RULE_NUM;
    WlanClientParams.u4PolicyListId = u4PolicyListId;

    pWlanClientNpModInfo = &(FsHwNp.WlanClientNpModInfo);
    pWlanClientNpWrRule = &pWlanClientNpModInfo->WlanClientNpRuleAdd;

    pWlanClientNpWrRule->pWlanClientRuleParams = &WlanClientParams;
    /* Call to program the NP */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4RuleId);
    UNUSED_PARAM (u4PolicyListId);
#endif
    return OSIX_SUCCESS;
}

 /*****************************************************************************
  * Function     : DbgDumpWssStaPkt
  *
  * Description  : Function to dump the Packet
  *
  * Input        : Poinyter to the packet buffer, Packet flow direction
  *
  * Output       : None
  *
  * Returns      : OSIX_SUCCESS/OSIX_FAILURE
  *
  *****************************************************************************/
INT4
DbgDumpWssStaPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                  UINT1 u1PktDirection, UINT2 u2PktLen)
{

    UINT1               u1Temp;
    INT4                i4ReadOffset = 0;
    UINT2               u2TotalLen = 0, u2Len = u2PktLen;

    UNUSED_PARAM (u1PktDirection);

    while (u2Len--)
    {
        GET_1_BYTE (pBuf, i4ReadOffset, u1Temp);
        PRINTF ("%02x ", u1Temp);
        if (((u2TotalLen - u2Len) % 8) == 0)
        {
            PRINTF ("\n");
        }
        i4ReadOffset++;
    }
    PRINTF ("\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaProcessTCPPacket                                     *
 *                                                                           *
 * Description  : Receives the TCP packets destined to port 80 which are     *
 *                lifted to the CPU and a response packet is generated       *
 *                                                                           *
 *                                                                           *
 * Input        : pBuf - received TCP/IP packet                              *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssStaProcessTCPPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    if (OsixQueSend (gWssStaGlobals.WssStaMsgQId,
                     (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Enqueuing the Web Auth Packets Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Enqueuing the Web Auth Packets Failed !!!"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    if (OsixEvtSend (gWssStaGlobals.WssStaProcTaskId,
                     WSSSTA_RX_MSGQ_EVENT) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaEnqueue packets:Sending the event failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaEnqueue packets:Sending the event failed !!!"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaWebAuthRecvTCPPacket                                 *
 *                                                                           *
 * Description  : Receives the TCP packets destined to port 80 which are     *
 *                lifted to the CPU and a response packet is generated       *
 *                                                                           *
 *                                                                           *
 * Input        : pBuf - received TCP/IP packet                              *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
WssStaWebAuthRecvTCPPacket ()
{
    UINT1              *pu1RxBuf = NULL;
    UINT1               au1Frame[CPU_MAX_PKT_LEN];
    INT4                i4PktLen = 0;
    INT4                i4AddrLen = 0, i4Status;
    UINT1               u1IsNotLinear = OSIX_FALSE;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    struct sockaddr_ll  Enet;
    struct sockaddr    *pPort;
    struct ifreq        ifr;
    WSSSTA_FN_ENTRY ()if (pu1RxBuf == NULL)
    {
        MEMSET (au1Frame, 0x00, CPU_MAX_PKT_LEN);
        pu1RxBuf = au1Frame;
        u1IsNotLinear = OSIX_TRUE;
    }
    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));
    sprintf (ifr.ifr_name, "%s", PORT_NAME);
    if (ioctl (i4WebAuthSock4Id, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("WssStaWebAuthSockInit: IOCTL Failed");
        return OSIX_FAILURE;
    }
    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    pPort = (struct sockaddr *) &Enet;
    while ((i4PktLen = recvfrom (i4WebAuthSock4Id,
                                 pu1RxBuf,
                                 CPU_MAX_PKT_LEN,
                                 0, (struct sockaddr *) pPort, &i4AddrLen)))
    {
        if (i4PktLen < 0)
        {
            break;
        }
        if (u1IsNotLinear == OSIX_TRUE)
        {

            pBuf = WSSSTA_ALLOCATE_CRU_BUF ((UINT4) i4PktLen, 0);
            if (pBuf == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaWebAuthRecvTCPPacket:Memory "
                            "Allocation Failed \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                              "WssStaWebAuthRecvTCPPacket:Memory Allocation Failed !!!"));
                return OSIX_FAILURE;
            }
            /* If the CRU buffer memory is not linear */
            WSSSTA_COPY_TO_BUF (pBuf, pu1RxBuf, 0, (UINT4) i4PktLen);

        }
        if ((gu4WssStaDebugMask & WSSSTA_PKTDUMP_TRC) == WSSSTA_PKTDUMP_TRC)
        {
            DbgDumpWssStaPkt (pBuf, WSSSTA_PKT_FLOW_IN, (UINT2) i4PktLen);
        }
        i4Status = WssStaProcessTCPPacket (pBuf);

        if (i4Status == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaWebAuthRecvTCPPacket:Failed to process the Received \
                packet \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          " WssStaWebAuthRecvTCPPacket:Failed to process the Received packet !!!"));
            WSSSTA_RELEASE_CRU_BUF (pBuf);
            return OSIX_FAILURE;
        }
        MEMSET (pu1RxBuf, 0, CPU_MAX_PKT_LEN);
    }
    WSSSTA_FN_EXIT ()return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 * Function     : WssStaWebAuthRcvMainTaskInit
 *
 * Description  : Main receiver function of Station Web Authentication task 
 *                routine.
 *
 * Input        : None
 *                                                                           
 * Output       : None
 *                                                                           
 * Returns      : OSIX_SUCCESS, if initialization succeeds
 *                OSIX_FAILURE, otherwise
 *                                                                           
 *****************************************************************************/
INT4
WssStaWebAuthRecvMainTaskInit (VOID)
{
    UINT4               u4Event = 0;
    tWssifauthDBMsgStruct *pWssStaAuthMsg = NULL;

    /* Create buffer pools for data structures  and packet to be constructed */
    if (WssStaWebAuthRecvMemInit () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Memory Pool Creation Failed\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Memory Pool Creation Failed !!!"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    if (OsixTskIdSelf (&gWssStaGlobals.WssStaRecvTaskId) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Wss STA Web Auth Recv Init Task Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Wss STA Web Auth Recv Init Task Failure !!!"));
        WssStaWebAuthRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    pWssStaAuthMsg =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssStaAuthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC,
                    "WssStaWebAuthRecvMainTaskInit:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthRecvMainTaskInit UtlShMemAllocWssIfAuthDbBuf returned failure!!!"));
        return OSIX_FAILURE;
    }
    MEMSET (pWssStaAuthMsg, 0, sizeof (tWssifauthDBMsgStruct));
    /* Create the Web Auth RB Tree */
    if (WssIfProcessWssAuthDBMsg (WSS_STA_WEBAUTH_INIT_DB, pWssStaAuthMsg) ==
        OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS STA Web Auth \
                                        DB Initialization Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Wss STA Web Auth DB Initialization Failed!!"));
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaAuthMsg);
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaAuthMsg);

    /* Initiate pool -TBD */
    if (WssStaInitiatePool () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS STA Web Auth \
                                   Rule Pool creation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Wss STA Web Auth  Rule Pool creation Failed !!!"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;

    }
    /* Configure the DNS rule */
    if (WssStaConfigDNSFwdRule () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS STA Web Auth \
                                   Rule addition Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Wss STA Web Auth Rule addition Failed !!!"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    /* Configure the DHCP rule */
    if (WssStaConfigDHCPFwdRule () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS STA Web Auth \
                                   Rule addition Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Wss STA Web Auth Rule addition Failed !!!"));
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    /* TCP drop rule -TBD */

    gWssStaGlobals.u1IsStaWebAuthInitialized = OSIX_TRUE;
    /* gu4WssStaDebugMask = WSSSTA_PKTDUMP_TRC; */
    WSSSTA_TRC (WSSSTA_INIT_TRC,
                "WssStaWebAuthRecvMainTaskInit: Receiver Task Init "
                "Completed \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "WssStaWebAuthRecvMainTaskInit: Receiver Task Init Completed!!!"));
    lrInitComplete (OSIX_SUCCESS);
    while (1)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC, "Receiver task waiting for the "
                    "event \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                      "Receiver task waiting for the event !!!"));
        OsixEvtRecv (gWssStaGlobals.WssStaRecvTaskId,
                     WSSSTA_TCPDATA_PACKET_ON_CPU, OSIX_WAIT, &u4Event);
        if (u4Event & WSSSTA_TCPDATA_PACKET_ON_CPU)
        {
            WssStaWebAuthRecvTCPPacket ();
            if (i4WebAuthSock4Id != -1)
            {
                if ((SelAddFd (i4WebAuthSock4Id, WssStaWebAuthPacketOnSocket))
                    != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "FD add Failed \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                  "FD add Failed !!!"));
                }
            }

        }

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                           *
 * Function     : WssStaWebAuthProcessTask                                   *
 *                                                                           *
 * Description  : Main function to process WSSSTA CPU packets                *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : VOID                                                       *
 *                                                                           *
 *****************************************************************************/

INT4
WssStaWebAuthProcessTask (VOID)
{
    UINT4               u4Event = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    WSSSTA_FN_ENTRY ();

    if (OsixTskIdSelf (&gWssStaGlobals.WssStaProcTaskId) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Wss STA Web Auth Process Task Failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Wss STA Web Auth Process Task Failure !!!"));
        WssStaWebAuthRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Wss STA Web Auth Process 1 \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "Wss STA Web Auth Process 1 !!!"));

    if (OsixQueCrt (WSSSTA_WEBAUTH_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    WSSSTA_MAX_QUEUE_DEPTH,
                    &(gWssStaGlobals.WssStaMsgQId)) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WSSSTA: Web Auth Queue Creation failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WSSSTA: Web Auth Queue Creation failed !!!"));
        WssStaWebAuthRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }
    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Wss STA Web Auth Process 2 \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "Wss STA Web Auth Process 2 !!!"));

    /* Initiate the Web Auth timer */
    if (WssWebAuthTmrInit () != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSS STA Web Auth \
                  Timer Initialization Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WSS STA Web Auth Timer Initialization Failed "));
        WssStaWebAuthRecvMemClear ();
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;

    }
    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Wss STA Web Auth Process 3 \r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                  "Wss STA Web Auth Process 3"));

    gWssStaGlobals.u1IsStaWebAuthProcInitialized = OSIX_TRUE;
    lrInitComplete (OSIX_SUCCESS);
    while (1)
    {
        if (OsixReceiveEvent (WSSSTA_RX_MSGQ_EVENT,
                              OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {
            if ((u4Event & WSSSTA_RX_MSGQ_EVENT) == WSSSTA_RX_MSGQ_EVENT)
            {
                while (OsixQueRecv (gWssStaGlobals.WssStaMsgQId,
                                    (UINT1 *) (&pBuf),
                                    OSIX_DEF_MSG_LEN,
                                    OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    if (WssStaWebAuthProcAndRespond ((tSegment *) pBuf) ==
                        OSIX_FAILURE)
                    {
                        WSSSTA_TRC (WSSSTA_SESS_TRC,
                                    "Failed to process the received packet "
                                    "lifted to CPU \n");
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                      "Failed to process the received packet lifted to CPU "));
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                    }
                    else
                    {
                        /* IND: Adding success scenario */
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    }
                }
            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssWebAuthTmrInit
 * Description  :  This function initializes the timer for Web Auth login
 *                          in WSSSTA module.
 * Input        : None
 * Output       : None
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssWebAuthTmrInit (VOID)
{
    if (TmrCreateTimerList ((CONST UINT1 *) WSSSTA_WEBAUTH_PROC_TASK_NAME,
                            WSSSTA_WEBAUTH_TMR_EXP_EVENT,
                            NULL,
                            (tTimerListId *)
                            & (gWssStaWebAuthTmrList.WssStaWebAuthTmrListId)) ==
        TMR_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssWebAuthTmrInit: Failed to creat "
                    "the Application Timer List");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssWebAuthTmrInit: Failed to creat the Application Timer List"));
        return OSIX_FAILURE;
    }
    WssWebAuthTmrInitTmrDesc ();

    /* Start the Timer that expires in 1 minute */
    WssStaWebAuthStartTimer (WSSSTA_WEBAUTH_TMR, WSSSTA_WEBAUTH_TIMEOUT);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaWebAuthStartTimer
 * Description  : This function used to start the Web Authentication timer.
 * Input        :
 *            u4TmrInterval - Time interval for which timer must run
 *               (in seconds)
 * Output       : None
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssStaWebAuthStartTimer (UINT1 u1TmrType, UINT4 u4TmrInterval)
{
    if (TmrStart (gWssStaWebAuthTmrList.WssStaWebAuthTmrListId,
                  &WssStaWebAuthTmr,
                  u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
    {
        WSSSTA_TRC1 (WSSSTA_FAILURE_TRC,
                     "WssStaWebAuthStartTimer:Failed to start the Timer Type = %d \r\n",
                     u1TmrType);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthStartTimer:Failed to start the Timer Type = %d \r\n",
                      u1TmrType));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaWebAuthTmrExp
 * Description  :  This function is called whenever a process DB timer
 *                 expires during Authentication
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
VOID
WssStaWebAuthTmrExp (VOID *pArg)
{
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaWebAuthTmrExp:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthTmrExp:- "
                      "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
        return;
    }
    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
    if (WssIfProcessWssAuthDBMsg (WSS_STA_WEB_AUTH_DB_WALK, pwssStaDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return;
    }
    /* Start the timer again */
    WssStaWebAuthStartTimer (WSSSTA_WEBAUTH_TMR, WSSSTA_WEBAUTH_TIMEOUT);
    UNUSED_PARAM (pArg);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    return;
}

/*****************************************************************************
 * Function     : WssStaWebAuthGetUser
 * Description  :  This function is called to check if the username is available 
 *                in Web Auth DB before starting the FPAM validation.
 * Input        : None
 * Output       : None
 * Returns      : None
 ****************************************************************************/
INT4
WssStaWebAuthGetUser (tSNMP_OCTET_STRING_TYPE * pu1UName)
{
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaWebAuthGetUser:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthGetUser:- "
                      "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
    MEMCPY (pwssStaDB->WssStaWebAuthDB.au1UserName, pu1UName->pu1_OctetList,
            pu1UName->i4_Length);
    if (WssIfProcessWssAuthDBMsg (WSS_STA_WEBAUTH_GET_USER, pwssStaDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaWebAuthDbUpdate
 * Description  : This function is called whenever a Username is mapped to a
 *                WlanId and Userlifetime.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/

INT4
WssStaWebAuthDbUpdate (tWssIfIsSetWebAuthDB * pWssStaWebAuthIsSetDB,
                       tWssIfWebAuthDB * pWssStaWebAuthDB)
{
    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaWebAuthDbUpdate:- "
                    "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthDbUpdate:- "
                      "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
    if (pWssStaWebAuthIsSetDB->bUserName == OSIX_TRUE)
    {
        MEMCPY (pwssStaDB->WssStaWebAuthDB.au1UserName,
                pWssStaWebAuthDB->au1UserName,
                STRLEN (pWssStaWebAuthDB->au1UserName));
    }
    if (pWssStaWebAuthIsSetDB->bWlanProfileId == OSIX_TRUE)
    {
        pwssStaDB->WssStaWebAuthDB.u4WlanId = pWssStaWebAuthDB->u4WlanId;
    }
    if (pWssStaWebAuthIsSetDB->bMaxUserLifetime == OSIX_TRUE)
    {
        pwssStaDB->WssStaWebAuthDB.u4MaxUserLifetime =
            pWssStaWebAuthDB->u4MaxUserLifetime;
    }
    if (pWssStaWebAuthIsSetDB->bUserEmailId == OSIX_TRUE)
    {
        MEMCPY (pwssStaDB->WssStaWebAuthDB.au1UserEmailId,
                pWssStaWebAuthDB->au1UserEmailId,
                STRLEN (pWssStaWebAuthDB->au1UserEmailId));
    }
    if (pWssStaWebAuthIsSetDB->bUserPwd == OSIX_TRUE)
    {
        MEMCPY (pwssStaDB->WssStaWebAuthDB.au1UserPwd,
                pWssStaWebAuthDB->au1UserPwd, WSS_STA_WEBUSER_LEN_MAX);
    }
    if (pWssStaWebAuthIsSetDB->bRowStatus == OSIX_TRUE)
    {
        pwssStaDB->WssStaWebAuthDB.u1RowStatus = pWssStaWebAuthDB->u1RowStatus;
        if (pWssStaWebAuthDB->u1RowStatus == DESTROY)
        {
            if (WssStaUpdateWebAuthDB (WSS_STA_WEBAUTH_DELETE_DB,
                                       pWssStaWebAuthDB) != OSIX_SUCCESS)
            {
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                return OSIX_FAILURE;
            }
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            return OSIX_SUCCESS;
        }
    }
    if (WssStaWebAuthTreeAdd (pWssStaWebAuthIsSetDB,
                              pWssStaWebAuthDB) != OSIX_SUCCESS)

    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaWebAuthUsrAuthenticated
 * Description  :  This function is called whenever a User gets authenticated.
 *               The function will update the user's state to authenticated in 
 *               the internal DB and invoke the NP API to configure the ACL rule
 *               allowing the station traffic to pass through.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
INT4
WssStaWebAuthUsrAuthenticated (UINT1 *pu1Uname, UINT1 *pu1UsrPwd,
                               tMacAddr CliMacAddr, UINT1 u1CheckFlag)
{

    tWssifauthDBMsgStruct *pwssStaDB = NULL;
    tWssifauthDBMsgStruct *pwssStationDB = NULL;
    tWssWlanDB          wssWlanDB;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4WlanID = 0;
    /* only when u1CheckFlag is set , we need to chack in station db */
    if (u1CheckFlag == OSIX_TRUE)
    {
        if (pu1UsrPwd == NULL || STRLEN (pu1UsrPwd) > WSS_STA_WEBUSER_LEN_MAX)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaWebAuthUsrAuthenticated:- "
                        "Password is invalid failure\n");
            return OSIX_FAILURE;
        }
        if (pu1Uname == NULL || STRLEN (pu1Uname) > WSS_STA_WEBUSER_LEN_MAX)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaWebAuthUsrAuthenticated:- "
                        "Username is invalid failure\n");
            return OSIX_FAILURE;
        }

        pwssStaDB =
            (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
        if (pwssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaWebAuthUsrAuthenticated:- "
                        "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          "WssStaWebAuthUsrAuthenticated:- "
                          "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
            return OSIX_FAILURE;
        }
        pwssStationDB =
            (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
        if (pwssStationDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaWebAuthUsrAuthenticated:- "
                        "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          "WssStaWebAuthUsrAuthenticated:- "
                          "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            return OSIX_FAILURE;
        }
        MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
        MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
        MEMSET (pwssStationDB, 0, sizeof (tWssifauthDBMsgStruct));
        MEMCPY (pwssStaDB->WssIfAuthStateDB.stationMacAddress, CliMacAddr,
                sizeof (tMacAddr));
        MEMCPY (pwssStationDB->WssIfAuthStateDB.stationMacAddress, CliMacAddr,
                sizeof (tMacAddr));
        if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB, pwssStaDB) !=
            OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaWebAuthUsrAuthenticated:"
                        " Station not found in the DB \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          "WssStaWebAuthUsrAuthenticated Station not found in the DB \r\n"));
        }
        /* Obtained BSS if index */
        u4BssIfIndex = pwssStaDB->WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex;

        /* Get the Inernal Wlan Id from the BSS If Index */
        wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
        wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssWlanProcessWssWlanDBMsg: \r\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          "WssWlanProcessWssWlanDBMsg: \r\n"));
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStationDB);
            return OSIX_FAILURE;
        }
        u4WlanID = (UINT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
        MEMCPY (pwssStationDB->WssStaWebAuthDB.au1UserName, pu1Uname,
                STRLEN (pu1Uname));
        MEMCPY (pwssStationDB->WssStaWebAuthDB.au1UserPwd, pu1UsrPwd,
                STRLEN (pu1UsrPwd));
        pwssStationDB->WssStaWebAuthDB.u4WlanId = u4WlanID;
        if (WssIfProcessWssAuthDBMsg (WSS_STA_WEBAUTH_AUTHENTICATE_USER,
                                      pwssStationDB) != OSIX_SUCCESS)
        {
            pwssStationDB->WssIfAuthStateDB.u1StaAuthRedirected = OSIX_FALSE;
            pwssStationDB->WssIfAuthStateDB.bStaAuthRedirected = OSIX_TRUE;

            if (WssIfProcessWssAuthDBMsg (WSS_AUTH_DB_UPDATE_RULENUM,
                                          pwssStationDB) != OSIX_SUCCESS)
            {

                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStationDB);
                return OSIX_FAILURE;
            }
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStationDB);
            return OSIX_FAILURE;
        }
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStationDB);
    }
    /* Add the NP API */
    WssStaConfigStaFwdRule (CliMacAddr, pu1Uname);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssWebAuthTmrInitTmrDesc
 * Description  : This function intializes the timer desc for all
 *                the timers in WSSSTA module.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
VOID
WssWebAuthTmrInitTmrDesc (VOID)
{
    /* Web Auth Timers */
    gWssStaWebAuthTmrList.aWssStaWebAuthTmrDesc[WSSSTA_WEBAUTH_TMR].TmrExpFn =
        WssStaWebAuthTmrExp;
    gWssStaWebAuthTmrList.aWssStaWebAuthTmrDesc[WSSSTA_WEBAUTH_TMR].i2Offset =
        -1;
    return;
}

/****************************************************************
*  Function Name   : WssStaHttpGetDate                         *
*  Description     : This function is used to get the date     *
*                                                              *
*  Input(s)        :                                           *
*  Output(s)       : None                                      *
*  Returns         : SUCCESS/FAILURE                           *
****************************************************************/
INT4
WssStaHttpGetDate (UINT1 *au1FullDate, INT1 i1FutureFlag)
{
    tUtlTm              tm;
    UINT1               au1Hrs[3], au1Min[3], au1Sec[3], au1Day[3], au1Mon[3],
        au1Year[5], au1Month[4], au1Wday[3], au1WeekDay[5];
    UINT4               u4Secs = 0;
    UINT4               u4Ticks = 0;

    MEMSET (&tm, WSSSTA_INIT_VAL, sizeof (tUtlTm));
    MEMSET (au1FullDate, WSSSTA_INIT_VAL, WSSSTA_MAX_DATE_LEN);
    MEMSET (au1Hrs, WSSSTA_INIT_VAL, 3);
    MEMSET (au1Min, WSSSTA_INIT_VAL, 3);
    MEMSET (au1Sec, WSSSTA_INIT_VAL, 3);
    MEMSET (au1Day, WSSSTA_INIT_VAL, 3);
    MEMSET (au1Mon, WSSSTA_INIT_VAL, 3);
    MEMSET (au1Year, WSSSTA_INIT_VAL, 5);
    MEMSET (au1Month, WSSSTA_INIT_VAL, 4);
    MEMSET (au1Wday, WSSSTA_INIT_VAL, 3);
    MEMSET (au1WeekDay, WSSSTA_INIT_VAL, sizeof (au1WeekDay));

    u4Secs = UtlGetTimeInSecs ();
    if (i1FutureFlag == TRUE)
    {
        u4Secs += 7200;
    }
    u4Ticks = u4Secs * gu4Stups;
    UtlGetTimeForTicks (u4Ticks, &tm);
    tm.tm_mon++;
    tm.tm_wday++;
    SNPRINTF ((CHR1 *) au1Wday, 3, "0%u", tm.tm_wday);

    if (tm.tm_mon <= 9)
    {
        SNPRINTF ((CHR1 *) au1Mon, 3, "0%u", tm.tm_mon);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Mon, 3, "%u", tm.tm_mon);
    }
    if (tm.tm_mday <= 9)
    {
        SNPRINTF ((CHR1 *) au1Day, 3, "0%u", tm.tm_mday);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Day, 3, "%u", tm.tm_mday);
    }
    if (tm.tm_hour <= 9)
    {
        SNPRINTF ((CHR1 *) au1Hrs, 3, "0%u", tm.tm_hour);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Hrs, 3, "%u", tm.tm_hour);
    }

    if (tm.tm_min <= 9)
    {
        SNPRINTF ((CHR1 *) au1Min, 3, "0%u", tm.tm_min);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Min, 3, "%u", tm.tm_min);
    }

    if (tm.tm_sec <= 9)
    {
        SNPRINTF ((CHR1 *) au1Sec, 3, "0%u", tm.tm_sec);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1Sec, 3, "%u", tm.tm_sec);
    }
    SNPRINTF ((CHR1 *) au1Year, 5, "%u", tm.tm_year);
    switch (tm.tm_mon)
    {
        case 1:
            STRCPY (au1Month, "Jan");
            break;
        case 2:
            STRCPY (au1Month, "Feb");
            break;
        case 3:
            STRCPY (au1Month, "Mar");
            break;
        case 4:
            STRCPY (au1Month, "Apr");
            break;
        case 5:
            STRCPY (au1Month, "May");
            break;
        case 6:
            STRCPY (au1Month, "Jun");
            break;
        case 7:
            STRCPY (au1Month, "Jul");
            break;
        case 8:
            STRCPY (au1Month, "Aug");
            break;
        case 9:
            STRCPY (au1Month, "Sep");
            break;
        case 10:
            STRCPY (au1Month, "Oct");
            break;
        case 11:
            STRCPY (au1Month, "Nov");
            break;
        case 12:
            STRCPY (au1Month, "Dec");
            break;

        default:
            break;
    }
    switch (tm.tm_wday)
    {
        case 1:
            STRCPY (au1WeekDay, "Sun,");
            break;
        case 2:
            STRCPY (au1WeekDay, "Mon,");
            break;
        case 3:
            STRCPY (au1WeekDay, "Tue,");
            break;
        case 4:
            STRCPY (au1WeekDay, "Wed,");
            break;
        case 5:
            STRCPY (au1WeekDay, "Thu,");
            break;
        case 6:
            STRCPY (au1WeekDay, "Fri,");
            break;
        case 7:
            STRCPY (au1WeekDay, "Sat,");
            break;
        default:
            break;
    }

    SPRINTF ((CHR1 *) au1FullDate, "%s %s %s %s %s:%s:%s",
             au1WeekDay, au1Day, au1Month, au1Year, au1Hrs, au1Min, au1Sec);
    return (OSIX_SUCCESS);
}

/*****************************************************************************
 *
 * p
 * Function     : WssStaAssembleWebRespPkts
 *
 * Description  : Function to assemble the Web Authentication response pkts.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS, if initialization succeeds
 *                OSIX_FAILURE, otherwise
 *
 *****************************************************************************/
INT4
WssStaAssembleWebRespPkts (tDot3EthFrmHdr * pDot3Hdr,
                           t_IP_HEADER * pIpHdr,
                           tWssStaTcpHdr * pTcpHdr,
                           UINT1 u1RespType, UINT1 u1TcpHdrLen, UINT1 u1TcpFlag)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tIpAddr             IpAddr;
    UINT2               u2TcpLenFlags = 0;
    UINT4               u4Len = 0, u4Length = 0, u4CalcLength = 0;
    INT4                i4TcpAckNum = 0, i4TcpSeqNum = 0, i4TcpSynAckSeqNum =
        0x00000001;
    UINT1               u1WebAuthType = INTERNAL_WEBAUTH;
    UINT2               u2Checksum = 0, u2IptotLen = 0, u2TcpPaylen = 0;
    tCRU_BUF_CHAIN_HEADER *pu1TxBuf = NULL;
    UINT1              *pTxBuf = NULL;
    UINT1              *pTx1Buf = NULL;
    UINT1              *pTx2Buf = NULL;
    UINT1              *pTxHead = NULL;
    UINT1              *pTxStart = NULL;
    UINT1               au1Date[WSSSTA_MAX_DATE_LEN];
    INT4                i = 0;
    UINT2               u2CkSum = 0;
    tOsixSysTime        CurrentTime = 0;
    UINT1               u1IsNotLinear1 = OSIX_FALSE;
    CHR1               *pcByte;
    UINT4               u4IfIndex = 0;
    UINT1              *pu1IfName = NULL;
#ifdef LNXIP4_WANTED
    UINT1              *pu1PortName = NULL;
#endif
    UINT1               u1Status = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT2               u2VlanId = 0;
    UINT2               u2Port = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4HttpPort = 0;
    INT4                i4IfType = 0;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssWlanDB         *pwssWlanDB = NULL;
#ifdef WSS_DEBUG_WANTED

    UINT1               u1TcpOptionLen = 0;
#endif
    UINT1               au1Tag[12] = { 0 };
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    UINT4               u4ApGroupEnabled = 0;
    UINT4               u4BssIfIndex = 0;
    tWssWlanDB         *pwssApGroupWlanDB = NULL;

    /*UNUSED_PARAM (u1TcpOptionLen); */
    WSSSTA_FN_ENTRY ();
    MEMSET (au1Date, WSSSTA_INIT_VAL, WSSSTA_MAX_DATE_LEN);
/* debug */

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pu1TxBuf = WSSSTA_ALLOCATE_CRU_BUF (CPU_MAX_PKT_LEN, 0);
    if (pu1TxBuf == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaAssembleWebRespPkts:Memory "
                    "Allocation Failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaAssembleWebRespPkts:Memory Allocation Failed \r\n"));

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pTxBuf = WSSSTA_BUF_IF_LINEAR (pu1TxBuf, 0, CPU_MAX_PKT_LEN);
    if (pTxBuf == NULL)
    {
        pTxBuf = (UINT1 *) MemAllocMemBlk (WSSSTA_PKTBUF_POOLID);
        if (pTxBuf == NULL)
        {
            /* Kloc Fix Start */
            WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            /* Kloc Fix Ends */
            return OSIX_FAILURE;
        }
        WSSSTA_COPY_FROM_BUF (pu1TxBuf, pTxBuf, 0, CPU_MAX_PKT_LEN);
        u1IsNotLinear1 = OSIX_TRUE;
    }
    u2IpIdentifier = (UINT2) (u2IpIdentifier + 1);
    pTx1Buf = pTxBuf;
    pTx2Buf = pTxBuf;
    pTxHead = pTxBuf;
    pTxStart = pTxBuf;
    /* Copy the 802.3 Header */
    /* Swap the source and destination Mac */
    WSSSTA_PUT_NBYTES (pTxBuf, pDot3Hdr->DstMacAddr, sizeof (tMacAddr));
    WSSSTA_PUT_NBYTES (pTxBuf, pDot3Hdr->SrcMacAddr, sizeof (tMacAddr));
    WSSSTA_PUT_2BYTES (pTxBuf, (pDot3Hdr->u2EthType));
    /**********************/
    /* Copy the IP header */
    /**********************/
    WSSSTA_PUT_1BYTE (pTxBuf, pIpHdr->u1Ver_hdrlen);
    WSSSTA_PUT_1BYTE (pTxBuf, pIpHdr->u1Tos);
    WSSSTA_PUT_2BYTES (pTxBuf, u2IptotLen);
    WSSSTA_PUT_2BYTES (pTxBuf, (u2IpIdentifier));
    WSSSTA_PUT_2BYTES (pTxBuf, (pIpHdr->u2Fl_offs));
    WSSSTA_PUT_1BYTE (pTxBuf, pIpHdr->u1Ttl);
    WSSSTA_PUT_1BYTE (pTxBuf, pIpHdr->u1Proto);
    WSSSTA_PUT_2BYTES (pTxBuf, ZERO_BYTES);
    WSSSTA_PUT_4BYTES (pTxBuf, (pIpHdr->u4Dest));
    WSSSTA_PUT_4BYTES (pTxBuf, (pIpHdr->u4Src));

    /***********************/
    /* Copy the TCP header */
    /***********************/
    /* Swap the Src port and the Dest port */
    WSSSTA_PUT_2BYTES (pTxBuf, (pTcpHdr->u2Dport));
    WSSSTA_PUT_2BYTES (pTxBuf, (pTcpHdr->u2Sport));
    /* Copying the Ack no to the sequence number */

    WSSSTA_WEBAUTH_TRC2 (WSSSTAWEBAUTH_MASK, "Req for SRC %x DST %x\n",
                         pIpHdr->u4Src, pIpHdr->u4Dest);

    if (u1RespType == WSSSTA_TCP_FIN_ACK)
    {
        i4TcpSeqNum = (pTcpHdr->i4Ack) + gWebAuthNextAck;
        WSSSTA_PUT_4BYTES (pTxBuf, (UINT4) i4TcpSeqNum);
    }
    else if (u1RespType == WSSSTA_TCP_SYNACK)
    {
        WSSSTA_PUT_4BYTES (pTxBuf, (UINT4) i4TcpSynAckSeqNum);
    }
    else
    {
        WSSSTA_PUT_4BYTES (pTxBuf, (UINT4) (pTcpHdr->i4Ack));
    }
    u2TcpPaylen = (UINT2) ((pIpHdr->u2Totlen) - (IP_HDR_LEN + u1TcpHdrLen));
    /* For HTTPGET ACK or HTTP response */
    if (u2TcpPaylen == 0)
    {
        i4TcpAckNum = ((pTcpHdr->i4Seq) + 1);
    }
    else
    {
        i4TcpAckNum = ((pTcpHdr->i4Seq) + u2TcpPaylen);
    }
    WSSSTA_PUT_4BYTES (pTxBuf, (UINT4) i4TcpAckNum);
    /* Copy the TCP Header len and flags */
    u2TcpLenFlags = (UINT2) ((((u1TcpHdrLen / 4) << 12)) | u1TcpFlag);
    WSSSTA_PUT_2BYTES (pTxBuf, u2TcpLenFlags);
    /* Copy the Window size from the received packet */
    WSSSTA_PUT_2BYTES (pTxBuf, OSIX_HTONS (pTcpHdr->u2Window));
    /* Put zero bytes in checksum field */
    WSSSTA_PUT_2BYTES (pTxBuf, ZERO_BYTES);

    WSSSTA_PUT_2BYTES (pTxBuf, OSIX_HTONS (pTcpHdr->u2Urgptr));
    /* Copy the received options field into the sending packet */
    /* TBD - Hardcoded for now */
    /* Considering the option length is 20 */
    /* blindly copy the first 8 bytes till the timestamp kind and length */
#ifdef WSS_DEBUG_WANTED

    u1TcpOptionLen = u1TcpHdrLen - TCP_HDR_LEN_WITHOUT_OPTION;
#endif
    if (u1TcpHdrLen > IP_HDR_LEN)
    {
        OsixGetSysTime (&CurrentTime);
        if ((u1RespType == WSSSTA_HTTPRESP) || (u1RespType == WSSSTA_TCP_ACK) ||
            (u1RespType == WSSSTA_TCP_HTTPGET_ACK) || (u1RespType ==
                                                       WSSSTA_TCP_FIN_ACK)
            || (u1RespType == WSSSTA_HTTPPOST_RESP))

        {
            for (i = 0; i < 4; i++)
            {
                WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[i]);
            }
            for (i = 8; i < 12; i++)
            {
                WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[i]);
            }
            for (i = 4; i < 8; i++)
            {
                WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[i]);
            }
        }
        else
        {
#ifdef WSS_DEBUG_WANTED
            /* For Syn ACK packets */
            u1TcpOffset = 0;
            while (u1TcpOptionLen > 0)
            {
                if (pTcpHdr->au1TcpOpts[u1TcpOffset] == TCP_OPT_NOP)
                {
                    WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[u1TcpOffset]);
                    u1TcpOffset++;
                    u1TcpOptionLen = u1TcpOptionLen - 1;
                }
                else if (pTcpHdr->au1TcpOpts[u1TcpOffset] == TCP_OPT_TIMESTAMP)
                {
                    WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[u1TcpOffset]);
                    u1TcpOffset++;
                    WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[u1TcpOffset]);
                    u1TcpOffset++;

                    WSSSTA_PUT_NBYTES (pTxBuf, CurrentTime, TCP_TIMESTAMP_SIZE);

                    for (u4Index = 0; u4Index < TCP_TIMESTAMP_SIZE; u4Index++)
                    {
                        WSSSTA_PUT_1BYTE (pTxBuf,
                                          pTcpHdr->au1TcpOpts[u1TcpOffset]);
                        u1TcpOffset++;
                    }
                    u1TcpOffset = u1TcpOffset + TCP_TIMESTAMP_SIZE;
                    u1TcpOptionLen =
                        u1TcpOptionLen - (TCP_TIMESTAMP_SIZE +
                                          TCP_TIMESTAMP_SIZE + 2);
                }
                else
                {
                    u2TempLen = (UINT2) pTcpHdr->au1TcpOpts[u1TcpOffset + 1];
                    for (u4Index = 0; u4Index < u2TempLen; u4Index++)
                    {
                        WSSSTA_PUT_1BYTE (pTxBuf,
                                          pTcpHdr->au1TcpOpts[u1TcpOffset]);
                        u1TcpOffset++;
                    }
                    u1TcpOptionLen = u1TcpOptionLen - u2TempLen;
                }

            }
#endif
            for (i = 0; i < 8; i++)
            {
                /*WSSSTA_PUT_1BYTE (pTxBuf, OSIX_HTONS (pTcpHdr->au1TcpOpts[i])); 
                 * Removed the OSIX_HTONS conversion as per static code analysis report
                 * since this expression has no effect on one byte variable.
                 * TBD - Need to take care of the above statement while handling 
                 * the Sync ACK packets*/

                WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[i]);
            }

            WSSSTA_PUT_NBYTES (pTxBuf, CurrentTime, sizeof (UINT4));

            for (i = 8; i < 12; i++)
            {
                WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[i]);
            }

            for (i = 16; i < 20; i++)
            {
                WSSSTA_PUT_1BYTE (pTxBuf, pTcpHdr->au1TcpOpts[i]);
            }

        }

    }
    /* Add the 802.3 Hdr(14) + IP4 hdr(20) + TCP Hdr(with options) */
    u4CalcLength += (UINT4) (WSSSTA_ETH_IP_HDR_LEN + u1TcpHdrLen);
    /***********************************************************
      HTTP/1.0 200 OK\r\n
      Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n
      Content-Type: text/html\r\n
      Content-Length: 1354\r\n
     ***********************************************************/
    /* For Internal Web Auth the HTML body would be as follows */
     /**********************************************************
       <HTML>
       <HEAD>
       <TITLE> Web Authentication redirect </TITLE>
       <META http-equiv="Cache-control" content="no-cache"> 
      <META http-equiv="refresh" content="1;URL=<https://<local IP address>\
      /login.html/">
       </HEAD> 
       </HTML>
    ***********************************************************/
    /* For External Web Auth the HTML body would be as follows */
     /**********************************************************
       <HTML>
       <HEAD>
       <TITLE> Web Authentication redirect </TITLE>
       <META http-equiv="Cache-control" content="no-cache">
      <META http-equiv="refresh" content="1;URL=<https://172.31.140.140/\
      login.html?\
      wlc_ip=<WLC's IP Addr>&station_id=<Station's MAC address>\
      &redirect_url=<URL client requested for>\
       &ap_mac=<BSSID>&wlan=<wlan_ssid>/">
       </HEAD>
       </HTML>
       ***********************************************************/
    /* Construct the HTTP redirection if needed */
    gWebAuthNextAck = (UINT1) u4CalcLength;

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
        if (u1IsNotLinear1 == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    pWssStaWepProcessDB = WssStaProcessEntryGet (pDot3Hdr->DstMacAddr);
    if (pWssStaWepProcessDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
        if (u1IsNotLinear1 == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB)
        == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
        if (u1IsNotLinear1 == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    pwssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bFsDot11RedirectFileName = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bExternalWebAuthUrl = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
        if (u1IsNotLinear1 == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    u4BssIfIndex = pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
    WsscfgGetApGroupEnable (&u4ApGroupEnabled);
    if (u4ApGroupEnabled == APGROUP_ENABLE)
    {
        pwssApGroupWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pwssApGroupWlanDB == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
            if (u1IsNotLinear1 == OSIX_TRUE)
            {
                MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxBuf);
            }
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        MEMSET (pwssApGroupWlanDB, 0, sizeof (tWssWlanDB));
        pwssApGroupWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
        pwssApGroupWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssApGroupWlanDB) == OSIX_FAILURE)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
            if (u1IsNotLinear1 == OSIX_TRUE)
            {
                MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxBuf);
            }
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssApGroupWlanDB);
            return OSIX_FAILURE;
        }
        u2VlanId = pwssApGroupWlanDB->WssWlanAttributeDB.u2VlanId;
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssApGroupWlanDB);
    }
    else
    {
        u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId;
    }

    if (u1RespType == WSSSTA_HTTPRESP)
    {
        u4Len = STRLEN (HTTP_VER_STRING);
        WSSSTA_PUT_NBYTES (pTxBuf, HTTP_VER_STRING, u4Len);
        u4CalcLength += u4Len;    /* Add to the total message len */

        WssStaHttpGetDate (au1Date, FALSE);
        u4Len = STRLEN (au1Date);
        WSSSTA_PUT_NBYTES (pTxBuf, au1Date, u4Len);
        u4CalcLength += u4Len;    /* Add to the total message len */
        /* Before calculating the content length, construct the array of the
           HTML Body */
        /* Get the array length and update the content length of the packet to
           be sent out */
        if (u1WebAuthType == INTERNAL_WEBAUTH)
        {

            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            pWssIfCapwapDB->CapwapIsGetAllDB.bLocalIpAddr = OSIX_TRUE;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
                OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaAssembleWebRespPkts:"
                            "Failed to get local Ip Address\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                              "WssStaAssembleWebRespPkts Failed to get local Ip Address\r\n"));
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);

                WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
                if (u1IsNotLinear1 == OSIX_TRUE)
                {
                    MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
                }
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;

            }

            if (pwssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod ==
                WEBAUTH_INTERNAL)
            {
                MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bIntfName = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

                if (WssIfGetCapwapDB (pDot3Hdr->DstMacAddr, pWssIfCapwapDB) ==
                    OSIX_FAILURE)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
                    if (u1IsNotLinear1 == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
                    }
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaAssembleWebRespPkts:"
                                "Failed to get Capwap DB \r\n");
                    return OSIX_FAILURE;

                }

                if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting ==
                    LOCAL_BRIDGING_ENABLED)
                {
                    if (pwssWlanDB->WssWlanAttributeDB.u2VlanId == 1)
                    {
                        ArpGetIntfForAddr (0, pIpHdr->u4Src, &u4IfIndex);
                        nmhGetIfMainSubType (u4IfIndex, &i4IfType);
                        nmhGetIfIpAddr ((INT4) u4IfIndex, &u4IpAddr);
                    }
                    else
                    {
                        /*This place should be revisited during local bridging tag support */
                        u2VlanId = 1;
                        if (VlanGetFdbEntryDetails
                            (u2VlanId,
                             pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, &u2Port,
                             &u1Status) != SUCCESS)
                        {
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
                            if (u1IsNotLinear1 == OSIX_TRUE)
                            {
                                MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID,
                                                    pTxStart);
                            }
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);

                            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                        "Mac Entry does not learnt in the given port\r\n");
                            return OSIX_FAILURE;
                        }
#ifdef LNXIP4_WANTED
                        pu1PortName = CfaGddGetLnxIntfnameForPort (u2Port);
                        if (pu1PortName != NULL)
                        {
                            u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId;
                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            SPRINTF ((CHR1 *) au1IfName, "%s%s%d", pu1PortName,
                                     ".", u2VlanId);

                        }
#endif
                        pu1IfName = au1IfName;
                        if (CfaCliGetFirstIfIndexFromName
                            (pu1IfName, &u4IfIndex) != OSIX_FAILURE)
                        {
                            nmhGetIfIpAddr ((INT4) u4IfIndex, &u4IpAddr);
                        }
                    }
                }
                else
                {
                    /* u2VlanId = pwssWlanDB->WssWlanAttributeDB.u2VlanId; */

                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    SPRINTF ((CHR1 *) au1IfName, "%s%d", "vlan", u2VlanId);
                    pu1IfName = au1IfName;
                    if (CfaCliGetFirstIfIndexFromName (pu1IfName, &u4IfIndex) !=
                        OSIX_FAILURE)
                    {
                        nmhGetIfIpAddr ((INT4) u4IfIndex, &u4IpAddr);
                    }

                }
                IpAddr.u4_addr[0] = OSIX_HTONL (u4IpAddr);

                MEMSET (HTTP_CNTENT, 0, STRLEN (HTTP_CNTENT));

                pcByte = (CHR1 *) & (IpAddr.u4_addr[0]);
#ifdef WEBNM_WANTED
                i4HttpPort = HttpGetSourcePort ();
                if (i4HttpPort != ISS_DEF_HTTP_PORT)
                {
                    (void) SPRINTF ((CHR1 *) HTTP_CNTENT,
                                    "%s%d.%d.%d.%d%s%d%s%s%s", HTTP_CNTENT1,
                                    (UINT1) pcByte[0], (UINT1) pcByte[1],
                                    (UINT1) pcByte[2], (UINT1) pcByte[3],
                                    HTTP_PORTCHANGED, i4HttpPort, HTTP_CNTENT2,
                                    pwssWlanDB->WssWlanAttributeDB.
                                    au1FsDot11RedirectFileName, HTTP_CNTENT4);
                }
                else
#endif
                {
                    (void) SPRINTF ((CHR1 *) HTTP_CNTENT,
                                    "%s%d.%d.%d.%d%s%s%s", HTTP_CNTENT1,
                                    (UINT1) pcByte[0], (UINT1) pcByte[1],
                                    (UINT1) pcByte[2], (UINT1) pcByte[3],
                                    HTTP_CNTENT2,
                                    pwssWlanDB->WssWlanAttributeDB.
                                    au1FsDot11RedirectFileName, HTTP_CNTENT4);
                }
            }
            else
            {
                (void) SPRINTF ((CHR1 *) HTTP_CNTENT, "%s%s%s",
                                HTTP_EXT_CNTENT1,
                                pwssWlanDB->WssWlanAttributeDB.
                                au1ExternalWebAuthUrl, HTTP_EXT_CNTENT2);
            }

            u4Len = STRLEN (HTTP_CNTENT);
            WSSSTA_PUT_NBYTES (pTxBuf, HTTP_CNTENT, u4Len);
            u4CalcLength += (u4Len);
        }
    }
    else if ((pwssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod ==
              WEBAUTH_EXTERNAL) && (u1RespType == WSSSTA_HTTPPOST_RESP))
    {
        u4Len = STRLEN (HTTP_POST_RESP_VER_STRING);
        WSSSTA_PUT_NBYTES (pTxBuf, HTTP_POST_RESP_VER_STRING, u4Len);
        u4CalcLength += u4Len;    /* Add to the total message len */

        WssStaHttpGetDate (au1Date, FALSE);
        u4Len = STRLEN (au1Date);
        WSSSTA_PUT_NBYTES (pTxBuf, au1Date, u4Len);
        u4CalcLength += u4Len;    /* Add to the total message len */

        u4Len = STRLEN (HTTP_POST_RESP_HDR_STRING);
        WSSSTA_PUT_NBYTES (pTxBuf, HTTP_POST_RESP_HDR_STRING, u4Len);
        u4CalcLength += u4Len;    /* Add to the total message len */

        /* Since the transfer encoding is chunked, Insert the chunk size and
         * chunk date */
        /* Data should be inserted chunks of 1024 which includes chunk header
         * and data */

        pWssifauthDBMsgStruct =
            (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
        if (pWssifauthDBMsgStruct == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);

            if (u1IsNotLinear1 == OSIX_TRUE)
            {
                MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
            }
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
        MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
                pDot3Hdr->DstMacAddr, sizeof (tMacAddr));

        if (WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_STATION_DB,
                                      pWssifauthDBMsgStruct) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);

            if (u1IsNotLinear1 == OSIX_TRUE)
            {
                MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
            }
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        if (pWssifauthDBMsgStruct->WssIfAuthStateDB.u1StaAuthFinished ==
            OSIX_TRUE)
        {
            /* Inserting chunk size */
            u4Len = STRLEN (HTTP_POST_RESP_SUCCESS_STRING);
            SPRINTF ((CHR1 *) au1Tag, "%x%s", u4Len, HTTP_DELIMITER);
            u4Len = STRLEN (au1Tag);
            WSSSTA_PUT_NBYTES (pTxBuf, au1Tag, u4Len);
            u4CalcLength += u4Len;

            /* Inserting data */
            u4Len = STRLEN (HTTP_POST_RESP_SUCCESS_STRING);
            WSSSTA_PUT_NBYTES (pTxBuf, HTTP_POST_RESP_SUCCESS_STRING, u4Len);
            u4CalcLength += u4Len;
        }
        else
        {
            /* Inserting chunk size */
            u4Len = STRLEN (HTTP_POST_RESP_FAILURE_STRING);
            SPRINTF ((CHR1 *) au1Tag, "%x%s", u4Len, HTTP_DELIMITER);
            u4Len = STRLEN (au1Tag);
            WSSSTA_PUT_NBYTES (pTxBuf, au1Tag, u4Len);
            u4CalcLength += u4Len;

            /* Inserting data */
            u4Len = STRLEN (HTTP_POST_RESP_FAILURE_STRING);
            WSSSTA_PUT_NBYTES (pTxBuf, HTTP_POST_RESP_FAILURE_STRING, u4Len);
            u4CalcLength += u4Len;
        }
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);

        /* Insering chunk boundary */
        u4Len = STRLEN (HTTP_DELIMITER);
        WSSSTA_PUT_NBYTES (pTxBuf, HTTP_DELIMITER, u4Len);
        u4CalcLength += u4Len;

        /* Inserting End of chunk encoding  */
        MEMSET (au1Tag, 0, sizeof (au1Tag));
        SPRINTF ((CHR1 *) au1Tag, "%s%s%s", "0", HTTP_DELIMITER,
                 HTTP_DELIMITER);
        u4Len = STRLEN (au1Tag);
        WSSSTA_PUT_NBYTES (pTxBuf, au1Tag, u4Len);
        u4CalcLength += u4Len;
    }
    gWebAuthNextAck = (UINT1) (u4CalcLength - gWebAuthNextAck);
    /* Final checksum calculation */
    u4Length = (u4CalcLength - WSSSTA_ETH_IP_HDR_LEN);
    pTx1Buf += WSSSTA_ETH_IP_HDR_LEN;
    u2CkSum = CalcDataChecksum (pTx1Buf, u4Length, pIpHdr, u4CalcLength,
                                TCP_CKSUM);
    u2CkSum = OSIX_HTONS (u2CkSum);
    /* Calculate the total TCP checksum */
    /* Update the IP total length, IP hdr checksum and tcp checksum at the end
     * */
    u2IptotLen = (UINT2) (u4CalcLength - WSSSTA_ETHERNET_HEADER_LEN);
    pTxHead += WSSSTA_IP_TOTAL_LEN;    /* Point to IP tot len field */
    WSSSTA_PUT_2BYTES (pTxHead, u2IptotLen);

    /* Point to start of IP header to calculate the checksum */
    pTx2Buf += WSSSTA_ETHERNET_HEADER_LEN;
    u2Checksum =
        CalcDataChecksum (pTx2Buf, IP_HDR_LEN, pIpHdr, u4CalcLength, 0);
    pTxHead += WSSSTA_IP_HEADER_CHECKSUM_OFFSET;    /* Point to IP Hdr 
                                                     * checksum field */
    MEMCPY (pTxHead, &u2Checksum, sizeof (UINT2));
    pTxHead += sizeof (UINT2);

    pTxHead += WSSSTA_TCP_CHECKSUM_OFFSET;    /* Point to TCP checksum field */
    WSSSTA_PUT_2BYTES (pTxHead, u2CkSum);

    /* Copy the packet to CRU Buffer */
    if (u1IsNotLinear1 == OSIX_TRUE)
    {
        WSSSTA_COPY_TO_BUF (pu1TxBuf, pTxStart, 0, (u4CalcLength));
        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pTxStart);
    }
    /* Transmit the packet */
    if (WssStaTxMessage (pu1TxBuf, (u4CalcLength)) == OSIX_FAILURE)
    {
        WSSSTA_RELEASE_CRU_BUF (pu1TxBuf);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WSSSTA:Failed to Transmit the packet\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WSSSTA:Failed to Transmit the packet\r\n"));
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaTxMessage                                            *
 *                                                                           *
 * Description  : This function used to  transmit the given                  *
 *               packet over                                                 *
 *                                                                           *
 * Input        : u4DestPort  - Destination UDP port                         *
 *                u4DestAddr  - Destination IP Address                       *
 *                pBuf        - Pointer to the linear buffer                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 ******************************************************************************/
INT4
WssStaTxMessage (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4MsgLen)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct;
    tWssifauthDBMsgStruct WssifauthDBMsgStruct;
    UINT1              *pu1TxBuf;
    UINT1               au1Frame[CPU_MAX_PKT_LEN];

#ifdef NPAPI_WANTED

    INT4                i4RetVal;
    struct sockaddr_ll  Enet;
    UINT4               u4ToAddrLen;
    struct ifreq        ifr;
    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));
    if ((gu4WssStaDebugMask & WSSSTA_PKTDUMP_TRC) == WSSSTA_PKTDUMP_TRC)
    {
        DbgDumpWssStaPkt (pBuf, WSSSTA_PKT_FLOW_OUT, (UINT2) u4MsgLen);
    }
    /* Copy the vlan1 port name */
    sprintf (ifr.ifr_name, "%s", PORT_NAME);

    if (ioctl (i4WebAuthSock4Id, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("\nInterface Index Get Failed");
        close (i4WebAuthSock4Id);
        return OSIX_FAILURE;
    }
    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    u4ToAddrLen = sizeof (Enet);

    pu1TxBuf = WSSSTA_BUF_IF_LINEAR (pBuf, 0, u4MsgLen);
    if (pu1TxBuf == NULL)
    {
        MEMSET (au1Frame, 0x00, u4MsgLen);
        WSSSTA_COPY_FROM_BUF (pBuf, au1Frame, 0, u4MsgLen);
        pu1TxBuf = au1Frame;
    }
    /* Send the packet in the socket */
    i4RetVal = sendto (i4WebAuthSock4Id, pu1TxBuf, (UINT4) u4MsgLen, 0,
                       (struct sockaddr *) &Enet, (socklen_t) u4ToAddrLen);
    if (i4RetVal != (INT4) u4MsgLen)
    {
        perror ("WssStaTxMessage:Failed to Transmit packet : ");
        WSSSTA_RELEASE_CRU_BUF (pBuf);
        return OSIX_FAILURE;
    }
    WSSSTA_RELEASE_CRU_BUF (pBuf);
#else

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "\rWssStaTxMessage :\
                UtlShMemAllocWlcBuf() returns failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaTxMessage  UtlShMemAllocWlcBuf() returns failure \r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&WssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));

    pu1TxBuf = WSSSTA_BUF_IF_LINEAR (pBuf, 0, u4MsgLen);
    if (pu1TxBuf == NULL)
    {
        MEMSET (au1Frame, 0x00, u4MsgLen);
        WSSSTA_COPY_FROM_BUF (pBuf, au1Frame, 0, u4MsgLen);
        pu1TxBuf = au1Frame;
    }
    WSSSTA_GET_NBYTE ((WssifauthDBMsgStruct.WssIfAuthStateDB.stationMacAddress),
                      pu1TxBuf, sizeof (tMacAddr));
    if (WssIfProcessWssAuthDBMsg
        (WSS_AUTH_GET_STATION_DB, &WssifauthDBMsgStruct) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "\rWssStaTxMessage :\
                WssIfProcessWssAuthDBMsg failed to get BssIfIndex\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "\rWssStaTxMessage WssIfProcessWssAuthDBMsg failed to get BssIfIndex\r\n"));
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                         "Send Rsp to STA %02x:%02x:%02x:%02x:02x:%02x ",
                         WssifauthDBMsgStruct.WssIfAuthStateDB.
                         stationMacAddress[0],
                         WssifauthDBMsgStruct.WssIfAuthStateDB.
                         stationMacAddress[1],
                         WssifauthDBMsgStruct.WssIfAuthStateDB.
                         stationMacAddress[2],
                         WssifauthDBMsgStruct.WssIfAuthStateDB.
                         stationMacAddress[3],
                         WssifauthDBMsgStruct.WssIfAuthStateDB.
                         stationMacAddress[4],
                         WssifauthDBMsgStruct.WssIfAuthStateDB.
                         stationMacAddress[5]);
    WSSSTA_WEBAUTH_TRC1 (WSSSTAWEBAUTH_MASK, "on AP %d\n",
                         (UINT2) WssifauthDBMsgStruct.WssIfAuthStateDB.
                         aWssStaBSS[0].u4BssIfIndex);

    pWlcHdlrMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
    pWlcHdlrMsgStruct->WlcHdlrQueueReq.u2SessId =
        (UINT2) WssifauthDBMsgStruct.WssIfAuthStateDB.aWssStaBSS[0].
        u4BssIfIndex;
    pWlcHdlrMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CFA_QUEUE_REQ, pWlcHdlrMsgStruct)
        != OSIX_SUCCESS)
    {

        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "\rWssStaTxMessage :\
                WLcHdlr failed for WSS_WLCHDLR_CFA_QUEUE_REQ\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "\rWssStaTxMessage WLcHdlr failed for WSS_WLCHDLR_CFA_QUEUE_REQ\r\n"));

        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);

#endif

    return OSIX_SUCCESS;
}

INT4
WssStaWebAuthGetPktHdr (UINT1 *pBuf,
                        tDot3EthFrmHdr * pEthHdr,
                        t_IP_HEADER * pIpHdr, tWssStaTcpHdr * pTcpHdr)
{
    UINT1               u1TcpHdrLen = 0;
    UINT1               u1TcpOptLen = 0;
    UINT1              *pRcvBuf = NULL;
    pRcvBuf = pBuf;
    /* Ethernet header */
    WSSSTA_GET_NBYTE (pEthHdr->SrcMacAddr, pRcvBuf, sizeof (tMacAddr));
    WSSSTA_GET_NBYTE (pEthHdr->DstMacAddr, pRcvBuf, sizeof (tMacAddr));
    WSSSTA_GET_2BYTE (pEthHdr->u2EthType, pRcvBuf);
    /* IP header */
    WSSSTA_GET_1BYTE (pIpHdr->u1Ver_hdrlen, pRcvBuf);
    WSSSTA_GET_1BYTE (pIpHdr->u1Tos, pRcvBuf);
    WSSSTA_GET_2BYTE (pIpHdr->u2Totlen, pRcvBuf);
    WSSSTA_GET_2BYTE (pIpHdr->u2Id, pRcvBuf);
    WSSSTA_GET_2BYTE (pIpHdr->u2Fl_offs, pRcvBuf);
    WSSSTA_GET_1BYTE (pIpHdr->u1Ttl, pRcvBuf);
    WSSSTA_GET_1BYTE (pIpHdr->u1Proto, pRcvBuf);
    WSSSTA_GET_2BYTE (pIpHdr->u2Cksum, pRcvBuf);
    WSSSTA_GET_4BYTE (pIpHdr->u4Src, pRcvBuf);
    WSSSTA_GET_4BYTE (pIpHdr->u4Dest, pRcvBuf);
    if ((pIpHdr->u1Ver_hdrlen != 0x45) || (pIpHdr->u1Ttl == 1))
    {
        WSSSTA_TRC (WSSSTA_SESS_TRC, "WssStaWebAuthProcAndRespond:IP "
                    "version Length Failure\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "WssStaWebAuthProcAndRespond:IP version Length Failure\r\n"));
        return OSIX_FAILURE;
    }
    /* TCP header */
    WSSSTA_GET_2BYTE (pTcpHdr->u2Sport, pRcvBuf);
    WSSSTA_GET_2BYTE (pTcpHdr->u2Dport, pRcvBuf);
    WSSSTA_MEMCPY (&(pTcpHdr->i4Seq), pRcvBuf, sizeof (INT4));
    pTcpHdr->i4Seq = (INT4) OSIX_NTOHL ((UINT4) (pTcpHdr->i4Seq));
    pRcvBuf += sizeof (INT4);
    WSSSTA_MEMCPY (&(pTcpHdr->i4Ack), pRcvBuf, sizeof (INT4));
    pTcpHdr->i4Ack = (INT4) OSIX_NTOHL ((UINT4) (pTcpHdr->i4Ack));
    pRcvBuf += sizeof (INT4);
    WSSSTA_GET_1BYTE (pTcpHdr->u1Hlen, pRcvBuf);
    WSSSTA_GET_1BYTE (pTcpHdr->u1Code, pRcvBuf);
    WSSSTA_GET_2BYTE (pTcpHdr->u2Window, pRcvBuf);
    WSSSTA_GET_2BYTE (pTcpHdr->u2Cksum, pRcvBuf);
    WSSSTA_GET_2BYTE (pTcpHdr->u2Urgptr, pRcvBuf);
    u1TcpHdrLen =
        (UINT1) ((((pTcpHdr->u1Hlen) & TCP_IPVER_MASK) >> 4) * (UINT1) 4);
    if (u1TcpHdrLen > WSSSTA_TCP_HEADER_LEN)
    {
        u1TcpOptLen = (UINT1) (u1TcpHdrLen - WSSSTA_TCP_HEADER_LEN);
    }
    WSSSTA_GET_NBYTE (pTcpHdr->au1TcpOpts, pRcvBuf, u1TcpOptLen);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssStaWebAuthProcAndRespond                       */
/*  Description     : The function validates the header and processes   */
/*                    message based on the message type                 */
/*  Input(s)        : pRcvBuf - WSSSTA Discovery packet received        */
/*                    u1DestPort - Received UDP port                    */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
INT4
WssStaWebAuthProcAndRespond (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tWssifauthDBMsgStruct WssifauthDBMsgStruct;
    UINT2               u2PktLen = 0;
    tDot3EthFrmHdr      pDot3EthHdr;
    t_IP_HEADER         pIpHdr;
    tWssStaTcpHdr       pTcpHdr;
    UINT2               u2IpTotLen = 0;
    UINT2               i = 0;
    INT1                i1Char = 0;
    INT1                ai1Line[8];
    UINT1              *pu1ReadPtr = NULL;
    UINT1               u1RespType = 0;
    UINT1               u1IsNotLinear = OSIX_FALSE, u1TcpFlag = 0;
    UINT1               u1TcpHdrLen = 0;
    MEMSET (&WssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
    MEMSET (ai1Line, 0, sizeof (ai1Line));
    MEMSET (&pIpHdr, 0, sizeof (t_IP_HEADER));
    MEMSET (&pDot3EthHdr, 0, sizeof (tDot3EthFrmHdr));
    MEMSET (&pTcpHdr, 0, sizeof (tWssStaTcpHdr));

    /* get the packet length */
    u2PktLen = (UINT2) WSSSTA_GET_BUF_LEN (pBuf);

    pu1ReadPtr = WSSSTA_BUF_IF_LINEAR (pBuf, 0, u2PktLen);
    if (pu1ReadPtr == NULL)
    {
        pu1ReadPtr = (UINT1 *) MemAllocMemBlk (WSSSTA_PKTBUF_POOLID);
        if (pu1ReadPtr == NULL)
        {
            return OSIX_FAILURE;
        }
        WSSSTA_COPY_FROM_BUF (pBuf, pu1ReadPtr, 0, u2PktLen);
        u1IsNotLinear = OSIX_TRUE;
    }
    if (WssStaWebAuthGetPktHdr (pu1ReadPtr, &pDot3EthHdr, &pIpHdr, &pTcpHdr) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_SESS_TRC,
                    "Error in retrieving the L2,L3,L4 headers");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Error in retrieving the L2,L3,L4 headers"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
        }
        return OSIX_FAILURE;
    }
    u1TcpHdrLen =
        (UINT1) ((((pTcpHdr.u1Hlen) & TCP_IPVER_MASK) >> 4) * (UINT1) 4);
    /* Check if it is a TCP SYN packet */
    if (pTcpHdr.u1Code == TCP_SYN_FLAG)
    {
        u1RespType = WSSSTA_TCP_SYNACK;
        u1TcpFlag = TCP_SYN_ACK_FLAG;
    }
    else if (pTcpHdr.u1Code == TCP_FIN_ACK_FLAG)    /* received a FIN ACK packet */
    {
        u1RespType = WSSSTA_TCP_ACK;
        if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                       &pIpHdr,
                                       &pTcpHdr,
                                       WSSSTA_TCP_ACK,
                                       u1TcpHdrLen,
                                       TCP_ACK_FLAG) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "Error in generating the ACK packet\n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                          "Error in generating the ACK packet\n"));
            if (u1IsNotLinear == OSIX_TRUE)
            {
                MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
            }
            return OSIX_FAILURE;
        }
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
        }
        return OSIX_SUCCESS;

    }
    /* Drop the Ack packets */
    else if (pTcpHdr.u1Code == TCP_ACK_FLAG)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Silently discard the TCP ACK packets\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Silently discard the TCP ACK packets\n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
        }
        return OSIX_FAILURE;
    }
    else
    {                            /* For HTTP response */
        /* Check  if the packet has the HTTP payload 
         * If yes and the HTTP payload is a GET request do the redirection
         * to internal/External login page and send it to the station */
        u2IpTotLen = pIpHdr.u2Totlen;
        if ((u2IpTotLen - (u1TcpHdrLen + IP_HDR_LEN)) > 0)
        {
            /* The packet has a HTTP payload to be processed */
            /* Parse the HTTP packet and if it is requesting for a GET method
             * then construct the HTTP response with a redirection page 
             */
            pu1ReadPtr += (WSSSTA_ETH_IP_HDR_LEN + u1TcpHdrLen);
            /* Process the HTTP request Line */
            for (i = 0; i < sizeof (ai1Line); i++)
            {
                i1Char = (INT1) *pu1ReadPtr;
                pu1ReadPtr += 1;
                if (i1Char == -1)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WSSSTA:Failed to Parse the HTTP packet \n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                  "WSSSTA:Failed to Parse the HTTP packet \n"));
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    return OSIX_FAILURE;
                }
                if (i1Char == ' ')
                {
                    ai1Line[i] = '\0';
                    break;
                }
                ai1Line[i] = i1Char;
            }
            if ((STRCMP (ai1Line, WSSSTA_HTTP_GET_METHOD) == 0))
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC, "Received a GET request \n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4StaSysLogId,
                              "Received a GET request \n"));
                /* Store the URL in the local DB */
                /* Generate a ACK before sending a HTTP response */
                if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                               &pIpHdr,
                                               &pTcpHdr,
                                               WSSSTA_TCP_HTTPGET_ACK,
                                               u1TcpHdrLen,
                                               TCP_ACK_FLAG) == OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Error in generating the ACK packet\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                  "Error in generating the ACK packet\n"));
                    /* Kloc Fix Start */
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    /* Kloc Fix Ends */
                    return OSIX_FAILURE;
                }
                /* Generate the HTTP response with redirect URL */
                u1RespType = WSSSTA_HTTPRESP;
                if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                               &pIpHdr,
                                               &pTcpHdr,
                                               WSSSTA_HTTPRESP,
                                               u1TcpHdrLen,
                                               TCP_PSH_ACK_FLAG) ==
                    OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Error in generating the HTTP packet\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                  "Error in generating the HTTP packet\n"));
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    return OSIX_FAILURE;
                }
                /* Generate the FINACK packet */
                u1RespType = WSSSTA_TCP_FIN_ACK;
                if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                               &pIpHdr,
                                               &pTcpHdr,
                                               WSSSTA_TCP_FIN_ACK,
                                               u1TcpHdrLen,
                                               TCP_FIN_ACK_FLAG) ==
                    OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Error in generating the HTTP packet\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                  "Error in generating the HTTP packet\n"));
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    return OSIX_FAILURE;
                }

                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                }

                MEMCPY (WssifauthDBMsgStruct.WssIfAuthStateDB.
                        stationMacAddress, pDot3EthHdr.DstMacAddr,
                        sizeof (tMacAddr));

                WssifauthDBMsgStruct.WssIfAuthStateDB.u1StaAuthRedirected =
                    OSIX_TRUE;
                WssifauthDBMsgStruct.WssIfAuthStateDB.bStaAuthRedirected =
                    OSIX_TRUE;

                if (WssIfProcessWssAuthDBMsg (WSS_AUTH_DB_UPDATE_RULENUM,
                                              &WssifauthDBMsgStruct) !=
                    OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WlcHdlrVerifyStaPkts:\
                            WssAuth DB Failed to set Auth Status for \
                            Station Mac Address\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                                  "WlcHdlrVerifyStaPkts:\
                            WssAuth DB Failed to set Auth Status for \
                            Station Mac Address\r\n"));
                    return OSIX_FAILURE;
                }

                return OSIX_SUCCESS;
            }
            else if (STRCMP (ai1Line, WSSSTA_HTTP_POST_METHOD) == 0)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC, "Received a POST request \n");
                /* Store the URL in the local DB */
                /* Generate a ACK before sending a HTTP response */
                if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                               &pIpHdr,
                                               &pTcpHdr,
                                               WSSSTA_TCP_HTTPGET_ACK,
                                               u1TcpHdrLen,
                                               TCP_ACK_FLAG) == OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Error in generating the ACK packet\n");
                    /* Kloc Fix Start */
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    /* Kloc Fix Ends */
                    return OSIX_FAILURE;
                }
                /* Generate the HTTP response with redirect URL */
                u1RespType = WSSSTA_HTTPPOST_RESP;
                if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                               &pIpHdr,
                                               &pTcpHdr,
                                               WSSSTA_HTTPPOST_RESP,
                                               u1TcpHdrLen,
                                               TCP_PSH_ACK_FLAG) ==
                    OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Error in generating the HTTP packet\n");
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    return OSIX_FAILURE;
                }
                /* Generate the FINACK packet */
                u1RespType = 4;
                if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                               &pIpHdr,
                                               &pTcpHdr,
                                               WSSSTA_TCP_FIN_ACK,
                                               u1TcpHdrLen,
                                               TCP_FIN_ACK_FLAG) ==
                    OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Error in generating the HTTP packet\n");
                    if (u1IsNotLinear == OSIX_TRUE)
                    {
                        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                    }
                    return OSIX_FAILURE;
                }

                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                }

                MEMCPY (WssifauthDBMsgStruct.WssIfAuthStateDB.
                        stationMacAddress, pDot3EthHdr.DstMacAddr,
                        sizeof (tMacAddr));

                WssifauthDBMsgStruct.WssIfAuthStateDB.u1StaAuthRedirected =
                    OSIX_TRUE;
                WssifauthDBMsgStruct.WssIfAuthStateDB.bStaAuthRedirected =
                    OSIX_TRUE;

                if (WssIfProcessWssAuthDBMsg (WSS_AUTH_DB_UPDATE_RULENUM,
                                              &WssifauthDBMsgStruct) !=
                    OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WlcHdlrVerifyStaPkts:\
                            WssAuth DB Failed to set Auth Status for \
                            Station Mac Address\r\n");
                    return OSIX_FAILURE;
                }

                return OSIX_SUCCESS;
            }
            else
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WSSSTA: Need not process any HTTP packet \
                          other than GET request \n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                              "WSSSTA: Need not process any HTTP packet \
                          other than GET request \n"));
                if (u1IsNotLinear == OSIX_TRUE)
                {
                    MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
                }
                return OSIX_SUCCESS;
            }
        }
    }
    /* Assemble the response packets */
    if (WssStaAssembleWebRespPkts (&pDot3EthHdr,
                                   &pIpHdr,
                                   &pTcpHdr,
                                   u1RespType,
                                   u1TcpHdrLen, u1TcpFlag) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Error in generating the packet\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4StaSysLogId,
                      "Error in generating the packet\n"));
        if (u1IsNotLinear == OSIX_TRUE)
        {
            MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
        }
        return OSIX_FAILURE;
    }

    if (u1IsNotLinear == OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSSTA_PKTBUF_POOLID, pu1ReadPtr);
    }
    UNUSED_PARAM (u1RespType);
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : WsStaIpCalcCheckSum                                    */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
UINT2
WsStaIpCalcCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size, UINT4 u4Offset,
                     t_IP_HEADER * pIpHdr, UINT2 u2Type)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               au1Frame[IP_HDR_LEN];

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);
    if (pu1Buf == NULL)
    {
        MEMSET (au1Frame, 0x00, u4Size);
        WSSSTA_COPY_FROM_BUF (pBuf, au1Frame, 0, u4Size);
        pu1Buf = au1Frame;
    }
    while (u4Size > 1)
    {
        WSSSTA_GET_2BYTE (u2Tmp, pu1Buf);
        u4Sum += u2Tmp;
        u4Size -= 2;
    }

    /* If the checksum is for TCP , then calculate the pseudo header as well */
    if (u2Type == TCP_CKSUM)
    {
        u4Sum += OSIX_NTOHS ((pIpHdr->u4Src) & TCP_MAX_UINT2);
        u4Sum += OSIX_NTOHS (((pIpHdr->u4Src) >> TCP_BIT_SHIFT_16) &
                             TCP_MAX_UINT2);
        u4Sum += OSIX_NTOHS ((pIpHdr->u4Dest) & TCP_MAX_UINT2);
        u4Sum += OSIX_NTOHS (((pIpHdr->u4Dest) >> TCP_BIT_SHIFT_16) &
                             TCP_MAX_UINT2);
        u4Sum += WSSSTA_IP_HEADER_CHECKSUM_OFFSET;    /* TCP protocol */
        u4Sum += WSSSTA_TCP_HEADER_LEN;    /* TCP header length */
    }

    u4Sum = (u4Sum >> WSSSTA_IP_TOTAL_LEN) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> WSSSTA_IP_TOTAL_LEN);
    u2Tmp = (UINT2) ~(u4Sum);
    return (OSIX_NTOHS (u2Tmp));
}

/*************************************************************************/
/*  Function Name   : CalcDataChecksum                                      */
/*  Input(s)        : pBuf - Pointer to the buffer holding the string of */
/*                    bytes.                                             */
/*                    u4Size - No of bytes                               */
/*                    u4ReadOffset - Where to start from in buffer       */
/*  Output(s)       : None.                                              */
/*  Returns         : Checksum value                                     */
/*  Description     : This function calculates the checksum of the       */
/*                    data portion                                       */
/*  CallingCondition: From TcpPktCksum                                   */
/*************************************************************************/
UINT2
CalcDataChecksum (UINT1 *pTx1Buf,
                  UINT4 u4Length,
                  t_IP_HEADER * pIpHdr, UINT4 u4CalcLength, UINT1 u1CkSumType)
{
    /* Calculate the total TCP checksum */
    UINT2               u2CkSum = 0, u2Tmp = 0, u2Val = 0;
    UINT1               u1Val = 0;
    UINT4               u4CkSum = 0;
    while (u4Length > 0)
    {
        if (u4Length == 1)
        {
            WSSSTA_MEMCPY (&u1Val, pTx1Buf, 1);
            u2Tmp =
                (UINT2) ((u1Val << TCP_BIT_SHIFT_8) & TCP_MAX_UINT2_FOR_BYTE);
            u4CkSum += (u2Tmp);
            break;
        }
        else
        {
            WSSSTA_MEMCPY (&u2Val, pTx1Buf, sizeof (UINT2));
            u4CkSum += OSIX_HTONS (u2Val);
            pTx1Buf += sizeof (UINT2);
        }
        u4Length -= sizeof (UINT2);
    }
    if (u1CkSumType == TCP_CKSUM)
    {
        u4CkSum += ((pIpHdr->u4Src) & TCP_MAX_UINT2);
        u4CkSum += (((pIpHdr->u4Src) >> TCP_BIT_SHIFT_16) & TCP_MAX_UINT2);
        u4CkSum += ((pIpHdr->u4Dest) & TCP_MAX_UINT2);
        u4CkSum += (((pIpHdr->u4Dest) >> TCP_BIT_SHIFT_16) & TCP_MAX_UINT2);
        u4CkSum += WSSSTA_IP_HEADER_CHECKSUM_OFFSET;    /* TCP protocol */
        u4CkSum += (u4CalcLength - WSSSTA_ETH_IP_HDR_LEN);

        u4CkSum = (u4CkSum >> TCP_BIT_SHIFT_16) + (u4CkSum & TCP_MAX_UINT2);
        u4CkSum = (u4CkSum >> TCP_BIT_SHIFT_16) + (u4CkSum & TCP_MAX_UINT2);

        u4CkSum += (u4CkSum >> WSSSTA_IP_TOTAL_LEN);
        u2CkSum = ((UINT2) (~((UINT2) u4CkSum)));
    }
    else
    {
        u4CkSum = (u4CkSum >> TCP_BIT_SHIFT_16) + (u4CkSum & TCP_MAX_UINT2);
        u4CkSum += (u4CkSum >> WSSSTA_IP_TOTAL_LEN);
        u2CkSum = (UINT2) (~((UINT2) u4CkSum));
    }
    return (OSIX_HTONS (u2CkSum));
}

/************************************************************************/
/*  Function Name   : WssStaSendWebAuthTrap                             */
/*  Description     : The function sends the trap when user             */
/*                    authentication failes in web auth                 */
/*  Input(s)        : au1ClientMac - Client Mac Address                 */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
WssStaSendWebAuthTrap (tMacAddr staMacAddr)
{
    tWlanSTATrapInfo    WTPTrapInfo;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4ProfileIndex = 0;
    UINT1               au1ClientMac[MAC_ADDR_LEN];

    MEMSET (&WTPTrapInfo, 0, sizeof (tWlanSTATrapInfo));
    MEMSET (au1ClientMac, 0, sizeof (MAC_ADDR_LEN));
    MEMCPY (au1ClientMac, staMacAddr, sizeof (tMacAddr));
    /* WEB AUTH STATIC CONFIG TRAP */
    pWssStaWepProcessDB = WssStaProcessEntryGet (au1ClientMac);

    if (pWssStaWepProcessDB == NULL)
    {
        return;
    }

    /* Retrivie the profile Index from the local database */
    if (WssIfGetProfileIfIndex (pWssStaWepProcessDB->u4BssIfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        return;
    }
    WTPTrapInfo.u4ifIndex = u4ProfileIndex;
    MEMCPY (WTPTrapInfo.au1CliMac, au1ClientMac, sizeof (tMacAddr));
    WTPTrapInfo.u4AuthFailure = WSS_WLAN_WEB_AUTH_FAIL_TRAP;
    WlanSnmpifSendTrap (WSS_WLAN_AUTH_FAILURE, &WTPTrapInfo);

    return;

}
#endif
