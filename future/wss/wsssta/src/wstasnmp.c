/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wstasnmp.c,v 1.2 2017/05/23 14:16:59 siva Exp $
 *
 * Description: This file contains the Station trap generation
 *              related code.
 *                            
 ********************************************************************/

#ifndef __WLANSNMP_C__
#define __WLANSNMP_C__

#include "wssstawlcinc.h"
#include "snmputil.h"
#include "wsscfginc.h"

/****************************************************************************
 *                                                                          *
 * Function     : WlanSnmpifSendTrap                                        *
 *                                                                          *
 * Description  : Routine to send trap to SNMP Agent.                       *
 *                                                                          *
 * Input        : u1TrapId: Trap identifier                                 *
 *                p_trap_info : Pointer to structure having the trap info.  *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
PUBLIC VOID
WlanSnmpifSendTrap (UINT1 u1TrapId, VOID *pTrapInfo)
{
#if ((defined(FUTURE_SNMP_WANTED))||(defined(SNMP_3_WANTED))||\
                 (defined(SNMPV3_WANTED)))
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4WlanTrapOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 83, 9, 0 };
    UINT4               au4BaseWtpId[] = { 1, 3, 6, 1, 2, 1, 196, 1, 5, 1 };
    UINT4               au4ClientCount[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 82, 3, 19, 1, 9 };
    UINT4               au4ifIndexOid[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 1 };
    UINT4               au4CliMacOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 83, 8, 1, 3 };
    UINT4               au4AssocStsOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 83, 3, 15, 1, 6 };
    UINT4               au4DissconReasOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 83, 8, 1, 2 };
    UINT4               au4AuthFailStatusOid[] =
        { 1, 3, 6, 1, 4, 1, 29601, 2, 83, 8, 1, 1 };
    UINT1               au1MacBuf[MAX_MAC_LENGTH];
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tWlanSTATrapInfo   *pSTATrapInfo = NULL;
    tSNMP_OCTET_STRING_TYPE *pOMacString = NULL;

    pSTATrapInfo = (tWlanSTATrapInfo *) pTrapInfo;

    if (gWsscfgGlobals.u4StationTrapStatus != WSS_WLAN_STATION_TRAP_ENABLE)
    {
        return;
    }

    /* The format of SNMP Trap varbinding which is being send using
     * SNMP_AGT_RIF_Notify_v2Trap() is as given below:
     *
     *
     ********************************************************************************
     *                   *                   *    Varbind     *     Varbind    *
     * sysUpTime = Time  * snmpTrapOID = OID * OID# 1 = Value * OID# 2 = Value *
     *                   *                   *                *                *
     ********************************************************************************
     *
     * First Var binding sysUpTime = Time - is filled by the SNMP_AGT_RIF_Notify_v2Trap().
     * Second Var binding snmpTrapOID - OID should be first Varbind in our list to the api SNMP_AGT_RIF_Notify_v2Trap().
     * The remaining Varbind are updated based on the type of the trap, which
     * are going to be generated.
     *
     */

    /* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and
     * the OID of the MIB object for those trap is going to be generated.
     */

    /* Allocate a memory for storing snmpTrapOID OID and assign it */

    pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        return;
    }

    /* Assigning the OID value and its length of snmpTrapOid to psnmpTrapOid */
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));

    pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    /* Allocate a memory for storing OID Value which is the OID of the
     * Notification MIB object and assign it */

    pEnterpriseOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    /* Assigning TrapOid to EnterpriseOid and binding it to packet. */
    MEMCPY (pEnterpriseOid->pu4_OidList, au4WlanTrapOid,
            sizeof (au4WlanTrapOid));
    pEnterpriseOid->u4_Length = sizeof (au4WlanTrapOid) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    /* Form a VarBind for snmpTrapOID OID and its value */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);

    /* If the pVbList is empty or values are binded then free the memory
       allocated to pEnterpriseOid and pSnmpTrapOid. */

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {
        case WSS_WLAN_MAX_USER_PER_WTP:
            if (gWsscfgGlobals.u4StationTrapStatus !=
                WSS_WLAN_STATION_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocate the Memory for WLAN Number of station exceeded */
            /* Trap OID to form the VbList */
            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Assigning the oid and oid length of BaseWtpId to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4BaseWtpId,
                    sizeof (au4BaseWtpId));
            pSnmpTrapOid->u4_Length = sizeof (au4BaseWtpId) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAX_MAC_LENGTH);
            /* Assigning MacAddress to MacBuf */
            SPRINTF ((CHR1 *) au1MacBuf, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
                     pSTATrapInfo->au1BaseWtpId[0],
                     pSTATrapInfo->au1BaseWtpId[1],
                     pSTATrapInfo->au1BaseWtpId[2],
                     pSTATrapInfo->au1BaseWtpId[3],
                     pSTATrapInfo->au1BaseWtpId[4],
                     pSTATrapInfo->au1BaseWtpId[5]);

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                                    (INT4) STRLEN (au1MacBuf));

            /* Forming a VarBind for BaseWtpId, its value and storing it in pVbList */

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocate the Memory for WLAN Number of station exceeded */
            /* Trap OID to form the VbList */
            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the OID value and its length of ClientCount to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4ClientCount,
                    sizeof (au4ClientCount));
            pSnmpTrapOid->u4_Length = sizeof (au4ClientCount) / sizeof (UINT4);

            /* Forming a VarBind for OID of ClientCount and its value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4ClientCount, NULL,
                                                         NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case WSS_WLAN_DISASSOCIATE:
            if (gWsscfgGlobals.u4StationTrapStatus !=
                WSS_WLAN_STATION_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            /* Allocate the Memory for WLAN Disassociate Trap OID to form the VbList */

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Assigning the oid and oid length of ifIndex to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4ifIndexOid,
                    sizeof (au4ifIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4ifIndexOid) / sizeof (UINT4);

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4ifIndex, NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            /* Assigning the oid and oid length of Client Mac to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4CliMacOid,
                    sizeof (au4CliMacOid));
            pSnmpTrapOid->u4_Length = sizeof (au4CliMacOid) / sizeof (UINT4);

            /* Assigning MacAddress to MacBuf */
            MEMSET (au1MacBuf, 0, MAX_MAC_LENGTH);
            SPRINTF ((CHR1 *) au1MacBuf, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
                     pSTATrapInfo->au1CliMac[0],
                     pSTATrapInfo->au1CliMac[1],
                     pSTATrapInfo->au1CliMac[2],
                     pSTATrapInfo->au1CliMac[3],
                     pSTATrapInfo->au1CliMac[4], pSTATrapInfo->au1CliMac[5]);

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                                    (INT4) STRLEN (au1MacBuf));

            /* Forming a VarBind , its value and storing it in pVbList */
            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of Association Status to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4AssocStsOid,
                    sizeof (au4AssocStsOid));
            pSnmpTrapOid->u4_Length = sizeof (au4AssocStsOid) / sizeof (UINT4);
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4AssocStatus, NULL,
                                                         NULL, u8CounterVal);
            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Allocate the Memory for Disassociate Trap OID to form the VbList */

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of Disconnection Reason to pSnmpTrapOid */

            MEMCPY (pSnmpTrapOid->pu4_OidList, au4DissconReasOid,
                    sizeof (au4DissconReasOid));
            pSnmpTrapOid->u4_Length =
                sizeof (au4DissconReasOid) / sizeof (UINT4);
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4DisconnReason, NULL,
                                                         NULL, u8CounterVal);
            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case WSS_WLAN_CONNECTION_STATUS:
            if (gWsscfgGlobals.u4StationTrapStatus !=
                WSS_WLAN_STATION_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocate the Memory for WLAN Connection status Trap OID to form the VbList */
            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of ifIndex to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4ifIndexOid,
                    sizeof (au4ifIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4ifIndexOid) / sizeof (UINT4);

            /* Forming a VarBind for OID of ifIndex and its value */
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4ifIndex, NULL, NULL,
                                                         u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of Client Mac to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4CliMacOid,
                    sizeof (au4CliMacOid));
            pSnmpTrapOid->u4_Length = sizeof (au4CliMacOid) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAX_MAC_LENGTH);
            /* Assigning MacAddress to MacBuf */
            SPRINTF ((CHR1 *) au1MacBuf, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
                     pSTATrapInfo->au1CliMac[0],
                     pSTATrapInfo->au1CliMac[1],
                     pSTATrapInfo->au1CliMac[2],
                     pSTATrapInfo->au1CliMac[3],
                     pSTATrapInfo->au1CliMac[4], pSTATrapInfo->au1CliMac[5]);

            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                                    (INT4) STRLEN (au1MacBuf));

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of Association Status to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4AssocStsOid,
                    sizeof (au4AssocStsOid));
            pSnmpTrapOid->u4_Length = sizeof (au4AssocStsOid) / sizeof (UINT4);
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4AssocStatus, NULL,
                                                         NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case WSS_WLAN_AUTH_FAILURE:
            if (gWsscfgGlobals.u4StationTrapStatus !=
                WSS_WLAN_STATION_TRAP_ENABLE)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }

            /* Allocate the Memory for WLAN Authentication Failure Trap OID to form the VbList */
            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);

            /* Checking for error in memory allocation for pSnmpTrapOid and
             * freeing the memory allocated to pEnterpriseOid if there is an
             * error */

            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of ifIndex to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4ifIndexOid,
                    sizeof (au4ifIndexOid));
            pSnmpTrapOid->u4_Length = sizeof (au4ifIndexOid) / sizeof (UINT4);
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4ifIndex, NULL, NULL,
                                                         u8CounterVal);
            /* Checking if the pVbList is empty and freeing the memory allocated
               to pSnmpTrapOid and pEnterpriseOid if it is NULL */

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of Client Mac to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4CliMacOid,
                    sizeof (au4CliMacOid));
            pSnmpTrapOid->u4_Length = sizeof (au4CliMacOid) / sizeof (UINT4);

            MEMSET (au1MacBuf, 0, MAX_MAC_LENGTH);
            /* Assigning MacAddress to MacBuf */
            SPRINTF ((CHR1 *) au1MacBuf, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
                     pSTATrapInfo->au1CliMac[0],
                     pSTATrapInfo->au1CliMac[1],
                     pSTATrapInfo->au1CliMac[2],
                     pSTATrapInfo->au1CliMac[3],
                     pSTATrapInfo->au1CliMac[4], pSTATrapInfo->au1CliMac[5]);
            /* Converting the normal string to OctetString using SNMP_AGT_FormOctetString function */
            pOMacString = SNMP_AGT_FormOctetString ((UINT1 *) au1MacBuf,
                                                    (INT4) STRLEN (au1MacBuf));

            pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                          SNMP_DATA_TYPE_OCTET_PRIM,
                                                          0, 0, pOMacString,
                                                          NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            pSnmpTrapOid = alloc_oid (WSSSTA_TRAP_OID_LEN);
            if (pSnmpTrapOid == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            /* Assigning the oid and oid length of Authentication Failure Status to pSnmpTrapOid */
            MEMCPY (pSnmpTrapOid->pu4_OidList, au4AuthFailStatusOid,
                    sizeof (au4AuthFailStatusOid));
            pSnmpTrapOid->u4_Length =
                sizeof (au4AuthFailStatusOid) / sizeof (UINT4);
            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0L,
                                                         (INT4) pSTATrapInfo->
                                                         u4AuthFailure, NULL,
                                                         NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        default:
            break;
    }
#ifdef SNMP_2_WANTED
    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
#endif
#else

    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pTrapInfo);

#endif /* FUTURE_SNMP_WANTED */

#ifdef DEBUG_WANTED
    DbgNotifyTrap (u1TrapId, pTrapInfo);
#endif
}
#endif
