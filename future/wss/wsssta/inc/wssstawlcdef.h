/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 * $Id: wssstawlcdef.h,v 1.3 2017/05/23 14:16:58 siva Exp $              *
 * Description: This file contains the macros forWSSSTA module                *
 ******************************************************************************/

#ifndef _WSSSTA_WLC_DEF_H_
#define _WSSSTA_WLC_DEF_H_

/*Trace Level*/
#define WSSSTA_MGMT_TRC                    0x00000001
#define WSSSTA_INIT_TRC                    0x00000002
#define WSSSTA_ENTRY_TRC                   0x00000004
#define WSSSTA_EXIT_TRC                    0x00000008
#define WSSSTA_FAILURE_TRC                 0x00000010
#define WSSSTA_BUF_TRC                     0x00000020
#define WSSSTA_SESS_TRC                    0x00000040
#define WSSSTA_PKTDUMP_TRC                 0x00000080
#define WSSSTA_DEBUG_TRC                   0x00000100
#define WSSSTA_INFO_TRC                    0x00000200

#define WSSSTA_RB_GREATER 1
#define WSSSTA_RB_EQUAL   0
#define WSSSTA_RB_LESS    -1

#define MAX_WSSSTA_WEBAUTH  MAX_STA_SUPP_PER_WLC
#define TCP_ONE 1
#define TCP_BYTES_FOR_SHORT 2
#define  TCP_BIT_SHIFT_8            8
#define  TCP_BIT_SHIFT_16           16
#define  TCP_IPHDRLEN_MASK          0x0F
#define  TCP_MAX_UINT2              0xFFFF
#define  TCP_MAX_UINT2_FOR_BYTE     0xFF00
#define WSSSTA_TIMER_EXP_EVENT 0x00000001
#define WSSSTA_SET_AID_BITS    0x0003
/* For web Authentication */
#define WSSSTA_WEBAUTH_TASK_NAME      "WMAIN"
#define WSSSTA_WEBAUTH_PROC_TASK_NAME      "WPROC"
#define WSSSTA_TCPDATA_PACKET_ON_CPU 0x1
#define WSSSTA_RX_MSGQ_EVENT    0x2
#define WSSSTA_WEBAUTH_TMR_EXP_EVENT 0x00000003
#define WSSSTA_MAX_DATE_LEN                         40
#define WSSSTA_WEBAUTH_TIMEOUT 60 /* 1 minute timer expiry */
#define CPU_MAX_PKT_LEN 1500
#define MAX_WSSSTA_PKTBUF 50
#define WSSSTA_ETH_IP_HDR_LEN 34
#define WSSSTA_PKT_FLOW_IN 1
#define WSSSTA_PKT_FLOW_OUT 2
#define WSSSTA_WEBAUTH_Q_NAME             (UINT1 *) "WSSSTAQ"
#define IP_HDR_LEN 20
#define TCP_HDR_LEN 40
#define TCP_DEFAULT_HDR_LEN_WORD 5
#define TCP_SYN_ACK_FLAG 0x12
#define TCP_FIN_ACK_FLAG 0x11
#define TCP_SYN_FLAG   0x02
#define TCP_ACK_FLAG 0x10
#define TCP_PSH_ACK_FLAG 0x18
#define WSSSTA_TWO_BYTES 2
#define WSSSTA_INIT_VAL 0
#define WSSSTA_TCP_SYNACK 1
#define WSSSTA_HTTPRESP  2
#define WSSSTA_TCP_ACK  3
#define WSSSTA_TCP_FIN_ACK 4
#define WSSSTA_TCP_HTTPGET_ACK 5
#define WSSSTA_HTTPPOST_RESP  6
#define WSSSTA_HTTP_OK        200
#define HTTP_REASON   "OK"
#define TCP_DATA_OFFSET 12
#define TCP_CKSUM 1
#define  TCP_MAX_UINT2              0xFFFF
#define  TCP_BIT_SHIFT_16           16
#define HTML_INFO_LEN  146

#define TCP_HDR_LEN_WITHOUT_OPTION  20 
#define TCP_OPT_NOP         0x01
#define TCP_OPT_TIMESTAMP   0x08
#define TCP_TIMESTAMP_SIZE  4

/* redirecting HTML body to be appeneded in the HTTP response */
/* For Internal Web Authentication */
#define HTTP_INTERNAL_REDIRECT_HTML "Content-length:146\r\n<HTML>\n\
    <HEAD><TITLE>Web Authentication redirect\
</TITLE><META http-equiv=\"refresh\" content=\"1;\
URL=https://42.0.0.10/login.html\"></HEAD></HTML>"
/* For External Web AUthentication */
#define HTTP_EXTERNAL_REDIRECT_HTML "\r\n<HTML>\n<HEAD><TITLE>Web \
    Authentication redirect\
</TITLE>\n<META http-equiv=\"refresh\" content=\"1;\
URL=<https://42.0.0.10/login.html? "

#define WLC_IP "&wlc_ip="
#define STATION_ID "&station_id="
#define REDIRECT_URL "&redirect_url="
#define AP_MAC "&ap_mac="
#define WLAN "&wlan="
#define END_HTML "</HEAD></HTML>"

#define WSSSTA_MAX_HTML_LENGTH 512
#define WSSSTA_MAX_CONTENT_LEN 40
#define INTERNAL_WEBAUTH 1
#define EXTERNAL_WEBAUTH 2
#define WSSSTA_ETH_HDR_LEN 14
#define WSSSTA_AUTH_SEQ1 0x0100
#define WSSSTA_AUTH_SEQ2 0x0200
#define WSSSTA_AUTH_SEQ3 0x0300
#define WSSSTA_AUTH_SEQ4 0x0400
#define WSSSTA_CHALLENGE_TXT_LEN 128
#define WSSSTA_COMMENTED_CODE 0
#define WSSSTA_AID_MASK 0xc000
#define WSSSTA_CAPABILITY_MASK 0x1000
#define WSSSTA_EDCA_FRAME_LEN 20
#define WSSSTA_IEEESTA_LEN 21
#define WSSSTA_SUPP_RATE_LEN 4
#define WSSSTA_OFDM_CAPABILITY 2
#define WSSSTA_SHORTSLOTTIME_CAPABILITY 5
#define WSSSTA_SPECTRUMMGMT_CAPABILITY 7
#define WSSSTA_CHANNELAGILITY_CAPABILITY 8
#define WSSSTA_PBCC_CAPABILITY 9
#define WSSSTA_SHORTPREAMBLE_CAPABILITY 10
#define WSSSTA_MAX_QUEUE_DEPTH 200
#define WSSSTA_TCP_LIFT_RULE_NUM 1
#define WSSSTA_TCP_LIFT_RULE_PRIO 0
#define WSSSTA_DEFAULT_RULE_ID 1
#define WSSSTA_DEFAULT_RULE_NUM 1
#define WSSSTA_DEFAULT_RULE_PRIO 0
#define WSSSTA_DNS_FWD_RULE_ID 1
#define WSSSTA_DNS_FWD_RULE_NUM 1
#define WSSSTA_DNS_FWD_RULE_PRIO 0 
#define WSSSTA_FWD_RULE_NUM 1
#define WSSSTA_FWD_RULE_PRIO 0
#define WSSSTA_DHCP_FWD_RULE_ID 2
#define WSSSTA_DHCP_FWD_RULE_NUM 1
#define WSSSTA_DHCP_FWD_RULE_PRIO 0
#define WSSSTA_DELETE_CLIENT_RULE_NUM 1
#endif

