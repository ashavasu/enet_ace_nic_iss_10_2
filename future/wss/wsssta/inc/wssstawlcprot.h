/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *                                                                            *
 * $Id: wssstawlcprot.h,v 1.3 2017/11/24 10:37:11 siva Exp $                                                                      *
 * Description: This file contains the function prototypes for WSSSTA module  *
 ******************************************************************************/
#ifndef _WSSSTAWLC_PROT_H_
#define _WSSSTAWLC_PROT_H_

#include "radioifinc.h"

UINT4 WssStaProcessAuthFrames PROTO((tWssStaMsgStruct *));
VOID WssStaRc4Prng PROTO((UINT1 *));
UINT4 WssStaProcessAssocFrames PROTO((tWssStaMsgStruct *));
UINT4 ValidateAssocFrame PROTO((tDot11AssocReqMacFrame *,
                               tDot11AssocRspMacFrame *,
                               tRadioIfGetDB *, UINT2)); 

UINT4 ValidateReAssocFrame PROTO((tDot11ReassocReqMacFrame *, 
                           tDot11ReassocRspMacFrame *,
                           tRadioIfGetDB *, UINT2));
UINT4 WssStaProcessDisAssocFrames PROTO((tWssStaMsgStruct *));
INT4 WssStaConfigResp PROTO((tWssStaConfigReqInfo *));
UINT4 WssStaProcessDeAuthFrames PROTO((tWssStaMsgStruct *));
UINT4 WssStaProcessReAssocFrames PROTO((tWssStaMsgStruct *));
UINT4 wssauthTmrInit PROTO((VOID));
VOID wssauthTmrInitTmrDesc PROTO((VOID));
VOID WssStaTmrExp PROTO((VOID *));
VOID WssStaAssocTmrExp PROTO((VOID *));
INT4 WssStaRespStartTimer PROTO((UINT1, UINT4));
UINT4 WssStaAssocStartTimer PROTO((tTmrBlk *, UINT1, UINT4));
UINT4 WssStaTmrStop PROTO((tWssStaWepProcessDB *));
UINT4 WssStaInit PROTO((VOID));
INT4  WssStaCompareWepProcessDBRBTree PROTO((tRBElem *, tRBElem *));
INT4  WssStaCompareTCFilterHandleRBTree PROTO((tRBElem *, tRBElem *));
VOID WssStaRecvMemClear PROTO((VOID));
UINT4 WssStaUpdateWepProcessDB PROTO((eProcessDB, tWssStaWepProcessDB *));
VOID WssStaConstructDBMsg(eWssStaUpdateAction, UINT4, tMacAddr,
                           UINT2, UINT2, tWssStaParams *, tWssStaDB *);
INT4 WepProcessDBWalkFn PROTO((tRBElem *, eRBVisit, UINT4, void *, void *));
VOID WssStationRateLimitCb PROTO((tMacAddr, UINT4 *));
UINT4 WssStaTmrExpHandler PROTO((VOID));
UINT4 WssstaDelEntryFromWepDb PROTO((VOID));
VOID WlanSnmpifSendTrap PROTO((UINT1 ,VOID *));
VOID
WssStaUtilStaDbDeleteEntry PROTO(( tMacAddr staMacAddr ));
INT1
WssStaUtilValidateDiscoveryMode PROTO(( tMacAddr ));
UINT1
WssStaSendDeAuthMsg PROTO ((tMacAddr));
INT1
WssStaAclRule PROTO((tWssStaDeleteStation *pWssStaDeleteStation));

UINT4
WssStaUtilMaxClientCount PROTO((UINT4 u4BssIndex , tMacAddr staMacAddr));
#ifdef BAND_SELECT_WANTED
UINT4
WssStaUpdateBandSteerProcessDB PROTO((eProcessDB ,
                          tWssStaBandSteerDB* ));
INT4
WssStaCompareWlcBandSteerDBRBTree PROTO((tRBElem *, tRBElem * ));
#endif


/*UINT4 WssIfProcessWssMacMsg PROTO((tWssMsgTypes, tWssMsgStruct *));*/
/*****************************/
/*  Called from WSS IF       */
/*****************************/
/* Web Auth APIS */
#define  MAX_TCP_OPT_LEN  64
typedef struct WSS_STA_GLOBAL_INFO{
   tOsixTaskId          WssStaRecvTaskId;
   tOsixQId             WssStaMsgQId;
   tOsixTaskId          WssStaProcTaskId;
   UINT1                u1IsStaWebAuthInitialized;
   UINT1                u1IsStaWebAuthProcInitialized;
   UINT1                au1Pad[2];
}tWssStaRecvTaskGlobals;
typedef struct {
    INT4   i4Seq;         /* sequence number */
    INT4   i4Ack;         /* acknowledgement number */
    UINT2  u2Sport;       /* source port */
    UINT2  u2Dport;       /* destination port */
    UINT2  u2Window;      /* window advertisement */
    UINT2  u2Cksum;       /* checksum */
    UINT2  u2Urgptr;                    /* urgent pointer */
    UINT1  u1Hlen;        /* header length in word size including options */
    UINT1  u1Code;        /* control flags */
    UINT1  au1TcpOpts[MAX_TCP_OPT_LEN];  /* TCP Options */
} tWssStaTcpHdr;

typedef struct{
    UINT4 u4ClientCount;
    UINT4 u4ifIndex;
    UINT4 u4AssocStatus;
    UINT4 u4DisconnReason;
    UINT4 u4WlanId;
    UINT4 u4UsedVol;
    UINT4 u4UsedTime;
    UINT4 u4AuthFailure;
    UINT1 au1UserName[32];
    UINT1 au1BaseWtpId[6];
    UINT1 au1CliMac[6];
    UINT1 au1StaMac[6];
    UINT1 au1pad[2];
}tWlanSTATrapInfo;



typedef struct{
    tMacAddr DstMacAddr;
    UINT2  u2EthType;
    tMacAddr SrcMacAddr;
    UINT1    au1Pad[2];
}tDot3EthFrmHdr;
INT4 WssStaWebAuthRecvMemInit PROTO ((VOID));
VOID  WssStaWebAuthPacketOnSocket (INT4);
INT4  WssStaWebAuthSockInit (VOID);
VOID WssStaWebAuthRecvMemClear (VOID);
/*UINT4 WssStaWebAuthRecvMainTaskInit(VOID);*/
INT4 WssStaWebAuthRecvTCPPacket(VOID);
INT4  DbgDumpWssStaPkt (tCRU_BUF_CHAIN_HEADER *, UINT1, UINT2);
INT4 WssStaWebAuthProcAndRespond (tSegment *);
INT4 WssStaAssembleWebRespPkts PROTO ((tDot3EthFrmHdr *, t_IP_HEADER *,
                                       tWssStaTcpHdr *, UINT1, UINT1, UINT1));
INT4 WssStaTxMessage (tCRU_BUF_CHAIN_HEADER *, UINT4);

INT4 WssWebAuthTmrInit PROTO((VOID));
VOID WssWebAuthTmrInitTmrDesc PROTO((VOID));
VOID WssStaWebAuthTmrExp PROTO((VOID *));
INT4 WssStaWebAuthStartTimer (UINT1, UINT4);
UINT2 WsStaIpCalcCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                           UINT4 u4Offset, t_IP_HEADER *, UINT2);
INT4  WssStaHttpGetDate (UINT1 *au1FullDate, INT1 i1FutureFlag);
INT4  WssStaWebAuthDbUpdate(tWssIfIsSetWebAuthDB *, tWssIfWebAuthDB *);
INT4  WssStaWebAuthUsrAuthenticated(UINT1 *, UINT1 *,tMacAddr , UINT1);
INT4 WssStaConfigDNSFwdRule(VOID);
INT4 WssStaConfigDHCPFwdRule(VOID);
INT4 WssStaConfigTCPLiftRule(tMacAddr , UINT2);
INT4 WssStaConfigDefaultRule(tMacAddr);
INT4 WssStaConfigStaFwdRule(tMacAddr,UINT1 *);
INT4 WssStaInitiatePool(VOID);
INT4 WssStaDeleteClientRule(UINT4, UINT4);
UINT2 CalcDataChecksum (UINT1 *, UINT4 ,t_IP_HEADER *, UINT4, UINT1);
INT4 WssStaWebAuthGetPktHdr(UINT1 *, tDot3EthFrmHdr *,
                            t_IP_HEADER *,tWssStaTcpHdr *);
INT4 WssStaRFClientDBDelete(UINT4, tMacAddr);
INT4 WssStaVerifyMaxClientCount(UINT4, UINT4 *,tMacAddr);
INT4 WssStaVerifyRadioMaxClientCount(UINT4, UINT4 *,tMacAddr);
VOID WssUtilConvertBytesToVolumeUsed (INT4 i4UsedBytes , UINT1 *pu1UsedVolume);
VOID WssStaSendWebAuthTrap (tMacAddr staMacAddr);

UINT4 WssStaGetDo111nValues(tRadioIfGetDB *pRadioIfgetDB);
UINT4 WssStaGetAssocId (tWssStaWepProcessDB *);
UINT4 WssFreeAssocId(tWssStaWepProcessDB *);
UINT4 WssStaWtpProfileIdfromBssIfIndex (tWssStaWepProcessDB *,UINT2 *);

VOID WssUtilGetTimeStr (CHR1 * pc1TimeStr);
#endif
