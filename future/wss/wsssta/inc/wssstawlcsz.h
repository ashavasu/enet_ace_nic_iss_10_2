/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved             *
 * $Id: wssstawlcsz.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $          *
 * Description: MempoolIds, Block size information for all the      *
 *              pools in WSSSTA module                              *
 ********************************************************************/

enum {
#ifdef BAND_SELECT_WANTED
    MAX_STA_PER_WLC_SIZING_ID,
#endif
    MAX_WSSSTA_WEPPROCESS_SIZING_ID,
    MAX_WSS_TCFILTER_HANDLE_SIZING_ID,
    WSSSTAWLC_MAX_SIZING_ID
};


#ifdef  _WSSSTAWLCSZ_C
tMemPoolId WSSSTAWLCMemPoolIds[ WSSSTAWLC_MAX_SIZING_ID];
INT4  WssstawlcSizingMemCreateMemPools(VOID);
VOID  WssstawlcSizingMemDeleteMemPools(VOID);
INT4  WssstawlcSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSSTAWLCSZ_C  */
extern tMemPoolId WSSSTAWLCMemPoolIds[ ];
extern INT4  WssstawlcSizingMemCreateMemPools(VOID);
extern VOID  WssstawlcSizingMemDeleteMemPools(VOID);
#endif /*  _WSSSTAWLCSZ_C  */


#ifdef  _WSSSTAWLCSZ_C
tFsModSizingParams FsWSSSTAWLCSizingParams [] = {
#ifdef BAND_SELECT_WANTED
{ "tWssStaBandSteerDB", "MAX_STA_PER_WLC", sizeof(tWssStaBandSteerDB),MAX_STA_PER_WLC, MAX_STA_PER_WLC,0 },
#endif
{ "tWssStaWepProcessDB", "MAX_WSSSTA_WEPPROCESS", sizeof(tWssStaWepProcessDB),MAX_WSSSTA_WEPPROCESS, MAX_WSSSTA_WEPPROCESS,0 },
{ "tWssStaTCFilterHandle", "MAX_WSS_TCFILTER_HANDLE", sizeof(tWssStaTCFilterHandle),MAX_WSS_TCFILTER_HANDLE, MAX_WSS_TCFILTER_HANDLE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSSTAWLCSZ_C  */
extern tFsModSizingParams FsWSSSTAWLCSizingParams [];
#endif /*  _WSSSTAWLCSZ_C  */


