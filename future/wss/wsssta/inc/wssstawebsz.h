/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved             *
 * $Id: wssstawebsz.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $          *
 * Description: MempoolIds, Block size information for all the      *
 *              pools in WSSSTA module                              *
 ********************************************************************/

enum {
    MAX_WSSSTA_PKTBUF_SIZING_ID,
    MAX_WSSSTA_WEBAUTH_SIZING_ID,
    WSSSTAWEB_MAX_SIZING_ID
};


#ifdef  _WSSSTAWEBSZ_C
tMemPoolId WSSSTAWEBMemPoolIds[ WSSSTAWEB_MAX_SIZING_ID];
INT4  WssstawebSizingMemCreateMemPools(VOID);
VOID  WssstawebSizingMemDeleteMemPools(VOID);
INT4  WssstawebSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSSTAWEBSZ_C  */
extern tMemPoolId WSSSTAWEBMemPoolIds[ ];
extern INT4  WssstawebSizingMemCreateMemPools(VOID);
extern VOID  WssstawebSizingMemDeleteMemPools(VOID);
#endif /*  _WSSSTAWEBSZ_C  */


#ifdef  _WSSSTAWEBSZ_C
tFsModSizingParams FsWSSSTAWEBSizingParams [] = {
{ "UINT1[1500]", "MAX_WSSSTA_PKTBUF", sizeof(UINT1[1500]),MAX_WSSSTA_PKTBUF, MAX_WSSSTA_PKTBUF,0 },
{ "tWssStaWebAuthDB", "MAX_WSSSTA_WEBAUTH", sizeof(tWssStaWebAuthDB),MAX_WSSSTA_WEBAUTH, MAX_WSSSTA_WEBAUTH,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSSTAWEBSZ_C  */
extern tFsModSizingParams FsWSSSTAWEBSizingParams [];
#endif /*  _WSSSTAWEBSZ_C  */


