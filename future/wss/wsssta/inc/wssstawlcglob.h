/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssstawlcglob.h,v 1.2 2017/05/23 14:16:58 siva Exp $                                                                            *
 * Description: This file contains the global definition for WSSSTA module    *
 ******************************************************************************/

#ifndef _WSSSTAGLOB_H_
#define _WSSSTAGLOB_H_
#include "wssstawlcmacr.h"
#include "wssstawlcprot.h"
#ifdef __WSSSTA__WLC_CMN_C__
INT4   gu4WssStaDebugMask;
#ifdef BAND_SELECT_WANTED
INT4   gi4StaSysLogId;
#endif
UINT4 gu4StaSysLogId;
#else
extern INT4  gu4WssStaDebugMask;
#ifdef BAND_SELECT_WANTED
extern INT4   gi4StaSysLogId;
#endif
extern UINT4 gu4StaSysLogId;
#endif
#endif /* WSSSTAGLOB_H */

