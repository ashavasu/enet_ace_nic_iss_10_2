/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 * $Id: wssstawlcmacr.h,v 1.3 2017/11/24 10:37:11 siva Exp $                                                                           *
 * Description: This file contains the macros for WSSSTA module               *
 ******************************************************************************/
#ifndef _WSSSTA__WLC_MACR_H_
#define _WSSSTA__WLC_MACR_H_

#define WSSSTA_MEMCPY memcpy
#define WSSSTA_GET_NBYTE(u4Val, pu1PktBuf, u4Len)    \
        do {                                         \
            WSSSTA_MEMCPY (&u4Val,pu1PktBuf,u4Len);  \
            pu1PktBuf += u4Len;                      \
        }while (0)

#define RADIO_ID_SUPP_RATE 1034
#define VENDOR_SPEC_USERROLE_MSGELE_LEN     14
#define VENDOR_SPEC_TYPELEN                  4
#define VENDOR_ID     1111
#define RSNA_C_FLAG_MASK    6
#define WSSSTA_AUTHDB_POOLID WSSSTAWLCMemPoolIds[MAX_WSSSTA_WEPPROCESS_SIZING_ID]
#define WSSSTA_TCFILTER_HANDLE_POOLID WSSSTAWLCMemPoolIds[MAX_WSS_TCFILTER_HANDLE_SIZING_ID]
#ifdef BAND_SELECT_WANTED
#define WSSSTA_BAND_STEER_POOLID WSSSTAWLCMemPoolIds[MAX_STA_PER_WLC_SIZING_ID]
#endif
#define WSSSTA_HTONS       OSIX_HTONS
#define WSSSTA_HTONL       OSIX_HTONL

#define RADIO_ID_SUPP_RATE 1034

/* Web Authentication */
#define WSSSTA_ALLOCATE_CRU_BUF(u4Size, u4Offset) \
            CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)

#define WSSSTA_GET_BUF_LEN(pBuf) \
            CRU_BUF_Get_ChainValidByteCount(pBuf)

#define WSSSTA_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
            CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                                          (u4OffSet), (u4Size))

#define WSSSTA_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
            CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset,\
                    u4Size)


#define WSSSTA_RELEASE_CRU_BUF(pBuf) \
                        CRU_BUF_Release_MsgBufChain ((pBuf), 0)

#define WSSSTA_COPY_TO_BUF(pBuf, pu1Src, u4Offset, u4Size) \
                        CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)(pu1Src),\
                                u4Offset, u4Size)

#define WSSSTA_PKTBUF_POOLID  \
                      WSSSTAWEBMemPoolIds[MAX_WSSSTA_PKTBUF_SIZING_ID]


#define  WSSSTA_PKTBUF_ALLOC_MEM_BLOCK(pu1Block)\
             (pu1Block = \
                        (UINT1 *)(MemAllocMemBlk (WSSSTA_PKTBUF_POOLID)))

#define   WSSSTA_MEMSET            MEMSET

#define WSSSTA_PUT_1BYTE(pu1PktBuf, u1Val) \
            do {                             \
                           *pu1PktBuf = u1Val;           \
                            pu1PktBuf += 1;        \
                        } while(0)

#define WSSSTA_PUT_2BYTES(pu1PktBuf, u2Val)        \
            do {                                    \
                *((UINT2 *) ((VOID*)pu1PktBuf)) = \
                WSSSTA_HTONS(u2Val);\
                               pu1PktBuf += 2;\
                                       } while(0)
#define WSSSTA_PUT_4BYTES(pu1PktBuf, u4Val)       \
            do {                                   \
                *((UINT4 *) ((VOID*)pu1PktBuf)) = \
                WSSSTA_HTONL(u4Val);\
                               pu1PktBuf += 4;\
                                       }while(0)

#define WSSSTA_PUT_NBYTES(pu1PktBuf, pu1PktBuf1, length)  \
            do {                                   \
                           WSSSTA_MEMCPY (pu1PktBuf, &pu1PktBuf1, length); \
                       pu1PktBuf += length;             \
                        }while(0)

#define WSSSTA_GET_2BYTE(u2Val, pu1PktBuf)            \
        do {                                        \
           WSSSTA_MEMCPY (&u2Val, pu1PktBuf, 2);\
           u2Val = (UINT2)OSIX_NTOHS(u2Val);               \
           pu1PktBuf += 2;                   \
        } while(0)

#define WSSSTA_GET_1BYTE(u1Val, pu1PktBuf)            \
        do {                                        \
            u1Val = *pu1PktBuf;              \
           pu1PktBuf += 1;                   \
        } while(0)
#define WSSSTA_GET_NBYTE(u4Val, pu1PktBuf, u4Len)    \
        do {                                         \
            WSSSTA_MEMCPY (&u4Val,pu1PktBuf,u4Len);  \
            pu1PktBuf += u4Len;                      \
        }while (0)
#define WSSSTA_GET_4BYTE(u4Val, pu1PktBuf)              \
        do {                                          \
           WSSSTA_MEMCPY (&u4Val, pu1PktBuf, 4); \
           u4Val = (UINT4)OSIX_NTOHL(u4Val);                 \
           pu1PktBuf += 4;                    \
        } while(0)
#define WSSSTA_SKIP_N_BYTES(u2Val, pu1PktBuf)        \
        do {                                         \
            pu1PktBuf += u2Val;                      \
        }while(0)


#define WSSSTA_KB 1024                     /* Specifies kilobytes*/
#define WSSSTA_MB 1024 * 1024              /* Specifies megabytes*/
#define WSSSTA_GB 1024 * 1024 * 1024       /* Specifies gigabytes*/

#define WSSSTA_TRAP_OID_LEN       11

#define   CLI_OPER_RATEB_1MB   0x2
#define   CLI_OPER_RATEB_2MB   0x4
#define   CLI_OPER_RATEB_5MB   0xb
#define   CLI_BSS_RATEB_1MB   0x82
#define   CLI_BSS_RATEB_2MB   0x84
#define   CLI_BSS_RATEB_5MB   0x8b


#define WSS_WLAN_MAX_USER_PER_WTP        1
#define WSS_WLAN_DISASSOCIATE            2
#define WSS_WLAN_CONNECTION_STATUS       3
#define WSS_WLAN_AUTH_FAILURE            4
#define WSS_WLAN_VOLUME_EXCEEDED         5
#define WSS_WLAN_TIME_EXCEEDED           6

#define WSS_WLAN_STATION_TRAP_ENABLE  1
#define WSS_WLAN_STATION_TRAP_DISABLE 2

#define WSS_WLAN_STATION_DEFAULT_TRAP_STATUS    WSS_WLAN_STATION_TRAP_DISABLE

#define WSS_WLAN_WEB_AUTH_FAIL_TRAP 1
#define WSS_WLAN_PSK_AUTH_FAIL_TRAP 2
#define WSS_WLAN_DOT1X_AUTH_FAIL_TRAP 3
#define WSS_WLAN_STA_CONNECTED       1
#define WSS_WLAN_STA_DISCONNECTED       0
#define WSS_STA_IDLE_TIMEOUT_TRAP   1

#define MAC_ADDR_LENGTH           6
#define MAX_MAC_LENGTH           25
#ifdef PMF_WANTED
#define RSNA_C_FLAG_MASK    6
#endif
#endif
