/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved                      */
/* $Id: wssstawlctrc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $                                                                          */
/* Licensee Aricent Inc.,                                                    */
/*****************************************************************************/
/*    FILE  NAME            : Wssstatrc.h                                    */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : WSSAUTH module                                  */
/*    MODULE NAME           : WSSAUTH module                                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains declarations of traces used */
/*                            in WSSSTA Module.                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*---------------------------------------------------------------------------*/
#ifndef _WSSAUTHTRC_H_
#define _WSSAUTHTRC_H_

#define  WSSAUTH_MOD                ((const char *)"WSSAUTH")

#define WSSAUTH_MASK                WSSAUTH_MGMT_TRC | WSSAUTH_FAILURE_TRC

/* Trace and debug flags */

#define WSSAUTH_FN_ENTRY() \
    MOD_FN_ENTRY (WSSAUTH_MASK, WSSAUTH_ENTRY_TRC,WSSAUTH_MOD)

#define WSSAUTH_FN_EXIT() \
    MOD_FN_EXIT (WSSAUTH_MASK, WSSAUTH_EXIT_TRC,WSSAUTH_MOD)

#define WSSAUTH_PKT_DUMP(mask, pBuf, Length, fmt)                           \
        MOD_PKT_DUMP(WSSAUTH_PKT_DUMP_TRC,mask, WSSAUTH_MOD, pBuf, Length, fmt)
#define WSSAUTH_TRC(mask, fmt)\
      MOD_TRC(WSSAUTH_MASK, mask, WSSAUTH_MOD, fmt)
#define WSSAUTH_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(WSSAUTH_MASK,mask,WSSAUTH_MOD,fmt,arg1)
#define WSSAUTH_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(WSSAUTH_MASK,mask,WSSAUTH_MOD,fmt,arg1,arg2)
#define WSSAUTH_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(WSSAUTH_MASK,mask,WSSAUTH_MOD,fmt,arg1,arg2,arg3)
#define WSSAUTH_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(WSSAUTH_MASK,mask,WSSAUTH_MOD,fmt,arg1,arg2,arg3,arg4)

#endif

