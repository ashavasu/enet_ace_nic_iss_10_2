/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 * $Id: wssstawlcinc.h,v 1.2 2017/11/24 10:37:11 siva Exp $                                                                       *
 * Description: This file contains the include file for WSSSTA module         *
 ******************************************************************************/
#ifndef _WSSSTA__WLC_INC_H_
#define _WSSSTA__WLC_INC_H_

#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "tcp.h"
#include "cli.h"
#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"
#ifdef RSNA_WANTED
#include "rsna.h"
#endif


#ifdef WSSUSER_WANTED
#include "userrole.h"
#endif
#include "radioif.h"
#include "wssmac.h"
#include "capwap.h"
#include "ip.h"
#include "wssifinc.h"
#include "wlchdlr.h"
#include "wsssta.h"
#include "wsscfgcli.h"
#include "wssstawlcmacr.h"
#include "wssstawlcdef.h"
#include "wssstawlcprot.h"
#include "wssstawlccmn.h"
#include "wssstawlctrc.h"
#include "wssstawlcsz.h"
#include "wssstawebsz.h"
#include "wssstawlcglob.h"
#endif

#include "capwaptrc.h"
#include "capwapdef.h"
