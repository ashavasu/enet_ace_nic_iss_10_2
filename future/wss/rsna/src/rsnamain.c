/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnamain.c,v 1.2 2017/05/23 14:16:54 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RsnaMainTask                                     */
/*                                                                           */
/*    Description         : This function will perform following task in RSNA*/
/*                          Module:                                          */
/*                          o  Initialized the RSNA task (Sem, queue,        */
/*                             mempool creation).                            */
/*                          o  Register RSNA Mib with SNMP module            */
/*                          o  Wait for external event and call the          */
/*                             corresponding even handler routines           */
/*                             on receiving the events.                      */
/*                                                                           */
/*    Input(s)            : pi1Arg -  Pointer to input arguments             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
RsnaTaskMain (INT1 *pi1Arg)
{

    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Arg);

    if (RsnaInitModuleStart () != RSNA_SUCCESS)
    {
        /* RSNA Initialization Failed.  Return SUCCESS */
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaTaskMain: RsnaInitModuleStart failed!\n");
        RSNA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Get tht Task Id of the RSNA Module */
    if (RSNA_GET_TASK_ID (SELF, RSNA_TASK_NAME,
                          &(RSNA_TASK_ID)) != OSIX_SUCCESS)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaTaskMain: Get Task ID Failed!\n");
        RSNA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    RegisterFSRSNA ();

    if (OsixQueCrt ((UINT1 *) RSNA_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    RSNA_TASK_QUEUE_DEPTH,
                    &(RSNA_TASK_QUEUE_ID)) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaTaskMain: Osix Queue Crt Failed!\n");
        RSNA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    RSNA_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (RSNA_TASK_ID, RSNA_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if ((u4Events & RSNA_QUEUE_EVENT) != 0)
            {
                RsnaInterfaceHandleQueueEvent ();
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : RsnaEnQFrameToRsnaTask                               */
/*                                                                           */
/* Description        : This enqueues a received frame to Rsna Interface     */
/*                      Queue and sends an event RSNA_QUEUE_EVENT to the     */
/*                      Interface task.                                      */
/*                                                                           */
/* Input(s)           : CRU Buffer, PortIndex, Frame type and protocol       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RsnaEnQFrameToRsnaTask (tWssRSNANotifyParams * pMsg)
{
    if (OsixQueSend (RSNA_TASK_QUEUE_ID, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaEnQFrameToRsnaTask: "
                  "Osix Queue Send Failed!\n");
        return;
    }

    if (OsixEvtSend (RSNA_TASK_ID, RSNA_QUEUE_EVENT) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaEnQFrameToRsnaTask: "
                  "Osix Queue Send Failed!\n");
        return;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : RsnaInterfaceHandleQueueEvent                        */
/*                                                                           */
/* Description        : This Initiates the Action for the Queue Event. It    */
/*                      receives the information from the buffer received    */
/*                      from the Queue and passes it to the Radius Server    */
/*                      Interface to Process It.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
RsnaInterfaceHandleQueueEvent (VOID)
{

    tWssRSNANotifyParams *pMsg = NULL;

    while (OsixQueRecv (RSNA_TASK_QUEUE_ID,
                        (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  " INF: Message received from Queue ... Processing...\n");

        if (pMsg == NULL)
        {
            continue;
        }
        PNAC_LOCK ();
        switch (pMsg->eWssRsnaNotifyType)
        {

            case WSSRSNA_PROFILEIF_ADD:
                RsnaAddPortEntry (pMsg->u4IfIndex);
                break;
            case WSSRSNA_PROFILEIF_DEL:
                RsnaDeletePortEntry (pMsg->u4IfIndex);
                break;
            case WSSRSNA_ASSOC_IND:
                RsnaProcAssocInd (pMsg);
                break;
            case WSSRSNA_REASSOC_IND:
                RsnaProcReAssocInd (pMsg);
                break;
            case WSSRSNA_DISSOC_IND:
            case WSSRSNA_DEAUTH_IND:
                RsnaProcDisDeAuthInd (pMsg, pMsg->eWssRsnaNotifyType);
                break;
            case WSSRSNA_MIC_FAILURE_IND:
                RsnaProcMicFailInd (pMsg);
                break;
            case WSSRSNA_PNAC_KEY_AVAILABLE:

                /* PNAC Key Available Indication  */

                RsnaUtilProcessKeyAvailable ((UINT2) pMsg->u4IfIndex,
                                             pMsg->MLMEPNACKEYIND.au1StaMac,
                                             pMsg->MLMEPNACKEYIND.au1Key);
                break;

            case WSSRSNA_SET_KEY:
                break;
            case WSSRSNA_DEL_KEY:
                break;
            case WSSRSNA_DISSOC_REQ:
                break;
            case WSSRSNA_DEAUTH_REQ:
                break;
            default:
                RSNA_TRC_ARG1 (ALL_FAILURE_TRC,
                               "RsnaInterfaceHandleQueueEvent:- "
                               "Invalid Message Type %d\n",
                               pMsg->eWssRsnaNotifyType);
                break;
        }
        MemReleaseMemBlock (RSNA_WSS_INFO_MEMPOOL_ID, (UINT1 *) pMsg);

        PNAC_UNLOCK ();
    }
}

/****************************************************************************
*                                                                           *
* Function     : RsnaMainFsmStart                                           *
*                                                                           *
* Description  : Function to execute the Key Handshake FSM                  *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaMainFsmStart (tRsnaSessionNode * pRsnaSessNode)
{

    tRsnaGlobalGtk     *pRsnaGlobalGtk = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tPnacAuthSessionNode *pPnacAuthSessionNode = NULL;
    UINT1               au1StaMacAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

    if ((pRsnaSessNode->pRsnaSupInfo == NULL) ||
        (pRsnaSessNode->pPnacAuthSessionNode == NULL) ||
        (pRsnaSessNode->pRsnaPaePortInfo == NULL))
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaMainFsmStart:- Failed !!! \n ");

        return (RSNA_FAILURE);
    }

    pRsnaPaePortInfo = pRsnaSessNode->pRsnaPaePortInfo;
    pPnacAuthSessionNode = pRsnaSessNode->pPnacAuthSessionNode;

    MEMCPY (au1StaMacAddr,
            (UINT1 *) &(pPnacAuthSessionNode->rmtSuppMacAddr),
            RSNA_ETH_ADDR_LEN);

    pRsnaGlobalGtk = &(pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk);

    do
    {
        pRsnaSessNode->bIsStateChanged = FALSE;
        pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bIsStateChanged = FALSE;

        if (RsnaMain4WayHandShakeFsm (pRsnaSessNode) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaMain4WayHandShakeFsm failed !!! \n");
            return (RSNA_FAILURE);
        }

        if (RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaMacAddr) ==
            NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaMainFsmStart:- Station Not Alive !!! \n ");

            RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                           "for MAC:%x:%x:%x:%x:%x:%x\n",
                           au1StaMacAddr[0],
                           au1StaMacAddr[1],
                           au1StaMacAddr[2],
                           au1StaMacAddr[3],
                           au1StaMacAddr[4], au1StaMacAddr[5]);

            return (RSNA_SUCCESS);

        }
        if (RsnaMainPerStaGrpKeyFsm (pRsnaSessNode) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaMainPerStaGrpKeyFsm failed !!! ");
            return (RSNA_FAILURE);
        }
        if (RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaMacAddr) ==
            NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaMainFsmStart:- Station Not Alive !!! \n");

            RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                           "for MAC:%x:%x:%x:%x:%x:%x\n",
                           au1StaMacAddr[0],
                           au1StaMacAddr[1],
                           au1StaMacAddr[2],
                           au1StaMacAddr[3],
                           au1StaMacAddr[4], au1StaMacAddr[5]);

            return (RSNA_SUCCESS);

        }
        if (RsnaMainGlobalGrpKeyFsm (pRsnaGlobalGtk) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaMainGlobalGrpKeyFsm failed !!! ");
            return (RSNA_FAILURE);
        }
        if (RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaMacAddr) ==
            NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaMainFsmStart:- Station Not Alive !!! \n ");

            RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                           "for MAC:%x:%x:%x:%x:%x:%x\n",
                           au1StaMacAddr[0],
                           au1StaMacAddr[1],
                           au1StaMacAddr[2],
                           au1StaMacAddr[3],
                           au1StaMacAddr[4], au1StaMacAddr[5]);

            return (RSNA_SUCCESS);

        }

    }
    while ((pRsnaSessNode->bIsStateChanged == TRUE) ||
           (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bIsStateChanged ==
            TRUE));

    return (RSNA_SUCCESS);
}

/****************************************************************************
* Function     : RsnaMain4WayHandShakeFsm                                   *
*                                                                           *
* Description  : Function to execute 4-Way handshake FSM                    *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaMain4WayHandShakeFsm (tRsnaSessionNode * pRsnaSessNode)
{
    if ((pRsnaSessNode->pRsnaSupInfo == NULL) ||
        (pRsnaSessNode->pPnacAuthSessionNode == NULL) ||
        (pRsnaSessNode->pRsnaPaePortInfo == NULL))
    {
        return (RSNA_FAILURE);
    }

    if (pRsnaSessNode->bRsnaInitStateMachine == FALSE)
    {
        return (RSNA_SUCCESS);
    }
    if (pRsnaSessNode->bInit == TRUE)
    {
        RsnaCorePtkInitializeState (pRsnaSessNode);
    }
    else if (pRsnaSessNode->bDisconnect == TRUE)
    {
        RsnaCorePtkDisconnectState (pRsnaSessNode);
    }
    else if (pRsnaSessNode->bDeAuthReq == TRUE)
    {
        RsnaCorePtkDisconnectedState (pRsnaSessNode);
    }
    else if (pRsnaSessNode->bAuthenticationRequest == TRUE)
    {
        RsnaCorePtkAuthenticationState (pRsnaSessNode);
    }
    else if (pRsnaSessNode->bReAutenticationRequest == TRUE)
    {
        RsnaCorePtkAuthentication2State (pRsnaSessNode);
    }
    else if (pRsnaSessNode->bPTKRequest == TRUE)
    {
        RsnaCorePtkStartState (pRsnaSessNode);
    }
    else
    {

        switch (pRsnaSessNode->u1PtkFsmState)
        {
            case RSNA_4WAY_INITIALIZE:
                break;
            case RSNA_4WAY_DISCONNECT:
                RsnaCorePtkDisconnectedState (pRsnaSessNode);
                break;
            case RSNA_4WAY_DISCONNECTED:
                RsnaCorePtkInitializeState (pRsnaSessNode);
                break;
            case RSNA_4WAY_AUTHENTICATION:
                RsnaCorePtkAuthentication2State (pRsnaSessNode);
                break;
            case RSNA_4WAY_AUTHENTICATION2:
                if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                             RSNA_AKM_UNSPEC_802_1X,
                             RSNA_CIPHER_SUITE_LEN) == 0) ||
                    (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                             WPA_AKM_UNSPEC_802_1X,
                             RSNA_CIPHER_SUITE_LEN) == 0))
                {
                    if (pRsnaSessNode->pPnacAuthSessionNode == NULL)
                    {
                        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                                  "RsnaMain4WayHandShakeFsm:- Waiting "
                                  "for Pnac Authentication to complete !!! \n");
                        return (RSNA_SUCCESS);
                    }
                    else if (pRsnaSessNode->pPnacAuthSessionNode->
                             authFsmInfo.bKeyRun == FALSE)
                    {
                        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                                  "RsnaMain4WayHandShakeFsm:- Waiting "
                                  "for Key from Pnac !!! \n");
                        return (RSNA_SUCCESS);
                    }
                    else
                    {
                        RsnaCoreInitPmkState (pRsnaSessNode);
                    }
                }
                else if ((MEMCMP (&pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                                  RSNA_AKM_PSK_OVER_802_1X,
                                  RSNA_CIPHER_SUITE_LEN) == 0) ||
                         (MEMCMP (&pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                                  WPA_AKM_PSK_OVER_802_1X,
                                  RSNA_CIPHER_SUITE_LEN) == 0)
#ifdef PMF_WANTED
                         || (MEMCMP (&pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                                     RSNA_AKM_PSK_SHA256_OVER_802_1X,
                                     RSNA_CIPHER_SUITE_LEN) == 0)
#endif
                    )
                {
                    RsnaCoreInitPskState (pRsnaSessNode);
                }
                break;
            case RSNA_4WAY_INITPMK:
                if (pRsnaSessNode->pPnacAuthSessionNode->authFsmInfo.
                    bKeyAvailable == TRUE)
                {
                    RsnaCorePtkStartState (pRsnaSessNode);
                }
                else
                {
                    pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                        u4NoOf4WayHandshakeFailures++;
                    RsnaCorePtkDisconnectState (pRsnaSessNode);
                }
                break;
            case RSNA_4WAY_INITPSK:
                if (pRsnaSessNode->pRsnaPaePortInfo->
                    RsnaConfigDB.bPskStatus == TRUE)
                {
                    RsnaCorePtkStartState (pRsnaSessNode);
                }
                else
                {
                    pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                        u4NoOf4WayHandshakeFailures++;
                    RsnaCorePtkDisconnectState (pRsnaSessNode);
                }
                break;
            case RSNA_4WAY_PTKSTART:
                if ((pRsnaSessNode->bEAPOLKeyReceived == TRUE) &&
                    (pRsnaSessNode->bEAPOLKeyRequest == FALSE) &&
                    (pRsnaSessNode->bEAPOLKeyPairwise == TRUE))
                {
                    if (RsnaCorePtkCalcNegotiatingState (pRsnaSessNode) !=
                        RSNA_SUCCESS)
                    {
                        return (RSNA_FAILURE);
                    }
                }
                else if (pRsnaSessNode->u4TimeOutCtr > pRsnaSessNode->
                         pRsnaPaePortInfo->RsnaConfigDB.u4PairwiseUpdateCount)
                {
                    pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                        u4NoOf4WayHandshakeFailures++;
                    RsnaCorePtkDisconnectState (pRsnaSessNode);
                }
                else if (pRsnaSessNode->bTimeOutEvt == TRUE)
                {
                    RsnaCorePtkStartState (pRsnaSessNode);
                }
                break;
            case RSNA_4WAY_PTKCALCNEGOTIATING:
                if (pRsnaSessNode->bMICVerified == TRUE)
                {
                    RsnaCorePtkCalcNegotiating2State (pRsnaSessNode);
                }
                else if ((pRsnaSessNode->bEAPOLKeyReceived == TRUE) &&
                         (pRsnaSessNode->bEAPOLKeyRequest == FALSE) &&
                         (pRsnaSessNode->bEAPOLKeyPairwise == TRUE))
                {
                    RsnaCorePtkCalcNegotiatingState (pRsnaSessNode);
                }
                else if (pRsnaSessNode->bTimeOutEvt == TRUE)
                {
                    RsnaCorePtkStartState (pRsnaSessNode);
                }
                break;
            case RSNA_4WAY_PTKCALCNEGOTIATING2:
                RsnaCorePtkInitNegotiatingState (pRsnaSessNode);
                break;
            case RSNA_4WAY_INITNEGOTIATING:
                if ((pRsnaSessNode->bEAPOLKeyReceived == TRUE) &&
                    (pRsnaSessNode->bEAPOLKeyRequest == FALSE) &&
                    (pRsnaSessNode->bEAPOLKeyPairwise == TRUE) &&
                    (pRsnaSessNode->bMICVerified == TRUE))
                {
                    RsnaCorePtkInitDoneState (pRsnaSessNode);
                }
                else if (pRsnaSessNode->u4TimeOutCtr >
                         pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                         u4PairwiseUpdateCount)
                {
                    RsnaCorePtkDisconnectState (pRsnaSessNode);
                }
                else if (pRsnaSessNode->bTimeOutEvt == TRUE)
                {
                    RsnaCorePtkInitNegotiatingState (pRsnaSessNode);
                }
                break;
            case RSNA_4WAY_PTKINITDONE:
                break;
            default:
                return (RSNA_FAILURE);
        }
    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaMainPerStaGrpKeyFsm                                    *
*                                                                           *
* Description  : Function to execute Per Sta Group Key State Machine        *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaMainPerStaGrpKeyFsm (tRsnaSessionNode * pRsnaSessNode)
{

    if ((pRsnaSessNode->pRsnaSupInfo == NULL) ||
        (pRsnaSessNode->pPnacAuthSessionNode == NULL) ||
        (pRsnaSessNode->pRsnaPaePortInfo == NULL))
    {
        return (RSNA_FAILURE);
    }

    if (pRsnaSessNode->bInit == TRUE)
    {
        RsnaCorePtkGrpKeyIdleState (pRsnaSessNode);
    }
    else
    {
        switch (pRsnaSessNode->u1PerStaGtkFsm)
        {
            case RSNA_PER_STA_GKH_IDLE:
                if ((pRsnaSessNode->bGUpdateStationKeys == TRUE) ||
                    (pRsnaSessNode->bPInitAkeys == OSIX_TRUE))
                {
                    RsnaCorePtkGrpReKeyNegoState (pRsnaSessNode);
                }
                break;
            case RSNA_PER_STA_GKH_REKEYNEGOTIATING:
                if ((pRsnaSessNode->bEAPOLKeyReceived == TRUE) &&
                    (pRsnaSessNode->bEAPOLKeyRequest == FALSE) &&
                    (pRsnaSessNode->bEAPOLKeyPairwise == FALSE) &&
                    (pRsnaSessNode->bMICVerified == TRUE))
                {
                    RsnaCorePtkGrpReKeyEstState (pRsnaSessNode);
                }
                else if (pRsnaSessNode->u4GTimeoutCtr >
                         pRsnaSessNode->pRsnaPaePortInfo->
                         RsnaConfigDB.u4GroupUpdateCount)
                {
                    RsnaCorePtkGrpKeyErrorState (pRsnaSessNode);
                }
                else if (pRsnaSessNode->bTimeOutEvt == TRUE)
                {
                    RsnaCorePtkGrpReKeyNegoState (pRsnaSessNode);
                }
                break;
            case RSNA_PER_STA_GKH_REKEYESTABLISHED:
                RsnaCorePtkGrpKeyIdleState (pRsnaSessNode);
                break;
            case RSNA_PER_STA_GKH_KEYERROR:
                RsnaCorePtkGrpKeyIdleState (pRsnaSessNode);
                break;
            default:
                return (RSNA_FAILURE);
        }
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     :RsnaMainGlobalGrpKeyFsm                                     *
*                                                                           *
* Description  : Function to execute Global Group Key State machine         *
*                                                                           *
* Input        : pRsnaGlobalGtk :- Pointer to rsna global gtk structure     *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaMainGlobalGrpKeyFsm (tRsnaGlobalGtk * pRsnaGlobalGtk)
{

    if (pRsnaGlobalGtk->bGInit == TRUE)
    {
        RsnaCoreGlobalGrpKeyInitState (pRsnaGlobalGtk);
    }
    switch (pRsnaGlobalGtk->u1Fsm)
    {
        case RSNA_GLOBAL_GKH_INIT:
            if (pRsnaGlobalGtk->bGTKAuthenticator == TRUE)
            {
                RsnaCoreGlobalGrpSetKeyDoneState (pRsnaGlobalGtk);
            }
            break;
        case RSNA_GLOBAL_GKH_SETKEYS:
            if (pRsnaGlobalGtk->u4GkeyDoneStations == 0)
            {
                RsnaCoreGlobalGrpSetKeyDoneState (pRsnaGlobalGtk);
            }
            else if (pRsnaGlobalGtk->bGtkRekeyEnable == TRUE)
            {
                RsnaCoreGlobalGrpSetKeysState (pRsnaGlobalGtk);
            }
            break;
        case RSNA_GLOBAL_SETKEYSDONE:
            if (pRsnaGlobalGtk->bGtkRekeyEnable == TRUE)
            {
                RsnaCoreGlobalGrpSetKeysState (pRsnaGlobalGtk);
            }
            break;
        default:
            return (RSNA_FAILURE);
    }

    return (RSNA_SUCCESS);

}
