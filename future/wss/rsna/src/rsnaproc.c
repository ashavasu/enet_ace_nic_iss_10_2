/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaproc.c,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"

/****************************************************************************
*                                                                           *
* Function     : RsnaProcessEapolKeyFrame                                   *
*                                                                           *
* Description  : Function to process Eapol Key Frame                        *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaProcEapolKeyFrame (tRsnaSessionNode * pRsnaSessNode)
{
    tEapolKeyFrame     *pEapolKeyFrame = NULL;
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;
    eExch               ExchType;
    UINT1              *pu1Pkt = NULL;
    UINT2               u2PktLen = 0;
    UINT2               u2KeyInfo = 0;
    UINT2               u2KeyDataLen = 0;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    CONST CHR1         *pu1MsgTxt = NULL;
    UINT4               u4TimeStamp = 0;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    UINT4               u4MacAddr = 0;
    UINT2               u2MacAddr = 0;
    UINT2               u2Mask = 0;
    UINT2               u2GrpMask = 0;
    UINT2               u2ReqMask = 0;

    pu1Pkt = pRsnaSessNode->pu1RxEapolKeyPkt;

    if (pu1Pkt == NULL)
    {
        return (RSNA_FAILURE);
    }

    u2PktLen = pRsnaSessNode->u2PktLen;

    if (u2PktLen < (RSNA_EAPOL_HDR_SIZE + sizeof (tEapolKeyFrame)))
    {
        return (RSNA_FAILURE);
    }

    pRsnaSupInfo = pRsnaSessNode->pRsnaSupInfo;

    if (pRsnaSupInfo == NULL)
    {
        return (RSNA_FAILURE);
    }

    pRsnaPaePortInfo = pRsnaSessNode->pRsnaPaePortInfo;

    if (pRsnaPaePortInfo == NULL)
    {
        return (RSNA_FAILURE);
    }

    pEapolKeyFrame = (tEapolKeyFrame *) (pu1Pkt + RSNA_EAPOL_HDR_SIZE);
    u2KeyInfo = OSIX_NTOHS (pEapolKeyFrame->u2KeyInfo);

    u2KeyDataLen = OSIX_NTOHS (pEapolKeyFrame->u2KeyDataLen);
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_REQUEST);
    u2GrpMask = (u2KeyInfo & RSNA_KEY_INFO_KEY_TYPE);
    if (u2Mask == RSNA_KEY_INFO_REQUEST)
    {
        ExchType = REQUEST;
        pu1MsgTxt = "Request";
    }
    else if (u2GrpMask == FALSE)
    {
        ExchType = GROUP_2;
        pu1MsgTxt = "2/2 Group";
    }
    else if (u2KeyDataLen == 0)
    {
        ExchType = PAIRWISE_4;
        pu1MsgTxt = "4/4 Pairwise";
    }
    else
    {
        ExchType = PAIRWISE_2;
        pu1MsgTxt = "2/4 Pairwise";
    }

    RSNA_TRC_ARG1 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   "Received %s ", pu1MsgTxt);
    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   "for Station MAC:%x:%x:%x:%x:%x:%x\n",
                   pRsnaSupInfo->au1SuppMacAddr[0],
                   pRsnaSupInfo->au1SuppMacAddr[1],
                   pRsnaSupInfo->au1SuppMacAddr[2],
                   pRsnaSupInfo->au1SuppMacAddr[3],
                   pRsnaSupInfo->au1SuppMacAddr[4],
                   pRsnaSupInfo->au1SuppMacAddr[5]);

    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_REQUEST);
    if (u2Mask == RSNA_KEY_INFO_REQUEST)
    {
        if ((pRsnaSupInfo->bReqReplayCtrValid == TRUE) &&
            (MEMCMP (pEapolKeyFrame->au1KeyReplayCtr,
                     pRsnaSupInfo->au1ReqReplayCtr, RSNA_REPLAY_CTR_LEN) <= 0))
        {
            RSNA_GET_FOURBYTE (u4MacAddr, pRsnaSupInfo->au1SuppMacAddr);
            RSNA_GET_TWOBYTE (u2MacAddr, (pRsnaSupInfo->au1SuppMacAddr +
                                          sizeof (UINT4)));
            RSNA_TRC_ARG2 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                           "received EAPOL-Key request from station -"
                           "%x - x with replayed counter \n",
                           u4MacAddr, u2MacAddr);

            pRsnaSuppStatsInfo =
                RsnaUtilGetStationFromStatsTable (pRsnaSessNode->
                                                  pRsnaPaePortInfo->u2Port,
                                                  pRsnaSessNode->pRsnaSupInfo->
                                                  au1SuppMacAddr);

            if (pRsnaSuppStatsInfo != NULL)
            {
                if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                             RSNA_CIPHER_SUITE_TKIP,
                             RSNA_CIPHER_SUITE_LEN)) == 0
                    ||
                    (MEMCMP
                     (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0)
                {
                    pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipReplays++;
                }
                else if ((MEMCMP
                          (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                           RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN)) == 0
                         ||
                         (MEMCMP
                          (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                           WPA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN)) == 0)
                {
                    pRsnaSuppStatsInfo->RsnaStatsDB.u4CCMReplays++;
                }
            }

            pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                u4NoOf4WayHandshakeFailures++;
            return (RSNA_FAILURE);
        }
    }
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_REQUEST);
    if ((u2Mask == FALSE) &&
        ((pRsnaSessNode->bIsKeyReplayCtrValid == FALSE) ||
         (MEMCMP (pEapolKeyFrame->au1KeyReplayCtr,
                  pRsnaSessNode->au1RsnaKeyReplayCtr,
                  RSNA_REPLAY_CTR_LEN) != 0)))
    {
        RSNA_TRC_ARG1 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                       "received EAPOL-Key %s with unexpected replay "
                       "counter", pu1MsgTxt);

        pRsnaSuppStatsInfo =
            RsnaUtilGetStationFromStatsTable (pRsnaSessNode->pRsnaPaePortInfo->
                                              u2Port,
                                              pRsnaSessNode->pRsnaSupInfo->
                                              au1SuppMacAddr);

        if (pRsnaSuppStatsInfo != NULL)
        {
            if (pRsnaSuppStatsInfo != NULL)
            {
                if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                             RSNA_CIPHER_SUITE_TKIP,
                             RSNA_CIPHER_SUITE_LEN)) == 0
                    ||
                    (MEMCMP
                     (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0)
                {
                    pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipReplays++;
                }
                else if ((MEMCMP
                          (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                           RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN)) == 0
                         ||
                         (MEMCMP
                          (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                           WPA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN)) == 0)
                {
                    pRsnaSuppStatsInfo->RsnaStatsDB.u4CCMReplays++;
                }
            }
        }

        pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
            u4NoOf4WayHandshakeFailures++;
        return (RSNA_FAILURE);
    }

    switch (ExchType)
    {
        case PAIRWISE_2:
            if ((pRsnaSessNode->u1PtkFsmState != RSNA_4WAY_PTKSTART) &&
                (pRsnaSessNode->u1PtkFsmState != RSNA_4WAY_PTKCALCNEGOTIATING))
            {
                RSNA_TRC_ARG1 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                               "Recvd EAPOL-Key msg 2/4 in invalid "
                               "state %d - Discarded",
                               pRsnaSessNode->u1PtkFsmState);
                pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                    u4NoOf4WayHandshakeFailures++;
                return (RSNA_FAILURE);
            }
            if ((pRsnaSupInfo->u1IELen != u2KeyDataLen) ||
                (MEMCMP (pRsnaSupInfo->au1RsnaIE,
                         (pu1Pkt + (RSNA_EAPOL_HDR_SIZE +
                                    sizeof (tEapolKeyFrame))),
                         u2KeyDataLen) != 0))
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "IE from (Re)Assoc Req did not match with" "msg 2/4");
                pRsnaSessNode->u4ReasonCode = RSNA_REASON_IE_IN_4WAY_DIFFERS;
                pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                    u4NoOf4WayHandshakeFailures++;
                RsnaSessDisconnectSta (pRsnaSessNode);
                return (RSNA_FAILURE);
            }

            break;
        case PAIRWISE_4:
            if ((pRsnaSessNode->u1PtkFsmState != RSNA_4WAY_INITNEGOTIATING) ||
                (pRsnaSessNode->bPtkValid == FALSE))
            {
                RSNA_TRC_ARG1 (RSNA_CONTROL_PATH_TRC |
                               RSNA_ALL_FAILURE_TRC,
                               "Recvd EAPOL-Key msg 4/4 in invalid "
                               "state %d - Discarded",
                               pRsnaSessNode->u1PtkFsmState);
                pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                    u4NoOf4WayHandshakeFailures++;
                return (RSNA_FAILURE);
            }
            break;
        case GROUP_2:
            if ((pRsnaSessNode->u1PerStaGtkFsm !=
                 RSNA_PER_STA_GKH_REKEYNEGOTIATING) ||
                (pRsnaSessNode->bPtkValid == FALSE))
            {
                RSNA_TRC_ARG1 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                               "Recvd EAPOL-Key msg 4/4 in invalid "
                               "state %d - Discarded",
                               pRsnaSessNode->u1PerStaGtkFsm);
                pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                    u4NoOf4WayHandshakeFailures++;
                return (RSNA_FAILURE);

            }
            pRsnaPaePortInfo->u4GroupHandshakeCounter++;
            break;
        case REQUEST:
            /* Do Nothing */
            break;
        default:                /* REQUEST */
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Recvd Eapol Request !!! \n");
            break;
    }
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_ACK);
    if (u2Mask == RSNA_KEY_INFO_ACK)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "Recvd Invalid Eapol Key Frame:- Ack Bit Set");
        pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
            u4NoOf4WayHandshakeFailures++;
        return (RSNA_FAILURE);
    }
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_MIC);
    if (u2Mask == FALSE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "Recvd Invalid Eapol Key Frame:- Mic Bit Not Set");
        pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
            u4NoOf4WayHandshakeFailures++;
        return (RSNA_FAILURE);

    }
    pRsnaSessNode->bMICVerified = FALSE;
    if (pRsnaSessNode->bPtkValid == TRUE)
    {
        if (RsnaProcVerifyKeyMic (&(pRsnaSessNode->PtkSaDB), pu1Pkt,
                                  u2PktLen) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Recvd EAPOL Ley Frame with Invalid Mic");

            pRsnaSuppStatsInfo =
                RsnaUtilGetStationFromStatsTable (pRsnaSessNode->
                                                  pRsnaPaePortInfo->u2Port,
                                                  pRsnaSessNode->pRsnaSupInfo->
                                                  au1SuppMacAddr);

            if (pRsnaSuppStatsInfo != NULL)
            {
                if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                             RSNA_CIPHER_SUITE_TKIP,
                             RSNA_CIPHER_SUITE_LEN)) == 0
                    ||
                    (MEMCMP
                     (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0)

                {
                    pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipLocalMicFailures++;
                }
            }

            pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                u4NoOf4WayHandshakeFailures++;
            return (RSNA_FAILURE);
        }
        pRsnaSessNode->bMICVerified = TRUE;
    }
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_REQUEST);
    if (u2Mask == RSNA_KEY_INFO_REQUEST)
    {
        if (pRsnaSessNode->bMICVerified == TRUE)
        {
            pRsnaSupInfo->bReqReplayCtrValid = TRUE;
            MEMCPY (pRsnaSupInfo->au1ReqReplayCtr,
                    pEapolKeyFrame->au1KeyReplayCtr, RSNA_REPLAY_CTR_LEN);
        }
        else
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Recvd Eapol Key Frame with Inavlid Mic");
            pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                u4NoOf4WayHandshakeFailures++;
            return (RSNA_FAILURE);
        }
        u2Mask = (u2KeyInfo & RSNA_KEY_INFO_ERROR);
        u2ReqMask = (u2KeyInfo & RSNA_KEY_INFO_KEY_TYPE);
        if (u2Mask == RSNA_KEY_INFO_ERROR)
        {

            pRsnaSuppStatsInfo =
                RsnaUtilGetStationFromStatsTable (pRsnaSessNode->
                                                  pRsnaPaePortInfo->u2Port,
                                                  pRsnaSessNode->pRsnaSupInfo->
                                                  au1SuppMacAddr);

            if (pRsnaSuppStatsInfo != NULL)
            {
                if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                             RSNA_CIPHER_SUITE_TKIP,
                             RSNA_CIPHER_SUITE_LEN)) == 0
                    ||
                    (MEMCMP
                     (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0)
                {
                    pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipRemoteMicFailures++;
                }
            }

            pRsnaSupInfo->RsnaStatsDB.u4TkipRemoteMicFailures++;
            OsixGetSysTime (&u4TimeStamp);

            if (u4TimeStamp >
                (pRsnaPaePortInfo->u4MichaelMicFailureTimeStamp +
                 RSNA_MIC_FAIL_CTR_MSR_INTRVL))
            {
                pRsnaPaePortInfo->u4MichaleMicFailurecount = 1;
            }
            else
            {
                pRsnaPaePortInfo->u4MichaleMicFailurecount++;
                if (pRsnaPaePortInfo->u4MichaleMicFailurecount > 1)
                {
                    RsnaUtilStartTkipCtrMsrs (pRsnaPaePortInfo);
                }
            }
            pRsnaPaePortInfo->u4MichaelMicFailureTimeStamp = u4TimeStamp;
            RsnaSessReqNewPtk (pRsnaSessNode);
        }
        else if (u2ReqMask == RSNA_KEY_INFO_KEY_TYPE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Recvd a Eapol Req for new 4-Way Key Handshake !!!");
            RsnaSessReqNewPtk (pRsnaSessNode);
        }
        else
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Recvd Req for GTK Rekeying !!! ");
            RsnaSessReqNewPtk (pRsnaSessNode);
            RsnaTmrStopTimer (&
                              (pRsnaPaePortInfo->RsnaGlobalGtk.
                               RsnaUtilGtkRekeyTmr));
            RsnaTmrStartTimer (&
                               (pRsnaPaePortInfo->RsnaGlobalGtk.
                                RsnaUtilGtkRekeyTmr), RSNA_GTK_TIMER_ID,
                               pRsnaPaePortInfo->RsnaConfigDB.
                               u4GroupGtkRekeyTime, pRsnaSessNode);
        }
    }
    else
    {
        pRsnaSessNode->bIsKeyReplayCtrValid = FALSE;
    }
    pRsnaSessNode->bEAPOLKeyReceived = TRUE;
    u2ReqMask = (u2KeyInfo & RSNA_KEY_INFO_KEY_TYPE);
    if (u2ReqMask == RSNA_KEY_INFO_KEY_TYPE)
    {
        pRsnaSessNode->bEAPOLKeyPairwise = TRUE;
    }
    else
    {
        pRsnaSessNode->bEAPOLKeyPairwise = FALSE;
    }
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_REQUEST);
    if (u2Mask == RSNA_KEY_INFO_REQUEST)
    {
        pRsnaSessNode->bEAPOLKeyRequest = TRUE;
    }
    else
    {
        pRsnaSessNode->bEAPOLKeyRequest = FALSE;
    }
    MEMCPY (pRsnaSessNode->au1SNonce, pEapolKeyFrame->au1KeyNonce,
            RSNA_NONCE_LEN);

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcVerifyKeyMic                                       *
*                                                                           *
* Description  : Function to verify the mic                                 *
*                                                                           *
* Input        : pPtkSaDB :- Points to ptssa db                             *
*                pu1Pkt   :- Points to the packet                           *
*                u2PktLen :- Length of the pkt recvd                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaProcVerifyKeyMic (tRsnaPtkSa * pPtkSaDB, UINT1 *pu1Pkt, UINT2 u2PktLen)
{

    tEapolKeyFrame     *pEapolKeyFrame = NULL;
    UINT2               u2KeyInfo = 0;
    UINT1               au1Digest[RSNA_MIC_LEN] = "";

    MEMSET (au1Digest, 0, RSNA_MIC_LEN);

    if (u2PktLen < (RSNA_EAPOL_HDR_SIZE + sizeof (tEapolKeyFrame)))
    {
        return (RSNA_FAILURE);
    }

    pEapolKeyFrame = (tEapolKeyFrame *) (pu1Pkt + RSNA_EAPOL_HDR_SIZE);
    u2KeyInfo = pEapolKeyFrame->u2KeyInfo;
    u2KeyInfo = OSIX_NTOHS (u2KeyInfo);

    MEMCPY (au1Digest, pEapolKeyFrame->au1KeyMic, RSNA_MIC_LEN);
    MEMSET (pEapolKeyFrame->au1KeyMic, 0, RSNA_MIC_LEN);

    if (RsnaAlgoComputeMic (u2KeyInfo, pPtkSaDB->au1Kck, pu1Pkt, u2PktLen,
                            pEapolKeyFrame->au1KeyMic) != RSNA_SUCCESS)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "Verify Key Mic:- RsnaAlgoComputeMic "
                  "returned Failure !!!!!");
        return (RSNA_FAILURE);
    }

    if (MEMCMP (au1Digest, pEapolKeyFrame->au1KeyMic, RSNA_MIC_LEN) != 0)
    {
        return (RSNA_FAILURE);
    }

    return (RSNA_SUCCESS);
}
