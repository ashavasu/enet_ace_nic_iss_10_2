/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaapme.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"
#include "rsnaprot.h"
#if 0

PRIVATE VOID        RsnaApmeProcMlmeMicFailInd (tApmeToRsnaMlmeMsg *
                                                pApmeToRsnaMlmeMsg);
PRIVATE VOID        RsnaApmeProcMlmeAssocInd (tApmeToRsnaMlmeMsg *
                                              pApmeToRsnaMlmeMsg);
PRIVATE VOID        RsnaApmeProcMlmeReAssocInd (tApmeToRsnaMlmeMsg *
                                                pApmeToRsnaMlmeMsg);
PRIVATE VOID        RsnaApmeProcMlmeDisDeAuthInd (tApmeToRsnaMlmeMsg *
                                                  pApmeToRsnaMlmeMsg);

/*PRIVATE UINT1       gau1RsnaBroadCast[] =
   { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };*/
/****************************************************************************
*                                                                           *
* Function     : RsnaApmeEnqRsnaMlmeMsgToPnacTask                           *
*                                                                           *
* Description  : Function to Post Apme Message to Pnac Task                 *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApmeEnqRsnaMlmeMsgToPnacTask (tApmeToRsnaMlmeMsg * pApmeToRsnaMlmeMsg)
{

    tPnacIntfMesg      *pMesg = NULL;


#if 1 /* RAJA */

    pMesg = (tPnacIntfMesg *)  PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);
    if (pMesg == NULL)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " Allocate mem block for ApmeMsg to Rsna failed\n");
        return RSNA_FAILURE;
    }

#else

    if ((PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, &pMesg)
         == MEM_FAILURE) || (pMesg == NULL))
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " Allocate mem block for ApmeMsg to Rsna failed\n");
        return RSNA_FAILURE;
    }

#endif

    pMesg->u4MesgType = PNAC_RSNA_MLME_MSG_FROM_APME;
    pMesg->Mesg.pApmeToRsnaMlmeMsg = (VOID *) pApmeToRsnaMlmeMsg;

#if 1 /* RAJA */

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg, OSIX_DEF_MSG_LEN) == 
                                        PNAC_OSIX_SUCCESS)

#else
    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_QUEUE_NAME, pMesg) == OSIX_SUCCESS)
#endif
    {

#if 1 /* RAJA */
        if ( PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,PNAC_QUEUE_EVENT) == OSIX_SUCCESS )
#else
        if (PNAC_SEND_EVENT (SELF, PNAC_INTF_TASK_NAME, PNAC_QUEUE_EVENT)
            == OSIX_SUCCESS)
#endif
        {

            /* Success */
            return RSNA_SUCCESS;
        }

        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_OS_RESOURCE_TRC |
                  RSNA_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return RSNA_FAILURE;
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeEnqRsnaConfMsgToPnacTask                           *
*                                                                           *
* Description  : Function to Post Apme Message to Pnac Task                 *
*                                                                           *
* Input        : pApmeToRsnaConfMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApmeEnqRsnaConfMsgToPnacTask (tApmeToRsnaConfMsg * pApmeToRsnaConfMsg)
{

    tPnacIntfMesg      *pMesg = NULL;


#if 1 /* RAJA */

    pMesg = (tPnacIntfMesg *)  PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);
    if (pMesg == NULL)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " Allocate mem block for ApmeMsg to Rsna failed\n");
        return RSNA_FAILURE;
    }

#else

    if ((PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, &pMesg)
         == MEM_FAILURE) || (pMesg == NULL))
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " Allocate mem block for ApmeMsg to Rsna failed\n");
        return RSNA_FAILURE;
    }

#endif

    pMesg->u4MesgType = PNAC_RSNA_CONFIG_FROM_APME;
    pMesg->Mesg.pApmeToRsnaConfMsg = (VOID *) pApmeToRsnaConfMsg;



#if 1 /* RAJA */

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg, OSIX_DEF_MSG_LEN) == 
                                        PNAC_OSIX_SUCCESS)
#else

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_QUEUE_NAME, pMesg) == OSIX_SUCCESS)

#endif
    {


#if 1 /* RAJA */

        if ( PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,PNAC_QUEUE_EVENT) == OSIX_SUCCESS )

#else
        if (PNAC_SEND_EVENT (SELF, PNAC_INTF_TASK_NAME, PNAC_QUEUE_EVENT)
            == OSIX_SUCCESS)

#endif
        {

            /* Success */
            return RSNA_SUCCESS;
        }

        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_OS_RESOURCE_TRC |
                  RSNA_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return RSNA_FAILURE;
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeEnqRsnaChgConfMsgToPnacTask                        *
*                                                                           *
* Description  : Function to Post Apme Message to Pnac Task                 *
*                                                                           *
* Input        : pApmeToRsnaChgConfMsg:- Pointer to Rsna msg from Apme      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApmeEnqRsnaChgConfMsgToPnacTask (tApmeToRsnaChgConfMsg *
                                     pApmeToRsnaChgConfMsg)
{

    tPnacIntfMesg      *pMesg = NULL;

#if 1 /* RAJA */

    pMesg = (tPnacIntfMesg *)  PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID);
    if (pMesg == NULL)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " Allocate mem block for ApmeMsg to Rsna failed\n");
        return RSNA_FAILURE;
    }

#else
    if ((PNAC_ALLOC_MEM_BLOCK (PNAC_INTFMESG_MEMPOOL_ID, &pMesg)
         == MEM_FAILURE) || (pMesg == NULL))
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " Allocate mem block for ApmeMsg to Rsna failed\n");
        return RSNA_FAILURE;
    }
#endif

    pMesg->u4MesgType = PNAC_RSNA_CHG_CONFIG_FROM_APME;
    pMesg->Mesg.pApmeToRsnaChgConfMsg = (VOID *) pApmeToRsnaChgConfMsg;

#if 1 /* RAJA */

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_INPUT_QID, (UINT1 *) &pMesg, OSIX_DEF_MSG_LEN) == 
                                        PNAC_OSIX_SUCCESS)
#else

    if (PNAC_SEND_TO_QUEUE (PNAC_INTF_QUEUE_NAME, pMesg) == OSIX_SUCCESS)

#endif
    {


#if 1 /* RAJA */

        if ( PNAC_SEND_EVENT (PNAC_INTF_TASK_ID,PNAC_QUEUE_EVENT) == OSIX_SUCCESS )

#else

        if (PNAC_SEND_EVENT (SELF, PNAC_INTF_TASK_NAME, PNAC_QUEUE_EVENT)
            == OSIX_SUCCESS)

#endif
        {

            /* Success */
            return RSNA_SUCCESS;
        }

        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_OS_RESOURCE_TRC |
                  RSNA_CONTROL_PATH_TRC, " INF: Send Event failed !!!\n");
        return RSNA_FAILURE;
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeRecvMlmeMsgFromApme                                *
*                                                                           *
* Description  : Function to Process Rsna Msg from Apme                     *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaApmeRecvMlmeMsgFromApme (tApmeToRsnaMlmeMsg * pApmeToRsnaMlmeMsg)
{

    switch (pApmeToRsnaMlmeMsg->mlmeIndication.eMlmeInd)
    {

        case MLME_ASSOC_IND:
            RsnaApmeProcMlmeAssocInd (pApmeToRsnaMlmeMsg);
            break;
        case MLME_REASSOC_IND:
            RsnaApmeProcMlmeReAssocInd (pApmeToRsnaMlmeMsg);
            break;

	case MLME_IND_BASE:
	case MLME_SETKEY_CFM_IND:
        case MLME_DISSOC_IND:
        case MLME_DEAUTH_IND:
            RsnaApmeProcMlmeDisDeAuthInd (pApmeToRsnaMlmeMsg);
            break;

        case MLME_MIC_FAILURE_IND:
            RsnaApmeProcMlmeMicFailInd (pApmeToRsnaMlmeMsg);
            break;

        default:
            RSNA_TRC_ARG1 (ALL_FAILURE_TRC,
                           "RsnaApmeRecvMsgFromApme:- "
                           "Invalid Message Type %d\n",
                           pApmeToRsnaMlmeMsg->mlmeIndication.eMlmeInd);
            break;

    }


#if 0 /* RAJA-RSNA */
    ApmeRsnaReleaseApmeToRsnaMlmeMsg (pApmeToRsnaMlmeMsg);
#endif
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeRecvConfMsgFromApme                                *
*                                                                           *
* Description  : Function to Process Rsna Conf Msg from Apme                *
*                                                                           *
* Input        : pApmeToRsnaConfMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
RsnaApmeRecvConfMsgFromApme (tApmeToRsnaConfMsg * pApmeToRsnaConfMsg)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RetVal = RSNA_FAILURE;
    UINT4               u4Time = 0;

   if (RsnaGetPortEntry ((UINT2)pApmeToRsnaConfMsg->u4IfIndex,
                          &pRsnaPaePortEntry) == OSIX_FAILURE)
        {
             RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaGetPortEntry"
                  "returned Failure\n");
             return; 
        }

   if (pRsnaPaePortEntry == NULL)
       {
           RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                    "No Rsna Info for the given port!! ... \n");
           return ; 
       }
 

    MEMCPY (&(pRsnaPaePortEntry->apmeRsnaConfInfo),
            &(pApmeToRsnaConfMsg->apmeRsnaConfInfo),
            sizeof (tApmeRsnaConfInfo));
    u4Time = pRsnaPaePortEntry->apmeRsnaConfInfo.apmeRsnaConfigDB.
        u4GroupGtkRekeyTime;
    if (u4Time > RSNA_MIN_REKEY_TIME)
    {
        u4RetVal = RsnaTmrStartTimer (&pRsnaPaePortEntry->RsnaGlobalGtk.
                                      RsnaUtilGtkRekeyTmr,
                                      RSNA_GTK_TIMER_ID, u4Time,
                                      (VOID *) pRsnaPaePortEntry);

        if (u4RetVal == RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_MGMT_TRC,
                      "nmhSetDot11RSNAConfigGroupGmkRekeyTime:-"
                      "RsnaTmrStartTimer returned Failure !!! \n");
#if 0 /* RAJA-RSNA */
            ApmeRsnaReleaseApmeToRsnaConfMsg (pApmeToRsnaConfMsg);
#endif
            return;
        }
    }

    u4Time = pRsnaPaePortEntry->apmeRsnaConfInfo.apmeRsnaConfigDB.
        u4GroupGmkRekeyTime;
    if (u4Time > RSNA_MIN_REKEY_TIME)
    {
        u4RetVal = RsnaTmrStartTimer (&pRsnaPaePortEntry->RsnaGlobalGtk.
                                      RsnaGmkRekeyTmr,
                                      RSNA_GMK_TIMER_ID,
                                      u4Time, (VOID *) pRsnaPaePortEntry);

        if (u4RetVal == RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_MGMT_TRC,
                      "nmhSetDot11RSNAConfigGroupGmkRekeyTime:-"
                      "RsnaTmrStartTimer returned Failure !!! \n");
#if 0 /* RAJA-RSNA */
            ApmeRsnaReleaseApmeToRsnaConfMsg (pApmeToRsnaConfMsg);
#endif
            return;
        }
    }
    pRsnaPaePortEntry->RsnaGlobalGtk.bGInit = TRUE;
    pRsnaPaePortEntry->RsnaGlobalGtk.bGTKAuthenticator = TRUE;
    if (RsnaMainGlobalGrpKeyFsm
        (&pRsnaPaePortEntry->RsnaGlobalGtk) == RSNA_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaInitPort:- RsnaCoreGlobalGroupKeyStateMachine"
                  "Returned Failure \n");
#if 0 /* RAJA-RSNA */
        ApmeRsnaReleaseApmeToRsnaConfMsg (pApmeToRsnaConfMsg);
#endif
        return;
    }
    pRsnaPaePortEntry->RsnaGlobalGtk.bGInit = FALSE;
    if (RsnaMainGlobalGrpKeyFsm
        (&pRsnaPaePortEntry->RsnaGlobalGtk) == RSNA_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaInitPort:- RsnaCoreGlobalGroupKeyStateMachine"
                  "Returned Failure \n");
#if 0 /* RAJA-RSNA */

        ApmeRsnaReleaseApmeToRsnaConfMsg (pApmeToRsnaConfMsg);
#endif
        return;
    }


#if 0 /* RAJA-RSNA */
    ApmeRsnaReleaseApmeToRsnaConfMsg (pApmeToRsnaConfMsg);

#endif
    return;


}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeRecvChgConfMsgFromApme                             *
*                                                                           *
* Description  : Function to Process chnaged Rsna Conf Msg from Apme        *
*                                                                           *
* Input        : pApmeToRsnaConfMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
RsnaApmeRecvChgConfMsgFromApme (tApmeToRsnaChgConfMsg * pApmeToRsnaChgConfMsg)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4IfCount = 0;
    UINT4               u4RetVal = RSNA_FAILURE;
    UINT4               u4Time = 0;

    for (u4IfCount = 0; u4IfCount < pApmeToRsnaChgConfMsg->u4IfCount;
         u4IfCount++)
    {


        if (RsnaGetPortEntry ((UINT2)pApmeToRsnaChgConfMsg->au4IfIndex[u4IfCount],
                              &pRsnaPaePortEntry) == OSIX_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaGetPortEntry"
                      "returned Failure\n");
            return ; 
        }

        if (pRsnaPaePortEntry == NULL)
        {
            continue;
        }

        
        MEMCPY (&(pRsnaPaePortEntry->apmeRsnaConfInfo),
                &(pApmeToRsnaChgConfMsg->apmeRsnaConfInfo),
                sizeof (tApmeRsnaConfInfo));
        if (pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.
            apmeRsnaConfigDB.bRSNAOptionImplemented == RSNA_DISABLED)
        {
#if 0 /* RAJA-RSNA */
            ApmeRsnaReleaseApmeToRsnaChgConfMsg (pApmeToRsnaChgConfMsg);
#endif
            return;
        }
        u4Time = pRsnaPaePortEntry->apmeRsnaConfInfo.apmeRsnaConfigDB.
            u4GroupGtkRekeyTime;
        if (u4Time > RSNA_MIN_REKEY_TIME)
        {
            u4RetVal = RsnaTmrStartTimer (&pRsnaPaePortEntry->RsnaGlobalGtk.
                                          RsnaUtilGtkRekeyTmr,
                                          RSNA_GTK_TIMER_ID, u4Time,
                                          (VOID *) pRsnaPaePortEntry);

            if (u4RetVal == RSNA_FAILURE)
            {
                RSNA_TRC (RSNA_MGMT_TRC,
                          "nmhSetDot11RSNAConfigGroupGmkRekeyTime:-"
                          "RsnaTmrStartTimer returned Failure !!! \n");

#if 0 /* RAJA-RSNA */
                ApmeRsnaReleaseApmeToRsnaChgConfMsg (pApmeToRsnaChgConfMsg);
#endif
                return;
            }
        }

        u4Time = pRsnaPaePortEntry->apmeRsnaConfInfo.apmeRsnaConfigDB.
            u4GroupGmkRekeyTime;
        if (u4Time > RSNA_MIN_REKEY_TIME)
        {
            u4RetVal = RsnaTmrStartTimer (&pRsnaPaePortEntry->RsnaGlobalGtk.
                                          RsnaGmkRekeyTmr,
                                          RSNA_GMK_TIMER_ID,
                                          u4Time, (VOID *) pRsnaPaePortEntry);

            if (u4RetVal == RSNA_FAILURE)
            {
                RSNA_TRC (RSNA_MGMT_TRC,
                          "nmhSetDot11RSNAConfigGroupGmkRekeyTime:-"
                          "RsnaTmrStartTimer returned Failure !!! \n");

#if 0 /* RAJA-RSNA */
                ApmeRsnaReleaseApmeToRsnaChgConfMsg (pApmeToRsnaChgConfMsg);
#endif
                return;
            }
        }
        pRsnaPaePortEntry->RsnaGlobalGtk.bGInit = TRUE;
        pRsnaPaePortEntry->RsnaGlobalGtk.bGTKAuthenticator = TRUE;
        if (RsnaMainGlobalGrpKeyFsm
            (&pRsnaPaePortEntry->RsnaGlobalGtk) == RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaInitPort:- RsnaCoreGlobalGroupKeyStateMachine"
                      "Returned Failure \n");

#if 0 /* RAJA-RSNA */
            ApmeRsnaReleaseApmeToRsnaChgConfMsg (pApmeToRsnaChgConfMsg);
#endif
            return;
        }
        pRsnaPaePortEntry->RsnaGlobalGtk.bGInit = FALSE;
        if (RsnaMainGlobalGrpKeyFsm
            (&pRsnaPaePortEntry->RsnaGlobalGtk) == RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaInitPort:- RsnaCoreGlobalGroupKeyStateMachine"
                      "Returned Failure \n");

#if 0 /* RAJA-RSNA */
            ApmeRsnaReleaseApmeToRsnaChgConfMsg (pApmeToRsnaChgConfMsg);
#endif
            return;
        }

    }
#if 0 /* RAJA-RSNA */
    ApmeRsnaReleaseApmeToRsnaChgConfMsg (pApmeToRsnaChgConfMsg);

#endif
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeProcMlmeMicFailInd                                 *
*                                                                           *
* Description  : Function to Process Mic Failure Indication From Apme       *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PRIVATE VOID
RsnaApmeProcMlmeMicFailInd (tApmeToRsnaMlmeMsg * pApmeToRsnaMlmeMsg)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4TimeStamp = 0;
    UINT2 u2ProfileIndex = 0;

    if (WssIfGetProfileIfIndex (pApmeToRsnaMlmeMsg->mlmeIndication.u4IfIndex,
               &u2ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaApmeProcMlmeMicFailInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return; 
    }

    if (RsnaGetPortEntry (u2ProfileIndex,
                          &pRsnaPaePortInfo) == RSNA_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaApmeProcMlmeMicFailInd:- RsnaGetPortEntry"
                  "returned Failure\n");
        return;
    }

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaApmeProcMlmeMicFailInd:-"
                  "No Rsna entry for the given port");
        return;
    }

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo,
                                                  pApmeToRsnaMlmeMsg->
                                                  mlmeIndication.MLMEMICFAILIND.
                                                  au1StaMac);

    if (pRsnaSupInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaApmeProcMlmeMicFailInd:- Recvd MLME"
                  "Failure Ind for not associated STA");
        return;
    }

    pRsnaSupInfo->RsnaStatsDB.u4TkipLocalMicFailures++;

    OsixGetSysTime (&u4TimeStamp);

    if (u4TimeStamp > (pRsnaPaePortInfo->u4MichaelMicFailureTimeStamp +
                       RSNA_MIC_FAIL_CTR_MSR_INTRVL))
    {
        pRsnaPaePortInfo->u4MichaleMicFailurecount = RSNA_TKIP_INIT_COUNT;
    }
    else
    {
        pRsnaPaePortInfo->u4MichaleMicFailurecount++;
        if (pRsnaPaePortInfo->u4MichaleMicFailurecount > RSNA_TKIP_INIT_COUNT)
        {
            RsnaUtilStartTkipCtrMsrs (pRsnaPaePortInfo);
        }
    }
    pRsnaPaePortInfo->u4MichaelMicFailureTimeStamp = u4TimeStamp;
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeProcMlmeAssocInd                                   *
*                                                                           *
* Description  : Function to Process Mlme Assoc Indication                  *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PRIVATE VOID
RsnaApmeProcMlmeAssocInd (tApmeToRsnaMlmeMsg * pApmeToRsnaMlmeMsg)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT1               au1StaAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
    tRsnaElements       RsnaElems;
    UINT4               u4ReasonCode = 0;
    BOOL1               bPmkSaFlag = OSIX_FALSE;
    UINT2               u2PmkIdCount = 0;
    tRsnaPmkSa         *pRsnaPmkSa = NULL;
    UINT4               u4RetVal = RSNA_FAILURE;
    UINT4               u4BssIfIndex = 0;
    UINT2 u2ProfileIndex = 0;

    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));

    u4BssIfIndex = pApmeToRsnaMlmeMsg->mlmeIndication.u4IfIndex;
    if (WssIfGetProfileIfIndex (pApmeToRsnaMlmeMsg->mlmeIndication.u4IfIndex,
               &u2ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaApmeProcMlmeAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return;
    }


    if (RsnaGetPortEntry (u2ProfileIndex,
                          &pRsnaPaePortInfo) == RSNA_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcessMlmeAssocInd:- RsnaGetPortEntry"
                  "returned Failure\n");
        return;
    }

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        return;
    }

    MEMCPY (au1StaAddr,
            pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.au1StaMac,
            RSNA_ETH_ADDR_LEN);

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaAddr);

    if (pRsnaSupInfo == NULL)
    {
        pRsnaSupInfo =
            RsnaUtilCreateAndAddStaToList (pRsnaPaePortInfo, au1StaAddr);
        if (pRsnaSupInfo == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                      "RsnaUtilCreateAndAddStaToList returned Failure \n");
            return;
        }
    }

    pRsnaSupInfo->u4SuppState = RSNA_STA_ASSOCIATED;
    MEMCPY (pRsnaSupInfo->au1RsnaIE,
            pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.au1RsnIe,
            pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.u1RsnaIELen);
    pRsnaSupInfo->u1IELen =
        pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.u1RsnaIELen;
    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));

    if (pRsnaSupInfo->au1RsnaIE[0] == RSNA_IE_ID)
    {
        u4RetVal = RsnaUtilParseIE (pRsnaSupInfo->au1RsnaIE, &RsnaElems,
                                    pRsnaSupInfo->u1IELen);
    }
    else if (pRsnaSupInfo->au1RsnaIE[0] == WPA_IE_ID)
    {
        u4RetVal = RsnaUtilParseWpaIE (pRsnaSupInfo->au1RsnaIE, &RsnaElems,
                                       pRsnaSupInfo->u1IELen);
    }
    else
    {
        u4RetVal = RSNA_FAILURE;
    }
    if (u4RetVal == RSNA_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = RSNA_REASON_INVALID_IE;
        RsnaApmeEnqMsgToApme (pRsnaPaePortInfo, pRsnaSupInfo->pRsnaSessNode,
                              pRsnaSupInfo, MLME_DISSOC_REQ, FALSE);

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK(RSNA_SUPP_INFO_MEMPOOL_ID,
                                       pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                return;
            }
        }
        else
        {
            pRsnaSupInfo->pRsnaSessNode->u4RefCount--;
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            return;
        }

        return;
    }
    u4ReasonCode = RsnaUtilValidateIE (&RsnaElems, pRsnaSupInfo,
                                       pApmeToRsnaMlmeMsg->mlmeIndication.
                                       u4IfIndex);
    if (u4ReasonCode == RSNA_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = u4ReasonCode;
        RsnaApmeEnqMsgToApme (pRsnaPaePortInfo, pRsnaSupInfo->pRsnaSessNode,
                              pRsnaSupInfo, MLME_DISSOC_REQ, FALSE);

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK(RSNA_SUPP_INFO_MEMPOOL_ID,
                                       pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                return;
            }
        }
        else
        {
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            return;
        }
        return;
    }
    pRsnaSupInfo->RsnaStatsDB.u4Version = RsnaElems.u2Ver;

    for (u2PmkIdCount = 0; ((u2PmkIdCount < RsnaElems.u2PmkidCount) &&
                            (bPmkSaFlag == OSIX_FALSE)); u2PmkIdCount++)
    {
        pRsnaPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaSupInfo->au1SuppMacAddr,
                                                RsnaElems.
                                                aPmkidList[u2PmkIdCount]);
        if (pRsnaPmkSa == NULL)
        {
            continue;
        }
        else
        {
            bPmkSaFlag = OSIX_TRUE;
            MEMCPY (pRsnaPaePortInfo->RsnaConfigDB.au1PmkIdUsed,
                    RsnaElems.aPmkidList[u2PmkIdCount], RSNA_PMKID_LEN);
        }
    }

    if ((bPmkSaFlag == OSIX_FALSE) && (u2PmkIdCount != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaApmeProcMlmeAssocReAssocInd:-"
                  "No Pmksa for the Given PMKID. !!! \n");
        return;
    }

    if (pRsnaSupInfo->pRsnaSessNode == NULL)
    {
        if (RsnaSessHandleNewStation (pRsnaPaePortInfo, pRsnaSupInfo, u4BssIfIndex) ==
            RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:-"
                      "RsnaSessHandleNewStation Failed !!! \n");
            return;
        }
    }
    else
    {
        RsnaApmeEnqMsgToApme (pRsnaSupInfo->pRsnaSessNode->pRsnaPaePortInfo,
                              pRsnaSupInfo->pRsnaSessNode,
                              NULL, MLME_DELKEYS_REQ, TRUE);
        pRsnaSupInfo->pRsnaSessNode->bPtkValid = FALSE;
        pRsnaSupInfo->pRsnaSessNode->bIsPairwiseSet = FALSE;
        MEMSET (&(pRsnaSupInfo->pRsnaSessNode->PtkSaDB), 0,
                sizeof (tRsnaPtkSa));
        pRsnaSupInfo->pRsnaSessNode->bReAutenticationRequest = TRUE;

        if (RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode) == RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:- "
                      "RsnaMainFsmStart Failed !!!!! \n");
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeProcMlmeReAssocInd                                 *
*                                                                           *
* Description  : Function to Process Mlme ReAssoc Indication                *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PRIVATE VOID
RsnaApmeProcMlmeReAssocInd (tApmeToRsnaMlmeMsg * pApmeToRsnaMlmeMsg)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT1               au1StaAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
    tRsnaElements       RsnaElems;
    UINT4               u4ReasonCode = 0;
    UINT2               u2PmkIdCount = 0;
    tRsnaPmkSa         *pRsnaPmkSa = NULL;
    BOOL1               bPmkSaFlag = OSIX_FALSE;
    UINT4               u4BssIfIndex = 0;
    UINT2               u2ProfileIndex = 0;

    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));


    u4BssIfIndex = pApmeToRsnaMlmeMsg->mlmeIndication.u4IfIndex;
    if (WssIfGetProfileIfIndex (pApmeToRsnaMlmeMsg->mlmeIndication.u4IfIndex,
               &u2ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcessMlmeReAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return;
    }

    if (RsnaGetPortEntry (u2ProfileIndex,
                          &pRsnaPaePortInfo) == RSNA_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcessMlmeReAssocInd:- RsnaGetPortEntry"
                  "returned Failure\n");
        return;
    }

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        return;
    }

    MEMCPY (au1StaAddr,
            pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.au1StaMac,
            RSNA_ETH_ADDR_LEN);

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaAddr);

    if (pRsnaSupInfo == NULL)
    {
        pRsnaSupInfo =
            RsnaUtilCreateAndAddStaToList (pRsnaPaePortInfo, au1StaAddr);
        if (pRsnaSupInfo == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                      "RsnaUtilCreateAndAddStaToList returned Failure \n");
            return;
        }
    }

    pRsnaSupInfo->u4SuppState = RSNA_STA_ASSOCIATED;
    MEMCPY (pRsnaSupInfo->au1RsnaIE,
            pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.au1RsnIe,
            pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.u1RsnaIELen);
    pRsnaSupInfo->u1IELen =
        pApmeToRsnaMlmeMsg->mlmeIndication.MLMEASSOCIND.u1RsnaIELen;
    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));
    if (RsnaUtilParseIE (pRsnaSupInfo->au1RsnaIE, &RsnaElems,
                         pRsnaSupInfo->u1IELen) == RSNA_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = RSNA_REASON_INVALID_IE;
        RsnaApmeEnqMsgToApme (pRsnaPaePortInfo, pRsnaSupInfo->pRsnaSessNode,
                              pRsnaSupInfo, MLME_DISSOC_REQ, FALSE);

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK(RSNA_SUPP_INFO_MEMPOOL_ID,
                                         pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                return;
            }
        }
        else
        {
            pRsnaSupInfo->pRsnaSessNode->u4RefCount--;
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            return;
        }

        return;
    }
    u4ReasonCode = RsnaUtilValidateIE (&RsnaElems, pRsnaSupInfo,
                                       pApmeToRsnaMlmeMsg->mlmeIndication.
                                       u4IfIndex);
    if (u4ReasonCode == RSNA_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = u4ReasonCode;
        RsnaApmeEnqMsgToApme (pRsnaPaePortInfo, pRsnaSupInfo->pRsnaSessNode,
                              pRsnaSupInfo, MLME_DISSOC_REQ, FALSE);

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK(RSNA_SUPP_INFO_MEMPOOL_ID,
                                         pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                return;
            }
        }
        else
        {
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            return;
        }
        return;
    }
    pRsnaSupInfo->RsnaStatsDB.u4Version = RsnaElems.u2Ver;

    for (u2PmkIdCount = 0; ((u2PmkIdCount < RsnaElems.u2PmkidCount) &&
                            (bPmkSaFlag == OSIX_FALSE)); u2PmkIdCount++)
    {
        pRsnaPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaSupInfo->au1SuppMacAddr,
                                                RsnaElems.
                                                aPmkidList[u2PmkIdCount]);
        if (pRsnaPmkSa == NULL)
        {
            continue;
        }
        else
        {
            bPmkSaFlag = OSIX_TRUE;
            MEMCPY (pRsnaPaePortInfo->RsnaConfigDB.au1PmkIdUsed,
                    RsnaElems.aPmkidList[u2PmkIdCount], RSNA_PMKID_LEN);
        }
    }

    if ((bPmkSaFlag == OSIX_FALSE) && (u2PmkIdCount != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaApmeProcMlmeAssocReAssocInd:-"
                  "No Pmksa for the Given PMKID. !!! \n");
        return;
    }

    if (pRsnaSupInfo->pRsnaSessNode == NULL)
    {
        if (RsnaSessHandleNewStation (pRsnaPaePortInfo, pRsnaSupInfo, u4BssIfIndex) ==
            RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:-"
                      "RsnaSessHandleNewStation Failed !!! \n");
            return;
        }
    }
    else
    {
        RsnaApmeEnqMsgToApme (pRsnaSupInfo->pRsnaSessNode->pRsnaPaePortInfo,
                              pRsnaSupInfo->pRsnaSessNode,
                              NULL, MLME_DELKEYS_REQ, TRUE);
        pRsnaSupInfo->pRsnaSessNode->bPtkValid = FALSE;
        pRsnaSupInfo->pRsnaSessNode->bIsPairwiseSet = FALSE;
        MEMSET (&(pRsnaSupInfo->pRsnaSessNode->PtkSaDB), 0,
                sizeof (tRsnaPtkSa));
        pRsnaSupInfo->pRsnaSessNode->bReAutenticationRequest = TRUE;

        if (RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode) == RSNA_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:- "
                      "RsnaMainFsmStart Failed !!!!! \n");
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeProcMlmeDisDeAuthInd                               *
*                                                                           *
* Description  : Function to Process Mlme Disassoc or Deauth Indication     *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PRIVATE VOID
RsnaApmeProcMlmeDisDeAuthInd (tApmeToRsnaMlmeMsg * pApmeToRsnaMlmeMsg)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    tPnacAuthSessionNode *pAuthSessionNode = NULL;
    tRsnaSessionNode   *pRsnaSessionNode = NULL;
    UINT2 u2ProfileIndex = 0;
    UINT1               au1StaAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };


    if (WssIfGetProfileIfIndex (pApmeToRsnaMlmeMsg->mlmeIndication.u4IfIndex,
               &u2ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaApmeProcMlmeDisDeAuthInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return;
    }

    if (RsnaGetPortEntry (u2ProfileIndex,
                          &pRsnaPaePortInfo) == RSNA_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaApmeProcMlmeDisDeAuthInd :- RsnaGetPortEntry"
                  "returned Failure\n");
        return;
    }

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaApmeProcMlmeDisDeAuthInd:-"
                  "No Rsna entry for the given port \n");
        return;
    }

    if (pApmeToRsnaMlmeMsg->mlmeIndication.eMlmeInd == MLME_DISSOC_IND)
    {
        MEMCPY (au1StaAddr,
                pApmeToRsnaMlmeMsg->mlmeIndication.MLMEDISSOCIND.au1StaMac,
                RSNA_ETH_ADDR_LEN);
    }
    else if (pApmeToRsnaMlmeMsg->mlmeIndication.eMlmeInd == MLME_DEAUTH_IND)
    {
        MEMCPY (au1StaAddr,
                pApmeToRsnaMlmeMsg->mlmeIndication.MLMEDEAUTHIND.au1StaMac,
                RSNA_ETH_ADDR_LEN);
    }

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaAddr);
    if (pRsnaSupInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaApmeProcMlmeDisDeAuthInd:- "
                  "No Station Info for the given " "Mac Address \n");
        return;
    }

    pRsnaSupInfo->u4SuppState =(UINT4) ~(RSNA_STA_ASSOCIATED);

    if (PnacAuthGetSessionTblEntryByMac (au1StaAddr, &pAuthSessionNode)
        == PNAC_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeDisassocInd:- No Pnac"
                  "Session for the given mac \n");
        return;
    }

    pRsnaSessionNode = (tRsnaSessionNode *) pAuthSessionNode->pRsnaSessionNode;
    if (pRsnaSessionNode == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeDisassocInd:- No Rsna"
                  "Session for the given mac \n");
        return;
    }
    pRsnaSessionNode->bDeAuthReq = TRUE;
    if (RsnaMainFsmStart (pRsnaSessionNode) == RSNA_FAILURE)
    {
        if (pRsnaSessionNode->bDeleteSession == TRUE)
        {
            if (RsnaSessDeleteSession (pRsnaSessionNode) == RSNA_FAILURE)
            {
                return;
            }
        }
    }
    pRsnaSessionNode->u4RefCount--;
    RsnaSessDeleteSession (pRsnaSessionNode);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeEnqMsgToApme                                       *
*                                                                           *
* Description  : Function to Post Message to Apme                           *
*                                                                           *
* Input        : pApmeToRsnaMlmeMsg:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApmeEnqMsgToApme (tRsnaPaePortEntry * pRsnaPaePortInfo,
                      tRsnaSessionNode * pRsnaSessionNode,
                      tRsnaSupInfo * pRsnaSupInfo,
                      UINT4 u4MsgType, BOOL1 bPairwiseKey)
{
    UNUSED_PARAM (*pRsnaPaePortInfo);
    UNUSED_PARAM (*pRsnaSessionNode);
    UNUSED_PARAM (*pRsnaSupInfo);
    UNUSED_PARAM (u4MsgType);
    UNUSED_PARAM (bPairwiseKey);

#if 0 /* RAJA */


    pMsg = ApmeRsnaAllocRsnaToApmeMsg ();

    if (pMsg == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaEngMsgToApme :- "
                  "ApmeRsnaAllocRsnaToApmeMsg Failed \n");
        return (OSIX_FAILURE);
    }

    MEMSET (pMsg, 0, sizeof (tRsnaToApmeMsg));

    switch (u4MsgType)
    {
        case MLME_DISSOC_REQ:
            pMsg->mlmeRequest.u4IfIndex = pRsnaPaePortInfo->u2Port;
            pMsg->mlmeRequest.eMlmeReq = MLME_DISSOC_REQ;
            if (pRsnaSessionNode != NULL)
            {
                MEMCPY (pMsg->mlmeRequest.MLMEDISSOCREQ.au1StaMac,
                        pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                        RSNA_ETH_ADDR_LEN);
                pMsg->mlmeRequest.MLMEDISSOCREQ.u2reasonCode =
                    pRsnaSessionNode->u4ReasonCode;
                pRsnaSessionNode->bDeAuthReq = TRUE;
                RsnaMainFsmStart (pRsnaSessionNode);
            }
            else if (pRsnaSupInfo != NULL)
            {
                MEMCPY (pMsg->mlmeRequest.MLMEDISSOCREQ.au1StaMac,
                        pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
                pMsg->mlmeRequest.MLMEDISSOCREQ.u2reasonCode =
                    pRsnaSupInfo->u4ReasonCode;
            }
            break;

        case MLME_DEAUTH_REQ:
            pMsg->mlmeRequest.u4IfIndex = pRsnaPaePortInfo->u2Port;
            pMsg->mlmeRequest.eMlmeReq = MLME_DEAUTH_REQ;
            if (pRsnaSessionNode != NULL)
            {
                MEMCPY (pMsg->mlmeRequest.MLMEDISSOCREQ.au1StaMac,
                        pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                        RSNA_ETH_ADDR_LEN);
                pMsg->mlmeRequest.MLMEDISSOCREQ.u2reasonCode =
                    pRsnaSessionNode->u4ReasonCode;
                pRsnaSessionNode->bDeAuthReq = TRUE;
                RsnaMainFsmStart (pRsnaSessionNode);
                pRsnaSessionNode->u4RefCount--;
                RsnaSessDeleteSession (pRsnaSessionNode);
            }
            break;

        case MLME_SETKEYS_REQ:
            pMsg->mlmeRequest.u4IfIndex = pRsnaPaePortInfo->u2Port;
            pMsg->mlmeRequest.eMlmeReq = MLME_SETKEYS_REQ;
            pMsg->mlmeRequest.MLMESETKEYSREQ.u4numKeyDesc = RSNA_GTK_FIRST_IDX;
            if (bPairwiseKey == TRUE)
            {
                MEMCPY (pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].au1Key,
                        pRsnaSessionNode->PtkSaDB.au1Ptk, RSNA_MAX_PTK_KEY_LEN);
                MEMCPY (pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].au1StaAddr,
                        pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                        RSNA_ETH_ADDR_LEN);
                MEMCPY (pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].
                        au1CipherSuite, pRsnaSessionNode->pRsnaSupInfo->
                        au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].u1KeyId = 0;
                pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].u1KeyType =
                    bPairwiseKey;
                if ((MEMCMP (pRsnaSessionNode->pRsnaSupInfo->au1PairwiseCipher,
                             RSNA_CIPHER_SUITE_TKIP,
                             RSNA_CIPHER_SUITE_LEN) == 0) ||
                    (MEMCMP (pRsnaSessionNode->pRsnaSupInfo->au1PairwiseCipher,
                             WPA_CIPHER_SUITE_TKIP,
                             RSNA_CIPHER_SUITE_LEN) == 0))
                {
                    pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].u1KeyLength =
                        RSNA_TKIP_KEY_LEN;
                }
                else if ((MEMCMP (pRsnaSessionNode->pRsnaSupInfo->
                                  au1PairwiseCipher,
                                  RSNA_CIPHER_SUITE_CCMP,
                                  RSNA_CIPHER_SUITE_LEN) == 0) ||
                         (MEMCMP (pRsnaSessionNode->pRsnaSupInfo->
                                  au1PairwiseCipher,
                                  WPA_CIPHER_SUITE_CCMP,
                                  RSNA_CIPHER_SUITE_LEN) == 0))
                {
                    pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].
                        u1KeyLength = RSNA_CCMP_KEY_LEN;
                }
                else
                {
                    return (OSIX_FAILURE);
                }
            }
            else
            {
                u1KeyIdx = pRsnaPaePortInfo->RsnaGlobalGtk.u1GN;
                MEMCPY (pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].au1Key,
                        &pRsnaPaePortInfo->RsnaGlobalGtk.au1Gtk[u1KeyIdx -
                                                                RSNA_GTK_FIRST_IDX],
                        RSNA_GTK_MAX_LEN);
                MEMCPY (pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].
                        au1CipherSuite,
                        pRsnaPaePortInfo->apmeRsnaConfInfo.apmeRsnaConfigDB.
                        au1GroupCipher, RSNA_CIPHER_SUITE_LEN);
                MEMCPY (pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].au1StaAddr,
                        gau1RsnaBroadCast, RSNA_ETH_ADDR_LEN);
                pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].u1KeyId = u1KeyIdx;
                pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].u1KeyType =
                    bPairwiseKey;
                if ((MEMCMP
                     (pRsnaPaePortInfo->apmeRsnaConfInfo.apmeRsnaConfigDB.
                      au1GroupCipher, RSNA_CIPHER_SUITE_TKIP,
                      RSNA_CIPHER_SUITE_LEN) == 0) ||
                    (MEMCMP
                     (pRsnaPaePortInfo->apmeRsnaConfInfo.apmeRsnaConfigDB.
                      au1GroupCipher, WPA_CIPHER_SUITE_TKIP,
                      RSNA_CIPHER_SUITE_LEN) == 0))
                {
                    pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].u1KeyLength =
                        RSNA_TKIP_KEY_LEN;
                }
                else if ((MEMCMP
                          (pRsnaPaePortInfo->apmeRsnaConfInfo.apmeRsnaConfigDB.
                           au1GroupCipher, RSNA_CIPHER_SUITE_CCMP,
                           RSNA_CIPHER_SUITE_LEN) == 0) ||
                         (MEMCMP
                          (pRsnaPaePortInfo->apmeRsnaConfInfo.apmeRsnaConfigDB.
                           au1GroupCipher, WPA_CIPHER_SUITE_CCMP,
                           RSNA_CIPHER_SUITE_LEN) == 0))
                {
                    pMsg->mlmeRequest.MLMESETKEYSREQ.aKeyDesc[0].
                        u1KeyLength = RSNA_CCMP_KEY_LEN;
                }
                else
                {
                    return (OSIX_FAILURE);
                }
            }
            break;

        case MLME_SETPROT_REQ:
            pMsg->mlmeRequest.u4IfIndex = pRsnaPaePortInfo->u2Port;
            pMsg->mlmeRequest.eMlmeReq = MLME_SETPROT_REQ;
            MEMCPY (pMsg->mlmeRequest.MLMESETPROTREQ.protReq[0].au1StaMac,
                    pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            pMsg->mlmeRequest.MLMESETPROTREQ.protReq[0].u1KeyType =
                bPairwiseKey;
            break;

        case MLME_DELKEYS_REQ:
            pMsg->mlmeRequest.u4IfIndex = pRsnaPaePortInfo->u2Port;
            pMsg->mlmeRequest.eMlmeReq = MLME_DELKEYS_REQ;
            pMsg->mlmeRequest.MLMEDELKEYSREQ.u4numKeyDesc = RSNA_GTK_FIRST_IDX;
            MEMCPY (pMsg->mlmeRequest.MLMEDELKEYSREQ.aKeyDescArr[0].
                    au1StaAddr, pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            pMsg->mlmeRequest.MLMEDELKEYSREQ.aKeyDescArr[0].u1KeyType =
                bPairwiseKey;
            if (bPairwiseKey == FALSE)
            {
                u1KeyIdx = pRsnaPaePortInfo->RsnaGlobalGtk.u1GN;
                pMsg->mlmeRequest.MLMEDELKEYSREQ.aKeyDescArr[0].u1KeyId =
                    u1KeyIdx;
            }
            break;

        case MLME_KICK_ALL_STA:
            pMsg->mlmeRequest.u4IfIndex = pRsnaPaePortInfo->u2Port;
            pMsg->mlmeRequest.eMlmeReq = MLME_KICK_ALL_STA;
            break;

        default:
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_MGMT_TRC,
                      "RsnaApmeEnqMsgToApme:- Invalid Message !!! \n");
            ApmeRsnaReleaseRsnaToApmeMsg (pMsg);
            return (OSIX_FAILURE);
            break;

    }

    ApmeRsnaRecvRsnaToApmeMsg (pMsg);


#endif 
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApmeConvertPassPhraseToPsk                             *
*                                                                           *
* Description  : Function to convert ascii test to psk in hex format        *
*                                                                           *
* Input        : pSsid :- Pointer to ssid information                       *
*                pu1PassPhrase :- Refers to pass phrase                     *
*                pu1Psk        : Refers to psk                              *
*                u1SsidLen     :- Length of the Ssid                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaApmeConvertPassPhraseToPsk (UINT1 *pu1Ssid, UINT1 *pu1PassPhrase,
                                UINT1 *pu1Psk, UINT1 u1SsidLen)
{

    INT1                i1Len = RSNA_PMK_LEN;
    UINT1               u1RemLen = 0;
    UINT4               u4Count = 0;
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT1              *pu1PskPos = pu1Psk;

    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);

    while (i1Len > 0)
    {
        u4Count++;
        RsnaAlgoPBKDF2 (pu1PassPhrase,(UINT1)( STRLEN (pu1PassPhrase)), pu1Ssid,
                        u1SsidLen, RSNA_PBKDF2_ITERATIONS, u4Count, au1Digest);
        u1RemLen = i1Len > SHA1_HASH_SIZE ? SHA1_HASH_SIZE : i1Len;
        MEMCPY (pu1PskPos, au1Digest, u1RemLen);
        pu1PskPos += u1RemLen;
        i1Len = (INT1)(i1Len + SHA1_HASH_SIZE);
    }
    return (RSNA_SUCCESS);
}
#endif
