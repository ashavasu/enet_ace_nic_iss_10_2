/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnapkt.c,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"

PRIVATE UINT4       RsnaPktFillEapolKeyFrame (UINT1 *pu1KeyDescBuf,
                                              tRsnaSessionNode * pRsnaSessNode,
                                              UINT2 u2KeyInfo,
                                              UINT1 *pu1KeyRsc,
                                              UINT1 *pu1Nonce,
                                              tRsnaKdeFlags * pRsnaKdeFlags,
                                              UINT2 *pu2BodyLen);

PRIVATE UINT4       RsnaPktConstructEapolHdr (UINT1 *pu1Pkt,
                                              tRsnaSessionNode * pRsnaSessNode,
                                              UINT2 u2PktLen);

UINT1               gau1RsnaEapol[RSNA_MAX_ENET_FRAME_SIZE];
/****************************************************************************
*                                                                           *
* Function     : RsnaPktSendEapolKeyPkt                                     *
*                                                                           *
* Description  : Construct and sends Rsna Eapol Key Pkt                     *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                u2KeyInfo :- Specifies the characteristics of the key      *
*                pu1KeyRsc :- Contains the receive sequence counter         *
*                pu1Nonce  :- Nonce to be conveyed to the supplicant        *
*                bGtk      :- Flag indicates whether gtk is part of exch    *
*                bIE       :- Flag Indicates whether IE is part of exch     *
*                bPmkIdKde :- Flag Indicates whether PmkId is part of exch  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaPktSendEapolKeyPkt (tRsnaSessionNode * pRsnaSessNode, UINT2 u2KeyInfo,
                        UINT1 *pu1KeyRsc, UINT1 *pu1Nonce, BOOL1 bGtk,
                        BOOL1 bIE, BOOL1 bPmkIdKde, BOOL1 bIGtk)
{
    UINT1              *pu1KeyDescBuf = NULL;
    tEapolKeyFrame     *pEapolKeyFrame = NULL;
    UINT2               u2BodyLen = 0;
    UINT2               u2PktLen = 0;
    tRsnaKdeFlags       RsnaKdeFlags = { 0, 0, 0, 0 };
    UINT2               u2Mask = 0;

    RsnaKdeFlags.bGtk = bGtk;
    RsnaKdeFlags.bIE = bIE;
    RsnaKdeFlags.bPmkIdKde = bPmkIdKde;
    RsnaKdeFlags.bIGtk = bIGtk;

    MEMSET (gau1RsnaEapol, RSNA_INIT_VAL, RSNA_MAX_ENET_FRAME_SIZE);

    /* Give an offset in 'pRsnaToApmeEapol->au1RsnaEapol 
     * before filling Key descriptor */

    pu1KeyDescBuf = (UINT1 *) (&gau1RsnaEapol[RSNA_EAPOL_HDR_SIZE]);

    if (RsnaPktFillEapolKeyFrame
        (pu1KeyDescBuf, pRsnaSessNode, u2KeyInfo, pu1KeyRsc, pu1Nonce,
         &RsnaKdeFlags, &u2BodyLen) != RSNA_SUCCESS)
    {

        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                  "RsnaPktFillEapolKeyFrame :returned Failure\n");
        return RSNA_FAILURE;
    }

    if (RsnaPktConstructEapolHdr (gau1RsnaEapol,
                                  pRsnaSessNode, u2BodyLen) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                  "RsnaPktConstructEapolHdr :returned Failure\n");
        return RSNA_FAILURE;
    }

    /* Total size of built KeyEapol frame */
    u2PktLen = (UINT2) (RSNA_EAPOL_HDR_SIZE + u2BodyLen);
    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_MIC);
    if (u2Mask == RSNA_KEY_INFO_MIC)
    {
        pu1KeyDescBuf = (UINT1 *) gau1RsnaEapol;

        pEapolKeyFrame =
            (tEapolKeyFrame *) & (gau1RsnaEapol[RSNA_EAPOL_HDR_SIZE]);

        RsnaAlgoComputeMic (u2KeyInfo, pRsnaSessNode->PtkSaDB.au1Kck,
                            pu1KeyDescBuf, u2PktLen, pEapolKeyFrame->au1KeyMic);
    }
    if (((gu1RsnaTestPairwiseUpdateCount == OSIX_FALSE) && (bGtk == FALSE))
        || ((gu1RsnaTestGroupwiseUpdateCount == OSIX_FALSE) && (bGtk == TRUE)))
    {

        if (RsnaTxFrame (gau1RsnaEapol, pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,    /* Station MAC */
                         pRsnaSessNode->au1BssMac,    /* BSS ID */
                         pRsnaSessNode->u4BssIfIndex, u2PktLen) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                      "RsnaTxFrame ()  :- returns failure \n");
            return RSNA_FAILURE;
        }
    }
    else
    {
        gu1RsnaTestPairwiseUpdateCount = OSIX_FALSE;
        gu1RsnaTestGroupwiseUpdateCount = OSIX_FALSE;
    }

    RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr));

    /* Start the Retransmission timer for the handshake message (both pair & group).
     * For the problem observed in the target environment, where 1/4 of Pairwise
     * Message getting dropped, sometimes, a workaround logic of the attempting
     * to retry 1/4 message for lesser retransmission interval is being
     * implemented.
     *
     * Hence shorter retransmission interval of 1 Sec shall be done for 1/4
     * message alone.
     */

    if (pRsnaSessNode->u1PtkFsmState == RSNA_4WAY_PTKSTART)
    {
        if (RsnaTmrStartTimer (&(pRsnaSessNode->RsnaReTransTmr),
                               RSNA_RETRANS_TIMER_ID,
                               RSNA_1OF4__SA_TIME_OUT,
                               pRsnaSessNode) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                      "RsnaPktSendEapolKeyPkt:- RsnaTmrStartTimer"
                      "returned failure \n");
            return RSNA_FAILURE;

        }
    }
    else
    {
        if (RsnaTmrStartTimer (&(pRsnaSessNode->RsnaReTransTmr),
                               RSNA_RETRANS_TIMER_ID,
                               pRsnaSessNode->pRsnaPaePortInfo->
                               RsnaConfigDB.u4SATimeOut,
                               pRsnaSessNode) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                      "RsnaPktSendEapolKeyPkt:- RsnaTmrStartTimer"
                      "returned failure \n");
            return RSNA_FAILURE;

        }

    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
* Function     : RsnaPktFillEapolKeyFrame                                   *
*                                                                           *
* Description  : Function used to construct Eapol Key Frame                 *
*                                                                           *
* Input        : pu1KeyDescBuf:- Buffer used for constructing Pkt           *
*                pRsnaSessNode:- Pointer to the session node                *
*                u2KeyInfo    :- Refers to characteristics of the key       *
*                pu1KeyRsc    :- Pointer to the recvd sequence counter      *
*                pu1Nonce     :- Refers to nonce to be sent in the pkt      *
*                bGtk         :- Indicates whether the key to be part of    *
*                                exch                                       *
*                bIE          :- Indicates whether IE is part of Exch       *
*                bPmkIdKde    :- Indicates if PMKID is part of exch         *
*                pu2BodyLen   :- Length of the Key Frame                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
PRIVATE UINT4
RsnaPktFillEapolKeyFrame (UINT1 *pu1KeyDescBuf,
                          tRsnaSessionNode * pRsnaSessNode,
                          UINT2 u2KeyInfo,
                          UINT1 *pu1KeyRsc,
                          UINT1 *pu1Nonce,
                          tRsnaKdeFlags * pRsnaKdeFlags, UINT2 *pu2BodyLen)
{

    tEapolKeyFrame     *pEapolKeyFrame = NULL;
    UINT1               au1Algo[] = { 0x0, 0x0, 0x0, 0x0 };
    UINT1               au1GroupCipher[] = { 0x0, 0x0, 0x0, 0x0 };
    UINT2               u2KeyDataLen = 0;
    UINT2               u2PadLen = 0;
    UINT2               u2BufPos = 0;
    UINT1              *pu1Ptr = NULL;
    UINT1               u1KeyIdx = 0;
    UINT2               u2Len = 0;
    UINT1               au1EncrKey[RSNA_NONCE_LEN] = "";
    UINT1               au1Buf[RSNA_MAX_KEY_DATA_LEN] = "";
    UINT1               au1KeyRsc[] =
        { 0x01, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
    UINT4               u4TempCipherLen = 0;
    UINT1               u4TempGroupCipherKeyLen = 0;
    UINT1               au1TempGroupCipher[RSNA_CIPHER_SUITE_LEN];
#ifdef WPA_WANTED
    UINT2               u4TempKey = RSNA_KEY_INFO_ENCR_KEY_DATA;
#endif
#ifdef PMF_WANTED
    UINT2               u2IGTKKeyIdx = 0;
    UINT1               au1GtkKey[16];
#endif
    /* Key Rsc filled is hardcoded for this release */

    MEMSET (pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE, 0,
            pRsnaSessNode->pRsnaPaePortInfo->u1IELen);
    MEMSET (au1EncrKey, 0, RSNA_NONCE_LEN);
    MEMSET (au1TempGroupCipher, 0, RSNA_CIPHER_SUITE_LEN);
    MEMSET (au1Buf, 0, RSNA_MAX_KEY_DATA_LEN);
#ifdef PMF_WANTED
    MEMSET (au1GtkKey, 0, 16);
#endif

    pEapolKeyFrame = (tEapolKeyFrame *) pu1KeyDescBuf;

    if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
    {
        pEapolKeyFrame->u1DescrType = RSNA_KEY_TYPE;
        MEMCPY (au1TempGroupCipher,
                pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
                RSNA_CIPHER_SUITE_LEN);
    }
#ifdef WPA_WANTED
    else if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
    {
        pEapolKeyFrame->u1DescrType = WPA_KEY_TYPE;
        MEMSET (au1KeyRsc, 0, sizeof (au1KeyRsc));
        MEMCPY (au1TempGroupCipher,
                pRsnaSessNode->pRsnaPaePortInfo->WpaConfigDB.au1GroupCipher,
                RSNA_CIPHER_SUITE_LEN);
    }
#endif

    if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
    {
        pEapolKeyFrame->u2KeyInfo = OSIX_NTOHS (u2KeyInfo);
    }
#ifdef WPA_WANTED
    if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
    {
        pEapolKeyFrame->u2KeyInfo = u2KeyInfo & (u4TempKey);
        pEapolKeyFrame->u2KeyInfo = OSIX_NTOHS (pEapolKeyFrame->u2KeyInfo);
    }
#endif
    if (pRsnaSessNode->bPair == TRUE)
    {
        MEMCPY (au1Algo, pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                RSNA_CIPHER_SUITE_LEN);
    }

    MEMCPY (au1GroupCipher, au1TempGroupCipher, RSNA_CIPHER_SUITE_LEN);

    if ((MEMCMP (au1GroupCipher, RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN))
        == 0)
    {
        pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.u4GroupCipherKeyLen =
            RSNA_TKIP_KEY_LEN;
    }
    else if ((MEMCMP
              (au1GroupCipher, RSNA_CIPHER_SUITE_CCMP,
               RSNA_CIPHER_SUITE_LEN)) == 0)
    {
        pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.u4GroupCipherKeyLen =
            RSNA_CCMP_KEY_LEN;
    }
#ifdef WPA_WANTED
    if (MEMCMP (au1GroupCipher, WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)
        == 0)
    {
        pRsnaSessNode->pRsnaPaePortInfo->WpaConfigDB.u4GroupCipherKeyLen =
            RSNA_TKIP_KEY_LEN;
    }
    else if ((MEMCMP
              (au1GroupCipher, WPA_CIPHER_SUITE_CCMP,
               RSNA_CIPHER_SUITE_LEN)) == 0)
    {
        pRsnaSessNode->pRsnaPaePortInfo->WpaConfigDB.u4GroupCipherKeyLen =
            RSNA_CCMP_KEY_LEN;
    }
#endif

    if ((MEMCMP (au1Algo, RSNA_CIPHER_SUITE_WEP40,
                 RSNA_CIPHER_SUITE_LEN) == 0) ||
        (MEMCMP (au1Algo, WPA_CIPHER_SUITE_WEP40, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        pEapolKeyFrame->u2KeyLen = OSIX_NTOHS (RSNA_WEP40_KEY_LEN);
    }
    else if ((MEMCMP (au1Algo, RSNA_CIPHER_SUITE_TKIP,
                      RSNA_CIPHER_SUITE_LEN) == 0) ||
             (MEMCMP (au1Algo, WPA_CIPHER_SUITE_TKIP,
                      RSNA_CIPHER_SUITE_LEN) == 0))
    {
        pEapolKeyFrame->u2KeyLen = OSIX_NTOHS (RSNA_TKIP_KEY_LEN);
    }
    else if ((MEMCMP (au1Algo, RSNA_CIPHER_SUITE_CCMP,
                      RSNA_CIPHER_SUITE_LEN) == 0) ||
             (MEMCMP (au1Algo, WPA_CIPHER_SUITE_CCMP,
                      RSNA_CIPHER_SUITE_LEN) == 0))
    {
        pEapolKeyFrame->u2KeyLen = OSIX_NTOHS (RSNA_CCMP_KEY_LEN);
    }
    else if ((MEMCMP (au1Algo, RSNA_CIPHER_SUITE_WEP104,
                      RSNA_CIPHER_SUITE_LEN) == 0) ||
             (MEMCMP (au1Algo, WPA_CIPHER_SUITE_WEP104,
                      RSNA_CIPHER_SUITE_LEN) == 0))
    {
        pEapolKeyFrame->u2KeyLen = OSIX_NTOHS (RSNA_WEP104_KEY_LEN);
    }
    else
    {
        return (RSNA_FAILURE);
    }
    RsnaUtilIncByteArray (pRsnaSessNode->au1RsnaKeyReplayCtr,
                          RSNA_REPLAY_CTR_LEN);
    MEMCPY (pEapolKeyFrame->au1KeyReplayCtr, pRsnaSessNode->au1RsnaKeyReplayCtr,
            RSNA_REPLAY_CTR_LEN);
    pRsnaSessNode->bIsKeyReplayCtrValid = TRUE;
    MEMCPY (pEapolKeyFrame->au1KeyNonce, pu1Nonce, RSNA_NONCE_LEN);
    if (pu1KeyRsc != NULL)
    {
        MEMCPY (pEapolKeyFrame->au1KeyRsc, pu1KeyRsc, RSNA_KEY_RSC_LEN);
    }
    /* Key RSC is hardcoded for this release */
    MEMCPY (pEapolKeyFrame->au1KeyRsc, au1KeyRsc, RSNA_KEY_RSC_LEN);

    (*pu2BodyLen) = sizeof (tEapolKeyFrame);
    u2BufPos = (*pu2BodyLen);

    if (pRsnaKdeFlags->bGtk == TRUE)
    {
        if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
        {
            u4TempGroupCipherKeyLen =
                pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                u4GroupCipherKeyLen;
        }
#ifdef WPA_WANTED
        else if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
        {
            u4TempGroupCipherKeyLen =
                pRsnaSessNode->pRsnaPaePortInfo->WpaConfigDB.
                u4GroupCipherKeyLen;
        }
#endif
        u2KeyDataLen = (UINT2) (u2KeyDataLen + (u4TempGroupCipherKeyLen));
        if ((pRsnaSessNode->u1ElemId == RSNA_IE_ID))
        {
            u2KeyDataLen = (UINT2) (u2KeyDataLen + RSNA_GTK_KDE_LEN);
#ifdef PMF_WANTED
            if (pRsnaKdeFlags->bIGtk == TRUE)
            {
                u2KeyDataLen = (UINT2) (u2KeyDataLen + RSNA_IGTK_KDE_LEN + 16);
            }
#endif

        }
        if (pRsnaKdeFlags->bIE == TRUE)
        {
            if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
            {
                if (RsnaGetBeaconRSNIEElements
                    (pRsnaSessNode->pRsnaPaePortInfo->u2Port,
                     pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                     &(pRsnaSessNode->pRsnaPaePortInfo->u1IELen)) ==
                    RSNA_SUCCESS)
                {
                    MEMCPY ((pu1KeyDescBuf + u2BufPos),
                            pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                            pRsnaSessNode->pRsnaPaePortInfo->u1IELen);
                    u2KeyDataLen = (UINT2) (u2KeyDataLen +
                                            pRsnaSessNode->pRsnaPaePortInfo->
                                            u1IELen);
                    u2BufPos =
                        (UINT2) (u2BufPos +
                                 pRsnaSessNode->pRsnaPaePortInfo->u1IELen);

                }
            }
#ifdef WPA_WANTED
            else if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
            {
                if (WpaGetBeaconWPAIEElements
                    (pRsnaSessNode->pRsnaPaePortInfo->u2Port,
                     pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                     &(pRsnaSessNode->pRsnaPaePortInfo->u1IELen)) ==
                    RSNA_SUCCESS)
                {
                    MEMCPY ((pu1KeyDescBuf + u2BufPos),
                            pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                            pRsnaSessNode->pRsnaPaePortInfo->u1IELen);
                    u2KeyDataLen = (UINT2) (u2KeyDataLen +
                                            pRsnaSessNode->pRsnaPaePortInfo->
                                            u1IELen);
                    u2BufPos =
                        (UINT2) (u2BufPos +
                                 pRsnaSessNode->pRsnaPaePortInfo->u1IELen);

                }

            }
#endif
        }
        if ((MEMCMP (au1Algo, RSNA_CIPHER_SUITE_CCMP,
                     RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (au1Algo, WPA_CIPHER_SUITE_CCMP,
                     RSNA_CIPHER_SUITE_LEN) == 0))
        {
            u2PadLen = u2KeyDataLen % RSNA_CCMP_BLOCK_LEN;
            if (u2PadLen != 0)
            {
                u2PadLen = (UINT2) (RSNA_CCMP_BLOCK_LEN - u2PadLen);
            }
            u2KeyDataLen =
                (UINT2) (u2KeyDataLen +
                         (UINT2) (u2PadLen + RSNA_CCMP_BLOCK_LEN));
        }
        if ((pRsnaSessNode->u1ElemId == RSNA_IE_ID))
        {
            pu1KeyDescBuf[u2BufPos] = RSNA_EID_GENERIC;
            u2BufPos++;
            if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
            {
                u4TempCipherLen = pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                    u4GroupCipherKeyLen;
            }
#ifdef WPA_WANTED
            if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
            {
                u4TempCipherLen = pRsnaSessNode->pRsnaPaePortInfo->WpaConfigDB.
                    u4GroupCipherKeyLen;
            }
#endif
            pu1KeyDescBuf[u2BufPos] =
                (UINT1) (RSNA_CIPHER_SUITE_LEN + RSNA_GTK_HDR_LEN +
                         u4TempCipherLen);
            u2BufPos++;
            if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
            {
                MEMCPY ((pu1KeyDescBuf + u2BufPos), RSNA_GTK_KDE,
                        RSNA_CIPHER_SUITE_LEN);
                u2BufPos = (UINT2) (u2BufPos + RSNA_CIPHER_SUITE_LEN);
            }
#ifdef WPA_WANTED
            if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
            {
                MEMCPY ((pu1KeyDescBuf + u2BufPos), WPA_GTK_KDE,
                        RSNA_CIPHER_SUITE_LEN);
                u2BufPos = (UINT2) (u2BufPos + RSNA_CIPHER_SUITE_LEN);
            }
#endif
            u1KeyIdx = pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u1GN;
            pu1KeyDescBuf[u2BufPos] = (u1KeyIdx & 0x03);
            u2BufPos++;
            pu1KeyDescBuf[u2BufPos] = 0;
            u2BufPos++;
        }
#ifdef PMF_WANTED
        MEMCPY (au1GtkKey,
                &(pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.
                  au1Gtk[u1KeyIdx - RSNA_GTK_FIRST_IDX]),
                u4TempGroupCipherKeyLen);
#endif
        MEMCPY ((pu1KeyDescBuf + u2BufPos),
                &(pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.
                  au1Gtk[u1KeyIdx - RSNA_GTK_FIRST_IDX]),
                pRsnaSessNode->pRsnaPaePortInfo->
                RsnaConfigDB.u4GroupCipherKeyLen);
        u2BufPos = (UINT2) (u2BufPos + pRsnaSessNode->pRsnaPaePortInfo->
                            RsnaConfigDB.u4GroupCipherKeyLen);

#ifdef PMF_WANTED
        if (pRsnaKdeFlags->bIGtk == TRUE)
        {
            /* Usually the GTK Key Index would be either  1 or 2, hence the GTK key 
             * corresponding to the specified index would be sent to the STA. For each
             * successive  Group Key Handshake, GTK key index will get changed and
             * this is taken care by the state machine. Here for IGTK, the key indices
             * are chosen relative to the GTK key Index like as follows, 
             *
             * GTK KEY INDEX = 1 -----> IGTK KEY INDEX = 4 
             * GTK KEY INDEX = 2 -----> IGTK KEY INDEX = 5*/

            u2IGTKKeyIdx =
                (UINT2) (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u1GN -
                         RSNA_GTK_FIRST_IDX + RSNA_IGTK_FIRST_IDX);
            pu1KeyDescBuf[u2BufPos] = RSNA_EID_GENERIC;
            u2BufPos++;
            pu1KeyDescBuf[u2BufPos] =
                (UINT1) (RSNA_CIPHER_SUITE_LEN + RSNA_IGTK_HDR_LEN + 16);
            u2BufPos++;
            MEMCPY ((pu1KeyDescBuf + u2BufPos), RSNA_IGTK_KDE,
                    RSNA_CIPHER_SUITE_LEN);
            u2BufPos = (UINT2) (u2BufPos + RSNA_CIPHER_SUITE_LEN);
            MEMCPY (pu1KeyDescBuf + u2BufPos, &u2IGTKKeyIdx, 2);
            u2BufPos = u2BufPos + 2;
            MEMCPY (pu1KeyDescBuf + u2BufPos, &(pRsnaSessNode->
                                                RsnaIgtkSeqNum[u2IGTKKeyIdx -
                                                               RSNA_IGTK_FIRST_IDX]),
                    6);
            u2BufPos = u2BufPos + 6;
            MEMCPY (pu1KeyDescBuf + u2BufPos,
                    &(pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalIGtk.
                      au1IGtk[u2IGTKKeyIdx - RSNA_IGTK_FIRST_IDX]), 16);
            u2BufPos = u2BufPos + 16;
        }
#endif
        if (u2PadLen != 0)
        {
            pu1KeyDescBuf[u2BufPos] = RSNA_EID_GENERIC;
            u2BufPos++;
        }
        if ((MEMCMP (au1Algo, RSNA_CIPHER_SUITE_CCMP,
                     RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (au1Algo, WPA_CIPHER_SUITE_CCMP,
                     RSNA_CIPHER_SUITE_LEN) == 0))
        {
            if (u2KeyDataLen > RSNA_MAX_KEY_DATA_LEN)
            {
                RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                          "RsnaPktFillEapolKeyFrame:- "
                          "KeyData Greater than max size\n");
                return RSNA_FAILURE;
            }
            pu1Ptr = (pu1KeyDescBuf + *pu2BodyLen);
            MEMCPY (au1Buf, pu1Ptr, u2KeyDataLen);
            u2Len =
                (UINT2) (u2KeyDataLen -
                         RSNA_CCMP_BLOCK_LEN) / (UINT2) RSNA_CCMP_BLOCK_LEN;
            RsnaAlgoAesWrap (pRsnaSessNode->PtkSaDB.au1Kek, u2Len, au1Buf,
                             pu1Ptr);
        }
        else
        {
            pu1Ptr = pu1KeyDescBuf + (*pu2BodyLen);
            MEMCPY (pEapolKeyFrame->au1KeyIv,
                    (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter +
                     RSNA_KEY_IV_LEN), RSNA_KEY_IV_LEN);
            RsnaUtilIncByteArray (pRsnaSessNode->pRsnaPaePortInfo->
                                  RsnaGlobalGtk.au1Counter, RSNA_NONCE_LEN);
            MEMCPY (au1EncrKey, pEapolKeyFrame->au1KeyIv, RSNA_KEY_IV_LEN);
            MEMCPY ((au1EncrKey + RSNA_KEY_IV_LEN),
                    pRsnaSessNode->PtkSaDB.au1Kek, RSNA_KEK_KEY_LEN);
            RsnaAlgoRc4Skip (au1EncrKey, RSNA_NONCE_LEN, RSNA_RC4_SKIP, pu1Ptr,
                             u2KeyDataLen);
        }
        pEapolKeyFrame->u2KeyDataLen = OSIX_HTONS (u2KeyDataLen);
        (*pu2BodyLen) = (UINT2) ((*pu2BodyLen) + u2KeyDataLen);
    }
    else if (pRsnaKdeFlags->bIE == TRUE)
    {
#ifdef WPA_WANTED
        if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
        {
            if (WpaGetBeaconWPAIEElements
                (pRsnaSessNode->pRsnaPaePortInfo->u2Port,
                 pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                 &(pRsnaSessNode->pRsnaPaePortInfo->u1IELen)) == RSNA_SUCCESS)
            {
                u2KeyDataLen = (UINT2) (u2KeyDataLen +
                                        pRsnaSessNode->pRsnaPaePortInfo->
                                        u1IELen);
                MEMCPY ((pu1KeyDescBuf + u2BufPos),
                        pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                        pRsnaSessNode->pRsnaPaePortInfo->u1IELen);
                pEapolKeyFrame->u2KeyDataLen = OSIX_HTONS (u2KeyDataLen);
                (*pu2BodyLen) = (UINT2) ((*pu2BodyLen) + u2KeyDataLen);
            }
        }
#endif
        if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
        {
            u2KeyDataLen = (UINT2) (u2KeyDataLen +
                                    pRsnaSessNode->pRsnaPaePortInfo->u1IELen);
            MEMCPY ((pu1KeyDescBuf + u2BufPos),
                    pRsnaSessNode->pRsnaPaePortInfo->au1RsnaIE,
                    pRsnaSessNode->pRsnaPaePortInfo->u1IELen);
            pEapolKeyFrame->u2KeyDataLen = OSIX_HTONS (u2KeyDataLen);
            (*pu2BodyLen) = (UINT2) ((*pu2BodyLen) + u2KeyDataLen);

        }
    }
    else if (pRsnaKdeFlags->bPmkIdKde == TRUE)
    {
        u2KeyDataLen = (UINT2) (u2KeyDataLen + RSNA_PMKID_KDE_LEN);
        MEMCPY ((pu1KeyDescBuf + u2BufPos),
                pRsnaSessNode->au1PmkIdKde, RSNA_PMKID_KDE_LEN);
        pEapolKeyFrame->u2KeyDataLen = OSIX_HTONS (u2KeyDataLen);
        (*pu2BodyLen) = (UINT2) ((*pu2BodyLen) + u2KeyDataLen);
    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaPnacTxFrame                                            *
*                                                                           *
* Description  : Sends an PNAC EAP frame                                    *
*                                                                           *
* Input        : pu1KeyFrame:- EAP packet to be transmitted                 *
*                u4PktLen  :-  EAP Packet Length                            *
*                u2PortNum :-  Port Number in which the packet has to be    *
*                              sent                                         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaPnacTxFrame (UINT1 *pu1KeyFrame, UINT4 u4PktLen, UINT2 u2PortNum)
{

#ifdef WLC_WANTED
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;
#endif
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2OffSet = 0;
    UINT4               u4BssIfIndex = 0;

    RsnaEapolRtyProcess (pu1KeyFrame, u4PktLen, u2PortNum);
    /* Allocate a CRU buffer message chain for this KeyEapol frame */
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, u2OffSet)) == NULL)
    {
        return RSNA_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, pu1KeyFrame, 0, u4PktLen);

#ifdef WLC_WANTED
    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

    if (pCapwapMsgStruct == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                  "RsnaTxFrame:- " "UtlShMemAllocWlcBuf returned failure\n");
        return RSNA_FAILURE;
    }

    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
    pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId = u2PortNum;
    pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;

    /* Invoke the WSSIF module to send the packet to MAC Handler module */
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CFA_QUEUE_REQ, pCapwapMsgStruct)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return RSNA_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
#else
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

#endif
    UNUSED_PARAM (u4BssIfIndex);
    return RSNA_SUCCESS;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaTxFrame                                                *
*                                                                           *
* Description  : Sends an RSNA Eapol Key frame                              *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                u2KeyInfo :- Specifies the characteristics of the key      *
*                pu1KeyRsc :- Contains the receive sequence counter         *
*                pu1Nonce  :- Nonce to be conveyed to the supplicant        *
*                bGtk      :- Flag indicates whether gtk is part of exch    *
*                bIE       :- Flag Indicates whether IE is part of exch     *
*                bPmkIdKde :- Flag Indicates whether PmkId is part of exch  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaTxFrame (UINT1 *pu1KeyFrame, tStaMac StaMac, tBssid BssMac,
             UINT4 u4BssIfIndex, UINT4 u4BufLen)
{
#ifdef WLC_WANTED
    unWlcHdlrMsgStruct *pCapwapMsgStruct = NULL;
#endif
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2OffSet = 0;
    UINT2               u2PktLen = 0;
    UINT2               u2PacketType = 0x888E;
    UINT2               u2Val = 0;

    /* Compute the EAPOL Key packet length by considering, */
    /* its Layer 2 header also */
    u2PktLen = (UINT2) (u4BufLen + 14);

    /* Allocate a CRU buffer message chain for this KeyEapol frame */
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u2PktLen, u2OffSet)) == NULL)
    {
        return RSNA_FAILURE;
    }

    u2Val = (UINT2) OSIX_HTONS (u2PacketType);

    CRU_BUF_Copy_OverBufChain (pBuf, StaMac, 0, 6);
    CRU_BUF_Copy_OverBufChain (pBuf, BssMac, 6, 6);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Val, 12, 2);
    CRU_BUF_Copy_OverBufChain (pBuf, pu1KeyFrame, 14, u4BufLen);

#ifdef WLC_WANTED
    pCapwapMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

    if (pCapwapMsgStruct == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_DATA_PATH_TRC,
                  "RsnaTxFrame:- " "UtlShMemAllocWlcBuf returned failure\n");
        return RSNA_FAILURE;
    }

    MEMSET (pCapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pCapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
    pCapwapMsgStruct->WlcHdlrQueueReq.u2SessId = (UINT2) u4BssIfIndex;
    pCapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;

    /* Invoke the WSSIF module to send the packet to MAC Handler module */
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CFA_QUEUE_REQ, pCapwapMsgStruct)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
        return RSNA_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pCapwapMsgStruct);
#else
    UNUSED_PARAM (u4BssIfIndex);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

#endif
    return RSNA_SUCCESS;

}

/****************************************************************************
* Function     : RsnaPktConstructEapolHdr                                   *
*                                                                           *
* Description  : Function to construct Eapol Header                         *
*                                                                           *
* Input        : pu1Pkt :- Pointer to the pkt to be sent                    *
*                pRsnaSessNode :- Pointer to Rsna Session Node              *
*                u2PktLen :- Length of the packet to be sent                *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PRIVATE UINT4
RsnaPktConstructEapolHdr (UINT1 *pu1Pkt, tRsnaSessionNode * pRsnaSessNode,
                          UINT2 u2PktLen)
{
    UINT1              *pu1WritePtr = NULL;
    UINT1               u1Val = 0;

    RSNA_UNUSED (pRsnaSessNode);
    pu1WritePtr = pu1Pkt;

    /* Write the EAPOL header - Protocol Version */
    u1Val = RSNA_EAPOL_VERSION;
    *(pu1WritePtr) = u1Val;
    pu1WritePtr++;

    /* Write the EAPOL Packet Type */
    *(pu1WritePtr) = RSNA_EAPOL_KEY;
    pu1WritePtr++;

    /* Write the EAPOL packet body length */
    *((UINT2 *) (VOID *) pu1WritePtr) = OSIX_HTONS (u2PktLen);

    return (RSNA_SUCCESS);

}
