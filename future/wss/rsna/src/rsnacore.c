/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnacore.c,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"
#include "wssstawlcprot.h"
#include "wssstawlcmacr.h"

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkInitializeState                                 *
*                                                                           *
* Description  : Function to Initialize the ptk state machine               *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaCorePtkInitializeState (tRsnaSessionNode * pRsnaSessNode)
{
    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_INITIALIZE;
    if (pRsnaSessNode->bInit == TRUE)
    {
        pRsnaSessNode->bIsStateChanged = FALSE;
    }
    if (pRsnaSessNode->bGUpdateStationKeys == TRUE)
    {
        pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u4GkeyDoneStations--;
    }
    pRsnaSessNode->bGUpdateStationKeys = FALSE;
    pRsnaSessNode->bPair = TRUE;
    pRsnaSessNode->bPtkValid = FALSE;

    RsnaUtilFillWssNotifyParams (pRsnaSessNode->pRsnaPaePortInfo, pRsnaSessNode,
                                 WSSRSNA_DEL_KEY, TRUE);

    MEMSET (&(pRsnaSessNode->PtkSaDB), 0, sizeof (tRsnaPtkSa));
    MEMSET (pRsnaSessNode->au1RsnaKeyReplayCtr, 0, RSNA_REPLAY_CTR_LEN);
    pRsnaSessNode->u4TimeOutCtr = 0;

    if ((pRsnaSessNode->u1ElemId == WPA_IE_ID) ||
        (pRsnaSessNode->u1ElemId == RSNA_IE_ID))
    {
        pRsnaSessNode->bPInitAkeys = OSIX_FALSE;
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkDisconnectState                                 *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to disconnect  *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkDisconnectState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_DISCONNECT;
    pRsnaSessNode->bDisconnect = FALSE;
    RsnaSessDisconnectSta (pRsnaSessNode);

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkDisconnectedState                               *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                disconnected state                                         *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaCorePtkDisconnectedState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_DISCONNECTED;
    pRsnaSessNode->bDeAuthReq = FALSE;
    pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u4GNoStations--;

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkAuthenticationState                             * 
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                Authentication State                                       *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaCorePtkAuthenticationState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_AUTHENTICATION;
    pRsnaSessNode->bAuthenticationRequest = FALSE;
    pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u4GNoStations++;
    MEMSET (&(pRsnaSessNode->PtkSaDB), 0, sizeof (tRsnaPtkSa));
    pRsnaSessNode->bPtkValid = FALSE;
    pRsnaSessNode->pPnacAuthSessionNode->authFsmInfo.u2PortMode =
        PNAC_PORTCNTRL_AUTO;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkAuthentication2State                            *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                Authentication2 State                                      *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkAuthentication2State (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_AUTHENTICATION2;

    MEMCPY (&(pRsnaSessNode->au1ANonce),
            &(pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter),
            RSNA_NONCE_LEN);

    RsnaUtilIncByteArray (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.
                          au1Counter, RSNA_NONCE_LEN);

    pRsnaSessNode->bReAutenticationRequest = FALSE;
    pRsnaSessNode->u4TimeOutCtr = 0;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkStartState                                      *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                Ptk Start State                                            *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkStartState (tRsnaSessionNode * pRsnaSessNode)
{

    BOOL1               bPmkIdKde = FALSE;
    UINT1               u1Offset = 0;
    UINT2               u2KeyInfo = 0;

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_PTKSTART;
    pRsnaSessNode->bPTKRequest = FALSE;
    pRsnaSessNode->bTimeOutEvt = FALSE;

    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
              " sending 1/4 msg of 4-Way Handshake ");

    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   "for Station MAC:%x:%x:%x:%x:%x:%x\n",
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[0],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[1],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[2],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[3],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[4],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[5]);

    if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 RSNA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 RSNA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0)
#ifdef PMF_DEBUG
        || (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                    RSNA_AKM_UNSPEC_802_1X_SHA256, RSNA_CIPHER_SUITE_LEN) == 0)
        ||
        (MEMCMP
         (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
          RSNA_AKM_PSK_SHA256_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0)
#endif
        )
    {
        bPmkIdKde = TRUE;
        pRsnaSessNode->au1PmkIdKde[u1Offset] = RSNA_EID_GENERIC;
        u1Offset++;
        pRsnaSessNode->au1PmkIdKde[u1Offset] = (RSNA_CIPHER_SUITE_LEN +
                                                RSNA_PMKID_LEN);
        u1Offset++;
        MEMCPY (&(pRsnaSessNode->au1PmkIdKde[u1Offset]), RSNA_PMKID_KDE,
                RSNA_CIPHER_SUITE_LEN);
        u1Offset = (UINT1) (u1Offset + RSNA_CIPHER_SUITE_LEN);
        MEMCPY (&(pRsnaSessNode->au1PmkIdKde[u1Offset]),
                &(pRsnaSessNode->au1PmkId), RSNA_PMKID_LEN);
    }

    /*PMF_WANTED */

    if (
#ifdef PMF_DEBUG
           ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                     RSNA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                     RSNA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0)) &&
#endif
           ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                     RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                     WPA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                     WPA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                     WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0)))
    {
        u2KeyInfo = RSNA_KEY_INFO_HMACMD5_RC4;
    }
#ifdef PMF_DEBUG
    else if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                      RSNA_AKM_UNSPEC_802_1X_SHA256,
                      RSNA_CIPHER_SUITE_LEN) == 0)
             ||
             (MEMCMP
              (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
               RSNA_AKM_PSK_SHA256_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        u2KeyInfo = RSNA_KEY_INFO_CMAC_128_AES;

    }
#endif
    else if (((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                       RSNA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
              (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                       RSNA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0))
             &&
             ((MEMCMP
               (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0)
              ||
              (MEMCMP
               (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite, WPA_AKM_UNSPEC_802_1X,
                RSNA_CIPHER_SUITE_LEN) == 0)
              ||
              (MEMCMP
               (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                WPA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0)
              ||
              (MEMCMP
               (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                WPA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0)))
    {
        u2KeyInfo = RSNA_KEY_INFO_HMACSHA1_AES;
    }

    u2KeyInfo |= RSNA_KEY_INFO_ACK;
    u2KeyInfo |= RSNA_KEY_INFO_KEY_TYPE;
    if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
    {
        RsnaPktSendEapolKeyPkt (pRsnaSessNode, u2KeyInfo, NULL,
                                pRsnaSessNode->au1ANonce, FALSE, FALSE,
                                bPmkIdKde, FALSE);
    }
    else if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
    {
        RsnaPktSendEapolKeyPkt (pRsnaSessNode, u2KeyInfo, NULL,
                                pRsnaSessNode->au1ANonce, FALSE, FALSE,
                                bPmkIdKde, FALSE);
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCoreInitPmkState                                       *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                InitPmkState                                               *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCoreInitPmkState (tRsnaSessionNode * pRsnaSessNode)
{

    tPnacAuthSessionNode *pAuthSessNode = NULL;

    pRsnaSessNode->bIsStateChanged = TRUE;
    pAuthSessNode = pRsnaSessNode->pPnacAuthSessionNode;

    if (pAuthSessNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCoreInitPmkState:- No Pnac Session !!!! \n");
        pRsnaSessNode->bDisconnect = TRUE;
        return (RSNA_SUCCESS);
    }
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_INITPMK;
    pRsnaSessNode->pPnacAuthSessionNode->authFsmInfo.bKeyRun = FALSE;
    pRsnaSessNode->pRsnaSupInfo->bReqReplayCtrValid = FALSE;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCoreInitPskState                                       *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                InitPskState                                               *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCoreInitPskState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_INITPSK;
    pRsnaSessNode->pRsnaSupInfo->bReqReplayCtrValid = FALSE;
    pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.bPskStatus = TRUE;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkCalcNegotiatingState                            *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                PtkCalcNegotiatingState                                    *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkCalcNegotiatingState (tRsnaSessionNode * pRsnaSessNode)
{
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;
    tWlanSTATrapInfo    WTPTrapInfo;

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_PTKCALCNEGOTIATING;

    if (RsnaSessGenPtkFromPmk (pRsnaSessNode) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessGenPtkFromPmk returned failure !!!! \n");
        return (RSNA_FAILURE);
    }
    if (RsnaProcVerifyKeyMic (&(pRsnaSessNode->PtkSaDB),
                              pRsnaSessNode->pu1RxEapolKeyPkt,
                              pRsnaSessNode->u2PktLen) != RSNA_SUCCESS)
    {
        /* Sending Auth Failure Trap in case of PSK and Dot1x authentication */
        MEMSET (&WTPTrapInfo, 0, sizeof (tWlanSTATrapInfo));
        WTPTrapInfo.u4ifIndex = pRsnaSessNode->pRsnaPaePortInfo->u2Port;
        MEMCPY (&WTPTrapInfo.au1CliMac,
                pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr, MAC_ADDR_LEN);

        if ((MEMCMP
             (&pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
              RSNA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) != 0)
#ifdef PMF_WANTED
            ||
            (MEMCMP
             (&pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
              RSNA_AKM_PSK_SHA256_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) != 0)
#endif
            )
        {
            WTPTrapInfo.u4AuthFailure = WSS_WLAN_DOT1X_AUTH_FAIL_TRAP;
        }
        else
        {
            WTPTrapInfo.u4AuthFailure = WSS_WLAN_PSK_AUTH_FAIL_TRAP;

        }
        WlanSnmpifSendTrap (WSS_WLAN_AUTH_FAILURE, &WTPTrapInfo);

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaProcVerifyKeyMic returned failure !!!! \n");
        pRsnaSessNode->bDisconnect = TRUE;
        pRsnaSessNode->u4ReasonCode = RSNA_REASON_MICHAEL_MIC_FAILURE;
        pRsnaSuppStatsInfo =
            RsnaUtilGetStationFromStatsTable (pRsnaSessNode->pRsnaPaePortInfo->
                                              u2Port,
                                              pRsnaSessNode->pRsnaSupInfo->
                                              au1SuppMacAddr);

        if (pRsnaSuppStatsInfo != NULL)
        {
            if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                         RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0 ||
                MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                        WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0)
            {
                pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipLocalMicFailures++;
            }

        }

        pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
            u4NoOf4WayHandshakeFailures++;
        RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr));
        pRsnaSessNode->u4RefCount--;
        return (RSNA_SUCCESS);
    }

#ifdef PMF_WANTED
    pRsnaSessNode->pRsnaSupInfo->u2TransactionIdentifier++;
#endif
    pRsnaSessNode->bPtkValid = TRUE;
    pRsnaSessNode->bMICVerified = TRUE;
    pRsnaSessNode->bEAPOLKeyReceived = FALSE;
    RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr));
    pRsnaSessNode->u4RefCount--;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkCalcNegotiating2State                           *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                PtkCalcNegotiating2State                                   *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkCalcNegotiating2State (tRsnaSessionNode * pRsnaSessNode)
{
    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_PTKCALCNEGOTIATING2;
    pRsnaSessNode->u4TimeOutCtr = 0;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkInitNegitiatingState                            *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                PtkInitNegotiationgState                                   *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkInitNegotiatingState (tRsnaSessionNode * pRsnaSessNode)
{
    UINT2               u2KeyInfo = 0;
    UINT1               au1KeyRsc[RSNA_KEY_RSC_LEN] = "";
    BOOL1               bIGtkKde = FALSE;

    MEMSET (au1KeyRsc, 0, RSNA_KEY_RSC_LEN);

    pRsnaSessNode->bIsStateChanged = FALSE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_INITNEGOTIATING;
    pRsnaSessNode->bTimeOutEvt = FALSE;

    MEMSET (au1KeyRsc, 0, RSNA_KEY_RSC_LEN);
#ifdef PMF_DEBUG_WANTED
    if (RsnaSessGetSeqNum (pRsnaSessNode, au1KeyRsc) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCorePtkInitNegotiatingState:- "
                  "RsnaSessGetSeqNum returned failure !!!! \n");
        return (OSIX_FAILURE);

    }
#endif
    if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 RSNA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 RSNA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 WPA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0) ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 WPA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                     RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0) ||
            (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                     WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0))
        {
            u2KeyInfo = RSNA_KEY_INFO_HMACMD5_RC4;
        }
        else if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                          RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0)
                 ||
                 (MEMCMP
                  (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                   WPA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0))
        {
            u2KeyInfo = RSNA_KEY_INFO_HMACSHA1_AES;
        }
    }
#ifdef PMF_WANTED
    else if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                      RSNA_AKM_UNSPEC_802_1X_SHA256,
                      RSNA_CIPHER_SUITE_LEN) == 0)
             ||
             (MEMCMP
              (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
               RSNA_AKM_PSK_SHA256_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        u2KeyInfo = RSNA_KEY_INFO_CMAC_128_AES;

    }
#endif
    else
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No matches Found! \n");
        return (RSNA_FAILURE);
    }

    if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
    {
        u2KeyInfo |= RSNA_KEY_INFO_SECURE;
        u2KeyInfo |= RSNA_KEY_INFO_ENCR_KEY_DATA;
    }
    u2KeyInfo |= RSNA_KEY_INFO_MIC;
    u2KeyInfo |= RSNA_KEY_INFO_ACK;
    u2KeyInfo |= RSNA_KEY_INFO_INSTALL;
    u2KeyInfo |= RSNA_KEY_INFO_KEY_TYPE;

    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
              " sending 3/4 msg of 4-Way Handshake ");

    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   "for Station MAC:%x:%x:%x:%x:%x:%x\n",
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[0],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[1],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[2],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[3],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[4],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[5]);

    if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
    {
#ifdef PMF_WANTED
        if (pRsnaSessNode->pRsnaSupInfo->bPMFCapable)
        {
            bIGtkKde = TRUE;
        }
#endif
        if (RsnaPktSendEapolKeyPkt
            (pRsnaSessNode, u2KeyInfo, au1KeyRsc, pRsnaSessNode->au1ANonce,
             TRUE, TRUE, FALSE, bIGtkKde) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaCorePtkInitNegotiatingState:- "
                      "RsnaPktSendEapolKeyPkt returned failure !!!! \n");
            return (RSNA_FAILURE);
        }
    }
    else if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
    {
        if (RsnaPktSendEapolKeyPkt
            (pRsnaSessNode, u2KeyInfo, NULL, pRsnaSessNode->au1ANonce,
             FALSE, TRUE, FALSE, FALSE) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaCorePtkInitNegotiatingState:- "
                      "RsnaPktSendEapolKeyPkt returned failure !!!! \n");
            return (RSNA_FAILURE);
        }
    }
    pRsnaSessNode->u4TimeOutCtr++;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkInitDoneState                                   *
*                                                                           *
* Description  : Function to Change the state of the Ptk Fsm to             *
*                PtkInitDoneState                                           *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkInitDoneState (tRsnaSessionNode * pRsnaSessNode)
{
    tWlanSTATrapInfo    WTPTrapInfo;

    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->u1PtkFsmState = RSNA_4WAY_PTKINITDONE;
    pRsnaSessNode->bEAPOLKeyReceived = FALSE;
    if (pRsnaSessNode->bPair == TRUE)
    {
        if (RsnaUtilFillWssNotifyParams (pRsnaSessNode->pRsnaPaePortInfo,
                                         pRsnaSessNode, WSSRSNA_SET_KEY,
                                         WSSRSNA_PTK_KEY) == OSIX_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaCorePtkInitDoneState:- RsnaApmeEnqMsgToApme "
                      " returned Failure !!!! \n");
            return (RSNA_FAILURE);
        }
        pRsnaSessNode->pRsnaPaePortInfo->u4FourWayHandshakeCounter++;
    }
    /*Sending STATION CONNECTED TRAP in case of DOT1X and PSK */
    MEMSET (&WTPTrapInfo, 0, sizeof (tWlanSTATrapInfo));
    WTPTrapInfo.u4ifIndex = pRsnaSessNode->pRsnaPaePortInfo->u2Port;
    MEMCPY (&WTPTrapInfo.au1CliMac,
            pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr, MAC_ADDR_LEN);
    WTPTrapInfo.u4AssocStatus = WSS_WLAN_STA_CONNECTED;
    WlanSnmpifSendTrap (WSS_WLAN_CONNECTION_STATUS, &WTPTrapInfo);

    MEMSET (pRsnaSessNode->PtkSaDB.au1Ptk, 0, RSNA_MAX_PTK_KEY_LEN);
    pRsnaSessNode->bIsPairwiseSet = TRUE;
    pRsnaSessNode->pPnacAuthSessionNode->authFsmInfo.
        u2AuthControlPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
    pRsnaSessNode->pRsnaSupInfo->u4SuppState = RSNA_STA_AUTHORIZED;

    pRsnaSessNode->pPnacAuthSessionNode->authFsmInfo.bKeyAvailable = FALSE;
    pRsnaSessNode->pPnacAuthSessionNode->authFsmInfo.bKeyDone = TRUE;
    if ((pRsnaSessNode->u1ElemId == WPA_IE_ID))
    {
        pRsnaSessNode->bPInitAkeys = OSIX_TRUE;
    }
    if (RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr)) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "Stop Tmr for ReTrans Failed");
        return (RSNA_FAILURE);
    }
    pRsnaSessNode->u4RefCount--;

    pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bKeyDeleteFlag = FALSE;

    if (pRsnaSessNode->pRsnaPaePortInfo->bGtkInstallFlag == FALSE)
    {
        pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bGTKAuthenticator = TRUE;
        pRsnaSessNode->pRsnaPaePortInfo->bGtkInstallFlag = TRUE;
    }
    if (pRsnaSessNode->pRsnaPaePortInfo->bGtkRefreshFlag == FALSE)
    {
        /*    pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bGInit = TRUE;
           pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bGTKAuthenticator = TRUE; */
        if ((pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.u1GroupRekeyMethod ==
             RSNA_GROUP_REKEY_TIMEBASED)
            || (pRsnaSessNode->pRsnaPaePortInfo->RsnaConfigDB.
                u1GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEPACKETBASED))
        {

            RsnaTmrStopTimer (&
                              (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.
                               RsnaUtilGtkRekeyTmr));
            RsnaTmrStartTimer (&
                               (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.
                                RsnaUtilGtkRekeyTmr), RSNA_GTK_TIMER_ID,
                               pRsnaSessNode->pRsnaPaePortInfo->
                               RsnaConfigDB.u4GroupGtkRekeyTime,
                               pRsnaSessNode->pRsnaPaePortInfo);
            pRsnaSessNode->pRsnaPaePortInfo->bGtkRefreshFlag = TRUE;
        }

    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkGrpKeyIdleState                                 *
*                                                                           *
* Description  : Function to Change the state of the Ptk Group Key FSM      *
*                To Idle State                                              *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkGrpKeyIdleState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->u1PerStaGtkFsm = RSNA_PER_STA_GKH_IDLE;
    if (pRsnaSessNode->bInit == TRUE)
    {
        pRsnaSessNode->bIsStateChanged = FALSE;
    }
    pRsnaSessNode->u4GTimeoutCtr = 0;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkGrpReKeyNegoState                               *
*                                                                           *
* Description  : Function to Change the state of the Ptk Group Key FSM to   *
*                GroupReKeyNegotiatingState                                 *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkGrpReKeyNegoState (tRsnaSessionNode * pRsnaSessNode)
{
    UINT1               au1KeyRsc[RSNA_KEY_RSC_LEN] = "";
    UINT2               u2KeyInfo = 0;

    MEMSET (au1KeyRsc, 0, RSNA_KEY_RSC_LEN);

    pRsnaSessNode->u1PerStaGtkFsm = RSNA_PER_STA_GKH_REKEYNEGOTIATING;
    pRsnaSessNode->bIsStateChanged = TRUE;
    MEMSET (au1KeyRsc, 0, RSNA_KEY_RSC_LEN);
    pRsnaSessNode->bTimeOutEvt = FALSE;

    if ((pRsnaSessNode->u1ElemId == WPA_IE_ID) ||
        (pRsnaSessNode->u1ElemId == RSNA_IE_ID))
    {
        pRsnaSessNode->bPInitAkeys = OSIX_FALSE;
    }
    if (pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u1Fsm ==
        RSNA_GLOBAL_SETKEYSDONE)
    {
#ifdef PMF_DEBUG_WANTED
        if (RsnaSessGetSeqNum (pRsnaSessNode, au1KeyRsc) == OSIX_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaCorePtkGrpReKeyNegoState:- "
                      "RsnaSessGetSeqNum returned failure !!!! \n");
            return (OSIX_FAILURE);
        }
#endif
    }
    if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                 RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0) ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                 WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        u2KeyInfo = RSNA_KEY_INFO_HMACMD5_RC4;
    }
    else if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0) ||
             (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      WPA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        u2KeyInfo = RSNA_KEY_INFO_HMACSHA1_AES;
    }
    u2KeyInfo |= RSNA_KEY_INFO_SECURE;
    u2KeyInfo |= RSNA_KEY_INFO_MIC;
    u2KeyInfo |= RSNA_KEY_INFO_ACK;
    if (pRsnaSessNode->u1ElemId == RSNA_IE_ID ||
        pRsnaSessNode->u1ElemId == WPA_IE_ID)
    {
        u2KeyInfo |= RSNA_KEY_INFO_ENCR_KEY_DATA;
    }
    if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
    {
        u2KeyInfo |= RSNA_KEY_INFO_KEY_INDEX;
    }
    if (pRsnaSessNode->u1PtkFsmState == RSNA_4WAY_PTKINITDONE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " sending 1/2 msg of 2-Way Group Handshake ");

        RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                       "for Station MAC:%x:%x:%x:%x:%x:%x\n",
                       pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[0],
                       pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[1],
                       pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[2],
                       pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[3],
                       pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[4],
                       pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[5]);
        if (pRsnaSessNode->u1ElemId == RSNA_IE_ID)
        {
            if (RsnaPktSendEapolKeyPkt (pRsnaSessNode, u2KeyInfo, au1KeyRsc,
                                        pRsnaSessNode->pRsnaPaePortInfo->
                                        RsnaGlobalGtk.au1Gnonce, TRUE, FALSE,
                                        FALSE, FALSE) != RSNA_SUCCESS)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaCorePtkGrpReKeyNegoState:- "
                          "RsnaPktSendEapolKeyPkt returned failure !!!! \n");
                return (RSNA_FAILURE);
            }
        }
        else if (pRsnaSessNode->u1ElemId == WPA_IE_ID)
        {
            if (RsnaPktSendEapolKeyPkt (pRsnaSessNode, u2KeyInfo, au1KeyRsc,
                                        pRsnaSessNode->pRsnaPaePortInfo->
                                        RsnaGlobalGtk.au1Gnonce, TRUE, FALSE,
                                        FALSE, FALSE) != RSNA_SUCCESS)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaCorePtkGrpReKeyNegoState:- "
                          "RsnaPktSendEapolKeyPkt returned failure !!!! \n");
                return (RSNA_FAILURE);
            }
        }

        pRsnaSessNode->u4GTimeoutCtr++;
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkGrpReKeyEstState                                *
*                                                                           *
* Description  : Function to Change the state of the Ptk Group Key FSM to   *
*                GroupReKeyEstablishedState                                 *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkGrpReKeyEstState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->u1PerStaGtkFsm = RSNA_PER_STA_GKH_REKEYESTABLISHED;
    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->bGUpdateStationKeys = FALSE;
    pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u4GkeyDoneStations--;
    pRsnaSessNode->u4GTimeoutCtr = 0;
    pRsnaSessNode->bEAPOLKeyReceived = FALSE;

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCorePtkGrpKeyErrorState                                *
*                                                                           *
* Description  : Function to Change the state of the Ptk GroupKeyFsm to     *
*                GroupKeyErrorState                                         *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCorePtkGrpKeyErrorState (tRsnaSessionNode * pRsnaSessNode)
{

    pRsnaSessNode->u1PerStaGtkFsm = RSNA_PER_STA_GKH_KEYERROR;
    pRsnaSessNode->bIsStateChanged = TRUE;
    pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.u4GkeyDoneStations--;
    pRsnaSessNode->bGUpdateStationKeys = FALSE;
    pRsnaSessNode->u4ReasonCode = RSNA_REASON_GROUP_KEY_UPDATE_TIMEOUT;
    pRsnaSessNode->bDisconnect = TRUE;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     :RsnaCoreGlobalGrpKeyInitState                               *
*                                                                           *
* Description  : Function to Change the state of the Ptk Group Key FSM to   *
*                GroupKeyInitState                                          *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCoreGlobalGrpKeyInitState (tRsnaGlobalGtk * pRsnaGlobalGtk)
{
    UINT1               u1Tmp = 0;
#ifdef PMF_WANTED
    UINT1               u1IGtkTmp = 0;
#endif
    UINT1               u1Count = 0;

    pRsnaGlobalGtk->bIsStateChanged = FALSE;
    pRsnaGlobalGtk->u1Fsm = RSNA_GLOBAL_GKH_INIT;
    MEMSET (pRsnaGlobalGtk->au1Gtk, 0, sizeof (pRsnaGlobalGtk->au1Gtk));

    pRsnaGlobalGtk->bGInit = FALSE;

    if (pRsnaGlobalGtk->u1RekeyFlag == FALSE)
    {
        pRsnaGlobalGtk->u1GN = RSNA_GTK_FIRST_IDX;
        pRsnaGlobalGtk->u1GM = RSNA_GTK_SECOND_IDX;
#ifdef PMF_WANTED
        pRsnaGlobalGtk->pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGN =
            RSNA_IGTK_FIRST_IDX;
        pRsnaGlobalGtk->pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGM =
            RSNA_IGTK_FIRST_IDX;
#endif
    }
    else
    {
        for (u1Count = 0; u1Count < RSNA_GTK_SECOND_IDX; u1Count++)
        {
            u1Tmp = pRsnaGlobalGtk->u1GM;
            pRsnaGlobalGtk->u1GM = pRsnaGlobalGtk->u1GN;
            pRsnaGlobalGtk->u1GN = u1Tmp;
#ifdef PMF_WANTED
            u1IGtkTmp = pRsnaGlobalGtk->pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGM;
            pRsnaGlobalGtk->pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGM =
                pRsnaGlobalGtk->pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGN;
            pRsnaGlobalGtk->pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGN = u1IGtkTmp;
#endif
        }
    }

    MEMCPY (pRsnaGlobalGtk->au1Gnonce, pRsnaGlobalGtk->au1Counter,
            RSNA_NONCE_LEN);

    if (RsnaSessGenGtkFromGmk (pRsnaGlobalGtk) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "PtkInitDoneState:- RsnaSessGenGtkFromGmk "
                  " returned Failure !!!! \n");
        return (RSNA_FAILURE);

    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     :RsnaCoreGlobalGrpSetKeyDoneState                            *
*                                                                           *
* Description  : Function to Change the state of the Ptk Group Key FSM to   *
*                GroupSetKeysDoneState                                      *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCoreGlobalGrpSetKeyDoneState (tRsnaGlobalGtk * pRsnaGlobalGtk)
{
    pRsnaGlobalGtk->u1Fsm = RSNA_GLOBAL_SETKEYSDONE;
    pRsnaGlobalGtk->bIsStateChanged = TRUE;

    if (RsnaUtilFillWssNotifyParams (pRsnaGlobalGtk->pRsnaPaePortInfo,
                                     NULL, WSSRSNA_SET_KEY,
                                     WSSRSNA_GTK_KEY) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCoreGlobalGroupSetKeysDoneState:- "
                  "RsnaApmeEnqMsgToApme " "returned Failure !!!! \n");
        return (RSNA_FAILURE);
    }
#ifdef PMF_WANTED
    if (RsnaUtilFillWssNotifyParams (pRsnaGlobalGtk->pRsnaPaePortInfo,
                                     NULL, WSSRSNA_SET_KEY,
                                     WSSRSNA_IGTK_KEY) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCoreGlobalGroupSetKeysDoneState:- "
                  "RsnaApmeEnqMsgToApme " "returned Failure !!!! \n");
        return (RSNA_FAILURE);
    }
#endif
    /* Setting gInit as true so that the state transition to RSNA_GLOBAL_SETKEYSDONE 
     * state happens properly*/
    pRsnaGlobalGtk->bGInit = FALSE;

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     :RsnaCoreGlobalGrpSetKeysState                               *
*                                                                           *
* Description  : Function to Change the state of the Ptk Global Key FSM to  *
*                GroupSetKeysState                                          *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaCoreGlobalGrpSetKeysState (tRsnaGlobalGtk * pRsnaGlobalGtk)
{

    UINT1               u1Tmp = 0;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;

    pRsnaGlobalGtk->bIsStateChanged = TRUE;
    pRsnaGlobalGtk->u1Fsm = RSNA_GLOBAL_GKH_SETKEYS;
    pRsnaGlobalGtk->bGtkRekeyEnable = FALSE;

    u1Tmp = pRsnaGlobalGtk->u1GM;
    pRsnaGlobalGtk->u1GM = pRsnaGlobalGtk->u1GN;
    pRsnaGlobalGtk->u1GN = u1Tmp;
    pRsnaGlobalGtk->u4GkeyDoneStations = pRsnaGlobalGtk->u4GNoStations;
    MEMCPY (pRsnaGlobalGtk->au1Gnonce, pRsnaGlobalGtk->au1Counter,
            RSNA_NONCE_LEN);

    if (RsnaSessGenGtkFromGmk (pRsnaGlobalGtk) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaCoreGlobalGroupSetKeysState:- "
                  "RsnaSessGenGtkFromGmk Failed !!!!! \n");
        return (RSNA_FAILURE);
    }

    if (pRsnaGlobalGtk->pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCoreGlobalGroupSetKeysState:-"
                  "pRsnaGlobalGtk->pRsnaPaePortInfo is Null !!!! \n");
        return (RSNA_FAILURE);
    }

    pRsnaPaePortInfo = pRsnaGlobalGtk->pRsnaPaePortInfo;

    TMO_SLL_Scan (&(pRsnaPaePortInfo->RsnaSupInfoList), pRsnaSupInfo,
                  tRsnaSupInfo *)
    {

        if ((pRsnaSupInfo->pRsnaSessNode == NULL) ||
            (pRsnaSupInfo->pRsnaSessNode->bDeleteSession == TRUE))
        {
            continue;
        }

        pRsnaSupInfo->pRsnaSessNode->bGUpdateStationKeys = TRUE;
        if (RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode) != RSNA_SUCCESS)
        {

            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaCoreGlobalGroupSetKeysState:-"
                      "RsnaMainFsmStart returned failure !!!! \n");
            return (RSNA_FAILURE);
        }
    }
    return (RSNA_SUCCESS);
}
