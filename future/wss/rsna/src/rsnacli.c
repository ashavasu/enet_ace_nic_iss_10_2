/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnacli.c,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : rsnacli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      :                                                  |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : RSNA                                             |
 * |                                                                           |
 * |  MODULE NAME           : RSNA CLI configurations                          |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : CLI interfaces for RSNA MIB.                     |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef RSNACLI_C
#define RSNACLI_C

#include "rsnainc.h"
#include "std802lw.h"
#include "wsscfglwg.h"
#include "wsscfgprot.h"
#ifdef WPS_WANTED
#include "wps.h"
#endif
/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : cli_process_rsna_cmd                                   */
/*                                                                           */
/* DESCRIPTION      : This function takes in variable no. of arguments       */
/*                    and process the commands for the RSNA module.          */
/*                    defined in rsnacli.h                                   */
/*                                                                           */
/* INPUT            : CliHandle -  CLIHandler                                */
/*                    u4Command -  Command Identifier                        */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : NONE                                                   */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_rsna_command (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS] = { NULL };
    UINT4               u4WlanId = 0;    /* Wlan Interface Id */
    UINT4               u4ProfileId = 0;    /* Wlan Profile Id */
    UINT4               u4GroupRekeyInterval = 0;    /* Time after which 
                                                     *  GTK shall be refreshed */
    UINT4               u4ErrCode = 0;    /* Used to set the 
                                         * error code */
    UINT4               u4NumberOfPackets = 0;    /* Packet count for 
                                                 * GTK Rekey method */
    UINT4               u4RetryCount = 0;    /* Retry count for msg1 
                                             * in 2 way handshake */
    UINT4               u4LifeTime = 0;    /* Maximum Life time of 
                                         * the PMK in the cache */
    UINT4               u4ThresholdValue = 0;    /* Percentage of Lifetime that 
                                                 * should expire before re-authentication */
    UINT4               u4TimeValue = 0;
    INT4                i4RetStatus = 0;
    UINT1               u1Argno = 0;
    UINT1               u1Status = 0;    /* Enable / Disable */
    UINT1               u1PSKFormat = 0;    /* PSK format either ascii or hex */
    UINT1               u1CipherType = 0;    /* Ciphertype either 
                                             * TKIP or CCMP */
    UINT1               u1GroupRekeyMethod = 0;    /* GroupRekeyMethod that 
                                                 * has been used */
    UINT1              *pu1PskValue = NULL;
    UINT1               u1AuthType = 0;    /* Authentication type either 
                                         * PSK or 8021x */
#ifdef PMF_WANTED
    UINT4               u4AssocComeBackTime = 0;    /* Association Come Back Time */
    UINT1               u1MFPType = 0;    /* Management Frame Protection type */
    UINT4               u4SaQueryRetryTime = 0;    /*SA Query Retry Time */
    UINT4               u4SaQueryMaxTimeOut = 0;    /*SA Query Max Timeout Value */
#endif
    va_start (ap, u4Command);

    while (u1Argno < CLI_MAX_ARGS)
    {
        args[u1Argno++] = va_arg (ap, UINT1 *);
    }

    CLI_SET_ERR (0);
    va_end (ap);

    CliRegisterLock (CliHandle, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    switch (u4Command)
    {
            /*Enables or disables the WPA2 security */
        case CLI_RSNA_SECURITY:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetSecurity (CliHandle, u1Status, u4ProfileId);
            break;

            /*Enables or disables the pre-authentication */
        case CLI_RSNA_PREAUTH_MODE:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetPreAuthMode (CliHandle,
                                                 u1Status, u4ProfileId);
            break;

            /* Sets the GroupCipher to be used */
        case CLI_RSNA_GROUPWISE_CIPHER:
            u1CipherType = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetGroupwiseCipher (CliHandle,
                                                     u1CipherType, u4ProfileId);
            break;

            /* Sets the Group Rekey Method to be used */
        case CLI_RSNA_GROUP_REKEY_METHOD:
            u1GroupRekeyMethod = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetGroupRekeyMethod (CliHandle,
                                                      u1GroupRekeyMethod,
                                                      u4ProfileId);
            break;

            /* Sets the group rekey-time interval */
        case CLI_RSNA_GROUP_REKEY_TIME:
            u4GroupRekeyInterval = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetGroupRekeyinterval (CliHandle,
                                                        u4GroupRekeyInterval,
                                                        u4ProfileId);
            break;

            /* Packet Count after which GTK handshake occurs */
        case CLI_RSNA_GROUP_REKEY_PKT:
            u4NumberOfPackets = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetGroupRekeyPackets (CliHandle,
                                                       u4NumberOfPackets,
                                                       u4ProfileId);
            break;

            /* Strict GTK Rekey handshake status */
        case CLI_RSNA_GROUP_REKEY_STRICT:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetStrictGtkMode (CliHandle,
                                                   u1Status, u4ProfileId);
            break;

            /* Set PSK value or PSK passphrase */
        case CLI_RSNA_PSK_CONFIG:
            u1PSKFormat = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            pu1PskValue = (UINT1 *) (args[RSNA_CLI_SECOND_ARG]);

            MEMCPY (&u4WlanId, args[RSNA_CLI_THIRD_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetAkmPsk (CliHandle,
                                            u1PSKFormat,
                                            pu1PskValue, u4ProfileId);

            break;

            /* Retry count for msg1 in 2Way handshake */
        case CLI_RSNA_GROUP_UPDATE_COUNT:
            u4RetryCount = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetGroupUpdateCount (CliHandle,
                                                      u4RetryCount,
                                                      u4ProfileId);
            break;
            /* Number of times MSG-1 & MSG-3 in 4-way handshake is retried */
        case CLI_RSNA_PAIRWISE_UPDATE_COUNT:
            u4RetryCount = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetPairwiseUpdateCount (CliHandle, u4RetryCount,
                                               u4ProfileId);

            break;

            /* Max life-time of PMK in the PMK cache */
        case CLI_RSNA_PMKLIFETIME:
            u4LifeTime = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetPMKLifeTime (CliHandle, u4LifeTime, u4ProfileId);
            break;

            /* Percentage of PMK life-time that should expire 
             * before re-authentication */
        case CLI_RSNA_PMKREAUTHTHRESHOLD:
            u4ThresholdValue = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }
            i4RetStatus =
                RsnaCliSetPMKReauthThreshold (CliHandle, u4ThresholdValue,
                                              u4ProfileId);

            break;

            /* Max-time to set up a security association */
        case CLI_RSNA_SATIMEOUT:
            u4TimeValue = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetSATimeOut (CliHandle, u4TimeValue, u4ProfileId);
            break;

            /* Enables or Disables the given pairwise cipher */
        case CLI_RSNA_PAIRWISE_CIPHER_STATUS:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            u1CipherType = (*(UINT1 *) (args[RSNA_CLI_SECOND_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_THIRD_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetPairwiseCipherStatus (CliHandle, u1CipherType,
                                                u1Status, u4ProfileId);
            break;

            /* Enables or Disables the given authentication suite */
        case CLI_RSNA_AUTHSUITE_MODE:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            u1AuthType = (*(UINT1 *) (args[RSNA_CLI_SECOND_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_THIRD_ARG], sizeof (UINT4));
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = RsnaCliSetAuthSuiteStatus (CliHandle, u1AuthType,    /*psk/802.1x */
                                                     u1Status, u4ProfileId);    /*enable/disable */
            break;
        case CLI_RSNA_OKC_STATUS:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetOkcStatus (CliHandle, u1Status, u4ProfileId);

            break;
#ifdef PMF_WANTED
        case CLI_RSNA_MFP_ASSOC_COMEBACK_TIME:
            u4AssocComeBackTime = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetAssocComeBackTime (CliHandle, u4AssocComeBackTime,
                                             u4ProfileId);

            break;

        case CLI_RSNA_MFPR_CONFIG:

            u1MFPType = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetMgmtFrameProtectionRequired (CliHandle, u1MFPType,
                                                       u4ProfileId);
            break;

        case CLI_RSNA_MFPC_CONFIG:

            u1MFPType = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetMgmtFrameProtectionCapable (CliHandle, u1MFPType,
                                                      u4ProfileId);
            break;

        case CLI_RSNA_MFP_SAQUERY_RETRY_TIME:

            u4SaQueryRetryTime = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetSAQueryRetryTime (CliHandle, u4SaQueryRetryTime,
                                            u4ProfileId);

            break;

        case CLI_RSNA_MFP_SAQUERY_MAX_TIME_OUT:

            u4SaQueryMaxTimeOut = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                RsnaCliSetSAQueryMaxTimeOut (CliHandle, u4SaQueryMaxTimeOut,
                                             u4ProfileId);

            break;
#endif
            /*Displays the WPA2 status */
        case CLI_RSNA_SHOW_WPA2_STATUS:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowWpa2Status (CliHandle, u4ProfileId);
            break;

            /* Displays the preauthentication status */
        case CLI_RSNA_SHOW_PREAUTH_STATUS:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }
            i4RetStatus = RsnaCliShowPreAuthStatus (CliHandle, u4ProfileId);
            break;

            /* Displays the RSNA Config details */
        case CLI_RSNA_SHOW_CONFIG_DETAILS:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }
            i4RetStatus = RsnaCliShowConfig (CliHandle, u4ProfileId);
            break;

            /* Displays the RSNA pairwise cipher details */
        case CLI_RSNA_SHOW_PAIRWISE_CIPHER:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowWpa2Cipher (CliHandle, u4ProfileId);
            break;

            /* Displays the RSNA authentication suite details */
        case CLI_RSNA_SHOW_AUTHSUITE_DETAILS:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));
            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowAuthSuiteInfo (CliHandle, u4ProfileId);
            break;
            /* Diplays the RSNA stats details */
        case CLI_RSNA_SHOW_RSNA_STATS:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowStats (CliHandle, u4ProfileId);
            break;

        case CLI_RSNA_SHOW_RSNA_HANDSHAKE_STATS:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowHandshakeStats (CliHandle, u4ProfileId);
            break;
#ifdef PMF_WANTED
        case CLI_RSNA_SHOW_MFP_CONFIG:

            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowMFPConfig (CliHandle, u4ProfileId);
            break;
#endif
            /* Clears counters in RSNA config and RSNA Stats */
        case CLI_RSNA_CLEAR_STATS:
            i4RetStatus = RsnaCliClearStats (CliHandle);
            break;

            /* Clears counters for particular WLan Index */
        case CLI_RSNA_CLEAR_COUNTERS_WLANID:
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliClearCountersDetail (CliHandle, u4ProfileId);
            break;

            /* Enable/disabe Trace */
        case CLI_RSNA_DEBUG:

            i4RetStatus = RsnaCliSetDebugs (CliHandle,
                                            CLI_PTR_TO_I4 (args[1]),
                                            CLI_ENABLE);

            break;

        case CLI_RSNA_NO_DEBUG:

            i4RetStatus = RsnaCliSetDebugs (CliHandle,
                                            CLI_PTR_TO_I4 (args[1]),
                                            CLI_DISABLE);
            break;
#ifdef RSNA_TEST_WANTED

        case CLI_RSNA_TEST_PAIRWISE_UPDATE_COUNT:
            i4RetStatus = RsnaCliTestPairwiseUpdateCount (CliHandle);
            break;

        case CLI_RSNA_TEST_GROUPWISE_UPDATE_COUNT:
            i4RetStatus = RsnaCliTestGroupwiseUpdateCount (CliHandle);
            break;

        case CLI_RSNA_SHOW_PMK_CACHE:
            i4RetStatus = RsnaCliShowPmkCacheEntry (CliHandle);
            break;
#endif
#ifdef WPA_WANTED
        case CLI_WPA_MIX_MODE:
            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }
            i4RetStatus = WpaCliSetMixMode (CliHandle, u1Status, u4ProfileId);
            break;

        case CLI_WPA_SECURITY:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }
            i4RetStatus = WpaCliSetSecurity (CliHandle, u1Status, u4ProfileId);
            break;
        case CLI_WPA_GROUPWISE_CIPHER:
            u1CipherType = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = WpaCliSetGroupwiseCipher (CliHandle,
                                                    u1CipherType, u4ProfileId);
            break;
        case CLI_WPA_GROUP_REKEY_TIME:
            u4GroupRekeyInterval = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = WpaCliSetGroupRekeyinterval (CliHandle,
                                                       u4GroupRekeyInterval,
                                                       u4ProfileId);
            break;
        case CLI_WPA_PSK_CONFIG:
            u1PSKFormat = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            pu1PskValue = (UINT1 *) (args[RSNA_CLI_SECOND_ARG]);
            if (args[RSNA_CLI_THIRD_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_THIRD_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = WpaCliSetAkmPsk (CliHandle,
                                           u1PSKFormat,
                                           pu1PskValue, u4ProfileId);
            break;
        case CLI_WPA_GROUP_UPDATE_COUNT:
            u4RetryCount = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = WpaCliSetGroupUpdateCount (CliHandle,
                                                     u4RetryCount, u4ProfileId);
            break;
        case CLI_WPA_PAIRWISE_UPDATE_COUNT:
            u4RetryCount = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                WpaCliSetPairwiseUpdateCount (CliHandle, u4RetryCount,
                                              u4ProfileId);
            break;
        case CLI_WPA_PMKLIFETIME:
            u4LifeTime = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                WpaCliSetPMKLifeTime (CliHandle, u4LifeTime, u4ProfileId);
            break;
        case CLI_WPA_SATIMEOUT:
            u4TimeValue = (*(UINT4 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus =
                WpaCliSetSATimeOut (CliHandle, u4TimeValue, u4ProfileId);
            break;
        case CLI_WPA_AUTHSUITE_MODE:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            u1AuthType = (*(UINT1 *) (args[RSNA_CLI_SECOND_ARG]));
            if (args[RSNA_CLI_THIRD_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_THIRD_ARG], sizeof (UINT4));
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = WpaCliSetAuthSuiteStatus (CliHandle, u1AuthType,    /*psk/802.1x */
                                                    u1Status, u4ProfileId);    /*enable/disable */
            break;
        case CLI_WPA_CLEAR_STATS:
            i4RetStatus = WpaCliClearStats (CliHandle);
            break;
        case CLI_WPA_CLEAR_COUNTERS_WLANID:
            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = WpaCliClearCountersDetail (CliHandle, u4ProfileId);
            break;
        case CLI_WPA_OKC_STATUS:
            u1Status = (*(UINT1 *) (args[RSNA_CLI_FIRST_ARG]));
            if (args[RSNA_CLI_SECOND_ARG] == NULL)
            {
                break;
            }

            MEMCPY (&u4WlanId, args[RSNA_CLI_SECOND_ARG], sizeof (UINT4));

            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                i4RetStatus = RSNA_FAILURE;
                break;
            }

            i4RetStatus = WpaCliSetOkcStatus (CliHandle, u1Status, u4ProfileId);
            break;
        case CLI_SHOW_MIX_MODE:
            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));
            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowMixModeStatus (CliHandle, u4ProfileId);
            break;

        case CLI_SHOW_WPA_STATUS:
            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));
            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = RsnaCliShowWpa1Status (CliHandle, u4ProfileId);
            break;
        case CLI_WPA_SHOW_CONFIG_DETAILS:
            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }
            i4RetStatus = WpaCliShowConfig (CliHandle, u4ProfileId);
            break;

        case CLI_WPA_SHOW_PAIRWISE_CIPHER:
            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = WpaCliShowWpaCipher (CliHandle, u4ProfileId);
            break;
        case CLI_WPA_SHOW_AUTHSUITE_DETAILS:

            if (args[RSNA_CLI_FIRST_ARG] == NULL)
            {
                break;
            }
            MEMCPY (&u4WlanId, args[RSNA_CLI_FIRST_ARG], sizeof (UINT4));

            if (u4WlanId != 0)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;
                }

                if (u4ProfileId == 0)
                {
                    CLI_SET_ERR (CLI_RSNA_WLAN_NOT_CREATED);
                    i4RetStatus = RSNA_FAILURE;
                    break;

                }
            }

            i4RetStatus = WpaCliShowAuthSuiteInfo (CliHandle, u4ProfileId);
            break;
#endif

        default:
            CliPrintf (CliHandle, "ERROR: Unknown Command !\r\n");
            return CLI_ERROR;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RSNA_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", RsnaCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);
    PNAC_UNLOCK ();
    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetDebugs                                        */
/*                                                                           */
/* Description     : Set trace level                                         */
/* Input(s)        : CliHandle - Handler                                     */
/*                 : i4CliTraceVal - trace level                             */
/*                 : u1TraceFlag   - Enable / Disable                        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliSetDebugs (tCliHandle CliHandle, INT4 i4CliTraceVal, INT4 u1TraceFlag)
{

    INT4                i4TraceVal = 0;
    UINT4               u4ErrorCode = 0;

    /* Get Rsna Trace Option */
    nmhGetFsRSNATraceOption (&i4TraceVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4CliTraceVal;
    }
    else
    {
        i4TraceVal &= (~(i4CliTraceVal));
    }

    if (nmhTestv2FsRSNATraceOption (&u4ErrorCode, i4TraceVal) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsRSNATraceOption (i4TraceVal) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliClearCounterDetail                               */
/*                                                                           */
/* Description     : Clears RSNA Stats and Counters in Config table          */
/* Input(s)        : CliHandle   - CLI Handler                               */
/*                 : u4ProfileId  - Profile Index                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliClearCountersDetail (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UNUSED_PARAM (CliHandle);
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRSNAClearCounter
        (&u4ErrorCode, (INT4) u4ProfileId,
         (INT4) RSNA_CLEAR_FLAG) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRSNAClearCounter ((INT4) u4ProfileId, (INT4) RSNA_CLEAR_FLAG) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliClearStats                                       */
/*                                                                           */
/* Description     : Clears RSNA Stats and Counters in Config table          */
/* Input(s)        : CliHandle   - CLI Handler                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliClearStats (tCliHandle CliHandle)
{

    UNUSED_PARAM (CliHandle);

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRSNAClearAllCounters (&u4ErrorCode, (INT4) RSNA_CLEAR_FLAG)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRSNAClearAllCounters (RSNA_CLEAR_FLAG) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetSecurity                                      */
/*                                                                           */
/* Description     : It enables and disables the wpa2 secuirty               */
/*                   security  mode                                          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1Status     - WPA2 security mode                       */
/*                   u4ProfileId  - index of wlan interface                  */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetSecurity (tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1Status*/

    if (nmhTestv2Dot11RSNAActivated (&u4ErrorCode, (INT4) u4ProfileId,
                                     (INT1) u1Status) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAEnabled object to either
     * enable or disable*/

    if (nmhSetDot11RSNAActivated ((INT4) u4ProfileId,
                                  (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetPreAuthMode                                   */
/*                                                                           */
/* Description     : It enables and disables the                             */
/*                   preauthentication mode                                  */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1Status      - PreAuthentication mode                  */
/*                   u4ProfileId   - index of wlan interface                */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetPreAuthMode (tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1Status*/

    if (nmhTestv2Dot11RSNAPreauthenticationActivated (&u4ErrorCode,
                                                      (INT4) u4ProfileId,
                                                      (INT1) u1Status) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_COMMAND_NOT_SUPPORTED);
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAPreauthenticationEnabled
     * object to either  enable or disable*/

    if (nmhSetDot11RSNAPreauthenticationActivated ((INT4) u4ProfileId,
                                                   (INT1) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetGroupwiseCipher                               */
/*                                                                           */
/* Description     : This function sets the given  groupwise cipher          */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1CipherType       - TKIP OR CCMP                       */
/*                   u4ProfileId        - index of the wlan interface        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetGroupwiseCipher (tCliHandle CliHandle, UINT1 u1CipherType,
                           UINT4 u4ProfileId)
{
    tSNMP_OCTET_STRING_TYPE tCipherType = { NULL, 0 };
    UINT4               u4ErrorCode = 0;
    UINT1               au1Buf[RSNA_MAX_CIPHER_TYPE_LENGTH] = "";
    UNUSED_PARAM (CliHandle);

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    tCipherType.pu1_OctetList = au1Buf;

    /* Verfies whether the given cipher type is TKIP or CCMP */
    if (u1CipherType == RSNA_TKIP)
    {
        MEMCPY (tCipherType.pu1_OctetList, gau1RsnaCipherSuiteTKIP,
                RSNA_MAX_CIPHER_TYPE_LENGTH);
        tCipherType.i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;
    }

    else if (u1CipherType == RSNA_CCMP)
    {
        MEMCPY (tCipherType.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                RSNA_MAX_CIPHER_TYPE_LENGTH);
        tCipherType.i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;
    }

    /* Tests whether the given index and GroupCipher type is valid or not */
    if (RsnaTestv2Dot11RSNAConfigGroupCipher (&u4ErrorCode, (INT4) u4ProfileId,
                                              &tCipherType) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* Sets the given GroupCipher type to the particular wlan index */
    if (RsnaSetDot11RSNAConfigGroupCipher ((INT4) u4ProfileId, &tCipherType)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetGroupRekeyMethod                              */
/*                                                                           */
/* Description     : This function sets the given  group rekey method        */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1GroupRekeyMethod - disabled(1),                       */
/*                                        timebased(2),                      */
/*                                        packetbased(3),                    */
/*                                        timepacketbased(4)                 */
/*                   u4ProfileId       - index of the wlan interface         */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetGroupRekeyMethod (tCliHandle CliHandle,
                            UINT1 u1GroupRekeyMethod, UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given RekeyMethod for  
     * dot11RSNAConfigGroupRekeyMethod object is either  disabled(1),
     * timebased(2), packetbased(3), timepacketbased(4)*/

    if (RsnaTestv2Dot11RSNAConfigGroupRekeyMethod (&u4ErrorCode,
                                                   (INT4) u4ProfileId,
                                                   (INT1) u1GroupRekeyMethod) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigGroupRekeyMethod object to the
     * given method*/

    if (RsnaSetDot11RSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                                (INT1) u1GroupRekeyMethod) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetGroupRekeyinterval                            */
/*                                                                           */
/* Description     : This function sets the group rekey interval             */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4GroupRekeyInterval- group rekey interval              */
/*                   u4ProfileId         - index of the wlan interface       */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetGroupRekeyinterval (tCliHandle CliHandle, UINT4 u4GroupRekeyInterval,
                              UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for  dot11RSNAConfigGroupRekeyTime 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigGroupRekeyTime (&u4ErrorCode,
                                                 (INT4) u4ProfileId,
                                                 u4GroupRekeyInterval) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigGroupRekeyTime object to specified
     * time interval*/
    if (RsnaSetDot11RSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                              u4GroupRekeyInterval) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetGroupRekeyPackets                             */
/*                                                                           */
/* Description     : This function calls set and test for setting the number of       
 *                   packets after which GTK-Rekey should initiate.          */
/*                                                                           */
/* Input(s)        : CliHandle          -  CLI Handler                       */
/*                   u4NumberOfPackets -  the number of packet               */
/*                                        after which GTK-Rekey initiates    */
/*                   u4ProfileId      - Wlan ID of the interface             */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetGroupRekeyPackets (tCliHandle CliHandle,
                             UINT4 u4NumberOfPackets, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigGroupRekeyPackets 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigGroupRekeyPackets (&u4ErrorCode,
                                                    (INT4) u4ProfileId,
                                                    u4NumberOfPackets) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /* This function sets the dot11RSNAConfigGroupRekeyPackets object to specified
     * number of packets */

    if (RsnaSetDot11RSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                                 u4NumberOfPackets) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  RsnaCliSetStrictGtkMode                                */
/*                                                                           */
/* Description     : This function test and set the status of Strict GTK     */
/*                   handshake                                               */
/*                                                                           */
/* Input(s)        : CliHandle          -  CLI Handler                       */
/*                   u1Status    - Status of Strict Group Key Handshake      */
/*                   u4ProfileId    - Wlan ID of the interface               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetStrictGtkMode (tCliHandle CliHandle, UINT1 u1Status,
                         UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigGroupRekeyStrict 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigGroupRekeyStrict (&u4ErrorCode,
                                                   (INT4) u4ProfileId,
                                                   u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigGroupRekeyStrict object to specified 
     * status */

    if (RsnaSetDot11RSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                                u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  RsnaCliSetAkmPsk                                       */
/*                                                                           */
/* Description     : This function test and sets the PSK Value or PSK        */
/*                   based on the PSK type mentioned.                        */
/*                                                                           */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u1PskFormat   - PSK passphrase or PSK Value             */
/*                   pu1Key       - Value entered as PSK                     */
/*                   u4ProfileId    - Wlan ID of the interface               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetAkmPsk (tCliHandle CliHandle, UINT1 u1PskFormat,
                  UINT1 *pu1Key, UINT4 u4ProfileId)
{
    tSNMP_OCTET_STRING_TYPE PSKPassphrase = { NULL, 0 };    /* used to pass 
                                                             * the Key */
    UINT4               u4ErrorCode = 0;
    UINT4               u4KeyLength = 0;
    UINT1               au1Buf[RSNA_MAX_PSK_LENGTH] = { 0 };
    UINT1               au1TempBuf[RSNA_CLI_THIRD_ARG] = { 0 };
    UINT1               au1Output[RSNA_PSK_LEN] = { 0 };
    UINT4               u4Index = 0;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Buf, 0, sizeof (au1Buf));

    PSKPassphrase.pu1_OctetList = au1Buf;
    MEMCPY (PSKPassphrase.pu1_OctetList, pu1Key, STRLEN (pu1Key));
    PSKPassphrase.i4_Length = (INT4) STRLEN (pu1Key);
    u4KeyLength = (UINT4) STRLEN (pu1Key);

    /* Checks if the value entered is PSKPassPhraseValue */

    if (u1PskFormat == RSNA_AKM_PSK_SETKEY_ASCII)
    {

        if ((RsnaTestv2Dot11RSNAConfigPSKPassPhrase
             (&u4ErrorCode, (INT4) u4ProfileId,
              &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if ((RsnaSetDot11RSNAConfigPSKPassPhrase
             ((INT4) u4ProfileId, &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    /* Checks if the value entered is PSK Value */
    else
    {

        /* Scan the given input and convert into the required form of
         * hexadecimal bytes. Total length will be 64. Converted length
         * will be 32*/
        for (u4Index = 0; u4Index < u4KeyLength; u4Index += 2)
        {
            /* Assemble a digit pair into the hexbyte string */
            au1TempBuf[0] = pu1Key[u4Index];
            au1TempBuf[1] = pu1Key[u4Index + 1];

            /* Convert the hex pair to an integer */
            SSCANF ((const CHR1 *) au1TempBuf, "%x",
                    (UINT4 *) &au1Output[u4Index / 2]);
        }
        MEMCPY (PSKPassphrase.pu1_OctetList, au1Output, (u4KeyLength / 2));
        PSKPassphrase.i4_Length = (INT4) (u4KeyLength / 2);

        if ((RsnaTestv2Dot11RSNAConfigPSKValue
             (&u4ErrorCode, (INT4) u4ProfileId,
              &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if ((RsnaSetDot11RSNAConfigPSKValue
             ((INT4) u4ProfileId, &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
#ifdef WPS_WANTED
    wpsSendPskUpdateFromRsna (u4ProfileId);
#endif
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetGroupUpdateCount                              */
/*                                                                           */
/* Description     : This function sets the retry-count value of the         */
/*                   group handshake for the mentioned wlan id               */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4RetryCount  - Group handshake retry count value       */
/*                   u4ProfileId      - Wlan ID of the interface             */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetGroupUpdateCount (tCliHandle CliHandle, UINT4 u4RetryCount,
                            UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigGroupUpdateCount 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigGroupUpdateCount (&u4ErrorCode,
                                                   (INT4) u4ProfileId,
                                                   u4RetryCount) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /* This function sets the  dot11RSNAConfigGroupUpdateCount object to specified 
     * retry count value */

    if (RsnaSetDot11RSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                                u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  RsnaCliSetPairwiseUpdateCount                          */
/*                                                                           */
/* Description     : This function set the retry count for message 1 & 3     */
/*                   of 4 way handshake                                      */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4RetryCount - Retransmission Count                     */
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetPairwiseUpdateCount (tCliHandle CliHandle, UINT4 u4RetryCount,
                               UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigPairwiseUpdateCount 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigPairwiseUpdateCount (&u4ErrorCode,
                                                      (INT4) u4ProfileId,
                                                      u4RetryCount) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the  dot11RSNAConfigPaiwiseUpdateCount object to specified 
     * retry count value */

    if (RsnaSetDot11RSNAConfigPairwiseUpdateCount
        ((INT4) u4ProfileId, u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  RsnaCliSetPMKLifeTime                                  */
/*                                                                           */
/* Description     : This function set the PMK Life Time of PMK in cache     */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4LifeTime   - Life Time of the PMK value to be in cache*/
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetPMKLifeTime (tCliHandle CliHandle, UINT4 u4LifeTime,
                       UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigPMKLifetime 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigPMKLifetime (&u4ErrorCode, (INT4) u4ProfileId,
                                              u4LifeTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigPMKLifetime object to specified 
     * interval  */

    if (RsnaSetDot11RSNAConfigPMKLifetime ((INT4) u4ProfileId, u4LifeTime)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetPMKReauthThreshold                            */
/*                                                                           */
/* Description     : This function sets the percentage of the PMK lifetime   */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4ThresholdValue - percentage of the PMK lifetime       */
/*                   u4ProfileId      - WLAN ID                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetPMKReauthThreshold (tCliHandle CliHandle, UINT4 u4ThresholdValue,
                              UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigPMKReauthThreshold 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigPMKReauthThreshold (&u4ErrorCode,
                                                     (INT4) u4ProfileId,
                                                     u4ThresholdValue) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /* This function sets the dot11RSNAConfigPMKReauthThreshold object to specified 
     * interval  */

    if (RsnaSetDot11RSNAConfigPMKReauthThreshold ((INT4) u4ProfileId,
                                                  u4ThresholdValue) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  RsnaCliSetSATimeOut                                    */
/*                                                                           */
/* Description     : This function Set the SA Timeout                        */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4TimeValue  - Maximum time for station association     */
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetSATimeOut (tCliHandle CliHandle, UINT4 u4TimeValue, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigSATimeout 
     * object is within range */

    if (RsnaTestv2Dot11RSNAConfigSATimeout (&u4ErrorCode, (INT4) u4ProfileId,
                                            u4TimeValue) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigSATimeout object to specified 
     * interval  */

    if (RsnaSetDot11RSNAConfigSATimeout ((INT4) u4ProfileId, u4TimeValue) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetPairwiseCipherStatus                          */
/*                                                                           */
/* Description     : This function sets the given pairwisekey cipher and     */
/*                   also enables and disables the given pairwisekey cipher  */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1CipherType       - TKIP OR CCMP                       */
/*                   u1Status           - enable/disable                     */
/*                   u4ProfileId        - index of the wlan interface        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetPairwiseCipherStatus (tCliHandle CliHandle, UINT1 u1CipherType,
                                UINT1 u1Status, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the wlan index given is available 
     * and a correct value for the dot11RSNAConfigPairwiseCipherEnabled is
     * given */

    if (RsnaTestv2Dot11RSNAConfigPairwiseCipherEnabled (&u4ErrorCode,
                                                        (INT4) u4ProfileId,
                                                        u1CipherType,
                                                        (INT1) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAConfigPairwiseCipherEnabled
     * to the given status */

    if (RsnaSetDot11RSNAConfigPairwiseCipherEnabled ((INT4) u4ProfileId,
                                                     u1CipherType,
                                                     (INT1) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetOkcStatus                                     */
/*                                                                           */
/* Description     : This function sets status of the opportunisic key       */
/*                   caching to enabled/disabled                             */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1Status           - enable/disable                     */
/*                   u4ProfileId        - index of the wlan interface        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetOkcStatus (tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the wlan index given is available
     *      * and a correct value for the dot11RSNAConfigPairwiseCipherEnabled is
     *           * given */

    if (nmhTestv2FsRSNAOKCEnabled (&u4ErrorCode, (INT4) u4ProfileId,
                                   (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAConfigPairwiseCipherEnabled
     *      * to the given status */

    if (nmhSetFsRSNAOKCEnabled ((INT4) u4ProfileId, (INT1) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetAuthSuiteStatus                               */
/*                                                                           */
/* Description     : This function enables or disables the given             */
/*                   Authentication suite (8021x or PSK)                     */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u1AuthType             -  PSK  or 8021x                 */
/*                   u1Status               - enable/disable                 */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliSetAuthSuiteStatus (tCliHandle CliHandle, UINT1 u1AuthType,
                           UINT1 u1Status, UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the wlan index given is available 
     * and a correct value for the is dot11RSNAConfigAuthenticationSuiteEnabled
     * given */

    if (RsnaTestv2Dot11RSNAConfigAuthenticationSuiteEnabled (&u4ErrorCode,
                                                             (UINT4) u1AuthType,
                                                             (INT4) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAConfigAuthenticationSuiteEnabled
     * to the given status */

    if (RsnaSetDot11RSNAConfigAuthenticationSuiteEnabled (u4ProfileId,
                                                          (UINT4) u1AuthType,
                                                          (INT1) u1Status)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef PMF_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetAssocComeBackTime                             */
/*                                                                           */
/* Description     : This function sets the Association Request Come Back    */
/*                   Time for the given WLAN                                 */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4AssocComeBackTime    -  Association Request ComeBack  */
/*                                             Time                          */
/*                   u4ProfileId            -  Index of the WLAN Interface   */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliSetAssocComeBackTime (tCliHandle CliHandle, UINT4 u4AssocComeBackTime,
                             UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the wlan index given is available
     * and a correct value for the Association Come Back Time is
     * given */

    if (nmhTestv2FsRSNAAssociationComeBackTime
        (&u4ErrorCode, (INT4) u4ProfileId, u4AssocComeBackTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the given association comeback time value to the following MIB
     * Object fsRSNAAssociationComeBackTime */

    if (nmhSetFsRSNAAssociationComeBackTime
        ((INT4) u4ProfileId, u4AssocComeBackTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetMgmtFrameProtectionRequired                   */
/*                                                                           */
/* Description     : This function enables the Management Frame Protection   */
/*                   capability for the given WLAN                           */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1MFPType              -  Management Frame Protection   */
/*                                             Type - Mandatory              */
/*                   u4ProfileId            -  Index of the WLAN Interface   */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliSetMgmtFrameProtectionRequired (tCliHandle CliHandle, UINT1 u1MFPType,
                                       UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1MFPType */

    if (nmhTestv2Dot11RSNAProtectedManagementFramesActivated (&u4ErrorCode,
                                                              (INT4)
                                                              u4ProfileId,
                                                              (INT4) u1MFPType)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    /* This function sets the status of the Dot11RSNAProtectedManagementFramesActivated object to 
     * Mandatory */

    if (nmhSetDot11RSNAProtectedManagementFramesActivated ((INT4) u4ProfileId,
                                                           (INT4) u1MFPType) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /*FIPS_cmac_aes128_test(); */

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetMgmtFrameProtectionCapable                    */
/*                                                                           */
/* Description     : This function enables the Management Frame Protection   */
/*                   capability for the given WLAN                           */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1MFPType              -  Management Frame Protection   */
/*                                             Type - Optional               */
/*                   u4ProfileId            -  Index of the WLAN Interface   */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliSetMgmtFrameProtectionCapable (tCliHandle CliHandle, UINT1 u1MFPType,
                                      UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1MFPType */

    if (nmhTestv2Dot11RSNAUnprotectedManagementFramesAllowed (&u4ErrorCode,
                                                              (INT4)
                                                              u4ProfileId,
                                                              (INT4) u1MFPType)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    /* This function sets the status of the Dot11RSNAProtectedManagementFramesActivated object to 
     * Mandatory */

    if (nmhSetDot11RSNAUnprotectedManagementFramesAllowed ((INT4) u4ProfileId,
                                                           (INT4) u1MFPType) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /*FIPS_cmac_aes128_test(); */

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetSAQueryRetryTime                              */
/*                                                                           */
/* Description     : This function sets the SA Query Retry Timeout Value     */
/*                                                                           */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4SaQueryRetryTime     -  SA Query Retry Time Value     */
/*                   u4ProfileId            -  Index of the WLAN Interface   */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliSetSAQueryRetryTime (tCliHandle CliHandle, UINT4 u4SaQueryRetryTime,
                            UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the SA Query Retry time value is within range */

    if (nmhTestv2Dot11AssociationSAQueryRetryTimeout (&u4ErrorCode,
                                                      (INT4) u4ProfileId,
                                                      u4SaQueryRetryTime) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    /* This function sets the status of the Dot11RSNAProtectedManagementFramesActivated object to 
     * Mandatory */

    if (nmhSetDot11AssociationSAQueryRetryTimeout ((INT4) u4ProfileId,
                                                   u4SaQueryRetryTime) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliSetSAQueryMaxTimeOut                             */
/*                                                                           */
/* Description     : This function sets the SA Query Max  Timeout Value      */
/*                                                                           */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u4SaQueryRetryTime     -  SA Query Retry Time Value     */
/*                   u4ProfileId            -  Index of the WLAN Interface   */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliSetSAQueryMaxTimeOut (tCliHandle CliHandle, UINT4 u4SaQueryMaxTimeOut,
                             UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the SA Query Retry time value is within range */

    if (nmhTestv2Dot11AssociationSAQueryMaximumTimeout (&u4ErrorCode,
                                                        (INT4) u4ProfileId,
                                                        u4SaQueryMaxTimeOut) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    /* This function sets the status of the Dot11RSNAProtectedManagementFramesActivated object to 
     * Mandatory */

    if (nmhSetDot11AssociationSAQueryMaximumTimeout ((INT4) u4ProfileId,
                                                     u4SaQueryMaxTimeOut) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}
#endif
/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowWpa2Status                                    */
/*                                                                           */
/* Description     : This function displays the WPA2  status                 */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliShowWpa2Status (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4Status = 0;
    UINT4               u4CurrWpa2Index = u4ProfileId;
    UINT4               u4NextWpa2Index = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;

    do
    {
        /*Verify whether to display all WPA2  details or 
         * to display for  particular WLAN. */
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
            u4NextWpa2Index = u4CurrWpa2Index;

        }
        else if (u4CurrWpa2Index == 0)
        {
            if (nmhGetFirstIndexDot11PrivacyTable ((INT4 *) &u4CurrWpa2Index)
                != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            u4NextWpa2Index = u4CurrWpa2Index;
        }
        else
        {
            /* Fetches the next index from the 
             * table by passing the current index */
            if (nmhGetNextIndexDot11PrivacyTable
                ((INT4) u4CurrWpa2Index,
                 (INT4 *) &u4NextWpa2Index) != SNMP_SUCCESS)
            {
                break;

            }
        }
        WssGetCapwapWlanId (u4NextWpa2Index, &u4WlanId);
        /*Fetches entries from the table and displays it */
        nmhGetDot11RSNAActivated ((INT4) u4NextWpa2Index, (INT4 *) &u4Status);
        CliPrintf (CliHandle, " \t\t WlanIndex       : %d \n", u4WlanId);
        if (u4Status == RSNA_ENABLED)
        {
            CliPrintf (CliHandle, " \t\t RSNAStatus      : Enabled\n");
        }
        else if (u4Status == RSNA_DISABLED)
        {
            CliPrintf (CliHandle, " \t\t RSNAStatus      : Disabled\n");
        }
        u4CurrWpa2Index = u4NextWpa2Index;
    }
    while (u4TableWalk != 0);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  RsnaCliShowPreAuthStatus                               */
/*                                                                           */
/* Description     : This function displays the Pre-authentication  status   */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliShowPreAuthStatus (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4Status = 0;
    UINT4               u4CurrPreAuthIndex = u4ProfileId;
    UINT4               u4NextPreAuthIndex = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;
    do
    {
        /* Verify whether to display all PreAuthentication details 
         * or to display for  particular WLAN. */
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
            u4NextPreAuthIndex = u4CurrPreAuthIndex;

        }
        else if (u4CurrPreAuthIndex == 0)
        {
            if (nmhGetFirstIndexDot11PrivacyTable ((INT4 *) &u4CurrPreAuthIndex)
                != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            u4NextPreAuthIndex = u4CurrPreAuthIndex;
        }
        else
        {
            /* Fetches the next index from the 
             * table by passing the current index */
            if (nmhGetNextIndexDot11PrivacyTable
                ((INT4) u4CurrPreAuthIndex,
                 (INT4 *) &u4NextPreAuthIndex) != SNMP_SUCCESS)
            {
                break;

            }
        }
        WssGetCapwapWlanId (u4NextPreAuthIndex, &u4WlanId);
        /*Fetches entries from the table and displays it */
        nmhGetDot11RSNAPreauthenticationActivated ((INT4) u4NextPreAuthIndex,
                                                   (INT4 *) &u4Status);
        CliPrintf (CliHandle, " \t\t WlanIndex                 : %d \n",
                   u4WlanId);
        if (u4Status == RSNA_ENABLED)
        {
            CliPrintf (CliHandle,
                       " \t\t Pre-AuthenticationStatus  : Enabled\n");
        }
        else if (u4Status == RSNA_DISABLED)
        {
            CliPrintf (CliHandle,
                       " \t\t Pre-AuthenticationStatus  : Disabled\n");
        }

        u4CurrPreAuthIndex = u4NextPreAuthIndex;
    }
    while (u4TableWalk != 0);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowConfig                                       */
/*                                                                           */
/* Description     : This function displays the RSNA Config  details         */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4ProfileId  - index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliShowConfig (tCliHandle CliHandle, UINT4 u4ProfileId)
{

    UINT4               u4CurrConfigIndex = u4ProfileId;
    UINT4               u4NextConfigIndex = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;

    /*Validating the WlanIndex that has 
     * been passed from the command line */
    if (RsnaValidateIndexInstanceDot11RSNAConfigTable ((INT4) u4ProfileId) ==
        SNMP_SUCCESS)
    {
        do
        {
            /*Verify whether to display all WPA2 Config 
             *details or to display particular WLAN. */
            if (u4ProfileId != 0)
            {
                u4TableWalk = 0;
                u4NextConfigIndex = u4CurrConfigIndex;

            }
            else
            {
                /* Fetches the next index from the
                 * table by passing the current index */
                if (RsnaGetNextIndexDot11RSNAConfigTable
                    ((INT4) u4CurrConfigIndex,
                     (INT4 *) &u4NextConfigIndex) != SNMP_SUCCESS)
                {
                    break;

                }
            }

            WssGetCapwapWlanId (u4NextConfigIndex, &u4WlanId);
            /*Fetches entries from 
             *the table and displays it */
            RsnaCliShowConfigDisplay (CliHandle, u4NextConfigIndex, u4WlanId);
            u4CurrConfigIndex = u4NextConfigIndex;
        }
        while (u4TableWalk != 0);
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowConfigDisplay                                */
/*                                                                           */
/* Description     : This function fetches the entries of the config table   */
/*                   and displays it.                                        */
/*                                                                           */
/*  Input(s)        : CliHandle   - CLI Handler                              */
/*                   u4NextConfigIndex - Index of the wlan-interface         */
/*                                                                           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/

VOID
RsnaCliShowConfigDisplay (tCliHandle CliHandle, UINT4 u4NextConfigIndex,
                          UINT4 u4WlanId)
{
    UNUSED_PARAM (u4WlanId);
    tSNMP_OCTET_STRING_TYPE GroupCipher = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE AuthSuiteSelected = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PairwiseCipherSelected = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherSelected = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PmkIdUsed = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE AuthSuiteRequested = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PairwiseCipherRequested = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherRequested = { NULL, 0 };
    UINT1               au1GroupCipher[RSNA_MAX_CIPHER_TYPE_LENGTH] = "";
    UINT1               au1AuthSuiteSelected[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH] =
        "";
    UINT1               au1PairwiseCipherSelected[RSNA_MAX_CIPHER_TYPE_LENGTH] =
        "";
    UINT1               au1GroupCipherSelected[RSNA_MAX_CIPHER_TYPE_LENGTH] =
        "";
    UINT1               au1PMKIDUsed[RSNA_MAX_PMKID_LENGTH] = "";
    UINT1               au1AuthSuiteRequested[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH] =
        "";
    UINT1               au1PairwiseCipherRequested[RSNA_MAX_CIPHER_TYPE_LENGTH]
        = "";
    UINT1               au1GroupwiseCipherRequested[RSNA_MAX_CIPHER_TYPE_LENGTH]
        = "";
    UINT4               u4ConfigVersion = 0;
    UINT4               u4PairwiseKeySupported = 0;
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupRekeyPkts = 0;
    UINT4               u4GroupRekeyStrict = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4GroupCipherSize = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4PTKSAReplayCounter = 0;
    UINT4               u4SATimeOut = 0;
    UINT4               u4TKIPCounterMeasuresInvoked = 0;
    UINT4               u4FourWayHandshakeFailures = 0;
    UINT4               u4GTKSAReplayCountersCount = 0;
    UINT4               u4RsnaOkcStatus = 0;

    MEMSET (au1GroupCipher, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1AuthSuiteSelected, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1PairwiseCipherSelected, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1GroupCipherSelected, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1PMKIDUsed, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1AuthSuiteRequested, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1PairwiseCipherRequested, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1GroupwiseCipherRequested, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));

    GroupCipher.pu1_OctetList = au1GroupCipher;
    AuthSuiteSelected.pu1_OctetList = au1AuthSuiteSelected;
    PairwiseCipherSelected.pu1_OctetList = au1PairwiseCipherSelected;
    GroupwiseCipherSelected.pu1_OctetList = au1GroupCipherSelected;
    PmkIdUsed.pu1_OctetList = au1PMKIDUsed;
    AuthSuiteRequested.pu1_OctetList = au1AuthSuiteRequested;
    PairwiseCipherRequested.pu1_OctetList = au1PairwiseCipherRequested;
    GroupwiseCipherRequested.pu1_OctetList = au1GroupwiseCipherRequested;

    /* Fetching the entries from dot11RSNAConfigTable */

    RsnaGetDot11RSNAConfigVersion ((INT4) u4NextConfigIndex,
                                   (INT4 *) &u4ConfigVersion);

    RsnaGetDot11RSNAConfigPairwiseKeysSupported ((INT4) u4NextConfigIndex,
                                                 &u4PairwiseKeySupported);

    RsnaGetDot11RSNAConfigGroupCipher ((INT4) u4NextConfigIndex, &GroupCipher);

    RsnaGetDot11RSNAConfigGroupRekeyMethod ((INT4) u4NextConfigIndex,
                                            (INT4 *) &u4GroupRekeyMethod);

    RsnaGetDot11RSNAConfigGroupRekeyTime ((INT4) u4NextConfigIndex,
                                          &u4GroupRekeyTime);

    RsnaGetDot11RSNAConfigGroupRekeyPackets ((INT4) u4NextConfigIndex,
                                             &u4GroupRekeyPkts);

    RsnaGetDot11RSNAConfigGroupRekeyStrict ((INT4) u4NextConfigIndex,
                                            (INT4 *) &u4GroupRekeyStrict);

    RsnaGetDot11RSNAConfigGroupUpdateCount ((INT4) u4NextConfigIndex,
                                            &u4GroupUpdateCount);

    RsnaGetDot11RSNAConfigPairwiseUpdateCount ((INT4) u4NextConfigIndex,
                                               &u4PairwiseUpdateCount);

    RsnaGetDot11RSNAConfigGroupCipherSize ((INT4) u4NextConfigIndex,
                                           &u4GroupCipherSize);

    RsnaGetDot11RSNAConfigPMKLifetime ((INT4) u4NextConfigIndex,
                                       &u4PMKLifetime);

    RsnaGetDot11RSNAConfigPMKReauthThreshold ((INT4) u4NextConfigIndex,
                                              &u4PMKReAuthThreshold);

    RsnaGetDot11RSNAConfigNumberOfPTKSAReplayCounters ((INT4) u4NextConfigIndex,
                                                       (INT4 *)
                                                       &u4PTKSAReplayCounter);

    RsnaGetDot11RSNAConfigSATimeout ((INT4) u4NextConfigIndex, &u4SATimeOut);

    RsnaGetDot11RSNAAuthenticationSuiteSelected ((INT4) u4NextConfigIndex,
                                                 &AuthSuiteSelected);

    RsnaGetDot11RSNAPairwiseCipherSelected ((INT4) u4NextConfigIndex,
                                            &PairwiseCipherSelected);

    RsnaGetDot11RSNAGroupCipherSelected ((INT4) u4NextConfigIndex,
                                         &GroupwiseCipherSelected);

    RsnaGetDot11RSNAPMKIDUsed ((INT4) u4NextConfigIndex, &PmkIdUsed);

    RsnaGetDot11RSNAAuthenticationSuiteRequested ((INT4)
                                                  u4NextConfigIndex,
                                                  &AuthSuiteRequested);

    RsnaGetDot11RSNAPairwiseCipherRequested ((INT4) u4NextConfigIndex,
                                             &PairwiseCipherRequested);

    RsnaGetDot11RSNAGroupCipherRequested ((INT4) u4NextConfigIndex,
                                          &GroupwiseCipherRequested);

    RsnaGetDot11RSNATKIPCounterMeasuresInvoked ((INT4) u4NextConfigIndex,
                                                &u4TKIPCounterMeasuresInvoked);

    RsnaGetDot11RSNA4WayHandshakeFailures ((INT4) u4NextConfigIndex,
                                           &u4FourWayHandshakeFailures);

    RsnaGetDot11RSNAConfigNumberOfGTKSAReplayCounters ((INT4) u4NextConfigIndex,
                                                       (INT4 *)
                                                       &u4GTKSAReplayCountersCount);

    nmhGetFsRSNAOKCEnabled ((INT4) u4NextConfigIndex,
                            (INT4 *) &u4RsnaOkcStatus);

    /* Printing the dot11RSNAConfigTable entries */

    CliPrintf (CliHandle, " \t\t IfIndex                           : %d\n",
               u4NextConfigIndex);
    CliPrintf (CliHandle, " \t\t RSNAVersion                       : %d\n",
               u4ConfigVersion);
    CliPrintf (CliHandle, " \t\t PairwiseKeySupported              : "
               "%d\n", u4PairwiseKeySupported);

    CliPrintf (CliHandle, " \t\t GroupCipher                       : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               GroupCipher.pu1_OctetList[0],
               GroupCipher.pu1_OctetList[1],
               GroupCipher.pu1_OctetList[2], GroupCipher.pu1_OctetList[3]);

    if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_DISABLED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : " "Disabled\n");
    }
    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEBASED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : " "TimeBased\n");
    }
    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_PACKETBASED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : "
                   "PacketBased\n");
    }
    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEPACKETBASED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : "
                   "TimePacketBased\n");
    }
    CliPrintf (CliHandle, " \t\t GroupRekeyTime                    : %u\n",
               u4GroupRekeyTime);

    CliPrintf (CliHandle, " \t\t GroupRekeyPackets                 : %d\n",
               u4GroupRekeyPkts);

    if (u4GroupRekeyStrict == RSNA_ENABLED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyStrict                  : Enable\n");
    }
    else if (u4GroupRekeyStrict == RSNA_DISABLED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyStrict                  : Disable\n");

    }

    CliPrintf (CliHandle, " \t\t GroupUpdateCount                  : %u\n",
               u4GroupUpdateCount);

    CliPrintf (CliHandle, " \t\t Pairwiseupdatecount               : %u\n",
               u4PairwiseUpdateCount);

    CliPrintf (CliHandle, " \t\t GroupCipherSize                   : %d\n",
               u4GroupCipherSize);

    CliPrintf (CliHandle, " \t\t PMKLifeTime                       : %u\n",
               u4PMKLifetime);

    CliPrintf (CliHandle, " \t\t PMKReauthThreshold                : %d\n",
               u4PMKReAuthThreshold);

    CliPrintf (CliHandle, " \t\t PTKSAReplayCounter                : %d\n",
               u4PTKSAReplayCounter);

    CliPrintf (CliHandle, " \t\t SATimeOut                         : %u\n",
               u4SATimeOut);

    CliPrintf (CliHandle, " \t\t AuthenticateSuiteSelected         : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               AuthSuiteSelected.pu1_OctetList[0],
               AuthSuiteSelected.pu1_OctetList[1],
               AuthSuiteSelected.pu1_OctetList[2],
               AuthSuiteSelected.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t PairwiseCipherSelected            : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               PairwiseCipherSelected.pu1_OctetList[0],
               PairwiseCipherSelected.pu1_OctetList[1],
               PairwiseCipherSelected.pu1_OctetList[2],
               PairwiseCipherSelected.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t GroupwiseCipherSelected           : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               GroupwiseCipherSelected.pu1_OctetList[0],
               GroupwiseCipherSelected.pu1_OctetList[1],
               GroupwiseCipherSelected.pu1_OctetList[2],
               GroupwiseCipherSelected.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t PMKIdUsed                         : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               PmkIdUsed.pu1_OctetList[0],
               PmkIdUsed.pu1_OctetList[1],
               PmkIdUsed.pu1_OctetList[2], PmkIdUsed.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t AuthenticationSuiteRequested      : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               AuthSuiteRequested.pu1_OctetList[0],
               AuthSuiteRequested.pu1_OctetList[1],
               AuthSuiteRequested.pu1_OctetList[2],
               AuthSuiteRequested.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t PairwiseCipherRequested           : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               PairwiseCipherRequested.pu1_OctetList[0],
               PairwiseCipherRequested.pu1_OctetList[1],
               PairwiseCipherRequested.pu1_OctetList[2],
               PairwiseCipherRequested.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t GroupwiseCipherRequested          : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               GroupwiseCipherRequested.pu1_OctetList[0],
               GroupwiseCipherRequested.pu1_OctetList[1],
               GroupwiseCipherRequested.pu1_OctetList[2],
               GroupwiseCipherRequested.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t TKIPCounterMeasuresInvoked        : %d\n",
               u4TKIPCounterMeasuresInvoked);

    CliPrintf (CliHandle, " \t\t 4WayHandshakeFailures             : %d\n",
               u4FourWayHandshakeFailures);

    CliPrintf (CliHandle, " \t\t GTKSAReplayCountersCount          : %d\n",
               u4GTKSAReplayCountersCount);

    if (u4RsnaOkcStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle,
                   " \t\t OKC Status                        : Enabled\n");
    }
    else if (u4RsnaOkcStatus == RSNA_DISABLED)
    {
        CliPrintf (CliHandle,
                   " \t\t OKC Status                        : Disabled\n");
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :RsnaCliShowAuthSuiteInfo                                 */
/*                                                                           */
/* Description     : This function displays the authentication suite details */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliShowAuthSuiteInfo (tCliHandle CliHandle, UINT4 u4ProfileId)
{

    UINT4               u4CurrAuthSuiteIndex = 0;
    UINT4               u4NextAuthSuiteIndex = 0;
    UINT4               u4CurrWlanId = 0;
    UINT4               u4NextWlanId = 0;
    UINT4               u4WlanId = 0;

    u4CurrWlanId = u4ProfileId;
    /* Displays all the entries of the 
     * dot11RSNAConfigAuthenticationSuitesTable */
    while (RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable
           ((INT4) u4CurrWlanId, (INT4 *) &u4NextWlanId,
            u4CurrAuthSuiteIndex, &u4NextAuthSuiteIndex) == SNMP_SUCCESS)
    {
        u4CurrAuthSuiteIndex = u4NextAuthSuiteIndex;
        u4CurrWlanId = u4NextWlanId;
        if (u4ProfileId != 0)
        {
            /* Verify if the entry fetched is for the input given
             * in this function call */
            if (u4CurrWlanId == u4ProfileId)
            {
                WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
                /* Print the entry from RSNA Stats
                 * Table for the u4CurrentWlanIndex */
                RsnaCliShowAuthSuiteInfoDisplay (CliHandle, u4CurrWlanId,
                                                 u4CurrAuthSuiteIndex,
                                                 u4WlanId);
            }
            else
            {
                /* Breaking the loop after printing the appropriate values
                 * for specific WlanIndex, StatsIndex */
                break;
            }
        }
        else
        {
            WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
            /* Print the entry from RSNA Stats
             * Table for the u4CurrentWlanIndex */
            RsnaCliShowAuthSuiteInfoDisplay (CliHandle, u4CurrWlanId,
                                             u4CurrAuthSuiteIndex, u4WlanId);
        }
    }

    /* SUNIL- Executes coverity report for unit test details */
    /*RsnaCliShowUnitTestDetailsDisplay(CliHandle); */

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowWpa2Cipher                                   */
/*                                                                           */
/* Description     : This function displays the details of pairwise          */
/*                   ciphersuite configured in this entity.                  */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliShowWpa2Cipher (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4CurrWlanId = 0;
    UINT4               u4NextWlanId = 0;
    UINT4               u4CurrConfigPairwiseCipherIndex = 0;
    UINT4               u4NextRSNAConfigPairwiseCipherIndex = 0;
    UINT4               u4WlanId = 0;
    /* Verify whether to display all WLAN details 
     * or to display particular WLAN detail. */

    u4CurrWlanId = u4ProfileId;

    while (RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable
           ((INT4) u4CurrWlanId, (INT4 *) &u4NextWlanId,
            u4CurrConfigPairwiseCipherIndex,
            &u4NextRSNAConfigPairwiseCipherIndex) == SNMP_SUCCESS)

    {
        u4CurrConfigPairwiseCipherIndex = u4NextRSNAConfigPairwiseCipherIndex;
        u4CurrWlanId = u4NextWlanId;

        /* Assign the next wlanindex and statsindex pointer to current to
         * get the appropriate value from GetnextIndex */

        if (u4ProfileId != 0)
        {
            /* Verify if the entry fetched is for the input given 
             * in this function call */
            if (u4CurrWlanId == u4ProfileId)
            {
                WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
                /* Print the entry from RSNA Stats 
                 * Table for the u4CurrentWlanIndex */
                RsnaCliShowWpa2CipherDisplay (CliHandle, u4CurrWlanId,
                                              u4CurrConfigPairwiseCipherIndex,
                                              u4WlanId);
            }
            else
            {
                /* Breaking the loop after printing the appropriate values 
                 * for specific WlanIndex, StatsIndex */
                break;
            }
        }
        else
        {
            WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
            /* Print the entry from RSNA Stats 
             * Table for the u4CurrentWlanIndex */
            RsnaCliShowWpa2CipherDisplay (CliHandle, u4CurrWlanId,
                                          u4CurrConfigPairwiseCipherIndex,
                                          u4WlanId);
        }

    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaCliShowWpa2CipherDisplay                           */
/*                                                                           */
/*  Description     : This function fetches the entries from PairwiseCiphers */
/*                    table and displays it.                                 */
/*                                                                           */
/*  Input(s)        : CliHandle   - CLI Handler                              */
/*                    u4ProfileId - Wlan Interface Index                     */
/*                    u4NextRSNAConfigPairwiseCipherIndex -  PairwiseCipher  */
/*                                                            Index          */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

VOID
RsnaCliShowWpa2CipherDisplay (tCliHandle CliHandle, UINT4 u4NextWlanId,
                              UINT4 u4NextRSNAConfigPairwiseCipherIndex,
                              UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE RSNAConfigPairwiseCipher = { NULL, 0 };
    UINT1               au1RSNAConfigCipher[4] = "";
    UINT4               u4PairwiseCipherEnabled = 0;
    UINT4               u4PairwiseCipherSize = 0;
    UINT4               u4RSNAConfigPairwiseCipherIndex = 0;
    UNUSED_PARAM (u4RSNAConfigPairwiseCipherIndex);

    MEMSET (au1RSNAConfigCipher, 0, sizeof (au1RSNAConfigCipher));
    RSNAConfigPairwiseCipher.pu1_OctetList = au1RSNAConfigCipher;
    RSNAConfigPairwiseCipher.i4_Length = (INT4) (STRLEN (au1RSNAConfigCipher));

    /* Get the RSNA Pairwise Cipher Table Entries using RsnaGet */

    RsnaGetDot11RSNAConfigPairwiseCipher ((INT4) u4NextWlanId,
                                          u4NextRSNAConfigPairwiseCipherIndex,
                                          &RSNAConfigPairwiseCipher);
    RsnaGetDot11RSNAConfigPairwiseCipherEnabled ((INT4) u4NextWlanId,
                                                 u4NextRSNAConfigPairwiseCipherIndex,
                                                 (INT4 *)
                                                 &u4PairwiseCipherEnabled);
    RsnaGetDot11RSNAConfigPairwiseCipherSize ((INT4) u4NextWlanId,
                                              u4NextRSNAConfigPairwiseCipherIndex,
                                              &u4PairwiseCipherSize);

    CliPrintf (CliHandle, "\rWlan Index              : %d\n", u4WlanId);
    CliPrintf (CliHandle, "\rCipher Index            : %d\n",
               u4NextRSNAConfigPairwiseCipherIndex);
    CliPrintf (CliHandle, "\rCipher Used             : %x:%x:%x:%x\n",
               RSNAConfigPairwiseCipher.pu1_OctetList[0],
               RSNAConfigPairwiseCipher.pu1_OctetList[1],
               RSNAConfigPairwiseCipher.pu1_OctetList[2],
               RSNAConfigPairwiseCipher.pu1_OctetList[3]);
    if (u4PairwiseCipherEnabled == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "\rPairwise Cipher status  : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rPairwise Cipher status  : Disabled\n");
    }
    CliPrintf (CliHandle, "\rPairwise Cipher size    : %d\n",
               u4PairwiseCipherSize);

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaCliShowAuthSuiteInfoDisplay                        */
/*                                                                           */
/*  Description     : This function fetches the entries from Authentication  */
/*               suite  table and displaty it                            */
/*                                                                           */
/*  Input(s)        : CliHandle   - CLI Handler                              */
/*                    u4ProfileId - Wlan Interface Index                     */
/*                    u4NextAuthSuiteIndex -  Authentication suit index      */
/*                    u4WlanId                                               */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

VOID
RsnaCliShowAuthSuiteInfoDisplay (tCliHandle CliHandle, UINT4 u4NextWlanId,
                                 UINT4 u4NextAuthSuiteIndex, UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE AuthenticationSuite = { NULL, 0 };
    UINT4               u4AuthSuiteStatus = 0;
    UINT1               au1AuthSuitetype[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH] = "";

    MEMSET (au1AuthSuitetype, 0, sizeof (au1AuthSuitetype));
    AuthenticationSuite.pu1_OctetList = au1AuthSuitetype;
    AuthenticationSuite.i4_Length = (INT4) (STRLEN (au1AuthSuitetype));

    /* Get the RSNA Authentication Suite Table Entries using RsnaGet */
    RsnaGetDot11RSNAConfigAuthenticationSuite ((UINT4) u4NextWlanId,
                                               u4NextAuthSuiteIndex,
                                               &AuthenticationSuite);
    RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled ((INT4) u4NextWlanId,
                                                      u4NextAuthSuiteIndex,
                                                      (INT4 *)
                                                      &u4AuthSuiteStatus);

    CliPrintf (CliHandle, "\rWlan Index              : %d\n", u4WlanId);
    if (u4NextAuthSuiteIndex == RSNA_AUTHSUITE_PSK)
    {
        CliPrintf (CliHandle, "AuthenticationSuiteType " "  :  PSK\n");
    }
    else if (u4NextAuthSuiteIndex == RSNA_AUTHSUITE_8021X)
    {
        CliPrintf (CliHandle, "AuthenticationSuiteType" "   :  8021X\n");
    }
    CliPrintf (CliHandle,
               "AuthenticationSuite       :  %x:%x:%x:%x\n ",
               AuthenticationSuite.pu1_OctetList[0],
               AuthenticationSuite.pu1_OctetList[1],
               AuthenticationSuite.pu1_OctetList[2],
               AuthenticationSuite.pu1_OctetList[3]);
    if (u4AuthSuiteStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "AuthenticationSuiteStatus :" "  Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "AuthenticationSuiteStatus :" "  Disabled\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowStats                                          */
/*                                                                           */
/*  Description     : This function displays RSNA statistics                 */
/*                                                                           */
/*  Input(s)        : CliHandle   - CLI Handler                              */
/*                    u4ProfileId - Wlan Interface Index                     */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliShowStats (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    INT4                i4CurrentWlanIndex = (INT4) u4ProfileId;
    INT4                i4NextWlanIndex = 0;
    UINT4               u4CurrentRSNAStatsIndex = 0;
    UINT4               u4NextRSNAStatsIndex = 0;
    UINT4               u4WlanId = 0;

    WssGetCapwapWlanId (i4CurrentWlanIndex, &u4WlanId);

    while ((RsnaGetNextIndexDot11RSNAStatsTable (i4CurrentWlanIndex,
                                                 &i4NextWlanIndex,
                                                 u4CurrentRSNAStatsIndex,
                                                 &u4NextRSNAStatsIndex)) ==
           SNMP_SUCCESS)
    {
        if (i4CurrentWlanIndex == i4NextWlanIndex)
        {
            RsnaCliShowStatsDisplay (CliHandle, i4NextWlanIndex,
                                     u4NextRSNAStatsIndex, u4WlanId);
            i4CurrentWlanIndex = i4NextWlanIndex;
            u4CurrentRSNAStatsIndex = u4NextRSNAStatsIndex;
        }
        else
        {
            break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowStatsDisplay                                 */
/*                                                                           */
/* Description     : This function displays the values from the              */
/*                   RSNA Statistics Table based using WLANIndex, StatsIndex */
/*                                                                           */
/* Input(s)        : u4ProfileIdValue      - Wlan Interface Index            */
/*                   u4RSNAStatsIndexValue - RSNA Statistics Index           */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
RsnaCliShowStatsDisplay (tCliHandle CliHandle, UINT4 u4ProfileIdValue,
                         UINT4 u4RSNAStatsIndexValue, UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE RSNAStatsSelectedPairwiseCipher = { NULL, 0 };
    tMacAddr            RSNAStatsSTAAddress = "";
    UINT1               au1RSNAStatsPairwiseCipher[RSNA_CIPHER_SUITE_LEN] = "";
    UINT1               au1MacAddr[RSNA_CLI_MAC_ADDR_LEN] = "";
    UINT4               u4RSNAStatsVersion = 0;
    UINT4               u4RSNAStatsTKIPICVErrors = 0;
    UINT4               u4RSNAStatsTKIPLocalMICFailures = 0;
    UINT4               u4RSNAStatsTKIPRemoteMICFailures = 0;
    UINT4               u4RSNAStatsCCMPReplays = 0;
    UINT4               u4RSNAStatsCCMPDecryptErrors = 0;
    UINT4               u4RSNAStatsTKIPReplays = 0;

    MEMSET (au1RSNAStatsPairwiseCipher, '\0', RSNA_CIPHER_SUITE_LEN);
    MEMSET (au1MacAddr, '\0', RSNA_CLI_MAC_ADDR_LEN);

    RSNAStatsSelectedPairwiseCipher.pu1_OctetList = au1RSNAStatsPairwiseCipher;
    RSNAStatsSelectedPairwiseCipher.i4_Length =
        (INT4) (STRLEN (au1RSNAStatsPairwiseCipher));

    /* Get the RSNA Statistics Table Entries using RsnaGet */

    RsnaGetDot11RSNAStatsSTAAddress ((INT4) u4ProfileIdValue,
                                     u4RSNAStatsIndexValue,
                                     &RSNAStatsSTAAddress);
    CliMacToStr (RSNAStatsSTAAddress, au1MacAddr);

    RsnaGetDot11RSNAStatsVersion ((INT4) u4ProfileIdValue,
                                  u4RSNAStatsIndexValue, &u4RSNAStatsVersion);

    RsnaGetDot11RSNAStatsSelectedPairwiseCipher ((INT4) u4ProfileIdValue,
                                                 u4RSNAStatsIndexValue,
                                                 &RSNAStatsSelectedPairwiseCipher);

    RsnaGetDot11RSNAStatsTKIPICVErrors ((INT4) u4ProfileIdValue,
                                        u4RSNAStatsIndexValue,
                                        &u4RSNAStatsTKIPICVErrors);

    RsnaGetDot11RSNAStatsTKIPLocalMICFailures ((INT4) u4ProfileIdValue,
                                               u4RSNAStatsIndexValue,
                                               &u4RSNAStatsTKIPLocalMICFailures);

    RsnaGetDot11RSNAStatsTKIPRemoteMICFailures ((INT4) u4ProfileIdValue,
                                                u4RSNAStatsIndexValue,
                                                &u4RSNAStatsTKIPRemoteMICFailures);

    RsnaGetDot11RSNAStatsCCMPReplays ((INT4) u4ProfileIdValue,
                                      u4RSNAStatsIndexValue,
                                      &u4RSNAStatsCCMPReplays);

    RsnaGetDot11RSNAStatsCCMPDecryptErrors ((INT4) u4ProfileIdValue,
                                            u4RSNAStatsIndexValue,
                                            &u4RSNAStatsCCMPDecryptErrors);

    RsnaGetDot11RSNAStatsTKIPReplays ((INT4) u4ProfileIdValue,
                                      u4RSNAStatsIndexValue,
                                      &u4RSNAStatsTKIPReplays);

    /* Display the retrieved RSNA Statistics Table Entries using cliPrintf */
    CliPrintf (CliHandle, "\r\r Wlan Index            : %d \r\n", u4WlanId);
    CliPrintf (CliHandle, "\r\r Stats Index           : %d \r\n",
               u4RSNAStatsIndexValue);
    CliPrintf (CliHandle, "\r\r Station MAC address   : %s \r\n", au1MacAddr);
    CliPrintf (CliHandle, "\r\r RSNA version          : %d \r\n",
               u4RSNAStatsVersion);
    CliPrintf (CliHandle, "\r\r Pairwise Cipher Used  : %x:%x:%x:%x \r\n",
               RSNAStatsSelectedPairwiseCipher.pu1_OctetList[0],
               RSNAStatsSelectedPairwiseCipher.pu1_OctetList[1],
               RSNAStatsSelectedPairwiseCipher.pu1_OctetList[2],
               RSNAStatsSelectedPairwiseCipher.pu1_OctetList[3]);
    CliPrintf (CliHandle, "\r\r TKIPICVErrors         : %d \r\n",
               u4RSNAStatsTKIPICVErrors);
    CliPrintf (CliHandle, "\r\r TKIPLocalMICFailures  : %d \r\n",
               u4RSNAStatsTKIPLocalMICFailures);
    CliPrintf (CliHandle, "\r\r TKIPRemoteMICFailures : %d \r\n",
               u4RSNAStatsTKIPRemoteMICFailures);
    CliPrintf (CliHandle, "\r\r CCMPReplays           : %d \r\n",
               u4RSNAStatsCCMPReplays);
    CliPrintf (CliHandle, "\r\r CCMPDecryptErrors     : %d \r\n",
               u4RSNAStatsCCMPDecryptErrors);
    CliPrintf (CliHandle, "\r\r TKIPReplays           : %d \r\n",
               u4RSNAStatsTKIPReplays);

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowHandshakeStats                               */
/*                                                                           */
/* Description     : This function displays the values from the              */
/*                   RSNA Statistics Table based using WLANIndex             */
/*                                                                           */
/* Input(s)        : u4ProfileIdValue      - Wlan Interface Index            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */

INT4
RsnaCliShowHandshakeStats (tCliHandle CliHandle, UINT4 u4ProfileId)
{

    UINT4               u4CurrConfigIndex = u4ProfileId;
    UINT4               u4NextConfigIndex = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;
    if (nmhValidateIndexInstanceFsRSNAExtTable ((INT4) u4ProfileId) ==
        SNMP_SUCCESS)
    {
        do
        {
            /*Verify whether to display all WPA2 Config
             *              *details or to display particular WLAN. */
            if (u4ProfileId != 0)
            {
                u4TableWalk = 0;
                u4NextConfigIndex = u4CurrConfigIndex;

            }
            else
            {
                /* Fetches the next index from the
                 *                  * table by passing the current index */
                if (nmhGetNextIndexFsRSNAExtTable
                    ((INT4) u4CurrConfigIndex,
                     (INT4 *) &u4NextConfigIndex) != SNMP_SUCCESS)
                {
                    break;

                }
            }

            WssGetCapwapWlanId (u4NextConfigIndex, &u4WlanId);
            /*Fetches entries from
             *               *the table and displays it */
            RsnaCliShowHandshakeStatsDisplay (CliHandle, u4NextConfigIndex,
                                              u4WlanId);
            u4CurrConfigIndex = u4NextConfigIndex;
        }
        while (u4TableWalk != 0);
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

#ifdef PMF_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowMFPConfig                                    */
/*                                                                           */
/* Description     : This function displays the values of the Management     */
/*                   Frame Protection related parameters                     */
/*                                                                           */
/* Input(s)        : u4ProfileIdValue      - Wlan Interface Index            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */

INT4
RsnaCliShowMFPConfig (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    INT4                i4MFPRequired = 0;
    INT4                i4MFPCapable = 0;
    UINT4               u4AssocComeBackTime = 0;
    UINT4               u4SAQueryRetryTime = 0;
    UINT4               u4SAQueryMaxTimeOut = 0;
    UINT4               u4WlanId = 0;

    WssGetCapwapWlanId (u4ProfileId, &u4WlanId);

    if (RsnaGetDot11RSNAProtectedManagementFramesActivated
        (u4ProfileId, &i4MFPRequired) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    if (RsnaGetDot11RSNAUnprotectedManagementFramesAllowed
        (u4ProfileId, &i4MFPCapable) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RsnaGetDot11AssociationSAQueryMaximumTimeout
        (u4ProfileId, &u4SAQueryMaxTimeOut) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RsnaGetDot11AssociationSAQueryRetryTimeout
        (u4ProfileId, &u4SAQueryRetryTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsRSNAAssociationComeBackTime
        (u4ProfileId, &u4AssocComeBackTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Display the retrieved RSNA Statistics Table Entries using cliPrintf */
    CliPrintf (CliHandle, "\r\r Wlan Index            : %d \r\n", u4WlanId);

    if (i4MFPRequired == RSNA_MFPR)
    {
        CliPrintf (CliHandle, "\r\r MFPR                  : Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\r MFPR                  : Disabled \r\n");
    }

    if (i4MFPCapable == RSNA_MFPC)
    {
        CliPrintf (CliHandle, "\r\r MFPC                  : Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\r MFPC                  : Disabled \r\n");
    }

    CliPrintf (CliHandle, "\r\r SAQueryRetryTime      : %d \r\n",
               u4SAQueryRetryTime);
    CliPrintf (CliHandle, "\r\r SAQueryMaxTimeOut     : %d \r\n",
               u4SAQueryMaxTimeOut);
    CliPrintf (CliHandle, "\r\r AssocComeBackTime     : %d \r\n",
               u4AssocComeBackTime);

    return CLI_SUCCESS;
}

#endif

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowHandshkaeStatsDisplay                        */
/*                                                                           */
/* Description     : This function displays the values from the              */
/*                   RSNA Statistics Table based using WLANIndex.            */
/*                                                                           */
/* Input(s)        : u4ProfileIdValue      - Wlan Interface Index            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/

VOID
RsnaCliShowHandshakeStatsDisplay (tCliHandle CliHandle, UINT4 u4NextConfigIndex,
                                  UINT4 u4WlanId)
{
    UNUSED_PARAM (u4WlanId);
    UINT4               u4RSNA4WayHandshakecounter = 0;
    UINT4               u4RSNAGroupHandshakeCounter = 0;

    /* Get the RSNA Statistics Table Entries using RsnaGet */
    nmhGetFsRSNA4WayHandshakeCompletedStats ((INT4) u4NextConfigIndex,
                                             &u4RSNA4WayHandshakecounter);

    nmhGetFsRSNAGroupHandshakeCompletedStats ((INT4) u4NextConfigIndex,
                                              &u4RSNAGroupHandshakeCounter);

    /* Display the retrieved RSNA Statistics Table Entries using cliPrintf */
    CliPrintf (CliHandle, "\r\n\r Wlan Index                        : %d \r\n",
               u4WlanId);
    CliPrintf (CliHandle, "\r\n\r RSNA4WayHandshakecounter          : %d \r\n",
               u4RSNA4WayHandshakecounter);
    CliPrintf (CliHandle, "\r\n\r RSNAGroupHandshakeCounter         : %d \r\n",
               u4RSNAGroupHandshakeCounter);

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowRunningConfig                                  */
/*                                                                           */
/*  Description     : This function displays current RSNA configurations     */
/*                                                                           */
/*  Input(s)        : CliHandle        - Handle to the CLI Context           */
/*                    u4ProfileIdValue - Specified Wlan Interface Index      */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaShowRunningConfig (tCliHandle CliHandle, UINT4 u4WlanId)
{
    UINT4               u4ProfileId = 0;
    if (u4WlanId != 0)
    {
        if (nmhGetCapwapDot11WlanProfileIfIndex
            (u4WlanId, (INT4 *) &u4ProfileId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (u4ProfileId == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }
    }

    CliRegisterLock (CliHandle, PnacLock, PnacUnLock);
    PNAC_LOCK ();

    RsnaShowRunningConfigTables (CliHandle, u4ProfileId);
#ifdef WPA_WANTED
    WpaShowRunningConfigTables (CliHandle, u4ProfileId);
#endif

    PNAC_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowRunningConfigTables                            */
/*                                                                           */
/*  Description     : This function displays the current WLAN configuration  */
/*                    for all dot11 Rsna tables                              */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4CurrentWlanIndex = 0;
    UINT4               u4NextWlanIndex = 0;
    UINT4               u4CurrentConfigPairwiseCipherIndex = 0;
    UINT4               u4NextConfigPairwiseCipherIndex = 0;
    UINT4               u4CurrentAuthSuiteIndex = 0;
    UINT4               u4NextAuthSuiteIndex = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;
    UINT4               u4RsnaStatus = RSNA_DISABLED;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;

    /* DOT11 PRIVACY TABLE */
    u4CurrentWlanIndex = u4ProfileId;

    /* Loop through the Dot11Privacy Table to fetch RSNA Configurations */
    do
    {
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
        }
        else
        {
            /* Fetches the next WlanIndex from the Dot11Privacy Table 
             * by Passing the current WlanIndex */
            if (nmhGetNextIndexDot11PrivacyTable ((INT4) u4CurrentWlanIndex,
                                                  (INT4 *) &u4NextWlanIndex)
                != SNMP_SUCCESS)
            {
                /* Breaking the loop after printing the appropriate values
                 * for specific WlanIndex */
                break;
            }

            /* Assign the next wlanindex pointer to current to
             * get the appropriate value from GetnextIndex */
            u4CurrentWlanIndex = u4NextWlanIndex;
        }

        WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

        /* Show the commands in show running config, only when RSNA is enabled */
        i4Dot11RSNAEnabled = RSNA_DISABLED;
        RsnaGetDot11RSNAEnabled ((INT4) u4CurrentWlanIndex,
                                 &i4Dot11RSNAEnabled);

        if (i4Dot11RSNAEnabled == RSNA_DISABLED)
        {
            continue;
        }
        /* The following variable will be used to display the AuthSuiteTable
         * If RSNA is enabled on any WLAN, AuthSuiteTable will be displayed*/

        u4RsnaStatus = RSNA_ENABLED;
        /* Fetches entry from the Dot11 Privacy 
         * Table for the u4CurrentWlanIndex */
        RsnaShowRunningConfigPrivacyTableDisplay (CliHandle, u4CurrentWlanIndex,
                                                  u4WlanId);
    }
    while (u4TableWalk != 0);

    /* DOT11 RSNA CONFIG TABLE */
    u4CurrentWlanIndex = u4ProfileId;
    u4TableWalk = 1;

    /* Loop through the Dot11RsnaConfig Table to fetch RSNA Configurations */
    do
    {
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
        }
        else
        {
            /* Fetches the next WlanIndex from the Dot11RsnaConfig Table
             * by Passing the current WlanIndex */
            if (RsnaGetNextIndexDot11RSNAConfigTable ((INT4) u4CurrentWlanIndex,
                                                      (INT4 *) &u4NextWlanIndex)
                != SNMP_SUCCESS)
            {
                /* Breaking the loop after printing 
                 * the appropriate values
                 * for specific WlanIndex */
                break;
            }

            /* Assign the next wlanindex pointer to current to
             * get the appropriate value from GetnextIndex */
            u4CurrentWlanIndex = u4NextWlanIndex;
        }

        WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

        /* Show the commands in show running config, only when RSNA is enabled */
        i4Dot11RSNAEnabled = RSNA_DISABLED;
        RsnaGetDot11RSNAEnabled ((INT4) u4CurrentWlanIndex,
                                 &i4Dot11RSNAEnabled);

        if (i4Dot11RSNAEnabled == RSNA_DISABLED)
        {
            continue;
        }

        /* Fetches entry from the Dot11 Rsna Config 
         * Table for the u4CurrentWlanIndex */
        RsnaShowRunningConfigTableDisplay (CliHandle, u4CurrentWlanIndex,
                                           u4WlanId);
    }
    while (u4TableWalk != 0);

    /* DOT11 RSNA CONFIG PAIRWISE CIPHER TABLE */
    u4CurrentWlanIndex = u4ProfileId;
    u4CurrentConfigPairwiseCipherIndex = 0;

    /* Loop through the Dot11RsnaConfigPairwiseCiphers 
     * Table to fetch RSNA Configurations */
    while (RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable
           ((INT4) u4CurrentWlanIndex, (INT4 *) &u4NextWlanIndex,
            u4CurrentConfigPairwiseCipherIndex,
            &u4NextConfigPairwiseCipherIndex) == SNMP_SUCCESS)
    {
        /* Assign the next wlanindex pointer to current to 
         * get the appropriate value from GetnextIndex */

        u4CurrentWlanIndex = u4NextWlanIndex;
        u4CurrentConfigPairwiseCipherIndex = u4NextConfigPairwiseCipherIndex;

        if (u4ProfileId != 0)
        {
            /* Verify if the entry fetched is for the input given 
             * in this function call */
            if (u4CurrentWlanIndex == u4ProfileId)
            {
                WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

                /* Show the commands in show running config, */
                /* only when RSNA is enabled */
                i4Dot11RSNAEnabled = RSNA_DISABLED;
                RsnaGetDot11RSNAEnabled ((INT4) u4CurrentWlanIndex,
                                         &i4Dot11RSNAEnabled);
                if (i4Dot11RSNAEnabled == RSNA_DISABLED)
                {
                    continue;
                }

                /* Print the entry from Dot11RsnaConfigPairwiseCiphers 
                 * Table for the u4CurrentWlanIndex */
                RsnaShowRunningConfigCipherTableDisplay (CliHandle,
                                                         u4CurrentWlanIndex,
                                                         u4CurrentConfigPairwiseCipherIndex,
                                                         u4WlanId);
            }
            else
            {
                /* Breaking the loop after printing the appropriate values 
                 * for specific WlanIndex */
                break;
            }
        }
        else
        {
            WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

            /* Show the commands in show running config, */
            /* only when RSNA is enabled */
            i4Dot11RSNAEnabled = RSNA_DISABLED;
            RsnaGetDot11RSNAEnabled ((INT4) u4CurrentWlanIndex,
                                     &i4Dot11RSNAEnabled);
            if (i4Dot11RSNAEnabled == RSNA_DISABLED)
            {
                continue;
            }

            /* Print the entry from Dot11RsnaConfigPairwiseCiphers 
             * Table for the u4CurrentWlanIndex */
            RsnaShowRunningConfigCipherTableDisplay (CliHandle,
                                                     u4CurrentWlanIndex,
                                                     u4CurrentConfigPairwiseCipherIndex,
                                                     u4WlanId);
        }
    }

    /* DOT11 AUTHENTICATION SUITES TABLE */
    u4CurrentAuthSuiteIndex = 0;
    u4CurrentWlanIndex = u4ProfileId;
    u4CurrentConfigPairwiseCipherIndex = 0;

    /* Loop through the Dot11RsnaConfigAuthenticationSuites Table to 
     * fetch RSNA Configurations */
    if (u4RsnaStatus == RSNA_ENABLED)
    {
        /*If the variable is enabled,then AuthSuiteTable will be displayed */
        while (RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable
               ((INT4) u4CurrentWlanIndex, (INT4 *) &u4NextWlanIndex,
                u4CurrentAuthSuiteIndex, &u4NextAuthSuiteIndex) == SNMP_SUCCESS)
        {
            u4CurrentAuthSuiteIndex = u4NextAuthSuiteIndex;
            u4CurrentWlanIndex = u4NextWlanIndex;
            if (u4ProfileId != 0)
            {
                /* Verify if the entry fetched is for the input given
                 *              * in this function call */
                if (u4CurrentWlanIndex == u4ProfileId)
                {
                    WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);
                    /* Print the entry from RSNA Stats
                     *                  * Table for the u4CurrentWlanIndex */
                    RsnaShowRunningConfigAuthSuiteTableDisplay (CliHandle,
                                                                u4CurrentWlanIndex,
                                                                u4CurrentAuthSuiteIndex,
                                                                u4WlanId);
                }
                else
                {
                    /* Breaking the loop after printing the appropriate values
                     *                  * for specific WlanIndex, StatsIndex */
                    break;
                }
            }
            else
            {
                WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);
                /* Print the entry from RSNA Stats
                 *              * Table for the u4CurrentWlanIndex */
                RsnaShowRunningConfigAuthSuiteTableDisplay (CliHandle,
                                                            u4CurrentWlanIndex,
                                                            u4CurrentAuthSuiteIndex,
                                                            u4WlanId);

            }
        }

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowRunningConfigPrivacyTableDisplay               */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11 Privacy Table                                */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaShowRunningConfigPrivacyTableDisplay (tCliHandle CliHandle,
                                          UINT4 u4ProfileId, UINT4 u4WlanId)
{
    UINT4               u4RsnaStatus = RSNA_DISABLED;

    /* Fetch and display the Dot11Privacy Table Entries using RsnaGet */
    nmhGetDot11RSNAActivated ((INT4) u4ProfileId, (INT4 *) &u4RsnaStatus);

    if (u4RsnaStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 "
                   "enable %d", u4WlanId);
    }

    RsnaGetDot11RSNAPreauthenticationEnabled ((INT4) u4ProfileId,
                                              (INT4 *) &u4RsnaStatus);

    if (u4RsnaStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 "
                   "pre-authentication enable %d", u4WlanId);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowRunningConfigTableDisplay                      */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11RsnaConfig Table                              */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaShowRunningConfigTableDisplay (tCliHandle CliHandle,
                                   UINT4 u4ProfileId, UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE GroupCipher = { NULL, 0 };
    UINT1               au1GroupCipher[RSNA_MAX_CIPHER_TYPE_LENGTH] = "";
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupRekeyPkts = 0;
    UINT4               u4GroupRekeyStrict = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4SATimeOut = 0;
    UINT1               u1RsnaOkcStatus = 0;

    MEMSET (au1GroupCipher, '\0', RSNA_MAX_CIPHER_TYPE_LENGTH);

    GroupCipher.pu1_OctetList = au1GroupCipher;

    /* Fetch and display the Dot11RsnaConfig Table Entries using RsnaGet */
    RsnaGetDot11RSNAConfigGroupCipher ((INT4) u4ProfileId, &GroupCipher);

    if ((MEMCMP (GroupCipher.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                 RSNA_MAX_CIPHER_TYPE_LENGTH) == 0))
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 "
                   "groupwise ciphers aes %d", u4WlanId);

    }
    else if ((MEMCMP (GroupCipher.pu1_OctetList, gau1RsnaCipherSuiteTKIP,
                      RSNA_MAX_CIPHER_TYPE_LENGTH) == 0))
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 "
                   "groupwise ciphers tkip %d", u4WlanId);

    }

    RsnaGetDot11RSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                            (INT4 *) &u4GroupRekeyMethod);

    switch (u4GroupRekeyMethod)
    {

        case RSNA_GROUP_REKEY_DISABLED:
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 group "
                       "rekey method disabled %d", u4WlanId);
            break;

        case RSNA_GROUP_REKEY_PACKETBASED:
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 group "
                       "rekey method packetBased %d", u4WlanId);
            break;
        case RSNA_GROUP_REKEY_TIMEPACKETBASED:
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 group "
                       "rekey method timepacketBased %d", u4WlanId);
            break;
        default:
            break;
    }

    RsnaGetDot11RSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                          &u4GroupRekeyTime);
    if (u4GroupRekeyTime != RSNA_DEF_GROUP_REKEY_TIME)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 gtk rekey "
                   "interval %d %d", u4GroupRekeyTime, u4WlanId);
    }
    RsnaGetDot11RSNAConfigGroupRekeyPackets ((INT4) u4ProfileId,
                                             &u4GroupRekeyPkts);

    if (u4GroupRekeyPkts != RSNA_DEF_REKEY_PKTS)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 gtk rekey "
                   "packets %d %d", u4GroupRekeyPkts, u4WlanId);
    }

    RsnaGetDot11RSNAConfigGroupRekeyStrict ((INT4) u4ProfileId,
                                            (INT4 *) &u4GroupRekeyStrict);

    if (u4GroupRekeyStrict == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 strict gtk "
                   "rekey enable %d", u4WlanId);
    }

    RsnaGetDot11RSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                            &u4GroupUpdateCount);
    /* Since Defval for u4GroupUpdateCount equals "3", it need not be printed */
    if (u4GroupUpdateCount != RSNA_DEF_UPDATE_COUNT)
    {
        CliPrintf (CliHandle,
                   "\r\n config wlan security wpa wpa2 group handshake "
                   "retry count %d %d", u4GroupUpdateCount, u4WlanId);
    }

    RsnaGetDot11RSNAConfigPairwiseUpdateCount ((INT4) u4ProfileId,
                                               &u4PairwiseUpdateCount);
    if (u4GroupUpdateCount != RSNA_DEF_UPDATE_COUNT)
    {
        CliPrintf (CliHandle,
                   "\r\n config wlan security wpa wpa2 pairwise-handshake"
                   "-retry-count %d %d", u4GroupUpdateCount, u4WlanId);
    }
    RsnaGetDot11RSNAConfigPMKLifetime ((INT4) u4ProfileId, &u4PMKLifetime);
    /* Since Defval for u4PMKLifetime equals "43200", it need not be printed */
    if (u4PMKLifetime != RSNA_DEF_PMK_LIFETIME)
    {

        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2"
                   " pmk-life-time %d %d", u4PMKLifetime, u4WlanId);
    }

    RsnaGetDot11RSNAConfigPMKReauthThreshold ((INT4) u4ProfileId,
                                              &u4PMKReAuthThreshold);

    /* Since Defval for u4PMKReAuthThreshold equals "70", */
    /* it need not be printed */
    if (u4PMKReAuthThreshold != RSNA_DEF_PMK_REAUTH_THRESHOLD)
    {
        CliPrintf (CliHandle,
                   "\r\n config wlan security wpa wpa2 pmk-reauth-threshold"
                   " %d %d", u4PMKReAuthThreshold, u4WlanId);
    }

    RsnaGetDot11RSNAConfigSATimeout ((INT4) u4ProfileId, &u4SATimeOut);

    /* Since Defval for u4SATimeOut equals "60", it need not be printed */
    if (u4SATimeOut != RSNA_DEF_SA_TIME_OUT)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 sa-timeout"
                   " %d %d", u4SATimeOut, u4WlanId);
    }

    nmhGetFsRSNAOKCEnabled ((INT4) u4ProfileId, (INT4 *) &u1RsnaOkcStatus);

    if (u1RsnaOkcStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 okc "
                   "enable %d", u4WlanId);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowRunningConfigCipherTableDisplay                */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11RsnaConfigPairwiseCiphersTable                */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaShowRunningConfigCipherTableDisplay (tCliHandle CliHandle,
                                         UINT4 u4ProfileId,
                                         UINT4 u4RSNAConfigPairwiseCipherIndex,
                                         UINT4 u4WlanId)
{
    UINT4               u4PairwiseCipherEnabled = RSNA_DISABLED;

    /* Fetch and display the Dot11RsnaConfigPairwiseCiphers 
     * Table Entries using RsnaGet */
    RsnaGetDot11RSNAConfigPairwiseCipherEnabled ((INT4) u4ProfileId,
                                                 u4RSNAConfigPairwiseCipherIndex,
                                                 (INT4 *)
                                                 &u4PairwiseCipherEnabled);

    if (u4RSNAConfigPairwiseCipherIndex == RSNA_CCMP)
    {
        if (u4PairwiseCipherEnabled == RSNA_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 pairwise "
                       "aes enable %d", u4WlanId);
        }
    }
    else if (u4RSNAConfigPairwiseCipherIndex == RSNA_TKIP)
    {
        if (u4PairwiseCipherEnabled == RSNA_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 pairwise "
                       "tkip enable %d", u4WlanId);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : RsnaShowRunningConfigAuthSuiteTableDisplay             */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11RsnaConfigAuthenticationSuitesTable           */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaShowRunningConfigAuthSuiteTableDisplay (tCliHandle CliHandle,
                                            UINT4 u4ProfileId,
                                            UINT4 u4AuthSuiteIndex,
                                            UINT4 u4WlanId)
{
    UINT4               u4AuthSuiteEnabled = RSNA_DISABLED;

    UNUSED_PARAM (u4WlanId);
    /* Fetch and display the Dot11RsnaConfigPairwiseCiphers Table 
     * Entries using RsnaGet */
    RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled ((INT4) u4ProfileId,
                                                      u4AuthSuiteIndex,
                                                      (INT4 *)
                                                      &u4AuthSuiteEnabled);

    if (u4AuthSuiteIndex == RSNA_AUTHSUITE_PSK)
    {
        if (u4AuthSuiteEnabled == RSNA_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 "
                       "akm psk enable");
        }
    }
    else if (u4AuthSuiteIndex == RSNA_AUTHSUITE_8021X)
    {
        if (u4AuthSuiteEnabled == RSNA_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa wpa2 "
                       "akm 8021x enable");
        }
    }

    return CLI_SUCCESS;
}

#ifdef RSNA_TEST_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliTestPairwiseUpdateCount                          */
/*                                                                           */
/* Description     : Clears RSNA Stats and Counters in Config table          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliTestPairwiseUpdateCount (tCliHandle CliHandle)
{
    gu1RsnaTestPairwiseUpdateCount = OSIX_TRUE;
    UNUSED_PARAM (CliHandle);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliTestGroupwiseUpdateCount                         */
/*                                                                           */
/* Description     : Clears RSNA Stats and Counters in Config table          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
RsnaCliTestGroupwiseUpdateCount (tCliHandle CliHandle)
{
    gu1RsnaTestGroupwiseUpdateCount = OSIX_TRUE;
    UNUSED_PARAM (CliHandle);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaShowPmkCacheEntry                                   */
/*                                                                           */
/* Description     : Shows PMKSa Cache Entries                               */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliShowPmkCacheEntry (tCliHandle CliHandle)
{

    CliPrintf (CliHandle, "\nBSSID \t\t\t STA MAC \t\t PMKID\n");
    CliPrintf (CliHandle,
               "\n-------------- \t\t-------------- \t\t --------\n");
    RBTreeWalk (gRsnaGlobals.RsnaPmkTable, RsnaCliPmkCacheWalkFn, &CliHandle,
                0);

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaCliPmkCacheWalkFn                                      *
*                                                                           *
* Description  : Function to Walk through the PMK Table to print PMKID's    *
*                                                                           *
* Input        : pRBEle        :- RBTree Node                               *
*                visit         :- Order of visiting the tree                *
*                u4Level       :- Level of the tree                         *
*                pArg          :- Input arg to the function                 * 
*                pOut          :- Output from the function                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : TRUE/FALSE                                                 *
*                                                                           *
*****************************************************************************/
INT4
RsnaCliPmkCacheWalkFn (tRBElem * pRBEle, eRBVisit visit, UINT4 u4Level,
                       VOID *pArg, VOID *pOut)
{

    tRsnaPmkSa         *pRsnaPmkSa = NULL;
    tCliHandle          CliHandle = 0;
    UINT4               u4Index = 0;

    CliHandle = *(tCliHandle *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {

        if (pRBEle != NULL)
        {
            pRsnaPmkSa = (tRsnaPmkSa *) pRBEle;

            CliPrintf (CliHandle, "\n%x:%x:%x:%x:%x:%x \t %x:%x:%x:%x:%x:%x \t",
                       pRsnaPmkSa->au1SrcMac[0],
                       pRsnaPmkSa->au1SrcMac[1],
                       pRsnaPmkSa->au1SrcMac[2],
                       pRsnaPmkSa->au1SrcMac[3],
                       pRsnaPmkSa->au1SrcMac[4],
                       pRsnaPmkSa->au1SrcMac[5],
                       pRsnaPmkSa->au1SuppMac[0],
                       pRsnaPmkSa->au1SuppMac[1],
                       pRsnaPmkSa->au1SuppMac[2],
                       pRsnaPmkSa->au1SuppMac[3],
                       pRsnaPmkSa->au1SuppMac[4], pRsnaPmkSa->au1SuppMac[5]);
            for (u4Index = 0; u4Index < RSNA_PMKID_LEN; u4Index++)
            {
                CliPrintf (CliHandle, "%x", pRsnaPmkSa->au1PmkId[u4Index]);
            }

            CliPrintf (CliHandle, "\n");
        }

    }

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    return RB_WALK_CONT;

}

#endif
#ifdef WPA_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetMixMode                         */
/*                                                                           */
/* Description     : It enables and disables the wpa2 secuirty               */
/*                   security  mode                                          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1Status     - WPA2 security mode                       */
/*                   u4ProfileId  - index of wlan interface                  */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetMixMode (tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1Status*/

    if (nmhTestv2FsRSNAConfigMixMode (&u4ErrorCode, (INT4) u4ProfileId,
                                      (INT1) u1Status) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAEnabled object to either
     * enable or disable*/

    if (nmhSetFsRSNAConfigMixMode ((INT4) u4ProfileId,
                                   (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetSecurity                                      */
/*                                                                           */
/* Description     : It enables and disables the wpa2 secuirty               */
/*                   security  mode                                          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u1Status     - WPA2 security mode                       */
/*                   u4ProfileId  - index of wlan interface                  */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetSecurity (tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available
     * and also verfies the value of u1Status*/

    if (nmhTestv2FsRSNAConfigActivated (&u4ErrorCode, (INT4) u4ProfileId,
                                        (INT1) u1Status) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAEnabled object to either
     * enable or disable*/

    if (nmhSetFsRSNAConfigActivated ((INT4) u4ProfileId,
                                     (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetGroupwiseCipher                               */
/*                                                                           */
/* Description     : This function sets the given  groupwise cipher          */
/*                                                                           */
/* Input(s)        : CliHandle          -  CLI Handler                       */
/*                   u1CipherType       - TKIP OR CCMP                       */
/*                   u4ProfileId        - index of the wlan interface        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetGroupwiseCipher (tCliHandle CliHandle, UINT1 u1CipherType,
                          UINT4 u4ProfileId)
{
    tSNMP_OCTET_STRING_TYPE tCipherType = { NULL, 0 };
    UINT4               u4ErrorCode = 0;
    UINT1               au1Buf[RSNA_MAX_CIPHER_TYPE_LENGTH] = "";
    UNUSED_PARAM (CliHandle);

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    tCipherType.pu1_OctetList = au1Buf;

    /* Verfies whether the given cipher type is TKIP or CCMP */
    if (u1CipherType == RSNA_TKIP)
    {
        MEMCPY (tCipherType.pu1_OctetList, gau1WpaCipherSuiteTKIP,
                RSNA_MAX_CIPHER_TYPE_LENGTH);
        tCipherType.i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;
    }

    else if (u1CipherType == RSNA_CCMP)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_PASSPHRASE);
        return CLI_FAILURE;
    }

    /* Tests whether the given index and GroupCipher type is valid or not */
    if (nmhTestv2FsRSNAConfigGroupCipher (&u4ErrorCode, (INT4) u4ProfileId,
                                          &tCipherType) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_SSID);
        return CLI_FAILURE;
    }

    /* Sets the given GroupCipher type to the particular wlan index */
    if (nmhSetFsRSNAConfigGroupCipher ((INT4) u4ProfileId, &tCipherType)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetGroupRekeyinterval                            */
/*                                                                           */
/* Description     : This function sets the group rekey interval             */
/*                                                                           */
/* Input(s)        : CliHandle           - CLI Handler                       */
/*                   u4GroupRekeyInterval- group rekey interval              */
/*                   u4ProfileId         - index of the wlan interface       */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetGroupRekeyinterval (tCliHandle CliHandle, UINT4 u4GroupRekeyInterval,
                             UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for  dot11RSNAConfigGroupRekeyTime 
     * object is within range */

    if (nmhTestv2FsRSNAConfigGroupRekeyTime (&u4ErrorCode,
                                             (INT4) u4ProfileId,
                                             u4GroupRekeyInterval) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigGroupRekeyTime object to specified
     * time interval*/
    if (nmhSetFsRSNAConfigGroupRekeyTime ((INT4) u4ProfileId,
                                          u4GroupRekeyInterval) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  WpaCliSetAkmPsk                                       */
/*                                                                           */
/* Description     : This function test and sets the PSK Value or PSK        */
/*                   based on the PSK type mentioned.                        */
/*                                                                           */
/*                                                                           */
/* Input(s)        : CliHandle   - CLI Handler                               */
/*                   u1PskFormat   - PSK passphrase or PSK Value             */
/*                   pu1Key       - Value entered as PSK                     */
/*                   u4ProfileId    - Wlan ID of the interface               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetAkmPsk (tCliHandle CliHandle, UINT1 u1PskFormat,
                 UINT1 *pu1Key, UINT4 u4ProfileId)
{
    tSNMP_OCTET_STRING_TYPE PSKPassphrase = { NULL, 0 };    /* used to pass 
                                                             * the Key */
    UINT4               u4ErrorCode = 0;
    UINT4               u4KeyLength = 0;
    UINT1               au1Buf[RSNA_MAX_PSK_LENGTH] = { 0 };
    UINT1               au1TempBuf[RSNA_CLI_THIRD_ARG] = { 0 };
    UINT1               au1Output[RSNA_PSK_LEN] = { 0 };
    UINT4               u4Index = 0;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1Buf, 0, sizeof (au1Buf));

    PSKPassphrase.pu1_OctetList = au1Buf;
    MEMCPY (PSKPassphrase.pu1_OctetList, pu1Key, STRLEN (pu1Key));
    PSKPassphrase.i4_Length = (INT4) STRLEN (pu1Key);
    u4KeyLength = (UINT4) STRLEN (pu1Key);

    /* Checks if the value entered is PSKPassPhraseValue */

    if (u1PskFormat == RSNA_AKM_PSK_SETKEY_ASCII)
    {

        if ((nmhTestv2FsRSNAConfigPSKPassPhrase
             (&u4ErrorCode, (INT4) u4ProfileId,
              &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetFsRSNAConfigPSKPassPhrase
             ((INT4) u4ProfileId, &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    /* Checks if the value entered is PSK Value */
    else
    {

        /* Scan the given input and convert into the required form of
         * hexadecimal bytes. Total length will be 64. Converted length
         * will be 32*/
        for (u4Index = 0; u4Index < u4KeyLength; u4Index += 2)
        {
            /* Assemble a digit pair into the hexbyte string */
            au1TempBuf[0] = pu1Key[u4Index];
            au1TempBuf[1] = pu1Key[u4Index + 1];

            /* Convert the hex pair to an integer */
            SSCANF ((const CHR1 *) au1TempBuf, "%x",
                    (UINT4 *) &au1Output[u4Index / 2]);
        }
        MEMCPY (PSKPassphrase.pu1_OctetList, au1Output, (u4KeyLength / 2));
        PSKPassphrase.i4_Length = (INT4) (u4KeyLength / 2);

        if ((nmhTestv2FsRSNAConfigPSKValue
             (&u4ErrorCode, (INT4) u4ProfileId,
              &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if ((nmhSetFsRSNAConfigPSKValue
             ((INT4) u4ProfileId, &PSKPassphrase)) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetGroupUpdateCount                              */
/*                                                                           */
/* Description     : This function sets the retry-count value of the         */
/*                   group handshake for the mentioned wlan id               */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4RetryCount  - Group handshake retry count value       */
/*                   u4ProfileId      - Wlan ID of the interface             */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetGroupUpdateCount (tCliHandle CliHandle, UINT4 u4RetryCount,
                           UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigGroupUpdateCount 
     * object is within range */

    if (nmhTestv2FsRSNAConfigGroupUpdateCount (&u4ErrorCode,
                                               (INT4) u4ProfileId,
                                               u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /* This function sets the  dot11RSNAConfigGroupUpdateCount object to specified 
     * retry count value */

    if (nmhSetFsRSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                            u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  WpaCliSetPairwiseUpdateCount                          */
/*                                                                           */
/* Description     : This function set the retry count for message 1 & 3     */
/*                   of 4 way handshake                                      */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4RetryCount - Retransmission Count                     */
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetPairwiseUpdateCount (tCliHandle CliHandle, UINT4 u4RetryCount,
                              UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigPairwiseUpdateCount 
     * object is within range */

    if (nmhTestv2FsRSNAConfigPairwiseUpdateCount (&u4ErrorCode,
                                                  (INT4) u4ProfileId,
                                                  u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the  dot11RSNAConfigPaiwiseUpdateCount object to specified 
     * retry count value */

    if (nmhSetFsRSNAConfigPairwiseUpdateCount
        ((INT4) u4ProfileId, u4RetryCount) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  WpaCliSetPMKLifeTime                                  */
/*                                                                           */
/* Description     : This function set the PMK Life Time of PMK in cache     */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4LifeTime   - Life Time of the PMK value to be in cache*/
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetPMKLifeTime (tCliHandle CliHandle, UINT4 u4LifeTime, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigPMKLifetime 
     * object is within range */

    if (nmhTestv2FsRSNAConfigPMKLifetime (&u4ErrorCode, (INT4) u4ProfileId,
                                          u4LifeTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigPMKLifetime object to specified 
     * interval  */

    if (nmhSetFsRSNAConfigPMKLifetime ((INT4) u4ProfileId, u4LifeTime)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :  WpaCliSetSATimeOut                                    */
/*                                                                           */
/* Description     : This function Set the SA Timeout                        */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4TimeValue  - Maximum time for station association     */
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetSATimeOut (tCliHandle CliHandle, UINT4 u4TimeValue, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the given wlan index is available,
     * and also checks whether given value for dot11RSNAConfigSATimeout 
     * object is within range */

    if (nmhTestv2FsRSNAConfigSATimeout (&u4ErrorCode, (INT4) u4ProfileId,
                                        u4TimeValue) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the dot11RSNAConfigSATimeout object to specified 
     * interval  */

    if (nmhSetFsRSNAConfigSATimeout ((INT4) u4ProfileId, u4TimeValue) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetAuthSuiteStatus                               */
/*                                                                           */
/* Description     : This function enables or disables the given             */
/*                   Authentication suite (8021x or PSK)                     */
/*                                                                           */
/* Input(s)        : CliHandle              -  CLI Handler                   */
/*                   u1AuthType             -  PSK  or 8021x                 */
/*                   u1Status               - enable/disable                 */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetAuthSuiteStatus (tCliHandle CliHandle, UINT1 u1AuthType,
                          UINT1 u1Status, UINT4 u4ProfileId)
{

    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the wlan index given is available 
     * and a correct value for the is dot11RSNAConfigAuthenticationSuiteEnabled
     * given */

    if (WpaTestv2FsRSNAConfigAuthenticationSuiteEnabled (&u4ErrorCode,
                                                         (UINT4) u1AuthType,
                                                         (INT4) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAConfigAuthenticationSuiteEnabled
     * to the given status */

    if (WpaSetFsRSNAConfigAuthenticationSuiteEnabled (u4ProfileId,
                                                      (UINT4) u1AuthType,
                                                      (INT1) u1Status)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliClearStats                                       */
/*                                                                           */
/* Description     : Clears RSNA Stats and Counters in Config table          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliClearStats (tCliHandle CliHandle)
{

    UNUSED_PARAM (CliHandle);

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRSNAClearAllCounters (&u4ErrorCode, (INT4) RSNA_CLEAR_FLAG)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRSNAClearAllCounters (RSNA_CLEAR_FLAG) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliClearCounterDetail                               */
/*                                                                           */
/* Description     : Clears RSNA Stats and Counters in Config table          */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                 : u4ProfileId  - Profile Index                            */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliClearCountersDetail (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UNUSED_PARAM (CliHandle);
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRSNAClearCounter
        (&u4ErrorCode, (INT4) u4ProfileId,
         (INT4) RSNA_CLEAR_FLAG) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsRSNAClearCounter ((INT4) u4ProfileId, (INT4) RSNA_CLEAR_FLAG) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliSetOkcStatus                                     */
/*                                                                           */
/* Description     : This function sets status of the opportunisic key       */
/*                   caching to enabled/disabled                             */
/*                                                                           */
/* Input(s)        : CliHandle          -  CLI Handler                       */
/*                   u1Status           - enable/disable                     */
/*                   u4ProfileId        - index of the wlan interface        */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliSetOkcStatus (tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    /* This function checks whether the wlan index given is available
     *      * and a correct value for the dot11RSNAConfigPairwiseCipherEnabled is
     *           * given */

    if (nmhTestv2FsRSNAOKCEnabled (&u4ErrorCode, (INT4) u4ProfileId,
                                   (INT1) u1Status) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* This function sets the status of the dot11RSNAConfigPairwiseCipherEnabled
     *      * to the given status */

    if (nmhSetFsRSNAOKCEnabled ((INT4) u4ProfileId, (INT1) u1Status) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowMixModeStatus                     */
/*                                                                           */
/* Description     : This function displays the WPA2  status                 */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliShowMixModeStatus (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4Status = 0;
    UINT4               u4CurrWpa2Index = u4ProfileId;
    UINT4               u4NextWpa2Index = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;

    do
    {
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
            u4NextWpa2Index = u4CurrWpa2Index;

        }
        else if (u4CurrWpa2Index == 0)
        {
            if (nmhGetFirstIndexDot11PrivacyTable ((INT4 *) &u4CurrWpa2Index)
                != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            u4NextWpa2Index = u4CurrWpa2Index;
        }
        else
        {
            /* Fetches the next index from the 
             * table by passing the current index */
            if (nmhGetNextIndexDot11PrivacyTable
                ((INT4) u4CurrWpa2Index,
                 (INT4 *) &u4NextWpa2Index) != SNMP_SUCCESS)
            {
                break;

            }
        }
        WssGetCapwapWlanId (u4NextWpa2Index, &u4WlanId);
        /*Fetches entries from the table and displays it */
        nmhGetFsRSNAConfigMixMode ((INT4) u4NextWpa2Index, (INT4 *) &u4Status);
        CliPrintf (CliHandle, " \t\t WlanIndex       : %d \n", u4WlanId);
        if (u4Status == MIX_MODE_ENABLED)
        {
            CliPrintf (CliHandle, " \t\t Mix-Mode Status      : Enabled\n");
        }
        else if (u4Status == MIX_MODE_DISABLED)
        {
            CliPrintf (CliHandle, " \t\t Mix-Mode Status      : Disabled\n");
        }
        u4CurrWpa2Index = u4NextWpa2Index;
    }
    while (u4TableWalk != 0);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RsnaCliShowWpa1Status                                    */
/*                                                                           */
/* Description     : This function displays the WPA2  status                 */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   WlanIndex    - Index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
RsnaCliShowWpa1Status (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4Status = 0;
    UINT4               u4CurrWpa2Index = u4ProfileId;
    UINT4               u4NextWpa2Index = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;

    do
    {
        /*Verify whether to display all WPA2  details or 
         * to display for  particular WLAN. */
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
            u4NextWpa2Index = u4CurrWpa2Index;

        }
        else if (u4CurrWpa2Index == 0)
        {
            if (nmhGetFirstIndexDot11PrivacyTable ((INT4 *) &u4CurrWpa2Index)
                != SNMP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            u4NextWpa2Index = u4CurrWpa2Index;
        }
        else
        {
            /* Fetches the next index from the 
             * table by passing the current index */
            if (nmhGetNextIndexDot11PrivacyTable
                ((INT4) u4CurrWpa2Index,
                 (INT4 *) &u4NextWpa2Index) != SNMP_SUCCESS)
            {
                break;

            }
        }
        WssGetCapwapWlanId (u4NextWpa2Index, &u4WlanId);
        /*Fetches entries from the table and displays it */
        nmhGetFsRSNAConfigActivated ((INT4) u4NextWpa2Index,
                                     (INT4 *) &u4Status);
        CliPrintf (CliHandle, " \t\t WlanIndex       : %d \n", u4WlanId);
        if (u4Status == WPA_ENABLED)
        {
            CliPrintf (CliHandle, " \t\t WpaStatus      : Enabled\n");
        }
        else if (u4Status == RSNA_DISABLED)
        {
            CliPrintf (CliHandle, " \t\t WpaStatus      : Disabled\n");
        }
        u4CurrWpa2Index = u4NextWpa2Index;
    }
    while (u4TableWalk != 0);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliShowConfig                                       */
/*                                                                           */
/* Description     : This function displays the RSNA Config  details         */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4ProfileId  - index of the wlan interface              */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliShowConfig (tCliHandle CliHandle, UINT4 u4ProfileId)
{

    UINT4               u4CurrConfigIndex = u4ProfileId;
    UINT4               u4NextConfigIndex = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;

    /*Validating the WlanIndex that has 
     * been passed from the command line */
    if (RsnaValidateIndexInstanceDot11RSNAConfigTable ((INT4) u4ProfileId) ==
        SNMP_SUCCESS)
    {
        do
        {
            /*Verify whether to display all WPA2 Config 
             *details or to display particular WLAN. */
            if (u4ProfileId != 0)
            {
                u4TableWalk = 0;
                u4NextConfigIndex = u4CurrConfigIndex;

            }
            else
            {
                /* Fetches the next index from the
                 * table by passing the current index */
                if (nmhGetNextIndexFsRSNAConfigTable
                    ((INT4) u4CurrConfigIndex,
                     (INT4 *) &u4NextConfigIndex) != SNMP_SUCCESS)
                {
                    break;

                }
            }

            WssGetCapwapWlanId (u4NextConfigIndex, &u4WlanId);
            /*Fetches entries from 
             *the table and displays it */
            WpaCliShowConfigDisplay (CliHandle, u4NextConfigIndex, u4WlanId);
            u4CurrConfigIndex = u4NextConfigIndex;
        }
        while (u4TableWalk != 0);
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliShowConfigDisplay                                */
/*                                                                           */
/* Description     : This function fetches the entries of the config table   */
/*                   and displays it.                                        */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4NextConfigIndex - Index of the wlan-interface         */
/*                                                                           */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*                                                                           */
/*****************************************************************************/

VOID
WpaCliShowConfigDisplay (tCliHandle CliHandle, UINT4 u4NextConfigIndex,
                         UINT4 u4WlanId)
{
    UNUSED_PARAM (u4WlanId);
    tSNMP_OCTET_STRING_TYPE GroupCipher = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE AuthSuiteSelected = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PairwiseCipherSelected = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherSelected = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE AuthSuiteRequested = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PairwiseCipherRequested = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE GroupwiseCipherRequested = { NULL, 0 };
    UINT1               au1GroupCipher[RSNA_MAX_CIPHER_TYPE_LENGTH] = "";
    UINT1               au1AuthSuiteSelected[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH] =
        "";
    UINT1               au1PairwiseCipherSelected[RSNA_MAX_CIPHER_TYPE_LENGTH] =
        "";
    UINT1               au1GroupCipherSelected[RSNA_MAX_CIPHER_TYPE_LENGTH] =
        "";
    UINT1               au1PMKIDUsed[RSNA_MAX_PMKID_LENGTH] = "";
    UINT1               au1AuthSuiteRequested[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH] =
        "";
    UINT1               au1PairwiseCipherRequested[RSNA_MAX_CIPHER_TYPE_LENGTH]
        = "";
    UINT1               au1GroupwiseCipherRequested[RSNA_MAX_CIPHER_TYPE_LENGTH]
        = "";
    UINT4               u4ConfigVersion = 0;
    UINT4               u4PairwiseKeySupported = 0;
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4GroupCipherSize = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4PTKSAReplayCounter = 0;
    UINT4               u4SATimeOut = 0;
    UINT4               u4TKIPCounterMeasuresInvoked = 0;
    UINT4               u4FourWayHandshakeFailures = 0;
    UINT4               u4GTKSAReplayCountersCount = 0;
    INT4                i4MixMode = 0;

    MEMSET (au1GroupCipher, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1AuthSuiteSelected, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1PairwiseCipherSelected, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1GroupCipherSelected, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1PMKIDUsed, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1AuthSuiteRequested, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1PairwiseCipherRequested, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));
    MEMSET (au1GroupwiseCipherRequested, '\0', (RSNA_MAX_CIPHER_TYPE_LENGTH));

    GroupCipher.pu1_OctetList = au1GroupCipher;
    AuthSuiteSelected.pu1_OctetList = au1AuthSuiteSelected;
    PairwiseCipherSelected.pu1_OctetList = au1PairwiseCipherSelected;
    GroupwiseCipherSelected.pu1_OctetList = au1GroupCipherSelected;
    AuthSuiteRequested.pu1_OctetList = au1AuthSuiteRequested;
    PairwiseCipherRequested.pu1_OctetList = au1PairwiseCipherRequested;
    GroupwiseCipherRequested.pu1_OctetList = au1GroupwiseCipherRequested;

    /* Fetching the entries from dot11RSNAConfigTable */

    nmhGetFsRSNAConfigVersion ((INT4) u4NextConfigIndex, &u4ConfigVersion);

    WpaGetFsRSNAConfigPairwiseKeysSupported ((INT4) u4NextConfigIndex,
                                             &u4PairwiseKeySupported);

    WpaGetFsRSNAConfigGroupCipher ((INT4) u4NextConfigIndex, &GroupCipher);

    nmhGetFsRSNAConfigGroupRekeyMethod ((INT4) u4NextConfigIndex,
                                        (INT4 *) &u4GroupRekeyMethod);

    nmhGetFsRSNAConfigGroupUpdateCount ((INT4) u4NextConfigIndex,
                                        &u4GroupUpdateCount);

    nmhGetFsRSNAConfigPairwiseUpdateCount ((INT4) u4NextConfigIndex,
                                           &u4PairwiseUpdateCount);

    nmhGetFsRSNAConfigGroupCipherSize ((INT4) u4NextConfigIndex,
                                       &u4GroupCipherSize);

    nmhGetFsRSNAConfigPMKReauthThreshold ((INT4) u4NextConfigIndex,
                                          &u4PMKReAuthThreshold);

    nmhGetFsRSNAConfigNumberOfPTKSAReplayCountersImplemented ((INT4)
                                                              u4NextConfigIndex,
                                                              &u4PTKSAReplayCounter);

    nmhGetFsRSNAConfigSATimeout ((INT4) u4NextConfigIndex, &u4SATimeOut);

    nmhGetFsRSNAAuthenticationSuiteSelected ((INT4) u4NextConfigIndex,
                                             &AuthSuiteSelected);

    WpaGetFsRSNAPairwiseCipherSelected ((INT4) u4NextConfigIndex,
                                        &PairwiseCipherSelected);

    nmhGetFsRSNAGroupCipherSelected ((INT4) u4NextConfigIndex,
                                     &GroupwiseCipherSelected);

    WpaGetFsRSNAAuthenticationSuiteRequested ((INT4)
                                              u4NextConfigIndex,
                                              &AuthSuiteRequested);

    WpaGetFsRSNAPairwiseCipherRequested ((INT4) u4NextConfigIndex,
                                         &PairwiseCipherRequested);

    WpaGetFsRSNAGroupCipherRequested ((INT4) u4NextConfigIndex,
                                      &GroupwiseCipherRequested);

    nmhGetFsRSNATKIPCounterMeasuresInvoked ((INT4) u4NextConfigIndex,
                                            &u4TKIPCounterMeasuresInvoked);

    nmhGetFsRSNA4WayHandshakeFailures ((INT4) u4NextConfigIndex,
                                       &u4FourWayHandshakeFailures);
    nmhGetFsRSNAConfigMixMode ((INT4) u4NextConfigIndex, &i4MixMode);

    nmhGetFsRSNAConfigNumberOfGTKSAReplayCountersImplemented ((INT4)
                                                              u4NextConfigIndex,
                                                              &u4GTKSAReplayCountersCount);

    /* Printing the dot11RSNAConfigTable entries */
    if (i4MixMode != MIX_MODE_DISABLED)
    {
        CliPrintf (CliHandle, " \t\t Mix-Mode Enabled\n");
    }

    CliPrintf (CliHandle, " \t\t IfIndex                           : %d\n",
               u4NextConfigIndex);
    CliPrintf (CliHandle, " \t\t RSNAVersion                       : %d\n",
               u4ConfigVersion);
    CliPrintf (CliHandle, " \t\t PairwiseKeySupported              : "
               "%d\n", u4PairwiseKeySupported);

    CliPrintf (CliHandle, " \t\t GroupCipher                       : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               GroupCipher.pu1_OctetList[0],
               GroupCipher.pu1_OctetList[1],
               GroupCipher.pu1_OctetList[2], GroupCipher.pu1_OctetList[3]);

    if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_DISABLED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : " "Disabled\n");
    }
    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEBASED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : " "TimeBased\n");
    }
    else if (u4GroupRekeyMethod == RSNA_GROUP_REKEY_PACKETBASED)
    {
        CliPrintf (CliHandle,
                   " \t\t GroupRekeyMethod                  : "
                   "PacketBased\n");
    }
    CliPrintf (CliHandle, " \t\t GroupUpdateCount                  : %u\n",
               u4GroupUpdateCount);

    CliPrintf (CliHandle, " \t\t Pairwiseupdatecount               : %u\n",
               u4PairwiseUpdateCount);

    CliPrintf (CliHandle, " \t\t GroupCipherSize                   : %d\n",
               u4GroupCipherSize);

    CliPrintf (CliHandle, " \t\t PMKLifeTime                       : %u\n",
               u4PMKLifetime);

    CliPrintf (CliHandle, " \t\t PMKReauthThreshold                : %d\n",
               u4PMKReAuthThreshold);

    CliPrintf (CliHandle, " \t\t PTKSAReplayCounter                : %d\n",
               u4PTKSAReplayCounter);

    CliPrintf (CliHandle, " \t\t SATimeOut                         : %u\n",
               u4SATimeOut);

    CliPrintf (CliHandle, " \t\t AuthenticateSuiteSelected         : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               AuthSuiteSelected.pu1_OctetList[0],
               AuthSuiteSelected.pu1_OctetList[1],
               AuthSuiteSelected.pu1_OctetList[2],
               AuthSuiteSelected.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t PairwiseCipherSelected            : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               PairwiseCipherSelected.pu1_OctetList[0],
               PairwiseCipherSelected.pu1_OctetList[1],
               PairwiseCipherSelected.pu1_OctetList[2],
               PairwiseCipherSelected.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t GroupwiseCipherSelected           : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               GroupwiseCipherSelected.pu1_OctetList[0],
               GroupwiseCipherSelected.pu1_OctetList[1],
               GroupwiseCipherSelected.pu1_OctetList[2],
               GroupwiseCipherSelected.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t AuthenticationSuiteRequested      : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               AuthSuiteRequested.pu1_OctetList[0],
               AuthSuiteRequested.pu1_OctetList[1],
               AuthSuiteRequested.pu1_OctetList[2],
               AuthSuiteRequested.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t PairwiseCipherRequested           : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               PairwiseCipherRequested.pu1_OctetList[0],
               PairwiseCipherRequested.pu1_OctetList[1],
               PairwiseCipherRequested.pu1_OctetList[2],
               PairwiseCipherRequested.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t GroupwiseCipherRequested          : ");
    CliPrintf (CliHandle, "%x:%x:%x:%x\n",
               GroupwiseCipherRequested.pu1_OctetList[0],
               GroupwiseCipherRequested.pu1_OctetList[1],
               GroupwiseCipherRequested.pu1_OctetList[2],
               GroupwiseCipherRequested.pu1_OctetList[3]);

    CliPrintf (CliHandle, " \t\t TKIPCounterMeasuresInvoked        : %d\n",
               u4TKIPCounterMeasuresInvoked);

    CliPrintf (CliHandle, " \t\t 4WayHandshakeFailures             : %d\n",
               u4FourWayHandshakeFailures);

    CliPrintf (CliHandle, " \t\t GTKSAReplayCountersCount          : %d\n",
               u4GTKSAReplayCountersCount);

}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : WpaCliShowWpaCipher                                   */
/*                                                                           */
/* Description     : This function displays the details of pairwise          */
/*                   ciphersuite configured in this entity.                  */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                   u4ProfileId     - WLAN ID                               */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliShowWpaCipher (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4CurrWlanId = 0;
    UINT4               u4NextWlanId = 0;
    UINT4               u4CurrConfigPairwiseCipherIndex = 0;
    UINT4               u4NextRSNAConfigPairwiseCipherIndex = 0;
    UINT4               u4WlanId = 0;
    /* Verify whether to display all WLAN details 
     * or to display particular WLAN detail. */

    u4CurrWlanId = u4ProfileId;

    while (RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable
           ((INT4) u4CurrWlanId, (INT4 *) &u4NextWlanId,
            u4CurrConfigPairwiseCipherIndex,
            &u4NextRSNAConfigPairwiseCipherIndex) == SNMP_SUCCESS)

    {
        u4CurrConfigPairwiseCipherIndex = u4NextRSNAConfigPairwiseCipherIndex;
        u4CurrWlanId = u4NextWlanId;

        /* Assign the next wlanindex and statsindex pointer to current to
         * get the appropriate value from GetnextIndex */

        if (u4ProfileId != 0)
        {
            /* Verify if the entry fetched is for the input given 
             * in this function call */
            if (u4CurrWlanId == u4ProfileId)
            {
                WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
                /* Print the entry from RSNA Stats 
                 * Table for the u4CurrentWlanIndex */
                WpaCliShowWpa1CipherDisplay (CliHandle, u4CurrWlanId,
                                             u4CurrConfigPairwiseCipherIndex,
                                             u4WlanId);
            }
            else
            {
                /* Breaking the loop after printing the appropriate values 
                 * for specific WlanIndex, StatsIndex */
                break;
            }
        }
        else
        {
            WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
            /* Print the entry from RSNA Stats 
             * Table for the u4CurrentWlanIndex */
            WpaCliShowWpa1CipherDisplay (CliHandle, u4CurrWlanId,
                                         u4CurrConfigPairwiseCipherIndex,
                                         u4WlanId);
        }

    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpaCliShowWpa1CipherDisplay                           */
/*                                                                           */
/*  Description     : This function fetches the entries from PairwiseCiphers */
/*                    table and displays it.                                 */
/*                                                                           */
/*  Input(s)        : CliHandle   - CLI Handler                              */
/*                    u4ProfileId - Wlan Interface Index                     */
/*                    u4NextRSNAConfigPairwiseCipherIndex -  PairwiseCipher  */
/*                                                            Index          */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

VOID
WpaCliShowWpa1CipherDisplay (tCliHandle CliHandle, UINT4 u4NextWlanId,
                             UINT4 u4NextRSNAConfigPairwiseCipherIndex,
                             UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE RSNAConfigPairwiseCipher = { NULL, 0 };
    UINT1               au1RSNAConfigCipher[4] = "";
    INT4                i4PairwiseCipherEnabled = 0;
    UINT4               u4RSNAConfigPairwiseCipherIndex = 0;
    INT4                i4WpaEnabled = WPA_DISABLED;
    UNUSED_PARAM (u4RSNAConfigPairwiseCipherIndex);

    MEMSET (au1RSNAConfigCipher, 0, sizeof (au1RSNAConfigCipher));
    RSNAConfigPairwiseCipher.pu1_OctetList = au1RSNAConfigCipher;
    RSNAConfigPairwiseCipher.i4_Length = (INT4) (STRLEN (au1RSNAConfigCipher));

    /* Get the RSNA Pairwise Cipher Table Entries using RsnaGet */

    nmhGetFsRSNAConfigGroupCipher ((INT4) u4NextWlanId,
                                   &RSNAConfigPairwiseCipher);
    nmhGetFsRSNAConfigPairwiseCipherActivated ((INT4) u4NextWlanId,
                                               u4NextRSNAConfigPairwiseCipherIndex,
                                               &i4PairwiseCipherEnabled);
    nmhGetFsRSNAConfigActivated ((INT4) u4NextWlanId, &i4WpaEnabled);
    if (i4WpaEnabled == WPA_ENABLED)
    {

        CliPrintf (CliHandle, "\rWlan Index              : %d\n", u4WlanId);
        CliPrintf (CliHandle, "\rCipher Index            : %d\n",
                   u4NextRSNAConfigPairwiseCipherIndex);
        if (i4PairwiseCipherEnabled == WPA_ENABLED)
        {
            CliPrintf (CliHandle, "\rCipher Used             : %x:%x:%x:%x\n",
                       RSNAConfigPairwiseCipher.pu1_OctetList[0],
                       RSNAConfigPairwiseCipher.pu1_OctetList[1],
                       RSNAConfigPairwiseCipher.pu1_OctetList[2],
                       RSNAConfigPairwiseCipher.pu1_OctetList[3]);

            CliPrintf (CliHandle, "\rPairwise Cipher status  : Enabled\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\rWlan Index              : %d\n", u4WlanId);
        CliPrintf (CliHandle, "\rPairwise Cipher status  : Disabled\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   :WpaCliShowAuthSuiteInfo                                 */
/*                                                                           */
/* Description     : This function displays the authentication suite details */
/*                                                                           */
/* Input(s)        : CliHandle    - CLI Handler                              */
/*                                                                           */
/*                                                                           */
/* Output(s)       : NONE                                                    */
/*                                                                           */
/* Returns         : CLI_SUCCESS/CLI_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
WpaCliShowAuthSuiteInfo (tCliHandle CliHandle, UINT4 u4ProfileId)
{

    UINT4               u4CurrAuthSuiteIndex = 0;
    UINT4               u4NextAuthSuiteIndex = 0;
    UINT4               u4CurrWlanId = 0;
    UINT4               u4NextWlanId = 0;
    UINT4               u4WlanId = 0;

    u4CurrWlanId = u4ProfileId;
    /* Displays all the entries of the 
     * dot11RSNAConfigAuthenticationSuitesTable */
    while (WpaGetNextIndexFsWPAConfigAuthenticationSuitesTable
           ((INT4) u4CurrWlanId, (INT4 *) &u4NextWlanId, u4CurrAuthSuiteIndex,
            &u4NextAuthSuiteIndex) == SNMP_SUCCESS)
    {
        u4CurrAuthSuiteIndex = u4NextAuthSuiteIndex;
        u4CurrWlanId = u4NextWlanId;
        if (u4ProfileId != 0)
        {
            /* Verify if the entry fetched is for the input given
             * in this function call */
            if (u4CurrWlanId == u4ProfileId)
            {
                WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
                /* Print the entry from RSNA Stats
                 * Table for the u4CurrentWlanIndex */
                WpaCliShowAuthSuiteInfoDisplay (CliHandle, u4CurrWlanId,
                                                u4CurrAuthSuiteIndex, u4WlanId);
            }
            else
            {
                /* Breaking the loop after printing the appropriate values
                 * for specific WlanIndex, StatsIndex */
                break;
            }
        }
        else
        {
            WssGetCapwapWlanId (u4CurrWlanId, &u4WlanId);
            /* Print the entry from RSNA Stats
             * Table for the u4CurrentWlanIndex */
            WpaCliShowAuthSuiteInfoDisplay (CliHandle, u4CurrWlanId,
                                            u4CurrAuthSuiteIndex, u4WlanId);
        }
    }

    /* SUNIL- Executes coverity report for unit test details */
    /*RsnaCliShowUnitTestDetailsDisplay(CliHandle); */

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpaCliShowAuthSuiteInfoDisplay                        */
/*                                                                           */
/*  Description     : This function fetches the entries from Authentication  */
/*               suite  table and displaty it                            */
/*                                                                           */
/*  Input(s)        : CliHandle   - CLI Handler                              */
/*                    u4ProfileId - Wlan Interface Index                     */
/*                    u4NextAuthSuiteIndex -  Authentication suit index      */
/*                    u4WlanId                                               */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

VOID
WpaCliShowAuthSuiteInfoDisplay (tCliHandle CliHandle, UINT4 u4NextWlanId,
                                UINT4 u4NextAuthSuiteIndex, UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE AuthenticationSuite = { NULL, 0 };
    UINT4               u4AuthSuiteStatus = 0;
    UINT1               au1AuthSuitetype[RSNA_MAX_AUTH_SUITE_TYPE_LENGTH] = "";

    MEMSET (au1AuthSuitetype, 0, sizeof (au1AuthSuitetype));
    AuthenticationSuite.pu1_OctetList = au1AuthSuitetype;
    AuthenticationSuite.i4_Length = (INT4) (STRLEN (au1AuthSuitetype));

    /* Get the RSNA Authentication Suite Table Entries using RsnaGet */
    RsnaGetFsRSNAConfigAuthenticationSuite ((UINT4) u4NextWlanId,
                                            u4NextAuthSuiteIndex,
                                            &AuthenticationSuite);
    WpaGetFsRSNAConfigAuthenticationSuiteEnabled ((INT4) u4NextWlanId,
                                                  u4NextAuthSuiteIndex,
                                                  (INT4 *) &u4AuthSuiteStatus);

    CliPrintf (CliHandle, "\rWlan Index              : %d\n", u4WlanId);
    if (u4NextAuthSuiteIndex == RSNA_AUTHSUITE_PSK)
    {
        CliPrintf (CliHandle, "AuthenticationSuiteType " "  :  PSK\n");
    }
    else if (u4NextAuthSuiteIndex == RSNA_AUTHSUITE_8021X)
    {
        CliPrintf (CliHandle, "AuthenticationSuiteType" "   :  8021X\n");
    }
    CliPrintf (CliHandle,
               "AuthenticationSuite       :  %x:%x:%x:%x\n ",
               AuthenticationSuite.pu1_OctetList[0],
               AuthenticationSuite.pu1_OctetList[1],
               AuthenticationSuite.pu1_OctetList[2],
               AuthenticationSuite.pu1_OctetList[3]);
    if (u4AuthSuiteStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "AuthenticationSuiteStatus :" "  Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "AuthenticationSuiteStatus :" "  Disabled\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpaShowRunningConfigTables                             */
/*                                                                           */
/*  Description     : This function displays the current WLAN configuration  */
/*                    for all dot11 Rsna tables                              */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
WpaShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ProfileId)
{
    UINT4               u4CurrentWlanIndex = 0;
    UINT4               u4NextWlanIndex = 0;
    UINT4               u4CurrentConfigPairwiseCipherIndex = 0;
    UINT4               u4NextConfigPairwiseCipherIndex = 0;
    UINT4               u4CurrentAuthSuiteIndex = 0;
    UINT4               u4NextAuthSuiteIndex = 0;
    UINT4               u4TableWalk = 1;
    UINT4               u4WlanId = 0;
    INT4                i4WpaEnabled = WPA_DISABLED;

    /* DOT11 PRIVACY TABLE */
    u4CurrentWlanIndex = u4ProfileId;
    /* Loop through the FsRsnaConfig Table to fetch WPA Configurations */
    do
    {
        if (u4ProfileId != 0)
        {
            u4TableWalk = 0;
        }
        else
        {
            /* Fetches the next WlanIndex from the Dot11RsnaConfig Table
             * by Passing the current WlanIndex */
            if (nmhGetNextIndexFsRSNAConfigTable ((INT4) u4CurrentWlanIndex,
                                                  (INT4 *) &u4NextWlanIndex)
                != SNMP_SUCCESS)
            {
                /* Breaking the loop after printing 
                 * the appropriate values
                 * for specific WlanIndex */
                break;
            }

            /* Assign the next wlanindex pointer to current to
             * get the appropriate value from GetnextIndex */
            u4CurrentWlanIndex = u4NextWlanIndex;
        }

        WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

        /* Show the commands in show running config, only when RSNA is enabled */
        i4WpaEnabled = WPA_DISABLED;
        WpaGetFsRSNAEnabled (u4CurrentWlanIndex, &i4WpaEnabled);

        if (i4WpaEnabled != WPA_ENABLED)
        {
            continue;
        }

        /* Fetches entry from the Dot11 Rsna Config 
         * Table for the u4CurrentWlanIndex */
        WpaShowRunningConfigTableDisplay (CliHandle, u4CurrentWlanIndex,
                                          u4WlanId);
    }
    while (u4TableWalk != 0);

    /* DOT11 RSNA CONFIG PAIRWISE CIPHER TABLE */
    u4CurrentWlanIndex = u4ProfileId;
    u4CurrentConfigPairwiseCipherIndex = 0;

    /* Loop through the Dot11RsnaConfigPairwiseCiphers 
     * Table to fetch RSNA Configurations */
    while (nmhGetNextIndexFsRSNAConfigPairwiseCiphersTable
           ((INT4) u4CurrentWlanIndex, (INT4 *) &u4NextWlanIndex,
            u4CurrentConfigPairwiseCipherIndex,
            &u4NextConfigPairwiseCipherIndex) == SNMP_SUCCESS)
    {
        /* Assign the next wlanindex pointer to current to 
         * get the appropriate value from GetnextIndex */

        u4CurrentWlanIndex = u4NextWlanIndex;
        u4CurrentConfigPairwiseCipherIndex = u4NextConfigPairwiseCipherIndex;

        if (u4ProfileId != 0)
        {
            /* Verify if the entry fetched is for the input given 
             * in this function call */
            if (u4CurrentWlanIndex == u4ProfileId)
            {
                WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

                /* Show the commands in show running config, */
                /* only when RSNA is enabled */
                i4WpaEnabled = WPA_DISABLED;
                WpaGetFsRSNAEnabled (u4CurrentWlanIndex, &i4WpaEnabled);
                if (i4WpaEnabled == WPA_DISABLED)
                {
                    continue;
                }

                /* Print the entry from Dot11RsnaConfigPairwiseCiphers 
                 * Table for the u4CurrentWlanIndex */
                WpaShowRunningConfigCipherTableDisplay (CliHandle,
                                                        u4CurrentWlanIndex,
                                                        u4CurrentConfigPairwiseCipherIndex,
                                                        u4WlanId);
            }
            else
            {
                /* Breaking the loop after printing the appropriate values
                 * for specific WlanIndex */
                break;
            }
        }
        else
        {
            WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);

            /* Show the commands in show running config, */
            /* only when RSNA is enabled */
            i4WpaEnabled = WPA_DISABLED;
            WpaGetFsRSNAEnabled (u4CurrentWlanIndex, &i4WpaEnabled);
            if (i4WpaEnabled == WPA_DISABLED)
            {
                continue;
            }

            /* Print the entry from Dot11RsnaConfigPairwiseCiphers 
             * Table for the u4CurrentWlanIndex */
            WpaShowRunningConfigCipherTableDisplay (CliHandle,
                                                    u4CurrentWlanIndex,
                                                    u4CurrentConfigPairwiseCipherIndex,
                                                    u4WlanId);
        }
    }

    /* DOT11 AUTHENTICATION SUITES TABLE */
    u4CurrentAuthSuiteIndex = 0;
    if (nmhGetFirstIndexFsWPAConfigAuthenticationSuitesTable
        ((INT4 *) &u4NextWlanIndex, &u4NextAuthSuiteIndex) == SNMP_SUCCESS)
    {
        /* Loop through the FsRsnaConfigAuthenticationSuites Table to 
         * fetch RSNA Configurations */
        /*If the variable is enabled,then AuthSuiteTable will be displayed */
        u4CurrentWlanIndex = u4NextWlanIndex;
        while (nmhGetNextIndexFsWPAConfigAuthenticationSuitesTable
               ((INT4) u4CurrentWlanIndex, (INT4 *) &u4NextWlanIndex,
                u4CurrentAuthSuiteIndex, &u4NextAuthSuiteIndex) == SNMP_SUCCESS)

        {
            /* Assign the next wlanindex pointer to current to 
             * get the appropriate value from GetnextIndex */

            WssGetCapwapWlanId (u4CurrentWlanIndex, &u4WlanId);
            u4CurrentAuthSuiteIndex = u4NextAuthSuiteIndex;
            u4CurrentWlanIndex = u4NextWlanIndex;
            /* Print the entry from Dot11RsnaConfigAuthenticationSuites 
             * Table for the u4CurrentWlanIndex */
            WpaShowRunningConfigAuthSuiteTableDisplay (CliHandle,
                                                       u4CurrentWlanIndex,
                                                       u4CurrentAuthSuiteIndex,
                                                       u4WlanId);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpaShowRunningConfigTableDisplay                      */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11RsnaConfig Table                              */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
WpaShowRunningConfigTableDisplay (tCliHandle CliHandle,
                                  UINT4 u4ProfileId, UINT4 u4WlanId)
{
    tSNMP_OCTET_STRING_TYPE GroupCipher = { NULL, 0 };
    UINT1               au1GroupCipher[RSNA_MAX_CIPHER_TYPE_LENGTH] = "";
    UINT4               u4GroupRekeyMethod = 0;
    UINT4               u4GroupRekeyTime = 0;
    UINT4               u4GroupUpdateCount = 0;
    UINT4               u4PairwiseUpdateCount = 0;
    UINT4               u4PMKLifetime = 0;
    UINT4               u4PMKReAuthThreshold = 0;
    UINT4               u4SATimeOut = 0;
    UINT1               u1RsnaOkcStatus = 0;

    MEMSET (au1GroupCipher, '\0', RSNA_MAX_CIPHER_TYPE_LENGTH);

    GroupCipher.pu1_OctetList = au1GroupCipher;

    /* Fetch and display the Dot11RsnaConfig Table Entries using RsnaGet */
    WpaGetFsRSNAConfigGroupCipher ((INT4) u4ProfileId, &GroupCipher);

    if ((MEMCMP (GroupCipher.pu1_OctetList, gau1WpaCipherSuiteCCMP,
                 RSNA_MAX_CIPHER_TYPE_LENGTH) == 0))
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa "
                   "groupwise ciphers aes %d", u4WlanId);

    }
    else if ((MEMCMP (GroupCipher.pu1_OctetList, gau1WpaCipherSuiteTKIP,
                      RSNA_MAX_CIPHER_TYPE_LENGTH) == 0))
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa "
                   "groupwise ciphers tkip %d", u4WlanId);

    }

    nmhGetFsRSNAConfigGroupRekeyMethod ((INT4) u4ProfileId,
                                        (INT4 *) &u4GroupRekeyMethod);

    switch (u4GroupRekeyMethod)
    {

        case RSNA_GROUP_REKEY_DISABLED:
            CliPrintf (CliHandle, "\r\n config wlan security wpa group "
                       "rekey method disabled %d", u4WlanId);
            break;

        case RSNA_GROUP_REKEY_PACKETBASED:
            CliPrintf (CliHandle, "\r\n config wlan security wpa group "
                       "rekey method packetBased %d", u4WlanId);
            break;
        case RSNA_GROUP_REKEY_TIMEPACKETBASED:
            CliPrintf (CliHandle, "\r\n config wlan security wpa group "
                       "rekey method timepacketBased %d", u4WlanId);
            break;
        default:
            break;
    }

    nmhGetFsRSNAConfigGroupRekeyTime ((INT4) u4ProfileId, &u4GroupRekeyTime);
    if (u4GroupRekeyTime != RSNA_DEF_GROUP_REKEY_TIME)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa gtk rekey "
                   "interval %d %d", u4GroupRekeyTime, u4WlanId);
    }
    nmhGetFsRSNAConfigGroupUpdateCount ((INT4) u4ProfileId,
                                        &u4GroupUpdateCount);
    /* Since Defval for u4GroupUpdateCount equals "3", it need not be printed */
    if (u4GroupUpdateCount != RSNA_DEF_UPDATE_COUNT)
    {
        CliPrintf (CliHandle,
                   "\r\n config wlan security wpa group handshake "
                   "retry count %d %d", u4GroupUpdateCount, u4WlanId);
    }

    nmhGetFsRSNAConfigPairwiseUpdateCount ((INT4) u4ProfileId,
                                           &u4PairwiseUpdateCount);
    if (u4GroupUpdateCount != RSNA_DEF_UPDATE_COUNT)
    {
        CliPrintf (CliHandle,
                   "\r\n config wlan security wpa pairwise-handshake"
                   "-retry-count %d %d", u4GroupUpdateCount, u4WlanId);
    }

    nmhGetFsRSNAConfigPMKLifetime ((INT4) u4ProfileId, &u4PMKLifetime);
    /* Since Defval for u4PMKLifetime equals "43200", it need not be printed */
    if (u4PMKLifetime != RSNA_DEF_PMK_LIFETIME)
    {

        CliPrintf (CliHandle, "\r\n config wlan security wpa "
                   " pmk-life-time %d %d", u4PMKLifetime, u4WlanId);
    }

    nmhGetFsRSNAConfigPMKReauthThreshold ((INT4) u4ProfileId,
                                          &u4PMKReAuthThreshold);

    /* Since Defval for u4PMKReAuthThreshold equals "70", */
    /* it need not be printed */
    if (u4PMKReAuthThreshold != RSNA_DEF_PMK_REAUTH_THRESHOLD)
    {
        CliPrintf (CliHandle,
                   "\r\n config wlan security wpa pmk-reauth-threshold"
                   " %d %d", u4PMKReAuthThreshold, u4WlanId);
    }

    nmhGetFsRSNAConfigSATimeout ((INT4) u4ProfileId, &u4SATimeOut);

    /* Since Defval for u4SATimeOut equals "60", it need not be printed */
    if (u4SATimeOut != RSNA_DEF_SA_TIME_OUT)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa sa-timeout"
                   " %d %d", u4SATimeOut, u4WlanId);
    }

    nmhGetFsRSNAOKCEnabled ((INT4) u4ProfileId, (INT4 *) &u1RsnaOkcStatus);

    if (u1RsnaOkcStatus == RSNA_ENABLED)
    {
        CliPrintf (CliHandle, "\r\n config wlan security wpa okc "
                   "enable %d", u4WlanId);

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpaShowRunningConfigCipherTableDisplay                */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11RsnaConfigPairwiseCiphersTable                */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
WpaShowRunningConfigCipherTableDisplay (tCliHandle CliHandle,
                                        UINT4 u4ProfileId,
                                        UINT4 u4RSNAConfigPairwiseCipherIndex,
                                        UINT4 u4WlanId)
{
    UINT4               u4PairwiseCipherEnabled = WPA_DISABLED;
    INT4                i4WpaEnabled = WPA_DISABLED;
    tSNMP_OCTET_STRING_TYPE PairwiseCipher = { NULL, 0 };
    UINT1               au1PairwiseCipherRequested[RSNA_MAX_CIPHER_TYPE_LENGTH]
        = "";
    PairwiseCipher.pu1_OctetList = au1PairwiseCipherRequested;

    /* Fetch and display the Dot11RsnaConfigPairwiseCiphers 
     * Table Entries using RsnaGet */
    nmhGetFsRSNAConfigPairwiseCipherActivated ((INT4) u4ProfileId,
                                               u4RSNAConfigPairwiseCipherIndex,
                                               (INT4 *)
                                               &u4PairwiseCipherEnabled);
    nmhGetFsRSNAConfigGroupCipher ((INT4) u4ProfileId, &PairwiseCipher);
    nmhGetFsRSNAConfigActivated ((INT4) u4ProfileId, &i4WpaEnabled);
    if (i4WpaEnabled != WPA_ENABLED)
    {
        return CLI_FAILURE;
    }

    if (u4PairwiseCipherEnabled == WPA_ENABLED)
    {
        if (MEMCMP
            (PairwiseCipher.pu1_OctetList, gau1WpaCipherSuiteCCMP,
             RSNA_MAX_CIPHER_TYPE_LENGTH) == 0)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa pairwise "
                       "aes enable %d", u4WlanId);
        }
        else if (MEMCMP
                 (PairwiseCipher.pu1_OctetList, gau1WpaCipherSuiteTKIP,
                  RSNA_MAX_CIPHER_TYPE_LENGTH) == 0)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa pairwise "
                       "tkip enable %d", u4WlanId);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*  Function Name   : WpaShowRunningConfigAuthSuiteTableDisplay             */
/*                                                                           */
/*  Description     : This function displays current WLAN configuration      */
/*                    for dot11RsnaConfigAuthenticationSuitesTable           */
/*                                                                           */
/*  Input(s)        : CliHandle   - Handle to the CLI Context                */
/*                    u4ProfileId - Specified Wlan Interface Index           */
/*                                                                           */
/*  Output(s)       : NONE                                                   */
/*                                                                           */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

INT4
WpaShowRunningConfigAuthSuiteTableDisplay (tCliHandle CliHandle,
                                           UINT4 u4NextWlanIndex,
                                           UINT4 u4AuthSuiteIndex,
                                           UINT4 u4WlanId)
{
    INT4                i4AuthSuiteEnabled = WPA_DISABLED;

    /* Fetch and display the Dot11RsnaConfigPairwiseCiphers Table 
     * Entries using RsnaGet */
    WpaGetFsRSNAEnabled (u4NextWlanIndex, &i4AuthSuiteEnabled);
    nmhGetFsRSNAConfigAuthenticationSuiteActivated (u4NextWlanIndex,
                                                    u4AuthSuiteIndex,
                                                    &i4AuthSuiteEnabled);
    if (i4AuthSuiteEnabled == WPA_ENABLED)
    {
        if (u4AuthSuiteIndex == RSNA_AUTHSUITE_PSK)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa "
                       "akm psk enable %d", u4WlanId);
        }
    }
    else if (i4AuthSuiteEnabled == WPA_ENABLED)
    {
        if (u4AuthSuiteIndex == RSNA_AUTHSUITE_8021X)
        {
            CliPrintf (CliHandle, "\r\n config wlan security wpa "
                       "akm 8021x enable %d", u4WlanId);
        }
    }

    return CLI_SUCCESS;
}

#endif

#endif /* RSNACLI_C */
