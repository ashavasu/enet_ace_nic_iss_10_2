/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnasess.c,v 1.4 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"

/****************************************************************************
*                                                                           *
* Function     : RsnaSessCreateAndInitSession                               *
*                                                                           *
* Description  : Function used to initialize the rsna session structure     *
*                                                                           *
* Input        : pRsnaPaePortEntry :- points to rsna port info              *
*                pAuthSessNode :- Points to auth session node               *
*                pu1Pkt :- Points to the packet recvd                       *
*                u2PktLen :- Length of the pkt recvd                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
tRsnaSessionNode   *
RsnaSessCreateAndInitSession (tRsnaPaePortEntry * pRsnaPaePortEntry,
                              tPnacAuthSessionNode * pAuthSessNode,
                              UINT1 *pu1Pkt, UINT2 u2PktLen, UINT4 u4BssIfIndex,
                              UINT1 *pu1PmkId, BOOL1 bRsnaInitStateMachine)
{

    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT1               u1type = 0;
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;
    tRsnaSessionNode   *pRsnaSessNode = NULL;
    tRsnaPmkSa         *pRsnaPmksa = NULL;
    UINT1               au1NullBssid[RSNA_ETH_ADDR_LEN];
    tCfaIfInfo          IfInfo;
    UINT1               au1TempCipherTypePSK[RSNA_CIPHER_SUITE_LEN];
    UINT1               au1TempCipherTypeSHA256[RSNA_CIPHER_SUITE_LEN];
    MEMSET (au1TempCipherTypePSK, 0, RSNA_CIPHER_SUITE_LEN);
    MEMSET (au1TempCipherTypeSHA256, 0, RSNA_CIPHER_SUITE_LEN);
    MEMSET (au1NullBssid, 0, RSNA_ETH_ADDR_LEN);

    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   "Creating Session for Station MAC:%x:%x:%x:%x:%x:%x\n",
                   pAuthSessNode->rmtSuppMacAddr[0],
                   pAuthSessNode->rmtSuppMacAddr[1],
                   pAuthSessNode->rmtSuppMacAddr[2],
                   pAuthSessNode->rmtSuppMacAddr[3],
                   pAuthSessNode->rmtSuppMacAddr[4],
                   pAuthSessNode->rmtSuppMacAddr[5]);

    pRsnaSessNode = (tRsnaSessionNode *) pAuthSessNode->pRsnaSessionNode;

    if (pRsnaSessNode == NULL)
    {
        if ((pRsnaSessNode =
             RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_AUTHSESSNODE_MEMPOOL_ID)) == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaSessCreateAndInitSession:-"
                      "MemAllocMemBlk Failed !!!!! \n");
            return (NULL);
        }
    }

    MEMSET (pRsnaSessNode, 0, sizeof (tRsnaSessionNode));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pRsnaSessNode->u4RefCount++;
    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortEntry,
                                                  (UINT1 *) &(pAuthSessNode->
                                                              rmtSuppMacAddr));

    if (pRsnaSupInfo == NULL)
    {

        if (RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr)) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaSessCreateAndInitSession:- RsnaTmrStopTimer Failed (1) !!!! \n");
        }

        RSNA_MEM_RELEASE_MEM_BLK (RSNA_AUTHSESSNODE_MEMPOOL_ID, pRsnaSessNode);
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessCreateAndInitSession :- "
                  "RsnaUtilGetStaInfoFromStaAddr returned NULL !!!!! \n");
        return (NULL);
    }

    pRsnaSessNode->pRsnaSupInfo = pRsnaSupInfo;
    pRsnaSessNode->pRsnaSupInfo->pRsnaSessNode = pRsnaSessNode;
    pRsnaSessNode->pPnacAuthSessionNode = pAuthSessNode;
    pRsnaSessNode->pRsnaPaePortInfo = pRsnaPaePortEntry;
    pRsnaSessNode->pu1RxEapolKeyPkt = pu1Pkt;
    pRsnaSessNode->u2PktLen = u2PktLen;
    pRsnaSessNode->u2Port = pRsnaPaePortEntry->u2Port;
    /* Store the binding interface index */
    pRsnaSessNode->u4BssIfIndex = u4BssIfIndex;
    pAuthSessNode->pRsnaSessionNode = pRsnaSessNode;
    pRsnaSessNode->u1ElemId = pRsnaSupInfo->au1RsnaIE[0];
    u1type = pRsnaSupInfo->au1RsnaIE[0];

    pRsnaSuppStatsInfo =
        RsnaUtilGetStationFromStatsTable (pRsnaPaePortEntry->u2Port,
                                          pRsnaSessNode->pRsnaSupInfo->
                                          au1SuppMacAddr);

    if (pRsnaSuppStatsInfo == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessCreateAndInitSession :- "
                  "RsnaUtilGetStationFromStatsTable returned NULL !!!!! \n");
        return NULL;
    }

    MEMCPY (pRsnaSuppStatsInfo->au1PairwiseCipher,
            pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
            RSNA_CIPHER_SUITE_LEN);

    CfaGetIfInfo (u4BssIfIndex, &IfInfo);

    MEMCPY (pRsnaSessNode->au1BssMac, IfInfo.au1MacAddr, RSNA_ETH_ADDR_LEN);
    if (u1type == RSNA_IE_ID)
    {
        MEMCPY (au1TempCipherTypePSK, RSNA_AKM_PSK_OVER_802_1X,
                RSNA_CIPHER_SUITE_LEN);
    }
#ifdef WPA_WANTED
    else if (u1type == WPA_IE_ID)
    {
        MEMCPY (au1TempCipherTypePSK, WPA_AKM_PSK_OVER_802_1X,
                RSNA_CIPHER_SUITE_LEN);
    }
#endif
    if (((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                  au1TempCipherTypePSK, RSNA_CIPHER_SUITE_LEN)) == 0)
#ifdef PMF_WANTED
        || ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                     au1TempCipherTypeSHA256, RSNA_CIPHER_SUITE_LEN)) == 0)
#endif
        )
    {
        if (u1type == RSNA_IE_ID)
        {
            MEMCPY (pRsnaSessNode->au1Pmk,
                    pRsnaPaePortEntry->RsnaConfigDB.au1ConfigPsk, RSNA_PMK_LEN);
        }
#ifdef WPA_WANTED
        else if (u1type == WPA_IE_ID)
        {
            MEMCPY (pRsnaSessNode->au1Pmk,
                    pRsnaPaePortEntry->WpaConfigDB.au1ConfigPsk, RSNA_PMK_LEN);
        }
#endif
    }
    else
    {
        if (pRsnaPaePortEntry->RsnaConfigDB.u1RsnaOkcStatus == RSNA_ENABLED)
        {
            pRsnaPmksa =
                RsnaUtilGetPmkSaFromCache (pRsnaSessNode->pRsnaSupInfo->
                                           au1SuppMacAddr, au1NullBssid);
        }
        else
        {
            pRsnaPmksa =
                RsnaUtilGetPmkSaFromCache (pRsnaSessNode->pRsnaSupInfo->
                                           au1SuppMacAddr,
                                           pRsnaSessNode->au1BssMac);

        }
        if (pRsnaPmksa != NULL)
        {
            MEMCPY (pRsnaSessNode->au1Pmk, pRsnaPmksa->au1Pmk, RSNA_PMK_LEN);
        }

    }

    MEMCPY (pRsnaSessNode->au1PmkId, pu1PmkId, RSNA_PMKID_LEN);

    pRsnaSessNode->bInit = TRUE;
    pRsnaSessNode->bRsnaInitStateMachine = bRsnaInitStateMachine;
    if (RsnaMainFsmStart (pRsnaSessNode) != RSNA_SUCCESS)
    {

        if (RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr)) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaSessCreateAndInitSession:- RsnaTmrStopTimer Failed (2) !!!! \n");
        }

        MEMSET (pRsnaSessNode, 0, sizeof (tRsnaSessionNode));
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_AUTHSESSNODE_MEMPOOL_ID, pRsnaSessNode);
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessCreateAndInitSession:- "
                  "RsnaMainFsmStart Failed !!!! \n");
        return (NULL);
    }

    pRsnaSessNode->bInit = FALSE;
    pRsnaSessNode->bAuthenticationRequest = TRUE;
    if (RsnaMainFsmStart (pRsnaSessNode) != RSNA_SUCCESS)
    {

        if (RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr)) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaSessCreateAndInitSession:- RsnaTmrStopTimer Failed (3) !!!! \n");
        }

        MEMSET (pRsnaSessNode, 0, sizeof (tRsnaSessionNode));
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_AUTHSESSNODE_MEMPOOL_ID, pRsnaSessNode);
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessCreateAndInitSession:- "
                  "RsnaMainFsmStart Failed !!!! \n");

        return (NULL);
    }
    return (pRsnaSessNode);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessCreatePnacAuthSess                                 *
*                                                                           *
* Description  : Function to create pnac auth session                       *
*                                                                           *
* Input        : srcAddr :- Refers to the source address                    *
*                u2PortNum :- Port number on which the pkt recvd            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

tPnacAuthSessionNode *
RsnaSessCreatePnacAuthSess (UINT1 *pu1SrcAddr,
                            UINT2 u2PortNum, BOOL1 bTriggerPnacStateMachine)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPortInfo = NULL;
    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    UINT4               u4Index = 0;
    tMacAddr            staMacAddr;

    MEMSET (staMacAddr, 0, RSNA_ETH_ADDR_LEN);

    if (PnacGetPortEntry (u2PortNum, &pPortInfo) != PNAC_SUCCESS)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreateSession: Invalid Port Number \n");
        return (NULL);
    }
    if (PNAC_ALLOCATE_AUTHSESSNODE_MEMBLK (pAuthSessNode) == NULL)
    {
        PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                  " CreateSession: Allocate auth sess node failed \n");
        return (NULL);
    }
    PNAC_MEMSET (pAuthSessNode, PNAC_INIT_VAL, sizeof (tPnacAuthSessionNode));
    PnacAuthSessionNodeInit (pAuthSessNode, u2PortNum);
    MEMCPY (pAuthSessNode->rmtSuppMacAddr, pu1SrcAddr, RSNA_ETH_ADDR_LEN);
    pAuthFsmInfo = &(pAuthSessNode->authFsmInfo);
    PnacAuthCalcSessionTblIndex (pAuthSessNode->rmtSuppMacAddr, &u4Index);
    (pPortInfo->u2SuppCount)++;
    pPortInfo->u1PortAuthMode = PNAC_PORT_AUTHMODE_MACBASED;

    pAuthFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    pAuthSessNode->u1IsAuthorizedOnce = PNAC_FALSE;
    pAuthFsmInfo->bKeyRun = TRUE;
    pAuthFsmInfo->bKeyAvailable = TRUE;
    pAuthFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_UNAUTHORIZED;
    pAuthFsmInfo->u2AuthPaeState = PNAC_ASM_STATE_AUTHENTICATED;
    pAuthFsmInfo->u2PortMode = PNAC_PORT_AUTHMODE_MACBASED;

    if (bTriggerPnacStateMachine == TRUE)
    {
        if (PnacAuthAddToAuthInProgressTbl (pAuthSessNode) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: Adding Node in AIP Table failed\n");
            MEMCPY (staMacAddr, pAuthSessNode->rmtSuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            PnacAuthDeleteSession (pAuthSessNode);
            RsnaPnacDeleteSession (staMacAddr);
            return (NULL);
        }

        if (PnacAuthStateMachine (PNAC_ASM_EV_INITIALIZE,
                                  u2PortNum,
                                  pAuthSessNode, NULL,
                                  PNAC_NO_VAL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: ASM returned failure\n");
            MEMCPY (staMacAddr, pAuthSessNode->rmtSuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            PnacAuthDeleteSession (pAuthSessNode);
            RsnaPnacDeleteSession (staMacAddr);
            return (NULL);
        }
        if (PnacAuthBackendStateMachine (PNAC_BSM_EV_INITIALIZE,
                                         u2PortNum, pAuthSessNode,
                                         NULL, PNAC_NO_VAL,
                                         NULL) != PNAC_SUCCESS)
        {
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      " CreateSession: BSM returned failure\n");
            MEMCPY (staMacAddr, pAuthSessNode->rmtSuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            PnacAuthDeleteSession (pAuthSessNode);
            RsnaPnacDeleteSession (staMacAddr);
            return (NULL);
        }
    }
    else
    {
        PNAC_HASH_ADD_NODE (PNAC_AUTH_SESSION_TABLE,
                            pAuthSessNode, u4Index, NULL);
        if (PnacL2IwfSetSuppMacAuthStatus
            (pAuthSessNode->rmtSuppMacAddr,
             PNAC_PORTSTATUS_AUTHORIZED) != PNAC_SUCCESS)
        {
            PNAC_HASH_DEL_NODE (PNAC_AUTH_SESSION_TABLE, pAuthSessNode,
                                u4Index);
            PNAC_TRC (PNAC_CONTROL_PATH_TRC | PNAC_ALL_FAILURE_TRC,
                      "Set in L2Iwf returned failure \n");
            return NULL;
        }
        pAuthFsmInfo->u2AuthBackendAuthState = PNAC_BSM_STATE_IDLE;
        pAuthFsmInfo->u2AuthControlPortStatus = PNAC_PORTSTATUS_AUTHORIZED;
    }
    return (pAuthSessNode);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessDeleteSession                                      *
*                                                                           *
* Description  : Function to delete rsna session                            *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessDeleteSession (tRsnaSessionNode * pRsnaSessNode)
{
    tPnacAuthSessionNode *pPnacAuthSessNode = NULL;
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;
    UINT4               u4Count = 0;
    tMacAddr            staMacAddr;

    MEMSET (staMacAddr, 0, RSNA_ETH_ADDR_LEN);

    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   "Deleting Session for Station MAC:%x:%x:%x:%x:%x:%x\n",
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[0],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[1],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[2],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[3],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[4],
                   pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr[5]);

    if (RsnaTmrStopTimer (&(pRsnaSessNode->RsnaReTransTmr)) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessDeleteSession:- RsnaTmrStopTimer Failed !!!! \n");
        return (RSNA_FAILURE);
    }

    if (PnacSnmpLowGetAuthSessionNode
        (pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,
         &pPnacAuthSessNode) == PNAC_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessDeleteSession:- pPnacAuthSessNode is null !!!! \n");
    }

    pRsnaSuppStatsInfo =
        RsnaUtilGetStationFromStatsTable (pRsnaSessNode->pRsnaPaePortInfo->
                                          u2Port,
                                          pRsnaSessNode->pRsnaSupInfo->
                                          au1SuppMacAddr);

    if (pRsnaSuppStatsInfo != NULL)
    {
        if ((pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipICVErrors == 0) &&
            (pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipLocalMicFailures == 0) &&
            (pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipRemoteMicFailures == 0) &&
            (pRsnaSuppStatsInfo->RsnaStatsDB.u4CCMReplays == 0) &&
            (pRsnaSuppStatsInfo->RsnaStatsDB.u4CCMDecryptErrors == 0) &&
            (pRsnaSuppStatsInfo->RsnaStatsDB.u4TkipReplays == 0))
        {
            RsnaUtilDeleteSuppStatsInfo (pRsnaSuppStatsInfo);
        }
    }

    TMO_SLL_Delete (&(pRsnaSessNode->pRsnaPaePortInfo->RsnaSupInfoList),
                    (tTMO_SLL_NODE *) pRsnaSessNode->pRsnaSupInfo);
    if (RSNA_MEM_RELEASE_MEM_BLK (RSNA_SUPP_INFO_MEMPOOL_ID,
                                  pRsnaSessNode->pRsnaSupInfo) == MEM_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                  "Failed !!!! \n");
        return (RSNA_FAILURE);
    }

    u4Count = TMO_SLL_Count (&(pRsnaSessNode->pRsnaPaePortInfo->
                               RsnaSupInfoList));
    if (u4Count == 0)
    {
#if 0

        /* This delete flag is used to differentiate between the installation 
         * and deletion of GTK. If the flag is set, GTK key deletion will happen*/
        pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.bKeyDeleteFlag = TRUE;

        /* If all the stations are disconnected, send a update WLAN message to
         * delete the existing GTK */
        WssSendUpdateWlanMsgfromRsna (pRsnaSessNode->pRsnaPaePortInfo->u2Port);

        /* When all the stations are disconnected , set the GTK install flag to false
         * so that when the new station again comes for connection, GTK has to be 
         * installed */
        pRsnaSessNode->pRsnaPaePortInfo->bGtkInstallFlag = FALSE;
#endif

        /* Reset the Gtk refresh flag as false, as all the */
        /* session nodes are deleted */
        pRsnaSessNode->pRsnaPaePortInfo->bGtkRefreshFlag = FALSE;

    }

    pRsnaSessNode->pRsnaSupInfo = NULL;
    MEMSET (pRsnaSessNode, 0, sizeof (tRsnaSessionNode));
    if (RSNA_MEM_RELEASE_MEM_BLK (RSNA_AUTHSESSNODE_MEMPOOL_ID,
                                  pRsnaSessNode) == MEM_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessDeleteSession:- "
                  "RSNA_RELEASE_SESS_NODE Failed !!!! \n");
        return (RSNA_FAILURE);
    }

    if (pPnacAuthSessNode != NULL)
    {
        MEMCPY (staMacAddr, pPnacAuthSessNode->rmtSuppMacAddr,
                RSNA_ETH_ADDR_LEN);
        RsnaPnacDeleteSession (staMacAddr);
        PnacAuthDeleteSession (pPnacAuthSessNode);
    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessReqNewPtk                                          *
*                                                                           *
* Description  : Function invoked when it recieves a ptk req pkt from sta   *
*                                                                           *
* Input        : pRsnaSessionNode :- Pointer to rsna session node           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessReqNewPtk (tRsnaSessionNode * pRsnaSessNode)
{
    pRsnaSessNode->bPTKRequest = TRUE;
    pRsnaSessNode->bPtkValid = FALSE;
    return (RSNA_SUCCESS);

}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessDisconnectSta                                      *
*                                                                           *
* Description  : Function invoked to disconnect the station                 *
*                                                                           *
* Input        : pRsnaSessionNode :- Pointer to rsna session node           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessDisconnectSta (tRsnaSessionNode * pRsnaSessNode)
{

    tWssRSNANotifyParams *pWssRSNANotifyParams = NULL;

    if (pRsnaSessNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "rsna session entry failed returned Failure !!!! \n");
        return RSNA_FAILURE;
    }
    if ((pWssRSNANotifyParams =
         (tWssRSNANotifyParams *)
         RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID)) == NULL)

    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaSessDisconnectSta:- "
                  "RSNA_MEM_ALLOCATE_MEM_BLK Failed \n");
        return (RSNA_FAILURE);
    }

    pWssRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DEAUTH_REQ;
    pWssRSNANotifyParams->u4IfIndex = pRsnaSessNode->u4BssIfIndex;
    if (pRsnaSessNode != NULL)
    {
        MEMCPY (pWssRSNANotifyParams->MLMEDEAUTHREQ.au1StaMac,
                pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
        pWssRSNANotifyParams->MLMEDEAUTHREQ.u2reasonCode =
            (UINT2) pRsnaSessNode->u4ReasonCode;
        pRsnaSessNode->bDeAuthReq = TRUE;
        RsnaMainFsmStart (pRsnaSessNode);
        pRsnaSessNode->u4RefCount--;
    }
    if (RsnaProcessWssNotification (pWssRSNANotifyParams) != OSIX_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessDisconnectSta:- RsnaProcessWssNotification"
                  "returned Failure !!!! \n");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                  pWssRSNANotifyParams);
        return (RSNA_FAILURE);
    }
    /* Call to delete the PTK key */
    WssStaDeleteRsnaSessionKey (pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,
                                pRsnaSessNode->u4BssIfIndex);

    /* Delete the RSNA session, after sending the deAuth message and delete
     * station message*/
    RsnaSessDeleteSession (pRsnaSessNode);

    RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pWssRSNANotifyParams);
    return (RSNA_SUCCESS);
}

#ifdef WPA_WANTED
/****************************************************************************
*                                                                           *
* Function     : WpaSessHandleNewStation                                   *
*                                                                           *
* Description  : Function invoked when assoc request is recvd from apme     *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Point to the rsna port info            *
*                pRsnaSupInfo :- Points to the rsna station info            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
WpaSessHandleNewStation (tRsnaPaePortEntry * pRsnaPaePortInfo,
                         tRsnaSupInfo * pRsnaSupInfo, UINT4 u4BssIfIndex,
                         tRsnaElements * pRsnaElements, UINT1 u1Type)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tRsnaSessionNode   *pRsnaSessionNode = NULL;
    UINT2               u2PktLen = 0;
    UINT1              *pu1Pkt = NULL;
    UINT1               au1NullPmkId[RSNA_PMKID_LEN];
    BOOL1               bTriggerRsnaStateMachine = FALSE;
    tCfaIfInfo          IfInfo;

    CfaGetIfInfo (u4BssIfIndex, &IfInfo);

    MEMSET (au1NullPmkId, 0, RSNA_PMKID_LEN);

    if (PnacSnmpLowGetAuthSessionNode (pRsnaSupInfo->au1SuppMacAddr,
                                       &pAuthSessNode) == PNAC_FAILURE)
    {

        /* Checks whether the AKM suite received in RSNA IE in Association/
         * ReAssociation Request is 802.1x or PSK */

        if (((MEMCMP (pRsnaSupInfo->au1AkmSuite, WPA_AKM_UNSPEC_802_1X,
                      RSNA_CIPHER_SUITE_LEN)) == 0))
        {
            /* Checks whether the PMKID is sent in the Association/ReAssociation 
               Request */
            if ((MEMCMP (pRsnaElements->aPmkidList[0], au1NullPmkId,
                         RSNA_PMKID_LEN)) == 0)
            {
                /* If PMKId is not sent in the Association/ReAssociation 
                 * Request, then trigger the 802.1x authentication */

                pAuthSessNode =
                    RsnaSessCreatePnacAuthSess (pRsnaSupInfo->au1SuppMacAddr,
                                                (UINT2) u4BssIfIndex, TRUE);

                /* As the Key is not available,Do not trigger the RSNA State 
                   Machine.It will be triggered once Key Available notification 
                   is received from PNAC */

                bTriggerRsnaStateMachine = FALSE;
            }
            else if ((RsnaUtilGetPmkSaFromCache (pRsnaSupInfo->au1SuppMacAddr,
                                                 IfInfo.au1MacAddr)) != NULL)
            {
                /* If PMKID sent in the Association/ReAssociation Request is 
                   available in the PMK Cache  then do not trigger the 
                   802.1x authentication */

                pAuthSessNode =
                    RsnaSessCreatePnacAuthSess (pRsnaSupInfo->au1SuppMacAddr,
                                                (UINT2) u4BssIfIndex, FALSE);

                /* Trigger the RSNA State Machine since the PMKSA for the 
                 * station is available in the PMK Cache */

                bTriggerRsnaStateMachine = TRUE;

            }
            else
            {
                /* If PMKId sent in the Association/ReAssociation Request is not 
                   available in the PMK Cache then trigger the 802.1x authentication */

                pAuthSessNode =
                    RsnaSessCreatePnacAuthSess (pRsnaSupInfo->au1SuppMacAddr,
                                                (UINT2) u4BssIfIndex, TRUE);

                /* As the Key is not available,Do not trigger the RSNA State 
                 * Machine. It will be triggered once Key Available 
                 * notification is received PNAC */

                bTriggerRsnaStateMachine = FALSE;
            }

        }
        else if (((MEMCMP (pRsnaSupInfo->au1AkmSuite, WPA_AKM_PSK_OVER_802_1X,
                           RSNA_CIPHER_SUITE_LEN) == 0)))
        {
            pAuthSessNode =
                RsnaSessCreatePnacAuthSess (pRsnaSupInfo->au1SuppMacAddr,
                                            (UINT2) u4BssIfIndex, FALSE);
            bTriggerRsnaStateMachine = TRUE;

        }
    }

    if (pAuthSessNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " AuthCreateSession returned failure\n");
        return (RSNA_FAILURE);
    }

    pRsnaSessionNode = RsnaSessCreateAndInitSession (pRsnaPaePortInfo,
                                                     pAuthSessNode, pu1Pkt,
                                                     u2PktLen, u4BssIfIndex,
                                                     pRsnaElements->
                                                     aPmkidList[0],
                                                     bTriggerRsnaStateMachine);
    if (pRsnaSessionNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessCreateAndInitSession failed in RsnaSessHandleNewStation\n");
        PnacAuthDeleteSession (pAuthSessNode);
        return RSNA_FAILURE;
    }
    UNUSED_PARAM (u1Type);
    return (RSNA_SUCCESS);
}

#endif

/****************************************************************************
*                                                                           *
* Function     : RsnaSessHandleNewStation                                   *
*                                                                           *
* Description  : Function invoked when assoc request is recvd from apme     *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Point to the rsna port info            *
*                pRsnaSupInfo :- Points to the rsna station info            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessHandleNewStation (tRsnaPaePortEntry * pRsnaPaePortInfo,
                          tRsnaSupInfo * pRsnaSupInfo, UINT4 u4BssIfIndex,
                          tRsnaElements * pRsnaElements, UINT1 u1Type)
{
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tRsnaSessionNode   *pRsnaSessionNode = NULL;
    UINT2               u2PktLen = 0;
    UINT1              *pu1Pkt = NULL;
    UINT1               au1NullPmkId[RSNA_PMKID_LEN];
    BOOL1               bTriggerRsnaStateMachine = FALSE;
    tCfaIfInfo          IfInfo;
    tRsnaPmkSa         *pRsnaPmkSa = NULL;
    tMacAddr            staMacAddr;

    MEMSET (staMacAddr, 0, RSNA_ETH_ADDR_LEN);

    CfaGetIfInfo (u4BssIfIndex, &IfInfo);

    MEMSET (au1NullPmkId, 0, RSNA_PMKID_LEN);

    if (PnacSnmpLowGetAuthSessionNode (pRsnaSupInfo->au1SuppMacAddr,
                                       &pAuthSessNode) == PNAC_FAILURE)
    {

        /* Checks whether the AKM suite received in RSNA IE in Association/
         * ReAssociation Request is 802.1x or PSK */

        if (((MEMCMP (pRsnaSupInfo->au1AkmSuite, RSNA_AKM_UNSPEC_802_1X,
                      RSNA_CIPHER_SUITE_LEN)) == 0)
#ifdef PMF_WANTED
            ||
            ((MEMCMP
              (pRsnaSupInfo->au1AkmSuite, RSNA_AKM_UNSPEC_802_1X_SHA256,
               RSNA_CIPHER_SUITE_LEN)) == 0)
#endif
            )
        {
            /* Check whether the Opportunistic Key Caching (OKC) is Enabled */
            if (pRsnaPaePortInfo->RsnaConfigDB.u1RsnaOkcStatus == RSNA_ENABLED)
            {
                /* Get the Base PMK for this STA */
                pRsnaPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaSupInfo->
                                                        au1SuppMacAddr,
                                                        au1NullPmkId);

                /* If the Base PMK for the STA is available,then use that PMK
                 * to generate the OKC PMKID */

                if (pRsnaPmkSa != NULL)
                {
                    if ((RsnaUtilGenerateOkcPmkid
                         (pRsnaPaePortInfo, pRsnaSupInfo, u4BssIfIndex,
                          &pRsnaElements, pRsnaPmkSa->au1Pmk)) != RSNA_SUCCESS)
                    {
                        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                                  "RsnaSessHandleNewStation:- "
                                  "RsnaUtilGenerateOkcPmkid retured Failure !!!!! \n");
                        return (RSNA_FAILURE);
                    }

                    pAuthSessNode = RsnaSessCreatePnacAuthSess (pRsnaSupInfo->
                                                                au1SuppMacAddr,
                                                                (UINT2)
                                                                u4BssIfIndex,
                                                                FALSE);

                    bTriggerRsnaStateMachine = TRUE;

                }
                else
                {
                    /* If the base PMK for the STA is not available then do not
                     * trigger the RSNA State Machine Until the Key is available
                     * from PNAC */

                    pAuthSessNode = RsnaSessCreatePnacAuthSess (pRsnaSupInfo->
                                                                au1SuppMacAddr,
                                                                (UINT2)
                                                                u4BssIfIndex,
                                                                TRUE);

                    bTriggerRsnaStateMachine = FALSE;
                }

            }
            else
            {
                /* Checks whether the PMKID is sent in the Association/ReAssociation 
                   Request */
                /*This part of code is temporarily commented so as to complete the */
                /*handshake irrespective of the PMKID */
#if 0
                if ((MEMCMP (pRsnaElements->aPmkidList[0], au1NullPmkId,
                             RSNA_PMKID_LEN)) == 0)
                {
                    /* If PMKId is not sent in the Association/ReAssociation 
                     * Request, then trigger the 802.1x authentication */

                    pAuthSessNode =
                        RsnaSessCreatePnacAuthSess (pRsnaSupInfo->
                                                    au1SuppMacAddr,
                                                    (UINT2) u4BssIfIndex, TRUE);

                    /* As the Key is not available,Do not trigger the RSNA State 
                       Machine.It will be triggered once Key Available notification 
                       is received from PNAC */

                    bTriggerRsnaStateMachine = FALSE;
                }
                else if ((RsnaUtilGetPmkSaFromCache
                          (pRsnaSupInfo->au1SuppMacAddr,
                           IfInfo.au1MacAddr)) != NULL)
                {
                    /* If PMKID sent in the Association/ReAssociation Request is 
                       available in the PMK Cache  then do not trigger the 
                       802.1x authentication */

                    pAuthSessNode =
                        RsnaSessCreatePnacAuthSess (pRsnaSupInfo->
                                                    au1SuppMacAddr,
                                                    (UINT2) u4BssIfIndex,
                                                    FALSE);

                    /* Trigger the RSNA State Machine since the PMKSA for the 
                     * station is available in the PMK Cache */

                    bTriggerRsnaStateMachine = TRUE;

                }
                else
#endif
                {
                    /* If PMKId sent in the Association/ReAssociation Request is not 
                       available in the PMK Cache then trigger the 802.1x authentication */

                    pAuthSessNode =
                        RsnaSessCreatePnacAuthSess (pRsnaSupInfo->
                                                    au1SuppMacAddr,
                                                    (UINT2) u4BssIfIndex, TRUE);

                    /* As the Key is not available,Do not trigger the RSNA State 
                     * Machine. It will be triggered once Key Available 
                     * notification is received PNAC */

                    bTriggerRsnaStateMachine = FALSE;
                }
            }

        }
        else if (((MEMCMP (pRsnaSupInfo->au1AkmSuite, RSNA_AKM_PSK_OVER_802_1X,
                           RSNA_CIPHER_SUITE_LEN) == 0))
#ifdef PMF_WANTED
                 ||
                 ((MEMCMP
                   (pRsnaSupInfo->au1AkmSuite, RSNA_AKM_PSK_SHA256_OVER_802_1X,
                    RSNA_CIPHER_SUITE_LEN) == 0))
#endif
            )
        {
            pAuthSessNode =
                RsnaSessCreatePnacAuthSess (pRsnaSupInfo->au1SuppMacAddr,
                                            (UINT2) u4BssIfIndex, FALSE);
            bTriggerRsnaStateMachine = TRUE;

        }
    }

    if (pAuthSessNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " AuthCreateSession returned failure\n");
        return (RSNA_FAILURE);
    }

    pRsnaSessionNode = RsnaSessCreateAndInitSession (pRsnaPaePortInfo,
                                                     pAuthSessNode, pu1Pkt,
                                                     u2PktLen, u4BssIfIndex,
                                                     pRsnaElements->
                                                     aPmkidList[0],
                                                     bTriggerRsnaStateMachine);
    if (pRsnaSessionNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessCreateAndInitSession failed in RsnaSessHandleNewStation\n");

        MEMCPY (staMacAddr, pAuthSessNode->rmtSuppMacAddr, RSNA_ETH_ADDR_LEN);
        PnacAuthDeleteSession (pAuthSessNode);
        RsnaPnacDeleteSession (staMacAddr);

        return RSNA_FAILURE;
    }
    UNUSED_PARAM (u1Type);
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessGenPmkId                                           *
*                                                                           *
* Description  : Function to generate PMKID                                 *
*                                                                           *
* Input        : pRsnaPmkSa :- Pointer to rsna pmksa                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessGenPmkId (tRsnaPmkSa * pRsnaPmkSa)
{

    UINT1               au1Data[RSNA_PMKID_DATA_LEN] = "";
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT1              *pu1Data = NULL;

    MEMSET (au1Data, 0, RSNA_PMKID_DATA_LEN);
    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);
    pu1Data = au1Data;

    MEMCPY (pu1Data, "PMK Name", STRLEN ("PMK Name"));
    pu1Data += STRLEN ("PMK Name");
    MEMCPY (pu1Data, pRsnaPmkSa->au1SrcMac, RSNA_ETH_ADDR_LEN);
    pu1Data += RSNA_ETH_ADDR_LEN;
    MEMCPY (pu1Data, pRsnaPmkSa->au1SuppMac, RSNA_ETH_ADDR_LEN);

    RsnaAlgoHmacSha1 (pRsnaPmkSa->au1Pmk, RSNA_PMK_LEN, au1Data,
                      RSNA_PMKID_DATA_LEN, au1Digest);

    MEMCPY (pRsnaPmkSa->au1PmkId, au1Digest, RSNA_PMKID_LEN);
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessGenPtkFromPmk                                      *
*                                                                           *
* Description  : Function to genearte ptk from pmk                          *
*                                                                           *
* Input        : pRsnaSessInfo :- Pointer to session info                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessGenPtkFromPmk (tRsnaSessionNode * pRsnaSessInfo)
{
    UINT1               u1Count = 0;
    UINT1               au1Buf[RSNA_PMK_TO_PTK_DATA_LEN + 2] = "";
    UINT1               au1PtkBuf[RSNA_KCK_KEY_LEN + RSNA_KEK_KEY_LEN +
                                  RSNA_MAX_PTK_KEY_LEN] = "";
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT1               au1SHA256Digest[SHA256_HASH_SIZE] = "";
    UINT2               u2PtkKeyLen = 0;
    INT4                i4TotalKeyLen = 0;
    INT2                i2Offset = 0;
    INT2                i2RemLen = 0;
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1Ptk = NULL;
    UINT1               u1ValIndex = 0;
#ifdef PMF_WANTED
    UINT2               u2Index = 0;
    UINT1               u1KeyIndex = 0;
    UINT1               u1KeyLength = 48;
    UINT1               u1Length = 0;
    UINT2               u2MaxKeyLength = 384;
#endif

    MEMSET (au1Buf, 0, RSNA_PMK_TO_PTK_DATA_LEN + 2);
    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);
    MEMSET (au1SHA256Digest, 0, SHA256_HASH_SIZE);

    u2PtkKeyLen = (RSNA_KCK_KEY_LEN + RSNA_KEK_KEY_LEN + RSNA_MAX_PTK_KEY_LEN);

    pu1Ptr = au1Buf;
    pu1Ptk = (UINT1 *) &(pRsnaSessInfo->PtkSaDB);

    /* When the AKM suite chosen is 00-0F-AC-5 or 00-0F-AC-6, HMAC-SHA-256 algorithm will be used 
     * In this algorithm the input must be prepended with the iteration number and also be appended 
     * with the length of the KDF used. Hence the first two bytes are skipped for including the 
     * iteration number. */

#ifdef PMF_WANTED
    if ((MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1RsnaAuthKeyMgmt8021XSHA256, 4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1RsnaAuthKeyMgmtPskSHA256Over8021X, 4) == 0))
    {
        u1ValIndex += 2;
    }
#endif

    MEMCPY (pu1Ptr + u1ValIndex, "Pairwise key expansion", RSNA_PTK_LABEL_LEN);
    pu1Ptr += RSNA_PTK_LABEL_LEN;
    pu1Ptr += u1ValIndex;

    if ((MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite, gau1RsnaAuthKeyMgmt8021X,
          4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1RsnaAuthKeyMgmtPskOver8021X, 4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite, gau1WpaAuthKeyMgmt8021X,
          4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1WpaAuthKeyMgmtPskOver8021X, 4) == 0))
    {
        pu1Ptr[0] = u1Count;
        pu1Ptr++;
    }
    if (MEMCMP (pRsnaSessInfo->au1BssMac,
                pRsnaSessInfo->pRsnaSupInfo->au1SuppMacAddr,
                RSNA_ETH_ADDR_LEN) < 0)
    {
        MEMCPY (pu1Ptr, pRsnaSessInfo->au1BssMac, RSNA_ETH_ADDR_LEN);
        pu1Ptr += RSNA_ETH_ADDR_LEN;
        MEMCPY (pu1Ptr, pRsnaSessInfo->pRsnaSupInfo->au1SuppMacAddr,
                RSNA_ETH_ADDR_LEN);
        pu1Ptr += RSNA_ETH_ADDR_LEN;
    }
    else
    {
        MEMCPY (pu1Ptr, pRsnaSessInfo->pRsnaSupInfo->au1SuppMacAddr,
                RSNA_ETH_ADDR_LEN);
        pu1Ptr += RSNA_ETH_ADDR_LEN;
        MEMCPY (pu1Ptr, pRsnaSessInfo->au1BssMac, RSNA_ETH_ADDR_LEN);
        pu1Ptr += RSNA_ETH_ADDR_LEN;
    }

    if (MEMCMP (pRsnaSessInfo->au1ANonce,
                pRsnaSessInfo->au1SNonce, RSNA_NONCE_LEN) < 0)
    {

        MEMCPY (pu1Ptr, pRsnaSessInfo->au1ANonce, RSNA_NONCE_LEN);
        pu1Ptr += RSNA_NONCE_LEN;
        MEMCPY (pu1Ptr, pRsnaSessInfo->au1SNonce, RSNA_NONCE_LEN);
        pu1Ptr += RSNA_NONCE_LEN;
    }
    else
    {
        MEMCPY (pu1Ptr, pRsnaSessInfo->au1SNonce, RSNA_NONCE_LEN);
        pu1Ptr += RSNA_NONCE_LEN;
        MEMCPY (pu1Ptr, pRsnaSessInfo->au1ANonce, RSNA_NONCE_LEN);
        pu1Ptr += RSNA_NONCE_LEN;
    }

    if ((MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite, gau1RsnaAuthKeyMgmt8021X,
          4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1RsnaAuthKeyMgmtPskOver8021X, 4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite, gau1WpaAuthKeyMgmt8021X,
          4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1WpaAuthKeyMgmtPskOver8021X, 4) == 0))
    {
        pu1Ptr[0] = u1Count;
        pu1Ptr++;
    }

    if ((MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite, gau1RsnaAuthKeyMgmt8021X,
          4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1RsnaAuthKeyMgmtPskOver8021X, 4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite, gau1WpaAuthKeyMgmt8021X,
          4) == 0)
        ||
        (MEMCMP
         (pRsnaSessInfo->pRsnaSupInfo->au1AkmSuite,
          gau1WpaAuthKeyMgmtPskOver8021X, 4) == 0))
    {

        while (i2Offset < u2PtkKeyLen)
        {
            i2RemLen = (INT2) (u2PtkKeyLen - i2Offset);
            if (i2RemLen >= SHA1_HASH_SIZE)
            {
                RsnaAlgoHmacSha1 (pRsnaSessInfo->au1Pmk,
                                  RSNA_PMK_LEN, au1Buf,
                                  RSNA_PMK_TO_PTK_DATA_LEN,
                                  &(au1PtkBuf[i2Offset]));
                i2Offset = (INT2) (i2Offset + SHA1_HASH_SIZE);
                i4TotalKeyLen = i2Offset;
            }
            else
            {
                RsnaAlgoHmacSha1 (pRsnaSessInfo->au1Pmk,
                                  RSNA_PMK_LEN, au1Buf,
                                  RSNA_PMK_TO_PTK_DATA_LEN, au1Digest);
                MEMCPY (&(au1PtkBuf[i2Offset]), au1Digest, i2RemLen);
                i4TotalKeyLen += i2RemLen;
                break;
            }
            u1Count++;
            au1Buf[(RSNA_PMK_TO_PTK_DATA_LEN - 1)] = u1Count;
        }
        MEMCPY (pu1Ptk + 0, au1PtkBuf, i4TotalKeyLen);
    }
#ifdef PMF_WANTED
    else
    {
        u1Length = 32;
        for (u2Index = 1; u2Index <=
             ((RSNA_PTK_BITS + RSNA_VALUE_255) / RSNA_VALUE_256); u2Index++)
        {

            memcpy (au1Buf, &u2Index, sizeof (UINT2));
            memcpy (au1Buf + RSNA_PMK_TO_PTK_DATA_LEN, &u2MaxKeyLength,
                    sizeof (UINT2));

            RsnaAlgoHmacSha2 (pRsnaSessInfo->au1Pmk, RSNA_PMK_LEN, au1Buf,
                              (RSNA_PMK_TO_PTK_DATA_LEN + 2), au1SHA256Digest);

            MEMCPY (pu1Ptk + u1KeyIndex, au1SHA256Digest, u1Length);

            u1KeyIndex += u1Length;

            u1Length = u1KeyLength - u1Length;

        }
    }
#endif
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessGenGtkFromGmk                                      *
*                                                                           *
* Description  : Function to generate gtk from gmk                          *
*                                                                           *
* Input        : pRsnaGlobalGtk :- Pointer to rsna global gtk               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaSessGenGtkFromGmk (tRsnaGlobalGtk * pRsnaGlobalGtk)
{

    UINT1               au1Data[RSNA_ETH_ADDR_LEN + RSNA_NONCE_LEN] = "";
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT1              *pu1Ptr = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    UINT1               u1Index = 0;
#ifdef PMF_WANTED
    UINT1               u1IGtkKeyIndex = 0;
#endif

    MEMSET (au1Data, 0, (RSNA_ETH_ADDR_LEN + RSNA_NONCE_LEN));
    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);

    pRsnaPaePortInfo = pRsnaGlobalGtk->pRsnaPaePortInfo;
    pu1Ptr = au1Data;
    if (pRsnaGlobalGtk->pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCoreGlobalGroupSetKeysState:-"
                  "pRsnaGlobalGtk->pRsnaPaePortInfo is Null !!!! \n");
        return (RSNA_FAILURE);
    }

    MEMCPY (pu1Ptr, pRsnaPaePortInfo->au1SrcMac, RSNA_ETH_ADDR_LEN);
    pu1Ptr += RSNA_ETH_ADDR_LEN;
    MEMCPY (pu1Ptr, pRsnaGlobalGtk->au1Gnonce, RSNA_NONCE_LEN);
    pu1Ptr -= RSNA_ETH_ADDR_LEN;

    RsnaAlgoHmacSha1 (pRsnaGlobalGtk->au1Gmk, RSNA_GMK_LEN,
                      pu1Ptr, (RSNA_ETH_ADDR_LEN + RSNA_NONCE_LEN), au1Digest);
    u1Index = (UINT1) (pRsnaGlobalGtk->u1GN - RSNA_GTK_FIRST_IDX);
#ifdef WPA_WANTED
    if (pRsnaGlobalGtk->bWpaEnabled == OSIX_TRUE)
    {
        if (MEMCMP
            (pRsnaPaePortInfo->WpaConfigDB.au1GroupCipher,
             gau1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)
        {
            MEMCPY (pRsnaGlobalGtk->au1Gtk[u1Index], au1Digest,
                    RSNA_TKIP_KEY_LEN);
            return (RSNA_SUCCESS);
        }
        else
        {
            return (RSNA_FAILURE);
        }
    }
#endif

#ifdef PMF_WANTED
    u1IGtkKeyIndex = u1Index + RSNA_IGTK_FIRST_IDX;

/*PMF WANTED*/

    RsnaSessGenIGtk (&(pRsnaPaePortInfo->RsnaGlobalIGtk), u1IGtkKeyIndex);
#endif

    if (MEMCMP
        (pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher, gau1RsnaCipherSuiteTKIP,
         RSNA_CIPHER_SUITE_LEN) == 0)
    {
        MEMCPY (pRsnaGlobalGtk->au1Gtk[u1Index], au1Digest, RSNA_TKIP_KEY_LEN);
    }
    else
    {
        MEMCPY (pRsnaGlobalGtk->au1Gtk[u1Index], au1Digest, RSNA_CCMP_KEY_LEN);
    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSessGetSeqNum                                          *
*                                                                           *
* Description  : Function to get next seq number                            *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                pu1KeyRsc :- Refers to the sequence number                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
#ifdef PMF_DEBUG_WANTED
UINT4
RsnaSessGetSeqNum (tRsnaSessionNode * pRsnaSessNode, UINT1 *pu1KeyRsc)
{
    UNUSED_PARAM (pRsnaSessNode);
    UNUSED_PARAM (pu1KeyRsc);

    ApmeTaskLock ();
    if (ApmeMlmeGetSeqNo (pRsnaSessNode->u2Port,
                          pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,
                          pRsnaSessNode->pRsnaPaePortInfo->
                          RsnaGlobalGtk.u1GN, pu1KeyRsc) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Get Seq Num Failed !!!! \n");
        ApmeTaskUnLock ();
        return (RSNA_FAILURE);
    }

    ApmeTaskUnLock ();

    return (RSNA_SUCCESS);
}
#endif

#ifdef PMF_WANTED
/****************************************************************************
*                                                                           *
* Function     : RsnaSessGenIGtk                                            *
*                                                                           *
* Description  : Function to generate the IGTK key                          *
*                                                                           *
* Input        : pRsnaSessNode :- Pointer to rsna session node              *
*                pu1KeyRsc :- Refers to the sequence number                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

VOID
RsnaSessGenIGtk (tRsnaGlobalIGtk * pRsnaGlobalIGtk, UINT1 u1IGtkKeyIdx)
{
    UINT1               au1RandNo[RSNA_NONCE_LEN];
    MEMSET (au1RandNo, 0, RSNA_NONCE_LEN);
    RsnaUtilGetRandom (au1RandNo, RSNA_NONCE_LEN);

    MEMCPY (pRsnaGlobalIGtk->au1IGtk[u1IGtkKeyIdx - RSNA_IGTK_FIRST_IDX],
            au1RandNo, RSNA_IGTK_MAX_LEN);

    return;
}
#endif
