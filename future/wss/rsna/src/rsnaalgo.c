/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaalgo.c,v 1.2 2017/05/23 14:16:53 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"

/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoPBKDF2                                             *
*                                                                           *
* Description  : Function to convert pass phrase to psk in hex              *
*                                                                           *
* Input        : pu1PassPhrase :- PassPhrase in string                      *
*                u1PassPhraseLen :- Length of the pass phrase               *
*                pu1Sssid :- Pointer to ssid                                *
*                u1SsidLen :- Length of the Ssid                            *
*                u2Iterations :- Count of the Iterations                    *
*                u1Count :- Count of the loop to be executed                * 
*                pu1Digest :- Computed Digest                               *
*                                                                           * 
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaAlgoPBKDF2 (UINT1 *pu1PassPhrase, UINT1 u1PassPhraseLen, UINT1 *pu1Ssid,
                UINT1 u1SsidLen, UINT2 u2Iterations, UINT4 u4Count,
                UINT1 *pu1Digest)
{

    UINT1               au1Temp[SHA1_HASH_SIZE] = "";
    UINT1               au1Temp2[SHA1_HASH_SIZE] = "";
    UINT2               u2LoopCount1 = 0;
    UINT2               u2LoopCount2 = 0;
    UINT1               au1CountBuf[] = { 0x0, 0x0, 0x0, 0x0 };
    UINT1               au1Buf[(sizeof (tSsid) + RSNA_CIPHER_SUITE_LEN)] = "";
    UINT1               u1Idx = 0;

    MEMSET (au1Temp, 0, SHA1_HASH_SIZE);
    MEMSET (au1Temp2, 0, SHA1_HASH_SIZE);
    MEMSET (au1CountBuf, 0, RSNA_CIPHER_SUITE_LEN);
    MEMSET (au1Buf, 0, (UINT1) (sizeof (tSsid) + RSNA_CIPHER_SUITE_LEN));

    au1CountBuf[u1Idx] = (UINT1) ((u4Count >> 24) & RSNA_ONE_BYTE_FULL_MASK);
    u1Idx++;

    au1CountBuf[u1Idx] = (UINT1) ((u4Count >> 16) & RSNA_ONE_BYTE_FULL_MASK);
    u1Idx++;

    au1CountBuf[u1Idx] = (UINT1) ((u4Count >> 8) & RSNA_ONE_BYTE_FULL_MASK);
    u1Idx++;

    au1CountBuf[u1Idx] = (UINT1) (u4Count & RSNA_ONE_BYTE_FULL_MASK);

    MEMCPY (au1Buf, pu1Ssid, u1SsidLen);
    MEMCPY (&au1Buf[u1SsidLen], au1CountBuf, RSNA_CIPHER_SUITE_LEN);

    RsnaAlgoHmacSha1 (pu1PassPhrase, u1PassPhraseLen, au1Buf,
                      (UINT4) (u1SsidLen + RSNA_CIPHER_SUITE_LEN), au1Temp);

    MEMCPY (pu1Digest, au1Temp, SHA1_HASH_SIZE);

    for (u2LoopCount1 = 1; u2LoopCount1 < u2Iterations; u2LoopCount1++)
    {
        RsnaAlgoHmacSha1 (pu1PassPhrase, u1PassPhraseLen,
                          au1Temp, SHA1_HASH_SIZE, au1Temp2);

        MEMCPY (au1Temp, au1Temp2, SHA1_HASH_SIZE);
        for (u2LoopCount2 = 0; u2LoopCount2 < SHA1_HASH_SIZE; u2LoopCount2++)
        {
            pu1Digest[u2LoopCount2] ^= au1Temp2[u2LoopCount2];
        }

    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoHmacSha1                                           *
*                                                                           *
* Description  : Wrapper function for rsna to invoke Hmachsha1              *
*                                                                           *
* Input        : pu1Key :- Refers to the key                                *
*                u4LeyLen :- Length of the Key                              *
*                pu1BufPtr :- Points to the packet                          *
*                u4BufLen :- Length of the packet                           *
*                pu1Digest :- Points to the computed digest                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaAlgoHmacSha1 (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1BufPtr,
                  UINT4 u4BufLen, UINT1 *pu1Digest)
{
    arHmac_Sha1 (pu1Key, u4KeyLen, pu1BufPtr, (INT4) u4BufLen, pu1Digest);
}

#ifdef PMF_WANTED
/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoHmacSha2                                           *
*                                                                           *
* Description  : Wrapper function for rsna to invoke Hmachsha2              *
*                                                                           *
* Input        : pu1Key :- Refers to the key                                *
*                u4LeyLen :- Length of the Key                              *
*                pu1BufPtr :- Points to the packet                          *
*                u4BufLen :- Length of the packet                           *
*                pu1Digest :- Points to the computed digest                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaAlgoHmacSha2 (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1BufPtr,
                  UINT4 u4BufLen, UINT1 *pu1Digest)
{

    unUtilAlgo          UtilAlgo;
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    UtilAlgo.UtilHmacAlgo.HmacShaVersion = AR_SHA256_ALGO;
    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = pu1BufPtr;
    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (INT4) u4BufLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1Key;
    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (INT4) u4KeyLen;
    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = pu1Digest;

    UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
}
#endif

/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoHmacMd5                                            *
*                                                                           *
* Description  : Wrapper function for rsna to invoke hmacmd5                *
*                                                                           *
* Input        : pu1Key :- pointer to the Key                               *
*                i4KeyLen :- Length of the Key                              *
*                pu1BufPrt :- Points to the packet                          *
*                i4BufLen  :- Length of the packet                          *
*                pu1Digest :- Points to the computed digest                 *
*                                                                           * 
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaAlgoHmacMd5 (UINT1 *pu1Key, INT4 i4KeyLen, UINT1 *pu1BufPtr,
                 INT4 i4BufLen, UINT1 *pu1Digest)
{
    arHmac_MD5 (pu1BufPtr, i4BufLen, pu1Key, (UINT4) i4KeyLen, pu1Digest);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoAesWrap                                            *
*                                                                           *
* Description  : Wrapper function for rsna to invoke Aes Wrap Algo          *
*                                                                           *
* Input        : pu1Key :- Pointer to the key                               *
*                i4Len  :- Length of the key                                *
*                pu1PlainTxt :- Points to the text to be encrypted          *
*                pu1CipherTxt :- Points to the encrypted text               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaAlgoAesWrap (UINT1 *pu1Key, INT4 i4Len, UINT1 *pu1PlainTxt,
                 UINT1 *pu1CipherTxt)
{
    arAes_wrap (pu1Key, (UINT4) i4Len, pu1PlainTxt, pu1CipherTxt);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoRc4Skip                                            *
*                                                                           *
* Description  : Wrapper function for rsna to invoke rc4 skip algo          *
*                                                                           *
* Input        : pu1EncrKey :- Pointer to the encryption key                *
*                u1nonceLen :- Length of the nonce                          *
*                u2Skip     :- Refers to the skip                           *
*                pu1Ptr     :- Points to the packet                         *
*                u2KeyDataLen :- Length of the Key Data                     *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaAlgoRc4Skip (UINT1 *pu1EncrKey, UINT1 u1NonceLen, UINT2 u2Skip,
                 UINT1 *pu1Ptr, UINT2 u2KeyDataLen)
{

    arRc4_Skip (pu1EncrKey, u1NonceLen, u2Skip, pu1Ptr, u2KeyDataLen);
}

#ifdef PMF_WANTED
/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoCmacAes128                                         *
*                                                                           *
* Description  : Wrapper function for rsna to invoke rc4 skip algo          *
*                                                                           *
* Input        : pu1EncrKey :- Pointer to the encryption key                *
*                u1nonceLen :- Length of the nonce                          *
*                u2Skip     :- Refers to the skip                           *
*                pu1Ptr     :- Points to the packet                         *
*                u2KeyDataLen :- Length of the Key Data                     *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

VOID
RsnaAlgoCmacAes128 (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1BufPtr,
                    UINT4 u4BufLen, UINT1 *pu1Digest)
{
    FIPS_cmac_aes128_test (pu1Key, u4KeyLen, pu1BufPtr, u4BufLen, pu1Digest);
}
#endif
/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoComputeMic                                         *
*                                                                           *
* Description  : Function to compute Mic                                    *
*                                                                           *
* Input        : u2KeyInfo :- Refers to the characteristics of the Key      *
*                pu1Key    :- Refers to the key to be sent as part of pkt   *
*                pu1Pkt    :- Refers to the packet to be sent               *
*                u2PktLen  :- Length of the packet                          *
*                pu1Mic    :- Mic to be sent along with the pkt             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*****************************************************************************/
UINT4
RsnaAlgoComputeMic (UINT2 u2KeyInfo, UINT1 *pu1Key, UINT1 *pu1Pkt,
                    UINT2 u2PktLen, UINT1 *pu1Mic)
{

    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT2               u2Mask = 0;

    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);

    u2Mask = (u2KeyInfo & RSNA_KEY_INFO_MASK);
    switch (u2Mask)
    {

        case RSNA_KEY_INFO_HMACMD5_RC4:
            RsnaAlgoHmacMd5 (pu1Key, RSNA_KEK_KEY_LEN, pu1Pkt, u2PktLen,
                             pu1Mic);
            break;
        case RSNA_KEY_INFO_HMACSHA1_AES:
            RsnaAlgoHmacSha1 (pu1Key, RSNA_KEK_KEY_LEN,
                              pu1Pkt, u2PktLen, au1Digest);
            MEMCPY (pu1Mic, au1Digest, RSNA_MIC_LEN);
            break;
#ifdef PMF_WANTED
        case RSNA_KEY_INFO_CMAC_128_AES:
            RsnaAlgoCmacAes128 (pu1Key, RSNA_KEK_KEY_LEN, pu1Pkt, u2PktLen,
                                pu1Mic);
            break;
#endif
        default:
            return (RSNA_FAILURE);

    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaAlgoConvertPassPhraseToPsk                             *
*                                                                           *
* Description  : Function to convert ascii test to psk in hex format        *
*                                                                           *
* Input        : pSsid :- Pointer to ssid information                       *
*                pu1PassPhrase :- Refers to pass phrase                     *
*                pu1Psk        : Refers to psk                              *
*                u1SsidLen     :- Length of the Ssid                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaAlgoConvertPassPhraseToPsk (UINT1 *pu1Ssid, UINT1 *pu1PassPhrase,
                                UINT1 *pu1Psk, UINT1 u1SsidLen)
{
    INT1                i1Offset = 0;
    INT1                i1Len = RSNA_PMK_LEN;
    UINT1               u1RemLen = 0;
    UINT4               u4Count = 0;
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT1              *pu1PskPos = pu1Psk;

    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);

    while (i1Offset < i1Len)
    {
        u1RemLen = (UINT1) (i1Len - i1Offset);
        if (u1RemLen >= SHA1_HASH_SIZE)
        {
            u4Count++;
            RsnaAlgoPBKDF2 (pu1PassPhrase,
                            (UINT1) (STRLEN (pu1PassPhrase)),
                            pu1Ssid, u1SsidLen,
                            RSNA_PBKDF2_ITERATIONS, u4Count, au1Digest);
            MEMCPY ((pu1PskPos + i1Offset), au1Digest, SHA1_HASH_SIZE);
            i1Offset = (INT1) (i1Offset + SHA1_HASH_SIZE);
        }
        else
        {
            u4Count++;
            RsnaAlgoPBKDF2 (pu1PassPhrase,
                            (UINT1) (STRLEN (pu1PassPhrase)),
                            pu1Ssid, u1SsidLen, RSNA_PBKDF2_ITERATIONS,
                            u4Count, au1Digest);
            MEMCPY ((pu1PskPos + i1Offset), au1Digest, u1RemLen);
            i1Offset = (INT1) (i1Offset + u1RemLen);
        }
    }
    return (RSNA_SUCCESS);
}
