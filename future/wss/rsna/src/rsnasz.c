/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnasz.c,v 1.2 2017/05/23 14:16:54 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#define _RSNASZ_C
#include "rsnainc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RsnaSizingMemCreateMemPools ()
{
    INT4                i4RetVal = 0;
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < RSNA_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsRSNASizingParams[i4SizingId].
                                     u4StructSize,
                                     FsRSNASizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(RSNAMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RsnaSizingMemDeleteMemPools ();
            return RSNA_FAILURE;
        }
    }
    return RSNA_SUCCESS;
}

INT4
RsnaSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRSNASizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RSNAMemPoolIds);
    return RSNA_SUCCESS;
}

VOID
RsnaSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < RSNA_MAX_SIZING_ID; i4SizingId++)
    {
        if (RSNAMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RSNAMemPoolIds[i4SizingId]);
            RSNAMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
