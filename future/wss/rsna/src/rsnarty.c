/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: rsnarty.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 * *
 * * Description : This file contains init functions for Rsna Module
 * *********************************************************************/


#include "rsnainc.h"

/****************************************************************************
 * *                                                                           *
 * * Function     : RsnaEapolRtyInit                                           *
 * *                                                                           *
 * * Description  : This function creates mempools used by the rsna module     *
 * *                This invoked by RsnaInitModuleStart                        *
 * *                                                                           *
 * * Input        : None                                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
 * *                                                                           *
 * *****************************************************************************/

UINT4
RsnaEapolRtyInit (VOID)
{
    /* Assign MempoolIds created in sz.c to global MempoolIds */
    RsnaEapolRtyAssignMempoolIds();

    /* Create the RBTree to store the MAX_RSNA_EAP_PACKET_INFO */
    gRsnaGlobals.RsnaEapPacketTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tRsnaEapPacketinfo,
                                              nextRsnaEapPacketinfo)),
                                       RsnaCompareEapPacketinfo);

    if (gRsnaGlobals.RsnaEapPacketTable == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, "RB Tree Creation for "
                  " RSNA Eap packet info Table Failed\n");
        RsnaSizingMemDeleteMemPools ();
        return RSNA_FAILURE;
    }

     return RSNA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RsnaEapolRtyAssignMempoolIds                         */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      RsnaMemPoolIds to global mempoolIds.                 */
/*                                                                           */
/* Input(s)           :  None                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
RsnaEapolRtyAssignMempoolIds (VOID)
{
    RSNA_EAP_PACKET_INFO_MEMPOOL_ID = RSNAMemPoolIds[MAX_RSNA_EAP_PACKET_INFO_SIZING_ID];
}

/*****************************************************************************/
/* Function Name      : RsnaCompareEapPacketinfo                             */
/*                                                                           */
/* Description        : This function compares the two Entry node            */
/*                      based on the Station Mac Address                     */
/*                                                                           */
/* Input(s)           : e1        Pointer to First BindingIfNode             */
/* Input(s)           : e2        Pointer to Second BindingIfNode            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1. RSNA_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                      2. RSNA_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                      3. RSNA_LESSER  - If key of first element is lesser  */
/*                                        than key of second element         */
/*****************************************************************************/
INT4
RsnaCompareEapPacketinfo (tRBElem * e1, tRBElem * e2)
{
    tRsnaEapPacketinfo  *pRBNode1 = (tRsnaEapPacketinfo*) e1;
    tRsnaEapPacketinfo  *pRBNode2 = (tRsnaEapPacketinfo*) e2;

    if ((MEMCMP (pRBNode1->StaMacAddr,
                 pRBNode2->StaMacAddr, sizeof (tMacAddr))) > 0)
    {
        return RSNA_GREATER;
    }
    else if ((MEMCMP (pRBNode1->StaMacAddr,
                      pRBNode2->StaMacAddr, sizeof (tMacAddr))) < 0)
    {
        return RSNA_LESSER;
    }

    return RSNA_EQUAL;
}

/*****************************************************************************/
/* Function Name      : RsnaEapolRtyProcess                                  */
/*                                                                           */
/* Description        : This function invoked to verify the eap packets type */
/*                      and calling RsnaEapolRtyAddEntry function create     */
/*                      entry in RBTree.                                     */
/*                                                                           */
/* Input(s)           : pu1KeyFrame - Buffer contains the packet             */
/*                      u4PktLen    - packet length                          */
/*                      u2PortNum   - port number                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RSNA_FAILURE/RSNA_SUCCESS                            */
/*****************************************************************************/
UINT1
RsnaEapolRtyProcess (UINT1 *pu1KeyFrame, UINT4 u4PktLen, UINT2 u2PortNum)
{
    if (*(pu1KeyFrame + RSNA_EAP_IDENTITY_OFFSET) == RSNA_EAP_IDENTITY)
    {
	return RSNA_SUCCESS; 
    }
    else if(*(pu1KeyFrame + RSNA_EAP_FAILURE_OFFSET) == RSNA_EAP_FAILURE)
    {
	return RSNA_SUCCESS; 
    }
    else if(*(pu1KeyFrame + RSNA_EAP_SUCCESS_OFFSET) == RSNA_EAP_SUCCESS)
    {
        return RSNA_SUCCESS;
    }
    else
    {
	RsnaEapolRtyAddEntry(pu1KeyFrame,u4PktLen,u2PortNum);
    }

    return RSNA_SUCCESS; 
}


/*****************************************************************************/
/* Function Name      : RsnaEapolRtyAddEntry                                 */
/*                                                                           */
/* Description        : This function invoked to add entry in RBTree when    */
/*                      the EAP Packets trasmitted to radius server          */
/*                                                                           */
/* Input(s)           : pu1KeyFrame - Buffer contains the packet             */
/*                      u4PktLen    - packet length                          */
/*                      u2PortNum   - port number                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : RSNA_FAILURE/RSNA_SUCCESS                            */
/*****************************************************************************/
UINT1
RsnaEapolRtyAddEntry (UINT1 *pu1KeyFrame, UINT4 u4PktLen, UINT2 u2PortNum)
{
    tRsnaEapPacketinfo  *pRsnaEapPacketinfo = NULL;
    tRsnaEapPacketinfo  RsnaEapPacketinfo;

    MEMSET (&RsnaEapPacketinfo, 0, sizeof (tRsnaEapPacketinfo));
    MEMCPY(RsnaEapPacketinfo.StaMacAddr, pu1KeyFrame, 6);

    pRsnaEapPacketinfo = RBTreeGet (gRsnaGlobals.RsnaEapPacketTable,
	                             (tRBElem *)&RsnaEapPacketinfo);

    if (pRsnaEapPacketinfo == NULL)
    {
	pRsnaEapPacketinfo = (tRsnaEapPacketinfo *) RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_EAP_PACKET_INFO_MEMPOOL_ID);

	if (pRsnaEapPacketinfo == NULL)
	{

	    RSNA_TRC (RSNA_INIT_SHUT_TRC,
		    "Rsna Eap Packet info:- MemAllocMemBlk" "returned Failure\n");
	    return (RSNA_FAILURE);
	}

	MEMSET (pRsnaEapPacketinfo, 0, sizeof (tRsnaEapPacketinfo));
	MEMCPY (pRsnaEapPacketinfo->StaMacAddr, pu1KeyFrame , MAC_ADDR_LEN);

	MEMCPY(pRsnaEapPacketinfo->au1Bufffer,pu1KeyFrame, u4PktLen);
	pRsnaEapPacketinfo->u4PktLen = u4PktLen;
	pRsnaEapPacketinfo->u2PortNum = u2PortNum;

	if ((RBTreeAdd (gRsnaGlobals.RsnaEapPacketTable, pRsnaEapPacketinfo)
		    == RB_SUCCESS))
	{
	    RsnaTmrStartTimer (&(pRsnaEapPacketinfo->RsnaEapRetransmitTmr), RSNA_EAP_PACKET_TIMER_ID,
		    RSNA_EAP_MAX_RETRANSMIT_TIME, pRsnaEapPacketinfo);
	}
    }
    return RSNA_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function     : RsnaEapolRtyDeleteEntry                                    */
/*                                                                            */
/*  Description  : Function invoked by pnac module when eapol response packet */
/*                 recevied from radius server and it removes the RBTree      */
/*                 entry which is created when sending radius request packet  */
/*                 to radius server                                           */
/*                                                                            */
/*  Input        : srcMacAddr  - Station Mac Address                          */
/*                                                                            */
/*  Output       : None                                                       */
/*                                                                            */
/*  Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                            */
/* *****************************************************************************/

UINT4
RsnaEapolRtyDeleteEntry(tMacAddr srcMacAddr)
{
    tRsnaEapPacketinfo *pRsnaEapPacketinfo = NULL;
    tRsnaEapPacketinfo  RsnaEapPacketinfo;

    RSNA_MEMSET (&RsnaEapPacketinfo, 0, sizeof (tRsnaEapPacketinfo));
    MEMCPY(RsnaEapPacketinfo.StaMacAddr, srcMacAddr , 6);

    pRsnaEapPacketinfo = RBTreeGet (gRsnaGlobals.RsnaEapPacketTable,
          (tRBElem *) &RsnaEapPacketinfo);

    if (pRsnaEapPacketinfo != NULL)
    {
      RsnaTmrStopTimer (&(pRsnaEapPacketinfo->RsnaEapRetransmitTmr));

      if ((RBTreeRemove (gRsnaGlobals.RsnaEapPacketTable, pRsnaEapPacketinfo)
                  == RB_FAILURE))
      {
          RSNA_TRC (ALL_FAILURE_TRC, "RsnaEapolRtyDeleteEntry:- Failure in RBTree remove \n");
          return RSNA_FAILURE;
      }
      RSNA_MEM_RELEASE_MEM_BLK (RSNA_EAP_PACKET_INFO_MEMPOOL_ID, pRsnaEapPacketinfo);
    }
    return RSNA_SUCCESS;
}

