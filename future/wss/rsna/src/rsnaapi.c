/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaapi.c,v 1.7 2017/11/24 10:37:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"

#include  "rsna.h"
#include  "rsnainc.h"
#include  "wssifinc.h"
#include  "wsscfglwg.h"
#include "wsswlanwlcproto.h"
/****************************************************************************
 Function    :  RsnaGetFirstIndexDot11PrivacyTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST_Routine. */
INT1
RsnaGetFirstIndexDot11PrivacyTable (INT4 *pi4NextIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4NextIfIndex = pRsnaPaePortEntry->u2Port;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetNextIndexDot11PrivacyTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */
INT1
RsnaGetNextIndexDot11PrivacyTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;

    if (i4IfIndex == 0)
    {
        /* Fetch the data structure of the first index */
        RsnaGetNextPortEntry (NULL, &pNextRsnaPaePortEntry);

        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }
    }
    else
    {
        /* fetch the data structure of the current index */
        RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "No Rsna Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }

        /* pass the data structure of the current index and get 
         * the data structure of the next index*/
        RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);

        /* if next index data structure is not NULL assign the
         * value for next index*/
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAEnabled (INT4 i4IfIndex, INT4 *pi4RetValDot11RSNAEnabled)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAEnabled =
        pRsnaPaePortEntry->RsnaConfigDB.bRSNAOptionImplemented;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAPreauthenticationEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAPreauthenticationEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAPreauthenticationEnabled (INT4 i4IfIndex,
                                          INT4
                                          *pi4RetValDot11RSNAPreauthenticationEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAPreauthenticationEnabled =
        pRsnaPaePortEntry->RsnaConfigDB.bRSNAPreauthenticationImplemented;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaSetDot11RSNAEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAEnabled (INT4 i4IfIndex, INT4 i4SetValDot11RSNAEnabled)
{
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT1               au1BssId[RSNA_ETH_ADDR_LEN];
    UINT1               au1TmpBSSID[RSNA_ETH_ADDR_LEN];
    tWssWlanDB          WssWlanDB;
    INT4                i4RadioIfIndex = 0;
    INT4                u4WlanProfileId = 0;
    INT4                i4NextRadioIfIndex = 0;
    INT4                i4BssIntfIndex = 0;
    UINT4               u4NextWlanProfileId = 0;
    UINT4               u4ProfileId = 0;
    UINT1               au1ZeroMac[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (pRsnaPaePortEntry->RsnaConfigDB.bRSNAOptionImplemented ==
        (BOOL1) i4SetValDot11RSNAEnabled)
    {
        return SNMP_SUCCESS;
    }

    pRsnaPaePortEntry->RsnaConfigDB.bRSNAOptionImplemented =
        (BOOL1) i4SetValDot11RSNAEnabled;

    /* Gets the profile id by passing the ifindex */
    WssGetCapwapWlanId (i4IfIndex, &u4ProfileId);

    /* Iterates through the capwapDot11WlanBindTable until the wlanprofileid of the table
       matches with the given profile id */

    if (nmhGetFirstIndexCapwapDot11WlanBindTable (&i4NextRadioIfIndex,
                                                  &u4NextWlanProfileId) !=
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    do
    {
        i4RadioIfIndex = i4NextRadioIfIndex;
        u4WlanProfileId = u4NextWlanProfileId;

        if (u4NextWlanProfileId == u4ProfileId)
        {
            /* If a match is found , then use the profileid & the corresponding 
             * i4RadioIfIndex to get the BSS interface index from the 
             * capwapDot11WlanBindTable */

            if (nmhGetCapwapDot11WlanBindBssIfIndex (i4RadioIfIndex,
                                                     u4WlanProfileId,
                                                     &i4BssIntfIndex) ==
                SNMP_SUCCESS)
            {

                WssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
                    (UINT4) i4BssIntfIndex;
                WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

                /* Gets the BSSId from WssWlanDB by passing the BSS interface index */

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    continue;
                }

                MEMCPY (au1BssId, WssWlanDB.WssWlanAttributeDB.BssId,
                        MAC_ADDR_LEN);
                MEMSET (au1TmpBSSID, 0, MAC_ADDR_LEN);
                /*Compares the BSSid with the zero Mac to identify whether the WTP is binded or not */

                if (MEMCMP
                    (WssWlanDB.WssWlanAttributeDB.BssId, au1ZeroMac,
                     MAC_ADDR_LEN) == 0)
                {
                    continue;
                }
            }
            if (pRsnaPaePortEntry->RsnaConfigDB.bRSNAOptionImplemented ==
                RSNA_DISABLED)
            {

                TMO_SLL_Scan (&(pRsnaPaePortEntry->RsnaSupInfoList),
                              pRsnaSupInfo, tRsnaSupInfo *)
                {
                    WssIfGetBSSIDofConnectedStation (pRsnaSupInfo->
                                                     au1SuppMacAddr,
                                                     au1TmpBSSID);
                    if (MEMCMP (au1BssId, au1TmpBSSID, MAC_ADDR_LEN) == 0)
                    {

                        if (pRsnaSupInfo->pRsnaSessNode != NULL)
                        {
                            WssRsnaSendDeAuthMsg (au1BssId,
                                                  pRsnaSupInfo->au1SuppMacAddr);
                            WssStaDeleteRsnaSessionKey (pRsnaSupInfo->
                                                        au1SuppMacAddr,
                                                        pRsnaSupInfo->
                                                        pRsnaSessNode->
                                                        u4BssIfIndex);
                            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
                        }

                        pRsnaSupInfo = NULL;
                        MEMSET (au1TmpBSSID, 0, MAC_ADDR_LEN);
                    }
                }

            }
        }
    }
    while (nmhGetNextIndexCapwapDot11WlanBindTable (i4RadioIfIndex,
                                                    &i4NextRadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4NextWlanProfileId) ==
           SNMP_SUCCESS);

#ifdef WPS_WANTED
    wpsSendUpdateFromRsna (i4IfIndex);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAPreauthenticationEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAPreauthenticationEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAPreauthenticationEnabled (INT4 i4IfIndex,
                                          INT4
                                          i4SetValDot11RSNAPreauthenticationEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.bRSNAPreauthenticationImplemented
        = (BOOL1) i4SetValDot11RSNAPreauthenticationEnabled;

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAEnabled (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4TestValDot11RSNAEnabled)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
#ifdef WPA_WANTED
    INT4                i4MixMode = 0;
    INT4                i4WpaMode = 0;
#endif
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return (SNMP_FAILURE);
    }

    if (i4TestValDot11RSNAEnabled != RSNA_ENABLED)
    {
        if (i4TestValDot11RSNAEnabled != RSNA_DISABLED)
        {
            CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
            return SNMP_FAILURE;
        }

    }
#ifdef WPA_WANTED
    /*Checking weather mix mode is enabled or not */
    if (nmhGetFsRSNAConfigMixMode (i4IfIndex, &i4MixMode) == SNMP_SUCCESS)
    {
        if (i4MixMode != MIX_MODE_ENABLED)
        {
            if (nmhGetFsRSNAConfigActivated (i4IfIndex, &i4WpaMode) ==
                SNMP_SUCCESS)
            {
                if (i4WpaMode == WPA_ENABLED)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "Mix Mode is not enabled!! ... \n");
                    CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
                    return (SNMP_FAILURE);
                }
            }
            else
            {
                CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return SNMP_FAILURE;
    }
#endif
    /* This function Checks whether Authentication Algorithm in the system
     * has been set to 'Open' or 'Shared-Key' before Setting RSNA to TRUE */
    if (i4TestValDot11RSNAEnabled == RSNA_ENABLED)
    {
        if (RsnaIsOpenAuthEnabled (i4IfIndex) != RSNA_SUCCESS)
        {
            CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAPreauthenticationEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAPreauthenticationEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAPreauthenticationEnabled (UINT4 *pu4ErrorCode,
                                             INT4 i4IfIndex,
                                             INT4
                                             i4TestValDot11RSNAPreauthenticationEnabled)
{

#ifdef UNUSED_CODE
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4TestValDot11RSNAPreauthenticationEnabled != RSNA_ENABLED)
    {
        if (i4TestValDot11RSNAPreauthenticationEnabled != RSNA_DISABLED)
        {
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValDot11RSNAPreauthenticationEnabled);
    *pu4ErrorCode = CLI_RSNA_COMMAND_NOT_SUPPORTED;
    return SNMP_FAILURE;

#endif

}

/* LOW LEVEL Routines for Table : Dot11RSNAConfigTable. */

/****************************************************************************
 Function    :  RsnaValidateIndexInstanceDot11RSNAConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
RsnaValidateIndexInstanceDot11RSNAConfigTable (INT4 i4IfIndex)
{
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetFirstIndexDot11RSNAConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRS Routine. */

INT1
RsnaGetFirstIndexDot11RSNAConfigTable (INT4 *pi4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4IfIndex = pRsnaPaePortEntry->u2Port;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetNextIndexDot11RSNAConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
RsnaGetNextIndexDot11RSNAConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;

    if (i4IfIndex == 0)
    {
        /* Fetch the data structure of the first index */
        RsnaGetNextPortEntry (NULL, &pNextRsnaPaePortEntry);

        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }
    }
    else
    {
        /* fetch the data structure of the current index */
        RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "No Rsna Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }

        /* pass the data structure of the current index and get 
         * the data structure of the next index*/
        RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);

        /* if next index data structure is not NULL assign the
         * value for next index*/
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }

    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigVersion
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigVersion (INT4 i4IfIndex,
                               INT4 *pi4RetValDot11RSNAConfigVersion)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAConfigVersion =
        (INT4) pRsnaPaePortEntry->RsnaConfigDB.u4Version;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPairwiseKeysSupported
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigPairwiseKeysSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPairwiseKeysSupported (INT4 i4IfIndex,
                                             UINT4
                                             *pu4RetValDot11RSNAConfigPairwiseKeysSupported)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigPairwiseKeysSupported =
        pRsnaPaePortEntry->RsnaConfigDB.u4PairwiseKeysSupported;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupCipher (INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValDot11RSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAConfigGroupCipher->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipher,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAConfigGroupCipher->i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupRekeyMethod
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupRekeyMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupRekeyMethod (INT4 i4IfIndex,
                                        INT4
                                        *pi4RetValDot11RSNAConfigGroupRekeyMethod)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAConfigGroupRekeyMethod =
        pRsnaPaePortEntry->RsnaConfigDB.u1GroupRekeyMethod;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupRekeyTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupRekeyTime (INT4 i4IfIndex,
                                      UINT4
                                      *pu4RetValDot11RSNAConfigGroupRekeyTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigGroupRekeyTime =
        pRsnaPaePortEntry->RsnaConfigDB.u4GroupGtkRekeyTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupRekeyPackets
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupRekeyPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupRekeyPackets (INT4 i4IfIndex,
                                         UINT4
                                         *pu4RetValDot11RSNAConfigGroupRekeyPackets)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigGroupRekeyPackets =
        pRsnaPaePortEntry->RsnaConfigDB.u4GroupRekeyPkts;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupRekeyStrict
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupRekeyStrict
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupRekeyStrict (INT4 i4IfIndex,
                                        INT4
                                        *pi4RetValDot11RSNAConfigGroupRekeyStrict)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (OSIX_TRUE == pRsnaPaePortEntry->RsnaConfigDB.bGroupRekeyStrict)
    {
        *pi4RetValDot11RSNAConfigGroupRekeyStrict = RSNA_ENABLED;
    }
    else
    {
        *pi4RetValDot11RSNAConfigGroupRekeyStrict = RSNA_DISABLED;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPSKValue
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigPSKValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPSKValue (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot11RSNAConfigPSKValue)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
#ifdef ISS_WANTED
    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        MEMCPY (pRetValDot11RSNAConfigPSKValue->pu1_OctetList,
                pRsnaPaePortEntry->RsnaConfigDB.au1ConfigPsk, RSNA_PSK_LEN);

        pRetValDot11RSNAConfigPSKValue->i4_Length = RSNA_PSK_LEN;
        return SNMP_SUCCESS;
    }
#endif
    MEMSET (pRetValDot11RSNAConfigPSKValue->pu1_OctetList, 0, RSNA_PSK_LEN);
    pRetValDot11RSNAConfigPSKValue->i4_Length = 0;    /*  RSNA_PSK_LEN; */

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPSKPassPhrase
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigPSKPassPhrase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPSKPassPhrase (INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot11RSNAConfigPSKPassPhrase)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
#ifdef ISS_WANTED
    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        MEMCPY (pRetValDot11RSNAConfigPSKPassPhrase->pu1_OctetList,
                pRsnaPaePortEntry->RsnaConfigDB.au1PskPassPhrase,
                RSNA_MAX_PSK_LENGTH);

        pRetValDot11RSNAConfigPSKPassPhrase->i4_Length = RSNA_MAX_PSK_LENGTH;
        return SNMP_SUCCESS;
    }
#endif

    MEMSET (pRetValDot11RSNAConfigPSKPassPhrase->pu1_OctetList, 0,
            RSNA_MAX_PSK_LENGTH);

    pRetValDot11RSNAConfigPSKPassPhrase->i4_Length = 0;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupUpdateCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupUpdateCount (INT4 i4IfIndex,
                                        UINT4
                                        *pu4RetValDot11RSNAConfigGroupUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigGroupUpdateCount =
        pRsnaPaePortEntry->RsnaConfigDB.u4GroupUpdateCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPairwiseUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigPairwiseUpdateCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPairwiseUpdateCount (INT4 i4IfIndex,
                                           UINT4
                                           *pu4RetValDot11RSNAConfigPairwiseUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigPairwiseUpdateCount =
        pRsnaPaePortEntry->RsnaConfigDB.u4PairwiseUpdateCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigGroupCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupCipherSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigGroupCipherSize (INT4 i4IfIndex,
                                       UINT4
                                       *pu4RetValDot11RSNAConfigGroupCipherSize)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigGroupCipherSize =
        pRsnaPaePortEntry->RsnaConfigDB.u4GroupCipherKeyLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigPMKLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPMKLifetime (INT4 i4IfIndex,
                                   UINT4 *pu4RetValDot11RSNAConfigPMKLifetime)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigPMKLifetime =
        pRsnaPaePortEntry->RsnaConfigDB.u4PMKLifeTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigPMKReauthThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPMKReauthThreshold (INT4 i4IfIndex,
                                          UINT4
                                          *pu4RetValDot11RSNAConfigPMKReauthThreshold)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigPMKReauthThreshold =
        pRsnaPaePortEntry->RsnaConfigDB.u4PmkReAuthThreshold;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigNumberOfPTKSAReplayCounters
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigNumberOfPTKSAReplayCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigNumberOfPTKSAReplayCounters (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValDot11RSNAConfigNumberOfPTKSAReplayCounters)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAConfigNumberOfPTKSAReplayCounters =
        (INT4) pRsnaPaePortEntry->RsnaConfigDB.u4NoOfPtkSaReplayCtr;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigSATimeout
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigSATimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigSATimeout (INT4 i4IfIndex,
                                 UINT4 *pu4RetValDot11RSNAConfigSATimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigSATimeout =
        pRsnaPaePortEntry->RsnaConfigDB.u4SATimeOut;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAAuthenticationSuiteSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAAuthenticationSuiteSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAAuthenticationSuiteSelected (INT4 i4IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValDot11RSNAAuthenticationSuiteSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAAuthenticationSuiteSelected->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1AuthSuiteSelected,
            RSNA_MAX_AUTH_SUITE_TYPE_LENGTH);

    pRetValDot11RSNAAuthenticationSuiteSelected->i4_Length =
        RSNA_MAX_AUTH_SUITE_TYPE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAPairwiseCipherSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAPairwiseCipherSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAPairwiseCipherSelected (INT4 i4IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValDot11RSNAPairwiseCipherSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAPairwiseCipherSelected->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1PairwiseCipherSelected,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAPairwiseCipherSelected->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAGroupCipherSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAGroupCipherSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAGroupCipherSelected (INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot11RSNAGroupCipherSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAGroupCipherSelected->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipherSelected,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAGroupCipherSelected->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAPMKIDUsed
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAPMKIDUsed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAPMKIDUsed (INT4 i4IfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValDot11RSNAPMKIDUsed)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAPMKIDUsed->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1PmkIdUsed, RSNA_PMKID_LEN);

    pRetValDot11RSNAPMKIDUsed->i4_Length = RSNA_MAX_PMKID_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAAuthenticationSuiteRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAAuthenticationSuiteRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAAuthenticationSuiteRequested (INT4 i4IfIndex,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pRetValDot11RSNAAuthenticationSuiteRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAAuthenticationSuiteRequested->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1AuthSuiteRequested,
            RSNA_MAX_AUTH_SUITE_TYPE_LENGTH);

    pRetValDot11RSNAAuthenticationSuiteRequested->i4_Length =
        RSNA_MAX_AUTH_SUITE_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAPairwiseCipherRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAPairwiseCipherRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAPairwiseCipherRequested (INT4 i4IfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValDot11RSNAPairwiseCipherRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAPairwiseCipherRequested->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1PairwiseCipherRequested,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAPairwiseCipherRequested->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAGroupCipherRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAGroupCipherRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAGroupCipherRequested (INT4 i4IfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValDot11RSNAGroupCipherRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAGroupCipherRequested->pu1_OctetList,
            pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipherRequested,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAGroupCipherRequested->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNATKIPCounterMeasuresInvoked
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNATKIPCounterMeasuresInvoked
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNATKIPCounterMeasuresInvoked (INT4 i4IfIndex,
                                            UINT4
                                            *pu4RetValDot11RSNATKIPCounterMeasuresInvoked)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNATKIPCounterMeasuresInvoked =
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOfCtrMsrsInvoked;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNA4WayHandshakeFailures
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNA4WayHandshakeFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNA4WayHandshakeFailures (INT4 i4IfIndex,
                                       UINT4
                                       *pu4RetValDot11RSNA4WayHandshakeFailures)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNA4WayHandshakeFailures =
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOf4WayHandshakeFailures;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigNumberOfGTKSAReplayCounters
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigNumberOfGTKSAReplayCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigNumberOfGTKSAReplayCounters (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValDot11RSNAConfigNumberOfGTKSAReplayCounters)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAConfigNumberOfGTKSAReplayCounters =
        (INT4) pRsnaPaePortEntry->RsnaConfigDB.u4NoOfGtkReplayCtrs;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigGroupCipher
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigGroupCipher (INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValDot11RSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipher,
            pSetValDot11RSNAConfigGroupCipher->pu1_OctetList,
            pSetValDot11RSNAConfigGroupCipher->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigGroupRekeyMethod
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigGroupRekeyMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigGroupRekeyMethod (INT4 i4IfIndex,
                                        INT4
                                        i4SetValDot11RSNAConfigGroupRekeyMethod)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (((i4SetValDot11RSNAConfigGroupRekeyMethod == RSNA_GROUP_REKEY_TIMEBASED)
         || (i4SetValDot11RSNAConfigGroupRekeyMethod ==
             RSNA_GROUP_REKEY_TIMEPACKETBASED))
        && (pRsnaPaePortEntry->RsnaConfigDB.u1GroupRekeyMethod ==
            RSNA_GROUP_REKEY_DISABLED))
    {
        if (RsnaGtkRekeyReInitiate (pRsnaPaePortEntry) == RSNA_FAILURE)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaGtkRekeyReInitiate failed");
            return SNMP_FAILURE;
        }
    }

    /* Stop GTK Rekey timer if the rekey method is disabled */
    if ((i4SetValDot11RSNAConfigGroupRekeyMethod == RSNA_GROUP_REKEY_DISABLED)
        &&
        ((pRsnaPaePortEntry->RsnaConfigDB.u1GroupRekeyMethod ==
          RSNA_GROUP_REKEY_TIMEBASED)
         || (pRsnaPaePortEntry->RsnaConfigDB.u1GroupRekeyMethod ==
             RSNA_GROUP_REKEY_TIMEPACKETBASED)))
    {
        RsnaTmrStopTimer (&
                          (pRsnaPaePortEntry->RsnaGlobalGtk.
                           RsnaUtilGtkRekeyTmr));
    }

    pRsnaPaePortEntry->RsnaConfigDB.u1GroupRekeyMethod =
        (UINT1) i4SetValDot11RSNAConfigGroupRekeyMethod;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigGroupRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigGroupRekeyTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigGroupRekeyTime (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValDot11RSNAConfigGroupRekeyTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4GroupGtkRekeyTime =
        u4SetValDot11RSNAConfigGroupRekeyTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigGroupRekeyPackets
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigGroupRekeyPackets
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigGroupRekeyPackets (INT4 i4IfIndex,
                                         UINT4
                                         u4SetValDot11RSNAConfigGroupRekeyPackets)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4GroupRekeyPkts =
        u4SetValDot11RSNAConfigGroupRekeyPackets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigGroupRekeyStrict
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigGroupRekeyStrict
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigGroupRekeyStrict (INT4 i4IfIndex,
                                        INT4
                                        i4SetValDot11RSNAConfigGroupRekeyStrict)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4SetValDot11RSNAConfigGroupRekeyStrict == RSNA_ENABLED)
    {
        pRsnaPaePortEntry->RsnaConfigDB.bGroupRekeyStrict = OSIX_TRUE;
    }
    else
    {
        pRsnaPaePortEntry->RsnaConfigDB.bGroupRekeyStrict = OSIX_FALSE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigPSKValue
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigPSKValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigPSKValue (INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValDot11RSNAConfigPSKValue)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMSET (pRsnaPaePortEntry->RsnaConfigDB.au1ConfigPsk, 0, RSNA_PSK_LEN);

    MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1ConfigPsk,
            pSetValDot11RSNAConfigPSKValue->pu1_OctetList,
            pSetValDot11RSNAConfigPSKValue->i4_Length);
#ifdef WPS_WANTED
    wpsSendPskUpdateFromRsna (i4IfIndex);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigPSKPassPhrase
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigPSKPassPhrase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigPSKPassPhrase (INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValDot11RSNAConfigPSKPassPhrase)
{
    UINT1               au1Dot11DesiredSSID[RSNA_SSID_LEN] = "";
    UINT1               au1Psk[RSNA_PASS_PHRASE_LEN] = "";
    UINT1               au1PskPassPhrase[RSNA_PASS_PHRASE_LEN] = "";
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    MEMSET (au1Dot11DesiredSSID, 0, RSNA_SSID_LEN);
    MEMSET (au1PskPassPhrase, 0, RSNA_PASS_PHRASE_LEN);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMSET (pRsnaPaePortEntry->RsnaConfigDB.au1PskPassPhrase, 0,
            RSNA_PASS_PHRASE_LEN);

    MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1PskPassPhrase,
            pSetValDot11RSNAConfigPSKPassPhrase->pu1_OctetList,
            pSetValDot11RSNAConfigPSKPassPhrase->i4_Length);

    if (MEMCMP (pRsnaPaePortEntry->RsnaConfigDB.au1PskPassPhrase,
                au1PskPassPhrase, RSNA_PASS_PHRASE_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    /* The pass phrase has to be converted into PSK 
     * Get the ssid name from WSS module and call the function 
     * RsnaAlgoConvertPassPhraseToPsk to convert passphrase to PSK*/

    WssCfGetDot11DesiredSSID ((UINT4) i4IfIndex, au1Dot11DesiredSSID);

    RsnaAlgoConvertPassPhraseToPsk (au1Dot11DesiredSSID,
                                    pRsnaPaePortEntry->RsnaConfigDB.
                                    au1PskPassPhrase,
                                    au1Psk,
                                    (UINT1) (STRLEN (au1Dot11DesiredSSID)));

    /* Store the PSK value in the database, as it will be used
     * to generated the PMK */
    MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1ConfigPsk, au1Psk, RSNA_PSK_LEN);
#ifdef WPS_WANTED
    wpsSendPskUpdateFromRsna (i4IfIndex);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigGroupUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigGroupUpdateCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigGroupUpdateCount (INT4 i4IfIndex,
                                        UINT4
                                        u4SetValDot11RSNAConfigGroupUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4GroupUpdateCount =
        u4SetValDot11RSNAConfigGroupUpdateCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigPairwiseUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigPairwiseUpdateCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigPairwiseUpdateCount (INT4 i4IfIndex,
                                           UINT4
                                           u4SetValDot11RSNAConfigPairwiseUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4PairwiseUpdateCount =
        u4SetValDot11RSNAConfigPairwiseUpdateCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigPMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigPMKLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigPMKLifetime (INT4 i4IfIndex,
                                   UINT4 u4SetValDot11RSNAConfigPMKLifetime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4PMKLifeTime =
        u4SetValDot11RSNAConfigPMKLifetime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigPMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigPMKReauthThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigPMKReauthThreshold (INT4 i4IfIndex,
                                          UINT4
                                          u4SetValDot11RSNAConfigPMKReauthThreshold)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4PmkReAuthThreshold =
        u4SetValDot11RSNAConfigPMKReauthThreshold;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigSATimeout
 Input       :  The Indices
                IfIndex

                The Object 
                setValDot11RSNAConfigSATimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigSATimeout (INT4 i4IfIndex,
                                 UINT4 u4SetValDot11RSNAConfigSATimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u4SATimeOut =
        u4SetValDot11RSNAConfigSATimeout;

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigGroupCipher
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigGroupCipher (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValDot11RSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    INT4                i4PairwiseCipherStatus = 0;
#ifdef WPA_WANTED
    INT4                i4MixModeStatus = 0;
#endif
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
#ifdef WPA_WANTED
    if (MEMCMP (pTestValDot11RSNAConfigGroupCipher->pu1_OctetList,
                gau1RsnaCipherSuiteCCMP, RSNA_MAX_CIPHER_TYPE_LENGTH) == 0)
    {
        if (nmhGetFsRSNAConfigMixMode (i4IfIndex, &i4MixModeStatus) ==
            SNMP_SUCCESS)
        {
            if (i4MixModeStatus == MIX_MODE_ENABLED)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "AES is not supported for Group Cipher in Mix-Mode!! ... \n");
                CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
                return (SNMP_FAILURE);
            }
        }
    }
#endif
    /** checks if the cipher entered is not equal to both TKIP and CCMP **/
    if (MEMCMP (pTestValDot11RSNAConfigGroupCipher->pu1_OctetList,
                gau1RsnaCipherSuiteCCMP, RSNA_MAX_CIPHER_TYPE_LENGTH) != 0)
    {
        if (MEMCMP (pTestValDot11RSNAConfigGroupCipher->pu1_OctetList,
                    gau1RsnaCipherSuiteTKIP, RSNA_MAX_CIPHER_TYPE_LENGTH) != 0)
        {
            *pu4ErrorCode = CLI_RSNA_INVALID_INVALID_GRP_CIPHER;
            CLI_SET_ERR (CLI_RSNA_INVALID_INVALID_GRP_CIPHER);

            return SNMP_FAILURE;
        }

    }

    /* Checks whether the Pairwise and Group cipher combination is valid */

    if ((MEMCMP (pTestValDot11RSNAConfigGroupCipher->pu1_OctetList,
                 gau1RsnaCipherSuiteCCMP, RSNA_MAX_CIPHER_TYPE_LENGTH)) == 0)
    {

        RsnaGetDot11RSNAConfigPairwiseCipherEnabled (i4IfIndex,
                                                     RSNA_CCMP,
                                                     &i4PairwiseCipherStatus);

        if (i4PairwiseCipherStatus == RSNA_DISABLED)
        {
            CLI_SET_ERR (CLI_RSNA_INCOMPATIBLE_PAIRWISE_AND_GROUPWISE_CIPHER);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigGroupRekeyMethod
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigGroupRekeyMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyMethod (UINT4 *pu4ErrorCode,
                                           INT4 i4IfIndex,
                                           INT4
                                           i4TestValDot11RSNAConfigGroupRekeyMethod)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC ((RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC),
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((i4TestValDot11RSNAConfigGroupRekeyMethod
         == RSNA_GROUP_REKEY_DISABLED) ||
        (i4TestValDot11RSNAConfigGroupRekeyMethod
         == RSNA_GROUP_REKEY_TIMEBASED)
        || (i4TestValDot11RSNAConfigGroupRekeyMethod ==
            RSNA_GROUP_REKEY_PACKETBASED)
        || (i4TestValDot11RSNAConfigGroupRekeyMethod ==
            RSNA_GROUP_REKEY_TIMEPACKETBASED))

    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_INVALID_GRP_METHOD;
        CLI_SET_ERR (CLI_RSNA_INVALID_INVALID_GRP_METHOD);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigGroupRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigGroupRekeyTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4
                                         u4TestValDot11RSNAConfigGroupRekeyTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinGrpRekeyTime =
        (UINT4) RSNA_MIN_GROUP_REKEY_TIME;
    UINT4               u4RsnaMaxGrpRekeyTime =
        (UINT4) RSNA_MAX_GROUP_REKEY_TIME;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigGroupRekeyTime < u4RsnaMinGrpRekeyTime)
        || (u4TestValDot11RSNAConfigGroupRekeyTime > u4RsnaMaxGrpRekeyTime))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_REKEY_TIME;
        CLI_SET_ERR (CLI_RSNA_INVALID_REKEY_TIME);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigGroupRekeyPackets
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigGroupRekeyPackets
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyPackets (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                            UINT4
                                            u4TestValDot11RSNAConfigGroupRekeyPackets)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinGrpRekeyPkts =
        (UINT4) RSNA_MIN_GROUP_REKEY_PKTS;
    UINT4               u4RsnaMaxGrpRekeyPkts =
        (UINT4) RSNA_MAX_GROUP_REKEY_PKTS;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigGroupRekeyPackets < u4RsnaMinGrpRekeyPkts) ||
        (u4TestValDot11RSNAConfigGroupRekeyPackets > u4RsnaMaxGrpRekeyPkts))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_REKEY_PKT;
        CLI_SET_ERR (CLI_RSNA_INVALID_REKEY_PKT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigGroupRekeyStrict
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigGroupRekeyStrict
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigGroupRekeyStrict (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                           INT4
                                           i4TestValDot11RSNAConfigGroupRekeyStrict)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((i4TestValDot11RSNAConfigGroupRekeyStrict == RSNA_ENABLED) ||
        (i4TestValDot11RSNAConfigGroupRekeyStrict == RSNA_DISABLED))
    {
        return SNMP_SUCCESS;
    }

    else
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_INVALID_GRP_STRICT;
        CLI_SET_ERR (CLI_RSNA_INVALID_INVALID_GRP_STRICT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigPSKValue
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigPSKValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigPSKValue (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValDot11RSNAConfigPSKValue)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((pTestValDot11RSNAConfigPSKValue->i4_Length > RSNA_MAX_PSK_VALUE) ||
        (pTestValDot11RSNAConfigPSKValue->i4_Length < RSNA_MIN_PSK_VALUE))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PSK_VALUE;
        CLI_SET_ERR (CLI_RSNA_INVALID_PSK_VALUE);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigPSKPassPhrase
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigPSKPassPhrase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigPSKPassPhrase (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValDot11RSNAConfigPSKPassPhrase)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    if ((pTestValDot11RSNAConfigPSKPassPhrase->i4_Length
         < RSNA_MIN_PASS_PHRASE_LEN) ||
        (pTestValDot11RSNAConfigPSKPassPhrase->i4_Length
         > RSNA_MAX_PASS_PHRASE_LEN))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_PASSPHRASE);
        return (SNMP_FAILURE);

    }
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigGroupUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigGroupUpdateCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigGroupUpdateCount (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                           UINT4
                                           u4TestValDot11RSNAConfigGroupUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinGrpUpdateCount =
        (UINT4) RSNA_MIN_GROUP_UPDATE_COUNT;
    UINT4               u4RsnaMaxGrpUpdateCount =
        (UINT4) RSNA_MAX_GROUP_UPDATE_COUNT;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigGroupUpdateCount < u4RsnaMinGrpUpdateCount) ||
        (u4TestValDot11RSNAConfigGroupUpdateCount > u4RsnaMaxGrpUpdateCount))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_GROUP_UPDATE_COUNT;
        CLI_SET_ERR (CLI_RSNA_INVALID_GROUP_UPDATE_COUNT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigPairwiseUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigPairwiseUpdateCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigPairwiseUpdateCount (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              UINT4
                                              u4TestValDot11RSNAConfigPairwiseUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinPairwiseUpdateCount =
        (UINT4) RSNA_MIN_PAIRWISE_UPDATE_COUNT;
    UINT4               u4RsnaMaxPairwiseUpdateCount =
        (UINT4) RSNA_MAX_PAIRWISE_UPDATE_COUNT;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigPairwiseUpdateCount <
         u4RsnaMinPairwiseUpdateCount)
        || (u4TestValDot11RSNAConfigPairwiseUpdateCount >
            u4RsnaMaxPairwiseUpdateCount))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PAIRWISE_UPDATE_COUNT;
        CLI_SET_ERR (CLI_RSNA_INVALID_PAIRWISE_UPDATE_COUNT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigPMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigPMKLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigPMKLifetime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      UINT4 u4TestValDot11RSNAConfigPMKLifetime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinPMKLifeTime = (UINT4) RSNA_MIN_PMK_LIFETIME;
    UINT4               u4RsnaMaxPMKLifeTime = (UINT4) RSNA_MAX_PMK_LIFETIME;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigPMKLifetime <
         u4RsnaMinPMKLifeTime) || (u4TestValDot11RSNAConfigPMKLifetime >
                                   u4RsnaMaxPMKLifeTime))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PMK_LIFETIME;
        CLI_SET_ERR (CLI_RSNA_INVALID_PMK_LIFETIME);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigPMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigPMKReauthThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigPMKReauthThreshold (UINT4 *pu4ErrorCode,
                                             INT4 i4IfIndex,
                                             UINT4
                                             u4TestValDot11RSNAConfigPMKReauthThreshold)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigPMKReauthThreshold <
         RSNA_MIN_PMK_REAUTH_THRESHOLD)
        || (u4TestValDot11RSNAConfigPMKReauthThreshold >
            RSNA_MAX_PMK_REAUTH_THRESHOLD))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PMK_REAUTH_THRESHOLD;
        CLI_SET_ERR (CLI_RSNA_INVALID_PMK_REAUTH_THRESHOLD);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigSATimeout
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNAConfigSATimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigSATimeout (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    UINT4 u4TestValDot11RSNAConfigSATimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinSaTimeout = (UINT4) RSNA_MIN_SA_TIMEOUT;
    UINT4               u4RsnaMaxSaTimeout = (UINT4) RSNA_MAX_SA_TIMEOUT;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValDot11RSNAConfigSATimeout < u4RsnaMinSaTimeout)
        || (u4TestValDot11RSNAConfigSATimeout > u4RsnaMaxSaTimeout))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_SA_TIMEOUT;
        CLI_SET_ERR (CLI_RSNA_INVALID_SA_TIMEOUT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNATKIPCounterMeasuresInvoked
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNATKIPCounterMeasuresInvoked
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNATKIPCounterMeasuresInvoked (UINT4 *pu4ErrorCode,
                                               INT4 i4IfIndex,
                                               UINT4
                                               u4TestValDot11RSNATKIPCounterMeasuresInvoked)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValDot11RSNATKIPCounterMeasuresInvoked);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNA4WayHandshakeFailures
 Input       :  The Indices
                IfIndex

                The Object 
                testValDot11RSNA4WayHandshakeFailures
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNA4WayHandshakeFailures (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          UINT4
                                          u4TestValDot11RSNA4WayHandshakeFailures)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValDot11RSNA4WayHandshakeFailures);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaDepv2Dot11RSNAConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaDepv2Dot11RSNAConfigTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot11RSNAConfigPairwiseCiphersTable. */

/****************************************************************************
 Function    :  RsnaValidateIndexInstanceDot11RSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
RsnaValidateIndexInstanceDot11RSNAConfigPairwiseCiphersTable (INT4 i4IfIndex,
                                                              UINT4
                                                              u4Dot11RSNAConfigPairwiseCipherIndex)
{
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    if ((u4Dot11RSNAConfigPairwiseCipherIndex < RSNA_MIN_CIPHER_SUITE_INDEX) ||
        (u4Dot11RSNAConfigPairwiseCipherIndex > RSNA_MAX_CIPHER_SUITE_INDEX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetFirstIndexDot11RSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
RsnaGetFirstIndexDot11RSNAConfigPairwiseCiphersTable (INT4 *pi4IfIndex,
                                                      UINT4
                                                      *pu4Dot11RSNAConfigPairwiseCipherIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    else
    {
        *pi4IfIndex = pRsnaPaePortEntry->u2Port;
    }

    *pu4Dot11RSNAConfigPairwiseCipherIndex = RSNA_MIN_CIPHER_SUITE_INDEX;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable

 Input       :  The Indices
                IfIndex
                nextIfIndex
                Dot11RSNAConfigPairwiseCipherIndex
                nextDot11RSNAConfigPairwiseCipherIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
RsnaGetNextIndexDot11RSNAConfigPairwiseCiphersTable (INT4 i4IfIndex,
                                                     INT4 *pi4NextIfIndex,
                                                     UINT4
                                                     u4Dot11RSNAConfigPairwiseCipherIndex,
                                                     UINT4
                                                     *pu4NextDot11RSNAConfigPairwiseCipherIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;
    if (i4IfIndex == 0)
    {
        RsnaGetFirstPortEntry (&pNextRsnaPaePortEntry);
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
            *pu4NextDot11RSNAConfigPairwiseCipherIndex =
                RSNA_MIN_CIPHER_SUITE_INDEX;
        }
    }
    else
    {
        if (u4Dot11RSNAConfigPairwiseCipherIndex == RSNA_MAX_CIPHER_SUITE_INDEX)
        {
            /* fetch the data structure of the current index */
            RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

            if (pRsnaPaePortEntry == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No Rsna Info for the given port!! ... \n");
                return (SNMP_FAILURE);
            }

            RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);
            if (pNextRsnaPaePortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
            *pu4NextDot11RSNAConfigPairwiseCipherIndex =
                RSNA_MIN_CIPHER_SUITE_INDEX;
        }
        else
        {
            *pi4NextIfIndex = i4IfIndex;
            *pu4NextDot11RSNAConfigPairwiseCipherIndex =
                u4Dot11RSNAConfigPairwiseCipherIndex +
                RSNA_MIN_CIPHER_SUITE_INDEX;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPairwiseCipher
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex

                The Object 
                retValDot11RSNAConfigPairwiseCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPairwiseCipher (INT4 i4IfIndex,
                                      UINT4
                                      u4Dot11RSNAConfigPairwiseCipherIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValDot11RSNAConfigPairwiseCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAConfigPairwiseCipher->pu1_OctetList,
            pRsnaPaePortEntry->
            aRsnaPwCipherDB[u4Dot11RSNAConfigPairwiseCipherIndex -
                            1].au1PairwiseCipher, RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAConfigPairwiseCipher->i4_Length
        = RSNA_MAX_CIPHER_TYPE_LENGTH;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPairwiseCipherEnabled
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex

                The Object 
                retValDot11RSNAConfigPairwiseCipherEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPairwiseCipherEnabled (INT4 i4IfIndex,
                                             UINT4
                                             u4Dot11RSNAConfigPairwiseCipherIndex,
                                             INT4
                                             *pi4RetValDot11RSNAConfigPairwiseCipherEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (pRsnaPaePortEntry->aRsnaPwCipherDB
        [u4Dot11RSNAConfigPairwiseCipherIndex - 1].bPairwiseCipherEnabled ==
        OSIX_TRUE)
    {

        *pi4RetValDot11RSNAConfigPairwiseCipherEnabled = RSNA_ENABLED;
    }

    else
    {
        *pi4RetValDot11RSNAConfigPairwiseCipherEnabled = RSNA_DISABLED;

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigPairwiseCipherSize
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex

                The Object 
                retValDot11RSNAConfigPairwiseCipherSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigPairwiseCipherSize (INT4 i4IfIndex,
                                          UINT4
                                          u4Dot11RSNAConfigPairwiseCipherIndex,
                                          UINT4
                                          *pu4RetValDot11RSNAConfigPairwiseCipherSize)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigPairwiseCipherSize =
        pRsnaPaePortEntry->aRsnaPwCipherDB
        [u4Dot11RSNAConfigPairwiseCipherIndex - 1].u4PairwiseCipherSize;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigPairwiseCipherEnabled
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex

                The Object 
                setValDot11RSNAConfigPairwiseCipherEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigPairwiseCipherEnabled (INT4 i4IfIndex,
                                             UINT4
                                             u4Dot11RSNAConfigPairwiseCipherIndex,
                                             INT4
                                             i4SetValDot11RSNAConfigPairwiseCipherEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4SetValDot11RSNAConfigPairwiseCipherEnabled == RSNA_ENABLED)
    {
        pRsnaPaePortEntry->aRsnaPwCipherDB
            [u4Dot11RSNAConfigPairwiseCipherIndex - 1].bPairwiseCipherEnabled
            = OSIX_TRUE;
#ifdef WPS_WANTED
        wpsSendEncrypUpdateFromRsna (i4IfIndex,
                                     u4Dot11RSNAConfigPairwiseCipherIndex,
                                     i4SetValDot11RSNAConfigPairwiseCipherEnabled);
#endif
        return SNMP_SUCCESS;
    }
    else if (i4SetValDot11RSNAConfigPairwiseCipherEnabled == RSNA_DISABLED)
    {
        pRsnaPaePortEntry->aRsnaPwCipherDB
            [u4Dot11RSNAConfigPairwiseCipherIndex - 1].bPairwiseCipherEnabled
            = OSIX_FALSE;
#ifdef WPS_WANTED
        wpsSendEncrypUpdateFromRsna (i4IfIndex,
                                     u4Dot11RSNAConfigPairwiseCipherIndex,
                                     i4SetValDot11RSNAConfigPairwiseCipherEnabled);
#endif
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigPairwiseCipherEnabled
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex

                The Object 
                testValDot11RSNAConfigPairwiseCipherEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigPairwiseCipherEnabled (UINT4
                                                *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                UINT4
                                                u4Dot11RSNAConfigPairwiseCipherIndex,
                                                INT4
                                                i4TestValDot11RSNAConfigPairwiseCipherEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tSNMP_OCTET_STRING_TYPE tGroupCipher = { NULL, 0 };
    UINT1               au1GroupCipher[RSNA_MAX_CIPHER_TYPE_LENGTH];

    MEMSET (au1GroupCipher, 0, RSNA_MAX_CIPHER_TYPE_LENGTH);

    tGroupCipher.pu1_OctetList = au1GroupCipher;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (((u4Dot11RSNAConfigPairwiseCipherIndex != RSNA_TKIP)
         && (u4Dot11RSNAConfigPairwiseCipherIndex != RSNA_CCMP)) ||
        ((i4TestValDot11RSNAConfigPairwiseCipherEnabled != RSNA_ENABLED)
         && (i4TestValDot11RSNAConfigPairwiseCipherEnabled != RSNA_DISABLED)))
    {
        return SNMP_FAILURE;
    }

    if (i4TestValDot11RSNAConfigPairwiseCipherEnabled == RSNA_ENABLED)
    {
        if (u4Dot11RSNAConfigPairwiseCipherIndex == RSNA_TKIP)
        {
            RsnaGetDot11RSNAConfigGroupCipher (i4IfIndex, &tGroupCipher);

            if ((MEMCMP (tGroupCipher.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                         RSNA_MAX_CIPHER_TYPE_LENGTH)) == 0)
            {
                CLI_SET_ERR
                    (CLI_RSNA_INCOMPATIBLE_PAIRWISE_AND_GROUPWISE_CIPHER);
                return SNMP_FAILURE;
            }
        }
    }
    else if (i4TestValDot11RSNAConfigPairwiseCipherEnabled == RSNA_DISABLED)
    {
        if (u4Dot11RSNAConfigPairwiseCipherIndex == RSNA_CCMP)
        {
            RsnaGetDot11RSNAConfigGroupCipher (i4IfIndex, &tGroupCipher);

            if ((MEMCMP (tGroupCipher.pu1_OctetList, gau1RsnaCipherSuiteCCMP,
                         RSNA_MAX_CIPHER_TYPE_LENGTH)) == 0)
            {
                CLI_SET_ERR
                    (CLI_RSNA_INCOMPATIBLE_PAIRWISE_AND_GROUPWISE_CIPHER);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaDepv2Dot11RSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                Dot11RSNAConfigPairwiseCipherIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaDepv2Dot11RSNAConfigPairwiseCiphersTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot11RSNAConfigAuthenticationSuitesTable. */

/****************************************************************************
 Function    :  RsnaValidateIndexInstanceDot11RSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    RsnaValidateIndexInstanceDot11RSNAConfigAuthenticationSuitesTable
    (UINT4 u4Dot11RSNAConfigAuthenticationSuiteIndex)
{

    if ((u4Dot11RSNAConfigAuthenticationSuiteIndex <
         RSNA_MIN_AUTH_SUITE_INDEX) ||
        (u4Dot11RSNAConfigAuthenticationSuiteIndex > RSNA_MAX_AUTH_SUITE_INDEX))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetFirstIndexDot11RSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
RsnaGetFirstIndexDot11RSNAConfigAuthenticationSuitesTable (INT4 *pi4IfIndex,
                                                           UINT4
                                                           *pu4Dot11RSNAConfigAuthenticationSuiteIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    else
    {
        *pi4IfIndex = (INT4) pRsnaPaePortEntry->u2Port;
    }

    *pu4Dot11RSNAConfigAuthenticationSuiteIndex = RSNA_MIN_AUTH_SUITE_INDEX;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex
                nextDot11RSNAConfigAuthenticationSuiteIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                          INT4 *pi4NextIfIndex,
                                                          UINT4
                                                          u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                          UINT4
                                                          *pu4NextDot11RSNAConfigAuthenticationSuiteIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;
    if (i4IfIndex == 0)
    {
        RsnaGetFirstPortEntry (&pNextRsnaPaePortEntry);
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = (INT4) pNextRsnaPaePortEntry->u2Port;
            *pu4NextDot11RSNAConfigAuthenticationSuiteIndex =
                RSNA_MIN_AUTH_SUITE_INDEX;
        }
    }
    else
    {
        if (u4Dot11RSNAConfigAuthenticationSuiteIndex <
            RSNA_MAX_AUTH_SUITE_INDEX)
        {
            *pi4NextIfIndex = i4IfIndex;
            *pu4NextDot11RSNAConfigAuthenticationSuiteIndex =
                u4Dot11RSNAConfigAuthenticationSuiteIndex +
                RSNA_MIN_AUTH_SUITE_INDEX;
        }
        else if (u4Dot11RSNAConfigAuthenticationSuiteIndex >
                 RSNA_MAX_AUTH_SUITE_INDEX)
        {
            return SNMP_FAILURE;
        }
        else
        {
            /* fetch the data structure of the current index */
            RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

            if (pRsnaPaePortEntry == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No Rsna Info for the given port!! ... \n");
                return (SNMP_FAILURE);
            }

            RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);
            if (pNextRsnaPaePortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            *pi4NextIfIndex = (INT4) pNextRsnaPaePortEntry->u2Port;
            *pu4NextDot11RSNAConfigAuthenticationSuiteIndex
                = RSNA_MIN_AUTH_SUITE_INDEX;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigAuthenticationSuite
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                retValDot11RSNAConfigAuthenticationSuite
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigAuthenticationSuite (UINT4 u4ProfileId,
                                           UINT4
                                           u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValDot11RSNAConfigAuthenticationSuite)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) u4ProfileId, &pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAConfigAuthenticationSuite->pu1_OctetList,
            pRsnaPaePortEntry->
            aRsnaAkmDB[u4Dot11RSNAConfigAuthenticationSuiteIndex -
                       1].au1AuthSuite, RSNA_AKM_SELECTOR_LEN);

    pRetValDot11RSNAConfigAuthenticationSuite->i4_Length =
        (INT4) RSNA_AKM_SELECTOR_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                retValDot11RSNAConfigAuthenticationSuiteEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled (INT4 i4IfIndex,
                                                  UINT4
                                                  u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                  INT4
                                                  *pi4RetValDot11RSNAConfigAuthenticationSuiteEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (pRsnaPaePortEntry->aRsnaAkmDB
        [u4Dot11RSNAConfigAuthenticationSuiteIndex - 1].bAkmSuiteEnabled ==
        OSIX_TRUE)
    {

        *pi4RetValDot11RSNAConfigAuthenticationSuiteEnabled =
            (INT4) RSNA_ENABLED;
    }
    else
    {
        *pi4RetValDot11RSNAConfigAuthenticationSuiteEnabled =
            (INT4) RSNA_DISABLED;
    }

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaSetDot11RSNAConfigAuthenticationSuiteEnabled
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                setValDot11RSNAConfigAuthenticationSuiteEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAConfigAuthenticationSuiteEnabled (UINT4 u4ProfileId,
                                                  UINT4
                                                  u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                  INT4
                                                  i4SetValDot11RSNAConfigAuthenticationSuiteEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) u4ProfileId, &pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return SNMP_FAILURE;
    }

    pRsnaPaePortEntry->RsnaConfigDB.u1AKMSuiteMethod =
        (UINT1) u4Dot11RSNAConfigAuthenticationSuiteIndex;
    if (i4SetValDot11RSNAConfigAuthenticationSuiteEnabled ==
        (INT4) RSNA_ENABLED)
    {
        pRsnaPaePortEntry->aRsnaAkmDB
            [u4Dot11RSNAConfigAuthenticationSuiteIndex - 1].bAkmSuiteEnabled
            = OSIX_TRUE;
        return SNMP_SUCCESS;
    }
    else if (i4SetValDot11RSNAConfigAuthenticationSuiteEnabled ==
             (INT4) RSNA_DISABLED)
    {
        pRsnaPaePortEntry->aRsnaAkmDB
            [u4Dot11RSNAConfigAuthenticationSuiteIndex - 1].bAkmSuiteEnabled
            = OSIX_FALSE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  RsnaValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                u4FsRSNAConfigAuthenticationSuiteIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
/* GET_EXACT Validate Index Instance Routine. */

INT1
RsnaValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                                UINT4
                                                                u4FsRSNAConfigAuthenticationSuiteIndex)
{
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    if (u4FsRSNAConfigAuthenticationSuiteIndex > RSNA_MAX_AUTH_SUITE_INDEX)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAConfigAuthenticationSuiteEnabled
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                testValDot11RSNAConfigAuthenticationSuiteEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAConfigAuthenticationSuiteEnabled (UINT4
                                                     *pu4ErrorCode,
                                                     UINT4
                                                     u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                     INT4
                                                     i4TestValDot11RSNAConfigAuthenticationSuiteEnabled)
{

    UNUSED_PARAM (pu4ErrorCode);

    if (((u4Dot11RSNAConfigAuthenticationSuiteIndex != RSNA_AUTHSUITE_PSK) &&
         (u4Dot11RSNAConfigAuthenticationSuiteIndex != RSNA_AUTHSUITE_8021X)
#ifdef PMF_WANTED
         && (u4Dot11RSNAConfigAuthenticationSuiteIndex !=
             RSNA_AUTHSUITE_PSK_SHA256)
         && (u4Dot11RSNAConfigAuthenticationSuiteIndex !=
             RSNA_AUTHSUITE_8021X_SHA256)
#endif
        )
        || ((i4TestValDot11RSNAConfigAuthenticationSuiteEnabled != RSNA_ENABLED)
            && (i4TestValDot11RSNAConfigAuthenticationSuiteEnabled !=
                RSNA_DISABLED)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

#ifdef PMF_WANTED
/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAProtectedManagementFramesActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11RSNAProtectedManagementFramesActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
****************************************************************************/

INT1
RsnaTestv2Dot11RSNAProtectedManagementFramesActivated (UINT4
                                                       *pu4ErrorCode,
                                                       INT4
                                                       i4IfIndex,
                                                       INT4
                                                       i4TestValDot11RSNAProtectedManagementFramesActivated)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4TestValDot11RSNAProtectedManagementFramesActivated != RSNA_MFPR)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11RSNAUnprotectedManagementFramesAllowed
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11RSNAUnprotectedManagementFramesAllowed
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11RSNAUnprotectedManagementFramesAllowed (UINT4
                                                       *pu4ErrorCode,
                                                       INT4
                                                       i4IfIndex,
                                                       INT4
                                                       i4TestValDot11RSNAUnprotectedManagementFramesAllowed)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4TestValDot11RSNAUnprotectedManagementFramesAllowed != RSNA_MFPC)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11AssociationSAQueryRetryTimeout
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11AssociationSAQueryRetryTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11AssociationSAQueryRetryTimeout (UINT4 *pu4ErrorCode,
                                               INT4 i4IfIndex,
                                               UINT4
                                               u4TestValDot11AssociationSAQueryRetryTimeout)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (u4TestValDot11AssociationSAQueryRetryTimeout <
        RSNA_MIN_SAQUERY_RETRY_TIME
        || u4TestValDot11AssociationSAQueryRetryTimeout >
        RSNA_MAX_SAQUERY_RETRY_TIME)
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_SAQUERY_RETRY_TIME;
        CLI_SET_ERR (CLI_RSNA_INVALID_SAQUERY_RETRY_TIME);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaTestv2Dot11AssociationSAQueryMaximumTimeout
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11AssociationSAQueryMaximumTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaTestv2Dot11AssociationSAQueryMaximumTimeout (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4
                                                 u4TestValDot11AssociationSAQueryMaximumTimeout)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (u4TestValDot11AssociationSAQueryMaximumTimeout <
        RSNA_MIN_SAQUERY_MAX_TIMEOUT
        || u4TestValDot11AssociationSAQueryMaximumTimeout >
        RSNA_MAX_SAQUERY_MAX_TIMEOUT)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}
#endif
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  RsnaDepv2Dot11RSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaDepv2Dot11RSNAConfigAuthenticationSuitesTable (UINT4 *pu4ErrorCode,
                                                   tSnmpIndexList *
                                                   pSnmpIndexList,
                                                   tSNMP_VAR_BIND *
                                                   pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot11RSNAStatsTable. */

/****************************************************************************
 Function    :  RsnaValidateIndexInstanceDot11RSNAStatsTable
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
RsnaValidateIndexInstanceDot11RSNAStatsTable (INT4 i4IfIndex,
                                              UINT4 u4Dot11RSNAStatsIndex)
{
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4Dot11RSNAStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetFirstIndexDot11RSNAStatsTable
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
RsnaGetFirstIndexDot11RSNAStatsTable (INT4 *pi4IfIndex,
                                      UINT4 *pu4Dot11RSNAStatsIndex)
{
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;

    RsnaUtilGetFirstSuppStatsEntry (&pRsnaSuppStatsInfo);

    if (pRsnaSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRsnaSuppStatsInfo->u2Port;
    *pu4Dot11RSNAStatsIndex = pRsnaSuppStatsInfo->u4StatsIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetNextIndexDot11RSNAStatsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Dot11RSNAStatsIndex
                nextDot11RSNAStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
RsnaGetNextIndexDot11RSNAStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4Dot11RSNAStatsIndex,
                                     UINT4 *pu4NextDot11RSNAStatsIndex)
{
    tRsnaSuppStatsInfo  SuppStatsInfo;
    tRsnaSuppStatsInfo *pNextSuppStatsInfo = NULL;

    RSNA_MEMSET (&SuppStatsInfo, 0, sizeof (tRsnaSuppStatsInfo));

    SuppStatsInfo.u2Port = i4IfIndex;
    SuppStatsInfo.u4StatsIndex = u4Dot11RSNAStatsIndex;

    RsnaUtilGetNextSuppStatsEntry (&SuppStatsInfo, &pNextSuppStatsInfo);

    if (pNextSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pNextSuppStatsInfo->u2Port;
    *pu4NextDot11RSNAStatsIndex = pNextSuppStatsInfo->u4StatsIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsSTAAddress
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsSTAAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsSTAAddress (INT4 i4IfIndex,
                                 UINT4 u4Dot11RSNAStatsIndex,
                                 tMacAddr * pRetValDot11RSNAStatsSTAAddress)
{
    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDot11RSNAStatsSTAAddress, pSuppStatsInfo->au1SuppMacAddr,
            RSNA_ALIGNED_ETH_ADDR_LEN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsVersion
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsVersion (INT4 i4IfIndex,
                              UINT4 u4Dot11RSNAStatsIndex,
                              UINT4 *pu4RetValDot11RSNAStatsVersion)
{

    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsVersion = pSuppStatsInfo->RsnaStatsDB.u4Version;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsSelectedPairwiseCipher
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsSelectedPairwiseCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsSelectedPairwiseCipher (INT4 i4IfIndex,
                                             UINT4 u4Dot11RSNAStatsIndex,
                                             tSNMP_OCTET_STRING_TYPE
                                             *
                                             pRetValDot11RSNAStatsSelectedPairwiseCipher)
{

    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDot11RSNAStatsSelectedPairwiseCipher->pu1_OctetList,
            pSuppStatsInfo->au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);

    pRetValDot11RSNAStatsSelectedPairwiseCipher->i4_Length =
        RSNA_CIPHER_SUITE_LEN;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsTKIPICVErrors
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsTKIPICVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsTKIPICVErrors (INT4 i4IfIndex,
                                    UINT4 u4Dot11RSNAStatsIndex,
                                    UINT4 *pu4RetValDot11RSNAStatsTKIPICVErrors)
{
    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsTKIPICVErrors = pSuppStatsInfo->RsnaStatsDB.
        u4TkipICVErrors;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsTKIPLocalMICFailures
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsTKIPLocalMICFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsTKIPLocalMICFailures (INT4 i4IfIndex,
                                           UINT4 u4Dot11RSNAStatsIndex,
                                           UINT4
                                           *pu4RetValDot11RSNAStatsTKIPLocalMICFailures)
{

    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsTKIPLocalMICFailures = pSuppStatsInfo->RsnaStatsDB.
        u4TkipLocalMicFailures;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsTKIPRemoteMICFailures
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsTKIPRemoteMICFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsTKIPRemoteMICFailures (INT4 i4IfIndex,
                                            UINT4 u4Dot11RSNAStatsIndex,
                                            UINT4
                                            *pu4RetValDot11RSNAStatsTKIPRemoteMICFailures)
{
    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsTKIPRemoteMICFailures = pSuppStatsInfo->RsnaStatsDB.
        u4TkipRemoteMicFailures;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsCCMPReplays
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsCCMPReplays
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsCCMPReplays (INT4 i4IfIndex,
                                  UINT4 u4Dot11RSNAStatsIndex,
                                  UINT4 *pu4RetValDot11RSNAStatsCCMPReplays)
{

    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsCCMPReplays =
        pSuppStatsInfo->RsnaStatsDB.u4CCMReplays;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsCCMPDecryptErrors
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsCCMPDecryptErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsCCMPDecryptErrors (INT4 i4IfIndex,
                                        UINT4 u4Dot11RSNAStatsIndex,
                                        UINT4
                                        *pu4RetValDot11RSNAStatsCCMPDecryptErrors)
{

    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsCCMPDecryptErrors =
        pSuppStatsInfo->RsnaStatsDB.u4CCMDecryptErrors;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAStatsTKIPReplays
 Input       :  The Indices
                IfIndex
                Dot11RSNAStatsIndex

                The Object 
                retValDot11RSNAStatsTKIPReplays
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAStatsTKIPReplays (INT4 i4IfIndex,
                                  UINT4 u4Dot11RSNAStatsIndex,
                                  UINT4 *pu4RetValDot11RSNAStatsTKIPReplays)
{
    tRsnaSuppStatsInfo *pSuppStatsInfo = NULL;

    RsnaUtilGetSuppStatsInfo ((UINT2) i4IfIndex, u4Dot11RSNAStatsIndex,
                              &pSuppStatsInfo);

    if (pSuppStatsInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDot11RSNAStatsTKIPReplays =
        pSuppStatsInfo->RsnaStatsDB.u4TkipReplays;

    return SNMP_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApiNotifyRsnaOfKeyAvailable                            *
*                                                                           *
* Description  : Function invoked by pnac to inform rsna of key avialable   *
*                                                                           *
* Input        : u2Port :- The port for which is key available              *
*                pAuthSessionNode :- Pointer to auth session node           *
*                pu1Pmk :- Refers to the PMK Key                            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApiNotifyRsnaOfKeyAvailable (UINT2 u2Port,
                                 UINT1 *pu1SuppMacAddr, UINT1 *pu1Pmk)
{
    tWssRSNANotifyParams WssRSNANotifyParams;
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));

    /* PNAC Key Available Indication  */
    MEMSET (WssRSNANotifyParams.MLMEPNACKEYIND.au1StaMac, 0, MAC_ADDR_LEN);
    MEMCPY (WssRSNANotifyParams.MLMEPNACKEYIND.au1StaMac, pu1SuppMacAddr,
            MAC_ADDR_LEN);
    MEMSET (WssRSNANotifyParams.MLMEPNACKEYIND.au1Key, 0,
            RSNA_MAX_SERVER_KEY_SIZE);
    MEMCPY (WssRSNANotifyParams.MLMEPNACKEYIND.au1Key, pu1Pmk,
            RSNA_MAX_SERVER_KEY_SIZE);

    WssRSNANotifyParams.u4IfIndex = (UINT4) u2Port;    /* Bss IfIndex  */
    WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_PNAC_KEY_AVAILABLE;

    /* Send Notification msg to Rsna */
    if (RsnaProcessWssNotification (&WssRSNANotifyParams) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaApiNotifyRsnaOfKeyAvailable() : RsnaProcessWssNotification No Rsna Info for the given port!! ... \n");

        return (OSIX_FAILURE);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApiHandleInEapolKeyFrame                               *
*                                                                           *
* Description  : Function to process eapol key frame. This is invoked from  *
*                Pnac Event Processing function                             *
*                                                                           *
* Input        : pu1Pkt :- Refers to the packet recvd                       *
*                u2PktLen :- Length of the packet recvd                     *
*                u2PortNum :- The port on which the pkt is recvd            *
*                srcAddr   :- Source mac address                            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaApiHandleInEapolKeyFrame (UINT1 *pu1Pkt, UINT2 u2PktLen,
                              UINT2 u2PortNum, UINT1 *pu1SrcAddr)
{

    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tPnacPaePortEntry  *pPnacPaePortEntry = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tEapolKeyFrame     *pEapolKeyFrame = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    tRsnaSessionNode   *pRsnaSessionNode = NULL;
    UINT4               u4ProfileIndex = 0;
    UINT2               u2Mask = 0;
    UINT1               au1PmkId[RSNA_PMKID_LEN];

    MEMSET (au1PmkId, 0, RSNA_PMKID_LEN);

    UNUSED_PARAM (pPnacPaePortEntry);
    pEapolKeyFrame = (tEapolKeyFrame *) (pu1Pkt + RSNA_EAPOL_HDR_SIZE);

    if (WssIfGetProfileIfIndex (u2PortNum, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaApiHandleInEapolKeyFrame:- "
                  "WssIfGetProfileIfIndex Failed !!!! \n");
        return (OSIX_FAILURE);
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (OSIX_FAILURE);

    }

    if (PnacAuthGetAuthInProgressTblEntryByMac (pu1SrcAddr, &pAuthSessNode)
        == PNAC_FAILURE)
    {
        /* Node is not found in the AIP table */
        if (PnacAuthGetSessionTblEntryByMac (pu1SrcAddr, &pAuthSessNode)
            == PNAC_FAILURE)
        {
            u2Mask = (pEapolKeyFrame->u2KeyInfo & RSNA_KEY_INFO_REQUEST);
            if (u2Mask == FALSE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No session exist for the recvd eapol frame\n");
                return (OSIX_FAILURE);
            }
            pRsnaSupInfo =
                RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortEntry, pu1SrcAddr);
            if (pRsnaSupInfo == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No station info  exist for the recvd "
                          "eapol frame!! ... \n");
                return (OSIX_FAILURE);
            }

            if (((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                          RSNA_AKM_PSK_OVER_802_1X,
                          RSNA_CIPHER_SUITE_LEN) != 0)) ||
                ((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                          WPA_AKM_PSK_OVER_802_1X, RSNA_CIPHER_SUITE_LEN) != 0))
#ifdef PMF_WANTED
                || ((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                             RSNA_AKM_PSK_SHA256_OVER_802_1X,
                             RSNA_CIPHER_SUITE_LEN) != 0))
#endif
                )
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "Pnac Authentication is yet to over !! ...\n");
                return (OSIX_FAILURE);

            }
            if (((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                          RSNA_AKM_PSK_OVER_802_1X,
                          RSNA_CIPHER_SUITE_LEN)) == 0)
                ||
                ((MEMCMP
                  (&pRsnaSupInfo->au1AkmSuite, WPA_AKM_PSK_OVER_802_1X,
                   RSNA_CIPHER_SUITE_LEN)) == 0)
#ifdef PMF_WANTED
                || ((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                             RSNA_AKM_PSK_SHA256_OVER_802_1X,
                             RSNA_CIPHER_SUITE_LEN)) == 0)
#endif
                )

            {
                pAuthSessNode = RsnaSessCreatePnacAuthSess (pu1SrcAddr,
                                                            u2PortNum, FALSE);
            }
            else if (((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                               RSNA_AKM_UNSPEC_802_1X,
                               RSNA_CIPHER_SUITE_LEN)) == 0) ||
                     ((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                               WPA_AKM_UNSPEC_802_1X,
                               RSNA_CIPHER_SUITE_LEN)) == 0)
#ifdef PMF_WANTED
                     || ((MEMCMP (&pRsnaSupInfo->au1AkmSuite,
                                  RSNA_AKM_UNSPEC_802_1X_SHA256,
                                  RSNA_CIPHER_SUITE_LEN)) == 0)
#endif
                )
            {
                pAuthSessNode = RsnaSessCreatePnacAuthSess (pu1SrcAddr,
                                                            u2PortNum, TRUE);
            }
            if (pAuthSessNode == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          " AuthCreateSession returned failure\n");
                return (OSIX_FAILURE);
            }
            pRsnaSessionNode = RsnaSessCreateAndInitSession (pRsnaPaePortEntry,
                                                             pAuthSessNode,
                                                             pu1Pkt, u2PktLen,
                                                             u2PortNum,
                                                             au1PmkId, TRUE);
            if (pRsnaSessionNode == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessCreateAndInitSession failed \n");
                return OSIX_FAILURE;
            }
            pRsnaSessionNode->pu1RxEapolKeyPkt = pu1Pkt;
            pRsnaSessionNode->u2PktLen = u2PktLen;
            if (RsnaProcEapolKeyFrame (pRsnaSessionNode) != RSNA_SUCCESS)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaProcEapolKeyFrame Failed \n");
                return OSIX_FAILURE;
            }

            if (RsnaMainFsmStart (pRsnaSessionNode) != RSNA_SUCCESS)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaMainFsmStart Failed \n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            pRsnaSessionNode =
                (tRsnaSessionNode *) pAuthSessNode->pRsnaSessionNode;
            if (pRsnaSessionNode == NULL)
            {
                u2Mask = (pEapolKeyFrame->u2KeyInfo & RSNA_KEY_INFO_REQUEST);
                if (u2Mask == FALSE)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "No session exist for the recvd "
                              "eapol frame!! ... \n");
                    return (OSIX_FAILURE);
                }
                pRsnaSessionNode =
                    RsnaSessCreateAndInitSession (pRsnaPaePortEntry,
                                                  pAuthSessNode, pu1Pkt,
                                                  u2PktLen, u2PortNum,
                                                  au1PmkId, TRUE);
                if (pRsnaSessionNode == NULL)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "RsnaSessCreateAndInitSession failed \n");
                    return OSIX_FAILURE;
                }
            }
            pRsnaSessionNode->pu1RxEapolKeyPkt = pu1Pkt;
            pRsnaSessionNode->u2PktLen = u2PktLen;
            if (RsnaProcEapolKeyFrame (pRsnaSessionNode) != RSNA_SUCCESS)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaProcEapolKeyFrame Failed \n");
                return OSIX_FAILURE;
            }
            if (RsnaMainFsmStart (pRsnaSessionNode) != RSNA_SUCCESS)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaMainFsmStart Failed \n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaApiHandleInEapolKeyFrame:- PnacAuthSessInProgress"
                  "Discarding the Frame !!! \n");
        return OSIX_FAILURE;
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcessWssNotification                                 *
*                                                                           *
* Description  : Function to handle WSS-RSNA and RSNA-WSS interactions      *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

UINT4
RsnaProcessWssNotification (tWssRSNANotifyParams * pWssRSNANotifyParams)
{

    tWssRSNANotifyParams *pMsg = NULL;
    UINT1               u1KeyType = 0;
    if ((pMsg = (tWssRSNANotifyParams *) RSNA_MEM_ALLOCATE_MEM_BLK
         (RSNA_WSS_INFO_MEMPOOL_ID)) == NULL)

    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessWssNotification:- "
                  "RSNA_MEM_ALLOCATE_MEM_BLK Failed \n");
        return (OSIX_FAILURE);
    }

    MEMCPY (pMsg, pWssRSNANotifyParams, sizeof (tWssRSNANotifyParams));

    switch (pMsg->eWssRsnaNotifyType)
    {

      /*******************************************************
       *              Indication from WSS --> RSNA 
       *****************************************************/

        case WSSRSNA_PROFILEIF_ADD:
        case WSSRSNA_PROFILEIF_DEL:
        case WSSRSNA_ASSOC_IND:
        case WSSRSNA_REASSOC_IND:
        case WSSRSNA_DISSOC_IND:
        case WSSRSNA_DEAUTH_IND:
        case WSSRSNA_MIC_FAILURE_IND:
        case WSSRSNA_PNAC_KEY_AVAILABLE:
            /* The above cases falls under the category of WSS --> 
             * RSNA  messages Since it is called by an external WSS  
             * module,posting to the
             * RSNA queue is required */
            RsnaEnQFrameToRsnaTask (pMsg);
            break;

      /********************************************************
       *              Indication from RSNA --> WSS 
       ********************************************************/

            /* The following cases falls under the category of RSNA 
             * --> WSS messages Since it is internally called by the RSNA 
             * module, here posting to the RSNA queue is not required. It 
             * triggers the external WSS functions directly */

            /*RSNA TODO MERGE */
        case WSSRSNA_SET_KEY:
            if (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyType ==
                WSSRSNA_PTK_KEY)
            {
                u1KeyType =
                    (UINT1) pMsg->MLMESETKEYSREQ.aKeyDesc[0].u1AssocType;
                WssStaSendRsnaSessionKey (pMsg->MLMESETKEYSREQ.aKeyDesc[0].
                                          au1StaAddr, pMsg->u4IfIndex,
                                          u1KeyType);
            }
            else if (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                     u1KeyType == WSSRSNA_GTK_KEY)
            {
                WssSendUpdateWlanMsgfromRsna (pMsg->u4IfIndex, WSSRSNA_GTK_KEY);
            }
#ifdef PMF_WANTED
            else if (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                     u1KeyType == WSSRSNA_IGTK_KEY)
            {
                /*WssSendUpdateWlanMsgfromRsna (pMsg->u4IfIndex,WSSRSNA_IGTK_KEY); */
            }
#endif

            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pMsg);
            break;
        case WSSRSNA_DEL_KEY:
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pMsg);
            break;
        case WSSRSNA_DISSOC_REQ:
            RsnaSendDisAssocMsg (pMsg);
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pMsg);
            break;
        case WSSRSNA_DEAUTH_REQ:
            RsnaSendDeAuthMsg (pMsg);
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pMsg);
            break;
        default:
            RSNA_TRC_ARG1 (ALL_FAILURE_TRC,
                           "RsnaInterfaceHandleQueueEvent:- "
                           "Invalid Message Type %d\n",
                           pMsg->eWssRsnaNotifyType);
            break;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaGetRsnIEParams                                         *
*                                                                           *
* Description  : Function to get the RSN IE elements                        *
*                                                                           *
* Input        : u4WlanProfileIfIndex - Wlan Profile If index               * 
*                                                                           *
* Output       : pRsnaIEElements - Pointer to RSNA params                   *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaGetRsnIEParams (UINT4 u4WlanProfileIfIndex,
                    tRsnaIEElements * pRsnaIEElements)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    INT4                i4PreAuthStatus = 0;
    UINT1               u1Index = 0;
    UINT1               u1Count = 0;
    UINT1               u1Length = 0;

    RsnaGetPortEntry ((UINT2) u4WlanProfileIfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnIEParams:- "
                  "RsnaGetPortEntry Failed \n");
        return RSNA_FAILURE;
    }

    /* Filling the RSN Element id */
    pRsnaIEElements->u1ElemId = RSNA_IE_ID;
    /*u1Length++; *//*PMF BUG */

    /* Copying the version */
    pRsnaIEElements->u2Ver = (UINT2) pRsnaPaePortEntry->RsnaConfigDB.u4Version;
    u1Length = (UINT1) (u1Length + 2);

    /* Copying the Group Cipher */
    MEMCPY (pRsnaIEElements->au1GroupCipherSuite,
            pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipher,
            RSNA_MAX_CIPHER_TYPE_LENGTH);
    u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);

    /* Copying the Pairwise Cipher */

    /* Initialize the u1Count Value to Zero, which used in the index
       for Pairwise Cipher DB in RsnaIEelement Structure.
     */
    u1Count = 0;

    for (u1Index = 0; u1Index < RSNA_PAIRWISE_CIPHER_COUNT; u1Index++)
    {
        if (pRsnaPaePortEntry->aRsnaPwCipherDB[u1Index].
            bPairwiseCipherEnabled == OSIX_TRUE)
        {
            MEMCPY (pRsnaIEElements->aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                    pRsnaPaePortEntry->aRsnaPwCipherDB[u1Index].
                    au1PairwiseCipher, RSNA_MAX_CIPHER_TYPE_LENGTH);
            u1Count++;
            u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);

        }
    }

    /* Update the Pairwise Cipher Count Value */
    pRsnaIEElements->u2PairwiseCipherSuiteCount = u1Count;

    /* Updating the length for u2PairwiseCipherSuiteCount */
    u1Length = (UINT1) (u1Length + 2);

    /* Copying the AKM suite */
    u1Count = 0;                /*Resetting the count to zero for reuse */

    if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].
        bAkmSuiteEnabled == OSIX_TRUE)
    {
        MEMCPY (pRsnaIEElements->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK -
                                              1].au1AuthSuite,
                RSNA_AKM_SELECTOR_LEN);
        u1Count++;
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    }
    else if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
             bAkmSuiteEnabled == OSIX_TRUE)
    {
        MEMCPY (pRsnaIEElements->aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                au1AKMSuite,
                pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_8021X -
                                              1].au1AuthSuite,
                RSNA_AKM_SELECTOR_LEN);
        u1Count++;
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    }
#ifdef PMF_WANTED
    else if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
             bAkmSuiteEnabled == OSIX_TRUE)
    {
        MEMCPY (pRsnaIEElements->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
                au1AKMSuite,
                pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 -
                                              1].au1AuthSuite,
                RSNA_AKM_SELECTOR_LEN);
        u1Count++;
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    }
    else if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 - 1].
             bAkmSuiteEnabled == OSIX_TRUE)
    {
        MEMCPY (pRsnaIEElements->aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 - 1].
                au1AKMSuite,
                pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 -
                                              1].au1AuthSuite,
                RSNA_AKM_SELECTOR_LEN);
        u1Count++;
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    }
#endif
#ifdef DEBUG_WANTED
    for (u1Index = 0; u1Index < RSNA_AKM_SUITE_COUNT; u1Index++)
    {
        if (gRsnaGlobals.aRsnaAkmDB[u1Index].bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pRsnaIEElements->aRsnaAkmDB[u1Count].au1AKMSuite,
                    gRsnaGlobals.aRsnaAkmDB[u1Index].au1AuthSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Count++;
            u1Length = (UINT1) (u1Length + RSNA_AKM_SELECTOR_LEN);
        }
    }
#endif
    /* Copying the pairwise cipher count */
    pRsnaIEElements->u2AKMSuiteCount = u1Count;

    /* Updating the length for u2AKMSuiteCount */
    u1Length = (UINT1) (u1Length + 2);

    /*Setting the following values as zero, as it is not required now */

    RsnaGetDot11RSNAPreauthenticationEnabled ((INT4) u4WlanProfileIfIndex,
                                              &i4PreAuthStatus);
    if (i4PreAuthStatus == RSNA_ENABLED)
    {
        pRsnaIEElements->u2RsnCapab = 1;
    }
#ifdef PMF_WANTED
    if (pRsnaPaePortEntry->bRsnaUPMFEnabled)    /* PMF_WANTED */
    {
        pRsnaIEElements->u2RsnCapab |= RSNA_CAPABILITY_MFPC;
    }
    if (pRsnaPaePortEntry->bRsnaPMFEnabled)
    {
        pRsnaIEElements->u2RsnCapab |= RSNA_CAPABILITY_MFPR;
    }

    if (pRsnaPaePortEntry->bRsnaPMFEnabled
        || pRsnaPaePortEntry->bRsnaUPMFEnabled)
    {
        MEMCPY (pRsnaIEElements->au1GroupMgmtCipherSuite,
                gau1RsnaCipherSuiteBIP, RSNA_MAX_CIPHER_TYPE_LENGTH);
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
        if ((pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].
             bAkmSuiteEnabled == OSIX_TRUE)
            || (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
                bAkmSuiteEnabled == OSIX_TRUE))
        {
            u1Length = u1Length + 2;
        }
    }
#endif

    /* TODO: Length should be update later */
    u1Length = u1Length + 2;

    /* Support for PMKId is not supported at present */
    pRsnaIEElements->u2PmkidCount = 0;
#ifdef PMF_DEBUG
    if ((pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].
         bAkmSuiteEnabled != OSIX_TRUE)
        && (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
            bAkmSuiteEnabled != OSIX_TRUE))

    {
        u1Length = u1Length + 2;
    }
#endif
    /* TODO: PMK id should be filled later
       u1Length = u1Length + 2;

       MEMSET (au1PmkdIdList, 0, RSNA_PMKID_LIST_LEN);
       MEMCPY (pRsnaIEElements->aPmkidList, au1PmkdIdList, RSNA_PMKID_LIST_LEN);

       Need to use the below code, when PMKID is supported 
       u1Length = u1Length + RSNA_PMKID_LIST_LEN; */

    /* Updating the length field and its counter value */
    /*u1Length++; */
    pRsnaIEElements->u1Len = u1Length;

    return RSNA_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaGetRsnSessionKey                                       *
*                                                                           *
* Description  : Function to Get the PTK Key from Rsna                      *
*                                                                           *
* Input        : staMacAddr - Station MAC Address                           *
*                                                                           *
* Output       : pu1KeyLength - PTK key length                              *
*                pu1PtkKey  - PTK KEY                                       *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaGetRsnSessionKey (tMacAddr staMacAddr, UINT1 *pu1PtkKey,
                      UINT1 *pu1KeyLength, UINT1 *pu1CipherIndex,
                      UINT1 *pu1AkmSuiteIndex)
{
    tPnacAuthSessionNode *pAuthSessionNode = NULL;
    tRsnaSessionNode   *pRsnaSessNode = NULL;
    UINT1               u1PairwiseCipherKeyLen = 0;

    if (PnacAuthGetSessionTblEntryByMac (staMacAddr, &pAuthSessionNode)
        != PNAC_SUCCESS)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnSessionKey:- No Pnac"
                  "Session for the given mac \n");
        return RSNA_FAILURE;
    }

    pRsnaSessNode = (tRsnaSessionNode *) pAuthSessionNode->pRsnaSessionNode;
    if (pRsnaSessNode == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, " NULL Value RsnaGetRsnSessionKey:- No Rsna"
                  "Session for the given mac \n");
        return RSNA_FAILURE;
    }

    if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                 RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0 ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                 WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN)) == 0)
    {
        u1PairwiseCipherKeyLen = RSNA_TKIP_KEY_LEN;
        *pu1CipherIndex = RSNA_TKIP_INDEX;

    }
    else if ((MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN)) == 0 ||
             (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1PairwiseCipher,
                      WPA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN)) == 0)
    {
        u1PairwiseCipherKeyLen = RSNA_CCMP_KEY_LEN;
        *pu1CipherIndex = RSNA_CCMP_INDEX;
    }

    if ((MEMCMP
         (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
          gau1RsnaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN)) == 0 ||
        (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                 gau1WpaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN)) == 0)
    {
        *pu1AkmSuiteIndex = RSNA_AUTH_PSK_INDEX;
    }
    else if ((MEMCMP
              (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
               gau1RsnaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN)) == 0 ||
             (MEMCMP (pRsnaSessNode->pRsnaSupInfo->au1AkmSuite,
                      gau1WpaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN)) == 0)
    {
        *pu1AkmSuiteIndex = RSNA_AUTH_AS_INDEX;
    }

    MEMCPY (pu1PtkKey, pRsnaSessNode->PtkSaDB.au1Ptk, u1PairwiseCipherKeyLen);
    *pu1KeyLength = u1PairwiseCipherKeyLen;

    return RSNA_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaGetRsnGtkKey                                           *
*                                                                           *
* Description  : Function to Get the GTK from Rsna                          *
*                                                                           *
* Input        : UINT4 u4WlanProfileIfIndex - Wlan Profile Interface        *
*                                                                           *
* Output       : pu1KeyLength - Key Length                                  *
*                pu1GtkKey - Gtk key                                        *
*                pu1KeyIndex - Key Index                                    *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaGetRsnGtkKey (UINT4 u4WlanProfileIfIndex,
                  UINT1 *pu1GtkKey, UINT2 *pu2KeyLength, UINT1 *pu1KeyIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT1               u1GroupCipherKeyLen = 0;
    UINT1               u1KeyIdx = 0;
    UINT1               au1GtkKey[RSNA_GTK_MAX_LEN];

    MEMSET (au1GtkKey, 0, RSNA_GTK_MAX_LEN);

    RsnaGetPortEntry ((UINT2) u4WlanProfileIfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnGtkKey:- "
                  "RsnaGetPortEntry Failed \n");
        return RSNA_FAILURE;
    }
    if ((MEMCMP (pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipher,
                 RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        u1GroupCipherKeyLen = RSNA_TKIP_KEY_LEN;
    }
    else if ((MEMCMP (pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipher,
                      RSNA_CIPHER_SUITE_CCMP, RSNA_CIPHER_SUITE_LEN) == 0))
    {
        u1GroupCipherKeyLen = RSNA_CCMP_KEY_LEN;
    }
    *pu2KeyLength = u1GroupCipherKeyLen;
    u1KeyIdx = pRsnaPaePortEntry->RsnaGlobalGtk.u1GN;

    /* If delete flag is true, pass the actual GTK key that needs to 
     * be programmed in the h/w*/
    if (pRsnaPaePortEntry->RsnaGlobalGtk.bKeyDeleteFlag == FALSE)
    {
        MEMCPY (pu1GtkKey,
                pRsnaPaePortEntry->RsnaGlobalGtk.au1Gtk[u1KeyIdx -
                                                        RSNA_GTK_FIRST_IDX],
                RSNA_GTK_MAX_LEN);
    }
    else                        /*Else pass zeroes to delete the gtk */
    {
        MEMCPY (pu1GtkKey, au1GtkKey, RSNA_GTK_MAX_LEN);
    }

    *pu1KeyIndex = u1KeyIdx;
    return RSNA_SUCCESS;

}

#ifdef PMF_WANTED
/****************************************************************************
*                                                                           *
* Function     : RsnaGetRsnIGtkKey                                           *
*                                                                           *
* Description  : Function to Get the IGTK from Rsna                          *
*                                                                           *
* Input        : UINT4 u4WlanProfileIfIndex - Wlan Profile Interface        *
*                                                                           *
* Output       : pu1KeyLength - Key Length                                  *
*                pu1GtkKey - IGtk key                                        *
*                pu1KeyIndex - Key Index                                    *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaGetRsnIGtkKey (UINT4 u4WlanProfileIfIndex,
                   UINT1 *pu1IGtkKey, UINT2 *pu2KeyLength, UINT1 *pu1KeyIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT1               u1KeyIdx = 0;
    UINT1               au1IGtkKey[RSNA_GTK_MAX_LEN];

    MEMSET (au1IGtkKey, 0, RSNA_GTK_MAX_LEN);

    RsnaGetPortEntry ((UINT2) u4WlanProfileIfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnIGtkKey:- "
                  "RsnaGetPortEntry Failed \n");
        return RSNA_FAILURE;
    }

    *pu2KeyLength = RSNA_BIP_KEY_LEN;
    u1KeyIdx = pRsnaPaePortEntry->RsnaGlobalIGtk.u1IGN;

    /* If delete flag is true, pass the actual GTK key that needs to 
     * be programmed in the h/w*/
    if (pRsnaPaePortEntry->RsnaGlobalIGtk.bKeyDeleteFlag == FALSE)
    {
        MEMCPY (pu1IGtkKey,
                pRsnaPaePortEntry->RsnaGlobalIGtk.au1IGtk[u1KeyIdx -
                                                          RSNA_IGTK_FIRST_IDX],
                RSNA_IGTK_MAX_LEN);
    }
    else                        /*Else pass zeroes to delete the gtk */
    {
        MEMCPY (pu1IGtkKey, au1IGtkKey, RSNA_IGTK_MAX_LEN);
    }

    *pu1KeyIndex = u1KeyIdx;
    return RSNA_SUCCESS;

}
#endif

/****************************************************************************
*                                                                           *
* Function     : RsnaSendDisAssocMsg                                        *
*                                                                           *
* Description  : Function to send dis association message from RSNA         *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna notify params       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaSendDisAssocMsg (tWssRSNANotifyParams * pWssRSNANotifyParams)
{
    UNUSED_PARAM (pWssRSNANotifyParams);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaSendDeAuthMsg*
*                                                                           *
* Description  : Function to send deauthentication  message from RSNA       *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna notify params       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaSendDeAuthMsg (tWssRSNANotifyParams * pWssRSNANotifyParams)
{
    tCfaIfInfo          IfInfo;

    CfaGetIfInfo (pWssRSNANotifyParams->u4IfIndex, &IfInfo);

    WssRsnaSendDeAuthMsg (IfInfo.au1MacAddr,
                          pWssRSNANotifyParams->MLMEDEAUTHREQ.au1StaMac);
}

#ifdef WPA_WANTED
/****************************************************************************
*                                                                           *
* Function     : WpaGetBeaconWPAIEElements                                 *
*                                                                           *
* Description  : Function to get the RSN beacon frames                      *
*                                                                           *
* Input        : u4WlanProfileIfIndex - Wlan Profile If index               *
*                                                                           *
* Output       : pu1RsnIE - Pointer to an array au1RsnIE                      *
*                pu1Length - Pointer to the length of the filled array      *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
WpaGetBeaconWPAIEElements (UINT4 u4WlanProfileIfIndex, UINT1 *pu1RsnIE,
                           UINT1 *pu1RsnIELength)
{
    UINT1               u1Index = 0;
    UINT1               u1Count = 0;
    tWpaIEElements      WpaIEElements;
    UINT1               au1Oui[] = { 0x00, 0x50, 0xF2, 0x01 };
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    MEMSET (&WpaIEElements, 0, sizeof (tWpaIEElements));

    if (WpaGetRsnIEParams (u4WlanProfileIfIndex,
                           &WpaIEElements) == RSNA_SUCCESS)
    {
        *(pu1RsnIE + u1Index) = WpaIEElements.u1ElemId;
        u1Index = (UINT1) (u1Index + RSN_IE_ELEMENT_LENGTH);

        *(pu1RsnIE + u1Index) =
            WpaIEElements.u1Len + RSNA_MAX_CIPHER_TYPE_LENGTH - 1;
        u1Index = (UINT1) (u1Index + RSN_IE_LENGTH_FIELD);

        MEMCPY (pu1RsnIE + u1Index, au1Oui, RSNA_MAX_CIPHER_TYPE_LENGTH);
        u1Index = (UINT1) (u1Index + RSNA_MAX_CIPHER_TYPE_LENGTH);

        *(pu1RsnIE + u1Index) = (UINT1) WpaIEElements.u2Ver;
        u1Index = (UINT1) (u1Index + RSN_IE_VERSION_LENGTH);

        MEMCPY (pu1RsnIE + u1Index, WpaIEElements.au1GroupCipherSuite,
                RSNA_MAX_CIPHER_TYPE_LENGTH);
        u1Index = (UINT1) (u1Index + RSNA_MAX_CIPHER_TYPE_LENGTH);

        *(pu1RsnIE + u1Index) =
            (UINT1) WpaIEElements.u2PairwiseCipherSuiteCount;
        u1Index = (UINT1) (u1Index + RSNA_PAIRWISE_CIPHER_COUNT);

        for (u1Count = 0;
             ((u1Count < WpaIEElements.u2PairwiseCipherSuiteCount)
              && (u1Count < WPA_PAIRWISE_CIPHER_COUNT)); u1Count++)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    WpaIEElements.aWpaPwCipherDB[u1Count].au1PairwiseCipher,
                    RSNA_MAX_CIPHER_TYPE_LENGTH);
            u1Index = (UINT1) (u1Index + RSNA_MAX_CIPHER_TYPE_LENGTH);
        }

        *(pu1RsnIE + u1Index) = (UINT1) WpaIEElements.u2AKMSuiteCount;
        u1Index = (UINT1) (u1Index + sizeof (UINT2));
        RsnaGetPortEntry ((UINT2) u4WlanProfileIfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnIEParams:- "
                      "RsnaGetPortEntry Failed \n");
            return RSNA_FAILURE;
        }
        if (pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].
            bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
        else if (pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                 bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X -
                                            1].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
        *pu1RsnIELength = u1Index;
        return RSNA_SUCCESS;
    }
    return RSNA_FAILURE;

}
#endif

/****************************************************************************
*                                                                           *
* Function     : RsnaGetBeaconRSNIEElements                                 *
*                                                                           *
* Description  : Function to get the RSN beacon frames                      *
*                                                                           *
* Input        : u4WlanProfileIfIndex - Wlan Profile If index               *
*                                                                           *
* Output       : pu1RsnIE - Pointer to an array au1RsnIE                      *
*                pu1Length - Pointer to the length of the filled array      *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaGetBeaconRSNIEElements (UINT4 u4WlanProfileIfIndex, UINT1 *pu1RsnIE,
                            UINT1 *pu1RsnIELength)
{
    UINT1               u1Index = 0;
    UINT1               u1Count = 0;
    tRsnaIEElements     RsnaIEElements;
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    MEMSET (&RsnaIEElements, 0, sizeof (tRsnaIEElements));

    if (RsnaGetRsnIEParams (u4WlanProfileIfIndex,
                            &RsnaIEElements) == RSNA_SUCCESS)
    {
        *(pu1RsnIE + u1Index) = RsnaIEElements.u1ElemId;
        u1Index = (UINT1) (u1Index + RSN_IE_ELEMENT_LENGTH);

        *(pu1RsnIE + u1Index) = RsnaIEElements.u1Len;
        u1Index = (UINT1) (u1Index + RSN_IE_LENGTH_FIELD);

        *(pu1RsnIE + u1Index) = (UINT1) RsnaIEElements.u2Ver;
        u1Index = (UINT1) (u1Index + RSN_IE_VERSION_LENGTH);

        MEMCPY (pu1RsnIE + u1Index, RsnaIEElements.au1GroupCipherSuite,
                RSNA_MAX_CIPHER_TYPE_LENGTH);
        u1Index = (UINT1) (u1Index + RSNA_MAX_CIPHER_TYPE_LENGTH);

        *(pu1RsnIE + u1Index) =
            (UINT1) RsnaIEElements.u2PairwiseCipherSuiteCount;
        u1Index = (UINT1) (u1Index + RSNA_PAIRWISE_CIPHER_COUNT);

        for (u1Count = 0;
             ((u1Count < RsnaIEElements.u2PairwiseCipherSuiteCount)
              && (u1Count < RSNA_PAIRWISE_CIPHER_COUNT)); u1Count++)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    RsnaIEElements.aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                    RSNA_MAX_CIPHER_TYPE_LENGTH);
            u1Index = (UINT1) (u1Index + RSNA_MAX_CIPHER_TYPE_LENGTH);
        }

        *(pu1RsnIE + u1Index) = (UINT1) RsnaIEElements.u2AKMSuiteCount;
#ifdef PMF_WANTED
        u1Index = (UINT1) (u1Index + sizeof (UINT2));
#else
        u1Index = (UINT1) (u1Index + sizeof (UINT2));
#endif
        RsnaGetPortEntry ((UINT2) u4WlanProfileIfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnIEParams:- "
                      "RsnaGetPortEntry Failed \n");
            return RSNA_FAILURE;
        }
        if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].
            bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK -
                                              1].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
        else if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                 bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X -
                                              1].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
#ifdef PMF_WANTED
        else if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
                 bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 -
                                              1].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
        else if (pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 - 1].
                 bAkmSuiteEnabled == OSIX_TRUE)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 -
                                              1].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
#endif
#ifdef DEBUG_WANTED
        for (u1Count = 0; u1Count < RsnaIEElements.u2AKMSuiteCount; u1Count++)
        {
            MEMCPY (pu1RsnIE + u1Index,
                    RsnaIEElements.aRsnaAkmDB[u1Count].au1AKMSuite,
                    RSNA_AKM_SELECTOR_LEN);
            u1Index = (UINT1) (u1Index + RSNA_AKM_SELECTOR_LEN);
        }
#endif
        *(pu1RsnIE + u1Index) = (UINT1) RsnaIEElements.u2RsnCapab;
        u1Index = (UINT1) (u1Index + RSN_IE_CAPAB_LENGTH);
#ifdef PMF_DEBUG
        if ((MEMCMP
             (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
              gau1RsnaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN) != 0)
            &&
            (MEMCMP
             (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
              au1AKMSuite, gau1RsnaAuthKeyMgmtPskSHA256Over8021X,
              RSNA_AKM_SELECTOR_LEN) != 0))

        {
            *(pu1RsnIE + u1Index) = (UINT1) RsnaIEElements.u2PmkidCount;
            u1Index = (UINT1) (u1Index + RSN_IE_PMKID_COUNT_LENGTH);
        }
#endif

#ifdef PMF_WANTED

        if ((RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPR) ==
            RSNA_CAPABILITY_MFPR
            || (RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPC) ==
            RSNA_CAPABILITY_MFPC)
        {
            if ((MEMCMP
                 (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                  gau1RsnaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN) == 0)
                ||
                (MEMCMP
                 (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
                  au1AKMSuite, gau1RsnaAuthKeyMgmtPskSHA256Over8021X,
                  RSNA_AKM_SELECTOR_LEN) == 0))
            {
                *(pu1RsnIE + u1Index) = (UINT1) RsnaIEElements.u2PmkidCount;
                u1Index = (UINT1) (u1Index + RSN_IE_PMKID_COUNT_LENGTH);
            }
            MEMCPY (pu1RsnIE + u1Index, RsnaIEElements.au1GroupMgmtCipherSuite,
                    RSNA_MAX_CIPHER_TYPE_LENGTH);

            u1Index = (UINT1) (u1Index + RSNA_MAX_CIPHER_TYPE_LENGTH);
        }
#endif
        *pu1RsnIELength = u1Index;
        return RSNA_SUCCESS;
    }
    return RSNA_FAILURE;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaHandleExistingStationAssocRequest                      *
*                                                                           *
* Description  : Function to handle the existing station assoc request      *
*                                                                           *
* Input        : staMacAddr - Station mac address                           *
*                u4BssIfIndex - Bss if index                                *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaHandleExistingStationAssocRequest (tMacAddr staMacAddr, UINT4 u4BssIfIndex,
                                       BOOL1 * pbSessDelete)
{
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaPaePortEntry  *pRsnaTmpPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    tRsnaSupInfo       *pRsnaTmpSupInfo = NULL;
    tPnacAuthSessionNode *pAuthSessNode = NULL;
    tRsnaSessionNode   *pRsnaSessNode = NULL;

    *pbSessDelete = OSIX_FALSE;

    UNUSED_PARAM (u4BssIfIndex);

    PNAC_LOCK ();

    if (PnacSnmpLowGetAuthSessionNode (staMacAddr, &pAuthSessNode) !=
        PNAC_SUCCESS)
    {
        PNAC_UNLOCK ();
        return RSNA_SUCCESS;
    }

    pRsnaSessNode = (tRsnaSessionNode *) pAuthSessNode->pRsnaSessionNode;

    if (pRsnaSessNode == NULL)
    {
        /*Since PnacSession Node alone is available,this may be a newly created
         *one in PNAC because of reaching the maximum reauth count.So this newly
         *created node has to be deleted and the RsnaSession which is already 
         *created is scanned through all the WLAN's and it is deleted.
         **/
        PnacAuthDeleteSession (pAuthSessNode);
        RsnaPnacDeleteSession (staMacAddr);

        RsnaGetFirstPortEntry (&pRsnaPaePortInfo);

        while (pRsnaPaePortInfo != NULL)
        {
            pRsnaTmpSupInfo =
                RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, staMacAddr);

            if ((pRsnaTmpSupInfo != NULL)
                && (pRsnaTmpSupInfo->pRsnaSessNode != NULL))
            {
                RsnaSessDeleteSession (pRsnaTmpSupInfo->pRsnaSessNode);
                *pbSessDelete = OSIX_TRUE;
                PNAC_UNLOCK ();
                return RSNA_SUCCESS;
            }

            RsnaGetNextPortEntry (pRsnaPaePortInfo, &pRsnaTmpPaePortInfo);
            pRsnaPaePortInfo = pRsnaTmpPaePortInfo;

        }
        PNAC_UNLOCK ();
        return RSNA_SUCCESS;

    }
    pRsnaPaePortInfo = pRsnaSessNode->pRsnaPaePortInfo;

    if (pRsnaPaePortInfo != NULL)
    {
        pRsnaSupInfo =
            RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, staMacAddr);
    }

#ifdef PMF_DEBUG
    /* PMF_WANTED */
    if ((pRsnaSessNode != NULL) && (pRsnaPaePortInfo->bRsnaPMFEnabled))
    {
        if (pRsnaSupInfo != NULL && !(pRsnaSupInfo->bSAQueryConfirm))
        {
            /* Association Reject has to be sent to the station with association comeback time */

            *pbSessDelete = OSIX_FALSE;
            return RSNA_SUCCESS;
        }

    }
#endif

    if (pRsnaSupInfo != NULL)
    {
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            if (MEMCMP (pRsnaSupInfo->au1SuppMacAddr, staMacAddr,
                        RSNA_ETH_ADDR_LEN) == 0)
            {
#ifdef PMF_DEBUG
                /* When the station roams to the new AP (AP2) ,PTK key needs to
                 * be deleted in the old AP (AP1), and at that time only key
                 * deletion needs to be called.*/

                if (pRsnaSupInfo->pRsnaSessNode->u4BssIfIndex != u4BssIfIndex)
                {
                    WssStaDeleteRsnaSessionKey (staMacAddr, u4BssIfIndex);
                }
#endif
                pRsnaSupInfo->pRsnaSessNode->u4RefCount = 0;
                pRsnaSupInfo->pRsnaSessNode->pRsnaPaePortInfo->RsnaGlobalGtk.
                    u4GNoStations--;
                if (RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode) !=
                    RSNA_SUCCESS)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "RsnaHandleExistingStationAssocRequest:- "
                              "RsnaSessDeleteSession Failed !!!! \n");
                    PNAC_UNLOCK ();
                    return (RSNA_FAILURE);
                }
                *pbSessDelete = OSIX_TRUE;
            }
        }
    }
    PNAC_UNLOCK ();
    return RSNA_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaCheckv2Dot11AuthAlgo
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11AuthenticationAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether the Authentication algorithm has been set to
                "open" to enable the RSNA in Set.
                Stores the value of error code in the Return val
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaIsOpenAuthEnabled (INT4 i4IfIndex)
{
    UINT1               u1Dot11AuthAlgo = AUTH_ALGO_OPEN;
    UINT1               u1WebAuthStatus = 0;

    if (WssIfGetAuthenticationAlgoDetails (i4IfIndex,
                                           &u1Dot11AuthAlgo,
                                           &u1WebAuthStatus) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaCheckv2Dot11AuthAlgo:- "
                  "WssIfGetAuthenticationAlgoDetails Failed !!!! \n");
        return (RSNA_FAILURE);
    }

    /* Check if Authentication Algorithm is "Open" to enable RSNA 
     * and WebAuthStatus should be disabled */
    if (u1Dot11AuthAlgo != AUTH_ALGO_OPEN)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "Auth Algo should be 'Open' to enable RSNA!! \n");
        return (RSNA_FAILURE);
    }

    if (u1WebAuthStatus == RSNA_ENABLED)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "Web Auth should be 'Disabled' to enable RSNA!! \n");
        return (RSNA_FAILURE);
    }
    return RSNA_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaHandleBSSIfDeletion                                    *
*                                                                           *
* Description  : Function to handle the BSS Interface index deletion        *
*                                                                           *
* Input        : u4BssIfIndex - Bss if index                                *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaHandleBSSIfDeletion (UINT4 u4BssIfIndex)
{
    UINT4               u4ProfileIndex = 0;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    tRsnaSupInfo       *pTempRsnaSupInfo = NULL;

    if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaHandleBSSIfDeletion:- "
                  "WssIfGetProfileIfIndex : returned Failure\n");
        return RSNA_FAILURE;
    }
    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaHandleBSSIfDeletion:-"
                  "No Rsna entry for the given port");
        return RSNA_FAILURE;
    }

    TMO_DYN_SLL_Scan (&(pRsnaPaePortInfo->
                        RsnaSupInfoList), pRsnaSupInfo,
                      pTempRsnaSupInfo, tRsnaSupInfo *)
    {
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            if (pRsnaSupInfo->pRsnaSessNode->u4BssIfIndex == u4BssIfIndex)
            {
                pRsnaSupInfo->pRsnaSessNode->u4RefCount = 0;
                if (RsnaSessDeleteSession (pRsnaSupInfo->
                                           pRsnaSessNode) != RSNA_SUCCESS)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC |
                              RSNA_ALL_FAILURE_TRC,
                              "RsnaHandleBSSIfDeletion:- "
                              "RsnaSessDeleteSession Failed !!!! \n");
                    return (RSNA_FAILURE);
                }
            }
        }
    }
    return RSNA_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaGetIsPtkInDoneState                                    *
*                                                                           *
* Description  : Function to check whether  the PTK State Machine is in     *
*                done state                                                 *
*                                                                           *
* Input        : staMacAddr - Station MAC Address                           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaGetIsPtkInDoneState (tMacAddr staMacAddr)
{
    tPnacAuthSessionNode *pAuthSessionNode = NULL;
    tRsnaSessionNode   *pRsnaSessNode = NULL;
    PNAC_LOCK ();

    if (PnacAuthGetSessionTblEntryByMac (staMacAddr, &pAuthSessionNode)
        != PNAC_SUCCESS)
    {
        PNAC_UNLOCK ();
        return RSNA_FAILURE;
    }

    pRsnaSessNode = (tRsnaSessionNode *) pAuthSessionNode->pRsnaSessionNode;
    if (pRsnaSessNode == NULL)
    {
        PNAC_UNLOCK ();
        return RSNA_FAILURE;
    }

    if (pRsnaSessNode->u1PtkFsmState == RSNA_4WAY_PTKINITDONE)
    {
        PNAC_UNLOCK ();
        return RSNA_SUCCESS;
    }
    else
    {
        PNAC_UNLOCK ();
        return RSNA_FAILURE;
    }
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApiFsmStop                                             *
*                                                                           *
* Description  : Function to stop the Rsna FSM  by stopping the Retransmit  *
*                Timer called from PNAC when an EAPOL-START comes from the  *
*                station which has an RSNA Session existing for it.         *        
*                                                                           *
* Input        : staMacAddr - Station MAC Address                           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/

UINT1
RsnaApiFsmStop (VOID *pRsnaSessionNode)
{
    RsnaTmrStopTimer (&
                      (((tRsnaSessionNode *) pRsnaSessionNode)->
                       RsnaReTransTmr));
    return RSNA_SUCCESS;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaDeletePmkCache                                         *
*                                                                           *
* Description  : Function to delete the PMK cache entry                     *
*                done state                                                 *
*                                                                           *
* Input        : staMacAddr - Station MAC Address                           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns     :  RSNA_SUCCESS or RSNA_FAILURE                               *
*****************************************************************************/
UINT1
RsnaDeletePmkCache (tMacAddr staMacAddr)
{
    tRsnaPmkSa         *pRsnaPmkSa = NULL;
    UINT1               au1NullBssid[RSNA_ETH_ADDR_LEN];

    MEMSET (au1NullBssid, 0, RSNA_ETH_ADDR_LEN);

    pRsnaPmkSa = RsnaUtilGetPmkSaFromCache (staMacAddr, au1NullBssid);
    if (pRsnaPmkSa != NULL)
    {
        RsnaUtilDeletePmkSa (pRsnaPmkSa);
    }
    return RSNA_SUCCESS;
}

#ifdef PMF_WANTED
/****************************************************************************
 Function    :  RsnaGetDot11RSNAProtectedManagementFramesActivated
 Input       :  The Indices
                IfIndex

                The Object 
                pi4RetValDot11RSNAProtectedManagementFramesActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAProtectedManagementFramesActivated (INT4 i4IfIndex,
                                                    INT4
                                                    *pi4RetValDot11RSNAProtectedManagementFramesActivated)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAProtectedManagementFramesActivated =
        pRsnaPaePortEntry->bRsnaPMFEnabled;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  RsnaGetDot11RSNAUnprotectedManagementFramesAllowed
 Input       :  The Indices
                IfIndex

                The Object 
                pi4retValDot11RSNAUnprotectedManagementFramesAllowed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11RSNAUnprotectedManagementFramesAllowed (INT4 i4IfIndex,
                                                    INT4
                                                    *pi4RetValDot11RSNAUnprotectedManagementFramesAllowed)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValDot11RSNAUnprotectedManagementFramesAllowed =
        pRsnaPaePortEntry->bRsnaUPMFEnabled;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11AssociationSAQueryMaximumTimeout
 Input       :  The Indices
                IfIndex

                The Object
                retValDot11AssociationSAQueryMaximumTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11AssociationSAQueryMaximumTimeout (INT4 i4IfIndex,
                                              UINT4
                                              *pu4RetValDot11AssociationSAQueryMaximumTimeout)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11AssociationSAQueryMaximumTimeout =
        pRsnaPaePortEntry->u4AssocSAQueryMaxTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaGetDot11AssociationSAQueryRetryTimeout
 Input       :  The Indices
                IfIndex

                The Object
                retValDot11AssociationSAQueryRetryTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetDot11AssociationSAQueryRetryTimeout (INT4 i4IfIndex,
                                            UINT4
                                            *pu4RetValDot11AssociationSAQueryRetryTimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11AssociationSAQueryRetryTimeout =
        pRsnaPaePortEntry->u4AssocSAQueryRetryTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAProtectedManagementFramesActivated 
 Input       :  The Indices
                IfIndex

                The Object 
                i4SetValDot11RSNAProtectedManagementFramesActivated 
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAProtectedManagementFramesActivated (INT4 i4IfIndex,
                                                    INT4
                                                    i4SetValDot11RSNAProtectedManagementFramesActivated)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->bRsnaPMFEnabled =
        (BOOL1) i4SetValDot11RSNAProtectedManagementFramesActivated;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11RSNAUnprotectedManagementFramesAllowed
 Input       :  The Indices
                IfIndex

                The Object 
                i4SetValDot11RSNAUnprotectedManagementFramesAllowed
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11RSNAUnprotectedManagementFramesAllowed (INT4 i4IfIndex,
                                                    INT4
                                                    i4SetValDot11RSNAUnprotectedManagementFramesAllowed)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->bRsnaUPMFEnabled =
        (BOOL1) i4SetValDot11RSNAUnprotectedManagementFramesAllowed;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11AssociationSAQueryMaximumTimeout 
 Input       :  The Indices
                IfIndex

                The Object 
                u4SetValDot11AssociationSAQueryMaximumTimeout 
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11AssociationSAQueryMaximumTimeout (INT4 i4IfIndex,
                                              UINT4
                                              u4SetValDot11AssociationSAQueryMaximumTimeout)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->u4AssocSAQueryMaxTime =
        u4SetValDot11AssociationSAQueryMaximumTimeout;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsnaSetDot11AssociationSAQueryRetryTimeout 
 Input       :  The Indices
                IfIndex

                The Object 
                u4SetValDot11AssociationSAQueryRetryTimeout 
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaSetDot11AssociationSAQueryRetryTimeout (INT4 i4IfIndex,
                                            UINT4
                                            u4SetValDot11AssociationSAQueryRetryTimeout)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->u4AssocSAQueryRetryTime =
        u4SetValDot11AssociationSAQueryRetryTimeout;

    return SNMP_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApiSAQueryInd                                         *
*                                                                           *
* Description  : Function to Process Mlme SAQuery Indication                *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApiSAQueryInd (tWssRSNANotifyParams * pWssRSNANotifyParams)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4ProfileIndex;

    if (WssIfGetProfileIfIndex (pWssRSNANotifyParams->u4IfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return OSIX_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        return OSIX_FAILURE;
    }

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo,
                                                  pWssRSNANotifyParams->
                                                  MLMESAQUERYIND.au1StaMac);

    if (pRsnaSupInfo != NULL)
    {

        if (pWssRSNANotifyParams->eWssRsnaNotifyType == WSSRSNA_SAQUERY_REQ_IND)
        {
            /* When the station information is available in the Supplicant Info DB,
               then send  the SAQuery Response message to the station */

            WssStaSendSAQueryResponseMsg (pWssRSNANotifyParams->MLMESAQUERYIND.
                                          au1StaMac,
                                          pWssRSNANotifyParams->u4IfIndex,
                                          pWssRSNANotifyParams->MLMESAQUERYIND.
                                          u2TransactionId,
                                          pWssRSNANotifyParams->MLMESAQUERYIND.
                                          u2SessId);
        }
        else if (pWssRSNANotifyParams->eWssRsnaNotifyType ==
                 WSSRSNA_SAQUERY_RESP_IND)
        {
            if (pRsnaSupInfo->u2TransactionIdentifier ==
                pWssRSNANotifyParams->MLMESAQUERYIND.u2TransactionId)
            {
                /*SA Query Response for the corresponding SA Query Request has been received */
                /*continue normal processing */

            }
            else
            {
                /* SA Query Response received has incorrect transaction identifier */
                /* So terminate the station entry */
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApiSendSAQueryReq                                      *
*                                                                           *
* Description  : Function to Construct and Send the SA Query Request        *
*                to Station                                                 *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApiSendSAQueryReq (tMacAddr StaMacAddr, UINT4 u4BssIfIndex, UINT2 u2SessId)
{
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4ProfileIndex;

    if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return OSIX_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        return OSIX_FAILURE;
    }

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, StaMacAddr);

    if (pRsnaSupInfo != NULL)
    {
        WssStaSendSAQueryReqMsg (StaMacAddr, u4BssIfIndex,
                                 pRsnaSupInfo->u2TransactionIdentifier,
                                 u2SessId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaApiIsStaPMFCapable                                     *
*                                                                           *
* Description  : Function to check whether the STA is PMF Capable           *
*                                                                           *
* Input        : StaMacAddr   :- Mac address of the STA                     *
*                u4BssIfIndex :- BssIfIndex of the WLAN to which STA is     *
*                                Associated                                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaApiIsStaPMFCapable (tMacAddr StaMacAddr, UINT4 u4BssIfIndex)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4ProfileIndex;

    if (WssIfGetProfileIfIndex (u4BssIfIndex, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return OSIX_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        return OSIX_FAILURE;
    }

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, StaMacAddr);

    /* Check whether a RSNA Session Node is available for the given STA MAC and also the
     * STA is PMF Capable */

    if (pRsnaSupInfo != NULL)
    {
        if ((pRsnaSupInfo->pRsnaSessNode != NULL)
            && (pRsnaSupInfo->bPMFCapable))
        {
            return OSIX_SUCCESS;
        }

    }

    return OSIX_FAILURE;

}
#endif

/****************************************************************************
*                                                                           *
* Function     : RsnaGetRSNAType                                            *
*                                                                           *
* Description  : To get the RSNA authentication type (PSK or Dot1x)         *
*          by calculating the length of passphrase length                *
*                                                                           *
* Input        : u2WlanProfileIfIndex - WlanIfIndex                         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns     :  Passphrase Length                                          *
*****************************************************************************/
UINT1
RsnaGetRSNAType (UINT2 u2WlanIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry (u2WlanIfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnIEParams:- "
                  "RsnaGetPortEntry Failed \n");
        return RSNA_FAILURE;
    }
    return (STRLEN (pRsnaPaePortEntry->RsnaConfigDB.au1PskPassPhrase));
}

/****************************************************************************
*                                                                           *
* Function     : RsnaPnacDeleteSession                                      *
*                                                                           *
* Description  : Function to clear the pnac session entry for the given     *
*                STA MAC                                                    *
*                                                                           *
* Input        : StaMacAddr   :- Mac address of the STA                     *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

VOID
RsnaPnacDeleteSession (tMacAddr staMacAddr)
{

    tPnacAuthSessionNode *ppSessNode = NULL;
    UINT4               u4Index = 0;

    if (PnacAuthGetAuthInProgressTblEntryByMac (staMacAddr, &ppSessNode)
        == PNAC_SUCCESS)
    {
        if (PnacAuthDeleteAuthInProgressTblEntry (ppSessNode) != PNAC_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      " DeleteSession: Error: Node Not Present in AIP Table...Continuing\n");
        }

    }

    if (PnacAuthGetSessionTblEntryByMac (staMacAddr, &ppSessNode) ==
        PNAC_SUCCESS)
    {
        PnacAuthCalcSessionTblIndex (ppSessNode->rmtSuppMacAddr, &u4Index);

        PNAC_HASH_DEL_NODE (PNAC_AUTH_SESSION_TABLE,
                            &ppSessNode->nextAuthSessionNode, u4Index);
        PnacL2IwfSetSuppMacAuthStatus (ppSessNode->rmtSuppMacAddr,
                                       PNAC_PORTSTATUS_UNAUTHORIZED);
    }

}

#ifdef WPA_WANTED
/****************************************************************************
*                                                                           *
* Function     : WpaGetFsRSNAEnabled                        *
*                                                                           *
* Description  : Function to check whether WPA is enabled or not           *
*                                                                           *
* Input        : IFINDEX                                        *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT1
WpaGetFsRSNAEnabled (UINT4 u4IfIndex, INT4 *pi4FsRSNAEnabled)
{
    if (nmhGetFsRSNAConfigActivated ((INT4) u4IfIndex, pi4FsRSNAEnabled) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
*                                                                           *
* Function     : WpaGetRsnIEParams                        *
*                                                                           *
* Description  : Function to Get the WPA IE Elements                    *
*                                                                           *
* Input        : IFINDEX                                        *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT1
WpaGetRsnIEParams (UINT4 u4IfIndex, tWpaIEElements * pWpaIEElements)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT1               u1Index = 0;
    UINT1               u1Count = 0;
    UINT1               u1Length = 0;
    UINT1               au1WpaOui[] = { 0x00, 0x50, 0xf2 };
    RsnaGetPortEntry ((UINT2) u4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaGetRsnIEParams:- "
                  "RsnaGetPortEntry Failed \n");
        return RSNA_FAILURE;
    }
    pWpaIEElements->u1ElemId = WPA_IE_ID;
    u1Length++;
    pWpaIEElements->u2Ver = 0x01;
    u1Length = (UINT1) (u1Length + 2);
    MEMCPY (pWpaIEElements->au1GroupCipherSuite,
            pRsnaPaePortEntry->WpaConfigDB.au1GroupCipher,
            RSNA_MAX_CIPHER_TYPE_LENGTH);
    u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    u1Count = 0;

    for (u1Index = 0; u1Index <= WPA_PAIRWISE_CIPHER_COUNT; u1Index++)
    {
        if (pRsnaPaePortEntry->aWpaPwCipherDB[u1Index].
            bPairwiseCipherEnabled == OSIX_TRUE)
        {
            MEMCPY (pWpaIEElements->aWpaPwCipherDB[u1Count].au1PairwiseCipher,
                    pRsnaPaePortEntry->aWpaPwCipherDB[u1Index].
                    au1PairwiseCipher, RSNA_MAX_CIPHER_TYPE_LENGTH);
            u1Count++;
            u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);

        }
    }
    pWpaIEElements->u2PairwiseCipherSuiteCount = u1Count;
    u1Length = (UINT1) (u1Length + 2);
    u1Count = 0;
    if (pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].
        bAkmSuiteEnabled == OSIX_TRUE)
    {
        MEMCPY (pWpaIEElements->aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTHSUITE_PSK -
                                             1].au1AuthSuite,
                RSNA_AKM_SELECTOR_LEN);
        u1Count++;
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    }
    else if (pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].
             bAkmSuiteEnabled == OSIX_TRUE)
    {
        MEMCPY (pWpaIEElements->aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].au1AKMSuite,
                pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTHSUITE_8021X -
                                             1].au1AuthSuite,
                RSNA_AKM_SELECTOR_LEN);
        u1Count++;
        u1Length = (UINT1) (u1Length + RSNA_MAX_CIPHER_TYPE_LENGTH);
    }
    pWpaIEElements->u2AKMSuiteCount = u1Count;
    u1Length = (UINT1) (u1Length + 2);
    pWpaIEElements->u1Len = u1Length;
    UNUSED_PARAM (au1WpaOui);
    return RSNA_SUCCESS;
}

/****************************************************************************
 Function    :  WpaTestv2FsRSNAConfigAuthenticationSuiteEnabled
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                testValDot11RSNAConfigAuthenticationSuiteEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaTestv2FsRSNAConfigAuthenticationSuiteEnabled (UINT4
                                                 *pu4ErrorCode,
                                                 UINT4
                                                 u4FsRSNAConfigAuthenticationSuiteIndex,
                                                 INT4
                                                 i4TestValFsRSNAConfigAuthenticationSuiteEnabled)
{

    UNUSED_PARAM (pu4ErrorCode);

    if (((u4FsRSNAConfigAuthenticationSuiteIndex != RSNA_AUTHSUITE_PSK) &&
         (u4FsRSNAConfigAuthenticationSuiteIndex != RSNA_AUTHSUITE_8021X))
        || ((i4TestValFsRSNAConfigAuthenticationSuiteEnabled != RSNA_ENABLED)
            && (i4TestValFsRSNAConfigAuthenticationSuiteEnabled !=
                RSNA_DISABLED)))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpaSetFsRSNAConfigAuthenticationSuiteEnabled
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                setValDot11RSNAConfigAuthenticationSuiteEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaSetFsRSNAConfigAuthenticationSuiteEnabled (UINT4 u4ProfileId,
                                              UINT4
                                              u4FsRSNAConfigAuthenticationSuiteIndex,
                                              INT4
                                              i4SetValFsRSNAConfigAuthenticationSuiteEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) u4ProfileId, &pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return SNMP_FAILURE;
    }

    pRsnaPaePortEntry->WpaConfigDB.u1AKMSuiteMethod =
        (UINT1) u4FsRSNAConfigAuthenticationSuiteIndex;
    if (i4SetValFsRSNAConfigAuthenticationSuiteEnabled == (INT4) RSNA_ENABLED)
    {
        pRsnaPaePortEntry->aWpaAkmDB
            [u4FsRSNAConfigAuthenticationSuiteIndex - 1].bAkmSuiteEnabled
            = OSIX_TRUE;
        return SNMP_SUCCESS;
    }
    else if (i4SetValFsRSNAConfigAuthenticationSuiteEnabled == RSNA_DISABLED)
    {
        pRsnaPaePortEntry->aWpaAkmDB
            [u4FsRSNAConfigAuthenticationSuiteIndex - 1].bAkmSuiteEnabled
            = OSIX_FALSE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  RsnaGetFsRSNAConfigAuthenticationSuite
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                retValDot11RSNAConfigAuthenticationSuite
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
RsnaGetFsRSNAConfigAuthenticationSuite (UINT4 u4ProfileId,
                                        UINT4
                                        u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValDot11RSNAConfigAuthenticationSuite)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) u4ProfileId, &pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAConfigAuthenticationSuite->pu1_OctetList,
            pRsnaPaePortEntry->
            aWpaAkmDB[u4Dot11RSNAConfigAuthenticationSuiteIndex -
                      1].au1AuthSuite, RSNA_AKM_SELECTOR_LEN);

    pRetValDot11RSNAConfigAuthenticationSuite->i4_Length =
        (INT4) RSNA_AKM_SELECTOR_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpaGetFsRSNAConfigPairwiseKeysSupported
 Input       :  The Indices
                IfIndex

                The Object 
                retValWpaGetFsRSNAConfigPairwiseKeysSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAConfigPairwiseKeysSupported (INT4 i4IfIndex,
                                         UINT4
                                         *pu4RetValDot11RSNAConfigPairwiseKeysSupported)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValDot11RSNAConfigPairwiseKeysSupported =
        pRsnaPaePortEntry->WpaConfigDB.u4PairwiseKeysSupported;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpaGetFsRSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAConfigGroupCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAConfigGroupCipher (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValDot11RSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAConfigGroupCipher->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1GroupCipher,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAConfigGroupCipher->i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  WpaGetFsRSNAAuthenticationSuiteRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAAuthenticationSuiteRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAAuthenticationSuiteRequested (INT4 i4IfIndex,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pRetValDot11RSNAAuthenticationSuiteRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAAuthenticationSuiteRequested->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1AuthSuiteRequested,
            RSNA_MAX_AUTH_SUITE_TYPE_LENGTH);

    pRetValDot11RSNAAuthenticationSuiteRequested->i4_Length =
        RSNA_MAX_AUTH_SUITE_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpaGetFsRSNAPairwiseCipherRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAPairwiseCipherRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAPairwiseCipherRequested (INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDot11RSNAPairwiseCipherRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAPairwiseCipherRequested->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1PairwiseCipherRequested,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAPairwiseCipherRequested->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  WpaGetFsRSNAGroupCipherRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAGroupCipherRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAGroupCipherRequested (INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDot11RSNAGroupCipherRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAGroupCipherRequested->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1GroupCipherRequested,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAGroupCipherRequested->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpaGetFsRSNAPairwiseCipherSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValDot11RSNAPairwiseCipherSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAPairwiseCipherSelected (INT4 i4IfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValDot11RSNAPairwiseCipherSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValDot11RSNAPairwiseCipherSelected->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1PairwiseCipherSelected,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValDot11RSNAPairwiseCipherSelected->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  WpaTestRadioType
 Input       :  The Indices
                IfIndex

 Description :  Ensures that WPA is not configured for BGN,AN or AC Type.  
                
 Returns     :  RSNA_SUCCESS or RSNA_FAILURE
****************************************************************************/
INT1
WpaTestRadioType (INT4 i4IfIndex)
{
    if (WssWlanTestRadioType (i4IfIndex) == OSIX_SUCCESS)
    {
        return RSNA_SUCCESS;
    }
    return RSNA_FAILURE;
}

/****************************************************************************
 Function    :  WpaGetFirstIndexFsWPAConfigAuthenticationSuitesTable 
 Input       :  AuthenticationSuite Index
                IfIndex

 Description :  Get the first entry of WPAConfigAuthenticationSuitesTable
                
 Returns     :  SNMP_FAILURE or SNMP_SUCCESS
****************************************************************************/

INT1
WpaGetFirstIndexFsWPAConfigAuthenticationSuitesTable (INT4 *pi4IfIndex,
                                                      UINT4
                                                      *pu4Dot11RSNAConfigAuthenticationSuiteIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4IfIndex = (INT4) pRsnaPaePortEntry->u2Port;
    }
    *pu4Dot11RSNAConfigAuthenticationSuiteIndex = RSNA_MIN_AUTH_SUITE_INDEX;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  WpaGetNextIndexFsWPAConfigAuthenticationSuitesTable 
 Input       :  AuthenticationSuite Index
                IfIndex

 Description :  Get the first entry of WPAConfigAuthenticationSuitesTable
                
 Returns     :  SNMP_FAILURE or SNMP_SUCCESS
****************************************************************************/
INT1
WpaGetNextIndexFsWPAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                     INT4 *pi4NextIfIndex,
                                                     UINT4
                                                     u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                     UINT4
                                                     *pu4NextDot11RSNAConfigAuthenticationSuiteIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;

    if (i4IfIndex == 0)
    {
        RsnaGetFirstPortEntry (&pNextRsnaPaePortEntry);
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = (INT4) pNextRsnaPaePortEntry->u2Port;
            *pu4NextDot11RSNAConfigAuthenticationSuiteIndex =
                RSNA_MIN_AUTH_SUITE_INDEX;
        }
    }
    else
    {
        if (u4Dot11RSNAConfigAuthenticationSuiteIndex <
            RSNA_MAX_AUTH_SUITE_INDEX)
        {
            *pi4NextIfIndex = i4IfIndex;
            *pu4NextDot11RSNAConfigAuthenticationSuiteIndex =
                u4Dot11RSNAConfigAuthenticationSuiteIndex +
                RSNA_MIN_AUTH_SUITE_INDEX;
        }
        else if (u4Dot11RSNAConfigAuthenticationSuiteIndex >
                 RSNA_MAX_AUTH_SUITE_INDEX)
        {
            return SNMP_FAILURE;
        }
        else
        {
            /* fetch the data structure of the current index */
            RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

            if (pRsnaPaePortEntry == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No Rsna Info for the given port!! ... \n");
                return (SNMP_FAILURE);
            }

            RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);
            if (pNextRsnaPaePortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            *pi4NextIfIndex = (INT4) pNextRsnaPaePortEntry->u2Port;
            *pu4NextDot11RSNAConfigAuthenticationSuiteIndex
                = RSNA_MIN_AUTH_SUITE_INDEX;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WpaGetFsRSNAConfigAuthenticationSuiteEnabled
 Input       :  The Indices
                Dot11RSNAConfigAuthenticationSuiteIndex

                The Object 
                retValDot11RSNAConfigAuthenticationSuiteEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
WpaGetFsRSNAConfigAuthenticationSuiteEnabled (INT4 i4IfIndex,
                                              UINT4
                                              u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                              INT4
                                              *pi4RetValFsRSNAConfigAuthenticationSuiteEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (pRsnaPaePortEntry->aWpaAkmDB
        [u4Dot11RSNAConfigAuthenticationSuiteIndex - 1].bAkmSuiteEnabled ==
        OSIX_TRUE)
    {

        *pi4RetValFsRSNAConfigAuthenticationSuiteEnabled = (INT4) RSNA_ENABLED;
    }
    else
    {
        *pi4RetValFsRSNAConfigAuthenticationSuiteEnabled = (INT4) RSNA_DISABLED;
    }

    return SNMP_SUCCESS;

}

#endif
