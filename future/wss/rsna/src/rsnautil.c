/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnautil.c,v 1.5 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"
#include "rsnaprot.h"
#include "utilrand.h"

PRIVATE UINT1       gau1RsnaBroadCast[] =
    { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGetRandom                                          *
*                                                                           *
* Description  : Function to generate random number                         *
*                                                                           *
* Input        : pu1Ptr :- Pointer to the generated random number           *
*                u1Len  :- Length of the random number                      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
RsnaUtilGetRandom (UINT1 *pu1Ptr, UINT1 u1Len)
{
    UINT4               u4Temp = 0;
    UINT1              *pu1Temp = NULL;

    pu1Temp = pu1Ptr;

    while (u1Len > sizeof (UINT4))
    {
        u4Temp = (UINT4) RAND ();
        MEMCPY (pu1Temp, &u4Temp, sizeof (UINT4));
        pu1Temp += sizeof (UINT4);
        u1Len = (UINT1) (u1Len - sizeof (UINT4));
    }

    u4Temp = (UINT4) RAND ();
    MEMCPY (pu1Temp, &u4Temp, sizeof (UINT4));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGetStaInfoFromStaAddr                              *
*                                                                           *
* Description  : Function to get station info from mac address              *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Pointer to rsna port info              *
*                pu1Addr          :- Refers to station mac address          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaSupInfo                                               * 
*                                                                           *
*****************************************************************************/
tRsnaSupInfo       *
RsnaUtilGetStaInfoFromStaAddr (tRsnaPaePortEntry * pRsnaPaePortInfo,
                               UINT1 *pu1Addr)
{

    tRsnaSupInfo       *pRsnaSuppInfo = NULL;

    TMO_SLL_Scan (&(pRsnaPaePortInfo->RsnaSupInfoList),
                  pRsnaSuppInfo, tRsnaSupInfo *)
    {

        if (MEMCMP (pRsnaSuppInfo->au1SuppMacAddr, pu1Addr, RSNA_ETH_ADDR_LEN)
            == 0)
        {
            return (pRsnaSuppInfo);
        }
    }
    return (NULL);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilStartTkipCtrMsrs                                   *
*                                                                           *
* Description  : Function to start tkip ctr measures                        *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Pointer to rsna port info              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
RsnaUtilStartTkipCtrMsrs (tRsnaPaePortEntry * pRsnaPaePortInfo)
{
    pRsnaPaePortInfo->u4NoOfCtrMsrsInvoked++;
    pRsnaPaePortInfo->bTkipCtrMsrFlag = TRUE;
    if (RsnaUtilGtkRekey (pRsnaPaePortInfo) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilStartTkipCtrMsrs:- RsnaUtilGtkRekey Failed !!!! \n");
        return;
    }
    if (RsnaTmrStopTimer (&(pRsnaPaePortInfo->RsnaCtrMsrTmr)) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilStartTkipCtrMsrs:- "
                  "RsnaTmrStopTimer Failed !!!! \n");
        return;
    }
    if (RsnaTmrStartTimer (&(pRsnaPaePortInfo->RsnaCtrMsrTmr),
                           RSNA_TKIP_CTR_MSR_TIMER_ID,
                           RSNA_MIC_FAIL_CTR_MSR_INTRVL,
                           (VOID *) pRsnaPaePortInfo) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilStartTkipCtrMsrs:- "
                  "RsnaTmrStartTimer Failed !!!! \n");
        return;
    }
    if (RsnaUtilKickOfAllStations (pRsnaPaePortInfo) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilStartTkipCtrMsrs:-"
                  " RsnaUtilKickOfAllStations Failed !!!! \n");
        return;
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilCreateAndAddStaToList                              *
*                                                                           *
* Description  : Function to create and station to the list                 *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Pointer to rsna port info              *
*                pu1SuppAddr :- Refers to the supplicant address            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaSupInfo                                               * 
*                                                                           *
*****************************************************************************/
tRsnaSupInfo       *
RsnaUtilCreateAndAddStaToList (tRsnaPaePortEntry * pRsnaPaePortInfo,
                               UINT1 *pu1SuppAddr)
{

    tRsnaSupInfo       *pRsnaSuppInfo = NULL;

    if ((pRsnaSuppInfo = RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_SUPP_INFO_MEMPOOL_ID))
        == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilCreateAndAddStaToList:- MemAllocMemBlk Failed \n");
        return (NULL);
    }

    MEMSET (pRsnaSuppInfo, 0, sizeof (tRsnaSupInfo));
    MEMCPY (pRsnaSuppInfo->au1SuppMacAddr, pu1SuppAddr, RSNA_ETH_ADDR_LEN);

    TMO_SLL_Add (&(pRsnaPaePortInfo->RsnaSupInfoList),
                 (tTMO_SLL_NODE *) pRsnaSuppInfo);
    return (pRsnaSuppInfo);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilIncByteArray                                       *
*                                                                           *
* Description  : Increment the byte to get the next seq no                  *
*                                                                           *
* Input        : pu1Ptr :- Refers to the byte that has to be incremented    *
*                u2Len :- Length of the ptr                                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilIncByteArray (UINT1 *pu1Ptr, UINT2 u2Len)
{

    INT2                i2PtrOff = 0;

    i2PtrOff = (INT2) u2Len;
    i2PtrOff--;

    while (i2PtrOff >= 0)
    {
        pu1Ptr[i2PtrOff]++;
        if (pu1Ptr[i2PtrOff] == 0)
        {
            i2PtrOff--;
        }
        else
        {
            break;
        }
    }
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGetPmkSaFromCache                                  *
*                                                                           *
* Description  : Function to get PMKSA from station address                 *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Pointer to rsna port info              *
*                pu1Addr :-  Refers to the supp mac addr                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaPmkSa                                                 *
*                                                                           *
*****************************************************************************/

tRsnaPmkSa         *
RsnaUtilGetPmkSaFromCache (UINT1 *pu1Addr, UINT1 *pu1Bssid)
{
    tRsnaPmkSa          pInRsnaPmkSa;
    tRsnaPmkSa         *pRsnaPmkSa = NULL;

    MEMCPY (pInRsnaPmkSa.au1SuppMac, pu1Addr, RSNA_ETH_ADDR_LEN);
    MEMCPY (pInRsnaPmkSa.au1SrcMac, pu1Bssid, RSNA_ETH_ADDR_LEN);

    pRsnaPmkSa =
        RBTreeGet (gRsnaGlobals.RsnaPmkTable, ((tRBElem *) & pInRsnaPmkSa));

    return pRsnaPmkSa;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGetPskFromStaAddr                                  *
*                                                                           *
* Description  : Function to get psk from station address                   *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Pointer to rsna port info              *
*                pu1Addr :- Refers to the mac address                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaPskDB                                                 *
*                                                                           *
*****************************************************************************/
tRsnaPskDB         *
RsnaUtilGetPskFromStaAddr (tRsnaPaePortEntry * pRsnaPaePortEntry,
                           UINT1 *pu1Addr)
{
    tRsnaPskDB         *pRsnaPsk = NULL;

    TMO_SLL_Scan (&(pRsnaPaePortEntry->RsnaPskList), pRsnaPsk, tRsnaPskDB *)
    {
        if (MEMCMP (pRsnaPsk->au1SuppMacAddr, pu1Addr, RSNA_ETH_ADDR_LEN) == 0)
        {
            return (pRsnaPsk);
        }
    }
    return (NULL);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGtkRekey                                           *
*                                                                           *
* Description  : Function to rekeyt the gtk                                 *
*                                                                           *
* Input        : pRsnaPaePortEntry :- Pointer to rsna port info             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilGtkRekey (tRsnaPaePortEntry * pRsnaPaePortInfo)
{
    UINT1               u1Tmp = 0;
    UINT1               u1Count = 0;

    for (u1Count = 0; u1Count < RSNA_GTK_SECOND_IDX; u1Count++)
    {
        u1Tmp = pRsnaPaePortInfo->RsnaGlobalGtk.u1GM;
        pRsnaPaePortInfo->RsnaGlobalGtk.u1GM =
            pRsnaPaePortInfo->RsnaGlobalGtk.u1GN;
        pRsnaPaePortInfo->RsnaGlobalGtk.u1GN = u1Tmp;
        MEMCPY (pRsnaPaePortInfo->RsnaGlobalGtk.au1Gnonce,
                pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter, RSNA_NONCE_LEN);
        RsnaUtilIncByteArray (pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter,
                              RSNA_NONCE_LEN);
    }

    if (RsnaSessGenGtkFromGmk (&(pRsnaPaePortInfo->RsnaGlobalGtk)) ==
        OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaUtilGtkRekey:- RsnaSessGenGtkFromGmk Failed !!!!! \n");
        return (RSNA_FAILURE);
    }
    return (RSNA_FAILURE);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilKickOfAllStations                                  *
*                                                                           *
* Description  : Function invoked to kick of all stations                   *
*                                                                           *
* Input        : pRsnaPaePortInfo :- Pointer to rsna port info              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilKickOfAllStations (tRsnaPaePortEntry * pRsnaPaePortInfo)
{
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4Count = 0;
    UINT4               u4LoopIdx = 0;

    u4Count = TMO_SLL_Count (&(pRsnaPaePortInfo->RsnaSupInfoList));

    for (u4LoopIdx = 0; u4LoopIdx < u4Count; u4LoopIdx++)
    {
        TMO_SLL_Scan (&(pRsnaPaePortInfo->RsnaSupInfoList), pRsnaSupInfo,
                      tRsnaSupInfo *)
        {

            if (pRsnaSupInfo->pRsnaSessNode != NULL)
            {
                pRsnaSupInfo->pRsnaSessNode->bDeleteSession = TRUE;
                pRsnaSupInfo->pRsnaSessNode->u4RefCount--;

                if (RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode) !=
                    RSNA_SUCCESS)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "RsnaUtilKickOfAllStations:- "
                              "RsnaSessDeleteSession Failed !!!! \n");
                    return (RSNA_FAILURE);
                }
                else
                {
                    break;
                }
            }
        }
    }
#ifdef WSS_DEBUG_WANTED            /*RSNA TODO MERGE */
    if (RsnaApmeEnqMsgToApme (pRsnaPaePortInfo, NULL, NULL, MLME_KICK_ALL_STA,
                              FALSE) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilKickOfAllStations:- "
                  "RsnaApmeEnqMsgToApme Failed !!!! \n");
        return (RSNA_FAILURE);
    }
#endif
    return (OSIX_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilDeletePmkSa                                        *
*                                                                           *
* Description  : Function to delete pmk sa                                  *
*                                                                           *
* Input        : pRsnaPmkSa :- Pointer to rsna pmksa                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilDeletePmkSa (tRsnaPmkSa * pRsnaPmkSa)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;

    RsnaGetPortEntry ((UINT2) pRsnaPmkSa->u4IfIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilDeletePmkSa:- pRsnaPaePortInfo is NULL !!!! \n");
        return (RSNA_FAILURE);
    }
    /*PmkSa Rekey timer usage needs to be determined */
/*

    RsnaTmrStopTimer (&(pRsnaPmkSa->PmkSaTimer));
*/
    RsnaTmrStopTimer (&(pRsnaPmkSa->PmkSaDeleteTmr));

    RBTreeRemove (gRsnaGlobals.RsnaPmkTable, (tRBElem *) pRsnaPmkSa);

    if ((RSNA_MEM_RELEASE_MEM_BLK (RSNA_PMKSA_MEMPOOL_ID, pRsnaPmkSa)) ==
        MEM_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaSessDeleteSession:- RSNA_RELEASE_PMKSA_NODE"
                  "Failed !!!! \n");
        return (RSNA_FAILURE);
    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilAddPmkSaToCache                                    *
*                                                                           *
* Description  : Function to add pmksa to cache                             *
*                                                                           *
* Input        : u2Port :- Port information                                 *
*                pPnacAuthSessionNode :- Pointer to pnac auth session node  *
*                pu1Pmk :- Refers to the PMK                                *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaUtilAddPmkSaToCache (tRsnaPaePortEntry * pRsnaPaePortInfo,
                         tPnacAuthSessionNode * pPnacAuthSessionNode,
                         UINT1 *pu1Pmk, UINT1 *pu1PmkId)
{

    tRsnaPmkSa         *pRsnaPmkSa = NULL;
    tRsnaPmkSa         *pRsnaOldPmkSa = NULL;
    tCfaIfInfo          IfInfo;
#if 0
    UINT4               u4LifeTime = 0;
#endif
    UINT1               au1Bssid[RSNA_ETH_ADDR_LEN];
    UINT1               au1NullBssid[RSNA_ETH_ADDR_LEN];

    MEMSET (au1Bssid, 0, RSNA_ETH_ADDR_LEN);
    MEMSET (au1NullBssid, 0, RSNA_ETH_ADDR_LEN);

    CfaGetIfInfo (pPnacAuthSessionNode->u2PortNo, &IfInfo);

    if ((pRsnaPmkSa =
         RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_PMKSA_MEMPOOL_ID)) == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilAddPmkSaToCache:- MemAllocMemBlk Failed !!!!! \n");
        return (RSNA_FAILURE);
    }

    MEMSET (pRsnaPmkSa, 0, sizeof (tRsnaPmkSa));

    /* In our case, element id is always RSNA_IE_ID */
    pRsnaPaePortInfo->au1RsnaIE[0] = RSNA_IE_ID;

    if (pRsnaPaePortInfo->au1RsnaIE[0] == RSNA_IE_ID)
    {
        MEMCPY (pRsnaPmkSa->au1Akm, RSNA_AKM_UNSPEC_802_1X,
                RSNA_CIPHER_SUITE_LEN);
    }
    else if (pRsnaPaePortInfo->au1RsnaIE[0] == WPA_IE_ID)
    {
        MEMCPY (pRsnaPmkSa->au1Akm, WPA_AKM_UNSPEC_802_1X,
                RSNA_CIPHER_SUITE_LEN);
    }
    MEMCPY (pRsnaPmkSa->au1SuppMac,
            &(pPnacAuthSessionNode->rmtSuppMacAddr), RSNA_ETH_ADDR_LEN);

    MEMCPY (pRsnaPmkSa->au1SrcMac, IfInfo.au1MacAddr, RSNA_ETH_ADDR_LEN);

    pRsnaPmkSa->u4IfIndex = pRsnaPaePortInfo->u2Port;

    MEMCPY (pRsnaPmkSa->au1Pmk, pu1Pmk, RSNA_PMK_LEN);

    if (RsnaSessGenPmkId (pRsnaPmkSa) != RSNA_SUCCESS)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilAddPmkSaToCache:- "
                  "RsnaSessGenPmkId retured Failure !!!!! \n");
        return (RSNA_FAILURE);
    }

    MEMCPY (pu1PmkId, pRsnaPmkSa->au1PmkId, RSNA_PMKID_LEN);

    if (pRsnaPaePortInfo->RsnaConfigDB.u4PMKLifeTime != 0)
    {

#if 0
        if (pRsnaPaePortInfo->RsnaConfigDB.u4PmkReAuthThreshold != 0)
        {
            u4LifeTime =
                ((pRsnaPaePortInfo->RsnaConfigDB.
                  u4PMKLifeTime *
                  pRsnaPaePortInfo->RsnaConfigDB.
                  u4PmkReAuthThreshold) / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        }
        if (RsnaTmrStartTimer
            (&(pRsnaPmkSa->PmkSaTimer), RSNA_PMK_REKEY_TIMER_ID, u4LifeTime,
             pRsnaPmkSa) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaUtilAddPmkSaToCache:-"
                      "RsnaTmrStartTimer for PMK_REAUTH returned"
                      "Failure !!!! \n");
            return (RSNA_FAILURE);
        }
#endif
        if (RsnaTmrStartTimer (&(pRsnaPmkSa->PmkSaDeleteTmr),
                               RSNA_DELETEPMK_TIMER_ID,
                               pRsnaPaePortInfo->RsnaConfigDB.
                               u4PMKLifeTime, pRsnaPmkSa) != RSNA_SUCCESS)
        {

            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaUtilAddPmkSaToCache:- "
                      "RsnaTmrStartTimer for PMK_DELETE returned"
                      "Failure !!!! \n");
            return (RSNA_FAILURE);
        }
    }

    if (pRsnaPaePortInfo->RsnaConfigDB.u1RsnaOkcStatus == RSNA_ENABLED)
    {
        pRsnaOldPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaPmkSa->au1SuppMac,
                                                   au1NullBssid);
    }
    else
    {
        pRsnaOldPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaPmkSa->au1SuppMac,
                                                   pRsnaPmkSa->au1SrcMac);
    }

    if (pRsnaOldPmkSa != NULL)
    {
        RsnaUtilDeletePmkSa (pRsnaOldPmkSa);
    }

    if (RBTreeAdd (gRsnaGlobals.RsnaPmkTable, pRsnaPmkSa) == RB_FAILURE)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC,
                  "RsnaUtilAddPmkSaToCache:- RBTreeAdd" "returned Failure\n");
        MemReleaseMemBlock (RSNA_PMKSA_MEMPOOL_ID, (UINT1 *) pRsnaPmkSa);
        return (RSNA_FAILURE);

    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilValidateIE                                         *
*                                                                           *
* Description  : Function to validate Rsna IE                               *
*                                                                           *
* Input        : pRsnaElems :- Pointer to rsna elements                     *
*                pRsnaSupInfo :- Pointer to station info                    *
*                u4IfIndex :- The port on which the pkt has been recvd      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaUtilValidateIE (tRsnaElements * pRsnaElems,
                    tRsnaSupInfo * pRsnaSupInfo, UINT4 u4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4ProfileIndex = 0;
    UINT1              *pu1AssocPtr = NULL;
    UINT1              *pu1TkipPtr = NULL;
    UINT1              *pu1CcmpPtr = NULL;
    UINT1              *pu1PskPtr = NULL;
    UINT1              *pu1AsPtr = NULL;
#ifdef PMF_WANTED
    UINT1              *pu1PskSha256Ptr = NULL;
    UINT1              *pu1AsSha256Ptr = NULL;
#endif

    if (WssIfGetProfileIfIndex (u4IfIndex, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaUtilValidateIE:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return (RSNA_FAILURE);
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "No Rsna Info for the given port !!!!! \n");
        return (RSNA_FAILURE);
    }

    if (MEMCMP (pRsnaElems->au1GroupCipherSuite,
                pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipher,
                RSNA_CIPHER_SUITE_LEN) != 0)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid Group Cipher !!!!!! |n");
        return (RSNA_REASON_GROUP_CIPHER_NOT_VALID);
    }
    else
    {
        MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipherSelected,
                pRsnaElems->au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);

        MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1GroupCipherRequested,
                pRsnaElems->au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);

    }

    pu1AssocPtr = pRsnaElems->au1PairwiseCipher;
    pu1TkipPtr =
        pRsnaPaePortEntry->aRsnaPwCipherDB[RSNA_TKIP_INDEX].au1PairwiseCipher;
    pu1CcmpPtr =
        pRsnaPaePortEntry->aRsnaPwCipherDB[RSNA_CCMP_INDEX].au1PairwiseCipher;
    if ((MEMCMP (pu1AssocPtr, pu1TkipPtr, RSNA_CIPHER_SUITE_LEN) != 0)
        && (MEMCMP (pu1AssocPtr, pu1CcmpPtr, RSNA_CIPHER_SUITE_LEN) != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid Pairwise Cipher !!!!!! |n");
        return (RSNA_REASON_PAIRWISE_CIPHER_NOT_VALID);
    }
    else
    {
        MEMCPY (pRsnaSupInfo->au1PairwiseCipher,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
        MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1PairwiseCipherSelected,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);

        MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1PairwiseCipherRequested,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);

    }
    pu1AssocPtr = pRsnaElems->au1AKMSuite;
    pu1PskPtr = pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTH_PSK_INDEX].au1AuthSuite;
    pu1AsPtr = pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTH_AS_INDEX].au1AuthSuite;
#ifdef PMF_WANTED
    pu1PskSha256Ptr =
        pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTH_PSK_SHA256_INDEX].au1AuthSuite;
    pu1AsSha256Ptr =
        pRsnaPaePortEntry->aRsnaAkmDB[RSNA_AUTH_AS_SHA256_INDEX].au1AuthSuite;
#endif
    if ((MEMCMP (pu1AssocPtr, pu1PskPtr, RSNA_AKM_SELECTOR_LEN) != 0)
        && (MEMCMP (pu1AssocPtr, pu1AsPtr, RSNA_AKM_SELECTOR_LEN) != 0)
#ifdef PMF_WANTED
        && (MEMCMP (pu1AssocPtr, pu1PskSha256Ptr, RSNA_AKM_SELECTOR_LEN) != 0)
        && (MEMCMP (pu1AssocPtr, pu1AsSha256Ptr, RSNA_AKM_SELECTOR_LEN)) != 0
#endif
        )
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid AKM Suite !!!!!! |n");
        return (RSNA_REASON_AKMP_NOT_VALID);
    }
    else
    {
        MEMCPY (pRsnaSupInfo->au1AkmSuite, pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
        MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1AuthSuiteSelected,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
        MEMCPY (pRsnaPaePortEntry->RsnaConfigDB.au1AuthSuiteRequested,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
    }

#ifdef PMF_WANTED
    if ((pRsnaElems->u2RsnCapab & RSNA_CAPABILITY_MFPC) == RSNA_CAPABILITY_MFPC)
    {
        pRsnaSupInfo->bPMFCapable = TRUE;
    }
#endif
    return (RSNA_SUCCESS);
}

#ifdef WPA_WANTED
/****************************************************************************
*                                                                           *
* Function     : WpaUtilValidateIE                                         *
*                                                                           *
* Description  : Function to validate Rsna IE                               *
*                                                                           *
* Input        : pRsnaElems :- Pointer to rsna elements                     *
*                pRsnaSupInfo :- Pointer to station info                    *
*                u4IfIndex :- The port on which the pkt has been recvd      *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
WpaUtilValidateIE (tRsnaElements * pRsnaElems,
                   tRsnaSupInfo * pRsnaSupInfo, UINT4 u4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4ProfileIndex = 0;
    UINT1              *pu1AssocPtr = NULL;
    UINT1              *pu1TkipPtr = NULL;
    UINT1              *pu1CcmpPtr = NULL;
    UINT1              *pu1PskPtr = NULL;
    UINT1              *pu1AsPtr = NULL;
    UINT1               au1TempCipherSuite[RSNA_CIPHER_SUITE_LEN];

    MEMSET (au1TempCipherSuite, 0, RSNA_CIPHER_SUITE_LEN);
    if (WssIfGetProfileIfIndex (u4IfIndex, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaUtilValidateIE:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return (RSNA_FAILURE);
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "No Rsna Info for the given port !!!!! \n");
        return (RSNA_FAILURE);
    }
    if (MEMCMP (pRsnaElems->au1GroupCipherSuite,
                pRsnaPaePortEntry->WpaConfigDB.au1GroupCipher,
                RSNA_CIPHER_SUITE_LEN) != 0)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid Group Cipher !!!!!! |n");
        return (RSNA_REASON_GROUP_CIPHER_NOT_VALID);
    }
    else
    {
        MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1GroupCipherSelected,
                pRsnaElems->au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);

        MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1GroupCipherRequested,
                pRsnaElems->au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);

    }

    pu1AssocPtr = pRsnaElems->au1PairwiseCipher;
    pu1TkipPtr =
        pRsnaPaePortEntry->aWpaPwCipherDB[RSNA_TKIP_INDEX].au1PairwiseCipher;
    pu1CcmpPtr =
        pRsnaPaePortEntry->aWpaPwCipherDB[RSNA_CCMP_INDEX].au1PairwiseCipher;
    if ((MEMCMP (pu1AssocPtr, pu1TkipPtr, RSNA_CIPHER_SUITE_LEN) != 0) &&
        (MEMCMP (pu1AssocPtr, pu1CcmpPtr, RSNA_CIPHER_SUITE_LEN) != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid Pairwise Cipher !!!!!! |n");
        return (RSNA_REASON_PAIRWISE_CIPHER_NOT_VALID);
    }
    else
    {
        MEMCPY (pRsnaSupInfo->au1PairwiseCipher,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
        MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1PairwiseCipherSelected,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);

        MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1PairwiseCipherRequested,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);

    }
    pu1AssocPtr = pRsnaElems->au1AKMSuite;
    pu1PskPtr = pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTH_PSK_INDEX].au1AuthSuite;
    pu1AsPtr = pRsnaPaePortEntry->aWpaAkmDB[RSNA_AUTH_AS_INDEX].au1AuthSuite;
    if ((MEMCMP (pu1AssocPtr, pu1PskPtr, RSNA_AKM_SELECTOR_LEN) != 0)
        && (MEMCMP (pu1AssocPtr, pu1AsPtr, RSNA_AKM_SELECTOR_LEN) != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid AKM Suite !!!!!! |n");
        return (RSNA_REASON_AKMP_NOT_VALID);
    }
    else
    {
        MEMCPY (pRsnaSupInfo->au1AkmSuite, pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
        MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1AuthSuiteSelected,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
        MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1AuthSuiteRequested,
                pu1AssocPtr, RSNA_CIPHER_SUITE_LEN);
    }

    return (RSNA_SUCCESS);
}
#endif

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilParseIE                                            *
*                                                                           *
* Description  : Function to parse IE                                       *
*                                                                           *
* Input        : pu1Pos :- Pointer to rsna IE                               *
*                pIappAddReq - Add Request primitive struture               *
*                u2Len :- Length of the Rsna Element                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilParseIE (UINT1 *pu1Pos, tRsnaElements * pRsna, UINT2 u2IeLen)
{
    UINT4               u4Idx = 0;
    tRsnaIE            *pRsnaIE = NULL;
    UINT2               u2RemLen = 0;

    pRsnaIE = (tRsnaIE *) (VOID *) pu1Pos;
    if (u2IeLen < sizeof (tRsnaIE))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Too small Rsna Information element !!!! \n");
        return (RSNA_FAILURE);
    }
    if ((pRsnaIE->u1RsnaElementId != RSNA_IE_ID) ||
        (LE16_TO_CPU (pRsnaIE->u2Version) != RSNA_VERSION))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }
    pRsna->pu1IE = pu1Pos;
    pRsna->u1ElemId = RSNA_IE_ID;
    pRsna->u1Len = pRsnaIE->u1Len;
    pRsna->u2Ver = LE16_TO_CPU (pRsnaIE->u2Version);

    pu1Pos += sizeof (tRsnaIE);
    u2RemLen = (UINT2) (u2IeLen - sizeof (tRsnaIE));

    if (u2RemLen > RSNA_CIPHER_SUITE_LEN)
    {
        MEMCPY (pRsna->au1GroupCipherSuite, pu1Pos, RSNA_CIPHER_SUITE_LEN);
        pu1Pos += RSNA_CIPHER_SUITE_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_CIPHER_SUITE_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "No Group Cipher Suite Information in IE !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen > RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2PairwiseCipherSuiteCount = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2PairwiseCipherSuiteCount =
            LE16_TO_CPU (pRsna->u2PairwiseCipherSuiteCount);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen > RSNA_CIPHER_SUITE_LEN)
    {
        MEMCPY (pRsna->au1PairwiseCipher, pu1Pos, RSNA_CIPHER_SUITE_LEN);
        pu1Pos += RSNA_CIPHER_SUITE_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_CIPHER_SUITE_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen > RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2AKMSuiteCount = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2AKMSuiteCount = LE16_TO_CPU (pRsna->u2AKMSuiteCount);
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen > RSNA_CIPHER_SUITE_LEN)
    {
        MEMCPY (pRsna->au1AKMSuite, pu1Pos, RSNA_CIPHER_SUITE_LEN);
        pu1Pos += RSNA_CIPHER_SUITE_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_CIPHER_SUITE_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen >= RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2RsnCapab = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2RsnCapab = LE16_TO_CPU (pRsna->u2RsnCapab);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
    }

    if (u2RemLen >= RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2PmkidCount = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2PmkidCount = LE16_TO_CPU (pRsna->u2PmkidCount);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
    }

    if (u2RemLen >= (pRsna->u2PmkidCount * RSNA_PMKID_LEN))
    {
        for (u4Idx = 0;
             ((u4Idx < pRsna->u2PmkidCount) &&
              (u4Idx < RSNA_MAX_PMKID_IDS)); u4Idx++)
        {
            MEMCPY (pRsna->
                    aPmkidList[MEM_MAX_BYTES (u4Idx, RSNA_MAX_PMKID_IDS)],
                    pu1Pos, RSNA_PMKID_LEN);
            pu1Pos += RSNA_PMKID_LEN;
            u2RemLen = (UINT2) (u2RemLen - RSNA_PMKID_LEN);
        }
    }
    if (u2RemLen > 0)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Incomplete RSNA IE Parsing !!!! \n");
        return (RSNA_FAILURE);
    }
    return (RSNA_SUCCESS);
}

#ifdef WPA_WANTED
/****************************************************************************
*                                                                           *
* Function     : RsnaUtilParseWpaIE                                         *
*                                                                           *
* Description  : Function to parse IE                                       *
*                                                                           *
* Input        : pu1Pos :- Pointer to rsna IE                               *
*                pIappAddReq - Add Request primitive struture               *
*                u2Len :- Length of the Rsna Element                        *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilParseWpaIE (UINT1 *pu1Pos, tRsnaElements * pRsna, UINT2 u2IeLen)
{
    tWpaIE             *pWpaIE = NULL;
    UINT2               u2RemLen = 0;

    pWpaIE = (tWpaIE *) (VOID *) pu1Pos;

    if (u2IeLen < sizeof (tWpaIE))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Too small Wpa Information element !!!! \n");
        return (RSNA_FAILURE);
    }
    if ((pWpaIE->u1WpaElementId != WPA_IE_ID) ||
        (pWpaIE->u1Len != (u2IeLen - RSNA_IE_VERSION_OFF)) ||
        (LE16_TO_CPU (pWpaIE->u2Version) != WPA_VERSION))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }
    pRsna->pu1IE = pu1Pos;
    pRsna->u1ElemId = WPA_IE_ID;
    pRsna->u1Len = pWpaIE->u1Len;
    pRsna->u2Ver = LE16_TO_CPU (pWpaIE->u2Version);

    pu1Pos += sizeof (tWpaIE);
    u2RemLen = (UINT2) (u2IeLen - sizeof (tWpaIE));

    if (u2RemLen >= RSNA_CIPHER_SUITE_LEN)
    {
        MEMCPY (pRsna->au1GroupCipherSuite, pu1Pos, RSNA_CIPHER_SUITE_LEN);
        pu1Pos += RSNA_CIPHER_SUITE_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_CIPHER_SUITE_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "No Group Cipher Suite Information in IE !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen >= RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2PairwiseCipherSuiteCount = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2PairwiseCipherSuiteCount =
            LE16_TO_CPU (pRsna->u2PairwiseCipherSuiteCount);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen >= RSNA_CIPHER_SUITE_LEN)
    {
        MEMCPY (pRsna->au1PairwiseCipher, pu1Pos, RSNA_CIPHER_SUITE_LEN);
        pu1Pos += RSNA_CIPHER_SUITE_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_CIPHER_SUITE_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }

    if (u2RemLen >= RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2AKMSuiteCount = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2AKMSuiteCount = LE16_TO_CPU (pRsna->u2AKMSuiteCount);
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }
    if (u2RemLen >= RSNA_CIPHER_SUITE_LEN)
    {
        MEMCPY (pRsna->au1AKMSuite, pu1Pos, RSNA_CIPHER_SUITE_LEN);
        pu1Pos += RSNA_CIPHER_SUITE_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_CIPHER_SUITE_LEN);
    }
    else
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Short Rsna IE Header !!!! \n");
        return (RSNA_FAILURE);
    }
    if (u2RemLen >= RSNA_IE_SUITE_COUNT_LEN)
    {
        pRsna->u2RsnCapab = *((UINT2 *) (VOID *) pu1Pos);
        pRsna->u2RsnCapab = LE16_TO_CPU (pRsna->u2RsnCapab);
        pu1Pos += RSNA_IE_SUITE_COUNT_LEN;
        u2RemLen = (UINT2) (u2RemLen - RSNA_IE_SUITE_COUNT_LEN);
    }
    if (u2RemLen > 0)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Incomplete RSNA IE Parsing !!!! \n");
        return (RSNA_FAILURE);
    }
    return (RSNA_SUCCESS);
}
#endif

/*****************************************************************************/
/* Function Name      : RsnaCompareProfileInterface                          */
/*                                                                           */
/* Description        : This function compares the two PortEntryNode         */
/*                      based on the port number                             */
/*                                                                           */
/* Input(s)           : e1        Pointer to First BindingIfNode             */
/* Input(s)           : e2        Pointer to Second BindingIfNode            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1. RSNA_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                      2. RSNA_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                      3. RSNA_LESSER  - If key of first element is lesser  */
/*                                        than key of second element         */
/*****************************************************************************/
INT4
RsnaCompareProfileInterface (tRBElem * e1, tRBElem * e2)
{
    tRsnaPaePortEntry  *pRBNode1 = (tRsnaPaePortEntry *) e1;
    tRsnaPaePortEntry  *pRBNode2 = (tRsnaPaePortEntry *) e2;

    if (pRBNode1->u2Port < pRBNode2->u2Port)
    {
        return RSNA_LESSER;
    }
    else if (pRBNode1->u2Port > pRBNode2->u2Port)
    {
        return RSNA_GREATER;
    }
    return RSNA_EQUAL;
}

/*****************************************************************************/
/* Function Name      : RsnaComparePmkId                                     */
/*                                                                           */
/* Description        : This function compares the two PortEntryNode         */
/*                      based on the port number                             */
/*                                                                           */
/* Input(s)           : e1        Pointer to First BindingIfNode             */
/* Input(s)           : e2        Pointer to Second BindingIfNode            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1. RSNA_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                      2. RSNA_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                      3. RSNA_LESSER  - If key of first element is lesser  */
/*                                        than key of second element         */
/*****************************************************************************/
INT4
RsnaComparePmkId (tRBElem * e1, tRBElem * e2)
{
    tRsnaPmkSa         *pRBNode1 = (tRsnaPmkSa *) e1;
    tRsnaPmkSa         *pRBNode2 = (tRsnaPmkSa *) e2;
    UINT1               auNullNBssId[RSNA_ETH_ADDR_LEN];

    MEMSET (auNullNBssId, 0, RSNA_ETH_ADDR_LEN);

    if (MEMCMP (pRBNode1->au1SrcMac, auNullNBssId, RSNA_ETH_ADDR_LEN) == 0)
    {

        if (MEMCMP
            (pRBNode1->au1SuppMac, pRBNode2->au1SuppMac, RSNA_ETH_ADDR_LEN) < 0)
        {
            return RSNA_LESSER;
        }
        else if (MEMCMP
                 (pRBNode1->au1SuppMac, pRBNode2->au1SuppMac,
                  RSNA_ETH_ADDR_LEN) > 0)
        {
            return RSNA_GREATER;
        }

        return RSNA_EQUAL;
    }

    if (MEMCMP (pRBNode1->au1SuppMac, pRBNode2->au1SuppMac, RSNA_ETH_ADDR_LEN) <
        0)
    {
        return RSNA_LESSER;
    }
    else if (MEMCMP
             (pRBNode1->au1SuppMac, pRBNode2->au1SuppMac,
              RSNA_ETH_ADDR_LEN) > 0)
    {
        return RSNA_GREATER;
    }
    else if (MEMCMP
             (pRBNode1->au1SrcMac, pRBNode2->au1SrcMac, RSNA_ETH_ADDR_LEN) < 0)
    {
        return RSNA_LESSER;
    }
    else if (MEMCMP
             (pRBNode1->au1SrcMac, pRBNode2->au1SrcMac, RSNA_ETH_ADDR_LEN) > 0)
    {
        return RSNA_GREATER;
    }

    return RSNA_EQUAL;
}

/*****************************************************************************/
/* Function Name      : RsnaCompareSuppStatsIndex                            */
/*                                                                           */
/* Description        : This function compares the two PortEntryNode         */
/*                      based on the port number                             */
/*                                                                           */
/* Input(s)           : e1        Pointer to First BindingIfNode             */
/* Input(s)           : e2        Pointer to Second BindingIfNode            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1. RSNA_GREATER - If key of first element is greater */
/*                                        than key of second element         */
/*                      2. RSNA_EQUAL   - If key of first element is equal   */
/*                                        to key of second element           */
/*                      3. RSNA_LESSER  - If key of first element is lesser  */
/*                                        than key of second element         */
/*****************************************************************************/

INT4
RsnaCompareSuppStatsIndex (tRBElem * e1, tRBElem * e2)
{

    tRsnaSuppStatsInfo *pRBNode1 = (tRsnaSuppStatsInfo *) e1;
    tRsnaSuppStatsInfo *pRBNode2 = (tRsnaSuppStatsInfo *) e2;

    UINT1               au1ZeroMac[RSNA_ETH_ADDR_LEN];

    MEMSET (au1ZeroMac, 0, RSNA_ETH_ADDR_LEN);

    if (pRBNode1->u2Port < pRBNode2->u2Port)
    {
        return RSNA_LESSER;
    }
    else if (pRBNode1->u2Port > pRBNode2->u2Port)
    {
        return RSNA_GREATER;
    }

    if (MEMCMP (pRBNode1->au1SuppMacAddr, au1ZeroMac, RSNA_ETH_ADDR_LEN) == 0)
    {

        if (pRBNode1->u4StatsIndex < pRBNode2->u4StatsIndex)
        {
            return RSNA_LESSER;
        }
        else if (pRBNode1->u4StatsIndex > pRBNode2->u4StatsIndex)
        {
            return RSNA_GREATER;
        }

        return RSNA_EQUAL;
    }

    if (MEMCMP (pRBNode1->au1SuppMacAddr, pRBNode2->au1SuppMacAddr,
                RSNA_ETH_ADDR_LEN) < 0)
    {
        return RSNA_LESSER;
    }
    else if (MEMCMP (pRBNode1->au1SuppMacAddr, pRBNode2->au1SuppMacAddr,
                     RSNA_ETH_ADDR_LEN) > 0)
    {
        return RSNA_GREATER;
    }

    return RSNA_EQUAL;
}

/*****************************************************************************/
/* Function Name      : RsnaAddPortEntry                                     */
/*                                                                           */
/* Description        : This function adds the entry to the RSNA             */
/*                      port table                                           */
/*                                                                           */
/* Input(s)           : u2PortNum -u2Port Index                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
RsnaAddPortEntry (UINT4 u4IfIndex)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    UINT1               au1RandNo[RSNA_NONCE_LEN] = "";
    UINT1               au1Buf[RSNA_INIT_CTR_DATA_LEN] = "";
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT4               u4TimeStamp = 0;
    UINT1               u1Ctr = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (au1RandNo, 0, RSNA_NONCE_LEN);
    MEMSET (au1Buf, 0, RSNA_INIT_CTR_DATA_LEN);
    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);

    if ((pRsnaPaePortInfo = (tRsnaPaePortEntry *) RSNA_MEM_ALLOCATE_MEM_BLK
         (RSNA_PORTINFO_MEMPOOL_ID)) == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC,
                  "RsnaAddPortEntry:- MemAllocMemBlk" "returned Failure\n");
        return (RSNA_FAILURE);
    }

    MEMSET (pRsnaPaePortInfo, 0, sizeof (tRsnaPaePortEntry));
    pRsnaPaePortInfo->u2Port = (UINT2) u4IfIndex;

    if (RBTreeAdd (gRsnaGlobals.RsnaPortEntryTable, pRsnaPaePortInfo)
        == RB_FAILURE)
    {

        if (RsnaTmrStopTimer (&(pRsnaPaePortInfo->RsnaCtrMsrTmr)) !=
            RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaAddPortEntry():- RsnaTmrStopTimer Failed !!!! \n");
            return (RSNA_FAILURE);
        }

        RSNA_TRC (RSNA_INIT_SHUT_TRC,
                  "RsnaAddPortEntry:- RBTreeAdd" "returned Failure\n");
        MemReleaseMemBlock (RSNA_PORTINFO_MEMPOOL_ID,
                            (UINT1 *) pRsnaPaePortInfo);
        return (RSNA_FAILURE);
    }

    CfaGetIfInfo (u4IfIndex, &IfInfo);
#ifdef WSS_DEBUG_WANTED
    if (RsnaInitPnacPortConfig (u2PortNum, PNAC_PORT_AUTHMODE_MACBASED)
        == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC,
                  "RsnaAddPortEntry :- RsnaInitPnacPortConfig "
                  "returned Failure\n");
        return (OSIX_FAILURE);
    }
#endif

    MEMCPY (pRsnaPaePortInfo->au1SrcMac, IfInfo.au1MacAddr, RSNA_ETH_ADDR_LEN);
    TMO_SLL_Init (&(pRsnaPaePortInfo->RsnaPskList));
    TMO_SLL_Init (&(pRsnaPaePortInfo->RsnaSupInfoList));
    RsnaUtilGetRandom (au1RandNo, RSNA_NONCE_LEN);
    RsnaUtilGetRandom (pRsnaPaePortInfo->RsnaGlobalGtk.au1Gmk, RSNA_GMK_LEN);
    MEMCPY (au1Buf, "Init Counter", RSNA_INIT_COUNT_LABLE_LEN);
    au1Buf[RSNA_INIT_COUNT_LABLE_LEN] = 0;
    MEMCPY (&(au1Buf[RSNA_INIT_COUNT_LABLE_LEN + 1]),
            pRsnaPaePortInfo->au1SrcMac, RSNA_ETH_ADDR_LEN);
    OsixGetSysTime (&u4TimeStamp);
    MEMCPY (&(au1Buf[RSNA_INIT_COUNT_LABLE_LEN + 1 + RSNA_ETH_ADDR_LEN]),
            &u4TimeStamp, sizeof (UINT4));
    OsixGetSysTime (&u4TimeStamp);
    MEMCPY (&(au1Buf[RSNA_INIT_COUNT_LABLE_LEN + 1 +
                     RSNA_ETH_ADDR_LEN + sizeof (UINT4)]),
            &u4TimeStamp, sizeof (UINT4));

    RsnaAlgoHmacSha1 (au1RandNo, RSNA_NONCE_LEN, au1Buf, RSNA_INIT_CTR_DATA_LEN,
                      pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter);

    u1Ctr++;
    au1Buf[RSNA_INIT_COUNT_LABLE_LEN] = u1Ctr;

    RsnaAlgoHmacSha1 (au1RandNo, RSNA_NONCE_LEN, au1Buf, RSNA_INIT_CTR_DATA_LEN,
                      au1Digest);

    MEMCPY (&(pRsnaPaePortInfo->RsnaGlobalGtk.
              au1Counter[SHA1_HASH_SIZE]), au1Digest,
            (RSNA_NONCE_LEN - SHA1_HASH_SIZE));

    pRsnaPaePortInfo->RsnaGlobalGtk.pRsnaPaePortInfo = (pRsnaPaePortInfo);

    pRsnaPaePortInfo->RsnaGlobalGtk.bGInit = TRUE;
    pRsnaPaePortInfo->RsnaGlobalGtk.bGTKAuthenticator = FALSE;

    /*Initialization of RSNA ConfigDB */
    pRsnaPaePortInfo->RsnaConfigDB.u4Version = RSNA_VERSION;
    pRsnaPaePortInfo->RsnaConfigDB.u4PairwiseKeysSupported =
        RSNA_PAIRWISE_CIPHER_COUNT;
    MEMCPY (pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
            gau1RsnaCipherSuiteCCMP, RSNA_MAX_CIPHER_TYPE_LENGTH);
    pRsnaPaePortInfo->RsnaConfigDB.u1GroupRekeyMethod =
        RSNA_GROUP_REKEY_TIMEBASED;
    pRsnaPaePortInfo->RsnaConfigDB.u4GroupGtkRekeyTime =
        RSNA_DEF_GROUP_REKEY_TIME;
    pRsnaPaePortInfo->RsnaConfigDB.u4GroupRekeyPkts = RSNA_DEF_REKEY_PKTS;
    pRsnaPaePortInfo->RsnaConfigDB.bGroupRekeyStrict = RSNA_DISABLED;
    pRsnaPaePortInfo->RsnaConfigDB.u4GroupUpdateCount = RSNA_DEF_UPDATE_COUNT;
    pRsnaPaePortInfo->RsnaConfigDB.u4PairwiseUpdateCount =
        RSNA_DEF_UPDATE_COUNT;
    pRsnaPaePortInfo->RsnaConfigDB.u4PMKLifeTime = RSNA_DEF_PMK_LIFETIME;
    pRsnaPaePortInfo->RsnaConfigDB.u4PmkReAuthThreshold =
        RSNA_DEF_PMK_REAUTH_THRESHOLD;
    /*pRsnaPaePortInfo->RsnaConfigDB.u4SATimeOut = RSNA_DEF_SA_TIME_OUT; */
    pRsnaPaePortInfo->RsnaConfigDB.u4SATimeOut = 1;

    pRsnaPaePortInfo->RsnaConfigDB.u1RsnaOkcStatus = RSNA_ENABLED;

    MEMCPY (pRsnaPaePortInfo->aRsnaPwCipherDB[RSNA_TKIP - 1].
            au1PairwiseCipher, gau1RsnaCipherSuiteTKIP,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    MEMCPY (pRsnaPaePortInfo->aRsnaPwCipherDB[RSNA_CCMP - 1].
            au1PairwiseCipher, gau1RsnaCipherSuiteCCMP,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRsnaPaePortInfo->aRsnaPwCipherDB[RSNA_CCMP - 1].
        bPairwiseCipherEnabled = OSIX_TRUE;

    pRsnaPaePortInfo->aRsnaPwCipherDB[RSNA_TKIP - 1].u4PairwiseCipherSize = 256;    /* Length in bits for 
                                                                                     * pairwise cipher TKIP*/
    pRsnaPaePortInfo->aRsnaPwCipherDB[RSNA_CCMP - 1].u4PairwiseCipherSize = 128;    /* Length in bits for 
                                                                                     * pairwise cipher CCMP */

    MEMCPY (pRsnaPaePortInfo->aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].
            au1AuthSuite, gau1RsnaAuthKeyMgmtPskOver8021X,
            RSNA_AKM_SELECTOR_LEN);
    MEMCPY (pRsnaPaePortInfo->aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
            au1AuthSuite, gau1RsnaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN);
#ifdef WPA_WANTED
    pRsnaPaePortInfo->WpaConfigDB.u4Version = RSNA_VERSION;
    pRsnaPaePortInfo->WpaConfigDB.u4PairwiseKeysSupported =
        WPA_PAIRWISE_CIPHER_COUNT;
    MEMCPY (pRsnaPaePortInfo->WpaConfigDB.au1GroupCipher,
            gau1WpaCipherSuiteTKIP, RSNA_MAX_CIPHER_TYPE_LENGTH);
    pRsnaPaePortInfo->WpaConfigDB.u1GroupRekeyMethod =
        RSNA_GROUP_REKEY_TIMEBASED;
    pRsnaPaePortInfo->WpaConfigDB.u4GroupGtkRekeyTime =
        RSNA_DEF_GROUP_REKEY_TIME;
    pRsnaPaePortInfo->WpaConfigDB.u4GroupRekeyPkts = RSNA_DEF_REKEY_PKTS;
    pRsnaPaePortInfo->WpaConfigDB.bGroupRekeyStrict = RSNA_DISABLED;
    pRsnaPaePortInfo->WpaConfigDB.u4GroupUpdateCount = RSNA_DEF_UPDATE_COUNT;
    pRsnaPaePortInfo->WpaConfigDB.u4PairwiseUpdateCount = RSNA_DEF_UPDATE_COUNT;
    pRsnaPaePortInfo->WpaConfigDB.u4PMKLifeTime = RSNA_DEF_PMK_LIFETIME;
    pRsnaPaePortInfo->WpaConfigDB.u4PmkReAuthThreshold =
        RSNA_DEF_PMK_REAUTH_THRESHOLD;
    pRsnaPaePortInfo->WpaConfigDB.u4SATimeOut = 1;
/*PairWise*/
    MEMCPY (pRsnaPaePortInfo->aWpaPwCipherDB[RSNA_TKIP - 1].
            au1PairwiseCipher, gau1WpaCipherSuiteTKIP,
            RSNA_MAX_CIPHER_TYPE_LENGTH);
    pRsnaPaePortInfo->aWpaPwCipherDB[RSNA_TKIP - 1].bPairwiseCipherEnabled =
        OSIX_TRUE;
    pRsnaPaePortInfo->aWpaPwCipherDB[RSNA_TKIP - 1].u4PairwiseCipherSize = 256;    /* Length in bits for 
                                                                                 * pairwise cipher TKIP*/
/*Auth Suite*/
    MEMCPY (pRsnaPaePortInfo->aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].
            au1AuthSuite, gau1WpaAuthKeyMgmtPskOver8021X,
            RSNA_AKM_SELECTOR_LEN);
    MEMCPY (pRsnaPaePortInfo->aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].
            au1AuthSuite, gau1WpaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN);

#endif
#ifdef PMF_WANTED
    MEMCPY (pRsnaPaePortInfo->aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
            au1AuthSuite, gau1RsnaAuthKeyMgmtPskSHA256Over8021X,
            RSNA_AKM_SELECTOR_LEN);
    MEMCPY (pRsnaPaePortInfo->aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 - 1].
            au1AuthSuite, gau1RsnaAuthKeyMgmt8021XSHA256,
            RSNA_AKM_SELECTOR_LEN);
#endif
    pRsnaPaePortInfo->RsnaConfigDB.u4GroupUpdateCount = RSNA_DEF_UPDATE_COUNT;
    pRsnaPaePortInfo->RsnaConfigDB.bRSNAPreauthenticationImplemented =
        RSNA_DISABLED;
    pRsnaPaePortInfo->RsnaConfigDB.bRSNAOptionImplemented = RSNA_DISABLED;

    pRsnaPaePortInfo->bGtkRefreshFlag = FALSE;
    pRsnaPaePortInfo->bGtkInstallFlag = FALSE;

    return RSNA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : RsnaDeletePortEntry                                  */
/*                                                                           */
/* Description        : This function removes the entry from                 */
/*                      RsnaPortEntryTable                                   */
/*                                                                           */
/* Input(s)           : pRsnaPaePortEntry    - Pointer to                    */
/*                                             tRsnaPaePortEntry             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/
INT4
RsnaDeletePortEntry (UINT4 u4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) u4IfIndex, &pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC, "RsnaGetPortEntry" "returned Failure\n");
        return RSNA_FAILURE;
    }

    if (RBTreeRemove (gRsnaGlobals.RsnaPortEntryTable,
                      pRsnaPaePortEntry) == RB_FAILURE)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC,
                  "RsnaDeletePortEntry :- RBTreeRemove" "returned Failure\n");
        return RSNA_FAILURE;
    }

    if (RsnaTmrStopTimer (&(pRsnaPaePortEntry->RsnaCtrMsrTmr)) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaDeletePortEntry():- RsnaTmrStopTimer Failed !!!! \n");
        return (RSNA_FAILURE);
    }

    MemReleaseMemBlock (RSNA_PORTINFO_MEMPOOL_ID, (UINT1 *) pRsnaPaePortEntry);
    return RSNA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RsnaGetPortEntry                                     */
/*                                                                           */
/* Description        : Gets the pointer to the port table entry             */
/*                                                                           */
/* Input              : u2PortNum - Port Number                              */
/*                                                                           */
/* Output             : ppPortInfo - double Pointer to the port table entry  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.aPnacPaePortInfo                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
RsnaGetPortEntry (UINT2 u2PortNum, tRsnaPaePortEntry ** ppPortInfo)
{

    tRsnaPaePortEntry  *pTmpPortInfo = NULL;
    tRsnaPaePortEntry  *pPortEntry = NULL;

    if ((pPortEntry = (tRsnaPaePortEntry *) RSNA_MEM_ALLOCATE_MEM_BLK
         (RSNA_PORTINFO_MEMPOOL_ID)) == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC,
                  "RsnaGetPortEntry:- MemAllocMemBlk" "returned Failure\n");
        return;
    }

    RSNA_MEMSET (pPortEntry, 0, sizeof (tRsnaPaePortEntry));

    pPortEntry->u2Port = u2PortNum;

    pTmpPortInfo = RBTreeGet (gRsnaGlobals.RsnaPortEntryTable,
                              (tRBElem *) pPortEntry);

    *ppPortInfo = pTmpPortInfo;

    RSNA_MEM_RELEASE_MEM_BLK (RSNA_PORTINFO_MEMPOOL_ID, pPortEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : RsnaGetFirstPortEntry                                */
/*                                                                           */
/* Description        : This function returns pointer to the first RSNA port */
/*                      entry                                                */
/*                                                                           */
/* Input (s)          : NONE                                                 */
/*                                                                           */
/* Output(s)          : pRsnaPortEntry - Rsna Port Entry pointer for         */
/*                      which first node should be returned                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RsnaGetFirstPortEntry (tRsnaPaePortEntry ** pRsnaPortEntry)
{
    tRsnaPaePortEntry  *pTmpRsnaPortEntry = NULL;

    pTmpRsnaPortEntry = (tRsnaPaePortEntry *) RBTreeGetFirst
        (gRsnaGlobals.RsnaPortEntryTable);

    *pRsnaPortEntry = pTmpRsnaPortEntry;
    return;
}

/*****************************************************************************/
/* Function Name      : RsnaGetNextPortEntry                                 */
/*                                                                           */
/* Description        : This function returns pointer to the next remote port*/
/*                      entry                                                */
/*                      If this function called with NULL as pRsnaPortEntry  */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pRsnaPortEntry - Rsna Port Entry pointer for         */
/*                      which next node should be returned                   */
/*                                                                           */
/* Output(s)          : pNextRsnaPortEntry - pointer to the next Node        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RsnaGetNextPortEntry (tRsnaPaePortEntry * pRsnaPortEntry,
                      tRsnaPaePortEntry ** pNextRsnaPortEntry)
{
    tRsnaPaePortEntry  *pTmpRsnaPortEntry = NULL;

    if (pRsnaPortEntry == NULL)
    {

        pTmpRsnaPortEntry = (tRsnaPaePortEntry *) RBTreeGetFirst
            (gRsnaGlobals.RsnaPortEntryTable);
    }
    else
    {

        pTmpRsnaPortEntry = (tRsnaPaePortEntry *) RBTreeGetNext
            (gRsnaGlobals.RsnaPortEntryTable, (tRBElem *) pRsnaPortEntry, NULL);
    }

    *pNextRsnaPortEntry = pTmpRsnaPortEntry;
    return;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcAssocInd                                   *
*                                                                           *
* Description  : Function to Process Mlme Assoc Indication                  *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaProcAssocInd (tWssRSNANotifyParams * pWssRSNANotifyParams)
{
    tWssRSNANotifyParams *pRSNANotifyParams = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;
    UINT1               au1StaAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
    UINT1               u1IEType = 0;
    tRsnaElements       RsnaElems;
    UINT4               u4ReasonCode = 0;
    UINT4               u4RetVal = OSIX_FAILURE;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4ProfileIndex = 0;

/*PMF_WANTED*/

    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));

    u4BssIfIndex = pWssRSNANotifyParams->u4IfIndex;

    if ((pRSNANotifyParams = (tWssRSNANotifyParams *) RSNA_MEM_ALLOCATE_MEM_BLK
         (RSNA_WSS_INFO_MEMPOOL_ID)) == NULL)

    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessWssNotification:- "
                  "RSNA_MEM_ALLOCATE_MEM_BLK Failed \n");
        return;
    }

    if (WssIfGetProfileIfIndex (pWssRSNANotifyParams->u4IfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }

    MEMCPY (au1StaAddr,
            pWssRSNANotifyParams->MLMEASSOCIND.au1StaMac, RSNA_ETH_ADDR_LEN);

    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   " Received Assoc Indication from Station MAC:%x:%x:%x:%x:%x:%x\n",
                   au1StaAddr[0],
                   au1StaAddr[1],
                   au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaAddr);

    if (pRsnaSupInfo == NULL)
    {
        pRsnaSupInfo =
            RsnaUtilCreateAndAddStaToList (pRsnaPaePortInfo, au1StaAddr);
        if (pRsnaSupInfo == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                      "RsnaUtilCreateAndAddStaToList returned Failure \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
        gRsnaGlobals.u4RsnaStatsIndex++;
        gRsnaGlobals.u4RsnaStatsIndex =
            gRsnaGlobals.u4RsnaStatsIndex % RSNA_MAX_SUPP;
        pRsnaSuppStatsInfo =
            RsnaUtilAddStaToStatsTable (pRsnaPaePortInfo->u2Port,
                                        gRsnaGlobals.u4RsnaStatsIndex,
                                        au1StaAddr);
    }

    pRsnaSupInfo->u4SuppState = RSNA_STA_ASSOCIATED;
    MEMCPY (pRsnaSupInfo->au1RsnaIE,
            pWssRSNANotifyParams->MLMEASSOCIND.au1RsnIe,
            pWssRSNANotifyParams->MLMEASSOCIND.u1RsnaIELen);
    pRsnaSupInfo->u1IELen = pWssRSNANotifyParams->MLMEASSOCIND.u1RsnaIELen;
    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));
    u1IEType = pRsnaSupInfo->au1RsnaIE[0];
    if (pRsnaSupInfo->au1RsnaIE[0] == RSNA_IE_ID)
    {
        u4RetVal = RsnaUtilParseIE (pRsnaSupInfo->au1RsnaIE, &RsnaElems,
                                    pRsnaSupInfo->u1IELen);
    }
#ifdef WPA_WANTED
    else if (pRsnaSupInfo->au1RsnaIE[0] == WPA_IE_ID)
    {
        u4RetVal = RsnaUtilParseWpaIE (pRsnaSupInfo->au1RsnaIE, &RsnaElems,
                                       pRsnaSupInfo->u1IELen);
    }
#endif
    else
    {
        u4RetVal = OSIX_FAILURE;
    }
    if (u4RetVal == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = RSNA_REASON_INVALID_IE;

        pRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DISSOC_REQ;
        pRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac, pRsnaSupInfo->pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);    /*to be discussed */
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->pRsnaSessNode->u4ReasonCode;
            pRsnaSupInfo->pRsnaSessNode->bDeAuthReq = TRUE;
            RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode);
        }
        else if (pRsnaSupInfo != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac,
                    pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->u4ReasonCode;
        }
        if (RsnaProcessWssNotification (pRSNANotifyParams) != OSIX_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaProcAssocInd:- RsnaProcessWssNotification"
                      " returned Failure !!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK (RSNA_SUPP_INFO_MEMPOOL_ID,
                                          pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                          pRSNANotifyParams);
                return;
            }
        }
        else
        {
            pRsnaSupInfo->pRsnaSessNode->u4RefCount--;
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }

        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }
    if (u1IEType == RSNA_IE_ID)
    {
        u4ReasonCode = RsnaUtilValidateIE (&RsnaElems, pRsnaSupInfo,
                                           pWssRSNANotifyParams->u4IfIndex);
    }
#ifdef WPA_WANTED
    else if (u1IEType == WPA_IE_ID)
    {
        u4ReasonCode = WpaUtilValidateIE (&RsnaElems, pRsnaSupInfo,
                                          pWssRSNANotifyParams->u4IfIndex);
    }
#endif
    if (u4ReasonCode != OSIX_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = u4ReasonCode;
        pRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DISSOC_REQ;
        pRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac,
                    pRsnaSupInfo->pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->pRsnaSessNode->u4ReasonCode;
            pRsnaSupInfo->pRsnaSessNode->bDeAuthReq = TRUE;
            RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode);
        }
        else if (pRsnaSupInfo != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac,
                    pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->u4ReasonCode;
        }
        if (RsnaProcessWssNotification (pRSNANotifyParams) != OSIX_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaProcAssocInd:- RsnaProcessWssNotification"
                      " returned Failure !!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK (RSNA_SUPP_INFO_MEMPOOL_ID,
                                          pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                          pRSNANotifyParams);
                return;
            }
        }
        else
        {
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }

    if (pRsnaSuppStatsInfo != NULL)
    {
        pRsnaSuppStatsInfo->RsnaStatsDB.u4Version = RsnaElems.u2Ver;
    }

    /* TBD - The below code is commented temporary.
     * Temporary fix for 1/4 message drop in WTP with kernel capwap
     * Explicility making the PmkidList as zero in all cases do to Full authentication 
     */

    if (RsnaElems.u2PmkidCount == 0)
    {
        MEMSET (RsnaElems.aPmkidList[0], 0, RSNA_PMKID_LEN);
    }

    /* MEMSET (RsnaElems.aPmkidList[0], 0, RSNA_PMKID_LEN); */

#if 0 /*RAJA*/
        for (u2PmkIdCount = 0; ((u2PmkIdCount < RsnaElems.u2PmkidCount) &&
                                (bPmkSaFlag == OSIX_FALSE)); u2PmkIdCount++)
    {
        pRsnaPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaSupInfo->au1SuppMacAddr,
                                                RsnaElems.
                                                aPmkidList[u2PmkIdCount]);
        if (pRsnaPmkSa == NULL)
        {
            continue;
        }
        else
        {
            bPmkSaFlag = OSIX_TRUE;
            MEMCPY (pRsnaPaePortInfo->RsnaConfigDB.au1PmkIdUsed,
                    RsnaElems.aPmkidList[u2PmkIdCount], RSNA_PMKID_LEN);
        }
    }

#endif

/* This is not required,need to be revisited during PMK Caching Implementation */
/*
    if ((bPmkSaFlag == OSIX_FALSE) && (u2PmkIdCount != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaApmeProcMlmeAssocReAssocInd:-"
                  "No Pmksa for the Given PMKID. !!! \n");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }
*/

    if (pRsnaSupInfo->pRsnaSessNode == NULL)
    {
        if (u1IEType == RSNA_IE_ID)
        {
            if (RsnaSessHandleNewStation
                (pRsnaPaePortInfo, pRsnaSupInfo, u4BssIfIndex,
                 &RsnaElems, RSNA_IE_ID) == OSIX_FAILURE)
            {
                RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                          "RsnaApmeProcMlmeAssocReAssocInd:-"
                          "RsnaSessHandleNewStation Failed !!! \n");
                RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                          pRSNANotifyParams);
                return;
            }
        }
#ifdef WPA_WANTED
        else if (u1IEType == WPA_IE_ID)
        {
            if (WpaSessHandleNewStation
                (pRsnaPaePortInfo, pRsnaSupInfo, u4BssIfIndex,
                 &RsnaElems, WPA_IE_ID) == OSIX_FAILURE)
            {
                RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                          "RsnaApmeProcMlmeAssocReAssocInd:-"
                          "WpaSessHandleNewStation Failed !!! \n");
                RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                          pRSNANotifyParams);
                return;
            }

        }
#endif
#ifdef PMF_DEBUG_WANTED
        MEMCPY (&pRsnaSupInfo->pRsnaSessNode->RsnaIgtkSeqNum[0],
                &pWssRSNANotifyParams->PMFVendorInfo.RsnaIGTKSeqInfo[0],
                sizeof (tRsnaIGTKSeqInfo));
        MEMCPY (&pRsnaSupInfo->pRsnaSessNode->RsnaIgtkSeqNum[1],
                &pWssRSNANotifyParams->PMFVendorInfo.RsnaIGTKSeqInfo[1],
                sizeof (tRsnaIGTKSeqInfo));
        MEMSET (&pRsnaSupInfo->pRsnaSessNode->RsnaIgtkSeqNum, 0,
                sizeof (tRsnaIGTKSeqInfo));
#endif
    }
    else
    {
        pRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DEL_KEY;
        pRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
        pRSNANotifyParams->MLMEDELKEYSREQ.u4numKeyDesc = RSNA_GTK_FIRST_IDX;
        /*if (pRsnaSupInfo->pRsnaSessNode != NULL)
           {
           MEMCPY (WssRSNANotifyParams.MLMEDELKEYSREQ.aKeyDescArr[0].
           au1StaAddr, pRsnaSupInfo->pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,
           RSNA_ETH_ADDR_LEN);
           } */
        if (pRsnaSupInfo != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].
                    au1StaAddr, pRsnaSupInfo->au1SuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
        }
        pRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].u1KeyType = TRUE;
        /* Keytype = TRUE indicates pairwise key */

        if (RsnaProcessWssNotification (pRSNANotifyParams) != OSIX_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaProcAssocInd:- RsnaProcessWssNotification"
                      " returned Failure !!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
        pRsnaSupInfo->pRsnaSessNode->bPtkValid = FALSE;
        pRsnaSupInfo->pRsnaSessNode->bIsPairwiseSet = FALSE;
        MEMSET (&(pRsnaSupInfo->pRsnaSessNode->PtkSaDB), 0,
                sizeof (tRsnaPtkSa));
        pRsnaSupInfo->pRsnaSessNode->bReAutenticationRequest = TRUE;

        if (RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode) == OSIX_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:- "
                      "RsnaMainFsmStart Failed !!!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
    }
    RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcReAssocInd                                 *
*                                                                           *
* Description  : Function to Process Mlme ReAssoc Indication                *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaProcReAssocInd (tWssRSNANotifyParams * pWssRSNANotifyParams)
{

    tWssRSNANotifyParams *pRSNANotifyParams = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT1               au1StaAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
    tRsnaElements       RsnaElems;
    UINT4               u4ReasonCode = 0;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4ProfileIndex = 0;

    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));

    if ((pRSNANotifyParams = (tWssRSNANotifyParams *) RSNA_MEM_ALLOCATE_MEM_BLK
         (RSNA_WSS_INFO_MEMPOOL_ID)) == NULL)

    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessWssNotification:- "
                  "RSNA_MEM_ALLOCATE_MEM_BLK Failed \n");
        return;
    }

    u4BssIfIndex = pWssRSNANotifyParams->u4IfIndex;
    if (WssIfGetProfileIfIndex (pWssRSNANotifyParams->u4IfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcessMlmeReAssocInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                  "No Rsna entry for the given port");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }

    MEMCPY (au1StaAddr,
            pWssRSNANotifyParams->MLMEASSOCIND.au1StaMac, RSNA_ETH_ADDR_LEN);

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaAddr);

    if (pRsnaSupInfo == NULL)
    {
        pRsnaSupInfo =
            RsnaUtilCreateAndAddStaToList (pRsnaPaePortInfo, au1StaAddr);
        if (pRsnaSupInfo == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeAssocInd:-"
                      "RsnaUtilCreateAndAddStaToList returned Failure \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
    }

    pRsnaSupInfo->u4SuppState = RSNA_STA_ASSOCIATED;
    MEMCPY (pRsnaSupInfo->au1RsnaIE,
            pWssRSNANotifyParams->MLMEASSOCIND.au1RsnIe,
            pWssRSNANotifyParams->MLMEASSOCIND.u1RsnaIELen);
    pRsnaSupInfo->u1IELen = pWssRSNANotifyParams->MLMEASSOCIND.u1RsnaIELen;
    MEMSET (&RsnaElems, 0, sizeof (tRsnaElements));
    if (RsnaUtilParseIE (pRsnaSupInfo->au1RsnaIE, &RsnaElems,
                         pRsnaSupInfo->u1IELen) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = RSNA_REASON_INVALID_IE;
        pRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DISSOC_REQ;
        pRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac,
                    pRsnaSupInfo->pRsnaSessNode->pRsnaSupInfo->
                    au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->pRsnaSessNode->u4ReasonCode;
            pRsnaSupInfo->pRsnaSessNode->bDeAuthReq = TRUE;
            RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode);
        }
        else if (pRsnaSupInfo != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac,
                    pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->u4ReasonCode;
        }
        if (RsnaProcessWssNotification (pRSNANotifyParams) != OSIX_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaProcReAssocInd:- RsnaProcessWssNotification"
                      " returned Failure !!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK (RSNA_SUPP_INFO_MEMPOOL_ID,
                                          pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                          pRSNANotifyParams);
                return;
            }
        }
        else
        {
            pRsnaSupInfo->pRsnaSessNode->u4RefCount--;
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }

        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }
    u4ReasonCode = RsnaUtilValidateIE (&RsnaElems, pRsnaSupInfo,
                                       pWssRSNANotifyParams->u4IfIndex);
    if (u4ReasonCode != OSIX_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "Invalid IE Recvd In Assoc Request !!! \n");
        pRsnaSupInfo->u4ReasonCode = u4ReasonCode;
        pRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DISSOC_REQ;
        pRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac, pRsnaSupInfo->pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);    /*to be discussed */
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->pRsnaSessNode->u4ReasonCode;
            pRsnaSupInfo->pRsnaSessNode->bDeAuthReq = TRUE;
            RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode);
        }
        else if (pRsnaSupInfo != NULL)
        {
            MEMCPY (pRSNANotifyParams->MLMEDISSOCREQ.au1StaMac,
                    pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
            pRSNANotifyParams->MLMEDISSOCREQ.u2reasonCode =
                (UINT2) pRsnaSupInfo->u4ReasonCode;
        }
        if (RsnaProcessWssNotification (pRSNANotifyParams) != OSIX_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaProcReAssocInd:- RsnaProcessWssNotification"
                      " returned Failure !!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }

        if (pRsnaSupInfo->pRsnaSessNode == NULL)
        {
            TMO_SLL_Delete (&(pRsnaPaePortInfo->RsnaSupInfoList),
                            (tTMO_SLL_NODE *) pRsnaSupInfo);
            if (RSNA_MEM_RELEASE_MEM_BLK (RSNA_SUPP_INFO_MEMPOOL_ID,
                                          pRsnaSupInfo) == MEM_FAILURE)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "RsnaSessDeleteSession:- RSNA_RELEASE_SUPP_INFO "
                          "Failed !!!! \n");
                RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                          pRSNANotifyParams);
                return;
            }
        }
        else
        {
            RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }

    if (RsnaElems.u2PmkidCount == 0)
    {
        MEMSET (RsnaElems.aPmkidList[0], 0, RSNA_PMKID_LEN);
    }

#if 0
    for (u2PmkIdCount = 0; ((u2PmkIdCount < RsnaElems.u2PmkidCount) &&
                            (bPmkSaFlag == OSIX_FALSE)); u2PmkIdCount++)
    {
        pRsnaPmkSa = RsnaUtilGetPmkSaFromCache (pRsnaSupInfo->au1SuppMacAddr,
                                                RsnaElems.
                                                aPmkidList[u2PmkIdCount]);
        if (pRsnaPmkSa == NULL)
        {
            continue;
        }
        else
        {
            bPmkSaFlag = OSIX_TRUE;
            MEMCPY (pRsnaPaePortInfo->RsnaConfigDB.au1PmkIdUsed,
                    RsnaElems.aPmkidList[u2PmkIdCount], RSNA_PMKID_LEN);
        }
    }

    if ((bPmkSaFlag == OSIX_FALSE) && (u2PmkIdCount != 0))
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaApmeProcMlmeAssocReAssocInd:-"
                  "No Pmksa for the Given PMKID. !!! \n");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
        return;
    }
#endif
    if (pRsnaSupInfo->pRsnaSessNode == NULL)
    {
        if (RsnaSessHandleNewStation
            (pRsnaPaePortInfo, pRsnaSupInfo, u4BssIfIndex,
             &RsnaElems, RSNA_IE_ID) == OSIX_FAILURE)
        {
            RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:-"
                      "RsnaSessHandleNewStation Failed !!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
    }
    else
    {
        pRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DEL_KEY;
        pRSNANotifyParams->u4IfIndex =
            pRsnaSupInfo->pRsnaSessNode->pRsnaPaePortInfo->u2Port;
        pRSNANotifyParams->MLMEDELKEYSREQ.u4numKeyDesc = RSNA_GTK_FIRST_IDX;
        MEMCPY (pRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].
                au1StaAddr,
                pRsnaSupInfo->pRsnaSessNode->pRsnaSupInfo->au1SuppMacAddr,
                RSNA_ETH_ADDR_LEN);
        pRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].u1KeyType = TRUE;
        /* Keytype = TRUE indicates pairwise key */

        if (RsnaProcessWssNotification (pRSNANotifyParams) != OSIX_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaProcAssocInd:- RsnaProcessWssNotification"
                      " returned Failure !!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
        pRsnaSupInfo->pRsnaSessNode->bPtkValid = FALSE;
        pRsnaSupInfo->pRsnaSessNode->bIsPairwiseSet = FALSE;
        MEMSET (&(pRsnaSupInfo->pRsnaSessNode->PtkSaDB), 0,
                sizeof (tRsnaPtkSa));
        pRsnaSupInfo->pRsnaSessNode->bReAutenticationRequest = TRUE;

        if (RsnaMainFsmStart (pRsnaSupInfo->pRsnaSessNode) == OSIX_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaApmeProcMlmeAssocReAssocInd:- "
                      "RsnaMainFsmStart Failed !!!!! \n");
            RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                      pRSNANotifyParams);
            return;
        }
    }
    RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pRSNANotifyParams);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcDisDeAuthInd                               *
*                                                                           *
* Description  : Function to Process Mlme Disassoc or Deauth Indication     *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaProcDisDeAuthInd (tWssRSNANotifyParams * pWssRSNANotifyParams,
                      enWssRsnaNotifyType eWssRsnaNotifyType)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    tPnacAuthSessionNode *pAuthSessionNode = NULL;
    tRsnaSessionNode   *pRsnaSessionNode = NULL;
    UINT4               u4Count = 0;
    UINT4               u4ProfileIndex = 0;
    UINT1               au1StaAddr[] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };
    tMacAddr            staMacAddr;

    MEMSET (staMacAddr, 0, RSNA_ETH_ADDR_LEN);

    if (WssIfGetProfileIfIndex (pWssRSNANotifyParams->u4IfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcDisDeAuthInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcDisDeAuthInd:-"
                  "No Rsna entry for the given port \n");
        return;
    }
    if (eWssRsnaNotifyType == WSSRSNA_DISSOC_IND)
    {
        MEMCPY (au1StaAddr,
                pWssRSNANotifyParams->MLMEDISSOCIND.au1StaMac,
                RSNA_ETH_ADDR_LEN);
    }
    else if (eWssRsnaNotifyType == WSSRSNA_DEAUTH_IND)
    {
        MEMCPY (au1StaAddr,
                pWssRSNANotifyParams->MLMEDEAUTHIND.au1StaMac,
                RSNA_ETH_ADDR_LEN);
    }

    RSNA_TRC_ARG6 (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                   " Received DeAuth Indication for MAC:%x:%x:%x:%x:%x:%x\n",
                   au1StaAddr[0],
                   au1StaAddr[1],
                   au1StaAddr[2], au1StaAddr[3], au1StaAddr[4], au1StaAddr[5]);

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo, au1StaAddr);

    if (pRsnaSupInfo != NULL)
    {
        pRsnaSupInfo->u4SuppState = (UINT4) ~(RSNA_STA_ASSOCIATED);

        if (PnacAuthGetSessionTblEntryByMac (au1StaAddr, &pAuthSessionNode)
            == PNAC_FAILURE)
        {
            /* It is possible that the PNAC session node may not be moved to the
             * HASH table,because of authentication failure and it may be still
             * present in AuthInProgress Table.
             * */
            if (PnacAuthGetAuthInProgressTblEntryByMac
                (au1StaAddr, &pAuthSessionNode) == PNAC_FAILURE)
            {
                RSNA_TRC (ALL_FAILURE_TRC,
                          "RsnaProcessMlmeDisassocInd:- No Pnac"
                          "Session for the given mac \n");
                return;
            }
        }

        pRsnaSessionNode =
            (tRsnaSessionNode *) pAuthSessionNode->pRsnaSessionNode;
        if (pRsnaSessionNode == NULL)
        {
            RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessMlmeDisassocInd:- No Rsna"
                      "Session for the given mac \n");
            MEMCPY (staMacAddr, pAuthSessionNode->rmtSuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            PnacAuthDeleteSession (pAuthSessionNode);
            RsnaPnacDeleteSession (staMacAddr);

            if (pRsnaSupInfo->pRsnaSessNode != NULL)
            {
                pRsnaSessionNode = pRsnaSupInfo->pRsnaSessNode;
            }
            else
            {
                RSNA_TRC (ALL_FAILURE_TRC,
                          "RsnaProcDisDeAuthInd:- No RSNA"
                          "Session for the given mac \n");
                return;
            }

        }
        pRsnaSessionNode->bDeAuthReq = TRUE;
        if (RsnaMainFsmStart (pRsnaSessionNode) == OSIX_FAILURE)
        {
            if (pRsnaSessionNode->bDeleteSession == TRUE)
            {
                if (RsnaSessDeleteSession (pRsnaSessionNode) == OSIX_FAILURE)
                {
                    return;
                }
                return;
            }
        }
        pRsnaSessionNode->u4RefCount--;
        RsnaSessDeleteSession (pRsnaSessionNode);

    }

    /* pRsnaSupInfo could be NULL, for the scenario of Deletion Session is already 
     * done by Already Existing Association Handling request.
     * In this scenario, let us only send GTK to other station in this WLAN
     */

    u4Count = TMO_SLL_Count (&(pRsnaPaePortInfo->RsnaSupInfoList));

    if (u4Count == 0)
    {
        /* This delete flag is used to differentiate between the installation 
         * and deletion of GTK. If the flag is set, GTK key deletion will happen*/
        pRsnaPaePortInfo->RsnaGlobalGtk.bKeyDeleteFlag = TRUE;

        /* If all the stations are disconnected, send a update WLAN message to
         * delete the existing GTK */
        WssSendUpdateWlanMsgfromRsna (pRsnaPaePortInfo->u2Port,
                                      WSSRSNA_GTK_KEY);

        /* When all the stations are disconnected , set the GTK install flag to false
         * so that when the new station again comes for connection, GTK has to be 
         * installed */
        pRsnaPaePortInfo->bGtkInstallFlag = FALSE;
        pRsnaPaePortInfo->RsnaGlobalGtk.bGInit = TRUE;
        pRsnaPaePortInfo->RsnaGlobalGtk.bGTKAuthenticator = FALSE;

    }
    else
    {
/*the below check was added to stop gtk rekey initiate if strick gtk rekey is not enabled*/
        if (pRsnaPaePortInfo->RsnaConfigDB.bGroupRekeyStrict == OSIX_TRUE)
        {
            /* After the session is deleted, GTK Rekey - 
             * Reinitiate Scenario should be triggered */
            if (RsnaGtkRekeyReInitiate (pRsnaPaePortInfo) == RSNA_FAILURE)
            {
                RSNA_TRC (ALL_FAILURE_TRC,
                          "RsnaProcessMlmeDisassocInd:- No Rsna");
                return;
            }
        }
    }

    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcMicFailInd                                 *
*                                                                           *
* Description  : Function to Process Mic Failure Indication From Apme       *
*                                                                           *
* Input        : pWssRSNANotifyParams:- Pointer to Rsna msg from Apme         *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaProcMicFailInd (tWssRSNANotifyParams * pWssRSNANotifyParams)
{

    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4TimeStamp = 0;
    UINT4               u4ProfileIndex = 0;

    if (WssIfGetProfileIfIndex (pWssRSNANotifyParams->u4IfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcMicFailInd:- WssIfGetProfileIfIndex"
                  "returned Failure\n");
        return;
    }

    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortInfo);

    if (pRsnaPaePortInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcMicFailInd:-"
                  "No Rsna entry for the given port");
        return;
    }

    pRsnaSupInfo = RsnaUtilGetStaInfoFromStaAddr (pRsnaPaePortInfo,
                                                  pWssRSNANotifyParams->
                                                  MLMEMICFAILIND.au1StaMac);

    if (pRsnaSupInfo == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaProcMicFailInd:- Recvd MLME"
                  "Failure Ind for not associated STA");
        return;
    }

    pRsnaSupInfo->RsnaStatsDB.u4TkipLocalMicFailures++;

    OsixGetSysTime (&u4TimeStamp);

    if (u4TimeStamp > (pRsnaPaePortInfo->u4MichaelMicFailureTimeStamp +
                       RSNA_MIC_FAIL_CTR_MSR_INTRVL))
    {
        pRsnaPaePortInfo->u4MichaleMicFailurecount = RSNA_TKIP_INIT_COUNT;
    }
    else
    {
        pRsnaPaePortInfo->u4MichaleMicFailurecount++;
        if (pRsnaPaePortInfo->u4MichaleMicFailurecount > RSNA_TKIP_INIT_COUNT)
        {
            RsnaUtilStartTkipCtrMsrs (pRsnaPaePortInfo);
        }
    }
    pRsnaPaePortInfo->u4MichaelMicFailureTimeStamp = u4TimeStamp;
    return;
}

/*****************************************************************************/
/* Function Name      : RsnaLock                                             */
/*                                                                           */
/* Description        : This function is used to take the Rsna mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*****************************************************************************/
INT4
RsnaLock (VOID)
{
    if (OsixSemTake (gRsnaSemId) != OSIX_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaLock" "returned Failure\n");
        return (SNMP_FAILURE);

    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RsnaUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the RSNA mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*****************************************************************************/
INT4
RsnaUnLock (VOID)
{
    OsixSemGive (gRsnaSemId);
    return SNMP_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilFillWssNotifyParams                                *
*                                                                           *
* Description  : Function to fill the WSS Notify params                     *
*                                                                           *
* Input        : pRsnaPaePortInfo - Pointer to RsnaPaePortInfo              *
*                pRsnaSessionNode - Pointer to RsnaSessNode                 *
*                pRsnaSupInfo - Pointer to  RsnaSupInfo                     *
*                u4MsgType - WSSRSNA_SET_KEY/WSSRSNA_DEL_KEY                *
*                bPairwiseKey - To differntiate between pairwise and        *
*                               groupwise key                               *  
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
RsnaUtilFillWssNotifyParams (tRsnaPaePortEntry * pRsnaPaePortInfo,
                             tRsnaSessionNode * pRsnaSessionNode,
                             UINT4 u4MsgType, UINT1 u1KeyType)
{
    tWssRSNANotifyParams *pWssRSNANotifyParams = NULL;
    UINT1               u1KeyIdx = 0;

    if ((pWssRSNANotifyParams =
         (tWssRSNANotifyParams *)
         RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID)) == NULL)

    {
        RSNA_TRC (ALL_FAILURE_TRC, "RsnaProcessWssNotification:- "
                  "RSNA_MEM_ALLOCATE_MEM_BLK Failed \n");
        return (RSNA_FAILURE);
    }

    switch (u4MsgType)
    {
        case WSSRSNA_SET_KEY:
            pWssRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_SET_KEY;
            pWssRSNANotifyParams->MLMESETKEYSREQ.u4numKeyDesc =
                RSNA_GTK_FIRST_IDX;

            switch (u1KeyType)
            {
                case WSSRSNA_PTK_KEY:
                    pWssRSNANotifyParams->u4IfIndex =
                        pRsnaSessionNode->u4BssIfIndex;
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1Key, pRsnaSessionNode->PtkSaDB.au1Ptk,
                            RSNA_MAX_PTK_KEY_LEN);
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1StaAddr,
                            pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                            RSNA_ETH_ADDR_LEN);
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1CipherSuite,
                            pRsnaSessionNode->pRsnaSupInfo->au1PairwiseCipher,
                            RSNA_CIPHER_SUITE_LEN);
                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyId =
                        0;
                    if (pRsnaSessionNode->u1ElemId == WPA_IE_ID)
                    {
                        pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            u1AssocType = WPA_IE_ID;
                    }
                    else if (pRsnaSessionNode->u1ElemId == RSNA_IE_ID)
                    {
                        pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            u1AssocType = RSNA_IE_ID;
                    }
                    else
                    {
                        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                                  pWssRSNANotifyParams);
                        return (RSNA_FAILURE);
                    }

                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyType =
                        WSSRSNA_PTK_KEY;
                    if ((MEMCMP
                         (pRsnaSessionNode->pRsnaSupInfo->au1PairwiseCipher,
                          RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0)
                        ||
                        (MEMCMP
                         (pRsnaSessionNode->pRsnaSupInfo->au1PairwiseCipher,
                          WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0))
                    {
                        pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            u1KeyLength = RSNA_TKIP_KEY_LEN;
                    }
                    else if ((MEMCMP (pRsnaSessionNode->pRsnaSupInfo->
                                      au1PairwiseCipher,
                                      RSNA_CIPHER_SUITE_CCMP,
                                      RSNA_CIPHER_SUITE_LEN) == 0) ||
                             (MEMCMP (pRsnaSessionNode->pRsnaSupInfo->
                                      au1PairwiseCipher,
                                      WPA_CIPHER_SUITE_CCMP,
                                      RSNA_CIPHER_SUITE_LEN) == 0))
                    {
                        pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            u1KeyLength = RSNA_CCMP_KEY_LEN;
                    }
                    else
                    {
                        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                                  pWssRSNANotifyParams);
                        return (RSNA_FAILURE);
                    }
                    break;

                case WSSRSNA_GTK_KEY:
                    pWssRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
                    u1KeyIdx = pRsnaPaePortInfo->RsnaGlobalGtk.u1GN;
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1Key,
                            &pRsnaPaePortInfo->RsnaGlobalGtk.au1Gtk[u1KeyIdx -
                                                                    RSNA_GTK_FIRST_IDX],
                            RSNA_GTK_MAX_LEN);
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1CipherSuite,
                            pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
                            RSNA_CIPHER_SUITE_LEN);
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1StaAddr, gau1RsnaBroadCast, RSNA_ETH_ADDR_LEN);
                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyId =
                        u1KeyIdx;
                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyType =
                        WSSRSNA_GTK_KEY;
                    if ((MEMCMP
                         (pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
                          RSNA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0)
                        ||
                        (MEMCMP
                         (pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
                          WPA_CIPHER_SUITE_TKIP, RSNA_CIPHER_SUITE_LEN) == 0))
                    {
                        pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            u1KeyLength = RSNA_TKIP_KEY_LEN;
                    }
                    else if ((MEMCMP
                              (pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
                               RSNA_CIPHER_SUITE_CCMP,
                               RSNA_CIPHER_SUITE_LEN) == 0)
                             ||
                             (MEMCMP
                              (pRsnaPaePortInfo->RsnaConfigDB.au1GroupCipher,
                               WPA_CIPHER_SUITE_CCMP,
                               RSNA_CIPHER_SUITE_LEN) == 0))
                    {
                        pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            u1KeyLength = RSNA_CCMP_KEY_LEN;
                    }
                    else
                    {
                        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                                  pWssRSNANotifyParams);
                        return (RSNA_FAILURE);
                    }
                    break;
#ifdef PMF_WANTED
                case WSSRSNA_IGTK_KEY:
                    pWssRSNANotifyParams->u4IfIndex = pRsnaPaePortInfo->u2Port;
                    u1KeyIdx = pRsnaPaePortInfo->RsnaGlobalIGtk.u1IGN;
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1Key,
                            &pRsnaPaePortInfo->RsnaGlobalIGtk.au1IGtk[u1KeyIdx -
                                                                      RSNA_IGTK_FIRST_IDX],
                            RSNA_IGTK_MAX_LEN);

                    /*Update the corresponding IGTK cipher BIP */
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1CipherSuite, gau1RsnaCipherSuiteBIP,
                            RSNA_CIPHER_SUITE_LEN);
                    MEMCPY (pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                            au1StaAddr, gau1RsnaBroadCast, RSNA_ETH_ADDR_LEN);
                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyId =
                        u1KeyIdx;
                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].u1KeyType =
                        WSSRSNA_IGTK_KEY;
                    pWssRSNANotifyParams->MLMESETKEYSREQ.aKeyDesc[0].
                        u1KeyLength = RSNA_BIP_KEY_LEN;
                    break;
#endif
                default:
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "RsnaUtilFillWssNotifyParams :- Invalid KeyType"
                              " returned Failure !!!! \n");
                    break;

            }
            break;

        case WSSRSNA_DEL_KEY:
            pWssRSNANotifyParams->eWssRsnaNotifyType = WSSRSNA_DEL_KEY;
            pWssRSNANotifyParams->u4IfIndex =
                pRsnaSessionNode->pRsnaPaePortInfo->u2Port;
            pWssRSNANotifyParams->MLMEDELKEYSREQ.u4numKeyDesc =
                RSNA_GTK_FIRST_IDX;
            MEMCPY (pWssRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].
                    au1StaAddr, pRsnaSessionNode->pRsnaSupInfo->au1SuppMacAddr,
                    RSNA_ETH_ADDR_LEN);
            pWssRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].u1KeyType =
                u1KeyType;

            if (u1KeyType == WSSRSNA_GTK_KEY)
            {
                u1KeyIdx = pRsnaPaePortInfo->RsnaGlobalGtk.u1GN;
                pWssRSNANotifyParams->MLMEDELKEYSREQ.aKeyDescArr[0].u1KeyId =
                    u1KeyIdx;
            }
            break;

        default:
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaUtilFillWssNotifyParams :- Invalid MsgType"
                      " returned Failure !!!! \n");
            break;
    }
    if (RsnaProcessWssNotification (pWssRSNANotifyParams) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilFillWssNotifyParams :- RsnaProcessWssNotification"
                  " returned Failure !!!! \n");
        RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID,
                                  pWssRSNANotifyParams);
        return (RSNA_FAILURE);
    }
    RSNA_MEM_RELEASE_MEM_BLK (RSNA_WSS_INFO_MEMPOOL_ID, pWssRSNANotifyParams);
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaGtkRekeyReInitiate                                     *
*                                                                           *
* Description  : Function to reinitiate GTK rekeying                        *
*                                                                           *
* Input        : pRsnaPaePortInfo:- Pointer to Rsna port info structure     *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaGtkRekeyReInitiate (tRsnaPaePortEntry * pRsnaPaePortInfo)
{

    tRsnaSupInfo       *pRsnaSupInfo = NULL;

    RsnaUtilIncByteArray (pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter,
                          RSNA_NONCE_LEN);

    pRsnaPaePortInfo->RsnaGlobalGtk.u1RekeyFlag = TRUE;
    pRsnaPaePortInfo->RsnaGlobalGtk.bGtkRekeyEnable = TRUE;

    TMO_SLL_Scan (&(pRsnaPaePortInfo->RsnaSupInfoList), pRsnaSupInfo,
                  tRsnaSupInfo *)
    {
        if (pRsnaSupInfo->pRsnaSessNode != NULL)
        {
            do
            {
                pRsnaPaePortInfo->RsnaGlobalGtk.bIsStateChanged = FALSE;
                if (RsnaMainGlobalGrpKeyFsm (&(pRsnaPaePortInfo->
                                               RsnaGlobalGtk)) != RSNA_SUCCESS)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "RsnaGtkRekeyReInitiate:-"
                              "RsnaMainGlobalGrpKeyFsm Failed !!!! \n");
                    return RSNA_FAILURE;
                }
            }
            while (pRsnaPaePortInfo->RsnaGlobalGtk.bIsStateChanged == TRUE);
        }
    }
    RsnaTmrStopTimer (&(pRsnaPaePortInfo->RsnaGlobalGtk.RsnaUtilGtkRekeyTmr));
    RsnaTmrStartTimer (&(pRsnaPaePortInfo->RsnaGlobalGtk.
                         RsnaUtilGtkRekeyTmr), RSNA_GTK_TIMER_ID,
                       pRsnaPaePortInfo->RsnaConfigDB.u4GroupGtkRekeyTime,
                       pRsnaPaePortInfo);
    return RSNA_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaProcessKeyAvailable                                    *
*                                                                           *
* Description  : Function invoked to process PNAC Key available             *
*                                                                           *
* Input        : u2Port :- The port for which is key available              *
*                pu1SuppMacAddr - Supplicant MAC address                    *
*                pu1Pmk :- Refers to the PMK Key                            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilProcessKeyAvailable (UINT2 u2Port, UINT1 *pu1SuppMacAddr, UINT1 *pu1Pmk)
{

    tPnacAuthFsmInfo   *pAuthFsmInfo = NULL;
    tRsnaSessionNode   *pRsnaSessionNode = NULL;
    tPnacAuthSessionNode *pAuthSessionNode = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    UINT4               u4ProfileIndex = 0;
    UINT1               au1PmkId[RSNA_PMKID_LEN];
    tMacAddr            staMacAddr;

    MEMSET (staMacAddr, 0, RSNA_ETH_ADDR_LEN);
    MEMSET (au1PmkId, 0, RSNA_PMKID_LEN);

    if (WssIfGetProfileIfIndex (u2Port, &u4ProfileIndex) == OSIX_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilProcessKeyAvailable :- "
                  "WssIfGetProfileIfIndex Failed !!!! \n");
        return (RSNA_FAILURE);
    }
    RsnaGetPortEntry ((UINT2) u4ProfileIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (ALL_FAILURE_TRC,
                  "RsnaUtilProcessKeyAvailable :- Such a port does not exist!! ... \n");
        return (RSNA_FAILURE);
    }

    if (PnacSnmpLowGetAuthSessionNode (pu1SuppMacAddr, &pAuthSessionNode) ==
        PNAC_FAILURE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC, "RsnaUtilProcessKeyAvailable "
                  "PnacSnmpLowGetAuthSessionNode returned Failure \n");
        return (RSNA_FAILURE);

    }
    if (RsnaUtilAddPmkSaToCache
        (pRsnaPaePortEntry, pAuthSessionNode, pu1Pmk, au1PmkId) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC, "RsnaUtilProcessKeyAvailable "
                  "RsnaUtilAddPmkSaToCache returned Failure \n");
        return (RSNA_FAILURE);
    }

    /* previously pAuthFsmInfo->u2AuthControlPortStatus was set as
     * PNAC_PORTSTATUS_UNAUTHORIZED since RSNA 4-WAY Handshake is not
     * completed yet.But making the node as unauthorized results in unsuccessful
     * removal of node from the PNAC HASH TABLE, when deleting the PnacAuthSession
     * Node because of 4-Way Handshake Failures.So here the port status for a
     * particular station MAC is kept as Authorized.
     * */

    pAuthFsmInfo = &(pAuthSessionNode->authFsmInfo);
    pAuthSessionNode->u1IsAuthorizedOnce = PNAC_FALSE;
    pAuthFsmInfo->bKeyRun = TRUE;
    pAuthFsmInfo->bKeyAvailable = TRUE;
    pAuthFsmInfo->u2AuthPaeState = PNAC_ASM_STATE_AUTHENTICATED;
    pAuthFsmInfo->u2PortMode = PNAC_PORT_AUTHMODE_MACBASED;

    if (pAuthSessionNode->bIsRsnPreAuthSess == OSIX_TRUE)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC, "RsnaUtilProcessKeyAvailable"
                  "PMK Cached Successfully for preauth session \n");
        return (RSNA_SUCCESS);
    }

    pRsnaSessionNode =
        RsnaSessCreateAndInitSession (pRsnaPaePortEntry,
                                      pAuthSessionNode, NULL, 0, u2Port,
                                      au1PmkId, TRUE);
    if (pRsnaSessionNode == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilSessCreateAndInitSession failed in RsnaUtilProcessKeyAvailable\n");
        MEMCPY (staMacAddr, pAuthSessionNode->rmtSuppMacAddr,
                RSNA_ETH_ADDR_LEN);
        PnacAuthDeleteSession (pAuthSessionNode);
        RsnaPnacDeleteSession (staMacAddr);

        return RSNA_FAILURE;
    }
    pRsnaSessionNode->u4RefCount++;
    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilAddStaToStatsTable                                 *
*                                                                           *
* Description  : Function to add station to the  stats table                *
*                                                                           *
* Input        : u2PortNum    :- ProfileIfIndex                             *
*                u4StatsIndex :- Auxiliary Index                            *
*                pu1SuppAddr  :- Refers to the supplicant address           *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaSuppStatsInfo                                         * 
*                                                                           *
*****************************************************************************/
tRsnaSuppStatsInfo *
RsnaUtilAddStaToStatsTable (UINT2 u2PortNum, UINT4 u4StatsIndex,
                            UINT1 *pu1SuppAddr)
{

    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;

    if ((pRsnaSuppStatsInfo =
         RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_SUPP_STATS_INFO_MEMPOOL_ID)) == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  " RsnaUtilAddStaToStatsTable :- MemAllocMemBlk Failed \n");
        return NULL;
    }

    MEMSET (pRsnaSuppStatsInfo, 0, sizeof (tRsnaSuppStatsInfo));
    MEMCPY (pRsnaSuppStatsInfo->au1SuppMacAddr, pu1SuppAddr, RSNA_ETH_ADDR_LEN);
    pRsnaSuppStatsInfo->u4StatsIndex = u4StatsIndex;
    pRsnaSuppStatsInfo->u2Port = u2PortNum;

    /* Before adding the node to the RB Tree.check whether the node is already
     * present */
    if ((RsnaUtilGetStationFromStatsTable (pRsnaSuppStatsInfo->u2Port,
                                           pRsnaSuppStatsInfo->
                                           au1SuppMacAddr)) == NULL)
    {
        if (RBTreeAdd (gRsnaGlobals.RsnaSuppStatsTable, pRsnaSuppStatsInfo) ==
            RB_FAILURE)
        {
            RSNA_TRC (RSNA_INIT_SHUT_TRC,
                      "RsnaUtilAddStaToStatsTable:- RBTreeAdd"
                      "returned Failure\n");
            MemReleaseMemBlock (RSNA_SUPP_STATS_INFO_MEMPOOL_ID,
                                (UINT1 *) pRsnaSuppStatsInfo);
            return (NULL);

        }
    }
    else
    {
        RSNA_TRC (RSNA_MGMT_TRC,
                  "RsnaUtilAddStaToStatsTable:- STA entry"
                  "already exists                              in Supplicant Stats table\n");
        MemReleaseMemBlock (RSNA_SUPP_STATS_INFO_MEMPOOL_ID,
                            (UINT1 *) pRsnaSuppStatsInfo);

    }

    return (pRsnaSuppStatsInfo);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGetStationFromStatsTable                           *
*                                                                           *
* Description  : Function to get the Supplicant Stats info  from the Stats  *
*                Table                                                      *
*                                                                           *
* Input        : u2PortNum :-  ProfileIfIndex                               *
*                pu1Addr   :-  Refers to the supplicant mac addr            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaSuppStatsInfo                                         *
*                                                                           *
*****************************************************************************/

tRsnaSuppStatsInfo *
RsnaUtilGetStationFromStatsTable (UINT2 u2PortNum, UINT1 *pu1Addr)
{
    tRsnaSuppStatsInfo  InRsnaSuppStatsInfo;
    tRsnaSuppStatsInfo *pRsnaSuppStatsInfo = NULL;

    InRsnaSuppStatsInfo.u2Port = u2PortNum;
    MEMCPY (InRsnaSuppStatsInfo.au1SuppMacAddr, pu1Addr, RSNA_ETH_ADDR_LEN);

    pRsnaSuppStatsInfo = RBTreeGet (gRsnaGlobals.RsnaSuppStatsTable,
                                    ((tRBElem *) & InRsnaSuppStatsInfo));

    return pRsnaSuppStatsInfo;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilDeleteStationFromStatsTable                        *
*                                                                           *
* Description  : Function to delete the Supplicant Stats info  from the     *
*                Stats Table                                                *
*                                                                           *
* Input        : pRsnaSuppStatsInfo :- SupplicantStatsInfo for the STA to   *
*                                      be deleted.                          *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : tRsnaSuppStatsInfo                                         *
*                                                                           *
*****************************************************************************/
UINT4
RsnaUtilDeleteSuppStatsInfo (tRsnaSuppStatsInfo * pRsnaSuppStatsInfo)
{
    RBTreeRemove (gRsnaGlobals.RsnaSuppStatsTable,
                  (tRBElem *) pRsnaSuppStatsInfo);

    if ((RSNA_MEM_RELEASE_MEM_BLK (RSNA_SUPP_STATS_INFO_MEMPOOL_ID,
                                   pRsnaSuppStatsInfo)) == MEM_FAILURE)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilDeleteSuppStatsInfo:-RSNA_SUPP_STATS_INFO_MEMPOOL_ID"
                  "Failed !!!! \n");
        return (RSNA_FAILURE);
    }
    return (RSNA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : RsnaUtilGetFirstPortEntry                            */
/*                                                                           */
/* Description        : This function returns pointer to the first RSNA port */
/*                      entry                                                */
/*                                                                           */
/* Input (s)          : NONE                                                 */
/*                                                                           */
/* Output(s)          : pRsnaPortEntry - Rsna Port Entry pointer for         */
/*                      which first node should be returned                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RsnaUtilGetFirstSuppStatsEntry (tRsnaSuppStatsInfo ** ppRsnaSuppStatsInfo)
{
    tRsnaSuppStatsInfo *pTmpRsnaSuppStatsInfo = NULL;

    pTmpRsnaSuppStatsInfo = (tRsnaSuppStatsInfo *) RBTreeGetFirst
        (gRsnaGlobals.RsnaSuppStatsTable);

    *ppRsnaSuppStatsInfo = pTmpRsnaSuppStatsInfo;

    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaUtilGetNextSuppStatsEntry                              *
*                                                                           *
* Description  : Function to get the next  Supplicant Stats info  from the  *
*                Stats Table                                                *
*                                                                           *
* Input        : pRsnaSuppStatsInfo :- SupplicantStatsInfo of the STA to    *
*                                      which next stats entry has to be     *
*                                      fetched.                             *
*                ppRsnaNextSuppStatsInfo :- Pointer for NextSuppStats entry *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

VOID
RsnaUtilGetNextSuppStatsEntry (tRsnaSuppStatsInfo * pRsnaSuppStatsInfo,
                               tRsnaSuppStatsInfo ** ppRsnaNextSuppStatsInfo)
{
    tRsnaSuppStatsInfo *pRsnaTmpSuppStatsInfo = NULL;

    if (pRsnaSuppStatsInfo == NULL)
    {

        pRsnaTmpSuppStatsInfo = (tRsnaSuppStatsInfo *) RBTreeGetFirst
            (gRsnaGlobals.RsnaSuppStatsTable);
    }
    else
    {

        pRsnaTmpSuppStatsInfo = (tRsnaSuppStatsInfo *) RBTreeGetNext
            (gRsnaGlobals.RsnaSuppStatsTable, (tRBElem *) pRsnaSuppStatsInfo,
             NULL);
    }

    *ppRsnaNextSuppStatsInfo = pRsnaTmpSuppStatsInfo;
    return;
}

/****************************************************************************
* Function Name      : RsnaUtilGetSuppStatsInfo                             *
*                                                                           *
* Description        : Gets the pointer to the next SuppStats entry         *
*                                                                           *
* Input              : u2PortNum        - Port Number.                      *
*                      u4StatsIndex     - An auxiliary index                *
*                                                                           *
* Output             : ppSuppStatsInfo  - double pointer for the stats      *
*                                         entry to be fetched from the      *
*                                         table                             *
*                                                                           *
* Return Value(s)    : None                                                 *
*                                                                           *
*****************************************************************************/

VOID
RsnaUtilGetSuppStatsInfo (UINT2 u2PortNum, UINT4 u4SuppStatsIndex,
                          tRsnaSuppStatsInfo ** ppSuppStatsInfo)
{

    tRsnaSuppStatsInfo *pTmpSuppStatsInfo = NULL;
    tRsnaSuppStatsInfo  SuppStatsInfo;

    RSNA_MEMSET (&SuppStatsInfo, 0, sizeof (tRsnaSuppStatsInfo));

    SuppStatsInfo.u2Port = u2PortNum;
    SuppStatsInfo.u4StatsIndex = u4SuppStatsIndex;

    pTmpSuppStatsInfo = RBTreeGet (gRsnaGlobals.RsnaSuppStatsTable,
                                   (tRBElem *) (&SuppStatsInfo));

    *ppSuppStatsInfo = pTmpSuppStatsInfo;

    return;
}

/****************************************************************************
* Function Name      : RsnaUtilGenerateOkcPmkid                             *
*                                                                           *
* Description        : Generates the OKC PMKID                              *
*                                                                           *
* Input              : pRsnaPaePortInfo - pointer to Port Info              *
*                      pRsnaSupInfo     - pointer to Supplicant Info        *
*                      u4BssIfIndex     - Interface Index                   *
*                      ppRsnaElements   - double pointer to RSNA IE Elements* 
*                      pu1Pmk           - Base PMK to be used               *
*                                                                           *
* Output             : ppRsnaElements->aPmkidList[0] - OKC PMKID            *
*                                                                           *
*                                                                           *
* Return Value(s)    : RSNA_FAILURE/RSNA_SUCCESS                            *
*                                                                           *
*****************************************************************************/

UINT4
RsnaUtilGenerateOkcPmkid (tRsnaPaePortEntry * pRsnaPaePortInfo,
                          tRsnaSupInfo * pRsnaSupInfo, UINT4 u4BssIfIndex,
                          tRsnaElements ** ppRsnaElements, UINT1 *pu1Pmk)
{
    tCfaIfInfo          IfInfo;
    tRsnaPmkSa         *pRsnaPmkSa = NULL;

    if ((pRsnaPmkSa =
         RSNA_MEM_ALLOCATE_MEM_BLK (RSNA_PMKSA_MEMPOOL_ID)) == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilGenerateOkcPmkid:- MemAllocMemBlk Failed !!!!! \n");
        return (RSNA_FAILURE);
    }

    MEMSET (pRsnaPmkSa, 0, sizeof (tRsnaPmkSa));

    CfaGetIfInfo (u4BssIfIndex, &IfInfo);

    MEMCPY (pRsnaPmkSa->au1Akm, RSNA_AKM_UNSPEC_802_1X, RSNA_CIPHER_SUITE_LEN);
    MEMCPY (pRsnaPmkSa->au1SuppMac,
            pRsnaSupInfo->au1SuppMacAddr, RSNA_ETH_ADDR_LEN);
    MEMCPY (pRsnaPmkSa->au1SrcMac, IfInfo.au1MacAddr, RSNA_ETH_ADDR_LEN);

    pRsnaPmkSa->u4IfIndex = pRsnaPaePortInfo->u2Port;

    MEMCPY (pRsnaPmkSa->au1Pmk, pu1Pmk, RSNA_PMK_LEN);

    if (RsnaSessGenPmkId (pRsnaPmkSa) != RSNA_SUCCESS)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaUtilGenerateOkcPmkid:- "
                  "RsnaSessGenPmkId retured Failure !!!!! \n");
        return (RSNA_FAILURE);
    }

    MEMCPY ((*ppRsnaElements)->aPmkidList[0], pRsnaPmkSa->au1PmkId,
            RSNA_PMKID_LEN);

    RSNA_MEM_RELEASE_MEM_BLK (RSNA_PMKSA_MEMPOOL_ID, pRsnaPmkSa);

    return RSNA_SUCCESS;

}

/*PMF_WANTED*/

/************ End of RSNAUTIL.C ************/
