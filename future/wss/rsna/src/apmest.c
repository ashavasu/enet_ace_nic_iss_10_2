/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: apmest.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"

#if 0
tMemPoolId   gRsnaEapolMsgPoolId;
tMemPoolId   gRsnaMsgPoolId;

#if 1 /* RSNA-MERGE TODO */
tMemPoolId   gApmeRsnaMlmeMsgPoolId;
#endif

/****************************************************************************
*                                                                           *
* Function     : ApmeStubInit                                               *
*                                                                           *
* Description  : Initializes & Creates a RSNA config and send to RSNA module*
*                through PNAC.                                              *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
ApmeStubInit (VOID)
{
#if 0
    tApmeToRsnaChgConfMsg   *pApmeToRsnaChgConfMsg;
    INT1  i1Idx;
    INT4  i4RetVal;


    /* Mem Pool Creation for the blocks from RSNA to APME */

    i4RetVal = MemCreateMemPool(sizeof (tRsnaToApmeEapol), 
                                10,  /*TODO */
                                MEM_DEFAULT_MEMORY_TYPE,
                                &(gRsnaEapolMsgPoolId));
    if( i4RetVal == (INT4) MEM_FAILURE) 
    {
        printf("MemPool creation for RSNA to APME Failed \n");
        return;
    }


    i4RetVal = MemCreateMemPool(sizeof (tRsnaToApmeMsg),
                                10,  /*TODO */
                                MEM_DEFAULT_MEMORY_TYPE,
                                &(gRsnaMsgPoolId));
    if( i4RetVal == (INT4) MEM_FAILURE) 
    {
        printf("MemPool creation for RSNA to APME Failed \n");
        return;
    }

#if 1 /* RSNA-MERGE TODO */
    i4RetVal = MemCreateMemPool(sizeof (tApmeToRsnaMlmeMsg), 
                                10,  /*TODO */
                                MEM_DEFAULT_MEMORY_TYPE,
                                &(gApmeRsnaMlmeMsgPoolId));
    if( i4RetVal == (INT4) MEM_FAILURE) 
    {
        printf("MemPool creation for RSNA to APME Failed \n");
        return;
    }
#endif

    /* TODO - Create Mem Pool & Create Memory for tApmeToRsnaChgConfMsg entry */

    pApmeToRsnaChgConfMsg = ApmeRsnaAllocApmeToRsnaChgConfMsg();

    if (pApmeToRsnaChgConfMsg == NULL)
    {
        printf("***Memory allocation failed in ApmeStubInit() \n");
        return;
    }


    /* Init the allocated the memory */

    MEMSET (pApmeToRsnaChgConfMsg, 0, sizeof(tApmeToRsnaChgConfMsg));


    /* Assume we are going to support for only one interface & having hard code value */

    UINT1 au1GrpCipher[APME_RSNA_CIPHER_SUITE_LEN] = {0x00, 0x0f, 0xac, 0x02};
    UINT1 au1PairTkipCipher[APME_RSNA_CIPHER_SUITE_LEN] = {0x00, 0x0f, 0xac, 0x02};
    UINT1 au1PairCCMPCipher[APME_RSNA_CIPHER_SUITE_LEN] = {0x00, 0x0f, 0xac, 0x04};
    UINT1 au1AKM[APME_RSNA_CIPHER_SUITE_LEN] = {0x00, 0x0f, 0xac, 0x01};

    pApmeToRsnaChgConfMsg->au4IfIndex[0] = 1;
    pApmeToRsnaChgConfMsg->u4IfCount = 1;



    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4Version = RSNA_VERSION;      /* Read-Only */
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4PairwiseKeysSupported = 10;  /* Read-Only */ /*Based on max. station being supported */


    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u1GroupRekeyMethod = RSNA_GROUP_REKEY_TIMEBASED;    

    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4GroupGtkRekeyTime = RSNA_DEF_GROUP_REKEY_TIME;     
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4GroupRekeyPkts = RSNA_DEF_REKEY_PKTS;       
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.bGroupRekeyStrict = RSNA_DISABLED;           


 /* pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.au1PskPassPhrase[APME_RSNA_ALIGNED_PASS_PHRASE_LEN]; */ /* Requires only for PSK mode */
 /* pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.au1ConfigPsk[APME_RSNA_ALIGNED_PSK_LEN];     */ /* Requires Only for PSK mode */

    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4GroupUpdateCount = RSNA_DEF_UPDATE_COUNT;          
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4PairwiseUpdateCount = RSNA_DEF_UPDATE_COUNT;         



    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4GroupCipherKeyLen;          /* Read-Only */
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4PMKLifeTime = RSNA_DEF_PMK_LIFETIME;                     
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4PmkReAuthThreshold = RSNA_DEF_PMK_REAUTH_THRESHOLD;             


    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4SATimeOut = RSNA_DEF_SA_TIME_OUT;                       




    memcpy(pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.au1GroupCipher,au1GrpCipher,APME_RSNA_CIPHER_SUITE_LEN);

    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4GroupGmkRekeyTime = RSNA_DEF_GROUP_REKEY_TIME;          
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.u4NoOfCtrMsrsInvoked;                 
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.bPskStatus;                            
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.bRSNAOptionImplemented = RSNA_DISABLED;                  
    pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.apmeRsnaConfigDB.bRSNAPreauthenticationImplemented = RSNA_DISABLED; 

#if 0 /* RSNA-MERGE TODO */ 
    for (i1Idx = 0; i1Idx < APME_RSNA_PAIRWISE_CIPHER_COUNT; i1Idx++)
    {
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[i1Idx].bPairwiseCipherEnabled = RSNA_DISABLED;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[i1Idx].u1PairwiseCipherRowStatus = NOT_IN_SERVICE;
    }
#endif
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[0].bPairwiseCipherEnabled = RSNA_DISABLED;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[0].u1PairwiseCipherRowStatus = NOT_IN_SERVICE;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[0].u4PairwiseCipherSize = 256;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[0].u4Index = 1;
        memcpy(pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[0].au1PairwiseCipher,au1PairTkipCipher,APME_RSNA_CIPHER_SUITE_LEN);
 
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[1].bPairwiseCipherEnabled = RSNA_ENABLED;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[1].u1PairwiseCipherRowStatus = ACTIVE;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[1].u4PairwiseCipherSize = 128;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[1].u4Index = 1;
        memcpy(pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaPwCipherDB[1].au1PairwiseCipher,au1PairCCMPCipher,APME_RSNA_CIPHER_SUITE_LEN);

#if 0 /* RSNA-MERGE TODO */
    for (i1Idx = 0; i1Idx < APME_RSNA_AKM_SUITE_COUNT; i1Idx++)
    {
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[i1Idx].bAkmSuiteEnabled = RSNA_DISABLED;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[i1Idx].u1AuthSuiteRowStatus = NOT_IN_SERVICE;
    }
#endif
       
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[0].bAkmSuiteEnabled = RSNA_DISABLED;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[0].u1AuthSuiteRowStatus = ACTIVE;
        memcpy(pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[0].au1AuthSuite,au1AKM,APME_RSNA_CIPHER_SUITE_LEN);


        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[1].bAkmSuiteEnabled = RSNA_ENABLED;
        pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[1].u1AuthSuiteRowStatus = ACTIVE;
        memcpy(pApmeToRsnaChgConfMsg->apmeRsnaConfInfo.aApmeRsnaAkmDB[1].au1AuthSuite,au1AKM,APME_RSNA_CIPHER_SUITE_LEN);

#if 0 /* RSNA-MERGE TODO - TODO */
    ApmeUtilSetRsnaIE (pApNode->u4IfIndex, pApmeEss);
#endif


printf("\n\n\n Calling Config Change msg to pnac");
    /* Inform Pnac and RSNA Task */
    if (RsnaApmeEnqRsnaChgConfMsgToPnacTask (pApmeToRsnaChgConfMsg) != OSIX_SUCCESS)
    {
        printf("RsnaApmeEnqRsnaChgConfMsgToPnacTask() Failed ... \n");
        return;
    }


#if 0 /* RSNA-MERGE TODO */

       RsnaApmeSendMlmeIndication();

#endif    


#if 0 /* RSNA-MERGE TODO - TODO */
    ApmeUtilApKickAllSta (pApNode->au1ApBssId, OSIX_TRUE);
#endif
#endif
    return;
}


/****************************************************************************
*                                                                           *
* Function     : ApmeRsnaAllocApmeToRsnaChgConfMsg()                        *
*                                                                           *
* Description  :                                                            *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
tApmeToRsnaChgConfMsg *
ApmeRsnaAllocApmeToRsnaChgConfMsg (VOID)
{
    tApmeToRsnaChgConfMsg   *pMsg = NULL;

    pMsg = MEM_MALLOC (sizeof (tApmeToRsnaChgConfMsg), tApmeToRsnaChgConfMsg);

    if (pMsg == NULL)
    {
        printf("***ApmeRsnaAllocApmeToRsnaChgConfMsg () .... \n");
        return NULL;
    }

    return pMsg;
}


/****************************************************************************
*                                                                           *
* Function     : ApmeRsnaReleaseApmeToRsnaChgConfMsg                        *
*                                                                           *
* Description  :                                                            *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
UINT4
ApmeRsnaReleaseApmeToRsnaChgConfMsg (tApmeToRsnaChgConfMsg * pMsg)
{
    MEM_FREE (pMsg);

    return 0;
}


/****************************************************************************
*                                                                           *
* Function     : ApmeRsnaAllocRsnaToApmeEapol                               *
*                                                                           *
* Description  :                                                            *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
tRsnaToApmeEapol*
ApmeRsnaAllocRsnaToApmeEapol ()
{
#if 0

    tRsnaToApmeEapol   *pMsg = NULL;

    if ((MemAllocateMemBlock (gRsnaEapolMsgPoolId,
                              (UINT1 **) (&pMsg))) == MEM_FAILURE)
    {
        printf("ApmeRsnaAllocRsnaToApmeEapol() for MemAllocateMemBlock() failed \n");
        return (NULL);
    }
#endif
    return (NULL);

}

/****************************************************************************
*                                                                           *
* Function     : ApmeRsnaReleaseRsnaToApmeEapol                             *
*                                                                           *
* Description  :                                                            *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
UINT4
ApmeRsnaReleaseRsnaToApmeEapol (tRsnaToApmeEapol * pMsg)
{

    if ((MemReleaseMemBlock (gRsnaEapolMsgPoolId,
                             (UINT1 *) (pMsg))) == MEM_FAILURE)
    {
        printf("ApmeRsnaReleaseRsnaToApmeEapol() for MemReleaseMemBlock() failed \n");
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);

}


/****************************************************************************
 * *                                                                           *
 * * Function     : ApmeRsnaAllocRsnaToApmeMsg                                 *
 * *                                                                           *
 * * Description  : Allocate a message of type tRsnaToApme to be used when     *
 * *                RSNA sends a message to APME                               *
 * *                This is an exported API called by RSNA                     *
 * *                                                                           *
 * * Input        : None                                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : Message of type RsnaToApmeMsg                              *
 * *                                                                           *
 * *****************************************************************************/
tRsnaToApmeMsg     *
ApmeRsnaAllocRsnaToApmeMsg ()
{
#if 0
    tRsnaToApmeMsg     *pMsg = NULL;

    if ((MemAllocateMemBlock (gRsnaMsgPoolId,
                              (UINT1 **) (&pMsg))) == MEM_FAILURE)
    {
        printf("Mem Allocation failed in ApmeRsnaAllocRsnaToApmeMsg() \n");
        return (NULL);
    }
#endif
    return (NULL);
}


/****************************************************************************
 * *                                                                           *
 * * Function     : ApmeRsnaReleaseRsnaToApmeMsg                               *
 * *                                                                           *
 * * Description  : Releases the RSNA message                                  *
 * *                This is a  local API used by APME to release allocated     *
 * *                RSNA messages after processing                             *
 * *                                                                           *
 * * Input        : pRsnaMsg  - pointer to RsnaToApmeMsg                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS / OSIX_FAILURE                                *
 * *                                                                           *
 * *****************************************************************************/
UINT4
ApmeRsnaReleaseRsnaToApmeMsg (tRsnaToApmeMsg * pMsg)
{
    if ((MemReleaseMemBlock (gRsnaMsgPoolId,
                             (UINT1 *) (pMsg))) == MEM_FAILURE)
    {
        printf("Mem Allocation failed in ApmeRsnaAllocRsnaToApmeMsg() \n");
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}


#if 1 /* RSNA-MERGE TODO */

/****************************************************************************
*                                                                           *
* Function     : ApmeRsnaAllocApmeToRsnaMlmeMsg                             *
*                                                                           *
* Description  : Function to allocate memory for message from apme to rsna  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Pointer to ApmeToRsnaMsg or NULL                           *
*                                                                           *
*****************************************************************************/

tApmeToRsnaMlmeMsg *
ApmeRsnaAllocApmeToRsnaMlmeMsg ()
{
#if 0
    tApmeToRsnaMlmeMsg *pMsg = NULL;

    if ((MemAllocateMemBlock (gApmeRsnaMlmeMsgPoolId,
                              (UINT1 **) (&pMsg)) == MEM_FAILURE))
    {
        printf("Error in mem alloc for rsn mlme msg\n");
        return (NULL);
    }
#endif
    return (NULL);
}


/****************************************************************************
*                                                                           *
* Function     : ApmeRsnaReleaseApmeToRsnaMlmeMsg                             *
*                                                                           *
* Description  : Function to release message posted to apme                 *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

UINT4
ApmeRsnaReleaseApmeToRsnaMlmeMsg (tApmeToRsnaMlmeMsg * pMsg)
{
    if ((MemReleaseMemBlock (gApmeRsnaMlmeMsgPoolId,
                             (UINT1 *) (pMsg)) == MEM_FAILURE))
    {
        printf("Error in mem release for rsn mlme msg\n");
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}


VOID
ApmeMlmeBuildMlmeAssocInd (tApmeToRsnaMlmeMsg * pMsg, UINT4 u4ApIndex,
                           UINT1 * pu1StaMac, UINT4 u1RsnIeLen, UINT1 * pu1RsnIE)
{

    tMlmeInd           *pMlme = NULL;
    tMlmeAssocInd      *pMlmeAssoc = NULL;

    /* get the actual MLME request structure */
    pMlme = &(pMsg->mlmeIndication);
    pMlme->u4IfIndex = u4ApIndex;   /* Need to obtain from cfa */
    /* fill the fields of the MLME structure */
    pMlme->eMlmeInd = MLME_ASSOC_IND;

    pMlmeAssoc = &(pMlme->unMlmeIndication.mlmeAssocInd);
    MEMSET (pMlmeAssoc->au1StaMac, 0, 8);
    MEMCPY (pMlmeAssoc->au1StaMac, pu1StaMac, 6);
    MEMSET (pMlmeAssoc->au1RsnIe, 0, 256);
    MEMCPY (pMlmeAssoc->au1RsnIe, pu1RsnIE, RSNA_IE_LEN);
    pMlmeAssoc->u1RsnaIELen = u1RsnIeLen;
    return;
}

#if 0 /* RSNA-MERGE TODO */

VOID RsnaApmeSendMlmeIndication()
{

    tApmeToRsnaMlmeMsg *pApmeToRsnaMlmeMsg = NULL;
    UINT4 u4IfIndex = 1;
    UINT1 au1StaMac[6] = {0x01,0x02,0x03,0x04,0x05,0x06};
    UINT4 u4RsnIeLength = 0;
    UINT1 au1RsnIE[256];
    tRSNInfo RSNInfo;
    UINT4 u4PairwiseSuit = 0x000FAC04;

    pApmeToRsnaMlmeMsg = ApmeRsnaAllocApmeToRsnaMlmeMsg();
    
    if (pApmeToRsnaMlmeMsg == NULL)
    {
         printf( "ApmeRsnaAllocApmeToRsnaMlmeMsg Failed\n");
         return (OSIX_FAILURE);
    }

    RSNInfo.u1MacFrameElemId = 48;
    RSNInfo.u1MacFrameElemLen = 22;
    RSNInfo.u2MacFrameRSN_Version = 256;
    RSNInfo.u4MacFrameRSN_GrpCipherSuit = 0x000FAC04;
    RSNInfo.u2MacFrameRSN_PairCiphSuitCnt = 256;
    MEMCPY(&RSNInfo.aMacFrameRSN_PairCiphSuitList[0],&u4PairwiseSuit,sizeof(u4PairwiseSuit));
    RSNInfo.u2MacFrameRSN_AKMSuitCnt = 256;
    RSNInfo.au4MacFrameRSN_AKMSuitList[0] = 0x000FAC01;
    RSNInfo.u2MacFrameRSN_Capability = 1;
//    RSNInfo.u2MacFrameRSN_PMKIdCnt = 0;
   
   
    MEMCPY(au1RsnIE,&RSNInfo,sizeof(RSNInfo));
    /* make a MLME indication */

    ApmeMlmeBuildMlmeAssocInd (pApmeToRsnaMlmeMsg, u4IfIndex, au1StaMac , u4RsnIeLength, au1RsnIE);

    RsnaApmeEnqRsnaMlmeMsgToPnacTask (pApmeToRsnaMlmeMsg);

}

#endif
#endif
#endif

