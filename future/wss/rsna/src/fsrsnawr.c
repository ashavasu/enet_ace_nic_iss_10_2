/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrsnawr.c,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsrsnawr.h"
# include  "fsrsnadb.h"
#include "rsnainc.h"
#include "rsnaprot.h"

VOID
RegisterFSRSNA ()
{
    SNMPRegisterMib (&fsrsnaOID, &fsrsnaEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsrsnaOID, (const UINT1 *) "fsrsna");
}

VOID
UnRegisterFSRSNA ()
{
    SNMPUnRegisterMib (&fsrsnaOID, &fsrsnaEntry);
    SNMPDelSysorEntry (&fsrsnaOID, (const UINT1 *) "fsrsna");
}

INT4
FsRSNATraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRSNATraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsRSNAClearAllCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRSNAClearAllCounters (&(pMultiData->i4_SLongValue)));
}

INT4
FsRSNATraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRSNATraceOption (pMultiData->i4_SLongValue));
}

INT4
FsRSNAClearAllCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRSNAClearAllCounters (pMultiData->i4_SLongValue));
}

INT4
FsRSNATraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRSNATraceOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRSNAClearAllCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRSNAClearAllCounters
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRSNATraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRSNATraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRSNAClearAllCountersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRSNAClearAllCounters
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRSNAClearCountersTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRSNAClearCountersTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRSNAClearCountersTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRSNAClearCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAClearCountersTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAClearCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNAClearCounterSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAClearCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsRSNAClearCounterTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAClearCounter (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRSNAClearCountersTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRSNAClearCountersTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRSNAExtTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRSNAExtTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRSNAExtTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRSNAOKCEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAOKCEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNA4WayHandshakeCompletedStatsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNA4WayHandshakeCompletedStats
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAGroupHandshakeCompletedStatsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAGroupHandshakeCompletedStats
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

#ifdef PMF_WANTED
INT4
FsRSNAAssociationComeBackTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAExtTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAAssociationComeBackTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}
#endif

INT4
FsRSNAOKCEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAOKCEnabled (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

#ifdef PMF_WANTED
INT4
FsRSNAAssociationComeBackTimeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsRSNAAssociationComeBackTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}
#endif

INT4
FsRSNAOKCEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAOKCEnabled (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigAuthenticationSuiteActivatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigAuthenticationSuiteActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

#ifdef PMF_WANTED
INT4
FsRSNAAssociationComeBackTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAAssociationComeBackTime (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}
#endif
INT4
GetNextIndexFsRSNAConfigAuthenticationSuitesTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRSNAConfigAuthenticationSuitesTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRSNAConfigAuthenticationSuitesTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRSNAExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRSNAExtTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRSNAConfigAuthenticationSuiteActivatedSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigAuthenticationSuiteActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigAuthenticationSuiteActivatedTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigAuthenticationSuiteActivated (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsRSNAConfigAuthenticationSuitesTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpvarbinds);
    return SNMP_SUCCESS;
}

INT4
FsRSNAConfigAuthenticationSuiteImplementedGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhGetFsRSNAConfigAuthenticationSuiteImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

#ifdef WPA_WANTED
INT4
GetNextIndexFsRSNAConfigTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRSNAConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRSNAConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRSNAConfigVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigGroupCipherGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupCipher (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigGroupRekeyMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupRekeyMethod
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNAConfigGroupRekeyTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupRekeyTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigGroupRekeyPacketsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupRekeyPackets
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigGroupRekeyStrictGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupRekeyStrict
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNAConfigPSKValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPSKValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigPSKPassPhraseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPSKPassPhrase
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigGroupUpdateCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupUpdateCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigPairwiseUpdateCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPairwiseUpdateCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigGroupCipherSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigGroupCipherSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigPMKLifetimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPMKLifetime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigPMKReauthThresholdGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPMKReauthThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigNumberOfPTKSAReplayCountersImplementedGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigNumberOfPTKSAReplayCountersImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigSATimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigSATimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAAuthenticationSuiteSelectedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAAuthenticationSuiteSelected
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAPairwiseCipherSelectedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAPairwiseCipherSelected
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAGroupCipherSelectedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAGroupCipherSelected
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAPMKIDUsedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAPMKIDUsed (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsRSNAAuthenticationSuiteRequestedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAAuthenticationSuiteRequested
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAPairwiseCipherRequestedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAPairwiseCipherRequested
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAGroupCipherRequestedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAGroupCipherRequested
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNATKIPCounterMeasuresInvokedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNATKIPCounterMeasuresInvoked
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNA4WayHandshakeFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNA4WayHandshakeFailures
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigNumberOfGTKSAReplayCountersImplementedGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigNumberOfGTKSAReplayCountersImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigActivatedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigActivated (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNAConfigMixModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigMixMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNAConfigGroupCipherSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupCipher (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigGroupRekeyMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupRekeyMethod
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigGroupRekeyTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupRekeyTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigGroupRekeyPacketsSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupRekeyPackets
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigGroupRekeyStrictSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupRekeyStrict
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigPSKValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigPSKValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigPSKPassPhraseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigPSKPassPhrase
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigGroupUpdateCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupUpdateCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigPairwiseUpdateCountSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigPairwiseUpdateCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigGroupCipherSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigGroupCipherSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigPMKLifetimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigPMKLifetime (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigPMKReauthThresholdSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigPMKReauthThreshold
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigSATimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigSATimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigActivatedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigActivated (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigMixModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigMixMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigGroupCipherTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupCipher (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigGroupRekeyMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupRekeyMethod (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigGroupRekeyTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupRekeyTime (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigGroupRekeyPacketsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupRekeyPackets (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigGroupRekeyStrictTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupRekeyStrict (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigPSKValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigPSKValue (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigPSKPassPhraseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigPSKPassPhrase (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigGroupUpdateCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupUpdateCount (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigPairwiseUpdateCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigPairwiseUpdateCount (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsRSNAConfigGroupCipherSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigGroupCipherSize (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigPMKLifetimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigPMKLifetime (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigPMKReauthThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigPMKReauthThreshold (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsRSNAConfigSATimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigSATimeout (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsRSNAConfigActivatedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigActivated (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigMixModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigMixMode (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRSNAConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRSNAConfigPairwiseCiphersTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRSNAConfigPairwiseCiphersTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRSNAConfigPairwiseCiphersTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRSNAConfigPairwiseCipherImplementedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigPairwiseCiphersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPairwiseCipherImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAConfigPairwiseCipherActivatedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigPairwiseCiphersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPairwiseCipherActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRSNAConfigPairwiseCipherSizeImplementedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAConfigPairwiseCiphersTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAConfigPairwiseCipherSizeImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAConfigPairwiseCipherActivatedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsRSNAConfigPairwiseCipherActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRSNAConfigPairwiseCipherActivatedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsRSNAConfigPairwiseCipherActivated (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsRSNAConfigPairwiseCiphersTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRSNAConfigPairwiseCiphersTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRSNAStatsTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRSNAStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRSNAStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRSNAStatsSTAAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsRSNAStatsSTAAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         (tMacAddr *) pMultiData->
                                         pOctetStrValue->pu1_OctetList));

}

INT4
FsRSNAStatsVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAStatsVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAStatsSelectedPairwiseCipherGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAStatsSelectedPairwiseCipher
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsRSNAStatsTKIPICVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAStatsTKIPICVErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAStatsTKIPLocalMICFailuresGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAStatsTKIPLocalMICFailures
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAStatsTKIPRemoteMICFailuresGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAStatsTKIPRemoteMICFailures
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRSNAStatsTKIPReplaysGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRSNAStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRSNAStatsTKIPReplays (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsWPAConfigAuthenticationSuitesTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWPAConfigAuthenticationSuitesTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWPAConfigAuthenticationSuitesTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsWPAConfigAuthenticationSuiteImplementedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWPAConfigAuthenticationSuitesTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWPAConfigAuthenticationSuiteImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsWPAConfigAuthenticationSuiteActivatedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWPAConfigAuthenticationSuitesTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWPAConfigAuthenticationSuiteActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWPAConfigAuthenticationSuiteActivatedSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsWPAConfigAuthenticationSuiteActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsWPAConfigAuthenticationSuiteActivatedTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsWPAConfigAuthenticationSuiteActivated (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
FsWPAConfigAuthenticationSuitesTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWPAConfigAuthenticationSuitesTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#endif
