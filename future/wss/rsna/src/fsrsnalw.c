/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrsnalw.c,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
#include  "rsnainc.h"
# include  "wsscfginc.h"
#include "wsswlanwlcproto.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNATraceOption
 Input       :  The Indices

                The Object 
                retValFsRSNATraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNATraceOption (INT4 *pi4RetValFsRSNATraceOption)
{
    *pi4RetValFsRSNATraceOption = (INT4) gRsnaGlobals.u4RsnaTrcOpt;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAClearAllCounters
 Input       :  The Indices

                The Object 
                retValFsRSNAClearAllCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAClearAllCounters (INT4 *pi4RetValFsRSNAClearAllCounters)
{

    *pi4RetValFsRSNAClearAllCounters = RSNA_CLEAR_FLAG_SET;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRSNATraceOption
 Input       :  The Indices

                The Object 
                setValFsRSNATraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNATraceOption (INT4 i4SetValFsRSNATraceOption)
{
    gRsnaGlobals.u4RsnaTrcOpt = (UINT4) i4SetValFsRSNATraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAClearAllCounters
 Input       :  The Indices

                The Object 
                setValFsRSNAClearAllCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAClearAllCounters (INT4 i4SetValFsRSNAClearAllCounters)
{

    UINT4               u4ProfileId = 0;
    UINT4               u4CurrentWlanIndex = 0;
    UINT4               u4NextWlanIndex = 0;
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;

    if (i4SetValFsRSNAClearAllCounters == RSNA_CLEAR_FLAG_SET)
    {
        /* Get the first index of the config table */
        if (RsnaGetFirstIndexDot11RSNAConfigTable ((INT4 *)
                                                   (&u4ProfileId)) !=
            SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4NextWlanIndex = u4ProfileId;

        do
        {
            /*Get the data structure for the particular Profile Index  */
            RsnaGetPortEntry ((UINT2) u4NextWlanIndex, &pRsnaPaePortEntry);

            if (pRsnaPaePortEntry == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No Rsna Info for the given port!! ... \n");
                return (SNMP_FAILURE);
            }

            /* Resets the Counters in Config table */
            pRsnaPaePortEntry->RsnaConfigDB.u4NoOf4WayHandshakeFailures = 0;
            pRsnaPaePortEntry->RsnaConfigDB.u4NoOfPtkSaReplayCtr = 0;
            pRsnaPaePortEntry->RsnaConfigDB.u4NoOfGtkReplayCtrs = 0;
            pRsnaPaePortEntry->RsnaConfigDB.u4NoOfCtrMsrsInvoked = 0;
            pRsnaPaePortEntry->u4FourWayHandshakeCounter = 0;
            pRsnaPaePortEntry->u4GroupHandshakeCounter = 0;
            /*reset the counters of Stats table */
            TMO_SLL_Scan (&pRsnaPaePortEntry->RsnaSupInfoList,
                          pRsnaSupInfo, tRsnaSupInfo *)
            {
                pRsnaSupInfo->RsnaStatsDB.u4TkipICVErrors = 0;
                pRsnaSupInfo->RsnaStatsDB.u4TkipLocalMicFailures = 0;
                pRsnaSupInfo->RsnaStatsDB.u4TkipRemoteMicFailures = 0;
                pRsnaSupInfo->RsnaStatsDB.u4CCMReplays = 0;
                pRsnaSupInfo->RsnaStatsDB.u4CCMDecryptErrors = 0;
                pRsnaSupInfo->RsnaStatsDB.u4TkipReplays = 0;
            }

            u4CurrentWlanIndex = u4NextWlanIndex;

        }
        while (RsnaGetNextIndexDot11RSNAConfigTable ((INT4) u4CurrentWlanIndex,
                                                     (INT4
                                                      *) (&u4NextWlanIndex)) ==
               SNMP_SUCCESS);

    }                            /* end of IF */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRSNATraceOption
 Input       :  The Indices

                The Object 
                testValFsRSNATraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNATraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsRSNATraceOption)
{
    if (i4TestValFsRSNATraceOption < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAClearAllCounters
 Input       :  The Indices

                The Object 
                testValFsRSNAClearAllCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAClearAllCounters (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsRSNAClearAllCounters)
{
    UNUSED_PARAM (pu4ErrorCode);

    if ((i4TestValFsRSNAClearAllCounters == RSNA_CLEAR_FLAG_SET) ||
        (i4TestValFsRSNAClearAllCounters == RSNA_CLEAR_FLAG_NOSET))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRSNATraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNATraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList
                           * pSnmpIndexList, tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRSNAClearAllCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNAClearAllCounters (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRSNAClearCountersTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRSNAClearCountersTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRSNAClearCountersTable (INT4 i4IfIndex)
{

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRSNAClearCountersTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRSNAClearCountersTable (INT4 *pi4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4IfIndex = pRsnaPaePortEntry->u2Port;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRSNAClearCountersTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRSNAClearCountersTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;

    if (i4IfIndex == 0)
    {
        RsnaGetNextPortEntry (NULL, &pNextRsnaPaePortEntry);

        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }
    }
    else
    {
        /* fetch the data structure of the current index */
        RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "No Rsna Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }

        /* pass the data structure of the current index and get
         * the data structure of the next index*/
        RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);

        /* if next index data structure is not NULL assign the
         * value for next index*/
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }

    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAClearCounter
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAClearCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAClearCounter (INT4 i4IfIndex, INT4 *pi4RetValFsRSNAClearCounter)
{
    UNUSED_PARAM (i4IfIndex);
    *pi4RetValFsRSNAClearCounter = RSNA_CLEAR_FLAG_SET;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRSNAClearCounter
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAClearCounter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAClearCounter (INT4 i4IfIndex, INT4 i4SetValFsRSNAClearCounter)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaSupInfo       *pRsnaSupInfo = NULL;

    if (i4SetValFsRSNAClearCounter == RSNA_CLEAR_FLAG_SET)
    {
        RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "No Rsna Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOf4WayHandshakeFailures = 0;
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOfPtkSaReplayCtr = 0;
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOfGtkReplayCtrs = 0;
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOfCtrMsrsInvoked = 0;
        pRsnaPaePortEntry->u4FourWayHandshakeCounter = 0;
        pRsnaPaePortEntry->u4GroupHandshakeCounter = 0;

        TMO_SLL_Scan (&(pRsnaPaePortEntry->RsnaSupInfoList),
                      pRsnaSupInfo, tRsnaSupInfo *)
        {

            pRsnaSupInfo->RsnaStatsDB.u4TkipICVErrors = 0;
            pRsnaSupInfo->RsnaStatsDB.u4TkipLocalMicFailures = 0;
            pRsnaSupInfo->RsnaStatsDB.u4TkipRemoteMicFailures = 0;
            pRsnaSupInfo->RsnaStatsDB.u4CCMReplays = 0;
            pRsnaSupInfo->RsnaStatsDB.u4CCMDecryptErrors = 0;
            pRsnaSupInfo->RsnaStatsDB.u4TkipReplays = 0;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRSNAClearCounter
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAClearCounter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAClearCounter (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             INT4 i4TestValFsRSNAClearCounter)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsRSNAClearCounter == RSNA_CLEAR_FLAG_SET) ||
        (i4TestValFsRSNAClearCounter == RSNA_CLEAR_FLAG_NOSET))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_CLEAR_FIELD;
        CLI_SET_ERR (CLI_RSNA_INVALID_CLEAR_FIELD);
        return SNMP_FAILURE;
    }

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRSNAClearCountersTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNAClearCountersTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRSNAExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRSNAExtTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRSNAExtTable (INT4 i4IfIndex)
{
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRSNAExtTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRSNAExtTable (INT4 *pi4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4IfIndex = pRsnaPaePortEntry->u2Port;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRSNAExtTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRSNAExtTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;

    if (i4IfIndex == 0)
    {
        /* Fetch the data structure of the first index */
        RsnaGetNextPortEntry (NULL, &pNextRsnaPaePortEntry);

        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }
    }
    else
    {
        /* fetch the data structure of the current index */
        RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "No Rsna Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }

        /* pass the data structure of the current index and get
         *  *            * the data structure of the next index*/
        RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);

        /* if next index data structure is not NULL assign the
         *  *            * value for next index*/
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
        }

    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAOKCEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAOKCEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAOKCEnabled (INT4 i4IfIndex, INT4 *pi4RetValFsRSNAOKCEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValFsRSNAOKCEnabled =
        (INT4) pRsnaPaePortEntry->RsnaConfigDB.u1RsnaOkcStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNA4WayHandshakeCompletedStats
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNA4WayHandshakeCompletedStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNA4WayHandshakeCompletedStats (INT4 i4IfIndex,
                                         UINT4
                                         *pu4RetValFsRSNA4WayHandshakeCompletedStats)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNA4WayHandshakeCompletedStats = pRsnaPaePortEntry->
        u4FourWayHandshakeCounter;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAGroupHandshakeCompletedStats
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAGroupHandshakeCompletedStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAGroupHandshakeCompletedStats (INT4 i4IfIndex,
                                          UINT4
                                          *pu4RetValFsRSNAGroupHandshakeCompletedStats)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAGroupHandshakeCompletedStats = pRsnaPaePortEntry->
        u4GroupHandshakeCounter;

    return SNMP_SUCCESS;

}

#ifdef PMF_WANTED
/****************************************************************************
 Function    :  nmhGetFsRSNAAssociationComeBackTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAAssociationComeBackTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAAssociationComeBackTime (INT4 i4IfIndex,
                                     UINT4
                                     *pu4RetValFsRSNAAssociationComeBackTime)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAAssociationComeBackTime =
        pRsnaPaePortEntry->u4AssocComeBackTime;

    return SNMP_SUCCESS;

}
#endif
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRSNAOKCEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAOKCEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAOKCEnabled (INT4 i4IfIndex, INT4 i4SetValFsRSNAOKCEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->RsnaConfigDB.u1RsnaOkcStatus =
        (UINT1) i4SetValFsRSNAOKCEnabled;

    return SNMP_SUCCESS;

}

#ifdef PMF_WANTED
/****************************************************************************
 Function    :  nmhSetFsRSNAAssociationComeBackTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAAssociationComeBackTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAAssociationComeBackTime (INT4 i4IfIndex,
                                     UINT4
                                     u4SetValFsRSNAAssociationComeBackTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->u4AssocComeBackTime =
        u4SetValFsRSNAAssociationComeBackTime;

    return SNMP_SUCCESS;

}
#endif
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRSNAOKCEnabled
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAOKCEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAOKCEnabled (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4TestValFsRSNAOKCEnabled)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (i4TestValFsRSNAOKCEnabled != RSNA_ENABLED)
    {
        if (i4TestValFsRSNAOKCEnabled != RSNA_DISABLED)
        {
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRSNAExtTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNAExtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef PMF_WANTED
/****************************************************************************
 Function    :  nmhTestv2FsRSNAAssociationComeBackTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAAssociationComeBackTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAAssociationComeBackTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        UINT4
                                        u4TestValFsRSNAAssociationComeBackTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if (u4TestValFsRSNAAssociationComeBackTime < RSNA_MIN_ASSOC_COMEBACK_TIME ||
        u4TestValFsRSNAAssociationComeBackTime > RSNA_MAX_ASSOC_COMEBACK_TIME)
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_ASSOC_COMEBACK_TIME;
        CLI_SET_ERR (CLI_RSNA_INVALID_ASSOC_COMEBACK_TIME);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
#endif
/* LOW LEVEL Routines for Table : FsRSNAConfigAuthenticationSuitesTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                               UINT4
                                                               u4FsRSNAConfigAuthenticationSuiteIndex)
{
    if (RsnaValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable
        (i4IfIndex, u4FsRSNAConfigAuthenticationSuiteIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRSNAConfigAuthenticationSuitesTable (INT4 *pi4IfIndex,
                                                       UINT4
                                                       *pu4FsRSNAConfigAuthenticationSuiteIndex)
{
    if (RsnaGetFirstIndexDot11RSNAConfigAuthenticationSuitesTable
        (pi4IfIndex, pu4FsRSNAConfigAuthenticationSuiteIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsRSNAConfigAuthenticationSuiteIndex
                nextFsRSNAConfigAuthenticationSuiteIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRSNAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                      INT4 *pi4NextIfIndex,
                                                      UINT4
                                                      u4FsRSNAConfigAuthenticationSuiteIndex,
                                                      UINT4
                                                      *pu4NextFsRSNAConfigAuthenticationSuiteIndex)
{
    if (RsnaGetNextIndexDot11RSNAConfigAuthenticationSuitesTable
        (i4IfIndex, pi4NextIfIndex, u4FsRSNAConfigAuthenticationSuiteIndex,
         pu4NextFsRSNAConfigAuthenticationSuiteIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigAuthenticationSuiteImplemented
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex

                The Object
                retValFsRSNAConfigAuthenticationSuiteImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigAuthenticationSuiteImplemented (INT4 i4IfIndex,
                                                  UINT4
                                                  u4FsRSNAConfigAuthenticationSuiteIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pRetValFsRSNAConfigAuthenticationSuiteImplemented)
{
    if (RsnaGetDot11RSNAConfigAuthenticationSuite (i4IfIndex,
                                                   u4FsRSNAConfigAuthenticationSuiteIndex,
                                                   pRetValFsRSNAConfigAuthenticationSuiteImplemented)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigAuthenticationSuiteActivated
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex

                The Object
                retValFsRSNAConfigAuthenticationSuiteActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigAuthenticationSuiteActivated (INT4 i4IfIndex,
                                                UINT4
                                                u4FsRSNAConfigAuthenticationSuiteIndex,
                                                INT4
                                                *pi4RetValFsRSNAConfigAuthenticationSuiteActivated)
{
    if (RsnaGetDot11RSNAConfigAuthenticationSuiteEnabled (i4IfIndex,
                                                          u4FsRSNAConfigAuthenticationSuiteIndex,
                                                          pi4RetValFsRSNAConfigAuthenticationSuiteActivated)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigAuthenticationSuiteActivated
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex

                The Object
                setValFsRSNAConfigAuthenticationSuiteActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigAuthenticationSuiteActivated (INT4 i4IfIndex,
                                                UINT4
                                                u4FsRSNAConfigAuthenticationSuiteIndex,
                                                INT4
                                                i4SetValFsRSNAConfigAuthenticationSuiteActivated)
{
    if (RsnaSetDot11RSNAConfigAuthenticationSuiteEnabled ((UINT4) i4IfIndex,
                                                          u4FsRSNAConfigAuthenticationSuiteIndex,
                                                          i4SetValFsRSNAConfigAuthenticationSuiteActivated)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigAuthenticationSuiteActivated
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex

                The Object
                testValFsRSNAConfigAuthenticationSuiteActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigAuthenticationSuiteActivated (UINT4 *pu4ErrorCode,
                                                   INT4 i4IfIndex,
                                                   UINT4
                                                   u4FsRSNAConfigAuthenticationSuiteIndex,
                                                   INT4
                                                   i4TestValFsRSNAConfigAuthenticationSuiteActivated)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAConfigAuthenticationSuiteIndex);
    UNUSED_PARAM (i4TestValFsRSNAConfigAuthenticationSuiteActivated);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRSNAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                FsRSNAConfigAuthenticationSuiteIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNAConfigAuthenticationSuitesTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef WPA_WANTED
/****************************************************************************
 Function    :  nmhGetFsRSNAConfigMixMode
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigMixMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigMixMode (INT4 i4IfIndex, INT4 *pi4RetValFsRSNAConfigMixMode)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValFsRSNAConfigMixMode = pRsnaPaePortEntry->WpaConfigDB.bWpaMixMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigMixMode
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigMixMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigMixMode (INT4 i4IfIndex, INT4 i4SetValFsRSNAConfigMixMode)
{
    tSNMP_OCTET_STRING_TYPE RSNAConfigGroupCipher = { NULL, 0 };
    UINT1               au1Buf[RSNA_MAX_CIPHER_TYPE_LENGTH];
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    MEMSET (au1Buf, 0, RSNA_MAX_CIPHER_TYPE_LENGTH);
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);
    RSNAConfigGroupCipher.pu1_OctetList = au1Buf;

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.bWpaMixMode =
        (BOOL1) i4SetValFsRSNAConfigMixMode;
    /*If Mix Mode is disabled then we are disabling the WPA2 and WPA1 Protection */
    if (pRsnaPaePortEntry->WpaConfigDB.bWpaMixMode == MIX_MODE_DISABLED)
    {
        if (nmhSetFsRSNAConfigActivated (i4IfIndex, WPA_DISABLED) !=
            SNMP_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Mix-Mode Disable Failed because of WPA1 Disable Failed!! ... \n");
            return (SNMP_FAILURE);
        }
        if (nmhSetDot11RSNAActivated (i4IfIndex, RSNA_DISABLED) != SNMP_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "Mix-Mode Disable Failed because of WPA2 Disable Failed!! ... \n");
            return (SNMP_FAILURE);
        }
    }
    if (pRsnaPaePortEntry->WpaConfigDB.bWpaMixMode == MIX_MODE_ENABLED)
    {
        /*If WPA2 GroupCipher is set as AES then it is changed to TKIP */
        if (RsnaGetDot11RSNAConfigGroupCipher
            (i4IfIndex, &RSNAConfigGroupCipher) == SNMP_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC,
                      "WPA2 GroupCipher is set as TKIP!! ... \n");
            MEMCPY (RSNAConfigGroupCipher.pu1_OctetList,
                    gau1RsnaCipherSuiteTKIP, RSNA_MAX_CIPHER_TYPE_LENGTH);
            RsnaSetDot11RSNAConfigGroupCipher (i4IfIndex,
                                               &RSNAConfigGroupCipher);
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigMixMode
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigMixMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigMixMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4TestValFsRSNAConfigMixMode)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsRSNAConfigMixMode != MIX_MODE_ENABLED)
    {
        if (i4TestValFsRSNAConfigMixMode != MIX_MODE_DISABLED)
        {
            CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
            return SNMP_FAILURE;
        }

    }
    return SNMP_SUCCESS;
}

#endif
#ifdef WPA_WANTED
/* LOW LEVEL Routines for Table : FsRSNAConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRSNAConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRSNAConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRSNAConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRSNAConfigTable (INT4 *pi4IfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4IfIndex = (INT4) pRsnaPaePortEntry->u2Port;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRSNAConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRSNAConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;

    if (i4IfIndex == 0)
    {
        /* Fetch the data structure of the first index */
        RsnaGetNextPortEntry (NULL, &pNextRsnaPaePortEntry);

        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = (INT4) pNextRsnaPaePortEntry->u2Port;
        }
    }
    else
    {
        /* fetch the data structure of the current index */
        RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

        if (pRsnaPaePortEntry == NULL)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "No Rsna Info for the given port!! ... \n");
            return (SNMP_FAILURE);
        }

        /* pass the data structure of the current index and get
         *          * the data structure of the next index*/
        RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);

        /* if next index data structure is not NULL assign the
         *          * value for next index*/
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = (INT4) pNextRsnaPaePortEntry->u2Port;
        }

    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigVersion
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigVersion (INT4 i4IfIndex, UINT4 *pu4RetValFsRSNAConfigVersion)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigVersion = pRsnaPaePortEntry->WpaConfigDB.u4Version;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPairwiseKeysImplemented
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigPairwiseKeysImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPairwiseKeysImplemented (INT4 i4IfIndex,
                                           UINT4
                                           *pu4RetValFsRSNAConfigPairwiseKeysImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigPairwiseKeysImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupCipher (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsRSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAConfigGroupCipher->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1GroupCipher,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValFsRSNAConfigGroupCipher->i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupRekeyMethod
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupRekeyMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupRekeyMethod (INT4 i4IfIndex,
                                    INT4 *pi4RetValFsRSNAConfigGroupRekeyMethod)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValFsRSNAConfigGroupRekeyMethod =
        (INT4) pRsnaPaePortEntry->WpaConfigDB.u1GroupRekeyMethod;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupRekeyTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupRekeyTime (INT4 i4IfIndex,
                                  UINT4 *pu4RetValFsRSNAConfigGroupRekeyTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigGroupRekeyTime =
        (UINT4) pRsnaPaePortEntry->WpaConfigDB.u4GroupGtkRekeyTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupRekeyPackets
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupRekeyPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupRekeyPackets (INT4 i4IfIndex,
                                     UINT4
                                     *pu4RetValFsRSNAConfigGroupRekeyPackets)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigGroupRekeyPackets);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupRekeyStrict
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupRekeyStrict
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupRekeyStrict (INT4 i4IfIndex,
                                    INT4 *pi4RetValFsRSNAConfigGroupRekeyStrict)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValFsRSNAConfigGroupRekeyStrict);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPSKValue
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigPSKValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPSKValue (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsRSNAConfigPSKValue)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
#ifdef ISS_WANTED
    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        MEMCPY (pRetValFsRSNAConfigPSKValue->pu1_OctetList,
                pRsnaPaePortEntry->WpaConfigDB.au1ConfigPsk, RSNA_PSK_LEN);

        pRetValFsRSNAConfigPSKValue->i4_Length = RSNA_PSK_LEN;
        return SNMP_SUCCESS;
    }
#endif
    MEMSET (pRetValFsRSNAConfigPSKValue->pu1_OctetList, 0, RSNA_PSK_LEN);
    pRetValFsRSNAConfigPSKValue->i4_Length = 0;    /*  WPA_PSK_LEN; */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPSKPassPhrase
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigPSKPassPhrase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPSKPassPhrase (INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsRSNAConfigPSKPassPhrase)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
#ifdef ISS_WANTED
    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        MEMCPY (pRetValFsRSNAConfigPSKPassPhrase->pu1_OctetList,
                pRsnaPaePortEntry->WpaConfigDB.au1PskPassPhrase,
                RSNA_MAX_PSK_LENGTH);

        pRetValFsRSNAConfigPSKPassPhrase->i4_Length = RSNA_MAX_PSK_LENGTH;
        return SNMP_SUCCESS;
    }
#endif

    MEMSET (pRetValFsRSNAConfigPSKPassPhrase->pu1_OctetList, 0,
            RSNA_MAX_PSK_LENGTH);

    pRetValFsRSNAConfigPSKPassPhrase->i4_Length = 0;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupUpdateCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupUpdateCount (INT4 i4IfIndex,
                                    UINT4
                                    *pu4RetValFsRSNAConfigGroupUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigGroupUpdateCount =
        pRsnaPaePortEntry->WpaConfigDB.u4GroupUpdateCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPairwiseUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigPairwiseUpdateCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPairwiseUpdateCount (INT4 i4IfIndex,
                                       UINT4
                                       *pu4RetValFsRSNAConfigPairwiseUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigPairwiseUpdateCount =
        pRsnaPaePortEntry->WpaConfigDB.u4PairwiseUpdateCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigGroupCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigGroupCipherSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigGroupCipherSize (INT4 i4IfIndex,
                                   UINT4 *pu4RetValFsRSNAConfigGroupCipherSize)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigGroupCipherSize =
        pRsnaPaePortEntry->WpaConfigDB.u4GroupCipherKeyLen;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigPMKLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPMKLifetime (INT4 i4IfIndex,
                               UINT4 *pu4RetValFsRSNAConfigPMKLifetime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigPMKLifetime =
        pRsnaPaePortEntry->WpaConfigDB.u4PMKLifeTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigPMKReauthThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPMKReauthThreshold (INT4 i4IfIndex,
                                      UINT4
                                      *pu4RetValFsRSNAConfigPMKReauthThreshold)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigPMKReauthThreshold =
        pRsnaPaePortEntry->WpaConfigDB.u4PmkReAuthThreshold;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigNumberOfPTKSAReplayCountersImplemented
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigNumberOfPTKSAReplayCountersImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigNumberOfPTKSAReplayCountersImplemented (INT4 i4IfIndex,
                                                          UINT4
                                                          *pu4RetValFsRSNAConfigNumberOfPTKSAReplayCountersImplemented)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigNumberOfPTKSAReplayCountersImplemented =
        pRsnaPaePortEntry->WpaConfigDB.u4NoOfPtkSaReplayCtr;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSATimeout
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSATimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSATimeout (INT4 i4IfIndex,
                             UINT4 *pu4RetValFsRSNAConfigSATimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigSATimeout =
        pRsnaPaePortEntry->WpaConfigDB.u4SATimeOut;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRSNAAuthenticationSuiteSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAAuthenticationSuiteSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAAuthenticationSuiteSelected (INT4 i4IfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsRSNAAuthenticationSuiteSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAAuthenticationSuiteSelected->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1AuthSuiteSelected,
            RSNA_MAX_AUTH_SUITE_TYPE_LENGTH);

    pRetValFsRSNAAuthenticationSuiteSelected->i4_Length =
        RSNA_MAX_AUTH_SUITE_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAPairwiseCipherSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAPairwiseCipherSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAPairwiseCipherSelected (INT4 i4IfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsRSNAPairwiseCipherSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAPairwiseCipherSelected->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1PairwiseCipherSelected,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValFsRSNAPairwiseCipherSelected->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAGroupCipherSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAGroupCipherSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAGroupCipherSelected (INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsRSNAGroupCipherSelected)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAGroupCipherSelected->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1GroupCipherSelected,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValFsRSNAGroupCipherSelected->i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAPMKIDUsed
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAPMKIDUsed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAPMKIDUsed (INT4 i4IfIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsRSNAPMKIDUsed)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRSNAPMKIDUsed);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAAuthenticationSuiteRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAAuthenticationSuiteRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAAuthenticationSuiteRequested (INT4 i4IfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsRSNAAuthenticationSuiteRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAAuthenticationSuiteRequested->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1AuthSuiteRequested,
            RSNA_MAX_AUTH_SUITE_TYPE_LENGTH);

    pRetValFsRSNAAuthenticationSuiteRequested->i4_Length =
        RSNA_MAX_AUTH_SUITE_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAPairwiseCipherRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAPairwiseCipherRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAPairwiseCipherRequested (INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsRSNAPairwiseCipherRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAPairwiseCipherRequested->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1PairwiseCipherRequested,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValFsRSNAPairwiseCipherRequested->i4_Length =
        RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAGroupCipherRequested
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAGroupCipherRequested
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAGroupCipherRequested (INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsRSNAGroupCipherRequested)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRetValFsRSNAGroupCipherRequested->pu1_OctetList,
            pRsnaPaePortEntry->WpaConfigDB.au1GroupCipherRequested,
            RSNA_MAX_CIPHER_TYPE_LENGTH);

    pRetValFsRSNAGroupCipherRequested->i4_Length = RSNA_MAX_CIPHER_TYPE_LENGTH;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNATKIPCounterMeasuresInvoked
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNATKIPCounterMeasuresInvoked
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNATKIPCounterMeasuresInvoked (INT4 i4IfIndex,
                                        UINT4
                                        *pu4RetValFsRSNATKIPCounterMeasuresInvoked)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNATKIPCounterMeasuresInvoked =
        pRsnaPaePortEntry->RsnaConfigDB.u4NoOfCtrMsrsInvoked;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNA4WayHandshakeFailures
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNA4WayHandshakeFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNA4WayHandshakeFailures (INT4 i4IfIndex,
                                   UINT4 *pu4RetValFsRSNA4WayHandshakeFailures)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNA4WayHandshakeFailures =
        pRsnaPaePortEntry->WpaConfigDB.u4NoOf4WayHandshakeFailures;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigNumberOfGTKSAReplayCountersImplemented
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigNumberOfGTKSAReplayCountersImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigNumberOfGTKSAReplayCountersImplemented (INT4 i4IfIndex,
                                                          UINT4
                                                          *pu4RetValFsRSNAConfigNumberOfGTKSAReplayCountersImplemented)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pu4RetValFsRSNAConfigNumberOfGTKSAReplayCountersImplemented =
        pRsnaPaePortEntry->WpaConfigDB.u4NoOfGtkReplayCtrs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSTKKeysImplemented
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSTKKeysImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSTKKeysImplemented (INT4 i4IfIndex,
                                      UINT4
                                      *pu4RetValFsRSNAConfigSTKKeysImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigSTKKeysImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSTKCipher
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSTKCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSTKCipher (INT4 i4IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsRSNAConfigSTKCipher)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRSNAConfigSTKCipher);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSTKRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSTKRekeyTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSTKRekeyTime (INT4 i4IfIndex,
                                UINT4 *pu4RetValFsRSNAConfigSTKRekeyTime)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigSTKRekeyTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSMKUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSMKUpdateCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSMKUpdateCount (INT4 i4IfIndex,
                                  UINT4 *pu4RetValFsRSNAConfigSMKUpdateCount)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigSMKUpdateCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSTKCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSTKCipherSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSTKCipherSize (INT4 i4IfIndex,
                                 UINT4 *pu4RetValFsRSNAConfigSTKCipherSize)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigSTKCipherSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSMKLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSMKLifetime (INT4 i4IfIndex,
                               UINT4 *pu4RetValFsRSNAConfigSMKLifetime)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigSMKLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigSMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigSMKReauthThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigSMKReauthThreshold (INT4 i4IfIndex,
                                      UINT4
                                      *pu4RetValFsRSNAConfigSMKReauthThreshold)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigSMKReauthThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigNumberOfSTKSAReplayCountersImplemented
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigNumberOfSTKSAReplayCountersImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigNumberOfSTKSAReplayCountersImplemented (INT4 i4IfIndex,
                                                          UINT4
                                                          *pu4RetValFsRSNAConfigNumberOfSTKSAReplayCountersImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigNumberOfSTKSAReplayCountersImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAPairwiseSTKSelected
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAPairwiseSTKSelected
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAPairwiseSTKSelected (INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsRSNAPairwiseSTKSelected)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRSNAPairwiseSTKSelected);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNASMKHandshakeFailures
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNASMKHandshakeFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNASMKHandshakeFailures (INT4 i4IfIndex,
                                  UINT4 *pu4RetValFsRSNASMKHandshakeFailures)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNASMKHandshakeFailures);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNASAERetransPeriod
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNASAERetransPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNASAERetransPeriod (INT4 i4IfIndex,
                              UINT4 *pu4RetValFsRSNASAERetransPeriod)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNASAERetransPeriod);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNASAEAntiCloggingThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNASAEAntiCloggingThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNASAEAntiCloggingThreshold (INT4 i4IfIndex,
                                      UINT4
                                      *pu4RetValFsRSNASAEAntiCloggingThreshold)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNASAEAntiCloggingThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNASAESync
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNASAESync
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNASAESync (INT4 i4IfIndex, UINT4 *pu4RetValFsRSNASAESync)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNASAESync);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupCipher
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupCipher (INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsRSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1GroupCipher,
            pSetValFsRSNAConfigGroupCipher->pu1_OctetList,
            pSetValFsRSNAConfigGroupCipher->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupRekeyMethod
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupRekeyMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupRekeyMethod (INT4 i4IfIndex,
                                    INT4 i4SetValFsRSNAConfigGroupRekeyMethod)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsRSNAConfigGroupRekeyMethod);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupRekeyTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupRekeyTime (INT4 i4IfIndex,
                                  UINT4 u4SetValFsRSNAConfigGroupRekeyTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.u4GroupGtkRekeyTime =
        u4SetValFsRSNAConfigGroupRekeyTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupRekeyPackets
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupRekeyPackets
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupRekeyPackets (INT4 i4IfIndex,
                                     UINT4
                                     u4SetValFsRSNAConfigGroupRekeyPackets)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigGroupRekeyPackets);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupRekeyStrict
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupRekeyStrict
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupRekeyStrict (INT4 i4IfIndex,
                                    INT4 i4SetValFsRSNAConfigGroupRekeyStrict)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsRSNAConfigGroupRekeyStrict);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigPSKValue
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigPSKValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigPSKValue (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsRSNAConfigPSKValue)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMSET (pRsnaPaePortEntry->WpaConfigDB.au1ConfigPsk, 0, RSNA_PSK_LEN);

    MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1ConfigPsk,
            pSetValFsRSNAConfigPSKValue->pu1_OctetList,
            pSetValFsRSNAConfigPSKValue->i4_Length);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigPSKPassPhrase
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigPSKPassPhrase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigPSKPassPhrase (INT4 i4IfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsRSNAConfigPSKPassPhrase)
{
    UINT1               au1Dot11DesiredSSID[RSNA_SSID_LEN] = "";
    UINT1               au1Psk[RSNA_PASS_PHRASE_LEN] = "";
    UINT1               au1PskPassPhrase[RSNA_PASS_PHRASE_LEN] = "";
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    MEMSET (au1Dot11DesiredSSID, 0, RSNA_SSID_LEN);
    MEMSET (au1PskPassPhrase, 0, RSNA_PASS_PHRASE_LEN);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    MEMSET (pRsnaPaePortEntry->WpaConfigDB.au1PskPassPhrase, 0,
            RSNA_PASS_PHRASE_LEN);

    MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1PskPassPhrase,
            pSetValFsRSNAConfigPSKPassPhrase->pu1_OctetList,
            pSetValFsRSNAConfigPSKPassPhrase->i4_Length);

    if (MEMCMP (pRsnaPaePortEntry->WpaConfigDB.au1PskPassPhrase,
                au1PskPassPhrase, RSNA_PASS_PHRASE_LEN) == 0)
    {
        return SNMP_SUCCESS;
    }

    /* The pass phrase has to be converted into PSK
     *      * Get the ssid name from WSS module and call the function
     *           * RsnaAlgoConvertPassPhraseToPsk to convert passphrase to PSK*/

    WssCfGetDot11DesiredSSID ((UINT4) i4IfIndex, au1Dot11DesiredSSID);

    RsnaAlgoConvertPassPhraseToPsk (au1Dot11DesiredSSID,
                                    (UINT1 *) pRsnaPaePortEntry->WpaConfigDB.
                                    au1PskPassPhrase,
                                    au1Psk,
                                    (UINT1) (STRLEN (au1Dot11DesiredSSID)));

    /* Store the PSK value in the database, as it will be used */
    MEMCPY (pRsnaPaePortEntry->WpaConfigDB.au1ConfigPsk, au1Psk, RSNA_PSK_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupUpdateCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupUpdateCount (INT4 i4IfIndex,
                                    UINT4 u4SetValFsRSNAConfigGroupUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.u4GroupUpdateCount =
        u4SetValFsRSNAConfigGroupUpdateCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigPairwiseUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigPairwiseUpdateCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigPairwiseUpdateCount (INT4 i4IfIndex,
                                       UINT4
                                       u4SetValFsRSNAConfigPairwiseUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.u4PairwiseUpdateCount =
        u4SetValFsRSNAConfigPairwiseUpdateCount;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigGroupCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigGroupCipherSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigGroupCipherSize (INT4 i4IfIndex,
                                   UINT4 u4SetValFsRSNAConfigGroupCipherSize)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigGroupCipherSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigPMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigPMKLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigPMKLifetime (INT4 i4IfIndex,
                               UINT4 u4SetValFsRSNAConfigPMKLifetime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.u4PMKLifeTime =
        u4SetValFsRSNAConfigPMKLifetime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigPMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigPMKReauthThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigPMKReauthThreshold (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValFsRSNAConfigPMKReauthThreshold)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigPMKReauthThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSATimeout
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSATimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSATimeout (INT4 i4IfIndex,
                             UINT4 u4SetValFsRSNAConfigSATimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.u4SATimeOut = u4SetValFsRSNAConfigSATimeout;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSTKCipher
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSTKCipher
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSTKCipher (INT4 i4IfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsRSNAConfigSTKCipher)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValFsRSNAConfigSTKCipher);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSTKRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSTKRekeyTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSTKRekeyTime (INT4 i4IfIndex,
                                UINT4 u4SetValFsRSNAConfigSTKRekeyTime)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigSTKRekeyTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSMKUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSMKUpdateCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSMKUpdateCount (INT4 i4IfIndex,
                                  UINT4 u4SetValFsRSNAConfigSMKUpdateCount)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigSMKUpdateCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSTKCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSTKCipherSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSTKCipherSize (INT4 i4IfIndex,
                                 UINT4 u4SetValFsRSNAConfigSTKCipherSize)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigSTKCipherSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSMKLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSMKLifetime (INT4 i4IfIndex,
                               UINT4 u4SetValFsRSNAConfigSMKLifetime)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigSMKLifetime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigSMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigSMKReauthThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigSMKReauthThreshold (INT4 i4IfIndex,
                                      UINT4
                                      u4SetValFsRSNAConfigSMKReauthThreshold)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsRSNAConfigSMKReauthThreshold);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupCipher
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupCipher
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupCipher (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsRSNAConfigGroupCipher)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    INT4                i4PairwiseCipherStatus = 0;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    /** checks if the cipher entered is not equal to both TKIP and CCMP **/
    if (MEMCMP (pTestValFsRSNAConfigGroupCipher->pu1_OctetList,
                gau1WpaCipherSuiteTKIP, RSNA_MAX_CIPHER_TYPE_LENGTH) != 0)
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_INVALID_GRP_CIPHER;
        CLI_SET_ERR (CLI_RSNA_INVALID_INVALID_GRP_CIPHER);

        return SNMP_FAILURE;
    }

    /* Checks whether the Pairwise and Group cipher combination is valid */

    if ((MEMCMP (pTestValFsRSNAConfigGroupCipher->pu1_OctetList,
                 gau1RsnaCipherSuiteTKIP, RSNA_MAX_CIPHER_TYPE_LENGTH)) == 0)
    {
        nmhGetFsRSNAConfigPairwiseCipherImplemented (i4IfIndex,
                                                     RSNA_CCMP,
                                                     (tSNMP_OCTET_STRING_TYPE *)
                                                     & i4PairwiseCipherStatus);

        if (i4PairwiseCipherStatus == RSNA_DISABLED)
        {
            CLI_SET_ERR (CLI_RSNA_INCOMPATIBLE_PAIRWISE_AND_GROUPWISE_CIPHER);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupRekeyMethod
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupRekeyMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupRekeyMethod (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       INT4
                                       i4TestValFsRSNAConfigGroupRekeyMethod)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsRSNAConfigGroupRekeyMethod);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupRekeyTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupRekeyTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     UINT4 u4TestValFsRSNAConfigGroupRekeyTime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinGrpRekeyTime =
        (UINT4) RSNA_MIN_GROUP_REKEY_TIME;
    UINT4               u4RsnaMaxGrpRekeyTime =
        (UINT4) RSNA_MAX_GROUP_REKEY_TIME;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((u4TestValFsRSNAConfigGroupRekeyTime < u4RsnaMinGrpRekeyTime)
        || (u4TestValFsRSNAConfigGroupRekeyTime > u4RsnaMaxGrpRekeyTime))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_REKEY_TIME;
        CLI_SET_ERR (CLI_RSNA_INVALID_REKEY_TIME);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupRekeyPackets
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupRekeyPackets
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupRekeyPackets (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        UINT4
                                        u4TestValFsRSNAConfigGroupRekeyPackets)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigGroupRekeyPackets);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupRekeyStrict
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupRekeyStrict
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupRekeyStrict (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       INT4
                                       i4TestValFsRSNAConfigGroupRekeyStrict)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsRSNAConfigGroupRekeyStrict);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigPSKValue
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigPSKValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigPSKValue (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsRSNAConfigPSKValue)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    if ((pTestValFsRSNAConfigPSKValue->i4_Length > RSNA_MAX_PSK_VALUE) ||
        (pTestValFsRSNAConfigPSKValue->i4_Length < RSNA_MIN_PSK_VALUE))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PSK_VALUE;
        CLI_SET_ERR (CLI_RSNA_INVALID_PSK_VALUE);

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigPSKPassPhrase
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigPSKPassPhrase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigPSKPassPhrase (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsRSNAConfigPSKPassPhrase)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    if ((pTestValFsRSNAConfigPSKPassPhrase->i4_Length
         < RSNA_MIN_PASS_PHRASE_LEN) ||
        (pTestValFsRSNAConfigPSKPassPhrase->i4_Length
         > RSNA_MAX_PASS_PHRASE_LEN))
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_PASSPHRASE);
        return (SNMP_FAILURE);

    }
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupUpdateCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupUpdateCount (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       UINT4
                                       u4TestValFsRSNAConfigGroupUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinGrpUpdateCount =
        (UINT4) RSNA_MIN_GROUP_UPDATE_COUNT;
    UINT4               u4RsnaMaxGrpUpdateCount =
        (UINT4) RSNA_MAX_GROUP_UPDATE_COUNT;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
    if ((u4TestValFsRSNAConfigGroupUpdateCount < u4RsnaMinGrpUpdateCount) ||
        (u4TestValFsRSNAConfigGroupUpdateCount > u4RsnaMaxGrpUpdateCount))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_GROUP_UPDATE_COUNT;
        CLI_SET_ERR (CLI_RSNA_INVALID_GROUP_UPDATE_COUNT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigPairwiseUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigPairwiseUpdateCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigPairwiseUpdateCount (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          UINT4
                                          u4TestValFsRSNAConfigPairwiseUpdateCount)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinPairwiseUpdateCount =
        (UINT4) RSNA_MIN_PAIRWISE_UPDATE_COUNT;
    UINT4               u4RsnaMaxPairwiseUpdateCount =
        (UINT4) RSNA_MAX_PAIRWISE_UPDATE_COUNT;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
    if ((u4TestValFsRSNAConfigPairwiseUpdateCount <
         u4RsnaMinPairwiseUpdateCount)
        || (u4TestValFsRSNAConfigPairwiseUpdateCount >
            u4RsnaMaxPairwiseUpdateCount))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PAIRWISE_UPDATE_COUNT;
        CLI_SET_ERR (CLI_RSNA_INVALID_PAIRWISE_UPDATE_COUNT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigGroupCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigGroupCipherSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigGroupCipherSize (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      UINT4
                                      u4TestValFsRSNAConfigGroupCipherSize)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigGroupCipherSize);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigPMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigPMKLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigPMKLifetime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  UINT4 u4TestValFsRSNAConfigPMKLifetime)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinPMKLifeTime = (UINT4) RSNA_MIN_PMK_LIFETIME;
    UINT4               u4RsnaMaxPMKLifeTime = (UINT4) RSNA_MAX_PMK_LIFETIME;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
    if ((u4TestValFsRSNAConfigPMKLifetime <
         u4RsnaMinPMKLifeTime) || (u4TestValFsRSNAConfigPMKLifetime >
                                   u4RsnaMaxPMKLifeTime))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_PMK_LIFETIME;
        CLI_SET_ERR (CLI_RSNA_INVALID_PMK_LIFETIME);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigPMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigPMKReauthThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigPMKReauthThreshold (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4
                                         u4TestValFsRSNAConfigPMKReauthThreshold)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigPMKReauthThreshold);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSATimeout
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSATimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSATimeout (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                UINT4 u4TestValFsRSNAConfigSATimeout)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    UINT4               u4RsnaMinSaTimeout = (UINT4) RSNA_MIN_SA_TIMEOUT;
    UINT4               u4RsnaMaxSaTimeout = (UINT4) RSNA_MAX_SA_TIMEOUT;

    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }
    if ((u4TestValFsRSNAConfigSATimeout < u4RsnaMinSaTimeout)
        || (u4TestValFsRSNAConfigSATimeout > u4RsnaMaxSaTimeout))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_SA_TIMEOUT;
        CLI_SET_ERR (CLI_RSNA_INVALID_SA_TIMEOUT);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSTKCipher
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSTKCipher
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSTKCipher (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsRSNAConfigSTKCipher)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValFsRSNAConfigSTKCipher);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSTKRekeyTime
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSTKRekeyTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSTKRekeyTime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   UINT4 u4TestValFsRSNAConfigSTKRekeyTime)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigSTKRekeyTime);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSMKUpdateCount
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSMKUpdateCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSMKUpdateCount (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     UINT4 u4TestValFsRSNAConfigSMKUpdateCount)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigSMKUpdateCount);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSTKCipherSize
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSTKCipherSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSTKCipherSize (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    UINT4 u4TestValFsRSNAConfigSTKCipherSize)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigSTKCipherSize);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSMKLifetime
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSMKLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSMKLifetime (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  UINT4 u4TestValFsRSNAConfigSMKLifetime)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigSMKLifetime);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigSMKReauthThreshold
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigSMKReauthThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigSMKReauthThreshold (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         UINT4
                                         u4TestValFsRSNAConfigSMKReauthThreshold)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsRSNAConfigSMKReauthThreshold);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRSNAConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNAConfigTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRSNAStatsTable. */
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsSTAAddress
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsSTAAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsSTAAddress (INT4 i4IfIndex, UINT4 u4FsRSNAStatsIndex,
                             tMacAddr * pRetValFsRSNAStatsSTAAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRSNAStatsSTAAddress);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRSNAConfigPairwiseCiphersTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRSNAConfigPairwiseCiphersTable (INT4 i4IfIndex,
                                                          UINT4
                                                          u4FsRSNAConfigPairwiseCipherIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAConfigPairwiseCipherIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRSNAConfigPairwiseCiphersTable (INT4 *pi4IfIndex,
                                                  UINT4
                                                  *pu4FsRSNAConfigPairwiseCipherIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetFirstPortEntry (&pRsnaPaePortEntry);
    if (pRsnaPaePortEntry == NULL)
    {
        return (SNMP_FAILURE);
    }

    else
    {
        *pi4IfIndex = pRsnaPaePortEntry->u2Port;
    }

    *pu4FsRSNAConfigPairwiseCipherIndex = RSNA_MIN_CIPHER_SUITE_INDEX;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsRSNAConfigPairwiseCipherIndex
                nextFsRSNAConfigPairwiseCipherIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRSNAConfigPairwiseCiphersTable (INT4 i4IfIndex,
                                                 INT4 *pi4NextIfIndex,
                                                 UINT4
                                                 u4FsRSNAConfigPairwiseCipherIndex,
                                                 UINT4
                                                 *pu4NextFsRSNAConfigPairwiseCipherIndex)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    tRsnaPaePortEntry  *pNextRsnaPaePortEntry = NULL;
    if (i4IfIndex == 0)
    {
        RsnaGetFirstPortEntry (&pNextRsnaPaePortEntry);
        if (pNextRsnaPaePortEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
            *pu4NextFsRSNAConfigPairwiseCipherIndex =
                RSNA_MIN_CIPHER_SUITE_INDEX;
        }
    }
    else
    {
        if (u4FsRSNAConfigPairwiseCipherIndex == RSNA_MAX_CIPHER_SUITE_INDEX)
        {
            /* fetch the data structure of the current index */
            RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

            if (pRsnaPaePortEntry == NULL)
            {
                RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                          "No Rsna Info for the given port!! ... \n");
                return (SNMP_FAILURE);
            }

            RsnaGetNextPortEntry (pRsnaPaePortEntry, &pNextRsnaPaePortEntry);
            if (pNextRsnaPaePortEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            *pi4NextIfIndex = pNextRsnaPaePortEntry->u2Port;
            *pu4NextFsRSNAConfigPairwiseCipherIndex =
                RSNA_MIN_CIPHER_SUITE_INDEX;
        }
        else
        {
            *pi4NextIfIndex = i4IfIndex;
            *pu4NextFsRSNAConfigPairwiseCipherIndex =
                u4FsRSNAConfigPairwiseCipherIndex + RSNA_MIN_CIPHER_SUITE_INDEX;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPairwiseCipherImplemented
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex

                The Object 
                retValFsRSNAConfigPairwiseCipherImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPairwiseCipherImplemented (INT4 i4IfIndex,
                                             UINT4
                                             u4FsRSNAConfigPairwiseCipherIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValFsRSNAConfigPairwiseCipherImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pRetValFsRSNAConfigPairwiseCipherImplemented);
    UNUSED_PARAM (u4FsRSNAConfigPairwiseCipherIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPairwiseCipherActivated
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex

                The Object 
                retValFsRSNAConfigPairwiseCipherActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPairwiseCipherActivated (INT4 i4IfIndex,
                                           UINT4
                                           u4FsRSNAConfigPairwiseCipherIndex,
                                           INT4
                                           *pi4RetValFsRSNAConfigPairwiseCipherActivated)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValFsRSNAConfigPairwiseCipherActivated =
        (INT4) pRsnaPaePortEntry->WpaConfigDB.u4PairwiseKeysSupported;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAConfigPairwiseCipherIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigPairwiseCipherSizeImplemented
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex

                The Object 
                retValFsRSNAConfigPairwiseCipherSizeImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigPairwiseCipherSizeImplemented (INT4 i4IfIndex,
                                                 UINT4
                                                 u4FsRSNAConfigPairwiseCipherIndex,
                                                 UINT4
                                                 *pu4RetValFsRSNAConfigPairwiseCipherSizeImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAConfigPairwiseCipherIndex);
    UNUSED_PARAM (pu4RetValFsRSNAConfigPairwiseCipherSizeImplemented);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigPairwiseCipherActivated
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex

                The Object 
                setValFsRSNAConfigPairwiseCipherActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigPairwiseCipherActivated (INT4 i4IfIndex,
                                           UINT4
                                           u4FsRSNAConfigPairwiseCipherIndex,
                                           INT4
                                           i4SetValFsRSNAConfigPairwiseCipherActivated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAConfigPairwiseCipherIndex);
    UNUSED_PARAM (i4SetValFsRSNAConfigPairwiseCipherActivated);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigPairwiseCipherActivated
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex

                The Object 
                testValFsRSNAConfigPairwiseCipherActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigPairwiseCipherActivated (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              UINT4
                                              u4FsRSNAConfigPairwiseCipherIndex,
                                              INT4
                                              i4TestValFsRSNAConfigPairwiseCipherActivated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsRSNAConfigPairwiseCipherIndex);
    UNUSED_PARAM (i4TestValFsRSNAConfigPairwiseCipherActivated);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRSNAConfigPairwiseCiphersTable
 Input       :  The Indices
                IfIndex
                FsRSNAConfigPairwiseCipherIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRSNAConfigPairwiseCiphersTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRSNAStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRSNAStatsTable
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRSNAStatsTable (INT4 i4IfIndex,
                                          UINT4 u4FsRSNAStatsIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRSNAStatsTable
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRSNAStatsTable (INT4 *pi4IfIndex, UINT4 *pu4FsRSNAStatsIndex)
{
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pu4FsRSNAStatsIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRSNAStatsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsRSNAStatsIndex
                nextFsRSNAStatsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRSNAStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                 UINT4 u4FsRSNAStatsIndex,
                                 UINT4 *pu4NextFsRSNAStatsIndex)
{
    pu4NextFsRSNAStatsIndex = NULL;
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    UNUSED_PARAM (pu4NextFsRSNAStatsIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsVersion
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsVersion (INT4 i4IfIndex, UINT4 u4FsRSNAStatsIndex,
                          UINT4 *pu4RetValFsRSNAStatsVersion)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    UNUSED_PARAM (pu4RetValFsRSNAStatsVersion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsSelectedPairwiseCipher
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsSelectedPairwiseCipher
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsSelectedPairwiseCipher (INT4 i4IfIndex,
                                         UINT4 u4FsRSNAStatsIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFsRSNAStatsSelectedPairwiseCipher)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    UNUSED_PARAM (pRetValFsRSNAStatsSelectedPairwiseCipher);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsTKIPICVErrors
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsTKIPICVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsTKIPICVErrors (INT4 i4IfIndex, UINT4 u4FsRSNAStatsIndex,
                                UINT4 *pu4RetValFsRSNAStatsTKIPICVErrors)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    UNUSED_PARAM (pu4RetValFsRSNAStatsTKIPICVErrors);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsTKIPLocalMICFailures
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsTKIPLocalMICFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsTKIPLocalMICFailures (INT4 i4IfIndex, UINT4 u4FsRSNAStatsIndex,
                                       UINT4
                                       *pu4RetValFsRSNAStatsTKIPLocalMICFailures)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    UNUSED_PARAM (pu4RetValFsRSNAStatsTKIPLocalMICFailures);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsTKIPRemoteMICFailures
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsTKIPRemoteMICFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsTKIPRemoteMICFailures (INT4 i4IfIndex,
                                        UINT4 u4FsRSNAStatsIndex,
                                        UINT4
                                        *pu4RetValFsRSNAStatsTKIPRemoteMICFailures)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAStatsTKIPRemoteMICFailures);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAStatsTKIPReplays
 Input       :  The Indices
                IfIndex
                FsRSNAStatsIndex

                The Object 
                retValFsRSNAStatsTKIPReplays
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAStatsTKIPReplays (INT4 i4IfIndex, UINT4 u4FsRSNAStatsIndex,
                              UINT4 *pu4RetValFsRSNAStatsTKIPReplays)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsRSNAStatsTKIPReplays);
    UNUSED_PARAM (u4FsRSNAStatsIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRSNAConfigActivated
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsRSNAConfigActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRSNAConfigActivated (INT4 i4IfIndex,
                             INT4 *pi4RetValFsRSNAConfigActivated)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    *pi4RetValFsRSNAConfigActivated =
        (INT4) pRsnaPaePortEntry->WpaConfigDB.bRSNAOptionImplemented;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRSNAConfigActivated
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsRSNAConfigActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRSNAConfigActivated (INT4 i4IfIndex, INT4 i4SetValFsRSNAConfigActivated)
{
    tRsnaSupInfo       *pRsnaSupInfo = NULL;
    UINT4               u4BssIfIndex = 0;
    UINT1               au1BssId[RSNA_ETH_ADDR_LEN];

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        return (SNMP_FAILURE);
    }

    pRsnaPaePortEntry->WpaConfigDB.bRSNAOptionImplemented =
        (BOOL1) i4SetValFsRSNAConfigActivated;
    /* Checks whether the WTP is binded to the wlan,if binded then UPDATE WLAN message
     *        is triggered. */

    if (WssUtilIsProfileIndexApBinded (i4IfIndex, &u4BssIfIndex, au1BssId) ==
        OSIX_TRUE)
    {
        if (pRsnaPaePortEntry->WpaConfigDB.bRSNAOptionImplemented ==
            RSNA_DISABLED)
        {

            TMO_SLL_Scan (&(pRsnaPaePortEntry->RsnaSupInfoList),
                          pRsnaSupInfo, tRsnaSupInfo *)
            {
                if (pRsnaSupInfo->pRsnaSessNode != NULL)
                {
                    WssRsnaSendDeAuthMsg (au1BssId,
                                          pRsnaSupInfo->au1SuppMacAddr);
                    WssStaDeleteRsnaSessionKey (pRsnaSupInfo->au1SuppMacAddr,
                                                pRsnaSupInfo->pRsnaSessNode->
                                                u4BssIfIndex);
                    RsnaSessDeleteSession (pRsnaSupInfo->pRsnaSessNode);
                }

                pRsnaSupInfo = NULL;
            }

        }
        else
        {
/*To be changed*/
            WssIfDeauthenticationStations (u4BssIfIndex, au1BssId);
        }

        WssSendUpdateWlanMsgfromRsna ((UINT4) i4IfIndex, WSSRSNA_GTK_KEY);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsRSNAConfigActivated
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsRSNAConfigActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRSNAConfigActivated (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValFsRSNAConfigActivated)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;
    INT4                i4MixMode = 0;
    INT4                i4Dot11RSNAStatus = 0;
    if ((i4IfIndex < CFA_MIN_WSS_IF_INDEX) ||
        (i4IfIndex > CFA_MAX_WSS_IF_INDEX))
    {
        *pu4ErrorCode = CLI_RSNA_INVALID_WLAN_ID;
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);

        return SNMP_FAILURE;
    }

    RsnaGetPortEntry ((UINT2) i4IfIndex, &pRsnaPaePortEntry);

    if (pRsnaPaePortEntry == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "No Rsna Info for the given port!! ... \n");
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return (SNMP_FAILURE);
    }
    if (i4TestValFsRSNAConfigActivated != RSNA_ENABLED)
    {
        if (i4TestValFsRSNAConfigActivated != RSNA_DISABLED)
        {
            CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
            return SNMP_FAILURE;
        }

    }

    /*Testing weather Mix Mode is enabled or not */
    if (nmhGetFsRSNAConfigMixMode (i4IfIndex, &i4MixMode) == SNMP_SUCCESS)
    {
        if (i4MixMode != MIX_MODE_ENABLED)
        {
            if (nmhGetDot11RSNAActivated (i4IfIndex, &i4Dot11RSNAStatus) ==
                SNMP_SUCCESS)
            {
                if (i4Dot11RSNAStatus == RSNA_ENABLED)
                {
                    RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                              "Mix Mode is not enabled !! ...\n");
                    CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
        return SNMP_FAILURE;
    }
    if (WpaTestRadioType (i4IfIndex) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "WPA not supported for AN,BGN and AC !! ...\n");
        return SNMP_FAILURE;
    }
    /* This function Checks whether Authentication Algorithm in the system
     *      * has been set to 'Open' or 'Shared-Key' before Setting RSNA to TRUE */
    if (i4TestValFsRSNAConfigActivated == RSNA_ENABLED)
    {
        if (RsnaIsOpenAuthEnabled (i4IfIndex) != RSNA_SUCCESS)
        {
            CLI_SET_ERR (CLI_RSNA_INVALID_WLAN_ID);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWPAConfigAuthenticationSuitesTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWPAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWPAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                              UINT4
                                                              u4FsWPAConfigAuthenticationSuiteIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsWPAConfigAuthenticationSuiteIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWPAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWPAConfigAuthenticationSuitesTable (INT4 *pi4IfIndex,
                                                      UINT4
                                                      *pu4FsWPAConfigAuthenticationSuiteIndex)
{
    if (WpaGetFirstIndexFsWPAConfigAuthenticationSuitesTable
        (pi4IfIndex, pu4FsWPAConfigAuthenticationSuiteIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWPAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsWPAConfigAuthenticationSuiteIndex
                nextFsWPAConfigAuthenticationSuiteIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWPAConfigAuthenticationSuitesTable (INT4 i4IfIndex,
                                                     INT4 *pi4NextIfIndex,
                                                     UINT4
                                                     u4FsWPAConfigAuthenticationSuiteIndex,
                                                     UINT4
                                                     *pu4NextFsWPAConfigAuthenticationSuiteIndex)
{
    if (WpaGetNextIndexFsWPAConfigAuthenticationSuitesTable
        (i4IfIndex, pi4NextIfIndex,
         u4FsWPAConfigAuthenticationSuiteIndex,
         pu4NextFsWPAConfigAuthenticationSuiteIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWPAConfigAuthenticationSuiteImplemented
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex

                The Object 
                retValFsWPAConfigAuthenticationSuiteImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWPAConfigAuthenticationSuiteImplemented (INT4 i4IfIndex,
                                                 UINT4
                                                 u4FsWPAConfigAuthenticationSuiteIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pRetValFsWPAConfigAuthenticationSuiteImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsWPAConfigAuthenticationSuiteIndex);
    UNUSED_PARAM (pRetValFsWPAConfigAuthenticationSuiteImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWPAConfigAuthenticationSuiteActivated
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex

                The Object 
                retValFsWPAConfigAuthenticationSuiteActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWPAConfigAuthenticationSuiteActivated (INT4 i4IfIndex,
                                               UINT4
                                               u4FsWPAConfigAuthenticationSuiteIndex,
                                               INT4
                                               *pi4RetValFsWPAConfigAuthenticationSuiteActivated)
{
    if (WpaGetFsRSNAConfigAuthenticationSuiteEnabled
        (i4IfIndex, u4FsWPAConfigAuthenticationSuiteIndex,
         pi4RetValFsWPAConfigAuthenticationSuiteActivated) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWPAConfigAuthenticationSuiteActivated
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex

                The Object 
                setValFsWPAConfigAuthenticationSuiteActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWPAConfigAuthenticationSuiteActivated (INT4 i4IfIndex,
                                               UINT4
                                               u4FsWPAConfigAuthenticationSuiteIndex,
                                               INT4
                                               i4SetValFsWPAConfigAuthenticationSuiteActivated)
{
    if (WpaSetFsRSNAConfigAuthenticationSuiteEnabled
        (i4IfIndex, u4FsWPAConfigAuthenticationSuiteIndex,
         i4SetValFsWPAConfigAuthenticationSuiteActivated) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWPAConfigAuthenticationSuiteActivated
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex

                The Object 
                testValFsWPAConfigAuthenticationSuiteActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWPAConfigAuthenticationSuiteActivated (UINT4 *pu4ErrorCode,
                                                  INT4 i4IfIndex,
                                                  UINT4
                                                  u4FsWPAConfigAuthenticationSuiteIndex,
                                                  INT4
                                                  i4TestValFsWPAConfigAuthenticationSuiteActivated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsWPAConfigAuthenticationSuiteIndex);
    UNUSED_PARAM (i4TestValFsWPAConfigAuthenticationSuiteActivated);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWPAConfigAuthenticationSuitesTable
 Input       :  The Indices
                IfIndex
                FsWPAConfigAuthenticationSuiteIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWPAConfigAuthenticationSuitesTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif
