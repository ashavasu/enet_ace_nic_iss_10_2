/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnainit.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description : This file contains init functions for Rsna Module 
*********************************************************************/

#ifndef _RSNAINIT_C
#define _RSNAINIT_C

#include "rsnainc.h"

/****************************************************************************
*                                                                           *
* Function     : RsnaInitModuleStart                                        *
*                                                                           *
* Description  : This function creates mempools used by the rsna module     *
*                This invoked by PnacModuleStart                            *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaInitModuleStart (VOID)
{

    RSNA_MEMSET (&gRsnaGlobals, RSNA_INIT_VAL, sizeof (tRsnaGlobals));
    gRsnaGlobals.u4RsnaTrcOpt = 0;
    RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC,
              "Initializing RSNA.....\n");

    if (RsnaSizingMemCreateMemPools () != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, " Create mem pool for Pnac failed\n");
        return RSNA_FAILURE;
    }

    /* Assign MempoolIds created in sz.c to global MempoolIds */
    RsnaAssignMempoolIds ();

    /* Initialise the timer module */
    if (RsnaTmrInit () != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, " Timer Init Failed\n");
        RsnaSizingMemDeleteMemPools ();
        return RSNA_FAILURE;
    }


    /* Initialise the Rsna Retry module */
    if (RsnaEapolRtyInit() != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, " Rsna Retry Init Failed\n");
        RsnaSizingMemDeleteMemPools ();
        return RSNA_FAILURE;
    }

    /* Create the RBTree to store the RSNA port entry */
    gRsnaGlobals.RsnaPortEntryTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tRsnaPaePortEntry,
                                              RsnaPortEntryNode)),
                              RsnaCompareProfileInterface);

    if (gRsnaGlobals.RsnaPortEntryTable == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, "RB Tree Creation for "
                  " Port Entry Table failed\n");
        RsnaSizingMemDeleteMemPools ();
        return RSNA_FAILURE;
    }

    /* Create the RBTree to store the RSNA PMKID */
    gRsnaGlobals.RsnaPmkTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tRsnaPmkSa,
                                              RsnaPmksaNode)),
                              RsnaComparePmkId);

    if (gRsnaGlobals.RsnaPmkTable == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, "RB Tree Creation for "
                  " PMK Table failed\n");
        RsnaSizingMemDeleteMemPools ();
        return RSNA_FAILURE;
    }


    /* Create the RBTree to store the RSNA SUPPLICANT STATS INFO */
    gRsnaGlobals.RsnaSuppStatsTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tRsnaSuppStatsInfo,
                                              RsnaSuppStatsNode)),
                                       RsnaCompareSuppStatsIndex);

    if (gRsnaGlobals.RsnaSuppStatsTable == NULL)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, "RB Tree Creation for "
                  " RSNA Supplicant Stats Table Failed\n");
        RsnaSizingMemDeleteMemPools ();
        return RSNA_FAILURE;
    }


    if (RsnaCreateSemaphore () != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, " Create Semaphore for Rsna failed\n");
        return RSNA_FAILURE;

    }

    RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC,
              "Initialized RSNA.....\n");

    return RSNA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RsnaCreateSemaphore                                  */
/*                                                                           */
/* Description        : This function creates the protocol semaphore.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : All variables in the GLobal structure are            */
/*                      initialised.                                         */
/*                                                                           */
/* Return Value(s)    : RSNA_SUCCESS - On success                            */
/*                      RSNA_FAILURE - On failure                            */
/*****************************************************************************/

INT4
RsnaCreateSemaphore (VOID)
{

    if (RSNA_CREATE_SEMAPHORE
        (RSNA_PROTOCOL_SEMAPHORE, RSNA_BINARY_SEM, RSNA_SEM_FLAGS,
         &gRsnaSemId) != OSIX_SUCCESS)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_CONTROL_PATH_TRC |
                  RSNA_ALL_FAILURE_TRC, " Create Semaphore for Rsna failed\n");
        return RSNA_FAILURE;

    }

    /* RSNA_TAKE_PROTOCOL_SEMAPHORE (gRsnaSemId); */

    return RSNA_SUCCESS;

}

/****************************************************************************
*                                                                           *
* Function     : RsnaInitPort                                               *
*                                                                           *
* Description  : Function to initialise the port specific rsna information  *
*                This is invoked by PnacInitPort                            *
*                                                                           *
* Input        : u2Port :- Refers to the port                               *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaInitPort (UINT2 u2Port)
{

    tPnacPaePortEntry  *pPnacPaePortEntry = NULL;
    tRsnaPaePortEntry  *pRsnaPaePortInfo = NULL;
    UINT1               au1RandNo[RSNA_NONCE_LEN] = "";
    UINT1               au1Buf[RSNA_INIT_CTR_DATA_LEN] = "";
    UINT1               au1Digest[SHA1_HASH_SIZE] = "";
    UINT4               u4TimeStamp = 0;
    UINT1               u1Ctr = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (au1RandNo, 0, RSNA_NONCE_LEN);
    MEMSET (au1Buf, 0, RSNA_INIT_CTR_DATA_LEN);
    MEMSET (au1Digest, 0, SHA1_HASH_SIZE);

    if (PnacGetPortEntry (u2Port, &pPnacPaePortEntry) == PNAC_FAILURE)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC, "RsnaInitPort:- PnacGetPortEntry"
                  "returned Failure\n");
        return (OSIX_FAILURE);
    }

    if ((pPnacPaePortEntry->portIfMap.u1IfType == CFA_WLAN) ||
        (pPnacPaePortEntry->portIfMap.u1IfType == CFA_RADIO))
    {
        /* FIXME  Start */
        pPnacPaePortEntry->u1Capabilities = PNAC_PORT_CAPABILITY_AUTH;
        /* FIXME End */

        pRsnaPaePortInfo =
            (tRsnaPaePortEntry *) (&(pPnacPaePortEntry->pRsnaPaePortInfo));

        if ((pRsnaPaePortInfo = RSNA_MEM_ALLOCATE_MEM_BLK
             (RSNA_PORTINFO_MEMPOOL_ID)) == NULL)
        {
            RSNA_TRC (RSNA_INIT_SHUT_TRC,
                      "RsnaInitPort:- RSNA_ALLOCATE_PORT_INFO_NODE"
                      "returned Failure\n");
            return (OSIX_FAILURE);
        }

        CfaGetIfInfo (u2Port, &IfInfo);
        MEMSET (pRsnaPaePortInfo, 0, sizeof (tRsnaPaePortEntry));
        pRsnaPaePortInfo->pPnacPaePortEntry = pPnacPaePortEntry;
        pRsnaPaePortInfo->u2Port = u2Port;
        MEMCPY (pRsnaPaePortInfo->au1SrcMac,
                IfInfo.au1MacAddr, RSNA_ETH_ADDR_LEN);
        TMO_SLL_Init (&(pRsnaPaePortInfo->RsnaPskList));
        TMO_SLL_Init (&(pRsnaPaePortInfo->RsnaSupInfoList));
        RsnaUtilGetRandom (au1RandNo, RSNA_NONCE_LEN);
        RsnaUtilGetRandom (pRsnaPaePortInfo->RsnaGlobalGtk.au1Gmk,
                           RSNA_GMK_LEN);
        MEMCPY (au1Buf, "Init Counter", RSNA_INIT_COUNT_LABLE_LEN);
        au1Buf[RSNA_INIT_COUNT_LABLE_LEN] = 0;
        MEMCPY (&(au1Buf[RSNA_INIT_COUNT_LABLE_LEN + 1]),
                pRsnaPaePortInfo->au1SrcMac, RSNA_ETH_ADDR_LEN);
        OsixGetSysTime (&u4TimeStamp);
        MEMCPY (&(au1Buf[RSNA_INIT_COUNT_LABLE_LEN + 1 + RSNA_ETH_ADDR_LEN]),
                &u4TimeStamp, sizeof (UINT4));
        OsixGetSysTime (&u4TimeStamp);
        MEMCPY (&(au1Buf[RSNA_INIT_COUNT_LABLE_LEN + 1 +
                         RSNA_ETH_ADDR_LEN + sizeof (UINT4)]),
                &u4TimeStamp, sizeof (UINT4));

        RsnaAlgoHmacSha1 (au1RandNo, RSNA_NONCE_LEN, au1Buf,
                          RSNA_INIT_CTR_DATA_LEN,
                          pRsnaPaePortInfo->RsnaGlobalGtk.au1Counter);
        u1Ctr++;
        au1Buf[RSNA_INIT_COUNT_LABLE_LEN] = u1Ctr;

        RsnaAlgoHmacSha1 (au1RandNo, RSNA_NONCE_LEN, au1Buf,
                          RSNA_INIT_CTR_DATA_LEN, au1Digest);

        MEMCPY (&
                (pRsnaPaePortInfo->RsnaGlobalGtk.
                 au1Counter[SHA1_HASH_SIZE]), au1Digest,
                (RSNA_NONCE_LEN - SHA1_HASH_SIZE));

        pRsnaPaePortInfo->RsnaGlobalGtk.pRsnaPaePortInfo = pRsnaPaePortInfo;
    }

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : RsnaAssignMempoolIds                                 */
/*                                                                           */
/* Description        : This function assign mempools from sz.c ie.          */
/*                      RsnaMemPoolIds to global mempoolIds.                 */
/*                                                                           */
/* Input(s)           :  None                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
RsnaAssignMempoolIds (VOID)
{

    /*tRsnaPaePortEntry */
    RSNA_PORTINFO_MEMPOOL_ID = RSNAMemPoolIds[MAX_RSNA_PORT_INFO_SIZING_ID];

    /* tRsnaSessionNode */
    RSNA_AUTHSESSNODE_MEMPOOL_ID
        = RSNAMemPoolIds[MAX_RSNA_AUTHSESSNODE_INFO_SIZING_ID];

    /* tRsnaPmkSa */
    RSNA_PMKSA_MEMPOOL_ID = RSNAMemPoolIds[MAX_RSNA_PMKSA_NODE_INFO_SIZING_ID];

    /* tRsnaSupInfo */
    RSNA_SUPP_INFO_MEMPOOL_ID = RSNAMemPoolIds[MAX_RSNA_SUPP_INFO_SIZING_ID];

    /*tRsnaSuppStatsInfo*/
    RSNA_SUPP_STATS_INFO_MEMPOOL_ID = RSNAMemPoolIds[MAX_RSNA_SUPP_STATS_INFO_SIZING_ID];

    /* tWssRSNANotifyParams */
    RSNA_WSS_INFO_MEMPOOL_ID = RSNAMemPoolIds[MAX_RSNA_WSS_INFO_SIZING_ID];
}

#endif /* _RSNAINIT_C */
