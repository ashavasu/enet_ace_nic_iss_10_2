/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnatmr.c,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#include "rsnainc.h"

extern UINT4        gu4Stups;

PRIVATE VOID        (*gaRsnaTimerRoutines[]) (VOID *) =
{
NULL,
        RsnaTmrProcRetransTimer,
        RsnaTmrProcessPMKReKeyTimer,
        RsnaTmrProcessDeletePMKTimer,
        RsnaTmrProcStopTkipCtrMsrTimer,
        RsnaTmrProcessReKeyGmk,
        RsnaTmrProcessReKeyGtk,
        RsnaTmrProcessEapPacket};

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrInit                                                *
*                                                                           *
* Description  : Function to create rsna timer list                         *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaTmrInit (VOID)
{

    if (TmrCreateTimerList (PNAC_INTF_TASK_NAME, PNAC_RSNA_TIMER_EXP_EVENT,
                            NULL,
                            (tTimerListId) & gRsnaGlobals.u4RsnaTimerId) ==
        TMR_FAILURE)
    {
        RSNA_TRC (RSNA_INIT_SHUT_TRC | RSNA_ALL_FAILURE_TRC |
                  RSNA_OS_RESOURCE_TRC | RSNA_CONTROL_PATH_TRC,
                  " TMR: Timer list creation failed\n");
        return (RSNA_FAILURE);
    }

    return (RSNA_SUCCESS);
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcessTimer                                        *
*                                                                           *
* Description  : Function invoked to process the expired timers             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcessTimer (VOID)
{
    tRsnaTimer         *pRsnaTimer = NULL;
    tTmrAppTimer       *pExpTimer = NULL;

    pExpTimer =
        TmrGetNextExpiredTimer ((tTimerListId) gRsnaGlobals.u4RsnaTimerId);
    while (pExpTimer != NULL)
    {
        pRsnaTimer = (tRsnaTimer *) pExpTimer->u4Data;

        if (pRsnaTimer != NULL)
        {
            RSNA_INIT_TIMER (pRsnaTimer);
            if ((pRsnaTimer->u1TimerId > RSNA_MIN_TIMER_ID) &&
                (pRsnaTimer->u1TimerId < RSNA_MAX_TIMER_ID))
            {
                (*gaRsnaTimerRoutines[pRsnaTimer->u1TimerId])
                    (pRsnaTimer->pContext);
            }
        }
        pExpTimer =
            TmrGetNextExpiredTimer ((tTimerListId) gRsnaGlobals.u4RsnaTimerId);
    }
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrStartTimer                                          *
*                                                                           *
* Description  : Function to start the timer                                *
*                                                                           *
* Input        : pRsnaTimer :- Points to the rsna timer                     *
*                TimerId    :- Timer Id                                     *
*                u4Timeout  :- The valu for which the timer to be started   *
*                pTmrContext :- Refers to the context that will be used     *
*                               for processing the timer                    *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaTmrStartTimer (tRsnaTimer * pRsnaTimer, eRsnaTimerId TimerId,
                   UINT4 u4TimeOut, VOID *pTmrContext)
{
    tRsnaSessionNode   *pRsnaSessNode = NULL;
    UINT4               u4Duration;

    if (RSNA_IS_TIMER_ACTIVE (pRsnaTimer) == TRUE)
    {
        RSNA_TRC (DATA_PATH_TRC, "RsnaTmrStartTimer:Attempt to start"
                  "already running timer\n");
        return RSNA_FAILURE;
    }

    RSNA_ACTIVATE_TIMER (pRsnaTimer);
    pRsnaTimer->u1TimerId = (UINT1) TimerId;
    pRsnaTimer->pContext = pTmrContext;
    pRsnaTimer->Node.u4Data = (FS_ULONG) pRsnaTimer;

    switch (pRsnaTimer->u1TimerId)
    {
        case RSNA_RETRANS_TIMER_ID:
            pRsnaSessNode = (tRsnaSessionNode *) pTmrContext;
            pRsnaSessNode->u4RefCount++;

            if (pRsnaSessNode->u1PtkFsmState == RSNA_4WAY_PTKSTART)
            {
                /* This is a millisecond timer  and it will run for 300 milliseconds. 
                 * This timer will be used in the case, if the first handshake message
                 * is getting dropped. Next 1/4 message will be sent after the interval
                 * of 300 milliseconds */
                u4Duration = ((gu4Stups * u4TimeOut) / 1000);
                if (TmrStartTimer
                    ((tTimerListId) gRsnaGlobals.u4RsnaTimerId,
                     &pRsnaTimer->Node, u4Duration) == TMR_SUCCESS)
                {
                    return (RSNA_SUCCESS);
                }
                else
                {
                    return (RSNA_FAILURE);
                }

            }
            break;
        case RSNA_PMK_REKEY_TIMER_ID:
        case RSNA_DELETEPMK_TIMER_ID:
        case RSNA_TKIP_CTR_MSR_TIMER_ID:
        case RSNA_GMK_TIMER_ID:
        case RSNA_GTK_TIMER_ID:
        case RSNA_EAP_PACKET_TIMER_ID:
            break;
        default:
            return (RSNA_FAILURE);
    }

    if (TmrStartTimer
        ((tTimerListId) gRsnaGlobals.u4RsnaTimerId, &pRsnaTimer->Node,
         (u4TimeOut * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_SUCCESS)
    {
        return (RSNA_SUCCESS);
    }
    else
    {
        return (RSNA_FAILURE);
    }
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrStopTimer                                           *
*                                                                           *
* Description  : Function invoked to stop timer                             *
*                                                                           *
* Input        : pRsnaTimer :- Points to the timer to be stopped            *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
UINT4
RsnaTmrStopTimer (tRsnaTimer * pRsnaTimer)
{
    if (RSNA_IS_TIMER_ACTIVE (pRsnaTimer) == TRUE)
    {
        if (TmrStopTimer
            ((tTimerListId) gRsnaGlobals.u4RsnaTimerId,
             &pRsnaTimer->Node) == TMR_FAILURE)
        {
            RSNA_TRC (DATA_PATH_TRC, "RsnaTmrStopTimer:TmrStopTimer failed!\n");
            return RSNA_FAILURE;
        }
        RSNA_INIT_TIMER (pRsnaTimer);
    }
    return RSNA_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcRetransTimer                                    *
*                                                                           *
* Description  : Function invoked when rsna retransmission timer expires    *
*                                                                           *
* Input        : pTmrContext :- Context that will be used for processing    *
*                the retransmission timer                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcRetransTimer (VOID *pTmrContext)
{

    tRsnaSessionNode   *pRsnaSessNode = NULL;
    pRsnaSessNode = (tRsnaSessionNode *) pTmrContext;
    pRsnaSessNode->u4RefCount--;
    pRsnaSessNode->bTimeOutEvt = TRUE;
    pRsnaSessNode->bRsnaInitStateMachine = TRUE;
    pRsnaSessNode->u4TimeOutCtr++;

    if (RsnaMainFsmStart (pRsnaSessNode) != RSNA_SUCCESS)
    {
        RSNA_TRC (RSNA_ALL_FAILURE_TRC | RSNA_CONTROL_PATH_TRC,
                  "RsnaTmrProcRetransTimer:- "
                  "RsnaMainFsmStart returned Failure !!!\n ");
        return;
    }
    return;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : RsnaTmrProcessEapPacket                                    *
 * *                                                                           *
 * * Description  : Function invoked when rsna retransmission timer expires    *
 * *                                                                           *
 * * Input        : pTmrContext :- Context that will be used for processing    *
 * *                the retransmission timer                                   *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
 * *                                                                           *
 * *****************************************************************************/

VOID
RsnaTmrProcessEapPacket (VOID *pTmrContext)
{
    tRsnaEapPacketinfo *pRsnaEapPacketinfo = NULL;
    pRsnaEapPacketinfo = (tRsnaEapPacketinfo*) pTmrContext;

    if (pRsnaEapPacketinfo->u4Retry < RSNA_EAP_MAX_RETRY_COUNT)
    {
	RsnaPnacTxFrame (pRsnaEapPacketinfo->au1Bufffer, pRsnaEapPacketinfo->u4PktLen,pRsnaEapPacketinfo->u2PortNum);

	RsnaTmrStartTimer (&(pRsnaEapPacketinfo->RsnaEapRetransmitTmr), RSNA_EAP_PACKET_TIMER_ID,
		RSNA_EAP_MAX_RETRANSMIT_TIME, pRsnaEapPacketinfo);
	pRsnaEapPacketinfo->u4Retry++;
    }
}


/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcessPMKReKeyTimer                                *
*                                                                           *
* Description  : Function invoked for processing the pmk rekey expiration   *
*                                                                           *
* Input        : pTmrContext :- Points to the timer context                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcessPMKReKeyTimer (VOID *pTmrContext)
{
    RSNA_UNUSED (pTmrContext);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcessDeletePMKTimer                               *
*                                                                           *
* Description  : Function to process delete pmk timer                       *
*                                                                           *
* Input        : pTmrContext :- Points to the timer context                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcessDeletePMKTimer (VOID *pTmrContext)
{
    tRsnaPmkSa         *pRsnaPmkSa = NULL;

    pRsnaPmkSa = (tRsnaPmkSa *) pTmrContext;

    if (pRsnaPmkSa == NULL)
    {
        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaTmrProcessDeletePMKTimer:- pRsnaPmkSa is Null !!!1 ");
        return;
    }
    RsnaUtilDeletePmkSa (pRsnaPmkSa);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcStopTkipCtrMsrTimer                             *
*                                                                           *
* Description  : Function to process tkip ctr measures tmr                  *
*                                                                           *
* Input        : pTmrContext :- Points to the timer context                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       * 
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcStopTkipCtrMsrTimer (VOID *pTmrContext)
{
    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    pRsnaPaePortEntry = (tRsnaPaePortEntry *) pTmrContext;

    if (pRsnaPaePortEntry == NULL)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaTmrProcStopTkipCtrMsrTimer:-"
                  " pRsnaPaePortEntry is NULL !!! \n");
        return;
    }
    pRsnaPaePortEntry->bTkipCtrMsrFlag = FALSE;
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcessReKeyGmk                                     *
*                                                                           *
* Description  : Function to process rekey gmk timer                        *
*                                                                           *
* Input        : pTmrContext :- Points to the timer context                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : RSNA_SUCCESS/RSNA_FAILURE                                  *
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcessReKeyGmk (VOID *pTmrContext)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    pRsnaPaePortEntry = (tRsnaPaePortEntry *) pTmrContext;

    if (pRsnaPaePortEntry == NULL)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaTmrProcessReKeyGmk:-"
                  "pRsnaPaePortEntry is NULL !!! \n");
        return;
    }

    RsnaUtilGetRandom (pRsnaPaePortEntry->RsnaGlobalGtk.au1Gmk, RSNA_GMK_LEN);

    if (pRsnaPaePortEntry->RsnaConfigDB.u4GroupGmkRekeyTime == 0)
    {
        return;
    }
    else
    {
        if (RsnaTmrStartTimer
            (&(pRsnaPaePortEntry->RsnaGlobalGtk.RsnaGmkRekeyTmr),
             RSNA_GMK_TIMER_ID,
             pRsnaPaePortEntry->RsnaConfigDB.
             u4GroupGmkRekeyTime, (VOID *) pRsnaPaePortEntry) == TMR_FAILURE)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaTmrProcessReKeyGmk:-"
                      "RsnaTmrStartTimer returned Failure !!! \n");
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : RsnaTmrProcessReKeyGtk                                     *
*                                                                           *
* Description  : Function to process rekey gtk                              *
*                                                                           *
* Input        : pTmrContext :- Points to the timer context                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
RsnaTmrProcessReKeyGtk (VOID *pTmrContext)
{

    tRsnaPaePortEntry  *pRsnaPaePortEntry = NULL;

    pRsnaPaePortEntry = (tRsnaPaePortEntry *) pTmrContext;

    if (pRsnaPaePortEntry == NULL)
    {

        RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                  "RsnaTmrProcessReKeyGtk:-"
                  "pRsnaPaePortEntry is NULL !!! \n");
        return;
    }

    pRsnaPaePortEntry->RsnaGlobalGtk.bGtkRekeyEnable = TRUE;
    pRsnaPaePortEntry->RsnaGlobalGtk.u1RekeyFlag = TRUE;

    RsnaUtilIncByteArray (pRsnaPaePortEntry->RsnaGlobalGtk.
                          au1Counter, RSNA_NONCE_LEN);
    do
    {

        pRsnaPaePortEntry->RsnaGlobalGtk.bIsStateChanged = FALSE;
        if (RsnaMainGlobalGrpKeyFsm (&(pRsnaPaePortEntry->
                                       RsnaGlobalGtk)) != RSNA_SUCCESS)
        {
            RSNA_TRC (RSNA_CONTROL_PATH_TRC | RSNA_ALL_FAILURE_TRC,
                      "RsnaTmrProcessReKeyGtk:-"
                      "RsnaMainGlobalGrpKeyFsm Failed !!!! \n");
            return;
        }
    }
    while (pRsnaPaePortEntry->RsnaGlobalGtk.bIsStateChanged == TRUE);

    if ((pRsnaPaePortEntry->RsnaConfigDB.u1GroupRekeyMethod ==
         RSNA_GROUP_REKEY_TIMEBASED)
        || (pRsnaPaePortEntry->RsnaConfigDB.
            u1GroupRekeyMethod == RSNA_GROUP_REKEY_TIMEPACKETBASED))
    {
        if (RsnaTmrStartTimer
            (&(pRsnaPaePortEntry->RsnaGlobalGtk.RsnaUtilGtkRekeyTmr),
             RSNA_GTK_TIMER_ID,
             pRsnaPaePortEntry->RsnaConfigDB.
             u4GroupGtkRekeyTime, (VOID *) pRsnaPaePortEntry) == TMR_FAILURE)
        {
            RSNA_TRC (RSNA_MGMT_TRC,
                      "RsnaTmrProcessReKeyGtk:-"
                      "RsnaTmrStartTimer returned Failure !!! \n");
            return;
        }
    }
    return;
}
