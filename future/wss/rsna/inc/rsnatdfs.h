/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnatdfs.h,v 1.3 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#ifndef _RSNATDFS_H
#define _RSNATDFS_H


  
/* This structure is used for constructing EAPOL Key Frame */

struct EAPOLKEYFRAME {
   UINT1  u1DescrType;                           /* Identifies type of 
                                                    EAPOL Key Frame. 
                                                    For RSNA the value is 2*/
   UINT2  u2KeyInfo;                             /* Specifies the 
                                                    characteristics of 
                                                    the Key */
   UINT2  u2KeyLen;                              /* Length of the Pairwise 
                                                    Temporal 
                                                    Keys */
   UINT1  au1KeyReplayCtr [RSNA_REPLAY_CTR_LEN]; /* Used to Detect the 
                                                    Replayed EAPOL
                                                    Frames */ 
   UINT1  au1KeyNonce[RSNA_NONCE_LEN];           /* To convey Nonce from 
                                                    Authenticator 
                                                    to Supplicant */
   UINT1  au1KeyIv[RSNA_KEY_IV_LEN];             /* IV Used along with KEK */ 
   UINT1  au1KeyRsc[RSNA_KEY_RSC_LEN];           /* Recvd Sequence count for 
                                                    GTK Installed */
   UINT1  u1Rsvd[RSNA_REPLAY_CTR_LEN];           /* Reserved Field */
   UINT1  au1KeyMic[RSNA_MIC_LEN];               /* Contains the MIC 
                                                    calculated over 
                                                    EAPOL Frame */
   UINT2  u2KeyDataLen;                          /* Length of the Key Data 
                                                    Field */
} __attribute__ ((packed));

typedef struct EAPOLKEYFRAME  tEapolKeyFrame;

/* Enum for Per STA 4 Way Handshake State Machine FSM */
typedef enum {
   RSNA_4WAY_INITIALIZE              = 0,
   RSNA_4WAY_DISCONNECT              = 1,
   RSNA_4WAY_DISCONNECTED            = 2,
   RSNA_4WAY_AUTHENTICATION          = 3,
   RSNA_4WAY_AUTHENTICATION2         = 4,
   RSNA_4WAY_INITPMK                 = 5,
   RSNA_4WAY_INITPSK                 = 6,
   RSNA_4WAY_PTKSTART                = 7,
   RSNA_4WAY_PTKCALCNEGOTIATING      = 8,
   RSNA_4WAY_PTKCALCNEGOTIATING2     = 9,
   RSNA_4WAY_INITNEGOTIATING         = 10,
   RSNA_4WAY_PTKINITDONE             = 11
}eFourWayHandShakeFsm;   


/* Enum for Per STA Group Key Handshake FSM */
typedef enum {
   RSNA_PER_STA_GKH_IDLE               = 0,
   RSNA_PER_STA_GKH_REKEYNEGOTIATING   = 1,
   RSNA_PER_STA_GKH_REKEYESTABLISHED   = 2,
   RSNA_PER_STA_GKH_KEYERROR           = 3
}ePerStaGrpKeyExchFsm;  



/*Macros for Global Group Key Handshake FSM */
typedef enum {
   RSNA_GLOBAL_GKH_INIT                = 0, 
   RSNA_GLOBAL_GKH_SETKEYS             = 1,
   RSNA_GLOBAL_SETKEYSDONE             = 2
}eGlobalGrpKeyExchFsm;   

typedef enum {
   RC4_KEY_TYPE   = 1,
   RSNA_KEY_TYPE  = 2, 
   WPA_KEY_TYPE   = 254
}eEapolKeyType;           


typedef enum
{

    RSNA_REASON_INVALID_IE  = 13,
    RSNA_REASON_MICHAEL_MIC_FAILURE = 14,
    RSNA_REASON_4WAY_HANDSHAKE_TIMEOUT = 15,
    RSNA_REASON_GROUP_KEY_UPDATE_TIMEOUT = 16,
    RSNA_REASON_IE_IN_4WAY_DIFFERS = 17,
    RSNA_REASON_GROUP_CIPHER_NOT_VALID = 18,
    RSNA_REASON_PAIRWISE_CIPHER_NOT_VALID = 19,
    RSNA_REASON_AKMP_NOT_VALID = 20,
    RSNA_REASON_UNSUPPORTED_RSN_IE_VERSION = 21,
    RSNA_REASON_INVALID_RSN_IE_CAPAB = 22,
    RSNA_REASON_IEEE_802_1X_AUTH_FAILED = 23,
    RSNA_REASON_CIPHER_SUITE_REJECTED = 24
}eRsnaReasonCode;

typedef enum {
    PAIRWISE_2  = 0, 
    PAIRWISE_4  = 1, 
    GROUP_2     = 2, 
    REQUEST     = 3,
}eExch;

typedef UINT1         tSsid[WLAN_MAX_SSID_LEN + 4];

typedef UINT1         tBssid[8];
typedef UINT1         tStaMac[8];

typedef struct RSNAELEMS {
    UINT1 *pu1IE;
    tPmkId aPmkidList[RSNA_MAX_PMKID_IDS];
    UINT1 au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN];
    UINT1 au1AKMSuite[RSNA_CIPHER_SUITE_LEN];
    UINT1 au1GroupCipherSuite[RSNA_CIPHER_SUITE_LEN];
    UINT2 u2RsnCapab;
    UINT2 u2PairwiseCipherSuiteCount;
    UINT2 u2AKMSuiteCount;
    UINT2 u2PmkidCount;
    UINT2 u2Ver;
    UINT1 u1ElemId;
    UINT1 u1Len;
}tRsnaElements;

/* This structure represents a PMK Node in the PMK Cache */
typedef struct RSNAPMKSA {
   tRBNodeEmbd   RsnaPmksaNode;
   tRsnaTimer    PmkSaTimer;                    /* PMK Rekey Timer*/
   tRsnaTimer    PmkSaDeleteTmr;                /* PMK Delete Timer */
   UINT1         au1Akm[RSNA_CIPHER_SUITE_LEN]; /* Refers to AKM Suite */
   UINT1         au1PmkId[RSNA_PMKID_LEN];      /* Id of the PMK in Use */
   UINT1         au1SuppMac[RSNA_ALIGNED_ETH_ADDR_LEN];  /* Station's Mac 
                                                            Address */
   UINT1         au1SrcMac[RSNA_ALIGNED_ETH_ADDR_LEN];  /* Destined AP's 
                                                           Wlan If Mac */
   UINT1         au1Pmk[RSNA_PMK_LEN];          /* Refers to the PMK */
   UINT4         u4IfIndex;                     /* The IfIndex on which the 
                                                   Pre-Auth Frames Recvd */
}tRsnaPmkSa;

typedef struct RSNAPTKSA {
   UINT1        au1Kck [RSNA_KCK_KEY_LEN];           /* Key used for 
                                                        genetrating MIC 
                                                        for eapol key frame */
   UINT1        au1Kek [RSNA_KEK_KEY_LEN];           /* Key used for 
                                                        encrypting the 
                                                        data field of 
                                                        EAPOL Key Frame */
   UINT1        au1Ptk [RSNA_MAX_PTK_KEY_LEN];       /* PTK Derived at the 
                                                        end of 4-Way 
                                                        Handshake */
} tRsnaPtkSa;

typedef struct RSNAGLOBALGTK {
   tRsnaTimer RsnaUtilGtkRekeyTmr;
   tRsnaTimer RsnaGmkRekeyTmr;
   struct  RSNAPAEPORTENTRY *pRsnaPaePortInfo; /* Port Specific Information */ 
   UINT1   au1Counter[RSNA_NONCE_LEN];   /* Refers to the Global Counter */ 
   UINT1   au1Gmk[RSNA_GMK_LEN];         /* Refers to the GMK */ 
   UINT1   au1Gtk[2][RSNA_GTK_MAX_LEN];  /* Refers to the GTK */
   UINT1   au1Gnonce[RSNA_NONCE_LEN];    /* GNonce generated by the 
                                            Authenticator */
   UINT4   u4GNoStations;                /* Count of stations for which 
                                            GTK to be sent */
   UINT4   u4GkeyDoneStations;           /* Count of stations for which 
                                            GTK to be updated */
   BOOL1 bGInit;                       /* Initializes the Group Key 
                                            State Machine */
   BOOL1 bGtkRekeyEnable;              /* When set to true ReKey for 
                                            Group Key Exch 
                                            done */
   BOOL1   bRsnaEnabled;
   BOOL1   bWpaEnabled;
   UINT1   u1Fsm;                        /* Current State in the FSM */
   UINT1   u1GN;                         /* Key Index for GTK */
   UINT1   u1GM;                         /* Key Index for GTK */ 
   BOOL1 bIsStateChanged;              /* Has the Session State Changed */ 
   BOOL1 bGTKAuthenticator;            /* Flag set to true if AP 
                                            acts as an Authenticator */
   BOOL1   bKeyDeleteFlag;
   UINT1   u1RekeyFlag;
   UINT1   au1Pad[1];
}tRsnaGlobalGtk;


#ifdef PMF_WANTED
typedef struct RSNAGLOBALIGTK {
  struct  RSNAPAEPORTENTRY *pRsnaPaePortInfo; /* Port Specific Information */ 
  UINT1   au1IGtk[2] [RSNA_IGTK_MAX_LEN]; /* Refers to IGTK */
  UINT1   u1IGN;                         /* Key Index for IGTK */
  UINT1   u1IGM;                         /* Key Index for IGTK */ 
  BOOL1   bKeyDeleteFlag;                /* Flag set to TRUE to delete the IGTK Key */
  UINT1   u1RekeyFlag;                   /* Flag set to TRUE when IGTK Rekey is enabled */
}tRsnaGlobalIGtk;
#endif

typedef struct RSNACONFIGDB {

   UINT1   au1PskPassPhrase[RSNA_ALIGNED_PASS_PHRASE_LEN];  /* Passphrase for
                                                              PSK when Auth
                                                        Suite is AKM */
   UINT1   au1ConfigPsk[RSNA_ALIGNED_PSK_LEN];       /* Configured PSK when
                                                        Auth Suite
                                                        is AKM */
   UINT1   au1GroupCipher[RSNA_CIPHER_SUITE_LEN];   /* Configured Group
                                                        Cipher */

   UINT1   au1PmkIdUsed[RSNA_PMKID_LEN];             /* Last Selected PMKID */
   UINT1   au1AuthSuiteSelected[RSNA_CIPHER_SUITE_LEN]; /* Last Selected Auth
                                                        Suite */
   UINT1   au1PairwiseCipherSelected[RSNA_CIPHER_SUITE_LEN];/* Last Selected
                                                        Pairwise Cipher */
   UINT1   au1GroupCipherSelected[RSNA_CIPHER_SUITE_LEN];/* Last selected Group
                                                        Cipher */
   UINT1   au1AuthSuiteRequested[RSNA_CIPHER_SUITE_LEN];/* Last AKM Suite
                                                        Requested */
   UINT1   au1PairwiseCipherRequested[RSNA_CIPHER_SUITE_LEN];/* Last Pairwise 
                                                        Cipher
                                                        Requested */
   UINT1   au1GroupCipherRequested[RSNA_CIPHER_SUITE_LEN];/* Last Group Cipher
                                                        Requested */
   UINT4   u4NoOfPtkSaReplayCtr;                     /* No of PTKSA Replay
                                                        Ctrs per
                                                        association */
   UINT4   u4NoOf4WayHandshakeFailures;              /* Count of 4-Way
                                                        Handshake
                                                        Failures */
   UINT4   u4NoOfGtkReplayCtrs;                      /* Count of GTKSA
                                                        Replay Ctrs
                                                        per assoc */
   UINT4   u4Version;                                /* The highest RSNA
                                                        Version
                                                        supported by the
                                                        system */
   UINT4   u4PairwiseKeysSupported;                  /* No of Pairwise Keys
                                                        supported */
   UINT4   u4GroupGtkRekeyTime;                      /* Time in seconds after
                                                        which GTK
                                                        will be refreshed */
   UINT4   u4GroupGmkRekeyTime;                      /* Time in seconds after
                                                        which GMK
                                                        will be refreshed */
   
   UINT4   u4GroupRekeyPkts;                         /* Count of the pkts
                                                        after which GTK
                                                        will be refreshed */
   UINT4   u4GroupUpdateCount;                       /* No of retries for
                                                        Msg 1 of Group
                                                        Key Handhshake */
   UINT4   u4PairwiseUpdateCount;                    /* No of retries for
                                                        Msg 1 and Msg 4
                                                        of 4-wayHandshake */
   UINT4   u4GroupCipherKeyLen;                      /* Length of the Group
                                                        Cipher Key */
   UINT4   u4PMKLifeTime;                            /* Lifetime of PMK in
                                                        the PMK Cache */
   UINT4   u4PmkReAuthThreshold;                     /* Percent of PMK
                                                        Lifetime expiry
                                                        after which
                                                        IEEE802.1X reauth
                                                        occurs */
   UINT4   u4SATimeOut;                              /* Max time a secassoc
                                                        wil take to
                                                        set up */
   UINT4   u4NoOfCtrMsrsInvoked;                     /* Count of TKIP Measures
                                                        Invoked */
   
   UINT1  u1RsnaConfigRowStatus;                     /* Row Status */
   UINT1   u1GroupRekeyMethod;                       /* Mechanism for
                                                        rekeying RSNA
                                                        GTK */
   BOOL1 bPskStatus;                                 /* Psk Status */

   BOOL1 bGroupRekeyStrict;                          /* When set GTK will
                                                        be refreshed when
                                                        an STA leaves the
                                                        ESS */
   BOOL1 bRSNAOptionImplemented;                   /* Indicates if the
                                                        entity
                                                        is rsna capable */
   BOOL1 bRSNAPreauthenticationImplemented;        /* Indicates if entity
                                                        supports rsna
                                                        preauthentication */

   UINT1 u1RsnaOkcStatus;                            /* Indicates whether the
                                                      opportunistic key caching
                                                      is enabled or not*/
   UINT1  u1AKMSuiteMethod;                           
}tRsnaConfigDB;
#ifdef WPA_WANTED
typedef struct
{
  UINT4   u4NoOfPtkSaReplayCtr;                     /* No of PTKSA Replay
                                                        Ctrs per
                                                        association */
   UINT4   u4NoOf4WayHandshakeFailures;              /* Count of 4-Way
                                                        Handshake
                                                        Failures */
   UINT4   u4NoOfGtkReplayCtrs;                      /* Count of GTKSA
                                                        Replay Ctrs
                                                        per assoc */
   UINT4   u4Version;                                /* The highest RSNA
                                                        Version
                                                        supported by the
                                                        system */
   UINT4   u4PairwiseKeysSupported;                  /* No of Pairwise Keys
                                                        supported */
   UINT4   u4GroupGtkRekeyTime;                      /* Time in seconds after
                                                        which GTK
                                                        will be refreshed */
   UINT4   u4GroupRekeyPkts;                         /* Count of the pkts
                                                        after which GTK
                                                        will be refreshed */
   UINT4   u4GroupUpdateCount;                       /* No of retries for
                                                        Msg 1 of Group
                                                        Key Handhshake */
   UINT4   u4PairwiseUpdateCount;                    /* No of retries for
                                                        Msg 1 and Msg 4
                                                        of 4-wayHandshake */
   UINT4   u4GroupCipherKeyLen;                      /* Length of the Group
                                                        Cipher Key */
   UINT4   u4PMKLifeTime;                            /* Lifetime of PMK in
                                                        the PMK Cache */
   UINT4   u4PmkReAuthThreshold;                     /* Percent of PMK
                                                        Lifetime expiry
                                                        after which
                                                        IEEE802.1X reauth
                                                        occurs */
   UINT4   u4SATimeOut;                              /* Max time a secassoc
                                                        wil take to
                                                        set up */
   INT1   au1PskPassPhrase[RSNA_ALIGNED_PASS_PHRASE_LEN];  /* Passphrase for
                                                              PSK when Auth Suite is AKM */
   UINT1   au1ConfigPsk[RSNA_ALIGNED_PSK_LEN];       /* Configured PSK when
                                                        Auth Suite
                                                        is AKM */
   UINT1   au1GroupCipher[RSNA_CIPHER_SUITE_LEN];   /* Configured Group
                                                        Cipher */
   UINT1   au1AuthSuiteSelected[RSNA_CIPHER_SUITE_LEN]; /* Last Selected Auth
                                                        Suite */
   UINT1   au1PairwiseCipherSelected[RSNA_CIPHER_SUITE_LEN];/* Last Selected
                                                        Pairwise Cipher */
   UINT1   au1GroupCipherSelected[RSNA_CIPHER_SUITE_LEN];/* Last selected Group
                                                        Cipher */
   UINT1   au1AuthSuiteRequested[RSNA_CIPHER_SUITE_LEN];/* Last AKM Suite
                                                        Requested */
   UINT1   au1PairwiseCipherRequested[RSNA_CIPHER_SUITE_LEN];/* Last Pairwise 
                                                        Cipher
                                                        Requested */
   UINT1   au1GroupCipherRequested[RSNA_CIPHER_SUITE_LEN];/* Last Group Cipher
                                                        Requested */

   UINT1  u1RsnaConfigRowStatus;                     /* Row Status */
   UINT1   u1GroupRekeyMethod;                       /* Mechanism for
                                                        rekeying RSNA
                                                        GTK */
   BOOL1 bPskStatus;                                 /* Psk Status */

   BOOL1 bGroupRekeyStrict;                          /* When set GTK will
                                                        be refreshed when
                                                        an STA leaves the
                                                        ESS */
   UINT1  u1AKMSuiteMethod;                           
   UINT1 u1RsnaOkcStatus;                            
   BOOL1 bRSNAOptionImplemented;                   /* Indicates if the
                                                        entity
                                                        is rsna capable */
   BOOL1 bWpaMixMode;    /*Indicates weather mix mode is enabled or not*/
}tWpaConfigDB;
#endif

typedef struct RSNAPWCIPHERDB {
   UINT1 au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN];    /* Refere to pairwise
                                                          cipher suite */
   UINT4  u4Index;                                    /* Index of pairwise
                                                         Cipher table */
   UINT4  u4PairwiseCipherSize;                       /* 256 for TKIP and
                                                         128 for CCMP */
   BOOL1 bPairwiseCipherEnabled;                    /* Indicates if the
                                                         suite is enabled
                                                         or disabled */
   UINT1  u1PairwiseCipherRowStatus;                  /* Pairwise Cipher
                                                         table Row Status */
   UINT1  au1Pad[2];                                  /* Padding */
}tRsnaPwCipherDB;

#ifdef WPA_WANTED
typedef struct WPAPWCIPHERDB {
   UINT4  u4Index;                                    /* Index of pairwise
                                                         Cipher table */
   UINT4  u4PairwiseCipherSize;                       /* 256 for TKIP and
                                                         128 for CCMP */
   UINT1 au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN];    /* Refere to pairwise
                                                          cipher suite */
   BOOL1 bPairwiseCipherEnabled;                    /* Indicates if the
                                                         suite is enabled
                                                         or disabled */
   UINT1  u1PairwiseCipherRowStatus;                  /* Pairwise Cipher
                                                         table Row Status */
   UINT1  au1Pad[2];                                  /* Padding */
}tWpaPwCipherDB;

#endif
typedef struct RSNAAKMDB {
   UINT1   au1AuthSuite[RSNA_AKM_SELECTOR_LEN];    /* Refers to AKM Suite */
   UINT4   u4Index;                                /* Refers to the Index of
                                                      AKM DB */
   BOOL1   bAkmSuiteEnabled;                       /* Indicates if this suite
                                                      is enabled or disabled */
   UINT1   u1AuthSuiteRowStatus;                   /* Auth Suite table row
                                                      status */
   UINT1   au1Pad[2];                              /* Padding */
}tRsnaAkmDB;


typedef struct WPAAKMDB {
   UINT1   au1AuthSuite[RSNA_AKM_SELECTOR_LEN];    /* Refers to AKM Suite */
   UINT4   u4Index;                                /* Refers to the Index of
                                                      AKM DB */
   BOOL1   bAkmSuiteEnabled;                       /* Indicates if this suite
                                                      is enabled or disabled */
   UINT1   u1AuthSuiteRowStatus;                   /* Auth Suite table row
                                                      status */
   UINT1   au1Pad[2];                              /* Padding */
}tWpaAkmDB;

typedef struct  RSNASTATSDB {
   UINT4  u4Version;                               /* Version of RSNA */
   UINT4  u4TkipICVErrors;                         /* No of Icv errors */
   UINT4  u4TkipLocalMicFailures;                  /* Count of Mic Failures 
                                                      for frames
                                                      recvd from supplicant */ 
   UINT4  u4TkipRemoteMicFailures;                 /* Count of Mic Failures 
                                                      for eapol frames send 
                                                      to station */
   UINT4  u4CCMReplays;                            /* Count of MPDUs 
                                                      discarded because 
                                                      of replay attack */
   UINT4  u4CCMDecryptErrors;                      /* Count of MPDUs 
                                                      discarded by 
                                                      decryption */
   UINT4  u4TkipReplays;                           /* Count of TKIP Replay 
                                                      errors */
}tRsnaStatsDB;


typedef struct RSNAPSK {
   tTMO_SLL_NODE  Next;
   UINT1 *pu1PskPassPhrase;                       /* Pointer to passphrase
                                                     if the PSK is
                                                     passphrase */
   UINT1  au1SuppMacAddr [RSNA_ALIGNED_ETH_ADDR_LEN]; /* Station's Mac 
 Address */
   UINT1  au1Psk[RSNA_PSK_LEN];                   /* The PSK for the given
                                                     station */
   UINT4 u4IfIndex;                               /* Refers to wlan IfIndex */
}tRsnaPskDB;

typedef struct APMERSNACONFIGDB {
   UINT1   au1PskPassPhrase[RSNA_ALIGNED_PASS_PHRASE_LEN];  /* Passphrase                                                                                 for 
                                                              PSK when Auth
                                                        Suite is AKM */
   UINT1   au1ConfigPsk[RSNA_ALIGNED_PSK_LEN];       /* Configured PSK when
                                                        Auth Suite
                                                        is AKM */
   UINT1   au1GroupCipher[RSNA_CIPHER_SUITE_LEN];   /* Configured Group
                                                        Cipher */
   UINT4   u4Version;                                /* The highest RSNA
                                                        Version
                                                        supported by the
                                                        system */
   UINT4   u4PairwiseKeysSupported;                  /* No of Pairwise Keys
                                                        supported */
   UINT4   u4GroupGtkRekeyTime;                      /* Time in seconds after
                                                        which GTK
                                                        will be refreshed */
   UINT4   u4GroupGmkRekeyTime;                      /* Time in seconds after
                                                        which GMK
                                                        will be refreshed */
   UINT4   u4GroupRekeyPkts;                         /* Count of the pkts
                                                        after which GTK
                                                        will be refreshed */
   UINT4   u4GroupUpdateCount;                       /* No of retries for
                                                        Msg 1 of Group
                                                        Key Handhshake */
   UINT4   u4PairwiseUpdateCount;                    /* No of retries for
                                                        Msg 1 and Msg 4
                                                        of 4-wayHandshake */
   UINT4   u4GroupCipherKeyLen;                      /* Length of the Group
                                                        Cipher Key */
   UINT4   u4PMKLifeTime;                            /* Lifetime of PMK in
                                                        the PMK Cache */
   UINT4   u4PmkReAuthThreshold;                     /* Percent of PMK
                                                        Lifetime expiry
                                                        after which
                                                        IEEE802.1X reauth
                                                        occurs */
   UINT4   u4SATimeOut;                              /* Max time a secassoc
                                                        wil take to 
                                                        set up */
   UINT4   u4NoOfCtrMsrsInvoked;                     /* Count of TKIP Measures
       Invoked */
   UINT1   u1GroupRekeyMethod;                       /* Mechanism for
                                                        rekeying RSNA
                                                        GTK */
   BOOL1 bPskStatus;                                 /* Psk Status */
   BOOL1 bGroupRekeyStrict;                          /* When set GTK will
                                                        be refreshed when
                                                        an STA leaves the
                                                        ESS */
   BOOL1 bRSNAOptionImplemented;                   /* Indicates if the
                                                        entity
                                                        is rsna capable */
   BOOL1 bRSNAPreauthenticationImplemented;        /* Indicates if entity 
                                                        supports rsna 
                                                        preauthentication */
   UINT1  au1Pad[3];                                    /* Padding */
}tApmeRsnaConfigDB;

typedef struct APEMRSNAPWCIPHERDB {
   UINT1 au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN];    /* Refere to pairwise 
                                                          cipher suite */
   UINT4  u4Index;                                    /* Index of pairwise 
                                                         Cipher table */
   UINT4  u4PairwiseCipherSize;                       /* 256 for TKIP and 
                                                         128 for CCMP */
   BOOL1 bPairwiseCipherEnabled;                    /* Indicates if the 
                                                         suite is enabled 
                                                         or disabled */
   UINT1  u1PairwiseCipherRowStatus;                  /* Pairwise Cipher 
                                                         table Row Status */
   UINT1  au1Pad[2];                                  /* Padding */
}tApmeRsnaPwCipherDB;


typedef struct APMERSNAAKMDB {
   UINT1   au1AuthSuite[RSNA_AKM_SELECTOR_LEN];    /* Refers to AKM Suite */
   UINT4   u4Index;                                /* Refers to the Index of 
                                                      AKM DB */
   BOOL1   bAkmSuiteEnabled;                       /* Indicates if this suite 
                                                      is enabled or disabled */
   UINT1   u1AuthSuiteRowStatus;                   /* Auth Suite table row 
                                                      status */ 
   UINT1   au1Pad[2];                              /* Padding */
}tApmeRsnaAkmDB;

typedef struct APMERSNACONFMSG {
   UINT1                     au1RsnaIE[RSNA_ALIGNED_IE_LEN];  /* Rsna IE Used in AP's
                                              Beacons/Probe
                                              Response */
   tApmeRsnaConfigDB         apmeRsnaConfigDB;   /* Rsna Config DB */
   tApmeRsnaPwCipherDB       aApmeRsnaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];
   tApmeRsnaAkmDB            aApmeRsnaAkmDB[RSNA_AKM_SUITE_COUNT]; /* AKM Suite DB */
   UINT1                     u1IELen;
   UINT1                     au1Pad[3];
}tApmeRsnaConfInfo;


typedef struct  RSNAPAEPORTENTRY {
   tRBNodeEmbd RsnaPortEntryNode;
   tPnacPaePortEntry *pPnacPaePortEntry;   /* Pointes to Pnac Port Entry */
   tTMO_SLL  RsnaPskList;                  /* PSK List */    
   tTMO_SLL  RsnaSupInfoList;              /* Station Info List */
   UINT1  au1SrcMac[RSNA_ALIGNED_ETH_ADDR_LEN];    /* Mac Address of the 
                                          Interface */
   tApmeRsnaConfInfo apmeRsnaConfInfo;     /* Rsna ConfigInfo from Apme */
   tRsnaTimer RsnaCtrMsrTmr;                /* Tkip Ctr Msrs Timer */
   tRsnaConfigDB  RsnaConfigDB;            /* Database configured as per
                                              dot11RSNAConfig Table */
   #ifdef WPA_WANTED
   tWpaConfigDB   WpaConfigDB;      /*DB Configured for WPA*/
   tWpaAkmDB      aWpaAkmDB[RSNA_AKM_SUITE_COUNT];
   tWpaPwCipherDB aWpaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];
   #endif
   tRsnaPwCipherDB aRsnaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];/*Database
                                           configured as per Pairwise
                                                              Cipher */
#ifdef PMF_WANTED
   tRsnaGlobalIGtk  RsnaGlobalIGtk;          /*Rferes to the Global IGTK 
                                              Database */
#endif
   tRsnaGlobalGtk RsnaGlobalGtk;            /*Refers to the Global GTK database */
 
   tRsnaAkmDB      aRsnaAkmDB[RSNA_AKM_SUITE_COUNT];
   UINT4   u4NoOfCtrMsrsInvoked;           /* Count of TKIP Measures
                                              Invoked */
   UINT4  u4MichaelMicFailureTimeStamp;    /* Time Stamp to record Michael 
                                              Mic Failure */
   UINT4  u4MichaleMicFailurecount;        /* Counter for Michale Mic 
                                              Failures */
    UINT4  u4FourWayHandshakeCounter;         /*4way handshake completed counter*/
    UINT4  u4GroupHandshakeCounter;           /*Group handshake completed counter*/

   UINT4   u4PairwiseCipherMask;            /* Mask Indicating the no of
                                              PairwiseCipher Suites 
                                              Supported */
   UINT4   u4AkmSuiteMask;                 /* MaskIndicating the no 
                                              of AKm Suites Supported */
   UINT2   u2Port;                         /* The Port Info */
   BOOL1 bTkipCtrMsrFlag;                  /* Flag will be set when 
                                              tkip counter
                                              measures are invoked. 
                                              Will be unset after 60 seconds 
                                              of TKIp Ctr Measures
                                              Invoked */
   BOOL1   bGtkRefreshFlag;                  /*  GTK Refresh flag*/ 
   BOOL1   bGtkInstallFlag;                  /*  GTK Install flag*/ 
   UINT1   au1RsnaIE[RSNA_ALIGNED_IE_LEN];  /* Rsna IE Used in AP's
                                              Beacons/Probe
                                              Response */
   UINT1   u1IELen;
#ifdef PMF_WANTED
   BOOL1   bRsnaPMFEnabled;                 /* Boolean Indicating whether 
                                               RSNA Protected Management Frames
                                               is Enabled or Not */      

   BOOL1   bRsnaUPMFEnabled;                /* Boolean Indicating whether RSNA 
                                               UnProtected Management Frames is 
                                               Enabled or Not */

   UINT4   u4AssocSAQueryMaxTime;            /* Max Time Units (TUs) that an AP can wait 
                                               from the first SA-Query request without
                                               starting additional SA Query Procedure
                                               if sucessful SA Query Response in not
                                               received*/

   UINT4   u4AssocSAQueryRetryTime;          /* Number of Time Units (TUs) that an
                                               AP can wait between two SA-Query
                                               Request Procedure.*/

   UINT4   u4AssocComeBackTime;              /* Number of Time Units (TUs) that a
                                                STA should wait before sending the 
                                                Association Request again.  */
#else
   UINT1   au1Pad[2];
#endif
}tRsnaPaePortEntry;



typedef struct RSNASUPINFO {
   tTMO_SLL_NODE   NextNode;
   struct RSNASESSIONNODE  *pRsnaSessNode;               /* Pointer to session
                                                            Node */
   tRsnaStatsDB    RsnaStatsDB;                          /* Stats DB as per
                                                            dot11RSNAStats
                                                            Table */
   UINT4           u4SuppState;                          /* Indiactes the
                                                            state of
                                                            the Supplicant
                                                            It can be Assoc 
                                                            or Authorized */
   UINT4           u4ReasonCode;                         /* Reason for
                                                            Deauth */
   UINT1           au1RsnaIE[RSNA_ALIGNED_IE_LEN];       /* Rsna IKE Recvd in
                                                            Assoc
                                                            or ReAssoc
                                                            Request */
   UINT1           au1SuppMacAddr[RSNA_ALIGNED_ETH_ADDR_LEN]; /* Station's Mac
                                                             Address */
   UINT1           au1ReqReplayCtr[RSNA_REPLAY_CTR_LEN]; /* Replay Ctr for
                                                            Req Pkt recvd */
   UINT1           au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN]; /* The Pairwise
                                                            Cipher to be
                                                            used */
   UINT1           au1AkmSuite[RSNA_CIPHER_SUITE_LEN];   /* The selected AKM
                                                            Suite */
   UINT1           u1IELen;                              /* IE Length */
   BOOL1           bReqReplayCtrValid;                   /* Is Request Replay
                                                            Ctr Valid */
#ifdef PMF_WANTED
   UINT2           u2TransactionIdentifier;               /* SA Query Transaction Identifier */
  
   BOOL1           bSAQueryConfirm ;                      /* Flag to determine whether the corresponding 
                                                             SA Query Response for SA Query Request received */

   BOOL1           bPMFCapable;                           /* Flag to determine Whether the STA is PMF Capable */
#endif
   UINT1           au1Pad[2];

}tRsnaSupInfo;

typedef struct RSNASUPPSTATSINFO {

   tRBNodeEmbd     RsnaSuppStatsNode;
  
   tRsnaStatsDB    RsnaStatsDB;                          /* Stats DB as per 
                                                            dot11RSNAStats
                                                            Table */
   UINT4           u4StatsIndex;                         /* An auxiliary index to the
                                                            dot11RSNAStatsTable.*/
   UINT1           au1SuppMacAddr[RSNA_ALIGNED_ETH_ADDR_LEN]; /* Station's Mac
                                                                 Address */
   UINT1           au1PairwiseCipher[RSNA_CIPHER_SUITE_LEN]; /*Pairwise Cipher 
                                                                 Selected*/
   UINT2           u2Port;                                  /* The Port Info */

   UINT1           au1Pad[2];
}tRsnaSuppStatsInfo;

typedef struct RSNASESSIONNODE {
   tRsnaPtkSa  PtkSaDB;                         /* The result of 
                                                   4-Wayhandshake */
   tRsnaTimer RsnaReTransTmr;                   /* ReTransmission Timer 
                                                   for Eapol Frames */ 
   tPnacAuthSessionNode *pPnacAuthSessionNode;  /* Refers to Pnac Auth 
                                                   Session Node */
   tRsnaSupInfo         *pRsnaSupInfo;          /* Supplicant Information */
   tRsnaPaePortEntry    *pRsnaPaePortInfo;      /* Information specific to 
                                                   the port */
   UINT1 *pu1RxEapolKeyPkt;                     /* Pointer to Last Recvd 
                                                   Eapol Frame */
   UINT1 au1PmkIdKde[RSNA_PMKID_ALLIGNED_KDE_LEN]; /* PMKID KDE Format */   
   UINT1 au1ANonce[RSNA_NONCE_LEN];             /* Nonce generated by 
                                                   Authenticator */
   UINT1 au1SNonce[RSNA_NONCE_LEN];             /* Nonce Recvd from the 
                                                   Supplicant */
   UINT1 au1RsnaKeyReplayCtr[RSNA_REPLAY_COUNTER_LEN]; /* Replay Counter */
   UINT4 u4TimeOutCtr;                          /* Count of EAPOL-Key receive 
                                                   timeouts
                                                   for 4-wayHandshake */ 
   UINT4 u4GTimeoutCtr;                         /* Count of EAPOL-Key 
                                                   receive timeouts 
                                                   for Group Key Handshake */
   UINT4 u4RefCount;                            /* if >0 indicates it is 
                                                   in use */
   UINT4 u4ReasonCode;                          /* Reason for Disassoc or 
                                                   Deauth */ 
   UINT4 u4BssIfIndex;                          /* Refers to Binding Interface 
                                                    */ 
   UINT2 u2Port;                                /* Refers to the Port 
                                                   Number */
   UINT2 u2PktLen;                              /* Length of the Recvd 
                                                   Packet */
   UINT1 u1PtkFsmState;                         /* Current State of 
                                                   4-Wayhandshake Sem */
   UINT1 u1PerStaGtkFsm;                        /* Current State of GTK 
                                                   Sem Per STA */
   BOOL1 bInit;                               /* Used to initialize 
                                                   the FSM */
   BOOL1 bDeAuthReq;                          /* Flag ser to true when 
                                                   DeAuthReq 
                                                   Message Recvd */
   BOOL1 bAuthenticationRequest;              /* Flag set to true 
                                                   when Authreq 
                                                   Message Recvd */
   BOOL1 bReAutenticationRequest;             /* Flag set to true when 
                                                   ReAuthReq
                                                   Msg Recvd */
   BOOL1 bDisconnect;                         /* Flag set to true when 
                                                   Disconnect Msg Recd */
   BOOL1 bTimeOutEvt;                         /* Set to true when 
                                                   ReTransmission timer
                                                   expired */
   BOOL1 bEAPOLKeyReceived;                   /* Set to true when EAPOL 
                                                   Key Frame is recvd */
   BOOL1 bEAPOLKeyPairwise;                   /* When set to true indicates 
                                                   4-WayHandshake */
   BOOL1 bEAPOLKeyRequest;                    /* Set to true when request 
                                                   from STA to
                                                   start 4-WayHandshake */   
   BOOL1 bMICVerified;                        /* Set to true when MIC is 
                                                   verified */ 
   BOOL1 bGUpdateStationKeys;                 /* Flag set to true when 
                                                   GTK is avialable to be 
                                                   sent to Stations */
   BOOL1 bPtkValid;                           /* Flag which indicates 
                                                   whether the
                                                   PTK is valid */
   BOOL1 bPair;                               /* Set to true if the
                                                   session is
                                                   4-Way Handshake Exch */
   BOOL1 bPTKRequest;                         /* Flag set to true when 
                                                   station 
                                                   request 4-Way Handshake */
   BOOL1 bDeleteSession;                      /* Session will be deleted 
                                                   when this
                                                   Flag is set to True */   
   BOOL1 bIsKeyReplayCtrValid;                /* This will indicate 
                                                   whether the
                                                   key replay ctr can be 
                                                   reused or
                                                   not */
   BOOL1 bIsStateChanged;                      /* Has the Session State 
                                                    Changed */ 
   BOOL1 bIsPairwiseSet;                       /* Flag indicates if the 
                                                    Pairwise keys are 
                                                    configured */
   BOOL1 bPInitAkeys;                          /* Used For WAP Only */
   BOOL1 bRsnaInitStateMachine;                /* Used to trigger the state
                                                  machine */
   BOOL1 bRsnaIsSessionInitiate;               /* Used for RSNA session initiate*/
   UINT1 u1ElemId;                             /* To identify if this
                                                  session is wpa/wpa2 */
   UINT1 au1PmkId[RSNA_PMKID_LEN];             /* PMKID to be sent in the Message 1/4*/
   UINT1 au1BssMac[RSNA_ETH_ADDR_LEN];         /* AP's MAC through which Assoc/ReAssoc
                                                  is received */
   UINT1 au1pad[2];
   UINT1 au1Pmk[RSNA_PMK_LEN];                 /* PMK Used for this session*/
  
   /* PMF_WANTED */ 
#ifdef PMF_WANTED
   tRsnaIGTKSeqInfo RsnaIgtkSeqNum[2];          /* IGTK info */
#endif

}tRsnaSessionNode;


typedef struct  RSNAGLOBALS {
   tMemPoolId RsnaPortInfoPoolId;
   tMemPoolId RsnaSessNodePoolId;
   tMemPoolId RsnaPmkSaNodePoolId;
   tMemPoolId RsnaSuppInfoPoolId;
   tMemPoolId RsnaSuppStatsInfoPoolId;
   tMemPoolId RsnaWssInfoPoolId;
   tMemPoolId RsnaEapPacketinfoPoolId;
   tRBTree RsnaPortEntryTable;
   tRBTree RsnaPmkTable;                   /* PMK List */
   tRBTree RsnaSuppStatsTable;             /* Supplicant Stats table */
   tRBTree RsnaEapPacketTable;
   UINT4 u4RsnaTrcOpt;
   tRsnaAkmDB      aRsnaAkmDB[RSNA_AKM_SUITE_COUNT];
   tOsixTaskId    gRsnaTaskId;
   tOsixSemId     gRsnaSemId;
   tOsixQId       gRsnaTaskQId; 
   UINT4 u4RsnaTimerId;
   UINT4 u4RsnaStatsIndex;
}tRsnaGlobals;

typedef struct MLME_EAPOLSEND  {
   tBssid   apBssid;
   /* changed tMacAddr to tStaMac */
   tStaMac staMac;
   UINT4    u4BufLen;
   UINT1    au1RsnaEapol[RSNA_MAX_ENET_FRAME_SIZE];
   BOOL1    bEncrypt;
   UINT1    au1pad[3];
}tRsnaToApmeEapol;

typedef struct RSNAIE{
   UINT1 u1RsnaElementId;
   UINT1 u1Len;
   UINT2 u2Version;
}tRsnaIE;   

typedef struct WPAIE {
   UINT1 u1WpaElementId;
   UINT1 u1Len;
   UINT1 au1OUI[4];
   UINT2 u2Version;
}tWpaIE;   
 
typedef struct RSNAKDEFLAGS {
   BOOL1 bGtk;                              /* Is GTK to be encapsulated */  
   BOOL1 bIE;                               /* Is IE to be encapsultaed */
   BOOL1 bPmkIdKde;                         /* Is PMKID to be encapsullated */ 
   BOOL1 bIGtk;                             /* Is IGTK to be encapsulated*/
}tRsnaKdeFlags;   
   
#endif /* _RSNAHDRS_H */

  
