/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnatmr.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#ifndef _RSNATMR_H
#define _RSNATMR_H

typedef enum { 
    RSNA_MIN_TIMER_ID           =   0,
    RSNA_RETRANS_TIMER_ID       =   1,
    RSNA_PMK_REKEY_TIMER_ID     =   2,
    RSNA_DELETEPMK_TIMER_ID     =   3,
    RSNA_TKIP_CTR_MSR_TIMER_ID  =   4,
    RSNA_GMK_TIMER_ID           =   5,
    RSNA_GTK_TIMER_ID           =   6,
    RSNA_EAP_PACKET_TIMER_ID    =   7,
    RSNA_MAX_TIMER_ID           =   8
} eRsnaTimerId;


typedef struct  RSNATIMER {
     tTmrAppTimer Node;
     VOID *pContext;              /* Refers to the context to which 
                                       timer is attached */
     UINT1  u1TimerId;              /* Identification of Timer */
     UINT1 u1Status;                /* Validity of the structure 
                                       stored */
     UINT2 u2Pad;                   /* Padding for 4 Byte allignment */
}tRsnaTimer;



#define RSNA_TIMER_ACTIVE     1
#define RSNA_TIMER_INACTIVE   0

#define RSNA_INIT_TIMER(pRsnaTimer)      \
    ((pRsnaTimer)->u1Status = RSNA_TIMER_INACTIVE)
#define RSNA_ACTIVATE_TIMER(pRsnaTimer)      \
    ((pRsnaTimer)->u1Status = RSNA_TIMER_ACTIVE)
#define RSNA_IS_TIMER_ACTIVE(pRsnaTimer) \
    (((pRsnaTimer)->u1Status == RSNA_TIMER_INACTIVE)?FALSE:TRUE)



UINT4 RsnaTmrInit                     PROTO ((VOID));
UINT4 RsnaTmrStartTimer                  PROTO ((tRsnaTimer *pRsnaTmr,
                                             eRsnaTimerId RsnaTmrId,
                                             UINT4 u4TimeOut,
                                             VOID *pTmrContext));
UINT4 RsnaTmrStopTimer                   PROTO ((tRsnaTimer *pRsnaTmr));
VOID  RsnaTmrProcRetransTimer  PROTO ((VOID *pTmrContext));
VOID  RsnaTmrProcessEapPacket  PROTO ((VOID *pTmrContext));
VOID  RsnaTmrProcessPMKReKeyTimer        PROTO ((VOID *pTmrContext));
VOID  RsnaTmrProcessDeletePMKTimer       PROTO ((VOID *pTmrContext)); 
VOID  RsnaTmrProcStopTkipCtrMsrTimer  PROTO ((VOID *pTmrContext));
VOID  RsnaTmrProcessReKeyGmk             PROTO ((VOID *pTmrContext));
VOID RsnaTmrProcessReKeyGtk              PROTO ((VOID *pTmrContext));

#endif /* _RSNATMR_H */
