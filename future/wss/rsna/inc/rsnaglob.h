/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaglob.h,v 1.3 2017/11/24 10:37:05 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#ifndef __RSNAGLOB_H__
#define __RSNAGLOB_H__

tRsnaGlobals gRsnaGlobals;
tRsnaOsixSemId    gRsnaSemId = RSNA_INIT_VAL;

CONST UINT1         gau1RsnaAuthKeyMgmt8021X[] = { 0x00, 0x0F, 0xAC, 1 };
CONST UINT1         gau1RsnaAuthKeyMgmtPskOver8021X[] = { 0x00, 0x0F, 0xAC, 2 };
#ifdef PMF_WANTED
CONST UINT1         gau1RsnaAuthKeyMgmtPskSHA256Over8021X[] = { 0x00, 0x0F, 0xAC, 6 };
CONST UINT1         gau1RsnaAuthKeyMgmt8021XSHA256[] = { 0x00, 0x0F, 0xAC, 5 };
#endif
CONST UINT1         gau1RsnaCipherSuiteNone[] = { 0x00, 0x0F, 0xAC, 0 };
CONST UINT1         gau1RsnaCipherSuiteWEP40[] = { 0x00, 0x0F, 0xAC, 1 };
CONST UINT1         gau1RsnaCipherSuiteTKIP[] = { 0x00, 0x0F, 0xAC, 2 };
CONST UINT1         gau1RsnaCipherSuiteCCMP[] = { 0x00, 0x0F, 0xAC, 4 };
CONST UINT1         gau1RsnaCipherSuiteWEP104[] = { 0x00, 0x0F, 0xAC, 5 };
CONST UINT1         gau1RsnaPmkIdKde[] = { 0x00, 0x0F, 0xAC, 4 };
CONST UINT1         gau1RsnaGtkKde[] = { 0x00, 0x0F, 0xAC, 1 };
#ifdef PMF_WANTED
CONST UINT1         gau1RsnaIGtkKde[] = { 0x00, 0x0F, 0xAC, 9 };
CONST UINT1         gau1RsnaCipherSuiteBIP[] = { 0x00, 0x0F, 0xAC, 6 };
#endif

CONST UINT1         gau1WpaAuthKeyMgmt8021X[] = { 0x00, 0x50, 0xF2, 1 };
CONST UINT1         gau1WpaAuthKeyMgmtPskOver8021X[] = { 0x00, 0x50, 0xF2, 2 };
CONST UINT1         gau1WpaAuthKeyMgmtPskSHA256Over8021X[] = { 0x00, 0x50, 0xF2, 6 };
CONST UINT1         gau1WpaCipherSuiteNone[] = { 0x00, 0x50, 0xF2, 0 };
CONST UINT1         gau1WpaCipherSuiteWEP40[] = { 0x00, 0x50, 0xF2, 1 };
CONST UINT1         gau1WpaCipherSuiteTKIP[] = { 0x00, 0x50, 0xF2, 2 };
CONST UINT1         gau1WpaCipherSuiteCCMP[] = { 0x00, 0x50, 0xF2, 4 };
CONST UINT1         gau1WpaCipherSuiteWEP104[] = { 0x00, 0x50, 0xF2, 5 };
CONST UINT1         gau1WpaOuiType[] = { 0x00, 0x50, 0xF2, 1 };
CONST UINT1         gau1WpaGtkKde[] = { 0x00, 0x50, 0xF2, 1 };
CONST UINT1         gau1WpaPmkIdKde[] = { 0x00, 0x50, 0xF2, 4 };

UINT1         gu1RsnaTestPairwiseUpdateCount = OSIX_FALSE;
UINT1         gu1RsnaTestGroupwiseUpdateCount = OSIX_FALSE;

#endif  /*__RSNAGLOB_H__*/

