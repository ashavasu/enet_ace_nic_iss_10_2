/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaextn.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#ifndef _RSNAEXTN_H_
#define _RSNAEXTN_H_

PUBLIC tRsnaGlobals gRsnaGlobals;
PUBLIC tRsnaOsixSemId    gRsnaSemId;
PUBLIC UINT1 gau1WpaAuthKeyMgmt8021X[RSNA_CIPHER_SUITE_LEN]; 
PUBLIC UINT1 gau1WpaAuthKeyMgmtPskOver8021X[RSNA_CIPHER_SUITE_LEN]; 
PUBLIC UINT1 gau1WpaCipherSuiteNone[RSNA_CIPHER_SUITE_LEN];
PUBLIC UINT1 gau1WpaCipherSuiteWEP40[RSNA_CIPHER_SUITE_LEN];
PUBLIC UINT1 gau1WpaCipherSuiteTKIP[RSNA_CIPHER_SUITE_LEN];
PUBLIC UINT1 gau1WpaCipherSuiteCCMP[RSNA_CIPHER_SUITE_LEN];
PUBLIC UINT1 gau1WpaCipherSuiteWEP104[RSNA_CIPHER_SUITE_LEN];
PUBLIC UINT1 gau1WpaOuiType[RSNA_CIPHER_SUITE_LEN];

PUBLIC UINT1         gu1RsnaTestPairwiseUpdateCount;
PUBLIC UINT1         gu1RsnaTestGroupwiseUpdateCount;



#endif/*_RSNAEXTN_H_*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file rsnaextn.h                      */
/*-----------------------------------------------------------------------*/

