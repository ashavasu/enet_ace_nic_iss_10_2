/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnamacr.h,v 1.2 2017/05/23 14:16:53 siva Exp $
*
* Description: RSNA File.
*********************************************************************/


#ifndef _RSNAMACR_H
#define _RSNAMACR_H


#define RSNA_MEMORY_TYPE      MEM_DEFAULT_MEMORY_TYPE
#define RSNA_PORTINFO_MEMPOOL_ID  gRsnaGlobals.RsnaPortInfoPoolId
#define RSNA_AUTHSESSNODE_MEMPOOL_ID  gRsnaGlobals.RsnaSessNodePoolId
#define RSNA_PMKSA_MEMPOOL_ID      gRsnaGlobals.RsnaPmkSaNodePoolId
#define RSNA_SUPP_INFO_MEMPOOL_ID  gRsnaGlobals.RsnaSuppInfoPoolId 
#define RSNA_SUPP_STATS_INFO_MEMPOOL_ID  gRsnaGlobals.RsnaSuppStatsInfoPoolId
#define RSNA_WSS_INFO_MEMPOOL_ID   gRsnaGlobals.RsnaWssInfoPoolId 


#define RSNA_AUTHSESSNODE_MEMBLK_COUNT          RSNA_MAX_SUPP
#define RSNA_PORTINFO_MEMBLK_COUNT              BRG_MAX_PHY_PORTS
#define RSNA_PMKSA_NODE_MEMBLK_COUNT            RSNA_MAX_SUPP
#define RSNA_SUPP_INFO_MEMBLK_COUNT             RSNA_MAX_SUPP

#define RSNA_AUTHSESSNODE_MEMBLK_SIZE sizeof(tRsnaSessionNode)
#define RSNA_PORTINFO_MEMBLK_SIZE     sizeof(tRsnaPaePortEntry)
#define RSNA_PMKSA_MEMBLK_SIZE                    sizeof(tRsnaPmkSa) 
#define RSNA_SUPP_INFO_MEMBLK_SIZE                sizeof(tRsnaSupInfo) 

#define RSNA_CREATE_MEM_POOL(x,y,pPoolId)\
             MemCreateMemPool(x,y,RSNA_MEMORY_TYPE,(tMemPoolId*)pPoolId)

#define RSNA_DELETE_MEM_POOL(pPoolId)\
            MemDeleteMemPool((tMemPoolId) pPoolId)



#define   RSNA_PROTOCOL_SEMAPHORE     ((UINT1*)"RMTS")
#define   RSNA_BINARY_SEM             1
#define   RSNA_SEM_FLAGS              0 /* unused */

/*FSAP related calls*/
#define   tRsnaOsixSemId         tOsixSemId

#define   RSNA_CREATE_SEMAPHORE(au1Name, count, flags, psemid) \
                 OsixCreateSem((au1Name), (count), (flags), (psemid))


/*RSNA related constants*/
#define   RSNA_INIT_VAL             0

#define RSNA_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
/* macros for allocating memory block */

#define RSNA_MEM_ALLOCATE_MEM_BLK(RSNA_MEMPOOL_ID) \
           MemAllocMemBlk(RSNA_MEMPOOL_ID)

/*Macros for releasing memory block*/

#define RSNA_MEM_RELEASE_MEM_BLK(RSNA_MEMPOOL_ID,pNode) \
          MemReleaseMemBlock(RSNA_MEMPOOL_ID, \
                             (UINT1 *)pNode)

#define RSNA_UNUSED(x) x = x; 
#define RSNA_GTK_FIRST_IDX   1
#define RSNA_GTK_SECOND_IDX  2  

#ifdef PMF_WANTED
#define RSNA_IGTK_FIRST_IDX 4
#define RSNA_IGTK_SECOND_IDX 5
#endif
 
#define RSNA_RC4_SKIP  256
#define RSNA_ONE_BYTE_FULL_MASK 0xFF

#define RSNA_GET_TWOBYTE(u2Val, MacAddr)            \
             MEMCPY (&u2Val, MacAddr, 2);\
             u2Val = (UINT2)OSIX_NTOHS(u2Val);               \

#define RSNA_GET_FOURBYTE(u4Val, MacAddr)              \
            MEMCPY (&u4Val, MacAddr, 4); \
            u4Val = OSIX_NTOHL(u4Val);                 \


#define   RSNA_MEMCPY              MEMCPY
#define   RSNA_MEMCMP              MEMCMP
#define   RSNA_MEMSET              MEMSET

#define   RSNA_GREATER  1
#define   RSNA_EQUAL    0
#define   RSNA_LESSER   -1

#define   RSNA_SUCCESS 0
#define   RSNA_FAILURE 1

/* RSNA task related macros */
#define   RSNA_TASK_NAME              ((UINT1 *)"RsnaT")
#define   RSNA_TASK_QUEUE_NAME        ((UINT1 *)"RsnaQ")
#define   RSNA_TASK_ID                gRsnaGlobals.gRsnaTaskId
#define   RSNA_TASK_QUEUE_ID          gRsnaGlobals.gRsnaTaskQId
#define   RSNA_TASK_QUEUE_DEPTH       50

/*FSAP related calls*/
#define   RSNA_GET_TASK_ID       OsixGetTaskId

#define   RSNA_CREATE_SEMAPHORE(au1Name, count, flags, psemid) \
                 OsixCreateSem((au1Name), (count), (flags), (psemid))

#define   RSNA_INIT_VAL             0

#define RSNA_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define RSNA_QUEUE_EVENT       0x01


#define RSNA_ALL_EVENTS       (RSNA_QUEUE_EVENT)

#endif /* _RSNAMACR_H */
