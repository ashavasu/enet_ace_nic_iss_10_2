/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: rsnarty.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 * *
 * * Description: RSNA File.
 * *********************************************************************/

#define MAX_RSNA_EAP_PACKET_INFO                256

#define RSNA_EAP_PACKET_INFO_MEMPOOL_ID   gRsnaGlobals.RsnaEapPacketinfoPoolId

#define RSNA_EAP_MAX_RETRY_COUNT 3

#define RSNA_EAP_MAX_RETRANSMIT_TIME 1

#define RSNA_EAP_IDENTITY_OFFSET 22

#define RSNA_EAP_FAILURE_OFFSET 18

#define RSNA_EAP_SUCCESS_OFFSET 18

#define RSNA_EAP_IDENTITY   0x01

#define RSNA_EAP_FAILURE   0x04

#define RSNA_EAP_SUCCESS   0x03

/* This Database is used in the case of Eap packet retransmit */
typedef struct{
    tRBNodeEmbd     nextRsnaEapPacketinfo;
    tRsnaTimer      RsnaEapRetransmitTmr;
    UINT4           u4PktLen;
    UINT4           u4Retry;
    tMacAddr        StaMacAddr;
    UINT2           u2PortNum;
    UINT1           au1Bufffer[1496];
}tRsnaEapPacketinfo;


UINT4
RsnaEapolRtyInit (VOID);

VOID
RsnaEapolRtyAssignMempoolIds (VOID);

INT4
RsnaCompareEapPacketinfo PROTO ((tRBElem * e1, tRBElem * e2));


UINT1
RsnaEapolRtyProcess (UINT1 *pu1KeyFrame, UINT4 u4PktLen, UINT2 u2PortNum);

UINT1
RsnaEapolRtyAddEntry (UINT1 *pu1KeyFrame, UINT4 u4PktLen, UINT2 u2PortNum);
