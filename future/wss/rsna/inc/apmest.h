/**********************************************************************
 * Copyright (C) Future Software Limited, 2002
 * $Id: apmest.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
 * Description: This has interface structure defintions for APME Stubs.
 *
 ***********************************************************************/
#ifndef _APMEST_H_
#define _APMEST_H_

VOID  ApmeStubInit (VOID);

#endif /* _APMEST_H_ */
