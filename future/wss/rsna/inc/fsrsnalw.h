/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrsnalw.h,v 1.3 2017/11/24 10:37:05 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNATraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsRSNAClearAllCounters ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRSNATraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsRSNAClearAllCounters ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRSNATraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRSNAClearAllCounters ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRSNATraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRSNAClearAllCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRSNAClearCountersTable. */
INT1
nmhValidateIndexInstanceFsRSNAClearCountersTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRSNAClearCountersTable  */

INT1
nmhGetFirstIndexFsRSNAClearCountersTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRSNAClearCountersTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNAClearCounter ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRSNAClearCounter ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRSNAClearCounter ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRSNAClearCountersTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRSNAExtTable. */
INT1
nmhValidateIndexInstanceFsRSNAExtTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRSNAExtTable  */

INT1
nmhGetFirstIndexFsRSNAExtTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRSNAExtTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNAOKCEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRSNA4WayHandshakeCompletedStats ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAGroupHandshakeCompletedStats ARG_LIST((INT4 ,UINT4 *));

#ifdef PMF_WANTED
INT1
nmhGetFsRSNAAssociationComeBackTime ARG_LIST((INT4 ,UINT4 *));
#endif
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRSNAOKCEnabled ARG_LIST((INT4  ,INT4 ));

#ifdef PMF_WANTED
INT1
nmhSetFsRSNAAssociationComeBackTime ARG_LIST((INT4  ,UINT4 ));
#endif

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRSNAOKCEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

#ifdef PMF_WANTED
INT1
nmhTestv2FsRSNAAssociationComeBackTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
#endif
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRSNAExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */


INT1
nmhValidateIndexInstanceFsRSNAConfigAuthenticationSuitesTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRSNAConfigAuthenticationSuitesTable  */

INT1
nmhGetFirstIndexFsRSNAConfigAuthenticationSuitesTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRSNAConfigAuthenticationSuitesTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNAConfigAuthenticationSuiteImplemented ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAConfigAuthenticationSuiteActivated ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRSNAConfigAuthenticationSuiteActivated ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRSNAConfigAuthenticationSuiteActivated ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRSNAConfigTable. */
INT1
nmhValidateIndexInstanceFsRSNAConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRSNAConfigTable  */

INT1
nmhGetFirstIndexFsRSNAConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRSNAConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNAConfigVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigPairwiseKeysImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigGroupCipher ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAConfigGroupRekeyMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRSNAConfigGroupRekeyTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigGroupRekeyPackets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigGroupRekeyStrict ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRSNAConfigPSKValue ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAConfigPSKPassPhrase ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAConfigGroupUpdateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigPairwiseUpdateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigGroupCipherSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigPMKLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigPMKReauthThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigNumberOfPTKSAReplayCountersImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigSATimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAAuthenticationSuiteSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAPairwiseCipherSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAGroupCipherSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAPMKIDUsed ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAAuthenticationSuiteRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAPairwiseCipherRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAGroupCipherRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNATKIPCounterMeasuresInvoked ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNA4WayHandshakeFailures ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigNumberOfGTKSAReplayCountersImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsRSNAConfigActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRSNAConfigMixMode ARG_LIST((INT4 ,INT4 *));

INT1 nmhGetFsRSNAConfigSTKKeysImplemented ARG_LIST((INT4  , UINT4 *));

INT1 nmhGetFsRSNAConfigSTKCipher ARG_LIST((INT4 , tSNMP_OCTET_STRING_TYPE * ));

INT1 nmhGetFsRSNAConfigSTKRekeyTime ARG_LIST((INT4  , UINT4 *));

INT1 nmhGetFsRSNAConfigSMKUpdateCount ARG_LIST((INT4  , UINT4 *));

INT1 nmhGetFsRSNAConfigSTKCipherSize ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNAConfigSTKCipherSize));

INT1 nmhGetFsRSNAConfigSMKLifetime ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNAConfigSMKLifetime));

INT1 nmhGetFsRSNAConfigSMKReauthThreshold ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNAConfigSMKReauthThreshold));

INT1 nmhGetFsRSNAConfigNumberOfSTKSAReplayCountersImplemented ARG_LIST((INT4  , UINT4 *));

INT1 nmhGetFsRSNAPairwiseSTKSelected ARG_LIST((INT4 i4IfIndex , tSNMP_OCTET_STRING_TYPE * ));

INT1 nmhGetFsRSNASMKHandshakeFailures ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNASMKHandshakeFailures));

INT1 nmhGetFsRSNASAERetransPeriod ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNASAERetransPeriod));

INT1 nmhGetFsRSNASAEAntiCloggingThreshold ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNASAEAntiCloggingThreshold));

INT1 nmhGetFsRSNASAESync ARG_LIST((INT4 i4IfIndex , UINT4 *pu4RetValFsRSNASAESync));

INT1 nmhSetFsRSNAConfigSMKReauthThreshold ARG_LIST((INT4 i4IfIndex , UINT4 u4SetValFsRSNAConfigSMKReauthThreshold));

INT1 nmhTestv2FsRSNAConfigSTKCipher ARG_LIST((UINT4 *pu4ErrorCode , INT4 i4IfIndex , tSNMP_OCTET_STRING_TYPE *pTestValFsRSNAConfigSTKCipher));

INT1 nmhTestv2FsRSNAConfigSTKRekeyTime ARG_LIST((UINT4 *pu4ErrorCode , INT4 i4IfIndex , UINT4 u4TestValFsRSNAConfigSTKRekeyTime));

INT1 nmhTestv2FsRSNAConfigSMKUpdateCount ARG_LIST((UINT4 *pu4ErrorCode , INT4 i4IfIndex , UINT4 u4TestValFsRSNAConfigSMKUpdateCount));

INT1 nmhTestv2FsRSNAConfigSTKCipherSize ARG_LIST((UINT4 *pu4ErrorCode , INT4 i4IfIndex , UINT4 u4TestValFsRSNAConfigSTKCipherSize));

INT1 nmhTestv2FsRSNAConfigSMKLifetime ARG_LIST((UINT4 *pu4ErrorCode , INT4 i4IfIndex , UINT4 u4TestValFsRSNAConfigSMKLifetime));

INT1 nmhTestv2FsRSNAConfigSMKReauthThreshold ARG_LIST((UINT4 *pu4ErrorCode , INT4 i4IfIndex , UINT4 u4TestValFsRSNAConfigSMKReauthThreshold));

INT1 nmhSetFsRSNAConfigSTKCipher ARG_LIST((INT4 i4IfIndex , tSNMP_OCTET_STRING_TYPE *pSetValFsRSNAConfigSTKCipher));

INT1 nmhSetFsRSNAConfigSTKRekeyTime(INT4 i4IfIndex , UINT4 u4SetValFsRSNAConfigSTKRekeyTime);

INT1 nmhSetFsRSNAConfigSMKUpdateCount(INT4 i4IfIndex , UINT4 u4SetValFsRSNAConfigSMKUpdateCount);

INT1 nmhSetFsRSNAConfigSTKCipherSize(INT4 i4IfIndex , UINT4 u4SetValFsRSNAConfigSTKCipherSize);

INT1 nmhSetFsRSNAConfigSMKLifetime(INT4 i4IfIndex , UINT4 u4SetValFsRSNAConfigSMKLifetime);

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRSNAConfigGroupCipher ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRSNAConfigGroupRekeyMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRSNAConfigGroupRekeyTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigGroupRekeyPackets ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigGroupRekeyStrict ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRSNAConfigPSKValue ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRSNAConfigPSKPassPhrase ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRSNAConfigGroupUpdateCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigPairwiseUpdateCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigGroupCipherSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigPMKLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigPMKReauthThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigSATimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsRSNAConfigActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRSNAConfigMixMode ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRSNAConfigGroupCipher ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRSNAConfigGroupRekeyMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRSNAConfigGroupRekeyTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigGroupRekeyPackets ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigGroupRekeyStrict ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRSNAConfigPSKValue ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRSNAConfigPSKPassPhrase ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRSNAConfigGroupUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigPairwiseUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigGroupCipherSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigPMKLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigPMKReauthThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigSATimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsRSNAConfigActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRSNAConfigMixMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRSNAConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRSNAConfigPairwiseCiphersTable. */
INT1
nmhValidateIndexInstanceFsRSNAConfigPairwiseCiphersTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRSNAConfigPairwiseCiphersTable  */

INT1
nmhGetFirstIndexFsRSNAConfigPairwiseCiphersTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRSNAConfigPairwiseCiphersTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNAConfigPairwiseCipherImplemented ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAConfigPairwiseCipherActivated ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRSNAConfigPairwiseCipherSizeImplemented ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRSNAConfigPairwiseCipherActivated ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRSNAConfigPairwiseCipherActivated ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRSNAConfigPairwiseCiphersTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRSNAStatsTable. */
INT1
nmhValidateIndexInstanceFsRSNAStatsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRSNAStatsTable  */

INT1
nmhGetFirstIndexFsRSNAStatsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRSNAStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRSNAStatsSTAAddress ARG_LIST((INT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsRSNAStatsVersion ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsRSNAStatsSelectedPairwiseCipher ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRSNAStatsTKIPICVErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsRSNAStatsTKIPLocalMICFailures ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsRSNAStatsTKIPRemoteMICFailures ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsRSNAStatsTKIPReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsWPAConfigAuthenticationSuitesTable. */
INT1
nmhValidateIndexInstanceFsWPAConfigAuthenticationSuitesTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsWPAConfigAuthenticationSuitesTable  */

INT1
nmhGetFirstIndexFsWPAConfigAuthenticationSuitesTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsWPAConfigAuthenticationSuitesTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsWPAConfigAuthenticationSuiteImplemented ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsWPAConfigAuthenticationSuiteActivated ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsWPAConfigAuthenticationSuiteActivated ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsWPAConfigAuthenticationSuiteActivated ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsWPAConfigAuthenticationSuitesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
