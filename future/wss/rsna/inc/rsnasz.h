/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnasz.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

enum {
    MAX_RSNA_AUTHSESSNODE_INFO_SIZING_ID,
    MAX_RSNA_PMKSA_NODE_INFO_SIZING_ID,
    MAX_RSNA_PORT_INFO_SIZING_ID,
    MAX_RSNA_SUPP_INFO_SIZING_ID,
    MAX_RSNA_SUPP_STATS_INFO_SIZING_ID,
    MAX_RSNA_WSS_INFO_SIZING_ID,
    MAX_RSNA_EAP_PACKET_INFO_SIZING_ID,
    RSNA_MAX_SIZING_ID
};


#ifdef  _RSNASZ_C
tMemPoolId RSNAMemPoolIds[ RSNA_MAX_SIZING_ID];
INT4  RsnaSizingMemCreateMemPools(VOID);
VOID  RsnaSizingMemDeleteMemPools(VOID);
INT4  RsnaSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RSNASZ_C  */
extern tMemPoolId RSNAMemPoolIds[ ];
extern INT4  RsnaSizingMemCreateMemPools(VOID);
extern VOID  RsnaSizingMemDeleteMemPools(VOID);
#endif /*  _RSNASZ_C  */


#ifdef  _RSNASZ_C
tFsModSizingParams FsRSNASizingParams [] = {
{ "tRsnaSessionNode", "MAX_RSNA_AUTHSESSNODE_INFO", sizeof(tRsnaSessionNode),MAX_RSNA_AUTHSESSNODE_INFO, MAX_RSNA_AUTHSESSNODE_INFO,0 },
{ "tRsnaPmkSa", "MAX_RSNA_PMKSA_NODE_INFO", sizeof(tRsnaPmkSa),MAX_RSNA_PMKSA_NODE_INFO, MAX_RSNA_PMKSA_NODE_INFO,0 },
{ "tRsnaPaePortEntry", "MAX_RSNA_PORT_INFO", sizeof(tRsnaPaePortEntry),MAX_RSNA_PORT_INFO, MAX_RSNA_PORT_INFO,0 },
{ "tRsnaSupInfo", "MAX_RSNA_SUPP_INFO", sizeof(tRsnaSupInfo),MAX_RSNA_SUPP_INFO, MAX_RSNA_SUPP_INFO,0 },
{ "tRsnaSuppStatsInfo", "MAX_RSNA_SUPP_STATS_INFO", sizeof(tRsnaSuppStatsInfo),MAX_RSNA_SUPP_STATS_INFO, MAX_RSNA_SUPP_STATS_INFO,0 },
{ "tWssRSNANotifyParams", "MAX_RSNA_WSS_INFO", sizeof(tWssRSNANotifyParams),MAX_RSNA_WSS_INFO, MAX_RSNA_WSS_INFO,0 },
{ "tRsnaEapPacketinfo", "MAX_RSNA_EAP_PACKET_INFO", sizeof(tRsnaEapPacketinfo),MAX_RSNA_EAP_PACKET_INFO, MAX_RSNA_EAP_PACKET_INFO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RSNASZ_C  */
extern tFsModSizingParams FsRSNASizingParams [];
#endif /*  _RSNASZ_C  */


