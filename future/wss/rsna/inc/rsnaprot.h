/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnaprot.h,v 1.4 2017/11/24 10:37:06 siva Exp $
*
* Description: RSNA File.
*********************************************************************/

#ifndef _RSNAPROT_H
#define _RSNAPROT_H
#include "wsscfgprot.h"
#include "wssstawlcprot.h"
/* Api to Process the Recvd EAPOl Key Frame */
UINT4 RsnaProcEapolKeyFrame             PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessNode));

/* Api to Kick start the FSM Related to Rsna */
UINT4 RsnaMainFsmStart                      PROTO ((tRsnaSessionNode 
                                                  *pRsnaSessNode));

/* Api to  Handle 4-Way Handshake FSM */
UINT4 RsnaMain4WayHandShakeFsm        PROTO ((tRsnaSessionNode 
                                                  *pRsnaSessNode));

/* Api to Handle Per Sta Group Key FSM */
UINT4 RsnaMainPerStaGrpKeyFsm       PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessNode));

/* Api to Handle Global Group Key State Machine */
UINT4 RsnaMainGlobalGrpKeyFsm       PROTO ((tRsnaGlobalGtk 
                                                  *pRsnaGlobalGtk));

/* Api to Construct the Outbound Eapol Key Frame */
UINT4 RsnaPktSendEapolKeyPkt                  PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessNode, 
                                                   UINT2 u2KeyInfo, 
                                                   UINT1 *pu1KeyRsc, 
                                                   UINT1 *pu1Nonce,
                                                   BOOL1 bGtk, 
                                                   BOOL1 bIE,
                                                   BOOL1 bPmkIdKde,
                                                   BOOL1 bIGtk)); 

/* Api to calcaulte MIC used by 4-Way Handshake */
UINT4 RsnaAlgoComputeMic                       PROTO ((UINT2 u2KeyInfo,
                                                   UINT1 *pu1Key, 
                                                   UINT1 *pu1Pkt, 
                                                   UINT2 u2PktLen, 
                                                   UINT1 *pu1Mic));

/* Api to Encrypt the Key Data Field of the 4-Way Handshake */
INT4 RsnaEncryptKeyData                    PROTO ((UINT2 u2KeyInfo,
                                                   tRsnaSessionNode 
                                                   *pRsnaSessInfo,
                                                   UINT1 *pu1Pkt,
                                                   UINT2 u2PktLen));

/* Api to generate the PMKID */
UINT4 RsnaSessGenPmkId                         PROTO ((tRsnaPmkSa *pRsnaPmkSa));

/* Api to map PMK to PTK */
UINT4 RsnaSessGenPtkFromPmk                    PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessInfo));

/* To generate GTK from GMK */
UINT4 RsnaSessGenGtkFromGmk                    PROTO ((tRsnaGlobalGtk 
                                                  *pRsnaGlobalGtk));

/* To get SSID from Apme with IfIndex ad the Input */
UINT1 *RsnaGetSsidFromIfIndex              PROTO ((UINT4 u4IfIndex,
                                                   UINT1 *pu1SsidLen));

UINT4
RsnaAlgoConvertPassPhraseToPsk PROTO ((UINT1 *pu1Ssid, 
                  UINT1 *pu1PassPhrase,
                                       UINT1 *pu1Psk,
           UINT1 u1SsidLen));

INT4
RsnaCliShowWpa1Status (tCliHandle CliHandle, UINT4 u4ProfileId);

INT4
WpaCliShowConfig (tCliHandle CliHandle, UINT4 u4ProfileId);

INT4
WpaCliShowWpaCipher (tCliHandle CliHandle, UINT4 u4ProfileId);

INT4
WpaCliShowAuthSuiteInfo (tCliHandle CliHandle, UINT4 u4ProfileId);

INT4
WpaShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4ProfileId);

VOID
WpaCliShowConfigDisplay (tCliHandle CliHandle, UINT4 u4NextConfigIndex,
                          UINT4 u4WlanId);

VOID
RsnaAlgoHmacSha1 (UINT1 *pu1Key, UINT4 u4KeyLen, UINT1 *pu1BufPtr,
                  UINT4 u4BufLen, UINT1 *pu1Digest);

#ifdef PMF_WANTED
VOID  RsnaAlgoHmacSha2  PROTO ((UINT1 *pu1Key, UINT4 u4KeyLen, 
                                 UINT1 *pu1BufPtr, UINT4 u4BufLen, 
                                 UINT1 *pu1Digest));
#endif


VOID RsnaAlgoHmacMd5                          PROTO ((UINT1 *pu1Key, 
                                                   INT4 i4KeyLen, 
                                                   UINT1 *pu1BufPtr, 
                                                   INT4 i4BufLen, 
                                                   UINT1 *pu1Digest));


UINT4 RsnaAlgoPBKDF2 PROTO ((UINT1 *pu1PassPhrase,
                             UINT1 u1PassPhraseLen,
                             UINT1 *pu1Ssid,
                             UINT1 u1SsidLen,
                             UINT2 u2Iterations,
                             UINT4 u4Count, 
        UINT1 *pu1Digest));

VOID  RsnaUtilGetRandom                        PROTO ((UINT1 *pu1Ptr, 
                                                   UINT1  u1Len));

tRsnaSupInfo *RsnaUtilGetStaInfoFromStaAddr    PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortInfo,
                                                   UINT1 *pu1Addr));

VOID RsnaUtilStartTkipCtrMsrs                  PROTO ((tRsnaPaePortEntry 
                                                  *pRsnaPaePortInfo));  

tRsnaSupInfo *RsnaUtilCreateAndAddStaToList    PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortInfo,
                                                   UINT1 *pu1SuppAddr));

UINT4 RsnaSessDeleteSession                    PROTO ((tRsnaSessionNode 
                                                  *pRsnaSessNode));

UINT4 RsnaApmeEnqMsgToApme                     PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortInfo,
                                                   tRsnaSessionNode 
                                                   *pRsnaSessionNode, 
                                                   tRsnaSupInfo   
                                                   *pRsnaSupInfo,
                                                   UINT4 u4MsgType,
                                                   BOOL1 bPairwiseKey));

VOID RsnaAlgoAesWrap                          PROTO ((UINT1 *pu1Key, 
                                                   INT4 i4Len, 
                                                   UINT1 *pu1PlainTxt,
                                                   UINT1 *pu1CipherTxt));

VOID RsnaAlgoRc4Skip                          PROTO ((UINT1* pu1EncrKey,
                                                   UINT1 u1NonceLen,
                                                   UINT2 u2Skip,
                                                   UINT1 *pu1Ptr,
                                                   UINT2 u2KeyDataLen));
#ifdef PMF_WANTED
VOID RsnaAlgoCmacAes128    PROTO ((UINT1 *pu1Key, UINT4 u4KeyLen,
                                 UINT1 *pu1BufPtr, UINT4 u4BufLen, 
                                 UINT1 *pu1Digest));
#endif

UINT4 RsnaUtilIncByteArray                     PROTO ((UINT1 *pu1Ptr,
                                                   UINT2  u2Len));

INT4 CfaHandlePktFromPnac                  PROTO ((tCRU_BUF_CHAIN_HEADER 
                                                   *pBuf, 
                                                   UINT2 u2IfIndex,
                                                   UINT4 u4PktSize, 
                                                   UINT2 u2Protocol, 
                                                   UINT1 u1EncapType));

tRsnaSessionNode *RsnaSessCreateAndInitSession    PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortEntry,
                                                   tPnacAuthSessionNode 
                                                   *pAuthSessNode,
                                                    UINT1 *pu1Pkt,
                                                    UINT2 u2PktLen,  
                                                    UINT4 u4BssIfIndex,
                                                    UINT1 * pu1PmkId,
                                                    BOOL1 bRsnaInitStateMachine));

tPnacAuthSessionNode *RsnaSessCreatePnacAuthSess PROTO ((UINT1 *pu1SrcAddr,
                                                     UINT2 u2PortNum, 
                                                     BOOL1 
                                                     bTriggerPnacStateMachine));


UINT4 RsnaSessDisconnectSta                    PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessNode));


UINT4 RsnaProcVerifyKeyMic                     PROTO ((tRsnaPtkSa *pPtkSaDB,
                                                   UINT1 * pu1Pkt,
                                                   UINT2 u2PktLen));

UINT4 RsnaSessReqNewPtk                        PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessNode));

UINT4 RsnaSessGetSeqNum                        PROTO ((tRsnaSessionNode 
                                                   *pRsnaSessNode, 
                                                    UINT1 *pu1KeyRsc));
#ifdef PMF_WANTED
VOID RsnaSessGenIGtk                           PROTO ((tRsnaGlobalIGtk * pRsnaGlobalIGtk,
                                                       UINT1 u1IGtkKeyIdx));
#endif


tRsnaPmkSa *RsnaUtilGetPmkSaFromCache          PROTO ((UINT1 *pu1Addr,
                                                     UINT1 *pu1Bssid
                                                     ));   


tRsnaPskDB *RsnaUtilGetPskFromStaAddr          PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortEntry,
                                                   UINT1 *pu1Addr));   

UINT4 RsnaUtilGtkRekey                         PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortInfo));

UINT4 RsnaUtilKickOfAllStations                PROTO ((tRsnaPaePortEntry 
                                                  *pRsnaPaePortInfo));

UINT4 RsnaUtilDeletePmkSa                      PROTO ((tRsnaPmkSa *pRsnaPmkSa));

UINT4 RsnaUtilAddPmkSaToCache                  PROTO ((tRsnaPaePortEntry *
                                                       pRsnaPaePortInfo,
                                                       tPnacAuthSessionNode 
                                                       *pPnacAuthSessNode,
                                                        UINT1 *pu1Pmk,
                                                        UINT1 *pu1PmkId));

tRsnaPaePortEntry *RsnaUtilGetRsnaPaePortEntry PROTO ((INT4 i4Index));

UINT4 RsnaUtilValidateIE                       PROTO ((tRsnaElements *
                                                   pRsnaElems,
                                                   tRsnaSupInfo *pRsnaSupInfo,
                                                   UINT4 u4IfIndex));


UINT4 RsnaUtilParseIE                          PROTO ((UINT1 *pu1Pos, 
                                                   tRsnaElements *pRsna,
                                                   UINT2 u2IeLen));

UINT4 RsnaUtilParseWpaIE                       PROTO ((UINT1 *pu1Pos, 
                                  tRsnaElements * pRsna, 
             UINT2 u2IeLen));

UINT4 RsnaUtilConstructWpaIE                   PROTO ((tApmeRsnaConfInfo *
                                  papmeRsnaConfInfo, 
                                                       UINT1 *pu1IE,
                                                       UINT1 *pu1IELen));
UINT4
RsnaSessHandleNewStation                       PROTO ((tRsnaPaePortEntry 
                                                   *pRsnaPaePortInfo,
                                                   tRsnaSupInfo 
                                                   *pRsnaSupInfo, UINT4 u4BssIfIndex,
                                                    tRsnaElements *pRsnaElements,UINT1));

UINT4
RsnaInitPnacPortConfig                     PROTO ((UINT4 u4Iface, 
                                                   UINT4 u4AuthMode));

VOID RsnaAssignMempoolIds (VOID);
INT4
RsnaCompareProfileInterface PROTO ((tRBElem * e1, tRBElem * e2));
INT4
RsnaComparePmkId PROTO ((tRBElem * e1, tRBElem * e2));

INT4
RsnaCompareSuppStatsIndex PROTO ((tRBElem * e1, tRBElem * e2));

INT4
RsnaAddPortEntry PROTO ((UINT4 u4IfIndex));
INT4
RsnaDeletePortEntry PROTO ((UINT4 u4IfIndex));
VOID
RsnaGetPortEntry PROTO ((UINT2 u2PortNum, tRsnaPaePortEntry **ppPortInfo));
VOID
RsnaGetNextPortEntry PROTO ((tRsnaPaePortEntry * pRsnaPortEntry,
                       tRsnaPaePortEntry ** pNextRsnaPortEntry));
VOID
RsnaGetFirstPortEntry PROTO ((tRsnaPaePortEntry **pRsnaPortEntry));

VOID 
RsnaGetFirstSuppStatsEntry PROTO ((tRsnaSuppStatsInfo ** ppRsnaSuppStatsInfo));

VOID
RsnaGetNextSuppStatsEntry PROTO ((tRsnaSuppStatsInfo * pRsnaSuppStatsInfo,
                                tRsnaSuppStatsInfo ** pRsnaNextSuppStatsInfo));
VOID
RsnaGetSuppStatsInfo PROTO ((UINT2 u2PortNum, UINT4 u4SuppStatsIndex,
                             tRsnaSuppStatsInfo ** ppSuppStatsInfo));

UINT4       RsnaCorePtkInitializeState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkDisconnectState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkDisconnectedState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkAuthenticationState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkAuthentication2State (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkStartState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCoreInitPmkState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCoreInitPskState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkCalcNegotiatingState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkCalcNegotiating2State (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkInitNegotiatingState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkInitDoneState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkGrpKeyIdleState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkGrpReKeyNegoState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkGrpReKeyEstState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCorePtkGrpKeyErrorState (tRsnaSessionNode * pRsnaSessNode);
UINT4       RsnaCoreGlobalGrpKeyInitState (tRsnaGlobalGtk * pRsnaGlobalGtk);
UINT4       RsnaCoreGlobalGrpSetKeyDoneState (tRsnaGlobalGtk * pRsnaGlobalGtk);
UINT4       RsnaCoreGlobalGrpSetKeysState (tRsnaGlobalGtk * pRsnaGlobalGtk);

VOID        RsnaProcMicFailInd PROTO ((tWssRSNANotifyParams *
                                        pWssRSNANotifyParams));
VOID        RsnaProcAssocInd  PROTO((tWssRSNANotifyParams *
                                      pWssRSNANotifyParams));
VOID        RsnaProcReAssocInd PROTO ((tWssRSNANotifyParams *
                                        pWssRSNANotifyParams));
VOID        RsnaProcDisDeAuthInd PROTO ((tWssRSNANotifyParams *
                                         pWssRSNANotifyParams,
                                         enWssRsnaNotifyType eWssRsnaNotifyType));

VOID        RsnaSendDisAssocMsg PROTO ((tWssRSNANotifyParams * pWssRSNANotifyParams));
VOID        RsnaSendDeAuthMsg PROTO ((tWssRSNANotifyParams * pWssRSNANotifyParams));


VOID        RsnaInterfaceHandleQueueEvent PROTO ((VOID));

VOID        RsnaEnQFrameToRsnaTask PROTO ((tWssRSNANotifyParams *pMsg));

UINT4       RsnaUtilFillWssNotifyParams PROTO ((tRsnaPaePortEntry * pRsnaPaePortInfo,
                      tRsnaSessionNode * pRsnaSessionNode,
                      UINT4 u4MsgType, UINT1 u1KeyType));


UINT1      RsnaGetBeaconRSNIEElements PROTO ((UINT4 u4WlanProfileIfIndex, UINT1 *pu1RsnIE, UINT1 *pu1RsnIELength));

UINT4      RsnaTxFrame PROTO ((UINT1 *pu1KeyFrame, tStaMac StaMac, tBssid BssMac,
             UINT4 u4BssIfIndex, UINT4 u4BufLen));

UINT4      RsnaGtkRekeyReInitiate PROTO ((tRsnaPaePortEntry * pRsnaPaePortInfo));

UINT4      RsnaUtilGenerateOkcPmkid PROTO ((tRsnaPaePortEntry * pRsnaPaePortInfo,
                                  tRsnaSupInfo * pRsnaSupInfo, UINT4 u4BssIfIndex,
                                  tRsnaElements ** ppRsnaElements,
                                  UINT1 *pu1Pmk));

UINT4      RsnaUtilProcessKeyAvailable PROTO ((UINT2 u2Port, UINT1 *pu1SuppMacAddr, 
                                           UINT1 *pu1Pmk));
tRsnaSuppStatsInfo * RsnaUtilAddStaToStatsTable PROTO ((UINT2 u2PortNum,                                                              UINT4 u4StatsIndex,
                                               UINT1 *pu1SuppAddr));
tRsnaSuppStatsInfo * RsnaUtilGetStationFromStatsTable PROTO ((UINT2 u2PortNum,
                        UINT1 *pu1Addr));
UINT4 RsnaUtilDeleteSuppStatsInfo PROTO ((tRsnaSuppStatsInfo  * pRsnaSuppStatsInfo));

VOID RsnaUtilGetSuppStatsInfo PROTO ((UINT2 u2PortNum, UINT4 u4SuppStatsIndex,
                                       tRsnaSuppStatsInfo ** ppSuppStatsInfo));
VOID RsnaUtilGetNextSuppStatsEntry PROTO ((tRsnaSuppStatsInfo * pRsnaSuppStatsInfo,
                                   tRsnaSuppStatsInfo ** ppRsnaNextSuppStatsInfo));
VOID RsnaUtilGetFirstSuppStatsEntry PROTO ((tRsnaSuppStatsInfo ** 
                                           ppRsnaSuppStatsInfo));

VOID
RsnaPnacDeleteSession PROTO ((tMacAddr staMacAddr));


UINT1 RsnaGetRSNAType PROTO ((UINT2 u2WlanIfIndex));
#ifdef WPA_WANTED
INT4
WpaCliSetSecurity PROTO ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId));


INT4
WpaCliSetGroupwiseCipher PROTO ((tCliHandle CliHandle, UINT1 u1CipherType,
                           UINT4 u4ProfileId));
INT4
WpaCliSetGroupRekeyinterval PROTO ((tCliHandle CliHandle, UINT4 u4GroupRekeyInterval,
                              UINT4 u4ProfileId));
INT4
WpaCliSetAkmPsk PROTO ((tCliHandle CliHandle, UINT1 u1PskFormat,
                  UINT1 *pu1Key, UINT4 u4ProfileId));
INT4
WpaCliSetGroupUpdateCount PROTO ((tCliHandle CliHandle, UINT4 u4RetryCount,
                            UINT4 u4ProfileId));
INT4
WpaCliSetPairwiseUpdateCount PROTO ((tCliHandle CliHandle, UINT4 u4RetryCount,
                               UINT4 u4ProfileId));
INT4
WpaCliSetPMKLifeTime PROTO ((tCliHandle CliHandle, UINT4 u4LifeTime,
                       UINT4 u4ProfileId));
INT4
WpaCliSetSATimeOut PROTO ((tCliHandle CliHandle, UINT4 u4TimeValue, UINT4 u4ProfileId));
INT4
WpaCliSetAuthSuiteStatus PROTO ((tCliHandle CliHandle, UINT1 u1AuthType,
                           UINT1 u1Status, UINT4 u4ProfileId));
INT4
WpaCliClearStats PROTO ((tCliHandle CliHandle));

INT4
WpaCliClearCountersDetail PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId));
INT4
WpaCliSetOkcStatus PROTO ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId));

UINT4
WpaUtilValidateIE PROTO((tRsnaElements * pRsnaElems,
                    tRsnaSupInfo * pRsnaSupInfo, UINT4 u4IfIndex));
UINT4
WpaSessHandleNewStation PROTO ((tRsnaPaePortEntry * pRsnaPaePortInfo,
                          tRsnaSupInfo * pRsnaSupInfo, UINT4 u4BssIfIndex,
                          tRsnaElements * pRsnaElements,UINT1 u1Type));
UINT1
WpaGetBeaconWPAIEElements PROTO((UINT4 u4WlanProfileIfIndex, UINT1 *pu1RsnIE,
                            UINT1 *pu1RsnIELength));
INT1
WpaGetFsRSNAConfigAuthenticationSuiteEnabled PROTO ((INT4 i4IfIndex,
                                                  UINT4
                                                  u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                  INT4 
                                                  *pi4RetValFsRSNAConfigAuthenticationSuiteEnabled));
INT1
WpaTestv2FsRSNAConfigAuthenticationSuiteEnabled PROTO ((UINT4
                                                     *pu4ErrorCode,
                                                     UINT4
                                                     u4FsRSNAConfigAuthenticationSuiteIndex,
                                                     INT4
                                                     i4TestValFsRSNAConfigAuthenticationSuiteEnabled));
INT1
WpaSetFsRSNAConfigAuthenticationSuiteEnabled PROTO ((UINT4 u4ProfileId,
                                                  UINT4
                                                  u4FsRSNAConfigAuthenticationSuiteIndex,
                                                  INT4
                                                  i4SetValFsRSNAConfigAuthenticationSuiteEnabled));
INT1
RsnaGetFsRSNAConfigAuthenticationSuite PROTO ((UINT4 u4ProfileId,
                                           UINT4
                                           u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValDot11RSNAConfigAuthenticationSuite));
INT1
WpaGetFsRSNAConfigPairwiseKeysSupported PROTO ((INT4 i4IfIndex,
                                             UINT4
                                             *pu4RetValDot11RSNAConfigPairwiseKeysSupported));
INT1
WpaGetFsRSNAConfigGroupCipher PROTO ((INT4 i4IfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValDot11RSNAConfigGroupCipher));
INT1
WpaGetFsRSNAAuthenticationSuiteRequested PROTO ((INT4 i4IfIndex,
                                              tSNMP_OCTET_STRING_TYPE
                                             *pRetValDot11RSNAAuthenticationSuiteRequested));
INT1
WpaGetFsRSNAPairwiseCipherRequested PROTO ((INT4 i4IfIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValDot11RSNAPairwiseCipherRequested));
INT1
WpaGetFsRSNAGroupCipherRequested PROTO((INT4 i4IfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValDot11RSNAGroupCipherRequested));
INT1
WpaGetFsRSNAPairwiseCipherSelected  PROTO((INT4 i4IfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValDot11RSNAPairwiseCipherSelected));
INT1
WpaTestRadioType PROTO ((INT4 i4IfIndex));

INT1
WpaGetNextIndexFsWPAConfigAuthenticationSuitesTable PROTO ((INT4 i4IfIndex,
                                                          INT4 *pi4NextIfIndex,
                                                          UINT4
                                                          u4Dot11RSNAConfigAuthenticationSuiteIndex,
                                                          UINT4
                                                          *pu4NextDot11RSNAConfigAuthenticationSuiteIndex));
INT1
WpaGetFirstIndexFsWPAConfigAuthenticationSuitesTable PROTO ((INT4 *pi4IfIndex, UINT4 *pu4Dot11RSNAConfigAuthenticationSuiteIndex));

INT4 WpaCliSetMixMode PROTO ((tCliHandle CliHandle, UINT1 u1Status, UINT4 u4ProfileId));

INT4 RsnaCliShowMixModeStatus PROTO ((tCliHandle CliHandle, UINT4 u4ProfileId));

VOID
WpaCliShowWpa1CipherDisplay (tCliHandle CliHandle, UINT4 u4NextWlanId,
                              UINT4 u4NextRSNAConfigPairwiseCipherIndex,
                              UINT4 u4WlanId);
VOID
WpaCliShowAuthSuiteInfoDisplay (tCliHandle CliHandle, UINT4 u4NextWlanId,
                              UINT4 u4NextAuthSuiteIndex,
                              UINT4 u4WlanId);
INT4
WpaShowRunningConfigTableDisplay (tCliHandle CliHandle,
                                   UINT4 u4ProfileId, UINT4 u4WlanId);
INT4
WpaShowRunningConfigCipherTableDisplay (tCliHandle CliHandle,
                                         UINT4 u4ProfileId,
                                         UINT4 u4RSNAConfigPairwiseCipherIndex,
                                         UINT4 u4WlanId);
INT4
WpaShowRunningConfigAuthSuiteTableDisplay (tCliHandle CliHandle,UINT4 u4NextWlanIndex,
                                            UINT4 u4AuthSuiteIndex,UINT4 u4WlanId);



#endif



#endif /* _RSNAPROT_H */
/********** END OF RSNAPROTO.H *****************/
