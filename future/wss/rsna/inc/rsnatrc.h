/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rsnatrc.h,v 1.1.1.1 2016/09/20 10:42:41 siva Exp $
*
* Description: RSNA File.
*********************************************************************/
#ifndef _RSNATRC_H_
#define _RSNATRC_H_

/* Trace and debug flags */
#define   RSNA_TRC_FLAG       gRsnaGlobals.u4RsnaTrcOpt     

/* Module names */
#define   RSNA_MOD_NAME           ((const char *)"RSNA")

#define   RSNA_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   RSNA_MGMT_TRC           MGMT_TRC          
#define   RSNA_DATA_PATH_TRC      DATA_PATH_TRC     
#define   RSNA_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   RSNA_DUMP_TRC           DUMP_TRC          
#define   RSNA_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   RSNA_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   RSNA_BUFFER_TRC         BUFFER_TRC        

/* Trace definitions */
#ifdef  TRACE_WANTED

#define RSNA_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, pBuf, Length, (const char *)Str)

#define RSNA_TRC(TraceType, Str)                                              \
        MOD_TRC(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str)

#define RSNA_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str, Arg1)

#define RSNA_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define RSNA_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define RSNA_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)                       \
        MOD_TRC_ARG4(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#define RSNA_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)                       \
        MOD_TRC_ARG5(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5)

#define RSNA_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)                       \
        MOD_TRC_ARG6(RSNA_TRC_FLAG, TraceType, RSNA_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)


#define   RSNA_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define RSNA_PKT_DUMP(TraceType, pBuf, Length, Str)
#define RSNA_TRC(TraceType, Str)
#define RSNA_TRC_ARG1(TraceType, Str, Arg1)
#define RSNA_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define RSNA_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define RSNA_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)
#define RSNA_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)
#define RSNA_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)

#define   RSNA_DEBUG(x)

#endif /* TRACE_WANTED */

#endif/* _RSNATRC_H_ */

