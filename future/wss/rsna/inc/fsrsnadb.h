/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrsnadb.h,v 1.3 2017/11/24 10:37:05 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRSNADB_H
#define _FSRSNADB_H

UINT1 FsRSNAClearCountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRSNAExtTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRSNAConfigAuthenticationSuitesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
#ifdef WPA_WANTED
UINT1 FsRSNAConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRSNAConfigPairwiseCiphersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsRSNAStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWPAConfigAuthenticationSuitesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
#endif

UINT4 fsrsna [] ={1,3,6,1,4,1,29601,2,87};
tSNMP_OID_TYPE fsrsnaOID = {9, fsrsna};


UINT4 FsRSNATraceOption [ ] ={1,3,6,1,4,1,29601,2,87,1};
UINT4 FsRSNAClearAllCounters [ ] ={1,3,6,1,4,1,29601,2,87,2};
UINT4 FsRSNAClearCounter [ ] ={1,3,6,1,4,1,29601,2,87,3,1,2};
UINT4 FsRSNAOKCEnabled [ ] ={1,3,6,1,4,1,29601,2,87,4,1,1};
UINT4 FsRSNA4WayHandshakeCompletedStats [ ] ={1,3,6,1,4,1,29601,2,87,4,1,2};
UINT4 FsRSNAGroupHandshakeCompletedStats [ ] ={1,3,6,1,4,1,29601,2,87,4,1,3};
#ifdef PMF_WANTED
UINT4 FsRSNAAssociationComeBackTime [ ] ={1,3,6,1,4,1,29601,2,87,4,1,4};
#endif
#ifdef WPA_WANTED
UINT4 FsRSNAConfigAuthenticationSuiteIndex [ ] ={1,3,6,1,4,1,29601,2,87,5,1,1};
UINT4 FsRSNAConfigAuthenticationSuiteImplemented [ ] ={1,3,6,1,4,1,29601,2,87,5,1,2};
#endif
UINT4 FsRSNAConfigAuthenticationSuiteActivated [ ] ={1,3,6,1,4,1,29601,2,87,5,1,3};
#ifdef WPA_WANTED
UINT4 FsRSNAConfigVersion [ ] ={1,3,6,1,4,1,29601,2,87,6,1,1};
UINT4 FsRSNAConfigGroupCipher [ ] ={1,3,6,1,4,1,29601,2,87,6,1,2};
UINT4 FsRSNAConfigGroupRekeyMethod [ ] ={1,3,6,1,4,1,29601,2,87,6,1,3};
UINT4 FsRSNAConfigGroupRekeyTime [ ] ={1,3,6,1,4,1,29601,2,87,6,1,4};
UINT4 FsRSNAConfigGroupRekeyPackets [ ] ={1,3,6,1,4,1,29601,2,87,6,1,5};
UINT4 FsRSNAConfigGroupRekeyStrict [ ] ={1,3,6,1,4,1,29601,2,87,6,1,6};
UINT4 FsRSNAConfigPSKValue [ ] ={1,3,6,1,4,1,29601,2,87,6,1,7};
UINT4 FsRSNAConfigPSKPassPhrase [ ] ={1,3,6,1,4,1,29601,2,87,6,1,8};
UINT4 FsRSNAConfigGroupUpdateCount [ ] ={1,3,6,1,4,1,29601,2,87,6,1,9};
UINT4 FsRSNAConfigPairwiseUpdateCount [ ] ={1,3,6,1,4,1,29601,2,87,6,1,10};
UINT4 FsRSNAConfigGroupCipherSize [ ] ={1,3,6,1,4,1,29601,2,87,6,1,11};
UINT4 FsRSNAConfigPMKLifetime [ ] ={1,3,6,1,4,1,29601,2,87,6,1,12};
UINT4 FsRSNAConfigPMKReauthThreshold [ ] ={1,3,6,1,4,1,29601,2,87,6,1,13};
UINT4 FsRSNAConfigNumberOfPTKSAReplayCountersImplemented [ ] ={1,3,6,1,4,1,29601,2,87,6,1,14};
UINT4 FsRSNAConfigSATimeout [ ] ={1,3,6,1,4,1,29601,2,87,6,1,15};
UINT4 FsRSNAAuthenticationSuiteSelected [ ] ={1,3,6,1,4,1,29601,2,87,6,1,16};
UINT4 FsRSNAPairwiseCipherSelected [ ] ={1,3,6,1,4,1,29601,2,87,6,1,17};
UINT4 FsRSNAGroupCipherSelected [ ] ={1,3,6,1,4,1,29601,2,87,6,1,18};
UINT4 FsRSNAPMKIDUsed [ ] ={1,3,6,1,4,1,29601,2,87,6,1,19};
UINT4 FsRSNAAuthenticationSuiteRequested [ ] ={1,3,6,1,4,1,29601,2,87,6,1,20};
UINT4 FsRSNAPairwiseCipherRequested [ ] ={1,3,6,1,4,1,29601,2,87,6,1,21};
UINT4 FsRSNAGroupCipherRequested [ ] ={1,3,6,1,4,1,29601,2,87,6,1,22};
UINT4 FsRSNATKIPCounterMeasuresInvoked [ ] ={1,3,6,1,4,1,29601,2,87,6,1,23};
UINT4 FsRSNA4WayHandshakeFailures [ ] ={1,3,6,1,4,1,29601,2,87,6,1,24};
UINT4 FsRSNAConfigNumberOfGTKSAReplayCountersImplemented [ ] ={1,3,6,1,4,1,29601,2,87,6,1,25};
UINT4 FsRSNAConfigActivated [ ] ={1,3,6,1,4,1,29601,2,87,6,1,26};
UINT4 FsRSNAConfigMixMode [ ] ={1,3,6,1,4,1,29601,2,87,6,1,27};
UINT4 FsRSNAConfigPairwiseCipherIndex [ ] ={1,3,6,1,4,1,29601,2,87,7,1,1};
UINT4 FsRSNAConfigPairwiseCipherImplemented [ ] ={1,3,6,1,4,1,29601,2,87,7,1,2};
UINT4 FsRSNAConfigPairwiseCipherActivated [ ] ={1,3,6,1,4,1,29601,2,87,7,1,3};
UINT4 FsRSNAConfigPairwiseCipherSizeImplemented [ ] ={1,3,6,1,4,1,29601,2,87,7,1,4};
UINT4 FsRSNAStatsIndex [ ] ={1,3,6,1,4,1,29601,2,87,9,1,1};
UINT4 FsRSNAStatsSTAAddress [ ] ={1,3,6,1,4,1,29601,2,87,9,1,2};
UINT4 FsRSNAStatsVersion [ ] ={1,3,6,1,4,1,29601,2,87,9,1,3};
UINT4 FsRSNAStatsSelectedPairwiseCipher [ ] ={1,3,6,1,4,1,29601,2,87,9,1,4};
UINT4 FsRSNAStatsTKIPICVErrors [ ] ={1,3,6,1,4,1,29601,2,87,9,1,5};
UINT4 FsRSNAStatsTKIPLocalMICFailures [ ] ={1,3,6,1,4,1,29601,2,87,9,1,6};
UINT4 FsRSNAStatsTKIPRemoteMICFailures [ ] ={1,3,6,1,4,1,29601,2,87,9,1,7};
UINT4 FsRSNAStatsTKIPReplays [ ] ={1,3,6,1,4,1,29601,2,87,9,1,8};
UINT4 FsWPAConfigAuthenticationSuiteIndex [ ] ={1,3,6,1,4,1,29601,2,87,10,1,1};
UINT4 FsWPAConfigAuthenticationSuiteImplemented [ ] ={1,3,6,1,4,1,29601,2,87,10,1,2};
UINT4 FsWPAConfigAuthenticationSuiteActivated [ ] ={1,3,6,1,4,1,29601,2,87,10,1,3};
#endif



tMbDbEntry fsrsnaMibEntry[]= {

{{10,FsRSNATraceOption}, NULL, FsRSNATraceOptionGet, FsRSNATraceOptionSet, FsRSNATraceOptionTest, FsRSNATraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRSNAClearAllCounters}, NULL, FsRSNAClearAllCountersGet, FsRSNAClearAllCountersSet, FsRSNAClearAllCountersTest, FsRSNAClearAllCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsRSNAClearCounter}, GetNextIndexFsRSNAClearCountersTable, FsRSNAClearCounterGet, FsRSNAClearCounterSet, FsRSNAClearCounterTest, FsRSNAClearCountersTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAClearCountersTableINDEX, 1, 0, 0, "0"},

{{12,FsRSNAOKCEnabled}, GetNextIndexFsRSNAExtTable, FsRSNAOKCEnabledGet, FsRSNAOKCEnabledSet, FsRSNAOKCEnabledTest, FsRSNAExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAExtTableINDEX, 1, 0, 0, "1"},

{{12,FsRSNA4WayHandshakeCompletedStats}, GetNextIndexFsRSNAExtTable, FsRSNA4WayHandshakeCompletedStatsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRSNAExtTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAGroupHandshakeCompletedStats}, GetNextIndexFsRSNAExtTable, FsRSNAGroupHandshakeCompletedStatsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRSNAExtTableINDEX, 1, 0, 0, NULL},
#ifdef PMF_WANTED
{{12,FsRSNAAssociationComeBackTime}, GetNextIndexFsRSNAExtTable, FsRSNAAssociationComeBackTimeGet, FsRSNAAssociationComeBackTimeSet, FsRSNAAssociationComeBackTimeTest, FsRSNAExtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAExtTableINDEX, 1, 0, 0, "2000"},
#endif
#ifdef WPA_WANTED
{{12,FsRSNAConfigAuthenticationSuiteIndex}, GetNextIndexFsRSNAConfigAuthenticationSuitesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRSNAConfigAuthenticationSuitesTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAConfigAuthenticationSuiteImplemented}, GetNextIndexFsRSNAConfigAuthenticationSuitesTable, FsRSNAConfigAuthenticationSuiteImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigAuthenticationSuitesTableINDEX, 2, 0, 0, NULL},
#endif
{{12,FsRSNAConfigAuthenticationSuiteActivated}, GetNextIndexFsRSNAConfigAuthenticationSuitesTable, FsRSNAConfigAuthenticationSuiteActivatedGet, FsRSNAConfigAuthenticationSuiteActivatedSet, FsRSNAConfigAuthenticationSuiteActivatedTest, FsRSNAConfigAuthenticationSuitesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAConfigAuthenticationSuitesTableINDEX, 2, 0, 0, NULL},

#ifdef WPA_WANTED
{{12,FsRSNAConfigVersion}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigGroupCipher}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupCipherGet, FsRSNAConfigGroupCipherSet, FsRSNAConfigGroupCipherTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigGroupRekeyMethod}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupRekeyMethodGet, FsRSNAConfigGroupRekeyMethodSet, FsRSNAConfigGroupRekeyMethodTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "2"},

{{12,FsRSNAConfigGroupRekeyTime}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupRekeyTimeGet, FsRSNAConfigGroupRekeyTimeSet, FsRSNAConfigGroupRekeyTimeTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "86400"},

{{12,FsRSNAConfigGroupRekeyPackets}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupRekeyPacketsGet, FsRSNAConfigGroupRekeyPacketsSet, FsRSNAConfigGroupRekeyPacketsTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigGroupRekeyStrict}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupRekeyStrictGet, FsRSNAConfigGroupRekeyStrictSet, FsRSNAConfigGroupRekeyStrictTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigPSKValue}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigPSKValueGet, FsRSNAConfigPSKValueSet, FsRSNAConfigPSKValueTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigPSKPassPhrase}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigPSKPassPhraseGet, FsRSNAConfigPSKPassPhraseSet, FsRSNAConfigPSKPassPhraseTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigGroupUpdateCount}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupUpdateCountGet, FsRSNAConfigGroupUpdateCountSet, FsRSNAConfigGroupUpdateCountTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "3"},

{{12,FsRSNAConfigPairwiseUpdateCount}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigPairwiseUpdateCountGet, FsRSNAConfigPairwiseUpdateCountSet, FsRSNAConfigPairwiseUpdateCountTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "3"},

{{12,FsRSNAConfigGroupCipherSize}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigGroupCipherSizeGet, FsRSNAConfigGroupCipherSizeSet, FsRSNAConfigGroupCipherSizeTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigPMKLifetime}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigPMKLifetimeGet, FsRSNAConfigPMKLifetimeSet, FsRSNAConfigPMKLifetimeTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "43200"},

{{12,FsRSNAConfigPMKReauthThreshold}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigPMKReauthThresholdGet, FsRSNAConfigPMKReauthThresholdSet, FsRSNAConfigPMKReauthThresholdTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "70"},

{{12,FsRSNAConfigNumberOfPTKSAReplayCountersImplemented}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigNumberOfPTKSAReplayCountersImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, "3"},

{{12,FsRSNAConfigSATimeout}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigSATimeoutGet, FsRSNAConfigSATimeoutSet, FsRSNAConfigSATimeoutTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, "60"},

{{12,FsRSNAAuthenticationSuiteSelected}, GetNextIndexFsRSNAConfigTable, FsRSNAAuthenticationSuiteSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAPairwiseCipherSelected}, GetNextIndexFsRSNAConfigTable, FsRSNAPairwiseCipherSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAGroupCipherSelected}, GetNextIndexFsRSNAConfigTable, FsRSNAGroupCipherSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAPMKIDUsed}, GetNextIndexFsRSNAConfigTable, FsRSNAPMKIDUsedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAAuthenticationSuiteRequested}, GetNextIndexFsRSNAConfigTable, FsRSNAAuthenticationSuiteRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAPairwiseCipherRequested}, GetNextIndexFsRSNAConfigTable, FsRSNAPairwiseCipherRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAGroupCipherRequested}, GetNextIndexFsRSNAConfigTable, FsRSNAGroupCipherRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNATKIPCounterMeasuresInvoked}, GetNextIndexFsRSNAConfigTable, FsRSNATKIPCounterMeasuresInvokedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNA4WayHandshakeFailures}, GetNextIndexFsRSNAConfigTable, FsRSNA4WayHandshakeFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigNumberOfGTKSAReplayCountersImplemented}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigNumberOfGTKSAReplayCountersImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAConfigTableINDEX, 1, 0, 0, "3"},

{{12,FsRSNAConfigActivated}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigActivatedGet, FsRSNAConfigActivatedSet, FsRSNAConfigActivatedTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigMixMode}, GetNextIndexFsRSNAConfigTable, FsRSNAConfigMixModeGet, FsRSNAConfigMixModeSet, FsRSNAConfigMixModeTest, FsRSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsRSNAConfigPairwiseCipherIndex}, GetNextIndexFsRSNAConfigPairwiseCiphersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAConfigPairwiseCipherImplemented}, GetNextIndexFsRSNAConfigPairwiseCiphersTable, FsRSNAConfigPairwiseCipherImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAConfigPairwiseCipherActivated}, GetNextIndexFsRSNAConfigPairwiseCiphersTable, FsRSNAConfigPairwiseCipherActivatedGet, FsRSNAConfigPairwiseCipherActivatedSet, FsRSNAConfigPairwiseCipherActivatedTest, FsRSNAConfigPairwiseCiphersTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAConfigPairwiseCipherSizeImplemented}, GetNextIndexFsRSNAConfigPairwiseCiphersTable, FsRSNAConfigPairwiseCipherSizeImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsIndex}, GetNextIndexFsRSNAStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsSTAAddress}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsVersion}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsSelectedPairwiseCipher}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsSelectedPairwiseCipherGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsTKIPICVErrors}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsTKIPICVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsTKIPLocalMICFailures}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsTKIPLocalMICFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsTKIPRemoteMICFailures}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsTKIPRemoteMICFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsRSNAStatsTKIPReplays}, GetNextIndexFsRSNAStatsTable, FsRSNAStatsTKIPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRSNAStatsTableINDEX, 2, 0, 0, NULL},

{{12,FsWPAConfigAuthenticationSuiteIndex}, GetNextIndexFsWPAConfigAuthenticationSuitesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsWPAConfigAuthenticationSuitesTableINDEX, 2, 0, 0, NULL},

{{12,FsWPAConfigAuthenticationSuiteImplemented}, GetNextIndexFsWPAConfigAuthenticationSuitesTable, FsWPAConfigAuthenticationSuiteImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWPAConfigAuthenticationSuitesTableINDEX, 2, 0, 0, NULL},

{{12,FsWPAConfigAuthenticationSuiteActivated}, GetNextIndexFsWPAConfigAuthenticationSuitesTable, FsWPAConfigAuthenticationSuiteActivatedGet, FsWPAConfigAuthenticationSuiteActivatedSet, FsWPAConfigAuthenticationSuiteActivatedTest, FsWPAConfigAuthenticationSuitesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWPAConfigAuthenticationSuitesTableINDEX, 2, 0, 0, NULL},

#endif

};
#if defined (WPA_WANTED) && defined (PMF_WANTED)
tMibData fsrsnaEntry = { 52, fsrsnaMibEntry };
#elif WPA_WANTED
tMibData fsrsnaEntry = { 51, fsrsnaMibEntry };
#elif PMF_WANTED
tMibData fsrsnaEntry = { 8, fsrsnaMibEntry };
#else
tMibData fsrsnaEntry = { 7, fsrsnaMibEntry };
#endif

#endif /* _FSRSNADB_H */

