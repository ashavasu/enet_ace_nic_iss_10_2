#!/bin/csh
# (C) 2002 Future Software Pvt. Ltd.
#/*$Id: make.h,v 1.2 2017/05/23 14:16:53 siva Exp $*/
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Future Software Pvt. Ltd.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS = $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

RSNA_BASE_DIR      = ${BASE_DIR}/wss/rsna
WPS_BASE_DIR      = ${BASE_DIR}/wss/wps
PNAC_BASE_DIR      = ${BASE_DIR}/pnac
WLAN_BASE_DIR      = ${BASE_DIR}/wss
RSNA_SRC_DIR       = ${RSNA_BASE_DIR}/src
WPS_INC_DIR       = ${WPS_BASE_DIR}/inc
RSNA_INC_DIR       = ${RSNA_BASE_DIR}/inc
RSNA_OBJ_DIR       = ${RSNA_BASE_DIR}/obj
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
PNAC_INC_DIR       = ${PNAC_BASE_DIR}/inc
WLAN_INC_DIR       = ${WLAN_BASE_DIR}/wsswlan/inc
WSSSTA_INC_DIR     = ${WLAN_BASE_DIR}/wsssta/inc
WSSIF_INC_DIR      = ${WLAN_BASE_DIR}/wssif/inc

OPENSSL_INC_DIR    = ${BASE_DIR}/../opensource/v1_0_1c/ssl/crypto

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${RSNA_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${PNAC_INC_DIR} -I${WLAN_INC_DIR} -I${WSSSTA_INC_DIR} -I${WSSIF_INC_DIR} -I${WPS_INC_DIR} -I${OPENSSL_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} -I$(BASE_DIR)/util/aes

#############################################################################
