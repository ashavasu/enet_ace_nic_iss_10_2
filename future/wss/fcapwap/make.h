#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                    ####
# $Id: make.h,v 1.2 2017/05/23 14:16:50 siva Exp $#
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved          ####
#####################################################################
##|                                                               |##
##|###############################################################|##
##|                                                               |##
##|    FILE NAME               ::  make.h                         |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.                   |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  WSS                            |##
##|                                                               |##
##|    MODULE NAME             ::  FCAPWAP                        |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  LINUX                          |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  04 Jun 2014                    |##
##|                                                               |##
##|    DESCRIPTION             ::  Include file for the Fcapwap   |##
##|                                Makefile.                      |##
##|                                                               |##
##|###############################################################|##
#!/bin/csh

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

TOTAL_OPNS = ${CAPWAP_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

FCAPWAP_BASE_DIR     = ${BASE_DIR}/wss/fcapwap
FCAPWAP_SRC_DIR   = ${FCAPWAP_BASE_DIR}/src
FCAPWAP_INC_DIR   = ${FCAPWAP_BASE_DIR}/inc
FCAPWAP_OBJ_DIR   = ${FCAPWAP_BASE_DIR}/obj
CMN_INC_DIR    = ${BASE_DIR}/inc
CFA_INC_DIR          = ${BASE_DIR}/cfa2/inc
VLAN_INC_DIR          = ${BASE_DIR}/vlangarp/vlan/inc
GARP_INC_DIR          = ${BASE_DIR}/vlangarp/garp/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES      = -I${CMN_INC_DIR} -I${FCAPWAP_INC_DIR} -I${CFA_INC_DIR} -I${VLAN_INC_DIR} -I${GARP_INC_DIR}
INCLUDES    = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

############################################################################
