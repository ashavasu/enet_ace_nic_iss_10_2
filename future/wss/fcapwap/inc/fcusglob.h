/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcusglob.h,v 1.5 2017/11/30 14:01:37 siva Exp $
 *
 * Description:This file contains the defintions for fast capwap
 *
 *******************************************************************/

#ifndef __FCUS_GLOB_H__
#define __FCUS_GLOB_H__

#include "lr.h"
#include "cfa.h"

#include <netdb.h>
#include <netinet/in.h>
#include <net/if.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if_arp.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <wssifcapdb.h>


/******************************************************************************
 *                             Macros
 *******************************************************************************/
#define    MAX_NUM_NODES    10000
#define    INTERFACE_LEN    CFA_MAX_PORT_NAME_LENGTH

#define    RB_CREATE                  100
#define    RB_DISPLAY                 101
#define    RB_INSERT                  102
#define    RB_REMOVE                  103
#define    RB_REPLACE                 104
#define    RB_DESTROY                 105
#define    RB_STATION_STATUS          107

#define TX_MODULE 1
#define RX_MODULE 2
#define DSCP_MODULE 3

#ifdef WTP_WANTED
#define AP_GLOBALS          1
#define AP_STATION_TABLE    2   
#define AP_STATION_DESTROY  3   
#define AP_ADD_DEVICE       4
#define AP_REMOVE_DEVICE    5
#define AP_STATION_STATUS 6
#define AP_GLOBAL_WLC_MAC 7
#define    AP_QOS_TABLE            9
#define    DEBUG_INFO              11

#endif

#ifdef WLC_WANTED
#define    AP_IP_TABLE             1
#define    AP_INT_TABLE            2
#define    AP_MAC_TABLE            3
#define    AP_STA_TABLE            4
#define    VLAN_TABLE              5
#define    MAC_TABLE               6
#define    SOCKET_INFO             7
#define    STA_IP_TABLE            8
#define    STATION_STATUS          10
#define    AP_QOS_TABLE            9
#define    DEBUG_INFO              11
#define    WLC_IP_ADDR             12

#define ADD_VLAN    1
#define DELETE_VLAN 2
#endif

#define BUF_LEN (4096)
#define PORT_NUM 5000

#define DSCP_DEBUG_PACKETS                      0x01
#define DSCP_DEBUG_CONFIGURAION                 0x02
#define DSCP_DEBUG_ALL                          0x03

#ifdef WTP_WANTED
/******************************************************************************
 *                     RB Interface Structure for WTP
 *******************************************************************************/
extern UINT1   gau1InterfaceName[INTERFACE_LEN];
extern UINT1   gau1TcFilterInterface[INTERFACE_LEN];
typedef struct
{
    UINT2       u2VlanId;
    tMacAddr    staMac;
    tMacAddr    bssid;
    UINT1       u1StationStatus;
    UINT1       u1WebAuthEnable;
    UINT1       u1WebAuthStatus;
    UINT1       u1WMMEnable;
    UINT1       wlanIntf[INTERFACE_LEN];
    UINT1       u1TaggingPolicy;
    UINT1       u1TrustMode;
    UINT1       u1QosProfilePriority;
    UINT1       u1MacType;
    UINT2       u2TcpPort;
    UINT1       u1ExternalWebAuth;
    UINT1       au1Pad[3];
}tWlanStaInfo;

typedef struct
{
    UINT1           au1InterfaceName[INTERFACE_LEN];
    UINT1           u1dscpin;
    UINT1           u1dscpout;
    UINT1           au1pad[2];
}tDscpQosData;

/* structure for rbtree*/
typedef struct
{
    tWlanStaInfo    staData;
    tDscpQosData    qosData;
    UINT4           u4WlcIpAddr;
    UINT4           u4WtpIpAddr;
    UINT2           u2WlcDataPort;
    UINT2           u2WtpDataPort;
    tMacAddr        WlcMacAddr;
    UINT1           u1MacType;
    UINT1           u1LocalRouting;
    UINT1           au1Interface[INTERFACE_LEN];
    UINT1           au1VlanInterface[INTERFACE_LEN];
    UINT1           au1DeviceName[INTERFACE_LEN];
    INT4            i4CapwapPortMtu;
    INT4             i4WssDataDebugMask;
    UINT2            u2VlanId;
    UINT1            u1ExternalWebAuth;
    UINT1            u1Pad;
}tRbTreeTypeWtp;

/*ioctl handle*/
typedef struct
{
    UINT1            u1RbOperation;
    INT4             i4TableNum;
    INT4             i4RbNodes;
    tRbTreeTypeWtp   rbData;
    tRbTreeTypeWtp   replaceData;
}tWtpKernelDB;
#endif

#ifdef WLC_WANTED
/******************************************************************************
 *                     RB Interface Structure for WLC Send
 *******************************************************************************/
typedef struct
{
    UINT4    u4ipAddr;
    UINT4    u4UdpPort;
    UINT4    u4PathMTU;
    UINT1    u1macType;
    UINT1    u1FragReassembleStatus;  
    UINT2    u2FragId;
    tCapFragment    CapDataFragStream[20];
    UINT1    u1QInitialized;
    UINT1    au1Pad[3];
}tApIpSnd;

typedef struct
{
    tMacAddr     bssid;
    UINT2        u2VlanId;
    UINT1        u1SsidIsolation;
    UINT1        au1Pad[3];
}tApIntSnd;

typedef struct
{
    UINT2    u2VlanId;
    UINT1    au1PhysicalPort[INTERFACE_LEN];
    UINT1    u1Tagged;
    UINT1    au1Pad[1];
    
}tVlanData;

typedef struct
{
    tMacAddr au1MacAddress;
    UINT2    u2VlanId;
    UINT1    au1PhysicalInterface[INTERFACE_LEN];
    UINT1    u1WlanFlag;
    UINT1    au1Pad[3];
    
}tMacData;

typedef struct
{
    UINT4    u4ipAddr;
    UINT4    u4UdpPort;
    tMacAddr apMacAddr;
    UINT2    u2FragId;
    UINT4    u4PathMTU;
    UINT1    u1macType;
    UINT1    u1FragReassembleStatus; 
    UINT1    au1Pad[2];
}tApIpRcv;

typedef struct
{
   tMacAddr staMac;
   tMacAddr bssid;
   UINT4   u4ipAddr;
   UINT4   u4StaIpAddr;
   UINT1   u1StationStatus;
   UINT1   u1WebAuthEnable;
   UINT1   u1WebAuthStatus;
   UINT1   u1WMMEnable;
   tMacAddr wtpMac;
   UINT1   u1TaggingPolicy;
   UINT1   u1TrustMode;
   UINT1   u1QosProfilePriority;
   UINT1    u1LocalRouting;
   UINT1   au1Pad[1];
}tApStaRcv;

typedef struct
{
   tMacAddr staMac;
   tMacAddr bssid;
   UINT4   u4ipAddr;
   UINT4   u4StaIpAddr;
   UINT1   u1StationStatus;
   UINT1   u1WebAuthEnable;
   UINT1   u1WebAuthStatus;
   UINT1   u1WMMEnable;
   tMacAddr wtpMac;
   UINT1   u1TaggingPolicy;
   UINT1   u1TrustMode;
   UINT1   u1QosProfilePriority;
   UINT1   au1Pad[2];
}tApStaSnd;


typedef struct
{
    UINT4       u4StaIp;
    tMacAddr    staMac;
    UINT1       bssid[6];
    UINT4       u4ipAddr;
    UINT1       u1WMMEnable;
    UINT1       u1TaggingPolicy;
    UINT1       u1TrustMode;
    UINT1       u1QosProfilePriority;
}tStaIpData;

typedef struct
{
    UINT1           au1InterfaceName[INTERFACE_LEN];
    UINT1           u1dscpin;
    UINT1           u1dscpout;
    UINT1           au1pad[2];
}tDscpQosData;

/* structure for rbtree*/
typedef struct
{
    INT4             i4CtrlUdpPort;
    INT4             i4WssDataDebugMask;
    UINT4      u4WlcIpAddr;
    union {
        tApIpSnd          apIpData;
        tApIntSnd         apIntData;
        tApIpRcv          apMacData;
        tApStaRcv         apStaData;
        tApStaRcv         apStaInfo;
        tMacData          MacData;
        tVlanData         VlanData;
        tStaIpData        StaIpData;
        tDscpQosData      qosData;
    }unTable;
}tRbTreeTypeWlc;

/*ioctl handle*/
typedef struct
{
    UINT1            u1RbOperation;
    INT4             i4TableNum;
    INT4             i4RbNodes;
    tRbTreeTypeWlc   rbData;
    tRbTreeTypeWlc   replaceData;
    tMacAddr         au1VlanMac;
    UINT1            au1Pad[2];
    
}tHandleRbWlc;

#endif

#endif

