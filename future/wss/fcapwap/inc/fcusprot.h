/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcusprot.h,v 1.4 2017/05/23 14:16:50 siva Exp $
 *
 * Description:This file contains the defintions for fast capwap
 *
 *******************************************************************/

#ifndef __CAP_KERN_PROT_H__
#define __CAP_KERN_PROT_H__

#include "fcusglob.h"
INT4 
WssWtpUpdateKernelDB PROTO ((UINT1, UINT1, UINT4, tMacAddr ));


INT4 
WssStaUpdateKernelDB PROTO ((UINT1, UINT1, tMacAddr ));

INT4
WssStaRemoveKernelDB PROTO ((tMacAddr));

VOID
WssStaRemoveAllKernelDB PROTO ((VOID));

INT4 
WssQosUpdateKernelDB PROTO ((tCapwapDscpMapTable *));

INT4
WssQosRemoveKernelDB PROTO ((tCapwapDscpMapTable *));

VOID
WssQosDestroyKernelDB PROTO ((VOID));

INT4
CapwapUpdateKernelIpAndMacTable PROTO ((UINT4, UINT4, UINT1, tMacAddr, INT4, UINT1, UINT4));

INT4
CapwapUpdateKernelIntfDB PROTO ((tMacAddr, UINT2,UINT1));

INT4
CapwapRemoveKernelIpAndMacTable PROTO ((UINT4 ));

INT4
CapwapKernelUpdateVlanTable PROTO ((UINT2, UINT1 *, UINT1));

INT4
CapwapKernelReplaceVlanTable PROTO ((UINT2, UINT1*, UINT1));

INT4
CapwapKernelRemoveVlanTable PROTO ((UINT2, UINT1 *));

INT4
CapwapKernelUpdateMacTable PROTO ((tMacAddr, UINT1 *, UINT1, UINT2));

INT4
CapwapKernelRemoveMacTable PROTO ((tMacAddr));

VOID
CapwapUpdateMacDB PROTO ((tMacAddr, UINT2, UINT2));

VOID
CapwapInitVlanDB PROTO ((VOID));

VOID
CapwapUpdateVlanEgressPorts PROTO ((UINT4, UINT1 *, UINT1));

VOID
CapwapUpdateVlanUnTaggedPorts PROTO ((UINT4, UINT1 *, UINT1));

VOID
CapwapUpdateArpTable PROTO ((UINT4, UINT1 *));

VOID
CapwapRemoveArpTable PROTO ((UINT4, UINT1 *));

VOID 
CwpKernInit PROTO ((VOID));

VOID
NpResolveArp PROTO ((UINT4 , tMacAddr *));

INT4
CapwapUpdateKernelUsrDeletionStaTable PROTO ((tMacAddr));

INT4
CapwapReplaceKernelStaTable PROTO ((tMacAddr));

INT4
CapwapRemoveKernelStaTable PROTO ((tMacAddr));

INT4
CapwapUpdateKernelStationIPTable PROTO ((tMacAddr, UINT4));

INT4
CapwapRemoveKernelStationIPTable PROTO ((UINT4));

INT4
CapwapRemoveKernelIntfDB PROTO ((tMacAddr));

INT4 FcusGetStationIdleSatus PROTO((tMacAddr));

INT4
WssIfSendCmdToTc(UINT1 *pu1Cmd);

#ifdef WTP_WANTED
INT4
capwapNpUpdateKernel PROTO ((UINT1, UINT2, tWtpKernelDB *));

INT4
capwapNpKernelRemove PROTO ((UINT1, UINT2, tWtpKernelDB *));

#endif
VOID
WssIfWmmInstallFilter PROTO ((UINT1 *pu1IntfName));

#ifdef WLC_WANTED
INT4
capwapNpUpdateKernelDBWlc PROTO ((UINT1, UINT2, tHandleRbWlc *));

INT4
capwapNpRemoveKernelDBWlc PROTO ((UINT1, UINT2, tHandleRbWlc *));

INT4
capwapNpReplaceKernelDBWlc PROTO ((UINT1, UINT2, tHandleRbWlc *));

INT4
CapwapUpdateKernelDebugInfo PROTO ((VOID));
#endif

#endif
