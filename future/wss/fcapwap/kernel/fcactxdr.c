
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcactxdr.c,v 1.5 2018/01/22 09:39:42 siva Exp $
 *
 * Description: This file contains driver related API for SND module in WLC
 *******************************************************************/
#ifndef _FCAC_TXDR_C_
#define _FCAC_TXDR_C_

/******************************************************************************
 *                           Local Include
 *****************************************************************************/
#include "fcksglob.h"
#include "fcksprot.h"

extern INT4         gi4CtrlUdpPort;
extern INT4         gi4DataUdpPort;
extern INT4         gu4WlcIpAddr;
UINT1               gau1VlanMac[6];

static tOsixCfg     LrOsixCfg;
static tMemPoolCfg  LrMemPoolCfg;
static tBufConfig   LrBufConfig;
UINT4               gFacMemPoolId;
UINT4               gu4SysTimeTicks;
static struct nf_hook_ops nfho_pre;
static struct nf_hook_ops nfho_pre_last;
static INT4         FcapwapWlcTxIoctl (struct file *filep, UINT4 u4Cmd,
                                       FS_ULONG arg);
void                FcapwapGetBssIdFromDot11Pkt (UINT1 *pu1ProcBuff,
                                                 UINT1 *pu1BssId);
static UINT1        verifyDot3FrameReceived (UINT1 *pu1Buf);

UINT1               gu1DropWanted = 0;

UINT2               IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size);
static struct timer_list my_timer;
UINT1               gu1TimerStarted = 0;
INT4                i4MacType = LOCAL_MAC;
UINT4               gu4PriorityMapping[USER_PRIORITY][PRIORITY_LAYER] =
    { {0, 0}, {1, 10}, {2, 18}, {0, 0}, {3, 26}, {4, 34}, {5, 46}, {6, 48} };
UINT4               gu4ProfilePriorityMapping[ACCESS_CATEGORIES][PRIORITY_LAYER]
    = { {0, 18}, {4, 34}, {5, 46}, {1, 10} };
UINT1               gu1DebugPrintEnable = 0;

/*struct sk_buff *gpSkb = NULL;*/
/*static int bit  = 0;*/
static VOID         FcapwapProcessReassmTimeExpiry (FS_ULONG arg);
/******************************************************************************
 *                           Global variables
 *****************************************************************************/
static struct file_operations file_opts = {
    .unlocked_ioctl = FcapwapWlcTxIoctl,
    .open = FcapwapWlcTxDeviceOpen,
    .release = FcapwapWlcTxDeviceRelease
};

/******************************************************************************
 *  Function Name          : init_module
 *  Description            : This function is used to initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
static __init INT4
FcapwapWlcDrvTxInit (void)
{
    INT4                i4RetVal = 0;
    UINT4               u4SelfNodeId = SELF;

    /*register character device */
    i4RetVal =
        register_chrdev (MAJOR_NUMBER_AC_SND, DEVICE_NAME_AC_SND, &file_opts);
    if (i4RetVal < 0)
        printk (KERN_ERR "init_module: %s registration failed !\n",
                DEVICE_NAME_AC_SND);
    else
        printk (KERN_INFO "init_module: %s successfully registered!!\n",
                DEVICE_NAME_AC_SND);

    nfho_pre.hook = FcapwapWlcHookPreRouting;
    nfho_pre.owner = THIS_MODULE;
    nfho_pre.hooknum = NF_INET_PRE_ROUTING;    /* First hook for IPv4 */
    nfho_pre.pf = PF_INET;
    nfho_pre.priority = NF_IP_PRI_FIRST;    /* Make our function first */

    nf_register_hook (&nfho_pre);

#if DEBUG_WANTED
    nfho_pre_last.hook = FcapwapWlcHookPreRoutingLast;
    nfho_pre_last.owner = THIS_MODULE;
    nfho_pre_last.hooknum = NF_INET_PRE_ROUTING;    /* First hook for IPv4 */
    nfho_pre_last.pf = PF_INET;
    nfho_pre_last.priority = NF_IP_PRI_LAST;

    nf_register_hook (&nfho_pre_last);
#endif
    /* Initialize Osix */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = u4SelfNodeId;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        return (1);
    }

    LrMemPoolCfg.u4MaxMemPools = 100;
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        return (1);
    }

/* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        return (1);
    }

    i4RetVal = (INT4) MemCreateMemPool (sizeof (tCapFragNode), 10,
                                        MEM_DEFAULT_MEMORY_TYPE,
                                        &gFacMemPoolId);

    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : cleanup_module
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : un-initializes the driver module
 *  Returns                : void 
 * ******************************************************************************/
static __exit void
FcapwapWlcDrvTxExit (void)
{
    /* Removing the registered timer */
    del_timer (&my_timer);
    printk (KERN_INFO "cleanup_module: unreg chardev\n");
    /* De-register the character driver */
    unregister_chrdev (MAJOR_NUMBER_AC_SND, DEVICE_NAME_AC_SND);

    nf_unregister_hook (&nfho_pre);
/*(    nf_unregister_hook (&nfho_pre_last);*/
    /* Mempools to be freed */
    printk (KERN_INFO "cleanup_module: %s successfully unregistered!!!\n",
            DEVICE_NAME_AC_SND);
}

module_init (FcapwapWlcDrvTxInit);
module_exit (FcapwapWlcDrvTxExit);

MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("CAPWAP in kernel layer");
MODULE_AUTHOR ("Aricent Group");

/******************************************************************************
 *  Function Name          : FcapwapWlcTxDeviceOpen
 *  Description            : This function is used to open the device
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device can be accessed
 *  Returns                : SUCCESS 
 * ******************************************************************************/
INT4
FcapwapWlcTxDeviceOpen (struct inode *inodep, struct file *filep)
{
#if DBG_LVL_01
    printk ("FcapwapWlcTxDeviceOpen: %s opened\n", DEVICE_NAME_AC_SND);
#endif
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcTxDeviceRelease
 *  Description            : This function is used to release the driver
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device cannot be accessed
 *  Returns                : SUCCESS
 * ******************************************************************************/
INT4
FcapwapWlcTxDeviceRelease (struct inode *inodep, struct file *filep)
{

#if DBG_LVL_01
    printk ("FcapwapWlcTxDeviceRelease: %s closed\n", DEVICE_NAME_AC_SND);
#endif
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcTxIoctl
 *  Description            : This function is used by user space to handle RBTree
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr, cmd - 
 *                           RB command, arg - RB tree arguments. 
 *  Output(s)              : rb tree creation, rb tree display
 *  Returns                : SUCCESS
 * ******************************************************************************/
static INT4
FcapwapWlcTxIoctl (struct file *filep, UINT4 u4Cmd, FS_ULONG arg)
{
    thandleRbWlcData    thandleRb;
    tAcSndStaTable     *StationTable = NULL;

    /*Copy user parameter from user space of Linux */
    if (copy_from_user (&thandleRb, (void *) arg, sizeof (thandleRbWlcData)))
    {
        printk (KERN_ERR "FcapwapWlcTxIoctl: copy from user failure\n");
        return -EFAULT;
    }

    /*Perform operation as per user information on RB Tree */
    switch (thandleRb.u1RbOperation)
    {
            /* create the mem */
        case CREATE_ENTRY:
            if (RbtCreate (thandleRb.i4RbNodes) != SUCCESS)
            {
                printk (KERN_ERR "FcapwapWlcTxIoctl: RbTree Create Failure\n");
                break;
            }
            memcpy (gau1VlanMac, thandleRb.au1VlanMac, MACADDR_SIZE);
            break;
        case DISPLAY_ENTRY:
            RbtDisplay (thandleRb.i4TableNum);
            break;
        case INSERT_ENTRY:
            if (RbtInsert (&thandleRb.rbData, thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR "FcapwapWlcTxIoctl: RbTree Insert Failure\n");
#if DBG_LVL_01
            RbtDisplay (thandleRb.i4TableNum);
#endif
            break;
        case REMOVE_ENTRY:
            if (RbtRemove (&thandleRb.rbData, thandleRb.i4TableNum) != SUCCESS)
#if DBG_LVL_01
                printk (KERN_ERR "FcapwapWlcTxIoctl: RB Tree Remove Failure\n");
#endif
            break;
        case REPLACE_ENTRY:
            break;
            if (RbtReplace (&thandleRb.rbData, &thandleRb.replaceData,
                            thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR
                        "FcapwapWlcTxIoctl: RB Tree Replace Failure\n");
            break;
        case DESTROY_ENTRY:
            if (RbtTxDestroy (thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR
                        "FcapwapWlcTxIoctl: RB Tree Replace Failure\n");
            break;
        case STATION_STATUS:

            StationTable =
                ApStaDBTableSearch (thandleRb.rbData.unTable.ApTxStaData.
                                    staMac);

            if (gu1DropWanted == 0)
            {
                gu1DropWanted = 1;
            }
            else
            {
                gu1DropWanted = 0;
            }

            if (StationTable != NULL)
            {
                thandleRb.rbData.unTable.ApTxStaData.u1StationStatus =
                    StationTable->StaInfo.u1StationStatus;
                if (copy_to_user
                    ((thandleRbWlcData *) arg, &thandleRb,
                     sizeof (thandleRbWlcData)))
                {
                    printk ("\n--->>copy_to_user failed\n");
                }
                StationTable->StaInfo.u1StationStatus = 0;
            }
            break;

        default:
            printk ("TX CASE : %d", thandleRb.u1RbOperation);

            break;
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcHookPreRouting 
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : hooknum - hook number
 *                           skb -  skb buffer
 *                           in  - input deviece
 *                           out - output device
 *  Returns                : SUCCESS/FAILURE
 * ******************************************************************************/

UINT4
FcapwapWlcHookPreRouting (UINT4 hooknum,
                          struct sk_buff *skb,
                          const struct net_device *in,
                          const struct net_device *out,
                          INT4 (*okfn) (struct sk_buff *))
{
    struct iphdr       *ih = NULL;
    struct iphdr       *iph = NULL;
    struct iphdr       *ipfh = NULL;
    struct sk_buff     *new_skb = NULL;
    INT4                i4Error = 0;
    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    INT4                i4DestIp = 0;
    UINT4               u4StaIp;
    UINT4               u4SrcIp;
    UINT4               u4DestIp;
    UINT4               u4InnerSrcIp;
    UINT4               u4InnerDestIp;
    UINT4               u4VlanTag;
    UINT4               u4Tos = 0;
    UINT2               u2Dstport, u2Srcport;
    UINT2               u2CheckSum = 0;
    UINT2               u2UdpLen = 0;
    UINT2               u2IpLen = 0;
    UINT2               u2Ethertype;
    UINT2               u2Pcp;
    UINT1               u1Val = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4Index = 0;
    UINT1               au1Bssid[MAC_ADDR_LEN];
    UINT1               au1SrcAddr[ETH_ALEN];
    UINT1               au1DestMacAddr[ETH_ALEN];
    UINT1               au1Buf[VLAN_NAME];
    UINT1               u1UdpProtocol = 0;
    UINT1               u1Value = 0;
    UINT1               u1TagStatus = 0;
    UINT1               au1BroadcastMac[MAC_ADDR_LEN] =
        { 0Xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    struct udphdr      *uh = NULL;
    struct udphdr      *udph = NULL;
    struct udphdr      *udpfh = NULL;
    tAcSndIpTable      *pRbData = NULL;
    tAcSndIpTable      *pRbDb = NULL;
    struct ethhdr      *eh;
    struct net_device  *dev;
    struct in_device   *in_dev;
    struct in_device   *out_dev;
    struct in_ifaddr   *if_info;
    struct net_device  *device;
    tAcSndStaIPTable   *rbData1 = NULL;
    tAcSndStaIPTable   *rbData2 = NULL;
    tAcSndIntfTable    *pRbDataIntf = NULL;
    tAcSndStaTable     *pStationDB = NULL;
    tAcSndStaTable     *pStaDB = NULL;
    tAcSndStaTable     *pStaDB1 = NULL;
    tAcSndStaTable     *pStaDB2 = NULL;
    tAcSndMacTable     *rbDataMac = NULL;
    tAcSndStaIPTable   *rbStaData = NULL;
    tAcSndStaIPTable   *rbData = NULL;
    tAcSndStaTable     *rbDataSta = NULL;
    UINT1               au1PhyIntf[INTERFACE_LEN];
    tCRU_BUF_CHAIN_HEADER *ppReassembleBuf = NULL;
    tAcSndApMacTable   *apSndMacDb = NULL;
    UINT2               u2frag = 0;
    UINT2               u2UdpPort = 0;
    static int          frag_check = 0;
    UINT2               u2TcpPort = 0;
    UINT4               u4Hdrlen = 0;
    UINT1               u1Flags = 0;

    if (skb_is_nonlinear (skb))
    {
        if (skb_linearize (skb) != 0)
        {
            printk ("Return value is %d\n", skb);
        }
    }

    memset (au1Buf, 0, VLAN_NAME);
    memset (au1PhyIntf, 0, INTERFACE_LEN);
    u4Protocol = (ip_hdr (skb))->protocol;
    u4Tos = (ip_hdr (skb))->tos;

    if (u4Protocol == UDP)
    {
        /* To get the ip header from buffer */
        ih = (struct iphdr *) ip_hdr (skb);
        u4SrcIp = ntohl (ih->saddr);
        u4DestIp = ntohl (ih->daddr);
        uh = (struct udphdr *) (skb->data + (ih->ihl * IHL_SIZE));
        u2Dstport = ntohs ((UINT2) uh->dest);
        u2Srcport = ntohs ((UINT2) uh->source);

        /* Added the condition to check AP and AC are running in the */
        /* same machine */
        if (u4SrcIp == u4DestIp)
        {
            return NF_ACCEPT;
        }
        rbData = StaIpTableSearch (u4SrcIp);
        if (rbData != NULL)
        {
            /* Packets destined to STA from server */
            /* Copy the station mac address from the recieved packet */
            rbDataSta = ApStaDBTableSearch (rbData->staData.staMac);

            /* If the mac address of the station is not present, 
             * transmit the packets adding the capwap header */
            if (rbDataSta != NULL)
            {
                /* Get the Destination IP (WTP IP) from the database */
                /* Since the database is queried, store the bssid too */
                i4DestIp = rbDataSta->StaInfo.u4ipAddr;

                if (i4DestIp == INADDR_LOOPBACK)
                {
                    return NF_ACCEPT;
                }
            }
        }
        if (u2Dstport == gi4DataUdpPort)
        {
            u1Val = (*(skb->data + CAPWAP_HDR_OFFSET + 1));    /*Move 28 (0x1c) bytes + 1, to compare with 0x00 0x20 */
            pRbData = ApIpTableSearch (u4SrcIp);

            if (pRbData != NULL)
            {
                if (pRbData->apIpData.u4UdpPort == u2Srcport)
                {

                    if ((pRbData->apIpData.u1macType != SPLIT_MAC)
                        && (u1Val == CAPWAP_BYTE))
                    {
                        memcpy (au1Bssid, skb->data + NETWORK_HDR_OFFSET +
                                TRANSPORT_HDR_SIZE + BSSID_OFFSET,
                                MACADDR_SIZE);

                        u1Value =
                            *(skb->data + NETWORK_HDR_OFFSET +
                              TRANSPORT_HDR_SIZE + 3);

                        pRbDataIntf = ApIntfTableSearch (au1Bssid);

                        if (pRbDataIntf != NULL)
                             /*RBDATAINERFACE*/
                        {
                            sprintf ((UINT1 *) au1Buf, "vlan%d",
                                     pRbDataIntf->apIntData.u2VlanId);

                            if ((u1Value & (0x80)) == 0x80)
                            {

                                if (pRbData->apIpData.u1FragReassembleStatus
                                    == FRAGMENTATION_ENABLE)
                                {
                                    skb_pull (skb,
                                              (NETWORK_HDR_OFFSET +
                                               TRANSPORT_HDR_SIZE));

                                    if ((verifyCapwapFragmentReceived (skb)) ==
                                        SUCCESS)
                                    {
                                        if (gu1DropWanted == 1)
                                        {
                                            /*  printk ("processing frag pkts\n"); */
                                            /* return NF_DROP; */
                                        }

                                        if ((CapwapDataFragReassemble
                                             (skb, &(pRbData->apIpData),
                                              skb->len,
                                              &ppReassembleBuf)) == FAIL)
                                        {
                                            return NF_STOLEN;

                                        }
                                        else
                                        {
                                            skb_push (skb,
                                                      (TRANSPORT_HDR_SIZE +
                                                       NETWORK_HDR_OFFSET));
                                            skb->network_header = skb->data;
                                            skb->mac_header =
                                                skb->data - MAC_HDR_OFFSET;
                                            skb->transport_header =
                                                skb->data + NETWORK_HDR_OFFSET;

                                            ipfh = ip_hdr (skb);
                                            ipfh->tot_len = htons (skb->len);
                                            ipfh->check = 0;
                                            u2CheckSum =
                                                IpCalcCheckSum (ipfh,
                                                                NETWORK_HDR_OFFSET);
                                            ipfh->check = u2CheckSum;

                                            udpfh =
                                                (struct udphdr *) (skb->data +
                                                                   (ipfh->ihl *
                                                                    IHL_SIZE));
                                            udpfh->len =
                                                htons (skb->len -
                                                       NETWORK_HDR_OFFSET);
                                            udpfh->check = 0;
                                            udpfh->check =
                                                csum_tcpudp_magic ((ipfh->
                                                                    saddr),
                                                                   ipfh->daddr,
                                                                   htons
                                                                   (udpfh->len),
                                                                   IPPROTO_UDP,
                                                                   csum_partial
                                                                   (udpfh,
                                                                    htons
                                                                    (udpfh->
                                                                     len), 0));

                                            if (udpfh->check == 0)
                                                udpfh->check = CSUM_MANGLED_0;

                                        }
                                    }
                                    else
                                    {
                                        /* BUG - Group all the kernel API's for pkt
                                         * processing (n/w hdr, mac hdr and transport
                                         * hdr) */
                                        skb_push (skb,
                                                  (NETWORK_HDR_OFFSET +
                                                   TRANSPORT_HDR_SIZE));
                                    }

                                }

                            }
                        }        /*END RBDATA INTERFACE */

                        if (skb->len > INNER_IP_HDR_OFFSET)
                        {
                            memcpy (au1SrcAddr, &skb->data[50], ETH_ALEN);
                            pStationDB = ApStaDBTableSearch (au1SrcAddr);
                            if (pStationDB != NULL)
                            {
                                /*Set the Station Status to 1 */
                                pStationDB->StaInfo.u1StationStatus =
                                    STATION_ACTIVE;
                                /* Check whether Webauth is enabled */
                                if (pStationDB->StaInfo.u1WebAuthEnable ==
                                    WEBAUTH_ENABLE)
                                {
                                    /* Check whether the station is
                                     * authenticated  */
                                    if (pStationDB->StaInfo.u1WebAuthStatus !=
                                        STA_AUTHENTICATED)
                                    {
                                        //u4Protocol = (UINT4) skb->data[67];

                                        skb_pull (skb, 58);

                                        skb->network_header = skb->data;
                                        skb->mac_header =
                                            skb->data - MAC_HDR_OFFSET;
                                        skb->transport_header =
                                            skb->data + NETWORK_HDR_OFFSET;

                                        u4Protocol = (ip_hdr (skb))->protocol;

                                        if (u4Protocol == IPPROTO_TCP)
                                        {
                                            u4Hdrlen = (ip_hdr (skb))->tot_len;

                                            iph =
                                                (struct iphdr *)
                                                skb_network_header (skb);

                                            if (iph != NULL)
                                            {
                                                u4DestIp = ntohl (iph->daddr);
                                                u4SrcIp = ntohl (iph->saddr);
                                            }

                                            MEMCPY (&u2TcpPort,
                                                    &(skb->data[22]), 2);

                                            /* When supporting tcp ports other
                                             * than default port below line has
                                             * to be modified*/

                                            /*if ((rbData_Sta->staData.u2TcpPort == ntohs(u2TcpPort)) || */
                                            if (ntohs (u2TcpPort) == 80)
                                            {
                                                /*Check whether the http packet is destined to WLC
                                                   If it is so, send it to user space */
                                                if ((u4DestIp & 0xFF000000) !=
                                                    (u4SrcIp & 0xFF000000))
                                                {
                                                    u1Flags =
                                                        skb->data[20 + 13];

                                                    if ((u1Flags == 0x02)
                                                        || (u1Flags == 0x10)
                                                        || (u1Flags == 0x12))
                                                    {
                                                        if ((dev =
                                                             dev_get_by_name
                                                             (&init_net,
                                                              au1Buf)) == NULL)
                                                        {
                                                            return NF_ACCEPT;
                                                        }
                                                        else
                                                        {
                                                            skb->dev = dev;
                                                            dev_put (dev);
                                                        }

                                                        return NF_ACCEPT;
                                                    }
                                                }
                                            }
                                        }
                                        skb_push (skb, 58);

                                        skb_reset_network_header (skb);
                                        skb_reset_mac_header (skb);
                                        skb_reset_transport_header (skb);

                                        /* If the station is not authenticated,
                                         * send the packet to user space for
                                         * processing */
                                        return NF_ACCEPT;
                                    }
                                }
                            }
                        }

                        skb_pull (skb, INNER_MAC_HDR_OFFSET);    /* Pull the packets till the mac header */
                        eh = (struct ethhdr *) skb->data;

                        /* If the ethertype is of PNAC, send the
                         * packets to user space with the CAPWAP
                         * header*/
                        if (ntohs (eh->h_proto) == PNAC_ETHERTYPE)
                        {
                            skb_push (skb, INNER_MAC_HDR_OFFSET);
                            /* Assign the network, mac and transport header accordingly */
                            return NF_ACCEPT;
                        }
                        /* If the ethertype is of PNAC, send the
                         * packets to user space with the CAPWAP
                         * header*/
                        if (ntohs (eh->h_proto) == ARP_ETHERTYPE)
                        {
                            skb_push (skb, INNER_MAC_HDR_OFFSET);    /* Pull the packets till the outer network header */

                            /* Assign the network, mac and transport header accordingly */
                            return NF_ACCEPT;
                        }

                        skb_pull (skb, MAC_HDR_OFFSET);    /* Pull the packets till the network header */

                        u2Dstport = 0;
                        u2Srcport = 0;
                        u4Protocol = 0;
                        skb->network_header = skb->data;

                        memcpy (&u4InnerDestIp, &skb->data[16], 4);
                        memcpy (&u4InnerSrcIp, &skb->data[12], 4);
                        u4Protocol = skb->data[9];
                        u4InnerDestIp = ntohl (u4InnerDestIp);
                        u4InnerSrcIp = ntohl (u4InnerSrcIp);

                        if (u4Protocol == 0x11)
                        {
                            memcpy (&u2Dstport, &skb->data[22], 2);
                            memcpy (&u2Srcport, &skb->data[20], 2);
                            u2Dstport = ntohs (u2Dstport);
                            u2Srcport = ntohs (u2Srcport);
                        }

                        /* If it is a dhcp packet, send the
                         * packets to user space with the CAPWAP
                         * header. It is done by verifying the
                         * source and destination port*/
                        if ((u4Protocol == UDP) && (u2Dstport == DHCP_DEST_PORT)
                            && (u2Srcport == DHCP_SRC_PORT))
                        {
                            skb_push (skb, INNER_IP_HDR_OFFSET);    /* Pull the packets till the outer network header */
                            skb_reset_network_header (skb);
                            return NF_ACCEPT;
                        }
                        /* If it is a packet destined to WLC IP, send the
                         * packets to user space with the CAPWAP
                         * header*/

                        for_each_netdev (&init_net, device)
                        {
                            if (device != NULL)
                            {
                                in_dev = (struct in_device *) device->ip_ptr;
                                if (in_dev != NULL)
                                {
                                    if_info = in_dev->ifa_list;
                                    if (if_info != NULL)
                                    {
                                        if (htonl (if_info->ifa_address) ==
                                            u4InnerDestIp)
                                        {
                                            skb_push (skb, INNER_IP_HDR_OFFSET);    /* Pull the packets till the outer network header */
                                            /* Assign the network, mac and transport header accordingly */
                                            skb_reset_network_header (skb);
                                            return NF_ACCEPT;
                                        }
                                    }
                                }
                            }
                        }

                        /* Get the src and destination ip from the inner ip
                         * header. If both the ip addresses are present in the
                         * station db. we have to swap the destination and src
                         * mac address, as well as the destination and src ip
                         * address*/
                        rbData1 = StaIpTableSearch (u4InnerDestIp);
                        rbData2 = StaIpTableSearch (u4InnerSrcIp);
                        if ((rbData1 != NULL) && (rbData2 != NULL))
                        {
                            if (pRbDataIntf->apIntData.u1SsidIsolation == 0)
                            {
                                pStaDB =
                                    ApStaDBTableSearch (rbData1->staData.
                                                        staMac);
                                if (pStaDB != NULL)
                                {
                                    skb_push (skb, INNER_IP_HDR_OFFSET);    /* Pull the packets till the outer network header */
                                    /* Assign the network, mac and transport header accordingly */
                                    skb->network_header = skb->data;
                                    skb->mac_header =
                                        skb->data - MAC_HDR_OFFSET;
                                    skb->transport_header =
                                        skb->data + NETWORK_HDR_OFFSET;

                                    iph = ip_hdr (skb);
                                    udph = (struct udphdr *) (skb->data +
                                                              (ih->ihl *
                                                               IHL_SIZE));

                                    iph->saddr = iph->daddr;
                                    iph->daddr =
                                        htonl (pStaDB->StaInfo.u4ipAddr);
                                    iph->check = 0;
                                    u2CheckSum =
                                        IpCalcCheckSum (iph,
                                                        NETWORK_HDR_OFFSET);
                                    iph->check = u2CheckSum;

                                    eh = eth_hdr (skb);
                                    memcpy (eh->h_source, eh->h_dest, ETH_ALEN);
                                    memcpy (eh->h_dest, pStaDB->StaInfo.wtpMac,
                                            ETH_ALEN);
                                    rbDataMac =
                                        Mac_Table_Search (pStaDB->StaInfo.
                                                          wtpMac);
                                    if (rbDataMac != NULL)
                                    {
                                        pRbDb =
                                            ApIpTableSearch (pStaDB->StaInfo.
                                                             u4ipAddr);
                                        if (pRbDb != NULL)
                                        {
                                            udph->source =
                                                htons (gi4DataUdpPort);
                                            /* To get the destination port from db */
                                            udph->dest =
                                                htons (pRbDb->apIpData.
                                                       u4UdpPort);
                                            udph->check = 0;
                                            udph->check =
                                                csum_tcpudp_magic ((iph->saddr),
                                                                   iph->daddr,
                                                                   htons (udph->
                                                                          len),
                                                                   IPPROTO_UDP,
                                                                   csum_partial
                                                                   (udph,
                                                                    htons
                                                                    (udph->len),
                                                                    0));
                                            if (udph->check == 0)
                                                udph->check = CSUM_MANGLED_0;
                                        }
                                        memcpy (au1PhyIntf,
                                                rbDataMac->MacData.
                                                au1PhysicalInterface,
                                                INTERFACE_LEN);
                                        if ((dev =
                                             dev_get_by_name (&init_net,
                                                              au1PhyIntf)) !=
                                            NULL)
                                        {
                                            skb->dev = dev;
                                            dev_put (dev);
                                            skb->pkt_type = PACKET_OUTGOING;
                                            eh->h_proto = htons (ETH_P_IP);
                                            skb_push (skb, MAC_HDR_OFFSET);
                                            i4Error = dev_queue_xmit (skb);
                                            if (i4Error)
                                            {
                                                printk (KERN_ERR
                                                        "sendToStation: data transmission failed = %d\n",
                                                        i4Error);
                                            }
                                            i4Error = net_xmit_eval (i4Error);
                                            return NF_STOLEN;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                /* Droping the packets between stations due to isolation enable */
                                return NF_STOLEN;
                            }
                        }
                        skb->mac_header = skb->data - MAC_HDR_OFFSET;
                        skb->transport_header = skb->data + NETWORK_HDR_OFFSET;

                        rbDataMac = Mac_Table_Search (eh->h_dest);

                        if (rbDataMac != NULL)
                        {
                            skb_push (skb, INNER_IP_HDR_OFFSET);
                            skb_reset_network_header (skb);
                            skb_reset_mac_header (skb);
                            skb_reset_transport_header (skb);
                            return NF_ACCEPT;
                        }
                        if ((dev = dev_get_by_name (&init_net, au1Buf)) == NULL)
                        {
                            return NF_ACCEPT;
                        }
                        else
                        {
                            skb->dev = dev;
                            dev_put (dev);
                        }
                    }
                    else
                    {
                        /* Split mac scenario, verify whether it is a capwap
                         * data packet*/
                        if (verifyCapwapDataPkt (skb->data + CAPWAP_HDR_OFFSET)
                            == DATA_PKT)
                        {
                            /* Check whether a dot3 frame is recieved */
                            if (verifyDot3FrameReceived
                                (skb->data + CAPWAP_HDR_OFFSET) != SUCCESS)
                            {
                                /* Check whethere it is a data packet, move the
                                 * offset to 28 + 8 bytes */
                                u1Val = 0;
                                u1Val =
                                    (*
                                     (skb->data + CAPWAP_HDR_OFFSET +
                                      CAPWAP_HDR_SPLIT_SIZE));
                                if (u1Val == DATA_BYTE)
                                {
                                    /* Moving to the specific offset and
                                     * checking for the packets that needs to be
                                     * sent it to user space*/
                                    /* If the ethertype is of ARP, send the
                                     * packets to user space with the CAPWAP
                                     * header*/
                                    memcpy (&u2Ethertype,
                                            (skb->data + ETHERTYPE_OFFSET),
                                            TWO_BYTE);

                                    if ((ntohs (u2Ethertype) == ARP_ETHERTYPE)
                                        || (ntohs (u2Ethertype) ==
                                            PNAC_ETHERTYPE))
                                    {
                                        /* Assign the network, mac and transport header accordingly */
                                        skb->network_header = skb->data;
                                        skb->mac_header =
                                            skb->data - MAC_HDR_OFFSET;
                                        skb->transport_header =
                                            skb->data + NETWORK_HDR_OFFSET;
                                        return NF_ACCEPT;
                                    }

                                    /* If it is a dhcp packet, send the
                                     * packets to user space with the CAPWAP
                                     * header. It is done by verifying the
                                     * source and destination port*/
                                    memcpy (&u2Dstport,
                                            (skb->data + DHCP_DEST_PORT_OFFSET),
                                            TWO_BYTE);
                                    memcpy (&u2Srcport,
                                            (skb->data + DHCP_SRC_PORT_OFFSET),
                                            TWO_BYTE);
                                    u1UdpProtocol =
                                        (*(skb->data + UDP_PROTOCOL_OFFSET));

                                    if ((u1UdpProtocol == UDP)
                                        && (ntohs (u2Dstport) == DHCP_DEST_PORT)
                                        && (ntohs (u2Srcport) == DHCP_SRC_PORT))
                                    {
                                        /* Assign the network, mac and transport header accordingly */
                                        skb->network_header = skb->data;
                                        skb->mac_header =
                                            skb->data - MAC_HDR_OFFSET;
                                        skb->transport_header =
                                            skb->data + NETWORK_HDR_OFFSET;
                                        return NF_ACCEPT;
                                    }

                                    /* If it is a packet destined to WLC IP, send the
                                     * packets to user space with the CAPWAP
                                     * header*/
                                    memcpy (&u4InnerDestIp,
                                            (skb->data + INNER_DEST_IP_OFFSET),
                                            FOUR_BYTE);
                                    for_each_netdev (&init_net, device)
                                    {
                                        if (device != NULL)
                                        {
                                            in_dev =
                                                (struct in_device *) device->
                                                ip_ptr;
                                            if (in_dev != NULL)
                                            {
                                                if_info = in_dev->ifa_list;
                                                if (if_info != NULL)
                                                {
                                                    if (if_info->ifa_address ==
                                                        u4InnerDestIp)
                                                    {
                                                        /* Assign the network, mac and transport header accordingly */
                                                        skb->network_header =
                                                            skb->data;
                                                        skb->mac_header =
                                                            skb->data -
                                                            MAC_HDR_OFFSET;
                                                        skb->transport_header =
                                                            skb->data +
                                                            NETWORK_HDR_OFFSET;
                                                        return NF_ACCEPT;

                                                    }
                                                }
                                            }
                                        }
                                    }

                                    /* All the scenarios in which, the packet is
                                     * sent to be user space is handled, now
                                     * the dot11 packets can be converted into
                                     * dot3 packets for further processing
                                     * */
                                    skb_pull (skb,
                                              CAPWAP_HDR_OFFSET +
                                              CAPWAP_HDR_SPLIT_SIZE);
                                    /* get Bssid from dot11 packet for split MAC */
                                    FcapwapGetBssIdFromDot11Pkt (skb->data,
                                                                 au1Bssid);
                                    /*Convert .11 to .3 pkt for split MAC */
                                    if (Dot11ToDot3
                                        (&skb->data, &skb->len) != SUCCESS)
                                    {
                                        printk (KERN_ERR
                                                "processRcvDataPkt: Dot11ToDot3 conversion"
                                                "failure\n");
                                    }

                                    skb_pull (skb, MAC_HDR_OFFSET);    /* Pull the packets till the network header */

                                    /* Assign the network, mac and transport header accordingly */
                                    skb->network_header = skb->data;
                                    skb->mac_header =
                                        skb->data - MAC_HDR_OFFSET;
                                    skb->transport_header =
                                        skb->data + NETWORK_HDR_OFFSET;
                                    pRbDataIntf = ApIntfTableSearch (au1Bssid);

                                    if (pRbDataIntf != NULL)
                                    {
                                        sprintf ((UINT1 *) au1Buf, "vlan%d",
                                                 pRbDataIntf->apIntData.
                                                 u2VlanId);

                                        if (pRbData->apIpData.
                                            u1FragReassembleStatus ==
                                            FRAGMENTATION_ENABLE)
                                        {
                                            if (skb->len >
                                                pRbData->apIpData.u4PathMTU)
                                            {

                                                /* Push the packets till the network header */
                                                skb_push (skb,
                                                          INNER_IP_HDR_OFFSET);
                                                /* Assign the network, mac and transport header accordingly */
                                                skb->network_header = skb->data;
                                                skb->mac_header =
                                                    skb->data - MAC_HDR_OFFSET;
                                                skb->transport_header =
                                                    skb->data +
                                                    NETWORK_HDR_OFFSET;
                                                return NF_ACCEPT;

                                            }
                                        }

                                        if ((dev =
                                             dev_get_by_name (&init_net,
                                                              au1Buf)) == NULL)
                                        {
                                            return NF_ACCEPT;
                                        }
                                        else
                                        {
                                            skb->dev = dev;
                                            dev_put (dev);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    printk ("<1>ApIpTableSearch returns NULL\n");
                }
            }
            else
            {
                printk ("<1>ApIpTableSearch returns NULL\n");
            }
        }
        else
        {
#if DBG_LVL_01
            printk
                ("\n gi4DataUdpPort doesnt match with the recieved destination port");
#endif
        }
    }
    else
    {
        return NF_ACCEPT;
        /* Below code is needed when 802.1p is implemented. Hence commenting the below code as it
         * leads to  kernel crash when vlan tagged packets are received.*/
#if DEBUG_WANTED
        /*Packets which are not having CAPWAP header will be received here */

        /* Check for VLAN tag in the Ethernet Header  and update the corresponding 
           fields */

        memcpy (&u2Ethertype, skb->data - ETHERTYPE_LEN, ETHERTYPE_LEN);
        if (ntohs (u2Ethertype) == VLAN_ETHERTYPE)
        {
            memcpy (&u4VlanTag, (skb->data - (ETHERTYPE_LEN + VLAN_HDR_LEN)),
                    VLAN_HDR_LEN);
            u2Pcp = (u4VlanTag & 255) >> 13;
            u1TagStatus = 1;
        }
#endif

        /* Fetching the  ip header from buffer */
        ih = (struct iphdr *) ip_hdr (skb);
        u4InnerSrcIp = ntohl (ih->saddr);
        u4InnerDestIp = ntohl (ih->daddr);

        /* From the STA's IP address get its MAC address from the following DB 
           tAcSndStaIPTable */

        rbStaData = StaIpTableSearch (u4InnerDestIp);

        if (rbStaData != NULL)
        {
            /* Using the station Mac as the key get the IP address of the 
               WTP */
            pStaDB = ApStaDBTableSearch (rbStaData->staData.staMac);

            if (pStaDB == NULL)
            {
                printk ("\nStation DB search fails \n");
                return NF_ACCEPT;
            }

            /*Fill in the WTP IP */
            u4DestIp = pStaDB->StaInfo.u4ipAddr;

            apSndMacDb = APSndMacTableSearch (u4DestIp);

            if (apSndMacDb == NULL)
            {
                printk ("<1> AP Mac Table Search failed\n");
                return NF_ACCEPT;
            }

            /* Fill in the WTP MAC */
            memcpy (au1DestMacAddr, pStaDB->StaInfo.wtpMac, ETH_ALEN);

            /* From the BSSId identify the VLAN ID and use its mac address
               as the inner SRC Mac address. Use the STA Mac address as the
               Inner DEST Mac address  */

            pRbDataIntf = ApIntfTableSearch (rbStaData->staData.bssid);

            if (pRbDataIntf != NULL)
            {
                sprintf ((UINT1 *) au1Buf, "vlan%d",
                         pRbDataIntf->apIntData.u2VlanId);

                if ((dev = dev_get_by_name (&init_net, au1Buf)) == NULL)
                {
                    return NF_ACCEPT;
                }

                skb_push (skb, MAC_HDR_OFFSET);

                memcpy (skb->data, rbStaData->staData.staMac, MACADDR_SIZE);
                memcpy ((skb->data + MACADDR_SIZE), dev->dev_addr,
                        MACADDR_SIZE);
                dev_put (dev);

            }
            else
            {
                skb_pull (skb, MAC_HDR_OFFSET);
                return NF_ACCEPT;

            }

            /*Get the dev of the interface in which WTP is connected */
            rbDataMac = Mac_Table_Search (au1DestMacAddr);

            if (rbDataMac == NULL)
            {
                skb_pull (skb, MAC_HDR_OFFSET);
                return NF_ACCEPT;
            }

            memcpy (au1PhyIntf, rbDataMac->MacData.au1PhysicalInterface,
                    INTERFACE_LEN);

            if ((dev = dev_get_by_name (&init_net, au1PhyIntf)) != NULL)
            {
                out_dev = (struct in_device *) dev->ip_ptr;
                if_info = out_dev->ifa_list;

                memcpy (au1SrcAddr, dev->dev_addr, MAC_ADDR_LEN);

                skb->dev = dev;
                dev_put (dev);

                /* Calculation of head room space for the CAPWAP header 
                   and the Outer Header */

                /*get available headroom size from skb */
                i4headroom = skb_headroom (skb);

                /*calculate required headroom in skb for us */
                i4Length =
                    TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;

                i4Length += CAPWAP_HDR_LOCAL_SIZE;

                if (i4headroom < i4Length)
                {
                    new_skb = skb_realloc_headroom (skb, i4Length + 18);
                    dev_kfree_skb (skb);
                    if (new_skb == NULL)
                    {
                        printk ("<1> skb_realloc_headroom failed\n");
                        return FAIL;
                    }
                    skb = new_skb;
                }
                /*re structure skb buffer */
                skb_push (skb, i4Length);

                skb_pull (skb, MAC_HDR_OFFSET);

                /*relocate skb data pointer */
                skb->network_header = (skb->data);
                skb->transport_header = (skb->data + NETWORK_HDR_OFFSET);
                skb->mac_header = (skb->data - MAC_HDR_OFFSET);

                /* Adding the CAPWAP Header */

                FcapwapAddCapwapHeader (&skb->data[CAPWAP_HDR_OFFSET],
                                        rbStaData->staData.bssid);

                if ((skb->len + CAPWAP_HDR_LOCAL_SIZE + MAC_HDR_OFFSET)
                    > apSndMacDb->apMacData.u4PathMTU)
                {
                    FcapwapFragmentDataAndSend (skb, &apSndMacDb->apMacData,
                                                skb->len, if_info->ifa_address,
                                                u4DestIp, &(rbStaData->staData),
                                                u1TagStatus, u2Pcp,
                                                pRbDataIntf->apIntData.u2VlanId,
                                                au1SrcAddr, &u4Tos);

                }
                else
                {
                    FCapwapFormOuterHeader (skb, &apSndMacDb->apMacData,
                                            OSIX_HTONL (gu4WlcIpAddr), u4DestIp,
                                            &(rbStaData->staData), u1TagStatus,
                                            u2Pcp,
                                            pRbDataIntf->apIntData.u2VlanId,
                                            au1SrcAddr, &u4Tos);
                }

                return NF_STOLEN;

            }

        }
    }

    return NF_ACCEPT;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcHookPreRoutingLast
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : hooknum - hook number
 *                           skb -  skb buffer
 *                           in  - input deviece
 *                           out - output device
 *  Returns                : SUCCESS/FAILURE
 * ******************************************************************************/

UINT4
FcapwapWlcHookPreRoutingLast (UINT4 hooknum,
                              struct sk_buff *skb,
                              const struct net_device *in,
                              const struct net_device *out,
                              INT4 (*okfn) (struct sk_buff *))
{
    struct iphdr       *ih = NULL;
    struct iphdr       *iph = NULL;
    struct iphdr       *ipfh = NULL;
    struct sk_buff     *new_skb = NULL;
    INT4                i4Error = 0;
    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    INT4                i4DestIp = 0;
    UINT4               u4StaIp;
    UINT4               u4SrcIp;
    UINT4               u4DestIp;
    UINT4               u4InnerSrcIp;
    UINT4               u4InnerDestIp;
    UINT4               u4VlanTag;
    UINT4               u4Tos = 0;
    UINT2               u2Dstport, u2Srcport;
    UINT2               u2CheckSum = 0;
    UINT2               u2UdpLen = 0;
    UINT2               u2IpLen = 0;
    UINT2               u2Ethertype;
    UINT2               u2Pcp;
    UINT1               u1Val = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4Index = 0;
    UINT1               au1Bssid[MAC_ADDR_LEN];
    UINT1               au1SrcAddr[ETH_ALEN];
    UINT1               au1DestMacAddr[ETH_ALEN];
    UINT1               au1Buf[VLAN_NAME];
    UINT1               u1UdpProtocol = 0;
    UINT1               u1Value = 0;
    UINT1               u1TagStatus = 0;
    struct udphdr      *uh = NULL;
    struct udphdr      *udph = NULL;
    struct udphdr      *udpfh = NULL;
    tAcSndIpTable      *pRbData = NULL;
    tAcSndIpTable      *pRbDb = NULL;
    struct ethhdr      *eh;
    struct net_device  *dev;
    struct in_device   *in_dev;
    struct in_device   *out_dev;
    struct in_ifaddr   *if_info;
    struct net_device  *device;
    tAcSndStaIPTable   *rbData1 = NULL;
    tAcSndStaIPTable   *rbData2 = NULL;
    tAcSndIntfTable    *pRbDataIntf = NULL;
    tAcSndStaTable     *pStationDB = NULL;
    tAcSndStaTable     *pStaDB = NULL;
    tAcSndStaTable     *pStaDB1 = NULL;
    tAcSndStaTable     *pStaDB2 = NULL;
    tAcSndMacTable     *rbDataMac = NULL;
    tAcSndStaIPTable   *rbStaData = NULL;
    tAcSndStaIPTable   *rbData = NULL;
    tAcSndStaTable     *rbDataSta = NULL;
    UINT1               au1PhyIntf[INTERFACE_LEN];
    tCRU_BUF_CHAIN_HEADER *ppReassembleBuf = NULL;
    tAcSndApMacTable   *apSndMacDb = NULL;
    UINT2               u2frag = 0;
    UINT2               u2UdpPort = 0;
    static int          frag_check = 0;

    if (skb_is_nonlinear (skb))
    {
        if (skb_linearize (skb) != 0)
        {
            printk ("Return value is %d\n", skb);
        }

    }

    memset (au1Buf, 0, VLAN_NAME);
    memset (au1PhyIntf, 0, INTERFACE_LEN);
    u4Protocol = (ip_hdr (skb))->protocol;
    u4Tos = (ip_hdr (skb))->tos;

    if (u4Protocol == UDP)
    {
        return NF_ACCEPT;
    }
    else
    {
        /*Packets which are not having CAPWAP header will be received here */

        /* Check for VLAN tag in the Ethernet Header  and update the corresponding 
           fields */

        memcpy (&u2Ethertype, skb->data - ETHERTYPE_LEN, ETHERTYPE_LEN);
        if (ntohs (u2Ethertype) == VLAN_ETHERTYPE)
        {
            memcpy (&u4VlanTag, (skb->data - (ETHERTYPE_LEN + VLAN_HDR_LEN)),
                    VLAN_HDR_LEN);
            u2Pcp = (u4VlanTag & 255) >> 13;
            u1TagStatus = 1;
        }

        /* Fetching the  ip header from buffer */
        ih = (struct iphdr *) ip_hdr (skb);
        u4InnerSrcIp = ntohl (ih->saddr);
        u4InnerDestIp = ntohl (ih->daddr);

        /* From the STA's IP address get its MAC address from the following DB 
           tAcSndStaIPTable */

        rbStaData = StaIpTableSearch (u4InnerDestIp);

        if (rbStaData != NULL)
        {
            if (skb->len > 1456)
            {
                return NF_DROP;
            }

            /* Using the station Mac as the key get the IP address of the 
               WTP */
            pStaDB = ApStaDBTableSearch (rbStaData->staData.staMac);

            if (pStaDB == NULL)
            {
                printk ("\nStation DB search fails \n");
                return NF_ACCEPT;
            }

            /*Fill in the WTP IP */
            u4DestIp = pStaDB->StaInfo.u4ipAddr;

            apSndMacDb = APSndMacTableSearch (u4DestIp);

            if (apSndMacDb == NULL)
            {
                printk ("<1> AP Mac Table Search failed\n");
                return NF_ACCEPT;
            }

            /* Fill in the WTP MAC */
            memcpy (au1DestMacAddr, pStaDB->StaInfo.wtpMac, ETH_ALEN);

            /* From the BSSId identify the VLAN ID and use its mac address
               as the inner SRC Mac address. Use the STA Mac address as the
               Inner DEST Mac address  */

            pRbDataIntf = ApIntfTableSearch (rbStaData->staData.bssid);

            if (pRbDataIntf != NULL)
            {
                sprintf ((UINT1 *) au1Buf, "vlan%d",
                         pRbDataIntf->apIntData.u2VlanId);

                if ((dev = dev_get_by_name (&init_net, au1Buf)) == NULL)
                {
                    return NF_ACCEPT;
                }

                skb_push (skb, MAC_HDR_OFFSET);

                memcpy (skb->data, rbStaData->staData.staMac, MACADDR_SIZE);
                memcpy ((skb->data + MACADDR_SIZE), dev->dev_addr,
                        MACADDR_SIZE);
                dev_put (dev);

            }
            else
            {
                return NF_DROP;

            }

            /*Get the dev of the interface in which WTP is connected */
            rbDataMac = Mac_Table_Search (au1DestMacAddr);

            if (rbDataMac == NULL)
            {
                return NF_DROP;
            }

            memcpy (au1PhyIntf, rbDataMac->MacData.au1PhysicalInterface,
                    INTERFACE_LEN);

            if ((dev = dev_get_by_name (&init_net, au1PhyIntf)) != NULL)
            {
                out_dev = (struct in_device *) dev->ip_ptr;
                if_info = out_dev->ifa_list;

                memcpy (au1SrcAddr, dev->dev_addr, MAC_ADDR_LEN);

                skb->dev = dev;
                dev_put (dev);

                /* Calculation of head room space for the CAPWAP header 
                   and the Outer Header */

                /*get available headroom size from skb */
                i4headroom = skb_headroom (skb);

                /*calculate required headroom in skb for us */
                i4Length =
                    TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;

                i4Length += CAPWAP_HDR_LOCAL_SIZE;

                if (i4headroom < i4Length)
                {
                    new_skb = skb_realloc_headroom (skb, i4Length + 18);
                    dev_kfree_skb (skb);
                    if (new_skb == NULL)
                    {
                        printk ("<1> skb_realloc_headroom failed\n");
                        return FAIL;
                    }
                    skb = new_skb;
                }
                /*re structure skb buffer */
                skb_push (skb, i4Length);

                skb_pull (skb, MAC_HDR_OFFSET);

                /*relocate skb data pointer */
                skb->network_header = (skb->data);
                skb->transport_header = (skb->data + NETWORK_HDR_OFFSET);
                skb->mac_header = (skb->data - MAC_HDR_OFFSET);

                /* Adding the CAPWAP Header */

                FcapwapAddCapwapHeader (&skb->data[CAPWAP_HDR_OFFSET],
                                        rbStaData->staData.bssid);

                if ((skb->len + CAPWAP_HDR_LOCAL_SIZE + MAC_HDR_OFFSET)
                    > apSndMacDb->apMacData.u4PathMTU)
                {
                    FcapwapFragmentDataAndSend (skb, &apSndMacDb->apMacData,
                                                skb->len, if_info->ifa_address,
                                                u4DestIp, &(rbStaData->staData),
                                                u1TagStatus, u2Pcp,
                                                pRbDataIntf->apIntData.u2VlanId,
                                                au1SrcAddr, &u4Tos);

                }
                else
                {

                    FCapwapFormOuterHeader (skb, &apSndMacDb->apMacData,
                                            if_info->ifa_address, u4DestIp,
                                            &(rbStaData->staData), u1TagStatus,
                                            u2Pcp,
                                            pRbDataIntf->apIntData.u2VlanId,
                                            au1SrcAddr, &u4Tos);
                }

                return NF_STOLEN;

            }

        }

    }

    return NF_ACCEPT;
}

/******************************************************************************
 *  Function Name          : verifyCapwapDataPkt
 *  Description            : This function is used to verify the incoming
 *                           packet is managment or data packet
 *  Input(s)               : *pBuff - Buffer
 *  Output(s)              : packet will be verified whether it is mgmnt or data
 *  Returns                : DATA_PKT - Data packet  / MGT_PKT - management packet
 * ******************************************************************************/
UINT1
verifyCapwapDataPkt (UINT1 *pu1Buf)
{
    UINT1               u1Val = 0;

    /* Read 4th bit to check the data packet or management packet */
    u1Val = *(pu1Buf + THREE_BYTE);

    /* Check the packet is management or data */
    if ((u1Val & MGMNT_BIT) == MGMNT_BIT)
    {
        return MGT_PKT;
    }
    return DATA_PKT;
}

/******************************************************************************
 *  Function Name          : FcapwapGetBssIdFromDot11Pkt
 *  Description            : This function is used to get the BSSID from 802.11
 *                           Packet
 *  Input(s)               : *pProcBuf - buffer, pu1BssId - array buff to be
 *                            updated
 *  Output(s)              : BSSID will be retrieved from 802.11 packet.
 *  Returns                : void
 * ******************************************************************************/
void
FcapwapGetBssIdFromDot11Pkt (UINT1 *pu1ProcBuff, UINT1 *pu1BssId)
{
    UINT1              *pu1TempBuf = NULL;
    UINT1               u1Val = 0;

    if (pu1ProcBuff == NULL)
    {
        return;
    }

    u1Val = *(pu1ProcBuff + 1);

    /* The packets are recieved in this order
     * First four bytes, refers to frame control
     * Next six bytes, bssid 
     * Next six byste, source mac address
     * Next six bytes, destinaion mac address etc.
     * Move 4 bytes and copy the bssid required*/
    if (u1Val == 0x01)
    {
        pu1TempBuf = pu1ProcBuff + BYTE_SHIFT_ADDR1;
        /*host variable memcpy of 6 bytes */
        memcpy (pu1BssId, pu1TempBuf, MACADDR_SIZE);
    }
}

/******************************************************************************
 *  Function Name          : verifyDot3FrameReceived
 *  Description            : This function is used to verify the incoming
 *                           frame is dot3 or dot 11 frame
 *  Input(s)               : *pBuff - Buffer
 *  Output(s)              : frame will be verified whether it is dot3 or dot11
 *  Returns                : 0 - dot3 frame  / 1 - dot11 frame
 * ******************************************************************************/
static UINT1
verifyDot3FrameReceived (UINT1 *pu1Buf)
{
    UINT2               u2Val = 0;

    /* skip 2 bytes and get next 2 bytes */
    u2Val = (*(pu1Buf + TWO_BYTE) << 8);
    u2Val = u2Val | (0x00FF & *(pu1Buf + THREE_BYTE));

    /* Check T & M bit for checking received frame is dot 3 or dot 11 */
    if ((u2Val & 0x0110) == 0x0010)
    {
        return SUCCESS;
    }
    else
    {
        return FAIL;
    }

}

/******************************************************************************
 *  Function Name          : CapwapFragmentReceived 
 *  Description            : This function is used to Check whether the packet 
 *                           received is a CAPWAP Fragment
 *  Input(s)               : pBuf ->Packet to be verified 
 *  Output(s)              : none
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

INT4
verifyCapwapFragmentReceived (struct sk_buff *pSkb)
{

    UINT1               u1Val = 0;
    UINT1              *pRcvBuf = NULL;
    UINT1               au1Frame[CAPWAP_MAX_HDR_LEN];

    memset (au1Frame, 0, CAPWAP_MAX_HDR_LEN);
    memcpy (au1Frame, pSkb->data, CAPWAP_MAX_HDR_LEN);
    pRcvBuf = au1Frame;
    /* Read 'F' bit in the CAPWAP Header */
    CAPWAP_SKIP_N_BYTES (KEEP_ALIVE_BYTE, pRcvBuf);
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);

    if ((u1Val & VAL_HEX) == VAL_HEX)
    {
        return SUCCESS;
    }
    return FAILURE;
}

/*****************************************************************************
 * Function     : FCapwapDataFragReassemble                                  *
 *                                                                           *
 * Description  : Reassembling of fragmented CAPWAP Data packet              *
 *                                                                           *
 * Input        : pBuf  - Fragment of the packet                             *
 *                pSess - Session Entry of AP and AC                         *
 *                u2PktLen - Fragment Length                                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
CapwapDataFragReassemble (struct sk_buff *skb,
                          tAPIpInfo * apIpData, UINT2 u2PktLen,
                          tCRU_BUF_CHAIN_HEADER ** ppReassembleBuf)
{
    tCapFragNode       *pPrevFrag = NULL;
    tCapFragNode       *pNextFrag = NULL;
    tCapFragNode       *pTempFrag = NULL;
    tUtlSysPreciseTime  SysPreciseTime;
    UINT2               u2Place = CAPWAP_FRAG_INSERT;
    UINT1               u1FlgOffs = 0;
    UINT1               u1IndexFound = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFlag;    /* 1 if not last fragment, 0 otherwise */
    UINT2               u2StartOffset = 0;    /* Index of first byte in fragment  */
    UINT2               u2FragId = 0;
    UINT2               u2EndOffset;
    UINT2               u2Index = 0;
    UINT2               u2FragIndex = 0;
    UINT1               u1ResourceTracker = 0;
    UINT4               u4Index = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    struct net_device  *dev = NULL;

    dev = skb->dev;

    if ((pBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        printk
            ("CapwapDataFragReassemble: CRU Buffer Memory Allocation Failed\n");
        kfree_skb (skb);
        return FAIL;
    }

    pBuf->pSkb = skb;

    if (pBuf->pSkb == NULL)
    {
        printk ("Skb Buffer passed in is a NULL pointer\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return FAIL;
    }

    if (apIpData == NULL)
    {
        printk ("Session Entry passed is a NULL Pointer \r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return FAIL;
    }

    u2PktLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen,
                          CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1FlgOffs,
                          CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

    /* Get CAPWAP Fragment ID */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2FragId, CAPWAP_FRAGID_OFFSET,
                          CAPWAP_FRAGID_OFFSET_LEN);
    u2FragId = OSIX_HTONS (u2FragId);

    /* Get CAPWAP Fragment Offset */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2StartOffset, CAPWAP_FRAG_OFFSET,
                          CAPWAP_FRAG_OFFSET_LEN);
    u2StartOffset = OSIX_HTONS (u2StartOffset);
    u2StartOffset = (UINT2) ((u2StartOffset & CAPWAP_FRAG_OFFSET_MASK) >> 3);

    u2EndOffset = (UINT2) (u2StartOffset + u2PktLen - u1HdrLen);
    i1LastFlag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Remove CAPWAP  header from all fragments except first */
    if (u2StartOffset != CAPWAP_ZERO_OFFSET)
    {
        CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);
    }

    if (apIpData->u1QInitialized == 0)
    {
        /* Intialisation of the queue */
        for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
        {
            apIpData->CapDataFragStream[u2Index].u1NumberOfFragments = 0;
            apIpData->CapDataFragStream[u2Index].u2FragmentId = 0;
            apIpData->CapDataFragStream[u2Index].u4FirstFragTime = 0;
        }
        apIpData->u1QInitialized = 1;
    }

    if (gu1TimerStarted == CAPWAP_DATA_REASSEMBLY_TMR_NOT_STARTED)
    {
        /* Get the Current System Time */
        TmrGetPreciseSysTime (&SysPreciseTime);
        apIpData->CapDataFragStream[u2FragIndex].u4FirstFragTime =
            SysPreciseTime.u4Sec;

        /* Intialise the timer with callback function and the argument
         * passed to that function and set the timer to expire for every 
         * 2 second */

        setup_timer (&my_timer, FcapwapProcessReassmTimeExpiry, apIpData);
        mod_timer (&my_timer, jiffies +
                   msecs_to_jiffies (CAPWAP_REASSEMBLE_TIME));

#if DBG_LVL_FRAG_01
        TmrGetPreciseSysTime (&SysPreciseTime);
        printk ("\n Reassembly Timer started succesfully at : %d\n",
                SysPreciseTime.u4Sec);
#endif
        gu1TimerStarted = CAPWAP_DATA_REASSEMBLY_TMR_STARTED;

    }

    /* Get the fragment id and the data frag index */
    for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
    {
        if (apIpData->CapDataFragStream[u2Index].u1NumberOfFragments != 0)
        {
            u1ResourceTracker++;

            if (apIpData->CapDataFragStream[u2Index].u2FragmentId == u2FragId)
            {
                u2FragIndex = u2Index;
                u1IndexFound++;
                break;
            }
        }
    }

    if (u1ResourceTracker == CAPWAP_MAX_DATA_FRAG_INSTANCE)
    {
        printk ("RESOURCE UNAVAILABLE TO STORE THE FRAGMENTS\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return FAIL;
    }
    if (u1IndexFound == 1)
    {
        /* Already received some fragments */
        if (i1LastFlag == TRUE)
        {
            /* Last Fragment Received, update the entire datagram size */
            apIpData->CapDataFragStream[u2FragIndex].u2TotalLen = u2EndOffset;
        }
        if (apIpData->CapDataFragStream[u2FragIndex].u1NumberOfFragments ==
            CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME)
        {
            printk ("Exceeding max no fragments, Cleaning the resources \r\n");
            CRU_BUF_Release_MsgBufChain (pBuf, 0);

            CapwapDataFreeReAssemble (&
                                      (apIpData->
                                       CapDataFragStream[u2FragIndex]),
                                      u2FragIndex, TRUE);
            return FAIL;
        }
        apIpData->CapDataFragStream[u2FragIndex].u1NumberOfFragments++;
    }
    else
    {
        /* First fragment received, Find the free Queue to store the fragment
         * and update the fragment id of the Queue */
        for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
        {
            if (apIpData->CapDataFragStream[u2Index].u1NumberOfFragments == 0)
            {
                u2FragIndex = u2Index;
                apIpData->CapDataFragStream[u2Index].u2FragmentId = u2FragId;

                break;

            }
        }

        /* If first fragment received is the last fragment of the packet 
           then the EndOffset of the packet should be updated here.  */
        if (i1LastFlag == TRUE)
        {
            /* Last Fragment Received, update the entire datagram size */
            apIpData->CapDataFragStream[u2FragIndex].u2TotalLen = u2EndOffset;
        }

        /* Initialise the Queue identified by the index u2FragIndex */
        TMO_DLL_Init (&(apIpData->CapDataFragStream[u2FragIndex].FragList));

        apIpData->CapDataFragStream[u2FragIndex].u1NumberOfFragments++;

    }

    /*Set pNextFrag to the first fragment which begins after us,
       and pLastFrag to the last fragment which begins before us.
     */
    pPrevFrag = NULL;
    TMO_DLL_Scan (&(apIpData->CapDataFragStream[u2FragIndex].FragList),
                  pNextFrag, tCapFragNode *)
    {
        if (pNextFrag->u2StartOffset > u2StartOffset)
        {
            break;
        }
        else if (pNextFrag->u2StartOffset == u2StartOffset)
        {
            /* Received the duplicate Fragment, drop the fragement here */
            printk ("Received the duplicate fragment, drop here %d \r\n",
                    pNextFrag->u2StartOffset);
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            return FAIL;
        }
        pPrevFrag = pNextFrag;
        /* Previous Frag exists */
    }

    /* pPrevfrag now points, as before, to the fragment before us;
     * pNextFrag points at the next fragment. Check to see if we can
     * join to either or both fragments.
     **/

    if ((pPrevFrag != NULL) && (pPrevFrag->u2EndOffset == u2StartOffset))
    {
        u2Place |= CAPWAP_FRAG_APPEND;
    }

    if ((pNextFrag != NULL) && (pNextFrag->u2StartOffset == u2EndOffset))
    {
        u2Place |= CAPWAP_FRAG_PREPEND;
    }

    switch (u2Place)
    {
        case CAPWAP_FRAG_APPEND:    /* Append the Received Fragment */
        {
            if (pPrevFrag->u2StartOffset == 0)
            {
                skb_push (pPrevFrag->pBuf->pSkb, TRANSPORT_HDR_SIZE +
                          NETWORK_HDR_OFFSET + MAC_HDR_OFFSET);
                CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                          pPrevFrag->pBuf->pSkb->len);
                skb_pull (pBuf->pSkb, MAC_HDR_OFFSET);
                pBuf->pSkb->network_header = pBuf->pSkb->data;
                pBuf->pSkb->transport_header = pBuf->pSkb->data +
                    NETWORK_HDR_OFFSET;
                pBuf->pSkb->mac_header = pBuf->pSkb->data - MAC_HDR_OFFSET;
                skb_pull (pBuf->pSkb,
                          (TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET));
            }
            else
            {
                CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                          pPrevFrag->pBuf->pSkb->len);
            }
            pPrevFrag->u2EndOffset = u2EndOffset;
            CRU_BUF_Release_MsgBufChain (pPrevFrag->pBuf, 0);
            pPrevFrag->pBuf = pBuf;
            if (gu1DebugPrintEnable == 1)
            {
                printk ("APPEND END OFFSET - %u Len %u\n", u2EndOffset,
                        pPrevFrag->pBuf->pSkb->len);
            }
            break;

        }
        case CAPWAP_FRAG_PREPEND:    /* Prepend the Received Fragment */
        {
            CRU_BUF_Concat_MsgBufChains (pBuf, pNextFrag->pBuf);
            pNextFrag->pBuf = pBuf;
            pNextFrag->u2StartOffset = u2StartOffset;    /* Extend backward */
            if (gu1DebugPrintEnable == 1)
            {
                printk ("PREPEND OFFSET - %d \n", u2StartOffset);
            }
            break;
        }
            /* If fragments are received out of order then the received fragment
             * may be need to be placed in between the fragments.*/
        case (CAPWAP_FRAG_APPEND | CAPWAP_FRAG_PREPEND):
        {
            if (pPrevFrag->u2StartOffset == 0)
            {
                skb_push (pPrevFrag->pBuf->pSkb, TRANSPORT_HDR_SIZE +
                          NETWORK_HDR_OFFSET + MAC_HDR_OFFSET);
                CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                          pPrevFrag->pBuf->pSkb->len);
                skb_pull (pBuf->pSkb, MAC_HDR_OFFSET);
                pBuf->pSkb->network_header = pBuf->pSkb->data;
                pBuf->pSkb->transport_header = pBuf->pSkb->data +
                    NETWORK_HDR_OFFSET;
                pBuf->pSkb->mac_header = pBuf->pSkb->data - MAC_HDR_OFFSET;
                skb_pull (pBuf->pSkb,
                          (TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET));
            }
            else
            {
                CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                          pPrevFrag->pBuf->pSkb->len);
            }
            pPrevFrag->u2EndOffset = u2EndOffset;
            CRU_BUF_Release_MsgBufChain (pPrevFrag->pBuf, 0);
            pPrevFrag->pBuf = pBuf;
            CRU_BUF_Concat_MsgBufChains (pPrevFrag->pBuf, pNextFrag->pBuf);
            pPrevFrag->u2EndOffset = pNextFrag->u2EndOffset;
            TMO_DLL_Delete (&
                            (apIpData->CapDataFragStream[u2FragIndex].FragList),
                            &pNextFrag->Link);
            if (gu1DebugPrintEnable == 1)
            {
                printk ("BOTH Len %u End %u \n", pPrevFrag->pBuf->pSkb->len,
                        pPrevFrag->u2EndOffset);
            }
            break;
        }

        default:                /* Insert new desc between pLastFrag and
                                   pNextFrag */
        {
            pTempFrag = CapwapCreateFrag (u2StartOffset, u2EndOffset,
                                          pBuf, u2FragId);
            if (pTempFrag == NULL)
            {
                printk ("Failed to create the CapwapCreateFrag Entry .\r\n");
                CRU_BUF_Release_MsgBufChain (pBuf, 0);
                pBuf = NULL;
                return FAIL;
            }

            TMO_DLL_Insert (&
                            (apIpData->CapDataFragStream[u2FragIndex].FragList),
                            &pPrevFrag->Link, &pTempFrag->Link);
            /* Add all the +ve sceario prints inside compilation flag */
            if (gu1DebugPrintEnable == 1)
            {
                printk ("FRAG Id %d Start Offset %d End Offset %d\n", u2FragId,
                        u2StartOffset, u2EndOffset);
            }
            break;
        }

            /* coverity fix end */
    }

    pTempFrag =
        (tCapFragNode *)
        TMO_DLL_First (&(apIpData->CapDataFragStream[u2FragIndex].FragList));
    if (pTempFrag == NULL)
    {
        printk ("First Fragment in the list is NULL, return FAILURE\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return FAIL;
    }
    /* Kloc Fix Start */
    if (pTempFrag->pBuf == NULL)
    {
        printk ("First Fragment in the list is NULL, return FAILURE\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return FAIL;
    }

    /* Kloc Fix Ends */

    if ((pTempFrag->u2StartOffset == CAPWAP_ZERO_OFFSET) &&
        (apIpData->CapDataFragStream[u2FragIndex].u2TotalLen !=
         CAPWAP_ZERO_LEN) &&
        (pTempFrag->u2EndOffset ==
         apIpData->CapDataFragStream[u2FragIndex].u2TotalLen))
    {
        /* We 've got a complete datagram, so extract it from the
         * reassembly buffer and pass it on.
         * */

        *ppReassembleBuf = pTempFrag->pBuf;

        /* Get CAPWAP Header Flags */
        CAPWAP_COPY_FROM_BUF (*ppReassembleBuf, &u1FlgOffs,
                              CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear the F and L Bit in CAPWAP Header */
        u1FlgOffs = (u1FlgOffs & CAPWAP_FRAG_BIT_CLEAR_OFFSET);

        /* Update Flag bits in CAPWAP Header */
        CRU_BUF_Copy_OverBufChain (*ppReassembleBuf, &u1FlgOffs,
                                   CAPWAP_FLAGS_BIT_OFFSET,
                                   CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear Fragment Id in CAPWAP Header */
        u2FragId = 0;
        CRU_BUF_Copy_OverBufChain (*ppReassembleBuf, &u2FragId,
                                   CAPWAP_FRAGID_OFFSET,
                                   CAPWAP_FRAGID_OFFSET_LEN);

        CapwapDataFreeReAssemble (&(apIpData->CapDataFragStream[u2FragIndex]),
                                  u2FragIndex, FALSE);

        return SUCCESS;
    }
    else
    {
        /* Not received the complete packet, Return Failure */
        return FAIL;
    }

}

/******************************************************************************
 *  Function Name          : CapwapCreateFrag 
 *
 *  Description            : This function creates the fragment node to be added
 *                           in to the Queue.
 *
 *  Input(s)               : u2StartOffset -  Start Offset of the fragment.
 *                           u2EndOffset   -  End Offset of the fragment. 
 *                           pBuf          -  Buffer contatining the fragment.
 *                           u2FragId      -  Fragment Id.
 *                           
 *  Output(s)              : None 
 *
 *  Returns                : pointer the fragment node/NULL 
 * ******************************************************************************/

tCapFragNode       *
CapwapCreateFrag (UINT2 u2StartOffset, UINT2 u2EndOffset,
                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2FragId)
{
    tCapFragNode       *pFrag = NULL;

    if (pBuf == NULL)
    {
        printk ("CRU Buffer passed in is a NULL pointer, Return NULL \r\n");
        return NULL;
    }
    pFrag = (tCapFragNode *) MemAllocMemBlk (CAPWAP_FRAG_POOLID);
    if (pFrag == NULL)
    {
        printk ("CapwapCreateFrag:Memory allocation failed \r\n");
        return NULL;
    }
    pFrag->pBuf = pBuf;
    pFrag->u2StartOffset = u2StartOffset;
    pFrag->u2EndOffset = u2EndOffset;
    pFrag->u2FragId = u2FragId;
    TMO_DLL_Init_Node (&pFrag->Link);
    return pFrag;
}

/******************************************************************************
 *  Function Name          : CapwapDataFreeReAssemble 
 *
 *  Description            : This function releases the fragments and its
 *                           associated resources from the Queue based on the
 *                           bForceRelease boolean varaible.
 *
 *  Input(s)               : pCapDataFragStream -  pointer to the Queue
 *                           u2FragIndex        -  Index of the Queue
 *                           bForceRelease      -  To release the resources
 *                                                 associated with the fragments.
 *                           
 *  Output(s)              : None 
 *
 *  Returns                : SUCCESS/FAIL 
 * ******************************************************************************/
INT4
CapwapDataFreeReAssemble (tCapFragment * pCapDataFragStream, UINT2 u2FragIndex,
                          BOOL1 bForceRelease)
{
    tCapFragNode       *pFrag = NULL;
    UINT4               u4TmrRetVal = 0;
    UINT4               u4RemainingTime = 0;

    UNUSED_PARAM (u2FragIndex);

    if (pCapDataFragStream == NULL)
    {
        printk ("CapwapDataFreeReAssemble : pCapDataFragStream is NULL \r\n");
        return OSIX_FAILURE;
    }
    /* Stop the Reassembly Timer */

#if DBG_LVL_FRAG_01
    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pCapDataFragStream->ReassemblyTimer.
                                         TimerNode), &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                     &(pCapDataFragStream->ReassemblyTimer)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop"
                        "ReAssembly timer \r\n");
            return OSIX_FAILURE;
        }
    }
#endif
    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (tCapFragNode *) TMO_DLL_First
            (&(pCapDataFragStream->FragList))))
    {
        if (pFrag->pBuf != NULL)
        {
            if (bForceRelease != TRUE)
            {
                pFrag->pBuf->pSkb = NULL;
            }
            CRU_BUF_Release_MsgBufChain (pFrag->pBuf, 0);
            pFrag->pBuf = NULL;
        }
        /* Should have deleted from reassembly list before freeing it. */
        TMO_DLL_Delete (&(pCapDataFragStream->FragList), &pFrag->Link);

        if (MemReleaseMemBlock (CAPWAP_FRAG_POOLID, (UINT1 *) pFrag) !=
            MEM_SUCCESS)
        {
            printk ("\nCapwapDataFreeReAssemble:- Error in releasing the"
                    " memory for the pFrag node\n");

        }
    }
    pCapDataFragStream->u2FragmentId = 0;
    pCapDataFragStream->u1NumberOfFragments = 0;
    pCapDataFragStream->u4FirstFragTime = 0;

    return OSIX_SUCCESS;
}

/******************************************************************************
 *  Function Name          : IpCalcCheckSum
 *  Description            : This function returns checksum value
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : UNIT2
 * ******************************************************************************/

UINT2
IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pBuf = pSkb;
    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);
    return ((u2Tmp));

}

/******************************************************************************
 *  Function Name          : FcapwapProcessReassmTimeExpiry 
 *  Description            : This function clears the Queue which has fragments
 *                           stored in it for more than 2 Seconds without 
 *                           reassembling.
 *  Input(s)               : Queue to be cleared 
 *  Output(s)              : NONE
 *  Returns                : NONE
 * ******************************************************************************/

static VOID
FcapwapProcessReassmTimeExpiry (FS_ULONG arg)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    tAPIpInfo          *apIpData = NULL;
    tUtlSysPreciseTime  SysPreciseTime;
    tUtlSysPreciseTime  SysPreciseTimeSample;
    UINT4               u4TimeDiff = 0;
    UINT2               u2Index = 0;
    UINT1               u1TimerId = 0;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    MEMSET (&SysPreciseTimeSample, 0, sizeof (tUtlSysPreciseTime));

    apIpData = (tAPIpInfo *) arg;

#if DBG_LVL_FRAG_01
    TmrGetPreciseSysTime (&SysPreciseTimeSample);
    printk ("\n Reassembly Timer expired at : %d \n",
            SysPreciseTimeSample.u4Sec);
#endif

    for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
    {
        if (apIpData->CapDataFragStream[u2Index].u1NumberOfFragments > 0)
        {
            TmrGetPreciseSysTime (&SysPreciseTime);

            /* if the diffence between the current system time and the time at 
             * which the first fragment for the Q received is greater than 2 
             * seconds then clear the fragments on the Queue */

            u4TimeDiff = SysPreciseTime.u4Sec -
                apIpData->CapDataFragStream[u2Index].u4FirstFragTime;

            if (u4TimeDiff >= 2)
            {
#if DBG_LVL_01
                printk ("\n Clearing the Q : %d Frag Id : %d "
                        "Time Added : %d \n", u2Index,
                        apIpData->CapDataFragStream[u2Index].u2FragmentId,
                        apIpData->CapDataFragStream[u2Index].u4FirstFragTime);
#endif
                CapwapDataFreeReAssemble (&
                                          (apIpData->
                                           CapDataFragStream[u2Index]), u2Index,
                                          TRUE);
            }

        }

    }

#if DBG_LVL_01
    TmrGetPreciseSysTime (&SysPreciseTimeSample);
    printk ("\nIn func %s timer restarted at %d \n", __func__,
            SysPreciseTimeSample.u4Sec);
#endif
    mod_timer (&my_timer, jiffies + msecs_to_jiffies (CAPWAP_REASSEMBLE_TIME));

}

/******************************************************************************
 *  Function Name          : FcapwapAddCapwapHeader
 *  Description            : This function is used to add the CAPWAP header
 *                           to the incoming data packet.
 *  Input(s)               : *pu1Buf - Buffer
 *  Output(s)              : packet will be verified whether it is mgmnt or data
 *  Returns                : void 
 * ******************************************************************************/
void
FcapwapAddCapwapHeader (UINT1 *pu1Buf, UINT1 *pu1Bssid)
{
    tCapwapHdr          pCapwapHdr;

    memcpy (pCapwapHdr.RadioMacAddr, pu1Bssid, MACADDR_SIZE);
    CapwapConstructCpHeader (&pCapwapHdr, i4MacType);
    AssembleCapwapHeader (pu1Buf, &pCapwapHdr);
}

/************************************************************************/
/*  Function Name   : FcapwapFragmentDataAndSend                        */
/*                                                                      */
/*  Description     : The function fragments  the received CAPWAP       */
/*                    Data Packets based on the MTU size                */
/*                                                                      */
/*  Input(s)        : skb   - Packet to be Fragmented                   */
/*                    u4MTU - MTU Size                                  */
/*                    pSessEntry - Session Entry for the AP/AC          */
/*                    i2PktLen - Packet Length                          */
/*                    u1PktType - CAPWAP Control/Data Packet            */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : SUCCESS/FAILURE                                   */
/************************************************************************/
INT4
FcapwapFragmentDataAndSend (struct sk_buff *skb, tApMacInfo * apMacData,
                            UINT4 u4PktLen, INT4 i4SrcIp, INT4 i4DestIp,
                            tStaIpInfo * pStaIpData, UINT1 u1TagStatus,
                            UINT2 u2Pcp, UINT2 u2VlanId, UINT1 *pSrcMacAddr,
                            UINT4 *pu4Tos)
{

    UINT2               u2FragDataSize = 0;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;
    UINT2               u2FragId = 0;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pPktBuf = NULL;
    UINT1               u1FlgOffs = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFrag;
    tCapwapHdr          CapHdr;
    UINT1               u1Type[2];
    UINT1               u1Length[2];
    UINT2               u2PacketLen = 0;
    UINT1               u1LastFrag = OSIX_FALSE;
    UINT4               u4MTU = 0;
    UINT1               u1IsFragmented = 0;
    struct net_device  *org_dev = NULL;

    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    struct sk_buff     *new_skb = NULL;

    MEMSET (&CapHdr, 0, sizeof (tCapwapHdr));
    MEMSET (&u1Type, 0, sizeof (UINT2));
    MEMSET (&u1Length, 0, sizeof (UINT2));

    u4MTU = apMacData->u4PathMTU;

    if ((pPktBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        printk
            ("FcapwapFragmentDataAndSend: CRU Buffer Memory Allocation Failed\n");
        return FAIL;
    }
    pPktBuf->pSkb = skb;
    org_dev = skb->dev;

    skb_pull (pPktBuf->pSkb, CAPWAP_HDR_OFFSET);

    u2PacketLen = CRU_BUF_Get_ChainValidByteCount (pPktBuf);

    /* Get CAPWAP Header Length */
    CRU_BUF_Copy_FromBufChain (pPktBuf, &u1HdrLen,
                               CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CRU_BUF_Copy_FromBufChain (pPktBuf, &u1FlgOffs,
                               CAPWAP_FLAGS_BIT_OFFSET,
                               CAPWAP_FLAGS_OFFSET_LEN);

    i1LastFrag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Get Capwap Header */
    CapwapExtractHdr (pPktBuf, &CapHdr);

    /* Move the cru buffer */
    u2PacketLen = (INT2) (u2PacketLen - u1HdrLen);
    CRU_BUF_Move_ValidOffset (pPktBuf, u1HdrLen);

    u2FragId = apMacData->u2FragId;

    /*  Fragments should be in  multpIple of 8 bytes */
    if (u4MTU < u1HdrLen)
    {
        printk
            ("FcapwapFragmentDataAndSend: MTU is less than capwap header length\n");
        CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
        return FAIL;
    }

    u4MTU = (UINT4) (u4MTU - u1HdrLen);

    pTmpBuf = pPktBuf;
    while (u2PacketLen > 0)
    {
        if ((UINT2) (u2PacketLen) <= u4MTU)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = (UINT2) u2PacketLen;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_LAST_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1LastFrag = OSIX_TRUE;
            u1IsFragmented = 0;
        }
        else
        {
            u2FragDataSize = (UINT2) u4MTU;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= i1LastFrag;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1IsFragmented = 1;

            if (CRU_BUF_Fragment_BufChain (pPktBuf, u2FragDataSize, &pTmpBuf)
                != CRU_SUCCESS)
            {
                printk ("FcapwapFragmentDataAndSend: Failed to Fragment the\ 
                        Packet\n");
                CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
                return FAIL;
            }

            if (pTmpBuf == NULL)
            {
                printk
                    ("Error in Fragmentation :- Fragmented Buff is NULL\r\n");
                CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
                return OSIX_FAILURE;
            }

        }

        u2Len = (UINT2) (CRU_BUF_Get_ChainValidByteCount (pPktBuf));

        /* Now the datagram to send is in pPktBuf; Add frag specific info */
        u2Offset += u2FragDataSize;    /* Each time offset gets updated */
        u2PacketLen -= u2FragDataSize;

        CapwapPutHdr (pPktBuf, &CapHdr);

        if (u1IsFragmented == 1)
        {
            /*get available headroom size from skb */
            i4headroom = skb_headroom (pTmpBuf->pSkb);
            i4Length = TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;
            /*allocate required headroom if we hold less */

            if (i4headroom < i4Length)
            {
                new_skb = skb_realloc_headroom (pTmpBuf->pSkb, i4Length + 18);
                dev_kfree_skb (pTmpBuf->pSkb);
                if (new_skb == NULL)
                {
                    printk ("<1> skb_realloc_headroom failed\n");
                    return FAIL;
                }
                pTmpBuf->pSkb = new_skb;
            }

        }

        skb_push (pPktBuf->pSkb, CAPWAP_HDR_OFFSET);

        /*relocate skb data pointer */
        pPktBuf->pSkb->network_header = (pPktBuf->pSkb->data);
        pPktBuf->pSkb->transport_header =
            (pPktBuf->pSkb->data + NETWORK_HDR_OFFSET);
        pPktBuf->pSkb->mac_header = (pPktBuf->pSkb->data - MAC_HDR_OFFSET);

        if (pPktBuf->pSkb->dev == NULL)
        {
            pPktBuf->pSkb->dev = org_dev;
        }

        FCapwapFormOuterHeader (pPktBuf->pSkb, apMacData, i4SrcIp, i4DestIp,
                                pStaIpData, u1TagStatus, u2Pcp, u2VlanId,
                                pSrcMacAddr, pu4Tos);

        if (pPktBuf != NULL)
        {
            pPktBuf->pSkb = NULL;
            CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
            pPktBuf = NULL;
        }

    }
    /* Increment the Fragment Id */
    apMacData->u2FragId++;
    return SUCCESS;
}

void
FCapwapFormOuterHeader (struct sk_buff *sk_buffer, tApMacInfo * apMacData,
                        INT4 i4SrcIp, INT4 i4DestIp, tStaIpInfo * pStaIpData,
                        UINT1 u1TagStatus, UINT2 u2Pcp, UINT2 u2VlanId,
                        UINT1 *pSrcMacAddr, UINT4 *pu4Tos)
{

    INT4                i4headroom = 0;
    INT4                i4Error = 0;
    UINT4               u4Index = 0;
    UINT2               u2UdpLen = 0;
    UINT2               u2IpLen = 0;
    UINT2               u2Priority = 0;
    UINT2               u2TCI = 0;
    struct ethhdr      *eh;
    struct udphdr      *udph;
    struct iphdr       *iph;
    struct sk_buff     *new_skb = NULL;
    struct vlan_ethhdr *vlan = NULL;

    /*Frame UDP Header */
    u2UdpLen = (INT4) (sk_buffer->len - NETWORK_HDR_OFFSET);
    /* Frame udp header */
    udph = (struct udphdr *) sk_buffer->transport_header;
    /* to get the source port from global variable */
    udph->source = htons (gi4DataUdpPort);
    /* To get the destination port from db */
    udph->dest = htons (apMacData->u4UdpPort);
    udph->len = htons (u2UdpLen);
    udph->check = 0;
    udph->check =
        csum_tcpudp_magic ((i4SrcIp), (htonl (i4DestIp)), u2UdpLen,
                           IPPROTO_UDP, csum_partial (udph, u2UdpLen, 0));
    if (udph->check == 0)
        udph->check = CSUM_MANGLED_0;

    /* Frame ip header */
    u2IpLen = (INT4) (sk_buffer->len);
    iph = (struct iphdr *) sk_buffer->network_header;
    iph->version = IP_VERSION;    /*IP_VERSION = 4 */
    iph->ihl = IP_HDR_LENGTH;    /*IP_HDR_LENGTH = 5 */
    iph->tos = 0;
    iph->tot_len = htons (u2IpLen);
    iph->id = htons (0);
    iph->frag_off = 0;
    iph->ttl = IP_TTL_VALUE;    /*IP_TTL_VALUE = 64 */
    iph->protocol = IPPROTO_UDP;
    if (pStaIpData->u1WMMEnable == WMM_ENABLE
        && (pStaIpData->u1TaggingPolicy == TAG_POLICY_DSCP
            || pStaIpData->u1TaggingPolicy == TAG_POLICY_BOTH))
    {

        if (pStaIpData->u1TrustMode == TRUST_MODE)
        {
            if (u1TagStatus == 1)

            {
                iph->tos = FcapwapGetDscpFromPcp (u2Pcp);
            }
            else
            {
                /*Assigning value of TOS from Buffer */
                iph->tos = (*pu4Tos);
            }

        }
        else
        {
            /*FIX for the bug unknown DSCP */
            iph->tos = ((iph->tos & 3)
                        |
                        (FcapwapGetDscpFromQosProfile
                         (pStaIpData->u1QosProfilePriority) << 2));
        }
    }
    else
    {
        iph->tos = 0;
    }

    iph->check = 0;
    iph->saddr = i4SrcIp;
    iph->daddr = htonl (i4DestIp);

    iph->check = ip_fast_csum ((UINT1 *) iph, iph->ihl);
    if ((pStaIpData->u1WMMEnable == WMM_ENABLE)
        && ((pStaIpData->u1TaggingPolicy == TAG_POLICY_PBIT)
            || (pStaIpData->u1TaggingPolicy == TAG_POLICY_BOTH)))
    {
        new_skb = NULL;
        i4headroom = skb_headroom (sk_buffer);
        if (i4headroom < VLAN_HDR_LEN)
        {
            new_skb = skb_realloc_headroom (sk_buffer, VLAN_HDR_LEN);
            dev_kfree_skb (sk_buffer);
            if (new_skb == NULL)
            {
                printk ("<1> FcapwapWtpTxProcessDataPkt:"
                        "skb_realloc_headroom failed\n");
                return;
            }
            sk_buffer = new_skb;
        }
        memcpy (sk_buffer->data, &sk_buffer->data[VLAN_HDR_LEN],
                MAC_HDR_OFFSET);
        vlan = (struct vlan_ethhdr *) sk_buffer->data;
        vlan->h_vlan_proto = htons (ETH_P_8021Q);
        vlan->h_vlan_encapsulated_proto = vlan->h_vlan_proto;

        if (pStaIpData->u1TrustMode == TRUST_MODE)
        {

            /*The Drop Eligibilty value is always set to 0 */
            if (u1TagStatus == 1)

            {
                u2Priority = FcapwapGetPcpFromUP (u2Pcp);
            }
            else
            {
                u2Priority = 0;
            }
            u2TCI = (u2Priority << 13) & u2VlanId;
        }
        else
        {
            u2Priority =
                FcapwapGetPcpFromQosProfile (pStaIpData->u1QosProfilePriority);
            u2TCI = (u2Priority << 13) & u2VlanId;
        }
        vlan->h_vlan_TCI = u2TCI;
        memcpy (vlan->h_source, pSrcMacAddr, ETH_ALEN);
        memcpy (vlan->h_dest, apMacData->apMacAddr, ETH_ALEN);

        /*move the pointer */
        skb_pull (sk_buffer, MAC_HDR_OFFSET + VLAN_HDR_LEN);
        sk_buffer->network_header = (sk_buffer->data);
        sk_buffer->transport_header = (sk_buffer->data + NETWORK_HDR_OFFSET);
        sk_buffer->mac_header =
            (sk_buffer->data - (MAC_HDR_OFFSET + VLAN_HDR_LEN));

    }
    else
    {
        /* Frame Ethernet header */
        eh = (struct ethhdr *) sk_buffer->mac_header;
        if (eh != NULL)
            memcpy (eh->h_source, pSrcMacAddr, ETH_ALEN);
        memcpy (eh->h_dest, apMacData->apMacAddr, ETH_ALEN);
    }
    sk_buffer->pkt_type = PACKET_OUTGOING;
    eh->h_proto = htons (ETH_P_IP);

    skb_push (sk_buffer, MAC_HDR_OFFSET);

    i4Error = dev_queue_xmit (sk_buffer);

    if (i4Error)
    {
        printk (KERN_ERR "sendToStation: data transmission failed = %d\n",
                i4Error);
    }
    i4Error = net_xmit_eval (i4Error);

}

/******************************************************************************
 *  Function Name          : FcapwapGetDscpFromPcp
 *  Description            : This function returns dscp priority equivalent to
 *                           priority code point
 *  Input(s)               : priority code point
 *  Output(s)              : dscp priority
 *  Returns                : void
 * ******************************************************************************/
UINT4
FcapwapGetDscpFromPcp (UINT1 u1Pcp)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][0] == u1Pcp)
        {
            break;
        }
    }
    return gu4PriorityMapping[u1Index][1];
}

 /******************************************************************************
 *  Function Name          : FcapwapGetDscpFromQosProfile
 *  Description            : This function returns dscp priority equivalent to
 *                           Qos Profile
 *  Input(s)               : Qos Profile
 *  Output(s)              : dscp priority
 *  Returns                : void
 * ******************************************************************************/

UINT4
FcapwapGetDscpFromQosProfile (UINT1 u1QosProfile)
{
    return gu4ProfilePriorityMapping[u1QosProfile][1];
}

/******************************************************************************
 *  Function Name          : FcapwapGetPcpFromUP
 *  Description            : This function returns pcp priority equivalent to
 *                           user priority
 *  Input(s)               : User priority
 *  Output(s)              : pcp priority
 *  Returns                : void
 * ******************************************************************************/

UINT4
FcapwapGetPcpFromUP (UINT1 u1UserPriority)
{
    return gu4PriorityMapping[u1UserPriority][0];
}

/******************************************************************************
 *  Function Name          : FcapwapGetPcpFromQosProfile
 *  Description            : This function returns pcp priority equivalent to
 *                           Qos Profile
 *  Input(s)               : Qos Profile
 *  Output(s)              : pcp priority
 *  Returns                : void
 * ******************************************************************************/

UINT4
FcapwapGetPcpFromQosProfile (UINT1 u1QosProfile)
{
    return gu4ProfilePriorityMapping[u1QosProfile][0];
}

/******************************************************************************
 *  Function Name          : FcapwapGetUPFromQosProfile
 *  Description            : This function returns user priority equivalent to
 *                           Qos Profile
 *  Input(s)               : Qos Profile
 *  Output(s)              : user priority
 *  Returns                : void
 * ******************************************************************************/

UINT4
FcapwapGetUPFromQosProfile (UINT1 u1QosProfile)
{
    UINT1               u1Index = 0;
    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][0] ==
            gu4ProfilePriorityMapping[u1QosProfile][0])
        {
            break;
        }
    }
    return (UINT4) u1Index;
}

INT4
CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_HDR_LENGTH];
    UINT1               u1HdrLen = 0;
    UINT1              *pCapHdr = NULL;

    /* Get CAPWAP Header Length */
    CRU_BUF_Copy_FromBufChain (pBuf, &u1HdrLen,
                               CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);

    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);
    CRU_BUF_Copy_FromBufChain (pBuf, au1CapHdr, 0, u1HdrLen);
    pCapHdr = au1CapHdr;
    CAPWAP_GET_1BYTE (pHdr->u1Preamble, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2HlenRidWbidFlagT, pCapHdr);
    CAPWAP_GET_1BYTE (pHdr->u1FlagsFLWMKResd, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragId, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragOffsetRes3, pCapHdr);
    if (u1HdrLen > 8)
    {
        CAPWAP_GET_1BYTE (pHdr->u1RadMacAddrLength, pCapHdr);
        CAPWAP_GET_NBYTE (pHdr->RadioMacAddr, pCapHdr,
                          pHdr->u1RadMacAddrLength);
    }
    pHdr->u2HdrLen = u1HdrLen;
    return SUCCESS;
}

void
CapwapPutHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_HDR_LENGTH];

    AssembleCapwapHeader (au1CapHdr, pHdr);
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) au1CapHdr, pHdr->u2HdrLen);

}

#endif
