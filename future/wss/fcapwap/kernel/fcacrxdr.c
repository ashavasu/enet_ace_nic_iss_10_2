
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 * $Id: fcacrxdr.c,v 1.3 2017/11/30 14:01:39 siva Exp $
 * Description: This file contains RCV module driver code for WLC
 *******************************************************************/
#ifndef _FCAC_RXDR_C_
#define _FCAC_RXDR_C_

/******************************************************************************
 *                           Local Include
 *****************************************************************************/
#include "fcksglob.h"
#include "fcksprot.h"

extern INT4         gu4WlcIpAddr;
extern INT4         gi4CtrlUdpPort;
extern INT4         gi4DataUdpPort;
static tOsixCfg     LrOsixCfg;
static tMemPoolCfg  LrMemPoolCfg;
static tBufConfig   LrBufConfig;
UINT4               gu4SysTimeTicks;
UINT4               gu4PriorityMapping[USER_PRIORITY][PRIORITY_LAYER] =
    { {0, 0}, {1, 10}, {2, 18}, {0, 0}, {3, 26}, {4, 34}, {5, 46}, {6, 48} };
UINT4               gu4ProfilePriorityMapping[ACCESS_CATEGORIES][PRIORITY_LAYER]
    = { {0, 18}, {4, 34}, {5, 46}, {1, 10} };
static struct nf_hook_ops nfho_post;
/******************************************************************************
 *                           Global variables
 *****************************************************************************/
/* Initialize socket variables for data and control channel */
static INT4         FcapwapWlcTxIoctl (struct file *filep, UINT4 cmd,
                                       unsigned long arg);

INT4                i4MacType = LOCAL_MAC;

static struct file_operations file_opts = {
    .unlocked_ioctl = FcapwapWlcTxIoctl,
    .open = FcapwapWlcRxDeviceOpen,
    .release = FcapwapWlcRxDeviceRelease
};

/******************************************************************************
 *  Function Name          : init_module
 *  Description            : This function is used to initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
static INT4         __init
FcapwapWlcDrvRxInit (void)
{
    INT4                i4RetVal = 0;
    UINT4               u4SelfNodeId = SELF;

    i4RetVal =
        register_chrdev (MAJOR_NUMBER_AC_RCV, DEVICE_NAME_AC_RCV, &file_opts);

    if (i4RetVal < 0)
        printk (KERN_ERR "init_module: %s registration failed !\n",
                DEVICE_NAME_AC_RCV);
    else
        printk (KERN_INFO "init_module: %s successfully registered !\n",
                DEVICE_NAME_AC_RCV);

    nfho_post.hook = FcapwapWlcHookPostRouting;
    nfho_post.owner = THIS_MODULE;
    nfho_post.hooknum = NF_INET_POST_ROUTING;    /* First hook for IPv4 */
    nfho_post.pf = PF_INET;
    nfho_post.priority = NF_IP_PRI_FIRST;    /* Make our function first */

    nf_register_hook (&nfho_post);

/* Initialize Osix */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = u4SelfNodeId;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        return (1);
    }

    LrMemPoolCfg.u4MaxMemPools = 100;
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        PRINTF ("MemInitMemPool Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        PRINTF ("BufInitManager Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : cleanup_module
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : un-initializes the driver module
 *  Returns                : void 
 * ******************************************************************************/
static void         __exit
FcapwapWlcDrvRxExit (void)
{
    /* De-register the character driver */
    unregister_chrdev (MAJOR_NUMBER_AC_RCV, DEVICE_NAME_AC_RCV);
    nf_unregister_hook (&nfho_post);
    printk (KERN_INFO "cleanup_module: %s successfully unregistered !\n",
            DEVICE_NAME_AC_RCV);
}

module_init (FcapwapWlcDrvRxInit);
module_exit (FcapwapWlcDrvRxExit);

MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("CAPWAP in kernel layer");
MODULE_AUTHOR ("Aricent Group");

/******************************************************************************
 *  Function Name          : FcapwapWlcRxDeviceOpen
 *  Description            : This function is used to open the device
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device can be accessed
 *  Returns                : SUCCESS 
 * ******************************************************************************/
INT4
FcapwapWlcRxDeviceOpen (struct inode *inodep, struct file *filep)
{
#if DBG_LVL_01
    printk ("FcapwapWlcRxDeviceOpen: %s opened\n", DEVICE_NAME_AC_RCV);
#endif

    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcRxDeviceRelease
 *  Description            : This function is used to release the driver
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device cannot be accessed
 *  Returns                : SUCCESS
 * ******************************************************************************/
INT4
FcapwapWlcRxDeviceRelease (struct inode *inodep, struct file *filep)
{

#if DBG_LVL_01
    printk ("FcapwapWlcRxDeviceRelease: %s closed\n", DEVICE_NAME_AC_RCV);
#endif

    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcTxIoctl
 *  Description            : This function is used by user space to handle RBTree
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr, cmd - 
 *                           RB command, arg - RB tree arguments. 
 *  Output(s)              : rb tree creation, rb tree display
 *  Returns                : SUCCESS
 * ******************************************************************************/
static INT4
FcapwapWlcTxIoctl (struct file *filep, UINT4 cmd, unsigned long arg)
{

    thandleRbWlcData    thandleRb;
    tAcRcvStaTable     *StationTable = NULL;

    /*Copy user parameter from user space of Linux */
    if (copy_from_user (&thandleRb, (void *) arg, sizeof (thandleRbWlcData)))
    {
        printk (KERN_ERR "FcapwapWlcTxIoctl: copy from user failure\n");
        return -EFAULT;
    }
    /*Perform operation as per user information on RB Tree */
    switch (thandleRb.u1RbOperation)
    {
            /* create the memory pool */
        case CREATE_ENTRY:
            if (RbtCreate (thandleRb.i4RbNodes) != SUCCESS)
                printk (KERN_ERR "FcapwapWlcTxIoctl: RbTree Create Failure\n");
            break;
        case DISPLAY_ENTRY:
            RbtDisplay (thandleRb.i4TableNum);
            break;
        case INSERT_ENTRY:
            if (RbtInsert (&thandleRb.rbData, thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR "FcapwapWlcTxIoctl: RB Tree Insert Failure\n");
#if DBG_LVL_01
            RbtDisplay (thandleRb.i4TableNum);
#endif
            break;
        case REMOVE_ENTRY:
            if (RbtRemove (&thandleRb.rbData, thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR "FcapwapWlcTxIoctl: RB Tree Remove Failure\n");
            break;
        case REPLACE_ENTRY:
            if (RbtReplace
                (&thandleRb.rbData, &thandleRb.replaceData,
                 thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR
                        "FcapwapWlcTxIoctl: RB Tree Replace Failure\n");
            break;
        case DESTROY_ENTRY:
            RbtDestroy (thandleRb.i4TableNum);
            break;
        case STATION_STATUS:
            StationTable =
                APStaTableSearch (thandleRb.rbData.unTable.ApStaData.staMac);
            if (StationTable != NULL)
            {
                thandleRb.rbData.unTable.ApStaData.u1StationStatus =
                    StationTable->apStaData.u1StationStatus;
                if (copy_to_user
                    ((thandleRbWlcData *) arg, &thandleRb,
                     sizeof (thandleRbWlcData)))
                {
                    printk ("\ncopy_to_user failed\n");
                }
                StationTable->apStaData.u1StationStatus = 0;

            }
            break;

        default:
            printk ("RX CASE : %d", thandleRb.u1RbOperation);
            break;
    }
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWlcHookPostRouting 
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : hooknum - hook number
 *                           skb -  skb buffer
 *                           in  - input deviece
 *                           out - output device
 *  Returns                : SUCCESS/FAILURE
 * ******************************************************************************/

UINT4
FcapwapWlcHookPostRouting (UINT4 u4hooknum,
                           struct sk_buff *skb,
                           const struct net_device *in,
                           const struct net_device *out,
                           INT4 (*okfn) (struct sk_buff *))
{
    INT4                i4DestIp = 0;
    INT4                i4SrcIp = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4Ip = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4VlanTag = 0;
    UINT4               u4index = 0;
    UINT2               u2Dstport, u2Srcport;
    UINT2               u2EtherType = 0;
    UINT2               u2Pcp = 0;
    UINT2               u2Priority = 0;
    UINT2               u2OldCheck = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2CheckSum = 0;
    tAcRcvStaTable     *rbDataSta = NULL;
    tAcRcvStaIpTable   *rbData = NULL;
    tAcRcvAPMacTable   *rbDataApMac = NULL;
    tAcRcvVlanTable    *rbDataVlan = NULL;
    tAcRcvMacTable     *rbDataMac = NULL;
    struct udphdr      *uh = NULL;
    UINT1               au1PhyIntf[INTERFACE_LEN];
    UINT1               u1TagStatus = 0;
    UINT1               u1Flag = 0;
    UINT1               u1UserPriority = 0;
    UINT1               u1QosProfile = 0;
    UINT1               u1TrustModeStatus = 0;
    struct in_device   *out_dev;
    struct net_device  *dev;
    struct in_ifaddr   *if_info;
    struct iphdr       *ih;
    struct iphdr       *ih1;
    tAcRcvAPMacTable   *rbData_wtp_ip = NULL;
    struct ethhdr      *eh;
    UINT1               au1Mac[ETH_ALEN];
    UINT4               u4Tos = 0;
    INT4                i4Length = 0;
    int                 i = 0;
    memset (au1Mac, 0, ETH_ALEN);

    if (skb_is_nonlinear (skb))
    {
        if (skb_linearize (skb) != 0)
        {
            printk ("RX Return value is %d\n", skb);
        }

    }

    ih = (struct iphdr *) ip_hdr (skb);
    /*Getting TOS Value from Buffer */
    u4Tos = (ip_hdr (skb))->tos;
    u4Ip = ntohl (ih->daddr);
    u4SrcIp = ntohl (ih->saddr);

    u4Protocol = (ip_hdr (skb))->protocol;
    uh = (struct udphdr *) (skb->data + (ih->ihl * IHL_SIZE));
    u2Dstport = ntohs ((UINT2) uh->dest);
    u2Srcport = ntohs ((UINT2) uh->source);

    if ((u4Protocol == UDP) && ((u2Dstport == DHCP_DEST_PORT) ||
                                (u2Dstport == DHCP_SRC_PORT))
        && ((u2Srcport == DHCP_DEST_PORT) || (u2Srcport == DHCP_SRC_PORT)))
    {
        return NF_ACCEPT;
    }

    /* Added the condition to check AP and AC are running in the */
    /* same machine */
    if (u4SrcIp == u4Ip)
    {
        return NF_ACCEPT;
    }

    rbData = APSta_IP_Table_Search (u4Ip);
    if (rbData != NULL)
    {
        /* Packets destined to STA from server */
        /* Copy the station mac address from the recieved packet */
        rbDataSta = APStaTableSearch (rbData->staIpData.staMac);

        /* If the mac address of the station is not present, 
         * transmit the packets adding the capwap header */
        if (rbDataSta != NULL)
        {
            /*Data packets towards station need not be processed in case of local routing */
            if (rbDataSta->apStaData.u1LocalRouting == 1)
            {
                return NF_ACCEPT;
            }
            /*Set the Station Status to 1 */
            rbDataSta->apStaData.u1StationStatus = 1;

            /* Destionation IP - WTP IP
               Source IP - WLC IP
               Destination MAC - WTP MAC
               Source MAC - WLC MAC */

            /* Get the Destination IP (WTP IP) from the database */
            /* Since the database is queried, store the bssid too */
            i4DestIp = rbDataSta->apStaData.u4ipAddr;

            /* Added the condition to check AP and AC are running in the */
            /* same machine */
            if (i4DestIp == INADDR_LOOPBACK)
            {
                return NF_ACCEPT;
            }

            rbDataApMac = APMacTableSearch (i4DestIp);

            if ((skb->len + (MAC_HDR_OFFSET + CAPWAP_HDR_LOCAL_SIZE)) >
                rbDataApMac->apMacData.u4PathMTU)
            {
                /*Packet size greater than the MTU of the WTP */

                /* return NF_ACCEPT; */
            }

            if (rbDataApMac != NULL)
            {
#if DBG_LVL_01
                printk ("<1> %x:%x:%x:%x:%x:%x\n",
                        rbDataApMac->apMacData.apMacAddr[0],
                        rbDataApMac->apMacData.apMacAddr[1],
                        rbDataApMac->apMacData.apMacAddr[2],
                        rbDataApMac->apMacData.apMacAddr[3],
                        rbDataApMac->apMacData.apMacAddr[4],
                        rbDataApMac->apMacData.apMacAddr[5]);
#endif
                rbDataMac = MacTableSearch (rbDataApMac->apMacData.apMacAddr);
                if (rbDataMac != NULL)
                {
                    memcpy (au1PhyIntf, rbDataMac->MacData.au1PhysicalInterface,
                            INTERFACE_LEN);
                    // printk ("<1> AP INTF - %s\n", au1PhyIntf);
                    /* Get the VLAN ID from mac table and check whether the vlan is
                     * tagged port or untagged */

#if DBG_LVL_01                    /*This code is temporarily commented out to resolve the issue that
                                   data packets are sent to user space since vlan support is not 
                                   added in WMM */
                    rbDataVlan = VlanRxTableSearch (rbDataMac->MacData.u2VlanId,
                                                    rbDataMac->MacData.
                                                    au1PhysicalInterface);

                    if (rbDataVlan == NULL)
                    {
                        printk ("remove rbDataVlan == NULL\n");
                        return NF_ACCEPT;
                    }

                    /* If Vlan tag status is set send the fetch the vlan id which needs
                     * to be added to the packet header */
                    printk ("\n Remove - VLAN ID = %d Tag Status = %d\n",
                            rbDataVlan->VlanData.u2VlanId,
                            rbDataVlan->VlanData.u1Tagged);
                    if (rbDataVlan->VlanData.u1Tagged != 0)
                    {
                        printk ("Entered Remove vlan\n");
                        FcapwapRemoveVlanHeader (skb,
                                                 rbDataVlan->VlanData.u2VlanId);
                    }
#endif

                }
                else
                {
                    //printk("<1>AP mac addr not found\n");
                }
            }
            else
            {
                //  printk("<1>AP mac table not found\n");
            }
            dev = dev_get_by_name (&init_net, au1PhyIntf);
            if (dev != NULL)
            {
                if (dev->dev_addr != NULL)
                {
                    skb->dev = dev;
                    out_dev = (struct in_device *) dev->ip_ptr;
                    if (out_dev != NULL)
                    {
                        for (if_info = out_dev->ifa_list; if_info != NULL;
                             if_info = if_info->ifa_next)
                        {
                            if (!(strcmp (if_info->ifa_label, dev->name)))
                            {
                                i4SrcIp = if_info->ifa_address;
                                break;
                            }
                        }
                        /* Source MAC (WLC MAC) is obtained from the outdevice strucuture out_devaddr */

                        if (out->dev_addr != NULL)
                        {
                            i4SrcIp = OSIX_HTONL (gu4WlcIpAddr);
                            FcapwapTransmitPacketToWtp (skb, dev->dev_addr,
                                                        out->dev_addr, i4SrcIp,
                                                        i4DestIp,
                                                        rbDataSta->apStaData.
                                                        bssid,
                                                        &rbData->staIpData,
                                                        &u4Tos);
                            dev_put (dev);
                            return NF_STOLEN;
                        }
                    }
                }
                dev_put (dev);
            }
            else
            {
                /* printk ("\n Device is null"); */
            }
        }
        else
        {
            /*printk ("\n APStaTableSearch returns NULL"); */
        }
    }
    else
    {
        /* printk ("SRC IP - %02x\n", u4SrcIp); */
        rbData = APSta_IP_Table_Search (u4SrcIp);
        if (rbData == NULL)
        {
            /* Check whether the packet is destined to AP and
             *              * CAPWAP header is present */
            rbData_wtp_ip = APMacTableSearch (u4Ip);
            if (rbData_wtp_ip != NULL)
            {
                /* Added the condition to check AP and AC are running in the */
                /* same machine */
                if (rbData_wtp_ip->apMacData.u4ipAddr == INADDR_LOOPBACK)
                {
                    return NF_ACCEPT;
                }
                if ((UINT2) rbData_wtp_ip->apMacData.u4UdpPort == u2Dstport)
                {
                    i4Length = NETWORK_HDR_OFFSET + TRANSPORT_HDR_SIZE;
                    /* Packet not destined to WTP */
                    if (rbData_wtp_ip->apMacData.u1macType == SPLIT_MAC)
                    {
                        i4Length += CAPWAP_HDR_SPLIT_SIZE;
                    }
                    else
                    {
                        i4Length += CAPWAP_HDR_LOCAL_SIZE;
                    }
                    skb_pull (skb, i4Length);
                    eh = (struct ethhdr *) skb->data;
                    if (eh != NULL)
                    {
                        memcpy (au1Mac, eh->h_dest, ETH_ALEN);
#if DBG_LVL_01
                        printk ("DEST MAC - %x:%x:%x:%x:%x:%x\n", au1Mac[0],
                                au1Mac[1], au1Mac[2], au1Mac[3], au1Mac[4],
                                au1Mac[5]);
#endif
                        rbDataSta = APStaTableSearch (au1Mac);
                        if (rbDataSta != NULL)
                        {
                            if ((rbDataSta->apStaData.u1WMMEnable == WMM_ENABLE)
                                &&
                                ((rbDataSta->apStaData.u1TaggingPolicy ==
                                  TAG_POLICY_DSCP)
                                 || (rbDataSta->apStaData.u1TaggingPolicy ==
                                     TAG_POLICY_BOTH)))
                            {
                                printk
                                    ("WMM is enabled and tagging policy is set\n");
                                u1Flag = 1;
                            }
                        }
                    }

                    skb_push (skb, i4Length);
                    if (u1Flag == 1)
                    {
                        u1UserPriority = (UINT1) skb->priority;
                        u1QosProfile =
                            rbDataSta->apStaData.u1QosProfilePriority;
                        u1TrustModeStatus = rbDataSta->apStaData.u1TrustMode;
                        printk ("u1TrustModeStatus = %d\n", u1TrustModeStatus);
                        printk ("u1QosProfile = %d\n", u1QosProfile);
                        printk ("u1UserPriority = %d\n", u1UserPriority);

                        if (u1TrustModeStatus == TRUST_MODE)
                        {
                            /*Moving the pointer to inner ip header to get dscp value */
                            skb_pull (skb, 58);

                            ih1 = (struct iphdr *) (skb->data);
                            u2OldCheck = ih->check;
                            ih->check = 0;
                            /*Assuming that dscp value consists of bott dscp 6 bits and ECN 2 bits */
                            ih->tos = ih1->tos;
                            u2CheckSum = IpCalcCheckSum (ih, 20);
                            ih->check = u2CheckSum;

                            skb_push (skb, 58);

                        }
                        else if (u1TrustModeStatus == NO_TRUST_MODE)
                        {
                            u2OldCheck = ih->check;
                            ih->check = 0;
/*FIX for the bug unknown DSCP*/
                            ih->tos = ((ih->tos & 3)
                                       |
                                       (FcapwapGetDscpFromQosProfile
                                        (u1QosProfile) << 2));
                            u2CheckSum = IpCalcCheckSum (ih, 20);
                            ih->check = u2CheckSum;
                        }
                        printk ("value of checksum %x Old checksum %x \n",
                                u2CheckSum, u2OldCheck);
                        printk ("value of ih->tos = %d\n", ih->tos);
                    }
                    else
                    {
#if DBG_LVL_01
                        printk ("WMM not enabled \n");
#endif
                    }
                }
            }

            return NF_ACCEPT;
        }
        else
        {
            return NF_ACCEPT;
        }

        /* Packets from STA to network */
        rbDataSta = APStaTableSearch (rbData->staIpData.staMac);

        /* If the mac address of the station is not present, 
         * transmit the packets adding the capwap header */
        if (rbDataSta != NULL)
        {
            memcpy (&u2EtherType, skb->data - ETHERTYPE_LEN, ETHERTYPE_LEN);
            if (ntohs (u2EtherType) == VLAN_ETHERTYPE)
            {
                memcpy (&u4VlanTag,
                        (skb->data - (ETHERTYPE_LEN + VLAN_HDR_LEN)),
                        VLAN_HDR_LEN);
                u2Pcp = (u4VlanTag & 255) >> 13;
                //      vlan_untag(skb);
                u1TagStatus = 1;
            }

            /* Get the Destination IP (WTP IP) from the database */
            /* Since the database is queried, store the bssid too */
            i4DestIp = rbDataSta->apStaData.u4ipAddr;

            rbDataApMac = APMacTableSearch (i4DestIp);
            if (rbDataApMac != NULL)
            {
#if DBG_LVL_01
                printk ("<1> %x:%x:%x:%x:%x:%x\n",
                        rbDataApMac->apMacData.apMacAddr[0],
                        rbDataApMac->apMacData.apMacAddr[1],
                        rbDataApMac->apMacData.apMacAddr[2],
                        rbDataApMac->apMacData.apMacAddr[3],
                        rbDataApMac->apMacData.apMacAddr[4],
                        rbDataApMac->apMacData.apMacAddr[5]);
#endif
                rbDataMac = MacTableSearch (rbDataApMac->apMacData.apMacAddr);
                if (rbDataMac == NULL)
                {
                    return NF_ACCEPT;
                }
                memcpy (au1PhyIntf, rbDataMac->MacData.au1PhysicalInterface,
                        INTERFACE_LEN);
            }
            else
            {
                return NF_ACCEPT;
            }
            /* Get the VLAN ID from mac table and check whether the vlan is
             * tagged port or untagged */
            rbDataVlan = VlanRxTableSearch (rbDataMac->MacData.u2VlanId,
                                            rbDataMac->MacData.
                                            au1PhysicalInterface);

            if (rbDataVlan == NULL)
            {
                return NF_ACCEPT;
            }

            if (rbDataSta->apStaData.u1WMMEnable == WMM_ENABLE
                && (rbDataSta->apStaData.u1TaggingPolicy == TAG_POLICY_DSCP
                    || rbDataSta->apStaData.u1TaggingPolicy == TAG_POLICY_BOTH))
            {

                if (rbDataSta->apStaData.u1TrustMode == TRUST_MODE)
                {
                    if (u1TagStatus == 1)

                    {
                        ih->tos = FcapwapGetDscpFromPcp (u2Pcp);
                    }
                    else
                    {
                        ih->tos = 0;
                    }

                }
                else
                {
/*FIX for the bug unknown DSCP*/
                    ih->tos = ((ih->tos & 3)
                               | (FcapwapGetDscpFromQosProfile (u1QosProfile) <<
                                  2));
                }
            }

            printk ("\n ADD - VLAN ID = %d Tag Status = %d\n",
                    rbDataVlan->VlanData.u2VlanId,
                    rbDataVlan->VlanData.u1Tagged);
            /* If Vlan tag status is set send the fetch the vlan id which needs
             * to be added to the packet header */
            if (rbDataVlan->VlanData.u1Tagged != 0)
            {
                /* printk("Entered add vlan tag\n"); */
                if (rbDataSta->apStaData.u1WMMEnable == WMM_ENABLE
                    &&
                    ((rbDataSta->apStaData.u1TaggingPolicy == TAG_POLICY_PBIT)
                     || (rbDataSta->apStaData.u1TaggingPolicy ==
                         TAG_POLICY_BOTH)))
                {
                    if (rbDataSta->apStaData.u1TrustMode == TRUST_MODE)
                    {

                        /*The Drop Eligibilty value is always set to 0 */
                        if (u1TagStatus == 1)
                        {
                            u2Priority = FcapwapGetPcpFromUP (u2Pcp);
                        }
                        else
                        {
                            u2Priority = 0;
                        }
                        u2VlanId =
                            (u2Priority << 13) & rbDataVlan->VlanData.u2VlanId;
                    }
                    else
                    {
                        u2Priority =
                            FcapwapGetPcpFromQosProfile (rbDataSta->apStaData.
                                                         u1QosProfilePriority);
                        u2VlanId =
                            (u2Priority << 13) & rbDataVlan->VlanData.u2VlanId;
                    }

                    FcapwapAddVlanHeader (skb, u2VlanId);
                }
            }

        }
    }

    return NF_ACCEPT;
}

/******************************************************************************
 *  Function Name          : FcapwapTransmitPacketToWtp 
 *  Description            : This function is used to transmit the packets to WTP
 *  Input(s)               : skb -  skb buffer
 *                           pSrcMacAddr  - Src mac addr
 *                           pDataSrcMacAddr - Vlan Mac addr
 *                           i4SrcIp  - Source IP address
 *                           i4DestIp - Destination IP address
 *                           pu1Bssid - BSSID
 *                           pDataDstMacAddr - Destination mac addr
 *  Returns                : SUCCESS/FAILURE
 * ******************************************************************************/

INT4
FcapwapTransmitPacketToWtp (struct sk_buff *sk_buffer, UINT1 *pSrcMacAddr,
                            UINT1 *pDataSrcMacAddr, INT4 i4SrcIp, INT4 i4DestIp,
                            UINT1 *pu1Bssid, tStaIpInfo * pStaIpData,
                            UINT4 *pu4Tos)
{
    INT4                i4headroom = 0, i4Length = 0, i4Error = 0;
    struct ethhdr      *eh;
    struct vlan_ethhdr *vlan = NULL;
    tAcRcvAPMacTable   *rbData = NULL;
    tAcRcvMacTable     *MacTable = NULL;
    UINT2               u2UdpLen = 0, u2IpLen = 0;
    UINT2               u2Tmp = 0;
    UINT2               u2EtherType = 0;
    UINT2               u2Pcp = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2Priority = 0;
    UINT2               u2TCI = 0;
    UINT4               u4VlanTag = 0;
    UINT1               u1TagStatus = 0;
    struct udphdr      *udph;
    struct iphdr       *iph;
    struct sk_buff     *new_skb = NULL;

    memcpy (&u2EtherType, sk_buffer->data - ETHERTYPE_LEN, ETHERTYPE_LEN);
    if (ntohs (u2EtherType) == VLAN_ETHERTYPE)
    {
        memcpy (&u4VlanTag, (sk_buffer->data - (ETHERTYPE_LEN + VLAN_HDR_LEN)),
                VLAN_HDR_LEN);
        u2Pcp = (u4VlanTag & 255) >> 13;
//  vlan_untag(sk_buffer);
        u1TagStatus = 1;
    }

    skb_push (sk_buffer, MAC_HDR_OFFSET);

    /* Frame Ethernet header */
    sk_buffer->mac_header = sk_buffer->data;
    eh = (struct ethhdr *) sk_buffer->mac_header;

    if (eh != NULL)
    {
        memcpy (eh->h_source, pDataSrcMacAddr, ETH_ALEN);
        memcpy (eh->h_dest, pStaIpData->staMac, ETH_ALEN);
        if (eh->h_proto != PNAC_ETHERTYPE)
        {
            eh->h_proto = htons (ETH_P_IP);
        }
    }

    /* Get WTP IP using RB Tree */
    rbData = APMacTableSearch (i4DestIp);

    if (rbData != NULL)
    {
        if (pStaIpData != NULL)
        {
            if (pStaIpData->u1WMMEnable == WMM_ENABLE)
            {
                /*TODO To be discussed */
                MacTable = MacTableSearch (pStaIpData->staMac);
                if (MacTable != NULL)
                {
                    u2VlanId = MacTable->MacData.u2VlanId;
                }
            }
        }
        /*get available headroom size from skb */
        i4headroom = skb_headroom (sk_buffer);

        /*calculate required headroom in skb for us */
        i4Length = TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;

        if (rbData->apMacData.u1macType == SPLIT_MAC)
        {
            i4Length += CAPWAP_HDR_SPLIT_SIZE;
        }
        else
        {
            i4Length += CAPWAP_HDR_LOCAL_SIZE;
        }
        /*allocate required headroom if we hold less */

        if (i4headroom < i4Length)
        {
            new_skb = skb_realloc_headroom (sk_buffer, i4Length + 18);
            dev_kfree_skb (sk_buffer);
            if (new_skb == NULL)
            {
                printk ("<1> skb_realloc_headroom failed\n");
                return FAIL;
            }
            sk_buffer = new_skb;
        }

        if (rbData->apMacData.u1macType == SPLIT_MAC)
        {
            i4MacType = SPLIT_MAC;

            /* Convert the dot 3 to dot11 packet in the case of split mac */
            if (Dot3ToDot11
                (&sk_buffer->data, pu1Bssid, &sk_buffer->len,
                 DOT11_FC_BYTE1_DIR_AP_ST) != SUCCESS)
            {
                printk ("Dot3 to Dot11 conversion failed\n");
            }
        }
        /*re structure skb buffer */
        skb_push (sk_buffer, i4Length);

        skb_pull (sk_buffer, MAC_HDR_OFFSET);

        /*relocate skb data pointer */
        sk_buffer->network_header = (sk_buffer->data);
        sk_buffer->transport_header = (sk_buffer->data + NETWORK_HDR_OFFSET);
        sk_buffer->mac_header = (sk_buffer->data - MAC_HDR_OFFSET);

        /* printk ("\n Before adding capwap header\n"); */
        FcapwapAddCapwapHeader (&sk_buffer->data[CAPWAP_HDR_OFFSET], pu1Bssid);

        if ((rbData->apMacData.u1FragReassembleStatus == FRAGMENTATION_ENABLE)
            && (sk_buffer->len > rbData->apMacData.u4PathMTU))
        {
            FcapwapFragmentDataAndSend (sk_buffer, &(rbData->apMacData),
                                        sk_buffer->len, i4SrcIp, i4DestIp,
                                        pStaIpData, u1TagStatus, u2Pcp,
                                        u2VlanId, pSrcMacAddr, pu4Tos);

        }
        else
        {

            FCapwapFormOuterHeader (sk_buffer, &(rbData->apMacData), i4SrcIp,
                                    i4DestIp, pStaIpData, u1TagStatus,
                                    u2Pcp, u2VlanId, pSrcMacAddr, pu4Tos);
        }

    }
    else
    {
        printk ("Packet not sent to AP %x\n", i4DestIp);
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : FcapwapAddCapwapHeader
 *  Description            : This function is used to add the CAPWAP header
 *                           to the incoming data packet.
 *  Input(s)               : *pu1Buf - Buffer
 *  Output(s)              : packet will be verified whether it is mgmnt or data
 *  Returns                : void 
 * ******************************************************************************/
void
FcapwapAddCapwapHeader (UINT1 *pu1Buf, UINT1 *pu1Bssid)
{
    tCapwapHdr          pCapwapHdr;

    memcpy (pCapwapHdr.RadioMacAddr, pu1Bssid, MACADDR_SIZE);
    CapwapConstructCpHeader (&pCapwapHdr, i4MacType);
    AssembleCapwapHeader (pu1Buf, &pCapwapHdr);
}

/******************************************************************************
 *  Function Name          : FcapwapAddVlanHeader
 *  Description            : This function is used to add the VLAN header
 *                           to the outgoing data packet.
 *  Input(s)               : *pu1Buf - Buffer
 *  Output(s)              : packet will be verified whether it is mgmnt or data
 *  Returns                : void 
 * ******************************************************************************/
void
FcapwapAddVlanHeader (struct sk_buff *pSkb, UINT2 u2VlanId)
{
    INT4                i4Len = VLAN_HDR_LEN;
    INT4                i4headroom = 0;
    struct sk_buff     *pNew_Skb = NULL;
    struct vlan_ethhdr *vlan = NULL;

    /* Move the skb data to ethernet header */
    skb_push (pSkb, MAC_HDR_OFFSET);

    i4headroom = skb_headroom (pSkb);
    /* Get the available headroom size from skb */
    if (i4headroom < i4Len)
    {
        printk ("Allocate headroom\n");
        pNew_Skb = skb_realloc_headroom (pSkb, i4Len);
        dev_kfree_skb (pSkb);
        if (pNew_Skb == NULL)
        {
            printk ("<1> FcapwapAddVlanHeader: skb_realloc_headroom failed\n");
            return;
        }
        pSkb = pNew_Skb;
    }
    /* Move the pointer to start of the skb */
    skb_push (pSkb, i4Len);

    memcpy (pSkb->data, &pSkb->data[VLAN_HDR_LEN], MAC_HDR_OFFSET);

    /* vlan = (struct vlan_ethhdr *)skb_mac_header(pSkb); */
    vlan = (struct vlan_ethhdr *) pSkb->data;

    vlan->h_vlan_encapsulated_proto = vlan->h_vlan_proto;
    vlan->h_vlan_proto = ntohs (0x8100);
    vlan->h_vlan_TCI = ntohs (u2VlanId);

    /* Move the pointer to IP header */
/*    skb_pull (pSkb, 2 + VLAN_HDR_LEN);*/
    skb_pull (pSkb, MAC_HDR_OFFSET + VLAN_HDR_LEN);
    pSkb->network_header = (pSkb->data);
    pSkb->transport_header = (pSkb->data + NETWORK_HDR_OFFSET);
    pSkb->mac_header = (pSkb->data - (MAC_HDR_OFFSET + VLAN_HDR_LEN));
}

/******************************************************************************
 *  Function Name          : FcapwapRemoveVlanHeader
 *  Description            : This function is used to add the VLAN header
 *                           to the outgoing data packet.
 *  Input(s)               : *pu1Buf - Buffer
 *  Output(s)              : packet will be verified whether it is mgmnt or data
 *  Returns                : void 
 * ******************************************************************************/
void
FcapwapRemoveVlanHeader (struct sk_buff *pSkb, UINT2 u2VlanId)
{
    memcpy (pSkb->data, &pSkb->data[VLAN_HDR_LEN], (pSkb->len - VLAN_HDR_LEN));

    pSkb->len = (pSkb->len - VLAN_HDR_LEN);

    /* Move the pointer to IP header */
    pSkb->network_header = (pSkb->data);
    pSkb->transport_header = (pSkb->data + NETWORK_HDR_OFFSET);
    pSkb->mac_header = (pSkb->data - MAC_HDR_OFFSET);

}

/******************************************************************************
 *  Function Name          : FcapwapGetDscpFromPcp
 *  Description            : This function returns dscp priority equivalent to 
 *                   priority code point
 *  Input(s)               : priority code point 
 *  Output(s)              : dscp priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetDscpFromPcp (UINT1 u1Pcp)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][0] == u1Pcp)
        {
            break;
        }
    }
    return gu4PriorityMapping[u1Index][1];
}

 /******************************************************************************
 *  Function Name          : FcapwapGetDscpFromQosProfile
 *  Description            : This function returns dscp priority equivalent to 
 *                   Qos Profile
 *  Input(s)               : Qos Profile 
 *  Output(s)              : dscp priority
 *  Returns                : void 
 * ******************************************************************************/

UINT4
FcapwapGetDscpFromQosProfile (UINT1 u1QosProfile)
{
    return gu4ProfilePriorityMapping[u1QosProfile][1];
}

/******************************************************************************
 *  Function Name          : FcapwapGetPcpFromUP
 *  Description            : This function returns pcp priority equivalent to 
 *                   user priority
 *  Input(s)               : User priority 
 *  Output(s)              : pcp priority
 *  Returns                : void 
 * ******************************************************************************/

UINT4
FcapwapGetPcpFromUP (UINT1 u1UserPriority)
{
    return gu4PriorityMapping[u1UserPriority][0];
}

/******************************************************************************
 *  Function Name          : FcapwapGetPcpFromQosProfile
 *  Description            : This function returns pcp priority equivalent to 
 *                   Qos Profile 
 *  Input(s)               : Qos Profile 
 *  Output(s)              : pcp priority
 *  Returns                : void 
 * ******************************************************************************/

UINT4
FcapwapGetPcpFromQosProfile (UINT1 u1QosProfile)
{
    return gu4ProfilePriorityMapping[u1QosProfile][0];
}

/******************************************************************************
 *  Function Name          : FcapwapGetUPFromQosProfile
 *  Description            : This function returns user priority equivalent to 
 *                   Qos Profile
 *  Input(s)               : Qos Profile
 *  Output(s)              : user priority
 *  Returns                : void 
 * ******************************************************************************/

UINT4
FcapwapGetUPFromQosProfile (UINT1 u1QosProfile)
{
    UINT1               u1Index = 0;
    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][0] ==
            gu4ProfilePriorityMapping[u1QosProfile][0])
        {
            break;
        }
    }
    return (UINT4) u1Index;
}

/******************************************************************************
 *  Function Name          : IpCalcCheckSum
 *  Description            : This function returns checksum value
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : UNIT2
 * ******************************************************************************/

UINT2
IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pBuf = pSkb;
    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);
    printk ("value of tmp %x\n", u2Tmp);
    return ((u2Tmp));

}

/******************************************************************************
 *  Function Name          : FcapwapGetUPFromPcp
 *  Description            : This function returns user priority equivalent to 
 *                           pcp
 *  Input(s)               : pcp
 *  Output(s)              : user priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetUPFromPcp (UINT1 u1Pcp)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][0] == u1Pcp)
        {
            break;
        }
    }
    return (UINT4) u1Index;
}

/************************************************************************/
/*  Function Name   : FcapwapFragmentDataAndSend                        */
/*                                                                      */
/*  Description     : The function fragments  the received CAPWAP       */
/*                    Data Packets based on the MTU size                */
/*                                                                      */
/*  Input(s)        : skb   - Packet to be Fragmented                   */
/*                    u4MTU - MTU Size                                  */
/*                    pSessEntry - Session Entry for the AP/AC          */
/*                    i2PktLen - Packet Length                          */
/*                    u1PktType - CAPWAP Control/Data Packet            */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : SUCCESS/FAILURE                                   */
/************************************************************************/

INT4
FcapwapFragmentDataAndSend (struct sk_buff *skb, tApMacInfo * apMacData,
                            UINT4 u4PktLen, INT4 i4SrcIp, INT4 i4DestIp,
                            tStaIpInfo * pStaIpData, UINT1 u1TagStatus,
                            UINT2 u2Pcp, UINT2 u2VlanId, UINT1 *pSrcMacAddr,
                            UINT4 *pu4Tos)
{

    UINT2               u2FragDataSize = 0;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;
    UINT2               u2FragId = 0;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pPktBuf = NULL;
    UINT1               u1FlgOffs = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFrag;
    tCapwapHdr          CapHdr;
    UINT1               u1Type[2];
    UINT1               u1Length[2];
    UINT2               u2PacketLen = 0;
    UINT1               u1LastFrag = OSIX_FALSE;
    UINT4               u4MTU = 0;
    UINT1               u1IsFragmented = 0;
    struct net_device  *org_dev = NULL;

    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    struct sk_buff     *new_skb = NULL;

    MEMSET (&CapHdr, 0, sizeof (tCapwapHdr));
    MEMSET (&u1Type, 0, sizeof (UINT2));
    MEMSET (&u1Length, 0, sizeof (UINT2));

    u4MTU = apMacData->u4PathMTU;

    if ((pPktBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        printk
            ("FcapwapFragmentDataAndSend: CRU Buffer Memory Allocation Failed\n");
        return FAIL;
    }

    pPktBuf->pSkb = skb;
    org_dev = skb->dev;

    skb_pull (pPktBuf->pSkb, CAPWAP_HDR_OFFSET);

    u2PacketLen = CRU_BUF_Get_ChainValidByteCount (pPktBuf);

    /* Get CAPWAP Header Length */
    CRU_BUF_Copy_FromBufChain (pPktBuf, &u1HdrLen,
                               CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CRU_BUF_Copy_FromBufChain (pPktBuf, &u1FlgOffs,
                               CAPWAP_FLAGS_BIT_OFFSET,
                               CAPWAP_FLAGS_OFFSET_LEN);

    i1LastFrag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Get Capwap Header */
    CapwapExtractHdr (pPktBuf, &CapHdr);

    /* Move the cru buffer */
    u2PacketLen = (INT2) (u2PacketLen - u1HdrLen);
    CRU_BUF_Move_ValidOffset (pPktBuf, u1HdrLen);

    u2FragId = apMacData->u2FragId;

    /*  Fragments should be in  multpIple of 8 bytes */
    if (u4MTU < u1HdrLen)
    {
        printk
            ("FcapwapFragmentDataAndSend: MTU is less than capwap header length\n");
        CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
        return FAIL;
    }

    u4MTU = (UINT4) (u4MTU - u1HdrLen);

    pTmpBuf = pPktBuf;

    while (u2PacketLen > 0)
    {
        if ((UINT2) (u2PacketLen) <= u4MTU)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = (UINT2) u2PacketLen;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_LAST_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1LastFrag = OSIX_TRUE;
            u1IsFragmented = 0;
        }
        else
        {
            u2FragDataSize = (UINT2) u4MTU;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= i1LastFrag;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1IsFragmented = 1;

            if (CRU_BUF_Fragment_BufChain (pPktBuf, u2FragDataSize, &pTmpBuf)
                != CRU_SUCCESS)
            {
                printk ("FcapwapFragmentDataAndSend: Failed to Fragment the "
                        "Packet\n");
                CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
                return FAIL;
            }

            if (pTmpBuf == NULL)
            {
                printk
                    ("Error in Fragmentation :- Fragmented Buff is NULL\r\n");
                CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
                return OSIX_FAILURE;
            }

        }

        u2Len = (UINT2) (CRU_BUF_Get_ChainValidByteCount (pPktBuf));

        /* Now the datagram to send is in pPktBuf; Add frag specific info */
        u2Offset += u2FragDataSize;    /* Each time offset gets updated */
        u2PacketLen -= u2FragDataSize;

        CapwapPutHdr (pPktBuf, &CapHdr);

        if (u1IsFragmented == 1)
        {
            /*get available headroom size from skb */
            i4headroom = skb_headroom (pTmpBuf->pSkb);
            i4Length = TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;
            /*allocate required headroom if we hold less */

            if (i4headroom < i4Length)
            {
                new_skb = skb_realloc_headroom (pTmpBuf->pSkb, i4Length + 18);
                dev_kfree_skb (pTmpBuf->pSkb);
                if (new_skb == NULL)
                {
                    printk ("<1> skb_realloc_headroom failed\n");
                    return FAIL;
                }
                pTmpBuf->pSkb = new_skb;
            }

        }

        skb_push (pPktBuf->pSkb, CAPWAP_HDR_OFFSET);

        /*relocate skb data pointer */
        pPktBuf->pSkb->network_header = (pPktBuf->pSkb->data);
        pPktBuf->pSkb->transport_header =
            (pPktBuf->pSkb->data + NETWORK_HDR_OFFSET);
        pPktBuf->pSkb->mac_header = (pPktBuf->pSkb->data - MAC_HDR_OFFSET);

        if (pPktBuf->pSkb->dev == NULL)
        {
            pPktBuf->pSkb->dev = org_dev;
        }

        FCapwapFormOuterHeader (pPktBuf->pSkb, apMacData, i4SrcIp, i4DestIp,
                                pStaIpData, u1TagStatus, u2Pcp, u2VlanId,
                                pSrcMacAddr, pu4Tos);

        if (pPktBuf != NULL)
        {
            pPktBuf->pSkb = NULL;
            CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
            pPktBuf = NULL;
        }

    }
    /* Increment the Fragment Id */
    apMacData->u2FragId++;
    return SUCCESS;
}

void
FCapwapFormOuterHeader (struct sk_buff *sk_buffer, tApMacInfo * apMacData,
                        INT4 i4SrcIp, INT4 i4DestIp, tStaIpInfo * pStaIpData,
                        UINT1 u1TagStatus, UINT2 u2Pcp, UINT2 u2VlanId,
                        UINT1 *pSrcMacAddr, UINT4 *pu4Tos)
{

    INT4                i4headroom = 0;
    INT4                i4Error = 0;
    UINT2               u2UdpLen = 0;
    UINT2               u2IpLen = 0;
    UINT2               u2Priority = 0;
    UINT2               u2TCI = 0;
    struct ethhdr      *eh;
    struct udphdr      *udph;
    struct iphdr       *iph;
    struct sk_buff     *new_skb = NULL;
    struct vlan_ethhdr *vlan = NULL;

    /*Frame UDP Header */
    u2UdpLen = (INT4) (sk_buffer->len - NETWORK_HDR_OFFSET);
    /* Frame udp header */
    udph = (struct udphdr *) sk_buffer->transport_header;
    /* to get the source port from global variable */
    udph->source = htons (gi4DataUdpPort);
    /* To get the destination port from db */
    udph->dest = htons (apMacData->u4UdpPort);
    udph->len = htons (u2UdpLen);
    udph->check = 0;
    udph->check =
        csum_tcpudp_magic ((i4SrcIp), (htonl (i4DestIp)), u2UdpLen,
                           IPPROTO_UDP, csum_partial (udph, u2UdpLen, 0));
    if (udph->check == 0)
        udph->check = CSUM_MANGLED_0;

    /* Frame ip header */
    u2IpLen = (INT4) (sk_buffer->len);
    iph = (struct iphdr *) sk_buffer->network_header;
    iph->version = IP_VERSION;    /*IP_VERSION = 4 */
    iph->ihl = IP_HDR_LENGTH;    /*IP_HDR_LENGTH = 5 */

    iph->tos = 0;
    iph->tot_len = htons (u2IpLen);
    iph->id = htons (0);
    iph->frag_off = 0;
    iph->ttl = IP_TTL_VALUE;    /*IP_TTL_VALUE = 64 */
    iph->protocol = IPPROTO_UDP;
    if (pStaIpData->u1WMMEnable == WMM_ENABLE
        && (pStaIpData->u1TaggingPolicy == TAG_POLICY_DSCP
            || pStaIpData->u1TaggingPolicy == TAG_POLICY_BOTH))
    {

        if (pStaIpData->u1TrustMode == TRUST_MODE)
        {
            if (u1TagStatus == 1)

            {
                /*Check for Pbit enabled */
                iph->tos = FcapwapGetDscpFromPcp (u2Pcp);
            }
            else
            {
                /*Assigning value of TOS from Buffer */
                iph->tos = (*pu4Tos);
            }

        }
        else
        {
            /*FIX for the bug unknown DSCP */
            iph->tos = ((iph->tos & 3)
                        |
                        (FcapwapGetDscpFromQosProfile
                         (pStaIpData->u1QosProfilePriority) << 2));
        }
    }
    else
    {
        iph->tos = 0;
    }

    iph->check = 0;
    iph->saddr = i4SrcIp;
    iph->daddr = htonl (i4DestIp);

    iph->check = ip_fast_csum ((UINT1 *) iph, iph->ihl);
    if ((pStaIpData->u1WMMEnable == WMM_ENABLE)
        && ((pStaIpData->u1TaggingPolicy == TAG_POLICY_PBIT)
            || (pStaIpData->u1TaggingPolicy == TAG_POLICY_BOTH)))
    {
        new_skb = NULL;
        i4headroom = skb_headroom (sk_buffer);
        if (i4headroom < VLAN_HDR_LEN)
        {
            new_skb = skb_realloc_headroom (sk_buffer, VLAN_HDR_LEN);
            dev_kfree_skb (sk_buffer);
            if (new_skb == NULL)
            {
                printk ("<1> FcapwapWtpTxProcessDataPkt:"
                        "skb_realloc_headroom failed\n");
                return FAIL;
            }
            sk_buffer = new_skb;
        }
        memcpy (sk_buffer->data, &sk_buffer->data[VLAN_HDR_LEN],
                MAC_HDR_OFFSET);
        vlan = (struct vlan_ethhdr *) sk_buffer->data;
        vlan->h_vlan_proto = htons (ETH_P_8021Q);
        vlan->h_vlan_encapsulated_proto = vlan->h_vlan_proto;

        if (pStaIpData->u1TrustMode == TRUST_MODE)
        {

            /*The Drop Eligibilty value is always set to 0 */
            if (u1TagStatus == 1)

            {
                u2Priority = FcapwapGetPcpFromUP (u2Pcp);
            }
            else
            {
                u2Priority = 0;
            }
            u2TCI = (u2Priority << 13) & u2VlanId;
        }
        else
        {
            u2Priority =
                FcapwapGetPcpFromQosProfile (pStaIpData->u1QosProfilePriority);
            u2TCI = (u2Priority << 13) & u2VlanId;
        }
        vlan->h_vlan_TCI = u2TCI;
        memcpy (vlan->h_source, pSrcMacAddr, ETH_ALEN);
        memcpy (vlan->h_dest, apMacData->apMacAddr, ETH_ALEN);

        /*move the pointer */
        skb_pull (sk_buffer, MAC_HDR_OFFSET + VLAN_HDR_LEN);
        sk_buffer->network_header = (sk_buffer->data);
        sk_buffer->transport_header = (sk_buffer->data + NETWORK_HDR_OFFSET);
        sk_buffer->mac_header =
            (sk_buffer->data - (MAC_HDR_OFFSET + VLAN_HDR_LEN));

    }
    else
    {
        /* Frame Ethernet header */
        eh = (struct ethhdr *) sk_buffer->mac_header;
        if (eh != NULL)
            memcpy (eh->h_source, pSrcMacAddr, ETH_ALEN);
        memcpy (eh->h_dest, apMacData->apMacAddr, ETH_ALEN);
    }
    sk_buffer->pkt_type = PACKET_OUTGOING;
    eh->h_proto = htons (ETH_P_IP);

    skb_push (sk_buffer, MAC_HDR_OFFSET);

    i4Error = dev_queue_xmit (sk_buffer);

    if (i4Error)
    {
        printk (KERN_ERR "sendToStation: data transmission failed = %d\n",
                i4Error);
    }
    i4Error = net_xmit_eval (i4Error);

}

INT4
CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_HDR_LENGTH];
    UINT1               u1HdrLen = 0;
    UINT1              *pCapHdr = NULL;

    /* Get CAPWAP Header Length */
    CRU_BUF_Copy_FromBufChain (pBuf, &u1HdrLen,
                               CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);

    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);
    CRU_BUF_Copy_FromBufChain (pBuf, au1CapHdr, 0, u1HdrLen);
    pCapHdr = au1CapHdr;
    CAPWAP_GET_1BYTE (pHdr->u1Preamble, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2HlenRidWbidFlagT, pCapHdr);
    CAPWAP_GET_1BYTE (pHdr->u1FlagsFLWMKResd, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragId, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragOffsetRes3, pCapHdr);
    if (u1HdrLen > 8)
    {
        CAPWAP_GET_1BYTE (pHdr->u1RadMacAddrLength, pCapHdr);
        CAPWAP_GET_NBYTE (pHdr->RadioMacAddr, pCapHdr,
                          pHdr->u1RadMacAddrLength);
    }
    pHdr->u2HdrLen = u1HdrLen;
    return SUCCESS;
}

void
CapwapPutHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_HDR_LENGTH];

    AssembleCapwapHeader (au1CapHdr, pHdr);
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) au1CapHdr, pHdr->u2HdrLen);

}

#endif
