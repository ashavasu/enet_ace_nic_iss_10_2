
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcaprxdr.c,v 1.5 2018/02/20 10:52:54 siva Exp $
 *
 * Description: This file contains driver related API for RCV module in AP
 *******************************************************************/
#ifndef _FCAP_RXDR_C_
#define _FCAP_RXDR_C_

/******************************************************************************
 *                             Local Includes
 *******************************************************************************/
#include "fcksglob.h"
#include "fcksprot.h"
#include "ip.h"

static struct nf_hook_ops nfho_pre;
static tOsixCfg     LrOsixCfg;
static tMemPoolCfg  LrMemPoolCfg;
static tBufConfig   LrBufConfig;
UINT4               gu4SysTimeTicks;
UINT1               gau1WlcInterface[INTERFACE_LEN];
UINT1               gau1VlanInterface[INTERFACE_LEN];
UINT1               gu1LocalRoutingStatus = 0;
UINT4               gu2WtpDataPort = 0;
UINT4               gu2WlcDataPort = 0;
UINT4               gu4WlcIpAddr = 0;
UINT4               gu4WtpIpAddr = 0;
UINT1               gau1WlcMacAddr[MAC_ADDR_LEN];
UINT1               gu1DebugPrintEnable = 0;
INT4                i4isRunStateReached = 0;
UINT4               gFacMemPoolId = 0;
UINT4               gu4PriorityMapping[USER_PRIORITY][PRIORITY_LAYER] =
    { {0, 0}, {1, 10}, {2, 18}, {0, 0}, {3, 26}, {4, 34}, {5, 46}, {6, 48} };
UINT4               gu4ProfilePriorityMapping[ACCESS_CATEGORIES][PRIORITY_LAYER]
    = { {0, 18}, {4, 34}, {5, 46}, {1, 10} };

static INT4         FcapwapWtpRxIoctl (struct file *, UINT4, FS_ULONG);
tCapFragment        gCapDataFragStream[CAPWAP_MAX_DATA_FRAG_INSTANCE];
UINT1               gu1TimerStarted = 0;
static struct timer_list my_timer;
static spinlock_t   my_lock = __SPIN_LOCK_UNLOCKED (my_lock);
UINT1               u1Flags;
/******************************************************************************
 *                           Global variables
 *****************************************************************************/

static struct file_operations file_opts = {
    .unlocked_ioctl = FcapwapWtpRxIoctl,
    .open = FcapwapWtpRxDeviceOpen,
    .release = FcapwapWtpRxDeviceRelease
};

/******************************************************************************
 *  Function Name          : init_module
 *  Description            : This function is used to initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
static __init int
FcapwapWtpDrvRxInit (void)
{
    INT4                i4RetVal = 0;
    UINT4               u4SelfNodeId = SELF;

    /* Done with all processing, enable processing now */

    i4RetVal =
        register_chrdev (MAJOR_NUMBER_AP_RCV, DEVICE_NAME_AP_RCV, &file_opts);

    if (i4RetVal < 0)
        printk (KERN_ERR "init_module: %s registration failed !\n",
                DEVICE_NAME_AP_RCV);
    else
        printk (KERN_INFO "init_module: %s successfully registered !\n",
                DEVICE_NAME_AP_RCV);

    printk (">>>>>>.Register pre-routing hook\n");
    nfho_pre.hook = FcapwapWtpHookPreRouting;
    nfho_pre.owner = THIS_MODULE;
    nfho_pre.hooknum = NF_INET_PRE_ROUTING;    /* First hook for IPv4 */
    nfho_pre.pf = PF_INET;
    nfho_pre.priority = NF_IP_PRI_FIRST;    /* Make our function first */

    nf_register_hook (&nfho_pre);

    /* Initialize Osix */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = u4SelfNodeId;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        return (1);
    }

    LrMemPoolCfg.u4MaxMemPools = 100;
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        return (1);
    }

    /* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        return (1);
    }

    i4RetVal = (INT4) MemCreateMemPool (sizeof (tCapFragNode), 200,
                                        MEM_DEFAULT_MEMORY_TYPE,
                                        &gFacMemPoolId);

    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : cleanup_module
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : un-initializes the driver module
 *  Returns                : void 
 * ******************************************************************************/
static __exit void
FcapwapWtpDrvRxExit (void)
{

    del_timer (&my_timer);
    /* De-register the character driver */
    unregister_chrdev (MAJOR_NUMBER_AP_RCV, DEVICE_NAME_AP_RCV);

    nf_unregister_hook (&nfho_pre);
    printk (KERN_INFO "cleanup_module: %s successfully unregistered !\n",
            DEVICE_NAME_AP_RCV);
}

module_init (FcapwapWtpDrvRxInit);
module_exit (FcapwapWtpDrvRxExit);

MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("WTP RCV CAPWAP");
MODULE_AUTHOR ("Aricent Group");

/******************************************************************************
 *  Function Name          : FcapwapWtpRxDeviceOpen
 *  Description            : This function is used to open the device
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device can be accessed
 *  Returns                : SUCCESS 
 * ******************************************************************************/
INT4
FcapwapWtpRxDeviceOpen (struct inode *inodep, struct file *filep)
{
#if DBG_LVL_01
    printk ("FcapwapWtpRxDeviceOpen: %s opened\n", DEVICE_NAME_AP_RCV);
#endif

    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWtpRxDeviceRelease
 *  Description            : This function is used to release the driver
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device cannot be accessed
 *  Returns                : SUCCESS
 * ******************************************************************************/
INT4
FcapwapWtpRxDeviceRelease (struct inode *inodep, struct file *filep)
{

#if DBG_LVL_01
    printk ("FcapwapWtpRxDeviceRelease: %s closed\n", DEVICE_NAME_AP_RCV);
#endif
    return 0;
}

/******************************************************************************
 * Function Name          : FcapwapWtpRxIoctl
 * Description            : This function is used by user space to handle RBTree
 * Input(s)               : *inodep - inode ptr,  *filep - file ptr, cmd -
 *                           RB command, arg - RB tree arguments.
 * Output(s)              : rb tree creation, rb tree display
 * Returns                : SUCCESS
 * ******************************************************************************/
static int
FcapwapWtpRxIoctl (struct file *filep, UINT4 u4Cmd, FS_ULONG arg)
{
    thandleRbData       thandleRb;
    tApRcvStationTable *StationTable = NULL;

    /*Copy user parameter from user space of Linux */
    if (copy_from_user (&thandleRb, (void *) arg, sizeof (thandleRbData)))
    {
        printk (KERN_ERR "FcapwapWtpRxIoctlRcv: copy from user failure\n");
        return -EFAULT;
    }
    /*Perform operation as per user information on RB Tree */
    switch (thandleRb.u1RbOperation)
    {
            /* create the memorypool */
        case CREATE_ENTRY:
            if (RbtCreate (thandleRb.i4RbNodes) != RB_SUCCESS)
                printk (KERN_ERR "FcapwapWtpRxIoctl: RbTree Create Failure\n");
            break;

            /* Display the rbtree data */
        case DISPLAY_ENTRY:
            RbtDisplay (thandleRb.i4TableNum);
            break;
            /* insert the rbtree data */
        case INSERT_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_WLC_MAC:
                    memcpy (gau1WlcMacAddr, &thandleRb.rbData.WlcMacAddr,
                            MAC_ADDR_LEN);
                    break;
                case AP_WTP_TABLE:
                    gu2WlcDataPort = thandleRb.rbData.u2WlcDataPort;
                    gu2WtpDataPort = thandleRb.rbData.u2WtpDataPort;
                    gu4WlcIpAddr = thandleRb.rbData.u4WlcIpAddr;
                    memcpy (gau1WlcMacAddr, thandleRb.rbData.WlcMacAddr, 6);
                    gu1LocalRoutingStatus = thandleRb.rbData.u1LocalRouting;
                    memcpy (gau1WlcInterface, thandleRb.rbData.au1Interface,
                            INTERFACE_LEN);
                    memcpy (gau1VlanInterface,
                            thandleRb.rbData.au1VlanInterface, INTERFACE_LEN);
                    i4isRunStateReached = 1;
#if DBG_LVL_01
                    RbtDisplay (thandleRb.i4TableNum);
#endif
                    break;

                case AP_STA_TABLE:
                    if (RbtInsert (&thandleRb.rbData, thandleRb.i4TableNum) !=
                        RB_SUCCESS)
                        printk (KERN_ERR
                                "FcapwapWtpRxIoctl: RB Tree Insert Failure\n");
#if DBG_LVL_01
                    RbtDisplay (thandleRb.i4TableNum);
#endif
                    break;
            }
            break;
            /* delete the rbtree data */
        case REMOVE_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_WTP_TABLE:
                    gu2WlcDataPort = 0;
                    gu2WtpDataPort = 0;
                    gu4WlcIpAddr = 0;
                    memset (gau1WlcMacAddr, 0, 6);
                    gu1LocalRoutingStatus = LOCAL_ROUTE_DISABLE;
                    memset (gau1WlcInterface, 0, INTERFACE_LEN);
                    memset (gau1VlanInterface, 0, INTERFACE_LEN);
                    i4isRunStateReached = 0;
                    break;

                case AP_STA_TABLE:
                    if (rbt_delete (&thandleRb.rbData, thandleRb.i4TableNum) !=
                        RB_SUCCESS)
                        printk (KERN_ERR
                                "FcapwapWtpRxIoctl: RB Tree Remove Failure\n");
                    break;

                default:
                    printk (KERN_ERR
                            "FcapwapWtpRxIoctl: switch reached default\n");
                    break;
            }
            break;
            /* Modify the rbtree data */
        case REPLACE_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_WTP_TABLE:
                    printk (KERN_ERR
                            "FcapwapWtpRxIoctl: RB Tree Remove Failure\n");
                    break;
                case AP_STA_TABLE:
                    if (RbtReplace
                        (&thandleRb.rbData, &thandleRb.replaceData,
                         thandleRb.i4TableNum) != RB_SUCCESS)
                        printk (KERN_ERR
                                "FcapwapWtpRxIoctl: RB Tree Remove Failure\n");
                    break;
                default:
                    printk (KERN_ERR
                            "FcapwapWtpRxIoctl: switch reached default\n");
                    break;
            }
            break;

            /* destroy memory pool */
        case DESTROY_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_WTP_TABLE:
                    break;
                case AP_STA_TABLE:
                    RbtDestroySta ();
                    break;
            }
            printk (KERN_INFO "FcapwapWtpRxIoctl: RB Tree destroyed\n");
            break;
        case STATION_STATUS:
            StationTable = RbtRxSearchStaMac (thandleRb.rbData.staData.staMac);
            if (StationTable != NULL)
            {
                thandleRb.rbData.staData.u1StationStatus =
                    StationTable->staData.u1StationStatus;
                copy_to_user ((thandleRbData *) arg, &thandleRb,
                              sizeof (thandleRbData));
                StationTable->staData.u1StationStatus = 0;
            }
            break;

        default:
            printk
                ("FcapwapWtpRxIoctl: switch case Error thandleRb.u1Operation\n");
            break;
    }
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWtpHookPreRouting
 *  Description            : This function is used to process and send the
 *                           packet to STA
 *  Input(s)               : void 
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
UINT4
FcapwapWtpHookPreRouting (UINT4 hooknum,
                          struct sk_buff *sk_buffer,
                          const struct net_device *in,
                          const struct net_device *out,
                          int (*okfn) (struct sk_buff *))
{
    struct iphdr       *ih;
    struct udphdr      *uh;
    INT4                i4RetVal = 0;
    UINT4               u4SrcPort = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DestIp = 0;
    struct net_device  *dev;
    struct in_device   *out_dev;
    struct in_ifaddr   *if_info;
    struct ethhdr      *eh = NULL;
    INT2                i2ErrVal = 0;
    UINT1               u1Index = 0;
    UINT1               au1TempArray[5];
    struct sk_buff     *pSkb = NULL;

    if (i4isRunStateReached == 1)
    {
        if (((memcmp (in->name, gau1WlcInterface, strlen (gau1WlcInterface)) ==
              0) && (strlen (gau1WlcInterface) != 0))
            ||
            ((memcmp (in->name, gau1VlanInterface, strlen (gau1VlanInterface))
              == 0) && (strlen (gau1VlanInterface) != 0)))

        {
            /* Forwarding Multicast Packet to SDK */
            eh = eth_hdr (sk_buffer);
            if (eh != NULL)
            {
                if (eh->h_dest[0] == 0x01)
                {
                    skb_push (sk_buffer, MAC_HDR_OFFSET);

                    skb_set_mac_header (sk_buffer, 0);
                    skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
                    skb_set_transport_header (sk_buffer,
                                              MAC_HDR_OFFSET +
                                              NETWORK_HDR_OFFSET);

                    MEMSET (au1TempArray, 0, sizeof (au1TempArray));
                    for (u1Index = 0; u1Index < MAX_INTERFACE; u1Index++)
                    {
                        SPRINTF ((char *) au1TempArray, "ath%d", u1Index);
                        if ((dev =
                             dev_get_by_name (&init_net, au1TempArray)) == NULL)
                        {
                            continue;
                        }
                        else
                        {
                            /*For Multicast packets duplicating the sk_buffer
                               dev_queue_xmit */
                            pSkb = skb_copy (sk_buffer, GFP_ATOMIC);
                            if (NULL == pSkb)
                            {
                                printk ("pSkb is NULL!!!\n");
                                return NF_ACCEPT;
                            }
                            {
                                /* changing the dev to output device we need */
                                pSkb->dev = dev;
                                i2ErrVal = dev_queue_xmit (pSkb);
                                i2ErrVal = net_xmit_eval (i2ErrVal);
                                if (i2ErrVal)
                                {
                                    printk (KERN_ERR
                                            "FcapwapWtpHookPreRouting: data transmission failed\n");
                                }
                                /*give dev back */
                                dev_put (dev);
                            }
                        }
                    }
                    kfree_skb (sk_buffer);
                    return NF_STOLEN;
                }
            }

            ih = (struct iphdr *) ip_hdr (sk_buffer);

            /* To get the protocol type */
            u4Protocol = (ip_hdr (sk_buffer))->protocol;

            /* To checkt the packet is UDP Packet */
            if (u4Protocol == UDP)
            {
                uh = (struct udphdr *) (sk_buffer->data + (ih->ihl * IHL_SIZE));

                /* 
                 * Identify the received pkt protocol and for valid port number
                 * this can be also done in process_frame
                 */

                if (ntohs (uh->source) == gu2WlcDataPort)
                {
                    dev = dev_get_by_name (&init_net, gau1WlcInterface);
                    if (dev != NULL)
                    {
                        if (dev->dev_addr != NULL)
                        {
                            out_dev = (struct in_device *) dev->ip_ptr;
                            if (out_dev != NULL)
                            {
                                for (if_info = out_dev->ifa_list;
                                     if_info != NULL;
                                     if_info = if_info->ifa_next)
                                {
                                    if (!
                                        (strcmp
                                         (if_info->ifa_label, dev->name)))
                                    {
                                        gu4WtpIpAddr =
                                            ntohl (if_info->ifa_address);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    /* To get the ip header from buffer */
                    ih = (struct iphdr *) ip_hdr (sk_buffer);
                    u4SrcIp = ntohl (ih->saddr);
                    u4DestIp = ntohl (ih->daddr);

                    if ((u4DestIp == gu4WtpIpAddr) && (u4SrcIp != gu4WlcIpAddr))
                    {
                        printk ("Drop packets from unknown source\n");
                        return NF_DROP;
                    }

                    /* check if the ip address for wlc is present in rbtree */
                    if ((gu2WlcDataPort == 0) || (gu4WlcIpAddr == 0))
                    {
                        printk (KERN_ERR
                                "FcapwapWtpHookPreRouting: src port/ip is not present in rbtree\n");
                        return FAIL;
                    }
                    else
                    {
                        uh = (struct udphdr *) (sk_buffer->data +
                                                (ih->ihl * IHL_SIZE));
                        u4SrcPort = ntohs ((unsigned short int) uh->source);
                        if (u4SrcPort == gu2WlcDataPort)
                        {
                            /*Identify Management Data or Actual Data */
                            {
                                /*actual processing of received pkt */
                                i4RetVal = FcapwapWtpRcvDataPkt (sk_buffer);
                                return i4RetVal;
                            }
                        }
                    }
                }

            }

        }
    }
    return NF_ACCEPT;
}

/******************************************************************************
 *  Function Name          : verifyDot3FrameReceived
 *  Description            : This function is used to verify the incoming
 *                           frame is dot3 or dot 11 frame
 *  Input(s)               : *pBuff - Buffer
 *  Output(s)              : frame will be verified whether it is dot3 or dot11
 *  Returns                : 0 - dot3 frame  / 1 - dot11 frame
 * ******************************************************************************/
static UINT1
verifyDot3FrameReceived (UINT1 *pu1Buf)
{
    UINT2               u2Val = 0;

    /* skip 2 bytes and get next 2 bytes */
    u2Val = (*(pu1Buf + TWO_BYTE) << 8);
    u2Val = u2Val | (0x00FF & *(pu1Buf + THREE_BYTE));

    /* Check T & M bit for checking received frame is dot 3 or dot 11 */
    if ((u2Val & 0x0110) == 0x0010)
    {
        return SUCCESS;
    }
    else
    {
        return FAIL;
    }

}

/*************************************************************************/
/*  Function Name : IPv4CalcChkSum                                       */
/*  Description   : This function calculates teh IP checksum             */
/*                :                                                      */
/*  Input(s)      : pBuf   -  Pointer to Pkt Rcvd                        */
/*                : u4Size -  Size of the Pkt Rcvd                       */
/*  Output(s)     : None                                                 */
/*  Return Values : Calculated Check Sum                                 */
/*************************************************************************/
UINT2
IPv4CalcChkSum (UINT1 *pBuf, UINT4 u4Size)
{

    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (void *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }
    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}

/******************************************************************************/
/*  Function Name : IPv4PutIpHdr                                             */
/*  Description   : This fuction Adds the new IP header on the buffer  and    */
/*                : copies the new checksum to the pkt                        */
/*  Input(s)      : pBuf  -  Actual Pkt on which the header should be put     */
/*                : pIp   - The new IP header                                 */
/*                : i1flag - Flag to indicate the presence of IP options      */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
IPv4PutIpHdr (tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1flag)
{
    t_IP_HEADER        *pIpHdr;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1LinearBuf = FALSE;

    UNUSED_PARAM (i1flag);
    if (pBuf == NULL)
    {
        return;
    }
    if ((pIpHdr =
         (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                                sizeof
                                                                (t_IP_HEADER) -
                                                                1)) == NULL)
    {
        pIpHdr = &TmpIpHdr;
    }
    else
    {
        u1LinearBuf = TRUE;
    }

    pIpHdr->u1Ver_hdrlen =
        (UINT1) IP_VERS_AND_HLEN (pIp->u1Version, pIp->u2Olen);
    pIpHdr->u1Tos = pIp->u1Tos;
    pIpHdr->u2Totlen = CRU_HTONS (pIp->u2Len);
    pIpHdr->u2Id = CRU_HTONS (pIp->u2Id);
    pIpHdr->u2Fl_offs = CRU_HTONS (pIp->u2Fl_offs);
    pIpHdr->u1Ttl = pIp->u1Ttl;
    pIpHdr->u1Proto = pIp->u1Proto;
    pIpHdr->u2Cksum = 0;        /* Clear Checksum */

    pIpHdr->u4Src = OSIX_NTOHL (pIp->u4Src);
    pIpHdr->u4Dest = OSIX_NTOHL (pIp->u4Dest);

    if (u1LinearBuf == FALSE)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    if (pIp->u2Olen > 0)
    {
        /* If Options are present, then Copy them to the Buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, pIp->au1Options, IP_HDR_LEN,
                                   pIp->u2Olen);
    }
    /* Calculate the Check sum for the IP Header and copy it to the Buffer  */
    pIp->u2Cksum =
        IPv4CalcChkSum ((UINT1 *) pIpHdr, (UINT4) (IP_HDR_LEN + pIp->u2Olen));

    pIpHdr->u2Cksum = OSIX_HTONS (pIp->u2Cksum);
    if (u1LinearBuf != TRUE)
    {
        /* In Case of Linear buffer , it is already modified in the buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }
    return;
}

/******************************************************************************
 *  Function Name          : FcapwapSendtoStation
 *  Description            : This function is used to send the data to station.
 *  Input(s)               : sk_buffer - buffer 
 *  Output(s)              : data will be sent to station
 *  Returns                : void
 * ******************************************************************************/
INT4
FcapwapIPFragmentAndSend (struct sk_buff *skb)
{
    t_IP                IpHdrForFrag;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pFragData = NULL;
    INT2                i2Orgmf = 0;    /* Temp to store original more flag */
    UINT2               u2Offset = 0;
    UINT2               u2FragDataSize = 0;    /* Size of this fragment's data     */
    INT2                i2IpHlen = 0;
    UINT2               u2Len = 0;
    UINT2               u2OrgLen = 0;    /* added for trace and debug */
    INT1                i1FirstFrag = TRUE;
    INT1                i1LastFrag = FALSE;
    t_IP               *pIp = NULL;
    UINT4               u4IfIndexMtu;
    UINT1               au1MacHdr[MAC_HDR_OFFSET];
    struct iphdr       *iph = NULL;
    struct ethhdr      *eh = NULL;
    UINT4               u4Index = 0;

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    MEMSET (au1MacHdr, 0, MAC_HDR_OFFSET);

    MEMSET (&IpHdrForFrag, 0, sizeof (t_IP));

    eh = eth_hdr (skb);

    if (eh == NULL)
    {
        kfree_skb (skb);
        PRINTF ("Ethernet header is NULL \n");
        return FAIL;
    }

    MEMCPY (au1MacHdr, eh->h_dest, 6);
    MEMCPY ((au1MacHdr + 6), eh->h_source, 6);
    MEMCPY ((au1MacHdr + 12), &(eh->h_proto), 2);

    if ((pBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        kfree_skb (skb);
        printk
            ("FcapwapFragmentDataAndSend: CRU Buffer Memory Allocation Failed\n");
        return FAIL;
    }

    pBuf->pSkb = skb;

    iph = ip_hdr (pBuf->pSkb);

    pIp = &IpHdrForFrag;

    pIp->u1Version = iph->version;
    pIp->u1Tos = iph->tos;
    pIp->u1Hlen = (UINT1) ((iph->ihl & 0x0f) * sizeof (UINT4));
    pIp->u2Len = OSIX_NTOHS (iph->tot_len);
    pIp->u2Id = OSIX_NTOHS (iph->id);
    pIp->u2Fl_offs = OSIX_NTOHS (iph->frag_off);
    pIp->u1Ttl = iph->ttl;
    pIp->u1Proto = iph->protocol;
    pIp->u2Cksum = OSIX_NTOHS (iph->check);
    pIp->u4Src = OSIX_NTOHL (iph->saddr);
    pIp->u4Dest = OSIX_NTOHL (iph->daddr);
    pIp->u2Olen = (UINT2) (pIp->u1Hlen - IP_HDR_LEN);

    u2OrgLen = pIp->u2Len;
    i2IpHlen = (INT2) (IP_HDR_LEN + pIp->u2Olen);

    /* Data length only. IP Hdr and Options are not included. */
    u2Len = (UINT2) (pIp->u2Len - i2IpHlen);
    u2Offset = (UINT2) IPv4Frag_OFF_IN_BYTES (pIp->u2Fl_offs);

    /* Save original MF flag */
    i2Orgmf = (pIp->u2Fl_offs & IPv4_MF);

    /* Secv4GetIfMtu (u4IfIndex, &u4IfIndexMtu); */

    u4IfIndexMtu = IPv4_MTU;

    /* As Long as there is data left */
    while (u2Len > 0)
    {
        pIp->u2Fl_offs = (UINT2) IPv4_OFF_IN_HDR_FORMAT (u2Offset);
        if ((UINT2) (u2Len + i2IpHlen) <= (UINT2) u4IfIndexMtu)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = u2Len;
            pIp->u2Fl_offs |= (UINT2) i2Orgmf;    /* Pass original MF flag */
            i1LastFrag = TRUE;
        }
        else
        {
            /* More to come, so send multpIple of 8 bytes */
            u2FragDataSize =
                (UINT2) (((UINT2) u4IfIndexMtu - i2IpHlen) & 0xfff8);
            pIp->u2Fl_offs |= IPv4_MF;
        }

        pIp->u2Len = (UINT2) (u2FragDataSize + i2IpHlen);
        /* Put IP header back on */
        if (i1FirstFrag == FALSE)
        {
            pTmpBuf =
                CRU_BUF_Allocate_MsgBufChain ((UINT4) IPv4_MAC_HDR_LEN +
                                              pIp->u2Olen,
                                              (UINT4) IPv4_MAC_HDR_LEN -
                                              IP_HDR_LEN);
            if (pTmpBuf == NULL)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return FAIL;
            }

            IPv4PutIpHdr (pTmpBuf, pIp, TRUE);
            /*
             * Extract data from original buffer and link it with our
             * header.
             */
            pFragData = pBuf;

            if (i1LastFrag == FALSE)
            {
                if (CRU_BUF_Fragment_BufChain (pFragData, u2FragDataSize, &pBuf)
                    == CRU_FAILURE)
                {
                    PRINTF ("FRAGMENT_BUF - " "Subsequent Failed\n");
                    CRU_BUF_Release_MsgBufChain (pTmpBuf, FALSE);
                    CRU_BUF_Release_MsgBufChain (pFragData, FALSE);
                    return FAIL;
                }
            }
            CRU_BUF_Concat_MsgBufChains (pTmpBuf, pFragData);
        }
        else
        {
            /*
             * Extract IP header  and data from front end and send it.
             */
            pTmpBuf = pBuf;
            if (CRU_BUF_Fragment_BufChain
                (pTmpBuf, (UINT4) (u2FragDataSize + i2IpHlen),
                 &pBuf) == CRU_FAILURE)
            {
                PRINTF ("FRAGMENT_BUF - First " "Failed\n");
                CRU_BUF_Release_MsgBufChain (pTmpBuf, FALSE);
                return FAIL;
            }
            IPv4PutIpHdr (pTmpBuf, pIp, TRUE);
        }

        /* Now the datagram to send is in pTmpBuf; Add frag specific info */
        IPv4_PKT_ASSIGN_LEN (pTmpBuf, pIp->u2Len);
        IPv4_PKT_ASSIGN_FLAGS (pTmpBuf, pIp->u2Fl_offs);

        skb_push (pTmpBuf->pSkb, MAC_HDR_OFFSET);
        MEMCPY (pTmpBuf->pSkb->data, au1MacHdr, MAC_HDR_OFFSET);

        FcapwapSendtoStation (pTmpBuf->pSkb, 16);

        pTmpBuf->pSkb = NULL;
        CRU_BUF_Release_MsgBufChain (pTmpBuf, FALSE);
        u2Offset = (UINT2) (u2Offset + u2FragDataSize);    /* Each time offset gets updated */
        u2Len = (UINT2) (u2Len - u2FragDataSize);

        if (i1FirstFrag == TRUE)
        {
            i1FirstFrag = FALSE;
            /* If there are no options dont waste time in copying */
        }
    }

    UNUSED_PARAM (u2OrgLen);
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : FcapwapSendtoStation
 *  Description            : This function is used to send the data to station.
 *  Input(s)               : sk_buffer - buffer 
 *  Output(s)              : data will be sent to station
 *  Returns                : void
 * ******************************************************************************/
INT4
FcapwapSendtoStation (struct sk_buff *sk_buffer, UINT4 u4HeaderLen)
{
    struct ethhdr      *eh = NULL;
    struct iphdr       *iph = NULL;
    struct vlan_ethhdr *vlan = NULL;
    tApRcvStationTable *rbData = NULL;
    UINT1               au1DestMac[MAC_ADDR_LEN];
    UINT1               au1BroadcastMac[MAC_ADDR_LEN] =
        { 0Xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    UINT2               u2Priority = 0;

    INT4                i4ErrVal = 0;
    struct net_device  *dev;

    skb_set_mac_header (sk_buffer, 0);
    /* If a broadcast packet is received, then send the packet to
     * to control plane */
    memcpy (au1DestMac, sk_buffer->data, MACADDR_SIZE);
    if (memcmp (au1DestMac, au1BroadcastMac, MACADDR_SIZE) == 0)
    {
        skb_push (sk_buffer,
                  (NETWORK_HDR_OFFSET + TRANSPORT_HDR_SIZE + u4HeaderLen +
                   MAC_HDR_OFFSET));
        skb_set_mac_header (sk_buffer, 0);
        skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
        skb_set_transport_header (sk_buffer,
                                  MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);
        skb_pull (sk_buffer, MAC_HDR_OFFSET);

        return NF_ACCEPT;
    }

    rbData = RbtRxSearchStaMac (au1DestMac);
    if (rbData != NULL)
    {
        /*Set Station Status to 1 */
        rbData->staData.u1StationStatus = 1;
        if ((dev = dev_get_by_name (&init_net, rbData->staData.wlanId)) == NULL)
        {
            kfree_skb (sk_buffer);
            printk (KERN_ERR "WLAN PORT NOT CFG %s\n", rbData->staData.wlanId);
        }
        else
        {
            if (rbData->staData.u1WMMEnable == WMM_ENABLE)
            {
                if (rbData->staData.u1TaggingPolicy == TAG_POLICY_PBIT)
                {
                    vlan = (struct vlan_ethhdr *) sk_buffer->mac_header;
                    u2Priority = FcapwapGetUPFromPcp (vlan->h_vlan_TCI >> 13);
                }
                else
                {
                    iph = ip_hdr (sk_buffer);
                    u2Priority = FcapwapGetUPFromDscp (iph->tos);
                }
                /* sk_buffer = vlan_untag(sk_buffer); */
                sk_buffer->priority = u2Priority + 256;
            }

            eh = eth_hdr (sk_buffer);
            if (eh == NULL)
            {
                return NF_ACCEPT;
            }

            if (ntohs (eh->h_proto) != ARP_ETHERTYPE)
            {
                skb_set_mac_header (sk_buffer, 0);
                skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
                skb_set_transport_header (sk_buffer,
                                          MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);
            }
            /* Setting it as outgoing packet */
            sk_buffer->pkt_type = PACKET_OUTGOING;

            /* changing the dev to output device we need */
            sk_buffer->dev = dev;

            i4ErrVal = dev_queue_xmit (sk_buffer);
            i4ErrVal = net_xmit_eval (i4ErrVal);
            if (i4ErrVal)
            {
                printk (KERN_ERR
                        "FcapwapSendtoStation: data transmission failed\n");
            }
            /*give dev back */
            dev_put (dev);
        }
        return NF_STOLEN;
    }
    else
    {
        return NF_ACCEPT;
    }
}

/******************************************************************************
 *  Function Name          : FcapwapWtpRcvDataPkt
 *  Description            : This function is used to process the data packet
 *  Input(s)               : sk_buffer - buffer 
 *  Output(s)              : received data packet will be processed
 *  Returns                : void
 * ******************************************************************************/
INT4
FcapwapWtpRcvDataPkt (struct sk_buff *sk_buffer)
{
    UINT1               au1BssId[MAC_ADDR_LEN];
    INT4                i4RetVal = 0;
    UINT1               u1Value = 0;
    UINT4               u4HeaderLen = 0;
    UINT1               au1DestAddr[MAC_ADDR_LEN];
    UINT1               au1BroadcastMac[MAC_ADDR_LEN] =
        { 0Xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    tCRU_BUF_CHAIN_HEADER *ppReassembleBuf = NULL;

    /*  verify whether the received frame is dot3 or dot11 frame */
    if (verifyDot3FrameReceived (sk_buffer->data +
                                 NETWORK_HDR_OFFSET + TRANSPORT_HDR_SIZE) !=
        SUCCESS)
    {
        u1Value =
            (*(sk_buffer->data + CAPWAP_HDR_OFFSET + CAPWAP_HDR_SPLIT_SIZE));
        if (u1Value == DATA_BYTE)
        {
            skb_pull (sk_buffer, CAPWAP_HDR_OFFSET + CAPWAP_HDR_SPLIT_SIZE);

            /* If a broadcast packet is received, then send the packet to
             * to control plane */
            memcpy (au1DestAddr, (sk_buffer->data + DEST_ADDR_OFFSET),
                    MACADDR_SIZE);

            if (memcmp (au1DestAddr, au1BroadcastMac, MACADDR_SIZE) == 0)
            {
                skb_push (sk_buffer, CAPWAP_HDR_OFFSET + CAPWAP_HDR_SPLIT_SIZE);
                return NF_ACCEPT;
            }

            if (Dot11ToDot3 (&sk_buffer->data, &sk_buffer->len) != OSIX_SUCCESS)
            {
                printk (KERN_ERR "%s Dot 11 to Dot 3 conversion failed\n",
                        __FUNCTION__);
            }
            else
            {
                i4RetVal =
                    FcapwapSendtoStation (sk_buffer, CAPWAP_HDR_SPLIT_SIZE);
                return i4RetVal;
            }
        }
    }
    else
    {
#if DBG_LVL_01
        printk (KERN_INFO "dot3 frame process...\n");
#endif
        /*
         *  To get BSSID, copy 6 bytes of bssid,
         *  skip 9 bytes in sk_buffer data
         *  to get BSSID
         */
        //tss
        skb_pull (sk_buffer, CAPWAP_HDR_OFFSET);
        memcpy (au1BssId, (sk_buffer->data + BSSID_OFFSET), MACADDR_SIZE);
        /*Transmit only if data packet */
        if (sk_buffer->data[1] != CAPWAP_BYTE)
        {
            printk ("drop the non data pkt\n");
            return NF_ACCEPT;
        }

        memcpy (au1DestAddr, (sk_buffer->data + CAPWAP_HDR_LOCAL_SIZE),
                MACADDR_SIZE);

        if ((verifyCapwapFragmentReceived (sk_buffer)) == SUCCESS)
        {
            if ((CapwapDataFragReassemble (sk_buffer, sk_buffer->len,
                                           &ppReassembleBuf)) == FAIL)
            {
                return NF_STOLEN;

            }
        }

        /* remove CAPWAP header */
        skb_pull (sk_buffer, CAPWAP_HDR_LOCAL_SIZE);
        u4HeaderLen = CAPWAP_HDR_LOCAL_SIZE;

        if ((sk_buffer->len - MAC_HDR_OFFSET) > IPv4_MTU)
        {
            skb_set_mac_header (sk_buffer, 0);
            skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
            skb_set_transport_header (sk_buffer,
                                      MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);

            skb_pull (sk_buffer, MAC_HDR_OFFSET);
            FcapwapIPFragmentAndSend (sk_buffer);
            return NF_STOLEN;
        }

        /* actual data Send the data to station */
        i4RetVal = FcapwapSendtoStation (sk_buffer, u4HeaderLen);
        return i4RetVal;

    }
    return NF_ACCEPT;
}

/******************************************************************************
 *  Function Name          : FcapwapGetUPFromDscp
 *  Description            : This function returns user priority equivalent to 
 *                   Dscp Priority
 *  Input(s)               : Dscp Priority
 *  Output(s)              : user priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetUPFromDscp (UINT1 u1Dscp)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][1] == u1Dscp)
        {
            break;
        }
    }
    return (UINT4) u1Index;
}

/******************************************************************************
 *  Function Name          : FcapwapGetUPFromPcp
 *  Description            : This function returns user priority equivalent to 
 *                   pcp
 *  Input(s)               : pcp
 *  Output(s)              : user priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetUPFromPcp (UINT1 u1Pcp)
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < USER_PRIORITY; u1Index++)
    {
        if (gu4PriorityMapping[u1Index][0] == u1Pcp)
        {
            break;
        }
    }
    return (UINT4) u1Index;
}

/******************************************************************************
 *  Function Name          : CapwapFragmentReceived
 *  Description            : This function is used to Check whether the packet
 *                           received is a CAPWAP Fragment
 *  Input(s)               : pBuf ->Packet to be verified
 *  Output(s)              : none
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

INT4
verifyCapwapFragmentReceived (struct sk_buff *pSkb)
{

    UINT1               u1Val = 0;
    UINT1              *pRcvBuf = NULL;
    UINT1               au1Frame[CAPWAP_MAX_HDR_LEN];

    memset (au1Frame, 0, CAPWAP_MAX_HDR_LEN);
    memcpy (au1Frame, pSkb->data, CAPWAP_MAX_HDR_LEN);
    pRcvBuf = au1Frame;
    /* Read 'F' bit in the CAPWAP Header */
    CAPWAP_SKIP_N_BYTES (KEEP_ALIVE_BYTE, pRcvBuf);
    CAPWAP_GET_1BYTE (u1Val, pRcvBuf);

    if ((u1Val & VAL_HEX) == VAL_HEX)
    {
        return SUCCESS;
    }
    return FAILURE;
}

/*****************************************************************************
 * Function     : FCapwapDataFragReassemble                                  *
 *                                                                           *
 * Description  : Reassembling of fragmented CAPWAP Data packet              *
 *                                                                           *
 * Input        : pBuf  - Fragment of the packet                             *
 *                pSess - Session Entry of AP and AC                         *
 *                u2PktLen - Fragment Length                                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

UINT4
CapwapDataFragReassemble (struct sk_buff *skb, UINT2 u2PktLen,
                          tCRU_BUF_CHAIN_HEADER ** ppReassembleBuf)
{

    spin_lock_irqsave (&my_lock, u1Flags);

    tCapFragNode       *pPrevFrag = NULL;
    tCapFragNode       *pNextFrag = NULL;
    tCapFragNode       *pTempFrag = NULL;
    tUtlSysPreciseTime  SysPreciseTime;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2Place = CAPWAP_FRAG_INSERT;
    UINT1               u1FlgOffs = 0, u1IndexFound = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFlag;    /* 1 if not last fragment, 0 otherwise */
    UINT2               u2StartOffset = 0;    /* Index of first byte in fragment  */
    UINT2               u2FragId = 0;
    UINT2               u2EndOffset;
    UINT2               u2Index = 0;
    UINT2               u2FragIndex = 0;
    UINT1               u1ResourceTracker = 0;
    UINT1               u1GetFragid = 0;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    if ((pBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        printk ("CapwapDataFragReassemble: CRU Buffer Memory Allocation"
                "Failed\n");
        kfree_skb (skb);
        return FAIL;
    }

    pBuf->pSkb = skb;

    if (pBuf->pSkb == NULL)
    {
        printk ("Skb Buffer passed in is a NULL pointer\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        spin_unlock_irqrestore (&my_lock, u1Flags);
        return FAIL;
    }

    u2PktLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Get CAPWAP Header Length */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1HdrLen,
                          CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1FlgOffs,
                          CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

    /* Get CAPWAP Fragment ID */
    CAPWAP_COPY_FROM_BUF (pBuf, &u1GetFragid, CAPWAP_FRAGID_OFFSET,
                          sizeof (UINT1));
    u2FragId = (UINT2) u1GetFragid;
    u2FragId = u2FragId << 8;
    CAPWAP_COPY_FROM_BUF (pBuf, &u1GetFragid, CAPWAP_FRAGID_OFFSET + 1,
                          sizeof (UINT1));
    u2FragId = u2FragId | (UINT2) u1GetFragid;
    u2FragId = OSIX_HTONS (u2FragId);

    /* Get CAPWAP Fragment Offset */
    CAPWAP_COPY_FROM_BUF (pBuf, &u2StartOffset, CAPWAP_FRAG_OFFSET,
                          CAPWAP_FRAG_OFFSET_LEN);
    u2StartOffset = OSIX_HTONS (u2StartOffset);
    u2StartOffset = (UINT2) ((u2StartOffset & CAPWAP_FRAG_OFFSET_MASK) >> 3);
    u2EndOffset = (UINT2) (u2StartOffset + u2PktLen - u1HdrLen);
    i1LastFlag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Remove CAPWAP  header from all fragments except first */
    if (u2StartOffset != CAPWAP_ZERO_OFFSET)
    {
        CRU_BUF_Move_ValidOffset (pBuf, u1HdrLen);
    }
    /* Get the fragment id and the data frag index */
    for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
    {
        if (gCapDataFragStream[u2Index].u1NumberOfFragments != 0)
        {
            u1ResourceTracker++;

            if (gCapDataFragStream[u2Index].u2FragmentId == u2FragId)
            {

                u2FragIndex = u2Index;
                u1IndexFound++;
                break;
            }
        }
    }

    if (u1ResourceTracker == CAPWAP_MAX_DATA_FRAG_INSTANCE)
    {
        printk ("\nRESOURCE UNAVAILABLE TO STORE THE FRAGMENTS\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        spin_unlock_irqrestore (&my_lock, u1Flags);
        return FAIL;
    }
    if (u1IndexFound == 1)
    {
        /* Already received some fragments */
        if (i1LastFlag == TRUE)
        {
            /* Last Fragment Received, update the entire datagram size */
            gCapDataFragStream[u2FragIndex].u2TotalLen = u2EndOffset;
        }
        if (gCapDataFragStream[u2FragIndex].u1NumberOfFragments ==
            CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME)
        {
            printk ("Exceeding max no fragments, Cleaning the resources \r\n");
            CRU_BUF_Release_MsgBufChain (pBuf, 0);

            CapwapDataFreeReAssemble (u2FragIndex, FALSE);

            spin_unlock_irqrestore (&my_lock, u1Flags);
            return FAIL;
        }
        gCapDataFragStream[u2FragIndex].u1NumberOfFragments++;
    }
    else
    {
        /* First fragment received, Find the free index and  Start Reassembly
         * Timer */

        for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
        {
            if (gCapDataFragStream[u2Index].u1NumberOfFragments == 0)
            {
                u2FragIndex = u2Index;
                gCapDataFragStream[u2Index].u2FragmentId = u2FragId;
                break;
            }
        }

#if DBG_LVL_FRAG_01
        /* First fragment received,  Start Reassembly Timer */
        if (TmrStart (gCapwapTmrList.capwapRunTmrListId,
                      &(gCapDataFragStream[u2FragIndex].ReassemblyTimer),
                      CAPWAP_REASSEMBLE_TMR,
                      CAPWAP_DATA_REASSENBLY_TIMEOUT, 0) == OSIX_FAILURE)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC, "Capwap Tmr start Failed \r\n");
            CAPWAP_RELEASE_CRU_BUF (pBuf);
            spin_unlock_irqrestore (&my_lock, u1Flags);
            return FAIL;
        }
#endif

        /* If first fragment received is the last fragment of the packet then the
         * the EndOffset of the packet should be updated here.  */

        if (i1LastFlag == TRUE)
        {
            /* Last Fragment Received, update the entire datagram size */
            gCapDataFragStream[u2FragIndex].u2TotalLen = u2EndOffset;
        }

        /* Get the Current System Time */
        TmrGetPreciseSysTime (&SysPreciseTime);

        gCapDataFragStream[u2FragIndex].u4FirstFragTime = SysPreciseTime.u4Sec;

        if (gu1TimerStarted == CAPWAP_DATA_REASSEMBLY_TMR_NOT_STARTED)
        {
            /* Intialisation of the queue */
            for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE;
                 u2Index++)
            {
                gCapDataFragStream[u2Index].u1NumberOfFragments = 0;
                gCapDataFragStream[u2Index].u2FragmentId = 0;
                gCapDataFragStream[u2Index].u4FirstFragTime = 0;
            }

            setup_timer (&my_timer, FcapwapProcessReassmTimeExpiry, 0);
            mod_timer (&my_timer,
                       jiffies + msecs_to_jiffies (CAPWAP_REASSEMBLE_TIME));

#if DBG_LVL_FRAG_01
            TmrGetPreciseSysTime (&SysPreciseTime);
            printk ("\n Reassembly Timer started succesfully at : %d\n",
                    SysPreciseTime.u4Sec);
#endif

            gu1TimerStarted = CAPWAP_DATA_REASSEMBLY_TMR_STARTED;
        }

        /* Initialise the frag list */
        TMO_DLL_Init (&(gCapDataFragStream[u2FragIndex].FragList));

        gCapDataFragStream[u2FragIndex].u1NumberOfFragments++;

    }

    /*Set pNextFrag to the first fragment which begins after us,
       and pLastFrag to the last fragment which begins before us.
     */
    pPrevFrag = NULL;
    TMO_DLL_Scan (&(gCapDataFragStream[u2FragIndex].FragList),
                  pNextFrag, tCapFragNode *)
    {
        if (pNextFrag->u2StartOffset > u2StartOffset)
        {
            break;
        }
        else if (pNextFrag->u2StartOffset == u2StartOffset)
        {
            /* Received the duplicate Fragment, drop the fragement here */
            printk ("Received the duplicate fragment, drop here \r\n");
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            spin_unlock_irqrestore (&my_lock, u1Flags);
            return FAIL;
        }
        pPrevFrag = pNextFrag;
        /* Previous Frag exists */
    }

    /* pPrevfrag now points, as before, to the fragment before us;
     * pNextFrag points at the next fragment. Check to see if we can
     * join to either or both fragments.
     **/

    if ((pPrevFrag != NULL) && (pPrevFrag->u2EndOffset == u2StartOffset))
    {
        u2Place |= CAPWAP_FRAG_APPEND;
    }

    if ((pNextFrag != NULL) && (pNextFrag->u2StartOffset == u2EndOffset))
    {
        u2Place |= CAPWAP_FRAG_PREPEND;
    }
    switch (u2Place)
    {
        case CAPWAP_FRAG_APPEND:    /* Append the Received Fragment */
        {

            if (pPrevFrag->u2StartOffset == 0)
            {
                skb_push (pPrevFrag->pBuf->pSkb, TRANSPORT_HDR_SIZE +
                          NETWORK_HDR_OFFSET + MAC_HDR_OFFSET);
                if (CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                              pPrevFrag->pBuf->pSkb->len) !=
                    CRU_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, 0);
                    spin_unlock_irqrestore (&my_lock, u1Flags);
                    return FAIL;
                }
                skb_set_mac_header (pBuf->pSkb, 0);
                skb_set_network_header (pBuf->pSkb, MAC_HDR_OFFSET);
                skb_set_transport_header (pBuf->pSkb,
                                          MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);
                skb_pull (pBuf->pSkb,
                          (TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET +
                           MAC_HDR_OFFSET));
            }
            else
            {
                if (CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                              pPrevFrag->pBuf->pSkb->len) !=
                    CRU_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, 0);
                    spin_unlock_irqrestore (&my_lock, u1Flags);
                    return FAIL;
                }
            }
            pPrevFrag->u2EndOffset = u2EndOffset;
            CRU_BUF_Release_MsgBufChain (pPrevFrag->pBuf, 0);
            pPrevFrag->pBuf = pBuf;
            if (gu1DebugPrintEnable == 1)
            {
                printk ("APPEND END OFFSET - %u Len %u Frag Id %d\n",
                        u2EndOffset, pPrevFrag->pBuf->pSkb->len, u2FragId);
            }

            break;
        }
        case CAPWAP_FRAG_PREPEND:    /* Prepend the Received Fragment */
        {
            if (CRU_BUF_Concat_MsgBufChains (pBuf, pNextFrag->pBuf) !=
                CRU_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, 0);
                spin_unlock_irqrestore (&my_lock, u1Flags);
                return FAIL;
            }
            pNextFrag->pBuf = pBuf;
            pNextFrag->u2StartOffset = u2StartOffset;    /* Extend backward */
            if (gu1DebugPrintEnable == 1)
            {
                printk ("PREPEND OFFSET - %d Frag Id - %d\n", u2StartOffset,
                        u2FragId);
            }

            break;
        }
            /* If fragments are received out of order then the received fragment
             * may be need to be placed in between the fragments.*/
        case (CAPWAP_FRAG_APPEND | CAPWAP_FRAG_PREPEND):
        {
            if (pPrevFrag->u2StartOffset == 0)
            {
                skb_push (pPrevFrag->pBuf->pSkb, TRANSPORT_HDR_SIZE +
                          NETWORK_HDR_OFFSET + MAC_HDR_OFFSET);
                CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                          pPrevFrag->pBuf->pSkb->len);
                skb_set_mac_header (pBuf->pSkb, 0);
                skb_set_network_header (pBuf->pSkb, MAC_HDR_OFFSET);
                skb_set_transport_header (pBuf->pSkb,
                                          MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);
                skb_pull (pBuf->pSkb,
                          (TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET +
                           MAC_HDR_OFFSET));
            }
            else
            {
                CRU_BUF_Prepend_BufChain (pBuf, pPrevFrag->pBuf->pSkb->data,
                                          pPrevFrag->pBuf->pSkb->len);
            }
            pPrevFrag->u2EndOffset = u2EndOffset;
            CRU_BUF_Release_MsgBufChain (pPrevFrag->pBuf, 0);
            pPrevFrag->pBuf = pBuf;
            CRU_BUF_Concat_MsgBufChains (pPrevFrag->pBuf, pNextFrag->pBuf);
            pPrevFrag->u2EndOffset = pNextFrag->u2EndOffset;
            TMO_DLL_Delete (&(gCapDataFragStream[u2FragIndex].FragList),
                            &pNextFrag->Link);
            if (gu1DebugPrintEnable == 1)
            {
                printk ("BOTH Len %u End %u Frag Id %d\n",
                        pPrevFrag->pBuf->pSkb->len, pPrevFrag->u2EndOffset,
                        u2FragId);
            }

            break;

        }

        default:                /* Insert new desc between pLastFrag and
                                   pNextFrag */

        {
            pTempFrag =
                CapwapCreateFrag (u2StartOffset, u2EndOffset, pBuf, u2FragId);
            if (pTempFrag == NULL)
            {
                printk ("Failed to create the CapwapCreateFrag Entry .\r\n");
                CRU_BUF_Release_MsgBufChain (pBuf, 0);
                /* pBuf == NULL to be done in all places */
                pBuf = NULL;
                spin_unlock_irqrestore (&my_lock, u1Flags);
                return FAIL;
            }

            TMO_DLL_Insert (&(gCapDataFragStream[u2FragIndex].FragList),
                            &pPrevFrag->Link, &pTempFrag->Link);
            if (gu1DebugPrintEnable == 1)
            {
                printk ("Insert in Index[%d] - FRAG Id %d Start Offset %d End "
                        "Offset %d\n", u2FragIndex, u2FragId,
                        u2StartOffset, u2EndOffset);
            }

            break;
        }

            /* coverity fix end */
    }

    pTempFrag =
        (tCapFragNode *)
        TMO_DLL_First (&(gCapDataFragStream[u2FragIndex].FragList));
    if (pTempFrag == NULL)
    {
        PRINTF ("First Fragment in the list is NULL  for index = %d\r\n",
                u2FragIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        spin_unlock_irqrestore (&my_lock, u1Flags);
        return FAIL;
    }
    /* Kloc Fix Start */
    if (pTempFrag->pBuf == NULL)
    {
        printk ("First Fragment in the list is NULL, return FAILURE\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        spin_unlock_irqrestore (&my_lock, u1Flags);
        return FAIL;
    }

    /* Kloc Fix Ends */

    if ((pTempFrag->u2StartOffset == CAPWAP_ZERO_OFFSET) &&
        (gCapDataFragStream[u2FragIndex].u2TotalLen !=
         CAPWAP_ZERO_LEN) &&
        (pTempFrag->u2EndOffset == gCapDataFragStream[u2FragIndex].u2TotalLen))

    {
        /* We 've got a complete datagram, so extract it from the
         * reassembly buffer and pass it on.
         * */
        *ppReassembleBuf = pTempFrag->pBuf;

        /* Get CAPWAP Header Flags */
        CAPWAP_COPY_FROM_BUF (*ppReassembleBuf, &u1FlgOffs,
                              CAPWAP_FLAGS_BIT_OFFSET, CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear the F and L Bit in CAPWAP Header */
        u1FlgOffs = (u1FlgOffs & CAPWAP_FRAG_BIT_CLEAR_OFFSET);

        /* Update Flag bits in CAPWAP Header */
        CRU_BUF_Copy_OverBufChain (*ppReassembleBuf, &u1FlgOffs,
                                   CAPWAP_FLAGS_BIT_OFFSET,
                                   CAPWAP_FLAGS_OFFSET_LEN);

        /* Clear Fragment Id in CAPWAP Header */
        u1FlgOffs = 0;
        CRU_BUF_Copy_OverBufChain (*ppReassembleBuf, &u1FlgOffs,
                                   CAPWAP_FRAGID_OFFSET, sizeof (UINT1));

        CRU_BUF_Copy_OverBufChain (*ppReassembleBuf, &u1FlgOffs,
                                   (CAPWAP_FRAGID_OFFSET + 1), sizeof (UINT1));
        if (gu1DebugPrintEnable == 1)
        {
            PRINTF ("\nReleasing from Q : %d\n", u2FragIndex);
        }
        spin_unlock_irqrestore (&my_lock, u1Flags);
        CapwapDataFreeReAssemble (u2FragIndex, FALSE);

        return SUCCESS;

    }
    else
    {
        spin_unlock_irqrestore (&my_lock, u1Flags);
        /* Not received the complete packet, Return Failure */
        return FAIL;
    }

}

tCapFragNode       *
CapwapCreateFrag (UINT2 u2StartOffset, UINT2 u2EndOffset,
                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2FragId)
{
    tCapFragNode       *pFrag = NULL;

    if (pBuf == NULL)
    {
        printk ("CRU Buffer passed in is a NULL pointer, Return NULL \r\n");
        return NULL;
    }
    pFrag = (tCapFragNode *) MemAllocMemBlk (CAPWAP_FRAG_POOLID);
    if (pFrag == NULL)
    {
        printk ("CapwapCreateFrag:Memory allocation failed \r\n");
        return NULL;
    }
    pFrag->pBuf = pBuf;
    pFrag->u2StartOffset = u2StartOffset;
    pFrag->u2EndOffset = u2EndOffset;
    pFrag->u2FragId = u2FragId;
    TMO_DLL_Init_Node (&pFrag->Link);
    return pFrag;
}

INT4
CapwapDataFreeReAssemble (UINT2 u2FragIndex, BOOL1 bForceRelease)
{

    spin_lock_irqsave (&my_lock, u1Flags);
    tCapFragNode       *pFrag = NULL;
#if DBG_LVL_FRAG_01
    UINT4               u4TmrRetVal = 0;
    UINT4               u4RemainingTime = 0;

    /* Stop the Reassembly Timer */

    u4TmrRetVal = TmrGetRemainingTime (gCapwapTmrList.capwapRunTmrListId,
                                       &(pCapDataFragStream->ReassemblyTimer.
                                         TimerNode), &u4RemainingTime);

    if (u4TmrRetVal != TMR_FAILURE)
    {
        if (TmrStop (gCapwapTmrList.capwapRunTmrListId,
                     &(pCapDataFragStream->ReassemblyTimer)) != TMR_SUCCESS)
        {
            CAPWAP_TRC (CAPWAP_FAILURE_TRC,
                        "capwapTmrStop:Failure to stop"
                        "ReAssembly timer \r\n");
            spin_unlock_irqrestore (&my_lock, u1Flags);
            return OSIX_FAILURE;
        }
    }
#endif

    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (tCapFragNode *) TMO_DLL_First
            (&(gCapDataFragStream[u2FragIndex].FragList))))
    {
        if (pFrag->pBuf != NULL)
        {
            if (bForceRelease != TRUE)
            {
                pFrag->pBuf->pSkb = NULL;
            }

            CRU_BUF_Release_MsgBufChain (pFrag->pBuf, 0);
            pFrag->pBuf = NULL;
        }
        /* Should have deleted from reassembly list before freeing it. */
        TMO_DLL_Delete (&(gCapDataFragStream[u2FragIndex].FragList),
                        &pFrag->Link);

        if (MemReleaseMemBlock (CAPWAP_FRAG_POOLID, (UINT1 *) pFrag) !=
            MEM_SUCCESS)
        {
            printk ("\nCapwapDataFreeReAssemble:-Error in releasing the memory "
                    "for the node pFrag\n");

        }
    }
    gCapDataFragStream[u2FragIndex].u2FragmentId = 0;
    gCapDataFragStream[u2FragIndex].u1NumberOfFragments = 0;
    gCapDataFragStream[u2FragIndex].u4FirstFragTime = 0;
    spin_unlock_irqrestore (&my_lock, u1Flags);
    return OSIX_SUCCESS;
}

VOID
FcapwapProcessReassmTimeExpiry (VOID)
{

    tUtlSysPreciseTime  SysPreciseTime;
    tUtlSysPreciseTime  SysPreciseTimeSample;
    UINT4               u4TimeDiff = 0;
    UINT2               u2Index = 0;
    UINT1               u1LockReleased = 0;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    MEMSET (&SysPreciseTimeSample, 0, sizeof (tUtlSysPreciseTime));

    TmrGetPreciseSysTime (&SysPreciseTimeSample);

#if DBG_LVL_FRAG_01

    printk ("\n Reassembly Timer expired at : %d \n",
            SysPreciseTimeSample.u4Sec);

#endif

    for (u2Index = 0; u2Index < CAPWAP_MAX_DATA_FRAG_INSTANCE; u2Index++)
    {
        spin_lock_irqsave (&my_lock, u1Flags);
        u1LockReleased = 0;
        if (gCapDataFragStream[u2Index].u1NumberOfFragments > 0)
        {
            TmrGetPreciseSysTime (&SysPreciseTime);

            /* if the diffence between the current system time and the time at which the
             * first fragment for the Q received is greater than 2 seconds then clear the 
             * fragments on the Queue*/

            u4TimeDiff =
                SysPreciseTime.u4Sec -
                gCapDataFragStream[u2Index].u4FirstFragTime;

            if (u4TimeDiff >= 5)
            {
                spin_unlock_irqrestore (&my_lock, u1Flags);
                if (gu1DebugPrintEnable == 1)
                {
                    PRINTF ("Release buffer for Index %d\n", u2Index);
                }
                CapwapDataFreeReAssemble (u2Index, TRUE);
            }
            else
            {
                spin_unlock_irqrestore (&my_lock, u1Flags);
            }
            u1LockReleased = 1;
        }
        if (u1LockReleased == 0)
        {
            spin_unlock_irqrestore (&my_lock, u1Flags);
        }

    }

    mod_timer (&my_timer, jiffies + msecs_to_jiffies (CAPWAP_REASSEMBLE_TIME));

}

/******************************************************************************
 *  Function Name          : IpCalcCheckSum
 *  Description            : This function returns checksum value
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : UNIT2
 * ******************************************************************************/

UINT2
IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pBuf = pSkb;
    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);
    return ((u2Tmp));

}

#endif
