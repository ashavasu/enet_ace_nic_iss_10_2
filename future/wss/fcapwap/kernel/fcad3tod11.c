
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcad3tod11.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains capwap specfic packet processing
 *******************************************************************/
#ifndef _FCKS_D3TOD11_C_
#define _FCKS_D3TOD11_C_

#include "fcksglob.h"
UINT1               gau1TempPktBuf[DOT11_MAX_PKT_SIZE];

/******************************************************************************
 *  Function Name          : GetDot11HdrSize
 *  Description            : This function is used to display RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/

INT4
GetDot11HdrSize (UINT1 *pu1Dot11PktBuf)
{
    INT4                i4HdrSize = 0;
    UINT1               u1Addr4Flag = DOT11_FLAG_NO;
    UINT1               u1QosFlag = DOT11_FLAG_NO;
    tDot11RegFrameHdr   pDot11Hdr;

    if (pu1Dot11PktBuf == NULL)
    {
        return i4HdrSize;
    }
    memset (&pDot11Hdr, 0, sizeof (tDot11RegFrameHdr));
    memcpy (&pDot11Hdr, pu1Dot11PktBuf, DOT11_REG_HDR_SIZE);

    /* Set the HdrSize to Regular Dot11 frame header size */
    i4HdrSize = DOT11_REG_HDR_SIZE;

    /* Check if the Frame Control's Direction is AP->AP */
    if (((pDot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_1]) & DOT11_FC_BYTE1_DIR_ALL)
        == DOT11_FC_BYTE1_DIR_AP_AP)
    {
        u1Addr4Flag = DOT11_FLAG_YES;
    }

    /* Check if the Frame is a QoS Data Frame */
    if (((pDot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_0]) &
         (DOT11_FC_BYTE0_TYPE_ALL | DOT11_FC_BYTE0_SUBTYPE_QOS)) ==
        (DOT11_FC_BYTE0_TYPE_DATA | DOT11_FC_BYTE0_SUBTYPE_QOS))
    {
        u1QosFlag = DOT11_FLAG_YES;
    }
    if (u1Addr4Flag == DOT11_FLAG_YES)
    {
        if (u1QosFlag == DOT11_FLAG_YES)
        {
            /* Frame is Reg.Frame + Addr4 + QoSCtrl */
            i4HdrSize = DOT11_FULL_QOS_HDR_SIZE;
        }
        else
        {
            /* Frame is Reg.Frame + Addr4 */
            i4HdrSize = DOT11_FULL_FRAM_HDR_SIZE;
        }
    }
    else
    {
        if (u1QosFlag == DOT11_FLAG_YES)
        {
            /* Frame is Reg.Frame + QoSCtrl */
            i4HdrSize = DOT11_REG_QOS_HDR_SIZE;
        }
    }

    return (i4HdrSize);
}

/******************************************************************************
 *  Function Name          : PullPtr
 *  Description            : This function is used to pull the buffer
 *  Input(s)               : void
 *  Output(s)              : none
 *  Returns                : void
 * ******************************************************************************/

UINT1              *
PullPtr (UINT1 **pBuf, INT4 u4NewLen)
{
    (*pBuf) += u4NewLen;
    return *pBuf;
}

/******************************************************************************
 *  Function Name          : Dot3ToDot11
 *  Description            : This function is used to convert dot3 packets to
 *                              dot11
 *  Input(s)               : void
 *  Output(s)              : none
 *  Returns                : void
 * ******************************************************************************/

UINT1
Dot3ToDot11 (UINT1 **pu1PktBuf, UINT1 *pu1BssId, UINT4 *u4PktLen,
             UINT1 u1Direction)
{
    tDot3EthFrameHdr    EthHdr;
    INT4                i4HdrSize = 0;
    UINT1               u1Qos = 0;
    tDot11RegQosFrameHdr Dot11QosHdr;
    tDot11RegFrameHdr   Dot11Hdr;
    tDot2LlcFrameHdr    LlcHdr;

    if ((*pu1PktBuf) == NULL)
    {
        return OSIX_FAILURE;
    }

    memset (&EthHdr, 0, sizeof (tDot3EthFrameHdr));
    memset (gau1TempPktBuf, 0, DOT11_MAX_PKT_SIZE);
    memset (&Dot11QosHdr, 0, sizeof (tDot11RegQosFrameHdr));
    memset (&Dot11Hdr, 0, sizeof (tDot11RegFrameHdr));
    memset (&LlcHdr, 0, sizeof (tDot2LlcFrameHdr));

    memcpy (&EthHdr, *pu1PktBuf, DOT3_ETH_HDR_SIZE);

    /* Check for VLAN Type */
    if (EthHdr.u2FrameEthType == DOT1X_ETHTYPE_VLAN)
    {
        u1Qos = 1;
    }

    Dot11QosHdr.au1FrameCtrl[DOT11_ELEM_BYTE_0] =
        Dot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_0] = DOT11_FC_BYTE0_VERSION |
        DOT11_FC_BYTE0_TYPE_DATA;
    if (u1Qos)
    {
        Dot11QosHdr.au1FrameCtrl[DOT11_ELEM_BYTE_1] = DOT11_FC_BYTE1_DIR_AP_ST;

        memcpy (Dot11QosHdr.au1FrameAddr1, EthHdr.au1FrameDAddr,
                DOT11_HDR_MAC_ADDR_LEN);
        memcpy (Dot11QosHdr.au1FrameAddr2, pu1BssId, DOT11_HDR_MAC_ADDR_LEN);
        memcpy (Dot11QosHdr.au1FrameAddr3, EthHdr.au1FrameSAddr,
                DOT11_HDR_MAC_ADDR_LEN);

        memcpy (gau1TempPktBuf, (UINT1 *) &Dot11QosHdr, DOT11_REG_QOS_HDR_SIZE);
        i4HdrSize = DOT11_REG_QOS_HDR_SIZE;
    }
    else
    {
        if (u1Direction == DOT11_FC_BYTE1_DIR_AP_ST)
        {
            Dot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_1] = DOT11_FC_BYTE1_DIR_AP_ST;

            memcpy (Dot11Hdr.au1FrameAddr1,
                    EthHdr.au1FrameDAddr, DOT11_HDR_MAC_ADDR_LEN);
            memcpy (Dot11Hdr.au1FrameAddr2, pu1BssId, DOT11_HDR_MAC_ADDR_LEN);
            memcpy (Dot11Hdr.au1FrameAddr3,
                    EthHdr.au1FrameSAddr, DOT11_HDR_MAC_ADDR_LEN);

            memcpy (gau1TempPktBuf, (UINT1 *) &Dot11Hdr, DOT11_REG_HDR_SIZE);
            i4HdrSize = DOT11_REG_HDR_SIZE;
        }
        else if (u1Direction == DOT11_FC_BYTE1_DIR_ST_AP)
        {
            Dot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_1] = DOT11_FC_BYTE1_DIR_ST_AP;

            memcpy (Dot11Hdr.au1FrameAddr1, pu1BssId, DOT11_HDR_MAC_ADDR_LEN);
            memcpy (Dot11Hdr.au1FrameAddr2,
                    EthHdr.au1FrameSAddr, DOT11_HDR_MAC_ADDR_LEN);
            memcpy (Dot11Hdr.au1FrameAddr3,
                    EthHdr.au1FrameDAddr, DOT11_HDR_MAC_ADDR_LEN);
            memcpy (gau1TempPktBuf, (UINT1 *) &Dot11Hdr, DOT11_REG_HDR_SIZE);
            i4HdrSize = DOT11_REG_HDR_SIZE;
        }
    }

    if (OSIX_HTONS (EthHdr.u2FrameEthType) >= DOT3_MAX_LEN)
    {
        LlcHdr.u1LlcDsap = LlcHdr.u1LlcSsap = DOT2_SNAP_LSAP;
        LlcHdr.unLlcType.SnapHdr.u1Control = DOT2_UI;

        if ((EthHdr.u2FrameEthType == DOT2_ETHTYPE_AARP) ||
            (EthHdr.u2FrameEthType == DOT2_ETHTYPE_IPX))
        {
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[0] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[1] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[2] = DOT2_SNAP_BRTUNL_ORGCODE;
        }
        else
        {
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[0] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[1] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[2] = DOT2_SNAP_ORGCODE;
        }

        LlcHdr.unLlcType.SnapHdr.u2EthType = EthHdr.u2FrameEthType;

        memcpy ((gau1TempPktBuf + i4HdrSize),
                (UINT1 *) &LlcHdr, DOT2_LLC_DSAP_SSAP_SIZE);
        memcpy ((gau1TempPktBuf + i4HdrSize + DOT2_LLC_DSAP_SSAP_SIZE),
                (UINT1 *) (&(LlcHdr.unLlcType.SnapHdr.u1Control)),
                sizeof (UINT1));
        memcpy ((gau1TempPktBuf + i4HdrSize + DOT2_LLC_DSAP_SSAP_SIZE +
                 sizeof (UINT1)),
                (UINT1 *) (&(LlcHdr.unLlcType.SnapHdr.u1OrgCode)),
                DOT2_ORG_CODE_LEN);
        memcpy ((gau1TempPktBuf + i4HdrSize + DOT2_LLC_DSAP_SSAP_SIZE +
                 sizeof (UINT1) + DOT2_ORG_CODE_LEN),
                &(LlcHdr.unLlcType.SnapHdr.u2EthType), sizeof (UINT2));
        i4HdrSize += DOT2_LLC_HDR_SIZE;

        memcpy ((gau1TempPktBuf + i4HdrSize),
                ((*pu1PktBuf) + DOT3_ETH_HDR_SIZE),
                ((*u4PktLen) - DOT3_ETH_HDR_SIZE));
    }
    else
    {
        memcpy ((gau1TempPktBuf + i4HdrSize),
                ((*pu1PktBuf) + DOT3_ETH_HDR_SIZE),
                ((*u4PktLen) - DOT3_ETH_HDR_SIZE));
    }
    *u4PktLen = ((*u4PktLen) + i4HdrSize - DOT3_ETH_HDR_SIZE);
    memcpy ((*pu1PktBuf), gau1TempPktBuf, *u4PktLen);
    return OSIX_SUCCESS;
}

/******************************************************************************
 *  Function Name          : CapwapConstructCpHeader
 *  Description            : This function is used to construct the capwap
 *                           header
 *  Input(s)               : void
 *  Output(s)              : none
 *  Returns                : void
 * ******************************************************************************/

INT4
CapwapConstructCpHeader (tCapwapHdr * pCapwapHdr, int macType)
{
    pCapwapHdr->u1Preamble = 0;
    if (macType != SPLIT_MAC)
    {
        pCapwapHdr->u2HlenRidWbidFlagT = CAPWAP_HDR_RID_WID_FLAGS_LOCAL;
        pCapwapHdr->u1FlagsFLWMKResd = CAPWAP_HDR_FLWMK_FLAGS;
        pCapwapHdr->u1RadMacAddrLength = CAPWAP_HDR_RADIO_MAC;
        pCapwapHdr->u1WirelessInfoLength = CAPWAP_HDR_NULL_WIRELESS_INFO;
        pCapwapHdr->u2HdrLen = CAPWAP_HDR_MIN_LEN +
            CAPWAP_HDR_RADIO_MAC_LEN + CAPWAP_HDR_RADIO_MAC +
            CAPWAP_HDR_WIRELESS_INFO_LEN;
    }
    else
    {
        pCapwapHdr->u2HlenRidWbidFlagT = CAPWAP_HDR_RID_WID_FLAGS_SPLIT;
        pCapwapHdr->u1FlagsFLWMKResd = 0;
        pCapwapHdr->u1WirelessInfoLength = 0;
        pCapwapHdr->u1RadMacAddrLength = 0;
        pCapwapHdr->u2HdrLen = CAPWAP_HDR_LEN;
    }

    pCapwapHdr->u2FragId = 0;
    pCapwapHdr->u2FragOffsetRes3 = 0;
    return 0;
}

/******************************************************************************
 *  Function Name          : AssembleCapwapHeader
 *  Description            : This function is used to assemble capwap header
 *                           header
 *  Input(s)               : void
 *  Output(s)              : none
 *  Returns                : void
 * ******************************************************************************/

VOID
AssembleCapwapHeader (UINT1 *pTxPkt, tCapwapHdr * pCapwapHdr)
{
    INT4                h_sz = 0;

    CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1Preamble);
    CAPWAP_PUT_2BYTE (pTxPkt, pCapwapHdr->u2HlenRidWbidFlagT);
    CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1FlagsFLWMKResd);
    CAPWAP_PUT_2BYTE (pTxPkt, pCapwapHdr->u2FragId);
    CAPWAP_PUT_2BYTE (pTxPkt, pCapwapHdr->u2FragOffsetRes3);
    h_sz = 8;

    if (pCapwapHdr->u1RadMacAddrLength != 0)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1RadMacAddrLength);
        CAPWAP_PUT_NBYTE (pTxPkt, pCapwapHdr->RadioMacAddr, sizeof (tMacAddr));
        /* 1Byte Padding for 4 Byte alignment */
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        h_sz += (2 + sizeof (tMacAddr));
    }

    if (pCapwapHdr->u1WirelessInfoLength != 0)
    {
        CAPWAP_PUT_1BYTE (pTxPkt, pCapwapHdr->u1WirelessInfoLength);
        CAPWAP_PUT_4BYTE (pTxPkt, pCapwapHdr->u4WirelessRadioInfo);
        /* 3 Bytes Padding for 4 Byte alignment */
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        CAPWAP_PUT_1BYTE (pTxPkt, 0);
        h_sz += 8;
    }
    return;
}

#endif
