
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcdscpdr.c,v 1.2 2017/05/23 14:16:51 siva Exp $
 *
 * Description: This file contains driver related API for SND module in WLC
 *******************************************************************/
#ifndef _FCAC_TXDR_C_
#define _FCAC_TXDR_C_

/******************************************************************************
 *                           Local Include
 *****************************************************************************/
#include "fcksglob.h"
#include "fcksprot.h"

static tOsixCfg     LrOsixCfg;
static tMemPoolCfg  LrMemPoolCfg;
static tBufConfig   LrBufConfig;
static struct nf_hook_ops nfho_pre;
static INT4         FcapwapDscpTxIoctl (struct file *filep, UINT4 u4Cmd,
                                        FS_ULONG arg);
UINT4               gu4SysTimeTicks;
UINT4               gFacMemPoolId;
UINT2               IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size);
extern UINT4        gu4DscpTrace;

/******************************************************************************
 *                           Global variables
 *****************************************************************************/
static struct file_operations file_opts = {
    .unlocked_ioctl = FcapwapDscpTxIoctl,
    .open = FcapwapDscpTxDeviceOpen,
    .release = FcapwapDscpTxDeviceRelease
};

/******************************************************************************
 *  Function Name          : init_module
 *  Description            : This function is used to initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
static __init INT4
FcapwapDscpDrvTxInit (void)
{
    INT4                i4RetVal = 0;
    UINT4               u4SelfNodeId = SELF;

    /*register character device */
    i4RetVal =
        register_chrdev (MAJOR_NUMBER_DSCP, DEVICE_NAME_DSCP, &file_opts);
    if (i4RetVal < 0)
        printk (KERN_ERR "init_module: %s registration failed !\n",
                DEVICE_NAME_DSCP);
    else
        printk (KERN_INFO "init_module: %s successfully registered!!\n",
                DEVICE_NAME_DSCP);

    nfho_pre.hook = FcapwapDscpHookPreRouting;
    nfho_pre.owner = THIS_MODULE;
    nfho_pre.hooknum = NF_INET_PRE_ROUTING;    /* First hook for IPv4 */
    nfho_pre.pf = PF_INET;
    nfho_pre.priority = NF_IP_PRI_FIRST;    /* Make our function first */

    nf_register_hook (&nfho_pre);

    /* Initialize Osix */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = u4SelfNodeId;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        return (1);
    }

    LrMemPoolCfg.u4MaxMemPools = 100;
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        PRINTF ("MemInitMemPool Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

/* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        PRINTF ("BufInitManager Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

    i4RetVal = (INT4) MemCreateMemPool (sizeof (tCapFragNode), 10,
                                        MEM_DEFAULT_MEMORY_TYPE,
                                        &gFacMemPoolId);

    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : cleanup_module
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : un-initializes the driver module
 *  Returns                : void 
 * ******************************************************************************/
static __exit void
FcapwapDscpDrvTxExit (void)
{
    printk (KERN_INFO "cleanup_module: unreg chardev\n");
    /* De-register the character driver */
    unregister_chrdev (MAJOR_NUMBER_DSCP, DEVICE_NAME_DSCP);

    nf_unregister_hook (&nfho_pre);
    /* Mempools to be freed */
    printk (KERN_INFO "cleanup_module: %s successfully unregistered!!!\n",
            DEVICE_NAME_DSCP);
}

module_init (FcapwapDscpDrvTxInit);
module_exit (FcapwapDscpDrvTxExit);

MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("DSCP in kernel layer");
MODULE_AUTHOR ("Aricent Group");

/******************************************************************************
 *  Function Name          : FcapwapDscpTxDeviceOpen
 *  Description            : This function is used to open the device
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device can be accessed
 *  Returns                : SUCCESS 
 * ******************************************************************************/
INT4
FcapwapDscpTxDeviceOpen (struct inode *inodep, struct file *filep)
{
#if DBG_LVL_01
    printk ("FcapwapDscpTxDeviceOpen: %s opened\n", DEVICE_NAME_DSCP);
#endif
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapDscpTxDeviceRelease
 *  Description            : This function is used to release the driver
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device cannot be accessed
 *  Returns                : SUCCESS
 * ******************************************************************************/
INT4
FcapwapDscpTxDeviceRelease (struct inode *inodep, struct file *filep)
{

#if DBG_LVL_01
    printk ("FcapwapDscpTxDeviceRelease: %s closed\n", DEVICE_NAME_DSCP);
#endif
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapDscpTxIoctl
 *  Description            : This function is used by user space to handle RBTree
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr, cmd - 
 *                           RB command, arg - RB tree arguments. 
 *  Output(s)              : rb tree creation, rb tree display
 *  Returns                : SUCCESS
 * ******************************************************************************/
static INT4
FcapwapDscpTxIoctl (struct file *filep, UINT4 u4Cmd, FS_ULONG arg)
{
#ifdef WLC_WANTED
    thandleRbWlcData    thandleRb;

    /*Copy user parameter from user space of Linux */
    if (copy_from_user (&thandleRb, (void *) arg, sizeof (thandleRbWlcData)))
    {
        printk (KERN_ERR "FcapwapDscpTxIoctl: copy from user failure\n");
        return -EFAULT;
    }

    /*Perform operation as per user information on RB Tree */
    switch (thandleRb.u1RbOperation)
    {
            /* create the mem */
        case CREATE_ENTRY:
            if (RbtCreate (thandleRb.i4RbNodes) != SUCCESS)
                printk ("FcapwapDscpTxIoctl: RbTree Create Failure\n");
            break;
        case INSERT_ENTRY:
            if (RbtInsert (&thandleRb.rbData, thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR "FcapwapDscpTxIoctl: RbTree Insert Failure\n");
#if DBG_LVL_01
            RbtDisplay (thandleRb.i4TableNum);
#endif
            break;
        case DISPLAY_ENTRY:
            RbtDisplay (thandleRb.i4TableNum);
            break;
        case REMOVE_ENTRY:
            if (RbtRemove (&thandleRb.rbData, thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR
                        "FcapwapDscpTxIoctl: RB Tree Remove Failure\n");
            break;
        case DESTROY_ENTRY:
            if (RbtTxDestroy (thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR
                        "FcapwapDscpTxIoctl: RB Tree Replace Failure\n");
            break;
        default:
            printk
                ("FcapwapDscpTxIoctl: switch case Error thandleRb.u1Operation\n");
            break;
    }
    return SUCCESS;
#else
    thandleRbData       thandleRb;

    /*Copy user parameter from user space of Linux */
    if (copy_from_user (&thandleRb, (void *) arg, sizeof (thandleRbData)))
    {
        printk (KERN_ERR "FcapwapDscpTxIoctl: copy from user failure\n");
        return -EFAULT;
    }

    /*Perform operation as per user information on RB Tree */
    switch (thandleRb.u1RbOperation)
    {
            /* create the mem */
        case CREATE_ENTRY:
            if (RbtCreate (thandleRb.i4RbNodes) != SUCCESS)
                printk ("FcapwapDscpTxIoctl: RbTree Create Failure\n");
            break;
        case INSERT_ENTRY:
            if (RbtInsertDscp (&thandleRb.rbData, thandleRb.i4TableNum) !=
                SUCCESS)
                printk (KERN_ERR "FcapwapDscpTxIoctl: RbTree Insert Failure\n");
#if DBG_LVL_01
            RbtDisplay (thandleRb.i4TableNum);
#endif
            break;
        case DISPLAY_ENTRY:
            RbtDisplay (thandleRb.i4TableNum);
            break;
        case REMOVE_ENTRY:
            if (RbtRemoveDscp (&thandleRb.rbData, thandleRb.i4TableNum) !=
                SUCCESS)
                printk (KERN_ERR
                        "FcapwapDscpTxIoctl: RB Tree Remove Failure\n");
            break;
        case DESTROY_ENTRY:
            if (RbtTxDestroy (thandleRb.i4TableNum) != SUCCESS)
                printk (KERN_ERR
                        "FcapwapDscpTxIoctl: RB Tree Replace Failure\n");
            break;
        default:
            printk
                ("FcapwapDscpTxIoctl: switch case Error thandleRb.u1Operation\n");
            break;
    }
    return SUCCESS;
#endif
}

/******************************************************************************
 *  Function Name          : FcapwapDscpHookPreRouting 
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : hooknum - hook number
 *                           skb -  skb buffer
 *                           in  - input deviece
 *                           out - output device
 *  Returns                : SUCCESS/FAILURE
 * ******************************************************************************/
UINT4
FcapwapDscpHookPreRouting (UINT4 hooknum,
                           struct sk_buff *skb,
                           const struct net_device *in,
                           const struct net_device *out,
                           INT4 (*okfn) (struct sk_buff *))
{
    tDscpQosTable      *rbData_qos;
    UINT2               u2OldCheck = 0;
    UINT2               u2CheckSum = 0;
    rbData_qos = DscpQosTableSearch (in->name, (ip_hdr (skb))->tos >> 2);
    if (rbData_qos != NULL)
    {
        /*Replacing dscp value with the coreesponding priority value */
        u2OldCheck = (ip_hdr (skb))->check;
        (ip_hdr (skb))->check = 0;
        (ip_hdr (skb))->tos = (((ip_hdr (skb))->tos & 3)
                               | ((rbData_qos->qosData.u1dscpout) << 2));
        u2CheckSum = IpCalcCheckSum ((ip_hdr (skb)), 20);

        (ip_hdr (skb))->check = u2CheckSum;
        if (((gu4DscpTrace) & (DSCP_DEBUG_PACKETS)) == DSCP_DEBUG_PACKETS)
        {
            printk
                ("Entry exist for interface %s and indscp %d and replaced with outdscp %d\r\n",
                 in->name, rbData_qos->qosData.u1dscpin,
                 rbData_qos->qosData.u1dscpout);
        }
    }
    return NF_ACCEPT;
}

/******************************************************************************
 *  Function Name          : IpCalcCheckSum
 *  Description            : This function returns checksum value
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : UNIT2
 * ******************************************************************************/
UINT2
IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pBuf = pSkb;
    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);
    return ((u2Tmp));

}

#endif
