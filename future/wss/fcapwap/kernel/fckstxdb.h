
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fckstxdb.h,v 1.2 2017/05/23 14:16:51 siva Exp $
 *
 * Description: This file contains tx module db related typedefs
 *******************************************************************/

#ifndef _FCASND_DB_H_
#define _FCASND_DB_H_

/******************************************************************************
 *                             Global Includes
 *******************************************************************************/
#include <linux/module.h>
/* #include "/lib/modules/3.3.4-5.fc17.i686.PAE/build/include/linux/module.h" */
#include <linux/rbtree.h>
#include <linux/mempool.h>
#include <linux/gfp.h>

#include "fcksglob.h"

#ifdef WTP_WANTED
/******************************************************************************
 *                     RB Interface Structure for WTP 
 *******************************************************************************/
typedef struct 
{
    struct rb_node node;
    twlanStaInfo staData;
}tApSndStationTable;
typedef struct
{
    struct rb_node node;
    tDscpQosInfo qosData;
}tDscpQosTable;


#endif

#ifdef WLC_WANTED
/******************************************************************************
 *                     RB Interface Structure for WLC
 *******************************************************************************/
typedef struct
{
    struct rb_node  node;
    tAPIpInfo       apIpData;
}tAcSndIpTable;

typedef struct
{
    struct rb_node  node;
    tApIntfInfo     apIntData;
}tAcSndIntfTable;

typedef struct
{
    struct rb_node  node;
    tVlanInfo       VlanData;
}tAcSndVlanTable;

typedef struct
{
    struct rb_node  node;
    tMacInfo        MacData;
}tAcSndMacTable;
typedef struct
{
    struct rb_node  node;
    tStaIpInfo      staData;
}tAcSndStaIPTable;

typedef struct
{
    struct rb_node  node;
    tApStaInfo      StaInfo;
}tAcSndStaTable;

typedef struct 
{
    struct rb_node node;
    tApMacInfo   apMacData;
}tAcSndApMacTable; 

typedef struct
{
    struct rb_node node;
    tDscpQosInfo qosData;
}tDscpQosTable;

#endif

#endif
