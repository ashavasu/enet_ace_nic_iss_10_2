
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcaptxdr.c,v 1.8 2018/02/20 10:52:54 siva Exp $
 *
 * Description: This file contains driver related API for SND module in AP
 *******************************************************************/
#ifndef _FCAP_TXDR_C_
#define _FCAP_TXDR_C_

/******************************************************************************
 *                             Local Includes
 *******************************************************************************/
#include "fcksglob.h"
#include "fcksprot.h"
/******************************************************************************
 *                           Global variables
 *****************************************************************************/
static struct nf_hook_ops nfho_post;
static struct nf_hook_ops nfho_pre;
UINT1               gu1LocalRoutingStatus = 0;
UINT2               gu2WtpDataPort = 0;
UINT2               gu2WlcDataPort = 0;
UINT4               gu4WlcIpAddr = 0;
UINT4               gu4WtpIpAddr = 0;
UINT1               gau1WlcMac[MAC_ADDR_LEN];
UINT1               gau1WlcInterface[INTERFACE_LEN];
UINT1               gau1VlanInterface[INTERFACE_LEN];
UINT2               gu2VlanID = 0;
static INT4         i4IsRunStateReached = 0;
static UINT1        au1Interface[MAX_INTERFACE][INTERFACE_LEN];
static INT4         FcapwapWtpTxIoctl (struct file *filep, UINT4 u4Cmd,
                                       FS_ULONG arg);
INT4                gi4CapwapPortMtu = 0;
UINT4               gu2FragId = 0;
UINT4               gu4Tos = 0;

UINT4               gu4PriorityMapping[USER_PRIORITY][PRIORITY_LAYER] =
    { {0, 0}, {1, 10}, {2, 18}, {0, 0}, {3, 26}, {4, 34}, {5, 46}, {6, 48} };

UINT4               gu4ProfilePriorityMapping[ACCESS_CATEGORIES][PRIORITY_LAYER]
    = { {0, 18}, {4, 34}, {5, 46}, {1, 10} };
static tOsixCfg     LrOsixCfg;
static tMemPoolCfg  LrMemPoolCfg;
static tBufConfig   LrBufConfig;
UINT4               gu4SysTimeTicks;

#define   IP_COPY_FROM_BUF(pBuf, pDst, u4Offset, u4Size)         \
          CRU_BUF_Copy_FromBufChain((pBuf),(UINT1 *)(pDst),(u4Offset), (u4Size))
#define IP_GET_DATA_PTR_IF_LINEAR(pBuf, u4Offset, u4NumBytes)    \
   CRU_BUF_Get_DataPtr_IfLinear((pBuf), (u4Offset), (u4NumBytes))

#define   IP_SIXTEEN               16
#define   IP_BIT_ALL          0xFFFF    /* the parameters  */

struct skb_user
{
    unsigned int        len;
    char               *buf;
    char                dev_name[10];
};

static struct sk_buff *construct_packet (struct skb_user *);

#define PACKET_WIFI 0x124
#define DEVICE_NAME "dprx0"
#define BUFFER_SIZE 1024
tDevInfo            gsDevInfo;
static int          goahead = 0;
static int          startqueuing = 0;
static              DECLARE_WAIT_QUEUE_HEAD (wq);

static char         device_buffer[BUFFER_SIZE];

void                FFInsertIntoRing (tFF_SLL *, tFF_SLL_NODE *, tFF_SLL_NODE *,
                                      tFF_SLL_NODE *);
void                FFDeleteNodeFromRing (tFF_SLL *, tFF_SLL_NODE *);
unsigned char       FFCreateRingBufferList (void);
void                FFFlushRing (tFF_SLL *);
void                FFFlushRingBufferData (tFF_SLL *);
void                FFQueuePkt (struct sk_buff *);
static ssize_t      device_write (struct file *filep, const char *buff,
                                  size_t len, loff_t * off);
struct sk_buff     *FFDequeuePkt (void);
void                FcapwapWtpDrvBrTxExit (void);
int                 FcapwapWtpDrvBrTxInit (void);

static int          device_open (struct inode *inodep, struct file *filep);

static int
device_open (struct inode *inodep, struct file *filep)
{
    UNUSED_PARAM (inodep);
    UNUSED_PARAM (filep);

    printk (">> Device: %s opened\n", DEVICE_NAME);

    startqueuing = 1;

    return 0;
}
static int
device_release (struct inode *inodep, struct file *filep)
{
    UNUSED_PARAM (inodep);
    UNUSED_PARAM (filep);

    startqueuing = 0;
    FFFlushRingBufferData (&gsDevInfo.sRingBuff);

    printk ("Device: %s closed\n", DEVICE_NAME);
    return 0;
}

static              ssize_t
device_read (struct file *filep, char *buff, size_t len, loff_t * off)
{
    struct sk_buff     *pskbuf = NULL;

    UNUSED_PARAM (filep);
    UNUSED_PARAM (len);
    UNUSED_PARAM (off);

    pskbuf = FFDequeuePkt ();

    if (pskbuf == NULL)
    {
        /* nothing to be read */
        wait_event_interruptible (wq, goahead != 0);
        pskbuf = FFDequeuePkt ();
        goahead = 0;
        if (pskbuf == NULL)
            return 0;
    }
    if (copy_to_user (buff, (pskbuf->data - 14), (pskbuf->len + 14)))
    {
        return -EFAULT;
    }

    kfree_skb (pskbuf);
    return (pskbuf->len + 14);
}

static              ssize_t
device_write (struct file *filep, const char *buff, size_t len, loff_t * off)
{
    int                 maxbytes;
    int                 bytes_to_write;
    int                 bytes_written;

    UNUSED_PARAM (filep);

    maxbytes = BUFFER_SIZE - *off;
    if ((unsigned int) maxbytes > len)
        bytes_to_write = len;
    else
        bytes_to_write = maxbytes;
    bytes_written =
        bytes_to_write - copy_from_user (device_buffer + *off, buff,
                                         bytes_to_write);
    *off += bytes_written;
    return bytes_written;
}

void
FFInsertIntoRing (tFF_SLL * pList, tFF_SLL_NODE * pPrev, tFF_SLL_NODE * pMid,
                  tFF_SLL_NODE * pNext)
{
    pList->u4_Count++;
    pMid->pNext = pNext;
    pPrev->pNext = pMid;
    if (pNext == &pList->Head)
        pList->Tail = pMid;
}

void
FFDeleteNodeFromRing (tFF_SLL * pList, tFF_SLL_NODE * pNode)
{
    tFF_SLL_NODE       *pPrev = NULL, *pTemp = NULL;
    unsigned int        u4Count = 0;
    unsigned int        u4NumNodes = 0;
    /***** Deleting The First Node *****/

    if (pNode == NULL)
        return;
    if (pNode->pNext == NULL)
        return;

    u4NumNodes = pList->u4_Count;
    for (pTemp = &pList->Head; u4Count++ < u4NumNodes && pNode != pTemp->pNext;
         pTemp = pTemp->pNext);
    pPrev =
        (((pTemp == &pList->Head) || (u4Count > u4NumNodes)) ? NULL : pTemp);

    if (pPrev == NULL)
        pPrev = &pList->Head;

    /***** Deleting The Last Node *****/
    if (pNode->pNext == &pList->Head)
        pList->Tail = pPrev;

    pPrev->pNext = pNode->pNext;
    pNode->pNext = NULL;

    pList->u4_Count--;
}
unsigned char
FFCreateRingBufferList ()
{
    unsigned short      u2Index = 0;
    tRingBuffNode      *psRingBuffNode = NULL;

    printk ("<1> dprx: create ring buffer\n");
    FF_SLL_INIT (&gsDevInfo.sRingBuff);

    /* initialize the valid buffer pointers */
    gsDevInfo.psFirstValidBuffer = NULL;
    gsDevInfo.psLastValidBuffer = NULL;

    /* init the semaphores related to the ring buffers */
    sema_init (&gsDevInfo.sRingSem, 1);

    /* create the ethernet ring  */
    for (u2Index = 0; u2Index < RINGBUFF_SIZE; u2Index++)
    {

        psRingBuffNode =
            (tRingBuffNode *) kmalloc (sizeof (tRingBuffNode), GFP_ATOMIC);

        if (psRingBuffNode == NULL)
        {
            /* if memory is not there for a node in the ring there is good
             *              * chance that it wouldn't be there for the rest of the nodes
             *                           * hence, flush the entire ring, or better try to reduce the
             *                                        * ring size and recompile
             *                                                     */
            FFFlushRing (&gsDevInfo.sRingBuff);
            return FF_FAILURE;
        }

        /* init the node */
        FF_SLL_INIT_NODE ((tFF_SLL_NODE *) psRingBuffNode);
        /* this is a damn important initialization !! */
        psRingBuffNode->pskbuf = NULL;

        /* add to the ring */
        FF_SLL_ADD (&gsDevInfo.sRingBuff, (tFF_SLL_NODE *) psRingBuffNode);
    }

    return FF_SUCCESS;
}

void
FFFlushRing (tFF_SLL * pList)
{
    tRingBuffNode      *psRingNode = NULL;
    tRingBuffNode      *psTemp = NULL;
    printk ("<1> dprx: flush ring buffer\n");

    FF_DYNM_SLL_SCAN (pList, psRingNode, psTemp, tRingBuffNode *)
    {

        FF_SLL_DELETE (pList, (tFF_SLL_NODE *) psRingNode);

        /* if there is a valid socket buffer, free it before freeing the node  */
        if (psRingNode->pskbuf != NULL)
        {
            kfree_skb (psRingNode->pskbuf);
            psRingNode->pskbuf = NULL;
        }

        kfree (psRingNode);
    }
}
void
FFFlushRingBufferData (tFF_SLL * pList)
{
    tRingBuffNode      *psRingNode = NULL;
    tRingBuffNode      *psTemp = NULL;
    printk ("<1> dprx: flush ring buffer data\n");

    FF_DYNM_SLL_SCAN (pList, psRingNode, psTemp, tRingBuffNode *)
    {
/* >> */
        printk ("+");
        /* if there is a valid socket buffer, free it before freeing the node  */
        if (psRingNode->pskbuf != NULL)
        {
            kfree_skb (psRingNode->pskbuf);
            psRingNode->pskbuf = NULL;
        }
    }
    printk ("\n\n");
}

void
FFQueuePkt (struct sk_buff *pskbuf)
{
    tRingBuffNode      *pFirst = NULL;
    tRingBuffNode      *pLast = NULL;
    tRingBuffNode      *pNext = NULL;

    /* printk("<1> dprx: in queue pkt\n"); */
    if (!startqueuing)
    {
        printk
            ("<1> dprx: trying to queue when not supposed to...should not be here\n");
        kfree_skb (pskbuf);
        return;
    }

    /* while queuing a pkt only the lastvalidbyte pointer moves arround */
    if (down_interruptible (&gsDevInfo.sRingSem))
    {
        kfree_skb (pskbuf);
        return;
    }

    if ((gsDevInfo.psFirstValidBuffer == NULL) &&
        (gsDevInfo.psLastValidBuffer == NULL))
    {
        /* when both the valid pointers are NULL => list empty
         *          * so assign the first node as the valid buffer.
         *                   */
        pFirst = (tRingBuffNode *) FF_SLL_FIRST (&gsDevInfo.sRingBuff);
        gsDevInfo.psFirstValidBuffer = pFirst;
        gsDevInfo.psLastValidBuffer = pFirst;

        /* assign the packet buffer to the node */
        gsDevInfo.psLastValidBuffer->pskbuf = pskbuf;

    }
    else
    {
        pLast = gsDevInfo.psLastValidBuffer;
        pNext =
            (tRingBuffNode *) FF_SLL_NEXT (&gsDevInfo.sRingBuff,
                                           (tFF_SLL_NODE *) pLast);

        if (pNext->pskbuf != NULL)
        {
            /* we have wrapped around...can't help it free the already existing buffer :-( */
            printk ("<1> dprx: ring wrapped!\n");
            kfree_skb (pNext->pskbuf);
            /*
             *              * shift the firstvalidbuffer to the next of the last, since chronologicaly that
             *                           * would be the next latest.
             *                                        */
            pFirst =
                (tRingBuffNode *) FF_SLL_NEXT (&gsDevInfo.sRingBuff,
                                               (tFF_SLL_NODE *) pNext);
            gsDevInfo.psFirstValidBuffer = pFirst;
        }

        /* store the pkt skbuf in the ring */
        pNext->pskbuf = pskbuf;

        /* update the last valid buffer */
        gsDevInfo.psLastValidBuffer = pNext;
    }

    up (&gsDevInfo.sRingSem);
    return;
}

struct sk_buff     *
FFDequeuePkt ()
{

    struct sk_buff     *pskbuf = NULL;
    tRingBuffNode      *pNext = NULL;
    tRingBuffNode      *pFirst = NULL;

    /* printk("<1> dprx: in dequeue pkt\n"); */
    if (!startqueuing)
    {
        printk
            ("<1> dprx: trying to dequeue when not supposed to...should not be here\n");
        return NULL;
    }

    /* while dequeuing a pkt only the firstvalidbyte pointer moves arround */

    /* shankar:
     *      * the interrupt lock avoids a race condition where, in the process of dequeuing,
     *           * control is transferred to queueing a new pkt which frees the dequeued skb
     *                * if the ring wraps !
     *                     */
    if (down_interruptible (&gsDevInfo.sRingSem))
        return NULL;

    if (gsDevInfo.psFirstValidBuffer != NULL)
    {
        /* store the sock buff in a local variable and set the sock
         *          * buff pointer in the ring node to NULL.
         *                   */
        pskbuf = gsDevInfo.psFirstValidBuffer->pskbuf;
        gsDevInfo.psFirstValidBuffer->pskbuf = NULL;

        /* start moving the first valid pointer  */
        pFirst = gsDevInfo.psFirstValidBuffer;
        pNext =
            (tRingBuffNode *) FF_SLL_NEXT (&gsDevInfo.sRingBuff,
                                           (tFF_SLL_NODE *) pFirst);

        if (pNext->pskbuf != NULL)
        {
            gsDevInfo.psFirstValidBuffer = pNext;
        }
        else
        {
            /* wrapped!!...the first valid has gone past the last so...  */
            gsDevInfo.psFirstValidBuffer = NULL;
            gsDevInfo.psLastValidBuffer = NULL;
        }

        up (&gsDevInfo.sRingSem);
        /* return the ptr to the sock buff so that the data can be copied
         *          * to user space...which should be freed thereafter!
         *                   */
        return (pskbuf);
    }
    else
    {

        up (&gsDevInfo.sRingSem);

        /* either both the valid pointers should be NULL or non NULL, any
         *          * other condition is not possible.
         *                   */
        return NULL;
    }
}
static struct sk_buff *
construct_packet (struct skb_user *info)
{
    int                 len, hh_len;
    struct sk_buff     *skb;
    unsigned char      *data, *payload;
    struct net_device  *wlandev;
    unsigned int        count = 0, i = 0;

    printk ("<1> construct_packet\n");

    wlandev = dev_get_by_name (&init_net, info->dev_name);

    printk ("<1> wlandev = %x\n", wlandev);

    printk ("<1> info->dev_name = %s\n", info->dev_name);

    if (!netif_running (wlandev))
    {
        printk (KERN_ERR "error: device is down: %s\n", WTP_WLAN_DEV_NAME);
        return NULL;
    }

    /* lets check this because apparently kute crashes the kernel in case of no
       carrier and Intel Gig Ethernet cards */
    if (!netif_carrier_ok (wlandev))
    {
        printk (KERN_ERR "device has no carrier: %s\n", WTP_WLAN_DEV_NAME);
        return NULL;
    }
    /* calculate packet length and get skb */
    printk ("<1> Info->len = %d\n", info->len);
    len = info->len;
//    len = 1500;
    printk ("<1> len = %d\n", len);

    skb = alloc_skb (len, GFP_ATOMIC);
    if (!skb)
    {
        printk ("<1> fail-4:\n");
        return NULL;
    }
    printk ("<1> skb = 0x%x\n", skb);
    printk ("<1> >>>>>>>>>> 5:\n");

    /* device, protocol etc. */
    skb->dev = wlandev;
    skb->protocol = __constant_htons (ETH_P_IP);
    skb->priority = 0;
    printk ("<1> >>>>>>>>>> new 6:\n");
    /* skb->dst = dst_clone(&rt->u.dst); */
    skb->pkt_type = PACKET_HOST;
    printk ("<1> >>>>>>>>>> 7: %x -  %x\n", skb, skb->data);

//>>>
    //   return NULL;
    /* Copy head (dest,src,type and priority) */
    copy_from_user (skb->data, info->buf, len);
//    memcpy(skb->data, info->buf, len);
    printk ("<1> >>>>>>>>>> 8:\n");

    skb->len = len;
    skb->tail = skb->data + skb->len;
// shankar _begin_
    skb->network_header = skb->data + 14;
    printk (">>>>>>>>>> fix for protocol buggy \r\n");
// shankar _end_
    printk ("<1> end of construct_packet\n");

    return skb;
}

/******************************************************************************
 *                           callback functions
 ******************************************************************************/
struct packet_type  frame[MAX_INTERFACE];

static struct file_operations file_opts = {
    /*##, Need to add ioctl support for FC17 */

    .read = device_read,
    .write = device_write,

    .unlocked_ioctl = FcapwapWtpTxIoctl,
    .open = FcapwapWtpTxDeviceOpen,
    .release = FcapwapWtpTxDeviceRelease
};

/******************************************************************************
 *  Function Name          : init_module
 *  Description            : This function is used to initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
static __init INT4
FcapwapWtpDrvTxInit (void)
{
    INT4                i4RetVal = 0;
    UINT4               u4SelfNodeId = SELF;

    goahead = 0;
    i4RetVal =
        register_chrdev (MAJOR_NUMBER_AP_SND, DEVICE_NAME_AP_SND, &file_opts);

    if (i4RetVal < 0)
        printk (KERN_ERR "init_module: %s registration failed !\n",
                DEVICE_NAME_AP_SND);
    else
        printk (KERN_INFO
                "init_module: %s successfully registered !!!!!!!!!!!\n",
                DEVICE_NAME_AP_SND);
    /* create the ring buffers for sending pkt to the user
     *      * space  */
    if (FFCreateRingBufferList () == FF_FAILURE)
    {
        printk ("<1> dprx: ring create failure\n");
        return (-ENOMEM);
    }

/*Register Bridge Post routing hook */
    FcapwapWtpDrvBrTxInit ();

/* Initialize Osix */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = u4SelfNodeId;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;
    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        return (1);
    }

    LrMemPoolCfg.u4MaxMemPools = 100;
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        return (1);
    }

/* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        return (1);
    }

    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : cleanup_module
 *  Description            : This function is used to un-initialize the driver
 *  Input(s)               : void 
 *  Output(s)              : un-initializes the driver module
 *  Returns                : void 
 * ******************************************************************************/
static void         __exit
FcapwapWtpDrvTxExit (void)
{

    UINT1               u1Index = 0;
    INT4                i4RetVal = 0;
    struct net_device  *dev;

    FFFlushRing (&gsDevInfo.sRingBuff);

    for (u1Index = 0; u1Index < MAX_INTERFACE; u1Index++)
    {
        i4RetVal = strcmp (au1Interface[u1Index], "");
        if (i4RetVal != 0)
        {
            dev = dev_get_by_name (&init_net, au1Interface[u1Index]);
            frame[u1Index].dev = dev;
            if (frame[u1Index].dev != NULL)
            {
                dev_remove_pack (&frame[u1Index]);
                dev_put (dev);
                memcpy (au1Interface[u1Index], "\0", INTERFACE_LEN);
            }
        }
    }

    FcapwapWtpDrvBrTxExit ();
    /* De-register the character driver */
    unregister_chrdev (MAJOR_NUMBER_AP_SND, DEVICE_NAME_AP_SND);

    printk (KERN_INFO "cleanup_module: %s successfully unregistered !!!!\n",
            DEVICE_NAME_AP_SND);
}

module_init (FcapwapWtpDrvTxInit);
module_exit (FcapwapWtpDrvTxExit);

MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("WTP SND CAPWAP");
MODULE_AUTHOR ("Aricent Group");

/******************************************************************************
 *  Function Name          : FcapwapWtpTxDeviceOpen
 *  Description            : This function is used to open the device
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device can be accessed
 *  Returns                : SUCCESS 
 * ******************************************************************************/
INT4
FcapwapWtpTxDeviceOpen (struct inode *inodep, struct file *filep)
{
#if DBG_LVL_01
    printk ("FcapwapWtpTxDeviceOpen: %s opened\n", DEVICE_NAME_AP_SND);
#endif
    device_open (inodep, filep);
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWtpTxDeviceRelease
 *  Description            : This function is used to release the driver
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr
 *  Output(s)              : device cannot be accessed
 *  Returns                : SUCCESS
 * ******************************************************************************/
INT4
FcapwapWtpTxDeviceRelease (struct inode *inodep, struct file *filep)
{

#if DBG_LVL_01
    printk ("FcapwapWtpTxDeviceRelease: %s closed\n", DEVICE_NAME_AP_SND);
#endif

    device_release (inodep, filep);
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapWtpTxIoctl
 *  Description            : This function is used by user space to handle RBTree
 *  Input(s)               : *inodep - inode ptr,  *filep - file ptr, cmd - 
 *                           RB command, arg - RB tree arguments. 
 *  Output(s)              : rb tree creation, rb tree display
 *  Returns                : SUCCESS
 * ******************************************************************************/
static INT4
FcapwapWtpTxIoctl (struct file *filep, UINT4 u4Cmd, FS_ULONG arg)
{
    thandleRbData       thandleRb;
    tApSndStationTable *StationTable = NULL;
    tMacAddr            MacAddrCheck;
    memset (MacAddrCheck, 0, sizeof (tMacAddr));
    /*Copy user parameter from user space of Linux */
    if (copy_from_user (&thandleRb, (void *) arg, sizeof (thandleRbData)))
    {
        printk (KERN_ERR "FcapwapWtpTxIoctlsnd: copy from user failure\n");
        return -EFAULT;
    }

    /*Perform operation as per user information on RB Tree */
    switch (thandleRb.u1RbOperation)
    {
            /* create the mem */
        case CREATE_ENTRY:
            if (RbtTxCreateSta (thandleRb.i4RbNodes) != RB_SUCCESS)
            {
                printk (KERN_ERR "FcapwapWtpTxIoctl: RB Tree Create Failure\n");
            }
            break;
        case DISPLAY_ENTRY:
            RbtDisplay (thandleRb.i4TableNum);
            break;
        case INSERT_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_WLC_MAC:
                    memcpy (gau1WlcMac, &thandleRb.rbData.WlcMacAddr,
                            MAC_ADDR_LEN);
                    memcpy (gau1WlcInterface, thandleRb.rbData.au1Interface,
                            INTERFACE_LEN);

                    break;
                case AP_WTP_TABLE:
                    gu2WlcDataPort = thandleRb.rbData.u2WlcDataPort;
                    gu2WtpDataPort = thandleRb.rbData.u2WtpDataPort;
                    gu4WlcIpAddr = thandleRb.rbData.u4WlcIpAddr;
                    gu4WtpIpAddr = thandleRb.rbData.u4WtpIpAddr;
                    gu1LocalRoutingStatus = thandleRb.rbData.u1LocalRouting;
                    memcpy (gau1WlcInterface, thandleRb.rbData.au1Interface,
                            INTERFACE_LEN);
                    memcpy (gau1VlanInterface,
                            thandleRb.rbData.au1VlanInterface, INTERFACE_LEN);
                    gu2VlanID = thandleRb.rbData.u2VlanId;
                    i4IsRunStateReached = 1;
                    if (memcmp
                        (thandleRb.rbData.WlcMacAddr, MacAddrCheck,
                         sizeof (tMacAddr)) != 0)
                    {
                        memcpy (gau1WlcMac, &thandleRb.rbData.WlcMacAddr,
                                MAC_ADDR_LEN);
                    }
                    gi4CapwapPortMtu = thandleRb.rbData.i4CapwapPortMtu;
#if DBG_LVL_01
                    RbtDisplay (thandleRb.i4TableNum);
#endif
                    break;

                case AP_STA_TABLE:
                    if (RbtTxInsertSta (&thandleRb.rbData) != RB_SUCCESS)
                    {
                        printk (KERN_ERR
                                "FcapwapWtpTxIoctl: RB Tree Insert Failure\n");
                    }
#if DBG_LVL_01
                    RbtDisplay (thandleRb.i4TableNum);
#endif
                    break;
                case ADD_DEVICE:
#ifdef QORIQ_WANTED
                    if (AddDevice (thandleRb.rbData.au1DeviceName) != SUCCESS)
                        printk (KERN_ERR
                                "FcapwapWtpTxIoctl: DevAdd Pack Failed\n");
#else
                    if (gu1LocalRoutingStatus == 1)
                    {
                        if (AddDevice (thandleRb.rbData.au1DeviceName) !=
                            SUCCESS)
                            printk (KERN_ERR
                                    "FcapwapWtpTxIoctl: DevAdd Pack Failed\n");
                    }
#endif
                    break;
                default:
                    printk (KERN_ERR
                            "FcapwapWtpTxIoctl: switch reached default\n");
                    break;
            }
            break;
        case REMOVE_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_WTP_TABLE:
                    gu2WlcDataPort = 0;
                    gu2WtpDataPort = 0;
                    gu4WlcIpAddr = 0;
                    memset (gau1WlcMac, 0, 6);
                    memset (gau1WlcInterface, 0, INTERFACE_LEN);
                    memset (gau1VlanInterface, 0, INTERFACE_LEN);
                    gu2VlanID = 0;
                    gu1LocalRoutingStatus = LOCAL_ROUTE_DISABLE;
                    i4IsRunStateReached = 0;
                    gi4CapwapPortMtu = 0;
                    break;

                case AP_STA_TABLE:
                    if (RbtTxDeleteStaMac (thandleRb.rbData.staData.staMac) !=
                        RB_SUCCESS)
                    {
                        printk (KERN_ERR
                                "FcapwapWtpTxIoctl: RB Tree Remove Failure\n");
                    }
                    break;
                case REMOVE_DEVICE:
#ifdef QORIQ_WANTED
                    RemoveDevice (&thandleRb.rbData.au1DeviceName);
#else
                    if (gu1LocalRoutingStatus == 1)
                    {
                        RemoveDevice (&thandleRb.rbData.au1DeviceName);
                    }
#endif
                    break;
                default:
                    printk (KERN_ERR
                            "FcapwapWtpTxIoctl: switch reached default\n");
                    break;
            }
            break;

        case REPLACE_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_STA_TABLE:
                    if (RbtTxReplaceStaMac (thandleRb.rbData.staData.staMac,
                                            &thandleRb.replaceData) !=
                        RB_SUCCESS)
                        printk (KERN_ERR
                                "FcapwapWtpTxIoctl: RB Tree Remove Failure\n");
                    break;
                default:
                    printk (KERN_ERR
                            "FcapwapWtpTxIoctl: switch reached default\n");
                    break;
            }
            break;
        case DESTROY_ENTRY:
            switch (thandleRb.i4TableNum)
            {
                case AP_STA_TABLE:
                    RbtDestroy_Sta ();
                    break;
                default:
                    printk (KERN_ERR
                            "FcapwapWtpTxIoctl: switch reached default\n");
                    break;
            }
            printk (KERN_INFO "FcapwapWtpTxIoctl: RB Tree destroyed\n");
            break;
        case STATION_STATUS:
            StationTable = RbtTxSearchstaMac (thandleRb.rbData.staData.staMac);
            if (StationTable != NULL)
            {
                thandleRb.rbData.staData.u1StationStatus =
                    StationTable->staData.u1StationStatus;
                copy_to_user ((thandleRbData *) arg, &thandleRb,
                              sizeof (thandleRb));
                StationTable->staData.u1StationStatus = 0;
            }
            break;

        default:
            printk
                ("FcapwapWtpTxIoctl: switch case Error thandleRb.u1Operation\n");
            break;
    }
    return 0;
}

/******************************************************************************
 *  Function Name          : FcapwapProcessFrameSnd
 *  Description            : This function is used to process the frame 
 *                           received from the Ethernet
 *  Input(s)               : void 
 *  Output(s)              : buffer data will be sent through queue.
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
INT4
FcapwapProcessFrameSnd (struct sk_buff *pSkb, struct net_device *pNetDev,
                        struct packet_type *pPktType,
                        struct net_device *pOrgDev)
{
    if (pSkb->pkt_type == PACKET_OUTGOING)
    {
        dev_kfree_skb_any (pSkb);
        return NET_RX_SUCCESS;
    }
    if ((i4IsRunStateReached == 1)
        && (gu1LocalRoutingStatus != LOCAL_BRIDGE_ENABLE))
    {
        /* CAPWAP CASE */
        if (FcapwapWtpProcessWlan (pSkb) != SUCCESS)
        {
            dev_kfree_skb_any (pSkb);
            return NET_RX_DROP;
        }
    }
    else
    {
        dev_kfree_skb_any (pSkb);
    }

    return NET_RX_SUCCESS;
}

/******************************************************************************
 *  Function Name          : FcapwapWtpProcessWlanPacket
 *  Description            : This function is used to process the WLAN packet
 *  Input(s)               : void 
 *  Output(s)              : WLAN Interface packet will be processed
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
INT4
FcapwapWtpProcessWlan (struct sk_buff *sk_buffer)
{
    /*perform actual processing of data */
    int                 i4RetVal = 0;
    i4RetVal = FcapwapWtpTxProcessDataPkt (sk_buffer);
    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : FcapwapWtpTxProcessDataPkt
 *  Description            : This function adds the capwap header and send to
 *                           WLC
 *  Input(s)               : psk_buffer - Pointer to skb
 *  Output(s)              : The data will be received.
 *  Returns                : buffer data
 * ******************************************************************************/
INT4
FcapwapWtpTxProcessDataPkt (struct sk_buff *psk_buffer)
{
    tApSndStationTable *rbData_Sta;
    UINT2               u2UdpLen = 0, u2IpLen = 0;
    struct iphdr       *iph;
    struct ethhdr      *eh;
    struct vlan_ethhdr *vlan = NULL;
    struct udphdr      *udph = NULL;
    struct net_device  *dev = NULL;
    struct in_device   *out_dev = NULL;
    struct in_device   *in_dev = NULL;
    struct in_ifaddr   *if_info = NULL;
    struct net_device  *device = NULL;
    struct inet6_dev   *ipv6dev = NULL;
    struct inet6_ifaddr *ifp = NULL;
    struct in6_addr    *saddr, *daddr = NULL;
    INT4                i4headroom = 0, i4Length = 0, i4CapwapLen = 0;
    INT4                i4Error = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DestIp = 0;
    UINT4               u4ArpIp = 0;
    UINT2               u2Ethertype = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2TCI = 0;
    UINT2               u2Priority = 0;
    UINT2               u2CheckSum = 0;
    UINT2               u2OldCheck = 0;
    UINT1               u1WMMStatus = 0;
    UINT1               u1TrustModeStatus = 0;
    UINT1               u1QosProfile = 0;
    UINT1               u1UserPriority = 0;
    UINT1               u1TaggingPolicy = 0;
    UINT1              *pu1SrcMac;
    UINT1               au1Bssid[MACADDR_SIZE];
    UINT1               u1IsIpV6 = 0;

    struct sk_buff     *new_skb = NULL;
    struct tcphdr      *tcph = NULL;
    UINT4               u4Hdrlen = 0;
    UINT4               packet_len = 0;

    UINT1               u1Flags = 0;
    UINT2               u2TcpPort = 0;
    struct iphdr       *ih;
    struct udphdr      *uh;
    struct ipv6hdr     *ip6h = NULL;
    UINT2               u2Dstport = 0;
    UINT2               u2Srcport = 0;

    packet_len = (psk_buffer)->len;

    /* Check the UDP protocol */
    if (ip_hdr (psk_buffer)->version == IP_VERSION)
    {
        u4Protocol = (ip_hdr (psk_buffer))->protocol;
        u4SrcIp = ntohl ((ip_hdr (psk_buffer))->saddr);
        u4DestIp = ntohl ((ip_hdr (psk_buffer))->daddr);
    }
    else
    {
        u1IsIpV6 = 1;
        ip6h = ipv6_hdr (psk_buffer);
        saddr = &ip6h->saddr;
        daddr = &ip6h->daddr;
    }
    pu1SrcMac = (eth_hdr (psk_buffer))->h_source;

    rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);
    if (rbData_Sta != NULL)
    {
        if (gu4WlcIpAddr == 0)
        {
            /*Set Status bit of Sation to 1 */
            rbData_Sta->staData.u1StationStatus = 1;
            u2Ethertype = (eth_hdr (psk_buffer))->h_proto;
            if (ntohs (u2Ethertype) == PNAC_ETHERTYPE)
            {
                if (!startqueuing)
                {
                    printk ("<1> dprx: device not yet openned...not queuing\n");
                    return NET_RX_SUCCESS;
                }

                FFQueuePkt (psk_buffer);
                goahead = 1;
                wake_up_interruptible (&wq);

                return NET_RX_SUCCESS;

            }
            else
            {
                dev_kfree_skb_any (psk_buffer);
                return NET_RX_SUCCESS;
            }
        }
        if (gu1LocalRoutingStatus == LOCAL_ROUTE_ENABLE)
        {
            rbData_Sta->staData.u1StationStatus = 1;
            if (!u1IsIpV6)
            {
                for_each_netdev (&init_net, device)
                {
                    if (device != NULL)
                    {
                        in_dev = (struct in_device *) device->ip_ptr;
                        if (in_dev != NULL)
                        {
                            if_info = in_dev->ifa_list;
                            if (if_info != NULL)
                            {
                                if (if_info->ifa_address == u4DestIp)
                                {
                                    dev_kfree_skb_any (psk_buffer);
                                    return SUCCESS;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                for_each_netdev (&init_net, device)
                {
                    if (device != NULL)
                    {
                        ipv6dev = __in6_dev_get (device);
                        if (ipv6dev)
                        {
                            list_for_each_entry (ifp, &ipv6dev->addr_list,
                                                 if_list)
                            {
                                if (ipv6_addr_equal (daddr, &ifp->addr))
                                {
                                    dev_kfree_skb_any (psk_buffer);
                                    return SUCCESS;
                                }
                            }
                        }
                    }
                }
            }

            u2Ethertype = (eth_hdr (psk_buffer))->h_proto;
            if (ntohs (u2Ethertype) != PNAC_ETHERTYPE)
            {
                if (rbData_Sta->staData.u1WebAuthStatus == WEBAUTH_ENABLE)
                {
                    if (rbData_Sta->staData.u1WebAuthEnable ==
                        STA_AUTHENTICATED)
                    {
                        pu1SrcMac = (eth_hdr (psk_buffer))->h_source;
                        rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);
                        if (rbData_Sta != NULL)
                        {
                            if (rbData_Sta->staData.u1WMMEnable == WMM_ENABLE)
                            {
                                u1WMMStatus = 1;
                                u1TrustModeStatus =
                                    rbData_Sta->staData.u1TrustMode;
                                u1QosProfile =
                                    rbData_Sta->staData.u1QosProfilePriority;
                                u1TaggingPolicy =
                                    rbData_Sta->staData.u1TaggingPolicy;
                                u2VlanId = rbData_Sta->staData.u2VlanId;

                                /*Assuming that the UP for 0  is mapped to skb_priority 256 */
                                u1UserPriority = (UINT1) psk_buffer->priority;
                            }
                            if ((u1WMMStatus == WMM_ENABLE)
                                && ((u1TaggingPolicy == TAG_POLICY_DSCP)
                                    || (u1TaggingPolicy == TAG_POLICY_BOTH)))
                            {
                                u2Ethertype = (eth_hdr (psk_buffer))->h_proto;

                                if (u1TrustModeStatus == TRUST_MODE)
                                {
                                    /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
                                    if ((ntohs (u2Ethertype) != 0x806)
                                        && ((ntohs (u2Ethertype) != 0x888e)))
                                    {
                                        u2OldCheck =
                                            (ip_hdr (psk_buffer))->check;
                                        (ip_hdr (psk_buffer))->check = 0;
                                        gu4Tos = (ip_hdr (psk_buffer))->tos;
                                        u2CheckSum =
                                            IpCalcCheckSum ((ip_hdr
                                                             (psk_buffer)), 20);
                                        (ip_hdr (psk_buffer))->check =
                                            u2CheckSum;
                                    }
                                }
                                else if (u1TrustModeStatus == NO_TRUST_MODE)
                                {

                                    if ((ntohs (u2Ethertype) != 0x806)
                                        && ((ntohs (u2Ethertype) != 0x888e)))
                                    {
                                        u2OldCheck =
                                            (ip_hdr (psk_buffer))->check;
                                        (ip_hdr (psk_buffer))->check = 0;
                                        /*FIX for the bug unknown DSCP */
                                        (ip_hdr (psk_buffer))->tos =
                                            (((ip_hdr (psk_buffer))->
                                              tos & 3) |
                                             (FcapwapGetDscpFromQosProfile
                                              (u1QosProfile) << 2));
                                        u2CheckSum =
                                            IpCalcCheckSum ((ip_hdr
                                                             (psk_buffer)), 20);
                                        (ip_hdr (psk_buffer))->check =
                                            u2CheckSum;
                                    }
                                }
                            }
                        }
                        /*Set Status bit of Sation to 1 */
                        rbData_Sta->staData.u1StationStatus = 1;
                        dev_kfree_skb_any (psk_buffer);
                        return SUCCESS;
                    }

                    if (u4Protocol == IPPROTO_UDP)
                    {
                        ih = (struct iphdr *) skb_network_header (psk_buffer);
                        uh = (struct udphdr *) (psk_buffer->data +
                                                (ih->ihl * IHL_SIZE));
                        u2Dstport = ntohs ((UINT2) uh->dest);
                        u2Srcport = ntohs ((UINT2) uh->source);
                        if ((u2Dstport == 0x43) || (u2Srcport == 0x44)
                            || (u2Dstport == 0x35))
                        {
                            dev_kfree_skb_any (psk_buffer);
                            return SUCCESS;
                        }
                    }

                    if (u4Protocol == IPPROTO_TCP)
                    {
                        u4Hdrlen = (ip_hdr (psk_buffer))->tot_len;
                        iph = (struct iphdr *) skb_network_header (psk_buffer);

                        MEMCPY (&u2TcpPort, &(psk_buffer->data[22]), 2);

                        if ((rbData_Sta->staData.u2TcpPort == ntohs (u2TcpPort))
                            || (ntohs (u2TcpPort) == 80))
                        {
                            /*Check whether the http packet is destined to WLC
                               If it is so, send it to user space */
                            if ((u4DestIp & 0xFF000000) !=
                                (u4SrcIp & 0xFF000000))
                            {
                                u1Flags = psk_buffer->data[20 + 13];

                                if (u1Flags == 0x02)
                                {
                                    dev_kfree_skb_any (psk_buffer);
                                    return SUCCESS;
                                }
                                else if (u1Flags == 0x10)
                                {
                                    dev_kfree_skb_any (psk_buffer);
                                    return SUCCESS;
                                }
                                else if (u1Flags == 0x12)
                                {
                                    dev_kfree_skb_any (psk_buffer);
                                    return SUCCESS;
                                }
                            }
                        }
                    }
                    if (u4Protocol == IPPROTO_ICMP)
                    {
                        return FAIL;
                    }
                    if (ntohs (u2Ethertype) == ARP_ETHERTYPE)
                    {
                        MEMCPY (&u4ArpIp,
                                &psk_buffer->data[psk_buffer->len - 4], 4);
                        u4ArpIp = OSIX_NTOHL (u4ArpIp);

                        for_each_netdev (&init_net, device)
                        {
                            if (device != NULL)
                            {
                                in_dev = (struct in_device *) device->ip_ptr;
                                if (in_dev != NULL)
                                {
                                    if_info = in_dev->ifa_list;
                                    if (if_info != NULL)
                                    {
                                        if (OSIX_HTONL (if_info->ifa_address) ==
                                            u4ArpIp)
                                        {
                                            dev_kfree_skb_any (psk_buffer);
                                            return SUCCESS;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    {
                        {
                            pu1SrcMac = (eth_hdr (psk_buffer))->h_source;
                            rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);
                            if (rbData_Sta != NULL)
                            {
                                if (rbData_Sta->staData.u1WMMEnable ==
                                    WMM_ENABLE)
                                {
                                    u1WMMStatus = 1;
                                    u1TrustModeStatus =
                                        rbData_Sta->staData.u1TrustMode;
                                    u1QosProfile =
                                        rbData_Sta->staData.
                                        u1QosProfilePriority;
                                    u1TaggingPolicy =
                                        rbData_Sta->staData.u1TaggingPolicy;
                                    u2VlanId = rbData_Sta->staData.u2VlanId;

                                    /*Assuming that the UP for 0  is mapped to skb_priority 256 */
                                    u1UserPriority =
                                        (UINT1) psk_buffer->priority;
                                }
                                if ((u1WMMStatus == WMM_ENABLE)
                                    && ((u1TaggingPolicy == TAG_POLICY_DSCP)
                                        || (u1TaggingPolicy ==
                                            TAG_POLICY_BOTH)))
                                {
                                    u2Ethertype =
                                        (eth_hdr (psk_buffer))->h_proto;

                                    if (u1TrustModeStatus == TRUST_MODE)
                                    {
                                        /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
                                        if ((ntohs (u2Ethertype) != 0x806)
                                            &&
                                            ((ntohs (u2Ethertype) != 0x888e)))
                                        {
                                            u2OldCheck =
                                                (ip_hdr (psk_buffer))->check;
                                            (ip_hdr (psk_buffer))->check = 0;
                                            gu4Tos = (ip_hdr (psk_buffer))->tos;
                                            u2CheckSum =
                                                IpCalcCheckSum ((ip_hdr
                                                                 (psk_buffer)),
                                                                20);
                                            (ip_hdr (psk_buffer))->check =
                                                u2CheckSum;
                                        }
                                    }
                                    else if (u1TrustModeStatus == NO_TRUST_MODE)
                                    {

                                        if ((ntohs (u2Ethertype) != 0x806)
                                            &&
                                            ((ntohs (u2Ethertype) != 0x888e)))
                                        {
                                            u2OldCheck =
                                                (ip_hdr (psk_buffer))->check;
                                            (ip_hdr (psk_buffer))->check = 0;
                                            /*FIX for the bug unknown DSCP */
                                            (ip_hdr (psk_buffer))->tos =
                                                (((ip_hdr (psk_buffer))->
                                                  tos & 3) |
                                                 (FcapwapGetDscpFromQosProfile
                                                  (u1QosProfile) << 2));
                                            u2CheckSum =
                                                IpCalcCheckSum ((ip_hdr
                                                                 (psk_buffer)),
                                                                20);
                                            (ip_hdr (psk_buffer))->check =
                                                u2CheckSum;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    /*Set Status bit of Sation to 1 */
                    rbData_Sta->staData.u1StationStatus = 1;
                    dev_kfree_skb_any (psk_buffer);
                    return SUCCESS;
                }
            }
        }
        if (rbData_Sta->staData.u1WMMEnable == WMM_ENABLE)
        {
            u1WMMStatus = 1;
            u1TrustModeStatus = rbData_Sta->staData.u1TrustMode;
            u1QosProfile = rbData_Sta->staData.u1QosProfilePriority;
            u1TaggingPolicy = rbData_Sta->staData.u1TaggingPolicy;
            u2VlanId = rbData_Sta->staData.u2VlanId;

            /*Assuming that the UP for 0  is mapped to skb_priority 256 */
            UINT4               u4PriorityValue = 0;
            u1UserPriority = (UINT1) psk_buffer->priority;
        }
        if ((u1WMMStatus == WMM_ENABLE)
            && ((u1TaggingPolicy == TAG_POLICY_DSCP)
                || (u1TaggingPolicy == TAG_POLICY_BOTH)))
        {
            u2Ethertype = (eth_hdr (psk_buffer))->h_proto;

            if (u1TrustModeStatus == TRUST_MODE)
            {
                /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
                if ((ntohs (u2Ethertype) != 0x806)
                    && ((ntohs (u2Ethertype) != 0x888e)))
                {
                    u2OldCheck = (ip_hdr (psk_buffer))->check;
                    (ip_hdr (psk_buffer))->check = 0;
                    gu4Tos = (ip_hdr (psk_buffer))->tos;
                    u2CheckSum = IpCalcCheckSum ((ip_hdr (psk_buffer)), 20);
                    (ip_hdr (psk_buffer))->check = u2CheckSum;
                }
            }
            else if (u1TrustModeStatus == NO_TRUST_MODE)
            {
                if ((ntohs (u2Ethertype) != 0x806)
                    && ((ntohs (u2Ethertype) != 0x888e)))
                {
                    u2OldCheck = (ip_hdr (psk_buffer))->check;
                    (ip_hdr (psk_buffer))->check = 0;
/*FIX for the bug unknown DSCP*/
                    (ip_hdr (psk_buffer))->tos =
                        (((ip_hdr (psk_buffer))->
                          tos & 3) |
                         (FcapwapGetDscpFromQosProfile (u1QosProfile) << 2));
                    u2CheckSum = IpCalcCheckSum ((ip_hdr (psk_buffer)), 20);
                    (ip_hdr (psk_buffer))->check = u2CheckSum;
                }
            }
        }

        /*Set Status bit of Sation to 1 */
        rbData_Sta->staData.u1StationStatus = 1;

        /*moving the skbuff pointer a head to bring the actual data */
        psk_buffer->data = psk_buffer->data - MAC_HDR_OFFSET;
        psk_buffer->len += MAC_HDR_OFFSET;

        if (rbData_Sta->staData.u1MacType == SPLIT_MAC)
        {
            memset (au1Bssid, 0, MACADDR_SIZE);
            memcpy (au1Bssid, rbData_Sta->staData.bssid, MACADDR_SIZE);

            /* In the case of split mac, convert the dot 3 to dot 11 before
             * transmitting*/

            if (Dot3ToDot11
                ((&psk_buffer->data), au1Bssid, &psk_buffer->len,
                 DOT11_FC_BYTE1_DIR_ST_AP) != SUCCESS)
            {
                printk (KERN_ERR "%s: Dot3 to Dot11 conversion failed\n",
                        __FUNCTION__);
                return FAIL;
            }
        }

        /*get available i4headroom size from skb */
        i4headroom = skb_headroom (psk_buffer);

        i4Length = TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;

        if (rbData_Sta->staData.u1MacType == SPLIT_MAC)
        {
            i4Length += CAPWAP_HDR_SPLIT_SIZE;
            i4CapwapLen = CAPWAP_HDR_SPLIT_SIZE;
        }
        else
        {
            i4Length += CAPWAP_HDR_LOCAL_SIZE;
            i4CapwapLen = CAPWAP_HDR_LOCAL_SIZE;
        }

        /*allocate required i4headroom if we hold less */
        if (i4headroom < i4Length)
        {
            new_skb = skb_realloc_headroom (psk_buffer, i4Length);
            dev_kfree_skb (psk_buffer);
            if (new_skb == NULL)
            {
                printk ("<1> skb_realloc_headroom failed\n");
                return FAIL;
            }
            psk_buffer = new_skb;
        }

        /*re structure skb buffer */
        skb_push (psk_buffer, i4Length);

        /*relocate all skb pointers */
        skb_set_mac_header (psk_buffer, 0);
        skb_set_network_header (psk_buffer, MAC_HDR_OFFSET);
        skb_set_transport_header (psk_buffer,
                                  MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);

        /*Construct CAPWAP Header */
        FcapwapAddCapwapHeader (&psk_buffer->
                                data[i4Length - i4CapwapLen],
                                rbData_Sta->staData.bssid,
                                rbData_Sta->staData.u1MacType);

#ifdef DEBUG_WANTED
        dev = dev_get_by_name (&init_net, gau1WlcInterface);
        if (dev != NULL)
        {
            if (dev->dev_addr != NULL)
            {
                out_dev = (struct in_device *) dev->ip_ptr;
                if (out_dev != NULL)
                {
                    for (if_info = out_dev->ifa_list; if_info != NULL;
                         if_info = if_info->ifa_next)
                    {
                        if (!(strcmp (if_info->ifa_label, dev->name)))
                        {
                            gu4WtpIpAddr = ntohl (if_info->ifa_address);
                            break;
                        }
                    }
                }
            }
        }
#endif

        /* pulling the packet till the NETWORK Header */

        skb_pull (psk_buffer, MAC_HDR_OFFSET);

        if (psk_buffer->len > gi4CapwapPortMtu)
        {
#ifdef QORIQ_WANTED
            /*This has been added as a workaround to prevent kernel panic in P1020 
             * board when connecting to internet. As a result ping size above 1415 will not work*/
            return FAIL;
#endif
            skb_push (psk_buffer, MAC_HDR_OFFSET);

            FcapwapWtpFragmentAndSend (psk_buffer, gi4CapwapPortMtu,
                                       &rbData_Sta->staData);
        }
        else
        {
            skb_push (psk_buffer, MAC_HDR_OFFSET);
            FCapwapFormOuterHeader (psk_buffer, &rbData_Sta->staData);
        }
    }
    else
    {
        dev_kfree_skb_any (psk_buffer);
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : FcapwapAddCapwapHeader
 *  Description            : This function is used to add the CAPWAP header
 *                           to the incoming data packet.
 *  Input(s)               : *pu1Buf - Buffer
 *  Output(s)              : capwap header will be added.
 *  Returns                : void 
 * ******************************************************************************/
void
FcapwapAddCapwapHeader (UINT1 *pu1Buf, UINT1 *pu1Bssid, UINT1 u1MacType)
{
    tCapwapHdr          pCapwapHdr;
    memcpy (pCapwapHdr.RadioMacAddr, pu1Bssid, MAC_ADDR_LEN);
    CapwapConstructCpHeader (&pCapwapHdr, (int) u1MacType);
    AssembleCapwapHeader (pu1Buf, &pCapwapHdr);
}

/******************************************************************************
 *  Function Name          : AddDevice
 *  Description            : This function is used to get the BSSID from 802.11
 *                           Packet
 *  Input(s)               : *pProcBuf - buffer, pu1BssId - array buff to be
 *                            updated
 *  Output(s)              : BSSID will be retrieved from 802.11 packet.
 *  Returns                : void
 * ******************************************************************************/
INT4
AddDevice (UINT1 *DeviceName)
{
    struct net_device  *dev;
    UINT1               u1Index = 0;
    UINT4               u4DriverIndex = 0;

    for (u1Index = 0; u1Index < MAX_INTERFACE; u1Index++)
    {
        if (memcmp (au1Interface[u1Index], DeviceName, INTERFACE_LEN) == 0)
        {
            printk ("<1> device - %s already present\n", DeviceName);
            return SUCCESS;
        }
        if (strcmp (au1Interface[u1Index], "") == 0)
        {
            u4DriverIndex = u1Index;
            break;
        }
    }

    dev = dev_get_by_name (&init_net, DeviceName);
    frame[u4DriverIndex].dev = dev;
    frame[u4DriverIndex].type = __constant_htons (ETH_P_ALL);
    frame[u4DriverIndex].func = FcapwapProcessFrameSnd;
    if (frame[u4DriverIndex].dev != NULL)
    {
        dev_add_pack (&frame[u4DriverIndex]);
        dev_put (dev);
        printk ("<1> dev add pack success\n");
        memcpy (au1Interface[u4DriverIndex], DeviceName, INTERFACE_LEN);
        return SUCCESS;
    }
    printk ("<1> dev add pack FAIL\n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RemoveDevice
 *  Description            : This function is used to get the BSSID from 802.11
 *                           Packet
 *  Input(s)               : *pProcBuf - buffer, pu1BssId - array buff to be
 *                            updated
 *  Output(s)              : BSSID will be retrieved from 802.11 packet.
 *  Returns                : void
 * ******************************************************************************/
VOID
RemoveDevice (UINT1 *DeviceName)
{
    struct net_device  *dev;
    UINT1               u1Index = 0;
    INT4                i4RetVal = 0;

    for (u1Index = 0; u1Index < MAX_INTERFACE; u1Index++)
    {
        i4RetVal = memcmp (au1Interface[u1Index], DeviceName, INTERFACE_LEN);
        if (i4RetVal == 0)
        {
            printk ("<1> dev remove index - %d\n", u1Index);
            dev = dev_get_by_name (&init_net, DeviceName);
            frame[u1Index].dev = dev;
            if (frame[u1Index].dev != NULL)
            {
                dev_remove_pack (&frame[u1Index]);
                dev_put (dev);
                memcpy (au1Interface[u1Index], "\0", INTERFACE_LEN);
                printk ("dev remove pack success for %d - %s\n", u1Index,
                        DeviceName);
                return;
            }
        }
    }
    return;
}

 /******************************************************************************
 *  Function Name          : FcapwapGetDscpFromQosProfile
 *  Description            : This function returns dscp priority equivalent to 
 *                   Qos Profile
 *  Input(s)               : Qos Profile
 *  Output(s)              : dscp priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetDscpFromQosProfile (UINT1 u1QosProfile)
{
    return gu4ProfilePriorityMapping[u1QosProfile][1];
}

/******************************************************************************
 *  Function Name          : FcapwapGetPcpFromUP
 *  Description            : This function returns pcp priority equivalent to 
 *                   User priority
 *  Input(s)               : User priority
 *  Output(s)              : pcp priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetPcpFromUP (UINT1 u1UserPriority)
{
    return gu4PriorityMapping[u1UserPriority][0];
}

 /******************************************************************************
 *  Function Name          : FcapwapGetPcpFromQosProfile
 *  Description            : This function returns pcp priority equivalent to 
 *                   Qos Profile
 *  Input(s)               : Qos Profile
 *  Output(s)              : pcp priority
 *  Returns                : void 
 * ******************************************************************************/
UINT4
FcapwapGetPcpFromQosProfile (UINT1 u1QosProfile)
{
    return gu4ProfilePriorityMapping[u1QosProfile][0];
}

/******************************************************************************
 *  Function Name          : FcapwapGetDscpFromUP
 *  Description            : This function returns dscp priority equivalent to 
 *                   User priority
 *  Input(s)               : User priority
 *  Output(s)              : dscp priority
 *  Returns                : void 
 * ******************************************************************************/

UINT4
FcapwapGetDscpFromUP (UINT1 u1UserPriority)
{
    return gu4PriorityMapping[u1UserPriority][1];
}

/******************************************************************************
 *  Function Name          : IpCalcCheckSum
 *  Description            : This function returns checksum value
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : UNIT2
 * ******************************************************************************/

UINT2
IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1              *pBuf = pSkb;
    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);
    return ((u2Tmp));
}

/******************************************************************************
 *  Function Name          : CRU_BUF_Copy_FromBufChain
 *  Description            : This function copies CRU_BUFF
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : INT4
 * ******************************************************************************/
#if DBG_LVL_FRAG_01
INT4
CRU_BUF_Copy_FromBufChain (struct sk_buff *pSkb, UINT1 *pu1Dst,
                           UINT4 u4Offset, UINT4 u4Size)
{
    UINT4               u4Data = pSkb->len - u4Offset;

    u4Size = (u4Size <= u4Data) ? u4Size : u4Data;
    memcpy (pu1Dst, pSkb->data + u4Offset, u4Size);
    return u4Size;
}

/******************************************************************************
 *  Function Name          : CRU_BUF_Get_DataPtr_IfLinear
 *  Description            : This function copies CRU_BUFF
 *  Input(s)               : SKB buffer
 *  Output(s)              : checksum value
 *  Returns                : INT4
 * ******************************************************************************/
UINT1              *
CRU_BUF_Get_DataPtr_IfLinear (struct sk_buff *pSkb, UINT4 u4Offset,
                              UINT4 u4Size)
{
    if ((pSkb == NULL))
    {
        return NULL;
    }
    if ((u4Offset + u4Size) <= (pSkb->len))
    {
        return ((pSkb->data) + u4Offset);
    }
    return (NULL);
}
#endif
INT4
FcapwapWtpFragmentAndSend (struct sk_buff *psk_buff, INT4 i4MTU,
                           twlanStaInfo * pStaData)
{
    UINT2               u2FragDataSize = 0;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;
    UINT2               u2FragId = 0;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pPktBuf = NULL;
    UINT1               u1FlgOffs = 0;
    UINT1               u1HdrLen = 0;
    INT1                i1LastFrag;
    tCapwapHdr          CapHdr;
    UINT1               u1Type[2];
    UINT1               u1Length[2];
    UINT2               u2PacketLen = 0;
    UINT1               u1LastFrag = OSIX_FALSE;
    UINT4               u4MTU = 0;
    UINT1               u1Index = 0;
    UINT1               u1IsFragmented = 0;
    struct net_device  *org_dev = NULL;
    INT4                i4Error = 0;
    struct net_device  *dev = NULL;

    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    struct sk_buff     *new_skb = NULL;

    MEMSET (&CapHdr, 0, sizeof (tCapwapHdr));
    MEMSET (&u1Type, 0, sizeof (UINT2));
    MEMSET (&u1Length, 0, sizeof (UINT2));

    u4MTU = (UINT4) i4MTU;

    if ((pPktBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        printk
            ("FcapwapFragmentDataAndSend: CRU Buffer Memory Allocation Failed\n");
        return FAIL;
    }

    if (psk_buff != NULL)
    {
        skb_pull (psk_buff,
                  TRANSPORT_HDR_SIZE + MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);
    }

    u4MTU = u4MTU - (TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET);

    pPktBuf->pSkb = psk_buff;
    org_dev = psk_buff->dev;

    /*skb_pull (pPktBuf->pSkb,CAPWAP_HDR_OFFSET); */

    u2PacketLen = CRU_BUF_Get_ChainValidByteCount (pPktBuf);

    /* Get CAPWAP Header Length */
    CRU_BUF_Copy_FromBufChain (pPktBuf, &u1HdrLen,
                               CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);
    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);

    /* Get CAPWAP Header Flags */
    CRU_BUF_Copy_FromBufChain (pPktBuf, &u1FlgOffs,
                               CAPWAP_FLAGS_BIT_OFFSET,
                               CAPWAP_FLAGS_OFFSET_LEN);

    i1LastFrag = (u1FlgOffs & CAPWAP_LAST_FRAG_BIT) ? TRUE : FALSE;

    /* Get Capwap Header */
    CapwapExtractHdr (pPktBuf, &CapHdr);

    /* Move the cru buffer */
    u2PacketLen = (INT2) (u2PacketLen - u1HdrLen);
    CRU_BUF_Move_ValidOffset (pPktBuf, u1HdrLen);

    u4MTU = u4MTU - u1HdrLen;

    u2FragId = gu2FragId;

    /*  Fragments should be in  multpIple of 8 bytes */
    if (u4MTU < u1HdrLen)
    {
        printk
            ("FcapwapFragmentDataAndSend: MTU is less than capwap header length\n");
        CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
        return FAIL;
    }

    pTmpBuf = pPktBuf;

    while (u2PacketLen > 0)
    {
        if ((UINT2) (u2PacketLen) <= u4MTU)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = (UINT2) u2PacketLen;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_LAST_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1LastFrag = OSIX_TRUE;
            u1IsFragmented = 0;
        }
        else
        {
            u2FragDataSize = (UINT2) u4MTU;
            CapHdr.u1FlagsFLWMKResd |= CAPWAP_MORE_FRAG_BIT;
            CapHdr.u1FlagsFLWMKResd |= i1LastFrag;
            CapHdr.u2FragId = u2FragId;
            CapHdr.u2FragOffsetRes3 = (UINT2) (u2Offset << 3);
            pPktBuf = pTmpBuf;
            u1IsFragmented = 1;

            if (CRU_BUF_Fragment_BufChain (pPktBuf, u2FragDataSize, &pTmpBuf)
                != CRU_SUCCESS)
            {
                printk ("FcapwapFragmentDataAndSend: Failed to Fragment the\ 
                        Packet\n");
                CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
                return FAIL;
            }

            if (pTmpBuf == NULL)
            {
                printk
                    ("Error in Fragmentation :- Fragmented Buff is NULL\r\n");
                CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
                return FAIL;
            }
        }

        u2Len = (UINT2) (CRU_BUF_Get_ChainValidByteCount (pPktBuf));

        /* Now the datagram to send is in pPktBuf; Add frag specific info */
        u2Offset += u2FragDataSize;    /* Each time offset gets updated */
        u2PacketLen -= u2FragDataSize;

        CapwapPutHdr (pPktBuf, &CapHdr);

        if (u1IsFragmented == 1)
        {
            /*get available headroom size from skb */
            i4headroom = skb_headroom (pTmpBuf->pSkb);
            i4Length = TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;
            /*allocate required headroom if we hold less */

            if (i4headroom < i4Length)
            {
                new_skb = skb_realloc_headroom (pTmpBuf->pSkb, i4Length + 18);
                dev_kfree_skb (pTmpBuf->pSkb);
                if (new_skb == NULL)
                {
                    printk ("<1> skb_realloc_headroom failed\n");
                    return FAIL;
                }
                pTmpBuf->pSkb = new_skb;
            }
        }

        skb_push (pPktBuf->pSkb,
                  TRANSPORT_HDR_SIZE + MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);

        skb_set_mac_header (pPktBuf->pSkb, 0);
        skb_set_network_header (pPktBuf->pSkb, MAC_HDR_OFFSET);
        skb_set_transport_header (pPktBuf->pSkb,
                                  MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);

        /*skb_pull(pPktBuf->pSkb,MAC_HDR_OFFSET); */

        if (pPktBuf->pSkb->dev == NULL)
        {
            pPktBuf->pSkb->dev = org_dev;
        }

        if (FCapwapFormOuterHeader (pPktBuf->pSkb, pStaData) == FAIL)
        {
            return FAIL;
        }

        dev = dev_get_by_name (&init_net, gau1WlcInterface);

        if (dev == NULL)
        {
            printk (KERN_ERR "Error : Ethernet Device\n");
            return FAIL;
        }

        pPktBuf->pSkb->pkt_type = PACKET_OUTGOING;
        /* changing the dev to output device we need */
        pPktBuf->pSkb->dev = dev;
        i4Error = dev_queue_xmit (pPktBuf->pSkb);
        i4Error = net_xmit_eval (i4Error);
        if (i4Error)
        {
            printk (KERN_ERR "sendToStation: data transmission failed\n");
        }

        /*give dev back */
        dev_put (dev);

        if (pPktBuf != NULL)
        {
            pPktBuf->pSkb = NULL;
            CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
            pPktBuf = NULL;
        }

    }

    /* Increment the Fragment Id */
    gu2FragId++;
    return SUCCESS;

}

INT4
FCapwapFormOuterHeader (struct sk_buff *sk_buffer, twlanStaInfo * pStaData)
{
    tApSndStationTable *rbData_Sta;
    UINT2               u2UdpLen = 0;
    UINT2               u2IpLen = 0;
    struct iphdr       *iph;
    struct iphdr       *iph1;
    struct ethhdr      *eh;
    struct vlan_ethhdr *vlan = NULL;
    struct udphdr      *udph;
    struct net_device  *dev = NULL;
    struct in_device   *out_dev;
    struct in_device   *in_dev;
    struct in_ifaddr   *if_info;
    struct net_device  *device;
    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    INT4                i4CapwapLen = 0;
    INT4                i4Error = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DestIp = 0;
    UINT2               u2Ethertype = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2TCI = 0;
    UINT2               u2Priority = 0;
    UINT2               u2CheckSum = 0;
    UINT2               u2OldCheck = 0;
    UINT1               u1WMMStatus = 0;
    UINT1               u1TrustModeStatus = 0;
    UINT1               u1QosProfile = 0;
    UINT1               u1UserPriority = 0;
    UINT1               u1TaggingPolicy = 0;
    UINT1              *pu1SrcMac;
    UINT1               au1Bssid[MACADDR_SIZE];
    struct sk_buff     *new_skb = NULL;
    UINT4               u4Index = 0;

    if (pStaData->u1WMMEnable == WMM_ENABLE)
    {
        u1WMMStatus = 1;
        u1TrustModeStatus = pStaData->u1TrustMode;
        u1QosProfile = pStaData->u1QosProfilePriority;
        u1TaggingPolicy = pStaData->u1TaggingPolicy;
        u2VlanId = pStaData->u2VlanId;
#if DBG_LVL_01
        printk ("WMM enabled \n");
#endif
        /*Assuming that the UP for 0  is mapped to skb_priority 256 */
        u1UserPriority = (UINT1) sk_buffer->priority;
    }

    /*Frame UDP Header */
    u2UdpLen = (UINT4) (sk_buffer->len - (MAC_HDR_OFFSET + NETWORK_HDR_OFFSET));

    /* Frame udp header */
    udph = udp_hdr (sk_buffer);
    if (udph != NULL)
    {
        udph->source = htons (gu2WtpDataPort);
        udph->dest = htons (gu2WlcDataPort);
        udph->len = htons (u2UdpLen);
        udph->check = 0;
        udph->check = csum_tcpudp_magic ((htonl (gu4WtpIpAddr)),
                                         (htonl (gu4WlcIpAddr)), u2UdpLen,
                                         IPPROTO_UDP, csum_partial (udph,
                                                                    u2UdpLen,
                                                                    0));
        if (udph->check == 0)
            udph->check = CSUM_MANGLED_0;
    }
    else
    {
        printk ("udph is nulll!!\n");
        return FAIL;
    }

    /* Frame ip header */
    u2IpLen =
        (UINT4) (sk_buffer->len -
                 (sk_buffer->network_header - sk_buffer->mac_header));
    iph = ip_hdr (sk_buffer);
    iph->version = IP_VERSION;
    iph->ihl = IP_HDR_LENGTH;
    if ((u1WMMStatus == WMM_ENABLE)
        && ((u1TaggingPolicy == TAG_POLICY_DSCP)
            || (u1TaggingPolicy == TAG_POLICY_BOTH)))
    {
        if (u1TrustModeStatus == TRUST_MODE)
        {
            /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
            iph->tos = gu4Tos;
        }
        else if (u1TrustModeStatus == NO_TRUST_MODE)
        {
            /*FIX for the bug unknown DSCP */
            iph->tos = ((iph->tos & 3)
                        | (FcapwapGetDscpFromQosProfile (u1QosProfile) << 2));
        }
    }
    else
    {
        iph->tos = 0;
#if DBG_LVL_01
        printk ("packets are sending via bronzee \n");
#endif
    }
    iph->tot_len = htons (u2IpLen);
    iph->id = htons (0);
    iph->frag_off = 0;
    iph->ttl = IP_TTL_VALUE;
    iph->protocol = IPPROTO_UDP;
    iph->check = 0;
    iph->saddr = htonl (gu4WtpIpAddr);
    iph->daddr = htonl (gu4WlcIpAddr);
    iph->check = ip_fast_csum ((UINT1 *) iph, iph->ihl);
    if (gu2VlanID != 0)
    {
        new_skb = NULL;
        i4headroom = skb_headroom (sk_buffer);
        if (i4headroom < VLAN_HDR_LEN)
        {
            new_skb = skb_realloc_headroom (sk_buffer, VLAN_HDR_LEN + 18);
            dev_kfree_skb (sk_buffer);
            if (new_skb == NULL)
            {
                printk ("<1> FcapwapWtpTxProcessDataPkt:"
                        "skb_realloc_headroom failed\n");
                return;
            }
            sk_buffer = new_skb;
        }
        skb_push (sk_buffer, VLAN_HDR_LEN);
        memcpy (sk_buffer->data, &sk_buffer->data[VLAN_HDR_LEN],
                MAC_HDR_OFFSET);
        vlan = (struct vlan_ethhdr *) sk_buffer->data;
        vlan->h_vlan_proto = 0x8100;
        vlan->h_vlan_encapsulated_proto = ETH_P_IP;
        vlan->h_vlan_TCI = gu2VlanID;

        dev = dev_get_by_name (&init_net, gau1WlcInterface);
        if (dev == NULL)
        {
            printk (KERN_ERR "Error : dev_get_by_name failed\n");
            return FAIL;
        }

        memcpy (vlan->h_source, dev->dev_addr, ETH_ALEN);
        memcpy (vlan->h_dest, gau1WlcMac, ETH_ALEN);

        skb_set_mac_header (sk_buffer, 0);
        sk_buffer->network_header = (sk_buffer->mac_header +
                                     (MAC_HDR_OFFSET + VLAN_HDR_LEN));
        sk_buffer->transport_header =
            (sk_buffer->network_header + NETWORK_HDR_OFFSET);
    }
    else if ((u1WMMStatus == WMM_ENABLE)
             && ((u1TaggingPolicy == TAG_POLICY_PBIT)
                 || (u1TaggingPolicy == TAG_POLICY_BOTH)))
    {
        /* Portion of code present here was removed as part of kenstel
           dakota changes, code which was present here can be accessed 
           in any of the old tags made before Aug 18 2017 from WSS Repo */
    }
    else
    {

        /* Frame Ethernet header */
        eh = eth_hdr (sk_buffer);
        if (eh == NULL)
        {
            printk (KERN_ERR "Error : Ethernet Header\n");
            return FAIL;

        }
        dev = dev_get_by_name (&init_net, gau1WlcInterface);
        if (dev == NULL)
        {
            printk (KERN_ERR "Error : Ethernet Device\n");
            return FAIL;
        }

        memcpy (eh->h_source, dev->dev_addr, ETH_ALEN);
        memcpy (eh->h_dest, gau1WlcMac, ETH_ALEN);
        eh->h_proto = htons (ETH_P_IP);
    }

#ifdef QORIQ_WANTED
    /* Setting it as outgoing packet */
    sk_buffer->pkt_type = PACKET_OUTGOING;
    /* changing the dev to output device we need */
    sk_buffer->dev = dev;
    i4Error = dev_queue_xmit (sk_buffer);
    i4Error = net_xmit_eval (i4Error);
    if (i4Error)
    {
        printk (KERN_ERR "sendToStation: data transmission failed\n");
    }

    /*give dev back */
    dev_put (dev);
#else
    if (gu1LocalRoutingStatus == 1)
    {
        {
            /* Setting it as outgoing packet */
            sk_buffer->pkt_type = PACKET_OUTGOING;
            /* changing the dev to output device we need */
            sk_buffer->dev = dev;
            i4Error = dev_queue_xmit (sk_buffer);
            i4Error = net_xmit_eval (i4Error);
            if (i4Error)
            {
                printk (KERN_ERR "sendToStation: data transmission failed\n");
            }

            /*give dev back */
            dev_put (dev);
        }
    }
#endif
    return SUCCESS;

}

INT4
CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_HDR_LENGTH];
    UINT1               u1HdrLen = 0;
    UINT1              *pCapHdr = NULL;

    /* Get CAPWAP Header Length */
    CRU_BUF_Copy_FromBufChain (pBuf, &u1HdrLen,
                               CAPWAP_HDRLEN_OFFSET, CAPWAP_HDRLEN_FIELD_LEN);

    /* Header length is 32 bit word */
    u1HdrLen = (UINT1) ((u1HdrLen & CAPWAP_HDRLEN_MASK) >> 3);
    u1HdrLen = (UINT1) (u1HdrLen << CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES);
    CRU_BUF_Copy_FromBufChain (pBuf, au1CapHdr, 0, u1HdrLen);
    pCapHdr = au1CapHdr;
    CAPWAP_GET_1BYTE (pHdr->u1Preamble, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2HlenRidWbidFlagT, pCapHdr);
    CAPWAP_GET_1BYTE (pHdr->u1FlagsFLWMKResd, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragId, pCapHdr);
    CAPWAP_GET_2BYTE (pHdr->u2FragOffsetRes3, pCapHdr);
    if (u1HdrLen > 8)
    {
        CAPWAP_GET_1BYTE (pHdr->u1RadMacAddrLength, pCapHdr);
        CAPWAP_GET_NBYTE (pHdr->RadioMacAddr, pCapHdr,
                          pHdr->u1RadMacAddrLength);
    }
    pHdr->u2HdrLen = u1HdrLen;
    return SUCCESS;
}

void
CapwapPutHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr)
{
    UINT1               au1CapHdr[CAPWAP_HDR_LENGTH];

    AssembleCapwapHeader (au1CapHdr, pHdr);
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) au1CapHdr, pHdr->u2HdrLen);

}

UINT4
FcapwapWtpBrHookPostRouting (UINT4 hooknum,
                             struct sk_buff *sk_buffer,
                             const struct net_device *in,
                             const struct net_device *out,
                             int (*okfn) (struct sk_buff *))
{
    UINT2               u2Ethertype = 0;
    tApSndStationTable *rbData_Sta;
    struct iphdr       *ih;
    struct net_device  *dev;
    struct in_device   *out_dev;
    struct in_ifaddr   *if_info;
    UINT4               u4Index = 0;
    UINT1              *pu1SrcMac = NULL;
    UINT1              *pu1DstMac = NULL;
    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    UINT1               u1Protocol = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DestIp = 0;
    struct udphdr      *uh = NULL;
    UINT2               u2Dstport = 0;
    UINT2               u2Srcport = 0;
    UINT4               u4Hdrlen = 0;
    struct iphdr       *iph = NULL;
    struct sk_buff     *new_skb = NULL;
    UINT2               u2TcpPort = 0;
    UINT1               u1Flags = 0;
    UINT4               u4Tos = 0;
    UINT4               u4Ip = 0;
    INT1                i1Char = 0;
    UINT1               u1WMMStatus = 0;
    UINT1               u1TrustModeStatus = 0;
    UINT1               u1QosProfile = 0;
    UINT1               u1UserPriority = 0;
    UINT1               u1TaggingPolicy = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2TCI = 0;
    UINT2               u2Priority = 0;
    UINT2               u2CheckSum = 0;
    UINT2               u2OldCheck = 0;

    pu1SrcMac = (eth_hdr (sk_buffer))->h_source;
    pu1DstMac = (eth_hdr (sk_buffer))->h_dest;
    u2Ethertype = (eth_hdr (sk_buffer))->h_proto;
    if (gu1LocalRoutingStatus != 1)
    {
        rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);

        if (rbData_Sta != NULL)
        {
            if ((ntohs (u2Ethertype) == 0x0800)
                || (ntohs (u2Ethertype) == ARP_ETHERTYPE))
            {
                u1Protocol = (ip_hdr (sk_buffer))->protocol;
                u4SrcIp = ntohl ((ip_hdr (sk_buffer))->saddr);
                u4DestIp = ntohl ((ip_hdr (sk_buffer))->daddr);
                gu4Tos = (ip_hdr (sk_buffer))->tos;

                if (ntohs (u2Ethertype) != PNAC_ETHERTYPE)
                {
                    if (gu1LocalRoutingStatus == 3)
                    {
                        if (rbData_Sta->staData.u1WebAuthStatus ==
                            WEBAUTH_ENABLE)
                        {
                            if (rbData_Sta->staData.u1WebAuthEnable ==
                                STA_AUTHENTICATED)
                            {
                                if (rbData_Sta->staData.u1WMMEnable ==
                                    WMM_ENABLE)
                                {
                                    u1WMMStatus = 1;
                                    u1TrustModeStatus =
                                        rbData_Sta->staData.u1TrustMode;
                                    u1QosProfile =
                                        rbData_Sta->staData.
                                        u1QosProfilePriority;
                                    u1TaggingPolicy =
                                        rbData_Sta->staData.u1TaggingPolicy;
                                    u2VlanId = rbData_Sta->staData.u2VlanId;

                                    /*Assuming that the UP for 0  is mapped to skb_priority 256 */
                                    u1UserPriority =
                                        (UINT1) sk_buffer->priority;
                                }
                                if ((u1WMMStatus == WMM_ENABLE)
                                    && ((u1TaggingPolicy == TAG_POLICY_DSCP)
                                        || (u1TaggingPolicy ==
                                            TAG_POLICY_BOTH)))
                                {
                                    u2Ethertype =
                                        (eth_hdr (sk_buffer))->h_proto;

                                    if (u1TrustModeStatus == TRUST_MODE)
                                    {
                                        /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
                                        if ((ntohs (u2Ethertype) != 0x806)
                                            &&
                                            ((ntohs (u2Ethertype) != 0x888e)))
                                        {
                                            u2OldCheck =
                                                (ip_hdr (sk_buffer))->check;
                                            (ip_hdr (sk_buffer))->check = 0;
                                            gu4Tos = (ip_hdr (sk_buffer))->tos;
                                            u2CheckSum =
                                                IpCalcCheckSum ((ip_hdr
                                                                 (sk_buffer)),
                                                                20);
                                            (ip_hdr (sk_buffer))->check =
                                                u2CheckSum;
                                        }
                                    }
                                    else if (u1TrustModeStatus == NO_TRUST_MODE)
                                    {

                                        if ((ntohs (u2Ethertype) != 0x806)
                                            &&
                                            ((ntohs (u2Ethertype) != 0x888e)))
                                        {
                                            u2OldCheck =
                                                (ip_hdr (sk_buffer))->check;
                                            (ip_hdr (sk_buffer))->check = 0;
                                            /*FIX for the bug unknown DSCP */
                                            (ip_hdr (sk_buffer))->tos =
                                                (((ip_hdr (sk_buffer))->
                                                  tos & 3) |
                                                 (FcapwapGetDscpFromQosProfile
                                                  (u1QosProfile) << 2));
                                            u2CheckSum =
                                                IpCalcCheckSum ((ip_hdr
                                                                 (sk_buffer)),
                                                                20);
                                            (ip_hdr (sk_buffer))->check =
                                                u2CheckSum;
                                        }
                                    }
                                }
                                /* Set Status bit of Station to 1 */
                                rbData_Sta->staData.u1StationStatus = 1;
                                return NF_ACCEPT;
                            }
                            if (sk_buffer->len > 1442)
                            {
                                return NF_DROP;
                            }

                            if (ntohs (u2Ethertype) == ARP_ETHERTYPE)
                            {
                                return NF_ACCEPT;
                            }

                            if (u1Protocol == IPPROTO_ICMP)
                            {
                                return NF_DROP;
                            }

                            if (u1Protocol == IPPROTO_UDP)
                            {
                                ih = (struct iphdr *)
                                    skb_network_header (sk_buffer);
                                uh = (struct udphdr *) (sk_buffer->data +
                                                        (ih->ihl * IHL_SIZE));

                                u2Dstport = ntohs ((UINT2) uh->dest);
                                u2Srcport = ntohs ((UINT2) uh->source);

                                if ((u2Dstport == 0x43) || (u2Srcport == 0x44)
                                    || (u2Dstport == 0x35))
                                {
                                    return NF_ACCEPT;
                                }
                                else
                                {
                                    return NF_DROP;
                                }
                            }

                            if (u1Protocol == IPPROTO_TCP)
                            {
                                u4Hdrlen = (ip_hdr (sk_buffer))->tot_len;
                                iph =
                                    (struct iphdr *)
                                    skb_network_header (sk_buffer);

                                MEMCPY (&u2TcpPort, &(sk_buffer->data[22]), 2);

                                if (ntohs (u2TcpPort) == 80)
                                {
                                    /* Check whether the http packet is destined to WLC
                                       If it is so, send it to user space */

                                    if (u4DestIp != gu4WlcIpAddr)
                                    {
                                        u1Flags = sk_buffer->data[20 + 13];
                                        if (u1Flags == 0x14)
                                        {
                                            return NF_DROP;
                                        }
                                    }
                                    else
                                    {
                                        return NF_ACCEPT;
                                    }

                                }
                                else
                                {
                                    return NF_DROP;
                                }

                            }
                            else
                            {
                                return NF_DROP;

                            }
                        }
                        else
                        {
                            if (rbData_Sta->staData.u1WMMEnable == WMM_ENABLE)
                            {
                                u1WMMStatus = 1;
                                u1TrustModeStatus =
                                    rbData_Sta->staData.u1TrustMode;
                                u1QosProfile =
                                    rbData_Sta->staData.u1QosProfilePriority;
                                u1TaggingPolicy =
                                    rbData_Sta->staData.u1TaggingPolicy;
                                u2VlanId = rbData_Sta->staData.u2VlanId;

                                /*Assuming that the UP for 0  is mapped to skb_priority 256 */
                                u1UserPriority = (UINT1) sk_buffer->priority;
                            }
                            if ((u1WMMStatus == WMM_ENABLE)
                                && ((u1TaggingPolicy == TAG_POLICY_DSCP)
                                    || (u1TaggingPolicy == TAG_POLICY_BOTH)))
                            {
                                u2Ethertype = (eth_hdr (sk_buffer))->h_proto;

                                if (u1TrustModeStatus == TRUST_MODE)
                                {
                                    /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
                                    if ((ntohs (u2Ethertype) != 0x806)
                                        && ((ntohs (u2Ethertype) != 0x888e)))
                                    {
                                        u2OldCheck =
                                            (ip_hdr (sk_buffer))->check;
                                        (ip_hdr (sk_buffer))->check = 0;
                                        gu4Tos = (ip_hdr (sk_buffer))->tos;
                                        u2CheckSum =
                                            IpCalcCheckSum ((ip_hdr
                                                             (sk_buffer)), 20);
                                        (ip_hdr (sk_buffer))->check =
                                            u2CheckSum;
                                    }
                                }
                                else if (u1TrustModeStatus == NO_TRUST_MODE)
                                {

                                    if ((ntohs (u2Ethertype) != 0x806)
                                        && ((ntohs (u2Ethertype) != 0x888e)))
                                    {
                                        u2OldCheck =
                                            (ip_hdr (sk_buffer))->check;
                                        (ip_hdr (sk_buffer))->check = 0;
                                        /*FIX for the bug unknown DSCP */
                                        (ip_hdr (sk_buffer))->tos =
                                            (((ip_hdr (sk_buffer))->
                                              tos & 3) |
                                             (FcapwapGetDscpFromQosProfile
                                              (u1QosProfile) << 2));
                                        u2CheckSum =
                                            IpCalcCheckSum ((ip_hdr
                                                             (sk_buffer)), 20);
                                        (ip_hdr (sk_buffer))->check =
                                            u2CheckSum;
                                    }
                                }
                            }
                            /*Set Status bit of Sation to 1 */
                            rbData_Sta->staData.u1StationStatus = 1;
                            return NF_ACCEPT;
                        }

                    }
                    else
                    {
                        /*Set Status bit of Sation to 1 */
                        rbData_Sta->staData.u1StationStatus = 1;
                        /*return NF_ACCEPT; */
                    }
                }
                else
                {
                    PRINTF ("\nReceived PNAC packet....\n");
                }

                /*Set Status bit of Sation to 1 */
                rbData_Sta->staData.u1StationStatus = 1;

                {
                    skb_push (sk_buffer, 14);

                    /* Calculation of head room space for the CAPWAP header 
                       and the Outer Header */

                    /*get available headroom size from skb */
                    i4headroom = skb_headroom (sk_buffer);

                    /*calculate required headroom in skb for us */
                    i4Length =
                        TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET +
                        MAC_HDR_OFFSET;

                    i4Length += CAPWAP_HDR_LOCAL_SIZE;

                    if (i4headroom < i4Length)
                    {
                        new_skb =
                            skb_realloc_headroom (sk_buffer, i4Length + 18);
                        dev_kfree_skb (sk_buffer);

                        if (new_skb == NULL)
                        {
                            printk ("<1> skb_realloc_headroom failed\n");
                            return NF_DROP;
                        }
                        sk_buffer = new_skb;
                    }

                    /*re structure skb buffer */
                    skb_push (sk_buffer, i4Length);

                    skb_set_mac_header (sk_buffer, 0);
                    skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
                    skb_set_transport_header (sk_buffer,
                                              MAC_HDR_OFFSET +
                                              NETWORK_HDR_OFFSET);

                    FcapwapAddCapwapHeader (&sk_buffer->
                                            data[i4Length -
                                                 CAPWAP_HDR_LOCAL_SIZE],
                                            rbData_Sta->staData.bssid,
                                            rbData_Sta->staData.u1MacType);

                    dev = dev_get_by_name (&init_net, gau1WlcInterface);
                    if (dev != NULL)
                    {
                        if (dev->dev_addr != NULL)
                        {
                            out_dev = (struct in_device *) dev->ip_ptr;
                            if (out_dev != NULL)
                            {
                                for (if_info = out_dev->ifa_list;
                                     if_info != NULL;
                                     if_info = if_info->ifa_next)
                                {
                                    if (!
                                        (strcmp
                                         (if_info->ifa_label, dev->name)))
                                    {
                                        gu4WtpIpAddr =
                                            ntohl (if_info->ifa_address);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    skb_pull (sk_buffer, MAC_HDR_OFFSET);

                    if (sk_buffer->len > gi4CapwapPortMtu)
                    {
                        skb_push (sk_buffer, MAC_HDR_OFFSET);

                        if ((FcapwapWtpFragmentAndSend
                             (sk_buffer, gi4CapwapPortMtu,
                              &rbData_Sta->staData)) != SUCCESS)
                        {
                            return NF_DROP;
                        }
                        else
                        {
                            return NF_STOLEN;
                        }
                    }

                    skb_push (sk_buffer, MAC_HDR_OFFSET);

                    if ((FCapwapFormOuterHeader
                         (sk_buffer, &rbData_Sta->staData)) == FAIL)
                    {
                        return NF_DROP;
                    }

                    if (gu1LocalRoutingStatus != 1)
                    {
                        skb_set_mac_header (sk_buffer, 0);
                        skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
                        skb_set_transport_header (sk_buffer,
                                                  MAC_HDR_OFFSET +
                                                  NETWORK_HDR_OFFSET);

                        skb_pull (sk_buffer, MAC_HDR_OFFSET);

                        return NF_ACCEPT;
                    }
                    else
                    {
                        return NF_STOLEN;
                    }
                }

            }
            else
            {
                rbData_Sta = RbtTxSearchstaMac (pu1DstMac);
                if (rbData_Sta != NULL)
                {
#if DBG_LVL_01
                    PRINTF ("DEST STA MAC %02x:%02x:%02x:%02x:%02x:%02x !!!\n",
                            pu1DstMac[0], pu1DstMac[1], pu1DstMac[2],
                            pu1DstMac[3], pu1DstMac[4], pu1DstMac[5]);
#endif
                    rbData_Sta->staData.u1StationStatus = 1;
                }
            }
        }
    }
    else
    {
        rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);
        if (rbData_Sta != NULL)
        {
            u1Protocol = (ip_hdr (sk_buffer))->protocol;

            if (rbData_Sta->staData.u1WebAuthEnable == WEBAUTH_ENABLE)
            {
                if (rbData_Sta->staData.u1WebAuthStatus != STA_AUTHENTICATED)
                {
                    if (u1Protocol == IPPROTO_ICMP)
                    {
                        return NF_DROP;
                    }
                }
            }
        }
    }
    return NF_ACCEPT;

}

UINT4
FcapwapWtpBrHookPreRouting (UINT4 hooknum,
                            struct sk_buff *sk_buffer,
                            const struct net_device *in,
                            const struct net_device *out,
                            int (*okfn) (struct sk_buff *))
{
    UINT2               u2Ethertype = 0;
    tApSndStationTable *rbData_Sta;
    struct ethhdr      *eh = NULL;
    struct iphdr       *ih;
    struct net_device  *dev;
    struct in_device   *out_dev;
    struct in_ifaddr   *if_info;
    UINT4               u4Index = 0;
    UINT1              *pu1SrcMac = NULL;
    INT4                i4headroom = 0;
    INT4                i4Length = 0;
    UINT1               u1Protocol = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4DestIp = 0;
    struct udphdr      *uh = NULL;
    UINT2               u2Dstport = 0;
    UINT2               u2Srcport = 0;
    UINT4               u4Hdrlen = 0;
    struct iphdr       *iph = NULL;
    struct sk_buff     *new_skb = NULL;
    UINT2               u2TcpPort = 0;
    UINT1               u1Flags = 0;

    /*if (gu1LocalRoutingStatus == LOCAL_BRIDGE_ENABLE) */
    {
        pu1SrcMac = (eth_hdr (sk_buffer))->h_source;
        u2Ethertype = (eth_hdr (sk_buffer))->h_proto;

        if (ntohs (u2Ethertype) == PNAC_ETHERTYPE)
        {
            rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);
            if (rbData_Sta != NULL)
            {

                skb_push (sk_buffer, 14);

                /* Calculation of head room space for the CAPWAP header 
                   and the Outer Header */

                /*get available headroom size from skb */
                i4headroom = skb_headroom (sk_buffer);

                /*calculate required headroom in skb for us */
                i4Length =
                    TRANSPORT_HDR_SIZE + NETWORK_HDR_OFFSET + MAC_HDR_OFFSET;

                i4Length += CAPWAP_HDR_LOCAL_SIZE;

                if (i4headroom < i4Length)
                {
                    new_skb = skb_realloc_headroom (sk_buffer, i4Length + 18);
                    dev_kfree_skb (sk_buffer);

                    if (new_skb == NULL)
                    {
                        printk ("<1> skb_realloc_headroom failed\n");
                        return NF_DROP;
                    }
                    sk_buffer = new_skb;
                }

                /*re structure skb buffer */
                skb_push (sk_buffer, i4Length);

                skb_set_mac_header (sk_buffer, 0);
                skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
                skb_set_transport_header (sk_buffer,
                                          MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);

                FcapwapAddCapwapHeader (&sk_buffer->
                                        data[i4Length - CAPWAP_HDR_LOCAL_SIZE],
                                        rbData_Sta->staData.bssid,
                                        rbData_Sta->staData.u1MacType);

                dev = dev_get_by_name (&init_net, gau1WlcInterface);

                if (dev != NULL)
                {
                    if (dev->dev_addr != NULL)
                    {
                        out_dev = (struct in_device *) dev->ip_ptr;
                        if (out_dev != NULL)
                        {
                            for (if_info = out_dev->ifa_list; if_info != NULL;
                                 if_info = if_info->ifa_next)
                            {
                                if (!(strcmp (if_info->ifa_label, dev->name)))
                                {
                                    gu4WtpIpAddr = ntohl (if_info->ifa_address);
                                    break;
                                }
                            }
                        }
                    }
                }
#ifndef AP200_WANTED
                if ((FCapwapFormOuterHeader (sk_buffer, &rbData_Sta->staData))
                    == FAIL)
                {
                    return NF_DROP;
                }

                skb_set_mac_header (sk_buffer, 0);
                skb_set_network_header (sk_buffer, MAC_HDR_OFFSET);
                skb_set_transport_header (sk_buffer,
                                          MAC_HDR_OFFSET + NETWORK_HDR_OFFSET);

                skb_pull (sk_buffer, MAC_HDR_OFFSET);
#else
                if ((FCapwapFormOuterHeaderEap (sk_buffer)) == FAIL)
                {
                    return NF_DROP;
                }
                else
                {
                    return NF_STOLEN;
                }

#endif
            }
        }
        else
        {
            rbData_Sta = RbtTxSearchstaMac (pu1SrcMac);
            if (rbData_Sta != NULL)
            {
                u1Protocol = (ip_hdr (sk_buffer))->protocol;

                if (rbData_Sta->staData.u1WebAuthEnable == WEBAUTH_ENABLE)
                {
                    if (rbData_Sta->staData.u1WebAuthStatus !=
                        STA_AUTHENTICATED)
                    {
                        if (u1Protocol == IPPROTO_ICMP)
                        {
                            return NF_DROP;
                        }
                    }
                }
            }
            else
            {

                if (gu1LocalRoutingStatus == 2)
                {

                    if (ntohs (u2Ethertype) == 0x800)
                    {
                        ih = (struct iphdr *) ip_hdr (sk_buffer);

                        /* To get the protocol type */
                        u1Protocol = (ip_hdr (sk_buffer))->protocol;

                        /* To checkt the packet is UDP Packet */
                        if (u1Protocol == UDP)
                        {
                            uh = (struct udphdr *) (sk_buffer->data +
                                                    (ih->ihl * IHL_SIZE));

                            /* 
                             * Identify the received pkt protocol and for valid port number
                             * this can be also done in process_frame
                             */

                            if (ntohs (uh->source) == gu2WlcDataPort)
                            {
                                /* The below code changes (size 44) are done for LOCAL MAC. If split
                                 * MAC mode is supported then the below code needs to be modified accordingly
                                 */
                                skb_pull (sk_buffer, 44);
                                skb_set_mac_header (sk_buffer, 0);
                                skb_set_network_header (sk_buffer,
                                                        MAC_HDR_OFFSET);
                                skb_set_transport_header (sk_buffer,
                                                          MAC_HDR_OFFSET +
                                                          NETWORK_HDR_OFFSET);

                                u2Ethertype = (eth_hdr (sk_buffer))->h_proto;

                                if (ntohs (u2Ethertype) == 0x800)
                                {
                                    skb_pull (sk_buffer, 14);

                                    ih = (struct iphdr *) ip_hdr (sk_buffer);

                                    /* To get the protocol type */
                                    u1Protocol = (ip_hdr (sk_buffer))->protocol;

                                    /* To checkt the packet is UDP Packet */
                                    if (u1Protocol == UDP)
                                    {
                                        uh = (struct udphdr *) (sk_buffer->
                                                                data + 20);

                                        u2Dstport = ((UINT2) uh->dest);
                                        u2Srcport = ((UINT2) uh->source);

                                        if ((ntohs (u2Dstport) == DHCP_SRC_PORT)
                                            || (ntohs (u2Srcport) ==
                                                DHCP_DEST_PORT))
                                        {
                                            return NF_ACCEPT;
                                        }
                                    }
                                    skb_push (sk_buffer, MAC_HDR_OFFSET);
                                }
                                skb_push (sk_buffer, INNER_IP_HDR_OFFSET);
                                skb_set_mac_header (sk_buffer, 0);
                                skb_set_network_header (sk_buffer,
                                                        MAC_HDR_OFFSET);
                                skb_set_transport_header (sk_buffer,
                                                          MAC_HDR_OFFSET +
                                                          NETWORK_HDR_OFFSET);
                                skb_pull (sk_buffer, MAC_HDR_OFFSET);

                            }

                        }

                    }

                }
            }
        }
    }
    return NF_ACCEPT;
}

#ifdef AP200_WANTED
/******************************************************************************
 **  Function Name          : FCapwapFormOuterHeaderEap
 **  Description            : This function is used to add capwap header over
 **                           the EAP packets in pre-routing hook and transmit
 **                           it via WLC interface.
 **  Input(s)               : sk_buffer - packet data
 **  Output(s)              : None
 **  Returns                : SUCCESS/FAILURE
 *********************************************************************************/
INT4
FCapwapFormOuterHeaderEap (struct sk_buff *sk_buffer)
{
    struct iphdr       *iph;
    struct iphdr       *iph1;
    struct ethhdr      *eh;
    struct vlan_ethhdr *vlan = NULL;
    struct udphdr      *udph;
    struct net_device  *dev;
    UINT2               u2UdpLen = 0;
    UINT2               u2IpLen = 0;
    INT4                i4Error = 0;
    UINT4               u4Index = 0;

    /*Frame UDP Header */
    u2UdpLen = (UINT4) (sk_buffer->len - (MAC_HDR_OFFSET + NETWORK_HDR_OFFSET));
    /* Frame udp header */
    udph = udp_hdr (sk_buffer);
    if (udph != NULL)
    {
        udph->source = htons (gu2WtpDataPort);
        udph->dest = htons (gu2WlcDataPort);
        udph->len = htons (u2UdpLen);
        udph->check = 0;
        udph->check = csum_tcpudp_magic ((htonl (gu4WtpIpAddr)),
                                         (htonl (gu4WlcIpAddr)), u2UdpLen,
                                         IPPROTO_UDP, csum_partial (udph,
                                                                    u2UdpLen,
                                                                    0));
        if (udph->check == 0)
            udph->check = CSUM_MANGLED_0;
    }
    else
    {
        printk ("udph is nulll!!\n");
        return FAIL;
    }

    /* Frame ip header */
    u2IpLen =
        (UINT4) (sk_buffer->len -
                 (sk_buffer->network_header - sk_buffer->mac_header));
    iph = ip_hdr (sk_buffer);
    iph->version = IP_VERSION;
    iph->ihl = IP_HDR_LENGTH;
    iph->tos = 0;
#if DBG_LVL_01
    printk ("packets are sending via bronzee \n");
#endif
    iph->tot_len = htons (u2IpLen);
    iph->id = htons (0);
    iph->frag_off = 0;
    iph->ttl = IP_TTL_VALUE;
    iph->protocol = IPPROTO_UDP;
    iph->check = 0;

    iph->saddr = htonl (gu4WtpIpAddr);
    iph->daddr = htonl (gu4WlcIpAddr);
    iph->check = ip_fast_csum ((UINT1 *) iph, iph->ihl);

    /* Frame Ethernet header */
    eh = eth_hdr (sk_buffer);

    if (eh == NULL)
    {
        printk (KERN_ERR "Error : Ethernet Header\n");
        return FAIL;
    }
    dev = dev_get_by_name (&init_net, gau1WlcInterface);
/* PRINTF("\n gau1WlcInterface %s ",gau1WlcInterface); */
    if (dev == NULL)
    {
        printk (KERN_ERR "Error : Ethernet Device\n");
        return FAIL;
    }

    memcpy (eh->h_source, dev->dev_addr, ETH_ALEN);
    memcpy (eh->h_dest, gau1WlcMac, ETH_ALEN);
    eh->h_proto = htons (ETH_P_IP);
#if DBG_LVL_01
    for (u4Index = 0; u4Index < sk_buffer->len; u4Index++)
    {
        if (!(u4Index % 16))
            PRINTF ("\n");

        PRINTF ("%2x ", sk_buffer->data[u4Index]);
    }
    PRINTF ("\n");
#endif
    /* Setting it as outgoing packet */
    sk_buffer->pkt_type = PACKET_OUTGOING;
    /* changing the dev to output device we need */
    sk_buffer->dev = dev;

    i4Error = dev_queue_xmit (sk_buffer);
    i4Error = net_xmit_eval (i4Error);

    if (i4Error)
    {
        printk (KERN_ERR "sendToStation: data transmission failed\n");
    }
    /*give dev back */
    dev_put (dev);

    return SUCCESS;
}
#endif

/******************************************************************************
 *  Function Name          : init_module
 *  Description            : This function is used to initialize the driver
 *  Input(s)               : void
 *  Output(s)              : initializes the driver module
 *  Returns                : SUCCESS/ERROR
 * ******************************************************************************/
int
FcapwapWtpDrvBrTxInit (void)
{
    INT4                i4RetVal = 0;

#ifndef QORIQ_WANTED
    printk (">>>>>>.Register Bridge Post-Routing hook\n");

    nfho_post.hook = FcapwapWtpBrHookPostRouting;
    nfho_post.owner = THIS_MODULE;
    nfho_post.hooknum = NF_BR_POST_ROUTING;    /* POST Roiting hook for Bridge */
    nfho_post.pf = PF_BRIDGE;
    nfho_post.priority = NF_BR_PRI_BRNF;    /* Make our function first */
    i4RetVal = nf_register_hook (&nfho_post);

    if (i4RetVal != 0)
    {
        PRINTF ("Bridge Post-Routing hook registration failed\n");
    }
    nfho_pre.hook = FcapwapWtpBrHookPreRouting;
    nfho_pre.owner = THIS_MODULE;
    nfho_pre.hooknum = NF_BR_PRE_ROUTING;    /* POST Roiting hook for Bridge */
    nfho_pre.pf = PF_BRIDGE;
    nfho_pre.priority = NF_BR_PRI_BRNF;    /* Make our function first */

    nf_register_hook (&nfho_pre);

    if (i4RetVal != 0)
    {
        PRINTF ("Bridge Pre-Routing hook registration failed\n");
    }
#endif
    return i4RetVal;

}

/******************************************************************************
 **  Function Name          : cleanup_module
 **  Description            : This function is used to un-initialize the driver
 **  Input(s)               : void
 **  Output(s)              : un-initializes the driver module
 **  Returns                : void
 *********************************************************************************/

void
FcapwapWtpDrvBrTxExit (void)
{
#ifndef QORIQ_WANTED
    nf_unregister_hook (&nfho_pre);
    nf_unregister_hook (&nfho_post);
#endif
}

INT4
FcapwapWMM (struct sk_buff *psk_buffer, tApSndStationTable * rbData_Sta)
{
    UINT1               u1WMMStatus = 0;
    UINT1               u1TrustModeStatus = 0;
    UINT1               u1QosProfile = 0;
    UINT1               u1UserPriority = 0;
    UINT1               u1TaggingPolicy = 0;
    UINT2               u2Ethertype = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2CheckSum = 0;
    UINT2               u2OldCheck = 0;
    if (rbData_Sta->staData.u1WMMEnable == WMM_ENABLE)
    {
        u1WMMStatus = 1;
        u1TrustModeStatus = rbData_Sta->staData.u1TrustMode;
        u1QosProfile = rbData_Sta->staData.u1QosProfilePriority;
        u1TaggingPolicy = rbData_Sta->staData.u1TaggingPolicy;
        u2VlanId = rbData_Sta->staData.u2VlanId;

        /*Assuming that the UP for 0  is mapped to skb_priority 256 */
        u1UserPriority = (UINT1) psk_buffer->priority;
    }
    if ((u1WMMStatus == WMM_ENABLE) && ((u1TaggingPolicy == TAG_POLICY_DSCP) ||
                                        (u1TaggingPolicy == TAG_POLICY_BOTH)))
    {
        u2Ethertype = (eth_hdr (psk_buffer))->h_proto;

        if (u1TrustModeStatus == TRUST_MODE)
        {
            /*Assuming that dscp value consists of both dscp 6 bits and ECN 2 bits */
            if ((ntohs (u2Ethertype) != 0x806)
                && ((ntohs (u2Ethertype) != 0x888e)))
            {
                u2OldCheck = (ip_hdr (psk_buffer))->check;
                (ip_hdr (psk_buffer))->check = 0;
                gu4Tos = (ip_hdr (psk_buffer))->tos;
                u2CheckSum = IpCalcCheckSum ((ip_hdr (psk_buffer)), 20);
                (ip_hdr (psk_buffer))->check = u2CheckSum;
            }
        }
        else if (u1TrustModeStatus == NO_TRUST_MODE)
        {

            if ((ntohs (u2Ethertype) != 0x806)
                && ((ntohs (u2Ethertype) != 0x888e)))
            {
                u2OldCheck = (ip_hdr (psk_buffer))->check;
                (ip_hdr (psk_buffer))->check = 0;
                /*FIX for the bug unknown DSCP */
                (ip_hdr (psk_buffer))->tos = (((ip_hdr (psk_buffer))->tos & 3)
                                              |
                                              (FcapwapGetDscpFromQosProfile
                                               (u1QosProfile) << 2));
                u2CheckSum = IpCalcCheckSum ((ip_hdr (psk_buffer)), 20);
                (ip_hdr (psk_buffer))->check = u2CheckSum;
            }
        }
    }

}
#endif
