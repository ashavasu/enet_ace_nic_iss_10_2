
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcaptxdb.c,v 1.5 2018/02/20 10:52:54 siva Exp $
 *
 * Description: This file contains DB related API for SND module in AP
 *******************************************************************/
#ifndef _FCAP_TXDB_C_
#define _FCAP_TXDB_C_

/******************************************************************************
 *                             Includes
 *******************************************************************************/
#include "fckstxdb.h"

#define   UNUSED_PARAM(x)   ((void)x)

/* Initialize root node */
struct rb_root      root_sta = RB_ROOT;

/* Initialize mempool variable */
static mempool_t   *memPool_Sta = NULL;

extern INT4         gu1LocalRoutingStatus;
extern INT4         gi4MacType;
extern INT4         gu2WtpDataPort;
extern INT4         gu2WlcDataPort;
extern INT4         gu4WlcIpAddr;
extern INT4         gu4WtpIpAddr;
extern INT4         gu4IfIndex;
extern INT4         gu2Vlan;
extern UINT1        gau1NextHopMac[6];
extern UINT1        gau1WlcMac[6];
extern UINT1        gau1WlcInterface[INTERFACE_LEN];
extern INT4         gi4CapwapPortMtu;

/******************************************************************************
 *  Function Name          : rbt_alloc_Sta
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
rbt_alloc_Sta (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tApSndStationTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
rbt_dealloc_Sta (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_search_Sta
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : struct rb_root *root, UINT1 *findData, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
static tApSndStationTable *
rbt_search_Sta (struct rb_root *root, char *StaMac)
{
    struct rb_node     *rbnode = root->rb_node;
    int                 result = 0;

    while (rbnode)
    {
        tApSndStationTable *data =
            container_of (rbnode, tApSndStationTable, node);

        result = memcmp (StaMac, data->staData.staMac, 6);

        if (result < 0)
            rbnode = rbnode->rb_left;
        else if (result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : RbtTxSearchstaMac
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : struct rb_root *root, UINT1 *findData
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tApSndStationTable *
RbtTxSearchstaMac (UINT1 *findData)
{
    tApSndStationTable *rbData_Sta;

    rbData_Sta = rbt_search_Sta (&root_sta, findData);
    if (rbData_Sta != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "rbt_search_macType: Search Success\n");
#endif
        return rbData_Sta;
    }
    else
    {
#if DBG_LVL_01
        printk (KERN_ERR "rbt_search_macType: Search Failure\n");
#endif
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : rbInsertSta
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
rbInsertSta (struct rb_root *root_sta, tRBTreeType * data)
{
    int                 result = 0;
    struct rb_node    **new = &root_sta->rb_node, *parent = NULL;
    tApSndStationTable *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Sta, GFP_ATOMIC);
    if (numNodes == NULL)
    {
#if DBG_LVL_01
        printk (KERN_ERR "\n Error in allocating node \n");
#endif
        return FAIL;
    }
    else
    {
        numNodes->staData = data->staData;

        /* To Figure out where to put new node */
        while (*new)
        {
            tApSndStationTable *this =
                container_of (*new, tApSndStationTable, node);

            result = memcmp (numNodes->staData.staMac, this->staData.staMac, 6);

            parent = *new;
            if (result < 0)
                new = &((*new)->rb_left);
            else if (result > 0)
                new = &((*new)->rb_right);
            else
            {
                this->staData.u1MacType = numNodes->staData.u1MacType;
                this->staData.u2VlanId = numNodes->staData.u2VlanId;
                memcpy (this->staData.bssid, numNodes->staData.bssid,
                        MAC_ADDR_LEN);
                this->staData.u1StationStatus =
                    numNodes->staData.u1StationStatus;
                this->staData.u1WebAuthEnable =
                    numNodes->staData.u1WebAuthEnable;
                this->staData.u1ExternalWebAuth =
                    numNodes->staData.u1ExternalWebAuth;
                this->staData.u1WebAuthStatus =
                    numNodes->staData.u1WebAuthStatus;
                this->staData.u1WMMEnable = numNodes->staData.u1WMMEnable;
                this->staData.u2TcpPort = numNodes->staData.u2TcpPort;
                this->staData.u1TaggingPolicy =
                    numNodes->staData.u1TaggingPolicy;
                this->staData.u1TrustMode = numNodes->staData.u1TrustMode;
                this->staData.u1QosProfilePriority =
                    numNodes->staData.u1QosProfilePriority;
                memcpy (this->staData.wlanId, numNodes->staData.wlanId, 24);
                mempool_free (numNodes, memPool_Sta);
                return SUCCESS;
            }
        }

        printk ("\nInsert STA Mac - %x:%x:%x:%x:%x:%x\n",
                numNodes->staData.staMac[0], numNodes->staData.staMac[1],
                numNodes->staData.staMac[2], numNodes->staData.staMac[3],
                numNodes->staData.staMac[4], numNodes->staData.staMac[5]);
        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root_sta);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : RbtDisplay
 *  Description            : This function is used to display RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/
void
RbtDisplay (int i4TableNum)
{
    struct rb_node     *node;
    UINT4               u4Index = 0;

    printk (KERN_INFO "\n Displaying Data SND MODULE.....\n");

    switch (i4TableNum)
    {
        case AP_WTP_TABLE:
            printk (KERN_INFO "WLC Data Port = %d", gu2WlcDataPort);
            printk (KERN_INFO "WTP Data Port = %d", gu2WtpDataPort);
            printk (KERN_INFO "WLC IP Address = 0x%x", gu4WlcIpAddr);
            printk (KERN_INFO "WLC MAC Address = %x:%x:%x:%x:%x:%x",
                    gau1WlcMac[0], gau1WlcMac[1], gau1WlcMac[2],
                    gau1WlcMac[3], gau1WlcMac[4], gau1WlcMac[5]);
            printk (KERN_INFO "Local Routing = %d", gu1LocalRoutingStatus);
            printk (KERN_INFO "Interface  = %s", gau1WlcInterface);
            printk (KERN_INFO "Capwap Port MTU  = %d", gi4CapwapPortMtu);
            break;

        case AP_STA_TABLE:
            node = rb_first (&root_sta);
#ifdef DEBUG_WANTED
            for (; node; node = rb_next (node))
            {
                printk (KERN_INFO "\n\n");
                for (u4Index = 0; u4Index < 6; u4Index++)
                    printk (KERN_INFO "staMac[%d]= %x\n",
                            u4Index, rb_entry (node, tApSndStationTable,
                                               node)->staData.staMac[u4Index]);
                for (u4Index = 0; u4Index < 6; u4Index++)
                    printk (KERN_INFO "bssid[%d] = %x\n",
                            u4Index, rb_entry (node, tApSndStationTable,
                                               node)->staData.bssid[u4Index]);
                printk (KERN_INFO "interface = %s\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.wlanId);
                printk (KERN_INFO "mac type = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1MacType);
                printk (KERN_INFO "vlan = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u2VlanId);
                printk (KERN_INFO "web auth enable = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1WebAuthEnable);
                printk (KERN_INFO "webauth status = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1WebAuthStatus);
                printk (KERN_INFO "WMMEnable  = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1WMMEnable);
                printk (KERN_INFO "TaggingPolicy = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1TaggingPolicy);
                printk (KERN_INFO "TrustMode = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1TrustMode);
                printk (KERN_INFO "QosProfilePriority = %d\n",
                        rb_entry (node, tApSndStationTable,
                                  node)->staData.u1QosProfilePriority);
            }
#endif
            break;
        default:
            printk (KERN_ERR "rbt_search: switch reached default\n");
            break;
    }
    return;
}

/******************************************************************************
 *  Function Name          : RbtTxDeleteStaMac
 *  Description            : This function is used to delete RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : search string
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
int
RbtTxDeleteStaMac (UINT1 *search)
{
    tApSndStationTable *data = NULL;

    data = rbt_search_Sta (&root_sta, search);

    if (data != NULL)
    {
        printk (KERN_INFO "\n Delete STA MAC %2x:%2x:%2x:%2x:%2x:%2x\n",
                search[0], search[1], search[2],
                search[3], search[4], search[5]);

        /* To delete the data */
        rb_erase (&data->node, &root_sta);
        mempool_free (&data->node, memPool_Sta);
        RbtDisplay (AP_STA_TABLE);    /* PS */
        return SUCCESS;
    }
    else
    {
        return FAIL;
    }
}

/******************************************************************************
 *  Function Name          : RbtReplace_sta
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : UINT1 *findData,struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
int
RbtTxReplaceStaMac (UINT1 *findData, tRBTreeType * rbData)
{
    tApSndStationTable *data = NULL;
    tApSndStationTable *replaceData = NULL;

    /* To check the memory allocation for the node to be inserted */
    replaceData = mempool_alloc (memPool_Sta, GFP_ATOMIC);
    if (replaceData == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }

    replaceData->staData = rbData->staData;
    /* To search the data to be replaced */
    data = rbt_search_Sta (&root_sta, findData);
    if (data != NULL)
    {
        /* To replace the data */
        rb_replace_node (&data->node, &replaceData->node, &root_sta);
        return SUCCESS;
    }
    else
    {
        mempool_free (replaceData, memPool_Sta);
        printk (KERN_ERR "Error: no data to replace \n");
        return FAIL;
    }
}

/******************************************************************************
 *  Function Name          : RbtInsert
 *  Description            : This function is used to insert RB tree data  
 *                           via ioctl of wtp driver, with root as reference
 *  Input(s)               : struct rbtree_type *rbdata
 *  Output(s)              : inserted node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
int
RbtTxInsertSta (tRBTreeType * rbdata)
{
    if (rbInsertSta (&root_sta, rbdata) != SUCCESS)
    {
        printk (KERN_INFO "rBinsert: Insert Failure\n");
        return FAIL;
    }
    else
    {
#if DBG_LVL_01
        printk (KERN_ERR "rBinsert: Insert Success\n");
#endif
        return SUCCESS;
    }

}

/******************************************************************************
 *  Function Name          : RbtTxCreateSta
 *  Description            : This function is used to create RB tree memory pool 
 *                           via ioctl of wtp driver with nodes as input
 *  Input(s)               : int num_nodes
 *  Output(s)              : mempool with reference. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
int
RbtTxCreateSta (int num_nodes)
{
    /* To create the memory pool */
    memPool_Sta =
        mempool_create (num_nodes, rbt_alloc_Sta, rbt_dealloc_Sta, NULL);
    if (memPool_Sta == NULL)
    {
        printk (KERN_ERR "\n Error in creating memory pool for rbtree sta\n");
        return FAIL;
    }
    printk ("\nMEMPOOL CREATED\n");
    return RB_SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroy_Sta (void)
{
    struct rb_node     *node;

    for (node = rb_first (&root_sta); node; node = rb_next (node))
    {
        RbtTxDeleteStaMac (rb_entry (node, tApSndStationTable, node)->staData.
                           staMac);
    }
    /* To delete the memory pool */
    mempool_destroy (memPool_Sta);    /*Connfirm whether to do this or not */
}

#endif
