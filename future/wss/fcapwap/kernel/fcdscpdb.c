
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcdscpdb.c,v 1.2 2017/05/23 14:16:51 siva Exp $
 *
 * Description: This file contains DB related API for SND module in WLC
 *******************************************************************/
#ifndef _FCAC_TXDB_C_
#define _FCAC_TXDB_C_
/******************************************************************************
 *                             Includes
 *******************************************************************************/
#include "fckstxdb.h"
#include "fcksprot.h"

/* Initialize root node */
struct rb_root      root_qos = RB_ROOT;

/* Initialize mempool variable */
static mempool_t   *memPool_qos = NULL;

UINT4               gu4DscpTrace;

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
rbt_alloc_qos (gfp_t gfp_mask, void *pool_data)
{
    UNUSED_PARAM (pool_data);
    return kmalloc (sizeof (tDscpQosTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
rbt_dealloc_qos (void *element, void *pool_data)
{
    UNUSED_PARAM (pool_data);
    kfree (element);
}

/******************************************************************************
 *  Function Name          : RbtCreate
 *  Description            : This function is used to create RB tree memory pool 
 *                           via ioctl of wtp driver with nodes as input
 *  Input(s)               : int i4Nodes
 *  Output(s)              : mempool with reference. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtCreate (INT4 i4Nodes)
{
    /*To create memory pool for qos table */
    memPool_qos =
        mempool_create (i4Nodes, rbt_alloc_qos, rbt_dealloc_qos, NULL);
    if (memPool_qos == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree\n");
        return FAIL;
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyQos (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_qos);
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : struct rb_root *root, unsigned char *findData, int
 *                           i4TableNum
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 * ******************************************************************************/
tDscpQosTable      *
RbtSearchQos (struct rb_root *root, UINT1 *au1InterfaceName, UINT1 u1dscpin)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                result = 0;
    UINT1               u1Len1 = 0;
    UINT1               u1Len2 = 0;

    while (rbnode)
    {
        tDscpQosTable      *data = container_of (rbnode, tDscpQosTable, node);
        if (data != NULL)
        {
            u1Len1 = strlen (au1InterfaceName);
            u1Len2 = strlen (data->qosData.au1InterfaceName);
            result =
                memcmp (au1InterfaceName, data->qosData.au1InterfaceName,
                        INTERFACE_LEN);

            if (result < 0)
                rbnode = rbnode->rb_left;
            else if (result > 0)
                rbnode = rbnode->rb_right;
            else if (u1dscpin < data->qosData.u1dscpin)
                rbnode = rbnode->rb_left;
            else if (u1dscpin > data->qosData.u1dscpin)
                rbnode = rbnode->rb_right;
            else
            {
                return data;
            }
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search_qos
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : struct rb_root *root, unsigned char *findData
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 * ******************************************************************************/
tDscpQosTable      *
DscpQosTableSearch (UINT1 *findInterfaceName, UINT1 u1dscpin)
{
    tDscpQosTable      *rbData_qos;

    rbData_qos = RbtSearchQos (&root_qos, findInterfaceName, u1dscpin);
    if (rbData_qos != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "RbtSearchQos: Search Success\n");
#endif
        return rbData_qos;
    }
    else
    {
#if DBG_LVL_01
        printk (KERN_ERR "RbtSearchQos: Search Failure\n");
#endif
    }

    return NULL;
}

#ifdef WLC_WANTED
/******************************************************************************
 *  Function Name          : RbInsertQos
 *  Description            : This function is used to insert RB tree data
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node.
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
RbInsertQos (struct rb_root *root, tRBtree * data)
{
    INT4                result = 0;    /* PS */
    UINT1               u1Len1 = 0;
    UINT1               u1Len2 = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tDscpQosTable      *numNodes = NULL;

    if (data == NULL)
    {
        printk (KERN_ERR "\n Data null \n");
        return FAIL;
    }
    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_qos, GFP_KERNEL);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->qosData = data->unTable.QosData;

        /* To Figure out where to put new node */
        while (*new)
        {
            tDscpQosTable      *this = container_of (*new, tDscpQosTable, node);
            if (this == NULL)
            {
                printk (KERN_ERR "\n this null \n");
                return FAIL;
            }
            u1Len1 = strlen (numNodes->qosData.au1InterfaceName);
            u1Len2 = strlen (this->qosData.au1InterfaceName);
            result =
                memcmp (numNodes->qosData.au1InterfaceName,
                        this->qosData.au1InterfaceName, INTERFACE_LEN);
            parent = *new;
            if (result < 0)
                new = &((*new)->rb_left);
            else if (result > 0)
                new = &((*new)->rb_right);
            else if (result == 0)
            {
                if (numNodes->qosData.u1dscpin > this->qosData.u1dscpin)
                    new = &((*new)->rb_right);
                else if (numNodes->qosData.u1dscpin < this->qosData.u1dscpin)
                    new = &((*new)->rb_left);
                else
                {
                    this->qosData.u1dscpout = numNodes->qosData.u1dscpout;
                    return SUCCESS;
                }
            }
        }
#if DBG_LVL_01
        printk (KERN_INFO "\nInsert new qos data\n");
#endif
        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : RbtDisplay
 *  Description            : This function is used to display RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/
void
RbtDisplay (int i4TableNum)
{
    struct rb_root     *root = NULL;
    struct rb_node     *node;

    printk (KERN_INFO "\n DSCP MODULE Displaying Data.....\n");

    switch (i4TableNum)
    {
        case AP_QOS_TABLE:
            root = &root_qos;
            break;
        default:
            printk (KERN_ERR "rbt_search: switch reached default\n");
            return;
            /* break;                */
    }
    /* to iteratrate the tree */
//    for (node = rb_first (root); node; node = rb_next (node))
    {
        switch (i4TableNum)
        {
            case AP_QOS_TABLE:
                root = &root_qos;
                printk (KERN_INFO "\n\n");

                for (node = rb_first (root); node; node = rb_next (node))
                {
                    printk (KERN_INFO "interface name = %s\n",
                            rb_entry (node, tDscpQosTable,
                                      node)->qosData.au1InterfaceName);
                    printk (KERN_INFO "ipdscpin = %d\n",
                            rb_entry (node, tDscpQosTable,
                                      node)->qosData.u1dscpin);
                    printk (KERN_INFO "ipdscpout = %d\n",
                            rb_entry (node, tDscpQosTable,
                                      node)->qosData.u1dscpout);
                }
                break;

            default:
                printk (KERN_ERR "rbt_search: switch reached default\n");
                break;
        }
    }
    return;
}

/******************************************************************************
 *  Function Name          : RbtDeleteQos
 *  Description            : This function is used to delete RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : search string
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
RbtDeleteQos (tRBtree * rbData)
{
    tDscpQosTable      *data = NULL;

    data = RbtSearchQos (&root_qos, rbData->unTable.QosData.au1InterfaceName,
                         rbData->unTable.QosData.u1dscpin);

    if (data != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "\n data is found\n");
#endif
        /* To delete the data */
        if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
            DSCP_DEBUG_CONFIGURAION)
        {
            printk
                ("Deleted entry of interface %s and indscp %d to replace with outdscp %d\r\n",
                 rbData->unTable.QosData.au1InterfaceName,
                 rbData->unTable.QosData.u1dscpin,
                 rbData->unTable.QosData.u1dscpout);
        }
        rb_erase (&data->node, &root_qos);
        mempool_free (&data->node, memPool_qos);
        return SUCCESS;
    }
    else
    {
        printk (KERN_ERR "\n data is not found");
        if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
            DSCP_DEBUG_CONFIGURAION)
        {
            printk ("Deleting entry for interface %s and indscp %d failed\r\n",
                    rbData->unTable.QosData.au1InterfaceName,
                    rbData->unTable.QosData.u1dscpin);
        }
        return FAIL;
    }
}

/******************************************************************************
 *  Function Name          : RbtRemove
 *  Description            : This function is used to remove the RbData
 *  Input(s)               : rbData - Data Structure to remove
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtRemove (tRBtree * rbData, INT4 i4TableNumber)
{
    INT4                i4RetVal = 0;
    switch (i4TableNumber)
    {
        case AP_QOS_TABLE:
            i4RetVal = RbtDeleteQos (rbData);
            break;
        default:
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return i4RetVal;
}
#else
/******************************************************************************
 *  Function Name          : RbInsertQos
 *  Description            : This function is used to insert RB tree data
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node.
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
RbInsertQos (struct rb_root *root, tRBTreeType * data)
{
    INT4                result = 0;    /* PS */
    UINT1               u1Len1 = 0;
    UINT1               u1Len2 = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tDscpQosTable      *numNodes = NULL;

    if (data == NULL)
    {
        printk (KERN_ERR "\n Data null \n");
        return FAIL;
    }
    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_qos, GFP_KERNEL);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->qosData = data->QosData;

        /* To Figure out where to put new node */
        while (*new)
        {
            tDscpQosTable      *this = container_of (*new, tDscpQosTable, node);
            if (this == NULL)
            {
                printk (KERN_ERR "\n this null \n");
                return FAIL;
            }
            u1Len1 = strlen (numNodes->qosData.au1InterfaceName);
            u1Len2 = strlen (this->qosData.au1InterfaceName);
            result =
                memcmp (numNodes->qosData.au1InterfaceName,
                        this->qosData.au1InterfaceName, INTERFACE_LEN);
            parent = *new;
            if (result < 0)
                new = &((*new)->rb_left);
            else if (result > 0)
                new = &((*new)->rb_right);
            else if (result == 0)
            {
                if (numNodes->qosData.u1dscpin > this->qosData.u1dscpin)
                    new = &((*new)->rb_right);
                else if (numNodes->qosData.u1dscpin < this->qosData.u1dscpin)
                    new = &((*new)->rb_left);
                else
                {
                    this->qosData.u1dscpout = numNodes->qosData.u1dscpout;
                    return SUCCESS;
                }
            }
        }
#if DBG_LVL_01
        printk (KERN_INFO "\nInsert new qos data\n");
        printk ("Adding entry for %s %d %d\n",
                numNodes->qosData.au1InterfaceName,
                numNodes->qosData.u1dscpin, numNodes->qosData.u1dscpout);
#endif
        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : RbtDisplay
 *  Description            : This function is used to display RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/
void
RbtDisplay (int i4TableNum)
{
    struct rb_root     *root = NULL;
    struct rb_node     *node;

    printk (KERN_INFO "\n DSCP MODULE Displaying Data.....\n");

    switch (i4TableNum)
    {
        case AP_QOS_TABLE:
            root = &root_qos;
            break;
        default:
            printk (KERN_ERR "rbt_search: switch reached default\n");
            return;
            /* break;                */
    }
    /* to iteratrate the tree */
//    for (node = rb_first (root); node; node = rb_next (node))
    {
        switch (i4TableNum)
        {
            case AP_QOS_TABLE:
                root = &root_qos;
                printk (KERN_INFO "\n\n");

                for (node = rb_first (root); node; node = rb_next (node))
                {
                    printk (KERN_INFO "interface name = %s\n",
                            rb_entry (node, tDscpQosTable,
                                      node)->qosData.au1InterfaceName);
                    printk (KERN_INFO "ipdscpin = %d\n",
                            rb_entry (node, tDscpQosTable,
                                      node)->qosData.u1dscpin);
                    printk (KERN_INFO "ipdscpout = %d\n",
                            rb_entry (node, tDscpQosTable,
                                      node)->qosData.u1dscpout);
                }
                break;

            default:
                printk (KERN_ERR "rbt_search: switch reached default\n");
                break;
        }
    }
    return;
}

/******************************************************************************
 *  Function Name          : RbtDeleteQos
 *  Description            : This function is used to delete RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : search string
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
RbtDeleteQos (tRBTreeType * rbData)
{
    tDscpQosTable      *data = NULL;

    data = RbtSearchQos (&root_qos, rbData->QosData.au1InterfaceName,
                         rbData->QosData.u1dscpin);

    if (data != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "\n data is found\n");
#endif
        /* To delete the data */
        if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
            DSCP_DEBUG_CONFIGURAION)
        {
            printk
                ("Deleted entry of interface %s and indscp %d to replace with outdscp %d\r\n",
                 rbData->QosData.au1InterfaceName, rbData->QosData.u1dscpin,
                 rbData->QosData.u1dscpout);
        }
        rb_erase (&data->node, &root_qos);
        mempool_free (&data->node, memPool_qos);
        return SUCCESS;
    }
    else
    {
        printk (KERN_ERR "\n data is not found");
        if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
            DSCP_DEBUG_CONFIGURAION)
        {
            printk ("Deleting entry for interface %s and indscp %d failed\r\n",
                    rbData->QosData.au1InterfaceName, rbData->QosData.u1dscpin);
        }
        return FAIL;
    }
}

/******************************************************************************
 *  Function Name          : RbtRemove
 *  Description            : This function is used to remove the RbData
 *  Input(s)               : rbData - Data Structure to remove
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtRemoveDscp (tRBTreeType * rbData, INT4 i4TableNumber)
{
    INT4                i4RetVal = 0;
    switch (i4TableNumber)
    {
        case AP_QOS_TABLE:
            i4RetVal = RbtDeleteQos (rbData);
            break;
        default:
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return i4RetVal;
}
#endif

#ifdef WLC_WANTED
/******************************************************************************
 *  Function Name          : RbtInsert
 *  Description            : This function is used to insert in the RBTree
 *  Input(s)               : rbData - Data Structure to be inserted
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtInsert (tRBtree * rbdata, INT4 i4TableNumber)
{
    switch (i4TableNumber)
    {
        case AP_QOS_TABLE:
            if (RbInsertQos (&root_qos, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Qos Table Insert Failure\n");
                if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
                    DSCP_DEBUG_CONFIGURAION)
                {
                    printk
                        ("Adding entry for interface %s and indscp %d failed\r\n",
                         rbdata->unTable.QosData.au1InterfaceName,
                         rbdata->unTable.QosData.u1dscpin);
                }
                return FAIL;
            }
            if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
                DSCP_DEBUG_CONFIGURAION)
            {
                printk
                    ("Added entry for interface %s and indscp %d to replace with outdscp %d\r\n",
                     rbdata->unTable.QosData.au1InterfaceName,
                     rbdata->unTable.QosData.u1dscpin,
                     rbdata->unTable.QosData.u1dscpout);
            }
            break;

        case DEBUG_INFO:
            gu4DscpTrace = (UINT4) (rbdata->i4WssDataDebugMask);
            break;

        default:
            printk (KERN_INFO "\nInvalid Case\n");
            break;
    }
    return SUCCESS;
}
#else
/******************************************************************************
 *  Function Name          : RbtInsert
 *  Description            : This function is used to insert in the RBTree
 *  Input(s)               : rbData - Data Structure to be inserted
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtInsertDscp (tRBTreeType * rbdata, INT4 i4TableNumber)
{
    switch (i4TableNumber)
    {
        case AP_QOS_TABLE:
            if (RbInsertQos (&root_qos, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Qos Table Insert Failure\n");
                if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
                    DSCP_DEBUG_CONFIGURAION)
                {
                    printk
                        ("Adding entry for interface %s and indscp %d failed\r\n",
                         rbdata->QosData.au1InterfaceName,
                         rbdata->QosData.u1dscpin);
                }
                return FAIL;
            }
            if (((gu4DscpTrace) & (DSCP_DEBUG_CONFIGURAION)) ==
                DSCP_DEBUG_CONFIGURAION)
            {
                printk
                    ("Added entry for interface %s and indscp %d to replace with outdscp %d\r\n",
                     rbdata->QosData.au1InterfaceName, rbdata->QosData.u1dscpin,
                     rbdata->QosData.u1dscpout);
            }
            break;

        default:
            printk (KERN_INFO "\nInvalid Case\n");
            break;
    }
    return SUCCESS;
}
#endif

/******************************************************************************
 *  Function Name          : RbtTxDestroy
 *  Description            : This function is used to destroy the RBTree
 *  Input(s)               : i4TableNumber - Table to be removed
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtTxDestroy (INT4 i4TableNumber)
{
    switch (i4TableNumber)
    {
        case AP_QOS_TABLE:
            RbtDestroyQos ();
            break;
        default:
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return SUCCESS;
}

#endif
