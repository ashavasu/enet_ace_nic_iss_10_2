
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcaprxdb.c,v 1.3 2018/02/20 10:52:54 siva Exp $
 *
 * Description: This file contains DB related API for RCV module in AP
 *******************************************************************/
#ifndef _FCAP_RXDB_C_
#define _FCAP_RXDB_C_

/******************************************************************************
 *                             Includes
 *******************************************************************************/
#include "fcksrxdb.h"
#include "fcksprot.h"

static int          rbInsert_sta (struct rb_root *root, tRBTreeType * data);
static void         rbt_dealloc_sta (void *element, void *pool_data);
static void        *rbt_alloc_sta (gfp_t gfp_mask, void *pool_data);
static int          RbtReplace_staMac (unsigned char *findData,
                                       tRBTreeType * rbData);

/* Initialize root node */
struct rb_root      root_sta = RB_ROOT;

/* Initialize mempool variable */
static mempool_t   *memPool_sta = NULL;

extern unsigned int gu1LocalRoutingStatus;
extern unsigned int gu2WtpDataPort;
extern unsigned int gu2WlcDataPort;
extern unsigned int gu4WlcIpAddr;
extern unsigned int gu4WtpIpAddr;
extern unsigned int gu4IfIndex;
extern unsigned int gu2Vlan;
extern unsigned char gau1NextHopMac[6];
extern unsigned char gau1WlcMacAddr[6];
extern unsigned char gau1WlcInterface[INTERFACE_LEN];

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
rbt_alloc_sta (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tApRcvStationTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
rbt_dealloc_sta (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : RbtCreate
 *  Description            : This function is used to create RB tree memory pool 
 *                           via ioctl of wtp driver with nodes as input
 *  Input(s)               : int num_nodes
 *  Output(s)              : mempool with reference. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
int
RbtCreate (int num_nodes)
{
    /* To create the memory pool */
    memPool_sta =
        mempool_create (num_nodes, rbt_alloc_sta, rbt_dealloc_sta, NULL);
    if (memPool_sta == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree\n");
        return FAIL;
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : struct rb_root *root, unsigned char *findData, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tApRcvStationTable *
RbtSearchSta (struct rb_root * root, unsigned char *StaMac)
{
    struct rb_node     *rbnode = root->rb_node;
    int                 result = 0;

    while (rbnode)
    {
        tApRcvStationTable *data =
            container_of (rbnode, tApRcvStationTable, node);
        result = memcmp (StaMac, data->staData.staMac, 6);

        if (result < 0)
            rbnode = rbnode->rb_left;
        else if (result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search_macType
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : struct rb_root *root, unsigned char *findData
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tApRcvStationTable *
RbtRxSearchStaMac (unsigned char *findData)
{
    tApRcvStationTable *rbData_sta;

    rbData_sta = RbtSearchSta (&root_sta, findData);
    if (rbData_sta != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "RbtSearchStaMac: Search Success\n");
#endif
        return rbData_sta;
    }
    else
    {
#if DBG_LVL_01
        printk (KERN_ERR
                "RbtSearchStaMac: Search Failure for %x:%x:%x:%x:%x:%x\n",
                findData[0], findData[1], findData[2], findData[3], findData[4],
                findData[5]);
#endif
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : rbInsert
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
rbInsert_sta (struct rb_root *root, tRBTreeType * data)
{
    int                 result = 0;    /* PS */
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tApRcvStationTable *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_sta, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->staData = data->staData;

        /* To Figure out where to put new node */
        while (*new)
        {
            tApRcvStationTable *this =
                container_of (*new, tApRcvStationTable, node);
            result = memcmp (numNodes->staData.staMac, this->staData.staMac, 6);

            parent = *new;
            if (result < 0)
                new = &((*new)->rb_left);
            else if (result > 0)
                new = &((*new)->rb_right);
            else
            {
                this->staData.u1MacType = numNodes->staData.u1MacType;
                this->staData.u2VlanId = numNodes->staData.u2VlanId;
                memcpy (this->staData.bssid, numNodes->staData.bssid,
                        MAC_ADDR_LEN);
                this->staData.u1StationStatus =
                    numNodes->staData.u1StationStatus;
                this->staData.u2TcpPort = numNodes->staData.u2TcpPort;
                this->staData.u1WMMEnable = numNodes->staData.u1WMMEnable;
                this->staData.u1TaggingPolicy =
                    numNodes->staData.u1TaggingPolicy;
                this->staData.u1TrustMode = numNodes->staData.u1TrustMode;
                this->staData.u1QosProfilePriority =
                    numNodes->staData.u1QosProfilePriority;
                memcpy (this->staData.wlanId, numNodes->staData.wlanId,
                        INTERFACE_LEN);

                mempool_free (numNodes, memPool_sta);

                return SUCCESS;
            }
        }
#if DBG_LVL_01
        printk (KERN_INFO "\nInsert\n");
        printk (KERN_INFO "\nstamac - %x:%x:%x:%x:%x:%x\n",
                numNodes->staData.staMac[0], numNodes->staData.staMac[1],
                numNodes->staData.staMac[2], numNodes->staData.staMac[3],
                numNodes->staData.staMac[4], numNodes->staData.staMac[5]);
        printk (KERN_INFO "\nbssid - %x:%x:%x:%x:%x:%x\n",
                numNodes->staData.bssid[0], numNodes->staData.bssid[1],
                numNodes->staData.bssid[2], numNodes->staData.bssid[3],
                numNodes->staData.bssid[4], numNodes->staData.bssid[5]);
#endif
        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : RbtDisplay
 *  Description            : This function is used to display RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/
void
RbtDisplay (int i4TableNum)        /* PS */
{
    struct rb_root     *root = NULL;
    struct rb_node     *node;
    UINT4               u4Index = 0;
    char                stamac[6];
    char                bssid[6];

    memset (&stamac, 0, 6);
    memset (&bssid, 0, 6);

    printk (KERN_INFO "\n RCV module Displaying Data.....\n");

    /* to iterate the tree */
    {
        switch (i4TableNum)
        {
            case AP_WTP_TABLE:
                printk (KERN_INFO "\n\n");
                printk (KERN_INFO "WLC Data Port = %d", gu2WlcDataPort);
                printk (KERN_INFO "WTP Data Port = %d", gu2WtpDataPort);
                printk (KERN_INFO "WLC IP Address = 0x%x", gu4WlcIpAddr);
                printk (KERN_INFO "WLC MAC Address = %x:%x:%x:%x:%x:%x",
                        gau1WlcMacAddr[0], gau1WlcMacAddr[1], gau1WlcMacAddr[2],
                        gau1WlcMacAddr[3], gau1WlcMacAddr[4],
                        gau1WlcMacAddr[5]);
                printk (KERN_INFO "Local Routing = %d", gu1LocalRoutingStatus);
                printk (KERN_INFO "Interface  = %s", gau1WlcInterface);
                break;

            case AP_STA_TABLE:
                root = &root_sta;
#ifdef DEBUG_WANTED
                printk (KERN_INFO "\n\n");

                for (node = rb_first (root); node; node = rb_next (node))
                {
                    for (u4Index = 0; u4Index < 6; u4Index++)
                        printk (KERN_INFO "staMac[%d]= %x\n",
                                u4Index, rb_entry (node, tApRcvStationTable,
                                                   node)->staData.
                                staMac[u4Index]);
                    for (u4Index = 0; u4Index < 6; u4Index++)
                        printk (KERN_INFO "bssid[%d] = %x\n",
                                u4Index, rb_entry (node, tApRcvStationTable,
                                                   node)->staData.
                                bssid[u4Index]);
                    printk (KERN_INFO "interface = %s\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.wlanId);
                    printk (KERN_INFO "vlan = %d\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.u2VlanId);
                    printk (KERN_INFO "mac type = %d\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.u1MacType);
                    printk (KERN_INFO "web auth enable = %d\n",
                            rb_entry (node, tApSndStationTable,
                                      node)->staData.u1WebAuthEnable);
                    printk (KERN_INFO "webauth status = %d\n",
                            rb_entry (node, tApSndStationTable,
                                      node)->staData.u1WebAuthStatus);
                    printk (KERN_INFO "WMMEnable  = %d\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.u1WMMEnable);
                    printk (KERN_INFO "TaggingPolicy = %d\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.u1TaggingPolicy);
                    printk (KERN_INFO "TrustMode = %d\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.u1TrustMode);
                    printk (KERN_INFO "QosProfilePriority = %d\n",
                            rb_entry (node, tApRcvStationTable,
                                      node)->staData.u1QosProfilePriority);

                }
#endif
                break;
            default:
                printk (KERN_ERR "rbt_search: switch reached default\n");
                break;
        }
    }
    return;
}

/******************************************************************************
 *  Function Name          : RbtDeleteStaMac
 *  Description            : This function is used to delete RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : search string
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
RbtDeleteStaMac (unsigned char *search)
{
    tApRcvStationTable *data = NULL;

    data = RbtSearchSta (&root_sta, search);

    if (data != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "\n data is found\n");
#endif
        /* To delete the data */
        rb_erase (&data->node, &root_sta);
        mempool_free (&data->node, memPool_sta);
        RbtDisplay (AP_STA_TABLE);    /* PS */

        return SUCCESS;
    }
    else
    {
        printk (KERN_ERR "\n data is not found");
        return FAIL;
    }
}

/******************************************************************************
 *  Function Name          : RbtReplace_sta
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wtp driver
 *  Input(s)               : unsigned char *findData,struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static int
RbtReplace_staMac (unsigned char *findData, tRBTreeType * rbData)
{
    tApRcvStationTable *data = NULL;
    tApRcvStationTable *replaceData = NULL;

    replaceData = mempool_alloc (memPool_sta, GFP_ATOMIC);
    if (replaceData == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }

    replaceData->staData = rbData->staData;
    /* To search the data to be replaced */
    data = RbtSearchSta (&root_sta, findData);
    if (data != NULL)
    {
        /* To replace the data */
        rb_replace_node (&data->node, &replaceData->node, &root_sta);
        return SUCCESS;
    }
    else
    {
        mempool_free (replaceData, memPool_sta);
        printk (KERN_ERR "Error: no data to replace \n");
        return FAIL;
    }
}

/******************************************************************************
 *  Function Name          : RbtInsert
 *  Description            : This function is used to insert RB tree data  
 *                           via ioctl of wtp driver, with root as reference
 *  Input(s)               : struct rbtree_type *rbdata
 *  Output(s)              : inserted node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
int
RbtInsert (tRBTreeType * rbdata, int i4TableNum)
{
    switch (i4TableNum)
    {
        case AP_STA_TABLE:
            if (rbInsert_sta (&root_sta, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "rBinsert: Insert Failure\n");
                return FAIL;
            }
            break;
    }
    return SUCCESS;
}

int
rbt_delete (tRBTreeType * rbdata, int i4TableNum)
{
    switch (i4TableNum)
    {
        case AP_STA_TABLE:
            if (RbtDeleteStaMac (rbdata->staData.staMac) != SUCCESS)
            {
                printk (KERN_INFO "Remove Failure\n");
                return FAIL;
            }
            break;
    }
    return SUCCESS;
}

int
RbtReplace (tRBTreeType * rbdata, tRBTreeType * replaceData, int i4TableNum)
{
    switch (i4TableNum)
    {
        case AP_STA_TABLE:
            if (RbtReplace_staMac (rbdata->staData.staMac, replaceData) !=
                SUCCESS)
            {
                printk (KERN_INFO "Remove Failure\n");
                return FAIL;
            }
            break;
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroySta (void)
{
    struct rb_node     *node;

    for (node = rb_first (&root_sta); node; node = rb_next (node))
    {
        RbtDeleteStaMac (rb_entry (node, tApRcvStationTable, node)->staData.
                         staMac);
    }
    /* To delete the memory pool */
    mempool_destroy (memPool_sta);
}

#endif
