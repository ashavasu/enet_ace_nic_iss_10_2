
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcksprot.h,v 1.3 2017/05/23 14:16:51 siva Exp $
 *
 * Description: This file contains prototype for all kernel related
 * files.
 *******************************************************************/

#ifndef _FCKS_PROT_H_
#define _FCKS_PROT_H_

#include "fcksglob.h"
#include "fcksrxdb.h"
#include "fckstxdb.h"

UINT4 FcapwapGetDscpFromUP(UINT1);
UINT4 FcapwapGetDscpFromQosProfile(UINT1);
UINT4 FcapwapGetPcpFromUP(UINT1);
UINT4 FcapwapGetPcpFromQosProfile(UINT1);
UINT4 FcapwapGetUPFromDscp(UINT1);
UINT4 FcapwapGetUPFromPcp(UINT1);
UINT4 FcapwapGetDscpFromPcp(UINT1);
UINT4 FcapwapGetUPFromQosProfile(UINT1);

#ifdef WLC_WANTED
void FcapwapAddCapwapHeader(UINT1 *pBuf,UINT1 *pBssid);
void FcapwapRemoveVlanHeader (struct sk_buff *pSkb ,UINT2 u2VlanId);
void FcapwapAddVlanHeader(struct sk_buff *pSkb ,UINT2 u2VlanId);
UINT4 FcapwapWlcHookPreRoutingLast (UINT4 , struct sk_buff *, 
        const struct net_device *, 
        const struct net_device *, INT4 (*okfn) (struct sk_buff *));
UINT4 FcapwapWlcHookPreRouting (UINT4 , struct sk_buff *, 
        const struct net_device *, 
        const struct net_device *, INT4 (*okfn) (struct sk_buff *));
UINT4 FcapwapWlcHookPostRouting (UINT4 , struct sk_buff *, 
        const struct net_device *, 
        const struct net_device *, INT4 (*okfn) (struct sk_buff *));
UINT4 FcapwapDscpHookPreRouting (UINT4 , struct sk_buff *, 
        const struct net_device *, 
        const struct net_device *, INT4 (*okfn) (struct sk_buff *));

/******************************************************************************
 *                           Function Prototypes
 ******************************************************************************/
/* function to process udp packets */
//static INT4 ioctl_wlcdrv(struct file *, UINT4, unsigned long);
static INT4 FcapwapWlcTxIoctl(struct file *filep, UINT4 u4Cmd, FS_ULONG arg);
INT4 FcapwapWlcTxDeviceOpen(struct inode *inodep, struct file *filep);
INT4 FcapwapWlcTxDeviceRelease ( struct inode * , struct file *);
INT4 FcapwapWlcRxDeviceOpen(struct inode *inodep, struct file *filep);
INT4 FcapwapWlcRxDeviceRelease ( struct inode * , struct file *);
UINT1 verifyCapwapDataPkt(UINT1 *pu1Buf);
INT4 FcapwapDscpTxDeviceOpen(struct inode *inodep, struct file *filep);
INT4 FcapwapDscpTxDeviceRelease ( struct inode * , struct file *);
/*Fix for Bug 7987 */
INT4 FcapwapTransmitPacketToWtp (struct sk_buff *sk_buffer, UINT1 *pSrcMacAddr,
        UINT1 *pDataSrcMacAddr, 
        INT4 i4src_ip, INT4 i4DestIp,  UINT1 *pu1Bssid, tStaIpInfo *pStaIpData,
        UINT4 *pu4Tos);
tAcRcvAPMacTable *RbtSearchApMac(struct rb_root *root, 
        UINT4 u4IpAddr);
tAcRcvStaTable *RbtSearchSta(struct rb_root *root, 
UINT1 *staMac);
tAcRcvVlanTable *RbtSearchVlan(struct rb_root *root, 
UINT2 u2VlanId, UINT1 *pPort);
tAcRcvMacTable *RbtSearchMac(struct rb_root *root, 
        UINT1 *MacAddr);
tAcRcvStaIpTable *RbtSearchStaIp(struct rb_root *root, 
        UINT4 u4StaIp);
tAcSndStaTable *RbtSearchStaDB(struct rb_root *root,  UINT1 *pstaMac);
tAcRcvAPMacTable *APMacTableSearch(UINT4 u4IpAddr);
tAcRcvStaTable *APStaTableSearch(UINT1 *staMac);
tAcRcvVlanTable *VlanRxTableSearch(UINT2 u2VlanId, 
        UINT1 *pPort);
tAcRcvMacTable *MacTableSearch(UINT1 *MacAddr);
tAcSndStaTable *ApStaDBTableSearch(UINT1 *staMac);
tAcRcvStaIpTable *APSta_IP_Table_Search(UINT4 u4IpAddr);
tAcSndIpTable *ApIpTableSearch(UINT4 u4IpAddr);
tAcSndIntfTable *ApIntfTableSearch(UINT1 *pBssid);
tAcSndVlanTable *VlanTxTableSearch(UINT1 *pIntfName, 
        UINT2 u2VlanId);
tAcSndMacTable *Mac_Table_Search(UINT1 *pMacAddr);
tAcSndStaIPTable *StaIpTableSearch(UINT4 u4IpAddr);
tAcSndApMacTable * APSndMacTableSearch (UINT4 u4IpAddr);
INT4 RbInsertApMac(struct rb_root *root, tRBtree *data);
void RbtDisplay(INT4 i4TableNum);
INT4 RbtDeleteApMac(tRBtree *pData);
INT4 RbtDeleteApSta(tRBtree *pData);
INT4 RbtDeleteVlan(tRBtree *pData);
INT4 RbtDeleteMac(tRBtree *pData);
INT4 RbtDeleteStaIp(tRBtree *pData);
INT4 RbtDeleteStaDB(tRBtree *rbData);
INT4 RbtReplace_apMac(tRBtree *rbData, tRBtree *replaceData);
INT4 RbtReplace_apSta(tRBtree *rbData, tRBtree *replaceData);
INT4 RbtReplace_vlan(tRBtree *rbData, tRBtree *replaceData);
INT4 RbtReplace_mac(tRBtree *rbData, tRBtree *replaceData);
INT4 RbtReplaceStaIp(tRBtree *rbData, tRBtree *replaceData);
INT4 RbtReplaceStaDB(tRBtree *rbData, tRBtree *replaceData);
INT4 RbtInsert(tRBtree *rbdata,INT4 i4TableNum);
INT4 RbtRemove(tRBtree *rbdata,INT4 i4TableNum);
INT4 RbtReplace(tRBtree *rbdata,tRBtree *replaceData, INT4 i4TableNum);
void RbtDestroy(INT4 i4TableNum);
INT4 RbtTxDestroy(INT4 i4TableNum);
INT4 RbtCreate(INT4 num_nodes);
void RbtDestroy_ap_mac(void);
void RbtDestroySta(void);
void RbtDestroyVlan(void);
void RbtDestroyMac(void);
void RbtDestroyStaIp(void);
UINT2 IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size);

/*Fix for Bug 7987 */
INT4 FcapwapFragmentDataAndSend (struct sk_buff *skb,tApMacInfo * apMacData,
                             UINT4 u4PktLen,INT4 i4SrcIp, INT4 i4DestIp,
                              tStaIpInfo *pStaIpData,UINT1 u1TagStatus,
                              UINT2 u2Pcp,UINT2 u2VlanId,UINT1* pSrcMacAddr , UINT4 *pu4Tos);

/*Fix for Bug 7987 */
void FCapwapFormOuterHeader(struct sk_buff *sk_buffer,tApMacInfo * apMacData,
        INT4 i4SrcIp, INT4 i4DestIp,tStaIpInfo *pStaIpData,UINT1 u1TagStatus,
        UINT2 u2Pcp,UINT2 u2VlanId,UINT1 *pSrcMacAddr ,UINT4 * pu4Tos);

INT4 CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr);

void CapwapPutHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr);

INT4 verifyCapwapFragmentReceived (struct sk_buff * pSkb);
UINT4 CapwapDataFragReassemble (struct sk_buff * skb,tAPIpInfo *apIpData,
                               UINT2 u2PktLen, tCRU_BUF_CHAIN_HEADER ** 
                               ppReassembleBuf);
tCapFragNode * CapwapCreateFrag (UINT2 u2StartOffset, UINT2 u2EndOffset, 
                         tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2FragId);

INT4
CapwapDataFreeReAssemble (tCapFragment * pCapDataFragStream, UINT2 u2FragIndex,
                          BOOL1 bForceRelease);

tDscpQosTable *RbtSearchQos(struct rb_root *root,
        UINT1 *au1InterfaceName, UINT1 u1dscpin);
void RbtDestroyQos(void);
tDscpQosTable *DscpQosTableSearch(UINT1 *findInterfaceName,UINT1 u1dscpin);


/* extern INT4 BufInitManager (tBufConfig * pBufLibInitData); */
#endif

#ifdef WTP_WANTED
/******************************************************************************
 *                           Function Prototypes
 ******************************************************************************/
/* function to process udp packets */
INT4 FcapwapWtpTxDeviceOpen(struct inode *inodep, struct file *filep);
INT4 FcapwapWtpTxDeviceRelease(struct inode *inodep, struct file *filep);
INT4 FcapwapWtpRxDeviceOpen    ( struct inode * , struct file *);
INT4 FcapwapWtpRxDeviceRelease ( struct inode * , struct file *);
UINT4 FcapwapWtpHookPreRouting  (UINT4 , struct sk_buff *, 
        const struct net_device *, const struct net_device *, 
        INT4 (*okfn) (struct sk_buff *));
INT4 FcapwapProcessFrameSnd(struct sk_buff *, struct net_device *, 
        struct packet_type *,struct net_device *);
INT4 FcapwapWtpTxProcessDataPkt(struct sk_buff *psk_buffer);
INT4 FcapwapWtpProcessWlan (struct sk_buff *sk_buffer);
INT4  FcapwapSendtoStation(struct sk_buff *sk_buffer, UINT4);
INT4  FcapwapWtpRcvDataPkt(struct sk_buff *sk_buffer);
INT4 processEthPacket(void);
INT4 RbtCreate(INT4 num_nodes);
tApRcvStationTable *RbtSearchSta(struct rb_root *root,  
        UINT1 *StaMac);
tApRcvStationTable *RbtSearchStaMac(UINT1 *findData);
void RbtDisplay(INT4 i4TableNum);
void RbtDestroySta(void);
void RbtDestroy_Sta(void);
INT4 RbtInsert(tRBTreeType *rbdata,INT4 i4TableNum);
INT4 RbtInsertDscp (tRBTreeType * rbdata, INT4 i4TableNumber);
INT4 rbt_delete(tRBTreeType *rbdata,INT4 i4TableNum);
INT4 RbtReplace(tRBTreeType *rbdata,tRBTreeType *replacedata, 
        INT4 i4TableNum);
INT4 RbtTxDestroy(INT4 i4TableNum);
INT4 RbtTxReplaceStaMac(UINT1 *findData,tRBTreeType *rbData);
INT4 RbtTxDeleteStaMac(UINT1 *search);
INT4 RbtTxInsertSta(tRBTreeType *rbdata);
INT4 RbtTxCreateSta(INT4 num_nodes);
tApRcvStationTable *RbtRxSearchStaMac(UINT1 *findData);
tApSndStationTable  *RbtTxSearchstaMac(UINT1 *findData);
INT4 AddDevice(UINT1 *DeviceName);
VOID RemoveDevice(UINT1 *DeviceName);
void FcapwapAddCapwapHeader(UINT1 *pBuf,UINT1 *pBssid, UINT1 u1MacType);
UINT2
IpCalcCheckSum (struct sk_buff *pSkb, UINT4 u4Size);
INT4 verifyCapwapFragmentReceived (struct sk_buff * pSkb);
UINT4
CapwapDataFragReassemble (struct sk_buff * skb, UINT2 u2PktLen,
                          tCRU_BUF_CHAIN_HEADER ** ppReassembleBuf);

INT4 CapwapDataFreeReAssemble (UINT2 u2FragIndex,BOOL1 bForceRelease);

tCapFragNode  * CapwapCreateFrag (UINT2 u2StartOffset, UINT2 u2EndOffset,
                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2FragId);

INT4 FcapwapWtpFragmentAndSend(struct sk_buff  *psk_buff,INT4 i4MTU,twlanStaInfo * pStaData);

INT4 CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr);

void CapwapPutHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tCapwapHdr * pHdr);
 INT4 FCapwapFormOuterHeader(struct sk_buff *sk_buffer,twlanStaInfo * pStaData);
void FcapwapProcessReassmTimeExpiry (VOID);
#ifdef AP200_WANTED
INT4 FCapwapFormOuterHeaderEap(struct sk_buff *sk_buffer);
#endif

INT4 FcapwapDscpTxDeviceOpen(struct inode *inodep, struct file *filep);
INT4 FcapwapDscpTxDeviceRelease ( struct inode * , struct file *);
UINT4 FcapwapDscpHookPreRouting (UINT4 , struct sk_buff *, 
        const struct net_device *, 
        const struct net_device *, INT4 (*okfn) (struct sk_buff *));
void RbtDestroyQos(void);
tDscpQosTable *DscpQosTableSearch(UINT1 *findInterfaceName,UINT1 u1dscpin);
INT4 RbtRemove(tRBtree *rbdata,INT4 i4TableNum);
INT4 RbtRemoveDscp (tRBTreeType *rbdata,INT4 i4TableNum);
INT4 FcapwapWMM(struct sk_buff *psk_buffer,tApSndStationTable *rbData_Sta);
#endif

#endif /* _FCKS_PROT_H_ */
