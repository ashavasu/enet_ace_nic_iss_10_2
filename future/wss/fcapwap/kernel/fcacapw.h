
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcacapw.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains capwap specfic macros and typedefs
 *******************************************************************/
#ifndef _FCA_CAPW_H_
#define _FCA_CAPW_H_


#define VOID            void
#define OSIX_SUCCESS                    0
#define OSIX_FAILURE                    1
#ifndef NULL
#define NULL    (0)
#endif

#define CAPWAP_HDR_RID_WID_FLAGS_LOCAL 0x2042
#define CAPWAP_HDR_FLWMK_FLAGS         0x10
#define CAPWAP_HDR_RADIO_MAC           6
#define CAPWAP_HDR_NULL_WIRELESS_INFO 0x00
#define CAPWAP_HDR_MIN_LEN            8
#define CAPWAP_HDR_RADIO_MAC_LEN      1
#define CAPWAP_HDR_WIRELESS_INFO_LEN   1

#define DOT11_HDR_MAC_ADDR_LEN          6
#define DOT2_ORG_CODE_LEN               3
#define DOT3_HDR_ADDR_LEN               6
#define DOT11_HDR_FRAME_CTRL_LEN        2
#define DOT11_HDR_DURN_LEN              2
#define DOT11_HDR_SEQ_CTRL_LEN          2
#define DOT11_HDR_QOS_CTRL_LEN          2
#define DOT11_FLAG_YES                  1
#define DOT11_FLAG_NO                   0

#define DOT2_ORG_CODE_LEN               3
#define DOT2_UI                         0x3
#define DOT2_SNAP_LSAP                  0xaa
#define DOT2_SNAP_ORGCODE               0x00
#define DOT2_SNAP_BRTUNL_ORGCODE        0xf8
#define DOT2_ETHTYPE_AARP               0x80f3
#define DOT2_ETHTYPE_IPX                0x8137

#define DOT11_FC_BYTE1_DIR_ALL          0x03
#define DOT11_FC_BYTE1_DIR_ST_ST        0x00    /* STA->STA */
#define DOT11_FC_BYTE1_DIR_ST_AP        0x01    /* STA->AP  */
#define DOT11_FC_BYTE1_DIR_AP_ST        0x02    /* AP->STA  */
#define DOT11_FC_BYTE1_DIR_AP_AP        0x03    /* AP->AP   */

#define DOT11_REG_HDR_SIZE              24
#define DOT11_FULL_FRAM_HDR_SIZE        30
#define DOT11_FULL_QOS_HDR_SIZE         32
#define DOT11_REG_QOS_HDR_SIZE          26

#define DOT11_ELEM_BYTE_0               0
#define DOT11_ELEM_BYTE_1               1

#define DOT11_FC_BYTE0_TYPE_ALL         0x0c
#define DOT11_FC_BYTE0_VERSION          0x00
#define DOT11_FC_BYTE0_TYPE_DATA        0x08
#define DOT11_FC_BYTE0_SUBTYPE_QOS      0x80

#define DOT3_ETH_HDR_SIZE               14
#define DOT2_LLC_HDR_SIZE               8
#define DOT2_LLC_DSAP_SSAP_SIZE         2

#define DOT3_HDR_ADDR_LEN               6
#define DOT3_MAX_LEN                    1536    /* 0x600 - Max Ethernet II */
#define DOT1X_ETHTYPE_VLAN              0x8100
#define DOT11_MAX_PKT_SIZE              2350


#define MAC_ADDR_LEN 6
typedef UINT1 tMacAddr [MAC_ADDR_LEN];

#define VOID void
#define CAPWAP_WTP_HLEN_RID_WBID_FLAGT 0x1000
#define CAPWAP_HDR_RID_WID_FLAGS_SPLIT 0x1083
#define CAPWAP_HDR_LEN 8
#define KEEP_ALIVE_BYTE 3 
#define CAPWAP_MAX_HDR_LEN 16
#define   VAL_HEX    0x80
#define   FRAME_ADDRESS   0x00

#define   CAPWAP_MEMCPY      memcpy
#define   CAPWAP_HTONS       OSIX_HTONS
#define   CAPWAP_HTONL       OSIX_HTONL
#define   CAPWAP_NTOHS       OSIX_NTOHS
#define   CAPWAP_NTOHL       OSIX_NTOHL


#define CAPWAP_PUT_1BYTE(pu1PktBuf, u1Val)  \
        do {                                \
           *pu1PktBuf = u1Val;              \
            pu1PktBuf += 1;                 \
        } while(0)

#define CAPWAP_PUT_2BYTE(pu1PktBuf, u2Val)      \
        do {                                    \
           *((UINT2 *) ((VOID*)pu1PktBuf)) = CAPWAP_HTONS(u2Val);\
           pu1PktBuf += 2;\
        } while(0)
#define CAPWAP_PUT_4BYTE(pu1PktBuf, u4Val)     \
        do {                                   \
           *((UINT4 *) ((VOID*)pu1PktBuf)) = OSIX_HTONL(u4Val);\
           pu1PktBuf += 4;\
        }while(0)

#define CAPWAP_PUT_NBYTE(pu1PktBuf, pu1PktBuf1, length)    \
        do {                                               \
           memcpy (pu1PktBuf, &pu1PktBuf1, length); \
           pu1PktBuf += length;                            \
        }while(0)

#define CHECK_FOR_SNAP(pLlcHdr) \
                                ( (pLlcHdr->u1LlcDsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->u1LlcSsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1Control == DOT2_UI) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[0] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[1] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[2] == DOT2_SNAP_ORGCODE) && \
                                  (!( (pLlcHdr->unLlcType.SnapHdr.u2EthType == OSIX_HTONS(DOT2_ETHTYPE_AARP)) || \
                                      (pLlcHdr->unLlcType.SnapHdr.u2EthType == OSIX_HTONS(DOT2_ETHTYPE_IPX)) )) )

#define CHECK_FOR_SNAP_BRTUNL(pLlcHdr) \
                                ( (pLlcHdr->u1LlcDsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->u1LlcSsap == DOT2_SNAP_LSAP) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1Control == DOT2_UI) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[0] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[1] == DOT2_SNAP_ORGCODE) && \
                                  (pLlcHdr->unLlcType.SnapHdr.u1OrgCode[2] == DOT2_SNAP_BRTUNL_ORGCODE) )

/* Macros for getting data from linear buffer */
#define CAPWAP_GET_1BYTE(u1Val, pu1PktBuf) \
        do {                             \
    CAPWAP_MEMCPY (&u1Val,   \
       pu1PktBuf, sizeof (UINT1));\ 
           pu1PktBuf += 1;               \ 
        } while(0)

#define CAPWAP_GET_2BYTE(u2Val, pu1PktBuf)          \
        do {                                        \
           CAPWAP_MEMCPY (&u2Val, pu1PktBuf, 2);    \
           u2Val = (UINT2)CAPWAP_NTOHS(u2Val);      \
           pu1PktBuf += 2;                          \
        } while(0)

#define CAPWAP_GET_4BYTE(u4Val, pu1PktBuf)            \
        do {                                          \
           CAPWAP_MEMCPY (&u4Val, pu1PktBuf, 4);      \
           u4Val = (UINT4)CAPWAP_NTOHL(u4Val);        \
           pu1PktBuf += 4;                            \
        } while(0)

#define CAPWAP_GET_NBYTE(u4Val, pu1PktBuf, u4Len)    \
        do {                                         \
            CAPWAP_MEMCPY (&u4Val,pu1PktBuf,u4Len);  \
            pu1PktBuf += u4Len;                      \
        }while (0)

#define CAPWAP_SKIP_N_BYTES(u2Val, pu1PktBuf)        \
        do {                                         \
            pu1PktBuf += u2Val;                      \
        }while(0)



#define CAPWAP_BUF_IF_LINEAR(pBuf, u4OffSet, u4Size) \
        CRU_BUF_Get_DataPtr_IfLinear ((tCRU_BUF_CHAIN_HEADER *)(pBuf), \
                                      (u4OffSet), (u4Size))

#define CAPWAP_COPY_FROM_BUF(pBuf, pu1Dst, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain ((pBuf), (UINT1 *)(pu1Dst), u4Offset, u4Size)


typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];
    UINT1           au1FrameAddr4[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameQosCtrl[DOT11_HDR_QOS_CTRL_LEN];

}tDot11FullQosFrameHdr;


typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];

}tDot11RegFrameHdr;


typedef struct {

    UINT1           u1LlcDsap;
    UINT1           u1LlcSsap;
    UINT1           au1Pad[2];

    union {

        struct {

            UINT1       u1Control;
            UINT1       u1OrgCode[DOT2_ORG_CODE_LEN];
            UINT2       u2EthType;
            UINT1       au1Pad[2];

        }SnapHdr;

    }unLlcType;

}tDot2LlcFrameHdr;

typedef struct {

    UINT1           au1FrameDAddr[DOT3_HDR_ADDR_LEN];
    UINT1           au1FrameSAddr[DOT3_HDR_ADDR_LEN];
    UINT2           u2FrameEthType;
    UINT1           au1Pad[2];

}tDot3EthFrameHdr;

typedef struct {

    UINT1           au1FrameCtrl[DOT11_HDR_FRAME_CTRL_LEN];
    UINT1           au1Duration[DOT11_HDR_DURN_LEN];
    UINT1           au1FrameAddr1[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr2[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameAddr3[DOT11_HDR_MAC_ADDR_LEN];
    UINT1           au1FrameSeqCtrl[DOT11_HDR_SEQ_CTRL_LEN];
    UINT1           au1FrameQosCtrl[DOT11_HDR_QOS_CTRL_LEN];
    UINT1           au1Pad[2];

}tDot11RegQosFrameHdr;

typedef struct{
    tMacAddr RadioMacAddr;
    UINT1  au1Pad[2];
    UINT4  u4WirelessRadioInfo;
    UINT2  u2FragId;            /* 16 bits fragment Id */
    UINT2  u2FragOffsetRes3;    /* 13 bits fragment offset */
    UINT2  u2HdrLen; /* Length of the CAPWAP header */
    UINT2  u2HlenRidWbidFlagT;  /* contains the 5 bits header length , 
                                   5 bits radio Id, 5 bits Wbid, Flags bits - T */
    UINT1  u1Preamble;          /* Capwap preamble-0 for payload type*/
    UINT1  u1FlagsFLWMKResd;    /* Flags F, L,W,M,K , last 3 bits reserved */
    UINT1  u1RadMacAddrLength; /* optional 32 bit radio MAC address field */
    UINT1  u1WirelessInfoLength; /* optional wireless info */
}tCapwapHdr;

INT4
CapwapConstructCpHeader (tCapwapHdr *pCapwapHdr, int macType);
VOID
AssembleCapwapHeader(UINT1 *pTxPkt,
                     tCapwapHdr *pCapwapHdr);

INT4
GetDot11HdrSize(UINT1* pu1Dot11PktBuf);

UINT1*
PullPtr (UINT1** pBuf, INT4 u4NewLen);

UINT1
Dot11ToDot3 (UINT1** pu1Dot11PktBuf, UINT4 *u4Dot11PktLen);

UINT1
Dot3ToDot11 (UINT1** pu1PktBuf, UINT1* pu1BssId, UINT4 *u4PktLen, UINT1 u1Direction);

#endif
