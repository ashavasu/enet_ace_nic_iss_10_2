
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcksrxdb.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains rcv module db related typedefs
 *******************************************************************/

#ifndef _FCRCV_DB_H_
#define _FCRCV_DB_H_

/******************************************************************************
 *                             Global Includes
 *******************************************************************************/
#include <linux/module.h>
#include <linux/rbtree.h>
#include <linux/mempool.h>
#include <linux/gfp.h>

#include "fcksglob.h"

#ifdef WTP_WANTED
/******************************************************************************
 *                     RB Interface Structure for WTP 
 *******************************************************************************/

typedef struct 
{
    struct rb_node node;
    twlanStaInfo staData;
}tApRcvStationTable;
#endif

#ifdef WLC_WANTED
/******************************************************************************
 *                     RB Interface Structure for WLC
 *******************************************************************************/
typedef struct
{
    struct rb_node  node;
    tApMacInfo      apMacData;
}tAcRcvAPMacTable;

typedef struct
{
    struct rb_node  node;
    tApStaInfo      apStaData;
}tAcRcvStaTable;

typedef struct
{
    struct rb_node  node;
    tVlanInfo       VlanData;
}tAcRcvVlanTable;

typedef struct
{
    struct rb_node  node;
    tMacInfo        MacData;
}tAcRcvMacTable;

typedef struct
{
    struct rb_node  node;
    tStaIpInfo      staIpData;
}tAcRcvStaIpTable;

#endif
#endif
