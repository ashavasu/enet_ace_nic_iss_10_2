
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcactxdb.c,v 1.4 2018/01/22 09:39:42 siva Exp $
 *
 * Description: This file contains DB related API for SND module in WLC
 *******************************************************************/
#ifndef _FCAC_TXDB_C_
#define _FCAC_TXDB_C_

/******************************************************************************
 *                             Includes
 *******************************************************************************/
#include "fckstxdb.h"

/* Initialize root node */
struct rb_root      root_Ip = RB_ROOT;
struct rb_root      root_Int = RB_ROOT;
struct rb_root      root_Vlan = RB_ROOT;
struct rb_root      root_Mac = RB_ROOT;
struct rb_root      root_Sta_Ip = RB_ROOT;
struct rb_root      root_Sta_db = RB_ROOT;
struct rb_root      root_Ap_Mac_db = RB_ROOT;

/* Initialize mempool variable */
static mempool_t   *memPool_Ip = NULL;
static mempool_t   *memPool_Int = NULL;
static mempool_t   *memPool_Vlan = NULL;
static mempool_t   *memPool_Mac = NULL;
static mempool_t   *memPool_Sta_Ip = NULL;
static mempool_t   *memPool_Sta_db = NULL;
static mempool_t   *memPool_Ap_Mac_db = NULL;

INT4                gi4CtrlUdpPort;
INT4                gi4DataUdpPort;
INT4                gu4WlcIpAddr;
INT4                gi4WssDataDebugMask;

/******************************************************************************
 *  Function Name          : RbtAllocIp
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocIp (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndIpTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocInt (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndIntfTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocVlan (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndVlanTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocMac (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndMacTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocStaIp (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndStaIPTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocStaDB (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndStaTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocApMacDB (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcSndApMacTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocInt (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocIp (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocVlan (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocMac (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocStaIp (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocStaDB (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocApMacDB (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : RbtCreate
 *  Description            : This function is used to create RB tree memory pool 
 *                           via ioctl of wtp driver with nodes as input
 *  Input(s)               : int i4Nodes
 *  Output(s)              : mempool with reference. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtCreate (INT4 i4Nodes)
{
    /* To create the memory pool for IP Table */
    memPool_Ip = mempool_create (i4Nodes, RbtAllocIp, RbtDeallocIp, NULL);
    if (memPool_Ip == NULL)
    {
        printk (KERN_INFO "\n Error in creating mempool for IP Table \n");
        return FAIL;
    }

    /* To create the memory pool for Interface Table */
    memPool_Int = mempool_create (i4Nodes, RbtAllocInt, RbtDeallocInt, NULL);
    if (memPool_Int == NULL)
    {
        printk (KERN_INFO
                "\n Error in creating mempool for Interface Table \n");
        return FAIL;
    }

    /* To create the memory pool for VLAN Table */
    memPool_Vlan = mempool_create (i4Nodes, RbtAllocVlan, RbtDeallocVlan, NULL);
    if (memPool_Vlan == NULL)
    {
        printk (KERN_INFO "\n Error in creating mempool for Vlan Table \n");
        return FAIL;
    }

    /* To create the memory pool for MAC Table */
    memPool_Mac = mempool_create (i4Nodes, RbtAllocMac, RbtDeallocMac, NULL);
    if (memPool_Mac == NULL)
    {
        printk (KERN_INFO "\n Error in creating mempool for Mac Table \n");
        return FAIL;
    }
    /* To create the memory pool for Station IP Table */
    memPool_Sta_Ip =
        mempool_create (i4Nodes, RbtAllocStaIp, RbtDeallocStaIp, NULL);
    if (memPool_Sta_Ip == NULL)
    {
        printk (KERN_INFO
                "\n Error in creating mempool for Station IP Table \n");
        return FAIL;
    }

    /* To create the memory pool for Station DB Table */
    memPool_Sta_db =
        mempool_create (i4Nodes, RbtAllocStaDB, RbtDeallocStaDB, NULL);
    if (memPool_Sta_db == NULL)
    {
        printk (KERN_INFO
                "\n Error in creating mempool for Station DB Table \n");
        return FAIL;
    }

    /* To create the memory pool for AP Mac DB Table */
    memPool_Ap_Mac_db =
        mempool_create (i4Nodes, RbtAllocApMacDB, RbtDeallocApMacDB, NULL);
    if (memPool_Ap_Mac_db == NULL)
    {
        printk (KERN_INFO
                "\n Error in creating mempool for AP Mac DB Table \n");
        return FAIL;
    }

    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyIp (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Ip);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyInt (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Int);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyVlan (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Vlan);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyMac (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Mac);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyStaIp (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Sta_Ip);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyStaDB (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Sta_db);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyApMacDB (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_Ap_Mac_db);
}

/******************************************************************************
 *  Function Name          : RbtSearchIp
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdSnd *trdSnd, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndIpTable      *
RbtSearchIp (struct rb_root *root, UINT4 u4IpAddr)
{
    struct rb_node     *rbnode = root->rb_node;
    int                 i4Result = 0;

    while (rbnode)
    {
        tAcSndIpTable      *data = container_of (rbnode, tAcSndIpTable, node);

        i4Result = u4IpAddr - data->apIpData.u4ipAddr;

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : RbtSearchStaDB
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdSnd *trdSnd, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndStaTable     *
RbtSearchStaDB (struct rb_root * root, UINT1 *pstaMac)
{

    struct rb_node     *rbnode = root->rb_node;
    int                 i4Result = 0;

    while (rbnode)
    {
        tAcSndStaTable     *data = container_of (rbnode, tAcSndStaTable, node);

        i4Result = memcmp (pstaMac, data->StaInfo.staMac, 6);

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : RbtSearchInt
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdSnd *trdSnd, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndIntfTable    *
RbtSearchInt (struct rb_root * root, UINT1 *pBssid)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcSndIntfTable    *data = container_of (rbnode, tAcSndIntfTable, node);

        i4Result = memcmp (pBssid, data->apIntData.bssid, 6);

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : RbtSearchVlan
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdSnd *trdSnd, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndVlanTable    *
RbtSearchVlan (struct rb_root * root, unsigned short u2VlanId, UINT1 *pIntfName)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcSndVlanTable    *data = container_of (rbnode, tAcSndVlanTable, node);

        i4Result = u2VlanId - data->VlanData.u2VlanId;
        if (i4Result == 0)
        {
            i4Result =
                memcmp (pIntfName, data->VlanData.au1PhysicalPort,
                        INTERFACE_LEN);
        }

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search_Mac
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdSnd *trdSnd, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndMacTable     *
rbt_search_Mac (struct rb_root * root, UINT1 *pMacAddr)
{
    struct rb_node     *rbnode = root->rb_node;
    int                 i4Result = 0;

    while (rbnode)
    {
        tAcSndMacTable     *data = container_of (rbnode, tAcSndMacTable, node);

        i4Result = memcmp (pMacAddr, data->MacData.au1MacAddress, MAC_ADDR_LEN);

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search_Sta_Ip
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdSnd *trdSnd, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndStaIPTable   *
rbt_search_Sta_Ip (struct rb_root * root, UINT4 u4IpAddr)
{
    struct rb_node     *rbnode = root->rb_node;
    int                 i4Result = 0;

    while (rbnode)
    {
        tAcSndStaIPTable   *data =
            container_of (rbnode, tAcSndStaIPTable, node);

        i4Result = u4IpAddr - data->staData.u4StaIp;

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdRcv *trdRcv, int
 *                           i4TableNum
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 * ******************************************************************************/
tAcSndApMacTable   *
RbtSearchApMac (struct rb_root * root, UINT4 u4IpAddr)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcSndApMacTable   *data = container_of (rbnode,
                                                 tAcSndApMacTable, node);

        i4Result = u4IpAddr - data->apMacData.u4ipAddr;

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : ApIpTableSearch
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndIpTable      *
ApIpTableSearch (UINT4 u4IpAddr)
{
    tAcSndIpTable      *rbData = NULL;

    rbData = RbtSearchIp (&root_Ip, u4IpAddr);
    if (rbData != NULL)
    {
        // printk(KERN_INFO "rbt_search_apMac: Search Success\n");   
        return rbData;
    }
    printk (KERN_ERR "rbt_search_apMac: Search Failure\n");
    return NULL;
}

/******************************************************************************
 *  Function Name          : ApStaDBTableSearch
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndStaTable     *
ApStaDBTableSearch (UINT1 *staMac)
{

    tAcSndStaTable     *rbData = NULL;

    rbData = RbtSearchStaDB (&root_Sta_db, staMac);
    if (rbData != NULL)
    {
        // printk(KERN_INFO "rbt_search_apMac: Search Success\n");   
        return rbData;
    }
#if DBG_LVL_01
    printk (KERN_ERR "RbtSearchStaDB: Search Failure\n");
#endif
    return NULL;
}

/******************************************************************************
 *  Function Name          : ApIntfTableSearch
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndIntfTable    *
ApIntfTableSearch (UINT1 *pBssid)
{
    tAcSndIntfTable    *rbData = NULL;

    rbData = RbtSearchInt (&root_Int, pBssid);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "rbt_search_apInt: Search Success\n");
        return rbData;
    }
#if DBG_LVL_01
    printk (KERN_ERR "ApIntfTableSearch:- rbt_search_apInt: Search Failure\n");
#endif
    return NULL;
}

/******************************************************************************
 *  Function Name          : VlanTxTableSearch
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndVlanTable    *
VlanTxTableSearch (UINT1 *pIntfName, unsigned short u2VlanId)
{
    tAcSndVlanTable    *rbData = NULL;

    rbData = RbtSearchVlan (&root_Vlan, u2VlanId, pIntfName);
    if (rbData != NULL)
    {
#if DBG_LVL_01
        //printk(KERN_INFO "rbt_search_apInt: Search Success\n");
#endif
        return rbData;
    }
    printk (KERN_ERR "VlanTxTableSearch:- rbt_search_apInt: Search Failure\n");
    return NULL;
}

/******************************************************************************
 *  Function Name          : Mac_Table_Search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndMacTable     *
Mac_Table_Search (UINT1 *pMacAddr)
{
    tAcSndMacTable     *rbData = NULL;

    rbData = rbt_search_Mac (&root_Mac, pMacAddr);
    if (rbData != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "rbt_search_apInt: Search Success\n");
#endif
        return rbData;
    }

#if DBG_LVL_01
    printk (KERN_ERR "Mac_Table_Search:- rbt_search_apInt: Search Failure\n");
#endif
    return NULL;
}

/******************************************************************************
 *  Function Name          : StaIpTableSearch
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcSndStaIPTable   *
StaIpTableSearch (UINT4 u4IpAddr)
{
    tAcSndStaIPTable   *rbData = NULL;

    rbData = rbt_search_Sta_Ip (&root_Sta_Ip, u4IpAddr);
    if (rbData != NULL)
    {
#if DBG_LVL_01
        printk (KERN_INFO "rbt_search_Sta_Ip: Search Success\n");
#endif
        return rbData;
    }
#if DBG_LVL_01
    printk (KERN_ERR "rbt_search_Sta_Ip: Search Failure\n");
#endif
    return NULL;
}

/******************************************************************************
 *  Function Name          : APSndMacTableSearch 
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 * ******************************************************************************/
tAcSndApMacTable   *
APSndMacTableSearch (UINT4 u4IpAddr)
{
    tAcSndApMacTable   *rbData;

    rbData = RbtSearchApMac (&root_Ap_Mac_db, u4IpAddr);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "APSndMacTableSearch: Search Success\n");
        return rbData;
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : RbInsertIp
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertIp (struct rb_root *root_Ip, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root_Ip->rb_node, *parent = NULL;
    tAcSndIpTable      *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Ip, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    numNodes->apIpData = data->unTable.ApIpData;
    /* To Figure out where to put new node */
    while (*new)
    {
        tAcSndIpTable      *this = container_of (*new, tAcSndIpTable, node);

        i4Result = numNodes->apIpData.u4ipAddr - this->apIpData.u4ipAddr;

        parent = *new;
        if (i4Result < 0)
            new = &((*new)->rb_left);
        else if (i4Result > 0)
            new = &((*new)->rb_right);
        else
        {
            this->apIpData.u4UdpPort = numNodes->apIpData.u4UdpPort;
            this->apIpData.u4PathMTU = numNodes->apIpData.u4PathMTU;
            this->apIpData.u1macType = numNodes->apIpData.u1macType;
            this->apIpData.u1FragReassembleStatus =
                numNodes->apIpData.u1FragReassembleStatus;
            return SUCCESS;
        }
    }

    /* Add new node and rebalance tree. */
    rb_link_node (&numNodes->node, parent, new);
    rb_insert_color (&numNodes->node, root_Ip);
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbInsertInt
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertInt (struct rb_root *root_Int, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root_Int->rb_node, *parent = NULL;
    tAcSndIntfTable    *numNodes = NULL;

    /* To Figure out where to put new node or update existing node */
    while (*new)
    {
        tAcSndIntfTable    *this = container_of (*new, tAcSndIntfTable, node);

        i4Result =
            memcmp (data->unTable.ApIntfData.bssid, this->apIntData.bssid,
                    MAC_ADDR_LEN);

        parent = *new;
        if (i4Result < 0)
            new = &((*new)->rb_left);
        else if (i4Result > 0)
            new = &((*new)->rb_right);
        else
        {
            this->apIntData.u2VlanId = data->unTable.ApIntfData.u2VlanId;
            this->apIntData.u1SsidIsolation =
                data->unTable.ApIntfData.u1SsidIsolation;
            return SUCCESS;
        }
    }

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Int, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }

    numNodes->apIntData = data->unTable.ApIntfData;
    /* Add new node and rebalance tree. */
    rb_link_node (&numNodes->node, parent, new);
    rb_insert_color (&numNodes->node, root_Int);
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbInsertVlan
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertVlan (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcSndVlanTable    *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Vlan, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    numNodes->VlanData = data->unTable.VlanData;
    /* To Figure out where to put new node */
    while (*new)
    {
        tAcSndVlanTable    *this = container_of (*new, tAcSndVlanTable, node);

        i4Result = numNodes->VlanData.u2VlanId - this->VlanData.u2VlanId;
        if (i4Result == 0)
        {
            i4Result =
                memcmp (numNodes->VlanData.au1PhysicalPort,
                        this->VlanData.au1PhysicalPort, INTERFACE_LEN);
        }

        parent = *new;
        if (i4Result < 0)
            new = &((*new)->rb_left);
        else if (i4Result > 0)
            new = &((*new)->rb_right);
        else
            return FAIL;
    }

    /* Add new node and rebalance tree. */
    rb_link_node (&numNodes->node, parent, new);
    rb_insert_color (&numNodes->node, root);
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbInsertMac
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertMac (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcSndMacTable     *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Mac, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    numNodes->MacData = data->unTable.MacData;
    /* To Figure out where to put new node */
    while (*new)
    {
        tAcSndMacTable     *this = container_of (*new, tAcSndMacTable, node);

        i4Result =
            memcmp (numNodes->MacData.au1MacAddress,
                    this->MacData.au1MacAddress, MAC_ADDR_LEN);

        parent = *new;
        if (i4Result < 0)
            new = &((*new)->rb_left);
        else if (i4Result > 0)
            new = &((*new)->rb_right);
        else
            return FAIL;
    }

    /* Add new node and rebalance tree. */
    rb_link_node (&numNodes->node, parent, new);
    rb_insert_color (&numNodes->node, root);
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbInsertStaIp
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertStaIp (struct rb_root *root_Sta_Ip, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root_Sta_Ip->rb_node, *parent = NULL;
    tAcSndStaIPTable   *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Sta_Ip, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    numNodes->staData = data->unTable.StaIpData;
    /* To Figure out where to put new node */
    while (*new)
    {
        tAcSndStaIPTable   *this = container_of (*new, tAcSndStaIPTable, node);

        i4Result = numNodes->staData.u4StaIp - this->staData.u4StaIp;

        parent = *new;
        if (i4Result < 0)
            new = &((*new)->rb_left);
        else if (i4Result > 0)
            new = &((*new)->rb_right);
        else
        {
            memcpy (this->staData.bssid, numNodes->staData.bssid, MAC_ADDR_LEN);
            memcpy (this->staData.staMac, numNodes->staData.staMac,
                    MAC_ADDR_LEN);
            this->staData.u4ipAddr = numNodes->staData.u4ipAddr;
            this->staData.u1WMMEnable = numNodes->staData.u1WMMEnable;
            this->staData.u1TaggingPolicy = numNodes->staData.u1TaggingPolicy;
            this->staData.u1TrustMode = numNodes->staData.u1TrustMode;
            this->staData.u1QosProfilePriority =
                numNodes->staData.u1QosProfilePriority;
            mempool_free (numNodes, memPool_Sta_Ip);
            return SUCCESS;
        }
    }

    /* Add new node and rebalance tree. */
    rb_link_node (&numNodes->node, parent, new);
    rb_insert_color (&numNodes->node, root_Sta_Ip);
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbInsertSta
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

static INT4
RbInsertSta (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcSndStaTable     *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Sta_db, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->StaInfo = data->unTable.ApStaData;
        /* To Figure out where to put new node */
        while (*new)
        {
            tAcSndStaTable     *this =
                container_of (*new, tAcSndStaTable, node);
            i4Result =
                memcmp (numNodes->StaInfo.staMac, this->StaInfo.staMac,
                        MAC_ADDR_LEN);
            parent = *new;
            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                memcpy (this->StaInfo.bssid, numNodes->StaInfo.bssid,
                        MAC_ADDR_LEN);
                memcpy (this->StaInfo.wtpMac, numNodes->StaInfo.wtpMac,
                        MAC_ADDR_LEN);
                this->StaInfo.u4ipAddr = numNodes->StaInfo.u4ipAddr;
                this->StaInfo.u4StaIpAddr = numNodes->StaInfo.u4StaIpAddr;
                this->StaInfo.u1WebAuthStatus =
                    numNodes->StaInfo.u1WebAuthStatus;
                this->StaInfo.u1WebAuthEnable =
                    numNodes->StaInfo.u1WebAuthEnable;
                this->StaInfo.u1WMMEnable = numNodes->StaInfo.u1WMMEnable;
                this->StaInfo.u1TaggingPolicy =
                    numNodes->StaInfo.u1TaggingPolicy;
                this->StaInfo.u1TrustMode = numNodes->StaInfo.u1TrustMode;
                this->StaInfo.u1QosProfilePriority =
                    numNodes->StaInfo.u1QosProfilePriority;
                mempool_free (numNodes, memPool_Sta_db);
                return SUCCESS;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : RbInsertApMacDB
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

static INT4
RbInsertApMacDB (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcSndApMacTable   *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_Ap_Mac_db, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->apMacData = data->unTable.ApMacData;

        /* To Figure out where to put new node */
        while (*new)
        {
            tAcSndApMacTable   *this =
                container_of (*new, tAcSndApMacTable, node);
            i4Result = numNodes->apMacData.u4ipAddr - this->apMacData.u4ipAddr;

            parent = *new;

            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                this->apMacData.u4UdpPort = numNodes->apMacData.u4UdpPort;
                this->apMacData.u1macType = numNodes->apMacData.u1macType;
                this->apMacData.u4PathMTU = numNodes->apMacData.u4PathMTU;
                this->apMacData.u1FragReassembleStatus =
                    numNodes->apMacData.u1FragReassembleStatus;
                memcpy (this->apMacData.apMacAddr,
                        numNodes->apMacData.apMacAddr, MAC_ADDR_LEN);
                mempool_free (numNodes, memPool_Ap_Mac_db);
                return SUCCESS;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }

}

/******************************************************************************
 *  Function Name          : RbtDisplay
 *  Description            : This function is used to display RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/
void
RbtDisplay (int i4TableNum)
{
    struct rb_root     *root = NULL;
    struct rb_node     *node;
    UINT1               mac[MAC_ADDR_LEN];
    INT4                u4Index = 0;

    printk (KERN_INFO "\n SND MODULE Displaying Data.....\n");

    switch (i4TableNum)
    {
        case AP_IP_TABLE:
            root = &root_Ip;
            break;
        case AP_INT_TABLE:
            root = &root_Int;
            break;
        case VLAN_TABLE:
            root = &root_Vlan;
            break;
        case MAC_TABLE:
            root = &root_Mac;
            break;
        case STA_IP_TABLE:
            root = &root_Sta_Ip;
            break;
        case AP_STA_TABLE:
            root = &root_Sta_db;
            break;
        case AP_MAC_TABLE:
            root = &root_Ap_Mac_db;
            break;
        default:
            printk (KERN_ERR "rbt_search: switch reached default\n");
            return;
            /* break;                */
    }
    /* to iteratrate the tree */
    for (node = rb_first (root); node; node = rb_next (node))
    {
        switch (i4TableNum)
        {
            case AP_IP_TABLE:
                printk (KERN_INFO "\nAP_IP_TABLE\n");
                printk (KERN_INFO "ipAddr = %x\n",
                        rb_entry (node, tAcSndIpTable,
                                  node)->apIpData.u4ipAddr);
                printk (KERN_INFO "macType = %d\n",
                        rb_entry (node, tAcSndIpTable,
                                  node)->apIpData.u1macType);
                printk (KERN_INFO "Udp Port = %d\n",
                        rb_entry (node, tAcSndIpTable,
                                  node)->apIpData.u4UdpPort);
                printk (KERN_INFO "FragReassembleStatus = %d\n",
                        rb_entry (node, tAcSndIpTable,
                                  node)->apIpData.u1FragReassembleStatus);
                printk (KERN_INFO "PathMTU = %d\n",
                        rb_entry (node, tAcSndIpTable,
                                  node)->apIpData.u4PathMTU);

                break;

            case AP_INT_TABLE:
                printk (KERN_INFO "\nAP_INT_TABLE\n");
                for (u4Index = 0; u4Index < 6; u4Index++)
                    printk (KERN_INFO "bssid = %x\n",
                            rb_entry (node, tAcSndIntfTable,
                                      node)->apIntData.bssid[u4Index]);
                printk (KERN_INFO "vlan = %d\n",
                        rb_entry (node, tAcSndIntfTable,
                                  node)->apIntData.u2VlanId);
                break;

            case VLAN_TABLE:
                printk (KERN_INFO "\nSND VLAN_TABLE\n");
                printk (KERN_INFO "vlan = %d\n",
                        rb_entry (node, tAcSndVlanTable,
                                  node)->VlanData.u2VlanId);
                printk (KERN_INFO "port = %s\n",
                        rb_entry (node, tAcSndVlanTable,
                                  node)->VlanData.au1PhysicalPort);
                printk (KERN_INFO "vlan tagged = %d\n",
                        rb_entry (node, tAcSndVlanTable,
                                  node)->VlanData.u1Tagged);
                break;

            case AP_STA_TABLE:
                printk (KERN_INFO "\nAP_STA_TABLE\n");
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.staMac[u4Index];
                printk (KERN_INFO "Sta Mac - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                printk (KERN_INFO "ipAddr = %x\n",
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.u4ipAddr);
                printk (KERN_INFO "sta IP = %x\n",
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.u4StaIpAddr);
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.bssid[u4Index];
                printk (KERN_INFO "Bssid - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.wtpMac[u4Index];
                printk (KERN_INFO "**************WTP MAC - %x:%x:%x:%x:%x:%x",
                        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
                printk (KERN_INFO "webauth enable = %d\n",
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.u1WebAuthEnable);
                printk (KERN_INFO "webauth status = %d\n",
                        rb_entry (node, tAcSndStaTable,
                                  node)->StaInfo.u1WebAuthStatus);
                break;

            case MAC_TABLE:
                printk (KERN_INFO "\nSND MAC_TABLE\n");
                printk (KERN_INFO "vlan = %d\n",
                        rb_entry (node, tAcSndMacTable,
                                  node)->MacData.u2VlanId);
                for (u4Index = 0; u4Index < 6; u4Index++)
                    printk (KERN_INFO "mac = %x\n",
                            rb_entry (node, tAcSndMacTable,
                                      node)->MacData.au1MacAddress[u4Index]);
                printk (KERN_INFO "intf  = %s\n",
                        rb_entry (node, tAcSndMacTable,
                                  node)->MacData.au1PhysicalInterface);
                printk (KERN_INFO "wlan flag = %d\n",
                        rb_entry (node, tAcSndMacTable,
                                  node)->MacData.u1WlanFlag);
                break;

            case STA_IP_TABLE:
                printk (KERN_INFO "\nSND STA IP TABLE\n");
                printk (KERN_INFO "\nSta Ip - %x\n",
                        rb_entry (node, tAcSndStaIPTable,
                                  node)->staData.u4StaIp);
                for (u4Index = 0; u4Index < 6; u4Index++)
                    printk (KERN_INFO "stamac = %x\n",
                            rb_entry (node, tAcSndStaIPTable,
                                      node)->staData.staMac[u4Index]);
                break;
            case AP_MAC_TABLE:
                printk (KERN_INFO "\nAP_MAC_TABLE\n");
                printk (KERN_INFO "ipAddr = %x\n",
                        rb_entry (node, tAcSndApMacTable,
                                  node)->apMacData.u4ipAddr);
                printk (KERN_INFO "macType = %d\n",
                        rb_entry (node, tAcSndApMacTable,
                                  node)->apMacData.u1macType);
                printk (KERN_INFO "Udp Port = %d\n",
                        rb_entry (node, tAcSndApMacTable,
                                  node)->apMacData.u4UdpPort);
                printk (KERN_INFO "FragReassembleStatus = %d\n",
                        rb_entry (node, tAcSndApMacTable,
                                  node)->apMacData.u1FragReassembleStatus);
                printk (KERN_INFO "PathMTU = %d\n",
                        rb_entry (node, tAcSndApMacTable,
                                  node)->apMacData.u4PathMTU);
                printk (KERN_INFO "FragId = %d\n",
                        rb_entry (node, tAcSndApMacTable,
                                  node)->apMacData.u2FragId);
                for (u4Index = 0; u4Index < 6; u4Index++)
                    printk (KERN_INFO "MAC = %x\n",
                            rb_entry (node, tAcSndApMacTable,
                                      node)->apMacData.apMacAddr[u4Index]);

            default:
                printk (KERN_ERR "rbt_search: switch reached default\n");
                break;
        }
    }
    return;
}

/******************************************************************************
 * Function Name          : RbtDeleteApIp
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteApIp (tRBtree * rbData)
{
    tAcSndIpTable      *pData = NULL;

    pData = RbtSearchIp (&root_Ip, rbData->unTable.ApIpData.u4ipAddr);
    if (pData != NULL)
    {
        rb_erase (&pData->node, &root_Ip);
        mempool_free (&pData->node, memPool_Ip);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 * Function Name          : RbtDeleteStaDB
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteStaDB (tRBtree * rbData)
{
    tAcSndStaTable     *pData = NULL;

    pData = RbtSearchStaDB (&root_Sta_db, rbData->unTable.ApTxStaData.staMac);
    if (pData != NULL)
    {
        rb_erase (&pData->node, &root_Sta_db);
        mempool_free (&pData->node, memPool_Sta_db);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtDeleteApInt
 *  Description            : This function is used to delete RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : findData1 - ipaddr, findData2 - bssid
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtDeleteApInt (tRBtree * rbData)
{
    tAcSndIntfTable    *pData = NULL;

    pData = RbtSearchInt (&root_Int, rbData->unTable.ApIntfData.bssid);
    if (pData != NULL)
    {
        /* To delete the data */
        rb_erase (&pData->node, &root_Int);
        mempool_free (&pData->node, memPool_Int);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtDeleteApVlan
 *  Description            : This function is used to delete RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : findData1 - ipaddr, findData2 - bssid
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtDeleteApVlan (tRBtree * rbData)
{
    tAcSndVlanTable    *pData = NULL;

    pData = RbtSearchVlan (&root_Vlan, rbData->unTable.VlanData.u2VlanId,
                           rbData->unTable.VlanData.au1PhysicalPort);
    if (pData != NULL)
    {
        /* To delete the data */
        rb_erase (&pData->node, &root_Vlan);
        mempool_free (&pData->node, memPool_Vlan);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtDeleteApMac
 *  Description            : This function is used to delete RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : findData1 - ipaddr, findData2 - bssid
 *  Output(s)              : delete a node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtDeleteApMac (tRBtree * rbData)
{
    tAcSndMacTable     *pData = NULL;

    pData = rbt_search_Mac (&root_Mac, rbData->unTable.MacData.au1MacAddress);
    if (pData != NULL)
    {
        /* To delete the data */
        rb_erase (&pData->node, &root_Mac);
        mempool_free (&pData->node, memPool_Mac);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 * Function Name          : RbtDeleteStaIp
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteStaIp (tRBtree * rbData)
{
    tAcSndStaIPTable   *pData = NULL;

    pData = rbt_search_Sta_Ip (&root_Sta_Ip, rbData->unTable.StaIpData.u4StaIp);
    if (pData != NULL)
    {
        rb_erase (&pData->node, &root_Sta_Ip);
        mempool_free (&pData->node, memPool_Sta_Ip);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApIp
 *  Description            : This function is used to replace RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData,struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceApIp (tRBtree * rbData, tRBtree * replaceData)
{
    tAcSndIpTable      *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchIp (&root_Ip, rbData->unTable.ApIpData.u4ipAddr);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_Ip, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->apIpData.u4ipAddr = replaceData->unTable.ApIpData.u4ipAddr;
        numNodes->apIpData.u4UdpPort = replaceData->unTable.ApIpData.u4UdpPort;
        numNodes->apIpData.u1macType = replaceData->unTable.ApIpData.u1macType;
        numNodes->apIpData.u4PathMTU = replaceData->unTable.ApIpData.u4PathMTU;
        numNodes->apIpData.u1FragReassembleStatus =
            replaceData->unTable.ApIpData.u1FragReassembleStatus;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_Ip);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApInt
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceApInt (tRBtree * rbData, tRBtree * replaceData)
{
    tAcSndIntfTable    *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchInt (&root_Int, rbData->unTable.ApIntfData.bssid);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_Int, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->apIntData = replaceData->unTable.ApIntfData;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_Int);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApVlan
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceApVlan (tRBtree * rbData, tRBtree * replaceData)
{
    tAcSndVlanTable    *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchVlan (&root_Vlan, rbData->unTable.VlanData.u2VlanId,
                          rbData->unTable.VlanData.au1PhysicalPort);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_Vlan, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->VlanData = replaceData->unTable.VlanData;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_Vlan);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApMac
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceApMac (tRBtree * rbData, tRBtree * replaceData)
{
    tAcSndMacTable     *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = rbt_search_Mac (&root_Mac, rbData->unTable.MacData.au1MacAddress);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_Mac, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->MacData = replaceData->unTable.MacData;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_Mac);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceStaIp
 *  Description            : This function is used to replace RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData,struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceStaIp (tRBtree * rbData, tRBtree * replaceData)
{
    tAcSndStaIPTable   *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = rbt_search_Sta_Ip (&root_Sta_Ip, rbData->unTable.StaIpData.u4StaIp);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_Sta_Ip, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }

        numNodes->staData = replaceData->unTable.StaIpData;

        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_Sta_Ip);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceStaDB
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceStaDB (tRBtree * rbData, tRBtree * replaceData)
{
    tAcSndStaTable     *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchStaDB (&root_Sta_db, rbData->unTable.ApTxStaData.staMac);
    if (data != NULL)
    {
        if (memcmp
            (rbData->unTable.ApStaData.staMac,
             replaceData->unTable.ApStaData.staMac, MAC_ADDR_LEN) == 0)
        {
            numNodes = mempool_alloc (memPool_Sta_db, GFP_ATOMIC);
            if (numNodes == NULL)
            {
                printk (KERN_ERR "\n Error in allocating node \n");
                return FAIL;
            }
            numNodes->StaInfo = replaceData->unTable.ApTxStaData;
            /* To replace the data */
            rb_replace_node (&data->node, &numNodes->node, &root_Sta_db);
            return SUCCESS;
        }
    }
    printk (KERN_ERR "Error: no data to replace \n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtRemove
 *  Description            : This function is used to remove the RbData
 *  Input(s)               : rbData - Data Structure to remove
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtRemove (tRBtree * rbData, INT4 i4TableNumber)
{
    INT4                i4RetVal = 0;
    switch (i4TableNumber)
    {
        case AP_IP_TABLE:
            i4RetVal = RbtDeleteApIp (rbData);
            break;
        case AP_INT_TABLE:
            i4RetVal = RbtDeleteApInt (rbData);
            break;
        case VLAN_TABLE:
            i4RetVal = RbtDeleteApVlan (rbData);
            break;
        case MAC_TABLE:
            i4RetVal = RbtDeleteApMac (rbData);
            break;
        case STA_IP_TABLE:
            i4RetVal = RbtDeleteStaIp (rbData);
            break;
        case AP_STA_TABLE:
            i4RetVal = RbtDeleteStaDB (rbData);
            break;
        default:
            printk (" 333 %d\n", i4TableNumber);
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : RbtReplace
 *  Description            : This function is used to replace the RbData
 *  Input(s)               : rbData - Data Structure to be replaced
 *                           replaceData - Data structure to replace
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplace (tRBtree * rbData, tRBtree * replaceData, int i4TableNumber)
{
    INT4                i4RetVal = 0;
    switch (i4TableNumber)
    {
        case AP_IP_TABLE:
            i4RetVal = RbtReplaceApIp (rbData, replaceData);
            break;
        case AP_INT_TABLE:
            i4RetVal = RbtReplaceApInt (rbData, replaceData);
            break;
        case VLAN_TABLE:
            i4RetVal = RbtReplaceApVlan (rbData, replaceData);
            break;
        case MAC_TABLE:
            i4RetVal = RbtReplaceApMac (rbData, replaceData);
            break;
        case STA_IP_TABLE:
            i4RetVal = RbtReplaceStaIp (rbData, replaceData);
            break;
        case AP_STA_TABLE:
            i4RetVal = RbtReplaceStaDB (rbData, replaceData);
            break;
        default:
            printk (" 444 %d\n", i4TableNumber);
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return i4RetVal;
}

/******************************************************************************
 *  Function Name          : RbtInsert
 *  Description            : This function is used to insert in the RBTree
 *  Input(s)               : rbData - Data Structure to be inserted
 *                           i4TableNumber - Table to be modified
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtInsert (tRBtree * rbdata, int i4TableNumber)
{
    switch (i4TableNumber)
    {
        case AP_IP_TABLE:
            if (RbInsertIp (&root_Ip, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: IP Table Insert Failure\n");
                return FAIL;
            }
            break;
        case AP_INT_TABLE:
            if (RbInsertInt (&root_Int, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Intf Table Insert Failure\n");
                return FAIL;
            }
            break;
        case VLAN_TABLE:
            if (RbInsertVlan (&root_Vlan, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Vlan Table Insert Failure\n");
                return FAIL;
            }
            break;
        case MAC_TABLE:
            if (RbInsertMac (&root_Mac, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Mac Table Insert Failure\n");
                return FAIL;
            }
            break;
        case SOCKET_INFO:
            gi4CtrlUdpPort = rbdata->i4CtrlUdpPort;
            gi4DataUdpPort = gi4CtrlUdpPort + 1;
            gi4WssDataDebugMask = rbdata->i4WssDataDebugMask;
            break;
        case STA_IP_TABLE:
            if (RbInsertStaIp (&root_Sta_Ip, rbdata) != SUCCESS)
            {
                printk (KERN_INFO
                        "RbtInsert: Station IP Table Insert Failure\n");
                return FAIL;
            }
            break;
        case AP_STA_TABLE:
            if (RbInsertSta (&root_Sta_db, rbdata) != SUCCESS)
            {
                printk (KERN_INFO
                        "RbtInsert: Station DB Table Insert Failure\n");
                return FAIL;
            }

            break;
        case AP_MAC_TABLE:

#if DBG_LVL_01
            printk ("\nEntering into the case \n");
#endif
            if (RbInsertApMacDB (&root_Ap_Mac_db, rbdata) != SUCCESS)
            {
#if DBG_LVL_01
                printk (KERN_INFO "RbtInsert: Mac Table Insert Failure\n");
#endif
                return FAIL;
            }
            break;

        default:
#if DBG_LVL_01
            printk (KERN_INFO "\nInvalid Case\n");
#endif
            break;
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtTxDestroy
 *  Description            : This function is used to destroy the RBTree
 *  Input(s)               : i4TableNumber - Table to be removed
 *  Output(s)              : NONE
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtTxDestroy (INT4 i4TableNumber)
{
    switch (i4TableNumber)
    {
        case AP_IP_TABLE:
            RbtDestroyIp ();
            break;
        case AP_INT_TABLE:
            RbtDestroyInt ();
            break;
        case VLAN_TABLE:
            RbtDestroyVlan ();
            break;
        case MAC_TABLE:
            RbtDestroyMac ();
            break;
        case STA_IP_TABLE:
            RbtDestroyStaIp ();
            break;
        case AP_STA_TABLE:
            RbtDestroyStaDB ();
        default:
            printk (" 666 %d\n", i4TableNumber);
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return SUCCESS;
}

#endif
