
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcksglob.h,v 1.8 2018/02/20 10:52:54 siva Exp $
 *
 * Description: This file contains global definitions for all kernel related
 * files.
 *******************************************************************/
#ifndef _FCKS_GLOB_H_
#define _FCKS_GLOB_H_

/******************************************************************************
 *                           Include Files
 *****************************************************************************/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/in.h>
#include <linux/inet.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <linux/etherdevice.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <net/route.h>
#include <net/checksum.h>
#include <net/ip.h>
#include <linux/delay.h>
#include <linux/version.h>
#include <linux/moduleparam.h>
#include <linux/semaphore.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter_arp.h>
#include <linux/netfilter.h>
#include <linux/kthread.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/if_ether.h>
#include <linux/if_vlan.h>
#include <linux/rtc.h>
#include <net/addrconf.h>
#include "osxstd.h"
#include "srmmem.h"
#include "srmbuf.h"
#include "srmtmr.h"
#include "osxsys.h"
#include "utltrc.h"

#include "fcacapw.h"

typedef char            BOOLEAN;
typedef char            BOOL1;
typedef char            CHR1;
typedef signed char     INT1;
typedef unsigned char   UINT1;
typedef UINT1           BYTE;
typedef signed short    INT2;
typedef unsigned short  UINT2;
typedef signed int      INT4;
typedef unsigned int    UINT4;
typedef float           FLT4;
typedef double          DBL8;
typedef unsigned long   FS_ULONG;
typedef signed long     FS_LONG;

#define    FAIL                    1
#define    INTERFACE_LEN           24
#define MAC_ADDR_LEN                6
#define    IP_VERSION              4
#define    IP_HDR_LENGTH           5
#define    IP_TTL_VALUE            64

#define    ONE_BYTE                1
#define    TWO_BYTE                2
#define    THREE_BYTE              3
#define    FOUR_BYTE               4

#define    ARP_ETHERTYPE           0x806
#define    VLAN_ETHERTYPE          0x8100
#define    PNAC_ETHERTYPE          0x888e
#define    VLAN_HDR_LEN            4
#define    ETHERTYPE_LEN           2
#define    BSSID_OFFSET            9
#define    DEST_ADDR_OFFSET        4
#define    CAPWAP_HDR_OFFSET       0x1c 

#define    CAPWAP_BYTE             0x20 /* PS */
#define    DATA_BYTE               0x08 /* PS */

#define    WEBAUTH_ENABLE          1
#define    STA_AUTHENTICATED       1

#define    ARP_REQ_OFFSET          7
#define    FRAGMENTATION_ENABLE    1
#define    INNER_IP_HDR_OFFSET     58

#define    USER_PRIORITY  8
#define    PRIORITY_LAYER 2
#define    ACCESS_CATEGORIES 4

#define    WMM_ENABLE           1
#define    TAG_POLICY_PBIT      16
#define    TAG_POLICY_DSCP      96
#define    TAG_POLICY_BOTH      120

#define    TRUST_MODE           1
#define    NO_TRUST_MODE        2

#define MEM_MAX_LENGTH(Len1,Len2)   ((Len1) <= (Len2)) ? (Len2) : (Len1)

#define IS_IPV4_MULTICAST(mac) ((mac)[0] == 0x01)

#define IS_IPV6_MULTICAST(mac)  \
    ((mac)[0] == 0x33 &&         \
     (mac)[1] == 0x33)

#define IS_BROADCAST(mac)                        \
    ((mac)[0] == 0xff &&                         \
     (mac)[1] == 0xff &&                         \
     (mac)[2] == 0xff &&                         \
     (mac)[3] == 0xff &&                         \
     (mac)[4] == 0xff &&                         \
     (mac)[5] == 0xff)


#ifdef WLC_WANTED
/******************************************************************************
 *                           MACORS
 *****************************************************************************/
#define    DEVICE_NAME_AC_SND         "dprx2"
#define    MAJOR_NUMBER_AC_SND        94
#define    DEVICE_NAME_AC_RCV         "dprx3"
#define    MAJOR_NUMBER_AC_RCV        95
#define    DEVICE_NAME_DSCP        "dprx4"
#define    MAJOR_NUMBER_DSCP       96
#define    WTP_WLC_ETH_DEV_NAME    "eth0"
#define    EXT_WLC_ETH_DEV_NAME    "eth1"
#define    DEBUG_DPRX001           0
#define    OFFSET_3             3
#define    STATION_ACTIVE          1
/* ipv4 header length in bits */
#define    IHL_SIZE                4
/* UDP Ports for control and data channels */
#define    CAPWAP_UDP_CTRL_PORT    5246
#define    CAPWAP_UDP_DATA_PORT    5247
#define    CAPWAP_HDR_LENGTH       16
#define    MGMNT_BIT               0x08 /* PS */
/*IP Protocol Numbers*/
/*Ref: http://en.wikipedia.org/wiki/List_of_IP_protocol_numbers*/
#define    ICMP                    1
#define    TCP                     6
#define    CBT                     7
#define    UDP                     17
#define    DBG_LVL_01              0
#define    DBG_LVL_02              0 
#define    DBG_LVL_FRAG_01         0
/*RB Tree operations macros*/
#define    CREATE_ENTRY            100
#define    DISPLAY_ENTRY           101
#define    INSERT_ENTRY            102
#define    REMOVE_ENTRY            103
#define    REPLACE_ENTRY           104
#define    DESTROY_ENTRY           105
#define    SEARCH_ENTRY       106
#define    STATION_STATUS          107

#define    INADDR_LOOPBACK         0x7f000001
#define    RINGBUFF_SIZE           20

#define    DATA_PKT                2
#define    CTL_PKT                 1
#define    MGT_PKT                 0

#define    BYTE_SHIFT_ADDR1        4
#define    BYTE_SHIFT_ADDR2        10
#define    BYTE_SHIFT_ADDR3        16
#define    BYTE_SHIFT_ADDR4        20
#define    MACADDR_SIZE            6
#define    DATA_BYTE_VAL           0x0
#define    CTRL_BYTE_VAL           0x40
#define    MGT_BYTE_VAL            0x80
#define    SPLIT_MAC               1
#define    LOCAL_MAC               0
#define    CAPWAP_HDR_SPLIT_SIZE   8
#define    CAPWAP_HDR_LOCAL_SIZE   16
/* ++ 20140324 */
#define    TRANSPORT_HDR_SIZE      8
#define    NETWORK_HDR_OFFSET      20
#define    MAC_HDR_OFFSET          14
#define    INNER_MAC_HDR_OFFSET    44
#define    DHCP_DEST_PORT          0x43
#define    DHCP_SRC_PORT           0x44
#define    VLAN_NAME               8 
#define    ETHERTYPE_OFFSET        66
#define    DHCP_DEST_PORT_OFFSET   90
#define    DHCP_SRC_PORT_OFFSET    88
#define    UDP_PROTOCOL_OFFSET     77
#define    INNER_DEST_IP_OFFSET    84
/* -- 20140324 */

/******************************************************************************
 *                             Macros
 *******************************************************************************/
#define    MAX_NUM_NODES    10000
#define    MAX_SSID_SZ      20
#define    MAX_MAC_SZ       6


#define    AP_IP_TABLE             1
#define    AP_INT_TABLE            2
#define    AP_MAC_TABLE            3
#define    AP_STA_TABLE            4
#define    VLAN_TABLE              5
#define    MAC_TABLE               6
#define    SOCKET_INFO             7
#define    STA_IP_TABLE            8
#define    AP_QOS_TABLE            9
#define    DEBUG_INFO              11
#define    WLC_IP_ADDR             12
#define CAPWAP_HDRLEN_OFFSET                    1
#define CAPWAP_HDRLEN_FIELD_LEN                 1
#define CAPWAP_HDRLEN_MASK                      0xf8
#define CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES        2
#define CAPWAP_FLAGS_BIT_OFFSET                 3
#define CAPWAP_FLAGS_OFFSET_LEN                 1
#define CAPWAP_LAST_FRAG_BIT                    0x40
#define CAPWAP_MORE_FRAG_BIT                    0x80
#define CAPWAP_FRAG_OFFSET                      6
#define CAPWAP_FRAG_OFFSET_LEN                  2
#define MAX_CAPWAP_FRAGMENTS_SIZE               20
#define CAPWAP_ZERO_OFFSET                      0
#define CAPWAP_FRAG_OFFSET_MASK                 0xfff8
#define CAPWAP_FRAGID_OFFSET                    4
#define CAPWAP_FRAGID_OFFSET_LEN                2
#define CAPWAP_FRAG_DATA_OFFSET                 0xf8
#define CAPWAP_MAX_DATA_FRAG_INSTANCE           35
#define CAPWAP_FRAG_INSERT                      0
#define CAPWAP_FRAG_APPEND                      1
#define CAPWAP_FRAG_PREPEND                     2
#define CAPWAP_FRAG_BIT_CLEAR_OFFSET            0x3f
#define CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME    25
#define CAPWAP_REASSEMBLE_TMR                   1
#define CAPWAP_DATA_REASSENBLY_TIMEOUT          4
#define CAPWAP_ZERO_LEN                         0
#define CAPWAP_DATA_REASSEMBLY_TMR_STARTED      1
#define CAPWAP_DATA_REASSEMBLY_TMR_NOT_STARTED  0
#define CAPWAP_FRAG_POOLID                      gFacMemPoolId
#define CAPWAP_REASSEMBLE_TIME                  2000

#define DSCP_DEBUG_PACKETS                      0x01
#define DSCP_DEBUG_CONFIGURAION                 0x02


/******************************************************************************
 *                           Global structures
 *******************************************************************************/
typedef struct
{
  tTMO_DLL_NODE Link;
  tCRU_BUF_CHAIN_HEADER *pBuf;
  UINT2 u2StartOffset;
  UINT2 u2EndOffset;
  UINT2 u2FragId;
  UINT1 au1pad[2];
}tCapFragNode;


typedef struct {
    tTmrBlk  ReassemblyTimer;
    tTMO_DLL       FragList;         /* List of fragments collected so far */
    UINT4  u4FirstFragTime;          /* Time on which first fragment is received */
    UINT2  u2FragmentId;
    UINT2  u2TotalLen;
    UINT1  u1NumberOfFragments;
    UINT1       au1pad[3];
}tCapFragment;



typedef struct
{
    unsigned int    u4ipAddr;
    unsigned int    u4UdpPort;
    unsigned int    u4PathMTU;
    unsigned char   u1macType;
    unsigned char   u1FragReassembleStatus;
    unsigned short  u2FragId;
    tCapFragment    CapDataFragStream[CAPWAP_MAX_DATA_FRAG_INSTANCE];
    UINT1           u1QInitialized;
    UINT1           au1Pad[3];

}tAPIpInfo;


typedef struct
{
    unsigned char     bssid[6];
    unsigned short    u2VlanId;
    unsigned char     u1SsidIsolation;
    unsigned char     au1Pad[3];
}tApIntfInfo;

typedef struct
{
    unsigned int    u4ipAddr;
    unsigned int    u4UdpPort;
    unsigned char   apMacAddr[6];
    unsigned short  u2FragId;
    unsigned int    u4PathMTU;
    unsigned char   u1macType;
    unsigned char   u1FragReassembleStatus; 
    unsigned char   au1Pad[2]; 
}tApMacInfo;

typedef struct
{
    unsigned char staMac[6];
    unsigned char bssid[6];
    unsigned int  u4ipAddr;
    unsigned int  u4StaIpAddr;
    unsigned char u1StationStatus;
    unsigned char u1WebAuthEnable;
    unsigned char u1WebAuthStatus;
    unsigned char u1WMMEnable;
    unsigned char   wtpMac[6];
    unsigned char u1TaggingPolicy;
    unsigned char u1TrustMode;
    unsigned char u1QosProfilePriority;
    unsigned char u1LocalRouting;
    unsigned char au1Pad[1];
}tApStaInfo;

typedef struct
{
    unsigned char   au1MacAddress[6];
    unsigned short  u2VlanId;
    unsigned char   au1PhysicalInterface[INTERFACE_LEN];
    unsigned char   u1WlanFlag;
    unsigned char   au1Pad[3];
}tMacInfo;

typedef struct
{
   unsigned short   u2VlanId;
    unsigned char    au1PhysicalPort[INTERFACE_LEN];
    unsigned char    u1Tagged;
   UINT1    au1Pad[1];
}tVlanInfo;

typedef struct
{
    unsigned int    u4StaIp;
    unsigned char   staMac[6];
    unsigned char   bssid[6];
    unsigned int    u4ipAddr;
    unsigned char   u1WMMEnable;
    unsigned char   u1TaggingPolicy;
    unsigned char   u1TrustMode;
    unsigned char   u1QosProfilePriority;
}tStaIpInfo;

typedef struct
{
    UINT1           au1InterfaceName[INTERFACE_LEN]; 
    UINT1           u1dscpin;
    UINT1           u1dscpout;
    UINT1           au1pad[2]; 
}tDscpQosInfo;

typedef struct
{
    int             i4CtrlUdpPort;
    int             i4WssDataDebugMask;
    unsigned int    u4WlcIpAddr;
    union{
        tAPIpInfo    ApIpData;
        tApIntfInfo  ApIntfData;
        tApMacInfo   ApMacData;
        tApStaInfo   ApStaData;
        tApStaInfo   ApTxStaData;
        tMacInfo     MacData;
        tVlanInfo    VlanData;
        tStaIpInfo   StaIpData;
        tDscpQosInfo QosData;
    }unTable;
}tRBtree;


typedef struct
{
    unsigned char   u1RbOperation;
    int             i4TableNum;
    int             i4RbNodes;
    tRBtree         rbData;
    tRBtree         replaceData;
    unsigned char   au1VlanMac[6];
    unsigned char   au1Pad[2];
}thandleRbWlcData;
#endif

#ifdef WTP_WANTED
/******************************************************************************
 *                       MACORS used in .C file
 *****************************************************************************/
#define    NUM_OF_RADIOS           2
#define    MAX_SSID_PER_RADIO      16
#define    MAX_INTERFACE           NUM_OF_RADIOS * MAX_SSID_PER_RADIO
#define    MAX_INTERFACE_NAME_LEN  6
#define    DEVICE_NAME_AP_SND         "dprx0"
#define    MAJOR_NUMBER_AP_SND         92
#define    DEVICE_NAME_AP_RCV         "dprx1"
#define    MAJOR_NUMBER_AP_RCV         93  
#define    WTP_WLAN_DEV_NAME       "wlan0"
#define    DEVICE_NAME_DSCP        "dprx4"
#define    MAJOR_NUMBER_DSCP       96
#define    WTP_WLC_ETH_DEV_NAME    "eth0"
#define    EXT_WLC_ETH_DEV_NAME    "eth1"
#define    DEBUG_DPRX001           0

#define    WTP_LOOP_DEV_NAME        "lo"
/* ipv4 header length in bits */
#define    IHL_SIZE                4 
/* UDP Ports for control and data channels */
#define    CAPWAP_UDP_CTRL_PORT    5246
#define    WTP_ETH_DEV_NAME        "eth0"
#define    CAPWAP_UDP_DATA_PORT    5247
#define    DEFAULT_PORT            5555
#define    CAPWAP_HDR_LENGTH       16
#define    MGMNT_BIT               0x08
#define    INADDR_LOOPBACK         0x7f000001
#define    RINGBUFF_SIZE           20
#define    FF_SLL_INIT             FF_SLL_Init
#define    FF_SLL_INIT_NODE        FF_SLL_Init_Node
#define    FF_SLL_DELETE           FFDeleteNodeFromRing
#define    FF_SLL_ADD              FF_SLL_Add
#define    FF_SLL_FIRST            FF_SLL_First
#define    FF_SLL_NEXT             TMO_SLL_CIRCULAR_Next
/*IP Protocol Numbers*/
/*Ref: http://en.wikipedia.org/wiki/List_of_IP_protocol_numbers*/
#define    ICMP                    1
#define    TCP                     6
#define    CBT                     7
#define    UDP                     17
#define    DBG_LVL_01              0
#define    DBG_LVL_02              0
/*RB Tree operations macros*/
#define    CREATE_ENTRY            100
#define    DISPLAY_ENTRY           101
#define    INSERT_ENTRY            102
#define    REMOVE_ENTRY            103
#define    REPLACE_ENTRY           104
#define    DESTROY_ENTRY           105
#define    STATION_STATUS          107

#define    AP_WTP_TABLE            1
#define    AP_STA_TABLE            2
#define    ADD_DEVICE              4
#define    REMOVE_DEVICE           5
#define    AP_WLC_MAC              7

/* returns */
#define    FF_FAILURE              (0)
#define    FF_SUCCESS              (1)
#define    RB_SUCCESS              0
#define    RB_FAIL                 1

#define    DATA_PKT                2
#define    CTL_PKT                 1
#define    MGT_PKT                 0


#define    AP_QOS_TABLE            9
#define    DEBUG_INFO              11



#define    TRANSPORT_HDR_SIZE      8
#define    NETWORK_HDR_OFFSET      20
#define    MAC_HDR_OFFSET          14

#define    LOCAL_ROUTE_ENABLE      1
#define    LOCAL_ROUTE_DISABLE     2
#define    LOCAL_BRIDGE_ENABLE     3

#define    BYTE_SHIFT_ADDR1        4
#define    BYTE_SHIFT_ADDR2        10
#define    BYTE_SHIFT_ADDR3        16
#define    BYTE_SHIFT_ADDR4        20
#define    MACADDR_SIZE            6
#define    DATA_BYTE_VAL           0x0
#define    CTRL_BYTE_VAL           0x40
#define    MGT_BYTE_VAL            0x80
#define    SPLIT_MAC               1
#define    LOCAL_MAC               2
#define    CAPWAP_HDR_SPLIT_SIZE   8
#define    CAPWAP_HDR_LOCAL_SIZE   16
#define    WLC_IP_ADDRESS          "192.168.1.5"
#define    WTP_IP_ADDRESS          "192.168.1.131"
#define    TEST_MAX_NODES           2

#define MAC_SIZE    6
#define CAPWAP_HDRLEN_OFFSET                    1
#define CAPWAP_HDRLEN_FIELD_LEN                 1
#define CAPWAP_HDRLEN_MASK                      0xf8
#define CAPWAP_4BYTE_WORD_TO_NUMOF_BYTES        2
#define CAPWAP_FLAGS_BIT_OFFSET                 3
#define CAPWAP_FLAGS_OFFSET_LEN                 1
#define CAPWAP_LAST_FRAG_BIT                    0x40
#define CAPWAP_MORE_FRAG_BIT                    0x80
#define CAPWAP_FRAG_OFFSET                      6
#define CAPWAP_FRAG_OFFSET_LEN                  2
#define MAX_CAPWAP_FRAGMENTS_SIZE               20
#define CAPWAP_ZERO_OFFSET                      0
#define CAPWAP_FRAG_OFFSET_MASK                 0xfff8
#define CAPWAP_FRAGID_OFFSET                    4
#define CAPWAP_FRAGID_OFFSET_LEN                2
#define CAPWAP_FRAG_DATA_OFFSET                 0xf8
#define CAPWAP_FRAG_INSERT                      0
#define CAPWAP_FRAG_APPEND                      1
#define CAPWAP_FRAG_PREPEND                     2
#define CAPWAP_FRAG_BIT_CLEAR_OFFSET            0x3f
#define CAPWAP_MAX_NO_OF_FRAGMENTS_PER_FRAME    25
#define CAPWAP_REASSEMBLE_TMR                   1
#define CAPWAP_DATA_REASSENBLY_TIMEOUT          4
#define CAPWAP_ZERO_LEN                         0
#define CAPWAP_MAX_DATA_FRAG_INSTANCE           50
#define CAPWAP_DATA_REASSEMBLY_TMR_STARTED      1
#define CAPWAP_DATA_REASSEMBLY_TMR_NOT_STARTED  0       
#define CAPWAP_FRAG_POOLID                      gFacMemPoolId
#define CAPWAP_REASSEMBLE_TIME                  2000
#define HTTP_TYPE_LEN                           5


#define DSCP_DEBUG_PACKETS                      0x01
#define DSCP_DEBUG_CONFIGURAION                 0x02

#define    DHCP_DEST_PORT          0x43
#define    DHCP_SRC_PORT           0x44

#define   IPv4_MF                    0x2000
#define   IPv4_FRAG_OFFSET_MASK      0x1fff
#define   IPv4_PKT_OFF_FLAGS        6

#define   IPv4_MTU                  1500

#define  IPv4_PKT_ASSIGN_LEN(pBuf,u2Len)\
         {                                                                   \
            UINT2 u2Tmp;                                                     \
            u2Tmp = OSIX_HTONS(u2Len);                                         \
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *)&u2Tmp, IP_PKT_OFF_LEN, sizeof(UINT2));     \
         }

#define  IPv4_PKT_ASSIGN_FLAGS(pBuf,u2Flags)\
         {                                                                   \
            UINT2 u2Tmp;                                                     \
            u2Tmp = OSIX_HTONS(u2Flags) ;    \
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *)&u2Tmp, IPv4_PKT_OFF_FLAGS, sizeof(UINT2)) ; \
        }


#define IPv4Frag_OFF_IN_BYTES(FlgOffs)\
 ((FlgOffs & IPv4_FRAG_OFFSET_MASK) << 3)

#define   IPv4_MAC_HDR_LEN                     34

#define IPv4_OFF_IN_HDR_FORMAT(fl_offs)\
        (fl_offs >> 3)

#define FF_SLL_Init(pList) \
{\
    (pList)->Head.pNext = &(pList)->Head; \
    (pList)->Tail       = &(pList)->Head; \
    (pList)->u4_Count   = 0;\
}

#define FF_SLL_Init_Node(pNode) \
            (pNode)->pNext = NULL;

#define FF_SLL_Add(pList,pNode) \
            FFInsertIntoRing((pList),(pList)->Tail,(pNode),&(pList)->Head)

#define FF_SLL_First(pList) \
            ((((pList)->u4_Count) == 0) ? NULL: ((void *)(pList)->Head.pNext))

#define TMO_SLL_Next(pList,pNode) \
            (((pNode) == NULL) ? FF_SLL_First(pList) : \
                     (((pNode)->pNext == &(pList)->Head) ? NULL : ((void *)(pNode)->pNext)))
#define TMO_SLL_CIRCULAR_Next(pList,pNode) \
            (((pNode) == NULL) ? FF_SLL_First(pList) : \
                     (((pNode)->pNext == &(pList)->Head) ? FF_SLL_First(pList) : (pNode)->pNext))


/* a sll scan inside which a delete can be done */
#define  FF_DYNM_SLL_SCAN(pList, pNode, pTempNode, TypeCast) \
    for(((pNode) = (TypeCast)(FF_SLL_First((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                ((TypeCast)    \
                 (TMO_SLL_Next((pList),  \
                               ((tFF_SLL_NODE *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                ((TypeCast)  \
                 (TMO_SLL_Next((pList),  \
                               ((tFF_SLL_NODE *)(pNode))))))))


/******************************************************************************
 *                       Global structures
 *****************************************************************************/
typedef struct FF_SLL_NODE{
    struct FF_SLL_NODE *pNext; /**** Points to The Next Node In The List ****/
}tFF_SLL_NODE;

typedef struct FF_SLL{
    tFF_SLL_NODE Head;     /**** Header List Node *****/
    tFF_SLL_NODE *Tail;    /**** Tail Node        *****/
    u_int32_t   u4_Count; /**** Number Of Nodes In List ****/
}tFF_SLL;

/* the ring buffer node */
typedef struct {
        tFF_SLL_NODE sNextBuffer;
        struct sk_buff *pskbuf;
} tRingBuffNode;

/* the device information structure */
typedef struct DeviceInfo {
    struct net_device *pDev; /* pointer to the device structure */
    tFF_SLL sRingBuff;
    struct semaphore sRingSem;
    tRingBuffNode *psFirstValidBuffer;
    tRingBuffNode *psLastValidBuffer;
} tDevInfo;

/******************************************************************************
 *                     RB Interface Structure for WTP
 *******************************************************************************/

typedef struct
{
  tTMO_DLL_NODE Link;
  tCRU_BUF_CHAIN_HEADER *pBuf;
  UINT2 u2StartOffset;
  UINT2 u2EndOffset;
  UINT2 u2FragId;
  UINT1 au1pad[2];
}tCapFragNode;


typedef struct {
    tTmrBlk  ReassemblyTimer;
    tTMO_DLL       FragList;         /* List of fragments collected so far */
    UINT4  u4FirstFragTime;          /* Time on which first fragment is received */
    UINT2  u2FragmentId;
    UINT2  u2TotalLen;
    UINT1  u1NumberOfFragments;
    UINT1       au1pad[3];
}tCapFragment;

typedef struct
{
    unsigned int    u4ipAddr;
    unsigned int    u4UdpPort;
    unsigned int    u4PathMTU;
    unsigned char   u1macType;
    unsigned char   u1FragReassembleStatus;
    unsigned short  u2FragId;
    tCapFragment    CapDataFragStream[CAPWAP_MAX_DATA_FRAG_INSTANCE];
    UINT1           u1QInitialized;
    UINT1           au1Pad[3];

}tAPIpInfo;


typedef struct
{
    unsigned char     bssid[6];
    unsigned short    u2VlanId;
}tApIntfInfo;

typedef struct
{
    unsigned int    u4ipAddr;
    unsigned int    u4UdpPort;
    unsigned char   u1macType;
    unsigned char   apMacAddr[6];
    unsigned int    u4PathMTU;
    unsigned char   u1FragReassembleStatus; 
    unsigned short  u2FragId;
    UINT1           au1Pad[2];
}tApMacInfo;

typedef struct
{
    unsigned char staMac[6];
    unsigned char bssid[6];
    unsigned int  u4ipAddr;
    unsigned int  u4StaIpAddr;
    unsigned char u1StationStatus;
    unsigned char u1WebAuthEnable;
    unsigned char u1WebAuthStatus;
    unsigned char u1WMMEnable;
    unsigned char u1TaggingPolicy;
    unsigned char u1TrustMode;
    unsigned char u1QosProfilePriority;
    unsigned char   wtpMac[6];
}tApStaInfo;

typedef struct
{
    unsigned char   au1MacAddress[6];
    unsigned short  u2VlanId;
    unsigned char   au1PhysicalInterface[INTERFACE_LEN];
    unsigned char   u1WlanFlag;
}tMacInfo;

typedef struct
{
   unsigned short   u2VlanId;
    unsigned char    au1PhysicalPort[INTERFACE_LEN];
    unsigned char    u1Tagged;
}tVlanInfo;

typedef struct
{
    unsigned int    u4StaIp;
    unsigned char  staMac[6];
    unsigned char  bssid[6];
    unsigned int    u4ipAddr;
    unsigned char   u1WMMEnable;
    unsigned char   u1TaggingPolicy;
    unsigned char   u1TrustMode;
    unsigned char   u1QosProfilePriority;
}tStaIpInfo;

typedef struct
{
    UINT1           au1InterfaceName[INTERFACE_LEN]; 
    UINT1           u1dscpin;
    UINT1           u1dscpout;
    UINT1           au1pad[2]; 
}tDscpQosInfo;


typedef struct
{
    int             i4CtrlUdpPort;
    int             i4WssDataDebugMask;
    union{
        tAPIpInfo    ApIpData;
        tApIntfInfo  ApIntfData;
        tApMacInfo   ApMacData;
        tApStaInfo   ApStaData;
        tApStaInfo   ApTxStaData;
        tMacInfo     MacData;
        tVlanInfo    VlanData;
        tStaIpInfo   StaIpData;
 tDscpQosInfo    QosData;
    }unTable;
}tRBtree;

typedef struct
{
    unsigned short u2VlanId;
    unsigned char  staMac[6];
    unsigned char  bssid[6];
    unsigned char  u1StationStatus;
    unsigned char  u1WebAuthEnable;
    unsigned char  u1WebAuthStatus;
    unsigned char  u1WMMEnable;
    unsigned char  wlanId[INTERFACE_LEN];
    unsigned char  u1TaggingPolicy;
    unsigned char  u1TrustMode;
    unsigned char  u1QosProfilePriority;
    unsigned char  u1MacType;
    unsigned short u2TcpPort;
    unsigned char  u1ExternalWebAuth;
    unsigned char   au1Pad[3];
}twlanStaInfo;

/* structure for rbtree*/
typedef struct
{
    twlanStaInfo    staData;
    tDscpQosInfo    QosData;
    unsigned int    u4WlcIpAddr;
    unsigned int    u4WtpIpAddr;
    unsigned short  u2WlcDataPort;
    unsigned short  u2WtpDataPort;
    unsigned char   WlcMacAddr[6];
    unsigned char   u1MacType;
    unsigned char   u1LocalRouting;
    unsigned char   au1Interface[INTERFACE_LEN];
    unsigned char   au1VlanInterface[INTERFACE_LEN];
    unsigned char   au1DeviceName[INTERFACE_LEN];
    int             i4CapwapPortMtu;
    int             i4WssDataDebugMask;
    unsigned short  u2VlanId;
    unsigned char   au1Pad[2];
}tRBTreeType;

/*ioctl handle*/
typedef struct
{
     unsigned char  u1RbOperation;
     int            i4TableNum;
     int            i4RbNodes;
     tRBTreeType    rbData;
     tRBTreeType    replaceData;
}thandleRbData;

#endif

#endif /* _FCKS_GLOB_H_ */
