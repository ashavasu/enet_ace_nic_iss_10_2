
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fcacrxdb.c,v 1.5 2018/02/20 10:52:54 siva Exp $
 *
 * Description: This file contains DB related API for RCV module in WLC
 *******************************************************************/
#ifndef _FCAC_RXDB_C_
#define _FCAC_RXDB_C_

/******************************************************************************
 *                             Includes
 *******************************************************************************/
#include "fcksglob.h"
#include "fcksrxdb.h"

/* Initialize root node */
struct rb_root      root_ap_mac = RB_ROOT;
struct rb_root      root_sta = RB_ROOT;
struct rb_root      root_vlan = RB_ROOT;
struct rb_root      root_mac = RB_ROOT;
struct rb_root      root_sta_Ip = RB_ROOT;

/* Initialize mempool variable */
static mempool_t   *memPool_ap_mac = NULL;
static mempool_t   *memPool_sta = NULL;
static mempool_t   *memPool_vlan = NULL;
static mempool_t   *memPool_mac = NULL;
static mempool_t   *memPool_sta_Ip = NULL;

/*GLOBAL VARIABLES*/
INT4                gi4CtrlUdpPort;
INT4                gi4DataUdpPort;
INT4                gi4WssDataDebugMask;
UINT4               gu4WlcIpAddr;

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocApMac (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcRcvAPMacTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocApMac (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocSta (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcRcvStaTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocSta (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocVlan (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcRcvVlanTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocVlan (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocMac (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcRcvMacTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocMac (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : rbt_alloc
 *  Description            : This function is used to allocate kernel memory for 
 *                           mempool via rbt_alloc
 *  Input(s)               : gfp_t gfp_mask, void *pool_data
 *  Output(s)              : allocate kernel memory
 *  Returns                : SUCCESS/ERROR - kmalloc
 * ******************************************************************************/
static void        *
RbtAllocStaIp (gfp_t gfp_mask, void *pool_data)
{
    return kmalloc (sizeof (tAcRcvStaIpTable), gfp_mask);
}

/******************************************************************************
 *  Function Name          : rbt_dealloc
 *  Description            : This function is used to free kernel memory from 
 *                           mempool via rbt_dealloc
 *  Input(s)               : void *element, void *pool_data
 *  Output(s)              : de-allocate kernel memory
 *  Returns                : void
 * ******************************************************************************/
static void
RbtDeallocStaIp (void *element, void *pool_data)
{
    kfree (element);
}

/******************************************************************************
 *  Function Name          : RbtCreate
 *  Description            : This function is used to create RB tree memory pool 
 *                           via ioctl of wtp driver with nodes as input
 *  Input(s)               : int i4Nodes
 *  Output(s)              : mempool with reference. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtCreate (INT4 i4Nodes)
{
    /* To create the memory pool for AP Mac Table */
    memPool_ap_mac = mempool_create (i4Nodes, RbtAllocApMac,
                                     RbtDeallocApMac, NULL);
    if (memPool_ap_mac == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree \n");
        return FAIL;
    }

    /* To create the memory pool for station table */
    memPool_sta = mempool_create (i4Nodes, RbtAllocSta, RbtDeallocSta, NULL);
    if (memPool_sta == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree\n");
        return FAIL;
    }

    /* To create the memory pool for VLAN Table */
    memPool_vlan = mempool_create (i4Nodes, RbtAllocVlan, RbtDeallocVlan, NULL);
    if (memPool_vlan == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree\n");
        return FAIL;
    }

    /* To create the memory pool for Mac Table */
    memPool_mac = mempool_create (i4Nodes, RbtAllocMac, RbtDeallocMac, NULL);
    if (memPool_mac == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree\n");
        return FAIL;
    }

    /* To create the memory pool for station table */
    memPool_sta_Ip = mempool_create (i4Nodes, RbtAllocStaIp,
                                     RbtDeallocStaIp, NULL);
    if (memPool_sta_Ip == NULL)
    {
        printk (KERN_INFO "\n error in creating memory pool for rbtree\n");
        return FAIL;
    }

    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroy_ap_mac (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_ap_mac);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroySta (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_sta);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyVlan (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_vlan);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyMac (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_mac);
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to destroy RB tree memory pool 
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : destroyed memory pool 
 *  Returns                : void
 * ******************************************************************************/
void
RbtDestroyStaIp (void)
{
    /* To delete the memory pool */
    mempool_destroy (memPool_sta_Ip);
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdRcv *trdRcv, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcRcvAPMacTable   *
RbtSearchApMac (struct rb_root *root, UINT4 u4IpAddr)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcRcvAPMacTable   *data = container_of (rbnode,
                                                 tAcRcvAPMacTable, node);

        i4Result = u4IpAddr - data->apMacData.u4ipAddr;

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }

    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdRcv *trdRcv, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcRcvStaTable     *
RbtSearchSta (struct rb_root * root, UINT1 *staMac)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcRcvStaTable     *data = container_of (rbnode, tAcRcvStaTable, node);
        i4Result = memcmp (staMac, data->apStaData.staMac, MAC_ADDR_LEN);

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdRcv *trdRcv, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcRcvVlanTable    *
RbtSearchVlan (struct rb_root * root, UINT2 u2VlanId, UINT1 *pPort)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcRcvVlanTable    *data = container_of (rbnode, tAcRcvVlanTable, node);
        i4Result = u2VlanId - data->VlanData.u2VlanId;
        if (i4Result == 0)
        {
            i4Result =
                memcmp (pPort, data->VlanData.au1PhysicalPort, INTERFACE_LEN);
        }

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdRcv *trdRcv, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcRcvMacTable     *
RbtSearchMac (struct rb_root * root, UINT1 *MacAddr)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcRcvMacTable     *data = container_of (rbnode, tAcRcvMacTable, node);
        i4Result = memcmp (MacAddr, data->MacData.au1MacAddress, MAC_ADDR_LEN);

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbt_search
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : struct rb_root *root, struct rdRcv *trdRcv, int
 *                           i4TableNum
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcRcvStaIpTable   *
RbtSearchStaIp (struct rb_root * root, UINT4 u4StaIp)
{
    struct rb_node     *rbnode = root->rb_node;
    INT4                i4Result = 0;

    while (rbnode)
    {
        tAcRcvStaIpTable   *data =
            container_of (rbnode, tAcRcvStaIpTable, node);
        i4Result = u4StaIp - data->staIpData.u4StaIp;

        if (i4Result < 0)
            rbnode = rbnode->rb_left;
        else if (i4Result > 0)
            rbnode = rbnode->rb_right;
        else
        {
            return data;
        }
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : APMacTableSearch
 *  Description            : This function is used to search RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData
 *  Output(s)              : searched branch information 
 *  Returns                : data or NULL on failure case 
 * ******************************************************************************/
tAcRcvAPMacTable   *
APMacTableSearch (UINT4 u4IpAddr)
{
    tAcRcvAPMacTable   *rbData;

    rbData = RbtSearchApMac (&root_ap_mac, u4IpAddr);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "APMacTableSearch: Search Success\n");   
        return rbData;
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : APStaTableSearch
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 *******************************************************************************/
tAcRcvStaTable     *
APStaTableSearch (UINT1 *staMac)
{
    tAcRcvStaTable     *rbData;

    rbData = RbtSearchSta (&root_sta, staMac);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "APStaTableSearch: Search Success\n");
        return rbData;
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : APStaTableSearch
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 *******************************************************************************/
tAcRcvVlanTable    *
VlanRxTableSearch (UINT2 u2VlanId, UINT1 *pPort)
{
    tAcRcvVlanTable    *rbData;

    rbData = RbtSearchVlan (&root_vlan, u2VlanId, pPort);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "VlanRxTableSearch: Search Success\n");
        return rbData;
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : APStaTableSearch
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 *******************************************************************************/
tAcRcvMacTable     *
MacTableSearch (UINT1 *MacAddr)
{
    tAcRcvMacTable     *rbData;

    rbData = RbtSearchMac (&root_mac, MacAddr);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "MacTableSearch: Search Success\n");
        return rbData;
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : APSta_IP_Table_Search
 *  Description            : This function is used to search RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData01, UINT1 *findData02
 *  Output(s)              : searched branch information
 *  Returns                : data or NULL on failure case
 *******************************************************************************/
tAcRcvStaIpTable   *
APSta_IP_Table_Search (UINT4 u4IpAddr)
{
    tAcRcvStaIpTable   *rbData;

    rbData = RbtSearchStaIp (&root_sta_Ip, u4IpAddr);
    if (rbData != NULL)
    {
        //printk(KERN_INFO "APStaTableSearch: Search Success\n");
        return rbData;
    }
    return NULL;
}

/******************************************************************************
 *  Function Name          : rbInsert
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

INT4
RbInsertApMac (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcRcvAPMacTable   *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_ap_mac, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->apMacData = data->unTable.ApMacData;
        /* To Figure out where to put new node */
        while (*new)
        {
            tAcRcvAPMacTable   *this =
                container_of (*new, tAcRcvAPMacTable, node);
            i4Result = numNodes->apMacData.u4ipAddr - this->apMacData.u4ipAddr;
            parent = *new;
            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                this->apMacData.u4UdpPort = numNodes->apMacData.u4UdpPort;
                this->apMacData.u1macType = numNodes->apMacData.u1macType;
                this->apMacData.u4PathMTU = numNodes->apMacData.u4PathMTU;
                this->apMacData.u1FragReassembleStatus =
                    numNodes->apMacData.u1FragReassembleStatus;
                memcpy (this->apMacData.apMacAddr,
                        numNodes->apMacData.apMacAddr, MAC_ADDR_LEN);
                mempool_free (numNodes, memPool_ap_mac);
                return SUCCESS;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : rbInsert
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

static INT4
RbInsertSta (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcRcvStaTable     *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_sta, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->apStaData = data->unTable.ApStaData;
        /* To Figure out where to put new node */
        while (*new)
        {
            tAcRcvStaTable     *this =
                container_of (*new, tAcRcvStaTable, node);
            i4Result =
                memcmp (numNodes->apStaData.staMac, this->apStaData.staMac,
                        MAC_ADDR_LEN);
            parent = *new;
            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                memcpy (this->apStaData.bssid, numNodes->apStaData.bssid,
                        MAC_ADDR_LEN);
                memcpy (this->apStaData.wtpMac, numNodes->apStaData.wtpMac,
                        MAC_ADDR_LEN);
                this->apStaData.u4ipAddr = numNodes->apStaData.u4ipAddr;
                this->apStaData.u4StaIpAddr = numNodes->apStaData.u4StaIpAddr;
                this->apStaData.u1WebAuthStatus =
                    numNodes->apStaData.u1WebAuthStatus;
                this->apStaData.u1WebAuthEnable =
                    numNodes->apStaData.u1WebAuthEnable;
                this->apStaData.u1WMMEnable = numNodes->apStaData.u1WMMEnable;
                this->apStaData.u1TaggingPolicy =
                    numNodes->apStaData.u1TaggingPolicy;
                this->apStaData.u1TrustMode = numNodes->apStaData.u1TrustMode;
                this->apStaData.u1QosProfilePriority =
                    numNodes->apStaData.u1QosProfilePriority;
                this->apStaData.u1LocalRouting =
                    numNodes->apStaData.u1LocalRouting;
                mempool_free (numNodes, memPool_sta);
                return SUCCESS;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : rbInsert
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/

static INT4
RbInsertVlan (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcRcvVlanTable    *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_vlan, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->VlanData = data->unTable.VlanData;
        /* To Figure out where to put new node */
        while (*new)
        {
            tAcRcvVlanTable    *this =
                container_of (*new, tAcRcvVlanTable, node);
            i4Result = numNodes->VlanData.u2VlanId - this->VlanData.u2VlanId;
            if (i4Result == 0)
            {
                i4Result = memcmp (numNodes->VlanData.au1PhysicalPort,
                                   this->VlanData.au1PhysicalPort,
                                   INTERFACE_LEN);
            }
            parent = *new;
            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                return FAIL;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : rbInsert
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertMac (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcRcvMacTable     *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_mac, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->MacData = data->unTable.MacData;
        /* To Figure out where to put new node */
        while (*new)
        {
            tAcRcvMacTable     *this =
                container_of (*new, tAcRcvMacTable, node);
            i4Result =
                memcmp (numNodes->MacData.au1MacAddress,
                        this->MacData.au1MacAddress, MAC_ADDR_LEN);
            parent = *new;
            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                return FAIL;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : rbInsert
 *  Description            : This function is used to insert RB tree data  
 *                           used by RbtInsert
 *  Input(s)               : struct rb_root *root, struct rbtree_type *datai
 *                           int i4TableNum
 *  Output(s)              : filled RB node. 
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
static INT4
RbInsertStaIp (struct rb_root *root, tRBtree * data)
{
    INT4                i4Result = 0;
    struct rb_node    **new = &root->rb_node, *parent = NULL;
    tAcRcvStaIpTable   *numNodes = NULL;

    /* To check the memory allocation for the node to be inserted */
    numNodes = mempool_alloc (memPool_sta_Ip, GFP_ATOMIC);
    if (numNodes == NULL)
    {
        printk (KERN_ERR "\n Error in allocating node \n");
        return FAIL;
    }
    else
    {
        numNodes->staIpData = data->unTable.StaIpData;
        /* To Figure out where to put new node */
        while (*new)
        {
            tAcRcvStaIpTable   *this =
                container_of (*new, tAcRcvStaIpTable, node);
            i4Result = numNodes->staIpData.u4StaIp - this->staIpData.u4StaIp;
            parent = *new;
            if (i4Result < 0)
            {
                new = &((*new)->rb_left);
            }
            else if (i4Result > 0)
            {
                new = &((*new)->rb_right);
            }
            else
            {
                memcpy (this->staIpData.bssid, numNodes->staIpData.bssid,
                        MAC_ADDR_LEN);
                memcpy (this->staIpData.staMac, numNodes->staIpData.staMac,
                        MAC_ADDR_LEN);
                this->staIpData.u4ipAddr = numNodes->staIpData.u4ipAddr;
                this->staIpData.u1WMMEnable = numNodes->staIpData.u1WMMEnable;
                this->staIpData.u1TaggingPolicy =
                    numNodes->staIpData.u1TaggingPolicy;
                this->staIpData.u1TrustMode = numNodes->staIpData.u1TrustMode;
                this->staIpData.u1QosProfilePriority =
                    numNodes->staIpData.u1QosProfilePriority;
                mempool_free (numNodes, memPool_sta_Ip);
                return SUCCESS;
            }
        }

        /* Add new node and rebalance tree. */
        rb_link_node (&numNodes->node, parent, new);
        rb_insert_color (&numNodes->node, root);

        return SUCCESS;
    }
}

/******************************************************************************
 *  Function Name          : RbtDisplay
 *  Description            : This function is used to display RB tree data  
 *                           via ioctl of wtp driver
 *  Input(s)               : void
 *  Output(s)              : display all RB data
 *  Returns                : void
 * ******************************************************************************/
void
RbtDisplay (INT4 i4TableNum)
{
    struct rb_root     *root = NULL;
    struct rb_node     *node;
    UINT4               u4Index;
    UINT1               mac[MAC_ADDR_LEN];

    printk ("\nRCV MODULE Displaying Data...........\n");
    switch (i4TableNum)
    {
        case AP_MAC_TABLE:
            root = &root_ap_mac;
            break;
        case AP_STA_TABLE:
            root = &root_sta;
            break;
        case VLAN_TABLE:
            root = &root_vlan;
            break;
        case MAC_TABLE:
            root = &root_mac;
            break;
        case STA_IP_TABLE:
            root = &root_sta_Ip;
            break;
        default:
            printk (KERN_ERR "rbt_search: switch reached default\n");
            return;
            /* break;                */
    }
    /* to iteratrate the tree */
    for (node = rb_first (root); node; node = rb_next (node))
    {
        switch (i4TableNum)
        {
            case AP_MAC_TABLE:
                printk (KERN_INFO "\nAP MAC TABLE\n");
                printk (KERN_INFO "ipAddr = %x\n",
                        rb_entry (node, tAcRcvAPMacTable,
                                  node)->apMacData.u4ipAddr);
                printk (KERN_INFO "macType = %d\n",
                        rb_entry (node, tAcRcvAPMacTable,
                                  node)->apMacData.u1macType);
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcRcvAPMacTable,
                                  node)->apMacData.apMacAddr[u4Index];
                printk (KERN_INFO "Ap Mac - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                printk (KERN_INFO "port - %x",
                        rb_entry (node, tAcRcvAPMacTable,
                                  node)->apMacData.u4UdpPort);
                printk (KERN_INFO "FragReassembleStatus = %d\n",
                        rb_entry (node, tAcRcvAPMacTable,
                                  node)->apMacData.u1FragReassembleStatus);
                printk (KERN_INFO "PathMTU = %d\n",
                        rb_entry (node, tAcRcvAPMacTable,
                                  node)->apMacData.u4PathMTU);

                break;
            case AP_STA_TABLE:
                printk (KERN_INFO "\nAP_STA_TABLE\n");
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.staMac[u4Index];
                printk (KERN_INFO "Sta Mac - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                printk (KERN_INFO "ipAddr = %x\n",
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.u4ipAddr);
                printk (KERN_INFO "sta IP = %x\n",
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.u4StaIpAddr);
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.bssid[u4Index];
                printk (KERN_INFO "Bssid - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.wtpMac[u4Index];
                printk (KERN_INFO "WTP MAC - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                printk (KERN_INFO "webauth enable = %d\n",
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.u1WebAuthEnable);
                printk (KERN_INFO "webauth status = %d\n",
                        rb_entry (node, tAcRcvStaTable,
                                  node)->apStaData.u1WebAuthStatus);
                break;
            case VLAN_TABLE:
                printk ("\nRCV VLAN TABLE\n");
                printk (KERN_INFO "\nvlan - %d\n",
                        rb_entry (node, tAcRcvVlanTable,
                                  node)->VlanData.u2VlanId);
                printk (KERN_INFO "\nTagged - %d\n",
                        rb_entry (node, tAcRcvVlanTable,
                                  node)->VlanData.u1Tagged);
                printk (KERN_INFO "\nPort - %s\n",
                        rb_entry (node, tAcRcvVlanTable,
                                  node)->VlanData.au1PhysicalPort);
                break;
            case MAC_TABLE:
                printk ("\nRCV MAC TABLE\n");
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcRcvMacTable,
                                  node)->MacData.au1MacAddress[u4Index];
                printk (KERN_INFO "Mac - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                printk (KERN_INFO "\nvlan - %d\n",
                        rb_entry (node, tAcRcvMacTable,
                                  node)->MacData.u2VlanId);
                printk (KERN_INFO "\nu1WlanFlag - %d\n",
                        rb_entry (node, tAcRcvMacTable,
                                  node)->MacData.u1WlanFlag);
                printk (KERN_INFO "\nport - %s\n",
                        rb_entry (node, tAcRcvMacTable,
                                  node)->MacData.au1PhysicalInterface);
                break;
            case STA_IP_TABLE:
                printk ("\nRCV STA IP TABLE\n");
                printk (KERN_INFO "sta IP = %x\n",
                        rb_entry (node, tAcRcvStaIpTable,
                                  node)->staIpData.u4StaIp);
                for (u4Index = 0; u4Index < MAC_ADDR_LEN; u4Index++)
                    mac[u4Index] =
                        rb_entry (node, tAcRcvStaIpTable,
                                  node)->staIpData.staMac[u4Index];
                printk (KERN_INFO "Sta Mac - %x:%x:%x:%x:%x:%x", mac[0], mac[1],
                        mac[2], mac[3], mac[4], mac[5]);
                break;
            default:
                printk (KERN_ERR "rbt_search: switch reached default\n");
                break;
        }
    }
    return;
}

/******************************************************************************
 * Function Name          : RbtDeleteApMac
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteApMac (tRBtree * pData)
{
    tAcRcvAPMacTable   *rbData = NULL;

    rbData = RbtSearchApMac (&root_ap_mac, pData->unTable.ApMacData.u4ipAddr);
    if (rbData != NULL)
    {
        rb_erase (&rbData->node, &root_ap_mac);
        mempool_free (&rbData->node, memPool_ap_mac);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 * Function Name          : RbtDeleteApSta
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteApSta (tRBtree * pData)
{
    tAcRcvStaTable     *rbData = NULL;

    rbData = RbtSearchSta (&root_sta, pData->unTable.ApStaData.staMac);
    if (rbData != NULL)
    {
        rb_erase (&rbData->node, &root_sta);
        mempool_free (&rbData->node, memPool_sta);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 * Function Name          : RbtDeleteApSta
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteVlan (tRBtree * pData)
{
    tAcRcvVlanTable    *rbData = NULL;

    rbData = RbtSearchVlan (&root_vlan, pData->unTable.VlanData.u2VlanId,
                            pData->unTable.VlanData.au1PhysicalPort);
    if (rbData != NULL)
    {
        rb_erase (&rbData->node, &root_vlan);
        mempool_free (&rbData->node, memPool_vlan);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 * Function Name          : RbtDeleteApSta
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteMac (tRBtree * pData)
{
    tAcRcvMacTable     *rbData = NULL;

    rbData = RbtSearchMac (&root_mac, pData->unTable.MacData.au1MacAddress);
    if (rbData != NULL)
    {
        rb_erase (&rbData->node, &root_mac);
        mempool_free (&rbData->node, memPool_mac);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 * Function Name          : RbtDeleteStaIp
 * Description            : This function is used to delete RB tree data
 *                          via ioctl of wlc driver
 * Input(s)               : search string
 * Output(s)              : delete a node
 * Returns                : SUCCESS/FAIL
 * * ******************************************************************************/
INT4
RbtDeleteStaIp (tRBtree * pData)
{
    tAcRcvStaIpTable   *rbData = NULL;

    rbData = RbtSearchStaIp (&root_sta_Ip, pData->unTable.StaIpData.u4StaIp);
    if (rbData != NULL)
    {
        rb_erase (&rbData->node, &root_sta_Ip);
        mempool_free (&rbData->node, memPool_sta_Ip);
        return SUCCESS;
    }
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApMac
 *  Description            : This function is used to replace RB tree data  
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData,struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceApMac (tRBtree * rbData, tRBtree * replaceData)
{
    tAcRcvAPMacTable   *numNodes = NULL, *data = NULL;

    /* To search the data to be replaced */
    data = RbtSearchApMac (&root_ap_mac, rbData->unTable.ApMacData.u4ipAddr);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_ap_mac, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->apMacData = replaceData->unTable.ApMacData;

        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_ap_mac);
        return SUCCESS;
    }
    printk (KERN_ERR "Error: no data to replace \n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApSta
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceApSta (tRBtree * rbData, tRBtree * replaceData)
{
    tAcRcvStaTable     *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchSta (&root_sta, rbData->unTable.ApStaData.staMac);
    if (data != NULL)
    {
        if (memcmp
            (rbData->unTable.ApStaData.staMac,
             replaceData->unTable.ApStaData.staMac, MAC_ADDR_LEN) == 0)
        {
            numNodes = mempool_alloc (memPool_sta, GFP_ATOMIC);
            if (numNodes == NULL)
            {
                printk (KERN_ERR "\n Error in allocating node \n");
                return FAIL;
            }
            numNodes->apStaData = replaceData->unTable.ApStaData;
            /* To replace the data */
            rb_replace_node (&data->node, &numNodes->node, &root_sta);
            return SUCCESS;
        }
    }
    printk (KERN_ERR "Error: no data to replace \n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApSta
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceVlan (tRBtree * rbData, tRBtree * replaceData)
{
    tAcRcvVlanTable    *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchVlan (&root_vlan, rbData->unTable.VlanData.u2VlanId,
                          rbData->unTable.VlanData.au1PhysicalPort);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_vlan, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->VlanData = replaceData->unTable.VlanData;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_vlan);
        return SUCCESS;
    }
    printk (KERN_ERR "Error: no data to replace \n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceApSta
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceMac (tRBtree * rbData, tRBtree * replaceData)
{
    tAcRcvMacTable     *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchMac (&root_mac, rbData->unTable.MacData.au1MacAddress);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_mac, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->MacData = replaceData->unTable.MacData;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_mac);
        return SUCCESS;
    }
    printk (KERN_ERR "Error: no data to replace \n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtReplaceStaIp
 *  Description            : This function is used to replace RB tree data
 *                           via ioctl of wlc driver
 *  Input(s)               : UINT4 findData1, UINT1 *findData2,
 *                           struct rbtree_type *replaceData
 *  Output(s)              : replaced node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplaceStaIp (tRBtree * rbData, tRBtree * replaceData)
{
    tAcRcvStaIpTable   *data = NULL, *numNodes = NULL;

    /* To search the data to be replaced */
    data = RbtSearchStaIp (&root_sta_Ip, rbData->unTable.StaIpData.u4StaIp);
    if (data != NULL)
    {
        numNodes = mempool_alloc (memPool_sta_Ip, GFP_ATOMIC);
        if (numNodes == NULL)
        {
            printk (KERN_ERR "\n Error in allocating node \n");
            return FAIL;
        }
        numNodes->staIpData = replaceData->unTable.StaIpData;
        /* To replace the data */
        rb_replace_node (&data->node, &numNodes->node, &root_sta_Ip);
        return SUCCESS;
    }
    printk (KERN_ERR "Error: no data to replace \n");
    return FAIL;
}

/******************************************************************************
 *  Function Name          : RbtInsert
 *  Description            : This function is used to insert RB tree data  
 *                           via ioctl of wlc driver, with root as reference
 *  Input(s)               : struct rbtree_type *rbdata
 *  Output(s)              : inserted node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtInsert (tRBtree * rbdata, INT4 i4TableNum)
{

    switch (i4TableNum)
    {
        case AP_MAC_TABLE:
            if (RbInsertApMac (&root_ap_mac, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Insert Failure\n");
                return FAIL;
            }
            break;
        case AP_STA_TABLE:
            if (RbInsertSta (&root_sta, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Insert Failure\n");
                return FAIL;
            }
            break;
        case VLAN_TABLE:
            if (RbInsertVlan (&root_vlan, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Insert Failure\n");
                return FAIL;
            }
            break;
        case MAC_TABLE:
            if (RbInsertMac (&root_mac, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Insert Failure\n");
                return FAIL;
            }
            break;
        case SOCKET_INFO:
            gi4CtrlUdpPort = rbdata->i4CtrlUdpPort;
            gi4DataUdpPort = gi4CtrlUdpPort + 1;
            gi4WssDataDebugMask = rbdata->i4WssDataDebugMask;
            break;
        case STA_IP_TABLE:
            if (RbInsertStaIp (&root_sta_Ip, rbdata) != SUCCESS)
            {
                printk (KERN_INFO "RbtInsert: Insert Failure\n");
                return FAIL;
            }
            break;
        case WLC_IP_ADDR:
            gu4WlcIpAddr = rbdata->u4WlcIpAddr;
            break;

        default:
            printk (KERN_ERR "rbt_search: switch reached default\n");
            break;
    }
#if DBG_LVL_01
    printk (KERN_ERR "RbtInsert: Insert Success\n");
#endif
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtRemove
 *  Description            : This function is used to insert RB tree data  
 *                           via ioctl of wlc driver, with root as reference
 *  Input(s)               : struct rbtree_type *rbdata
 *  Output(s)              : inserted node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtRemove (tRBtree * rbdata, INT4 i4TableNum)
{
    switch (i4TableNum)
    {
        case AP_MAC_TABLE:
            if (RbtDeleteApMac (rbdata) != SUCCESS)
            {
                printk ("\nAP Mac Table Delete Failed\n");
                return FAIL;
            }
            break;
        case AP_STA_TABLE:
            if (RbtDeleteApSta (rbdata) != SUCCESS)
            {
#if DBG_LVL_01
                printk ("\nAP Station Table Delete Failed\n");
#endif
                return FAIL;
            }
            break;
        case VLAN_TABLE:
            if (RbtDeleteVlan (rbdata) != SUCCESS)
            {
                printk ("\nVLAN Table Delete Failed\n");
                return FAIL;
            }
            break;
        case MAC_TABLE:
            if (RbtDeleteMac (rbdata) != SUCCESS)
            {
                printk ("\n Mac Table Delete Failed\n");
                return FAIL;
            }
            break;
        case STA_IP_TABLE:
            if (RbtDeleteStaIp (rbdata) != SUCCESS)
            {
                printk ("\nAP IP Station Table Delete Failed\n");
                return FAIL;
            }
            break;
        default:
            printk (" 22 %d\n", i4TableNum);
            printk (KERN_INFO "\nInvalid case\n");
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtReplace
 *  Description            : This function is used to insert RB tree data  
 *                           via ioctl of wlc driver, with root as reference
 *  Input(s)               : struct rbtree_type *rbdata
 *  Output(s)              : inserted node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
INT4
RbtReplace (tRBtree * rbdata, tRBtree * replaceData, INT4 i4TableNum)
{
    switch (i4TableNum)
    {
        case AP_MAC_TABLE:
            if (RbtReplaceApMac (rbdata, replaceData) != SUCCESS)
            {
                printk ("\nAP Mac Table replace failed\n");
                return FAIL;
            }
            break;
        case AP_STA_TABLE:
            if (RbtReplaceApSta (rbdata, replaceData) != SUCCESS)
            {
                printk ("\nAP Sta Table replace failed\n");
                return FAIL;
            }
            break;
        case VLAN_TABLE:
            if (RbtReplaceVlan (rbdata, replaceData) != SUCCESS)
            {
                printk ("\nVLAN Table replace failed\n");
                return FAIL;
            }
            break;
        case MAC_TABLE:
            if (RbtReplaceMac (rbdata, replaceData) != SUCCESS)
            {
                printk ("\n Mac Table replace failed\n");
                return FAIL;
            }
            break;
        case STA_IP_TABLE:
            if (RbtReplaceStaIp (rbdata, replaceData) != SUCCESS)
            {
                printk ("\nAP Sta Table replace failed\n");
                return FAIL;
            }
            break;
        default:
            printk (" 111 %d\n", i4TableNum);
            printk (KERN_INFO "\nInvalid case\n");
            break;
    }
    return SUCCESS;
}

/******************************************************************************
 *  Function Name          : RbtDestroy
 *  Description            : This function is used to insert RB tree data  
 *                           via ioctl of wlc driver, with root as reference
 *  Input(s)               : struct rbtree_type *rbdata
 *  Output(s)              : inserted node
 *  Returns                : SUCCESS/FAIL
 * ******************************************************************************/
void
RbtDestroy (INT4 i4TableNum)
{
    switch (i4TableNum)
    {
        case AP_MAC_TABLE:
            RbtDestroyMac ();
            break;
        case AP_STA_TABLE:
            RbtDestroySta ();
            break;
        case VLAN_TABLE:
            RbtDestroyVlan ();
            break;
        case MAC_TABLE:
            RbtDestroyMac ();
            break;
        case STA_IP_TABLE:
            RbtDestroyStaIp ();
            break;
        default:
            printk (KERN_ERR "Invalid Case\n");
            break;
    }
    return;
}

#endif
