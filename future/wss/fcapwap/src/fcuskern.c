/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: fcuskern.c,v 1.6 2018/01/18 09:56:47 siva Exp $
 *
 * Description: This file contains the IOCTL implementations of KERNEL related 
 * configurations
 ****************************************************************************/
#include "fcusglob.h"
#include "fcusprot.h"
#include "ip.h"
#include "capwap.h"
#include "arp.h"
#include "vlaninc.h"
#include "strings.h"
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
INT4                gi4KernelWlcSndFd = 0;
INT4                gi4KernelWlcRcvFd = 0;
INT4                gi4KernelDscpRcvFd = 0;
#endif

#ifdef WTP_WANTED
INT4                gi4KernelWtpSndFd = 0;
INT4                gi4KernelWtpRcvFd = 0;
INT4                gi4KernelWtpDscpRcvFd = 0;
UINT1               gau1InterfaceName[INTERFACE_LEN] = { 0 };
UINT1               gau1VlanName[INTERFACE_LEN] = { 0 };
UINT2               gu2VlanID = 0;
UINT4               gu4WtpIpAddr = 0;
UINT1               gau1TcFilterInterface[INTERFACE_LEN] = { 0 };
#endif

extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u4IfIndex);

#ifdef WTP_WANTED
/************************************************************************/
/*  Function Name   : NpResolveArp                                      */
/*  Description     : The function is used to resolve the arp           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
NpResolveArp (UINT4 u4IpAddr, tMacAddr * pMacAddr)
{
    INT4                u4socketFd;
    struct arpreq       req;
    struct hostent     *hp;
    struct sockaddr_in *sin_addr;
    struct in_addr      ip;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4TmpIfIndex = 0;
    UINT1               u1EncapType = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = 0;
    tIpConfigInfo       IpIfInfo;
    char               *host = inet_ntoa (ip);
    UINT1              *pu1DevName = NULL;
    UINT1               u1IfType = 0;
    UINT1               au1SubIfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    tVlanIfaceVlanId    VlanId = 0;
    tVlanCurrEntry     *pCurrEntry = NULL;
    tVlanFdbEntry      *pVlanFdbEntry = NULL;
    INT4                i4PhyPort = 0;
    INT4                i4LPortNum = 0;
    INT4                i4SlotNum = 0;

    MEMSET (&IpIfInfo, 0, sizeof (tIpConfigInfo));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (au1SubIfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    RtQuery.u4DestinationIpAddress = u4IpAddr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = 0x01;

    /* Check whether a router is present between AN and AP. If yes then 
     * resolve the mac for the router IP*/
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpRtInfo.u4NextHop != 0)
        {
            u4IpAddr = NetIpRtInfo.u4NextHop;
        }
    }

    ip.s_addr = OSIX_HTONL (u4IpAddr);

    bzero ((char *) &req, sizeof (req));

    sin_addr = (struct sockaddr_in *) (VOID *) &req.arp_pa;
    sin_addr->sin_family = AF_INET;    /* Address Family: Internet */
    sin_addr->sin_addr.s_addr = OSIX_HTONL (inet_addr (host));

    if (inet_addr (host) == INADDR_NONE)
    {
        if (!(hp = gethostbyname (host)))
        {
            fprintf (stderr, "arp: %s ", host);
            herror ((char *) NULL);
            return;
        }
        bcopy ((char *) hp->h_addr, (char *) &sin_addr->sin_addr,
               sizeof (sin_addr->sin_addr));
    }
    if ((u4socketFd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("socket() failed.");
        /* exit(-1); */
        return;
    }                            /* Socket is opened. */
    close (u4socketFd);            /* Close the socket, we don't need it anymore. */

    if (req.arp_flags & ATF_COM)
    {
#ifdef DEBUG_WANTED
        MEMCPY (pMacAddr, req.arp_ha.sa_data, sizeof (tMacAddr));
#endif
    }
    else
    {
        printf ("incomplete");
    }

    if (req.arp_flags & ATF_PERM)
    {
        printf ("ATF_PERM");
    }
    if (req.arp_flags & ATF_PUBL)
    {
        printf ("ATF_PUBL");
    }
    if (req.arp_flags & ATF_USETRAILERS)
    {
        printf ("ATF_USETRAILERS");
    }

    /* Update WTP IP Address and WLC MAC */
    /* ARP protocol lock is taken within the API ArpResolveWithIndex */
    i4RetVal = CfaIpIfGetIfIndexForNetInCxt (0, u4IpAddr, &u4IfIndex);
    if (i4RetVal != CFA_SUCCESS)
    {
        return;
    }

    i4RetVal = ArpResolveWithIndex (u4IfIndex, u4IpAddr,
                                    (INT1 *) pMacAddr, &u1EncapType);

    if (i4RetVal != ARP_SUCCESS)
    {
        printf ("\n Failed to Resolve ARP for WLC IP address \n");
        return;
    }

    /* Get Interface IP address from IfIndex */
    i4RetVal = CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo);

    if (i4RetVal == CFA_SUCCESS)
    {
        gu4WtpIpAddr = IpIfInfo.u4Addr;
    }

    if ((CfaGetIfType (u4IfIndex, &u1IfType)) != CFA_SUCCESS)
    {
        /* Invalid Interface */
        /* To support basic functionality, adding eth0 as an WLC interface, in case of invalid interface */
        u4IfIndex = 1;
    }

    /* Check Interface Type and if its L3 IP VLAN, invoke Port number
     * on which the MAC address is learnt from VLAN FDB */
    if (u1IfType == CFA_L3IPVLAN)
    {
        if ((CfaGetVlanId (u4IfIndex, &VlanId)) != CFA_FAILURE)
        {
            MEMSET (gau1VlanName, 0, INTERFACE_LEN);
            SPRINTF ((CHR1 *) gau1VlanName, "vlan%d", VlanId);
            VLAN_LOCK ();
            pCurrEntry = VlanGetVlanEntry (VlanId);
            if (pCurrEntry != NULL)
            {
                pVlanFdbEntry =
                    VlanGetUcastEntry (pCurrEntry->u4FidIndex, *pMacAddr);
                if (pVlanFdbEntry != NULL)
                {
                    u4IfIndex = pVlanFdbEntry->u2Port;
                }
                else
                {
                    /* If no entry is found on FDB, Add eth0 as default WLC interface name */
                    /* Added as a workaround */
                    u4IfIndex = 1;    /* Eth0 IfIndex */
                }
            }
            VLAN_UNLOCK ();
        }
    }

    if (u1IfType == CFA_L3SUB_INTF)
    {
        CfaGetPortName ((UINT2) u4IfIndex, au1SubIfName);
        CfaGetL3SubIfParentIndex (u4IfIndex, &u4TmpIfIndex);
        CfaCliGetPhysicalAndLogicalPortNum ((INT1 *) &au1SubIfName[4],
                                            &i4PhyPort, &i4LPortNum,
                                            &i4SlotNum);
        u4IfIndex = u4TmpIfIndex;
        gu2VlanID = (UINT2) i4LPortNum;
    }
    /* Get Port Name only if the ARP is learnt against physical interface */
    if (u4IfIndex <= (SYS_DEF_MAX_PHYSICAL_INTERFACES -
                      SYS_DEF_MAX_RADIO_INTERFACES))
    {
        pu1DevName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

        MEMCPY (gau1InterfaceName, pu1DevName, STRLEN (pu1DevName));
    }
    return;
}
#endif

/************************************************************************/
/*  Function Name   : CwpKernInit                                       */
/*  Description     : The function is used to init capwap DB            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
CwpKernInit ()
{
    INT4                i4RetVal = 0;
#ifdef WLC_WANTED
    tHandleRbWlc        CapwapRbCreateWlc;
    MEMSET (&CapwapRbCreateWlc, 0, sizeof (tHandleRbWlc));
#endif
#ifdef WTP_WANTED
    tWtpKernelDB        CapwapRbCreateWtp;
    MEMSET (&CapwapRbCreateWtp, 0, sizeof (tWtpKernelDB));
#endif

#ifdef WLC_WANTED
    gi4KernelWlcSndFd = open ("/dev/dprx2", OSIX_FILE_RW);
    if (gi4KernelWlcSndFd < 0)
    {
        printf ("\nFAILED at dprx2 INIT\n");
        return;
    }

    CapwapRbCreateWlc.u1RbOperation = RB_CREATE;
    CapwapRbCreateWlc.i4RbNodes = MAX_NUM_NODES;
    MEMCPY (CapwapRbCreateWlc.au1VlanMac, IssGetSwitchMacFromNvRam (),
            sizeof (tMacAddr));
    i4RetVal = ioctl (gi4KernelWlcSndFd, 0, ((FS_ULONG *) & CapwapRbCreateWlc));
    if (i4RetVal < 0)
    {
        printf ("Failed to create the RB Tree with the maximum nodes \n");
        return;
    }

    gi4KernelWlcRcvFd = open ("/dev/dprx3", OSIX_FILE_RW);
    if (gi4KernelWlcRcvFd < 0)
    {
        printf ("\nFAILED at dprx3 INIT\n");
        return;
    }

    MEMSET (&CapwapRbCreateWlc, 0, sizeof (tHandleRbWlc));
    CapwapRbCreateWlc.u1RbOperation = RB_CREATE;
    CapwapRbCreateWlc.i4RbNodes = MAX_NUM_NODES;
    i4RetVal = ioctl (gi4KernelWlcRcvFd, 0, ((FS_ULONG *) & CapwapRbCreateWlc));
    if (i4RetVal < 0)
    {
        printf ("Failed to create the RB Tree with the maximum nodes \n");
        return;
    }
    gi4KernelDscpRcvFd = open ("/dev/dprx4", OSIX_FILE_RW);
    if (gi4KernelDscpRcvFd < 0)
    {
        return;
    }

    MEMSET (&CapwapRbCreateWlc, 0, sizeof (tHandleRbWlc));
    CapwapRbCreateWlc.u1RbOperation = RB_CREATE;
    CapwapRbCreateWlc.i4RbNodes = MAX_NUM_NODES;
    i4RetVal =
        ioctl (gi4KernelDscpRcvFd, 0, ((FS_ULONG *) & CapwapRbCreateWlc));
    if (i4RetVal < 0)
    {
        printf ("Failed to create the RB Tree with the nodes \n");
        return;
    }

#endif
#ifdef WTP_WANTED

    /* Opening the TX module */
    gi4KernelWtpSndFd = open ("/dev/dprx0", OSIX_FILE_RW);
    if (gi4KernelWtpSndFd < 0)
    {
        printf ("Failed to open the dprx module \n");
        return;
    }

    /* Allocating the memory for RB tree */
    CapwapRbCreateWtp.u1RbOperation = RB_CREATE;
    CapwapRbCreateWtp.i4RbNodes = MAX_NUM_NODES;
    i4RetVal = ioctl (gi4KernelWtpSndFd, 0, ((FS_ULONG *) & CapwapRbCreateWtp));
    if (i4RetVal < 0)
    {
        printf ("Failed to create the RB Tree with the maximum nodes \n");
        return;
    }

    /* Opening the RX module */
    gi4KernelWtpRcvFd = open ("/dev/dprx1", OSIX_FILE_RW);
    if (gi4KernelWtpRcvFd < 0)
    {
        printf ("Failed to open the dprx module \n");
        return;
    }

    /* Allocating the memory for RB tree */
    MEMSET (&CapwapRbCreateWtp, 0, sizeof (tWtpKernelDB));
    CapwapRbCreateWtp.u1RbOperation = RB_CREATE;
    CapwapRbCreateWtp.i4RbNodes = MAX_NUM_NODES;
    i4RetVal = ioctl (gi4KernelWtpRcvFd, 0, ((FS_ULONG *) & CapwapRbCreateWtp));
    if (i4RetVal < 0)
    {
        printf ("Failed to create the RB Tree with the maximum nodes \n");
        return;
    }
    gi4KernelWtpDscpRcvFd = open ("/dev/dprx4", OSIX_FILE_RW);
    if (gi4KernelWtpDscpRcvFd < 0)
    {
        return;
    }
    MEMSET (&CapwapRbCreateWtp, 0, sizeof (tWtpKernelDB));
    CapwapRbCreateWtp.u1RbOperation = RB_CREATE;
    CapwapRbCreateWtp.i4RbNodes = MAX_NUM_NODES;
    i4RetVal =
        ioctl (gi4KernelWtpDscpRcvFd, 0, ((FS_ULONG *) & CapwapRbCreateWtp));
    if (i4RetVal < 0)
    {
        printf ("Failed to create the RB Tree with the nodes \n");
        return;
    }
#endif
    return;
}

#ifdef WTP_WANTED
/************************************************************************/
/*  Function Name   : capwapNpUpdateKernel                              */
/*  Description     : The function is used to update kernel DB          */
/*  Input(s)        : u1Module - Module Id                              */
/*                    u2Field - Field Id                                */
/*                    pWssKernelDB - Strucutre to update                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/
INT4
capwapNpUpdateKernel (UINT1 u1Module, UINT2 u2Field,
                      tWtpKernelDB * pWssKernelDB)
{
    INT4                i4RetVal = 0, i4IntfId = 0;
    tMacAddr            WlcMacAddr;
    UINT1               au1NullString[INTERFACE_LEN];
    UINT1              *pu1DevName = NULL;
#ifdef DAK_WANTED
    UINT2               u2IfIndex = 1;
#else
    UINT2               u2IfIndex = 1;
#endif
    UINT4               u4IfIndex = 0;

    tWtpKernelDB        KernelDB;

    MEMSET (&WlcMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&KernelDB, 0, sizeof (tWtpKernelDB));
    MEMSET (au1NullString, 0, INTERFACE_LEN);

    switch (u1Module)
    {
        case TX_MODULE:
        {
            KernelDB.u1RbOperation = RB_INSERT;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_GLOBAL_WLC_MAC:
                {
                    MEMCPY (KernelDB.rbData.WlcMacAddr,
                            pWssKernelDB->rbData.WlcMacAddr, sizeof (tMacAddr));
                    i4RetVal =
                        CfaIpIfGetIfIndexForNetInCxt (0,
                                                      pWssKernelDB->rbData.
                                                      u4WlcIpAddr, &u4IfIndex);
                    if (i4RetVal != CFA_SUCCESS)
                    {
                        return OSIX_FAILURE;
                    }

                    pu1DevName =
                        CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

                    if (pu1DevName != NULL)
                    {
                        MEMCPY (gau1InterfaceName, pu1DevName,
                                STRLEN (pu1DevName));
                    }

                    MEMCPY (KernelDB.rbData.au1Interface,
                            gau1InterfaceName, STRLEN (gau1InterfaceName));
                }
                    break;
                case AP_GLOBALS:
                {
                    KernelDB.rbData.u2WlcDataPort =
                        pWssKernelDB->rbData.u2WlcDataPort;
                    KernelDB.rbData.u2WtpDataPort =
                        pWssKernelDB->rbData.u2WtpDataPort;
                    KernelDB.rbData.u4WlcIpAddr =
                        pWssKernelDB->rbData.u4WlcIpAddr;
                    KernelDB.rbData.u1MacType = pWssKernelDB->rbData.u1MacType;
                    KernelDB.rbData.u1LocalRouting =
                        pWssKernelDB->rbData.u1LocalRouting;
                    i4IntfId = KernelDB.rbData.au1Interface[0];

                    NpResolveArp (KernelDB.rbData.u4WlcIpAddr, &WlcMacAddr);
#ifdef DAK_WANTED
                    for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES;
                         u2IfIndex <= SYS_DEF_TGT_MAX_ENET_INTERFACES + 1;
                         u2IfIndex++)
                    {
                        pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                        MEMCPY (gau1TcFilterInterface, pu1DevName,
                                STRLEN (pu1DevName));
                        if (pWssKernelDB->rbData.u1LocalRouting != 1)
                        {
                            WssIfWmmInstallFilter (gau1TcFilterInterface);
                        }
                    }
#else
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMCPY (gau1TcFilterInterface, pu1DevName,
                            STRLEN (pu1DevName));
                    if (pWssKernelDB->rbData.u1LocalRouting != 1)
                    {
                        WssIfWmmInstallFilter (gau1TcFilterInterface);
                    }
#endif
                    /* Update WTP IP Addr as WlcInterface IP */
                    KernelDB.rbData.u4WtpIpAddr = gu4WtpIpAddr;

                    MEMCPY (KernelDB.rbData.au1Interface,
                            gau1InterfaceName, STRLEN (gau1InterfaceName));
                    MEMCPY (KernelDB.rbData.au1VlanInterface,
                            gau1VlanName, STRLEN (gau1VlanName));

                    KernelDB.rbData.u2VlanId = gu2VlanID;
                    MEMCPY (KernelDB.rbData.WlcMacAddr, WlcMacAddr,
                            sizeof (tMacAddr));
                    KernelDB.rbData.i4CapwapPortMtu =
                        pWssKernelDB->rbData.i4CapwapPortMtu;

                }
                    break;
                case AP_STATION_TABLE:
                {
                    MEMCPY (KernelDB.rbData.staData.staMac,
                            pWssKernelDB->rbData.staData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.staData.bssid,
                            pWssKernelDB->rbData.staData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.staData.u2VlanId =
                        pWssKernelDB->rbData.staData.u2VlanId;
                    KernelDB.rbData.staData.u1StationStatus =
                        pWssKernelDB->rbData.staData.u1StationStatus;
                    KernelDB.rbData.staData.u1WebAuthEnable =
                        pWssKernelDB->rbData.staData.u1WebAuthEnable;
                    KernelDB.rbData.staData.u1ExternalWebAuth =
                        pWssKernelDB->rbData.staData.u1ExternalWebAuth;
                    KernelDB.rbData.staData.u1WebAuthStatus =
                        pWssKernelDB->rbData.staData.u1WebAuthStatus;
                    KernelDB.rbData.staData.u2TcpPort =
                        pWssKernelDB->rbData.staData.u2TcpPort;
                    KernelDB.rbData.staData.u1WMMEnable =
                        pWssKernelDB->rbData.staData.u1WMMEnable;
                    MEMCPY (KernelDB.rbData.staData.wlanIntf,
                            pWssKernelDB->rbData.staData.wlanIntf,
                            INTERFACE_LEN);
                    KernelDB.rbData.staData.u1TaggingPolicy =
                        pWssKernelDB->rbData.staData.u1TaggingPolicy;
                    KernelDB.rbData.staData.u1TrustMode =
                        pWssKernelDB->rbData.staData.u1TrustMode;
                    KernelDB.rbData.staData.u1QosProfilePriority =
                        pWssKernelDB->rbData.staData.u1QosProfilePriority;
                    KernelDB.rbData.staData.u1MacType =
                        pWssKernelDB->rbData.staData.u1MacType;
                }
                    break;
                case AP_ADD_DEVICE:
                    MEMCPY (KernelDB.rbData.au1DeviceName,
                            pWssKernelDB->rbData.au1DeviceName, INTERFACE_LEN);
                    break;
                case AP_STATION_STATUS:
                {
                    KernelDB.u1RbOperation = RB_STATION_STATUS;
                    MEMCPY (KernelDB.rbData.staData.staMac,
                            pWssKernelDB->rbData.staData.staMac,
                            sizeof (tMacAddr));
                    i4RetVal =
                        ioctl (gi4KernelWtpSndFd, 0, ((FS_ULONG *) & KernelDB));
                    if (i4RetVal < 0)
                    {
                        printf ("\nKernel updation Failed\n");
                        return OSIX_FAILURE;
                    }
                    pWssKernelDB->rbData.staData.u1StationStatus =
                        KernelDB.rbData.staData.u1StationStatus;
                    return OSIX_SUCCESS;
                }
                    break;

                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWtpSndFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                printf ("\nKernel updation Failed\n");
                return OSIX_FAILURE;
            }
        }
            break;
        case RX_MODULE:
        {
            KernelDB.u1RbOperation = RB_INSERT;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_GLOBAL_WLC_MAC:
                {
                    MEMCPY (KernelDB.rbData.WlcMacAddr,
                            pWssKernelDB->rbData.WlcMacAddr, sizeof (tMacAddr));
                }
                    break;
                case AP_GLOBALS:
                {
                    KernelDB.rbData.u2WlcDataPort =
                        pWssKernelDB->rbData.u2WlcDataPort;
                    KernelDB.rbData.u2WtpDataPort =
                        pWssKernelDB->rbData.u2WtpDataPort;
                    KernelDB.rbData.u4WlcIpAddr =
                        pWssKernelDB->rbData.u4WlcIpAddr;
                    KernelDB.rbData.u1MacType = pWssKernelDB->rbData.u1MacType;
                    KernelDB.rbData.u1LocalRouting =
                        pWssKernelDB->rbData.u1LocalRouting;
                    i4IntfId = KernelDB.rbData.au1Interface[0];
                    NpResolveArp (KernelDB.rbData.u4WlcIpAddr, &WlcMacAddr);
                    /* Update WTP IP Addr as WlcInterface IP */
                    KernelDB.rbData.u4WtpIpAddr = gu4WtpIpAddr;
                    MEMCPY (KernelDB.rbData.au1Interface,
                            gau1InterfaceName, STRLEN (gau1InterfaceName));
                    MEMCPY (KernelDB.rbData.au1VlanInterface,
                            gau1VlanName, STRLEN (gau1VlanName));
                    KernelDB.rbData.u2VlanId = gu2VlanID;

                    MEMCPY (KernelDB.rbData.WlcMacAddr, WlcMacAddr,
                            sizeof (tMacAddr));
                    KernelDB.rbData.i4CapwapPortMtu =
                        pWssKernelDB->rbData.i4CapwapPortMtu;

                }
                    break;
                case AP_STATION_TABLE:
                {
                    MEMCPY (KernelDB.rbData.staData.staMac,
                            pWssKernelDB->rbData.staData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.staData.bssid,
                            pWssKernelDB->rbData.staData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.staData.u2VlanId =
                        pWssKernelDB->rbData.staData.u2VlanId;
                    MEMCPY (KernelDB.rbData.staData.wlanIntf,
                            pWssKernelDB->rbData.staData.wlanIntf,
                            INTERFACE_LEN);
                    KernelDB.rbData.staData.u1WebAuthEnable =
                        pWssKernelDB->rbData.staData.u1WebAuthEnable;
                    KernelDB.rbData.staData.u1ExternalWebAuth =
                        pWssKernelDB->rbData.staData.u1ExternalWebAuth;
                    KernelDB.rbData.staData.u1WebAuthStatus =
                        pWssKernelDB->rbData.staData.u1WebAuthStatus;
                    KernelDB.rbData.staData.u2TcpPort =
                        pWssKernelDB->rbData.staData.u2TcpPort;
                    KernelDB.rbData.staData.u1WMMEnable =
                        pWssKernelDB->rbData.staData.u1WMMEnable;
                    KernelDB.rbData.staData.u1TaggingPolicy =
                        pWssKernelDB->rbData.staData.u1TaggingPolicy;
                    KernelDB.rbData.staData.u1TrustMode =
                        pWssKernelDB->rbData.staData.u1TrustMode;
                    KernelDB.rbData.staData.u1QosProfilePriority =
                        pWssKernelDB->rbData.staData.u1QosProfilePriority;
                    KernelDB.rbData.staData.u1MacType =
                        pWssKernelDB->rbData.staData.u1MacType;
                }
                    break;
                case AP_STATION_STATUS:
                {
                    KernelDB.u1RbOperation = RB_STATION_STATUS;
                    MEMCPY (KernelDB.rbData.staData.staMac,
                            pWssKernelDB->rbData.staData.staMac,
                            sizeof (tMacAddr));
                    i4RetVal =
                        ioctl (gi4KernelWtpSndFd, 0, ((FS_ULONG *) & KernelDB));
                    if (i4RetVal < 0)
                    {
                        printf ("\nKernel updation Failed\n");
                        return OSIX_FAILURE;
                    }
                    pWssKernelDB->rbData.staData.u1StationStatus =
                        KernelDB.rbData.staData.u1StationStatus;
                    return OSIX_SUCCESS;
                }
                    break;
                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWtpRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                printf ("\nKernel updation Failed\n");
                return OSIX_FAILURE;
            }
        }
            break;
        case DSCP_MODULE:
        {
            KernelDB.u1RbOperation = RB_INSERT;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_QOS_TABLE:

                    if (MEMCMP (pWssKernelDB->rbData.qosData.au1InterfaceName,
                                au1NullString, INTERFACE_LEN) != 0)
                    {
                        MEMCPY (KernelDB.rbData.qosData.au1InterfaceName,
                                pWssKernelDB->rbData.qosData.au1InterfaceName,
                                INTERFACE_LEN);
                    }
                    else
                    {
                        MEMCPY (KernelDB.rbData.qosData.au1InterfaceName,
                                gau1InterfaceName, INTERFACE_LEN);
                    }
                    KernelDB.rbData.qosData.u1dscpin =
                        pWssKernelDB->rbData.qosData.u1dscpin;
                    KernelDB.rbData.qosData.u1dscpout =
                        pWssKernelDB->rbData.qosData.u1dscpout;
                    break;

                case DEBUG_INFO:
                    KernelDB.rbData.i4WssDataDebugMask =
                        pWssKernelDB->rbData.i4WssDataDebugMask;
                    break;

                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal =
                ioctl (gi4KernelWtpDscpRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        default:
            printf ("\nInvalid Case\n");
            break;
    }
    UNUSED_PARAM (i4IntfId);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : capwapNpKernelRemove                              */
/*  Description     : The function is used to remove kernel DB          */
/*  Input(s)        : u1Module - Module Id                              */
/*                    u2Field - Field Id                                */
/*                    pWssKernelDB - Strucutre to update                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/
INT4
capwapNpKernelRemove (UINT1 u1Module, UINT2 u2Field,
                      tWtpKernelDB * pWssKernelDB)
{
    INT4                i4RetVal = 0;
    tWtpKernelDB        KernelDB;

    MEMSET (&KernelDB, 0, sizeof (tWtpKernelDB));
    switch (u1Module)
    {
        case TX_MODULE:
        {
            switch (u2Field)
            {
                case AP_GLOBALS:
                    KernelDB.u1RbOperation = RB_REMOVE;
                    KernelDB.i4TableNum = AP_GLOBALS;
                    break;
                case AP_STATION_TABLE:
                    KernelDB.u1RbOperation = RB_REMOVE;
                    KernelDB.i4TableNum = AP_STATION_TABLE;
                    MEMCPY (KernelDB.rbData.staData.staMac,
                            pWssKernelDB->rbData.staData.staMac,
                            sizeof (tMacAddr));
                    break;
                case AP_STATION_DESTROY:
                    KernelDB.u1RbOperation = RB_DESTROY;
                    KernelDB.i4TableNum = AP_STATION_TABLE;
                    break;
                case AP_REMOVE_DEVICE:
                    KernelDB.u1RbOperation = RB_REMOVE;
                    KernelDB.i4TableNum = AP_REMOVE_DEVICE;
                    MEMCPY (KernelDB.rbData.au1DeviceName,
                            pWssKernelDB->rbData.au1DeviceName, INTERFACE_LEN);
                    break;
                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWtpSndFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                printf ("\nKernel updation Failed\n");
                return OSIX_FAILURE;
            }
        }
            break;
        case RX_MODULE:
        {
            switch (u2Field)
            {
                case AP_GLOBALS:
                    KernelDB.u1RbOperation = RB_REMOVE;
                    KernelDB.i4TableNum = AP_GLOBALS;
                    break;
                case AP_STATION_TABLE:
                    KernelDB.u1RbOperation = RB_REMOVE;
                    KernelDB.i4TableNum = AP_STATION_TABLE;
                    MEMCPY (KernelDB.rbData.staData.staMac,
                            pWssKernelDB->rbData.staData.staMac,
                            sizeof (tMacAddr));
                    break;
                case AP_STATION_DESTROY:
                    KernelDB.u1RbOperation = RB_DESTROY;
                    KernelDB.i4TableNum = AP_STATION_TABLE;
                    break;
                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWtpRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                printf ("\nKernel updation Failed\n");
                return OSIX_FAILURE;
            }
        }
            break;
        case DSCP_MODULE:
        {
            KernelDB.u1RbOperation = RB_REMOVE;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_QOS_TABLE:
                    MEMCPY (KernelDB.rbData.qosData.au1InterfaceName,
                            pWssKernelDB->rbData.qosData.au1InterfaceName,
                            INTERFACE_LEN);
                    KernelDB.rbData.qosData.u1dscpin =
                        pWssKernelDB->rbData.qosData.u1dscpin;
                    break;

                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal =
                ioctl (gi4KernelWtpDscpRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        default:
            printf ("\nInvalid Case\n");
            break;
    }
    return OSIX_SUCCESS;
}

#endif

#ifdef WLC_WANTED
/************************************************************************/
/*  Function Name   : capwapNpUpdateKernelDBWlc                         */
/*  Description     : The function is used to update kernel DB          */
/*  Input(s)        : u1Module - Module Id                              */
/*                    u2Field - Field Id                                */
/*                    pWssKernelDB - Strucutre to update                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/
INT4
capwapNpUpdateKernelDBWlc (UINT1 u1Module, UINT2 u2Field,
                           tHandleRbWlc * pWssKernelDB)
{
    INT4                i4RetVal = 0;
    tHandleRbWlc        KernelDB;

    MEMSET (&KernelDB, 0, sizeof (tHandleRbWlc));
    switch (u1Module)
    {
        case TX_MODULE:
        {
            KernelDB.u1RbOperation = RB_INSERT;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_IP_TABLE:
                    KernelDB.rbData.unTable.apIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apIpData.u4ipAddr;
                    KernelDB.rbData.unTable.apIpData.u4UdpPort =
                        pWssKernelDB->rbData.unTable.apIpData.u4UdpPort;
                    KernelDB.rbData.unTable.apIpData.u1macType =
                        pWssKernelDB->rbData.unTable.apIpData.u1macType;
                    KernelDB.rbData.unTable.apIpData.u1FragReassembleStatus =
                        pWssKernelDB->rbData.unTable.apIpData.
                        u1FragReassembleStatus;
                    KernelDB.rbData.unTable.apIpData.u4PathMTU =
                        pWssKernelDB->rbData.unTable.apIpData.u4PathMTU;
                    KernelDB.rbData.unTable.apIpData.u1QInitialized = 0;
                    break;
                case AP_INT_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apIntData.bssid,
                            pWssKernelDB->rbData.unTable.apIntData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apIntData.u2VlanId =
                        pWssKernelDB->rbData.unTable.apIntData.u2VlanId;
                    KernelDB.rbData.unTable.apIntData.u1SsidIsolation =
                        pWssKernelDB->rbData.unTable.apIntData.u1SsidIsolation;
                    break;
                case VLAN_TABLE:
                    KernelDB.rbData.unTable.VlanData.u2VlanId =
                        pWssKernelDB->rbData.unTable.VlanData.u2VlanId;
                    MEMCPY (KernelDB.rbData.unTable.VlanData.au1PhysicalPort,
                            pWssKernelDB->rbData.unTable.VlanData.
                            au1PhysicalPort, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.VlanData.u1Tagged =
                        pWssKernelDB->rbData.unTable.VlanData.u1Tagged;
                    break;
                case MAC_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.MacData.au1MacAddress,
                            pWssKernelDB->rbData.unTable.MacData.au1MacAddress,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.MacData.
                            au1PhysicalInterface,
                            pWssKernelDB->rbData.unTable.MacData.
                            au1PhysicalInterface, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.MacData.u2VlanId =
                        pWssKernelDB->rbData.unTable.MacData.u2VlanId;
                    KernelDB.rbData.unTable.MacData.u1WlanFlag =
                        pWssKernelDB->rbData.unTable.MacData.u1WlanFlag;
                    break;
                case SOCKET_INFO:
                    KernelDB.rbData.i4CtrlUdpPort =
                        pWssKernelDB->rbData.i4CtrlUdpPort;
                    KernelDB.rbData.i4WssDataDebugMask =
                        pWssKernelDB->rbData.i4WssDataDebugMask;
                    break;
                case STA_IP_TABLE:
                    KernelDB.rbData.unTable.StaIpData.u4StaIp =
                        pWssKernelDB->rbData.unTable.StaIpData.u4StaIp;
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.staMac,
                            pWssKernelDB->rbData.unTable.StaIpData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.bssid,
                            pWssKernelDB->rbData.unTable.StaIpData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.StaIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.StaIpData.u4ipAddr;
                    KernelDB.rbData.unTable.StaIpData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.StaIpData.u1WMMEnable;
                    KernelDB.rbData.unTable.StaIpData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.StaIpData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TrustMode;
                    KernelDB.rbData.unTable.StaIpData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.StaIpData.
                        u1QosProfilePriority;
                    break;
                case AP_STA_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apStaData.staMac,
                            pWssKernelDB->rbData.unTable.apStaData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.apStaData.bssid,
                            pWssKernelDB->rbData.unTable.apStaData.bssid,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.apStaData.wtpMac,
                            pWssKernelDB->rbData.unTable.apStaData.wtpMac,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apStaData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apStaData.u4ipAddr;
                    KernelDB.rbData.unTable.apStaData.u4StaIpAddr =
                        pWssKernelDB->rbData.unTable.apStaData.u4StaIpAddr;
                    KernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
                        pWssKernelDB->rbData.unTable.apStaData.u1WebAuthEnable;
                    KernelDB.rbData.unTable.apStaData.u1WebAuthStatus =
                        pWssKernelDB->rbData.unTable.apStaData.u1WebAuthStatus;
                    KernelDB.rbData.unTable.apStaData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.apStaData.u1WMMEnable;
                    KernelDB.rbData.unTable.apStaData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.apStaData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.apStaData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.apStaData.u1TrustMode;
                    KernelDB.rbData.unTable.apStaData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.apStaData.
                        u1QosProfilePriority;
                    /*Kernel programming is needed only in case of centralised forwarding
                     *and webauth in local routing*/
                    if ((pWssKernelDB->rbData.unTable.apStaData.
                         u1LocalRouting == 2)
                        || (KernelDB.rbData.unTable.apStaData.u1WebAuthEnable ==
                            1))
                    {
                        KernelDB.rbData.unTable.apStaData.u1LocalRouting =
                            pWssKernelDB->rbData.unTable.apStaData.
                            u1LocalRouting;
                    }
                    else
                    {
                        return OSIX_SUCCESS;
                    }
                    break;
                case STATION_STATUS:
                    KernelDB.u1RbOperation = RB_STATION_STATUS;
                    MEMCPY (KernelDB.rbData.unTable.apStaInfo.staMac,
                            pWssKernelDB->rbData.unTable.apStaInfo.staMac,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apStaInfo.u1StationStatus = 10;
                    i4RetVal =
                        ioctl (gi4KernelWlcSndFd, 0, ((FS_ULONG *) & KernelDB));
                    if (i4RetVal < 0)
                    {
                        return OSIX_FAILURE;
                    }
                    printf ("\nStation Status: %d\n",
                            KernelDB.rbData.unTable.apStaInfo.u1StationStatus);
                    KernelDB.rbData.unTable.apStaInfo.u1StationStatus =
                        pWssKernelDB->rbData.unTable.apStaInfo.u1StationStatus;
                    return OSIX_SUCCESS;

                case AP_MAC_TABLE:

                    KernelDB.rbData.unTable.apMacData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apMacData.u4ipAddr;
                    KernelDB.rbData.unTable.apMacData.u4UdpPort =
                        pWssKernelDB->rbData.unTable.apMacData.u4UdpPort;
                    KernelDB.rbData.unTable.apMacData.u1macType =
                        pWssKernelDB->rbData.unTable.apMacData.u1macType;
                    MEMCPY (KernelDB.rbData.unTable.apMacData.apMacAddr,
                            pWssKernelDB->rbData.unTable.apMacData.apMacAddr,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apMacData.u4PathMTU =
                        pWssKernelDB->rbData.unTable.apMacData.u4PathMTU;
                    KernelDB.rbData.unTable.apMacData.u1FragReassembleStatus =
                        pWssKernelDB->rbData.unTable.apMacData.
                        u1FragReassembleStatus;
                    break;
                case WLC_IP_ADDR:
                    KernelDB.rbData.u4WlcIpAddr =
                        pWssKernelDB->rbData.u4WlcIpAddr;
                    break;

                default:
                    printf ("\nInvalid case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWlcSndFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }

        }
            break;
        case RX_MODULE:
        {
            KernelDB.u1RbOperation = RB_INSERT;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_MAC_TABLE:
                    KernelDB.rbData.unTable.apMacData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apMacData.u4ipAddr;
                    KernelDB.rbData.unTable.apMacData.u4UdpPort =
                        pWssKernelDB->rbData.unTable.apMacData.u4UdpPort;
                    KernelDB.rbData.unTable.apMacData.u1macType =
                        pWssKernelDB->rbData.unTable.apMacData.u1macType;
                    MEMCPY (KernelDB.rbData.unTable.apMacData.apMacAddr,
                            pWssKernelDB->rbData.unTable.apMacData.apMacAddr,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apMacData.u4PathMTU =
                        pWssKernelDB->rbData.unTable.apMacData.u4PathMTU;
                    KernelDB.rbData.unTable.apMacData.u1FragReassembleStatus =
                        pWssKernelDB->rbData.unTable.apMacData.
                        u1FragReassembleStatus;

                    break;
                case AP_STA_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apStaData.staMac,
                            pWssKernelDB->rbData.unTable.apStaData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.apStaData.bssid,
                            pWssKernelDB->rbData.unTable.apStaData.bssid,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.apStaData.wtpMac,
                            pWssKernelDB->rbData.unTable.apStaData.wtpMac,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apStaData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apStaData.u4ipAddr;
                    KernelDB.rbData.unTable.apStaData.u4StaIpAddr =
                        pWssKernelDB->rbData.unTable.apStaData.u4StaIpAddr;
                    KernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
                        pWssKernelDB->rbData.unTable.apStaData.u1WebAuthEnable;
                    KernelDB.rbData.unTable.apStaData.u1WebAuthStatus =
                        pWssKernelDB->rbData.unTable.apStaData.u1WebAuthStatus;
                    KernelDB.rbData.unTable.apStaData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.apStaData.u1WMMEnable;
                    KernelDB.rbData.unTable.apStaData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.apStaData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.apStaData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.apStaData.u1TrustMode;
                    KernelDB.rbData.unTable.apStaData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.apStaData.
                        u1QosProfilePriority;
                    KernelDB.rbData.unTable.apStaData.u1LocalRouting =
                        pWssKernelDB->rbData.unTable.apStaData.u1LocalRouting;
                    /*Kernel programming is needed only in case of centralised forwarding
                     *and webauth in local routing*/
                    if ((pWssKernelDB->rbData.unTable.apStaData.
                         u1LocalRouting == 2)
                        || (KernelDB.rbData.unTable.apStaData.u1WebAuthEnable ==
                            1))
                    {
                        break;
                    }
                    else
                    {
                        return OSIX_SUCCESS;
                    }
                case VLAN_TABLE:
                    KernelDB.rbData.unTable.VlanData.u2VlanId =
                        pWssKernelDB->rbData.unTable.VlanData.u2VlanId;
                    MEMCPY (KernelDB.rbData.unTable.VlanData.au1PhysicalPort,
                            pWssKernelDB->rbData.unTable.VlanData.
                            au1PhysicalPort, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.VlanData.u1Tagged =
                        pWssKernelDB->rbData.unTable.VlanData.u1Tagged;
                    break;
                case MAC_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.MacData.au1MacAddress,
                            pWssKernelDB->rbData.unTable.MacData.au1MacAddress,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.MacData.
                            au1PhysicalInterface,
                            pWssKernelDB->rbData.unTable.MacData.
                            au1PhysicalInterface, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.MacData.u2VlanId =
                        pWssKernelDB->rbData.unTable.MacData.u2VlanId;
                    KernelDB.rbData.unTable.MacData.u1WlanFlag =
                        pWssKernelDB->rbData.unTable.MacData.u1WlanFlag;
                    break;
                case SOCKET_INFO:
                    KernelDB.rbData.i4CtrlUdpPort =
                        pWssKernelDB->rbData.i4CtrlUdpPort;
                    KernelDB.rbData.i4WssDataDebugMask =
                        pWssKernelDB->rbData.i4WssDataDebugMask;
                    break;
                case STA_IP_TABLE:
                    KernelDB.rbData.unTable.StaIpData.u4StaIp =
                        pWssKernelDB->rbData.unTable.StaIpData.u4StaIp;
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.staMac,
                            pWssKernelDB->rbData.unTable.StaIpData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.bssid,
                            pWssKernelDB->rbData.unTable.StaIpData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.StaIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.StaIpData.u4ipAddr;
                    KernelDB.rbData.unTable.StaIpData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.StaIpData.u1WMMEnable;
                    KernelDB.rbData.unTable.StaIpData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.StaIpData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TrustMode;
                    KernelDB.rbData.unTable.StaIpData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.StaIpData.
                        u1QosProfilePriority;
                    break;
                case STATION_STATUS:
                    KernelDB.u1RbOperation = RB_STATION_STATUS;
                    MEMCPY (KernelDB.rbData.unTable.apStaData.staMac,
                            pWssKernelDB->rbData.unTable.apStaData.staMac,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apStaData.u1StationStatus = 10;
                    i4RetVal =
                        ioctl (gi4KernelWlcRcvFd, 0, ((FS_ULONG *) & KernelDB));
                    if (i4RetVal < 0)
                    {
                        return OSIX_FAILURE;
                    }
                    printf ("\nStation Status : %d\n",
                            KernelDB.rbData.unTable.apStaData.u1StationStatus);
                    KernelDB.rbData.unTable.apStaData.u1StationStatus =
                        pWssKernelDB->rbData.unTable.apStaData.u1StationStatus;
                    return OSIX_SUCCESS;

                case WLC_IP_ADDR:
                    KernelDB.rbData.u4WlcIpAddr =
                        pWssKernelDB->rbData.u4WlcIpAddr;
                    break;

                default:
                    printf ("\nInvalid case\n");
                    break;
            }

            i4RetVal = ioctl (gi4KernelWlcRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        case DSCP_MODULE:
        {
            KernelDB.u1RbOperation = RB_INSERT;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_QOS_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.qosData.au1InterfaceName,
                            pWssKernelDB->rbData.unTable.qosData.
                            au1InterfaceName, INTERFACE_LEN);
                    KernelDB.rbData.unTable.qosData.u1dscpin =
                        pWssKernelDB->rbData.unTable.qosData.u1dscpin;
                    KernelDB.rbData.unTable.qosData.u1dscpout =
                        pWssKernelDB->rbData.unTable.qosData.u1dscpout;
                    break;

                case DEBUG_INFO:
                    KernelDB.rbData.i4WssDataDebugMask =
                        pWssKernelDB->rbData.i4WssDataDebugMask;
                    break;

                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelDscpRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        default:
            printf ("\nInvalid case\n");
            break;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : capwapNpRemoveKernelDBWlc                         */
/*  Description     : The function is used to remove kernel DB          */
/*  Input(s)        : u1Module - Module Id                              */
/*                    u2Field - Field Id                                */
/*                    pWssKernelDB - Strucutre to update                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/
INT4
capwapNpRemoveKernelDBWlc (UINT1 u1Module, UINT2 u2Field,
                           tHandleRbWlc * pWssKernelDB)
{
    INT4                i4RetVal = 0;
    tHandleRbWlc        KernelDB;

    MEMSET (&KernelDB, 0, sizeof (tHandleRbWlc));

    switch (u1Module)
    {
        case TX_MODULE:
        {
            KernelDB.u1RbOperation = RB_REMOVE;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_IP_TABLE:
                    KernelDB.rbData.unTable.apIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apIpData.u4ipAddr;
                    break;
                case AP_INT_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apIntData.bssid,
                            pWssKernelDB->rbData.unTable.apIntData.bssid,
                            sizeof (tMacAddr));
                    break;
                case VLAN_TABLE:
                    KernelDB.rbData.unTable.VlanData.u2VlanId =
                        pWssKernelDB->rbData.unTable.VlanData.u2VlanId;
                    MEMCPY (KernelDB.rbData.unTable.VlanData.au1PhysicalPort,
                            pWssKernelDB->rbData.unTable.VlanData.
                            au1PhysicalPort, sizeof (tMacAddr));
                    break;
                case MAC_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.MacData.au1MacAddress,
                            pWssKernelDB->rbData.unTable.MacData.au1MacAddress,
                            sizeof (tMacAddr));
                    break;
                case STA_IP_TABLE:
                    KernelDB.rbData.unTable.StaIpData.u4StaIp =
                        pWssKernelDB->rbData.unTable.StaIpData.u4StaIp;
                    break;
                case AP_STA_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apStaInfo.staMac,
                            pWssKernelDB->rbData.unTable.apStaInfo.staMac,
                            sizeof (tMacAddr));
                    break;
                default:
                    printf ("\nInvalid case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWlcSndFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        case RX_MODULE:
        {
            KernelDB.u1RbOperation = RB_REMOVE;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_MAC_TABLE:
                    KernelDB.rbData.unTable.apMacData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apMacData.u4ipAddr;
                    break;
                case AP_STA_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apStaData.staMac,
                            pWssKernelDB->rbData.unTable.apStaData.staMac,
                            sizeof (tMacAddr));
                    break;
                case VLAN_TABLE:
                    KernelDB.rbData.unTable.VlanData.u2VlanId =
                        pWssKernelDB->rbData.unTable.VlanData.u2VlanId;
                    MEMCPY (KernelDB.rbData.unTable.VlanData.au1PhysicalPort,
                            pWssKernelDB->rbData.unTable.VlanData.
                            au1PhysicalPort, sizeof (tMacAddr));
                    break;
                case MAC_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.MacData.au1MacAddress,
                            pWssKernelDB->rbData.unTable.MacData.au1MacAddress,
                            sizeof (tMacAddr));
                    break;
                case STA_IP_TABLE:
                    KernelDB.rbData.unTable.StaIpData.u4StaIp =
                        pWssKernelDB->rbData.unTable.StaIpData.u4StaIp;
                    break;
                default:
                    printf ("\nInvalid case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelWlcRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        case DSCP_MODULE:
        {
            KernelDB.u1RbOperation = RB_REMOVE;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_QOS_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.qosData.au1InterfaceName,
                            pWssKernelDB->rbData.unTable.qosData.
                            au1InterfaceName, INTERFACE_LEN);
                    KernelDB.rbData.unTable.qosData.u1dscpin =
                        pWssKernelDB->rbData.unTable.qosData.u1dscpin;
                    break;

                default:
                    printf ("\nInvalid Case\n");
                    break;
            }
            i4RetVal = ioctl (gi4KernelDscpRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
        }
            break;
        default:
            printf ("\nInvalid Case\n");
            break;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : capwapNpReplaceKernelDBWlc                        */
/*  Description     : The function is used to replace kernel DB         */
/*  Input(s)        : u1Module - Module Id                              */
/*                    u2Field - Field Id                                */
/*                    pWssKernelDB - Strucutre to update                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/
INT4
capwapNpReplaceKernelDBWlc (UINT1 u1Module, UINT2 u2Field,
                            tHandleRbWlc * pWssKernelDB)
{
    INT4                i4RetVal = 0;
    tHandleRbWlc        KernelDB;

    MEMSET (&KernelDB, 0, sizeof (tHandleRbWlc));

    switch (u1Module)
    {
        case TX_MODULE:
        {
            KernelDB.u1RbOperation = RB_REPLACE;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_IP_TABLE:
                    KernelDB.rbData.unTable.apIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apIpData.u4ipAddr;
                    KernelDB.rbData.unTable.apIpData.u4UdpPort =
                        pWssKernelDB->rbData.unTable.apIpData.u4UdpPort;
                    KernelDB.rbData.unTable.apIpData.u1macType =
                        pWssKernelDB->rbData.unTable.apIpData.u1macType;
                    break;
                case AP_INT_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apIntData.bssid,
                            pWssKernelDB->rbData.unTable.apIntData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.apIntData.u2VlanId =
                        pWssKernelDB->rbData.unTable.apIntData.u2VlanId;
                    break;
                case VLAN_TABLE:
                    KernelDB.rbData.unTable.VlanData.u2VlanId =
                        pWssKernelDB->rbData.unTable.VlanData.u2VlanId;
                    MEMCPY (KernelDB.rbData.unTable.VlanData.au1PhysicalPort,
                            pWssKernelDB->rbData.unTable.VlanData.
                            au1PhysicalPort, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.VlanData.u1Tagged =
                        pWssKernelDB->rbData.unTable.VlanData.u1Tagged;
                    break;
                case MAC_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.MacData.au1MacAddress,
                            pWssKernelDB->rbData.unTable.MacData.au1MacAddress,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.MacData.
                            au1PhysicalInterface,
                            pWssKernelDB->rbData.unTable.MacData.
                            au1PhysicalInterface, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.MacData.u2VlanId =
                        pWssKernelDB->rbData.unTable.MacData.u2VlanId;
                    KernelDB.rbData.unTable.MacData.u1WlanFlag =
                        pWssKernelDB->rbData.unTable.MacData.u1WlanFlag;
                    break;
                case SOCKET_INFO:
                    KernelDB.rbData.i4CtrlUdpPort =
                        pWssKernelDB->rbData.i4CtrlUdpPort;
                    KernelDB.rbData.i4WssDataDebugMask =
                        pWssKernelDB->rbData.i4WssDataDebugMask;
                    break;
                case STA_IP_TABLE:
                    KernelDB.rbData.unTable.StaIpData.u4StaIp =
                        pWssKernelDB->rbData.unTable.StaIpData.u4StaIp;
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.staMac,
                            pWssKernelDB->rbData.unTable.StaIpData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.bssid,
                            pWssKernelDB->rbData.unTable.StaIpData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.StaIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.StaIpData.u4ipAddr;
                    KernelDB.rbData.unTable.StaIpData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.StaIpData.u1WMMEnable;
                    KernelDB.rbData.unTable.StaIpData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.StaIpData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TrustMode;
                    KernelDB.rbData.unTable.StaIpData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.StaIpData.
                        u1QosProfilePriority;
                    break;
                case AP_STA_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apStaInfo.staMac,
                            pWssKernelDB->rbData.unTable.apStaInfo.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.replaceData.unTable.apStaInfo.staMac,
                            pWssKernelDB->replaceData.unTable.apStaInfo.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.replaceData.unTable.apStaInfo.bssid,
                            pWssKernelDB->replaceData.unTable.apStaInfo.bssid,
                            sizeof (tMacAddr));
                    KernelDB.replaceData.unTable.apStaInfo.u4ipAddr =
                        pWssKernelDB->replaceData.unTable.apStaInfo.u4ipAddr;
                    KernelDB.replaceData.unTable.apStaInfo.u4StaIpAddr =
                        pWssKernelDB->replaceData.unTable.apStaInfo.u4StaIpAddr;
                    KernelDB.replaceData.unTable.apStaInfo.u1WebAuthEnable =
                        pWssKernelDB->replaceData.unTable.apStaInfo.
                        u1WebAuthEnable;
                    KernelDB.replaceData.unTable.apStaInfo.u1WebAuthStatus =
                        pWssKernelDB->replaceData.unTable.apStaInfo.
                        u1WebAuthStatus;
                    KernelDB.replaceData.unTable.apStaInfo.u1WMMEnable =
                        pWssKernelDB->replaceData.unTable.apStaInfo.u1WMMEnable;
                    KernelDB.replaceData.unTable.apStaInfo.u1TaggingPolicy =
                        pWssKernelDB->replaceData.unTable.apStaInfo.
                        u1TaggingPolicy;
                    KernelDB.replaceData.unTable.apStaInfo.u1TrustMode =
                        pWssKernelDB->replaceData.unTable.apStaInfo.u1TrustMode;
                    KernelDB.replaceData.unTable.apStaInfo.
                        u1QosProfilePriority =
                        pWssKernelDB->replaceData.unTable.apStaInfo.
                        u1QosProfilePriority;

                    break;
                default:
                    printf ("\nInvalid case\n");
                    break;
            }
        }
            i4RetVal = ioctl (gi4KernelWlcSndFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }

            break;
        case RX_MODULE:
        {
            KernelDB.u1RbOperation = RB_REPLACE;
            KernelDB.i4TableNum = u2Field;
            switch (u2Field)
            {
                case AP_MAC_TABLE:
                    KernelDB.rbData.unTable.apMacData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.apMacData.u4ipAddr;
                    KernelDB.rbData.unTable.apMacData.u4UdpPort =
                        pWssKernelDB->rbData.unTable.apMacData.u4UdpPort;
                    KernelDB.rbData.unTable.apMacData.u1macType =
                        pWssKernelDB->rbData.unTable.apMacData.u1macType;
                    MEMCPY (KernelDB.rbData.unTable.apMacData.apMacAddr,
                            pWssKernelDB->rbData.unTable.apMacData.apMacAddr,
                            sizeof (tMacAddr));
                    break;
                case AP_STA_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.apStaData.staMac,
                            pWssKernelDB->rbData.unTable.apStaData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.replaceData.unTable.apStaData.staMac,
                            pWssKernelDB->replaceData.unTable.apStaData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.replaceData.unTable.apStaData.bssid,
                            pWssKernelDB->replaceData.unTable.apStaData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.replaceData.unTable.apStaData.u4ipAddr =
                        pWssKernelDB->replaceData.unTable.apStaData.u4ipAddr;
                    KernelDB.replaceData.unTable.apStaData.u4StaIpAddr =
                        pWssKernelDB->replaceData.unTable.apStaData.u4StaIpAddr;
                    KernelDB.rbData.unTable.apStaData.u1WebAuthEnable =
                        pWssKernelDB->rbData.unTable.apStaData.u1WebAuthEnable;
                    KernelDB.rbData.unTable.apStaData.u1WebAuthStatus =
                        pWssKernelDB->rbData.unTable.apStaData.u1WebAuthStatus;
                    KernelDB.rbData.unTable.apStaData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.apStaData.u1WMMEnable;
                    KernelDB.rbData.unTable.apStaData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.apStaData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.apStaData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.apStaData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.apStaData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.apStaData.
                        u1QosProfilePriority;
                    break;
                case VLAN_TABLE:
                    KernelDB.rbData.unTable.VlanData.u2VlanId =
                        pWssKernelDB->rbData.unTable.VlanData.u2VlanId;
                    MEMCPY (KernelDB.rbData.unTable.VlanData.au1PhysicalPort,
                            pWssKernelDB->rbData.unTable.VlanData.
                            au1PhysicalPort, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.VlanData.u1Tagged =
                        pWssKernelDB->rbData.unTable.VlanData.u1Tagged;
                    break;
                case MAC_TABLE:
                    MEMCPY (KernelDB.rbData.unTable.MacData.au1MacAddress,
                            pWssKernelDB->rbData.unTable.MacData.au1MacAddress,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.MacData.
                            au1PhysicalInterface,
                            pWssKernelDB->rbData.unTable.MacData.
                            au1PhysicalInterface, sizeof (INTERFACE_LEN));
                    KernelDB.rbData.unTable.MacData.u2VlanId =
                        pWssKernelDB->rbData.unTable.MacData.u2VlanId;
                    KernelDB.rbData.unTable.MacData.u1WlanFlag =
                        pWssKernelDB->rbData.unTable.MacData.u1WlanFlag;
                    break;
                case SOCKET_INFO:
                    KernelDB.rbData.i4CtrlUdpPort =
                        pWssKernelDB->rbData.i4CtrlUdpPort;
                    KernelDB.rbData.i4WssDataDebugMask =
                        pWssKernelDB->rbData.i4WssDataDebugMask;
                    break;
                case STA_IP_TABLE:
                    KernelDB.rbData.unTable.StaIpData.u4StaIp =
                        pWssKernelDB->rbData.unTable.StaIpData.u4StaIp;
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.staMac,
                            pWssKernelDB->rbData.unTable.StaIpData.staMac,
                            sizeof (tMacAddr));
                    MEMCPY (KernelDB.rbData.unTable.StaIpData.bssid,
                            pWssKernelDB->rbData.unTable.StaIpData.bssid,
                            sizeof (tMacAddr));
                    KernelDB.rbData.unTable.StaIpData.u4ipAddr =
                        pWssKernelDB->rbData.unTable.StaIpData.u4ipAddr;
                    KernelDB.rbData.unTable.StaIpData.u1WMMEnable =
                        pWssKernelDB->rbData.unTable.StaIpData.u1WMMEnable;
                    KernelDB.rbData.unTable.StaIpData.u1TaggingPolicy =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TaggingPolicy;
                    KernelDB.rbData.unTable.StaIpData.u1TrustMode =
                        pWssKernelDB->rbData.unTable.StaIpData.u1TrustMode;
                    KernelDB.rbData.unTable.StaIpData.u1QosProfilePriority =
                        pWssKernelDB->rbData.unTable.StaIpData.
                        u1QosProfilePriority;
                    break;
                default:
                    printf ("\nInvalid case\n");
                    break;
            }
        }
            i4RetVal = ioctl (gi4KernelWlcRcvFd, 0, ((FS_ULONG *) & KernelDB));
            if (i4RetVal < 0)
            {
                return OSIX_FAILURE;
            }
            break;
        default:
            printf ("\nInvalid Case\n");
            break;
    }
    return OSIX_SUCCESS;
}
#endif
/************************************************************************/
/*  Function Name   : GetStationIdleSatus                               */
/*  Description     : The function is used to replace kernel DB         */
/*  Input(s)        : u1Module - Module Id                              */
/*                    u2Field - Field Id                                */
/*                    pWssKernelDB - Strucutre to update                */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/

INT4
FcusGetStationIdleSatus (tMacAddr StaMacAddr)
{
    UINT1               u1StationStatus = 0;
#ifdef WLC_WANTED
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));
    MEMCPY (WssKernelDB.rbData.unTable.apStaInfo.staMac, StaMacAddr,
            sizeof (tMacAddr));
    capwapNpUpdateKernelDBWlc (TX_MODULE, STATION_STATUS, &WssKernelDB);
    if (WssKernelDB.rbData.unTable.apStaInfo.u1StationStatus == 1)
    {
        u1StationStatus = 1;
    }
    capwapNpUpdateKernelDBWlc (RX_MODULE, STATION_STATUS, &WssKernelDB);
    if (WssKernelDB.rbData.unTable.apStaInfo.u1StationStatus == 1)
    {
        u1StationStatus = 1;
    }
#endif
#ifdef WTP_WANTED
    tWtpKernelDB        WssKernelDB;
    MEMSET (&WssKernelDB, 0, sizeof (tWtpKernelDB));

    MEMCPY (WssKernelDB.rbData.staData.staMac, StaMacAddr, sizeof (tMacAddr));
    capwapNpUpdateKernel (TX_MODULE, AP_STATION_STATUS, &WssKernelDB);
    if (WssKernelDB.rbData.staData.u1StationStatus == 1)
    {
        u1StationStatus = 1;
    }
    capwapNpUpdateKernel (RX_MODULE, AP_STATION_STATUS, &WssKernelDB);
    if (WssKernelDB.rbData.staData.u1StationStatus == 1)
    {
        u1StationStatus = 1;
    }
#endif
    if (u1StationStatus == 1)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

#endif
/************************************************************************/
/*  Function Name   : WssIfSendCmdToTc                                  */
/*  Description     : The function is used to configure TC             */
/*  Input(s)        :                                                  */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/

INT4
WssIfSendCmdToTc (UINT1 *pu1Cmd)
{
    INT4                i4Sockfd = 0;
    UINT1               u1SendBuff[BUF_LEN];
    UINT1              *pu1WritePtr = &u1SendBuff[0];
    struct sockaddr_in  serv_addr;
    INT4                i4Loop = 0;
    INT4                i4SBLoop = 0;
    INT4                i4WrittenSoFar = 0;
    INT4                i4Written = 0;
    INT4                i4TryToWrite = 0;

    MEMSET (u1SendBuff, 0, sizeof (u1SendBuff));
    for (i4Loop = 0; pu1Cmd[i4Loop] != '\0' && (i4SBLoop < BUF_LEN - 1);
         i4Loop++)
    {
        if (u1SendBuff[i4SBLoop] == ' ' && pu1Cmd[i4Loop] == ' ')
        {
            continue;
        }

        u1SendBuff[i4SBLoop++] = pu1Cmd[i4Loop];
    }
    u1SendBuff[i4SBLoop] = '\0';

    if ((i4Sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&serv_addr, '0', sizeof (serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons (PORT_NUM);
    serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);

    if (connect (i4Sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) <
        0)
    {
        close (i4Sockfd);
        return OSIX_FAILURE;
    }

    i4TryToWrite = STRLEN (pu1WritePtr) + 1;
    i4Written = 0;
    i4WrittenSoFar = 0;
    while ((i4Written =
            write (i4Sockfd, pu1WritePtr, i4TryToWrite - i4WrittenSoFar)) > 0)
    {
        if (i4Written < 0)
        {
            close (i4Sockfd);
            return OSIX_FAILURE;
        }

        i4WrittenSoFar += i4Written;
        pu1WritePtr += i4Written;

        if (i4WrittenSoFar >= i4TryToWrite)
        {
            break;
        }
    }
    close (i4Sockfd);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssIfWmmInstallFilter                */
/*  Description     : The function is used to configure TC              */
/*  Input(s)        :                                                   */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESSS or OSIX_FAILURE                     */
/************************************************************************/
VOID
WssIfWmmInstallFilter (UINT1 *pu1IntfName)
{
#if DEBUG_WANTED
    PRINTF ("Install filter HIT !!! with intf : %s", pu1IntfName);
    UINT1               au1IntfName[200];
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x30 0xfc flowid 1:10",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x22 0xfc flowid 1:20",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 2 u32 match ip tos 0xa 0xfc flowid 1:40",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x2e 0xfc flowid 1:20",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x1a 0xfc flowid 1:30",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
#else
    UNUSED_PARAM (pu1IntfName);
#endif
}
