/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *  $Id: wsswlanextn.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: extern variables for WSSWLAN Module
 ********************************************************************/

#ifndef  __WSSWLAN_EXTN_H
#define  __WSSWLAN_EXTN_H

PUBLIC UINT1 gu1IsWlanConfigResponseReceived;

#endif

