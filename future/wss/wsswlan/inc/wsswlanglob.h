/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wsswlanglob.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * Description: globals variables for WSSWLAN Module
 * *******************************************************************/


#ifndef  __WSSWLAN_GLOB_H
#define  __WSSWLAN_GLOB_H

UINT1 gu1IsWlanConfigResponseReceived;

#endif

