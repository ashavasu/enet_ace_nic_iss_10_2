/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wsswlantrc.h,v 1.2 2017/05/23 14:16:59 siva Exp $
 * Description: traces for WSSWLAN Module
 * *******************************************************************/



#ifndef _WSSWLANTRC_H_
#define _WSSWLANTRC_H_


#define  WSSWLAN_MOD                ((const char *)"WSSWLAN")

#define WSSWLAN_MGMT_TRC                 0x00000001
#define WSSWLAN_INIT_TRC                 0x00000002
#define WSSWLAN_ENTRY_TRC                0x00000004
#define WSSWLAN_EXIT_TRC                 0x00000008
#define WSSWLAN_FAILURE_TRC              0x00000010
#define WSSWLAN_BUF_TRC                  0x00000020
#define WSSWLAN_SESS_TRC                 0x00000040
#define WSSWLAN_INFO_TRC                 0x00000080


#define WSSWLAN_MASK                  WSSWLAN_FAILURE_TRC

/*
 * if WSSWLAN Trace/Debug messages wanted this flags can be enabled
 * by repacing the above WSSWLAN_MASK macro value with the below 
#define WSSWLAN_MASK                WSSWLAN_MGMT_TRC | WSSWLAN_FAILURE_TRC |\
     WSSWLAN_ENTRY_TRC | WSSWLAN_EXIT_TRC
*/

/* Trace and debug flags */

#define WSSWLAN_FN_ENTRY() MOD_FN_ENTRY (WSSWLAN_MASK,WSSWLAN_ENTRY_TRC,\
        WSSWLAN_MOD)

#define WSSWLAN_FN_EXIT() MOD_FN_EXIT (WSSWLAN_MASK, WSSWLAN_EXIT_TRC,\
        WSSWLAN_MOD)

#define WSSWLAN_PKT_DUMP(mask, pBuf, Length, fmt)                           \
        MOD_PKT_DUMP(WSSWLAN_PKT_DUMP_TRC,mask, WSSWLAN_MOD, pBuf, Length, fmt)
#define WSSWLAN_TRC(mask, fmt)\
        MOD_TRC(WSSWLAN_MASK, mask, WSSWLAN_MOD, fmt)
#define WSSWLAN_TRC1(mask,fmt,arg1)\
        MOD_TRC_ARG1(WSSWLAN_MASK,mask,WSSWLAN_MOD,fmt,arg1)
#define WSSWLAN_TRC2(mask,fmt,arg1,arg2)\
        MOD_TRC_ARG2(WSSWLAN_MASK,mask,WSSWLAN_MOD,fmt,arg1,arg2)
#define WSSWLAN_TRC3(mask,fmt,arg1,arg2,arg3)\
        MOD_TRC_ARG3(WSSWLAN_MASK,mask,WSSWLAN_MOD,fmt,arg1,arg2,arg3)
#define WSSWLAN_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
        MOD_TRC_ARG4(WSSWLAN_MASK,mask,WSSWLAN_MOD,fmt,arg1,arg2,arg3,arg4)
#define WSSWLAN_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
        MOD_TRC_ARG5(WSSWLAN_MASK,mask,WSSWLAN_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define WSSWLAN_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
        MOD_TRC_ARG6(WSSWLAN_MASK,mask,WSSWLAN_MOD,fmt,arg1,arg2,arg3,arg4,\
                arg5,arg6)




#endif

