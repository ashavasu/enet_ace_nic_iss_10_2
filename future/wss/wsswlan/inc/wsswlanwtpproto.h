/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * 
 * Aricent Inc . All Rights Reserved
 *
 * $Id: wsswlanwtpproto.h,v 1.6 2017/11/24 10:37:12 siva Exp $
 *
 * This file contains prototypes for functions defined
 *                      in Module WssWlan.
 *
 **********************************************************************/
   
#ifndef  __WSSWLAN_WTPPROTO_H__
#define  __WSSWLAN_WTPPROTO_H__

#include "wssifinc.h"
#include "wsswlan.h"
#include "wsswlancmn.h"

UINT1 WssWlanProcessWssIfMsg PROTO ((UINT1 , tWssWlanMsgStruct *));

UINT1 WssWlanProcessConfigRequest PROTO ((UINT1 , tWssWlanMsgStruct*));

VOID WssWlanGetDot11DesiredSSID PROTO ((UINT4 , UINT1 *));

UINT1 WssWlanSetWlanProfile PROTO ((tWssWlanDB *));

UINT1 WssWlanSetStationConfig PROTO ((tWssWlanDB *pWssWlanDB));

VOID WssWlanGetProfileCount PROTO ((UINT2 *));

VOID WssWlanGetWlanProfileId PROTO ((UINT2 , UINT2 *));

UINT1 WssWlanGetWlanInternalId PROTO ((UINT4 , UINT2 *));

UINT1 WssWlanSetDot11SpectrumManagementTable PROTO ((tWssWlanDB *));

UINT1 WssWlanSetAuthenticationProfile PROTO ((tWssWlanDB *));

UINT1 WssWlanSetManagmentSSID PROTO ((UINT1 *));

UINT1 WssWlanGetManagmentSSID PROTO ((UINT1 *));

UINT1 WssWlanSetVlanIdMapping PROTO ((tWssWlanMsgStruct*));

UINT1 WssWlanGetAthHwAddr PROTO ((tWssWlanDB *, UINT1 *));

UINT1
WssWlanSetBridgeInterface (tWssWlanDB *pWssWlanDB, UINT1 *pu1LocalRouting);

#ifdef BAND_SELECT_WANTED

UINT1 WssWlanSetBandSelect PROTO ((tWssWlanMsgStruct * pWssWlanMsgStruct));

UINT1
WssWlanSetHwBandSelect PROTO ((tWssWlanDB *, UINT1 *));

#endif
UINT1 WssWlanGetBrIfName PROTO ((UINT1 u1RadioId,UINT1 u1WlanId ,UINT1 *pu1InterfaceName));
#ifdef NPAPI_WANTED
UINT1 WssWlanCreateAthInterface PROTO ((tWssWlanDB *, UINT1 *));
UINT1 WssWlanSetDot11xSuppMode PROTO ((tWssWlanDB *, UINT1 *));


UINT1 WssWlanSetDesiredSsid PROTO ((tWssWlanDB *, UINT1 *));
UINT1 WssWlanSetPowerConstraint PROTO ((tWssWlanDB *, UINT1 *));

UINT1 WssWlanSetCapability PROTO((tWssWlanDB *, UINT1 *));
UINT1 WssWlanMulticastMode PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));
UINT1 WssWlanSetWpaIEElements PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));
UINT1 WssWlanIsolationEnable PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));
UINT1 WssWlanMulticastLength PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));
UINT1 WssWlanMulticastTimer PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));
UINT1 WssWlanMulticastTimeout PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));
UINT1 WssWlanCreateBrInterface PROTO((tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName));


UINT1 WssWlanSetHideSsid PROTO ((tWssWlanDB *, UINT1 *));

UINT1 WssWlanSetStaWepKeyEncryptn PROTO ((tWssWlanDB *, UINT1 *));

UINT1 WssWlanSetStWpAuthKeyType PROTO ((tWssWlanDB *, UINT1 *));

UINT1 WssWlanSetStaWepKeyStatus PROTO ((tWssWlanDB *, UINT1 *));

UINT1 WssWlanDestroyAthInterface PROTO ((tWssWlanDB *, UINT1 *));
UINT1 WssWlanSetRsnaIEElements PROTO ((tWssWlanDB *, UINT1 *));
UINT1 WssWlanSetRsnaGroupwiseParams PROTO ((tWssWlanDB *, UINT1 *));
UINT1 WssWlanGetWlanIntfName PROTO ((UINT1, UINT1, UINT1 *, UINT1 *));
UINT1 WssWlanSetRsnaMgmtGroupwiseParams PROTO((tWssWlanDB * , UINT1 *));

UINT1 WssWlanSetdot11N PROTO ((tWssWlanDB * , UINT1 *));
UINT1 WssWlanSetdot11AC PROTO ((tWssWlanDB * , UINT1 *));
UINT1 WssWlanAddVlanInterface PROTO ((tWssWlanDB *, UINT1 *));

#ifdef ROGUEAP_WANTED
UINT1 WssWlanRougeApDetatils  PROTO ((tWssWlanDB *, UINT1 *));
#endif
UINT1 WssWlan80211NConfiguration PROTO ((tRadioIfGetDB * , tWssWlanDB * ));
UINT1 WssWlan80211ACConfiguration PROTO ((tRadioIfGetDB * , tWssWlanDB * ));
#ifdef WPS_WTP_WANTED
UINT1
WssWlanSetWpsAdditionalIEElements PROTO (( UINT1 *, tWssWlanDB *));
UINT1
WssWlanSetWpsIEElements PROTO (( UINT1 *, tWssWlanDB *));
UINT1
WssWlanWpsDisablePushButtonAfterTimerExp PROTO ((VOID));
VOID
WssWlanWpsDisablePushButton PROTO ((VOID));

#endif
#endif
#endif
