/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * 
 * $Id: wsswlaninc.h,v 1.2 2017/11/24 10:37:12 siva Exp $
 *
 * Description: Header files to be included for WSSWLAN Module
 ********************************************************************/



#ifndef  __WSSWLAN_INC_H
#define  __WSSWLAN_INC_H


#include "lr.h"
#include "cfa.h"
#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"

#include "radioif.h"
#if defined  (__WSSWLANWLCMAIN_C__) || defined (__WSSWLANWTPMAIN_C__) 
#include "wsswlanglob.h"
#else
#include "wsswlanextn.h"
#endif

#include "wssifinc.h"

#include "wsswlanconst.h"
#include "wsswlanmacr.h"
#include "wsswlan.h"
#include "wsswlancmn.h"
#include "wsswlantrc.h"

#ifdef WLC_WANTED
#include "wsswlanwlcproto.h"
#endif

#ifdef WTP_WANTED
#include "wsswlanwtpproto.h"
#endif
#include "rsna.h"

#endif
