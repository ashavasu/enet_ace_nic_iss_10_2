/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wsswlancmn.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: common macros and prototypes for WSSWLAN Module
 *    *******************************************************************/
#ifndef _WSSWLANCMN_H__
#define _WSSWLANCMN_H__

UINT1
WssWlanGetAuthKey PROTO ((UINT4 , tWssWlanDB *, UINT1 *));
UINT1
WssWlanGetHideSsid PROTO ((UINT4 , tWssWlanDB *, UINT1 *));
UINT1
WssWlanGetKeyStatus PROTO ((UINT4 , tWssWlanDB *, UINT1 *));


#endif

 
