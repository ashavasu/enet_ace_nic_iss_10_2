/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * 
 * $Id: wsswlanmacr.h,v 1.2 2017/05/23 14:16:59 siva Exp $
 *
 * Description: macros for WSSWLAN Module
 * *******************************************************************/


#ifndef _WSSWLANMACR_H
#define _WSSWLANMACR_H


#define WSSWLAN_ADDWLANMSG_POOLID\
    WSSWLANMemPoolIds[MAX_WSSWLAN_ADDWLANMSG_SIZING_ID]


#define WSSWLAN_ADD_WLAN_MSG_TYPE 1024
#define WSSWLAN_DELETE_WLAN_MSG_TYPE 1027
#define WSSWLAN_UPDATE_WLAN_MSG_TYPE 1044
/* WLAN rowstatus*/
#define WSSWLAN_RS_CREATE_AND_WAIT 5
#define WSSWLAN_RS_NOT_READY 3
#define WSSWLAN_RS_NOT_IN_SERVICE 2
#define WSSWLAN_RS_DESTROY 6
#define WSSWLAN_RS_ACTIVE 1
#define WSSWLAN_NOT_IN_SERVICE 2

/* WLAN Response OpCodes */
#define WSSWLAN_ADD_BSSID_MAPPING_DB 1
#define WSSWLAN_DELETE_BSSID_MAPPING_DB 2

#define WSS_WLAN_MAC_SIZE 6
#define WSS_WLAN_GROUP_TSC_LEN 6

#define EDCA_PARAMETER_SET_UPDATE_COUNT 0
#define POWER_CONSTRAINT_ELEMENT_ID 1
#define POWER_CONSTRAINT_ELEMENT_LENGTH 1 
#define MULTI_CAST_ELEMENT_ID 10
#define MULTI_CAST_ELEMENT_LENGTH 12 
#define QOS_CAPAB_ELEMENT_ID 46
#define QOS_CAPAB_ELEMENT_LENGTH 1
#define  EDCA_PARAM_ELEMENT_ID 12
#define  EDCA_PARAM_ELEMENT_LENGTH 18
#endif
