/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wsswlansz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 *  Description: This file contains WSSWLAN Sizing parameters
 *      *******************************************************************/
#ifndef _WSSWLANSZ_H_
#define _WSSWLANSZ_H_

#include "wsswlaninc.h"

enum {
    MAX_WSSWLAN_ADDWLANMSG_SIZING_ID,
    WSSWLAN_MAX_SIZING_ID

};

extern tMemPoolId WssWlanMemPoolIds[ ];
extern INT4  WssWlanSizingMemCreateMemPools(VOID);
extern VOID  WssWlanSizingMemDeleteMemPools(VOID);

#endif
