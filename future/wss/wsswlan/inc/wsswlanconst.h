/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: wsswlanconst.h,v 1.3 2017/11/24 10:37:12 siva Exp $
 * Description: constant macros for WSSWLAN Module
 * *******************************************************************/


#ifndef  __WSSWLAN_CONST_H__
#define  __WSSWLAN_CONST_H__


#define MAX_USER_CHAR   100
#define MAX_PWD_CHAR    100
#define KEY_LENGTH      104
#define WSS_WEP_KEY_SIZE      104
#define WSS_AUTH_PROFILE_NAME_SIZE  32 
#define WSS_QOS_PROFILE_NAME_SIZE  32 
#define WSS_CAPAB_PROFILE_NAME_SIZE  32 
#define MAX_SSID_PROFILE_INDEX  512 
#define WSS_WLAN_RESPONSE_TIMEOUT 3
#define WSS_WLAN_NO_AP_PRESENT 2
#define WSS_WLAN_HEADER_LEN 7
#define VEND_SSID_ISOLATION_LEN 7
#define WSS_WLAN_MULTICAST_LEN 18
#define VEND_WLAN_INTERFACE_IP_LEN 13
#define VEND_SSID_RATE_LIMIT_LEN 40
#define VEND_WPA_LEN 22
#define VEND_HEADER_LEN 4
#define VEND_WLAN_VLAN_LEN  8
#define WLCHDLR_NO_AP_PRESENT 2 

#define WSS_WLAN_ADD_REQ_MIN_LEN 20
#define WSS_WLAN_UPDATE_REQ_MIN_LEN 8
#define WSS_WLAN_DEL_REQ_MIN_LEN 2
#define RADIO_INTF_NAME_LEN     5

#define WSS_WLAN_INFO_RATE_MIN 0
#define WSS_WLAN_INFO_RATE_MAX 1000
#define WSS_WLAN_BURST_SIZE_MIN 0
#define WSS_WLAN_BURST_SIZE_MAX 150000

#define RADIO_QOS_IND_BE 1
#define RADIO_QOS_IND_VI 2
#define RADIO_QOS_IND_VO 3
#define RADIO_QOS_IND_BG 4


#define WSSWLAN_MGMT_TRC                 0x00000001
#define WSSWLAN_INIT_TRC                 0x00000002
#define WSSWLAN_ENTRY_TRC                0x00000004
#define WSSWLAN_EXIT_TRC                 0x00000008
#define WSSWLAN_FAILURE_TRC              0x00000010
#define WSSWLAN_BUF_TRC                  0x00000020
#define WSSWLAN_SESS_TRC                 0x00000040

#ifdef ROGUEAP_WANTED
#define RF_GROUP_NAME_LENGTH       19
#endif

#ifdef BAND_SELECT_WANTED
#define  WSSWLAN_VEND_BANDSELECT_LEN     9
#endif
#endif
