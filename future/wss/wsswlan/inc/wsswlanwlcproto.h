/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * 
 * Aricent Inc . All Rights Reserved
 *
 *  $Id: wsswlanwlcproto.h,v 1.3 2017/11/24 10:37:12 siva Exp $
 *
 * This file contains prototypes for functions defined
 *                      in Module WssWlan.
 *
 **********************************************************************/
   
#ifndef  __WSSWLAN_WLCPROTO_H__
#define  __WSSWLAN_WLCPROTO_H__

#include "wssifinc.h"
#include "wsswlan.h"
#include "wsswlancmn.h"

UINT1 WssWlanSetProfileAdminStatus PROTO ((tWssWlanMsgStruct*));

INT4 WssConstructWlanInterfaceVendorMessage PROTO ((UINT4 u4RadioIfIndex, 
     UINT2 u2WlanProfileId,unWlcHdlrMsgStruct *pWlcHdlrMsgStruct));

INT4 WssConstructDiffServVendorMessage PROTO ((UINT4 u4RadioIfIndex, 
     UINT2 u2WlanProfileId,unWlcHdlrMsgStruct *pWlcHdlrMsgStruct));

UINT1 WssWlanBindingIntfRowStatus PROTO ((UINT2,UINT4, UINT1));

UINT1 WssWlanSetBssAdminStatus PROTO ((tWssWlanMsgStruct*));

UINT1 WssWlanConstructAddWlanMsg PROTO ((UINT4 ,UINT2, tWssWlanAddReq*));

UINT1 WssWlanConstructUpdateWlanMsg PROTO ((UINT4,UINT2,tWssWlanUpdateReq*));

UINT1 WssWlanConstructDeleteWlanMsg PROTO ((UINT4,UINT2,tWssWlanDeleteReq*));

VOID WssWlanGetProfileCount PROTO ((UINT2 *));

UINT1 WssWlanProcessConfigResponse PROTO ((UINT1 ,tWssWlanMsgStruct *));

UINT1 WssWlanProfileIntfRowStatus PROTO ((UINT2 , UINT1 , UINT4*));

UINT1 WssWlanSetStationConfig PROTO ((tWssWlanDB *)); 

UINT1 WssWlanSetAuthenticationAlgorithm PROTO ((tWssWlanDB *));

UINT1 WssWlanSetWEPDefaultKeys PROTO ((tWssWlanDB *));

UINT1 WssWlanSetWEPKeyMappingsTable PROTO ((tWssWlanDB *));

UINT1 WssWlanSetWlanProfile PROTO ((tWssWlanDB *));
                             
UINT1 WssWlanSetWlanBinding PROTO ((tWssWlanDB *));

UINT1 WssWlanSetFsDot11StationConfigTable PROTO ((tWssWlanDB *));

UINT1 WssWlanSetVlanIsolation PROTO ((tWssWlanDB *));
                              
UINT1 WssWlanSetCapabilityProfile PROTO ((tWssWlanDB *));

UINT1 WssWlanSetMulticastProfile PROTO ((tWssWlanDB *));

UINT1 WssWlanSetQosProfile PROTO ((tWssWlanDB *));
                              
VOID WssWlanGetWlanProfileId PROTO ((UINT2 , UINT2 *));

UINT1 WssWlanGetWlanInternalId PROTO ((UINT4 , UINT2 *));

VOID WssWlanGetDot11DesiredSSID PROTO ((UINT4 , UINT1 *));

UINT1 WssWlanGetWlanIfIndexfromSSID PROTO ((UINT1 * , UINT4 *));

VOID WssWlanGetSSIDfromIfIndex PROTO ((UINT4 , UINT1 *));

VOID WssWlanGetDot11BssIdCount PROTO ((UINT4 , UINT1 *));

VOID WssWlanGetDot11BssIfIndex PROTO ((UINT4 , UINT1 , UINT4 *));

 VOID WssWlanGetDot11BssId PROTO ((UINT4 , UINT1 *));

UINT1 WssWlanSetAuthenticationProfile PROTO ((tWssWlanDB *));

UINT1 WssWlanSetDot11SpectrumManagementTable PROTO ((tWssWlanDB *));

UINT1 WssWlanGetWlanIdfromIfIndex PROTO ((UINT4 , UINT2 *));

VOID WssWlanGetDot11SSID PROTO ((UINT4 , UINT1 *));

UINT1 WssWlanClearConfig PROTO ((tWssWlanMsgStruct *));

UINT1 WssWlanSetManagmentSSID PROTO ((UINT1*));

UINT1 WssWlanGetManagmentSSID PROTO ((UINT1*));

UINT1 WssWlanProcessBindingRequest PROTO ((UINT1 ,tWssWlanMsgStruct *));

UINT1 WssWlanProcessWssIfMsg PROTO ((UINT1 , tWssWlanMsgStruct *));

INT4 WssWlanGetInternalId PROTO ((UINT2 u2WlanProfileId, 
            UINT2 *pu2WlanInternalId));

INT4 WssWlanFreeInternalId PROTO ((UINT2 u2WlanInternalId));
#ifdef WPA_WANTED
INT1
WssWlanTestRadioType PROTO ((INT4 i4IfIndex));
#endif

UINT1 WssWlanUpdateEdcaParams PROTO ((UINT4  u4RadioIfIndex , UINT1 u1QosIndex));

#ifdef BAND_SELECT_WANTED
UINT1
WssWlanSendBandSelectStatus PROTO ((UINT4 u4RadioIfIndex, UINT4 u4BssIfIndex,
        UINT1 u1WlanId, UINT2 u2WtpInternalId, UINT1   u1RadioId));
UINT1 WlanSetBandSteerParams PROTO ((tWssWlanDB *));
UINT1 WssWlanSendBandSelectStatusAll PROTO ((UINT1 ));
#endif
UINT1 WssWlanSetFsDot11ExternalWebAuthProfileTable (tWssWlanDB *);
#endif
