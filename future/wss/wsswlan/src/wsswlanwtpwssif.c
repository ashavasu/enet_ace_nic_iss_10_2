
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wsswlanwtpwssif.c,v 1.2 2017/11/24 10:37:12 siva Exp $                                                                     *
 *  Description: This file contains the WSSWLAN AP module APIs in WTP           *
 *                                                                            *
 ******************************************************************************/
#ifndef __WSSWLANWTPWSSIF_C__
#define __WSSWLANWTPWSSIF_C__

#include "wsswlaninc.h"
#ifdef WPS_WTP_WANTED
#include "wps.h"
#endif
/*******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanProcessWssIfMsg                                     *
 * *                                                                           *
 * * Description  : WLAN                *
 * *                                                                           *
 * * Input        : None                                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanProcessWssIfMsg (UINT1 u1OpCode, tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tWssMacMsgStruct   *pWssMacMsgReqStruct = NULL;
    tWssMacMsgStruct   *pWssMacMsgRspStruct = NULL;
    pWssMacMsgReqStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacMsgReqStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessWssIfMsg:-"
                     "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pWssMacMsgRspStruct =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacMsgRspStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessWssIfMsg:-"
                     "UtlShMemAllocMacMsgStructBuf returned failure\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssMacMsgReqStruct, 0, sizeof (tWssMacMsgStruct));
    MEMSET (pWssMacMsgRspStruct, 0, sizeof (tWssMacMsgStruct));
    switch (u1OpCode)
    {
        case WSS_WLAN_INIT_MSG:
            if (WssWlanInit () != OSIX_SUCCESS)
            {

                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg: Wlan Initialization\
                        Failed \r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_TEST_CONFIG_REQ:
        case WSS_WLAN_CONFIG_REQ:
            if (WssWlanProcessConfigRequest (u1OpCode, pWssWlanMsgStruct)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Processing of Config Request\
                        failed, return failure\r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_MAC_PROBE_REQUEST:
            MEMCPY (&(pWssMacMsgReqStruct->unMacMsg.ProbRspMacFrame),
                    &(pWssWlanMsgStruct->unWssWlanMsg.ProbReqMacFrame),
                    sizeof (tDot11ProbReqMacFrame));
            if (WssIfConstructProbeRspStruct (pWssMacMsgReqStruct,
                                              pWssMacMsgRspStruct) !=
                OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:WssIfConstructProbeRspStruct\
                        failed, return failure\r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
                return OSIX_FAILURE;
            }

            if (WssIfProcessApHdlrMsg (WSS_APHDLR_MAC_PROBE_RSP,
                                       (unApHdlrMsgStruct *)
                                       pWssMacMsgRspStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:WssIfProcessApHdlrMsg failed,\
                        return failure\r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
#ifdef BAND_SELECT_WANTED
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanBandSelect.isPresent == OSIX_TRUE)
                {
                    return OSIX_SUCCESS;
                }
#endif
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_SET_MGMT_SSID:
            if (WssWlanSetManagmentSSID
                (pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID) !=
                OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Setting ManagmentSSID failed,\
                        return failure\r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
                return OSIX_FAILURE;
            }
            break;
        case WSS_WLAN_GET_MGMT_SSID:
            if (WssWlanGetManagmentSSID
                (pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID) !=
                OSIX_SUCCESS)
            {
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_SET_VLAN_MAPPING:
            if (WssWlanSetVlanIdMapping (pWssWlanMsgStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg: WssWlanSetVlanIdMapping "
                             "FAILED\r\n");
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
                return OSIX_FAILURE;
            }
            break;
#ifdef WPS_WTP_WANTED
        case WSS_WLAN_UNSET_WPS_PBC:
            WssWlanWpsDisablePushButton ();
            break;
#endif
#ifdef BAND_SELECT_WANTED
        case WSS_WLAN_SET_BANDSELECT:
            WssWlanSetBandSelect (pWssWlanMsgStruct);
            break;
#endif
#ifdef ROGUEAP_WANTED
        case WSS_WLAN_SET_RF_GROUP_ID:
            WssWlanSetRfGroupId (pWssWlanMsgStruct);
            break;
#endif

        default:
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessWssIfMsg: Invalid OpCode ,\
                    return failure\r\n");
            break;
    }
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgReqStruct);
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacMsgRspStruct);
    return OSIX_SUCCESS;

}
#endif
