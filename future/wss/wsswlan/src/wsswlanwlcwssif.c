
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wsswlanwlcwssif.c,v 1.2 2017/11/24 10:37:12 siva Exp $                                                                     *
 *  Description: This file contains the WSSWLAN AP module APIs in WLC         * 
 *                                                                            *
 ******************************************************************************/

#ifndef __WSSWLANWLCWSSIF_C__
#define __WSSWLANWLCWSSIF_C__

#include "wsswlaninc.h"
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanProcessWssIfMsg
 * *                                                                           *
 * * Description  : WLAN                *
 * *                                                                           *
 * * Input        : None                                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanProcessWssIfMsg (UINT1 u1OpCode, tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tWssWlanIfIndexDB   WssWlanGetIfIndexDB;
    MEMSET (&WssWlanGetIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));

    switch (u1OpCode)
    {
        case WSS_WLAN_INIT_MSG:
            if (WssWlanInit () != OSIX_SUCCESS)
            {

                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg: Wlan Initialization\
                        Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_PROFILE_ADMIN_STATUS_CHG_MSG:
            if (WssWlanSetProfileAdminStatus (pWssWlanMsgStruct) !=
                OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Failed to set Profile Admin\
                        Status, return failure\r\n");

                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_CONFIG_REQ:
        case WSS_WLAN_BSS_ADMIN_STATUS_CHG_MSG:
            if (WssWlanSetBssAdminStatus (pWssWlanMsgStruct) != OSIX_SUCCESS)
            {

                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Failed to set BSS Admin\
                        Status, return failure\r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_CLEAR_CONFIG:
            if (WssWlanClearConfig (pWssWlanMsgStruct) != OSIX_SUCCESS)
            {

                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Failed to Clear the WTP\
                        config, return failure\r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_CONFIG_RSP:
            if (WssWlanProcessConfigResponse (u1OpCode, pWssWlanMsgStruct)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Processing of Config Response\
                        failed, return failure\r\n");
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_SET_MGMT_SSID:
            if (WssWlanSetManagmentSSID
                (pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID) !=
                OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Setting managmentSSID failed,\
                        return failure");
                return OSIX_FAILURE;
            }
            break;
        case WSS_WLAN_GET_MGMT_SSID:
            if (WssWlanGetManagmentSSID
                (pWssWlanMsgStruct->unWssWlanMsg.au1ManagmentSSID) !=
                OSIX_SUCCESS)
            {

                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Getting managmentSSID failed,\
                        return failure");
                return OSIX_FAILURE;

            }
            break;

        case WSS_WLAN_PROCESS_ADD_BIND_REQ:
        case WSS_WLAN_PROCESS_DELETE_BIND_REQ:
        {
            if (WssWlanProcessBindingRequest (u1OpCode, pWssWlanMsgStruct)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessWssIfMsg:Processing of Binding\
                            Request failed, return failure\r\n");
                return OSIX_FAILURE;
            }
        }
            break;
        default:
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessWssIfMsg: Invalid OpCode ,\
                    return failure\r\n");
            break;
    }
    return OSIX_SUCCESS;

}
#endif
