
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wsswlanwtpmain.c,v 1.6 2018/01/24 12:41:27 siva Exp $                                                                     *
 *  Description: This file contains the WSSWLAN main module APIs in WTP       *
 *                                                                            *
 ******************************************************************************/
#ifndef __WSSWLANWTPMAIN_C__
#define __WSSWLANWTPMAIN_C__

#include "wsswlaninc.h"
#include "bcnmgrinc.h"
#include "radioifproto.h"
#include "iss.h"
#ifdef WPS_WTP_WANTED
#include "wps.h"
#endif

#ifdef KERNEL_CAPWAP_WANTED
#include "wssifstawtpprot.h"
#endif
extern UINT1
 
 
 
 WssWlanSetRsnaMgmtGroupwiseParams (tWssWlanDB * pWssWlanDB,
                                    UINT1 *u1RadioName);
extern UINT1        WssWlanDestroyAthInterface (tWssWlanDB * pWssWlanDB,
                                                UINT1 *u1RadioName);
extern UINT1        WssWlanCreateBrInterface (tWssWlanDB * pWssWlanDB,
                                              UINT1 *u1RadioName);
extern UINT1        WssWlanSetWpaIEElements (tWssWlanDB * pWssWlanDB,
                                             UINT1 *u1RadioName);
extern UINT1        WssWlanIsolationEnable (tWssWlanDB * pWssWlanDB,
                                            UINT1 *u1RadioName);
extern UINT1        WssWlanMulticastMode (tWssWlanDB * pWssWlanDB,
                                          UINT1 *u1RadioName);
extern UINT1        WssWlanMulticastLength (tWssWlanDB * pWssWlanDB,
                                            UINT1 *u1RadioName);
extern UINT1        WssWlanMulticastTimer (tWssWlanDB * pWssWlanDB,
                                           UINT1 *u1RadioName);
extern UINT1        WssWlanMulticastTimeout (tWssWlanDB * pWssWlanDB,
                                             UINT1 *u1RadioName);
extern UINT1        CfaGddGetIfIndexforInterface (UINT1 *u1InterfaceName,
                                                  INT4 *i4Ifindex);
extern INT1         nmhSetIfIpAddr (INT4 i4IfMainIndex, UINT4 u4SetValIfIpAddr);
extern INT1
        nmhSetIfIpSubnetMask (INT4 i4IfMainIndex, UINT4 u4SetValIfIpSubnetMask);

UINT2               gu2Pvid = 1;

#ifdef BAND_SELECT_WANTED
UINT1               gu1GlobalBandSelectStatus = 0;
#endif

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanProcessConfigRequest
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : None                                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanProcessConfigRequest (UINT1 u1OpCode, tWssWlanMsgStruct * pWssWlanMsgStruct)    /* use WTP DB */
{
    tWssWlanDB          WssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;
    tRadioIfMsgStruct   RadioIfMsgStruct;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1Count = 0;
    UINT1               u1Index = 0;
    UINT1               au1GtkKey[WSSWLAN_RSNA_KEY_LEN];
    UINT1               u1AdmissionCtrl = 0;
#ifdef NPAPI_WANTED
    UINT1               u1QosIndex = 0;
    UINT1               u1LocalRouting = 0;
#endif
    tCfaMsgStruct       CfaMsgStruct;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif
    tWssIfCapDB        *pWssIfCapDB = NULL;
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN] = { 0 };

    WSSWLAN_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));
    MEMSET (au1GtkKey, 0, WSSWLAN_RSNA_KEY_LEN);

    switch (u1OpCode)
    {
        case WSS_WLAN_TEST_CONFIG_REQ:
            if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                u1WlanOption == WSSWLAN_ADD_REQ)
            {
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId <
                     RADIOIF_START_RADIOID)
                    || (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId >
                        RADIOIF_END_RADIOID))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid RadioId values; Valid Range(1-31)\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId <
                     WSSWLAN_START_WLANID_PER_RADIO)
                    || (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId >
                        WSSWLAN_END_WLANID_PER_RADIO))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid WlanId values; Valid Range(1-16)\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                     u2Capability && 0x0001) != 1)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid Capability values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u2KeyLength !=
                    0)
                {

                    if (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u1KeyIndex == 0)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                                invalid keyindex values\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        return OSIX_FAILURE;
                    }

                    if (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u2KeyLength !=
                        (STRLEN
                         (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanAddReq.au1Key)))
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "\nWssWlanProcessConfigRequest:WssWlanAddReq\
                                has invalid key length values\r\n");
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        return OSIX_FAILURE;
                    }
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1KeyStatus >
                    (WSSWLAN_ENABLE + 1))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid KeyStatus values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1Qos >
                    (BACKGROUND_ACI + 1))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid Qos values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1AuthType !=
                     WSSWLAN_AUTH_TYPE_WEB)
                    && (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u1AuthType != WSSWLAN_AUTH_TYPE_WEP)
                    && (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u1AuthType != WSSWLAN_AUTH_TYPE_OPEN))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid AuthType values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                     u1TunnelMode != WSSWLAN_TUNNEL_MODE_BRIDGE)
                    && (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u1TunnelMode != WSSWLAN_TUNNEL_MODE_DOT3)
                    && (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u1TunnelMode != WSSWLAN_TUNNEL_MODE_NATIVE))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid tunnelmode values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1MacMode >
                    (WSSWLAN_MAC_SPLIT + 1))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid MacMode values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1SupressSsid >
                    (WSSWLAN_ENABLE + 1))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid SupressSsid values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanPowerConstraint.u1ElementId !=
                    POWER_CONSTRAINT_ELEMENT_ID)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid power constraint element Id\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanPowerConstraint.u1Length !=
                    POWER_CONSTRAINT_ELEMENT_LENGTH)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid power constraint element length\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanEdcaParam.u1ElementId != EDCA_PARAM_ELEMENT_ID)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid Edca  element Id\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanEdcaParam.u1Length != EDCA_PARAM_ELEMENT_LENGTH)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid Edca element length\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanQosCapab.u1ElementId != QOS_CAPAB_ELEMENT_ID)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid Qos Capability element Id\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanQosCapab.u1Length != QOS_CAPAB_ELEMENT_LENGTH)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid Qos Capability element length\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    u2MessageType != WSSWLAN_ADD_WLAN_MSG_TYPE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid MessageType\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    u2MessageLength < WSS_WLAN_ADD_REQ_MIN_LEN)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanAddReq has\
                            invalid MessageLength values\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

            }
            if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                u1WlanOption == WSSWLAN_DEL_REQ)
            {
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.u1RadioId <
                     RADIOIF_START_RADIOID)
                    || (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                        u1RadioId > RADIOIF_END_RADIOID))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanDeleteReq\
                            has invalid RadioId values\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.u1WlanId <
                     WSSWLAN_START_WLANID_PER_RADIO)
                    || (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                        u1WlanId > WSSWLAN_END_WLANID_PER_RADIO))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanDeleteReq \
                            has invalid WlanId values\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                    u2MessageType != WSSWLAN_DELETE_WLAN_MSG_TYPE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanDeleteReq\
                            has invalid MessageType\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                    u2MessageLength != WSS_WLAN_DEL_REQ_MIN_LEN)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanDeleteReq \
                            has invalid MessageLength values\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
            }
            if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                u1WlanOption == WSSWLAN_UPDATE_REQ)
            {
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1RadioId <
                     RADIOIF_START_RADIOID)
                    || (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        u1RadioId > RADIOIF_END_RADIOID))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanUpdateReq \
                            has invalid RadioId values\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1WlanId <
                     WSSWLAN_START_WLANID_PER_RADIO)
                    || (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        u1WlanId > WSSWLAN_END_WLANID_PER_RADIO))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanUpdateReq "
                                 "has invalid WlanId values\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1Qos >
                    (BACKGROUND_ACI + 1))
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid Qos values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    WssWlanPowerConstraint.u1ElementId !=
                    POWER_CONSTRAINT_ELEMENT_ID)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid Power constraint element Id\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    WssWlanPowerConstraint.u1Length !=
                    POWER_CONSTRAINT_ELEMENT_LENGTH)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid power constraint element length\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    WssWlanEdcaParam.u1ElementId != EDCA_PARAM_ELEMENT_ID)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid EDCA element Id\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    WssWlanEdcaParam.u1Length != EDCA_PARAM_ELEMENT_LENGTH)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid EDCA element length\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    WssWlanQosCapab.u1ElementId != QOS_CAPAB_ELEMENT_ID)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid Qos Capability element Id\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    WssWlanQosCapab.u1Length != QOS_CAPAB_ELEMENT_LENGTH)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid Qos Capability element length\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                if ((pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                     u2Capability && 0x0001) != 1)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid Capability values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    u2MessageType != WSSWLAN_UPDATE_WLAN_MSG_TYPE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid MessageType\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    u2MessageLength < WSS_WLAN_UPDATE_REQ_MIN_LEN)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanProcessConfigRequest:WssWlanUpdateReq has\
                            invalid MessageLength values\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }
            }
            for (u1Index = 0; u1Index < VEND_CONF_MAX; u1Index++)
            {
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[u1Index].isOptional != OSIX_FALSE)
                {
                    if (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].u2MsgEleType ==
                        VENDOR_SSID_ISOLATION_MSG)
                    {
                        if (pWssWlanMsgStruct->unWssWlanMsg.
                            WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                            VendSsidIsolation.u1Val > 1)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid vlan\
                                    Islation\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                    }
                    if (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].u2MsgEleType ==
                        VENDOR_SSID_RATE_MSG)
                    {
                        if (pWssWlanMsgStruct->
                            unWssWlanMsg.WssWlanConfigReq.vendSpec[u1Index].
                            unVendorSpec.VendSsidRate.u1QosRateLimit > 1)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "WssWlanProcessConfigRequest:Invalid Qos "
                                         "Rate Limit Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosUpStreamCIR > WSS_WLAN_INFO_RATE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid\
                                    Qos UpStreamCIR Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosUpStreamCBS > WSS_WLAN_BURST_SIZE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid Qos\
                                    UpStreamCBS Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosUpStreamEIR > WSS_WLAN_INFO_RATE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid Qos\
                                    UpStreamEIR Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosUpStreamEBS > WSS_WLAN_BURST_SIZE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid \
                                    Qos UpStreamEBS Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosDownStreamCIR > WSS_WLAN_INFO_RATE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid Qos\
                                    DownStreamCIR Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosDownStreamCBS > WSS_WLAN_BURST_SIZE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid Qos\
                                    DownStreamCBS Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosDownStreamEIR > WSS_WLAN_INFO_RATE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid Qos\
                                    DownStreamEIR Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }
                        if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.VendSsidRate.
                            u4QosDownStreamEBS > WSS_WLAN_BURST_SIZE_MAX)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessConfigRequest:Invalid Qos\
                                    DownStreamEBS Value\n");
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                            UtlShMemFreeAntennaSelectionBuf
                                (RadioIfGetDB.RadioIfGetAllDB.
                                 pu1AntennaSelection);
                            return OSIX_FAILURE;
                        }

                    }

                }
            }
            break;
        case WSS_WLAN_CONFIG_REQ:

            if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                u1WlanOption == WSSWLAN_ADD_REQ)
            {
                /* Adding New node to WssWlanInterfaceDB */
                WssWlanDB.WssWlanAttributeDB.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId;
                WssWlanDB.WssWlanAttributeDB.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB) == OSIX_SUCCESS)
                {
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_SUCCESS;
                }

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_CREATE_INTERFACE_ENTRY,
                     &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:RBTreeAdd Failure in "
                                 "WssWlanInterfaceDB\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                    CFA_WLAN_RADIO;

                if (CfaProcessWssIfMsg (CFA_WSS_CREATE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_INTERFACE_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanInterfaceDB failed\r\n");
                    }
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:CFA failed to create\
                            WlanIfIndex\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:CFA failed to create\
                            WlanIfIndex, revert DB changes\r\n");
                    if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                            &CfaMsgStruct) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Delete CFA index\
                                failed\n");
                    }
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_INTERFACE_ENTRY, &WssWlanDB)
                        != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanInterfaceDB failed\n");
                    }
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;

                }

                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus =
                    CFA_IF_UP;
                if (CfaProcessWssIfMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:CFA admin statuts up\
                            failed");
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanIfIndexDB failed\n");
                    }
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_INTERFACE_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanInterfaceDB failed\n");
                    }
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_INTERFACE_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:pWlanIfIndexDB \
                            pointer updation in interface db failed\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                WssWlanDB.WssWlanAttributeDB.u4WlanIfType =
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType;
                WssWlanDB.WssWlanIsPresentDB.bWlanIfType = OSIX_TRUE;
                WssWlanDB.WssWlanAttributeDB.u1AdminStatus =
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus;
                WssWlanDB.WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u2Capability =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u2Capability;
                WssWlanDB.WssWlanIsPresentDB.bCapability = OSIX_TRUE;
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    (UINT4) (SYS_DEF_MAX_ENET_INTERFACES +
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                             u1RadioId);

                if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest: RadioDBMsg\
                            failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                WssWlanDB.WssWlanAttributeDB.u1KeyIndex =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1KeyIndex;
                WssWlanDB.WssWlanIsPresentDB.bKeyIndex = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u1QosProfileId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1Qos;
                WssWlanDB.WssWlanIsPresentDB.bQosProfileId = OSIX_TRUE;

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1AuthType ==
                    WSSWLAN_AUTH_TYPE_OPEN)
                {
                    WssWlanDB.WssWlanAttributeDB.u1AuthMethod =
                        WSSWLAN_AUTH_METHOD_OPEN;
                }
                else if (pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                         u1AuthType == WSSWLAN_AUTH_TYPE_WEP)
                {
                    WssWlanDB.WssWlanAttributeDB.u1AuthMethod =
                        WSSWLAN_AUTH_METHOD_SHARED;
                }
                else if (pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                         u1AuthType == WSSWLAN_AUTH_TYPE_WEB)
                {
                    WssWlanDB.WssWlanAttributeDB.u1AuthMethod =
                        WSSWLAN_AUTH_METHOD_OPEN;
                    WssWlanDB.WssWlanAttributeDB.u1WebAuthStatus = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u1ExternalWebAuthMethod =
                        OSIX_FALSE;
                    WssWlanDB.WssWlanIsPresentDB.bExternalWebAuthMethod =
                        OSIX_TRUE;
                }
                else if (pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                         u1AuthType == WSSWLAN_AUTH_TYPE_WEB_EXT)
                {
                    WssWlanDB.WssWlanAttributeDB.u1AuthMethod =
                        WSSWLAN_AUTH_METHOD_OPEN;
                    WssWlanDB.WssWlanAttributeDB.u1WebAuthStatus = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u1ExternalWebAuthMethod =
                        OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bExternalWebAuthMethod =
                        OSIX_TRUE;
                }

                WssWlanDB.WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u1MacType =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1MacMode;
                WssWlanDB.WssWlanIsPresentDB.bMacType = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u1TunnelMode =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1TunnelMode;
                WssWlanDB.WssWlanIsPresentDB.bTunnelMode = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u1SupressSsid =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1SupressSsid;
                WssWlanDB.WssWlanIsPresentDB.bSupressSsid = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u1KeyStatus =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1KeyStatus;
                WssWlanDB.WssWlanIsPresentDB.bKeyStatus = OSIX_TRUE;

                WssWlanDB.WssWlanAttributeDB.u1PowerConstraint =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanPowerConstraint.u1LocalPowerConstraint;
                WssWlanDB.WssWlanIsPresentDB.bPowerConstraint = OSIX_TRUE;

                MEMCPY (WssWlanDB.WssWlanAttributeDB.au1DesiredSsid,
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.au1Ssid,
                        sizeof (WssWlanDB.WssWlanAttributeDB.au1DesiredSsid) /
                        sizeof (WssWlanDB.
                                WssWlanAttributeDB.au1DesiredSsid[0]));
                WssWlanDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;

                MEMCPY (WssWlanDB.WssWlanAttributeDB.au1WepKey,
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.au1Key,
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        u2KeyLength);
                WssWlanDB.WssWlanIsPresentDB.bWepKey = OSIX_TRUE;

                MEMCPY (WssWlanDB.WssWlanAttributeDB.au1GroupTsc,
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        au1GroupTsc,
                        sizeof (pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.au1GroupTsc) /
                        sizeof (pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.au1GroupTsc[0]));
                WssWlanDB.WssWlanIsPresentDB.bGroupTsc = OSIX_TRUE;

                if (&
                    (pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                     WssWlanEdcaParam) != NULL)
                {
                    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam),
                            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                              unWlanConfReq.WssWlanAddReq.WssWlanEdcaParam),
                            (sizeof
                             (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                              unWlanConfReq.WssWlanAddReq.WssWlanEdcaParam) -
                             1));
                    switch (WssWlanDB.WssWlanAttributeDB.u1QosProfileId)
                    {
                        case BEST_EFFORT_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BE_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                 WssWlanEdcaParam.AC_BE_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[BEST_EFFORT_ACI] =
                                u1AdmissionCtrl;
                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[BEST_EFFORT_ACI] =
                                WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u2TxOpLimit;
                            WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u1ACI_AIFSN =
                                pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                WssWlanEdcaParam.AC_BE_ParameterRecord.
                                u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BE_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u1ECWmin_ECWmax;
                            break;
                        case VIDEO_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VI_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                 WssWlanEdcaParam.AC_VI_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[VIDEO_ACI] =
                                u1AdmissionCtrl;

                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[VIDEO_ACI] =
                                WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u2TxOpLimit;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VI_ParameterRecord.
                                u1ACI_AIFSN =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VI_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u1ECWmin_ECWmax;
                            break;
                        case VOICE_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VO_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                 WssWlanEdcaParam.AC_VO_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[VOICE_ACI] =
                                u1AdmissionCtrl;

                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[VOICE_ACI] =
                                WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u2TxOpLimit;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VO_ParameterRecord.
                                u1ACI_AIFSN =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VO_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u1ECWmin_ECWmax;
                            break;
                        case BACKGROUND_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_BK_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                 WssWlanEdcaParam.AC_BK_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[BACKGROUND_ACI] =
                                u1AdmissionCtrl;

                            RadioIfGetDB.
                                RadioIfGetAllDB.au2TxOpLimit[BACKGROUND_ACI] =
                                WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u2TxOpLimit;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u1ACI_AIFSN =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_BK_ParameterRecord.u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanAddReq.WssWlanEdcaParam.
                                AC_BK_ParameterRecord.u1ECWmin_ECWmax;

                            break;
                        default:
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "Invalid QosProfileId\r\n");
                            break;
                    }

                    WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.u1QosInfo =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        WssWlanEdcaParam.u1QosInfo;

                    WssWlanDB.WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1KeyStatus ==
                    0)
                {
                    /* RSN elements */
                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u1ElemId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u1ElemId;

                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u1Len =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u1Len;

                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2Ver =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u2Ver;

                    MEMCPY (&
                            (WssWlanDB.WssWlanAttributeDB.
                             RsnaIEElements.au1GroupCipherSuite),
                            &(pWssWlanMsgStruct->unWssWlanMsg.
                              WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                              RsnaIEElements.au1GroupCipherSuite),
                            RSNA_CIPHER_SUITE_LEN);

                    WssWlanDB.WssWlanAttributeDB.
                        RsnaIEElements.u2PairwiseCipherSuiteCount =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u2PairwiseCipherSuiteCount;

                    for (u1Count = 0;
                         u1Count <
                         WssWlanDB.WssWlanAttributeDB.
                         RsnaIEElements.u2PairwiseCipherSuiteCount; u1Count++)
                    {
                        MEMCPY (&
                                (WssWlanDB.WssWlanAttributeDB.
                                 RsnaIEElements.aRsnaPwCipherDB[u1Count].
                                 au1PairwiseCipher),
                                &(pWssWlanMsgStruct->
                                  unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                  WssWlanAddReq.RsnaIEElements.
                                  aRsnaPwCipherDB[u1Count].au1PairwiseCipher),
                                RSNA_CIPHER_SUITE_LEN);
                    }
                    WssWlanDB.WssWlanAttributeDB.
                        RsnaIEElements.u2AKMSuiteCount =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u2AKMSuiteCount;
                    /*As only one AKM suit can be set for per SSID,
                     *the AKM suit count is 1 for per SSID*/
                    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.
                              RsnaIEElements.aRsnaAkmDB[0].au1AKMSuite),
                            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                              unWlanConfReq.WssWlanAddReq.RsnaIEElements.
                              aRsnaAkmDB[0].au1AKMSuite),
                            RSNA_AKM_SELECTOR_LEN);
#ifdef DEBUG_WANTED
                    for (u1Count = 0;
                         u1Count <
                         WssWlanDB.WssWlanAttributeDB.
                         RsnaIEElements.u2AKMSuiteCount; u1Count++)
                    {
                        MEMCPY (&
                                (WssWlanDB.WssWlanAttributeDB.
                                 RsnaIEElements.aRsnaAkmDB[u1Count].
                                 au1AKMSuite),
                                &(pWssWlanMsgStruct->
                                  unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                  WssWlanAddReq.RsnaIEElements.
                                  aRsnaAkmDB[u1Count].au1AKMSuite),
                                RSNA_AKM_SELECTOR_LEN);
                    }
#endif
                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2RsnCapab =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u2RsnCapab;

                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2PmkidCount =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        RsnaIEElements.u2PmkidCount;

                    MEMCPY (&
                            (WssWlanDB.WssWlanAttributeDB.
                             RsnaIEElements.aPmkidList),
                            &(pWssWlanMsgStruct->unWssWlanMsg.
                              WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                              RsnaIEElements.aPmkidList), RSNA_PMKID_LEN);

#ifdef PMF_WANTED

                    if ((WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                         u2RsnCapab & RSNA_CAPABILITY_MFPR) ==
                        RSNA_CAPABILITY_MFPR
                        || (WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                            u2RsnCapab & RSNA_CAPABILITY_MFPC) ==
                        RSNA_CAPABILITY_MFPC)
                    {
                        MEMCPY (&(WssWlanDB.WssWlanAttributeDB.
                                  RsnaIEElements.au1GroupMgmtCipherSuite),
                                &(pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                  RsnaIEElements.au1GroupMgmtCipherSuite),
                                RSNA_CIPHER_SUITE_LEN);

                    }
#endif

                }

                /* SET RSNA IE Elements in DB */

                WssWlanDB.WssWlanIsPresentDB.bRSNAIEElements = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_SET_IFINDEX_ENTRY, &WssWlanDB) != OSIX_SUCCESS)
                {
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                if (&
                    (pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                     WssWlanQosCapab) != NULL)
                {
                    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.WssWlanQosCapab),
                            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                              unWlanConfReq.WssWlanAddReq.WssWlanQosCapab),
                            sizeof (pWssWlanMsgStruct->
                                    unWssWlanMsg.WssWlanConfigReq.
                                    unWlanConfReq.WssWlanAddReq.
                                    WssWlanQosCapab));
                    WssWlanDB.WssWlanIsPresentDB.bWssWlanQosCapab = OSIX_TRUE;
                }

                WssWlanDB.WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                    WssWlanQosCapab.u1QosInfo;
                WssWlanDB.WssWlanIsPresentDB.bWssWlanQosCapab = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bRadioId = OSIX_TRUE;

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    (UINT4) (SYS_DEF_MAX_ENET_INTERFACES +
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                             u1RadioId);
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
                /*Enable the flags to read the details from the RadioDB */
                RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSuppMCSSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bManMCSSet = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bHTCapInfo = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUParam = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bHTInfoSecChanAbove = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bHTInfoSecChanBelow = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bTxSupportMcsSet = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest: RadioDBMsg\
                            failed to return MacAddr\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_BGN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_AN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_NG) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_AC))
                {
                    RadioIfGetDB.RadioIfIsGetAllDB.bHtCapInfo = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bAmpduParam = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bHtCapaMcs = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bHtExtCap = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxBeamCapParam = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bHTOpeInfo = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bBasicMCSSet = OSIX_TRUE;

                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:: RadioDBMsg\
                                failed to return MacAddr\r\n");
                        return OSIX_FAILURE;
                    }
                }

                if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AC)
                {
                    /*11AC Vht booleans */
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy1 = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTransPower = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxPower20 = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxPower40 = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxPower80 = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify =
                        OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeElem = OSIX_TRUE;

                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest: RadioDBMsg"
                                     " failed to get 11AC params from radio DB\r\n");
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        return OSIX_FAILURE;
                    }
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapInfo = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperInfo = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bSpecMgmt = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth =
                        OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy1 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTransPower = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxPower20 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxPower40 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bTxPower80 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth =
                        OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy1 = OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify =
                        OSIX_FALSE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeElem =
                        OSIX_FALSE;
                }
                MEMCPY (WssWlanDB.WssWlanAttributeDB.BssId,
                        RadioIfGetDB.RadioIfGetAllDB.MacAddr, MAC_ADDR_LEN);

                /* In Atheros hardware mac address cannot be assigned to 
                 * WLAN binding interface and hence using the MAC address
                 * assigned by the hardware. Hence the velow protion of the code
                 * needs to be uncommented if required for the other targets */
                WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                         u1RadioId - 1);
                /*multi radio */
                if (WssWlanCreateAthInterface (&WssWlanDB,
                                               (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "Ath Interface creation failed\r\n");
                }

                if (WssWlanSetDot11xSuppMode (&WssWlanDB,
                                              (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "Dot1x Supp failed\r\n");
                }

                /* Get the hardware address from ath interface */
                if (WssWlanGetAthHwAddr (&WssWlanDB,
                                         (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "get Ath HwAddr failed\r\n");
                }

                if (WssWlanSetDesiredSsid (&WssWlanDB,
                                           (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "Set Desired ssid failed\r\n");
                }
                if (WssWlanSetHideSsid (&WssWlanDB,
                                        (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "set hide ssid failed\r\n");
                }
                if (WssWlanSetCapability (&WssWlanDB,
                                          (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "set capability failed\r\n");
                }
#ifdef NPAPI_WANTED
                WssWlanDB.WssWlanAttributeDB.u1BrInterfaceStatus =
                    CREATE_BR_INTERFACE;
                if (WssWlanSetBridgeInterface (&WssWlanDB, &u1LocalRouting) !=
                    OSIX_FAILURE)
                {
                    if (u1LocalRouting == 1)
                    {
                        WssWlanCreateBrInterface (&WssWlanDB,
                                                  (UINT1 *) &au1RadioName);
                    }
                }
#endif
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1AuthType ==
                    WSSWLAN_AUTH_TYPE_WEP)
                {
                    if (WssWlanSetStWpAuthKeyType (&WssWlanDB,
                                                   (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "set Wp Auth Key type failed\r\n");
                    }
                    if (WssWlanSetStaWepKeyEncryptn (&WssWlanDB,
                                                     (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "set key encryption failed\r\n");
                    }
                    if (WssWlanSetStaWepKeyStatus (&WssWlanDB,
                                                   (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "set sta wep key status failed\r\n");
                    }
                }
                if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_BGN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_AN) ||
                    (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_NG))
                {
                    /* This function update the 802.11n configuration
                       details from radioDB to wsswlandb */
#ifdef NPAPI_WANTED
                    WssWlan80211NConfiguration (&RadioIfGetDB, &WssWlanDB);
                    if (WssWlanSetdot11N (&WssWlanDB,
                                          (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "set 11N failed\r\n");
                        i4RetVal = OSIX_FAILURE;
                    }
#endif
                }
#ifdef NPAPI_WANTED
#ifdef ROGUEAP_WANTED
                WssWlanRougeApDetatils (&WssWlanDB, (UINT1 *) &au1RadioName);
#endif
#endif
                if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AC)
                {
#ifdef NPAPI_WANTED
                    WssWlan80211NConfiguration (&RadioIfGetDB, &WssWlanDB);
                    if (WssWlanSetdot11N (&WssWlanDB,
                                          (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "set 11N in RADIO_TYPE_AC failed\r\n");
                    }
                    WssWlan80211ACConfiguration (&RadioIfGetDB, &WssWlanDB);
                    if (WssWlanSetdot11AC (&WssWlanDB,
                                           (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "set 11AC failed\r\n");
                    }
#endif
                }

                /*RSNA related changes */
                if ((WssWlanDB.WssWlanAttributeDB.u1KeyStatus == 0)
                    && (WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u1ElemId ==
                        WSSMAC_RSN_ELEMENT_ID))
                {
#ifdef NPAPI_WANTED
                    if (WssWlanSetRsnaIEElements (&WssWlanDB,
                                                  (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "RSNA IE Element failed\r\n");
                    }
#endif
                }
                if (WssWlanDB.WssWlanAttributeDB.u1KeyStatus == 0)
                {
#ifdef NPAPI_WANTED
                    /*                      if (WssWlanSetRsnaIEElements (&WssWlanDB,
                       (UINT1 *) "wifi0") !=
                       OSIX_SUCCESS)
                       {
                       i4RetVal = OSIX_FAILURE;
                       }
                     */
                    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             pWssWlanMsgStruct->
                             unWssWlanMsg.WssWlanConfigReq.
                             unWlanConfReq.WssWlanAddReq.u1RadioId - 1);
                    if (WssWlanSetRsnaGroupwiseParams
                        (&WssWlanDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "set group wise params failed\r\n");
                    }
#endif
                }

                UINT2               u2QosCapab = 0;
                u2QosCapab =
                    (WssWlanDB.
                     WssWlanAttributeDB.u2Capability & 0x0200) >> SHIFT_VALUE_9;
                /* Update QoS Params */
                if (u2QosCapab == 1)
                {

                    /* Update QoS Params */

                    for (u1QosIndex = 1;
                         u1QosIndex <= WSSIF_RADIOIF_QOS_CONFIG_LEN;
                         u1QosIndex++)
                    {
                        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
                        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1QosIndex;
                        if (WssIfProcessRadioIfDBMsg
                            (WSS_GET_RADIO_QOS_CONFIG_DB,
                             &RadioIfGetDB) != OSIX_SUCCESS)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "Failed to Get IEEE Wtp QoS from RadioDB \r\n");
                            return OSIX_FAILURE;
                        }

                        /* In the existing MACROs, the mapping is not according to SDK. Currently,
                         * changing the values for TxOpLimit and Admission control alone. remainin
                         * mappings need to be changed */
                        if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                            BEST_EFFORT_ACI)
                        {
                            RadioIfGetDB.
                                RadioIfGetAllDB.
                                au2TxOpLimit[WSSIF_RADIO_QOS_ID_BE] =
                                WSSIF_RADIOIF_DEF_TXOP_BE;
                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[WSSIF_RADIO_QOS_ID_BE] =
                                WSSIF_RADIOIF_DEF_ADMISSION_BE;
                        }
                        if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                            VIDEO_ACI)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[WSSIF_RADIO_QOS_ID_VI] =
                                WSSIF_RADIOIF_DEF_TXOP_VI;
                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[WSSIF_RADIO_QOS_ID_VI] =
                                WSSIF_RADIOIF_DEF_ADMISSION_VI;
                        }

                        if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                            VOICE_ACI)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[WSSIF_RADIO_QOS_ID_VO] =
                                WSSIF_RADIOIF_DEF_TXOP_VO;
                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[WSSIF_RADIO_QOS_ID_VO] =
                                WSSIF_RADIOIF_DEF_ADMISSION_VO;
                        }

                        if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                            BACKGROUND_ACI)
                        {
                            RadioIfGetDB.
                                RadioIfGetAllDB.
                                au2TxOpLimit[WSSIF_RADIO_QOS_ID_BG] =
                                WSSIF_RADIOIF_DEF_TXOP_BG;
                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[WSSIF_RADIO_QOS_ID_BG] =
                                WSSIF_RADIOIF_DEF_ADMISSION_BG;
                        }
                    }

                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId;

                    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId;

                    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId;
                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId;
                    RadioIfGetDB.RadioIfGetAllDB.u4QosInfo =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                        WssWlanQosCapab.u1QosInfo;
                    if (RadioIfSetQosInfoParams
                        (&RadioIfGetDB,
                         (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "QosInfo hw call to set values failed\r\n");
                    }

                    if (RadioIfSetEdcaParams
                        (&RadioIfGetDB,
                         (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "QOS : EDCA - HW call to set values failed\r\n");
                    }
                    RadioIfGetDB.RadioIfGetAllDB.u1RadioId = 0;
                }
#endif
                SPRINTF ((CHR1 *) au1RadioName, "wlan%d",
                         pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                         u1RadioId - 1);

                /* Get the hardware address from ath interface */
                WssWlanGetAthHwAddr (&WssWlanDB, (UINT1 *) &au1RadioName);

                if (i4RetVal != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "NpUtilHwProgram returned failure\r\n");

#ifdef NPAPI_WANTED
                    if (WssWlanDestroyAthInterface
                        (&WssWlanDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest: Hardware call failed\
                for ATH intf delete\r\n");
                    }
#endif
                    /* Revert changes in database */
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanIfIndexDB failed\r\n");
                    }
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_INTERFACE_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanInterfaceDB failed\r\n");
                    }
                    if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                            &CfaMsgStruct) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "CfaProcessWssIfMsg call \
                to delete index failed\n");
                    }

                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_CREATE_BSSID_MAPPING_ENTRY,
                     &WssWlanDB) != OSIX_SUCCESS)
                {

                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "RBTreeAdd Failure in BssIdMappingDB\r\n");
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanIfIndexDB failed\n");
                    }
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_INTERFACE_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanInterfaceDB failed\r\n");
                    }
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "RBTree Set Failure in WssWlanIfIndexDB\r\n");
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanIfIndexDB failed\n");
                    }
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_INTERFACE_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                WssWlanInterfaceDB failed\n");
                    }
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Destroy\
                                BssIdMappingDB failed\n");
                    }
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount =
                    (UINT1) (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount + 1);
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_FALSE;

                if (WssIfProcessRadioIfDBMsg
                    (WSS_SET_RADIO_IF_DB, &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:\
                            RadioDBMsg failed to set BssIdCount\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                RadioIfMsgStruct.unRadioIfMsg.RadioIfWlanUpdate.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId;
                RadioIfMsgStruct.unRadioIfMsg.RadioIfWlanUpdate.u4IfIndex =
                    (UINT4) (SYS_DEF_MAX_ENET_INTERFACES +
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                             u1RadioId);

                if (WssIfProcessRadioIfMsg (WSS_WLAN_UPDATE_RADIO_IF_DB,
                                            &RadioIfMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:\
                            RadioDBMsg failed to make updations\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
#ifdef BCNMGR_WANTED
                WSSWLAN_TRC1 (WSSWLAN_MGMT_TRC,
                              "ISDK %s: Updating Beacon Params\r\n",
                              __FUNCTION__);

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    SYS_DEF_MAX_ENET_INTERFACES +
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId;

                WssifUpdateBeacon (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
                                   WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex);
#endif
            }
            /* Commit changes to database */

            if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                u1WlanOption == WSSWLAN_UPDATE_REQ)
            {
#ifdef DEBUG_WANTED
                /*condition check for rsna enable/disable */
                if (WssWlanDB.WssWlanAttributeDB.u1KeyStatus == 1)
                {
                    system ("ifconfig wlan0 down");
                    WssWlanDB.WssWlanAttributeDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1RadioId;
                    WssWlanDB.WssWlanAttributeDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId;

                    /* Creating WLAN during WLAN Update */

#ifdef NPAPI_WANTED
                    if (WssWlanCreateAthInterface (&WssWlanDB,
                                                   (UINT1 *) "wifi0") !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }

                    if (WssWlanSetDot11xSuppMode (&WssWlanDB,
                                                  (UINT1 *) "wifi0") !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }

                    /* Get the hardware address from ath interface */
                    if (WssWlanGetAthHwAddr (&WssWlanDB,
                                             (UINT1 *) "wifi0") != OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }

                    if (WssWlanSetDesiredSsid (&WssWlanDB,
                                               (UINT1 *) "wifi0") !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
                    if (WssWlanSetHideSsid (&WssWlanDB,
                                            (UINT1 *) "wifi0") != OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
#endif
                }
#endif
                /* Retrieve WlanIfIndex from RadioId and WlanId of WlanAddReq */
                WssWlanDB.WssWlanAttributeDB.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1RadioId;
                WssWlanDB.WssWlanIsPresentDB.bRadioId = OSIX_TRUE;
                WssWlanDB.WssWlanAttributeDB.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1WlanId;
                WssWlanDB.WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Get Intf entry "
                                 "failed");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                WssWlanDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:DB Set Failure in\
                            WssWlanIfIndexDB\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                WssWlanDB.WssWlanAttributeDB.u2Capability =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    u2Capability;
                WssWlanDB.WssWlanAttributeDB.u1QosProfileId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1Qos;

                WssWlanDB.WssWlanIsPresentDB.bCapability = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bQosProfileId = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:DB Set Failure\
                            in WssWlanIfIndexDB\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                    u1KeyStatus == 0)
                {
                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u1ElemId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u1ElemId;

                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u1Len =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u1Len;

                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2Ver =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u2Ver;

                    MEMCPY (&
                            (WssWlanDB.WssWlanAttributeDB.
                             RsnaIEElements.au1GroupCipherSuite),
                            &(pWssWlanMsgStruct->unWssWlanMsg.
                              WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                              RsnaIEElements.au1GroupCipherSuite),
                            RSNA_CIPHER_SUITE_LEN);

                    WssWlanDB.WssWlanAttributeDB.
                        RsnaIEElements.u2PairwiseCipherSuiteCount =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u2PairwiseCipherSuiteCount;

                    for (u1Count = 0;
                         u1Count <
                         WssWlanDB.WssWlanAttributeDB.
                         RsnaIEElements.u2PairwiseCipherSuiteCount; u1Count++)
                    {
                        MEMCPY (&
                                (WssWlanDB.WssWlanAttributeDB.
                                 RsnaIEElements.aRsnaPwCipherDB[u1Count].
                                 au1PairwiseCipher),
                                &(pWssWlanMsgStruct->
                                  unWssWlanMsg.WssWlanConfigReq.
                                  unWlanConfReq.WssWlanUpdateReq.
                                  RsnaIEElements.aRsnaPwCipherDB[u1Count].
                                  au1PairwiseCipher), RSNA_CIPHER_SUITE_LEN);

                    }
                    WssWlanDB.WssWlanAttributeDB.
                        RsnaIEElements.u2AKMSuiteCount =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u2AKMSuiteCount;
                    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.
                              RsnaIEElements.aRsnaAkmDB[0].
                              au1AKMSuite), &(pWssWlanMsgStruct->
                                              unWssWlanMsg.WssWlanConfigReq.
                                              unWlanConfReq.WssWlanUpdateReq.
                                              RsnaIEElements.aRsnaAkmDB[0].
                                              au1AKMSuite),
                            RSNA_AKM_SELECTOR_LEN);
#ifdef DEBUG_WANTED
                    for (u1Count = 0;
                         u1Count <
                         WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                         u2AKMSuiteCount; u1Count++)
                    {
                        MEMCPY (&
                                (WssWlanDB.WssWlanAttributeDB.
                                 RsnaIEElements.aRsnaAkmDB[u1Count].
                                 au1AKMSuite),
                                &(pWssWlanMsgStruct->
                                  unWssWlanMsg.WssWlanConfigReq.
                                  unWlanConfReq.WssWlanUpdateReq.
                                  RsnaIEElements.aRsnaAkmDB[u1Count].
                                  au1AKMSuite), RSNA_AKM_SELECTOR_LEN);
                    }
#endif
                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2RsnCapab =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u2RsnCapab;

                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2PmkidCount =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u2PmkidCount;

                    MEMCPY (&
                            (WssWlanDB.WssWlanAttributeDB.
                             RsnaIEElements.aPmkidList),
                            &(pWssWlanMsgStruct->unWssWlanMsg.
                              WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                              RsnaIEElements.aPmkidList), RSNA_PMKID_LEN);
#ifdef PMF_WANTED

                    if ((WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                         u2RsnCapab & RSNA_CAPABILITY_MFPR) ==
                        RSNA_CAPABILITY_MFPR
                        || (WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                            u2RsnCapab & RSNA_CAPABILITY_MFPC) ==
                        RSNA_CAPABILITY_MFPC)
                    {
                        MEMCPY (&(WssWlanDB.WssWlanAttributeDB.
                                  RsnaIEElements.au1GroupMgmtCipherSuite),
                                &(pWssWlanMsgStruct->unWssWlanMsg.
                                  WssWlanConfigReq.unWlanConfReq.
                                  WssWlanUpdateReq.RsnaIEElements.
                                  au1GroupMgmtCipherSuite),
                                RSNA_CIPHER_SUITE_LEN);

                    }
#endif

                    WssWlanDB.WssWlanAttributeDB.u1KeyStatus =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        u1KeyStatus;

                    WssWlanDB.WssWlanAttributeDB.u2KeyLength =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        u2KeyLength;

                    WssWlanDB.WssWlanAttributeDB.u1KeyIndex =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        u1KeyIndex;

                    MEMCPY (WssWlanDB.WssWlanAttributeDB.au1GtkKey,
                            pWssWlanMsgStruct->unWssWlanMsg.
                            WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                            au1Key, WssWlanDB.WssWlanAttributeDB.u2KeyLength);
                    WssWlanDB.WssWlanIsPresentDB.bWssWlanQosCapab = OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        WssWlanQosCapab.u1QosInfo;

                    /* If the GTK key received is all zeroes, then make the length 
                     * of the key as zero , because it will cause the key to be 
                     * deleted in NPAPI */

                    if (MEMCMP
                        (WssWlanDB.WssWlanAttributeDB.au1GtkKey, au1GtkKey,
                         WSSWLAN_RSNA_KEY_LEN) == 0)
                    {
                        WssWlanDB.WssWlanAttributeDB.u2KeyLength = 0;
                    }

                    if (WssWlanDB.WssWlanAttributeDB.u1KeyStatus == 0)
                    {
#ifdef NPAPI_WANTED
                        /*                      if (WssWlanSetRsnaIEElements (&WssWlanDB,
                           (UINT1 *) "wifi0") !=
                           OSIX_SUCCESS)
                           {
                           i4RetVal = OSIX_FAILURE;
                           }
                         */

                        MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
                        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                                 pWssWlanMsgStruct->
                                 unWssWlanMsg.WssWlanConfigReq.
                                 unWlanConfReq.WssWlanUpdateReq.u1RadioId - 1);
                        if ((WssWlanDB.WssWlanAttributeDB.u1KeyIndex == 1) ||
                            (WssWlanDB.WssWlanAttributeDB.u1KeyIndex == 2))
                        {
                            if (WssWlanSetRsnaGroupwiseParams
                                (&WssWlanDB,
                                 (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                            {
                                i4RetVal = OSIX_FAILURE;
                            }
                        }
#ifdef PMF_WANTED
                        if ((WssWlanDB.WssWlanAttributeDB.u1KeyIndex == 4) ||
                            (WssWlanDB.WssWlanAttributeDB.u1KeyIndex == 5))
                        {
                            if (WssWlanSetRsnaMgmtGroupwiseParams
                                (&WssWlanDB,
                                 (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                            {
                                i4RetVal = OSIX_FAILURE;
                            }
                        }
#endif
#endif
                    }
                }
#ifdef NPAPI_WANTED
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    SYS_DEF_MAX_ENET_INTERFACES +
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1RadioId;
                UINT2               u2QosCapab = 0;
                u2QosCapab =
                    (WssWlanDB.
                     WssWlanAttributeDB.u2Capability & 0x0200) >> SHIFT_VALUE_9;
                MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                         u1RadioId - 1);
                if (&
                    (pWssWlanMsgStruct->unWssWlanMsg.
                     WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                     WssWlanEdcaParam) != NULL)
                {
                    MEMCPY (&(WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam),
                            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                              unWlanConfReq.WssWlanUpdateReq.WssWlanEdcaParam),
                            (sizeof
                             (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                              unWlanConfReq.WssWlanUpdateReq.WssWlanEdcaParam) -
                             1));
                    switch (WssWlanDB.WssWlanAttributeDB.u1QosProfileId)
                    {
                            u1AdmissionCtrl = 0;
                        case BEST_EFFORT_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BE_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.
                                 WssWlanUpdateReq.WssWlanEdcaParam.
                                 AC_BE_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[BEST_EFFORT_ACI] =
                                u1AdmissionCtrl;
                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[BEST_EFFORT_ACI] =
                                WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u2TxOpLimit;
                            WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u1ACI_AIFSN =
                                pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                                WssWlanEdcaParam.AC_BE_ParameterRecord.
                                u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BE_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_BE_ParameterRecord.u1ECWmin_ECWmax;
                            break;
                        case VIDEO_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VI_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.
                                 WssWlanUpdateReq.WssWlanEdcaParam.
                                 AC_VI_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[VIDEO_ACI] =
                                u1AdmissionCtrl;

                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[VIDEO_ACI] =
                                WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u2TxOpLimit;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VI_ParameterRecord.
                                u1ACI_AIFSN =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VI_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_VI_ParameterRecord.u1ECWmin_ECWmax;
                            break;
                        case VOICE_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VO_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.
                                 WssWlanUpdateReq.WssWlanEdcaParam.
                                 AC_VO_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[VOICE_ACI] =
                                u1AdmissionCtrl;

                            RadioIfGetDB.RadioIfGetAllDB.
                                au2TxOpLimit[VOICE_ACI] =
                                WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u2TxOpLimit;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VO_ParameterRecord.
                                u1ACI_AIFSN =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_VO_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_VO_ParameterRecord.u1ECWmin_ECWmax;
                            break;
                        case BACKGROUND_ACI:
                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u2TxOpLimit =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_BK_ParameterRecord.u2TxOpLimit;
                            u1AdmissionCtrl =
                                (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.
                                 WssWlanUpdateReq.WssWlanEdcaParam.
                                 AC_BK_ParameterRecord.
                                 u1ACI_AIFSN & (1 << SHIFT_VALUE_4));
                            u1AdmissionCtrl = (u1AdmissionCtrl >> 4);

                            RadioIfGetDB.RadioIfGetAllDB.
                                au1AdmissionControl[BACKGROUND_ACI] =
                                u1AdmissionCtrl;

                            RadioIfGetDB.
                                RadioIfGetAllDB.au2TxOpLimit[BACKGROUND_ACI] =
                                WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u2TxOpLimit;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u1ACI_AIFSN =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_BK_ParameterRecord.u1ACI_AIFSN;

                            WssWlanDB.WssWlanAttributeDB.
                                WssWlanEdcaParam.AC_BK_ParameterRecord.
                                u1ECWmin_ECWmax =
                                pWssWlanMsgStruct->
                                unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq.WssWlanEdcaParam.
                                AC_BK_ParameterRecord.u1ECWmin_ECWmax;

                            break;
                        default:
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "Invalid QosProfileId\r\n");
                            break;
                    }

                    WssWlanDB.WssWlanAttributeDB.WssWlanEdcaParam.u1QosInfo =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        WssWlanEdcaParam.u1QosInfo;

                    WssWlanDB.WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
                }

                /* Update QoS Params */
                if (u2QosCapab == 1)
                {

                    for (u1QosIndex = 1;
                         u1QosIndex <= WSSIF_RADIOIF_QOS_CONFIG_LEN;
                         u1QosIndex++)
                    {
                        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bQueueDepth = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
                        RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
                        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1QosIndex;
                        if (WssIfProcessRadioIfDBMsg
                            (WSS_GET_RADIO_QOS_CONFIG_DB,
                             &RadioIfGetDB) != OSIX_SUCCESS)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "Failed to Get IEEE Wtp QoS from RadioDB \r\n");
                            return OSIX_FAILURE;
                        }
                    }

                    if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                        BEST_EFFORT_ACI)
                    {
                        RadioIfGetDB.
                            RadioIfGetAllDB.au2TxOpLimit[BEST_EFFORT_ACI] =
                            WSSIF_RADIOIF_DEF_TXOP_BE;
                    }
                    if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                        VIDEO_ACI)
                    {
                        if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_B
                            || RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_BG
                            || RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_BGN)
                        {
                            RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI]
                                = WSSIF_RADIOIF_DEF_TXOP_VI_B;
                        }
                        else
                        {
                            RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[VIDEO_ACI]
                                = WSSIF_RADIOIF_DEF_TXOP_VI;
                        }
                    }

                    if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                        VOICE_ACI)
                    {
                        RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[VOICE_ACI]
                            = WSSIF_RADIOIF_DEF_TXOP_VO;
                    }

                    if (WssWlanDB.WssWlanAttributeDB.u1QosProfileId !=
                        BACKGROUND_ACI)
                    {
                        RadioIfGetDB.
                            RadioIfGetAllDB.au2TxOpLimit[BACKGROUND_ACI] =
                            WSSIF_RADIOIF_DEF_TXOP_BG;
                    }

                    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        u1RadioId;
                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanUpdateReq.u1WlanId;
                    RadioIfGetDB.RadioIfGetAllDB.u4QosInfo =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanUpdateReq.WssWlanQosCapab.
                        u1QosInfo;
                    if (RadioIfSetQosInfoParams
                        (&RadioIfGetDB,
                         (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "QosInfo hw call to set values failed\r\n");
                    }

#ifdef DEBUG_WANTED
                    if (RadioIfSetEdcaParams
                        (&RadioIfGetDB,
                         (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "QOS : EDCA - HW call to set values failed\r\n");
                    }
#endif
                }
#endif

#ifdef BCNMGR_WANTED

                tBcnMgrMsgStruct    BcnMgrMsgStruct;

                MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1Ssid,
                        WssWlanDB.WssWlanAttributeDB.au1DesiredSsid,
                        STRLEN (WssWlanDB.WssWlanAttributeDB.au1DesiredSsid));

                if (WssWlanGetAthHwAddr (&WssWlanDB,
                                         &au1RadioName) != OSIX_SUCCESS)
                {
                    i4RetVal = OSIX_FAILURE;
                }

                MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid,
                        WssWlanDB.WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

                WSSWLAN_TRC6 (WSSWLAN_MGMT_TRC, "SENDING BSSID MAC:\
                        %x.%x.%x.%x.%x.%x \r\n", BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid[0], BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid[1], BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid[2], BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid[3], BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid[4], BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid[5]);
                /* Get the hardware address from ath interface */
                BcnMgrMsgStruct.u2TimerStart = OSIX_TRUE;

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    SYS_DEF_MAX_ENET_INTERFACES +
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.u1RadioId;
                RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg
                    (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:\
                            RadioDBMsg failed to return MacAddr\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                if (RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod != 0)
                {
                    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2BeaconPeriod =
                        RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;
                }
                else
                {
                    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2BeaconPeriod = 100;

                }

                if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 2)
                {
                    /* Radio Type a */
                    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x0104;
                }
                else if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 1)
                {
                    /* Radio Type b */
                    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x2100;
                }
                else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 4) ||
                         (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 13))
                {
                    /* Radio Type g or ng */
                    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x2104;
                }

                MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1SuppRate,
                        RadioIfGetDB.RadioIfGetAllDB.au1SupportedRate,
                        sizeof (BcnMgrMsgStruct.unBcn.
                                BcnMgrBcnParams.au1SuppRate));

                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1DsCurChannel =
                    RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1DtimPeriod =
                    RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod;

                MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1CountryStr,
                        RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                        sizeof (BcnMgrMsgStruct.unBcn.
                                BcnMgrBcnParams.au1CountryStr));
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1FirstChanlNum =
                    RadioIfGetDB.RadioIfGetAllDB.u2FirstChannelNumber;
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1MaxTxPowerLevel =
                    RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;

                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1MaxTxPowerLevel = 19;

                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1NumOfChanls =
                    RadioIfGetDB.RadioIfGetAllDB.u2NoOfChannels;

                if (WssWlanDB.WssWlanAttributeDB.u1KeyStatus == 0)
                {
                    if (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq.
                        RsnaIEElements.u1ElemId != 0)
                    {
                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability =
                            0x3101;

                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1KeyStatus = 1;
                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_Version =
                            WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2Ver;

                        MEMCPY (&
                                (BcnMgrMsgStruct.unBcn.
                                 BcnMgrBcnParams.u4RSN_GrpCipherSuit),
                                WssWlanDB.WssWlanAttributeDB.
                                RsnaIEElements.au1GroupCipherSuite,
                                RSNA_CIPHER_SUITE_LEN);
                        BcnMgrMsgStruct.unBcn.
                            BcnMgrBcnParams.u2RSN_PairCiphSuitCnt =
                            WssWlanDB.WssWlanAttributeDB.
                            RsnaIEElements.u2PairwiseCipherSuiteCount;

                        for (u1Count = 0;
                             u1Count <
                             WssWlanDB.WssWlanAttributeDB.
                             RsnaIEElements.u2PairwiseCipherSuiteCount;
                             u1Count++)
                        {
                            MEMCPY (&
                                    (BcnMgrMsgStruct.unBcn.
                                     BcnMgrBcnParams.aRSN_PairCiphSuitList
                                     [u1Count]),
                                    WssWlanDB.WssWlanAttributeDB.
                                    RsnaIEElements.aRsnaPwCipherDB[u1Count].
                                    au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                        }

                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_AKMSuitCnt =
                            WssWlanDB.WssWlanAttributeDB.
                            RsnaIEElements.u2AKMSuiteCount;

                        for (u1Count = 0;
                             u1Count <
                             WssWlanDB.WssWlanAttributeDB.
                             RsnaIEElements.u2AKMSuiteCount; u1Count++)
                        {
                            MEMCPY (&
                                    (BcnMgrMsgStruct.unBcn.
                                     BcnMgrBcnParams.au4RSN_AKMSuitList
                                     [u1Count]),
                                    WssWlanDB.WssWlanAttributeDB.
                                    RsnaIEElements.aRsnaAkmDB[u1Count].
                                    au1AKMSuite, RSNA_AKM_SELECTOR_LEN);
                        }

                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_Capability =
                            WssWlanDB.WssWlanAttributeDB.
                            RsnaIEElements.u2RsnCapab;
                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_PMKIdCnt =
                            WssWlanDB.WssWlanAttributeDB.
                            RsnaIEElements.u2PmkidCount;
                    }

                    else
                    {
                        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1KeyStatus = 0;
                    }
                }

                if (WssIfProcessBcnMgrMsg (BCNMGR_RECV_CONF_UPDATE,
                                           &BcnMgrMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest: "
                                 "Beacon Manager failed to make updations\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    return OSIX_FAILURE;
                }

#endif
                /*WMM Changes */
                if ((WssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode ==
                     OSIX_TRUE)
                    || (WssWlanDB.WssWlanIsPresentDB.bQosProfileId ==
                        OSIX_TRUE))
                {
                    if (WssIfProcessRadioIfDBMsg
                        (WSS_GET_WLAN_BSS_IFINDEX_DB,
                         &RadioIfGetDB) == OSIX_SUCCESS)
                    {
                        WssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
                            RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                    }
                }
            }
            if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                u1WlanOption == WSSWLAN_DEL_REQ)
            {
                /* Retrieve WlanIfIndex from RadioId and WlanId of WlanAddReq */
                WssWlanDB.WssWlanAttributeDB.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.u1RadioId;
                WssWlanDB.WssWlanAttributeDB.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.u1WlanId;
                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Get Intf Entry "
                                 "failed2\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Get Wlan IfIndex "
                                 "failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                /* Local Routing Changes */
                pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapDB->CapwapGetDB.u2WtpInternalId = 0;
                pWssIfCapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB)
                    != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WSSWLAN Delete:"
                                 "WSS_CAPWAP_GET_DB Failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

                /* If Local Routing is enabled, delete the Vlan for that WLAN */
                if (pWssIfCapDB->CapwapGetDB.u1WtpLocalRouting
                    == LOCAL_ROUTING_ENABLED)
                {
                    if (WssDeleteVlan (WssWlanDB.WssWlanAttributeDB.u2VlanId)
                        != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssDeleteVlan Failed\r\n");
                    }

#ifdef KERNEL_CAPWAP_WANTED
                    if (WssStaDeleteVlanToWlanIntf
                        (WssWlanDB.WssWlanAttributeDB.u2VlanId) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssStaUpdateVlanDB Failed\r\n");
                    }
#endif
                }

                /*
                 * Invoke STA to check if any STA is connected to BSS ifindex
                 * if yes , deauthenticate the STA and delete all
                 * STA related info
                 * */

#ifdef NPAPI_WANTED
                MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         pWssWlanMsgStruct->unWssWlanMsg.
                         WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                         u1RadioId - 1);
                if (WssWlanDestroyAthInterface
                    (&WssWlanDB, (UINT1 *) &au1RadioName) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest: Hardware call failed\
                            for ATH intf delete\r\n");
                    i4RetVal = OSIX_FAILURE;
                }
#endif
                /* Construct CFA delete message before deleting the node */
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                    CFA_WLAN_RADIO;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Destroy WlanIfIndex\
                            failed");
                    i4RetVal = OSIX_FAILURE;
                }

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_INTERFACE_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Destroy WlanInterface\
                            failed");
                    i4RetVal = OSIX_FAILURE;
                }

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
                     &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Destroy\
                            BssIdMappingDB failed\n");
                    i4RetVal = OSIX_FAILURE;
                }
                /* Call Cfa to delete IfIndex */
                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "CfaProcessWssIfMsg call\
                            to delete index returned failure\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }

#ifdef NPAPI_WANTED
                /* WSS_WANTED */
                WssWlanDB.WssWlanIsPresentDB.bInterfaceName = OSIX_TRUE;
                /*                if(WssIfProcessWssWlanDBMsg(WSS_WLAN_GET_INTERFACE_ENTRY,
                   &WssWlanDB) != OSIX_FAILURE) */
                {
                    WssWlanDB.WssWlanAttributeDB.u1BrInterfaceStatus =
                        DELETE_BR_INTERFACE;
                    if (WssWlanSetBridgeInterface (&WssWlanDB, &u1LocalRouting)
                        == OSIX_FAILURE)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "Bridge deletion "
                                     "failed\n");
                    }
                }
#endif

#ifdef RFMGMT_WANTED
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                    (UINT4) (SYS_DEF_MAX_ENET_INTERFACES +
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                             u1RadioId);

                if (WssIfProcessRfMgmtMsg (RFMGMT_WLAN_DELETION_MSG,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "NOtification to\
                            RF module  failed\n");

                }
#endif
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    (UINT4) (SYS_DEF_MAX_ENET_INTERFACES +
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                             u1RadioId);
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg
                    (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:\
                            RadioDBMsg failed to return BssIdCount\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return OSIX_FAILURE;
                }
                if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount != 0)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanDeleteReq.
                        u1WlanId;

                    if (WssIfProcessRadioIfDBMsg
                        (WSS_DESTROY_WLAN_BSS_IFINDEX_DB,
                         &RadioIfGetDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:\
                                Destroy BSS Entry in radio db failed\n");
                        i4RetVal = OSIX_FAILURE;
                    }

                    RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount =
                        (UINT1) (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount - 1);
                    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:\
                                RadioDBMsg failed to return BssIdCount\n");
                        i4RetVal = OSIX_FAILURE;
                    }
                }
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                return OSIX_SUCCESS;
            }
#ifdef WPS_WTP_WANTED
            if (pWssWlanMsgStruct->unWssWlanMsg.
                WssWlanConfigReq.RadioIfInfoElement.isPresent == OSIX_TRUE)
            {
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.RadioIfInfoElement.u1EleId ==
                    WPS_VENDOR_ELEMENT_ID)
                {

                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.u1ElemId =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.u1EleId;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.u1Len =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.u1Len;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.bWpsEnabled =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.
                        bWpsEnabled;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.wscState =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.wscState;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                        bWpsAdditionalIE =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.
                        bWpsAdditionalIE;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.noOfAuthMac =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.
                        noOfAuthMac;
                    for (u1Index = 0;
                         u1Index <
                         WssWlanDB.WssWlanAttributeDB.WpsIEElements.noOfAuthMac;
                         u1Index++)
                    {
                        MEMCPY (WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                                authorized_macs[u1Index],
                                pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.RadioIfInfoElement.unInfoElem.
                                pWpsIEElements.authorized_macs[u1Index],
                                WPS_ETH_ALEN);
                    }
                    MEMCPY (WssWlanDB.WssWlanAttributeDB.WpsIEElements.uuid_e,
                            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            RadioIfInfoElement.unInfoElem.pWpsIEElements.uuid_e,
                            16);
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.configMethods =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.
                        configMethods;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.statusOrAddIE =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.unInfoElem.pWpsIEElements.
                        statusOrAddIE;
                    WssWlanDB.WssWlanIsPresentDB.bWPSAdditionalIE = OSIX_FALSE;
                    WssWlanDB.WssWlanIsPresentDB.bWPSEnabled = OSIX_FALSE;
                    if (WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                        bWpsEnabled != OSIX_FALSE)
                    {
                        WssWlanDB.WssWlanIsPresentDB.bWPSEnabled = OSIX_TRUE;
                    }
                    if (WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                        bWpsAdditionalIE != OSIX_FALSE)
                    {
                        WssWlanDB.WssWlanIsPresentDB.bWPSAdditionalIE =
                            OSIX_TRUE;
                    }

                    WssWlanDB.WssWlanAttributeDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.u1RadioId;

                    WssWlanDB.WssWlanIsPresentDB.bRadioId = OSIX_TRUE;

                    WssWlanDB.WssWlanAttributeDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        RadioIfInfoElement.u1WlanId;

                    WssWlanDB.WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

                    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                                  &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:Get Intf entry "
                                     "failed3\r\n");
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        return OSIX_FAILURE;
                    }

                    pWssIfCapDB->CapwapIsGetAllDB.bWtpDescriptor = OSIX_TRUE;
                    pWssIfCapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
                    pWssIfCapDB->CapwapIsGetAllDB.bWtpSerialNumber = OSIX_TRUE;
                    pWssIfCapDB->CapwapIsGetAllDB.bWtpName = OSIX_TRUE;
                    pWssIfCapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;
                    pWssIfCapDB->CapwapIsGetAllDB.bWtpRadioType = OSIX_TRUE;
                    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB)
                        != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WSS_WLAN_CONFIG_REQ:"
                                     "WSS_CAPWAP_GET_DB Failed\r\n");
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        return OSIX_FAILURE;
                    }

                    MEMCPY (WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                            au1WtpModelNumber,
                            pWssIfCapDB->CapwapGetDB.au1WtpModelNumber,
                            WPS_MAX_LEN);
                    MEMCPY (WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                            au1WtpSerialNumber,
                            pWssIfCapDB->CapwapGetDB.au1WtpSerialNumber,
                            WPS_MAX_LEN);
                    MEMCPY (WssWlanDB.WssWlanAttributeDB.WpsIEElements.
                            au1WtpName, pWssIfCapDB->CapwapGetDB.au1WtpName,
                            WPS_MAX_LEN);
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.u1WtpNoofRadio =
                        pWssIfCapDB->CapwapGetDB.u1WtpNoofRadio;
                    WssWlanDB.WssWlanAttributeDB.WpsIEElements.u2WtpInternalId =
                        pWssIfCapDB->CapwapGetDB.u2WtpInternalId;

                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                                  &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProcessConfigRequest:DB Set Failure in\
                                WssWlanIfIndexDB\r\n");
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        return OSIX_FAILURE;
                    }
                    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
#ifdef NPAPI_WANTED
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                             RadioIfInfoElement.u1RadioId - 1);
                    if (WssWlanSetWpsAdditionalIEElements
                        ((UINT1 *) &au1RadioName, &WssWlanDB) != OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
#endif
                }
            }
#endif
            for (u1Index = 0; u1Index < VEND_CONF_MAX; u1Index++)
            {
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[u1Index].elementId ==
                    VENDOR_WPA_TYPE)
                {
                    WssWlanDB.WssWlanAttributeDB.u1KeyStatus = 0;
                    WssWlanDB.WssWlanAttributeDB.WpaIEElements.u1ElemId =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].unVendorSpec.WpaIEElements.
                        u2MsgEleType;
                    WssWlanDB.WssWlanAttributeDB.WpaIEElements.u1Len =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].unVendorSpec.WpaIEElements.
                        u2MsgEleLen;
                    WssWlanDB.WssWlanAttributeDB.WpaIEElements.u2Ver =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].unVendorSpec.WpaIEElements.u2Ver;
                    MEMCPY (&
                            (WssWlanDB.WssWlanAttributeDB.WpaIEElements.
                             au1GroupCipherSuite),
                            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                            vendSpec[u1Index].unVendorSpec.WpaIEElements.
                            au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);
                    WssWlanDB.WssWlanAttributeDB.WpaIEElements.
                        u2PairwiseCipherSuiteCount =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].unVendorSpec.WpaIEElements.
                        u2PairwiseCipherSuiteCount;
                    for (u1Count = 0;
                         u1Count <
                         WssWlanDB.WssWlanAttributeDB.WpaIEElements.
                         u2PairwiseCipherSuiteCount; u1Count++)
                    {
                        MEMCPY (&
                                (WssWlanDB.WssWlanAttributeDB.
                                 WpaIEElements.aWpaPwCipherDB[u1Count].
                                 au1PairwiseCipher),
                                pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                                WpaIEElements.aWpaPwCipherDB[u1Index].
                                au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                    }
                    WssWlanDB.WssWlanAttributeDB.
                        WpaIEElements.u2AKMSuiteCount =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].unVendorSpec.WpaIEElements.
                        u2AKMSuiteCount;
                    for (u1Count = 0;
                         u1Count <
                         WssWlanDB.WssWlanAttributeDB.WpaIEElements.
                         u2AKMSuiteCount; u1Count++)
                    {
                        MEMCPY (&
                                (WssWlanDB.WssWlanAttributeDB.
                                 WpaIEElements.aWpaAkmDB[u1Count].
                                 au1AKMSuite),
                                pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                                WpaIEElements.aWpaAkmDB[u1Index].au1AKMSuite,
                                RSNA_CIPHER_SUITE_LEN);
                    }
                    WssWlanDB.WssWlanIsPresentDB.bWpaIEElements = OSIX_TRUE;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_SET_IFINDEX_ENTRY,
                         &WssWlanDB) != OSIX_SUCCESS)
                    {
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        return OSIX_FAILURE;
                    }

#ifdef NPAPI_WANTED
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                             WpaIEElements.u1RadioId - 1);

                    if (WssWlanSetWpaIEElements (&WssWlanDB,
                                                 (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
#endif
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[u1Index].elementId ==
                    SSID_ISOLATION_VENDOR_MSG)
                {
                    WssWlanDB.WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;

                    WssWlanDB.WssWlanAttributeDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidIsolation.u1RadioId;
                    WssWlanDB.WssWlanAttributeDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidIsolation.u1WlanId;

                    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                    pWssIfCapDB->CapwapGetDB.u2WtpInternalId = 0;
                    pWssIfCapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB)
                        != OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
                    else
                    {

                        if (pWssIfCapDB->CapwapGetDB.u1WtpLocalRouting
                            == LOCAL_ROUTING_ENABLED)
                        {
                            WssWlanDB.WssWlanAttributeDB.u1SsidIsolation =
                                pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                                VendSsidIsolation.u1Val;
                        }
                        else
                        {
                            /* Always enable the vlan isolation in central routing */
                            WssWlanDB.WssWlanAttributeDB.u1SsidIsolation = 1;
                        }
#ifdef NPAPI_WANTED
                        SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                                 pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                                 u1RadioId - 1);
                        if (WssWlanIsolationEnable (&WssWlanDB,
                                                    (UINT1 *) &au1RadioName) !=
                            OSIX_SUCCESS)
                        {
                            i4RetVal = OSIX_FAILURE;
                        }
#endif
                    }
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[u1Index].elementId ==
                    VENDOR_WLAN_VLAN_MSG)
                {
                    /*Incase of untagged bridging support we set the vlan id to 0, 
                       so as to prevent the creation of sub-interface in AP */
                    if (gu2Pvid != 1)
                    {
                        WssWlanDB.WssWlanAttributeDB.u2VlanId =
                            pWssWlanMsgStruct->unWssWlanMsg.
                            WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                            VendWlanVlan.u2VlanId;
                    }
                    WssWlanDB.WssWlanAttributeDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendWlanVlan.u1RadioId;
                    WssWlanDB.WssWlanAttributeDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendWlanVlan.u1WlanId;
                    /* Create Br-VLAN interface and ATH interface as member port
                     * to bridge interface */
#ifdef NPAPI_WANTED
                    if (WssWlanAddVlanInterface (&WssWlanDB,
                                                 (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
#endif
                }

                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[u1Index].elementId ==
                    MULTICAST_VENDOR_MSG)
                {
                    WssWlanDB.WssWlanAttributeDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendorMulticast.u1RadioId;
                    WssWlanDB.WssWlanAttributeDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendorMulticast.u1WlanId;

                    WssWlanDB.WssWlanIsPresentDB.bWlanMulticastMode = OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u2WlanMulticastMode =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendorMulticast.u2WlanMulticastMode;

                    WssWlanDB.WssWlanIsPresentDB.bWlanSnoopTableLength =
                        OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u2WlanSnoopTableLength =
                        pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        vendSpec[u1Index].unVendorSpec.VendorMulticast.
                        u2WlanSnoopTableLength;

                    WssWlanDB.WssWlanIsPresentDB.bWlanSnoopTimer = OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u4WlanSnoopTimer =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendorMulticast.u4WlanSnoopTimer;

                    WssWlanDB.WssWlanIsPresentDB.bWlanSnoopTimeout = OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u4WlanSnoopTimeout =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendorMulticast.u4WlanSnoopTimeout;

#ifdef NPAPI_WANTED
                    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                             pWssWlanMsgStruct->unWssWlanMsg.
                             WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.
                             u1RadioId - 1);
                    if (WssWlanMulticastMode (&WssWlanDB,
                                              (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }

                    if (WssWlanMulticastLength (&WssWlanDB,
                                                (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }

                    if (WssWlanMulticastTimer (&WssWlanDB,
                                               (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }

                    if (WssWlanMulticastTimeout (&WssWlanDB,
                                                 (UINT1 *) &au1RadioName) !=
                        OSIX_SUCCESS)
                    {
                        i4RetVal = OSIX_FAILURE;
                    }
#endif
                }
                if (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[u1Index].elementId ==
                    SSID_RATE_VENDOR_MSG)
                {

                    WssWlanDB.WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;
                    WssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode =
                        OSIX_TRUE;
                    WssWlanDB.WssWlanAttributeDB.u1QosRateLimit =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u1QosRateLimit;
                    WssWlanDB.WssWlanAttributeDB.u4QosUpStreamCIR =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosUpStreamCIR;
                    WssWlanDB.WssWlanAttributeDB.u4QosUpStreamCBS =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosUpStreamCBS;
                    WssWlanDB.WssWlanAttributeDB.u4QosUpStreamEIR =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosUpStreamEIR;
                    WssWlanDB.WssWlanAttributeDB.u4QosUpStreamEBS =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosUpStreamEBS;
                    WssWlanDB.WssWlanAttributeDB.u4QosDownStreamCIR =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosDownStreamCIR;
                    WssWlanDB.WssWlanAttributeDB.u4QosDownStreamCBS =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosDownStreamCBS;
                    WssWlanDB.WssWlanAttributeDB.u4QosDownStreamEIR =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosDownStreamEIR;
                    WssWlanDB.WssWlanAttributeDB.u4QosDownStreamEBS =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u4QosDownStreamEBS;
                    WssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u1PassengerTrustMode;
                    WssWlanDB.WssWlanAttributeDB.u1RadioId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u1RadioId;
                    WssWlanDB.WssWlanAttributeDB.u1WlanId =
                        pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.vendSpec[u1Index].unVendorSpec.
                        VendSsidRate.u1WlanId;

                }
                WssWlanDB.WssWlanIsPresentDB.bRadioId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                              &WssWlanDB) == OSIX_SUCCESS)
                {
                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                                  &WssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "ConfigRequest:DB Set Failure in "
                                     "WssWlanIfIndexDB\r\n");
                        UtlShMemFreeAntennaSelectionBuf
                            (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                        return OSIX_FAILURE;
                    }
                }

            }
            if (pWssWlanMsgStruct->unWssWlanMsg.
                WssWlanConfigReq.vendSpec[0].elementId ==
                SSID_ISOLATION_VENDOR_MSG)

            {
                WssWlanDB.WssWlanAttributeDB.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[0].unVendorSpec.
                    VendSsidIsolation.u1RadioId;
                WssWlanDB.WssWlanAttributeDB.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.vendSpec[0].unVendorSpec.
                    VendSsidIsolation.u1WlanId;

                WssWlanDB.WssWlanIsPresentDB.bRadioId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Get Intf entry "
                                 "failed3\r\n");
                    UtlShMemFreeAntennaSelectionBuf
                        (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    return i4RetVal;
                }

                WssWlanDB.WssWlanIsPresentDB.bPowerConstraint = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    WSSWLAN_FN_EXIT ();
                    return i4RetVal;
                }

#ifdef NPAPI_WANTED
                SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
                         WssWlanDB.WssWlanAttributeDB.u1RadioId - 1);
                if (WssWlanSetPowerConstraint (&WssWlanDB,
                                               (UINT1 *) &au1RadioName) !=
                    OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessConfigRequest:Get Intf entry "
                                 "failed3\r\n");
                }
#endif
            }
            break;
        default:
            break;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);
    WSSWLAN_FN_EXIT ();
    return i4RetVal;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetProfileCount                                     *
 * *                                                                           *
 * * Description  : This function will return the WLAN Profile count           *
 * *                                                                           *
 * * Input        : None                                                       * 
 * *                                                                           *
 * * Output       : WlanCount                                                  *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
VOID
WssWlanGetProfileCount (UINT2 *pu2WlanCount)
{
    UNUSED_PARAM (pu2WlanCount);
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanProfileId                                    *
 * *                                                                           *
 * * Description  : This function will return the WLAN Profile Id              *
 * *                                                                           *
 * * Input        : u2WlanId  - WLAN Internal Profile Id                       * 
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
VOID
WssWlanGetWlanProfileId (UINT2 u2WlanInternalId, UINT2 *pu2WlanProfileId)
{
    UNUSED_PARAM (u2WlanInternalId);

    tWssWlanDB          WlanMsgDB;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu2WlanProfileId == NULL)
    {
        return;
    }
    WSSWLAN_FN_EXIT ();
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanInternalId                                   *
 * *                                                                           *
 * * Description  : This function will return the WLAN Internal Id             *
 * *                                                                           *
 * * Input        : u2WlanId  - WLAN Internal Profile Id                       * 
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      :                                                            *
 * *                                                                           *
 * *                                                                           *
 *******************************************************************************/
UINT1
WssWlanGetWlanInternalId (UINT4 u4WlanProfileId, UINT2 *pu2WlanInternalId)
{
    UNUSED_PARAM (u4WlanProfileId);

    tWssWlanDB          WlanMsgDB;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu2WlanInternalId == NULL)
    {
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetDot11DesiredSSID                                 *
 * *                                                                           *
 * * Description  : This function will return the WLAN SSID                    *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       * 
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 *******************************************************************************/
VOID
WssWlanGetDot11DesiredSSID (UINT4 u4WlanIfIndex, UINT1 *pu1WlanSSID)
{
    tWssWlanDB          WlanMsgDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu1WlanSSID == NULL)
    {
        return;
    }

    WlanMsgDB.WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    WlanMsgDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
    WSSWLAN_FN_EXIT ();
    return;
}

#ifdef NPAPI_WANTED
/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetBridgeInterface                                  *
 * *                                                                           *
 * * Description  : This function will return the WLAN SSID                    *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 *******************************************************************************/
UINT1
WssWlanSetBridgeInterface (tWssWlanDB * pWssWlanDB, UINT1 *pu1LocalRouting)
{
    tWssWlanDB          WssWlanTempDB;
    UINT1               au1InterfaceName[24];
    UINT1               u1RadioId = 1;
    UINT1               u1WlanId = 1;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    INT4                i4Ifindex = 0;
    UINT4               u4IpAddr = 0x0;
    UINT4               u4SubnetMask = 0x0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&WssWlanTempDB, 0, sizeof (tWssWlanDB));
    MEMSET (au1InterfaceName, 0, 24);

    /* Local Routin check */
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    *pu1LocalRouting = pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting != 2)
    {
        if (pWssWlanDB->WssWlanAttributeDB.u1RadioId == 1)
        {
            /*If the Incoming Radio Id is 1, then we need the compare with the second radio bindings */
            u1RadioId = 2;
        }
        WssWlanTempDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
        for (u1WlanId = 1; u1WlanId <= 16; u1WlanId++)
        {
            WssWlanTempDB.WssWlanAttributeDB.u1WlanId = u1WlanId;
            WssWlanTempDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            WssWlanTempDB.WssWlanIsPresentDB.bInterfaceName = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          &WssWlanTempDB) == OSIX_FAILURE)
            {
                continue;
            }
            WssWlanTempDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          &WssWlanTempDB) != OSIX_SUCCESS)
            {
                continue;
            }
            if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
                        WssWlanTempDB.WssWlanAttributeDB.au1DesiredSsid,
                        WSSWLAN_SSID_NAME_LEN) == 0)
            {
                if (pWssWlanDB->WssWlanAttributeDB.u1BrInterfaceStatus
                    != DELETE_BR_INTERFACE)
                {
                    MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1InterfaceName,
                            WssWlanTempDB.WssWlanAttributeDB.au1InterfaceName,
                            24);
                    pWssWlanDB->WssWlanIsPresentDB.bInterfaceName = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_INTERFACE_ENTRY,
                                                  pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        return OSIX_FAILURE;
                    }
                }
                break;
            }
        }
        if (u1WlanId == 17)
        {
            /*If the RadioId is value is one, then the actual binding is done on radio 2
               This is because we are changing the value of RadioId above for searching purposes */
            if (WssWlanTempDB.WssWlanAttributeDB.u1RadioId == 1)
            {
                SPRINTF ((CHR1 *) au1InterfaceName, "%s%d",
                         RADIO_BRIDGE_INTF_NAME,
                         (pWssWlanDB->WssWlanAttributeDB.u1WlanId - 1 + 16));
            }
            else
            {
                SPRINTF ((CHR1 *) au1InterfaceName, "%s%d",
                         RADIO_BRIDGE_INTF_NAME,
                         (pWssWlanDB->WssWlanAttributeDB.u1WlanId - 1));
            }
            MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1InterfaceName,
                    au1InterfaceName, 24);
            if (pWssWlanDB->WssWlanAttributeDB.u1BrInterfaceStatus ==
                DELETE_BR_INTERFACE)
            {
                if (CfaGddGetIfIndexforInterface (au1InterfaceName, &i4Ifindex)
                    != CFA_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "CfaGddGetIfIndexforInterface failed :\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_SUCCESS;
                }
                /*To Set the ip interface to 0.0.0.0 */
                if (nmhSetIfIpAddr (i4Ifindex, u4IpAddr) == SNMP_FAILURE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "CfaGddGetIfIndexforInterface failed :\n");
                }
                if (nmhSetIfIpSubnetMask (i4Ifindex, u4SubnetMask) ==
                    SNMP_FAILURE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "CfaGddGetIfIndexforInterface failed :\n");
                }
                pWssWlanDB->WssWlanIsPresentDB.bInterfaceName = OSIX_FALSE;
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_SUCCESS;
            }
            /*If the RadioId is value is one, then the actual binding is done on radio 2
               This is because we are changing the value of RadioId above for searching purposes */
            pWssWlanDB->WssWlanIsPresentDB.bInterfaceName = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_INTERFACE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

        }
    }
    pWssWlanDB->WssWlanIsPresentDB.bInterfaceName = OSIX_FALSE;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}
#endif
/* WSS_WANTED */
/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetDot11SpectrumManagementTable                     *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWssWlanMsgDB - pointer to tWssWlanDB                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetDot11SpectrumManagementTable (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetDot11SpectrumManagementTable :\
        Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetDot11SpectrumManagementTable : \
        WssWLAN DB access failed. \r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetAuthenticationProfile                            *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWssWlanMsgDB - pointer to tWssWlanDB                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetAuthenticationProfile (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetAuthenticationProfile : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetAuthenticationProfile :\
        WssWLAN DB access failed. \r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlan80211ACConfiguration                                *
 * *                                                                           *
 * * Description  : This function update the 802.11ac                          *
 * *                configuration details from radioDB to wsswlandb            *
 * * Input        : Pointer to tRadioIfGetDB, tWssWlanDB                       *
 * *                                                                           *
 * * Output       :                                                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS                                               *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
#ifdef NPAPI_WANTED
UINT1
WssWlan80211ACConfiguration (tRadioIfGetDB * pRadioIfGetDB,
                             tWssWlanDB * pWssWlanDB)
{
    /*Contruction of Vht Capability Element */
    pWssWlanDB->WssWlanAttributeDB.VHTCapabilities.u4VhtCapaInfo
        = pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo;
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.VHTCapabilities.au1SuppMCS,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs,
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    au1VhtCapaMcs));
    /*Contruction of Vht Operation Element */
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.VHTOperation.au1VhtOperInfo,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo,
            sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    au1VhtOperInfo));
    pWssWlanDB->WssWlanAttributeDB.VHTOperation.u2BasicMCS =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet;
    if ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
         u1SpecMgmt == OSIX_TRUE) && (pRadioIfGetDB->RadioIfGetAllDB.
                                      Dot11AcOperParams.u1VhtOption ==
                                      OSIX_TRUE))
    {
        /*Contruction of Vht Transmit Power Envelope element */
        pWssWlanDB->WssWlanAttributeDB.VhtTxPower.isOptional = OSIX_TRUE;
        pWssWlanDB->WssWlanAttributeDB.
            VhtTxPower.u1TransPower = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TransPower;
        pWssWlanDB->WssWlanAttributeDB.
            VhtTxPower.u1TxPower20 = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TxPower20;
        pWssWlanDB->WssWlanAttributeDB.
            VhtTxPower.u1TxPower40 = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TxPower40;
        pWssWlanDB->WssWlanAttributeDB.
            VhtTxPower.u1TxPower80 = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TxPower80;

        /*Contruction of Channel Switch Wrapper Element */
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.isOptional = OSIX_TRUE;
        /*Contruction of Vht Wide bandwidth channel switch sub element */
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            ChanwidthSwitch.isOptional = OSIX_TRUE;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            ChanwidthSwitch.u1ChanWidth = pRadioIfGetDB->RadioIfGetAllDB.
            Dot11AcOperParams.u1VhtChannelWidth;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            ChanwidthSwitch.u1CenterFcy0 = pRadioIfGetDB->RadioIfGetAllDB.
            Dot11AcOperParams.u1CenterFcy0;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            ChanwidthSwitch.u1CenterFcy1 = pRadioIfGetDB->RadioIfGetAllDB.
            Dot11AcOperParams.u1CenterFcy1;
        /*Contruction of Vht Transmit Power Envelope sub element */
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            VhtTxPower.isOptional = OSIX_TRUE;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            VhtTxPower.u1TransPower = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TransPower;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            VhtTxPower.u1TxPower20 = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TxPower20;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            VhtTxPower.u1TxPower40 = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TxPower40;
        pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.WrapperSubElem.
            VhtTxPower.u1TxPower80 = pRadioIfGetDB->RadioIfGetAllDB.
            VhtTransmitPower.u1TxPower80;

    }
    /*Contruction of Vht Operation Notification Element */
    if (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
        u1VhtOperModeNotify == OSIX_TRUE)
    {
        pWssWlanDB->WssWlanAttributeDB.OperModeNotify.u1VhtOperModeElem =
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeElem;
        pWssWlanDB->WssWlanAttributeDB.OperModeNotify.isOptional = OSIX_TRUE;
    }
    pWssWlanDB->WssWlanAttributeDB.u1Dot11acSupport = DOT11AC_SUPPORT_ENABLE;
    return OSIX_SUCCESS;
}
#endif
/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlan80211NConfiguration                      *
 * *                                                                           *
 * * Description  : This function update the 802.11n                           *
 * *                configuration details from radioDB to wsswlandb            *
 * * Input        : Pointer to tRadioIfGetDB, tWssWlanDB                       *
 * *                                                                           *
 * * Output       :                                                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS                                               *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
#ifdef NPAPI_WANTED
UINT1
WssWlan80211NConfiguration (tRadioIfGetDB * pRadioIfGetDB,
                            tWssWlanDB * pWssWlanDB)
{
    pWssWlanDB->WssWlanAttributeDB.u1HTCapEnable = DOT11_INFO_ELEM_HTCAPABILITY;
    pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u2HTCapInfo =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo;
    pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u1AMPDUParam =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam;
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.HTCapabilities.au1SuppMCSSet,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.au1HtCapaMcs,
            DOT11N_HT_CAP_MCS_LEN);
    pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u2HTExtCap =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap;
    pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u4TranBeamformCap =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u4TxBeamCapParam;

    pWssWlanDB->WssWlanAttributeDB.u1HTOpeEnable = DOT11_INFO_ELEM_HTOPERATION;
    pWssWlanDB->WssWlanAttributeDB.HTOperation.u1PrimaryCh =
        pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;

    MEMCPY (pWssWlanDB->WssWlanAttributeDB.HTOperation.au1HTOpeInfo,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo,
            WSSMAC_HTOPE_INFO);
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.HTOperation.au1BasicMCSSet,
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1BasicMCSSet,
            WSSMAC_BASIC_MCS_SET);

    pWssWlanDB->WssWlanAttributeDB.u1Dot11nSupport = DOT11N_SUPPORT_ENABLE;
    return OSIX_SUCCESS;
}
#endif
/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetWlanProfile                                      *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWssWlanMsgDB - pointer to tWssWlanDB                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetWlanProfile (tWssWlanDB * pWssWlanMsgDB)
{
    UNUSED_PARAM (pWssWlanMsgDB);
    return 0;

}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetStationConfig                                    *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWssWlanMsgDB - pointer to tWssWlanDB                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/

UINT1
WssWlanSetStationConfig (tWssWlanDB * pWssWlanDB)
{
    UNUSED_PARAM (pWssWlanDB);
    return 0;
}

/******************************************************************************
 * * Function     : WssWlanGetManagmentSSID
 *
 * * Description  : This function will get the WLAN Managment SSID
 * * Input        : None
 * * Output       : None
 * * Returns      : None
 * ****************************************************************************/
UINT1
WssWlanGetManagmentSSID (UINT1 *pu1ManagmentSSID)
{

    tWssWlanDB          WssWlanDB;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    WssWlanDB.WssWlanIsPresentDB.bManagmentSSID = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_MANAGMENT_SSID,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pu1ManagmentSSID,
            WssWlanDB.WssWlanAttributeDB.au1ManagmentSSID,
            WSSWLAN_SSID_NAME_LEN);
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * * Function     : WssWlanSetManagmentSSID
 *
 * * Description  : This function will store the WLAN Managment SSID
 * * Input        : Managment SSID to be set
 * * Output       : None
 * * Returns      : None
 * ****************************************************************************/
UINT1
WssWlanSetManagmentSSID (UINT1 *pu1ManagmentSSID)
{

    tWssWlanDB          WssWlanDB;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMCPY (WssWlanDB.WssWlanAttributeDB.au1ManagmentSSID,
            pu1ManagmentSSID, WSSWLAN_SSID_NAME_LEN);
    WssWlanDB.WssWlanIsPresentDB.bManagmentSSID = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_MANAGMENT_SSID,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetManagmentSSID :\
        WssWLAN DB access failed. \r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * * Function     : WssWlanSetVlanIdMapping
 *
 * * Description  : This function will make a WLAN Index as a VLAN Member port
 * * Input        : pWssWlanMsgStruct
 * * Output       : None
 * * Returns      : OSIX_SUCCESS / OSIX_FAILURE
 * ****************************************************************************/
UINT1
WssWlanSetVlanIdMapping (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tWssWlanDB          WssWlanDB;
    UINT1               u1VlanFlag = OSIX_FALSE;
    tWssIfCapDB        *pWssIfCapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapDB, OSIX_FAILURE)
        MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapDB, 0, sizeof (tWssIfCapDB));

    /* Local Routing Changes */
    pWssIfCapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapDB->CapwapGetDB.u2WtpInternalId = 0;
    pWssIfCapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetVlanIdMapping:\
        WSS_CAPWAP_GET_DB failed\n");
    }

    /* If Local Routing is enabled, create the Vlan for that WLAN */
    if (pWssIfCapDB->CapwapGetDB.u1WtpLocalRouting != LOCAL_ROUTING_ENABLED)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
        return OSIX_SUCCESS;
    }

    WssWlanDB.WssWlanAttributeDB.u1RadioId =
        pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.unAuthMsg.
        WssStaConfigReqInfo.StaMsg.u1RadioId;
    WssWlanDB.WssWlanIsPresentDB.bRadioId = OSIX_TRUE;
    WssWlanDB.WssWlanAttributeDB.u1WlanId =
        pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.unAuthMsg.
        WssStaConfigReqInfo.StaMsg.u1WlanId;
    WssWlanDB.WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIdMapping: Get Intf entry " "failed\n");
    }

    WssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    WssWlanDB.WssWlanAttributeDB.u2VlanId =
        pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.unAuthMsg.
        WssStaConfigReqInfo.AddSta.u2VlanId;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY, &WssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIdMapping: Set VlanId FAILED\n");
    }

    WssWlanDB.WssWlanIsPresentDB.bVlanFlag = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIdMapping: Get VlanFlag FAILED\n");
    }

    if (WssWlanDB.WssWlanAttributeDB.u1VlanFlag == OSIX_FALSE)
    {
        if (WssCreateVlan (WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex,
                           WssWlanDB.WssWlanAttributeDB.u2VlanId) ==
            OSIX_SUCCESS)
        {
            u1VlanFlag = OSIX_TRUE;
#if 0
            if (WssCreateVlanInterface (WssWlanDB.WssWlanAttributeDB.u2VlanId)
                == OSIX_SUCCESS)
            {
                u1VlanFlag = OSIX_TRUE;
            }
            else
            {
                printf ("CfaWssCreateVlanInterface FAILURE\r\n");
            }
#endif
        }

        if (WssWlanDB.WssWlanAttributeDB.u1VlanFlag != u1VlanFlag)
        {
            WssWlanDB.WssWlanIsPresentDB.bVlanFlag = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1VlanFlag = u1VlanFlag;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                          &WssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetVlanIdMapping: Set VlanFlag FAILED\n");
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapDB);
    return OSIX_SUCCESS;
}

#ifndef NPAPI_WANTED
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetAthHwAddr
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : AthHardware Address                                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanGetAthHwAddr (tWssWlanDB * pWssWlanDB, UINT1 *pu1RadioName)
{
    UINT1              *pu1SwitchMac = NULL;

    UNUSED_PARAM (pWssWlanDB);

    UINT1               u1RadioId = 0;
    u1RadioId = (UINT1) ((UINT1) pu1RadioName[RADIO_INTF_NAME_LEN - 1] - 48);

    pu1SwitchMac = IssGetSwitchMacFromNvRam ();
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.BssId, pu1SwitchMac, MAC_ADDR_LEN);
    pWssWlanDB->WssWlanAttributeDB.BssId[MAC_ADDR_LEN - 1] =
        (UINT1) (pu1SwitchMac[MAC_ADDR_LEN - 1] +
                 (u1RadioId * MAX_NUM_OF_BSSID_PER_RADIO) + 1);

    return OSIX_SUCCESS;
}

#endif

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetBrIfName  
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Radio Id, WLAN Id                                          *
 * *                                                                           *
 * * Output       : Binding interface name                                     *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/
UINT1
WssWlanGetBrIfName (UINT1 u1RadioId, UINT1 u1WlanId, UINT1 *pu1InterfaceName)
{
    tWssWlanDB          WssWlanTempDB;
    MEMSET (&WssWlanTempDB, 0, sizeof (tWssWlanDB));
    WssWlanTempDB.WssWlanAttributeDB.u1WlanId = u1WlanId;
    WssWlanTempDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
    WssWlanTempDB.WssWlanIsPresentDB.bInterfaceName = OSIX_TRUE;
    /*The funciotn will give the Br Interface name */
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                  &WssWlanTempDB) != OSIX_FAILURE)
    {
        MEMCPY (pu1InterfaceName,
                WssWlanTempDB.WssWlanAttributeDB.au1InterfaceName, 24);
        return OSIX_SUCCESS;
    }
    return OSIX_SUCCESS;
}

#ifdef WPS_WTP_WANTED
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanWpsDisablePushButton
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : NONE                                                       *
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : NONE                                                       *
 * *                                                                           *
 * *****************************************************************************/
VOID
WssWlanWpsDisablePushButton (VOID)
{
#ifdef NPAPI_WANTED
    if (WssWlanWpsDisablePushButtonAfterTimerExp () != OSIX_SUCCESS)
    {
        return;
    }
#endif
    return;
}
#endif
#ifdef ROGUEAP_WANTED
VOID
WssWlanSetRfGroupId (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
#ifdef NPAPI_WANTED
    if (WssWlanRfGroupId (pWssWlanMsgStruct) != OSIX_SUCCESS)
    {
        return;
    }
#endif
    return;
}
#endif

#ifdef BAND_SELECT_WANTED
/******************************************************************************
 * * Function     : WssWlanSetBandSelect 
 * * Description  : This function will update the bandselect status in WLAN DB
 *                  and invoke NPAPI to update the shadow database
 * * Input        : pWssWlanMsgStruct
 * * Output       : None
 * * Returns      : OSIX_SUCCESS / OSIX_FAILURE
 * ****************************************************************************/
UINT1
WssWlanSetBandSelect (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tWssWlanDB         *pWssWlanDB = NULL;
#ifdef NPAPI_WANTED
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];
#endif

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_MGMT_TRC, "Failed to allocate memory "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u1RadioId =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1RadioId;
    pWssWlanDB->WssWlanIsPresentDB.bRadioId = OSIX_TRUE;
    pWssWlanDB->WssWlanAttributeDB.u1WlanId =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1WlanId;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetBandSelect: Get WlanIfIndex " "failed\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression = OSIX_TRUE;
    pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1BandSelectStatus;
    pWssWlanDB->WssWlanAttributeDB.u1AgeOutSuppression =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanBandSelect.u1AgeOutSuppression;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetBandSelect: Set Bandselect status failed\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

#ifdef NPAPI_WANTED
    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);
    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             pWssWlanDB->WssWlanAttributeDB.u1RadioId - 1);

    if (WssWlanSetHwBandSelect (pWssWlanDB, (UINT1 *) &au1RadioName) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetBandSelect: Setting hardware failed\n");
    }
#endif
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}
#endif
#endif
