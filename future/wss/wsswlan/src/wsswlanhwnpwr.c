
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wsswlanhwnpwr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                     *
 *  Description: This file contains the WSSWLAN NP harware module APIs in WLC *
 *                                                                            *
 ******************************************************************************/

#ifdef NPAPI_WANTED
#include "nputil.h"
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : WlanNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tWlanNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
WlanNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_FAILURE;
    UINT4               u4Opcode = 0;
    tWlanNpModInfo     *pWlanNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pWlanNpModInfo = &(pFsHwNp->WlanNpModInfo);

    if (NULL == pWlanNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case WLAN_ENTRY_ADD:
        {
            tWlanNpWrEntryAdd *pEntry = NULL;
            pEntry = &pWlanNpModInfo->WlanNpEntryAdd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpWlanCreate (pEntry->pWlanNpParams);
            break;
        }
        case WLAN_ENTRY_GET:
        {
            tWlanNpWrEntryGet *pEntry = NULL;
            pEntry = &pWlanNpModInfo->WlanNpEntryGet;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpWlanGet (pEntry->pWlanNpParams);
            break;
        }

        case WLAN_ENTRY_DELETE:
        {
            tWlanNpWrEntryDelete *pEntry = NULL;
            pEntry = &pWlanNpModInfo->WlanNpEntryDelete;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = capwapNpWlanDelete (pEntry->pWlanNpParams);
            break;
        }

        default:
            break;

    }                 
    /* switch */

    return (u1RetVal);
}
