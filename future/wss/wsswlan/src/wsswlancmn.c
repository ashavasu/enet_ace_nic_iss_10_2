
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wsswlancmn.c,v 1.5 2017/11/24 10:37:12 siva Exp $                                                                     *
 *  Description: This file contains the WSSWLAN AP module APIs in WTP         *
 *                                                                            *
 ******************************************************************************/

#ifndef __WSSWLANCMN_C__
#define __WSSWLANCMN_C__

#include "wsswlaninc.h"
#include "nputil.h"
#include "radioifhwnpwr.h"
#include "radioifconst.h"
#include "rfminc.h"
#ifdef KERNEL_CAPWAP_WANTED
extern INT4         CapwapAddDeviceInKernel (UINT1 *pu1DeviceName);
extern UINT1        WssWlanGetWlanIntfName (UINT1 u1RadioId, UINT1 u1WlanId,
                                            UINT1 *pu1WlanIntfName,
                                            UINT1 *pu1WlanId);
#endif

extern UINT1        gu1GlobalBandSelectStatus;
#ifdef NPAPI_WANTED

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanCreateAthInterface
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Creation of Ath Interface                                  *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetDot11xSuppMode (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    UINT4               u4RadioType = 0;

    tRadioIfGetDB       RadioIfGetDB;
    tFsHwNp             FsHwNp;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pWssWlanDB->WssWlanAttributeDB.u1RadioId;

    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetDot11xSuppMode:Getting Radio Type failed\r\n");
        return OSIX_FAILURE;
    }

    u4RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    switch (u4RadioType)
    {

        case RADIO_TYPEA:
            NP_UTIL_FILL_PARAMS (FsHwNp,
                                 /*Generic NP structure */
                                 NP_RADIO_MODULE,
                                 /* Module ID */
                                 FS_RADIO_HW_SET_WLAN_A_SUPP_MODE,
                                 /* Function/OpCode */
                                 0,
                                 /* IfIndex value if applicable */
                                 0,
                                 /* No. of Port Params */
                                 0);

            MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11aSuppMode.
                    RadInfo.au1RadioIfName, u1RadioName,
                    STRLEN (u1RadioName) + 1);

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11aSuppMode.RadInfo.
                au2WlanIfIndex[0] =
                (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11aSuppMode.RadInfo.
                u1CurrentChannel =
                RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
            WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                                    pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11aSuppMode.RadInfo.
                                    au1WlanIfname[0],
                                    &FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11aSuppMode.RadInfo.u1WlanId);

            break;

        case RADIO_TYPEB:

            NP_UTIL_FILL_PARAMS (FsHwNp,
                                 /*Generic NP structure */
                                 NP_RADIO_MODULE,
                                 /* Module ID */
                                 FS_RADIO_HW_SET_WLAN_B_SUPP_MODE,
                                 /* Function/OpCode */
                                 0,
                                 /* IfIndex value if applicable */
                                 0,
                                 /* No. of Port Params */
                                 0);

            MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11bSuppMode.
                    RadInfo.au1RadioIfName, u1RadioName,
                    STRLEN (u1RadioName) + 1);

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11bSuppMode.RadInfo.
                au2WlanIfIndex[0] =
                (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11bSuppMode.RadInfo.
                u1CurrentChannel =
                RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

            WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                                    pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11bSuppMode.RadInfo.
                                    au1WlanIfname[0],
                                    &FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11bSuppMode.RadInfo.u1WlanId);

            break;

        case RADIO_TYPEG:

            NP_UTIL_FILL_PARAMS (FsHwNp,
                                 /*Generic NP structure */
                                 NP_RADIO_MODULE,
                                 /* Module ID */
                                 FS_RADIO_HW_SET_WLAN_G_SUPP_MODE,
                                 /* Function/OpCode */
                                 0,
                                 /* IfIndex value if applicable */
                                 0,
                                 /* No. of Port Params */
                                 0);

            MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11gSuppMode.
                    RadInfo.au1RadioIfName, u1RadioName,
                    STRLEN (u1RadioName) + 1);

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11gSuppMode.RadInfo.
                au2WlanIfIndex[0] =
                (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11gSuppMode.RadInfo.
                u1CurrentChannel =
                RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
            WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                                    pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11gSuppMode.RadInfo.
                                    au1WlanIfname[0],
                                    &FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11gSuppMode.RadInfo.u1WlanId);

            break;

        case RADIO_TYPEAN:
            NP_UTIL_FILL_PARAMS (FsHwNp,
                                 /*Generic NP structure */
                                 NP_RADIO_MODULE,
                                 /* Module ID */
                                 FS_RADIO_HW_SET_WLAN_AN_SUPP_MODE,
                                 /* Function/OpCode */
                                 0,
                                 /* IfIndex value if applicable */
                                 0,
                                 /* No. of Port Params */
                                 0);

            MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11anSuppMode.
                    RadInfo.au1RadioIfName, u1RadioName,
                    STRLEN (u1RadioName) + 1);

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11anSuppMode.RadInfo.
                au2WlanIfIndex[0] =
                (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11anSuppMode.RadInfo.
                u1CurrentChannel =
                RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

            WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                                    pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11anSuppMode.RadInfo.
                                    au1WlanIfname[0],
                                    &FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11anSuppMode.RadInfo.u1WlanId);
            break;
        case RADIO_TYPE_BG:
        case RADIO_TYPEGN:
            NP_UTIL_FILL_PARAMS (FsHwNp,
                                 /*Generic NP structure */
                                 NP_RADIO_MODULE,
                                 /* Module ID */
                                 FS_RADIO_HW_SET_WLAN_GN_SUPP_MODE,
                                 /* Function/OpCode */
                                 0,
                                 /* IfIndex value if applicable */
                                 0,
                                 /* No. of Port Params */
                                 0);

            MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11gnSuppMode.
                    RadInfo.au1RadioIfName, u1RadioName,
                    STRLEN (u1RadioName) + 1);

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11gnSuppMode.RadInfo.
                au2WlanIfIndex[0] =
                (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11gnSuppMode.RadInfo.
                u1CurrentChannel =
                RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

            WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                                    pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11gnSuppMode.RadInfo.
                                    au1WlanIfname[0],
                                    &FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11gnSuppMode.RadInfo.u1WlanId);

            u4RadioType = FS_RADIO_HW_SET_WLAN_GN_SUPP_MODE;
            break;

        case RADIO_TYPE_AC:
            NP_UTIL_FILL_PARAMS (FsHwNp,
                                 /*Generic NP structure */
                                 NP_RADIO_MODULE,
                                 /* Module ID */
                                 FS_RADIO_HW_SET_WLAN_AC_SUPP_MODE,
                                 /* Function/OpCode */
                                 0,
                                 /* IfIndex value if applicable */
                                 0,
                                 /* No. of Port Params */
                                 0);

            MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11acSuppMode.
                    RadInfo.au1RadioIfName, u1RadioName,
                    STRLEN (u1RadioName) + 1);

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11acSuppMode.RadInfo.
                au2WlanIfIndex[0] =
                (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11acSuppMode.RadInfo.
                u1CurrentChannel =
                RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
            WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                                    pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                                    FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11acSuppMode.RadInfo.
                                    au1WlanIfname[0],
                                    &FsHwNp.RadioIfNpModInfo.unOpCode.
                                    sConfigSetDot11acSuppMode.RadInfo.u1WlanId);

            u4RadioType = FS_RADIO_HW_SET_WLAN_AC_SUPP_MODE;
            break;

        default:
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "Invalid Radio Type received\r\n");
            return OSIX_FAILURE;
    }

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetDot11xSuppMode:NpUtilHwProgram  failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanCreateAthInterface
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Creation of Ath Interface                                  *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanCreateAthInterface (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tRadioIfGetDB       RadioIfGetDB;
    tFsHwNp             FsHwNp;
    UINT1               u1RadioId = 0;
    UINT1               u1VId = 0;
    UINT1               au1InterfaceName[6] = "\0";
    UINT1               u1WlanId = 0;
    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_WLAN_CREATE_ATH,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
            au1RadioIfName, u1RadioName, STRLEN (u1RadioName) + 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
        au2WlanIfIndex[0] =
        (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.u1WlanId =
        pWssWlanDB->WssWlanAttributeDB.u1WlanId;

    RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
        pWssWlanDB->WssWlanAttributeDB.u1RadioId;

    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;

    WssWlanGetWlanIntfName (RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11WlanCreate.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11WlanCreate.RadInfo.u1WlanId);

    if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanCreateAthInterface:Getting Radio Type failed\r\n");
        return OSIX_FAILURE;
    }
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
        u4Dot11RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
            au1SSID, pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
            WSSWLAN_SSID_NAME_LEN);
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
            au1WlanIfname[0], WSS_WLAN_NAME_MAX_LEN);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanCreateAthInterface:NpUtilHwProgram  failed\r\n");
        return OSIX_FAILURE;
    }

#ifdef KERNEL_CAPWAP_WANTED

    WssIfWmmQueueCreation (pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName);

    /* Kernel Add Device */
    CapwapAddDeviceInKernel (pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName);

#endif

    WSSWLAN_FN_EXIT ();
    UNUSED_PARAM (u1WlanId);
    UNUSED_PARAM (au1InterfaceName);
    UNUSED_PARAM (u1VId);
    UNUSED_PARAM (u1RadioId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetAthHwAddr
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : AthHardware Address                                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanGetAthHwAddr (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_GET_ATH_HW_ADDR,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11AthHwAddr.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11AthHwAddr.
        RadInfo.au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigGetDot11AthHwAddr.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigGetDot11AthHwAddr.RadInfo.u1WlanId);
    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetHideSsid : NpUtilHwProgram failed\r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.BssId,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetDot11AthHwAddr.
            au1AthHwAddr, MAC_ADDR_LEN);

    WSSWLAN_TRC6 (WSSWLAN_MGMT_TRC, "BSsid : %x:%x:%x:%x:%x:%x\n",
                  pWssWlanDB->WssWlanAttributeDB.BssId[0],
                  pWssWlanDB->WssWlanAttributeDB.BssId[1],
                  pWssWlanDB->WssWlanAttributeDB.BssId[2],
                  pWssWlanDB->WssWlanAttributeDB.BssId[3],
                  pWssWlanDB->WssWlanAttributeDB.BssId[4],
                  pWssWlanDB->WssWlanAttributeDB.BssId[5]);

    pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetDesiredSsid
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Desired SSID written to hardware                           *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetDesiredSsid (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_ESSID,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms
     */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Essid.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Essid.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Essid.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Essid.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.u1WlanId =
        pWssWlanDB->WssWlanAttributeDB.u1WlanId;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Essid.
            au1ValDot11Essid,
            pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid));

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetPowerConstraint
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Set the Power Constraint in shadow DB                      *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetPowerConstraint (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_POWER_CONSTRAINT,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms
     */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11PowerConstraint.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11PowerConstraint.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11PowerConstraint.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11PowerConstraint.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11PowerConstraint.
        u1PowerConstraint = pWssWlanDB->WssWlanAttributeDB.u1PowerConstraint;

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram: Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanMulticastMode                                       *
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : Set the Capability in shadow DB                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanMulticastMode (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_MULTICAST_MODE,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
        u2WlanMulticastMode =
        pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode;
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

UINT1
WssWlanSetWpaIEElements (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    UINT1               u1Count;
    UINT1               au1WpaCipherSuiteTKIP[] = { 0x00, 0x50, 0xF2, 2 };
    UINT1               au1WpaCipherSuiteCCMP[] = { 0x00, 0x50, 0xF2, 4 };
    UINT1               au1WpaAuthKeyMgmt8021X[] = { 0x00, 0x50, 0xF2, 1 };
    UINT1               au1WpaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x50, 0xF2, 2 };

    UINT1               au1WpaAuthKeyMgmt8021XSHA256[] =
        { 0x00, 0x50, 0xF2, 5 };
    UINT1               au1WpaAuthKeyMgmtPskSHA256Over8021X[] =
        { 0x00, 0x50, 0xF2, 6 };

    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_WPA_PROTECTION,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0, 0);
    /* No. of PortList Parms */
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    sprintf ((char *) FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
             RadInfo.au1WlanIfname[0],
             "%s%d", RADIO_ATH_INTF_NAME,
             pWssWlanDB->WssWlanAttributeDB.u1WlanId - 1);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.RadInfo.u1WlanId =
        pWssWlanDB->WssWlanAttributeDB.u1WlanId;

    if (pWssWlanDB->WssWlanAttributeDB.u1KeyStatus == 0)
    {
        if (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u1ElemId !=
            VENDOR_WPA_TYPE)
        {
            /* Wpa Disabled Case */
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                u1WpaStatus = RSNA_DISABLED;

        }
        else if (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u1ElemId ==
                 VENDOR_WPA_TYPE)
        {

            /* Set rsna as enable */
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                u1WpaStatus = WPA_ENABLED;
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.u1WpaLen =
                pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u1Len;
            /* Setting the group cipher */
            if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                        au1GroupCipherSuite,
                        au1WpaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)

            {
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                    u1GroupCipher = 1;
                /*IEEE80211_CIPHER_TKIP */
            }
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                u2PairwiseCipherSuiteCount =
                pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                u2PairwiseCipherSuiteCount;
            for (u1Count = 0;
                 u1Count <
                 FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                 u2PairwiseCipherSuiteCount; u1Count++)
            {
                if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                            aWpaPwCipherDB[u1Count].au1PairwiseCipher,
                            au1WpaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)

                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                        au1PairwiseCipher[u1Count] = 1;
                    /*IEEE80211_CIPHER_TKIP */
                }
                if (MEMCMP
                    (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                     aWpaPwCipherDB[u1Count].au1PairwiseCipher,
                     au1WpaCipherSuiteCCMP, RSNA_CIPHER_SUITE_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                        au1PairwiseCipher[u1Count] = 3;
                }
            }
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                u2AKMSuiteCount =
                pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u2AKMSuiteCount;

            for (u1Count = 0;
                 u1Count <
                 FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.
                 u2AKMSuiteCount; u1Count++)
            {
                if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                            aWpaAkmDB[u1Count].au1AKMSuite,
                            au1WpaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.u1AKMSuite = 1;    /*RSNA_ASE_8021X_UNSPEC */
                }
                else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                                 aWpaAkmDB[u1Count].au1AKMSuite,
                                 au1WpaAuthKeyMgmtPskOver8021X,
                                 RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.u1AKMSuite = 2;    /*RSNA_ASE_8021X_PSK */
                }
                if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                            aWpaAkmDB[u1Count].au1AKMSuite,
                            au1WpaAuthKeyMgmt8021XSHA256,
                            RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.u1AKMSuite = 5;    /*RSNA_ASE_8021X_UNSPEC SHA 256 */
                }
                else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                                 aWpaAkmDB[u1Count].au1AKMSuite,
                                 au1WpaAuthKeyMgmtPskSHA256Over8021X,
                                 RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpaIEParams.u1AKMSuite = 6;    /*RSNA_ASE_8021X_PSK SHA 256 */
                }
            }
        }
    }
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;

}

UINT1
WssWlanIsolationEnable (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_WLAN_ISOLATION,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWlanIsolation.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWlanIsolation.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetWlanIsolation.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetWlanIsolation.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWlanIsolation.
        u2WlanIsolationiEnable = pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation;
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanMulticastLength                                     *
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : Set the Capability in shadow DB                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanMulticastLength (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_MULTICAST_LENGTH,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
        u2WlanSnoopTableLength =
        pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength;
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanMulticastTimer                                      *
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : Set the Capability in shadow DB                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanMulticastTimer (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_MULTICAST_TIMER,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
        u4WlanSnoopTimer = pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer;
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanMulticastTimeout                                    *
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : Set the Capability in shadow DB                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanMulticastTimeout (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_MULTICAST_TIMEOUT,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Multicast.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Multicast.
        u4WlanSnoopTimeout = pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout;
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetCapability
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Set the Capability in shadow DB                            *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetCapability (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_CAPABILITY,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Capability.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Capability.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Capability.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11Capability.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11Capability.
        u2Capability = pWssWlanDB->WssWlanAttributeDB.u2Capability;

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetHideSsid
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Value of Hide SSID written to hardware                     *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetHideSsid (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_HIDE_ESSID,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetDot11HideEssid.RadInfo.au1RadioIfName,
            u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11HideEssid.
        RadInfo.au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11HideEssid.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11HideEssid.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11HideEssid.u1ValDot11Essid =
        pWssWlanDB->WssWlanAttributeDB.u1SupressSsid;

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
* *                                                                           *
* * Function     : WssWlanSetdot11AC                                          *
* *                                                                           *
* * Description  : WLAN                                                       *
* *                                                                           *
* * Input        : Pointer to tWssWlanDB, RadioName                           *
* *                                                                           *
* * Output       : Configure dot11ac                                          *
* *                                                                           *
* * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
* *                OSIX_FAILURE, otherwise                                    *
* *                                                                           *
******************************************************************************/
UINT1
WssWlanSetdot11AC (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_DOT11AC,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                            RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
                            RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.RadInfo.u1WlanId =
        pWssWlanDB->WssWlanAttributeDB.u1WlanId;

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.u1Dot11acSupport =
        pWssWlanDB->WssWlanAttributeDB.u1Dot11acSupport;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTCapabilities.
        u4VhtCapaInfo =
        pWssWlanDB->WssWlanAttributeDB.VHTCapabilities.u4VhtCapaInfo;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTCapabilities.
            au1SuppMCS,
            pWssWlanDB->WssWlanAttributeDB.VHTCapabilities.au1SuppMCS,
            sizeof (pWssWlanDB->WssWlanAttributeDB.VHTCapabilities.au1SuppMCS));

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTOperation.
            au1VhtOperInfo,
            pWssWlanDB->WssWlanAttributeDB.VHTOperation.au1VhtOperInfo,
            sizeof (pWssWlanDB->WssWlanAttributeDB.VHTOperation.
                    au1VhtOperInfo));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.VHTOperation.u2BasicMCS
        = pWssWlanDB->WssWlanAttributeDB.VHTOperation.u2BasicMCS;

    if (pWssWlanDB->WssWlanAttributeDB.OperModeNotify.isOptional == OSIX_TRUE)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.OperModeNotify.
            u1VhtOperModeElem = pWssWlanDB->WssWlanAttributeDB.OperModeNotify.
            u1VhtOperModeElem;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.OperModeNotify.
            isOptional = OSIX_TRUE;
    }

    /*Vht Transmit Power Envelope sub element */
    if (pWssWlanDB->WssWlanAttributeDB.VhtTxPower.isOptional == OSIX_TRUE)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.isOptional = OSIX_TRUE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TransPower = pWssWlanDB->
            WssWlanAttributeDB.VhtTxPower.u1TransPower;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TxPower20 = pWssWlanDB->
            WssWlanAttributeDB.VhtTxPower.u1TxPower20;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TxPower40 = pWssWlanDB->
            WssWlanAttributeDB.VhtTxPower.u1TxPower40;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TxPower80 = pWssWlanDB->
            WssWlanAttributeDB.VhtTxPower.u1TxPower80;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            VhtTxPower.u1TxPower160 = pWssWlanDB->
            WssWlanAttributeDB.VhtTxPower.u1TxPower160;
    }

/*Channel Switch Wrapper element*/
    if (pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.
        isOptional == OSIX_TRUE)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.isOptional = OSIX_TRUE;

/*Wide bandwidth Channel Switch sub element*/
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
            isOptional = OSIX_TRUE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
            u1ChanWidth = pWssWlanDB->WssWlanAttributeDB.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.u1ChanWidth;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
            u1CenterFcy0 = pWssWlanDB->WssWlanAttributeDB.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.u1CenterFcy0;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.
            u1CenterFcy1 = pWssWlanDB->WssWlanAttributeDB.
            ChanSwitchWrapper.WrapperSubElem.ChanwidthSwitch.u1CenterFcy1;

/*Vht Transmit Power Envelope sub element*/
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.VhtTxPower.isOptional = OSIX_TRUE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.VhtTxPower.u1TransPower
            = pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.
            WrapperSubElem.VhtTxPower.u1TransPower;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.VhtTxPower.u1TxPower20
            = pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.
            WrapperSubElem.VhtTxPower.u1TxPower20;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.VhtTxPower.u1TxPower40
            = pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.
            WrapperSubElem.VhtTxPower.u1TxPower40;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.VhtTxPower.u1TxPower80
            = pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.
            WrapperSubElem.VhtTxPower.u1TxPower80;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11ac.
            ChanSwitchWrapper.WrapperSubElem.VhtTxPower.u1TxPower160
            = pWssWlanDB->WssWlanAttributeDB.ChanSwitchWrapper.
            WrapperSubElem.VhtTxPower.u1TxPower160;
    }
    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;

}

#ifdef ROGUEAP_WANTED
UINT1
WssWlanRougeApDetatils (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_ROUGE_AP_DETECTION,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0, 0);
    /* No. of PortList Parms */
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    sprintf ((char *) FsHwNp.RadioIfNpModInfo.unOpCode.
             sConfigSetRougeApDetection.RadInfo.au1WlanIfname[0], "%s%d",
             RADIO_ATH_INTF_NAME, pWssWlanDB->WssWlanAttributeDB.u1WlanId - 1);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.RadInfo.
        u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.
        u1RougeApDetectionEnable = gu1RogueDetection;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetRougeApDetection.au1ValGroupId,
            gu1RfGroupName, RF_GROUP_NAME_LENGTH);

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;
}

#endif
/******************************************************************************
* *                                                                           *
* * Function     : WssWlanSetdot11N                          *
* *                                                                           *
* * Description  : WLAN                                                       *
* *                                                                           *
* * Input        : Pointer to tWssWlanDB, RadioName                           *
* *                                                                           *
* * Output       : Configure dot11n                                           *
* *                                                                           *
* * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
* *                OSIX_FAILURE, otherwise                                    *
* *                                                                           *
******************************************************************************/
UINT1
WssWlanSetdot11N (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_DOT11N,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                            RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                            RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.RadInfo.u1WlanId =
        pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    /* Set the 802.11n Support flag */
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1Dot11nSupport =
        pWssWlanDB->WssWlanAttributeDB.u1Dot11nSupport;

    if (pWssWlanDB->WssWlanAttributeDB.u1HTCapEnable ==
        DOT11_INFO_ELEM_HTCAPABILITY)
    {
        /*Configure HT Capabilities element */
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1HTCapEnable =
            pWssWlanDB->WssWlanAttributeDB.u1HTCapEnable;
        MEMCPY (&FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.u2HTCapInfo,
                &pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u2HTCapInfo,
                sizeof (pWssWlanDB->WssWlanAttributeDB.HTCapabilities.
                        u2HTCapInfo));
        MEMCPY (&FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.u1AMPDUParam,
                &pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u1AMPDUParam,
                sizeof (pWssWlanDB->WssWlanAttributeDB.HTCapabilities.
                        u1AMPDUParam));
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.au1SuppMCSSet,
                &pWssWlanDB->WssWlanAttributeDB.HTCapabilities.au1SuppMCSSet,
                WSSMAC_SUPP_MCS_SET);
        MEMCPY (&FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.u2HTExtCap,
                &pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u2HTExtCap,
                sizeof (pWssWlanDB->WssWlanAttributeDB.HTCapabilities.
                        u2HTExtCap));
        MEMCPY (&FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.u4TranBeamformCap,
                &pWssWlanDB->WssWlanAttributeDB.HTCapabilities.
                u4TranBeamformCap,
                sizeof (pWssWlanDB->WssWlanAttributeDB.HTCapabilities.
                        u4TranBeamformCap));
        MEMCPY (&FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.
                HTCapabilities.u1ASELCap,
                &pWssWlanDB->WssWlanAttributeDB.HTCapabilities.u1ASELCap,
                sizeof (pWssWlanDB->WssWlanAttributeDB.HTCapabilities.
                        u1ASELCap));
    }

    if (pWssWlanDB->WssWlanAttributeDB.u1HTOpeEnable ==
        DOT11_INFO_ELEM_HTOPERATION)
    {
        /*Configure HT Operation Element */
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.u1HTOpeEnable =
            pWssWlanDB->WssWlanAttributeDB.u1HTOpeEnable;
        MEMCPY (&FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
                u1PrimaryCh,
                &pWssWlanDB->WssWlanAttributeDB.HTOperation.u1PrimaryCh,
                sizeof (pWssWlanDB->WssWlanAttributeDB.HTOperation.
                        u1PrimaryCh));
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
                au1HTOpeInfo,
                pWssWlanDB->WssWlanAttributeDB.HTOperation.au1HTOpeInfo,
                WSSMAC_HTOPE_INFO);
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11n.HTOperation.
                au1BasicMCSSet,
                pWssWlanDB->WssWlanAttributeDB.HTOperation.au1BasicMCSSet,
                WSSMAC_BASIC_MCS_SET);
    }

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetStaWepKeyEncryptn
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : WepKey written to hardware                                 *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetStaWepKeyEncryptn (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_STA_WEP_KEY_EN,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetStaWepKeyEncryptn.RadInfo.au1RadioIfName,
            u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStaWepKeyEncryptn.
        RadInfo.au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetStaWepKeyEncryptn.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetStaWepKeyEncryptn.RadInfo.u1WlanId);

    MEMCPY (&(FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStaWepKeyEncryptn.
              au1ValDot11StaWepKeyEnVal),
            pWssWlanDB->WssWlanAttributeDB.au1WepKey,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.au1WepKey));
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStaWepKeyEncryptn.
        i2ValDot11StaWepKeyEnType =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1KeyType;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStaWepKeyEncryptn.
        i2ValDot11StaWepKeyEnIndex =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStaWepKeyEncryptn.
        i2ValDot11StaWepKeyAuthIndex =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1AuthenticationIndex;

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetStWpAuthKeyType
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : Station Wep Auth Type written to hardware                  *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetStWpAuthKeyType (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_STA_WEP_KEY_AUTH,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStWpKeyAuthStatus.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetStWpKeyAuthStatus.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetStWpKeyAuthStatus.RadInfo.u1WlanId);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStWpKeyAuthStatus.
        i2ValDot11StWpKeyAuthType =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1AuthMethod;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStWpKeyAuthStatus.
        i2ValDot11StWpKeyAuthStat =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1AuthAlgorithm;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetStWpKeyAuthStatus.
        i2ValDot11StWpKeyAuthIndex =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1AuthenticationIndex;

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetStaWepKeyStatus
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : WepKey status written to hardware                          *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetStaWepKeyStatus (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    tMacAddr            MacAddr;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_STA_WEP_KEY_STAT,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11StaWepKeyStatus.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11StaWepKeyStatus.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11StaWepKeyStatus.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetDot11StaWepKeyStatus.RadInfo.u1WlanId);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11StaWepKeyStatus.
        i2ValDot11StaWepKeyStat =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetDot11StaWepKeyStatus.
        i2ValDot11StaWepKeyIndex =
        (INT2) pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanDestroyAthInterface
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : Destroys the Ath interface in  hardware                    *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanDestroyAthInterface (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
#ifdef KERNEL_CAPWAP_WANTED
    UINT1               u1RadioId = 0;
    UINT1               u1VId = 0;
#endif
    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_WLAN_DESTROY_ATH,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanDestroy.RadInfo.
            au1RadioIfName, u1RadioName, STRLEN (u1RadioName) + 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
        au2WlanIfIndex[0] = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11WlanDestroy.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11WlanDestroy.RadInfo.u1WlanId);
    MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
            au1WlanIfname[0], WSS_WLAN_NAME_MAX_LEN);

#ifdef KERNEL_CAPWAP_WANTED
    CapwapRemoveDeviceInKernel (pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName);
#endif

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanDestroyAthInterface:NpUtilHwProgram failed\r\n");
        return OSIX_FAILURE;
    }

#ifdef KERNEL_CAPWAP_WANTED
#ifndef WASP_WANTED
    /* Kernel Remove Device */
    /* Handle WLAN 0 as a special case */
    if (pWssWlanDB->WssWlanAttributeDB.u1WlanId == 1)
    {
        WSSWLAN_FN_EXIT ();
        return OSIX_SUCCESS;
    }
    else
    {
        u1RadioId = u1RadioName[WLAN_NAME_PREFIX_LEN] - ASCII_FOR_ZERO;
        u1VId =
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11WlanCreate.RadInfo.
            au1WlanIfname[0][WLAN_NAME_PREFIX_LEN] - ASCII_FOR_ZERO;
        SPRINTF ((char *) pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName,
                 "linx%d", (u1VId + (u1RadioId * WLAN_INDEX_MAX)));

    }
#endif

    UNUSED_PARAM (u1VId);
    UNUSED_PARAM (u1RadioId);
#endif

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetRsnaIEElements                                   *
 * *                                                                           *
 * * Description  : Set the RSNA paramters to the hardware                     *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : RSNA parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetRsnaIEElements (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1RsnaCipherSuiteTKIP[] = { 0x00, 0x0F, 0xAC, 2 };
    UINT1               au1RsnaCipherSuiteCCMP[] = { 0x00, 0x0F, 0xAC, 4 };
    UINT1               au1RsnaAuthKeyMgmt8021X[] = { 0x00, 0x0F, 0xAC, 1 };
    UINT1               au1RsnaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x0F, 0xAC, 2 };
#ifdef PMF_WANTED
    UINT1               au1RsnaAuthKeyMgmt8021XSHA256[] =
        { 0x00, 0x0F, 0xAC, 5 };
    UINT1               au1RsnaAuthKeyMgmtPskSHA256Over8021X[] =
        { 0x00, 0x0F, 0xAC, 6 };
#endif
    UINT1               u1Count = 0;
    WSSWLAN_FN_ENTRY ();

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_RSNA_IE_PARAMS,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetRsnaIEParams.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetRsnaIEParams.RadInfo.u1WlanId);

    if (pWssWlanDB->WssWlanAttributeDB.u1KeyStatus == 0)
    {
        if (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u1ElemId !=
            WSSMAC_RSN_ELEMENT_ID)
        {
            /* Rsna Disabled Case */
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                u1RsnaStatus = RSNA_DISABLED;

        }
        else if (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u1ElemId ==
                 WSSMAC_RSN_ELEMENT_ID)
        {

            /* Set rsna as enable */
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                u1RsnaStatus = RSNA_ENABLED;

            /* Setting the group cipher */
            if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                        au1GroupCipherSuite,
                        au1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)

            {
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                    u1GroupCipher = 1;
                /*IEEE80211_CIPHER_TKIP */
            }
            else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                             au1GroupCipherSuite,
                             au1RsnaCipherSuiteCCMP,
                             RSNA_CIPHER_SUITE_LEN) == 0)
            {
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                    u1GroupCipher = 3;
                /*IEEE80211_CIPHER_AES_CCM */
            }

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                u2PairwiseCipherSuiteCount =
                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                u2PairwiseCipherSuiteCount;

            for (u1Count = 0;
                 u1Count <
                 FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                 u2PairwiseCipherSuiteCount; u1Count++)
            {
                if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                            aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                            au1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)

                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                        au1PairwiseCipher[u1Count] = 1;
                    /*IEEE80211_CIPHER_TKIP */
                }
                else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                                 aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                                 au1RsnaCipherSuiteCCMP,
                                 RSNA_CIPHER_SUITE_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                        au1PairwiseCipher[u1Count] = 3;
                    /*IEEE80211_CIPHER_AES_CCM */
                }
            }
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                u2AKMSuiteCount =
                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2AKMSuiteCount;

            for (u1Count = 0;
                 u1Count <
                 FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                 u2AKMSuiteCount; u1Count++)
            {
                if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                            aRsnaAkmDB[u1Count].au1AKMSuite,
                            au1RsnaAuthKeyMgmt8021X,
                            RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.au1AKMSuite[u1Count] = 1;    /*RSNA_ASE_8021X_UNSPEC */
                }
                else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                                 aRsnaAkmDB[u1Count].au1AKMSuite,
                                 au1RsnaAuthKeyMgmtPskOver8021X,
                                 RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.au1AKMSuite[u1Count] = 2;    /*RSNA_ASE_8021X_PSK */
                }
#ifdef PMF_WANTED
                if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                            aRsnaAkmDB[u1Count].au1AKMSuite,
                            au1RsnaAuthKeyMgmt8021XSHA256,
                            RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.au1AKMSuite[u1Count] = 5;    /*RSNA_ASE_8021X_UNSPEC SHA 256 */
                }
                else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                                 aRsnaAkmDB[u1Count].au1AKMSuite,
                                 au1RsnaAuthKeyMgmtPskSHA256Over8021X,
                                 RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.au1AKMSuite[u1Count] = 6;    /*RSNA_ASE_8021X_PSK SHA 256 */
                }

#endif
            }

#ifdef PMF_WANTED

            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.u2RsnCapab
                = pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab;
            if ((pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab &
                 RSNA_CAPABILITY_MFPR) == RSNA_CAPABILITY_MFPR ||
                (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab &
                 RSNA_CAPABILITY_MFPC) == RSNA_CAPABILITY_MFPC)
            {
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                    u1GroupMgmtCipher = 6;
                /*IEEE80211_CIPHER_BIP */
            }
#endif
            /* Set the privacy field as  enable */
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaIEParams.
                u1Privacy = 1;
        }

    }

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

#ifdef WPS_WTP_WANTED
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanWpsDisablePushButtonAfterTimerExp                   *
 * *                                                                           *
 * * Description  : disable the push button in the hardware                    *
 * *                                                                           *
 * * Input        : NONE                                                       *
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanWpsDisablePushButtonAfterTimerExp ()
{
    tFsHwNp             FsHwNp;

    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_DISABLE_WPS_PUSH_BUTTON,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetWpsAdditionalIEElements                          *
 * *                                                                           *
 * * Description  : Set the WPS paramters to the hardware                     *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : WPS parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetWpsAdditionalIEElements (UINT1 *u1RadioName, tWssWlanDB * pWssWlanDB)
{
    tFsHwNp             FsHwNp;
    UINT1               u1Count = 0;

    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_WPS_ADDITIONAL_PARAMS,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));
    STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            RadInfo.au1RadioIfName, u1RadioName);

    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetWpsAdditionalParams.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetWpsAdditionalParams.RadInfo.u1WlanId);

    if (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.bWpsAdditionalIE !=
        WPS_ENABLED)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            u1WpsAdditionalIe = OSIX_FALSE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            u1WpsStatus = OSIX_TRUE;

    }
    else
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            u1WpsStatus = OSIX_TRUE;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            u1WpsAdditionalIe = OSIX_TRUE;
    }
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.wscState =
        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.wscState;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.noOfAuthMac =
        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.noOfAuthMac;
    for (u1Count = 0;
         u1Count < pWssWlanDB->WssWlanAttributeDB.WpsIEElements.noOfAuthMac;
         u1Count++)
    {
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
                authorized_macs,
                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.authorized_macs,
                ETH_ALEN);
    }
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            uuid_e, pWssWlanDB->WssWlanAttributeDB.WpsIEElements.uuid_e, 16);
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
        configMethods =
        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.configMethods;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            au1WtpModelNumber,
            pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpModelNumber,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                    au1WtpModelNumber));
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            au1WtpSerialNumber,
            pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpSerialNumber,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                    au1WtpSerialNumber));
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            au1WtpName, pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpName,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpName));
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
        au1NoOfRadio =
        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u1WtpNoofRadio;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
        u2WtpInternalId =
        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u2WtpInternalId;

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetWpsIEElements                                   *
 * *                                                                           *
 * * Description  : Set the WPS paramters to the hardware                     *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : WPS parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetWpsIEElements (UINT1 *u1RadioName, tWssWlanDB * pWssWlanDB)
{
    tFsHwNp             FsHwNp;

    WSSWLAN_FN_ENTRY ();

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_WPS_IE_PARAMS,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetWpsIEParams.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetWpsIEParams.RadInfo.u1WlanId);

    if (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.bWpsEnabled != WPS_ENABLED)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
            u1WpsStatus = WPS_DISABLED;

    }
    else
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
            u1WpsStatus = WPS_ENABLED;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
            wscState = pWssWlanDB->WssWlanAttributeDB.WpsIEElements.wscState;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.uuid_e,
                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.uuid_e, 16);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.configMethods =
            pWssWlanDB->WssWlanAttributeDB.WpsIEElements.configMethods;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
                au1WtpModelNumber,
                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpModelNumber,
                STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpModelNumber));
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
                au1WtpSerialNumber,
                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpSerialNumber,
                STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpSerialNumber));
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsIEParams.
                au1WtpName,
                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpName,
                STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpName));
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            au1NoOfRadio =
            pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u1WtpNoofRadio;
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetWpsAdditionalParams.
            u2WtpInternalId =
            pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u2WtpInternalId;

    }
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetRsnaGroupwiseParams                              *
 * *                                                                           *
 * * Description  : Set the RSNA paramters to the hardware                     *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : RSNA parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetRsnaGroupwiseParams (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    UINT1               au1RsnaCipherSuiteTKIP[] = { 0x00, 0x0F, 0xAC, 2 };
    UINT1               au1RsnaCipherSuiteCCMP[] = { 0x00, 0x0F, 0xAC, 4 };
    WSSWLAN_FN_ENTRY ();

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_RSNA_GROUPWISE_PARAMS,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaGroupwiseParams.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));
    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetRsnaGroupwiseParams.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetRsnaGroupwiseParams.RadInfo.u1WlanId);

    /* Setting the group cipher */
    /* Setting the group cipher */
    if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                au1GroupCipherSuite,
                au1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)

    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaGroupwiseParams.
            u1GroupCipher = 1;
        /*IEEE80211_CIPHER_TKIP */
    }
    else if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                     au1GroupCipherSuite,
                     au1RsnaCipherSuiteCCMP, RSNA_CIPHER_SUITE_LEN) == 0)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaGroupwiseParams.
            u1GroupCipher = 3;
        /*IEEE80211_CIPHER_AES_CCM */
    }

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaGroupwiseParams.
        u2KeyLength = pWssWlanDB->WssWlanAttributeDB.u2KeyLength;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaGroupwiseParams.
            au1GtkKey, pWssWlanDB->WssWlanAttributeDB.au1GtkKey,
            pWssWlanDB->WssWlanAttributeDB.u2KeyLength);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaGroupwiseParams.u1KeyIndex =
        pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanCreateBrInterface                              *
 * *                                                                           *
 * * Description  : Create the mapping of Br interfaces to ath interface
                                                   *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : RSNA parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/
UINT1
WssWlanCreateBrInterface (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tRadioIfGetDB       RadioIfGetDB;
    tFsHwNp             FsHwNp;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_WLAN_CREATE_BR_INTERFACE,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11CreateBrInterface.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName) + 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11CreateBrInterface.RadInfo.
        au2WlanIfIndex[0] =
        (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11CreateBrInterface.RadInfo.
        u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11CreateBrInterface.
        u1BrInterfaceStatus =
        pWssWlanDB->WssWlanAttributeDB.u1BrInterfaceStatus;

    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11CreateBrInterface.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11CreateBrInterface.RadInfo.u1WlanId);
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11CreateBrInterface.
            au1brInterfacename, pWssWlanDB->WssWlanAttributeDB.au1InterfaceName,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.au1InterfaceName));
    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanCreateAthInterface:NpUtilHwProgram  failed\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanIntfName                                     *
 * *                                                                           *
 * * Description  : This function will return the corresponding wlan interface *
 *                  name for the given radio id and wlan id                    *
 * *                                                                           *
 * * Input        : Radio Id and Wlan Id                                       *
 * *                                                                           *
 * * Output       : RSNA parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanGetWlanIntfName (UINT1 u1RadioId, UINT1 u1WlanId,
                        UINT1 *pu1WlanIntfName, UINT1 *pu1WlanId)
{
    tFsHwNp             FsHwNp;
    WSSWLAN_FN_ENTRY ();

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_GET_WLAN_IFNAME,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetWlanIfName.u1RadioId = u1RadioId;

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetWlanIfName.u1WlanIdInRadio
        = u1WlanId;

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    *pu1WlanId = FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetWlanIfName.u1WlanId;

    MEMCPY (pu1WlanIntfName,
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetWlanIfName.
            au1WlanIntfName,
            STRLEN (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigGetWlanIfName.
                    au1WlanIntfName));

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

#ifdef BAND_SELECT_WANTED

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetHwBandSelect                                     *
 * *                                                                           *
 * * Description  : Set the bandselect attributes the hardware                 *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : NONE                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetHwBandSelect (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;

    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_BANDSELECT,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sBandSelectStatus.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sBandSelectStatus.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sBandSelectStatus.RadInfo.u1WlanId);

    if ((pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus == 1)
        && gu1GlobalBandSelectStatus)
    {
        /* Setting the group cipher */
        FsHwNp.RadioIfNpModInfo.unOpCode.sBandSelectStatus.
            u1BandSelectStatus = 1;
    }
    else
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sBandSelectStatus.
            u1BandSelectStatus = 0;
    }
    if (gu1GlobalBandSelectStatus)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sBandSelectStatus.u1BsGlobalStatus = 1;
    }
    else
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sBandSelectStatus.u1BsGlobalStatus = 0;

    }
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif

#ifdef PMF_WANTED
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetRsnaMgmtGroupwiseParams                          *
 * *                                                                           *
 * * Description  : Set the RSNA paramters to the hardware                     *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                           *
 * *                                                                           *
 * * Output       : RSNA parameters written to hardware                        *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanSetRsnaMgmtGroupwiseParams (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
#ifdef WLC_WANTED
    UINT1               au1RsnaCipherSuiteTKIP[] = { 0x00, 0x0F, 0xAC, 2 };
    UINT1               au1RsnaCipherSuiteCCMP[] = { 0x00, 0x0F, 0xAC, 4 };
#endif
    WSSWLAN_FN_ENTRY ();

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_RSNA_MGMT_GROUPWISE_PARAMS,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaMgmtGroupwiseParams.
            RadInfo.au1RadioIfName, u1RadioName, STRLEN (u1RadioName));

    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetRsnaGroupwiseParams.RadInfo.
                            au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigSetRsnaGroupwiseParams.RadInfo.u1WlanId);
    /* Setting the MGMT group cipher */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaMgmtGroupwiseParams.
        u1MgmtGroupCipher = 6;
    /*IEEE80211_CIPHER_BIP */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaMgmtGroupwiseParams.
        u2KeyLength = pWssWlanDB->WssWlanAttributeDB.u2KeyLength;

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaMgmtGroupwiseParams.
            au1IGtkKey, pWssWlanDB->WssWlanAttributeDB.au1GtkKey,
            pWssWlanDB->WssWlanAttributeDB.u2KeyLength);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaMgmtGroupwiseParams.
        u1KeyIndex = pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif

#ifdef ROGUEAP_WANTED
/******************************************************************************
* *                                                                           *
* * Function     : WssWlanRfGroupId                                           *
* *                                                                           *
* * Description  : Set the Rf Group Id in Beacons via Config Update Request.  *
* *                                                                           *
* * Input        : Pointer to WssWlanMsgStruct                                *
* *                                                                           *
* * Output       : Configure Rf Group Id in Beacons.                          *
* *                                                                           *
* * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
* *                OSIX_FAILURE, otherwise                                    *
* *                                                                           *
******************************************************************************/
UINT1
WssWlanRfGroupId (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tFsHwNp             FsHwNp;
    UINT1               au1RadioName[RADIO_INTF_NAME_LEN];

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_ROUGE_AP_DETECTION,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0, 0);

    MEMSET (au1RadioName, 0, RADIO_INTF_NAME_LEN);

    /* Radio Interface Name */
    SPRINTF ((CHR1 *) au1RadioName, "%s%d", RADIO_INTF_NAME,
             pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
             unWlanConfReq.WssWlanAddReq.u1RadioId - 1);

    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.
            RadInfo.au1RadioIfName, au1RadioName, STRLEN (au1RadioName));

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.RadInfo.
        au2WlanIfIndex[0] = pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
        unWlanConfReq.WssWlanAddReq.u1WlanId;

    /* Wlan Interface Name */
    sprintf ((char *) FsHwNp.RadioIfNpModInfo.unOpCode.
             sConfigSetRougeApDetection.RadInfo.au1WlanIfname[0], "%s%d",
             RADIO_ATH_INTF_NAME,
             pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
             WssWlanAddReq.u1WlanId - 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.
        RadInfo.u1WlanId = pWssWlanMsgStruct->unWssWlanMsg.
        WssWlanConfigReq.unWlanConfReq.WssWlanAddReq.u1WlanId;

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRougeApDetection.
        u1RougeApDetectionEnable = gu1RogueDetection;
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
            sConfigSetRougeApDetection.au1ValGroupId,
            gu1RfGroupName, RF_GROUP_NAME_LENGTH);

    /*To Set from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "NpUtilHwProgram : Values to set  Hw failed\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanAddVlanInterface
 * *                                                                           *
 * * Description  : WLAN                                                       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanDB, RadioName                          *
 * *                                                                           *
 * * Output       : Creation of Ath Interface                                  *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *****************************************************************************/

UINT1
WssWlanAddVlanInterface (tWssWlanDB * pWssWlanDB, UINT1 *u1RadioName)
{
    tFsHwNp             FsHwNp;
    WSSWLAN_FN_ENTRY ();
    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_WLAN_ADD_VLAN,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigWlanAddVlanIntf.WlanVlanInf.
        au2WlanIfIndex[0] =
        (UINT2) pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigWlanAddVlanIntf.WlanVlanInf.
        u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigWlanAddVlanIntf.WlanVlanInf.
        u2VlanId = pWssWlanDB->WssWlanAttributeDB.u2VlanId;

    WssWlanGetWlanIntfName (pWssWlanDB->WssWlanAttributeDB.u1RadioId,
                            pWssWlanDB->WssWlanAttributeDB.u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigWlanAddVlanIntf.WlanVlanInf.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigWlanAddVlanIntf.WlanVlanInf.u1WlanId);

    /*To Get from the Hw */
    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanCreateAthInterface:NpUtilHwProgram  failed\r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    UNUSED_PARAM (u1RadioName);
    return OSIX_SUCCESS;
}

#endif
#endif
