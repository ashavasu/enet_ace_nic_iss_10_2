
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: wsswlanwlcmain.c,v 1.6 2018/02/15 10:22:25 siva Exp $
 *  Description: This file contains the WSSWLAN main module APIs in WLC       *
 *                                                                            *
 ******************************************************************************/
#ifndef __WSSWLANWLCMAIN_C__
#define __WSSWLANWLCMAIN_C__

#include "wsswlaninc.h"
#include "wsscfgwlanproto.h"

#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#include "fcusglob.h"
#endif
#include "capwap.h"
#include "fscfalw.h"
#include "wssifradiodb.h"
#include "radioifinc.h"
#include "wsscfglwg.h"
#include "wssstawlcprot.h"
UINT2               gu2WlanInternaldelcount = 0;
UINT2               gu2WssWlanStaRuleCount = 0;
extern INT4         CapwapGetWtpProfileIdFromProfileName (UINT1
                                                          *pu1WtpProfileName,
                                                          UINT4
                                                          *pu4WtpProfileId);
extern VOID         WssFindTheApGroupFromWtpProfileId (UINT4,
                                                       tApGroupConfigEntry **,
                                                       tApGroupConfigEntry **);

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanConstructAddWlanMsg
 * *                                                                           *
 * * Description  : Constructs the AddWlan message packet                      *
 * *                                                                           *
 * * Input        : RadioIfIndex , ProfileId ,pointer to add wlan request      *
 * *                                                                           *
 * * Output       : Constructed AddWlan packet                                 *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if construction   succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanConstructAddWlanMsg (UINT4 u4RadioIfIndex, UINT2 u2WlanProfileId,
                            tWssWlanAddReq * pWlanGetAddReq)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
#ifdef RSNA_WANTED
    tRsnaIEElements     RsnaIEElements;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;
    UINT1               u1RsnIELength = 0;
    UINT1               au1GtkKey[RSNA_GTK_MAX_LEN];
    UINT2               u2KeyLength = 0;;
    UINT1               u1Index = 0;
    UINT1               u1KeyIndex = 0;
#ifdef PMF_WANTED
    UINT1               au1RsnaAuthKeyMgmt8021XSHA256[] =
        { 0x00, 0x0F, 0xAC, 5 };
    UINT1               au1RsnaAuthKeyMgmtPskSHA256Over8021X[] =
        { 0x00, 0x0F, 0xAC, 6 };
#endif
    UINT1               au1RsnaAuthKeyMgmt8021X[] = { 0x00, 0x0F, 0xAC, 1 };
    UINT1               au1RsnaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x0F, 0xAC, 2 };

#endif

    UINT1               u1index = 0;
    UINT1               u1WlanQosIndex = 0, u1RadioQosIndex = 0;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
#ifdef RSNA_WANTED
    MEMSET (&RsnaIEElements, 0, sizeof (tRsnaIEElements));
#endif

    /* Get WssWlanBssInterfaceDB through keys
     * RadioIfIndex and WlanProfileId */

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: BssInterface DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: Bss IfIndex DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB->WssWlanIsPresentDB.bWlanMacType = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanTunnelMode = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC
            (WSSWLAN_FAILURE_TRC,
             "WssWlanConstructAddWlanMsg: WlanProfile DB Get failed,"
             "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bKeyIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bKeyStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWepKey = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bGroupTsc = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bQosProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bSupressSsid = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWssWlanQosCapab = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: WlanIfIndex DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    /* Get pointer from struct tRadioIfDB */
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "\nWssWlanConstructAddWlanMsg:WssIfProcessRadioIfDBMsg call"
                     "for RadioIfDB failed to get RadioId and WtpInternalId\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    u1WlanQosIndex = pWssWlanDB->WssWlanAttributeDB.u1QosTraffic;
    switch (u1WlanQosIndex)
    {
        case BEST_EFFORT_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_BE;
            break;
        case VIDEO_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_VI;
            break;
        case VOICE_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_VO;
            break;
        case BACKGROUND_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_BG;
            break;
        default:
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1RadioQosIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "\nWssWlanConstructAddWlanMsg:WssIfProcessRadioIfDBMsg "
                     "call for RadioIfDB to get Qos Config failed\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    /* Construct message from the above derived pointers */
    pWlanGetAddReq->u2MessageType = WSSWLAN_ADD_WLAN_MSG_TYPE;
    pWlanGetAddReq->u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlanGetAddReq->u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlanGetAddReq->u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    pWlanGetAddReq->u2Capability = pWssWlanDB->WssWlanAttributeDB.u2Capability;

    MEMCPY (pWlanGetAddReq->au1GroupTsc,
            pWssWlanDB->WssWlanAttributeDB.au1GroupTsc,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.au1GroupTsc));

    pWlanGetAddReq->u1Qos = pWssWlanDB->WssWlanAttributeDB.u1QosTraffic;

    if (pWssWlanDB->WssWlanAttributeDB.u1AuthMethod == WSSWLAN_AUTH_METHOD_OPEN)
    {
        if (pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus == OSIX_TRUE)
        {
            if (pWssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod ==
                WEBAUTH_EXTERNAL)
            {
                pWlanGetAddReq->u1AuthType = WSSWLAN_AUTH_TYPE_WEB_EXT;
            }
            else
            {
                pWlanGetAddReq->u1AuthType = WSSWLAN_AUTH_TYPE_WEB;
            }
        }
        else
        {
            pWlanGetAddReq->u1AuthType = WSSWLAN_AUTH_TYPE_OPEN;
        }
        pWlanGetAddReq->u2KeyLength = 0;
        MEMSET (pWlanGetAddReq->au1Key, 0, WSSWLAN_WEP_KEY_LEN);
#ifdef  RSNA_WANTED
        RsnaGetDot11RSNAEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                                 &i4Dot11RSNAEnabled);
        if (i4Dot11RSNAEnabled == RSNA_ENABLED)
        {
            pWlanGetAddReq->u1KeyStatus = 0;
        }
        else
        {
            pWlanGetAddReq->u1KeyStatus =
                pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
        }
#else
        pWlanGetAddReq->u1KeyStatus =
            pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
#endif
        pWlanGetAddReq->u1KeyIndex = 0;
    }
    else if (pWssWlanDB->WssWlanAttributeDB.u1AuthMethod ==
             WSSWLAN_AUTH_METHOD_SHARED)
    {
        pWlanGetAddReq->u1AuthType = WSSWLAN_AUTH_TYPE_WEP;
        pWlanGetAddReq->u2KeyLength =
            (UINT2) (STRLEN (pWssWlanDB->WssWlanAttributeDB.au1WepKey));
        MEMCPY (pWlanGetAddReq->au1Key,
                pWssWlanDB->WssWlanAttributeDB.au1WepKey,
                STRLEN (pWssWlanDB->WssWlanAttributeDB.au1WepKey));
        /* Set the KeyStatus to '1' for WEP */
        pWlanGetAddReq->u1KeyStatus = 1;
        pWlanGetAddReq->u1KeyIndex = pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;
    }
    pWlanGetAddReq->u1MacMode = pWssWlanDB->WssWlanAttributeDB.u1MacType;
    pWlanGetAddReq->u1TunnelMode = pWssWlanDB->WssWlanAttributeDB.u1TunnelMode;
    /*When HideSSID is enabled (1) , SupressSSID should be '0' and vice versa */
    pWlanGetAddReq->u1SupressSsid =
        !(pWssWlanDB->WssWlanAttributeDB.u1SupressSsid);

    MEMCPY (pWlanGetAddReq->au1Ssid,
            pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
            STRLEN (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid));

    /* Filling Power constraint element */
    pWlanGetAddReq->WssWlanPowerConstraint.u1ElementId =
        POWER_CONSTRAINT_ELEMENT_ID;
    pWlanGetAddReq->WssWlanPowerConstraint.u1Length =
        POWER_CONSTRAINT_ELEMENT_LENGTH;
    pWlanGetAddReq->WssWlanPowerConstraint.u1LocalPowerConstraint =
        pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement;

    /* Filling the Multicast element */
    pWlanGetAddReq->WssWlanMulticast.u1ElementId = MULTI_CAST_ELEMENT_ID;
    pWlanGetAddReq->WssWlanMulticast.u1Length = MULTI_CAST_ELEMENT_LENGTH;
    pWlanGetAddReq->WssWlanMulticast.u2WlanMulticastMode =
        pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode;
    pWlanGetAddReq->WssWlanMulticast.u2WlanSnoopTableLength =
        pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength;
    pWlanGetAddReq->WssWlanMulticast.u4WlanSnoopTimer =
        pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer;
    pWlanGetAddReq->WssWlanMulticast.u4WlanSnoopTimeout =
        pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout;

    /* Filling QosCapab element */

    pWlanGetAddReq->WssWlanQosCapab.u1ElementId = QOS_CAPAB_ELEMENT_ID;
    pWlanGetAddReq->WssWlanQosCapab.u1Length = QOS_CAPAB_ELEMENT_LENGTH;

    pWlanGetAddReq->WssWlanQosCapab.u1QosInfo =
        pWssWlanDB->WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo;
    /* a 4bit value initially 0,incremented on updation */

    /* Filling EDCA Parameters */
    pWlanGetAddReq->WssWlanEdcaParam.u1ElementId = EDCA_PARAM_ELEMENT_ID;
    pWlanGetAddReq->WssWlanEdcaParam.u1Length = EDCA_PARAM_ELEMENT_LENGTH;
    pWlanGetAddReq->WssWlanEdcaParam.u1QosInfo =
        pWssWlanDB->WssWlanAttributeDB.WssWlanEdcaParam.u1QosInfo;

    /* Filling TxOpLimit of Ecda */

    u1index = pWssWlanDB->WssWlanAttributeDB.u1QosTraffic;
    if (u1index == BEST_EFFORT_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_BE_ParameterRecord.u2TxOpLimit =
            RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1RadioQosIndex - 1];
    }
    else if (u1index == VIDEO_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_VI_ParameterRecord.u2TxOpLimit =
            RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1RadioQosIndex - 1];
    }
    else if (u1index == VOICE_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_VO_ParameterRecord.u2TxOpLimit =
            RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1RadioQosIndex - 1];
    }
    else if (u1index == BACKGROUND_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_BK_ParameterRecord.u2TxOpLimit =
            RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1RadioQosIndex - 1];
    }

    /* Filling u1ACI_AIFSN of Ecda */

    if (u1index == BEST_EFFORT_ACI)
    {

        pWlanGetAddReq->WssWlanEdcaParam.AC_BE_ParameterRecord.u1ACI_AIFSN =
            (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.
                      au1Aifsn[u1RadioQosIndex - 1]) +
                     (RadioIfGetDB.RadioIfGetAllDB.
                      au1AdmissionControl[u1RadioQosIndex -
                                          1] << SHIFT_VALUE_4) +
                     (BEST_EFFORT_ACI << SHIFT_VALUE_5));
    }
    else if (u1index == VIDEO_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_VI_ParameterRecord.u1ACI_AIFSN =
            (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.
                      au1Aifsn[u1RadioQosIndex - 1]) +
                     (RadioIfGetDB.RadioIfGetAllDB.
                      au1AdmissionControl[u1RadioQosIndex -
                                          1] << SHIFT_VALUE_4) +
                     (VIDEO_ACI << SHIFT_VALUE_5));
    }
    else if (u1index == VOICE_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_VO_ParameterRecord.u1ACI_AIFSN =
            (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.
                      au1Aifsn[u1RadioQosIndex - 1]) +
                     (RadioIfGetDB.RadioIfGetAllDB.
                      au1AdmissionControl[u1RadioQosIndex -
                                          1] << SHIFT_VALUE_4) +
                     (VOICE_ACI << SHIFT_VALUE_5));
    }
    else if (u1index == BACKGROUND_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_BK_ParameterRecord.u1ACI_AIFSN =
            (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.
                      au1Aifsn[u1RadioQosIndex - 1]) +
                     (RadioIfGetDB.RadioIfGetAllDB.
                      au1AdmissionControl[u1RadioQosIndex -
                                          1] << SHIFT_VALUE_4) +
                     (BACKGROUND_ACI << SHIFT_VALUE_5));
    }
    /* Filling u1ECWmin_ECWmax of Edca */
    if (u1index == BEST_EFFORT_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_BE_ParameterRecord.u1ECWmin_ECWmax =
            (((UINT1)
              (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1RadioQosIndex - 1]
                    + 1) / log (2)) << SHIFT_VALUE_4) +
             (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1RadioQosIndex - 1]
                   + 1) / log (2)));
    }
    else if (u1index == VIDEO_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_VI_ParameterRecord.u1ECWmin_ECWmax =
            (((UINT1)
              (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1RadioQosIndex - 1]
                    + 1) / log (2)) << SHIFT_VALUE_4) +
             (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1RadioQosIndex - 1]
                   + 1) / log (2)));
    }
    else if (u1index == VOICE_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_VO_ParameterRecord.u1ECWmin_ECWmax =
            (((UINT1)
              (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1RadioQosIndex - 1]
                    + 1) / log (2)) << SHIFT_VALUE_4) +
             (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1RadioQosIndex - 1]
                   + 1) / log (2)));
    }
    else if (u1index == BACKGROUND_ACI)
    {
        pWlanGetAddReq->WssWlanEdcaParam.AC_BK_ParameterRecord.u1ECWmin_ECWmax =
            (((UINT1)
              (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1RadioQosIndex - 1]
                    + 1) / log (2)) << SHIFT_VALUE_4) +
             (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1RadioQosIndex - 1]
                   + 1) / log (2)));
    }

#ifdef RSNA_WANTED
    /* RSNA ELEMENTS SHOULD BE FILLED */
    RsnaGetDot11RSNAEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                             &i4Dot11RSNAEnabled);
    if (i4Dot11RSNAEnabled == RSNA_ENABLED)
    {
        /* To get the GTK key from RSNA module for Update WLAN message */
        if (RsnaGetRsnGtkKey (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                              au1GtkKey, &u2KeyLength,
                              &u1KeyIndex) == OSIX_SUCCESS)
        {
            if (u2KeyLength != 0 && u1KeyIndex != 0)
            {
                pWlanGetAddReq->u2KeyLength = u2KeyLength;
                MEMCPY (pWlanGetAddReq->au1Key, au1GtkKey,
                        pWlanGetAddReq->u2KeyLength);
                pWlanGetAddReq->u1KeyIndex = u1KeyIndex;
            }
        }

        if (RsnaGetRsnIEParams
            (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
             &RsnaIEElements) == OSIX_SUCCESS)
        {
            pWlanGetAddReq->RsnaIEElements.u1ElemId = RsnaIEElements.u1ElemId;
            u1RsnIELength = u1RsnIELength + RSN_IE_ELEMENT_LENGTH;

            pWlanGetAddReq->RsnaIEElements.u1Len = RsnaIEElements.u1Len;
            u1RsnIELength = u1RsnIELength + RSN_IE_LENGTH_FIELD;

            pWlanGetAddReq->RsnaIEElements.u2Ver = RsnaIEElements.u2Ver;
            u1RsnIELength = u1RsnIELength + RSN_IE_VERSION_LENGTH;

            MEMCPY (pWlanGetAddReq->RsnaIEElements.au1GroupCipherSuite,
                    RsnaIEElements.au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);
            u1RsnIELength = u1RsnIELength + RSNA_CIPHER_SUITE_LEN;

            pWlanGetAddReq->RsnaIEElements.u2PairwiseCipherSuiteCount =
                RsnaIEElements.u2PairwiseCipherSuiteCount;
            u1RsnIELength = u1RsnIELength + RSNA_PAIRWISE_CIPHER_COUNT;

            for (u1Index = 0;
                 u1Index < RsnaIEElements.u2PairwiseCipherSuiteCount
                 && u1Index < RSNA_PAIRWISE_CIPHER_COUNT; u1Index++)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aRsnaPwCipherDB[u1Index].
                        au1PairwiseCipher,
                        RsnaIEElements.aRsnaPwCipherDB[u1Index].
                        au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_CIPHER_SUITE_LEN;
            }
            pWlanGetAddReq->RsnaIEElements.u2AKMSuiteCount =
                RsnaIEElements.u2AKMSuiteCount;
/*        u1RsnIELength = u1RsnIELength + RSNA_AKM_SUITE_COUNT;*/
            u1RsnIELength = u1RsnIELength + 2;
            /*To Set single AKM suit configured per SSID, Checking whether
             *the SSID is having passphrase, if so then considering the SSID
             *as PSK, else mapping the SSID to Dot1x.*/
            /*This should be taken care when additional AKM suits are 
             *added like PMF*/
            if (MEMCMP
                (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                 au1RsnaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aRsnaAkmDB[0].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
            else if (MEMCMP
                     (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                      au1AKMSuite, au1RsnaAuthKeyMgmt8021X,
                      RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aRsnaAkmDB[0].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
#ifdef PMF_WANTED
            else if (MEMCMP
                     (RsnaIEElements.
                      aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 - 1].au1AKMSuite,
                      au1RsnaAuthKeyMgmt8021XSHA256,
                      RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aRsnaAkmDB[0].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
            else if (MEMCMP
                     (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
                      au1AKMSuite, au1RsnaAuthKeyMgmtPskSHA256Over8021X,
                      RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aRsnaAkmDB[0].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
#endif
#ifdef DEBUG_WANTED
            for (u1Index = 0;
                 u1Index < RsnaIEElements.u2AKMSuiteCount
                 && u1Index < RSNA_AKM_SUITE_COUNT; u1Index++)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aRsnaAkmDB[u1Index].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[u1Index].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
#endif
            pWlanGetAddReq->RsnaIEElements.u2RsnCapab =
                RsnaIEElements.u2RsnCapab;
            u1RsnIELength = u1RsnIELength + RSN_IE_CAPAB_LENGTH;

            pWlanGetAddReq->RsnaIEElements.u2PmkidCount =
                RsnaIEElements.u2PmkidCount;

            if (RsnaIEElements.u2PmkidCount != 0)
            {
                /* The pmkid count field should be considered only when pmkid count
                 * is not zero */
                u1RsnIELength = u1RsnIELength + RSN_IE_PMKID_COUNT_LENGTH;

                /* TODO: need to revisit to fill the PMK id list */
                MEMCPY (pWlanGetAddReq->RsnaIEElements.aPmkidList,
                        RsnaIEElements.aPmkidList, RSNA_PMKID_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_PMKID_LEN;
            }

#ifdef PMF_WANTED

            if ((RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPR) ==
                RSNA_CAPABILITY_MFPR
                || (RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPC) ==
                RSNA_CAPABILITY_MFPC)
            {
                MEMCPY (pWlanGetAddReq->RsnaIEElements.au1GroupMgmtCipherSuite,
                        RsnaIEElements.au1GroupMgmtCipherSuite,
                        RSNA_MAX_CIPHER_TYPE_LENGTH);

                u1RsnIELength =
                    (UINT1) (u1RsnIELength + RSNA_MAX_CIPHER_TYPE_LENGTH);
            }

#endif
            /* Calculate and assign for Message Length */
            pWlanGetAddReq->u2MessageLength = sizeof (tWssWlanAddReq) -
                WSS_WLAN_HEADER_LEN - sizeof (tRsnaIEElements) + u1RsnIELength;
            /* Length excludes MsgType and MsgLength */
        }
    }
    /* If rsna module status is not enabled, have the default 
     * behavior, set the length without RSNA elements*/
    else
    {
        pWlanGetAddReq->u2MessageLength = sizeof (tWssWlanAddReq) -
            WSS_WLAN_HEADER_LEN - sizeof (tRsnaIEElements);
        /* Length excludes MsgType and MsgLength */
    }
#else
    pWlanGetAddReq->u2MessageLength =
        sizeof (tWssWlanAddReq) - WSS_WLAN_HEADER_LEN -
        sizeof (tRsnaIEElements);
    /* Length excludes MsgType and MsgLength */
#endif
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanConstructUpdateWlanMsg
 * *                                                                           *
 * * Description  : Constructs Wlan Updation request packet                    *
 * *                                                                           *
 * * Input        : RadioIfIndex,WlanProfileId,pointer to WlanUpdateReq        *
 * *                                                                           *
 * * Output       : Constructed Wlan update packet                             *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if construction   succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanConstructUpdateWlanMsg (UINT4 u4RadioIfIndex, UINT2 u2WlanProfileId,
                               tWssWlanUpdateReq * pWlanGetUpdateReq)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
#ifdef RSNA_WANTED
    tRsnaIEElements     RsnaIEElements;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;
    UINT1               u1RsnIELength = 0;
    UINT1               au1GtkKey[RSNA_GTK_MAX_LEN];
    UINT2               u2KeyLength = 0;;
    UINT1               u1KeyIndex = 0;
    UINT1               u1Count = 0;
    UINT1               au1RsnaAuthKeyMgmt8021X[] = { 0x00, 0x0F, 0xAC, 1 };
    UINT1               au1RsnaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x0F, 0xAC, 2 };
#ifdef PMF_DEBUG_WANTED
    UINT1               au1IGtkKey[RSNA_IGTK_MAX_LEN];
    UINT1               au1RsnaAuthKeyMgmt8021XSHA256[] =
        { 0x00, 0x0F, 0xAC, 5 };
    UINT1               au1RsnaAuthKeyMgmtPskSHA256Over8021X[] =
        { 0x00, 0x0F, 0xAC, 6 };
#endif
#endif

    UINT1               u1WlanQosIndex = 0, u1RadioQosIndex = 0;
    UINT1               u1index = 0;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructUpdateWlanMsg:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
#ifdef RSNA_WANTED
    MEMSET (&RsnaIEElements, 0, sizeof (tRsnaIEElements));
#endif

    /* Get WssWlanBssInterfaceDB through keys
     * RadioIfIndex and WlanProfileId */
    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructUpdateWlanMsg: BssInterface DB Get failed"
                     " return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructUpdateWlanMsg: Bss IfIndex DB Get failed"
                     " return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB->WssWlanIsPresentDB.bWlanMacType = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanTunnelMode = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructUpdateWlanMsg: WlanProfile DB Get failed"
                     " return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWepKey = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bKeyIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bKeyStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWssWlanQosCapab = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructUpdateWlanMsg: IfIndex  DB Get failed,"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    /* Get pointer from struct tRadioIfDB */
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfQosConfig = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB))
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructUpdateWlanMsg:WssIfProcessRadioIfDBMsg call\
                for RadioIfDB failed, return failure");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    /* Construct message from the above derived pointers */
    u1WlanQosIndex = pWssWlanDB->WssWlanAttributeDB.u1QosTraffic;
    switch (u1WlanQosIndex)
    {
        case BEST_EFFORT_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_BE;
            break;
        case VIDEO_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_VI;
            break;
        case VOICE_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_VO;
            break;
        case BACKGROUND_ACI:
            u1RadioQosIndex = RADIO_QOS_IND_BG;
            break;
        default:
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = u1RadioQosIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "\nWssWlanConstructAddWlanMsg:WssIfProcessRadioIfDBMsg "
                     "call for RadioIfDB to get Qos Config failed\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    pWlanGetUpdateReq->u2Capability =
        pWssWlanDB->WssWlanAttributeDB.u2Capability;

    pWlanGetUpdateReq->u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlanGetUpdateReq->u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlanGetUpdateReq->u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    pWlanGetUpdateReq->u2KeyLength =
        (UINT2) (STRLEN (pWssWlanDB->WssWlanAttributeDB.au1WepKey));
    MEMCPY (pWlanGetUpdateReq->au1Key, pWssWlanDB->WssWlanAttributeDB.au1WepKey,
            pWlanGetUpdateReq->u2KeyLength);
    pWlanGetUpdateReq->u1KeyIndex = pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;
#ifdef RSNA_WANTED
    RsnaGetDot11RSNAEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                             &i4Dot11RSNAEnabled);
    if (i4Dot11RSNAEnabled == RSNA_ENABLED)
    {
        pWlanGetUpdateReq->u1KeyStatus = 0;
    }
    else
    {
        pWlanGetUpdateReq->u1KeyStatus =
            pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
    }
#else
    pWlanGetUpdateReq->u1KeyStatus = pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
#endif
    pWlanGetUpdateReq->u1Qos = pWssWlanDB->WssWlanAttributeDB.u1QosTraffic;

    /* Filling Power constraint element */
    pWlanGetUpdateReq->WssWlanPowerConstraint.u1ElementId =
        POWER_CONSTRAINT_ELEMENT_ID;
    pWlanGetUpdateReq->WssWlanPowerConstraint.u1Length =
        POWER_CONSTRAINT_ELEMENT_LENGTH;
    pWlanGetUpdateReq->WssWlanPowerConstraint.u1LocalPowerConstraint =
        pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement;

    /* Filling the Multicast element */
    pWlanGetUpdateReq->WssWlanMulticast.u1ElementId = MULTI_CAST_ELEMENT_ID;
    pWlanGetUpdateReq->WssWlanMulticast.u1Length = MULTI_CAST_ELEMENT_LENGTH;
    pWlanGetUpdateReq->WssWlanMulticast.u2WlanMulticastMode =
        pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode;
    pWlanGetUpdateReq->WssWlanMulticast.u2WlanSnoopTableLength =
        pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength;
    pWlanGetUpdateReq->WssWlanMulticast.u4WlanSnoopTimer =
        pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer;
    pWlanGetUpdateReq->WssWlanMulticast.u4WlanSnoopTimeout =
        pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout;
    /* Filling QosCapab element */

    pWlanGetUpdateReq->WssWlanQosCapab.u1ElementId = QOS_CAPAB_ELEMENT_ID;
    pWlanGetUpdateReq->WssWlanQosCapab.u1Length = QOS_CAPAB_ELEMENT_LENGTH;

    pWlanGetUpdateReq->WssWlanQosCapab.u1QosInfo =
        pWssWlanDB->WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo;
    /* a 4bit value initially 0,incremented on updation */

    /* Filling EDCA Parameters */
    pWlanGetUpdateReq->WssWlanEdcaParam.u1ElementId = EDCA_PARAM_ELEMENT_ID;
    pWlanGetUpdateReq->WssWlanEdcaParam.u1Length = EDCA_PARAM_ELEMENT_LENGTH;
    pWlanGetUpdateReq->WssWlanEdcaParam.u1QosInfo =
        pWssWlanDB->WssWlanAttributeDB.WssWlanEdcaParam.u1QosInfo;
    /* Filling TxOpLimit of Edca */
    u1index = 0;
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_BE_ParameterRecord.u2TxOpLimit =
        RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1index++];
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_VI_ParameterRecord.u2TxOpLimit =
        RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1index++];
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_VO_ParameterRecord.u2TxOpLimit =
        RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1index++];
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_BK_ParameterRecord.u2TxOpLimit =
        RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1index++];
    /* Filling u1ACI_AIFSN of Ecda */
    u1index = 0;

    pWlanGetUpdateReq->WssWlanEdcaParam.AC_BE_ParameterRecord.u1ACI_AIFSN =
        (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1index]
                  << SHIFT_VALUE_4) +
                 (RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[u1index]
                  << SHIFT_VALUE_3) + (BEST_EFFORT_ACI << SHIFT_VALUE_1));
    u1index++;
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_VI_ParameterRecord.u1ACI_AIFSN =
        (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1index]
                  << SHIFT_VALUE_4) +
                 (RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[u1index]
                  << SHIFT_VALUE_3) + (VIDEO_ACI << SHIFT_VALUE_1));
    u1index++;

    pWlanGetUpdateReq->WssWlanEdcaParam.AC_VO_ParameterRecord.u1ACI_AIFSN =
        (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1index]
                  << SHIFT_VALUE_4) +
                 (RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[u1index]
                  << SHIFT_VALUE_3) + (VOICE_ACI << SHIFT_VALUE_1));
    u1index++;
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_BK_ParameterRecord.u1ACI_AIFSN =
        (UINT1) ((RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1index]
                  << SHIFT_VALUE_4) +
                 (RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[u1index]
                  << SHIFT_VALUE_3) + (BACKGROUND_ACI << SHIFT_VALUE_1));

    /* Filling u1ECWmin_ECWmax of Edca */
    u1index = 0;
    pWlanGetUpdateReq->WssWlanEdcaParam.AC_BE_ParameterRecord.u1ECWmin_ECWmax =
        ((UINT1)
         (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1index]
               + 1) / log (2)) << SHIFT_VALUE_4) +
        (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1index] + 1) / log (2));
    u1index++;

    pWlanGetUpdateReq->WssWlanEdcaParam.AC_BK_ParameterRecord.u1ECWmin_ECWmax =
        ((UINT1)
         (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1index]
               + 1) / log (2)) << SHIFT_VALUE_4) +
        (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1index] + 1) / log (2));
    u1index++;

    pWlanGetUpdateReq->WssWlanEdcaParam.AC_VI_ParameterRecord.u1ECWmin_ECWmax =
        ((UINT1)
         (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1index]
               + 1) / log (2)) << SHIFT_VALUE_4) +
        (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1index] + 1) / log (2));
    u1index++;

    pWlanGetUpdateReq->WssWlanEdcaParam.AC_VO_ParameterRecord.u1ECWmin_ECWmax =
        ((UINT1)
         (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1index]
               + 1) / log (2)) << SHIFT_VALUE_4) +
        (log (RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1index] + 1) / log (2));

#ifdef RSNA_WANTED
    RsnaGetDot11RSNAEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                             &i4Dot11RSNAEnabled);
    if (i4Dot11RSNAEnabled == RSNA_ENABLED)
    {
        if (pWlanGetUpdateReq->u1KeyType == WSSRSNA_GTK_KEY)
        {
            /* To get the GTK key from RSNA module for Update WLAN message */
            if (RsnaGetRsnGtkKey (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                                  au1GtkKey, &u2KeyLength,
                                  &u1KeyIndex) == OSIX_SUCCESS)
            {
                pWlanGetUpdateReq->u2KeyLength = u2KeyLength;
                MEMCPY (pWlanGetUpdateReq->au1Key, au1GtkKey,
                        pWlanGetUpdateReq->u2KeyLength);
                pWlanGetUpdateReq->u1KeyIndex = u1KeyIndex;
            }
        }
#ifdef PMF_DEBUG
        if (pWlanGetUpdateReq->u1KeyType == WSSRSNA_IGTK_KEY)
        {
            /* To get the IGTK key from RSNA module for Update WLAN message */
            if (RsnaGetRsnIGtkKey (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                                   au1IGtkKey, &u2KeyLength,
                                   &u1KeyIndex) == OSIX_SUCCESS)
            {
                pWlanGetUpdateReq->u2KeyLength = u2KeyLength;
                MEMCPY (pWlanGetUpdateReq->au1Key, au1IGtkKey,
                        pWlanGetUpdateReq->u2KeyLength);
                pWlanGetUpdateReq->u1KeyIndex = u1KeyIndex;
            }

        }
#endif

        /* RSNA ELEMENTS SHOULD BE FILLED */
        if (RsnaGetRsnIEParams (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                                &RsnaIEElements) == RSNA_SUCCESS)
        {
            pWlanGetUpdateReq->RsnaIEElements.u1ElemId =
                RsnaIEElements.u1ElemId;
            u1RsnIELength = u1RsnIELength + RSN_IE_ELEMENT_LENGTH;

            pWlanGetUpdateReq->RsnaIEElements.u1Len = RsnaIEElements.u1Len;
            u1RsnIELength = u1RsnIELength + RSN_IE_LENGTH_FIELD;

            pWlanGetUpdateReq->RsnaIEElements.u2Ver = RsnaIEElements.u2Ver;
            u1RsnIELength = u1RsnIELength + RSN_IE_VERSION_LENGTH;

            MEMCPY (pWlanGetUpdateReq->RsnaIEElements.au1GroupCipherSuite,
                    RsnaIEElements.au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);

            u1RsnIELength = u1RsnIELength + RSNA_CIPHER_SUITE_LEN;

            pWlanGetUpdateReq->RsnaIEElements.u2PairwiseCipherSuiteCount =
                RsnaIEElements.u2PairwiseCipherSuiteCount;
            u1RsnIELength = u1RsnIELength + RSNA_PAIRWISE_CIPHER_COUNT;

            for (u1Count = 0;
                 u1Count < RsnaIEElements.u2PairwiseCipherSuiteCount
                 && u1Count < RSNA_PAIRWISE_CIPHER_COUNT; u1Count++)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.
                        aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                        RsnaIEElements.aRsnaPwCipherDB[u1Count].
                        au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_CIPHER_SUITE_LEN;
            }
            pWlanGetUpdateReq->RsnaIEElements.u2AKMSuiteCount =
                RsnaIEElements.u2AKMSuiteCount;
            u1RsnIELength = u1RsnIELength + 2;

            if (MEMCMP
                (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                 au1RsnaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.
                        aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
            else if (MEMCMP
                     (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                      au1AKMSuite, au1RsnaAuthKeyMgmt8021X,
                      RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.
                        aRsnaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
#ifdef PMF_DEBUG
            else if (MEMCMP
                     (RsnaIEElements.
                      aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 - 1].au1AKMSuite,
                      au1RsnaAuthKeyMgmt8021XSHA256,
                      RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.aRsnaAkmDB[0].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_8021X_SHA256 -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
            else if (MEMCMP
                     (RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 - 1].
                      au1AKMSuite, au1RsnaAuthKeyMgmtPskSHA256Over8021X,
                      RSNA_AKM_SELECTOR_LEN) == 0)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.aRsnaAkmDB[0].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[RSNA_AUTHSUITE_PSK_SHA256 -
                                                  1].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
#endif
#ifdef DEBUG_WANTED
            for (u1Count = 0;
                 u1Count < RsnaIEElements.u2AKMSuiteCount
                 && u1Count < RSNA_AKM_SUITE_COUNT; u1Count++)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.aRsnaAkmDB[u1Count].
                        au1AKMSuite,
                        RsnaIEElements.aRsnaAkmDB[u1Count].au1AKMSuite,
                        RSNA_AKM_SELECTOR_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_AKM_SELECTOR_LEN;
            }
#endif
            pWlanGetUpdateReq->RsnaIEElements.u2RsnCapab =
                RsnaIEElements.u2RsnCapab;
            u1RsnIELength = u1RsnIELength + RSN_IE_CAPAB_LENGTH;

            pWlanGetUpdateReq->RsnaIEElements.u2PmkidCount =
                RsnaIEElements.u2PmkidCount;

            if (RsnaIEElements.u2PmkidCount != 0)
            {
                /* The pmkid count field should be considered only when pmkid count
                 * is not zero */
                u1RsnIELength = u1RsnIELength + RSN_IE_PMKID_COUNT_LENGTH;

                /* TODO: need to revisit to fill the PMK id list */
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.aPmkidList,
                        RsnaIEElements.aPmkidList, RSNA_PMKID_LEN);
                u1RsnIELength = u1RsnIELength + RSNA_PMKID_LEN;
            }

#ifdef PMF_WANTED
            if ((RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPR) ==
                RSNA_CAPABILITY_MFPR
                || (RsnaIEElements.u2RsnCapab & RSNA_CAPABILITY_MFPC) ==
                RSNA_CAPABILITY_MFPC)
            {
                MEMCPY (pWlanGetUpdateReq->RsnaIEElements.
                        au1GroupMgmtCipherSuite,
                        RsnaIEElements.au1GroupMgmtCipherSuite,
                        RSNA_MAX_CIPHER_TYPE_LENGTH);

                u1RsnIELength =
                    (UINT1) (u1RsnIELength + RSNA_MAX_CIPHER_TYPE_LENGTH);
            }
#endif
            pWlanGetUpdateReq->u2MessageType = WSSWLAN_UPDATE_WLAN_MSG_TYPE;
            pWlanGetUpdateReq->u2MessageLength = sizeof (tWssWlanUpdateReq) -
                WSS_WLAN_HEADER_LEN - sizeof (tRsnaIEElements) + u1RsnIELength;
        }
    }
    /* If rsna module status is not enabled, have the default 
     * behavior, set the length without RSNA elements*/
    else
    {
        pWlanGetUpdateReq->u2MessageType = WSSWLAN_UPDATE_WLAN_MSG_TYPE;
        pWlanGetUpdateReq->u2MessageLength = sizeof (tWssWlanUpdateReq) -
            WSS_WLAN_HEADER_LEN - sizeof (tRsnaIEElements);
    }
#else
    pWlanGetUpdateReq->u2MessageType = WSSWLAN_UPDATE_WLAN_MSG_TYPE;
    pWlanGetUpdateReq->u2MessageLength =
        sizeof (tWssWlanUpdateReq) - WSS_WLAN_HEADER_LEN -
        sizeof (tRsnaIEElements);
#endif
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanConstructDeleteWlanMsg
 * *                                                                           *
 * * Description  : Constructs Wlan deletion request packet                    *
 * *                                                                           *
 * * Input        : RadioIfIndex,WlanProfileId,pointer to Wlan delete request  *
 * *                                                                           *
 * * Output       : Constructed delete request                                 *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if construction   succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanConstructDeleteWlanMsg (UINT4 u4RadioIfIndex, UINT2 u2WlanProfileId,
                               tWssWlanDeleteReq * pWlanGetDeleteReq)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    WSSWLAN_FN_ENTRY ();
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructDeleteWlanMsg:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    /* Get WssWlanBssInterfaceDB through keys
     * RadioIfIndex and WlanProfileId */
    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructDeleteWlanMsg: BssInterface DB Get failed,\
                return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    /* Get pointer from struct tRadioIfDB */
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfQosConfig = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructDeleteWlanMsg:WssIfProcessRadioIfDBMsg call\
                for RadioIfDB failed, return failure");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    /* Construct message from the above derived pointers */
    pWlanGetDeleteReq->u2MessageType = WSSWLAN_DELETE_WLAN_MSG_TYPE;
    pWlanGetDeleteReq->u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlanGetDeleteReq->u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlanGetDeleteReq->u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
    pWlanGetDeleteReq->u2MessageLength = sizeof (UINT2);

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetProfileAdminStatus 
 * *                                                                           *
 * * Description  : Enable or disable the admin status for Wlan Profile.       *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanMsgStruct                               *
 * *                                                                           *
 * * Output       : Updation of Profile admin status                           *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if setting admin status succeeds             *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanSetProfileAdminStatus (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tCfaMsgStruct       CfaMsgStruct;
    tWssWlanDB          WssWlanDB;

    WSSWLAN_FN_ENTRY ();

    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    /* Get the WlanIfIndex node from RBTree to be updated */
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProfileAdminStatus: Wlan IfIndex DB Get failed,\
                return failure \r\n");
        return OSIX_FAILURE;
    }

    WssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProfileAdminStatus: Wlan Get Profile DB failed,\
                return failure \r\n");
        return OSIX_FAILURE;
    }

    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_FALSE;
    WssWlanDB.WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, &WssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProfileAdminStatus: Wlan Get Profile DB failed,\
                return failure \r\n");
        return OSIX_FAILURE;
    }

    if (WssWlanDB.WssWlanAttributeDB.u1AdminStatus == CFA_IF_DOWN)
    {
        if (WssWlanDB.WssWlanAttributeDB.u1WlanRadioMappingCount != 0)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProfileAdminStatus: Wlan bound to RadioInterface,\
                    Cannot disable admin status\r\n");
        }
    }
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    WssWlanDB.WssWlanAttributeDB.u1AdminStatus =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u1AdminStatus;
    WssWlanDB.WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY, &WssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProfileAdminStatus: Wlan IfIndex DB Set failed,\
                return failure \r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanCreateWlanProfile
 * *                                                                           *
 * * Description  : Creation,deletion or activation of a Wlan Profile          *
 * *                                                                           *
 * * Input        : WlanProfileId,RowStatus,pointer to return WlanIfIndex      *
 * *                                                                           *
 * * Output       : WlanIfIndex and Updaton of rowstatus                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if creation or deletion or activation        *
 * *                succeeds                                                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanProfileIntfRowStatus (UINT2 u2WlanProfileId, UINT1 u1RowStatus,
                             UINT4 *pu4WlanIfIndex)
{
    tCfaMsgStruct       CfaMsgStruct;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
#ifdef RSNA_WANTED
    tWssRSNANotifyParams WssRSNANotifyParams;
#endif
#ifdef WPS_WANTED
    tWssWpsNotifyParams WssWpsNotifyParams;
#endif
    INT4                i4FsDot11WlanRowStatus = 0;
    INT4                i4FsDot11WlanProfileIfIndex = 0;

    UINT4               u4WlanIfIndex = 0;
    UINT4               WlanIfIndex = 0;
    UINT2               WlanProfileId = 0;
    UINT2               u2WlanInternalId = 0;
    UINT1               SSID[WSSWLAN_SSID_NAME_LEN];
    MEMSET (SSID, 0, WSSWLAN_SSID_NAME_LEN);

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProfileIntfRowStatus:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();

    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
#ifdef WPS_WANTED
    MEMSET (&WssWpsNotifyParams, 0, sizeof (tWssWpsNotifyParams));
#endif
#ifdef RSNA_WANTED
    MEMSET (&WssRSNANotifyParams, 0, sizeof (tWssRSNANotifyParams));
#endif

    switch (u1RowStatus)
    {
        case WSSWLAN_RS_CREATE_AND_WAIT:

            /* Check if the WLC has reached maximum SSID capacity */
            if ((gu2WlanInternalIdCount + 1) > MAX_NUM_OF_SSID_PER_WLC)
            {
                /* Max reached. return failure */
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:Array reached max size,\
                        return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Create Wlan Profile Entry */
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1RowStatus = u1RowStatus;

            /* Get free interface index */
            if (WssWlanGetInternalId (u2WlanProfileId,
                                      &pWssWlanDB->WssWlanAttributeDB.
                                      u2WlanInternalId) == OSIX_FAILURE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:,WssWlanGetInternalId\
                            Failed to get the Free Interface Index \r\n");
            }

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Wlan Profile DB creation\
                        failed, return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Update RowStatus */
            if (nmhGetFsDot11WlanRowStatus (u2WlanProfileId,
                                            &i4FsDot11WlanRowStatus) ==
                SNMP_SUCCESS)
            {
                if (nmhGetFsDot11WlanProfileIfIndex (u2WlanProfileId,
                                                     &i4FsDot11WlanProfileIfIndex)
                    == SNMP_SUCCESS)
                {
                    /* already we have created & got from MSR. just use the
                     * index alone */
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                        (UINT4) i4FsDot11WlanProfileIfIndex;
                }
            }
            else
            {
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus =
                    CFA_IF_DOWN;
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                    CFA_CAPWAP_DOT11_PROFILE;

                if (WssIfProcessCfaMsg (CFA_WSS_CREATE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus:CFA index creation \
                            error, return failure \r\n");
                    /* Revert the entry created */
                    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
                    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                        u2WlanProfileId;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProfileIntfRowStatus: Destroy Profile\
                                failed \r\n");
                    }
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                /* store it in the local database */
                if (nmhSetFsDot11WlanRowStatus (u2WlanProfileId,
                                                CREATE_AND_WAIT) !=
                    SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus: Local database\
                            creation error, return failure \r\n");
                    /* Revert the entry created */
                    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
                    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                        u2WlanProfileId;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProfileIntfRowStatus: Destroy Profile\
                                failed \r\n");
                    }
                    /* need to release the cfs index here. todo */
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                if (nmhSetFsDot11WlanProfileIfIndex (u2WlanProfileId,
                                                     (INT4) (CfaMsgStruct.
                                                             unCfaMsg.
                                                             CfaCreateIfIndex.
                                                             u4IfIndex)) !=
                    SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus: Local database CFA\
                            index creation, return failure \r\n");
                    /* Revert the entry created */
                    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
                    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                        u2WlanProfileId;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProfileIntfRowStatus: Destroy Profile\
                                failed \r\n");
                    }
                    /* need to release the cfs index here. todo */
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                /* store it in the local database */
                if (nmhSetFsDot11WlanRowStatus (u2WlanProfileId, ACTIVE)
                    != SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus:CFA index creation \
                            error, return failure \r\n");
                    /* Revert the entry created */
                    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
                    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                        u2WlanProfileId;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanProfileIntfRowStatus: Destroy Profile\
                                failed \r\n");
                    }
                    /* need to release the cfs index here. todo */
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
            }

            /* Add new node to WlanIfIndexDB Tree */
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Create IfIndex entry\
                        failed \r\n");
                /* Revert the entry created */
                MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
                pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                    u2WlanProfileId;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus: Destroy WlanProfileDB\
                            entry failed \r\n");
                }
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Associate WlanIfIndexDb pointer to WlanProfile */
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: failed to set WlanIfIndex\
                        to WlanProfileDB entry\r\n");
                /* Revert the entry created */
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus: Destroy IfIndex entry\
                            failed\r\n");
                }
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus: Destroy WlanProfile\
                            failed \r\n");
                }
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            pWssWlanDB->WssWlanIsPresentDB.bWlanIfType = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1WlanIfType =
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType;
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_SET_IFINDEX_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Setting IfType,InternalId\
                        to IfIndexDB failed\r\n");
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus: Destroy IfIndex entry\
                            failed\r\n");
                }
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus:Destroy Profile entry\
                            failed\r\n");
                }
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            *pu4WlanIfIndex = CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            gu2WlanInternalIdCount++;
#ifdef RSNA_WANTED
            WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_PROFILEIF_ADD;
            WssRSNANotifyParams.u4IfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            if (RsnaProcessWssNotification (&WssRSNANotifyParams) ==
                OSIX_FAILURE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:- RsnaProcessWssNotification"
                             " returned Failure !!!! \n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return (OSIX_FAILURE);
            }
#endif
#ifdef WPS_WANTED
            WssWpsNotifyParams.eWssWpsNotifyType = WSSWPS_PROFILEIF_ADD;
            WssWpsNotifyParams.u4IfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            if (WpsProcessWssNotification (&WssWpsNotifyParams) == OSIX_FAILURE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:- WpsProcessWssNotification"
                             " returned Failure !!!! \n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return (OSIX_FAILURE);
            }
#endif
            break;

        case WSSWLAN_RS_NOT_IN_SERVICE:
            /* Get RadioMappingCount from WLAN Profile table */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                        WSS_WLAN_GET_PROFILE_ENTRY failed\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount != 0)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:WLAN Profile bound to\
                        Radio Interface, return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus = CFA_IF_DOWN;

            if (WssIfProcessCfaMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                    &CfaMsgStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Cfa status updation\
                        (up)failed , return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Update WlanIfIndex AdminStatus */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1AdminStatus = CFA_IF_DOWN;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                        WSS_WLAN_SET_PROFILE_ENTRY failed\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Update WlanProfile RowStatus */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1RowStatus = u1RowStatus;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                        WSS_WLAN_SET_PROFILE_ENTRY failed\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            break;

        case WSSWLAN_RS_DESTROY:

            WssWlanGetManagmentSSID (SSID);
            WssWlanGetWlanIfIndexfromSSID (SSID, &WlanIfIndex);
            WssWlanGetWlanIdfromIfIndex (WlanIfIndex, &WlanProfileId);

            if (u2WlanProfileId == WlanProfileId)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Cannot delete\
                        ManagmentSSID profile \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Get RadioMappingCount from WLAN Profile table */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                        failed, return failure \r\n");
            }
            if (pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount != 0)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:WLAN Profile bound to\
                        Radio Interface, return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Destroy SSID Mapping DB */
            pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Get SSID from WlanIfIndex\
                        failed\r\n");
            }
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = *pu4WlanIfIndex;
            u2WlanInternalId = pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId;
            /* Update CFA */
            u4WlanIfIndex = pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex = u4WlanIfIndex;

            if (WssIfProcessCfaMsg (CFA_WSS_DELETE_IFINDEX_MSG, &CfaMsgStruct)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Cfa deletion failed, \
                        return failure \r\n");
            }
            /* Update Wlan If Index DB */

            /* delete the stored item in the local database */
            if (nmhSetFsDot11WlanRowStatus (u2WlanProfileId, DESTROY)
                != SNMP_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Local database deletion\
                        error, return failure \r\n");
            }
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                        WSS_WLAN_DESTROY_IFINDEX_ENTRY failed\r\n");
            }
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_SSID_MAPPING_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Destroy SSID Mapping\
                        entry failed\r\n");
            }

            /* Update Wlan Profile DB */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                         WSS_WLAN_DESTROY_PROFILE_ENTRY failed \r\n");
            }

            if (WssWlanFreeInternalId (u2WlanInternalId) == OSIX_FAILURE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssWlanFreeInternalId\
                              Failed to reset the Wlan Internal ID\r\n");
            }
            gu2WlanInternalIdCount--;
            gu2WlanInternaldelcount++;
#ifdef RSNA_WANTED
            WssRSNANotifyParams.eWssRsnaNotifyType = WSSRSNA_PROFILEIF_DEL;
            WssRSNANotifyParams.u4IfIndex = u4WlanIfIndex;
            if (RsnaProcessWssNotification (&WssRSNANotifyParams)
                == OSIX_FAILURE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus:- RsnaProcessWssNotification"
                             " returned Failure !!!! \n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return (OSIX_FAILURE);
            }
#endif
            break;

        case WSSWLAN_RS_ACTIVE:
            /* Get WlanIfIndex from WLAN Profile table */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanMacType = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanTunnelMode = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                         WSS_WLAN_GET_PROFILE_ENTRY failed\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* 0 - Local MAC Type - Valid value */
            if (pWssWlanDB->WssWlanAttributeDB.u1MacType == WSSWLAN_MAC_EMPTY)
            {

                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Tunnel mode or Mactype\
                        empty \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            u4WlanIfIndex = pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex = u4WlanIfIndex;
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus = CFA_IF_UP;

            if (WssIfProcessCfaMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                    &CfaMsgStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Cfa intf (up)active\
                        failed, return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Update WlanIfIndex AdminStatus */
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1AdminStatus = CFA_IF_UP;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: WssIfProcessWssWlanDBMsg\
                         WSS_WLAN_SET_IFINDEX_ENTRY failed\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            break;

        default:
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProfileIntfRowStatus: Invalid OpCode\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return OSIX_FAILURE;

    }

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;

}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanBindingIntfRowStatus
 * *                                                                           *
 * * Description  : Creation or deletion of a binding of a radioIfIndex to a
 *                  WlanProfileId                                              *
 * *                                                                           *
 * * Input        : WlanProfileId , RadioIfIndex , RowStatus                   *
 * *                                                                           *
 * * Output       : Creation of bind entries and Updation of binding rowstatus *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if binding creation or deletion succeeds     *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanBindingIntfRowStatus (UINT2 u2WlanProfileId, UINT4 u4RadioIfIndex,
                             UINT1 u1RowStatus)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tCfaMsgStruct       CfaMsgStruct;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1BssIdCount = 0;
    INT4                i4FsDot11WlanBindRowStatus = 0;
    INT4                i4FsDot11WlanBindBssIfIndex = 0;
    UINT1               u1WlanIndex = 0;
    UINT4               u4WtpInternalId = 0;
    tWssIfCapDB        *pWssIfApGrpCapwapDB = NULL;
    tApGroupConfigEntry *pFristApGroup = NULL;
    tApGroupConfigEntry *pSecondApGroup = NULL;
    UINT4               u4ProfileId = 0;
    UINT2               u2GroupVlan = 0;
    UINT4               u4GetRadioType = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4RadioPolicy = 0;

    WSSWLAN_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanBindingIntfRowStatus:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    switch (u1RowStatus)
    {
        case WSSWLAN_RS_CREATE_AND_WAIT:
        {
            /* Creating and updating node to WssWlanBssInterfaceDB */

            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;

            /* Check for WlanProfile and if present get
             * WlanInternalId,RadioMappingCount for
             * further Updation in BssIfIndex */
            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_PROFILE_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:WlanProfile not found or\
                        Getting WlanInternalId from WlanProfile failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Get RadioIfDB for BssIdCount */
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus: \
                        DB Request from RadioIfDB Failed, return failure\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            if ((RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount + 1) >
                MAX_NUM_OF_BSSID_PER_RADIO)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:Bssid count exceeds\
                            MAX_NUM_OF_SSID_PER_AP; No new configurations\
                            allowed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Create Bss Interface */
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_BSS_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssInterface creation\
                            failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            pWssWlanDB->WssWlanAttributeDB.u1RowStatus = NOT_READY;
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssInterface RowStatus\
                            updation failed, Reverting DB changes\r\n");
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_BSS_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssInterface \
                                Deletionfailed\r\n");
                }
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

        }
            break;
        case WSSWLAN_RS_ACTIVE:
            /* Creating and updating BssIfIndex node to WssWlanBssInterfaceDB */
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
            /* Get and Check if the RowStatus is already active */
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            /* Get WlanId to update in BssIfIndexDB after creating it */
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssInterface Call to get "
                             "RowStatus failed. Failed to activate\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u1RowStatus == WSSWLAN_RS_ACTIVE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:RowStatus already "
                             "set\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_SUCCESS;
            }

            /* Check for WlanProfile and if present get
             * WlanInternalId,RadioMappingCount for
             * further Updation in BssIfIndex */

            pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_PROFILE_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:WlanProfile not found or"
                             "Getting WlanInternalId from WlanProfile failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Get RadioIfDB for BssIdCount */
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:"
                             "Getting BssIdCount from RadioIfDB failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;

            }
            u4WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

            /* Call CFA to get BssIfIndex */

            WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfApGrpCapwapDB, OSIX_FAILURE)
                MEMSET (pWssIfApGrpCapwapDB, 0, sizeof (tWssIfCapDB));

            pWssIfApGrpCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfApGrpCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfApGrpCapwapDB->CapwapGetDB.u2WtpInternalId = u4WtpInternalId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfApGrpCapwapDB)
                != OSIX_SUCCESS)
            {
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);

                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfApGrpCapwapDB);
                return OSIX_FAILURE;
            }
            u4ProfileId = pWssIfApGrpCapwapDB->CapwapGetDB.u2ProfileId;

            if (CapwapGetWtpProfileIdFromProfileName
                (pWssIfApGrpCapwapDB->CapwapGetDB.au1ProfileName,
                 &u4ProfileId) != OSIX_SUCCESS)
            {

            }
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfApGrpCapwapDB);

            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                CFA_CAPWAP_DOT11_BSS;
            if (nmhGetFsDot11WlanBindRowStatus
                ((INT4) u4RadioIfIndex, u2WlanProfileId,
                 &i4FsDot11WlanBindRowStatus) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11WlanBindBssIfIndex ((INT4) u4RadioIfIndex,
                                                     u2WlanProfileId,
                                                     &i4FsDot11WlanBindBssIfIndex)
                    == SNMP_SUCCESS)
                {
                    /* already we have created & got from MSR. just use the
                     * index alone */
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                        (UINT4) i4FsDot11WlanBindBssIfIndex;
                }
            }
            else
            {
                if (CfaProcessWssIfMsg (CFA_WSS_CREATE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to create"
                                 "index for BSS, reverting DB changes\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                /* store it in the local database */
                if (nmhSetFsDot11WlanBindRowStatus ((INT4) u4RadioIfIndex,
                                                    u2WlanProfileId,
                                                    CREATE_AND_WAIT) !=
                    SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Local database\
                    create failed, reverting DB changes\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                if (nmhSetFsDot11WlanBindBssIfIndex ((INT4) u4RadioIfIndex,
                                                     u2WlanProfileId,
                                                     (INT4) (CfaMsgStruct.
                                                             unCfaMsg.
                                                             CfaCreateIfIndex.
                                                             u4IfIndex)) !=
                    SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Local database save\
                    failed, reverting DB changes\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                if (nmhSetFsDot11WlanBindWlanId ((INT4) u4RadioIfIndex,
                                                 u2WlanProfileId,
                                                 pWssWlanDB->WssWlanAttributeDB.
                                                 u1WlanId) != SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Local database save\
                    failed, reverting DB changes\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                /* store it in the local database */
                if (nmhSetFsDot11WlanBindRowStatus ((INT4) u4RadioIfIndex,
                                                    u2WlanProfileId,
                                                    ACTIVE) != SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Local database active\
                    failed, reverting DB changes\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
            }
            /* Adding new node to WssWlanBssIfIndexDB using BssIfIndex */
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            pWssWlanDB->WssWlanAttributeDB.u1IfType =
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType;
            pWssWlanDB->WssWlanIsPresentDB.bIfType = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1RowStatus = WSSWLAN_RS_ACTIVE;
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssIfIndex Creation\
                        failed\r\n");

                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to\
                            delete index for BSS, reverting DB changes\r\n");
                }

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            WssFindTheApGroupFromWtpProfileId (u4ProfileId, &pFristApGroup,
                                               &pSecondApGroup);

            if (pFristApGroup != NULL && pSecondApGroup == NULL)
            {
                u2GroupVlan = pFristApGroup->u2InterfaceVlan;
            }
            if (pFristApGroup != NULL && pSecondApGroup != NULL)
            {
                if ((i4RetStatus =
                     nmhGetFsDot11RadioType ((INT4) u4RadioIfIndex,
                                             &u4GetRadioType)) != SNMP_SUCCESS)
                {
                    PRINTF ("FAILED %s !!!\n", __func__);
                }
                else
                {
                    if ((u4GetRadioType == CLI_RADIO_TYPEA) ||
                        (u4GetRadioType == CLI_RADIO_TYPEAN) ||
                        (u4GetRadioType == CLI_RADIO_TYPEAC))
                    {
                        u4RadioPolicy = CLI_RADIO_TYPEA;
                    }
                    else if ((u4GetRadioType == CLI_RADIO_TYPEB) ||
                             (u4GetRadioType == CLI_RADIO_TYPEG) ||
                             (u4GetRadioType == CLI_RADIO_TYPEBG) ||
                             (u4GetRadioType == CLI_RADIO_TYPEBGN))
                    {
                        u4RadioPolicy = CLI_RADIO_TYPEB;
                    }

                }
                if (pFristApGroup->u2RadioPolicy == u4RadioPolicy)
                {
                    u2GroupVlan = pFristApGroup->u2InterfaceVlan;
                }
                else if (pSecondApGroup->u2RadioPolicy == u4RadioPolicy)
                {
                    u2GroupVlan = pSecondApGroup->u2InterfaceVlan;
                }
            }

            if (pFristApGroup != NULL || pSecondApGroup != NULL)
            {
                pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                pWssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                pWssWlanDB->WssWlanAttributeDB.u2VlanId = u2GroupVlan;
                /* Assign Vlan to the BssifIndex node */
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Setting GroupVlan to\
                        to BssIfIndexDB failed,revert DB\r\n");

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus:BssIfIndex deletion\
                                   failed\r\n");
                    }
                    if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                            &CfaMsgStruct) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus:Cfa failed to delete\
                            index for BSS\r\n");
                    }

                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
            }

            /* Assign WlanInternalId and RadioIfDB to the BssIfIndex node */

            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_FALSE;
            pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:Setting WlanInternalId\
                        to BssIfIndexDB failed,revert DB\r\n");
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                     pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssIfIndex deletion\
                            failed\r\n");
                }
                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to delete\
                            index for BSS\r\n");
                }

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

            /* Increment BssId and assign it to WlanId for the first WLAN,
             * assign RowStatus.
             * This logic will not work if a first WLAN is deleted when a
             * second WLAN is already present and then when a third one is
             * created. Hence for the second WLAN onwards else case will 
             * be hit */
            u1BssIdCount =
                (UINT1) (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount + 1);
            if (RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount == 0)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanId = u1BssIdCount;
            }
            else
            {
                /* Get the Free BssIfIndex entry and assign the index 
                 * to the next WLAN */
                for (u1WlanIndex = WSSWLAN_START_WLANID_PER_RADIO;
                     u1WlanIndex <= WSSWLAN_END_WLANID_PER_RADIO; u1WlanIndex++)
                {
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        u4RadioIfIndex;
                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanIndex;
                    if (WssIfProcessRadioIfDBMsg
                        (WSS_GET_NEXT_WLAN_BSS_IFINDEX_DB,
                         &RadioIfGetDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus:BssInterface\
                                    Call to get BSSIfIndex failed. Failed to\
                                    activate\r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        return OSIX_FAILURE;
                    }
                    else
                    {
                        /* Get the Next Free WLAN Index */
                        if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex == 0)
                        {
                            break;
                        }
                    }
                }
                pWssWlanDB->WssWlanAttributeDB.u1WlanId = u1WlanIndex;
            }

            /* Update newly created BssIfIndex in BssInterfaceDB */
            pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssIfIndex Updation in\
                    Interface DB failed\r\n");
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                     pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssIfIndex deletion\
                        failed\r\n");
                }
                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to delete\
                        index for BSS, reverting DB changes\r\n");
                }

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus =
                WSS_WLAN_BIND_FROM_USERIF;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus: WlanDBMsg\
                        failed to set Wlan Bind Status\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Call CFA to update admin status */
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus = CFA_IF_UP;

            if (CfaProcessWssIfMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                    &CfaMsgStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:"
                             "Cfa failed to update(up) admin status,"
                             "reverting DB changes\r\n");
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                     pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:"
                                 "BssIfIndex deletion failed\r\n");
                }
                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to delete\
                        index for BSS, reverting DB changes\r\n");
                }

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Update RadioMappingCount */
            pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount
                = pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount + 1;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus: Call for WlanInternal\
                        ProfileDB to set RadioMappingCount failed\r\n");

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY, pWssWlanDB)
                    != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssIfIndex deletion\
                            failed\r\n");
                }
                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to delete\
                            index for BSS, reverting DB changes\r\n");
                }

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Incerment BssIdCount in RadioIfDB */
            RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount =
                RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount + 1;
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, &RadioIfGetDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanBindingIntfRowStatus:\
                    Setting RadioIfDB for BssIdCount failed\r\n");

                /* Decrement WlanRadioMappingCount to old value */
                pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount
                    =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount - 1;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_SET_PROFILE_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Call for WlanInternal\
                        ProfileDB to set RadioMappingCount failed\r\n");
                }

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                     pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssIfIndex deletion \
                        failed\r\n");
                }
                if (CfaProcessWssIfMsg
                    (CFA_WSS_DELETE_IFINDEX_MSG, &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to delete \
                        index for BSS, reverting DB changes\r\n");
                }

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            if (gu1IsWlanConfigResponseReceived == WSS_WLAN_NO_AP_PRESENT)
            {
                pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
                pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus =
                    WSS_WLAN_BIND_STAT_DOWN;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\nWssWlanBindingIntfRowStatus: WlanDBMsg\
                            failed to set Wlan Bind Status\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                /* Reset the Flag */
                gu1IsWlanConfigResponseReceived = OSIX_SUCCESS;
            }

            break;
        case WSSWLAN_RS_DESTROY:

            /* Retrieve WssWlanBssInterfaceDB node with particular radioIfIndex
             * and WlanProfileId */
            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssInterface Get\
                        BssInterface RowStatus failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanAttributeDB.u1RowStatus == WSSWLAN_RS_ACTIVE)
            {
                pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssInterface Get\
                        BssIfIndex failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
                pWssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: WlanDBMsg\
                            failed to Get Wlan Bind Status\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                if (pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus !=
                    WSS_WLAN_BIND_STAT_DOWN)
                {
                    pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
                    pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus =
                        WSS_WLAN_BIND_FROM_USERIF;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "\nWssWlanBindingIntfRowStatus: WlanDBMsg\
                                    failed to set Wlan Bind Status");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        return OSIX_FAILURE;
                    }

                    /* Call CFA to update adminstatus */
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                        CFA_CAPWAP_DOT11_BSS;
                    CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus =
                        CFA_IF_DOWN;
                    if (CfaProcessWssIfMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                            &CfaMsgStruct) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus:Cfa failed to up\
                                index status for BSS, reverting DB changes\r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        return OSIX_FAILURE;
                    }

                    /*Destroy BssId Mapping DB Entry */
                    /*Get Mac key from BssIfindexDB Entry */
                    pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                         pWssWlanDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus:BssIfIndexDB entry\
                                GET for MacAddr failed\r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        return OSIX_FAILURE;
                    }
                    if ((MEMCMP (pWssWlanDB->WssWlanAttributeDB.BssId,
                                 "\0\0\0\0\0\0", sizeof (tMacAddr))) != 0)
                    {

                        if (WssIfProcessWssWlanDBMsg
                            (WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
                             pWssWlanDB) != OSIX_SUCCESS)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "WssWlanProfileIntfRowStatus:BssIdMappingDB\
                                entry failed\r\n");
                            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                             RadioIfGetAllDB.
                                                             pu1AntennaSelection);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                            return OSIX_FAILURE;
                        }

                    }
                }
                else
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: BssInterface is "
                                 "already DOWN, Deleting it\r\n");
                }

                if (WssIfBssidVlanMemberAction
                    (CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex,
                     pWssWlanDB->WssWlanAttributeDB.u2VlanId,
                     VLAN_DELETE) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "VLAN Member Deletion failed \r\n");
                }

#ifdef KERNEL_CAPWAP_WANTED
                if (CapwapRemoveKernelIntfDB
                    (pWssWlanDB->WssWlanAttributeDB.BssId) == OSIX_FAILURE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "Failed to update Kernel DB\r\n");
                }
#endif
                /* Destroy BssIfIndexDB Entry */
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
                pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
                pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                     pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProfileIntfRowStatus:BssIfIndexDB entry\
                            failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
            }
            /* End if : To be executed when rowstatus is still active */

            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_DESTROY_BSS_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssInterface deletion\
                        failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u1RowStatus == WSSWLAN_RS_ACTIVE)
            {
                /* Decrement RadioMappingCount in InternalProfileDB */

                /* Retrieve WlanProfile's RadioMappingCount */
                pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount =
                    OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_PROFILE_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Call for WlanInternal\
                        ProfileDB to get RadioMappingCount failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                /* Update RadioMappingCount */
                pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount - 1;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                              pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Call for WlanInternal\
                        ProfileDB to set RadioMappingCount failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                /* Decrement BssIdCount in RadioIfDB */
                /* Decrement BssIdCount of struct tRadioIfDB */
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:\
                        DB Request RadioIfDB to get BssIdCount Failed\n");

                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount =
                    RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount - 1;
                if (WssIfProcessRadioIfDBMsg
                    (WSS_SET_RADIO_IF_DB, &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Call for RadioIfDB to\
                        set BssIdCount failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }

                /* Call CFA to delete Admin Status */
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
                CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                    CFA_CAPWAP_DOT11_BSS;

                if (CfaProcessWssIfMsg (CFA_WSS_DELETE_IFINDEX_MSG,
                                        &CfaMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:Cfa failed to"
                                 "delete index for BSS, reverting DB changes\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;

                }

                /* delete the stored item in the local database */
                if (nmhSetFsDot11WlanBindRowStatus (u4RadioIfIndex,
                                                    u2WlanProfileId,
                                                    DESTROY) != SNMP_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus: Local database\
                                 delete failed\r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
            }

            break;
        case WSSWLAN_RS_NOT_IN_SERVICE:

            /* Retrieve WssWlanBssInterfaceDB node with particular radioIfIndex
             * and WlanProfileId and set rowstatus */

            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
            pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
            pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanBindingIntfRowStatus:\
                        BssInterface Call to get BssIfIndex failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus =
                WSS_WLAN_BIND_FROM_USERIF;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus: WlanDBMsg\
                        failed to set Wlan Bind Status\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Call CFA to update admin status */
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                CFA_CAPWAP_DOT11_BSS;
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus = CFA_IF_DOWN;

            if (CfaProcessWssIfMsg (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                    &CfaMsgStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanBindingIntfRowStatus:\
                        Cfa failed to update (up)admin status, \
                        reverting DB changes\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /*Destroy BssId Mapping DB Entry */
            /*Get Mac key from BssIfindexDB Entry */
            pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssIfIndexDB entry GET\
                        for MacAddr failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssIdMappingDB\
                        entry failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Delete BssIfIndex entry */
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssIfIndexDB entry\
                        failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Update RadioMappingCount */
            pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount =
                pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount - 1;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus: Call for WlanInternal\
                        ProfileDB to set RadioMappingCount failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            /* Decrement BssIdCount in RadioIfDB */
            /* Decrement BssIdCount of struct tRadioIfDB */
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanBindingIntfRowStatus:\
                        DB Request RadioIfDB to get BssIdCount Failed\r\n");

                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount =
                RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount - 1;
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus: Call for RadioIfDB\
                        to set BssIdCount failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            /* Update BssInterface RowStatus */
            pWssWlanDB->WssWlanAttributeDB.u1RowStatus = u1RowStatus;
            pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_FALSE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_ENTRY, pWssWlanDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanBindingIntfRowStatus:BssInterface Call to\
                        set RowStatus failed\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }

            break;
        default:
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanBindingIntfRowStatus:\
                    Invalid Opcode, return failure\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return OSIX_FAILURE;

            break;
    }
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function     : WssWlanClearConfig
 * Description  : Updation of Clear Config message
 * Input        : Pointer to tWssWlanMsgStruct
 * Output       : None
 * Returns      : OSIX_SUCCESS, if updation succeeds
 *                OSIX_FAILURE, otherwise
 *****************************************************************************/

UINT1
WssWlanClearConfig (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssStaMsgStruct    WssStaMsgStruct;
    tCfaMsgStruct       CfaMsgStruct;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssStaMsgStruct, 0, sizeof (tWssStaMsgStruct));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    /* Retrieve WssWlanBssIfIndexDB record with particular BssIfIndex */

    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig: Bss IfIndex DB Get failed, "
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    if (WssCfgDestroyDot11WlanBindTable
        (pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex,
         pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig: BSS Interface deletion "
                     "failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    /* Delete the stations */
    WssStaMsgStruct.unAuthMsg.AuthDelProfile.u4BssIfIndex =
        pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u4IfIndex;
    WssStaMsgStruct.unAuthMsg.AuthDelProfile.u1MsgType = WSSSTA_CLEAR_CONFIG;
    if (WssIfProcessWssStaMsg (WSS_STA_WLAN_DELETE, &WssStaMsgStruct)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig: WssIfProcessWssStaMsg to delete Station "
                     "failed, return failure\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    /* When station deletion is successful, delete the Wlans */
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig:BssInterface Get "
                     "BssIfIndex failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  pWssWlanDB) == OSIX_SUCCESS)
    {
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanClearConfig: BssIdMappingDB entry "
                         "failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return OSIX_FAILURE;
        }
    }
    /* delete the stored item in the local database */
    if (nmhSetFsDot11WlanBindRowStatus
        (pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex,
         pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId,
         DESTROY) != SNMP_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanClearConfig: Local database\
                delete failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    /* Destroy BssIfIndexDB Entry */
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig: BssIfIndexDB entry failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_DESTROY_BSS_ENTRY, pWssWlanDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanClearConfig: BssInterface deletion " "failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;

}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetBssAdminStatus
 * *                                                                           *
 * * Description  : Updation of Bss Admin status                               *
 * *                                                                           *
 * * Input        : Pointer to tWssWlanMsgStruct                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if updation succeeds                         *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/

UINT1
WssWlanSetBssAdminStatus (tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanMsgStruct  *pwssWlanMsgStruct = NULL;
    UINT2               u2WlanProfileId = 0, u2WtpInternalId = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               i4RetVal = 1;
    tCfaMsgStruct       CfaMsgStruct;
#ifdef WPA_WANTED
    tWpaIEElements      WpaIEElements;
    INT4                i4FsRSNAEnabled = 2;
    UINT1               u1WpaIELength = 0;
    UINT1               au1WpaAuthKeyMgmt8021X[] = { 0x00, 0x50, 0xF2, 1 };
    UINT1               au1WpaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x50, 0xF2, 2 };
    UINT1               u1Index;
#endif

#ifdef WPS_WANTED
    tWpsIEElements      WpsIEElements;
    INT4                i4WPSEnabled = 0;
    INT4                i4WpsIELength = 0;
    UINT1               u1Count = 0;
#endif

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetBssAdminStatus:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetBssAdminStatus:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    pwssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pwssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetBssAdminStatus:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pwssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u1AdminStatus
        == CFA_IF_UP)
    {
        /* Retrieve WssWlanBssIfIndexDB record with particular BssIfIndex */
        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u4IfIndex;
        pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanBssAdminStatus: Bss IfIndex DB Get failed,\
            return failure \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

        pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB)
            != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanBssAdminStatus: Failed to get BSS IF\
            Index entry\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

        pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanBssAdminStatus: Internal Profile DB Get failed\
            return failure \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse: RadioDBMsg failed to\
            return RadioIfIndex\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

        /* Retrieve WlanProfileId from WlanInternalProfileDB usind index
         * WlanInternalId from BssIfIndexDB */
        u2WlanProfileId = pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;

        /* Retrieve RadioIfIndex from RadioIfDB pointer in BssIfIndexDB */
        u4RadioIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

        u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

        /* Construct WlanAddReq message */
        if (WssWlanConstructAddWlanMsg (u4RadioIfIndex, u2WlanProfileId,
                                        &(pwssWlanMsgStruct->unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanAddReq)) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetBssAdminStatus:Construction of Add Wlan\
            Msg failed, return failure\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

#ifdef WPS_WANTED
        MEMCPY (&
                (pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                 WssWlanAddReq),
                &(pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                  unWlanConfReq.WssWlanAddReq),
                sizeof (pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanAddReq));
        WpsGetWPSEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                          u2WtpInternalId, &i4WPSEnabled);
        if (i4WPSEnabled == WPS_ENABLED)
        {
            if (WpsGetWpsIEParams
                (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex, u2WtpInternalId,
                 &WpsIEElements) == OSIX_SUCCESS)
            {
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.u1ElemId = WpsIEElements.u1ElemId;
                i4WpsIELength = i4WpsIELength + WPS_IE_ELEMENT_LENGTH;

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.u1Len = WpsIEElements.u1Len;
                i4WpsIELength = i4WpsIELength + WPS_IE_LENGTH_FIELD;

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.bWpsEnabled =
                    WpsIEElements.bWpsEnabled;
                i4WpsIELength = i4WpsIELength + WPS_IE_CAPAB_LENGTH;

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.wscState = WpsIEElements.wscState;
                i4WpsIELength = i4WpsIELength + WPS_IE_STATE_LENGTH;

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.bWpsAdditionalIE =
                    WpsIEElements.bWpsAdditionalIE;
                i4WpsIELength = i4WpsIELength + WPS_IE_ADDITIONALIE_LENGTH;

                for (u1Count = 0; u1Count < WpsIEElements.noOfAuthMac;
                     u1Count++)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.
                            RadioIfInfoElement.unInfoElem.WpsIEElements.
                            authorized_macs[u1Count],
                            WpsIEElements.authorized_macs[u1Count],
                            WPS_ETH_ALEN);

                    i4WpsIELength = i4WpsIELength + WPS_ETH_ALEN;
                }

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.noOfAuthMac =
                    WpsIEElements.noOfAuthMac;
                i4WpsIELength = i4WpsIELength + WPS_IE_CAPAB_LENGTH;

                MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                        unInfoElem.WpsIEElements.uuid_e, WpsIEElements.uuid_e,
                        WPS_UUID_LEN);
                i4WpsIELength = i4WpsIELength + WPS_UUID_LEN;

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.configMethods =
                    WpsIEElements.configMethods;
                i4WpsIELength = i4WpsIELength + WPS_IE_CONFIG_METHOD_LENGTH;

                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.statusOrAddIE =
                    WpsIEElements.statusOrAddIE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u1RadioId =
                    pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanAddReq.u1RadioId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u1WlanId =
                    pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanAddReq.u1WlanId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u2MessageType = RADIO_INFO_ELEM_MSG_TYPE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u2MessageLength =
                    (UINT2) (RADIO_INFO_ELEM_FIXED_MSG_LEN + i4WpsIELength);
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    isPresent = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.u1EleId =
                    WPS_VENDOR_ELEMENT_ID;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u1BPFlag = RadioIfGetDB.RadioIfGetAllDB.u1BPFlag;
            }
        }
#endif

        MEMCPY (&
                (pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                 WssWlanAddReq),
                &(pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                  unWlanConfReq.WssWlanAddReq),
                sizeof (pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanAddReq));
        pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].u2MsgEleLen =
            VEND_SSID_ISOLATION_LEN + VEND_HEADER_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].elementId =
            VENDOR_SSID_ISOLATION_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.u2MsgEleType = SSID_ISOLATION_VENDOR_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.u2MsgEleLen = VEND_SSID_ISOLATION_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.u1Val =
            pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.u1RadioId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1RadioId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
            VendSsidIsolation.u1WlanId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1WlanId;

        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].u2MsgEleLen =
            VEND_SSID_RATE_LIMIT_LEN + VEND_HEADER_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].elementId =
            VENDOR_SSID_RATE_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u2MsgEleType = SSID_RATE_VENDOR_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u2MsgEleLen = VEND_SSID_RATE_LIMIT_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u1QosRateLimit =
            pWssWlanDB->WssWlanAttributeDB.u1QosRateLimit;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosUpStreamCIR =
            pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosUpStreamCBS =
            pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosUpStreamEIR =
            pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosUpStreamEBS =
            pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosDownStreamCIR =
            pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosDownStreamCBS =
            pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosDownStreamEIR =
            pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u4QosDownStreamEBS =
            pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u1PassengerTrustMode =
            pWssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u1RadioId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1RadioId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[1].unVendorSpec.
            VendSsidRate.u1WlanId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1WlanId;

        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].u2MsgEleLen =
            WSS_WLAN_MULTICAST_LEN + VEND_HEADER_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].elementId =
            VENDOR_MULTICAST_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u2MsgEleType = MULTICAST_VENDOR_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u2MsgEleLen = WSS_WLAN_MULTICAST_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u1RadioId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1RadioId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u1WlanId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1WlanId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u2WlanMulticastMode =
            pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u2WlanSnoopTableLength =
            pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u4WlanSnoopTimer =
            pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[2].unVendorSpec.
            VendorMulticast.u4WlanSnoopTimeout =
            pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout;

        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].u2MsgEleLen =
            VEND_WLAN_VLAN_LEN + VEND_HEADER_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].elementId =
            VENDOR_WLAN_VLAN_TYPE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.u2MsgEleType = VENDOR_WLAN_VLAN_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.u2MsgEleLen = VEND_WLAN_VLAN_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.vendorId = VENDOR_ID;

        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.u2VlanId = pWssWlanDB->WssWlanAttributeDB.u2VlanId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.u1RadioId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1RadioId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendWlanVlan.u1WlanId =
            pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanAddReq.u1WlanId;

#ifdef WPA_WANTED
        WpaGetFsRSNAEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                             &i4FsRSNAEnabled);
        if (i4FsRSNAEnabled == WPA_ENABLED)
        {
            if (WpaGetRsnIEParams
                (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                 &WpaIEElements) == OSIX_SUCCESS)
            {
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].isOptional =
                    OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].u2MsgEleType =
                    VENDOR_SPECIFIC_PAYLOAD;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].u2MsgEleLen =
                    VEND_WPA_LEN + VEND_HEADER_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].elementId =
                    VENDOR_WPA_TYPE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.isOptional = OSIX_TRUE;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2MsgEleType = VENDOR_WPA_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2MsgEleLen = VEND_WPA_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u4VendorId = VENDOR_ID;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u1ElemId = WpaIEElements.u1ElemId;
                u1WpaIELength = u1WpaIELength + RSN_IE_ELEMENT_LENGTH;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u1Len = WpaIEElements.u1Len;
                u1WpaIELength = u1WpaIELength + RSN_IE_LENGTH_FIELD;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2Ver = WpaIEElements.u2Ver;
                u1WpaIELength = u1WpaIELength + RSN_IE_VERSION_LENGTH;

                MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                        unVendorSpec.WpaIEElements.au1GroupCipherSuite,
                        WpaIEElements.au1GroupCipherSuite,
                        RSNA_CIPHER_SUITE_LEN);
                u1WpaIELength = u1WpaIELength + RSNA_CIPHER_SUITE_LEN;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2PairwiseCipherSuiteCount =
                    WpaIEElements.u2PairwiseCipherSuiteCount;
                u1WpaIELength = u1WpaIELength + RSNA_PAIRWISE_CIPHER_COUNT;

                for (u1Index = 0;
                     u1Index < WpaIEElements.u2PairwiseCipherSuiteCount
                     && u1Index < WPA_PAIRWISE_CIPHER_COUNT; u1Index++)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                            unVendorSpec.WpaIEElements.aWpaPwCipherDB[u1Index].
                            au1PairwiseCipher,
                            WpaIEElements.aWpaPwCipherDB[u1Index].
                            au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                    u1WpaIELength = u1WpaIELength + RSNA_CIPHER_SUITE_LEN;
                }
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2AKMSuiteCount =
                    WpaIEElements.u2AKMSuiteCount;
                u1WpaIELength = u1WpaIELength + RSNA_AKM_SUITE_COUNT;
                /*This should be taken care when additional AKM suits are 
                 *added like PMF*/
                if (MEMCMP
                    (WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].
                     au1AKMSuite, au1WpaAuthKeyMgmtPskOver8021X,
                     RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                            unVendorSpec.WpaIEElements.
                            aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK -
                                                    1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                    u1WpaIELength = u1WpaIELength + RSNA_AKM_SELECTOR_LEN;
                }
                else if (MEMCMP
                         (WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                          au1AKMSuite, au1WpaAuthKeyMgmt8021X,
                          RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                            unVendorSpec.WpaIEElements.
                            aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X -
                                                    1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                    u1WpaIELength = u1WpaIELength + RSNA_AKM_SELECTOR_LEN;
                }
            }
        }

#endif

        WssConstructWlanInterfaceVendorMessage (u4RadioIfIndex, u2WlanProfileId,
                                                pWlcHdlrMsgStruct);
        WssConstructDiffServVendorMessage (u4RadioIfIndex, u2WlanProfileId,
                                           pWlcHdlrMsgStruct);
        pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption = WSSWLAN_ADD_REQ;

        i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                           pWlcHdlrMsgStruct);

        if (i4RetVal == OSIX_FAILURE)

        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetBssAdminStatus:WlcHdlr\
            Call returned failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        else if (i4RetVal == WSS_WLAN_NO_AP_PRESENT)
        {
            gu1IsWlanConfigResponseReceived = WSS_WLAN_NO_AP_PRESENT;
        }
#ifdef WPA_WANTED
        WpaGetFsRSNAEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                             &i4FsRSNAEnabled);
        if (i4FsRSNAEnabled == WPA_ENABLED)
        {
            if (WpaGetRsnIEParams
                (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                 &WpaIEElements) == OSIX_SUCCESS)
            {
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].isOptional =
                    OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].u2MsgEleType =
                    VENDOR_SPECIFIC_PAYLOAD;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].u2MsgEleLen =
                    VEND_WPA_LEN + VEND_HEADER_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].elementId =
                    VENDOR_WPA_TYPE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.isOptional = OSIX_TRUE;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2MsgEleType = VENDOR_WPA_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2MsgEleLen = VEND_WPA_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u4VendorId = VENDOR_ID;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u1ElemId = WpaIEElements.u1ElemId;
                u1WpaIELength = u1WpaIELength + RSN_IE_ELEMENT_LENGTH;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u1Len = WpaIEElements.u1Len;
                u1WpaIELength = u1WpaIELength + RSN_IE_LENGTH_FIELD;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2Ver = WpaIEElements.u2Ver;
                u1WpaIELength = u1WpaIELength + RSN_IE_VERSION_LENGTH;

                MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                        unVendorSpec.WpaIEElements.au1GroupCipherSuite,
                        WpaIEElements.au1GroupCipherSuite,
                        RSNA_CIPHER_SUITE_LEN);
                u1WpaIELength = u1WpaIELength + RSNA_CIPHER_SUITE_LEN;

                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2PairwiseCipherSuiteCount =
                    WpaIEElements.u2PairwiseCipherSuiteCount;
                u1WpaIELength = u1WpaIELength + RSNA_PAIRWISE_CIPHER_COUNT;

                for (u1Index = 0;
                     u1Index < WpaIEElements.u2PairwiseCipherSuiteCount
                     && u1Index < WPA_PAIRWISE_CIPHER_COUNT; u1Index++)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                            unVendorSpec.WpaIEElements.aWpaPwCipherDB[u1Index].
                            au1PairwiseCipher,
                            WpaIEElements.aWpaPwCipherDB[u1Index].
                            au1PairwiseCipher, RSNA_CIPHER_SUITE_LEN);
                    u1WpaIELength = u1WpaIELength + RSNA_CIPHER_SUITE_LEN;
                }
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                    WpaIEElements.u2AKMSuiteCount =
                    WpaIEElements.u2AKMSuiteCount;
                u1WpaIELength = u1WpaIELength + RSNA_AKM_SUITE_COUNT;
                /*This should be taken care when additional AKM suits are 
                 *added like PMF*/
                if (MEMCMP
                    (WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].
                     au1AKMSuite, au1WpaAuthKeyMgmtPskOver8021X,
                     RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                            unVendorSpec.WpaIEElements.
                            aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_PSK -
                                                    1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                    u1WpaIELength = u1WpaIELength + RSNA_AKM_SELECTOR_LEN;
                }
                else if (MEMCMP
                         (WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X - 1].
                          au1AKMSuite, au1WpaAuthKeyMgmt8021X,
                          RSNA_AKM_SELECTOR_LEN) == 0)
                {
                    MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].
                            unVendorSpec.WpaIEElements.
                            aWpaAkmDB[RSNA_AUTHSUITE_PSK - 1].au1AKMSuite,
                            WpaIEElements.aWpaAkmDB[RSNA_AUTHSUITE_8021X -
                                                    1].au1AKMSuite,
                            RSNA_AKM_SELECTOR_LEN);
                    u1WpaIELength = u1WpaIELength + RSNA_AKM_SELECTOR_LEN;
                }
            }
        }

#endif

    }
    else if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.
             u1AdminStatus == CFA_IF_DOWN)
    {
        /* Retrieve WssWlanBssIfIndexDB record with particular BssIfIndex */

        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u4IfIndex;
        pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanBssAdminStatus: Bss IfIndex DB Get failed,\
            return failure \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanBssAdminStatus: Internal Profile DB Get failed,\
            return failure \r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

        /* Retrieve RadioIfIndex from RadioIfDB pointer in BssIfIndexDB */

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse: RadioDBMsg failed to \
            return RadioIfIndex\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }

        u4RadioIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;

        pwssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.unAuthMsg.
            AuthDelProfile.u4BssIfIndex =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanSetAdminOperStatus.u4IfIndex;
        if (WssIfProcessWssStaMsg (WSS_STA_WLAN_DELETE,
                                   &(pwssWlanMsgStruct->unWssWlanMsg.
                                     WssStaMsgStruct)) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetBssAdminStatus: WssIfProcessWssStaMsg to delete\
            Station failed\r\n");
        }
#ifdef RSNA_WANTED
        RsnaHandleBSSIfDeletion (pWssWlanMsgStruct->unWssWlanMsg.
                                 WssWlanSetAdminOperStatus.u4IfIndex);
#endif

        /* Construct WlanDeleteReq message */
        if (WssWlanConstructDeleteWlanMsg (u4RadioIfIndex,
                                           pWssWlanDB->WssWlanAttributeDB.
                                           u2WlanProfileId,
                                           &(pwssWlanMsgStruct->unWssWlanMsg.
                                             WssWlanConfigReq.unWlanConfReq.
                                             WssWlanDeleteReq)) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetBssAdminStatus:Construction of Delete Wlan\
            Msg failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

        MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                  WssWlanDeleteReq),
                &(pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                  unWlanConfReq.WssWlanDeleteReq),
                sizeof (pwssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanDeleteReq));
        pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;

        pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption = WSSWLAN_DEL_REQ;
        i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                           pWlcHdlrMsgStruct);

        if (i4RetVal == OSIX_FAILURE)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetBssAdminStatus:WlcHdlr\
            Call returned failed\r\n");
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        else if (i4RetVal == WSS_WLAN_NO_AP_PRESENT)
        {
            gu1IsWlanConfigResponseReceived = WSS_WLAN_NO_AP_PRESENT;
        }
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanProcessBindingRequest                               *
 * *                                                                           *
 * * Description  : Processcess the queued config Request from WTP             *
 * *                                                                           *
 * * Input        : OpCode , pointer to tWssWlanMsgStruct                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanProcessBindingRequest (UINT1 u1OpCode,
                              tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCfaMsgStruct       CfaMsgStruct;
    tWssWlanMsgStruct  *pwssWlanMsgStruct = NULL;
    INT4                i4AdminStatus = 0;
    UINT1               RadioId = 0;
    UINT1               WlanId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessBindingRequest:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pwssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pwssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessBindingRequest:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));
    MEMSET (pwssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessBindingRequest:Input struct tWssWlanMsgStruct is\
                empty, return failure\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    switch (u1OpCode)
    {
        case WSS_WLAN_PROCESS_ADD_BIND_REQ:
        {
            for (RadioId = 1;
                 RadioId <= pWssWlanMsgStruct->unWssWlanMsg.
                 WssWlanPendingConfReq.u1NoOfRadios; RadioId++)
            {
                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = RadioId;

                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanPendingConfReq.
                    u2WtpInternalId;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanProcessBindingRequest : Getting \
                                Radio index failed\r\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
                    return OSIX_FAILURE;
                }

                for (WlanId = WSSWLAN_START_WLANID_PER_RADIO;
                     WlanId <= WSSWLAN_END_WLANID_PER_RADIO; WlanId++)
                {
                    /* Call RadioIf module for retrieving BssIfIndex */
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = WlanId;
                    if (WssIfProcessRadioIfDBMsg
                        (WSS_GET_NEXT_WLAN_BSS_IFINDEX_DB,
                         &RadioIfGetDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "\nWssWlanProcessBindingRequest: RadioDBMsg\
                                    failed to return BssIfIndex");
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
                        return OSIX_FAILURE;
                    }

                    /* u4BssIfIndex should not be 0 for valid WLANs */
                    if (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex != 0)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                            RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                        pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus =
                            OSIX_TRUE;
                        pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus =
                            WSS_WLAN_BIND_FROM_CAPWAP;
                        if (WssIfProcessWssWlanDBMsg
                            (WSS_WLAN_SET_BSS_IFINDEX_ENTRY, pWssWlanDB)
                            != OSIX_SUCCESS)
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "\nWssWlanProcessBindingRequest:\
                                        WlanDBMsg failed to set Wlan Bind \
                                        Status\r\n");
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                    pwssWlanMsgStruct);
                            return OSIX_FAILURE;
                        }

                        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                            RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                            CFA_CAPWAP_DOT11_BSS;
                        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1AdminStatus
                            = CFA_IF_UP;

                        if (nmhGetIfMainAdminStatus ((INT4) RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     u4BssIfIndex,
                                                     &i4AdminStatus) !=
                            SNMP_SUCCESS)
                        {
                        }

                        if (i4AdminStatus != CFA_IF_UP)
                        {
                            if (WssIfProcessCfaMsg
                                (CFA_WSS_ADMIN_STATUS_CHG_MSG,
                                 &CfaMsgStruct) != OSIX_SUCCESS)
                            {
                                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                             "WssWlanProcessBindingReq:CFA status\
                                        updation (up)failed\r\n");
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                UtlShMemFreeWssWlanBuf ((UINT1 *)
                                                        pwssWlanMsgStruct);
                                return OSIX_FAILURE;
                            }
                        }
                        else
                        {
                            pwssWlanMsgStruct->unWssWlanMsg.
                                WssWlanSetAdminOperStatus.u1AdminStatus =
                                CFA_IF_UP;
                            pwssWlanMsgStruct->unWssWlanMsg.
                                WssWlanSetAdminOperStatus.u4IfIndex =
                                RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;

                            if (WssWlanSetBssAdminStatus (pwssWlanMsgStruct) !=
                                OSIX_SUCCESS)
                            {
                                WSSWLAN_TRC1 (WSSWLAN_MGMT_TRC,
                                              "WlanBindRequest: Sending Bind "
                                              "request failed for WLAN ID %d "
                                              "\r\n", WlanId);
                            }

                        }
                    }
                    else
                    {
                        WSSWLAN_TRC1 (WSSWLAN_MGMT_TRC, "BssIfIndex is 0 for\
                                    WlanId %d\r\n", WlanId);
                    }

                }
            }
        }
            break;
        default:
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "Invalid Msg Type received in\
                        ProcessBindingRequest\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
            return OSIX_FAILURE;
        }
            break;
    }
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pwssWlanMsgStruct);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * *                                                                           *
 * * Function     : WssWlanProcessConfigResponse
 * *                                                                           *
 * * Description  : Processcess the config Response from WTP                   *
 * * Input        : OpCode , pointer to tWssWlanMsgStruct                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/
UINT1
WssWlanProcessConfigResponse (UINT1 u1OpCode,
                              tWssWlanMsgStruct * pWssWlanMsgStruct)
{
    UNUSED_PARAM (u1OpCode);
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCfaMsgStruct       CfaMsgStruct;
#ifdef KERNEL_CAPWAP_WANTED

    UINT1               u1SsidIsolation = 0;

#endif
#ifdef NPAPI_WANTED
    tWlanParams         WlanNpParams;
    tFsHwNp             FsHwNp;
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));
    MEMSET (&WlanNpParams, 0, sizeof (tWlanParams));
#endif
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessConfigResponse:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_ENTRY ();

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&CfaMsgStruct, 0, sizeof (tCfaMsgStruct));

    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanProcessConfigResponse:Input struct WssWlanMsgStruct is\
        empty, return failure\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.WssWlanResultCode.
        u1IsPresent != OSIX_FALSE)
    {
        gu1IsWlanConfigResponseReceived =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
            WssWlanResultCode.u4ResultCode;

        if ((gu1IsWlanConfigResponseReceived == OSIX_FAILURE) ||
            (gu1IsWlanConfigResponseReceived == WSS_WLAN_RESPONSE_TIMEOUT))
        {
            gu1IsWlanConfigResponseReceived = OSIX_SUCCESS;
        }
    }

    if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.bIsSetAddRsp
        != OSIX_FALSE)
    {

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId
            = pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.WssWlanRsp.
            u1RadioId;

        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.u2WtpInternalId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse : Getting Radio index\
            failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* Call RadioIf module for retrieving BssIfIndex */
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
            WssWlanRsp.u1WlanId;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            RadioDBMsg failed to return BssIfIndex\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
        pWssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            WSS_WLAN_GET_BSS_IFINDEX_ENTRY failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        /*Setting the MAC-address for the CAPWAP-BSS binding interface */

        pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
        MEMCPY (pWssWlanDB->WssWlanAttributeDB.BssId,
                pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
                WssWlanRsp.BssId, MAC_ADDR_LEN);

        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

        MEMCPY (CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.CfaWssMacAddr,
                pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

        if (WssIfProcessCfaMsg (CFA_WSS_SET_MAC_ADDR,
                                &CfaMsgStruct) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse : \
            CFA_WSS_SET_MAC_ADDR failed\r\n");
        }

        /* Set the Radio Base MAC Addr only for the first Wlan ID */
        if (RadioIfGetDB.RadioIfGetAllDB.u1WlanId == 1)
        {
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
            MEMCPY (RadioIfGetDB.RadioIfGetAllDB.MacAddr,
                    pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessConfigResponse : \
            WSS_SET_RADIO_IF_DB failed\r\n");
            }
        }

        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.u2WtpInternalId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_RSM_FROM_INDEX,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse : Getting Radio \
            index failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (WssIfBssidVlanMemberAction
            (RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex,
             pWssWlanDB->WssWlanAttributeDB.u2VlanId, VLAN_ADD) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC2 (WSSWLAN_INFO_TRC,
                          "VLAN Member addition failed BSSID %d, VLAN %d\n",
                          RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex,
                          pWssWlanDB->WssWlanAttributeDB.u2VlanId);
        }
        else
        {
            WSSWLAN_TRC2 (WSSWLAN_INFO_TRC,
                          "VLAN Member addition SUCCESS BSSID %d, VLAN %d\n",
                          RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex,
                          pWssWlanDB->WssWlanAttributeDB.u2VlanId);
        }

#ifdef KERNEL_CAPWAP_WANTED
        if (WssIfGetIsolatedStationIpAddr
            (&pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex) == OSIX_SUCCESS)
        {
            u1SsidIsolation = 1;
        }

        if (CapwapUpdateKernelIntfDB (pWssWlanDB->WssWlanAttributeDB.BssId,
                                      pWssWlanDB->WssWlanAttributeDB.u2VlanId,
                                      u1SsidIsolation) == OSIX_FAILURE)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "Failed to update Kernel DB\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
#endif

#ifdef NPAPI_WANTED
        /* NpUsr */
        NP_UTIL_FILL_PARAMS (FsHwNp,
                             /*Generic NP structure */
                             NP_WLAN_MODULE,
                             /* Module ID */
                             WLAN_ENTRY_ADD,
                             /* Function/OpCode */
                             0,
                             /* IfIndex value if applicable */
                             0,
                             /* No. of Port Params */
                             0);
        /* No. of PortList Parms */

        WlanNpParams.u4IfIndex = RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
        WlanNpParams.radioId = pWssWlanMsgStruct->unWssWlanMsg.
            WssWlanConfigRsp.WssWlanRsp.u1RadioId;
        WlanNpParams.u4TunnelId = pWssIfCapwapDB->pSessEntry->u4TunnelId;
        MEMCPY (WlanNpParams.bssIdMacAddr,
                pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
                WssWlanRsp.BssId, MAC_ADDR_LEN);
        WlanNpParams.wlanVlanId = pWssWlanDB->WssWlanAttributeDB.u2VlanId;

        FsHwNp.WlanNpModInfo.unOpCode.sWlanNpEntryAdd.pWlanNpParams =
            &WlanNpParams;

        /*To Set from the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "NpUtilHwProgram : Values to get from Hw failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
#endif
        /* NpUsr */

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                      pWssWlanDB) == OSIX_SUCCESS)
        {
            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType =
                CFA_CAPWAP_DOT11_BSS;

            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

            CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1OperStatus = CFA_IF_UP;

            if (WssIfProcessCfaMsg (CFA_WSS_OPER_STATUS_CHG_MSG,
                                    &CfaMsgStruct) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProfileIntfRowStatus: Cfa status updation\
                        (up)failed , return failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_BSSID_MAPPING_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            DB Request to create BssidMapping Failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse: \
            DB Request to create BssidMapping Failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        /* When the binding interface is made up after processing the config
         * response,oper status change indication to be sent to CFA */

        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1IfType = CFA_CAPWAP_DOT11_BSS;

        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u4IfIndex =
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

        CfaMsgStruct.unCfaMsg.CfaCreateIfIndex.u1OperStatus = CFA_IF_UP;

        if (WssIfProcessCfaMsg (CFA_WSS_OPER_STATUS_CHG_MSG,
                                &CfaMsgStruct) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProfileIntfRowStatus: Cfa status updation\
            (up)failed , return failure \r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
#ifdef BAND_SELECT_WANTED
        WssWlanSendBandSelectStatus (RadioIfGetDB.RadioIfGetAllDB.
                                     u4RadioIfIndex,
                                     pWssWlanDB->WssWlanAttributeDB.
                                     u4BssIfIndex,
                                     RadioIfGetDB.RadioIfGetAllDB.u1WlanId,
                                     pWssIfCapwapDB->CapwapGetDB.
                                     u2WtpInternalId,
                                     pWssIfCapwapDB->CapwapGetDB.u1RadioId);
#endif

#ifdef NPAPI_WANTED
        /* Get WebAuthStatus from BssIdMappingDB */
        pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            Failed to return WebAuthStatus\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus == WSSWLAN_ENABLE)
        {
            pWssWlanDB->WssWlanIsPresentDB.bRuleId = OSIX_TRUE;
            pWssWlanDB->WssWlanAttributeDB.u2RuleId = gu2WssWlanStaRuleCount++;
            /* Rule to lift the HTTP packets destined to a BSSID */

            if (WssStaConfigTCPLiftRule (pWssWlanDB->WssWlanAttributeDB.BssId,
                                         pWssWlanDB->WssWlanAttributeDB.
                                         u2RuleId) == OSIX_FAILURE)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessConfigResponse : Rule to lift the  HTTP \
            packets from destined to a BSSID failed\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            /* Create and store rule in BssId Mapping DB */
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSSID_MAPPING_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanProcessConfigResponse:\
            WlanDB failed to set RuleId\r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
#endif
    }
    else if (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.bIsSetDeleteRsp
             != OSIX_FALSE)
    {

        pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u1RadioId
            = pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
            WssWlanRsp.u1RadioId;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.u2WtpInternalId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse : Getting Radio\
            index failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        /* Call RadioIf module for retrieving BssIfIndex */
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
            pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

        RadioIfGetDB.RadioIfGetAllDB.u1WlanId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigRsp.
            WssWlanRsp.u1WlanId;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            RadioDBMsg failed to return BssIfIndex\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;

        pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse: \
            DB Request to get wlanInternalId Failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse: DB Request to \
            wlanProfileId Failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
#ifdef NPAPI_WANTED
        /* Get rule in BssId Mapping DB */
        pWssWlanDB->WssWlanIsPresentDB.bRuleId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            WlanDB failed to get RuleId\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        /* Npusr */
        NP_UTIL_FILL_PARAMS (FsHwNp,
                             /*Generic NP structure */
                             NP_WLAN_MODULE,
                             /* Module ID */
                             WLAN_ENTRY_DELETE,
                             /* Function/OpCode */
                             0,
                             /* IfIndex value if applicable */
                             0,
                             /* No. of Port Params */
                             0);
        /* No. of PortList Parms */

        MEMCPY (WlanNpParams.bssIdMacAddr,
                pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

        FsHwNp.WlanNpModInfo.unOpCode.sWlanNpEntryAdd.pWlanNpParams =
            &WlanNpParams;

        /*To Set from the Hw */
        if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "NpUtilHwProgram : Values to get from Hw failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (WssStaDeleteClientRule (pWssWlanDB->WssWlanAttributeDB.u2RuleId,
                                    CUST_NP_ACL_LIST_ID4) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanProcessConfigResponse:WssStaDeleteClientRule\
            returns failure\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

#endif

        if (WssWlanBindingIntfRowStatus (pWssIfCapwapDB->CapwapGetDB.u4IfIndex,
                                         pWssWlanDB->WssWlanAttributeDB.
                                         u2WlanProfileId,
                                         WSSWLAN_RS_DESTROY) != OSIX_FAILURE)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanProcessConfigResponse:\
            WssWlanBindingIntfRowStatus destroy Failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

    }

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/* CLI procs - Start */

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetStationConfig                                    *
 * *                                                                           *
 * * Description  : This function processes the request received and invoke all*
 * *                the radios bound to the WLAN profile and send Configuration*
 * *                Update request and if the values is applied successfully   *
 * *                will update the DB.                                        *
 * *                                                                           *
 * * Input        : pWsscfgDot11StationConfigEntry - Pointer to dot11 station  *
 * *                config table                                               *
 * *                pWsscfgIsSetDot11StationConfigEntry                        *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetStationConfig (tWssWlanDB * pWssWlanDB)
{
    tWssWlanDB         *pwssWlanDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2WtpInternalId = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1IsValChanged = OSIX_FALSE;
    INT4                i4RetVal = 0;
    UINT1               u1UpdatedCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetStationConfig:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetStationConfig:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_ENTRY ();
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetStationConfig : Null Input received \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    if (pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid != OSIX_FALSE)
    {
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_CREATE_SSID_MAPPING_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetStationConfig : Creating SSID mapping\
                    DB failed. \r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                      pWssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetStationConfig : WssWLAN DB access failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }
    }

    if (pWssWlanDB->WssWlanIsPresentDB.bDtimPeriod != OSIX_FALSE)
    {
        /* Get the WlanRadioMappingCount from WLAN DB info to
         * fill the structure  */
        pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
            pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
        pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bDtimPeriod = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      pwssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetStationConfig : WssWLAN DB access failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }

        pwssWlanDB->WssWlanIsPresentDB.bShortPreamble = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                      pwssWlanDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetStationConfig : WssWLAN DB access failed\r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;

        }

        if ((pwssWlanDB->WssWlanAttributeDB.i2DtimPeriod) !=
            (pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod))
        {
            u1IsValChanged = OSIX_TRUE;
        }

        if (pwssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount == 0)
        {
            /* Update DB */

            pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            pwssWlanDB->WssWlanAttributeDB.i2DtimPeriod =
                pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod;
            pwssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
            pwssWlanDB->WssWlanIsPresentDB.bDtimPeriod = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_SET_IFINDEX_ENTRY, pwssWlanDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetStationConfig : WssWLAN DB \
                        Update failure \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                return OSIX_FAILURE;
            }

        }
        else
        {
            for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
                 u2WtpInternalId++)
            {
                if (pwssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount
                    == u1UpdatedCount)
                {
                    break;
                }

                /* Fill WLC Handler structure to get no of radios */
                pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
                    = u2WtpInternalId;
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB access\
                            failed. \r\n");
                    continue;
                }
                for (u1RadioId = 1; u1RadioId <=
                     pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                 pWssIfCapwapDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanSetStationConfig : WlcHdlr DB \
                                access failed. \r\n");
                        continue;
                    }
                    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                        UtlShMemAllocAntennaSelectionBuf ();

                    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection ==
                        NULL)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "\r%% Memory allocation for Antenna"
                                     " selection failed.\r\n");
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanSetStationConfig : WlcHdlr DB access\
                                failed. \r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        continue;
                    }

                    pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                                  pwssWlanDB) != OSIX_SUCCESS)
                    {
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        continue;
                    }
                    u1UpdatedCount++;
                    if (u1IsValChanged == OSIX_FALSE)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanSetStationConfig: No Value Change for "
                                     "DtimPeriod, Not trigering Config Update \r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_SUCCESS;
                    }

                    /* nmhRoutines already validated whether the 
                     * input received and old
                     * values are different. Hence fill config update 
                     * request structure */

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                        RadioIfConfig.isPresent = OSIX_TRUE;
                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u2MessageType = RADIO_CONF_UPDATE_STA_MSG_TYPE;

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u2MessageLength = RADIO_CONF_UPDATE_STA_MSG_LENGTH;

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u1ShortPreamble =
                        RadioIfGetDB.RadioIfGetAllDB.u1ShortPreamble;

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u1NoOfBssId = RadioIfGetDB.RadioIfGetAllDB.u1NoOfBssId;

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u2BeaconPeriod =
                        RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;

                    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                            RadioIfConfig.au1CountryString,
                            RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                            STRLEN (RadioIfGetDB.RadioIfGetAllDB.
                                    au1CountryString));

                    MEMCPY (pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.
                            RadioIfConfig.MacAddr,
                            RadioIfGetDB.RadioIfGetAllDB.MacAddr, MAC_ADDR_LEN);

                    pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfConfig.
                        u1DtimPeriod =
                        pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod;

                    /* Invoke WLC Handler to send the config update request */
                    i4RetVal =
                        WssIfProcessWlcHdlrMsg
                        (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ, pWlcHdlrMsgStruct);

                    if (i4RetVal == WLCHDLR_NO_AP_PRESENT)
                    {
                        gu1IsConfigResponseReceived = WLCHDLR_NO_AP_PRESENT;
                    }
                    else if (i4RetVal != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanSetStationConfig : To change the Radio "
                                     "IF config \r\n");
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        return OSIX_FAILURE;
                    }

                    while (1)
                    {
                        if ((gu1IsConfigResponseReceived == OSIX_SUCCESS) ||
                            (gu1IsConfigResponseReceived ==
                             WLCHDLR_NO_AP_PRESENT))
                        {
                            /* If the configuration is success then update
                             * the DB */

                            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                                pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                            RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod =
                                (UINT1) pWssWlanDB->WssWlanAttributeDB.
                                i2DtimPeriod;
                            RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod =
                                OSIX_TRUE;
                            /*RadioIfGetDB.RadioIfIsGetAllDB.bRadioId 
                               = OSIX_TRUE; */

                            if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB,
                                                          &RadioIfGetDB) !=
                                OSIX_SUCCESS)
                            {
                                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                             "WssWlanSetStationConfig : "
                                             "RadioDB is failed\r\n");
                                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                                 RadioIfGetAllDB.
                                                                 pu1AntennaSelection);
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                UtlShMemFreeWlcBuf ((UINT1 *)
                                                    pWlcHdlrMsgStruct);
                                return OSIX_FAILURE;
                            }

                            /* Update DB */

                            pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
                            pwssWlanDB->WssWlanAttributeDB.i2DtimPeriod =
                                pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod;
                            pwssWlanDB->WssWlanIsPresentDB.
                                bWlanRadioMappingCount = OSIX_TRUE;
                            pwssWlanDB->WssWlanIsPresentDB.bDtimPeriod =
                                OSIX_TRUE;

                            if (WssIfProcessWssWlanDBMsg
                                (WSS_WLAN_SET_IFINDEX_ENTRY,
                                 pwssWlanDB) != OSIX_SUCCESS)
                            {
                                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                             "WssWlanSetStationConfig : WssWLAN DB \
                                        Update failure \r\n");
                                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                                 RadioIfGetAllDB.
                                                                 pu1AntennaSelection);
                                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                                UtlShMemFreeWlcBuf ((UINT1 *)
                                                    pWlcHdlrMsgStruct);
                                return OSIX_FAILURE;
                            }
                        }

                        if ((gu1IsConfigResponseReceived == OSIX_FAILURE) ||
                            (gu1IsConfigResponseReceived ==
                             WSS_WLAN_RESPONSE_TIMEOUT))
                        {
                            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                         "WssWlanSetStationConfig : Response\
                                    failed \r\n");
                            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                             RadioIfGetAllDB.
                                                             pu1AntennaSelection);
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                            return OSIX_FAILURE;
                        }
                        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                         RadioIfGetAllDB.
                                                         pu1AntennaSelection);
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                        WSSWLAN_FN_EXIT ();
                        return OSIX_SUCCESS;
                    }
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);

                }

            }
        }

    }
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetAuthenticationAlgorithm                          *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWsscfgDot11AuthenticationAlgorithmsEntry - Pointer to     *
 * *                input structure                                            *
 * *                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry - Pointer to*
 * *                the boolean structure                                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetAuthenticationAlgorithm (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetAuthenticationAlgorithm : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetAuthenticationAlgorithm : WssWLAN DB\
                access failed. \r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetWEPDefaultKeys                                   *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWsscfgDot11WEPDefaultKeysEntry - Pointer to input         *
 * *                structure                                                  *
 * *                pWsscfgIsSetDot11WEPDefaultKeysEntry - Pointer to the      *
 * *                boolean structure                                          *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetWEPDefaultKeys (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetWEPDefaultKeys : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetWEPDefaultKeys : WssWLAN DB access failed. \r\n");
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetWlanProfile                                      *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWsscfgCapwapDot11WlanEntry - Pointer to input structure   *
 * *                                                                           *
 * *                pWsscfgIsSetCapwapDot11WlanEntry - Pointer to the boolean  *
 * *                structure                                                  *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetWlanProfile (tWssWlanDB * pWssWlanMsgDB)
{
    UINT4               u4WlanIfIndex = 0;

    WSSWLAN_FN_ENTRY ();

    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetWlanProfile : Null Input received \r\n");
        WSSWLAN_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus == WSSWLAN_RS_DESTROY)
    {
        if (WssWlanProfileIntfRowStatus
            (pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId,
             pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus,
             &pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetWlanProfile : Row Deletion failure \r\n");
            return OSIX_FAILURE;
        }
        WSSWLAN_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if ((pWssWlanMsgDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE) &&
        (pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus != WSSWLAN_RS_ACTIVE))
    {
        if (WssWlanProfileIntfRowStatus
            (pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId,
             pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus,
             &u4WlanIfIndex) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetWlanProfile : Row Creation failure \r\n");
            return OSIX_FAILURE;
        }
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_PROFILE_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetWlanProfile : WssWLAN DB access failed. \r\n");

        return OSIX_FAILURE;
    }

    if ((pWssWlanMsgDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE) &&
        (pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus) == WSSWLAN_RS_ACTIVE)
    {
        if (WssWlanProfileIntfRowStatus
            (pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId,
             pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus,
             &u4WlanIfIndex) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetWlanProfile : Row Creation/Deletion\
                    failure \r\n");

            return OSIX_FAILURE;
        }
    }
    if (pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus ==
        WSSWLAN_RS_CREATE_AND_WAIT)
    {
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                      pWssWlanMsgDB) != OSIX_SUCCESS)
        {
            WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                         "WssWlanSetWlanProfile : WssWLAN Get \
                    DB access failed. \r\n");

            return OSIX_FAILURE;
        }
    }
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;

}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetWlanBinding                                      *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWsscfgCapwapDot11WlanBindEntry -Pointer to input structure*
 * *                                                                           *
 * *                pWsscfgIsSetCapwapDot11WlanBindEntry - Pointer to boolean  *
 * *                structure                                                  *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetWlanBinding (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetWlanBinding : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    if (WssWlanBindingIntfRowStatus
        (pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId,
         pWssWlanMsgDB->WssWlanAttributeDB.u4RadioIfIndex,
         pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetWlanBinding : WssWlanBindingIntfRowStatus\
                failed to update RowStatus \r\n");

        return OSIX_FAILURE;
    }
    /* Return if the binding has been already destroyed or inactivated without
     * getting bssIfIndex */
    if ((pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus == WSSWLAN_RS_DESTROY) ||
        (pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus ==
         WSSWLAN_RS_NOT_IN_SERVICE))
    {
        WSSWLAN_FN_EXIT ();
        return OSIX_SUCCESS;
    }
    /* To get the BssIfIndex from database */
    pWssWlanMsgDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanMsgDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssCfgSetWlanBinding : WssWLAN DB access\
                WSS_WLAN_GET_BSS_ENTRY failed. \r\n");

        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetFsDot11StationConfigTable                        *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWsscfgFsDot11StationConfigEntry - Pointer to the input    *
 * *                structure                                                  *
 * *                pWsscfgIsSetFsDot11StationConfigEntry - Pointer to         *
 * *                the boolean structure                                      *
 * *                                                                           *
 * * Output       : None                                                       *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetFsDot11StationConfigTable (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetFsDot11StationConfigTable : \
                Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetFsDot11StationConfigTable:WssWLAN DB access\
                failed\r\n");

        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetQosProfile                                *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWsscfgFsVlanIsolationEntry - Pointer to input structure   *
 * *                pWsscfgIsSetFsVlanIsolationEntry - Pointer to the boolean  *
 * *                structure                                                  *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetQosProfile (tWssWlanDB * pWssWlanMsgDB)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanDB         *pwssWlanMsgDB = NULL;
    tWssifauthDBMsgStruct *pWssStaMsg = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2WlanInternalId = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1WlanId = 0;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4RadioIfIndex = 0;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT1               u1UpdatedCount = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssStaMsg =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssStaMsg == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile:- "
                     "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pWssStaMsg, 0, sizeof (tWssifauthDBMsgStruct));

    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile : Null Input received \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }
    pwssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile : WSS_WLAN_GET_IFINDEX_ENTRY DB \
        access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;

    }
    u2WlanInternalId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId;
    u2WlanProfileId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;

    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile : WSS_WLAN_GET_PROFILE_ENTRY DB \
        access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetQosProfile : WSS_WLAN_SET_IFINDEX_ENTRY\
        WssWLAN DB access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
        return OSIX_FAILURE;
    }

    u1WlanRadioMappingCount =
        pwssWlanMsgDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1UpdatedCount == u1WlanRadioMappingCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetQosProfile : WSS_CAPWAP_GET_DB access\
                failed. \r\n");
                continue;
            }
            for (u1RadioId = 1;
                 u1RadioId <= pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
                 u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetQosProfile : WSS_CAPWAP_GET_DB DB\
                access failed. \r\n");
                    continue;
                }
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                u1UpdatedCount++;
                /* Construct WlanUpdateReq message */
                if (WssWlanConstructUpdateWlanMsg (u4RadioIfIndex,
                                                   u2WlanProfileId,
                                                   &(pWssWlanMsgStruct->
                                                     unWssWlanMsg.
                                                     WssWlanConfigReq.
                                                     unWlanConfReq.
                                                     WssWlanUpdateReq)) !=
                    OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetQosProfile:Construction of Update\
                Wlan Msg failed\r\n");
                    continue;
                }
                MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                          WssWlanUpdateReq),
                        &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanUpdateReq),
                        sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq));

                /* Call DB for retrieving Updated QosRateLimit values */
                MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
                pwssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
                    pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex;
                pwssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

                pwssWlanMsgDB->WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bPassengerTrustMode =
                    OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              pwssWlanMsgDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetQosProfile : WSS_WLAN_GET_IFINDEX_\
                ENTRY DB access failed. \r\n");
                    continue;
                }

                pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].isOptional =
                    OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].u2MsgEleType
                    = VENDOR_SPECIFIC_PAYLOAD;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].u2MsgEleLen
                    = VEND_SSID_RATE_LIMIT_LEN + VEND_HEADER_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].elementId
                    = VENDOR_SSID_RATE_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u2MsgEleType =
                    SSID_RATE_VENDOR_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u2MsgEleLen =
                    VEND_SSID_RATE_LIMIT_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.vendorId = VENDOR_ID;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u1QosRateLimit =
                    pwssWlanMsgDB->WssWlanAttributeDB.u1QosRateLimit;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosUpStreamCIR =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosUpStreamCIR;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosUpStreamCBS =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosUpStreamCBS;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosUpStreamEIR =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosUpStreamEIR;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosUpStreamEBS =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosUpStreamEBS;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosDownStreamCIR =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosDownStreamCIR;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosDownStreamCBS =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosDownStreamCBS;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosDownStreamEIR =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosDownStreamEIR;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u4QosDownStreamEBS =
                    pwssWlanMsgDB->WssWlanAttributeDB.u4QosDownStreamEBS;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u1PassengerTrustMode =
                    pwssWlanMsgDB->WssWlanAttributeDB.u1PassengerTrustMode;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1RadioId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidRate.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1WlanId;

                pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption =
                    WSSWLAN_UPDATE_REQ;
                if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                            pWlcHdlrMsgStruct) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetQosProfile:\
                WlcHdlr Call returned failed\r\n");
                    continue;
                }
                /* WMM changes */
                if ((pwssWlanMsgDB->WssWlanIsPresentDB.bPassengerTrustMode ==
                     OSIX_TRUE)
                    || (pwssWlanMsgDB->WssWlanIsPresentDB.bQosProfileId ==
                        OSIX_TRUE))
                {
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        u4RadioIfIndex;
                    for (u1WlanId = 1; u1WlanId <= 16; u1WlanId++)
                    {
                        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
                        if (WssIfProcessRadioIfDBMsg
                            (WSS_GET_WLAN_BSS_IFINDEX_DB,
                             &RadioIfGetDB) == OSIX_SUCCESS)
                        {
                            pwssWlanMsgDB->WssWlanAttributeDB.u4BssIfIndex =
                                RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
                            pwssWlanMsgDB->WssWlanIsPresentDB.
                                bWlanInternalId = OSIX_TRUE;
                            pwssWlanMsgDB->WssWlanIsPresentDB.
                                bWlanProfileId = OSIX_TRUE;
                            if (WssIfProcessWssWlanDBMsg
                                (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                 pwssWlanMsgDB) == OSIX_SUCCESS)
                            {
                                if (pwssWlanMsgDB->WssWlanAttributeDB.
                                    u2WlanProfileId == u2WlanProfileId)
                                {
                                    pWssStaMsg->WssStaDB.u4BssIfIndex =
                                        pwssWlanMsgDB->WssWlanAttributeDB.
                                        u4BssIfIndex;
                                    WssIfProcessWssAuthDBMsg
                                        (WSS_STA_GET_CLIENT_MAC_PER_BSSID,
                                         pWssStaMsg);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssStaMsg);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetAuthenticationProfile                            *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : Pointer to WssWlanDB                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetAuthenticationProfile (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetAuthenticationProfile : Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetAuthenticationProfile : WssWLAN DB access\
                failed. \r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetMulticastProfile                                 *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : Pointer to WssWlanDB                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetMulticastProfile (tWssWlanDB * pWssWlanMsgDB)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanDB         *pwssWlanMsgDB = NULL;
    UINT2               u2WlanInternalId = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    UINT1               u1RadioId = 0;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4RadioIfIndex = 0;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT1               u1UpdatedCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile : Null Input received \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    /* Update the database */
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile:WssWLAN DB access \
                WSS_WLAN_SET_IFINDEX_ENTRY failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

/* To update WTP */
    pwssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile:WssWLAN DB access\
                WSS_WLAN_GET_IFINDEX_ENTRY failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;

    }
    u2WlanInternalId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId;
    u2WlanProfileId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;

    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetMulticastProfile:WssWLAN DB access\
                WSS_WLAN_GET_PROFILE_ENTRY failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    u1WlanRadioMappingCount =
        pwssWlanMsgDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1UpdatedCount == u1WlanRadioMappingCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetMulticastProfile : WlcHdlr DB\
                            access for NoOfRadio failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetMulticastProfile : WlcHdlr DB\
                                access failed. \r\n");
                    continue;
                }
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                /* Construct WlanUpdateReq message */
                if (WssWlanConstructUpdateWlanMsg (u4RadioIfIndex,
                                                   u2WlanProfileId,
                                                   &(pWssWlanMsgStruct->
                                                     unWssWlanMsg.
                                                     WssWlanConfigReq.
                                                     unWlanConfReq.
                                                     WssWlanUpdateReq)) !=
                    OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetMulticastProfile :Construction of\
                                Update Wlan Msg failed, return failure\r\n");
                    continue;
                }
                MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                          WssWlanUpdateReq),
                        &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanUpdateReq),
                        sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq));

                pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].isOptional =
                    OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    u2MsgEleLen = WSS_WLAN_MULTICAST_LEN + VEND_HEADER_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].elementId =
                    VENDOR_MULTICAST_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendorMulticast.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendorMulticast.u2MsgEleType =
                    MULTICAST_VENDOR_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendorMulticast.u2MsgEleLen =
                    WSS_WLAN_MULTICAST_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendorMulticast.vendorId = VENDOR_ID;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendorMulticast.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1RadioId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendorMulticast.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1WlanId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
                    VendorMulticast.u2WlanMulticastMode =
                    pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                    WssWlanUpdateReq.WssWlanMulticast.u2WlanMulticastMode;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
                    VendorMulticast.u2WlanSnoopTableLength =
                    pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                    WssWlanUpdateReq.WssWlanMulticast.u2WlanSnoopTableLength;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
                    VendorMulticast.u4WlanSnoopTimer =
                    pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                    WssWlanUpdateReq.WssWlanMulticast.u4WlanSnoopTimer;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].unVendorSpec.
                    VendorMulticast.u4WlanSnoopTimeout =
                    pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                    WssWlanUpdateReq.WssWlanMulticast.u4WlanSnoopTimeout;

                pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption =
                    WSSWLAN_UPDATE_REQ;
                if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                            pWlcHdlrMsgStruct) == OSIX_FAILURE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetMulticastProfile:WlcHdlr Call \
                                returned failed\r\n");
                    continue;
                }
                u1UpdatedCount++;
            }
        }
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetVlanIsolation :\
                WssWLAN DB access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetCapabilityProfile                                *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : Pointer to WssWlanDB                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetCapabilityProfile (tWssWlanDB * pWssWlanMsgDB)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanDB         *pwssWlanMsgDB = NULL;
    UINT2               u2WlanInternalId = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    UINT1               u1RadioId = 0;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4RadioIfIndex = 0;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT1               u1UpdatedCount = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile : Null Input received \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    /* Update the database */
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile:WssWLAN DB access \
                WSS_WLAN_SET_IFINDEX_ENTRY failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

/* To update WTP */
    pwssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWssWlanQosCapab = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile:WssWLAN DB access\
                WSS_WLAN_GET_IFINDEX_ENTRY failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;

    }
    u2WlanInternalId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId;
    u2WlanProfileId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;

    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetCapabilityProfile:WssWLAN DB access\
                WSS_WLAN_GET_PROFILE_ENTRY failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    u1WlanRadioMappingCount =
        pwssWlanMsgDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1WlanRadioMappingCount == u1UpdatedCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetCapabilityProfile : WlcHdlr DB\
            access for NoOfRadio failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetCapabilityProfile : WlcHdlr DB\
                access failed. \r\n");
                    continue;
                }
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;

                u1UpdatedCount++;
                /* Construct WlanUpdateReq message */
                if (WssWlanConstructUpdateWlanMsg (u4RadioIfIndex,
                                                   u2WlanProfileId,
                                                   &(pWssWlanMsgStruct->
                                                     unWssWlanMsg.
                                                     WssWlanConfigReq.
                                                     unWlanConfReq.
                                                     WssWlanUpdateReq)) !=
                    OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetCapabilityProfile :Construction of\
                Update Wlan Msg failed, return failure\r\n");
                    continue;
                }
                MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                          WssWlanUpdateReq),
                        &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanUpdateReq),
                        sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq));
                pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;

                pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption =
                    WSSWLAN_UPDATE_REQ;
                if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                            pWlcHdlrMsgStruct) == OSIX_FAILURE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetCapabilityProfile:WlcHdlr Call \
                returned failed\r\n");
                    continue;
                }
            }
        }
    }

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetVlanIsolation                                    *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : Pointer to WssWlanDB                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetVlanIsolation (tWssWlanDB * pWssWlanMsgDB)
{

    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanDB         *pwssWlanMsgDB = NULL;
    UINT2               u2WlanInternalId = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    UINT1               u1RadioId = 0;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4RadioIfIndex = 0;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
#ifdef KERNEL_CAPWAP_WANTED
    UINT1               u1VlanIsolation = 0;
#endif
    UINT1               u1UpdatedCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIsolation:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIsolation:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIsolation:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIsolation : Null Input received \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    pwssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetVlanIsolation :\
                         WssWLAN DB access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;

    }
    u2WlanInternalId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId;
    u2WlanProfileId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;

    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pwssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetVlanIsolation : WssWLAN DB access\
                WSS_WLAN_GET_PROFILE_ENTRY failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    u1WlanRadioMappingCount =
        pwssWlanMsgDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1UpdatedCount == u1WlanRadioMappingCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetVlanIsolation : WlcHdlr DB access\
            WSS_CAPWAP_GET_DB for NoOfRadio failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetVlanIsolation : WlcHdlr DB access\
                WSS_CAPWAP_GET_DB failed. \r\n");
                    continue;
                }
                u4RadioIfIndex = pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                u1UpdatedCount++;
                /* Construct WlanUpdateReq message */
                if (WssWlanConstructUpdateWlanMsg (u4RadioIfIndex,
                                                   u2WlanProfileId,
                                                   &(pWssWlanMsgStruct->
                                                     unWssWlanMsg.
                                                     WssWlanConfigReq.
                                                     unWlanConfReq.
                                                     WssWlanUpdateReq)) !=
                    OSIX_SUCCESS)
                {
                    continue;
                }
                MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                          WssWlanUpdateReq),
                        &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanUpdateReq),
                        sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq));
                pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].isOptional =
                    OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    u2MsgEleLen = VEND_SSID_ISOLATION_LEN + VEND_HEADER_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].elementId =
                    VENDOR_SSID_ISOLATION_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.u2MsgEleType =
                    SSID_ISOLATION_VENDOR_MSG;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.u2MsgEleLen =
                    VEND_SSID_ISOLATION_LEN;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.vendorId = VENDOR_ID;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.u1Val =
                    pWssWlanMsgDB->WssWlanAttributeDB.u1SsidIsolation;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.u1RadioId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1RadioId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
                    unVendorSpec.VendSsidIsolation.u1WlanId =
                    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanUpdateReq.u1WlanId;

                pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption =
                    WSSWLAN_UPDATE_REQ;
                if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                            pWlcHdlrMsgStruct) == OSIX_FAILURE)
                {
                    continue;
                }

                /* Update the kernel DB  */
                MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
                pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId =
                    u2WlanProfileId;
                pwssWlanMsgDB->WssWlanAttributeDB.u4RadioIfIndex =
                    u4RadioIfIndex;
                pwssWlanMsgDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                pwssWlanMsgDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSS_ENTRY, pwssWlanMsgDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanBindingIntfRowStatus:BssInterface Get\
                BssInterface RowStatus failed\r\n");
                    continue;
                }

                if (pwssWlanMsgDB->WssWlanAttributeDB.u1RowStatus ==
                    WSSWLAN_RS_ACTIVE)
                {
                    pwssWlanMsgDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSS_ENTRY, pwssWlanMsgDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus:BssInterface Get\
                BssIfIndex failed\r\n");
                        continue;
                    }
                    pwssWlanMsgDB->WssWlanIsPresentDB.bWlanBindStatus =
                        OSIX_TRUE;
                    pwssWlanMsgDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
                    pwssWlanMsgDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                         pwssWlanMsgDB) != OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WssWlanBindingIntfRowStatus: WlanDBMsg\
                failed to Get Wlan Bind Status\r\n");
                        continue;
                    }
#ifdef KERNEL_CAPWAP_WANTED
                    if (pWssWlanMsgDB->WssWlanAttributeDB.u1SsidIsolation)
                    {
                        u1VlanIsolation = 1;
                    }
                    if (CapwapUpdateKernelIntfDB
                        (pwssWlanMsgDB->WssWlanAttributeDB.BssId,
                         pwssWlanMsgDB->WssWlanAttributeDB.u2VlanId,
                         u1VlanIsolation) == OSIX_FAILURE)
                    {
                        continue;
                    }
#endif
                }

            }
        }
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetVlanIsolation :\
                WssWLAN DB access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetDot11SpectrumManagementTable                     *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : Pointer to WssWlanDB                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSetDot11SpectrumManagementTable (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetDot11SpectrumManagementTable : \
                Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetDot11SpectrumManagementTable :\
                WssWLAN DB access failed. \r\n");
        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * * Function     : WssWlanGetManagmentSSID
 *
 * * Description  : This function will get the WLAN Managment SSID
 * * Input        : None
 * * Output       : None
 * * Returns      : None
 * ****************************************************************************/
UINT1
WssWlanGetManagmentSSID (UINT1 *pu1ManagmentSSID)
{

    tWssWlanDB          WssWlanDB;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    WssWlanDB.WssWlanIsPresentDB.bManagmentSSID = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_MANAGMENT_SSID,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_INFO_TRC, "WssWlanSetManagmentSSID :\
                    WssWLAN DB access failed. \r\n");
        return OSIX_FAILURE;

    }
    MEMCPY (pu1ManagmentSSID, WssWlanDB.WssWlanAttributeDB.
            au1ManagmentSSID, WSSWLAN_SSID_NAME_LEN);
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * * Function     : WssWlanSetManagmentSSID
 *
 * * Description  : This function will store the WLAN Managment SSID
 * * Input        : Managment SSID to be set
 * * Output       : None
 * * Returns      : None
 * **************************************************************************/
UINT1
WssWlanSetManagmentSSID (UINT1 *pau1ManagmentSSID)
{
    tWssWlanDB         *pWssWlanDB = NULL;
#ifdef WLCHDLR_WANTED
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetManagmentSSID:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
#endif
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetManagmentSSID:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
#ifdef WLCHDLR_WANTED
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    WSSWLAN_FN_ENTRY ();
#ifdef WLCHDLR_WANTED
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen = 40;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendSsid.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendSsid.u2MsgEleType = MGMT_SSID_VENDOR_MSG;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendSsid.
        u2MsgEleLen = 36;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.VendSsid.
        vendorId = VENDOR_ID;
    MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendSsid.au1ManagmentSSID, pau1ManagmentSSID,
            WSSWLAN_SSID_NAME_LEN);
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                pWlcHdlrMsgStruct) == OSIX_FAILURE)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssIfProcessWlcHdlrMsg WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ\
                failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif

    MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1ManagmentSSID,
            pau1ManagmentSSID, WSSWLAN_SSID_NAME_LEN);
    pWssWlanDB->WssWlanIsPresentDB.bManagmentSSID = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_MANAGMENT_SSID,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_INFO_TRC, "WssWlanSetManagmentSSID :\
                WssWLAN DB access failed. \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetProfileCount                                     *
 * *                                                                           *
 * * Description  : This function will return the WLAN Profile count           *
 * *                                                                           *
 * * Input        : Pointer to WssWlanDB                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                    *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
VOID
WssWlanGetProfileCount (UINT2 *pu2WlanCount)
{
    WSSWLAN_FN_ENTRY ();
    *pu2WlanCount = gu2WlanInternalIdCount;
    WSSWLAN_FN_EXIT ();
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanIdfromIfIndex                                *
 * *                                                                           *
 * * Description  : This function will return WlanProfileId from the If Index  *
 * *                                                                           *
 * * Input        : u4WlanProfileIfIndex                                       * 
 * *                                                                           *
 * * Output       : WlanProfileId                                              *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanGetWlanIdfromIfIndex (UINT4 u4WlanProfileIfIndex,
                             UINT2 *pu2WlanProfileId)
{
    tWssWlanDB          WlanMsgDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    WlanMsgDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    WlanMsgDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    WlanMsgDB.WssWlanAttributeDB.u4WlanIfIndex = u4WlanProfileIfIndex;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  &WlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetWlanIdfromIfIndex: DB\
                Request from RadioIfDB Failed, return failure\r\n");

        *pu2WlanProfileId = 0;
        return OSIX_FAILURE;
    }
    *pu2WlanProfileId = WlanMsgDB.WssWlanAttributeDB.u2WlanProfileId;
    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetSSIDfromIfIndex                                  *
 * *                                                                           *
 * * Description  : This function will return the WLAN If Index from ssid      *
 * *                                                                           *
 * * Input        : WlanIfIndex                                                *
 * *                                                                           *
 * * Output       : SSID                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/

VOID
WssWlanGetSSIDfromIfIndex (UINT4 u4WlanIfIndex, UINT1 *pu1SSID)
{
    tWssWlanDB          WlanMsgDB;
    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));
    WlanMsgDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
    WlanMsgDB.WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  &WlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetSSIDfromIfIndex: DB Request\
                to get Desired Ssid Failed, return failure\r\n");

        return;
    }
    MEMCPY (pu1SSID, WlanMsgDB.WssWlanAttributeDB.au1DesiredSsid,
            WSSWLAN_SSID_NAME_LEN);
    WSSWLAN_FN_EXIT ();
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanIfIndexfromSSID                              *
 * *                                                                           *
 * * Description  : This function will return the WLAN If Index from ssid      *
 * *                                                                           *
 * * Input        : SSID                                                       * 
 * *                                                                           *
 * * Output       : WlanIfIndex                                                *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanGetWlanIfIndexfromSSID (UINT1 *pu1SSID, UINT4 *pu4WlanIfIndex)
{
    tWssWlanDB          WlanMsgDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu1SSID == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetWlanIfIndexfromSSID : SSID is empty. \r\n");
        return OSIX_FAILURE;
    }
    if (STRLEN (pu1SSID) > WSSWLAN_SSID_NAME_LEN)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetWlanIfIndexfromSSID : SSID length greater than Limit. \r\n");
        return OSIX_FAILURE;
    }
    MEMCPY (WlanMsgDB.WssWlanAttributeDB.au1DesiredSsid,
            pu1SSID, STRLEN (pu1SSID));
    WlanMsgDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_SSID_MAPPING_ENTRY,
                                  &WlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetWlanIfIndexfromSSID : DB access failed. \r\n");

        *pu4WlanIfIndex = 0;
        return OSIX_FAILURE;
    }
    *pu4WlanIfIndex = WlanMsgDB.WssWlanAttributeDB.u4WlanIfIndex;

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanProfileId                                    *
 * *                                                                           *
 * * Description  : This function will return the WLAN Profile Id              *
 * *                                                                           *
 * * Input        : u2WlanId  - WLAN Internal Profile Id                       * 
 * *                                                                           *
 * * Output       : WlanProfileId                                              *
 * *                                                                           *
 * * Returns      : None                                                    *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
VOID
WssWlanGetWlanProfileId (UINT2 u2WlanInternalId, UINT2 *pu2WlanProfileId)
{
    tWssWlanDB          WlanMsgDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu2WlanProfileId == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetWlanProfileId: Null\
                Inputs\r\n");
        return;
    }

    WlanMsgDB.WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    WlanMsgDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY,
                                  &WlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetWlanProfileId: DB Request\
                to get WlanProfileId Failed, return failure\r\n");

        *pu2WlanProfileId = 0;
        return;
    }
    *pu2WlanProfileId = WlanMsgDB.WssWlanAttributeDB.u2WlanProfileId;
    WSSWLAN_FN_EXIT ();

    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetWlanInternalId                                   *
 * *                                                                           *
 * * Description  : This function will return the WLAN Internal Id for ProfileId
 * *                                                                           *
 * * Input        : u2WlanId  - WLAN Profile Id                                * 
 * *                                                                           *
 * * Output       : WlanInternalId                                             *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanGetWlanInternalId (UINT4 u4WlanProfileId, UINT2 *pu2WlanInternalId)
{
    tWssWlanDB          WlanMsgDB;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu2WlanInternalId == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetWlanInternalId:\
                Null Input\r\n");
        return OSIX_FAILURE;
    }

    WlanMsgDB.WssWlanAttributeDB.u2WlanProfileId = (UINT2) u4WlanProfileId;
    WlanMsgDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, &WlanMsgDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetWlanInternalId: DB Request\
                to get WlanInternelId Failed, return failure\r\n");

        *pu2WlanInternalId = 0;
        return OSIX_FAILURE;
    }
    *pu2WlanInternalId = WlanMsgDB.WssWlanAttributeDB.u2WlanInternalId;

    WSSWLAN_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetDot11DesiredSSID                                 *
 * *                                                                           *
 * * Description  : This function will return the WLAN SSID                    *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       * 
 * *                                                                           *
 * * Output       : Wlan SSID                                                  *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 *******************************************************************************/
VOID
WssWlanGetDot11DesiredSSID (UINT4 u4WlanIfIndex, UINT1 *pu1WlanSSID)
{
    tWssWlanDB          WlanMsgDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));

    if (pu1WlanSSID == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11DesiredSSID:\
                Null input\r\n");
        return;
    }

    WlanMsgDB.WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    WlanMsgDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WlanMsgDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetDot11DesiredSSID: DB Request\
                to get Desired SSID Failed, return failure\r\n");

        *pu1WlanSSID = 0;
        return;
    }
    MEMCPY (pu1WlanSSID, WlanMsgDB.WssWlanAttributeDB.au1DesiredSsid,
            STRLEN (WlanMsgDB.WssWlanAttributeDB.au1DesiredSsid));
    WSSWLAN_FN_EXIT ();

    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetDot11BssIdCount                                 *
 * *                                                                           *
 * * Description  : This function will return the WLAN BssIdCount              *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       * 
 * *                                                                           *
 * * Output       : BssIdCount                                                 *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 *******************************************************************************/
VOID
WssWlanGetDot11BssIdCount (UINT4 u4RadioIfIndex, UINT1 *pu1BssIdCount)
{
    tRadioIfGetDB       RadioIfGetDB;
    WSSWLAN_FN_ENTRY ();

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (u4RadioIfIndex == 0)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11BssIdCount: Empty\
                RadioIfIndex\r\n");

        *pu1BssIdCount = 0;
        return;
    }

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return;
    }
    /* Get RadioIfDB for BssIdCount */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanGetDot11BssIdCount: DB Request\
                from RadioIfDB Failed, return failure\r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return;
    }
    *pu1BssIdCount = RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    WSSWLAN_FN_EXIT ();

    return;

}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetDot11BssIfIndex                                  *
 * *                                                                           *
 * * Description  : This function will return the WLAN BssIfIndex              *
 * *                                                                           *
 * * Input        : u4RadioIfIndex , u1WlanId                                  * 
 * *                                                                           *
 * * Output       : BssIfIndex                                                 *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 *******************************************************************************/
VOID
WssWlanGetDot11BssIfIndex (UINT4 u4RadioIfIndex, UINT1 u1WlanId,
                           UINT4 *pu4BssIfIndex)
{
    tRadioIfGetDB       RadioIfGetDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((u4RadioIfIndex == 0) || (u1WlanId == 0))
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11BssIfIndex: Empty\
                RadioIfIndex or WlanId\r\n");

        *pu4BssIfIndex = 0;
        return;
    }

    /* Get RadioIfDB for BssIdCount */
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_INFO_TRC, "WssWlanGetDot11BssIfIndex: DB Request\
                from RadioIfDB Failed, return failure\r\n");
        return;
    }
    *pu4BssIfIndex = RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
    WSSWLAN_FN_EXIT ();
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetDot11BssId                                       *
 * *                                                                           *
 * * Description  : This function will return the BssId                        *
 * *                                                                           *
 * * Input        : u4BssIfIndex = Bss Interface index                       * 
 * *                                                                           *
 * * Output       : BssId                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 ******************************************************************************/
VOID
WssWlanGetDot11BssId (UINT4 u4BssIfIndex, UINT1 *pu1BssId)
{
    tWssWlanDB          WlanMsgDB;

    WSSWLAN_FN_ENTRY ();
    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));
    if (u4BssIfIndex == 0)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11BssId: Empty\
                BssIfIndex \r\n");
        return;
    }

    WlanMsgDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    WlanMsgDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &WlanMsgDB)
        != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11BssId: DB Request\
                to get BssId Failed, return failure");

        *pu1BssId = 0;
        return;
    }
    MEMCPY (pu1BssId, WlanMsgDB.WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
    WSSWLAN_FN_EXIT ();
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetDot11Ssid                                       *
 * *                                                                           *
 * * Description  : This function will return the Ssid                        *
 * *                                                                           *
 * * Input        : BssIfIndex                                                 * 
 * *                                                                           *
 * * Output       : SSID                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 ******************************************************************************/
VOID
WssWlanGetDot11SSID (UINT4 u4BssIfIndex, UINT1 *pu1SSID)
{
    tWssWlanDB          WlanMsgDB;

    MEMSET (&WlanMsgDB, 0, sizeof (tWssWlanDB));
    WSSWLAN_FN_ENTRY ();
    if (u4BssIfIndex == 0)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11SSID: BssIfIndex\
                is NULL\r\n");

        return;
    }

    WlanMsgDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    WlanMsgDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    WlanMsgDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                  &WlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanGetDot11SSID: DB Request\
                to get WlanInternalId,WlanIfindex Failed, return failure");

        return;
    }

    WssCfGetDot11DesiredSSID (WlanMsgDB.WssWlanAttributeDB.u4WlanIfIndex,
                              pu1SSID);

    WSSWLAN_FN_EXIT ();
    return;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssSendDeleteWlanMsgfromRsna                               *
 * *                                                                           *
 * * Description  : This function will send the delete wlan message.           *
 * *                when recieved a indication from RSNA                       *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 ******************************************************************************/

UINT4
WssSendDeleteWlanMsgfromRsna (UINT4 u4WlanIfIndex)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT4               u4RadioIfIndex = 0;
    INT4                i4RetVal = 1;
    UINT2               u2WlanProfileId = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2WlanInternalId = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    UINT1               u1UpdatedCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendDeleteWlanMsgfromRsna:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendDeleteWlanMsgfromRsna:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendDeleteWlanMsgfromRsna:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    u2WlanProfileId = pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
    u2WlanInternalId = pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId;

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "RsnaSendDeleteWlanMsg : WssWLAN DB access failed\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;

    }
    u1WlanRadioMappingCount =
        pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount;

    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1UpdatedCount == u1WlanRadioMappingCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
                = u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "RsnaSendDeleteWlanMsg : WlcHdlr DB access\
                            failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "RsnaSendDeleteWlanMsg : WlcHdlr DB \
                                access failed. \r\n");
                    continue;
                }
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\r%% Memory allocation for Antenna"
                                 " selection failed.\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB access\
                                failed. \r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                u4RadioIfIndex = pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                u1UpdatedCount++;
            }
        }
    }
    if ((WssWlanConstructDeleteWlanMsg (u4RadioIfIndex, u2WlanProfileId,
                                        &(pWssWlanMsgStruct->unWssWlanMsg.
                                          WssWlanConfigReq.unWlanConfReq.
                                          WssWlanDeleteReq))) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    /*u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId; */

    MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
              WssWlanDeleteReq),
            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
              unWlanConfReq.WssWlanDeleteReq),
            sizeof (pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                    unWlanConfReq.WssWlanDeleteReq));
    pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;

    pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption = WSSWLAN_DEL_REQ;
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                       pWlcHdlrMsgStruct);

    if (i4RetVal == OSIX_FAILURE)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WssWlanSetBssAdminStatus:WlcHdlr\
                    Call returned failed\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

#ifdef WPS_WANTED
/******************************************************************************
 * *                                                                           *
 * * Function     : WssSendUpdateWlanMsgfromWps                               *
 * *                                                                           *
 * * Description  : This function will send the update wlan message.           *
 * *                when recieved a indication from WPS                        *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 ******************************************************************************/
UINT4
WssSendUpdateWlanMsgfromWps (UINT4 u4WlanIfIndex, UINT2 u2WpsWtpInternalId,
                             UINT1 statusOrAddIE)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tWpsIEElements      WpsIEElements;
    UINT4               u4RadioIfIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4WPSEnabled;
    UINT2               u2WlanProfileId = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2WlanInternalId = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    INT4                i4WpsIELength = 0;
    UINT1               u1Count = 0;
    UINT1               u1UpdatedCount = 0;

    MEMSET (&WpsIEElements, 0, sizeof (tWpsIEElements));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendUpdateWlanMsgfromWps:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendUpdateWlanMsgfromWps:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendUpdateWlanMsgfromWps:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    u2WlanProfileId = pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
    u2WlanInternalId = pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId;

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetStationConfig : WssWLAN DB access failed\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }
    u1WlanRadioMappingCount =
        pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1UpdatedCount == u1WlanRadioMappingCount)
            {
                break;
            }
            if (u2WpsWtpInternalId != WPS_ALL_AP)
            {
                if (u2WpsWtpInternalId != u2WtpInternalId)
                {
                    continue;
                }
            }
            /* Fill WLC Handler structure to get no of radios */
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
                = u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetStationConfig : WlcHdlr DB access\
                failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB \
                access failed. \r\n");
                    continue;
                }
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\r%% Memory allocation for Antenna"
                                 " selection failed.\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBPFlag = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB access\
                failed. \r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                u4RadioIfIndex = pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);

                /* Let the WssSendUpdateWlanMsgfromWps(), send a Update WLAN message to AP only
                 * if the WLAN Binding has been done, so that ADD WLAN is already invoked.
                 * This condition is made to ensure that ADD WLAN message is send to AP at fist,
                 * and followed by which UPDATE WLAN messages are called.
                 */

                /*Calling WlanDB to Know whether binding has done or not */

                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                    u2WlanProfileId;
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
                pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanConstructAddWlanMsg: BssInterface DB Get failed,\
                return failure \r\n");
                    continue;
                }

                if ((WssWlanConstructUpdateWlanMsg
                     (u4RadioIfIndex, u2WlanProfileId,
                      &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanUpdateReq))) == OSIX_FAILURE)
                {
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }

                MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                          WssWlanUpdateReq),
                        &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanUpdateReq),
                        sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq));
                pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;

                WpsGetWPSEnabled (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                                  (UINT4) u2WtpInternalId, &i4WPSEnabled);
                {
                    if (WpsGetWpsIEParams
                        (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex,
                         (UINT4) u2WtpInternalId,
                         &WpsIEElements) == OSIX_SUCCESS)
                    {
                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.u1ElemId =
                            WpsIEElements.u1ElemId;
                        i4WpsIELength = i4WpsIELength + WPS_IE_ELEMENT_LENGTH;

                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.u1Len =
                            WpsIEElements.u1Len;
                        i4WpsIELength = i4WpsIELength + WPS_IE_LENGTH_FIELD;

                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.bWpsEnabled =
                            WpsIEElements.bWpsEnabled;
                        i4WpsIELength = i4WpsIELength + WPS_IE_CAPAB_LENGTH;

                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.wscState =
                            WpsIEElements.wscState;
                        i4WpsIELength = i4WpsIELength + WPS_IE_STATE_LENGTH;

                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.bWpsAdditionalIE =
                            WpsIEElements.bWpsAdditionalIE;
                        i4WpsIELength =
                            i4WpsIELength + WPS_IE_ADDITIONALIE_LENGTH;

                        for (u1Count = 0; u1Count < WpsIEElements.noOfAuthMac;
                             u1Count++)
                        {
                            STRNCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.
                                     RadioIfInfoElement.unInfoElem.
                                     WpsIEElements.authorized_macs[u1Count],
                                     WpsIEElements.authorized_macs[u1Count],
                                     WPS_ETH_ALEN);
                            i4WpsIELength = i4WpsIELength + WPS_ETH_ALEN;
                        }

                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.noOfAuthMac =
                            WpsIEElements.noOfAuthMac;
                        i4WpsIELength = i4WpsIELength + WPS_IE_CAPAB_LENGTH;

                        MEMCPY (pWlcHdlrMsgStruct->WssWlanConfigReq.
                                RadioIfInfoElement.unInfoElem.WpsIEElements.
                                uuid_e, WpsIEElements.uuid_e, WPS_UUID_LEN);
                        i4WpsIELength = i4WpsIELength + WPS_UUID_LEN;

                        pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                            unInfoElem.WpsIEElements.configMethods =
                            WpsIEElements.configMethods;
                        i4WpsIELength =
                            i4WpsIELength + WPS_IE_CONFIG_METHOD_LENGTH;

                    }
                }
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.statusOrAddIE = statusOrAddIE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    unInfoElem.WpsIEElements.isOptional = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u1WlanId = pWssWlanDB->WssWlanAttributeDB.u1WlanId;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u2MessageType = RADIO_INFO_ELEM_MSG_TYPE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u2MessageLength =
                    (UINT2) (RADIO_INFO_ELEM_FIXED_MSG_LEN + i4WpsIELength);
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    isPresent = OSIX_TRUE;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.u1EleId =
                    WPS_VENDOR_ELEMENT_ID;
                pWlcHdlrMsgStruct->WssWlanConfigReq.RadioIfInfoElement.
                    u1BPFlag = RadioIfGetDB.RadioIfGetAllDB.u1BPFlag;

                pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption =
                    WSSWLAN_UPDATE_REQ;
                /* Invoke WLC Handler to send the config update request for Information
                 *      * Element*/
                i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                                   pWlcHdlrMsgStruct);

                /*pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.pWpsIEElements.isPresent
                   = OSIX_TRUE;
                   pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.RadioIfInfoElem.isPresent
                   = OSIX_FALSE;
                   i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_RADIO_CONF_UPDATE_REQ,
                   pWlcHdlrMsgStruct);
                 */

                if (i4RetVal == OSIX_FAILURE)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetBssAdminStatus:WlcHdlr"
                                 " Call returned failed\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                    return OSIX_FAILURE;
                }
                u1UpdatedCount++;
            }
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}
#endif

/******************************************************************************
 * *                                                                           *
 * * Function     : WssSendUpdateWlanMsgfromRsna                               *
 * *                                                                           *
 * * Description  : This function will send the update wlan message.           *
 * *                when recieved a indication from RSNA                       *
 * *                                                                           *
 * * Input        : u4WlanIfIndex = WLAN Interface index                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 ******************************************************************************/
UINT4
WssSendUpdateWlanMsgfromRsna (UINT4 u4WlanIfIndex, UINT1 u1KeyType)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT4               u4RadioIfIndex = 0;
    INT4                i4RetVal = 1;
    UINT2               u2WlanProfileId = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2WlanInternalId = 0;
    UINT1               u1RadioId = 0;
    UINT1               u1WlanRadioMappingCount = 0;
    UINT1               u1UpdatedCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendUpdateWlanMsgfromRsna:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendUpdateWlanMsgfromRsna:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssSendUpdateWlanMsgfromRsna:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));

    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaProcessAssocFrames: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    u2WlanProfileId = pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
    u2WlanInternalId = pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId;

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId = u2WlanInternalId;
    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetStationConfig : WssWLAN DB access failed\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }
    u1WlanRadioMappingCount =
        pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
    if (u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (u1UpdatedCount == u1WlanRadioMappingCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWlcHdlrMsgStruct->RadioIfConfigUpdateReq.u2WtpInternalId
                = u2WtpInternalId;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetStationConfig : WlcHdlr DB access\
                            failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB \
                                access failed. \r\n");
                    continue;
                }

                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB access\
                                failed. \r\n");
                    continue;
                }
                /* Let the WssSendUpdateWlanMsgfromRsna(), send a Update WLAN message to AP only
                 * if the WLAN Binding has been done, so that ADD WLAN is already invoked.
                 * This condition is made to ensure that ADD WLAN message is send to AP at fist,
                 * and followed by which UPDATE WLAN messages are called.
                 */

                /*Calling WlanDB to Know whether binding has done or not */

                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                u4RadioIfIndex = pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                    u2WlanProfileId;
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
                pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) != OSIX_SUCCESS)
                {
                    continue;
                }

                pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
                    WssWlanUpdateReq.u1KeyType = u1KeyType;
                if ((WssWlanConstructUpdateWlanMsg
                     (u4RadioIfIndex, u2WlanProfileId,
                      &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                        unWlanConfReq.WssWlanUpdateReq))) == OSIX_FAILURE)
                {
                    continue;
                }

                MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                          WssWlanUpdateReq),
                        &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                          unWlanConfReq.WssWlanUpdateReq),
                        sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                                WssWlanConfigReq.unWlanConfReq.
                                WssWlanUpdateReq));
                pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;

                pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption =
                    WSSWLAN_UPDATE_REQ;
                i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                                   pWlcHdlrMsgStruct);

                if (i4RetVal == OSIX_FAILURE)
                {
                    continue;
                }
                u1UpdatedCount++;
            }
        }
    }

    pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
        WssWlanUpdateReq.u1KeyType = 0;
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssUtilIsProfileIndexApBinded*
 * *                                                                           *
 * * Description  : This function is used by RSNA to check whether the WTP is  *
 * *                binded or not                                              *
 * *                                                                           *
 * * Input        : i4IfIndex = WLAN Interface index                           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_TRUE/OSIX_FALSE                                       *
 * *                                                                           *
 ******************************************************************************/

BOOL1
WssUtilIsProfileIndexApBinded (INT4 i4IfIndex, UINT4 *u4BssIfIndex,
                               UINT1 *pu1BssId)
{

    tWssWlanDB          WssWlanDB;
    INT4                i4RadioIfIndex = 0;
    INT4                u4WlanProfileId = 0;
    INT4                i4NextRadioIfIndex = 0;
    INT4                i4BssIntfIndex = 0;
    UINT4               u4NextWlanProfileId = 0;
    UINT4               u4ProfileId = 0;
    UINT1               au1ZeroMac[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    /* Gets the profile id by passing the ifindex */
    WssGetCapwapWlanId (i4IfIndex, &u4ProfileId);

    /* Iterates through the capwapDot11WlanBindTable until the wlanprofileid of the table
       matches with the given profile id */

    if (nmhGetFirstIndexCapwapDot11WlanBindTable (&i4NextRadioIfIndex,
                                                  &u4NextWlanProfileId) !=
        SNMP_SUCCESS)
    {
        return OSIX_FALSE;
    }

    do
    {
        i4RadioIfIndex = i4NextRadioIfIndex;
        u4WlanProfileId = u4NextWlanProfileId;

        if (u4NextWlanProfileId == u4ProfileId)
        {
            break;
        }

    }
    while (nmhGetNextIndexCapwapDot11WlanBindTable (i4RadioIfIndex,
                                                    &i4NextRadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4NextWlanProfileId) ==
           SNMP_SUCCESS);

    /* If a match is found , then use the profileid & the corresponding i4RadioIfIndex to get
       the BSS interface index from the capwapDot11WlanBindTable */

    if (nmhGetCapwapDot11WlanBindBssIfIndex (i4RadioIfIndex,
                                             u4WlanProfileId,
                                             &i4BssIntfIndex) == SNMP_SUCCESS)
    {

        WssWlanDB.WssWlanAttributeDB.u4BssIfIndex = (UINT4) i4BssIntfIndex;
        *u4BssIfIndex = (UINT4) i4BssIntfIndex;
        WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;

        /* Gets the BSSId from WssWlanDB by passing the BSS interface index */

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                      &WssWlanDB) != OSIX_SUCCESS)
        {
            return OSIX_FALSE;
        }

        MEMCPY (pu1BssId, WssWlanDB.WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

        /*Compares the BSSid with the zero Mac to identify whether the WTP is binded or not */

        if (MEMCMP
            (WssWlanDB.WssWlanAttributeDB.BssId, au1ZeroMac, MAC_ADDR_LEN) != 0)
        {
            return OSIX_TRUE;
        }
    }

    return OSIX_FALSE;

}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanGetInternalId                       *
 * *                                                                           *
 * * Description  : This function is used to get the free internal Id           *
 * *                                                                           *
 * * Input        : u2WlanProfileId - WlanProfileId                            *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS                                           *
 * *                                                                           *
 ******************************************************************************/
INT4
WssWlanGetInternalId (UINT2 u2WlanProfileId, UINT2 *pu2WlanInternalId)
{
    UINT2               u2Index = 0;

    while (u2Index < MAX_NUM_OF_SSID_PER_WLC)
    {
        if (gWssWlanFreeIndex[u2Index].u1Used == OSIX_FALSE)
        {
            *pu2WlanInternalId = u2Index + 1;
            gWssWlanFreeIndex[u2Index].u1Used = OSIX_TRUE;
            gWssWlanFreeIndex[u2Index].u2WlanProfileId = u2WlanProfileId;
            break;
        }
        u2Index++;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanFreeInternalId                                      *
 * *                                                                           *
 * * Description  : This function is used to reset the Wlan Internal Id        *
 * *                                                                           *
 * * Input        : u2WlanInternalId - WlanInternalId                          *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS                                               *
 * *                                                                           *
 ******************************************************************************/

INT4
WssWlanFreeInternalId (UINT2 u2WlanInternalId)
{
    gWssWlanFreeIndex[u2WlanInternalId - 1].u1Used = OSIX_FALSE;
    gWssWlanFreeIndex[u2WlanInternalId - 1].u2WlanProfileId = 0;

    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
/************************************************************************/
/*  Function Name   : CapwapUpdateKernelIntfDB                          */
/*  Description     : The function updates the kernel DB                */
/*  Input(s)        : bssid -  Bssid                                    */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
INT4
CapwapUpdateKernelIntfDB (tMacAddr bssid, UINT2 u2VlanId, UINT1 u1SsidIsolation)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));

    MEMCPY (WssKernelDB.rbData.unTable.apIntData.bssid, bssid,
            sizeof (tMacAddr));
    WssKernelDB.rbData.unTable.apIntData.u2VlanId = u2VlanId;
    WssKernelDB.rbData.unTable.apIntData.u1SsidIsolation = u1SsidIsolation;

    u1Module = TX_MODULE;
    u2Field = AP_INT_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "Failed from Kernel NPAPI  \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : CapwapRemoveKernelIntfDB                          */
/*  Description     : The function updates the kernel DB                */
/*  Input(s)        : bssid -  Bssid                                    */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
INT4
CapwapRemoveKernelIntfDB (tMacAddr bssid)
{
    UINT1               u1Module;
    UINT2               u2Field;
    tHandleRbWlc        WssKernelDB;

    MEMSET (&WssKernelDB, 0, sizeof (tHandleRbWlc));

    MEMCPY (WssKernelDB.rbData.unTable.apIntData.bssid, bssid,
            sizeof (tMacAddr));

    u1Module = TX_MODULE;
    u2Field = AP_INT_TABLE;
    if (capwapNpRemoveKernelDBWlc (u1Module, u2Field, &WssKernelDB) ==
        OSIX_FAILURE)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "Failed from Kernel NPAPI  \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSetFsDot11ExternalWebAuthProfileTable               *
 * *                                                                           *
 * * Description  : This function will update the WLAN DB with the values      *
 * *                received.                                                  *
 * *                                                                           *
 * * Input        : pWssWlanMsgDB -  Pointer to the input structure            *
 * *                                                                           *
 * * Output       : None                                                       *                 
 * *                                                                           *     
 * * Returns      : OSIX_SUCCESS, if succeeds                                  *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 ******************************************************************************/

UINT1
WssWlanSetFsDot11ExternalWebAuthProfileTable (tWssWlanDB * pWssWlanMsgDB)
{
    WSSWLAN_FN_ENTRY ();
    if (pWssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetFsDot11ExternalWebAuthProfileTable : \
                Null Input received \r\n");
        return OSIX_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetFsDot11ExternalWebAuthProfileTable:WssWLAN DB access\
                failed\r\n");

        return OSIX_FAILURE;
    }

    WSSWLAN_FN_EXIT ();

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssConstructDiffServVendorMessage                 */
/*  Description     : The function send the Diff Serv values for each   */
/*                    interface                                         */
/*  Input(s)        : BSSID, u2Wlan Profile Id                          */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
INT4
WssConstructDiffServVendorMessage (UINT4 u4RadioIfIndex,
                                   UINT2 u2WlanProfileId,
                                   unWlcHdlrMsgStruct * pWlcHdlrMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapDscpMapTable CapDscpMapEntry;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapDscpMapEntry, 0, sizeof (tCapwapDscpMapTable));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssConstructWlanInterfaceVendorMessage:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: BssInterface DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanDiffServId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: Bss IfIndex DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Invoke Diff Serv DB */
    CapDscpMapEntry.u4IfIndex = u4RadioIfIndex;
    CapDscpMapEntry.u2WlanProfileId = u2WlanProfileId;

    if (WssIfCapwapDscpMapEntryGet (&CapDscpMapEntry) != OSIX_SUCCESS)
    {
    }

    if (CapDscpMapEntry.i4RowStatus == ACTIVE)
    {
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].elementId =
            VENDOR_DIFF_SERV_TYPE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u4VendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u2MsgEleType = VENDOR_DIFF_SERV_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u2MsgEleLen = 8;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u1InPriority = CapDscpMapEntry.u1InPriority;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u1OutDscp = CapDscpMapEntry.u1OutDscp;

        if (u2WlanProfileId == 0)
        {
            pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                VendorSpecDiffServ.u1RadioId = 0;
        }
        else
        {
            pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
                VendorSpecDiffServ.u1RadioId =
                RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
        }
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u1EntryStatus = ACTIVE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].u2MsgEleLen =
            pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[4].unVendorSpec.
            VendorSpecDiffServ.u2MsgEleLen + 4;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssConstructWlanInterfaceVendorMessage            */
/*  Description     : The function sends the WLAN interface IP address  */
/*                    and interface info to AP                          */
/*  Input(s)        : BSSID, u2Wlan Profile Id                          */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

INT4
WssConstructWlanInterfaceVendorMessage (UINT4 u4RadioIfIndex,
                                        UINT2 u2WlanProfileId,
                                        unWlcHdlrMsgStruct * pWlcHdlrMsgStruct)
{
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssConstructWlanInterfaceVendorMessage:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = u2WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = u4RadioIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: BssInterface DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: Bss IfIndex DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB->WssWlanIsPresentDB.bWlanMacType = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanTunnelMode = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC
            (WSSWLAN_FAILURE_TRC,
             "WssWlanConstructAddWlanMsg: WlanProfile DB Get failed,"
             "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanConstructAddWlanMsg: WlanIfIndex DB Get failed"
                     "return failure \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1RowStatus == ACTIVE)
    {
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].u2MsgEleLen =
            VEND_WLAN_INTERFACE_IP_LEN + VEND_HEADER_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].elementId =
            VENDOR_WLAN_INTERFACE_IP;

        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.u4vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.u2MsgEleType = WLAN_INTERFACE_IP;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.u2MsgEleLen = VEND_WLAN_INTERFACE_IP_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.u4IpAddr =
            pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4IpAddr;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.u4SubMask =
            pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4SubMask;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.u1AdminStatus =
            pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1AdminStatus;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[3].unVendorSpec.
            VendorWlanInterfaceIp.isOptional = OSIX_TRUE;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssWlanUpdateEdcaParams                */
/*  Description     : The function sends the UPDATE WLAN MSG to AP    */
/*  Input(s)        : u4RadioIfIndex                    */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT1
WssWlanUpdateEdcaParams (UINT4 u4RadioIfIndex, UINT1 u1QosIndex)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanDB         *pwssWlanMsgDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2WlanInternalId = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               u1WlanId = 0;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanMsgDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanUpdateEdcaParams:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanUpdateEdcaParams:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanUpdateEdcaParams:- "
                     "UtlShMemAllocWssWlanBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    MEMSET (pwssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    WSSWLAN_FN_ENTRY ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    for (u1WlanId = 1; u1WlanId < 16; u1WlanId++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
        RadioIfGetDB.RadioIfIsGetAllDB.bBssIfIndex = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            continue;
        }

        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      (&RadioIfGetDB)) != OSIX_SUCCESS)
        {
            continue;
        }

        pwssWlanMsgDB->WssWlanAttributeDB.u4BssIfIndex =
            RadioIfGetDB.RadioIfGetAllDB.u4BssIfIndex;
        pwssWlanMsgDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pwssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pwssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanMsgDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        pwssWlanMsgDB->WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanMsgDB)
            != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
            return OSIX_FAILURE;
        }
        if (pwssWlanMsgDB->WssWlanAttributeDB.u1QosTraffic != u1QosIndex)
        {
            continue;
        }
        u2WlanInternalId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanInternalId;
        u2WlanProfileId = pwssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;

        /* Construct WlanUpdateReq message */
        if (WssWlanConstructUpdateWlanMsg (u4RadioIfIndex,
                                           u2WlanProfileId,
                                           &(pWssWlanMsgStruct->
                                             unWssWlanMsg.
                                             WssWlanConfigReq.
                                             unWlanConfReq.
                                             WssWlanUpdateReq)) != OSIX_SUCCESS)
        {
            continue;
        }
        MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
                  WssWlanUpdateReq),
                &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
                  unWlanConfReq.WssWlanUpdateReq),
                sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                        WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq));
        pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId = u2WtpInternalId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            u2MsgEleType = VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            u2MsgEleLen = VEND_SSID_ISOLATION_LEN + VEND_HEADER_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].elementId =
            VENDOR_SSID_ISOLATION_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.u2MsgEleType =
            SSID_ISOLATION_VENDOR_MSG;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.u2MsgEleLen =
            VEND_SSID_ISOLATION_LEN;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.vendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.u1Val =
            pwssWlanMsgDB->WssWlanAttributeDB.u1SsidIsolation;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.u1RadioId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanUpdateReq.u1RadioId;
        pWlcHdlrMsgStruct->WssWlanConfigReq.vendSpec[0].
            unVendorSpec.VendSsidIsolation.u1WlanId =
            pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
            unWlanConfReq.WssWlanUpdateReq.u1WlanId;

        pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption = WSSWLAN_UPDATE_REQ;
        if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                    pWlcHdlrMsgStruct) == OSIX_FAILURE)
        {
            continue;
        }
    }

    UNUSED_PARAM (u2WlanInternalId);
    WSSWLAN_FN_EXIT ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanMsgDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

#ifdef BAND_SELECT_WANTED
/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSendBandSelectStatus                                *
 * *                                                                           *
 * * Description  : This function is used to get the free internal Id           *
 * *                                                                           *
 * * Input        : u2WlanProfileId - WlanProfileId                            *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS / OSIX_FAILURE                                   *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSendBandSelectStatus (UINT4 u4RadioIfIndex, UINT4 u4BssIfIndex,
                             UINT1 u1WlanId, UINT2 u2WtpInternalId,
                             UINT1 u1RadioId)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    UINT1               u1BandSelectGlobStatus = 0;

    UNUSED_PARAM (u4RadioIfIndex);

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSendBandSelectStatus:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetManagmentSSID:- "
                     "UtlShMemAllocWlcBuf returned failure\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    if (WssIfGetBandSelectStatus (&u1BandSelectGlobStatus) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSendBandSelectStatus: BssInterface DB Get failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSendBandSelectStatus: WLAN Index DB Get failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    if (pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus == OSIX_FALSE)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
        VENDOR_BANDSELECT_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId = u2WtpInternalId;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
        WSSWLAN_VEND_BANDSELECT_LEN + 4;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u2MsgEleType = VENDOR_BANDSELECT_MSG;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u2MsgEleLen = WSSWLAN_VEND_BANDSELECT_LEN;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u1RadioId = u1RadioId;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u1WlanId = u1WlanId;

    if (u1BandSelectGlobStatus == 1)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorBandSelect.u1BandSelectGlobalStatus = 1;
    }
    else
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorBandSelect.u1BandSelectGlobalStatus = 0;
    }

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u1BandSelectStatus =
        pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus;

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        VendorBandSelect.u1AgeOutSuppression =
        pWssWlanDB->WssWlanAttributeDB.u1AgeOutSuppression;

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                pWlcHdlrMsgStruct) == OSIX_FAILURE)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssIfProcessWlcHdlrMsg WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ\
                failed\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * *                                                                           *
 * * Function     : WssWlanSendBandSelectStatusAll                             *
 * *                                                                           *
 * * Description  : This function is used to get the free internal Id           *
 * *                                                                           *
 * * Input        : u2WlanProfileId - WlanProfileId                            *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS / OSIX_FAILURE                                   *
 * *                                                                           *
 ******************************************************************************/
UINT1
WssWlanSendBandSelectStatusAll (UINT1 u1BsGlobalStatus)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    INT4                i4WlanIfIndex = 0;
    UINT4               u4NextWlanProfileId = 0;
    UINT4               u4FirstWlanProfileId = 0;

    UNUSED_PARAM (u1BsGlobalStatus);
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pWssWlanDB != NULL)
    {
        MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

        if (nmhGetFirstIndexFsWlanBandSelectTable (&u4FirstWlanProfileId) !=
            SNMP_FAILURE)
        {
            do
            {
                u4NextWlanProfileId = u4FirstWlanProfileId;
                nmhGetCapwapDot11WlanProfileIfIndex
                    (u4NextWlanProfileId, &i4WlanIfIndex);

                MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                    (UINT4) i4WlanIfIndex;
                pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              pWssWlanDB) == OSIX_FAILURE)
                {
                    continue;
                }

                if (WlanSetBandSteerParams (pWssWlanDB) == OSIX_FAILURE)
                {
                    continue;
                }
            }
            while (nmhGetNextIndexFsWlanBandSelectTable (u4NextWlanProfileId,
                                                         &u4FirstWlanProfileId));
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : WlanSetBandSteerParams                               *
 *                                                                           *
 * Description        : This function constructs the config update request   * 
 *                      message with band steering and send to WLC Handler   *
 *                      which in turn update the WSSIF DB in case the        *
 *                      configuration is succesfully applied in WTP          *
 *                                                                           *
 * Input(s)           : pWssWlanDB - Pointer to WssWlan DB with ifIndex      *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
WlanSetBandSteerParams (tWssWlanDB * pWssWlanDB)
{
    tWssWlanDB          WssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1RadioId = 0;
    UINT1               u2WtpInternalId = 0;
    UINT1               u1UpdatedCount = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (pWssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WlanSetBandSteerParams:- \
                      Configured value is NULL\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
    WssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bBandSelectStatus = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bAgeOutSuppression = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB))
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC, "WlanSetBandSteerParams:- \
                      WlanDb failed to fetch band steering values\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    WssWlanDB.WssWlanIsPresentDB.bShortPreamble = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WssWlanSetStationConfig : WssWLAN DB access failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (WssWlanDB.WssWlanAttributeDB.u1WlanRadioMappingCount != 0)
    {
        for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
             u2WtpInternalId++)
        {
            if (WssWlanDB.WssWlanAttributeDB.u1WlanRadioMappingCount
                == u1UpdatedCount)
            {
                break;
            }
            /* Fill WLC Handler structure to get no of radios */
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssWlanSetStationConfig : WlcHdlr DB access\
            failed. \r\n");
                continue;
            }
            for (u1RadioId = 1; u1RadioId <=
                 pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
            {
                pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;

                pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB \
                access failed. \r\n");
                    continue;
                }
                RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                    UtlShMemAllocAntennaSelectionBuf ();

                if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "\r%% Memory allocation for Antenna"
                                 " selection failed.\r\n");
                    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                    return OSIX_FAILURE;
                }
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                RadioIfGetDB.RadioIfIsGetAllDB.bDTIMPeriod = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssWlanSetStationConfig : WlcHdlr DB access\
                failed. \r\n");
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }
                WssWlanDB.WssWlanAttributeDB.u4RadioIfIndex =
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                WssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                WssWlanDB.WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                              &WssWlanDB) != OSIX_SUCCESS)
                {
                    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                     RadioIfGetAllDB.
                                                     pu1AntennaSelection);
                    continue;
                }

                WssWlanSendBandSelectStatus (RadioIfGetDB.RadioIfGetAllDB.
                                             u4RadioIfIndex,
                                             WssWlanDB.WssWlanAttributeDB.
                                             u4BssIfIndex,
                                             WssWlanDB.WssWlanAttributeDB.
                                             u1WlanId,
                                             pWssIfCapwapDB->CapwapGetDB.
                                             u2WtpInternalId,
                                             pWssIfCapwapDB->CapwapGetDB.
                                             u1RadioId);

                u1UpdatedCount++;
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                                 RadioIfGetAllDB.
                                                 pu1AntennaSelection);
            }
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}
#endif
#ifdef WPA_WANTED
/****************************************************************************
 Function    :  WssWlanTestRadioType
 Input       :  The Indices
                IfIndex

 Description :  Ensures that WPA is not configured for BGN,AN or AC Type.  
                
 Returns     :  RSNA_SUCCESS or RSNA_FAILURE
****************************************************************************/
INT1
WssWlanTestRadioType (INT4 i4IfIndex)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioCount = 0;
    UINT1               u1RadioId = 0;
    tWssWlanDB         *pwssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2WtpInternalId = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WpaTesRadioType:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4IfIndex;
    pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WpaTesRadioType: WssWLAN DB access failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;

    }
    pwssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY,
                                  pwssWlanDB) != OSIX_SUCCESS)
    {
        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                     "WpaTesRadioType: WssWLAN DB access failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;

    }

    if (pwssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount != 0)
    {
        for (u4RadioCount =
             pwssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
             u4RadioCount > 0; u4RadioCount--)
        {
            for (u2WtpInternalId = 0; u2WtpInternalId < NUM_OF_AP_SUPPORTED;
                 u2WtpInternalId++)
            {
                /* Fill WLC Handler structure to get no of radios */
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio = OSIX_TRUE;

                if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                             pWssIfCapwapDB) != OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WpaTesRadioType: WlcHdlr DB access\
                            failed. \r\n");
                    continue;
                }
                for (u1RadioId = 1; u1RadioId <=
                     pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio; u1RadioId++)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1RadioId = u1RadioId;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB = OSIX_TRUE;
                    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                                 pWssIfCapwapDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WpaTesRadioType: WlcHdlr DB \
                                access failed. \r\n");
                        continue;
                    }
                    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                        pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bNoOfBssId = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bMacAddr = OSIX_TRUE;
                    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
                    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                                  &RadioIfGetDB) !=
                        OSIX_SUCCESS)
                    {
                        WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                     "WpaTesRadioType: WlcHdlr DB access\
                                failed. \r\n");
                        continue;
                    }

                    pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex;
                    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                                  pwssWlanDB) == OSIX_SUCCESS)
                    {
                        if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_BGN
                            || RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_AN
                            || RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                            RADIO_TYPE_AC)
                        {
                            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                            return OSIX_FAILURE;
                        }
                    }
                }
            }
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

#endif

#endif
