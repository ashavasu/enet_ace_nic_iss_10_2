#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   $Id: make.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                  |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

FCAP_INC              = ${BASE_DIR}/wss/fcapwap/inc
WSSWLAN_BASE_DIR = ${BASE_DIR}/wss/wsswlan
WSSWLAN_INC_DIR  = ${WSSWLAN_BASE_DIR}/inc
WSSWLAN_SRC_DIR  = ${WSSWLAN_BASE_DIR}/src
WSSWLAN_OBJ_DIR  = ${WSSWLAN_BASE_DIR}/obj
WSSIF_INC_DIR  = ${BASE_DIR}/wss/wssif/inc
RADIOIF_INC_DIR  = ${BASE_DIR}/wss/radioif/inc
WSSMAC_INC_DIR  = ${BASE_DIR}/wss/wssmac/inc
BCNMGR_INC_DIR  = ${BASE_DIR}/wss/bcnmgr/inc
APHDLR_INC_DIR = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR = ${BASE_DIR}/wss/wlchdlr/inc
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
WSSAUTH_INC_DIR = ${BASE_DIR}/wss/wssauth/inc
WSSSTA_INC_DIR = ${BASE_DIR}/wss/wsssta/inc
CFA_INC_DIR = ${BASE_DIR}/cfa2/inc
WSSPM_INC_DIR = ${BASE_DIR}/wss/wsspm/inc
WSSCFG_INC_DIR = ${BASE_DIR}/wss/wsscfg/inc
RMMGMT_INC_DIR = ${BASE_DIR}/wss/rfmgmt/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${CFA_INC_DIR}  -I${WSSWLAN_INC_DIR} -I${WSSIF_INC_DIR} -I${RADIOIF_INC_DIR} -I${BCNMGR_INC_DIR} -I${APHDLR_INC_DIR} -I${WLCHDLR_INC_DIR} -I${CAPWAP_INC_DIR} -I${WSSMAC_INC_DIR} -I${WSSAUTH_INC_DIR} -I${WSSSTA_INC_DIR} -I${WSSPM_INC_DIR} -I${WSSCFG_INC_DIR} -I${RMMGMT_INC_DIR} -I${FCAP_INC}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
