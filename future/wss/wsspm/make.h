# $Id: make.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

WSSPM_BASE_DIR = ${BASE_DIR}/wss/wsspm
WSSPM_INC_DIR  = ${WSSPM_BASE_DIR}/inc
WSSIF_INC_DIR  = ${BASE_DIR}/wss/wssif/inc
CAPWAP_INC_DIR  = ${BASE_DIR}/wss/capwap/inc
WSSPM_SRC_DIR  = ${WSSPM_BASE_DIR}/src
APHDLR_INC_DIR      = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR    = ${BASE_DIR}/wss/wlchdlr/inc
DTLS_INC_DIR       = ${BASE_DIR}/wss/dtls/inc
SNMP_INCL_DIR      = ${BASE_DIR}/inc/snmp
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
WSSUSER_INC_DIR = ${BASE_DIR}/wss/wssuser/inc

WSSPM_OBJ_DIR  = ${WSSPM_BASE_DIR}/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${WSSPM_INC_DIR} -I${WSSIF_INC_DIR} -I${CAPWAP_INC_DIR} -I${DTLS_INC_DIR} -I${CMN_INC_DIR} -I${WSSUSER_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
