/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmdb.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the WSSPM API related functions
 *
 *****************************************************************************/

#ifndef __WSSPM_RBT_C__
#define __WSSPM_RBT_C__

#include "wsspminc.h"
#include "wssifpmdb.h"
#ifdef WSSSTA_WANTED
#include "wsssta.h"
#endif

tRBTree             gWssPmWTPIDStatsProcessDB;
tRBTree             gWssPmWTPIDRadIDStatsProcessDB;
tRBTree             gWssPmWLANBSSIDStatsProcessDB;
tRBTree             gWssPmWlanSSIDStatsProcessDB;
tRBTree             gWssPmWlanClientStatsProcessDB;
tRBTree             gWssPmWlanRadioStatsProcessDB;
tRBTree             gWssPmCapwATPStatsProcessDB;
tRBTree             gWssPmWTPRunEvents;
/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssPmRBTreeInit                                            *
 *                                                                           *
 * DESCRIPTION  : Initialization with memory allocation required for         *
 *                WssPm module                                               *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmRBTreeInit (VOID)
{
    WSSPM_FN_ENTRY ();
    if (WsspmSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    gWssPmWTPIDStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWTPIDStatsProcessRBDB,
                                              nextWssPmWTPIDStatsProcessNode)),
                              WssPmWTPIDCompareStatsProcessDBRBTree);
    if (gWssPmWTPIDStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssPmWTPIDRadIDStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWTPIDRadIDStatsProcessRBDB,
                                              nextWssPmWTPIDRadIDStatsProcessNode)),
                              WssPmWTPIDRadIDCompareStatsProcessDBRBTree);
    if (gWssPmWTPIDRadIDStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssPmWLANBSSIDStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWLANBSSIDStatsProcessRBDB,
                                              nextWssPmWLANBSSIDStatsProcessNode)),
                              WssPmWLANBSSIDCompareStatsProcessDBRBTree);
    if (gWssPmWLANBSSIDStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssPmWlanSSIDStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWlanSSIDStatsProcessRBDB,
                                              nextWssPmWlanSSIDStatsProcessNode)),
                              WssPmWlanSSIDCompareStatsProcessDBRBTree);
    if (gWssPmWlanSSIDStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssPmWlanClientStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWlanClientStatsProcessRBDB,
                                              nextWssPmWlanClientStatsProcessNode)),
                              WssPmWlanClientCompareStatsProcessDBRBTree);
    if (gWssPmWlanClientStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssPmCapwATPStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmCapwATPStatsProcessRBDB,
                                              nextWssPmCapwATPStatsProcessNode)),
                              WssPmWlanCapwATPCompareStatsProcessDBRBTree);
    if (gWssPmCapwATPStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }


    gWssPmWTPRunEvents =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWTPRunEvents,
                                              nextWssPmWTPRunEventsNode)),
                              WssPmWTPCompareRunEventsRBTree);
    if (gWssPmWTPRunEvents == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }



    gWssPmWlanRadioStatsProcessDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssPmWlanRadioStatsProcessRBDB,
                                              nextWssPmWlanRadioStatsProcessNode)),
                              WssPmWlanRadioCompareStatsProcessDBRBTree);
    if (gWssPmWlanRadioStatsProcessDB == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWTPIDCompareStatsProcessDBRBTree                     *
 *                                                                           *
 * DESCRIPTION  : This function is used during the adding of a new wtp       *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWTPIDCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWTPIDStatsProcessRBDB *pNode1 = e1;
    tWssPmWTPIDStatsProcessRBDB *pNode2 = e2;
    WSSPM_FN_ENTRY ();
    if (pNode1->u2WtpIntProfileIndex > pNode2->u2WtpIntProfileIndex)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->u2WtpIntProfileIndex < pNode2->u2WtpIntProfileIndex)
    {
        return WSSPM_RB_LESS;
    }
    WSSPM_FN_EXIT ();
    return WSSPM_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssPmWTPIDRadIDCompareStatsProcessDBRBTree                 *
 *                                                                           *
 * DESCRIPTION  : This function is used during the adding of a new wtp       *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWTPIDRadIDCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWTPIDRadIDStatsProcessRBDB *pNode1 = e1;
    tWssPmWTPIDRadIDStatsProcessRBDB *pNode2 = e2;
    WSSPM_FN_ENTRY ();
    if (pNode1->u4WtpRadioIndex > pNode2->u4WtpRadioIndex)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->u4WtpRadioIndex < pNode2->u4WtpRadioIndex)
    {
        return WSSPM_RB_LESS;
    }
    WSSPM_FN_EXIT ();
    return WSSPM_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWLANBSSIDCompareStatsProcessDBRBTree                 *
 *                                                                           *
 * DESCRIPTION   : This function is used during the adding of a new wtp      *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWLANBSSIDCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWLANBSSIDStatsProcessRBDB *pNode1 = e1;
    tWssPmWLANBSSIDStatsProcessRBDB *pNode2 = e2;
    if (pNode1->u4WlanBSSIndex > pNode2->u4WlanBSSIndex)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->u4WlanBSSIndex < pNode2->u4WlanBSSIndex)
    {
        return WSSPM_RB_LESS;
    }
    return WSSPM_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWlanSSIDCompareStatsProcessDBRBTree                 *
 *                                                                           *
 * DESCRIPTION   : This function is used during the adding of a new wtp      *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWlanSSIDCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWlanSSIDStatsProcessRBDB *pNode1 = e1;
    tWssPmWlanSSIDStatsProcessRBDB *pNode2 = e2;
    if (pNode1->u4FsDot11WlanProfileId > pNode2->u4FsDot11WlanProfileId)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->u4FsDot11WlanProfileId < pNode2->u4FsDot11WlanProfileId)
    {
        return WSSPM_RB_LESS;
    }
    return WSSPM_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWlanClientCompareStatsProcessDBRBTree                 *
 *                                                                           *
 * DESCRIPTION   : This function is used during the adding of a new wtp      *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWlanClientCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWlanClientStatsProcessRBDB *pNode1 = e1;
    tWssPmWlanClientStatsProcessRBDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal =
        MEMCMP (pNode1->FsWlanClientStatsMACAddress,
                pNode2->FsWlanClientStatsMACAddress, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return WSSPM_RB_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return WSSPM_RB_LESS;
    }
    return WSSPM_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWlanRadioCompareStatsProcessDBRBTree                 *
 *                                                                           *
 * DESCRIPTION   : This function is used during the adding of a new wtp      *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWlanRadioCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWlanRadioStatsProcessRBDB *pNode1 = e1;
    tWssPmWlanRadioStatsProcessRBDB *pNode2 = e2;
    if (pNode1->i4IfIndex > pNode2->i4IfIndex)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->i4IfIndex < pNode2->i4IfIndex)
    {
        return WSSPM_RB_LESS;
    }
    return WSSPM_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWlanCapwATPCompareStatsProcessDBRBTree                 *
 *                                                                           *
 * DESCRIPTION   : This function is used during the adding of a new wtp      *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT         : None                                                      *
 *                                                                           *
 * OUTPUT        : None                                                      *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWlanCapwATPCompareStatsProcessDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmCapwATPStatsProcessRBDB *pNode1 = e1;
    tWssPmCapwATPStatsProcessRBDB *pNode2 = e2;
    if (pNode1->u4CapwapBaseWtpProfileId > pNode2->u4CapwapBaseWtpProfileId)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->u4CapwapBaseWtpProfileId <
             pNode2->u4CapwapBaseWtpProfileId)
    {
        return WSSPM_RB_LESS;
    }
    return WSSPM_RB_EQUAL;
}
/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME : WssPmWTPCompareRunEventsRBTree                            *
 *                                                                           *
 * DESCRIPTION   : This function is used during the adding of a new wtp      *
 *                profile id to the Rb tree                                  *
 *                                                                           *
 * INPUT         : None                                                      *
 *                                                                           *
 * OUTPUT        : None                                                      *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWTPCompareRunEventsRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssPmWTPRunEvents *pNode1 = e1;
    tWssPmWTPRunEvents *pNode2 = e2;
    if (pNode1->u4CapwapWtpProfileId > pNode2->u4CapwapWtpProfileId)
    {
        return WSSPM_RB_GREATER;
    }
    else if (pNode1->u4CapwapWtpProfileId <
             pNode2->u4CapwapWtpProfileId)
    {
        return WSSPM_RB_LESS;
    }
    return WSSPM_RB_EQUAL;
}


/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWtpIDStatsEntryCreate                                 *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWtpIDStatsEntryCreate (UINT2 u2WtpIntProfileIndex)
{
    /* WTP ID Table Stats */
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;

    WSSPM_FN_ENTRY ();
    /* Add entry to the RB tree */
    pWtpIdStatsMap =
        (tWssPmWTPIDStatsProcessRBDB *)
        MemAllocMemBlk (PM_FSWTPIDSTATSTABLE_POOL);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWtpIdStatsMap is NULL returning FAILURE \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWtpIdStatsMap, 0, sizeof (tWssPmWTPIDStatsProcessRBDB));
    pWtpIdStatsMap->u2WtpIntProfileIndex = u2WtpIntProfileIndex;
    if (RBTreeAdd (gWssPmWTPIDStatsProcessDB, pWtpIdStatsMap) != RB_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Unable to add entry DB(ProfileID)\r\n");
        MemReleaseMemBlock (PM_FSWTPIDSTATSTABLE_POOL,
                            (UINT1 *) pWtpIdStatsMap);
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWtpIDRadIDStatsEntryCreate                            *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWtpIDRadIDStatsEntryCreate (UINT2 u4WtpRadioIndex)
{
    /* WTP ID Rad ID Stats */
    tWssPmWTPIDRadIDStatsProcessRBDB *pwtpIDRadIDStatsMap = NULL;

    WSSPM_FN_ENTRY ();
    /* Add entry to the RB tree */
    pwtpIDRadIDStatsMap =
        (tWssPmWTPIDRadIDStatsProcessRBDB *) MemAllocMemBlk
        (PM_FSWTPIDRADIDSTATSTABLE_POOL);
    if (pwtpIDRadIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Mem Alloc for pwtpIDRadIDStatsMap\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pwtpIDRadIDStatsMap, 0, sizeof (tWssPmWTPIDRadIDStatsProcessRBDB));
    pwtpIDRadIDStatsMap->u4WtpRadioIndex = u4WtpRadioIndex;

    if (RBTreeAdd (gWssPmWTPIDRadIDStatsProcessDB,
                   pwtpIDRadIDStatsMap) != RB_SUCCESS)
    {
        MemReleaseMemBlock (PM_FSWTPIDRADIDSTATSTABLE_POOL,
                            (UINT1 *) pwtpIDRadIDStatsMap);
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(prof_id,radioindex)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWlanBSSIDStatsEntryCreate                             *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanBSSIDStatsEntryCreate (UINT4 u4WlanBSSIndex)
{
    tWssPmWLANBSSIDStatsProcessRBDB *pWlanBSSIDStatsMap = NULL;

    WSSPM_FN_ENTRY ();
    /* Add entry to the RB tree */
    pWlanBSSIDStatsMap =
        (tWssPmWLANBSSIDStatsProcessRBDB *)
        MemAllocMemBlk (PM_FSWTPIDBSSIDSTATSTABLE_POOL);
    if (pWlanBSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Mem Alloc for pWlanBSSIDStatsMap \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlanBSSIDStatsMap, 0, sizeof (tWssPmWLANBSSIDStatsProcessRBDB));

    pWlanBSSIDStatsMap->u4WlanBSSIndex = u4WlanBSSIndex;

    if (RBTreeAdd (gWssPmWLANBSSIDStatsProcessDB,
                   pWlanBSSIDStatsMap) != RB_SUCCESS)
    {
        MemReleaseMemBlock (PM_FSWTPIDBSSIDSTATSTABLE_POOL,
                            (UINT1 *) pWlanBSSIDStatsMap);
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(BSS ID)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWtpIDStatsEntryGet                                    *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWTPIDStatsProcessRBDB *
WssIfWtpIDStatsEntryGet (UINT2 u2WtpIntProfileIndex)
{
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    tWssPmWTPIDStatsProcessRBDB wtpIdStatsProcessRBDB;

    WSSPM_FN_ENTRY ();
    MEMSET (&wtpIdStatsProcessRBDB, 0, sizeof (tWssPmWTPIDStatsProcessRBDB));
    wtpIdStatsProcessRBDB.u2WtpIntProfileIndex = u2WtpIntProfileIndex;

    pWtpIdStatsMap = ((tWssPmWTPIDStatsProcessRBDB *)
                      RBTreeGet (gWssPmWTPIDStatsProcessDB,
                                 (tRBElem *) & wtpIdStatsProcessRBDB));
    WSSPM_FN_EXIT ();
    return pWtpIdStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWtpIDRadIDStatsEntryGet                          *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWTPIDRadIDStatsProcessRBDB *
WssIfWtpIDRadIDStatsEntryGet (UINT2 u2WtpRadioIndex)
{
    tWssPmWTPIDRadIDStatsProcessRBDB *pwtpIDRadIDStatsMap = NULL;
    tWssPmWTPIDRadIDStatsProcessRBDB wtpIDRadIDStatsProcessRBDB;
    WSSPM_FN_ENTRY ();
    MEMSET (&wtpIDRadIDStatsProcessRBDB, 0,
            sizeof (tWssPmWTPIDRadIDStatsProcessRBDB));

    wtpIDRadIDStatsProcessRBDB.u4WtpRadioIndex = u2WtpRadioIndex;

    pwtpIDRadIDStatsMap = ((tWssPmWTPIDRadIDStatsProcessRBDB *)
                           RBTreeGet (gWssPmWTPIDRadIDStatsProcessDB,
                                      (tRBElem *) &
                                      wtpIDRadIDStatsProcessRBDB));
    if (pwtpIDRadIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Radio ID)\r\n");
        return NULL;
    }
    WSSPM_FN_EXIT ();
    return pwtpIDRadIDStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWLANBSSIDStatsEntryGet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWLANBSSIDStatsProcessRBDB *
WssIfWLANBSSIDStatsEntryGet (UINT2 u4WlanBSSIndex)
{
    tWssPmWLANBSSIDStatsProcessRBDB *pWlanBSSIDStatsMap = NULL;
    tWssPmWLANBSSIDStatsProcessRBDB wlanBSSIDStatsProcessRBDB;

    MEMSET (&wlanBSSIDStatsProcessRBDB, 0,
            sizeof (tWssPmWLANBSSIDStatsProcessRBDB));
    WSSPM_FN_ENTRY ();
    wlanBSSIDStatsProcessRBDB.u4WlanBSSIndex = u4WlanBSSIndex;

    pWlanBSSIDStatsMap = ((tWssPmWLANBSSIDStatsProcessRBDB *)
                          RBTreeGet (gWssPmWLANBSSIDStatsProcessDB,
                                     (tRBElem *) & wlanBSSIDStatsProcessRBDB));
    if (pWlanBSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (BSS ID)\r\n");
        return NULL;
    }
    WSSPM_FN_EXIT ();
    return pWlanBSSIDStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWLANBSSIDStatsEntryGet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWlanSSIDStatsProcessRBDB *
WssIfWlanSSIDStatsEntryGet (UINT4 u4FsDot11WlanProfileId)
{
    tWssPmWlanSSIDStatsProcessRBDB *pWlanSSIDStatsMap = NULL;
    tWssPmWlanSSIDStatsProcessRBDB wlanSSIDStatsProcessRBDB;

    MEMSET (&wlanSSIDStatsProcessRBDB, 0,
            sizeof (tWssPmWlanSSIDStatsProcessRBDB));
    WSSPM_FN_ENTRY ();
    wlanSSIDStatsProcessRBDB.u4FsDot11WlanProfileId = u4FsDot11WlanProfileId;

    pWlanSSIDStatsMap = ((tWssPmWlanSSIDStatsProcessRBDB *)
                         RBTreeGet (gWssPmWlanSSIDStatsProcessDB,
                                    (tRBElem *) & wlanSSIDStatsProcessRBDB));
    if (pWlanSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (SSID)\r\n");
        return NULL;
    }
    WSSPM_FN_EXIT ();
    return pWlanSSIDStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanRadioStatsEntryGet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWlanRadioStatsProcessRBDB *
WssIfWlanRadioStatsEntryGet (INT4 i4IfIndex)
{
    tWssPmWlanRadioStatsProcessRBDB *pWlanRadioStatsMap = NULL;
    tWssPmWlanRadioStatsProcessRBDB wlanRadioStatsProcessRBDB;

    MEMSET (&wlanRadioStatsProcessRBDB, 0,
            sizeof (tWssPmWlanRadioStatsProcessRBDB));
    WSSPM_FN_ENTRY ();
    wlanRadioStatsProcessRBDB.i4IfIndex = i4IfIndex;

    pWlanRadioStatsMap = ((tWssPmWlanRadioStatsProcessRBDB *)
                          RBTreeGet (gWssPmWlanRadioStatsProcessDB,
                                     (tRBElem *) & wlanRadioStatsProcessRBDB));
    if (pWlanRadioStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Radio Index)\r\n");
        return NULL;
    }
    WSSPM_FN_EXIT ();
    return pWlanRadioStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWLANBSSIDStatsEntryGet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmCapwATPStatsProcessRBDB *
WssIfCapwATPStatsEntryGet (UINT4 u4CapwapBaseWtpProfileId)
{
    tWssPmCapwATPStatsProcessRBDB *pWlanCapwATPStatsMap = NULL;
    tWssPmCapwATPStatsProcessRBDB wlanCapwATPStatsProcessRBDB;

    MEMSET (&wlanCapwATPStatsProcessRBDB, 0,
            sizeof (tWssPmCapwATPStatsProcessRBDB));
    WSSPM_FN_ENTRY ();
    wlanCapwATPStatsProcessRBDB.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;

    pWlanCapwATPStatsMap = ((tWssPmCapwATPStatsProcessRBDB *)
                            RBTreeGet (gWssPmCapwATPStatsProcessDB,
                                       (tRBElem *) &
                                       wlanCapwATPStatsProcessRBDB));
    if (pWlanCapwATPStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Profile ID)\r\n");
        return NULL;
    }
    WSSPM_FN_EXIT ();
    return pWlanCapwATPStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWLANBSSIDStatsEntryGet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWlanClientStatsProcessRBDB *
WssIfWlanClientStatsEntryGet (tMacAddr FsWlanClientStatsMACAddress)
{
    tWssPmWlanClientStatsProcessRBDB *pWlanClientStatsMap = NULL;
    tWssPmWlanClientStatsProcessRBDB wlanClientStatsProcessRBDB;

    MEMSET (&wlanClientStatsProcessRBDB, 0,
            sizeof (tWssPmWlanClientStatsProcessRBDB));
    WSSPM_FN_ENTRY ();
    MEMCPY (wlanClientStatsProcessRBDB.FsWlanClientStatsMACAddress,
            FsWlanClientStatsMACAddress, sizeof (tMacAddr));

    pWlanClientStatsMap = ((tWssPmWlanClientStatsProcessRBDB *)
                           RBTreeGet (gWssPmWlanClientStatsProcessDB,
                                      (tRBElem *) &
                                      wlanClientStatsProcessRBDB));
    if (pWlanClientStatsMap == NULL)
    {
        return NULL;
    }
    WSSPM_FN_EXIT ();
    return pWlanClientStatsMap;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWtpIDStatEntryDelete                             *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWtpIDStatEntryDelete (UINT2 u2WtpIntProfileIndex)
{
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (u2WtpIntProfileIndex);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWTPIDStatsProcessDB, (tRBElem *) pWtpIdStatsMap);
    MemReleaseMemBlock (PM_FSWTPIDSTATSTABLE_POOL, (UINT1 *) pWtpIdStatsMap);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWtpIDRadIDStatsEntryDelete                       *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWtpIDRadIDStatsEntryDelete (UINT2 u2WtpRadioIndex)
{
    tWssPmWTPIDRadIDStatsProcessRBDB *pwtpIDRadIDStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pwtpIDRadIDStatsMap = WssIfWtpIDRadIDStatsEntryGet (u2WtpRadioIndex);
    if (pwtpIDRadIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWtpIdStatsMap from DB(WTP profile ID, RadioID) is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWTPIDRadIDStatsProcessDB, (tRBElem *) pwtpIDRadIDStatsMap);
    MemReleaseMemBlock (PM_FSWTPIDRADIDSTATSTABLE_POOL,
                        (UINT1 *) pwtpIDRadIDStatsMap);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanBSSIDStatsEntryDelete                        *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanBSSIDStatsEntryDelete (UINT2 u4WlanBSSIndex)
{
    tWssPmWLANBSSIDStatsProcessRBDB *pWlanBSSIDStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWlanBSSIDStatsMap = WssIfWLANBSSIDStatsEntryGet (u4WlanBSSIndex);
    if (pWlanBSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWlanBSSIDStatsMap from DB(BSS ID) is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWLANBSSIDStatsProcessDB, (tRBElem *) pWlanBSSIDStatsMap);
    MemReleaseMemBlock (PM_FSWTPIDBSSIDSTATSTABLE_POOL,
                        (UINT1 *) pWlanBSSIDStatsMap);

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanBSSIDStatsEntryDelete                        *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanSSIDStatsEntryDelete (UINT4 u4FsDot11WlanProfileId)
{
    tWssPmWlanSSIDStatsProcessRBDB *pWlanSSIDStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWlanSSIDStatsMap = WssIfWlanSSIDStatsEntryGet (u4FsDot11WlanProfileId);
    if (pWlanSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWlanSSIDStatsMap from DB is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWlanSSIDStatsProcessDB, (tRBElem *) pWlanSSIDStatsMap);
    MemReleaseMemBlock (PM_FSWTPIDBSSIDSTATSTABLE_POOL,
                        (UINT1 *) pWlanSSIDStatsMap);

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanRadioStatsEntryDelete                        *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanRadioStatsEntryDelete (INT4 i4IfIndex)
{
    tWssPmWlanRadioStatsProcessRBDB *pWlanRadioStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWlanRadioStatsMap = WssIfWlanRadioStatsEntryGet (i4IfIndex);
    if (pWlanRadioStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWlanRadioStatsMap from DB is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWlanRadioStatsProcessDB, (tRBElem *) pWlanRadioStatsMap);
    MemReleaseMemBlock (PM_FSWLANRADIO_STATS_TABLE_POOL,
                        (UINT1 *) pWlanRadioStatsMap);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanClientStatsEntryDelete                        *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanClientStatsEntryDelete (tMacAddr FsWlanClientStatsMACAddress)
{
    tWssPmWlanClientStatsProcessRBDB *pWlanClientStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWlanClientStatsMap =
        WssIfWlanClientStatsEntryGet (FsWlanClientStatsMACAddress);
    if (pWlanClientStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWlanClientStatsMap from DB is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWlanClientStatsProcessDB, (tRBElem *) pWlanClientStatsMap);
    MemReleaseMemBlock (PM_FSWLANCLIENT_STATS_TABLE_POOL,
                        (UINT1 *) pWlanClientStatsMap);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanCapwATPStatsEntryDelete                        *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfCapwATPStatsEntryDelete (UINT4 u4CapwapBaseWtpProfileId)
{
    tWssPmCapwATPStatsProcessRBDB *pWlanCapwATPStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWlanCapwATPStatsMap = WssIfCapwATPStatsEntryGet (u4CapwapBaseWtpProfileId);
    if (pWlanCapwATPStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWtpIdStatsMap from DB(BSS ID) is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmCapwATPStatsProcessDB, (tRBElem *) pWlanCapwATPStatsMap);
    MemReleaseMemBlock (PM_FSCAPWATP_STATS_TABLE_POOL,
                        (UINT1 *) pWlanCapwATPStatsMap);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssPmRbtStatsEntrySet                                 *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmRbtStatsEntrySet (UINT2 u2WtpIntProfileIndex,
                       tWtpRebootStats * pWssPmRebootStats)
{

    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;

    WSSPM_FN_ENTRY ();
    if (pWssPmRebootStats == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, " NULL Pointer pWssPmWTPIDStats \r\n");
        return OSIX_FAILURE;
    }

    pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (u2WtpIntProfileIndex);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Profile id) \r\n");
        return OSIX_FAILURE;
    }

    /* Update Reboot Stats */
    MEMCPY (&pWtpIdStatsMap->pWtpRebootStat,
            pWssPmRebootStats, sizeof (tWtpRebootStats));
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssPmWLCRbtStatsEntrySet                              *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWTPCapwapStatsEntrySet (UINT2 u2WtpIntProfileIndex,
                             tWtpCPWPWtpStats * pWssWTPCapwapSessStats)
{

    WSSPM_FN_ENTRY ();
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;

    if (pWssWTPCapwapSessStats == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, " NULL Pointer pWssPmWTPIDStats \r\n");
        return OSIX_FAILURE;
    }

    pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (u2WtpIntProfileIndex);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Profile Id)\r\n");
        return OSIX_FAILURE;
    }

    /* Update wtp capwap Stats */
    MEMCPY (&pWtpIdStatsMap->CapwapSessStats,
            pWssWTPCapwapSessStats, sizeof (tWtpCPWPWtpStats));

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssPmWLCRbtStatsEntrySet                              *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWLCCapwapStatsEntrySet (UINT2 u2WtpIntProfileIndex,
                             tWtpCapwapSessStats * pWssWLCCapwapSessStats)
{

    WSSPM_FN_ENTRY ();
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;

    if (pWssWLCCapwapSessStats == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, " NULL Pointer pWssPmWTPIDStats \r\n");
        return OSIX_FAILURE;
    }

    pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (u2WtpIntProfileIndex);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Profile id db)\r\n");
        return OSIX_FAILURE;
    }

    /* Update capwap Stats */
    MEMCPY (&pWtpIdStatsMap->pWtpCapwapSessStats,
            pWssWLCCapwapSessStats, sizeof (tWtpCapwapSessStats));

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWtpIDRadIDStatsEntrySet                          *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 PM_FSWLANCLIENT_STATS_TABLE_POOL                                                   *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWtpIDRadIDStatsEntrySet (UINT4 u4WtpRadioIndex,
                              tWtpRadioStats * pWssPmRadioStats)
{
    tWssPmWTPIDRadIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    WSSPM_FN_ENTRY ();
    if (pWssPmRadioStats == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, " NULL Pointer pWssPmRadioStats \r\n");
        return OSIX_FAILURE;
    }

    pWtpIdStatsMap = WssIfWtpIDRadIDStatsEntryGet ((UINT2) u4WtpRadioIndex);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Radio ID) \r\n");
        return OSIX_FAILURE;
    }

    /* Update Reboot Stats */
    MEMCPY (&pWtpIdStatsMap->pWtpRadioStats,
            pWssPmRadioStats, sizeof (tWtpRadioStats));
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWtpIDRadIDStatsEntrySet                               *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIIfIEEEStatsEntrySet (UINT2 u2WtpProfileIndex, UINT4 u4WtpRadioIndex,
                         tDot11Statistics * pWssPmIeeeStats)
{
    tWssPmWTPIDRadIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    UNUSED_PARAM (u2WtpProfileIndex);
    WSSPM_FN_ENTRY ();
    if (pWssPmIeeeStats == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, " NULL Pointer pWssPmRadioStats \r\n");
        return OSIX_FAILURE;
    }

    pWtpIdStatsMap = WssIfWtpIDRadIDStatsEntryGet ((UINT2) u4WtpRadioIndex);
    if (pWtpIdStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to find entry from DB (Radio DB) \r\n");
        return OSIX_FAILURE;
    }

    /* Update Reboot Stats */
    MEMCPY (&pWtpIdStatsMap->pWtpdot11Stats,
            pWssPmIeeeStats, sizeof (tDot11Statistics));
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanBSSIDStatsEntrySet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanBSSIDStatsEntrySet (UINT4 u4WlanBSSIndex,
                             tBSSIDMacHndlrStats * WssPmWTPWLANBSSIDStats)
{
    tWssPmWLANBSSIDStatsProcessRBDB *pWlanBSSIDStatsMap = NULL;
    tWssPmWLANBSSIDStatsProcessRBDB wlanBSSIDStatsProcessRBDB;
    WSSPM_FN_ENTRY ();
    if (WssPmWTPWLANBSSIDStats == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&wlanBSSIDStatsProcessRBDB, 0,
            sizeof (tWssPmWLANBSSIDStatsProcessRBDB));
    wlanBSSIDStatsProcessRBDB.u4WlanBSSIndex = u4WlanBSSIndex;

    pWlanBSSIDStatsMap = ((tWssPmWLANBSSIDStatsProcessRBDB *)
                          RBTreeGet (gWssPmWLANBSSIDStatsProcessDB,
                                     (tRBElem *) & wlanBSSIDStatsProcessRBDB));
    if (pWlanBSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Entry is not present in BSS If Index DB \r\n");
        /*Coverity Fix  Begin */
        return OSIX_FAILURE;
        /*Coverity Fix Ends */
    }

    /* Update BSS ID Stats */
    if(WssPmWTPWLANBSSIDStats->isOptional != OSIX_TRUE)
    {
        MEMCPY (&pWlanBSSIDStatsMap->pBSSIDMacHndlrStats,
                WssPmWTPWLANBSSIDStats, sizeof (tBSSIDMacHndlrStats));
    }
    else
    {
        pWlanBSSIDStatsMap->pBSSIDMacHndlrStats.wlanBSSIDDataPktRcvdCount +=
            WssPmWTPWLANBSSIDStats->wlanBSSIDDataPktRcvdCount;
        pWlanBSSIDStatsMap->pBSSIDMacHndlrStats.wlanBSSIDDataPktSentCount +=
            WssPmWTPWLANBSSIDStats->wlanBSSIDDataPktSentCount;
    }

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanSSIDStatsEntrySet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanSSIDStatsEntrySet (UINT4 u4FsDot11WlanProfileId,
                             tBSSIDMacHndlrStats * WssPmWTPWLANBSSIDStats)
{
    tWssPmWlanSSIDStatsProcessRBDB *pWlanSSIDStatsMap = NULL;
    tWssPmWlanSSIDStatsProcessRBDB wlanSSIDStatsProcessRBDB;
    WSSPM_FN_ENTRY ();
    if (WssPmWTPWLANBSSIDStats == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&wlanSSIDStatsProcessRBDB, 0,
            sizeof (tWssPmWlanSSIDStatsProcessRBDB));
    wlanSSIDStatsProcessRBDB.u4FsDot11WlanProfileId = u4FsDot11WlanProfileId;

    pWlanSSIDStatsMap = ((tWssPmWlanSSIDStatsProcessRBDB *)
            RBTreeGet (gWssPmWlanSSIDStatsProcessDB,
                (tRBElem *) & wlanSSIDStatsProcessRBDB));
    if (pWlanSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                "Entry is not present in SSID DB \r\n");
        return OSIX_FAILURE;
    }
    /* Update SSID Stats */
    if(WssPmWTPWLANBSSIDStats->isOptional != OSIX_TRUE)
    {
        pWlanSSIDStatsMap->pSSIDMacHndlrStats.wlanBSSIDBeaconsSentCount += 
            WssPmWTPWLANBSSIDStats->wlanBSSIDBeaconsSentCount;
        pWlanSSIDStatsMap->pSSIDMacHndlrStats.wlanBSSIDProbeReqRcvdCount += 
            WssPmWTPWLANBSSIDStats->wlanBSSIDProbeReqRcvdCount;
        pWlanSSIDStatsMap->pSSIDMacHndlrStats.wlanBSSIDProbeRespSentCount += 
            WssPmWTPWLANBSSIDStats->wlanBSSIDProbeRespSentCount;
    }
    else
    {
        pWlanSSIDStatsMap->pSSIDMacHndlrStats.wlanBSSIDDataPktRcvdCount +=
            WssPmWTPWLANBSSIDStats->wlanBSSIDDataPktRcvdCount;
        pWlanSSIDStatsMap->pSSIDMacHndlrStats.wlanBSSIDDataPktSentCount +=
            WssPmWTPWLANBSSIDStats->wlanBSSIDDataPktSentCount;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}
/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanRadioStatsEntrySet                           *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
WssIfWlanRadioStatsEntrySet (INT4 i4IfIndex, tWtpRCStats * pWssPmWlanRadioStats)
{
    tWssPmWlanRadioStatsProcessRBDB *pWlanRadioStatsMap = NULL;
    tWssPmWlanRadioStatsProcessRBDB wlanRadioStatsProcessRBDB;
    WSSPM_FN_ENTRY ();
    if (pWssPmWlanRadioStats == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&wlanRadioStatsProcessRBDB, 0,
            sizeof (tWssPmWlanRadioStatsProcessRBDB));
    wlanRadioStatsProcessRBDB.i4IfIndex = i4IfIndex;

    pWlanRadioStatsMap = ((tWssPmWlanRadioStatsProcessRBDB *)
                          RBTreeGet (gWssPmWlanRadioStatsProcessDB,
                                     (tRBElem *) & wlanRadioStatsProcessRBDB));

    if (pWlanRadioStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Entry is not present in Radio Index DB \r\n");
        return OSIX_FAILURE;
    }

    pWlanRadioStatsMap->pradioClientStats.u4IfInOctets +=
        pWssPmWlanRadioStats->u4IfInOctets;

    pWlanRadioStatsMap->pradioClientStats.u4IfOutOctets +=
        pWssPmWlanRadioStats->u4IfOutOctets;

    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanClientStatsEntrySet                          *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanClientStatsEntrySet (tMacAddr FsWlanClientStatsMACAddress,
                              tWtpStaStats * pWssPmWlanClientStats)
{
    tWssPmWlanClientStatsProcessRBDB *pWlanClientStatsMap = NULL;
    tWssPmWlanClientStatsProcessRBDB wlanClientStatsProcessRBDB;

#ifdef WSSSTA_WANTED
    FLT4        f4ClientStatsLocalRecvdCount = 0.0;
    FLT4        f4ClientStatsLocalSentCount = 0.0;
#endif


#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;
    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));
#endif

    MEMSET (&wlanClientStatsProcessRBDB, 0,
            sizeof (tWssPmWlanClientStatsProcessRBDB));
    WSSPM_FN_ENTRY ();
    if (pWssPmWlanClientStats == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (wlanClientStatsProcessRBDB.FsWlanClientStatsMACAddress,
            FsWlanClientStatsMACAddress, sizeof (tMacAddr));

    pWlanClientStatsMap = ((tWssPmWlanClientStatsProcessRBDB *)
                           RBTreeGet (gWssPmWlanClientStatsProcessDB,
                                      (tRBElem *) &
                                      wlanClientStatsProcessRBDB));

    if (pWlanClientStatsMap == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMCPY (pWlanClientStatsMap->pclientStats.StaMacAddr,
	pWssPmWlanClientStats->StaMacAddr , MAC_ADDR_LEN);
    pWlanClientStatsMap->pclientStats.u4ClientStatsBytesSentCount =  
	pWssPmWlanClientStats->u4ClientStatsBytesSentCount;
    pWlanClientStatsMap->pclientStats.u4ClientStatsBytesRecvdCount =  
	pWssPmWlanClientStats->u4ClientStatsBytesRecvdCount;
    pWlanClientStatsMap->pclientStats.u4StaIpAddr =  
	pWssPmWlanClientStats->u4StaIpAddr;

#ifdef WSSSTA_WANTED


    pWlanClientStatsMap->pclientStats.u4ClientStatsBytesSentCount =  
	pWssPmWlanClientStats->u4ClientStatsBytesSentCount;
    pWlanClientStatsMap->pclientStats.u4ClientStatsBytesRecvdCount =  
	pWssPmWlanClientStats->u4ClientStatsBytesRecvdCount;

    f4ClientStatsLocalSentCount  = 
	(FLT4) pWssPmWlanClientStats->u4ClientStatsBytesSentCount/1024;
    f4ClientStatsLocalRecvdCount = 
	(FLT4) pWssPmWlanClientStats->u4ClientStatsBytesRecvdCount/1024;

    pWlanClientStatsMap->pclientStats.f4ClientStatsTotalRecvdCount = 
			f4ClientStatsLocalRecvdCount;
    pWlanClientStatsMap->pclientStats.f4ClientStatsTotalSentCount = 
			f4ClientStatsLocalSentCount;

    if (pWlanClientStatsMap->pclientStats.f4ClientStatsTotalRecvdCount + 
	    pWlanClientStatsMap->pclientStats.f4ClientStatsTotalSentCount != 0)
    {
#ifdef WSSUSER_WANTED
	WssUserNotifyParams.f4TxBytes =  
		pWlanClientStatsMap->pclientStats.f4ClientStatsTotalSentCount;
	WssUserNotifyParams.f4RxBytes =  
		pWlanClientStatsMap->pclientStats.f4ClientStatsTotalRecvdCount;

	MEMCPY(WssUserNotifyParams.staMac,FsWlanClientStatsMACAddress,MAC_ADDR_LEN);
	WssUserNotifyParams.eWssUserNotifyType = WSSUSER_VOLUME_CONSUMED_IND;

	if (WssUserProcessNotification (&WssUserNotifyParams)
		== OSIX_FAILURE)
	{
	    WSSPM_TRC (WSSPM_FAILURE_TRC, "WSSHandleRadResponse: "
		    "WssUserProcessNotification returned failure\r\n");
	    return OSIX_FAILURE;
	}
#endif

    }
#endif
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssIfWlanCapwATPStatsEntrySet                         *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/

INT4
WssIfCapwATPStatsEntrySet (UINT4 u4CapwapBaseWtpProfileId,
                           tApParams * pWssPmCapwATPStats)
{
    tWssPmCapwATPStatsProcessRBDB *pWlanCapwATPStatsMap = NULL;
    tWssPmCapwATPStatsProcessRBDB wlanCapwATPStatsProcessRBDB;
    UINT4               u4PrevSentBytes = 0;
    UINT4               u4PrevRcvdBytes = 0;
    tWssIfPMDB          wssIfPMDB;
    MEMSET (&wssIfPMDB, 0, sizeof (tWssIfPMDB));

    WSSPM_FN_ENTRY ();
    if (pWssPmCapwATPStats == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&wlanCapwATPStatsProcessRBDB, 0,
            sizeof (tWssPmCapwATPStatsProcessRBDB));
    wlanCapwATPStatsProcessRBDB.u4CapwapBaseWtpProfileId =
        u4CapwapBaseWtpProfileId;

    wssIfPMDB.u2ProfileId = (UINT2)u4CapwapBaseWtpProfileId;
    if (WssIfProcessPMDBMsg
        (WSS_PM_WTP_EVT_VSP_AP_STATS_GET, &wssIfPMDB) == OSIX_SUCCESS)
    {
        u4PrevSentBytes = wssIfPMDB.ApElement.u4SentBytes;
        u4PrevRcvdBytes = wssIfPMDB.ApElement.u4RcvdBytes;
    }
    pWlanCapwATPStatsMap = ((tWssPmCapwATPStatsProcessRBDB *)
                            RBTreeGet (gWssPmCapwATPStatsProcessDB,
                                       (tRBElem *) &
                                       wlanCapwATPStatsProcessRBDB));
    if (pWlanCapwATPStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Entry is not present in Profile Index DB \r\n");
        return OSIX_FAILURE;
    }
    pWlanCapwATPStatsMap->pApElement.u4GatewayIp =
        pWssPmCapwATPStats->u4GatewayIp;
    pWlanCapwATPStatsMap->pApElement.u4Subnetmask =
        pWssPmCapwATPStats->u4Subnetmask;

    pWlanCapwATPStatsMap->pApElement.u4SentBytes +=
        pWssPmCapwATPStats->u4SentBytes;

    pWlanCapwATPStatsMap->pApElement.u4RcvdBytes +=
        pWssPmCapwATPStats->u4RcvdBytes;

    if (u4PrevSentBytes > 0)
    {
        pWlanCapwATPStatsMap->pApElement.u4SentTrafficRate =
            ((pWlanCapwATPStatsMap->pApElement.u4SentBytes -
              u4PrevSentBytes) * 8) / 120;
    }
    if (u4PrevRcvdBytes > 0)
    {
        pWlanCapwATPStatsMap->pApElement.u4RcvdTrafficRate =
            ((pWlanCapwATPStatsMap->pApElement.u4RcvdBytes -
              u4PrevRcvdBytes) * 8) / 120;
    }
    pWlanCapwATPStatsMap->pApElement.u1TotalInterfaces =
        pWssPmCapwATPStats->u1TotalInterfaces;
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWlanSSIDStatsEntryCreate                             *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanSSIDStatsEntryCreate (UINT4 u4FsDot11WlanProfileId)
{
    tWssPmWlanSSIDStatsProcessRBDB *pWlanSSIDStatsMap = NULL;
    tWssPmWlanSSIDStatsProcessRBDB *pGetWlanSSIDStatsMap = NULL;
    tWssPmWlanSSIDStatsProcessRBDB GetSSIDStatsMap;

    WSSPM_FN_ENTRY ();
    MEMSET (&GetSSIDStatsMap, 0, sizeof (tWssPmWlanSSIDStatsProcessRBDB));

    GetSSIDStatsMap.u4FsDot11WlanProfileId = u4FsDot11WlanProfileId;
    pGetWlanSSIDStatsMap = (tWssPmWlanSSIDStatsProcessRBDB *) RBTreeGet
        (gWssPmWlanSSIDStatsProcessDB, (tRBElem *) & GetSSIDStatsMap);

    if (pGetWlanSSIDStatsMap != NULL)
    {
        return OSIX_SUCCESS;
    }
    pWlanSSIDStatsMap =
        (tWssPmWlanSSIDStatsProcessRBDB *)
        MemAllocMemBlk (PM_FSWTPIDBSSIDSTATSTABLE_POOL);

    if (pWlanSSIDStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWlanSSIDStatsMap Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlanSSIDStatsMap, 0, sizeof (tWssPmWlanSSIDStatsProcessRBDB));

    pWlanSSIDStatsMap->u4FsDot11WlanProfileId = u4FsDot11WlanProfileId;

    if (RBTreeAdd (gWssPmWlanSSIDStatsProcessDB,
                   pWlanSSIDStatsMap) != RB_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(SSID)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWlanRadioStatsEntryCreate                             *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanRadioStatsEntryCreate (INT4 i4IfIndex)
{
    tWssPmWlanRadioStatsProcessRBDB *pWlanRadioStatsMap = NULL;

    WSSPM_FN_ENTRY ();
    /* Add entry to the RB tree */
    pWlanRadioStatsMap =
        (tWssPmWlanRadioStatsProcessRBDB *)
        MemAllocMemBlk (PM_FSWLANRADIO_STATS_TABLE_POOL);

    if (pWlanRadioStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Mem Alloc for pWlanRadioStatsMap \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlanRadioStatsMap, 0, sizeof (tWssPmWlanRadioStatsProcessRBDB));

    pWlanRadioStatsMap->i4IfIndex = i4IfIndex;

    if (RBTreeAdd (gWssPmWlanRadioStatsProcessDB,
                   pWlanRadioStatsMap) != RB_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(Radio ID)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWlanBSSIDStatsEntryCreate                             *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfCapwATPStatsEntryCreate (UINT4 u4CapwapBaseWtpProfileId)
{
    tWssPmCapwATPStatsProcessRBDB *pWlanCapwATPStatsMap = NULL;

    WSSPM_FN_ENTRY ();
    pWlanCapwATPStatsMap =
        (tWssPmCapwATPStatsProcessRBDB *)
        MemAllocMemBlk (PM_FSCAPWATP_STATS_TABLE_POOL);
    /* Add entry to the RB tree */
    if (pWlanCapwATPStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Mem Alloc for pWlanCapwATPStatsMap \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlanCapwATPStatsMap, 0, sizeof (tWssPmCapwATPStatsProcessRBDB));

    pWlanCapwATPStatsMap->u4CapwapBaseWtpProfileId = u4CapwapBaseWtpProfileId;

    if (RBTreeAdd (gWssPmCapwATPStatsProcessDB,
                   pWlanCapwATPStatsMap) != RB_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(Profile ID)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssIfWlanClientStatsEntryCreate                             *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfWlanClientStatsEntryCreate (tMacAddr FsWlanClientStatsMACAddress)
{
    tWssPmWlanClientStatsProcessRBDB *pWlanClientStatsMap = NULL;
    tWssPmWlanClientStatsProcessRBDB *pGetWlanClientStatsMap = NULL;
    tWssPmWlanClientStatsProcessRBDB GetClientStatsMap;

    WSSPM_FN_ENTRY ();
    MEMSET (&GetClientStatsMap, 0, sizeof (tWssPmWlanClientStatsProcessRBDB));

    MEMCPY (GetClientStatsMap.FsWlanClientStatsMACAddress,
            FsWlanClientStatsMACAddress, sizeof (tMacAddr));
    pGetWlanClientStatsMap = (tWssPmWlanClientStatsProcessRBDB *) RBTreeGet
        (gWssPmWlanClientStatsProcessDB, (tRBElem *) & GetClientStatsMap);

    if (pGetWlanClientStatsMap != NULL)
    {
        return OSIX_SUCCESS;
    }

    pWlanClientStatsMap =
        (tWssPmWlanClientStatsProcessRBDB *)
        MemAllocMemBlk (PM_FSWLANCLIENT_STATS_TABLE_POOL);

    /* Add entry to the RB tree */
    if (pWlanClientStatsMap == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Mem Alloc for pWlanClientStatsMap \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWlanClientStatsMap, 0, sizeof (tWssPmWlanClientStatsProcessRBDB));

    MEMCPY (pWlanClientStatsMap->FsWlanClientStatsMACAddress,
            FsWlanClientStatsMACAddress, sizeof (tMacAddr));

    if (RBTreeAdd (gWssPmWlanClientStatsProcessDB,
                   pWlanClientStatsMap) != RB_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(Client)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}
/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssPmWTPRunEventsEntryCreate                               *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWTPRunEventsEntryCreate (UINT4 u4CapwapBaseWtpProfileId)
{
    tWssPmWTPRunEvents    *pWssPmWTPRunEvents = NULL;
    WSSPM_FN_ENTRY ();
    pWssPmWTPRunEvents =
        (tWssPmWTPRunEvents *)
        MemAllocMemBlk (PM_WTPRUN_EVENTS_TABLE_POOL);
    /* Add entry to the RB tree */
    if (pWssPmWTPRunEvents == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Mem Alloc for tWssPmWTPRunEvents \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssPmWTPRunEvents, 0, sizeof (tWssPmWTPRunEvents));

    pWssPmWTPRunEvents->u4CapwapWtpProfileId = u4CapwapBaseWtpProfileId;
    if (RBTreeAdd (gWssPmWTPRunEvents,
                   pWssPmWTPRunEvents) != RB_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Failed : Failed to add entry in RB Tree(Profile ID)\r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}


/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: WssPmWTPRunEventsfEntrySet                                  *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
tWssPmWTPRunEvents*
WssPmWTPRunEventsEntryGet (UINT4 u4CapwapWtpProfileId)
{

    tWssPmWTPRunEvents *pWssPmWTPRunEvents = NULL;
    tWssPmWTPRunEvents WssPmWTPRunEvents;

    WSSPM_FN_ENTRY ();
    MEMSET (&WssPmWTPRunEvents , 0 , sizeof(tWssPmWTPRunEvents));
    WssPmWTPRunEvents.u4CapwapWtpProfileId = u4CapwapWtpProfileId;
    pWssPmWTPRunEvents = (tWssPmWTPRunEvents*) RBTreeGet
        (gWssPmWTPRunEvents, (tRBElem *) &WssPmWTPRunEvents);
    WSSPM_FN_EXIT ();
    return pWssPmWTPRunEvents;


} 
/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : WssPmWTPRunEventsEntryDelete                          *
 *                                                                           *
 * DESCRIPTION  :                                                            *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssPmWTPRunEventsEntryDelete (UINT4 u4CapwapBaseWtpProfileId)
{
    tWssPmWTPRunEvents *pWssPmWTPRunEvents = NULL;
    WSSPM_FN_ENTRY ();
    /* Get the RBTree Node for the particular indices */
    pWssPmWTPRunEvents = WssPmWTPRunEventsEntryGet (u4CapwapBaseWtpProfileId);
    if (pWssPmWTPRunEvents == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pWssPmWTPRunEvents from DB is NULL \r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gWssPmWTPRunEvents, (tRBElem *) pWssPmWTPRunEvents);
    MemReleaseMemBlock (PM_WTPRUN_EVENTS_TABLE_POOL,
                        (UINT1 *) pWssPmWTPRunEvents);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif /* __WSSPM_RBT_C__ */
