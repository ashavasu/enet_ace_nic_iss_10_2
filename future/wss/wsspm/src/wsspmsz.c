/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsspmsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the WSSPM sizing  routines. 
 * 
 *****************************************************************************/

#define _WSSPMSZ_C
#include "wsspminc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WsspmSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSPM_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsWSSPMSizingParams[i4SizingId].u4StructSize,
                          FsWSSPMSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSPMMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WsspmSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WsspmSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSPMSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSPMMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WsspmSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSPM_MAX_SIZING_ID; i4SizingId++) {
        if(WSSPMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSPMMemPoolIds[ i4SizingId] );
            WSSPMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
