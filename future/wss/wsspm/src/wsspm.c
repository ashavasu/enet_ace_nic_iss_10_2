/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspm.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the WSSPM API related functions
 *
 *****************************************************************************/
#ifndef __WSSPM_C__
#define __WSSPM_C__

#include "wsspminc.h"
#include "wssifpmdb.h"
#include "wssifstatdfs.h"
#include "wssifcapdb.h"
#include "capwapinc.h"
#include "wssifstaproto.h"

tPmTaskGlobals      gPmTaskGlobals;
extern tRBTree      gWssPmWLANBSSIDStatsProcessDB;
tTMO_SLL            WtpStatTimerInfoLL;    /* WTP and its Stats Timer LL */

/*****************************************************************************/
/* FUNCTION NAME    : pmWTPInfoNotify                                        */
/*                                                                           */
/* DESCRIPTION      : Notification if any WTP related modification, addition */
/*                    is required.                                           */
/*                                                                           */
/* INPUT            : u1OpCode                                               */
/*                    pWssPMInfo                                             */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
pmWTPInfoNotify (UINT1 u1OpCode, tWssPMInfo * pWssPMInfo)
{
    tMacAddr            FsWlanClientStatsMACAddress;
    INT4                i4Status = 0;
    UINT2               u2ProfileId = 0;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT4               u4FsDot11WlanProfileId = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;
    UINT2               stat_timer = 0;
    UINT2               u2RadioIfIndex = 0;
    UINT1               u1RadioId = 0;

    WSSPM_FN_ENTRY ();
    if (pWssPMInfo)
    {
        u2ProfileId = pWssPMInfo->u2ProfileId;
        u4BssIfIndex = pWssPMInfo->u2ProfileBSSID;
        u4RadioIfIndex = pWssPMInfo->u4RadioIfIndex;
        u4FsDot11WlanProfileId = pWssPMInfo->u4FsDot11WlanProfileId;
        u4CapwapBaseWtpProfileId = pWssPMInfo->u4CapwapBaseWtpProfileId;

        stat_timer = pWssPMInfo->stats_timer;
        u2RadioIfIndex = (UINT2) (pWssPMInfo->u4RadioIfIndex);
        u1RadioId = pWssPMInfo->u1RadioId;

        MEMCPY (FsWlanClientStatsMACAddress,
                pWssPMInfo->FsWlanClientStatsMACAddress, 6);
    }
    switch (u1OpCode)
    {
        case WTPPRF_ADD:
            i4Status = pmAddNewWtpEntry (u2ProfileId, (UINT1) stat_timer,
                                         u2RadioIfIndex);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to add entries to DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case WTPPRF_DEL:
            i4Status = pmDelExistingWtpEntry (u2ProfileId, u2RadioIfIndex,
                                              u1RadioId);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entries from DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case BSSID_ADD:
            i4Status = pmAddBSSIDEntry (u4BssIfIndex);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to add entry to BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case BSSID_DEL:
            i4Status = pmDelBSSIDEntry (u4BssIfIndex);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case RADIO_ADD:
            i4Status = pmAddRadioEntry (u4RadioIfIndex);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case RADIO_DEL:
            i4Status = pmDelRadioEntry (u4RadioIfIndex);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case SSID_ADD:
            i4Status = pmAddSSIDEntry (u4FsDot11WlanProfileId);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case SSID_DEL:
            i4Status = pmDelSSIDEntry (u4FsDot11WlanProfileId);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case CLIENT_ADD:
            i4Status = pmAddClientEntry (FsWlanClientStatsMACAddress);
            if (i4Status != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            break;

        case CLIENT_DEL:
            i4Status = pmDelClientEntry (FsWlanClientStatsMACAddress);
            if (i4Status != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            break;

        case CAPWATP_ADD:
            i4Status = pmAddCapwATPEntry (u4CapwapBaseWtpProfileId);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;
        case CAPWATP_DEL:
            i4Status = pmDelCapwATPEntry (u4CapwapBaseWtpProfileId);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to delete entry from BSS DB \r\n");
                return OSIX_FAILURE;
            }
            break;

        case STAT_TIMER_RESET:
            i4Status = pmResetStatTimer (u2ProfileId, stat_timer);
            if (i4Status != OSIX_SUCCESS)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to update stats timer entry \r\n");
                return OSIX_FAILURE;
            }
            break;
        default:
            break;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmWTPEvtReqNotify                                      */
/*                                                                           */
/* DESCRIPTION      : Notification if any WTP Event request is received      */
/*                                                                           */
/* INPUT            : u1OpCode                                               */
/*                    pWssPMInfo                                             */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmWTPEvtReqNotify (UINT1 u1OpCode, tWssPMInfo * pWssPMInfo)
{
    tWssPMInfo         *pWssPMWTPInfo = NULL;

    WSSPM_FN_ENTRY ();
    if (!pWssPMInfo)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "pWssPMInfo pointer is NULL \r\n");
        return OSIX_FAILURE;
    }

    pWssPMWTPInfo = (tWssPMInfo *)
        MemAllocMemBlk (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL);
    if (pWssPMWTPInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    switch (u1OpCode)
    {
        case REBOOT_STATS_SET:
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmRbtStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.rebootStats,
                    &pWssPMInfo->wtpEventInfo.rebootStats,
                    sizeof (tWtpRebootStats));

            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "REBOOT_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "REBOOT_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case RADIO_STATS_SET:
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            pWssPMWTPInfo->u4RadioIfIndex = pWssPMInfo->u4RadioIfIndex;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmRadStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.radioStats,
                    &pWssPMInfo->wtpEventInfo.radioStats,
                    sizeof (tWtpRadioStats));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "RADIO_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "RADIO_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case IEEE_802_11_STATS_SET:
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            pWssPMWTPInfo->u4RadioIfIndex = pWssPMInfo->u4RadioIfIndex;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmIEEE80211StatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.dot11Stats,
                    &pWssPMInfo->wtpEventInfo.dot11Stats,
                    sizeof (tDot11Statistics));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {

                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "IEEE_802_11_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "IEEE_802_11_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case CAPWAP_SESS_STATS_SET:
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmWtpEvtWtpCWStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.wtpCapwapStats,
                    &pWssPMInfo->wtpEventInfo.wtpCapwapStats,
                    sizeof (tWtpCPWPWtpStats));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "CAPWAP_SESS_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "CAPWAP_SESS_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case CAPWAP_WLC_SESS_RUN_STATS_SET:
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            MEMCPY (&(pWssPMWTPInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag),
                    &(pWssPMInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag),
                    sizeof (tWssPmWLCCapwapStatsSetDB));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "CAPWAP_WLC_SESS_RUN_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                   PM_STATS_WLC_CAPWAP_RUN_STATS_UPDATE_EVENT) == 
                         OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "CAPWAP_WLC_SESS_RUN_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case VSP_BSSID_STATS_SET:
            pWssPMWTPInfo->u2ProfileBSSID = pWssPMInfo->u2ProfileBSSID;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmVSPBSSIDStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.bssIdMachdlrStats,
                    &pWssPMInfo->wtpEventInfo.bssIdMachdlrStats,
                    sizeof (tBSSIDMacHndlrStats));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "VSP_BSSID_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "VSP_BSSID_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case VSP_SSID_STATS_SET:
            MEMSET (pWssPMWTPInfo, 0, sizeof (tWssPMInfo));
            pWssPMWTPInfo->u4FsDot11WlanProfileId = 
                            pWssPMInfo->u4FsDot11WlanProfileId;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmVSPSSIDStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.bssIdMachdlrStats,
                    &pWssPMInfo->wtpEventInfo.bssIdMachdlrStats,
                    sizeof (tBSSIDMacHndlrStats));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "VSP_SSID_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "VSP_SSID_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;

        case CAPWAP_WLC_SESS_STATS_SET:
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            MEMCPY (&pWssPMWTPInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag,
                    &pWssPMInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag,
                    (MEM_MAX_BYTES
                     (sizeof
                      (pWssPMWTPInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag),
                      sizeof (pWssPMInfo->wssPmIntAttr.
                              WssPmWLCCapwapStatsSetFlag))));
            if (OsixQueSend
                (gPmTaskGlobals.pmStatsRxMsgQId, (UINT1 *) &pWssPMWTPInfo,
                 OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "CAPWAP_WLC_SESS_STATS_SET Queue Send Failed \r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_WLC_CAPWAP_STATS_UPDATE_EVENT) ==
                OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "CAPWAP_WLC_SESS_STATS_SET Event Send Failed \r\n");
                return OSIX_FAILURE;
            }
            break;
        case VSP_RADIO_STATS_SET:
            MEMSET (pWssPMWTPInfo, 0, sizeof (tWssPMInfo));
            pWssPMWTPInfo->u4RadioIfIndex = pWssPMInfo->u4RadioIfIndex;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmVSPRadioStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.radioClientStats,
                    &pWssPMInfo->wtpEventInfo.radioClientStats,
                    sizeof (tWtpRCStats));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                        "VSP_RADIO_STATS_SET Queue Send Failed\r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                        "VSP_RADIO_STATS_SET Event Send Failed\r\n");
                return OSIX_FAILURE;
            }
            break;
        case CLIENT_STATS_SET:
            MEMSET (pWssPMWTPInfo, 0, sizeof (tWssPMInfo));
            MEMCPY (pWssPMWTPInfo->FsWlanClientStatsMACAddress,
                    pWssPMInfo->FsWlanClientStatsMACAddress, sizeof (tMacAddr));
            pWssPMWTPInfo->wssPmIntAttr.bWssPmVSPClientStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.clientStats,
                    &pWssPMInfo->wtpEventInfo.clientStats,
                    sizeof (tWtpStaStats));
            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                        "CLIENT_STATS_SET Queue Send Failed\r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                        "CLIENT_STATS_SET Event Send Failed\r\n");
                return OSIX_FAILURE;
            }
            break;
        case VSP_AP_STATS_SET:
            MEMSET (pWssPMWTPInfo, 0, sizeof (tWssPMInfo));
            pWssPMWTPInfo->u2ProfileId = pWssPMInfo->u2ProfileId;
            pWssPMWTPInfo->wssPmIntAttr.bWssPmVSPApStatsSet = TRUE;
            MEMCPY (&pWssPMWTPInfo->wtpEventInfo.ApElement,
                    &pWssPMInfo->wtpEventInfo.ApElement, sizeof (tApParams));

            if (OsixQueSend (gPmTaskGlobals.pmStatsRxMsgQId,
                             (UINT1 *) &pWssPMWTPInfo,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                        "VSP_AP_STATS_SET Queue Send Failed\r\n");
                return OSIX_FAILURE;
            }
            if (OsixEvtSend (gPmTaskGlobals.pmTaskId,
                             PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                    (UINT1 *) pWssPMWTPInfo);
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                        "VSP_AP_STATS_SET Event Send Failed\r\n");
                return OSIX_FAILURE;
            }
            break;

        default:
            MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                                (UINT1 *) pWssPMWTPInfo);
            WSSPM_TRC (WSSPM_FAILURE_TRC, "pWssPMInfo pointer is NULL \r\n");
            return OSIX_FAILURE;
            break;
    }
    WSSPM_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : WssPMGetRadioIDForWTPProfile                           */
/*                                                                           */
/* DESCRIPTION      : Retrieved the Radi related info based on the WTP       */
/*                    profile Id                                             */
/*                                                                           */
/* INPUT            : u4PMWtpProfileId                                       */
/*                                                                           */
/* OUTPUT           :  u4RadioIfIndex                                        */
/*                     pu2NumRadioPerWTP                                     */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
WssPMGetRadioIDForWTPProfile (UINT4 u4PMWtpProfileId,
                              UINT4 *au4RadioIfIndex, UINT2 *pu2NumRadioPerWTP)
{
    UINT4               u4WTPProfileID = 0;
    tCapwapFsCapwapWirelessBindingEntry *pEntry = NULL;
    tCapwapFsCapwapWirelessBindingEntry *pNextEntry = NULL;
    UINT2               u2RadioIdPerWTP = 0;

    WSSPM_FN_ENTRY ();

    pEntry = CapwapGetFirstFsCapwapWirelessBindingTable ();
    if (pEntry != NULL)
    {
        u4WTPProfileID = (UINT4) (pEntry->MibObject.u4CapwapBaseWtpProfileId);
        if (u4WTPProfileID == u4PMWtpProfileId)
        {
            au4RadioIfIndex[u2RadioIdPerWTP] =
                (UINT4) (pEntry->MibObject.
                         i4FsCapwapWirelessBindingVirtualRadioIfIndex);
            u2RadioIdPerWTP++;
        }
    }

    while (u2RadioIdPerWTP <= 31 && pEntry != NULL)
    {
        pNextEntry = CapwapGetNextFsCapwapWirelessBindingTable (pEntry);
        if (pNextEntry != NULL)
        {
            u4WTPProfileID =
                (UINT4) (pNextEntry->MibObject.u4CapwapBaseWtpProfileId);
            if (u4WTPProfileID == u4PMWtpProfileId)
            {
                au4RadioIfIndex[u2RadioIdPerWTP] =
                    (UINT4) (pNextEntry->MibObject.
                             i4FsCapwapWirelessBindingVirtualRadioIfIndex);
                u2RadioIdPerWTP++;
            }
        }
        pEntry = pNextEntry;
    }

    *pu2NumRadioPerWTP = u2RadioIdPerWTP;
    WSSPM_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmAddNewWtpEntry                                       */
/*                                                                           */
/* DESCRIPTION      : Add Stats related entries for newly added WTPs         */
/*                                                                           */
/* INPUT            : u2ProfileId                                            */
/*                    stat_timer                                             */
/*                    u2RadioId                                              */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmAddNewWtpEntry (UINT2 u2ProfileId, UINT1 stat_timer, UINT2 u2RadioId)
{
    tWtpStatTimerInfo  *pWtpStatTimerInfo = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               au4RadioIfIndex[WSSPM_WORD_LEN];

    WSSPM_FN_ENTRY ();
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (au4RadioIfIndex, 0, WSSPM_WORD_LEN);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    /* check, if wtp timer info & wtp db is created already. if so, skip it.
     * this condition will come when a wtp profile has more than one radios */
    if (WssIfWtpIDStatsEntryGet (u2ProfileId) == NULL)
    {
        pWtpStatTimerInfo =
            (tWtpStatTimerInfo *) MemAllocMemBlk (PM_FSWTPMSTATINFOTABLE_POOL);

        if (!pWtpStatTimerInfo)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to allocate memory pWtpStatTimerInfo)\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return (OSIX_FAILURE);
        }

        MEMSET (pWtpStatTimerInfo, 0, sizeof (tWtpStatTimerInfo));
        TMO_SLL_Init_Node (&(pWtpStatTimerInfo->NextNode));

        pWtpStatTimerInfo->wtp_id = u2ProfileId;
        pWtpStatTimerInfo->stat_timer = stat_timer;
        pWtpStatTimerInfo->issTimerTicks = 0;

        TMO_SLL_Add (&WtpStatTimerInfoLL,
                     (tTMO_SLL_NODE *) & (pWtpStatTimerInfo->NextNode));
        if (WssIfWtpIDStatsEntryCreate (u2ProfileId) != OSIX_SUCCESS)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to add entry DB(ProfileID)\r\n");
            TMO_SLL_Delete (&WtpStatTimerInfoLL, &pWtpStatTimerInfo->NextNode);
            MemReleaseMemBlock (PM_FSWTPMSTATINFOTABLE_POOL,
                                (UINT1 *) pWtpStatTimerInfo);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (WssIfCapwATPStatsEntryCreate ((UINT4) u2ProfileId) != OSIX_SUCCESS)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to add ATP stats entry DB(ProfileID)\r\n");
            TMO_SLL_Delete (&WtpStatTimerInfoLL, &pWtpStatTimerInfo->NextNode);
            MemReleaseMemBlock (PM_FSWTPMSTATINFOTABLE_POOL,
                                (UINT1 *) pWtpStatTimerInfo);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (WssPmWTPRunEventsEntryCreate ((UINT4) u2ProfileId) != OSIX_SUCCESS)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to add Run stats entry DB(ProfileID)\r\n");
            TMO_SLL_Delete (&WtpStatTimerInfoLL, &pWtpStatTimerInfo->NextNode);
            MemReleaseMemBlock (PM_FSWTPMSTATINFOTABLE_POOL,
                                (UINT1 *) pWtpStatTimerInfo);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }

    if (WssIfWtpIDRadIDStatsEntryCreate (u2RadioId) != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to add entry to DB(ProfileID, RadioIFIndex)\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (WssIfWlanRadioStatsEntryCreate ((INT4) u2RadioId) != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to add entry to DB(RadioIFIndex)\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;

    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmResetStatTimer                                       */
/*                                                                           */
/* DESCRIPTION      : Reset the Stats timer value                            */
/*                                                                           */
/* INPUT            : u2ProfileId                                            */
/*                    stat_timer                                             */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmResetStatTimer (UINT2 u2ProfileId, UINT2 stat_timer)
{
    tWtpStatTimerInfo  *pWtpStatTimerInfo = NULL;
    TMO_SLL_Scan (&WtpStatTimerInfoLL, pWtpStatTimerInfo, tWtpStatTimerInfo *)
    {
        if (pWtpStatTimerInfo)
        {
            if (pWtpStatTimerInfo->wtp_id == u2ProfileId)
                (pWtpStatTimerInfo->stat_timer) = (UINT1) stat_timer;
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmDelExistingWtpEntry                                  */
/*                                                                           */
/* DESCRIPTION      : Delete the stats entry for the requested WTP.          */
/*                                                                           */
/* INPUT            : u2ProfileId                                            */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmDelExistingWtpEntry (UINT2 u2ProfileId, UINT2 u2RadioIfIndex, UINT1 u1RadioId)
{
    tWtpStatTimerInfo  *pWtpStatTimerInfo = NULL;
    tWtpStatTimerInfo  *tempWtpTimerInfo = NULL;
    UINT4               au4RadioIfIndex[WSSPM_WORD_LEN];
    INT4                i4Status = OSIX_SUCCESS;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               wtpInternalId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (au4RadioIfIndex, 0, WSSPM_WORD_LEN);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    wtpInternalId = u2ProfileId;

    /* check, if wtp timer info & wtp db is deleted already. if so, skip it.
     * this condition will come when a wtp profile has more than one radios */
    if (u1RadioId == 1)
    {
        TMO_SLL_Scan (&WtpStatTimerInfoLL, tempWtpTimerInfo,
                      tWtpStatTimerInfo *)
        {
            if (tempWtpTimerInfo->wtp_id == wtpInternalId)
            {
                pWtpStatTimerInfo = tempWtpTimerInfo;
                continue;
            }
        }

        TMO_SLL_Delete (&WtpStatTimerInfoLL, &pWtpStatTimerInfo->NextNode);

        MemReleaseMemBlock (PM_FSWTPMSTATINFOTABLE_POOL,
                            (UINT1 *) pWtpStatTimerInfo);

        /* Delete from WTP profile ID DB */
        i4Status = WssIfWtpIDStatEntryDelete (u2ProfileId);
        if (i4Status != OSIX_SUCCESS)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to delete entry DB (ProfileID)\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        i4Status = WssIfCapwATPStatsEntryDelete ((UINT4) u2ProfileId);
        if (i4Status != OSIX_SUCCESS)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to delete ATP stats entry DB (ProfileID)\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        i4Status = WssPmWTPRunEventsEntryDelete ((UINT4) u2ProfileId);
        if (i4Status != OSIX_SUCCESS)
        {
            WSSPM_TRC (WSSPM_FAILURE_TRC,
                       "Unable to delete Run stats entry DB (ProfileID)\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }

    if (WssIfWtpIDRadIDStatsEntryDelete (u2RadioIfIndex) != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to del entry from DB(ProfileID, RadioIFIndex)\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (WssIfWlanRadioStatsEntryDelete ((INT4) u2RadioIfIndex) != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to del entry from DB(ProfileID, RadioIFIndex)\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmAddBSSIDEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Adds a Stats entry for a BSSID                         */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmAddBSSIDEntry (UINT4 u4BssIfIndex)
{
    INT4                i4Status = OSIX_SUCCESS;

    WSSPM_FN_ENTRY ();
    i4Status = WssIfWlanBSSIDStatsEntryCreate (u4BssIfIndex);
    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Unable to add entry to BSS ID DB \r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmAddSSIDEntry                                         */
/*                                                                           */
/* DESCRIPTION      : Adds a Stats entry for a SSID                          */
/*                                                                           */
/* INPUT            : u4FsDot11WlanProfileId                                 */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmAddSSIDEntry (UINT4 u4FsDot11WlanProfileId)
{
    INT4                i4Status = OSIX_SUCCESS;

    WSSPM_FN_ENTRY ();
    i4Status = WssIfWlanSSIDStatsEntryCreate (u4FsDot11WlanProfileId);
    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Unable to add entry to SSID DB \r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmAddBSSIDEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Adds a Stats entry for a BSSID                         */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmAddRadioEntry (UINT4 u4RadioIfIndex)
{
    INT4                i4Status = OSIX_SUCCESS;

    WSSPM_FN_ENTRY ();
    i4Status = WssIfWlanRadioStatsEntryCreate ((INT4) u4RadioIfIndex);
    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to add entry to RADIO ID DB \r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmAddBSSIDEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Adds a Stats entry for a BSSID                         */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmAddClientEntry (tMacAddr FsWlanClientStatsMACAddress)
{
    INT4                i4Status = OSIX_SUCCESS;

    WSSPM_FN_ENTRY ();
    i4Status = WssIfWlanClientStatsEntryCreate (FsWlanClientStatsMACAddress);
    if (i4Status != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmAddBSSIDEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Adds a Stats entry for a BSSID                         */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmAddCapwATPEntry (UINT4 u4CapwapBaseWtpProfileId)
{
    INT4                i4Status = OSIX_SUCCESS;

    WSSPM_FN_ENTRY ();
    i4Status = WssIfCapwATPStatsEntryCreate (u4CapwapBaseWtpProfileId);
    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Unable to add entry to BSS ID DB \r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmDelBSSIDEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Deletes the stats entry for a BSSID                    */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
INT4
pmDelBSSIDEntry (UINT4 u4BssIfIndex)
{
    INT4                i4Status = OSIX_SUCCESS;

    i4Status = WssIfWlanBSSIDStatsEntryDelete ((UINT2) u4BssIfIndex);
    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to delete entry from BSS ID DB \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmDelRadioEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Deletes the stats entry for a BSSID                    */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
pmDelRadioEntry (UINT4 u4RadioIfIndex)
{
    INT4                i4Status = OSIX_SUCCESS;
    i4Status = WssIfWlanRadioStatsEntryDelete ((INT4) u4RadioIfIndex);

    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to delete entry from RADIO ID DB \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmDelSSIDEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Deletes the stats entry for a BSSID                    */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
pmDelSSIDEntry (UINT4 u4FsDot11WlanProfileId)
{
    INT4                i4Status = OSIX_SUCCESS;
    i4Status = WssIfWlanSSIDStatsEntryDelete (u4FsDot11WlanProfileId);

    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to delete entry from SSID DB \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmDelClientEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Deletes the stats entry for a BSSID                    */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
pmDelClientEntry (tMacAddr FsWlanClientStatsMACAddress)
{
    INT4                i4Status = OSIX_SUCCESS;
    i4Status = WssIfWlanClientStatsEntryDelete (FsWlanClientStatsMACAddress);

    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to delete entry from Client DB \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmDelCapwATPEntry                                        */
/*                                                                           */
/* DESCRIPTION      : Deletes the stats entry for a BSSID                    */
/*                                                                           */
/* INPUT            : u4BssIfIndex                                           */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
pmDelCapwATPEntry (UINT4 u4CapwapBaseWtpProfileId)
{
    INT4                i4Status = OSIX_SUCCESS;
    i4Status = WssIfCapwATPStatsEntryDelete (u4CapwapBaseWtpProfileId);

    if (i4Status != OSIX_SUCCESS)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to delete entry from BSS ID DB \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : pmWtpLocalStatsTmrTmOutCollectData                     */
/*                                                                           */
/* DESCRIPTION      : Collect the WTP Local Stats Data on timeout            */
/*                                                                           */
/* INPUT            : u2ProfileId                                            */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : OSIX_SUCCESS/ FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

INT4
pmWtpLocalStatsTmrTmOutCollectData (UINT2 u2ProfileId)
{

    WSSPM_FN_ENTRY ();

#ifdef WLC_WANTED
    tWssPmWLANBSSIDStatsProcessRBDB *pBssIdEntry = NULL;
    tWssPmWLANBSSIDStatsProcessRBDB *pTempBssIdEntry = NULL;
    tWssifauthDBMsgStruct *pWssBssIdStnStats = NULL;
    pWssBssIdStnStats =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssBssIdStnStats == NULL)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "pmWtpLocalStatsTmrTmOutCollectData:- "
                   "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssBssIdStnStats, 0, sizeof (tWssifauthDBMsgStruct));

    /* Run through BSS ID RB Tree */
    pBssIdEntry = (tWssPmWLANBSSIDStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWLANBSSIDStatsProcessDB);

    if (pBssIdEntry != NULL)
    {
        pTempBssIdEntry = pBssIdEntry;
        do
        {
            pWssBssIdStnStats->WssBssStnStatsDB.u4BssIfIndex =
                pBssIdEntry->u4WlanBSSIndex;
            if ((WssIfProcessWssAuthDBMsg (WSS_AUTH_GET_BSSSID_STATION_STATS,
                                           pWssBssIdStnStats)) != OSIX_SUCCESS)
            {
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                return OSIX_FAILURE;
            }
            MEMCPY (&pBssIdEntry->pBSSIDStnStats,
                    &(pWssBssIdStnStats->WssBssStnStatsDB.bssIdStnStats),
                    sizeof (tBSSIDStnStats));

            pBssIdEntry = (tWssPmWLANBSSIDStatsProcessRBDB *)
                RBTreeGetNext (gWssPmWLANBSSIDStatsProcessDB,
                               (tRBElem *) pTempBssIdEntry, NULL);
            pTempBssIdEntry = pBssIdEntry;
        }
        while (pTempBssIdEntry != NULL);
    }
    UNUSED_PARAM (u2ProfileId);
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
#else
    UNUSED_PARAM (u2ProfileId);
#endif
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : PmCollectStatsFromWTPEventReqMsg                       *
 *                                                                           *
 * DESCRIPTION      : Collect Stats from WTP Event Req. Msg. Elements        *
 *                    This has WTP Radio, Reboot, IEEE 802.11,               *
 *                    802.11 Mac Handler Stats, and WTP's CPWP basic stats   *
 *                    from VSP                                               *
 * INPUT            : None                                                   *
 *                                                                           *
 * OUTPUT           : None                                                   *
 *                                                                           *
 * RETURNS          : OSIX_SUCCESS or OSIX_FAILURE                           *
 *****************************************************************************/
INT4
PmCollectStatsFromWTPEventReqMsg (UINT4 u4Event)
{
    tWssPMInfo         *pWssWtpInfo = NULL;
    tWssPMWtpAllStatsInfo WssIfPMInfo;
    tWssPmWTPIDRadIDStatsProcessRBDB WssPmWTPIDRadIDStats;
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    tWssPmWTPRunEvents *pWssPmWTPRunEvents = NULL;
    INT4                i4Status = OSIX_FAILURE;
    UINT4              u4CurrentTime = 0;
    tOsixSysTime        sysTime = 0;

    WSSPM_FN_ENTRY ();

    MEMSET (&WssIfPMInfo, 0, sizeof (tWssPMWtpAllStatsInfo));
    MEMSET (&WssPmWTPIDRadIDStats, 0,
            sizeof (tWssPmWTPIDRadIDStatsProcessRBDB));

    while (OsixQueRecv (gPmTaskGlobals.pmStatsRxMsgQId,
                        (UINT1 *) (&pWssWtpInfo),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (u4Event == PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT)
        {
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmRbtStatsSet) == OSIX_TRUE)
            {
                i4Status = WssPmRbtStatsEntrySet (pWssWtpInfo->u2ProfileId,
                                                  &pWssWtpInfo->wtpEventInfo.
                                                  rebootStats);
                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssPmRbtStatsEntrySet -  Unable to find entry from DB \r\n");
                }
		pWssWtpInfo->wssPmIntAttr.bWssPmRbtStatsSet = OSIX_FALSE;
            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmRadStatsSet) == OSIX_TRUE)
            {
                i4Status =
                    WssIfWtpIDRadIDStatsEntrySet (pWssWtpInfo->u4RadioIfIndex,
                                                  &pWssWtpInfo->wtpEventInfo.
                                                  radioStats);
                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssIfWtpIDRadIDStatsEntrySet -  Unable to find entry from DB \r\n");
                }
		pWssWtpInfo->wssPmIntAttr.bWssPmRadStatsSet = OSIX_FALSE;

            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmIEEE80211StatsSet)
                == OSIX_TRUE)
            {
                i4Status = WssIIfIEEEStatsEntrySet (pWssWtpInfo->u2ProfileId,
                                                    pWssWtpInfo->u4RadioIfIndex,
                                                    &pWssWtpInfo->wtpEventInfo.
                                                    dot11Stats);

                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssIIfIEEEStatsEntrySet -  Unable to find entry from DB \r\n");
                }
		pWssWtpInfo->wssPmIntAttr.bWssPmIEEE80211StatsSet = OSIX_FALSE;

            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmWtpEvtWtpCWStatsSet)
                == OSIX_TRUE)
            {
                i4Status =
                    WssPmWTPCapwapStatsEntrySet (pWssWtpInfo->u2ProfileId,
                                                 &pWssWtpInfo->wtpEventInfo.
                                                 wtpCapwapStats);

                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssPmWTPCapwapStatsEntrySet -  Unable to find entry from DB \r\n");
                }
		pWssWtpInfo->wssPmIntAttr.bWssPmWtpEvtWtpCWStatsSet = OSIX_FALSE;

            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmVSPBSSIDStatsSet) == OSIX_TRUE)
            {
                i4Status =
                    WssIfWlanBSSIDStatsEntrySet (pWssWtpInfo->u2ProfileBSSID,
                                                 &pWssWtpInfo->wtpEventInfo.
                                                 bssIdMachdlrStats);

                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssIfWlanBSSIDStatsEntrySet -  Unable to find entry from DB \r\n");
                }

		pWssWtpInfo->wssPmIntAttr.bWssPmVSPBSSIDStatsSet = OSIX_FALSE;

            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmVSPSSIDStatsSet) == OSIX_TRUE)
            {
                i4Status =
                    WssIfWlanSSIDStatsEntrySet (pWssWtpInfo->u4FsDot11WlanProfileId,
                                                 &pWssWtpInfo->wtpEventInfo.
                                                 bssIdMachdlrStats);

                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssIfWlanSSIDStatsEntrySet -  Unable to find entry from DB \r\n");
                }

		pWssWtpInfo->wssPmIntAttr.bWssPmVSPSSIDStatsSet = OSIX_FALSE;

            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmVSPRadioStatsSet) == OSIX_TRUE)
            {
                i4Status =
                    WssIfWlanRadioStatsEntrySet ((INT4)
                                                 (pWssWtpInfo->u4RadioIfIndex),
                                                 &pWssWtpInfo->wtpEventInfo.
                                                 radioClientStats);

                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssIfWlanRadioStatsEntrySet -  Unable to find entry from DB \r\n");
                }

                pWssWtpInfo->wssPmIntAttr.bWssPmVSPRadioStatsSet = OSIX_FALSE;
            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmVSPClientStatsSet) ==
                OSIX_TRUE)
            {
                i4Status =
                    WssIfWlanClientStatsEntrySet (pWssWtpInfo->
                                                  FsWlanClientStatsMACAddress,
                                                  &pWssWtpInfo->wtpEventInfo.
                                                  clientStats);
                if (i4Status != OSIX_SUCCESS)
                {
                }

                pWssWtpInfo->wssPmIntAttr.bWssPmVSPClientStatsSet = OSIX_FALSE;
            }
            if ((pWssWtpInfo->wssPmIntAttr.bWssPmVSPApStatsSet) == OSIX_TRUE)
            {
                i4Status =
                    WssIfCapwATPStatsEntrySet (pWssWtpInfo->u2ProfileId,
                                               &pWssWtpInfo->wtpEventInfo.
                                               ApElement);

                if (i4Status != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                               "WssIfCapwATPStatsEntrySet -  Unable to find entry from DB \r\n");
                }

                pWssWtpInfo->wssPmIntAttr.bWssPmVSPApStatsSet = OSIX_FALSE;
            }
        }
        if (u4Event == PM_STATS_WLC_CAPWAP_STATS_UPDATE_EVENT)
        {
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssWtpInfo->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                           "Unable to find entry from DB \r\n");
                return OSIX_FAILURE;
            }

            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapDiscReqRxd == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats.
                    discReqRxd++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapDiscRespSent == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats.
                    discRespSent++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapDiscUnsuccReqProcessed == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats.
                    discUnsuccReqProcessed++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapDiscReasonLastUnsuccAtt == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats.
                    discReasonLastUnsuccAtt++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapDiscLastSuccAttTime == OSIX_TRUE)
            {
                OsixGetSysTime (&sysTime);
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats.
                    discLastSuccAttTime = sysTime;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapDiscLastUnsuccAttTime == OSIX_TRUE)
            {
                OsixGetSysTime (&sysTime);
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats.
                    discLastUnsuccAttTime = sysTime;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapJoinReqRxd == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats.
                    joinReqRxd++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapJoinRespSent == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats.
                    joinRespSent++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapJoinUnsuccReqProcessed == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats.
                    joinUnsuccReqProcessed++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapJoinReasonForLastUnsuccAtt == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats.
                    joinReasonForLastUnsuccAtt++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapJoinLastSuccAttTime == OSIX_TRUE)
            {
                OsixGetSysTime (&sysTime);
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats.
                    joinLastSuccAttTime = sysTime;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapJoinLastUnsuccAttTime == OSIX_TRUE)
            {
                OsixGetSysTime (&sysTime);
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats.
                    joinLastUnsuccAttTime = sysTime;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapCfgRespRcvd == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessConfigStats.
                    cfgRespRcvd++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapCfgReqSent == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessConfigStats.
                    cfgReqSent++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapCfgUnsuccRespProcessed == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessConfigStats.
                    cfgUnsuccReqProcessed++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapCfgReasonForLastUnsuccAtt == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessConfigStats.
                    cfgReasonForLastUnsuccAtt++;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapCfgLastSuccAttTime == OSIX_TRUE)
            {
                OsixGetSysTime (&sysTime);
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessConfigStats.
                    cfgLastSuccAttTime = sysTime;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapCfgLastUnsuccAttTime == OSIX_TRUE)
            {
                OsixGetSysTime (&sysTime);
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessConfigStats.
                    cfgLastUnsuccAttTime = sysTime;
            }
            if (pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                bWssPmWLCCapwapKeepAlivePkts == OSIX_TRUE)
            {
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpKeepAlivePktsSent++;
            }
        }
        if (u4Event == PM_STATS_WLC_CAPWAP_RUN_STATS_UPDATE_EVENT)
        {

            pWssPmWTPRunEvents = WssPmWTPRunEventsEntryGet (pWssWtpInfo->u2ProfileId);
            if (pWssPmWTPRunEvents == NULL)
            {
                WSSPM_TRC (WSSPM_FAILURE_TRC,
                     "WssPmWTPRunEventsEntryGet -  Unable to find entry from DB \r\n");
                return OSIX_FAILURE;
            }
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bConfigUpdateStats) == OSIX_TRUE)
            {
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateReqReceived++;
                      
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateRspReceived++;
                     OsixGetSysTime (&u4CurrentTime);
                     UtlGetTimeStrForTicks (u4CurrentTime, 
                    (CHR1 *) pWssPmWTPRunEvents->ConfigUpdateStats.au1RunUpdateLastSuccAttemptTime);


                }

                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateRspTransmitted++;

                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ConfigUpdateStats.u4RunUpdateunsuccessfulProcessed++;

                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->ConfigUpdateStats.au1RunUpdateLastUnsuccessfulAttemptTime);
                }
                 
            }
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bStaConfigStats) == OSIX_TRUE)
            {
                 if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateReqReceived++;
                      
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateRspReceived++;
                     OsixGetSysTime (&u4CurrentTime);
                     UtlGetTimeStrForTicks (u4CurrentTime, 
                    (CHR1 *) pWssPmWTPRunEvents->StaConfigStats.au1RunUpdateLastSuccAttemptTime);

                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateRspTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->StaConfigStats.u4RunUpdateunsuccessfulProcessed++;

                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->StaConfigStats.au1RunUpdateLastUnsuccessfulAttemptTime);
                }
 
            }
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bClearConfigStats) == OSIX_TRUE)
            {
                 if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateReqReceived++;
                      
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateRspReceived++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->ClearConfigStats.au1RunUpdateLastSuccAttemptTime);


                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateRspTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ClearConfigStats.u4RunUpdateunsuccessfulProcessed++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->ClearConfigStats.au1RunUpdateLastUnsuccessfulAttemptTime);
                }
 
            }
 
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bDataTransferStats) == OSIX_TRUE)
            {
                 if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateReqReceived++;
                      
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateRspReceived++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->DataTransferStats.au1RunUpdateLastSuccAttemptTime);


                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateRspTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->DataTransferStats.u4RunUpdateunsuccessfulProcessed++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->DataTransferStats.au1RunUpdateLastUnsuccessfulAttemptTime);
                }
 
            }
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bResetStats) == OSIX_TRUE)
            {
                 if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ResetStats.u4RunUpdateReqReceived++;
                      
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ResetStats.u4RunUpdateRspReceived++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->ResetStats.au1RunUpdateLastSuccAttemptTime);


                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ResetStats.u4RunUpdateRspTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ResetStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->ResetStats.u4RunUpdateunsuccessfulProcessed++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->ResetStats.au1RunUpdateLastUnsuccessfulAttemptTime);
                }
 
            }
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bPrimaryDiscStats) == OSIX_TRUE)
            {
                 if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateReqReceived++;
                      
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateRspReceived++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateRspTransmitted++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->PrimaryDiscStats.au1RunUpdateLastSuccAttemptTime);

                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->PrimaryDiscStats.u4RunUpdateunsuccessfulProcessed++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->PrimaryDiscStats.au1RunUpdateLastUnsuccessfulAttemptTime);


                }
 
            }
            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bEchoStats) == OSIX_TRUE)
            {
                 if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->EchoStats.u4RunUpdateReqReceived++;
                                    }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspReceived == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->EchoStats.u4RunUpdateRspReceived++;
                   
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateReqTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->EchoStats.u4RunUpdateReqTransmitted++;
                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateRspTransmitted == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->EchoStats.u4RunUpdateRspTransmitted++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->EchoStats.au1RunUpdateLastSuccAttemptTime);

                }
                if(pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                    bRunUpdateunsuccessfulProcessed == OSIX_TRUE) 
                {
                    pWssPmWTPRunEvents->EchoStats.u4RunUpdateunsuccessfulProcessed++;
                    OsixGetSysTime (&u4CurrentTime);
                    UtlGetTimeStrForTicks (u4CurrentTime, 
                   (CHR1 *) pWssPmWTPRunEvents->EchoStats.au1RunUpdateLastUnsuccessfulAttemptTime);

                }
 

            }

            if ((pWssWtpInfo->wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                       bRowStatus) == OSIX_TRUE)
            {

               pWssPmWTPRunEvents->u1RowStatus = ACTIVE;

            }
 
        }
        MemReleaseMemBlock (PM_FSWTPMWTPEVENT_INFO_TABLE_POOL,
                            (UINT1 *) pWssWtpInfo);
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /*__WSSPM_C__*/
