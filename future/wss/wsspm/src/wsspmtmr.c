/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmtmr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the WSSPM related functions
 *
 *****************************************************************************/

#ifndef _WSSPM_WLC_TMR_C_
#define _WSSPM_WLC_TMR_C_
#include "wsspminc.h"

extern tTMO_SLL WtpStatTimerInfoLL; 
tWssPmWtpStatsTimerList gWssPmWtpStatsTmrList;

UINT1 gTimerEntry = 0;


/* static tPmTaskGlobals gPmTaskGlobals; */

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME     : wsspmStatsTmrInit                                     *
 *                                                                           *
 * DESCRIPTION  :  This function creates a timer list for the timers         *
 *                          in WSSPM module.                                 *
 *                                                                           *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
wssPmStatsTmrInit(VOID)
{
    WSSPM_FN_ENTRY();
    /* Timer List for WTP Statistics Timers */
    if (TmrCreateTimerList ((CONST UINT1 *) PM_TASK_NAME, WSSPM_TMR_EXP_EVENT,
                NULL, 
                (tTimerListId *) &(gWssPmWtpStatsTmrList.pmWtpStatsTmrListId))
            == TMR_FAILURE)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                "Unable to craete Stats collection Timer \r\n");
        return OSIX_FAILURE;
    }
    wssPmWtpStatsTmrInitTmrDesc ();
    WSSPM_FN_EXIT();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * FUNCTION NAME: wssPmWtpStatsTmrInitTmrDesc                                *
 *                                                                           *
 * DESCRIPTION  : This function intializes the timer desc for                *
 *                the timers in WSSPM module.                                *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
wssPmWtpStatsTmrInitTmrDesc (VOID)
{
    /* STAT TIMER */
    gWssPmWtpStatsTmrList.aWssPmWtpStatsTmrDesc[0].TmrExpFn
        = WssPmWtpStatsTmrExp;
    gWssPmWtpStatsTmrList.aWssPmWtpStatsTmrDesc[0].i2Offset =
        (INT2) FSAP_OFFSETOF (tWssPmWtpStatsTimerList, pmWtpStatsTimer); 
    return;
}

/*****************************************************************************
 * FUNCTION NAME: pmWtpStatsTmrStart                                         *
 * DESCRIPTION  : Function to start the PM Stats timer                       *
 * INPUT        : None                                                       *
 * OUTPUT       : None                                                       *
 * RETURNS      : None                                                       *
 *****************************************************************************/
INT4
pmWtpStatsTmrStart (UINT1 u1TmrType, UINT4 u4TmrInterval)
{
    UNUSED_PARAM(u1TmrType);
    WSSPM_FN_ENTRY();
    if (TmrStart (gWssPmWtpStatsTmrList.pmWtpStatsTmrListId, 
                &(gWssPmWtpStatsTmrList.pmWtpStatsTimer),
                0, u4TmrInterval, 0) == TMR_FAILURE)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                "Unable to start Stats collection Timer \r\n");
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME: WssPmWtpStatsTmrExp                                        *
 * DESCRIPTION  : Function invoked for PM Stats timer expiry                 *
 * INPUT        : None                                                       *
 * OUTPUT       : None                                                       *
 * RETURNS      : None                                                       *
 *****************************************************************************/
VOID
WssPmWtpStatsTmrExp (VOID *pArg)
{
    tWtpStatTimerInfo *pWtpStatTimerInfo= NULL;
    INT2 timer_temp = 0;
    UINT2 u2WtpProfileIndex = 0;

    WSSPM_FN_ENTRY();
    UNUSED_PARAM (pArg);
    gTimerEntry++;
    WSSPM_TRC1(WSSPM_UTIL_TRC, 
            "gTimerEntry No.of times wlc timer expired %d \r\n ",gTimerEntry);

    TMO_SLL_Scan (&WtpStatTimerInfoLL, pWtpStatTimerInfo, tWtpStatTimerInfo *)
    {
        if (pWtpStatTimerInfo != NULL)
        {
            WSSPM_TRC1(WSSPM_UTIL_TRC, "pWtpStatTimerInfo->wtp_id is %d \r\n", 
                   pWtpStatTimerInfo->wtp_id);
            pWtpStatTimerInfo->issTimerTicks++;
            timer_temp = pWtpStatTimerInfo->issTimerTicks;
            if (pWtpStatTimerInfo->stat_timer == timer_temp)
            {
                u2WtpProfileIndex = pWtpStatTimerInfo->wtp_id;
                pWtpStatTimerInfo->issTimerTicks = 0;
                if (pmWtpLocalStatsTmrTmOutCollectData(u2WtpProfileIndex) 
                        != OSIX_SUCCESS)
                {
                    WSSPM_TRC (WSSPM_FAILURE_TRC,
                 " Unable to collect statistics for this profile id \r\n");
                }
            }
        }
    }

    if (pmWtpStatsTmrStart (WTP_STATS_TIMER_TYPE, 
                WTP_MAX_STATS_TIMER_TIMEOUT) == OSIX_FAILURE)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                "Unable to start stats collection timer \r\n");
        return;
    }
    return;
    WSSPM_FN_EXIT();
}
/*****************************************************************************
 * FUNCTION NAME: WssPmTmrExpHandler                                         *
 * DESCRIPTION  : WSS PM timer expiry handler function                       *
 * INPUT        : None                                                       *
 * OUTPUT       : None                                                       *
 * RETURNS      : None                                                       * 
 *****************************************************************************/
VOID
WssPmTmrExpHandler (VOID)
{
    tTmrAppTimer      *pExpiredTimers = NULL;
    UINT1              u1TimerId = 0;
    INT2               i2Offset = 0;

    WSSPM_FN_ENTRY ();

    while ((pExpiredTimers =
                TmrGetNextExpiredTimer (
                    gWssPmWtpStatsTmrList.pmWtpStatsTmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        WSSPM_TRC1(WSSPM_MGMT_TRC, "Timer to be processed %d\r\n",
                u1TimerId);

        i2Offset = gWssPmWtpStatsTmrList.aWssPmWtpStatsTmrDesc[0].i2Offset; 

        if (i2Offset == -1)
        {
            /* The timer function does not take any parameter. */
        }
        else
        {
            (*( gWssPmWtpStatsTmrList.aWssPmWtpStatsTmrDesc[0].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    WSSPM_FN_EXIT ();
    return ;
}

#endif
