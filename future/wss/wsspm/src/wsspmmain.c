/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmmain.c,v 1.3 2017/11/24 10:37:11 siva Exp $
 *
 * Description: This file contains the WSSPM related functions
 *
 *****************************************************************************/
#ifndef __WSSPM_WLC_MAIN_C__
#define __WSSPM_WLC_MAIN_C__
#include "wsspminc.h"
extern tPmTaskGlobals gPmTaskGlobals;
extern tTMO_SLL     WtpStatTimerInfoLL;    /* WTP and its Stats Timer LL */

/*****************************************************************************
 * FUNCTION NAME: PmTaskInit                                                 *
 * DESCRIPTION  : PM Receiver task initialization routine.                   *
 *                                                                           *
 * INPUT        : None                                                       *
 *                                                                           *
 * OUTPUT       : None                                                       *
 *                                                                           *
 * RETURNS      : OSIX_SUCCESS, if initialization succeeds                   *
 *                OSIX_FAILURE, otherwise                                    *
 *                                                                           *
 *****************************************************************************/
UINT4
PmTaskInit ()
{
    UINT4               u4Event = 0;

    WSSPM_FN_ENTRY ();
    if (OsixTskIdSelf (&gPmTaskGlobals.pmTaskId) == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Not able to get the task id \r\n");
        return OSIX_FAILURE;
    }

    /* WSSPM Semaphore */
    if (OsixCreateSem (WSSPM_SEM_NAME, WSSPM_SEM_CREATE_INIT_CNT, 0,
                       &gPmTaskGlobals.SemId) == OSIX_FAILURE)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Semaphore Creation Failed \n\n\n");
        return OSIX_FAILURE;
    }

    /* Create PM statistics queue */
    if (OsixQueCrt (PM_STATS_Q_NAME, OSIX_MAX_Q_MSG_LEN, 150 /* Q depth */ ,
                    &(gPmTaskGlobals.pmStatsRxMsgQId)) != OSIX_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Unable to create PM_STATS_Q_NAME \r\n");
        return OSIX_FAILURE;
    }

    /* Create the RB Tree */
    if (WssPmRBTreeInit () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        WSSPM_TRC (WSSPM_FAILURE_TRC, "Unable to init DB's \r\n");
        return OSIX_FAILURE;
    }
    /* Initialize WTP-Stats Info LL */
    TMO_SLL_Init (&WtpStatTimerInfoLL);

    gPmTaskGlobals.u1IsPmTaskInitialized = 1;

    /* Create the Application timer List */
    if (wssPmStatsTmrInit () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to init local WLC stats collection timer \r\n");
        return OSIX_FAILURE;
    }

    if (pmWtpStatsTmrStart (WTP_STATS_TIMER_TYPE,
                            WTP_MAX_STATS_TIMER_TIMEOUT) == OSIX_FAILURE)
    {
        WSSPM_TRC (WSSPM_FAILURE_TRC,
                   "Unable to start the local WLC stats collection timer \r\n");
        return OSIX_FAILURE;
    }

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixReceiveEvent (PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT |
                              WSSPM_TMR_EXP_EVENT |
                              PM_STATS_WLC_CAPWAP_STATS_UPDATE_EVENT |
                              PM_STATS_WLC_CAPWAP_RUN_STATS_UPDATE_EVENT,
                              OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {
            WSSPM_LOCK;
            if ((u4Event & PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT)
                == PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT)
            {
                /* Read WTP Event Request Statistics (WTP Radio, Reboot, 
                 * WLAN BSSID in VSP)from PM queue */
                PmCollectStatsFromWTPEventReqMsg (u4Event);
            }

            if ((u4Event & WSSPM_TMR_EXP_EVENT) == WSSPM_TMR_EXP_EVENT)
            {
                WssPmTmrExpHandler ();
            }

            if ((u4Event & PM_STATS_WLC_CAPWAP_STATS_UPDATE_EVENT) ==
                PM_STATS_WLC_CAPWAP_STATS_UPDATE_EVENT)
            {
                PmCollectStatsFromWTPEventReqMsg (u4Event);
            }
            if ((u4Event & PM_STATS_WLC_CAPWAP_RUN_STATS_UPDATE_EVENT) ==
                PM_STATS_WLC_CAPWAP_RUN_STATS_UPDATE_EVENT)
            {
                PmCollectStatsFromWTPEventReqMsg (u4Event);
            }

            WSSPM_UNLOCK;
        }
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
WsspmLock (VOID)
{
    WSSPM_FN_ENTRY ();
    if (OsixSemTake (gPmTaskGlobals.SemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
WsspmUnLock (VOID)
{
    WSSPM_FN_ENTRY ();

    if (OsixSemGive (gPmTaskGlobals.SemId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WSSPM_FN_EXIT ();

    return OSIX_SUCCESS;
}

#endif /*__WSSPM_WLC_MAIN_C__*/
