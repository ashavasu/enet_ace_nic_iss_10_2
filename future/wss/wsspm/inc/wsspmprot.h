/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmprot.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the prototype of functions in
 *        WSSPM module*
 *******************************************************************/

#ifndef _WSSPM_WLC_PROT_H_
#define _WSSPM_WLC_PROT_H_

INT4 WssPmRBTreeInit (VOID);

INT4 WssPmWTPIDCompareStatsProcessDBRBTree PROTO((tRBElem *, tRBElem *));
INT4 WssPmWTPIDRadIDCompareStatsProcessDBRBTree PROTO((tRBElem *, tRBElem *));
INT4 WssPmWLANBSSIDCompareStatsProcessDBRBTree PROTO((tRBElem *, tRBElem *));
INT4 WssPmWlanSSIDCompareStatsProcessDBRBTree  PROTO((tRBElem *, tRBElem *));
INT4 WssPmWlanClientCompareStatsProcessDBRBTree  PROTO((tRBElem *, tRBElem *));
INT4 WssPmWlanCapwATPCompareStatsProcessDBRBTree PROTO((tRBElem *, tRBElem *));
INT4 WssPmWlanRadioCompareStatsProcessDBRBTree PROTO((tRBElem *, tRBElem *));

INT4 WssIfWtpIDStatsEntryCreate (UINT2 u4WtpProfileIndex);
INT4 WssIfWtpIDRadIDStatsEntryCreate(UINT2 u4WtpRadioIndex);
INT4 WssIfWlanBSSIDStatsEntryCreate(UINT4 u4WlanBSSIndex);
INT4
WssIfWlanSSIDStatsEntryDelete (UINT4 u4FsDot11WlanProfileId);
INT4
WssIfWlanRadioStatsEntryDelete (INT4 i4IfIndex);
INT4
WssIfWlanClientStatsEntryDelete (tMacAddr FsWlanClientStatsMACAddress);
INT4
WssIfCapwATPStatsEntryDelete (UINT4 u4CapwapBaseWtpProfileId);

INT4 WssIfWtpIDStatEntryDelete (UINT2 u4WtpProfileIndex);
INT4 
pmWTPEvtReqNotify(UINT1 u1OpCode, tWssPMInfo* pWssPMInfo);
INT4 pmWTPInfoNotify (UINT1, tWssPMInfo*);
VOID PmCollectStatsFromCAPWAPandStationMod(VOID);

tWssPmWLANBSSIDStatsProcessRBDB * 
             WssIfWlanBSSIDStatsEntryGet (UINT2 u4WlanBSSIndex);
tWssPmWTPIDStatsProcessRBDB * 
             WssIfWtpIDStatsEntryGet (UINT2 u4WtpIntProfileIndex);

INT4 pmAddNewWtpEntry(UINT2 wtp_id, UINT1 u2StatsTimer,  UINT2 u2RadioId);
INT4 pmDelExistingWtpEntry(UINT2 wtp_id, UINT2 u2RadioIfIndex, UINT1 u1RadioId);
INT4 pmAddBSSIDEntry(UINT4 u4WlanBSSIndex);
INT4 pmDelBSSIDEntry(UINT4 u4WlanBSSIndex);
INT4 pmAddRadioEntry(UINT4 u4RadioIfIndex);
INT4 pmDelRadioEntry(UINT4 u4RadioIfIndex);
INT4 pmAddSSIDEntry(UINT4 u4FsDot11WlanProfileId);
INT4 pmDelSSIDEntry(UINT4 u4FsDot11WlanProfileId);
INT4 pmAddClientEntry(tMacAddr FsWlanClientStatsMACAddress);
INT4 pmDelClientEntry(tMacAddr FsWlanClientStatsMACAddress);
INT4 pmAddCapwATPEntry(UINT4 u4CapwapBaseWtpProfileId);
INT4 pmDelCapwATPEntry(UINT4 u4CapwapBaseWtpProfileId);
INT4 pmResetStatTimer(UINT2 wtp_id, UINT2);
INT4 pmWtpLocalStatsTmrTmOutCollectData(UINT2 wtp_id);

tWssPmWTPIDRadIDStatsProcessRBDB* WssIfWtpIDRadIDStatsEntryGet (UINT2 );
tWssPmWLANBSSIDStatsProcessRBDB* WssIfWLANBSSIDStatsEntryGet (UINT2 );
INT4 WssIfWtpIDRadIDStatsEntryDelete (UINT2 );
INT4 WssIfWlanBSSIDStatsEntryDelete (UINT2 );
tWssPmCapwATPStatsProcessRBDB* WssIfCapwATPStatsEntryGet (UINT4);
tWssPmWlanRadioStatsProcessRBDB* WssIfWlanRadioStatsEntryGet (INT4);
tWssPmWlanSSIDStatsProcessRBDB* WssIfWlanSSIDStatsEntryGet (UINT4);
tWssPmWlanClientStatsProcessRBDB* WssIfWlanClientStatsEntryGet (tMacAddr);
INT4 WssIfWlanSSIDStatsEntryCreate (UINT4 );
INT4 WssIfWlanRadioStatsEntryCreate (INT4 );
INT4 WssIfWlanClientStatsEntryCreate (tMacAddr );
INT4 WssIfCapwATPStatsEntryCreate (UINT4);
INT4 PmCollectStatsFromWTPEventReqMsg(UINT4 u4Event);
VOID wssPmWtpStatsTmrInitTmrDesc (VOID);
INT4 WssPMGetRadioIDForWTPProfile (UINT4 u4PMWtpProfileId,
                                   UINT4 *au4RadioIfIndex,
                                   UINT2 *pu2NumRadioPerWTP);

VOID
WssPmTmrExpHandler (VOID);

INT4 WsspmLock PROTO ((VOID));

INT4 WsspmUnLock PROTO ((VOID));


INT4
WssPmWLCCapwapStatsEntrySet(UINT2 u2WtpIntProfileIndex, 
                      tWtpCapwapSessStats *pWssWLCCapwapSessStats);

INT4
WssPmRbtStatsEntrySet(UINT2 u2WtpIntProfileIndex, 
                      tWtpRebootStats *pWssPmRebootStats);
INT4
WssIfWlanRadioStatsEntrySet(INT4 i4IfIndex,
                tWtpRCStats  *pWssPmWlanRadioStats);
INT4
WssIfWlanClientStatsEntrySet(tMacAddr FsWlanClientStatsMACAddress,
                tWtpStaStats  *pWssPmWlanClientStats);
INT4
WssIfCapwATPStatsEntrySet(UINT4 u4CapwapBaseWtpProfileId,
                tApParams  *pWssPmCapwATPStats);

INT4
WssPmWTPCompareRunEventsRBTree (tRBElem * e1, tRBElem * e2);

INT4
WssPmWTPRunEventsEntryCreate (UINT4 u4CapwapBaseWtpProfileId);

tWssPmWTPRunEvents*
WssPmWTPRunEventsEntryGet (UINT4 u4CapwapWtpProfileId);
INT4
WssPmWTPRunEventsEntryDelete (UINT4 u4CapwapBaseWtpProfileId);
#endif


