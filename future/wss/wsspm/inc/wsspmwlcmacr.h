/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmwlcmacr.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the macros in
 *        WSSPM module*
 *******************************************************************/
#ifndef _PMMACR_H 
#define _PMMACR_H 

#define MAX_PM_PKT_QUE_SIZE_SIZING_ID 1
#define PM_QUEMSG_POOLID PMMemPoolIds[MAX_PM_PKT_QUE_SIZE_SIZING_ID]
#endif
