/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmdef.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the function macros of
 *        WSSPM module*
 *******************************************************************/

#ifndef  __PM_DEF_H__
#define  __PM_DEF_H__

#define PM_STATS_Q_NAME (UINT1 *) "PMQ"
#define WSSPM_LOCK                WsspmLock ()
#define WSSPM_UNLOCK              WsspmUnLock ()
#define WSSPM_SEM_NAME        (UINT1 *) "WSSPM"
#define WSSPM_SEM_CREATE_INIT_CNT    1



#endif

