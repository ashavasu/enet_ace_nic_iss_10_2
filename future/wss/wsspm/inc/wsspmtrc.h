/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmtrc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the declaration of traces in
 *        WSSPM module*
 *******************************************************************/
#ifndef _WSSPMTRC_H_
#define _WSSPMTRC_H_

#define  WSSPM_MOD                ((const char *)"WSSPM")

#define WSSPM_MGMT_TRC                    0x00000001
#define WSSPM_INIT_TRC                    0x00000002
#define WSSPM_ENTRY_TRC                   0x00000004
#define WSSPM_EXIT_TRC                    0x00000008 
#define WSSPM_FAILURE_TRC                 0x00000010
#define WSSPM_BUF_TRC                     0x00000020
#define WSSPM_SESS_TRC                    0x00000040
#define WSSPM_PKTDUMP_TRC                 0x00000080
#define WSSPM_STATS_TRC                   0x00000100
#define WSSPM_FSM_TRC                     0x00000200
#define WSSPM_MAIN_TRC                    0x00000400
#define WSSPM_UTIL_TRC                    0x00000800

/* #define WSSPM_MASK  WSSPM_MGMT_TRC | WSSPM_FAILURE_TRC | WSSPM_STATS_TRC 
 * | WSSPM_FSM_TRC | WSSPM_ENTRY_TRC | WSSPM_EXIT_TRC | WSSPM_UTIL_TRC */
#define WSSPM_MASK  WSSPM_FAILURE_TRC

#define WSSPM_FN_ENTRY() MOD_FN_ENTRY (WSSPM_MASK, WSSPM_ENTRY_TRC,WSSPM_MOD)

#define WSSPM_FN_EXIT() MOD_FN_EXIT (WSSPM_MASK, WSSPM_EXIT_TRC,WSSPM_MOD)

#define WSSPM_TRC(mask, fmt)\
      MOD_TRC(WSSPM_MASK, mask, WSSPM_MOD, fmt)
#define WSSPM_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(WSSPM_MASK,mask,WSSPM_MOD,fmt,arg1)
#define WSSPM_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(WSSPM_MASK,mask,WSSPM_MOD,fmt,arg1,arg2)
#define WSSPM_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(WSSPM_MASK,mask,WSSPM_MOD,fmt,arg1,arg2,arg3)
#define WSSPM_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(WSSPM_MASK,mask,WSSPM_MOD,fmt,arg1,arg2,arg3,arg4)
#endif
