/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmdb.h,v 1.2 2017/05/23 14:16:58 siva Exp $
 *
 * Description: This file contains the function prototypes and macros of
 *        WSSPM module*
 *******************************************************************/
#ifndef _WSSPM_RBT_H_
#define _WSSPM_RBT_H_

#include "capwap.h"
#include "wsspm.h"

#define WSSPM_RB_GREATER 1
#define WSSPM_RB_EQUAL   0
#define WSSPM_RB_LESS   -1
#define WSSPM_WORD_LEN  32

#define PM_TASK_NAME                         "PMST"
#define PM_TASK_PRIORITY                      100
#define PM_DISC_STACK_SIZE                  OSIX_DEFAULT_STACK_SIZE


#define PM_STATS_RX_WTP_EV_RQ_MSGQ_EVENT 0x00000001
#define WSSPM_TMR_EXP_EVENT              0x00000010
#define PM_STATS_WLC_CAPWAP_STATS_UPDATE_EVENT             0x00000100
#define PM_STATS_WLC_CAPWAP_RUN_STATS_UPDATE_EVENT             0x00001000
/* #define PM_STATS_RX_LOCAL_TIME_EXP_MSGQ_EVENT 0x00000002 */

#define WTP_STATS_TIMER_TYPE                        1
#define WTP_MAX_STATS_TIMER_TIMEOUT                 5
#define MAX_WSSPM_AP_RUNSTATS      NUM_OF_AP_SUPPORTED
#define MAX_WSSPM_WTPID_TABLE      NUM_OF_AP_SUPPORTED
#define MAX_WSSPM_WTPIDRADID_TABLE MAX_NUM_OF_RADIOS
#define MAX_WSSPM_WLANBSSID_TABLE  MAX_NUM_OF_RADIOS * MAX_NUM_OF_BSSID_PER_RADIO
#define MAX_WSSPM_STATTIMER_TABLE  NUM_OF_AP_SUPPORTED
#define MAX_WSSPM_STAT_TABLE       NUM_OF_AP_SUPPORTED
#define MAX_WSSPM_PMINFO_TABLE      200
#define MAX_WSSPM_LOCAL_STAT_TABLE  200
#define MAX_CLIENT_COUNT    MAX_NUM_OF_STA_PER_WLC

#define PM_FSWTPIDSTATSTABLE_POOL\
 WSSPMMemPoolIds[MAX_WSSPM_WTPID_TABLE_SIZING_ID]
#define PM_FSWTPIDRADIDSTATSTABLE_POOL\
        WSSPMMemPoolIds[MAX_WSSPM_WTPIDRADID_TABLE_SIZING_ID]
#define PM_FSWTPIDBSSIDSTATSTABLE_POOL\
        WSSPMMemPoolIds[MAX_WSSPM_WLANBSSID_TABLE_SIZING_ID]
#define PM_FSWTPMSTATINFOTABLE_POOL\
        WSSPMMemPoolIds[MAX_WSSPM_STATTIMER_TABLE_SIZING_ID]
#define PM_FSWTPMLOCAL_STATS_TABLE_POOL\
        WSSPMMemPoolIds[MAX_WSSPM_LOCAL_STAT_TABLE_SIZING_ID]
#define PM_FSWTPMWTPEVENT_INFO_TABLE_POOL\
  WSSPMMemPoolIds[MAX_WSSPM_PMINFO_TABLE_SIZING_ID]
#define PM_FSCAPWATP_STATS_TABLE_POOL\
 WSSPMMemPoolIds[NUM_OF_AP_SUPPORTED_SIZING_ID]
#define PM_FSWLANRADIO_STATS_TABLE_POOL\
 WSSPMMemPoolIds[MAX_NUM_OF_RADIOS_SIZING_ID]
#define PM_FSWLANCLIENT_STATS_TABLE_POOL\
 WSSPMMemPoolIds[MAX_CLIENT_COUNT_SIZING_ID]
#define PM_WTPRUN_EVENTS_TABLE_POOL\
 WSSPMMemPoolIds[MAX_WSSPM_AP_RUNSTATS_SIZING_ID]


typedef struct{
tTimerListId        pmWtpStatsTmrListId;
tTmrDesc            aWssPmWtpStatsTmrDesc[1]; 
tTmrBlk             pmWtpStatsTimer;
}tWssPmWtpStatsTimerList;

typedef struct pmWtpInfo{
 tTMO_SLL_NODE NextNode;
 UINT2 wtp_id;
 UINT1 stat_timer;
 UINT1 issTimerTicks;
}tWtpStatTimerInfo;

typedef struct {
 BOOL1       bWssPmWLCCapwapDiscReqRxd;
 BOOL1       bWssPmWLCCapwapDiscRespSent;
 BOOL1       bWssPmWLCCapwapDiscUnsuccReqProcessed;
 BOOL1       bWssPmWLCCapwapDiscReasonLastUnsuccAtt;
 BOOL1       bWssPmWLCCapwapDiscLastSuccAttTime;
 BOOL1       bWssPmWLCCapwapDiscLastUnsuccAttTime;
 BOOL1       bWssPmWLCCapwapJoinReqRxd;
 BOOL1       bWssPmWLCCapwapJoinRespSent;
 BOOL1       bWssPmWLCCapwapJoinUnsuccReqProcessed;
 BOOL1       bWssPmWLCCapwapJoinReasonForLastUnsuccAtt;
 BOOL1       bWssPmWLCCapwapJoinLastSuccAttTime;
 BOOL1       bWssPmWLCCapwapJoinLastUnsuccAttTime;
 BOOL1       bWssPmWLCCapwapCfgRespRcvd;
 BOOL1       bWssPmWLCCapwapCfgReqSent;
 BOOL1       bWssPmWLCCapwapCfgUnsuccRespProcessed;
 BOOL1       bWssPmWLCCapwapCfgReasonForLastUnsuccAtt;
 BOOL1       bWssPmWLCCapwapCfgLastSuccAttTime;
 BOOL1       bWssPmWLCCapwapCfgLastUnsuccAttTime;
 BOOL1       bWssPmWLCCapwapKeepAlivePkts;
 BOOL1       bConfigUpdateStats;
 BOOL1       bStaConfigStats;
 BOOL1       bClearConfigStats;
 BOOL1       bDataTransferStats;
 BOOL1       bResetStats;
 BOOL1       bPrimaryDiscStats;
 BOOL1       bEchoStats;
 BOOL1       bRowStatus;
 BOOL1       bRunUpdateReqReceived;
 BOOL1       bRunUpdateRspReceived;
 BOOL1       bRunUpdateReqTransmitted;
 BOOL1       bRunUpdateRspTransmitted;
 BOOL1       bRunUpdateunsuccessfulProcessed;
 BOOL1       bRunUpdateReasonLastUnsuccAttempt;
 BOOL1       bRunUpdateLastSuccAttemptTime;
 BOOL1       bRunUpdateLastUnsuccessfulAttemptTime;
 UINT1       u1pad;
} tWssPmWLCCapwapStatsSetDB;

typedef struct {
    BOOL1       bWssPmRbtStatsSet;
    BOOL1       bWssPmRadStatsSet;
    BOOL1       bWssPmIEEE80211StatsSet;
    BOOL1       bWssPmWtpEvtWtpCWStatsSet;
    BOOL1       bWssPmVSPBSSIDStatsSet;
    BOOL1       bWssPmVSPRadioStatsSet;
    BOOL1       bWssPmVSPClientStatsSet;
    BOOL1       bWssPmVSPApStatsSet;
    BOOL1       bWssPmVSPSSIDStatsSet;
    UINT1       au1pad[3];
    tWssPmWLCCapwapStatsSetDB WssPmWLCCapwapStatsSetFlag;
}tWssPmInternalAttributeDB;


typedef enum {
   WTPPRF_ADD,
   WTPPRF_DEL,
   BSSID_ADD,
   BSSID_DEL,
   RADIO_ADD,
   RADIO_DEL,
   SSID_ADD,
   SSID_DEL,
   CLIENT_ADD,
   CLIENT_DEL,
   CAPWATP_ADD,
   CAPWATP_DEL,
   STAT_TIMER_RESET,
   REBOOT_STATS_SET,
   RADIO_STATS_SET,
   IEEE_802_11_STATS_SET,
   CAPWAP_SESS_STATS_SET,
   CAPWAP_WLC_SESS_STATS_SET,
   CAPWAP_WLC_SESS_RUN_STATS_SET,
   VSP_BSSID_STATS_SET,
   VSP_SSID_STATS_SET,
   REBOOT_STATS_GET,
   RADIO_STATS_GET,
   IEEE_802_11_STATS_GET,
   CAPWAP_SESS_STATS_GET,
   VSP_BSSID_STATS_GET,
   VSP_SSID_STATS_GET,
   CLI_ALL_WTP_STATS_GET,
   CLI_WTP_ID_STATS_GET,
   CLI_BSS_ID_STATS_GET,
   CLI_CPWP_DISC_STATS_GET,
   CLI_CPWP_JOIN_STATS_GET,
   CLI_PM_CLEAR_STATS_SET,
   VSP_RADIO_STATS_SET,
   CLIENT_STATS_SET,
   VSP_AP_STATS_SET,
   RADIO_CLIENT_STATS_SET,
   CAPWAP_CAPW_ATP_STATS_SET,
   WSSCFG_CLIENT_STATS_SET,
   WSSCFG_SSID_STATS_SET,
   WSSCFG_RADIO_STATS_SET
}ePmWtpIntEvts;

typedef enum {
   WSS_WTP_FAILURE_UNSUPPORTED,
   WSS_WTP_FAILURE_AC_INIT,
   WSS_WTP_FAILURE_LINK_FAILURE,
   WSS_WTP_FAILURE_SW_FAILURE,
   WSS_WTP_FAILURE_HW_FAILURE,
   WSS_WTP_FAILURE_OTHERS,
   WSS_WTP_FAILURE_UNKNOWN
}eWtpLastFailureType;

typedef enum {
   WSS_RADIO_FAILURE_UNSUPPORTED,
   WSS_RADIO_FAILURE_SW_FAILURE,
   WSS_RADIO_FAILURE_HW_FAILURE,
   WSS_RADIO_FAILURE_OTHERS,
   WSS_RADIO_FAILURE_UNKNOWN
}eRadioLastFailureType;

typedef struct
{
    UINT2 u2RebootCount;
    UINT2 u2AcInitiatedCount;
    UINT2 u2LinkFailCount;
    UINT2 u2SwFailCount;
    UINT2 u2HwFailCount;
    UINT2 u2OtherFailCount;
    UINT2 u2UnknownFailCount;
    UINT1 u1LastFailureType;
    UINT1 au1pad[1];
}tWtpRebootStats;


typedef struct {
UINT4    authReqRcvd;
UINT4    deAuthReqSent;
UINT4    unknownAuthReqRcvd;
UINT4    invalidAuthReqRcvd;
UINT4    authFailureCount;
UINT4    unknownReAuthReqRcvd;
UINT4    invalidReAuthReqRcvd;
UINT4    asscReqRcvd;
UINT4    reAsscReqRcvd;
UINT4    asscFailure;
UINT4    unknownReAsscReqRcvd;
UINT4    invalidReAssocReqRcvd;                 
UINT4    handOffReqRcvd;
UINT4    handOffReqSucc;
UINT4    handOffReqFailure;                     /* Mobility Related Stats */
UINT4    stnReqRejAbvLimit;
UINT4    stnReqAccAbvLimit;        /* Load Balancing Related Stats*/
}tBSSIDStnStats;      /* WLAN BSSID Station Stats collected from WLC context */

typedef struct {
tWtpCapwapSessStats pPmWtpCapwapSessStats;
tBSSIDStnStats pPmWtpBSSIDStnStats;
UINT4 u4BssIfIndex;
UINT2 u2WtpProfileIndex; /* pick this from wlchandler */
UINT1 au1pad[2];
}tWssPmWtpLocalStatsDB;

typedef struct {
tWtpRebootStats     wtpRebootStatistics;
tWtpRadioStats      wtpRadioStatistics;
tDot11Statistics    wtpDot11Statistics;
tWtpCapwapSessStats wtpCWSessStats;
tBSSIDStnStats      wtpBSSIDStnStats;
tBSSIDMacHndlrStats wtpBSSIDStats;
tWtpCPWPWtpStats    wtpCPWPWtpStats;
UINT4               u4BssIfIndex;
UINT2               u2ProfileId;
UINT2               stats_timer;
UINT1               u1RadioId;
UINT1 au1pad[3];
}tWssPMWtpAllStatsInfo;

/******* RB DB Begin **********/
typedef struct {
tRBNodeEmbd      nextWssPmWTPIDStatsProcessNode;
tWtpRebootStats      pWtpRebootStat;
tWtpCapwapSessStats  pWtpCapwapSessStats;
tWtpCPWPWtpStats     CapwapSessStats;
UINT2 u2WtpIntProfileIndex; 
UINT1 au1pad[2];
}tWssPmWTPIDStatsProcessRBDB;   /* RB DB based on WTP Profile ID */

typedef struct
{
tRBNodeEmbd  FsCapwapWirelessBindingTableNode;
UINT4 u4PMWtpProfileId;
UINT4 u4PMWirelessBindingRadioId;
INT4 i4FsPMWrelessBindingVirtualRadioIfIndex;
INT4 i4FsPMWirelessBindingType;
INT4 i4FsPMWirelessBindingRowStatus;
}tPMWtpIDRadIDEntry;

typedef struct {
tRBNodeEmbd      nextWssPmWTPIDRadIDStatsProcessNode;
tWtpRadioStats pWtpRadioStats;
tDot11Statistics pWtpdot11Stats;
UINT4 u4WtpRadioIndex;
}tWssPmWTPIDRadIDStatsProcessRBDB;/* RB DB based on WTP ProfileID and RadioID*/

typedef struct {
UINT4 u4WlanBSSIndex;
tBSSIDMacHndlrStats pBSSIDMacHndlrStats;
tBSSIDStnStats pBSSIDStnStats;
tRBNodeEmbd      nextWssPmWLANBSSIDStatsProcessNode;
}tWssPmWLANBSSIDStatsProcessRBDB;        /* RB DB based on WLAN BSSID */
typedef struct {
tRBNodeEmbd    nextWssPmWlanRadioStatsProcessNode;
tWtpRCStats    pradioClientStats;
UINT4          u4FsWlanRadioIfInOctets;
UINT4          u4FsWlanRadioIfOutOctets;
UINT4          u4FsWlanRadioClear;
INT4           i4FsWlanRadioMaxClientCount;
INT4           i4FsWlanRadioRadioBand;
INT4           i4IfIndex;
tMacAddr       RadiobaseMACAddress;
UINT1          au1Pad[2];
}tWssPmWlanRadioStatsProcessRBDB;

typedef struct{
tRBNodeEmbd         nextWssPmWlanSSIDStatsProcessNode;
tBSSIDMacHndlrStats pSSIDMacHndlrStats;
UINT4               u4FsDot11WlanProfileId;
UINT4               u4FsWlanSSIDStatsAssocClientCount;
UINT4               u4FsWlanSSIDStatsMaxClientCount;
}tWssPmWlanSSIDStatsProcessRBDB;

typedef struct{
tRBNodeEmbd nextWssPmWlanClientStatsProcessNode;
tWtpStaStats   pclientStats;
tIpAddr     FsWlanClientStatsIpAddress;
UINT4       u4FsWlanClientStatsIdleTimeout;
UINT4       u4FsWlanClientStatsBytesReceivedCount;
UINT4       u4FsWlanClientStatsBytesSentCount;
UINT4       u4FsWlanClientStatsAssocTime;
UINT4       u4FsWlanClientStatsSessionTimeLeft;
UINT4       u4FsWlanClientStatsConnTime;
INT4        i4FsWlanClientStatsAuthState;
INT4        i4FsWlanClientStatsToACStatus;
UINT1       au1FsWlanClientStatsRadiusUserName[512];
UINT1       au1FsWlanClientStatsSSID[32];
tMacAddr    FsWlanClientStatsMACAddress;
UINT1       au1Pad[2];
}tWssPmWlanClientStatsProcessRBDB;

typedef  struct{
       tRBNodeEmbd   nextWssPmCapwATPStatsProcessNode;
       tApParams     pApElement;
       UINT4 u4CapwapBaseWtpProfileId;
       UINT4 u4FsCapwATPStatsSentBytes;
       UINT4 u4FsCapwATPStatsRcvdBytes;
       UINT4 u4FsCapwATPStatsWiredSentBytes;
       UINT4 u4FsCapwATPStatsWirelessSentBytes;
       UINT4 u4FsCapwATPStatsWiredRcvdBytes;
       UINT4 u4FsCapwATPStatsWirelessRcvdBytes;
       UINT4 u4FsCapwATPStatsSentTrafficRate;
       UINT4 u4FsCapwATPStatsRcvdTrafficRate;
       UINT4 u4FsCapwATPStatsTotalClient;
       UINT4 u4FsCapwATPStatsMaxClientPerAPRadio;
       UINT4 u4FsCapwATPStatsMaxClientPerSSID;
}tWssPmCapwATPStatsProcessRBDB;

typedef struct {
    UINT4               u2ProfileBSSID;
    UINT2               u2ProfileId;
    UINT2               stats_timer;
    UINT1          u1RadioId;
    UINT1               au1pad[3];
}tWssIfPMInfo;

typedef union {
    tWtpRebootStats       rebootStats;
    tWtpRadioStats        radioStats;
    tDot11Statistics      dot11Stats;
    tWtpCapwapSessStats   capwapSessStats;
    tBSSIDStnStats        bssIdStnStats;
    tBSSIDMacHndlrStats   bssIdMachdlrStats;
    tWtpCPWPWtpStats      wtpCapwapStats;
    tWtpRCStats           radioClientStats;
    tWtpStaStats          clientStats;
    tApParams             ApElement;
} unWssPmWTPEventInfo;

typedef struct {
    tWssPmInternalAttributeDB wssPmIntAttr;
    unWssPmWTPEventInfo  wtpEventInfo;
    UINT4               u4RadioIfIndex;
    UINT4               u2ProfileBSSID;
    UINT4  u4CapwapBaseWtpProfileId;
    UINT4  u4FsDot11WlanProfileId;
    UINT2               stats_timer;
    UINT2               u2ProfileId;
    tMacAddr  FsWlanClientStatsMACAddress;
    UINT1               u1RadioId;
    UINT1               au1pad[1];
}tWssPMInfo;

INT4
WssIfWtpIDStatsEntrySet(UINT2 u4WtpIntProfileIndex, 
                        tWssPmWTPIDStatsProcessRBDB *WssPmWTPIDStats);
INT4
WssIfWtpIDRadIDStatsEntrySet(UINT4 WtpRadioIndex, 
                                   tWtpRadioStats *pWssPmRadioStats);
INT4
WssIfWlanBSSIDStatsEntrySet(UINT4 WlanBSSIndex, 
                            tBSSIDMacHndlrStats *WssPmWLANBSSIDStats);

INT4
WssPmWTPCapwapStatsEntrySet(UINT2 , 
        tWtpCPWPWtpStats *);

INT4
WssIIfIEEEStatsEntrySet(UINT2 , UINT4 , 
                             tDot11Statistics *);
INT4
WssIfWlanSSIDStatsEntrySet (UINT4 u4FsDot11WlanProfileId,
                            tBSSIDMacHndlrStats * WssPmWTPWLANBSSIDStats);
#endif /* _WSSPM_RBT_H_ */
