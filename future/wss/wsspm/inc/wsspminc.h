/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspminc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the files included in
 *        WSSPM module*
 *******************************************************************/
#ifndef _WSSPM_INC_H_
#define _WSSPM_INC_H_
#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "tcp.h"
#include "cli.h"
#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"

#include "wsspmdef.h"
#include "wsspmtmr.h"
#include "wsspmdb.h"
#include "wsspmprot.h"
#include "wsspmwlcmacr.h"
#include "wsspm.h"
#include "wsspmsz.h"
#include "wsspmtrc.h"

#ifdef WSSUSER_WANTED
#include "userinc.h"
#endif
#endif


