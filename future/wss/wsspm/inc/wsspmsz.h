/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsspmsz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the WSSPM sizing  routines. 
 * 
 *****************************************************************************/

enum {
    MAX_CLIENT_COUNT_SIZING_ID,
    MAX_NUM_OF_RADIOS_SIZING_ID,
    MAX_WSSPM_AP_RUNSTATS_SIZING_ID,
    MAX_WSSPM_LOCAL_STAT_TABLE_SIZING_ID,
    MAX_WSSPM_PMINFO_TABLE_SIZING_ID,
    MAX_WSSPM_STATTIMER_TABLE_SIZING_ID,
    MAX_WSSPM_WLANBSSID_TABLE_SIZING_ID,
    MAX_WSSPM_WTPIDRADID_TABLE_SIZING_ID,
    MAX_WSSPM_WTPID_TABLE_SIZING_ID,
    NUM_OF_AP_SUPPORTED_SIZING_ID,
    WSSPM_MAX_SIZING_ID
};


#ifdef  _WSSPMSZ_C
tMemPoolId WSSPMMemPoolIds[ WSSPM_MAX_SIZING_ID];
INT4  WsspmSizingMemCreateMemPools(VOID);
VOID  WsspmSizingMemDeleteMemPools(VOID);
INT4  WsspmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSPMSZ_C  */
extern tMemPoolId WSSPMMemPoolIds[ ];
extern INT4  WsspmSizingMemCreateMemPools(VOID);
extern VOID  WsspmSizingMemDeleteMemPools(VOID);
#endif /*  _WSSPMSZ_C  */


#ifdef  _WSSPMSZ_C
tFsModSizingParams FsWSSPMSizingParams [] = {
{ "tWssPmWlanClientStatsProcessRBDB", "MAX_CLIENT_COUNT", sizeof(tWssPmWlanClientStatsProcessRBDB),MAX_CLIENT_COUNT, MAX_CLIENT_COUNT,0 },
{ "tWssPmWlanRadioStatsProcessRBDB", "MAX_NUM_OF_RADIOS", sizeof(tWssPmWlanRadioStatsProcessRBDB),MAX_NUM_OF_RADIOS, MAX_NUM_OF_RADIOS,0 },
{ "tWssPmWTPRunEvents", "MAX_WSSPM_AP_RUNSTATS", sizeof(tWssPmWTPRunEvents),MAX_WSSPM_AP_RUNSTATS, MAX_WSSPM_AP_RUNSTATS,0 },
{ "tWssPmWtpLocalStatsDB", "MAX_WSSPM_LOCAL_STAT_TABLE", sizeof(tWssPmWtpLocalStatsDB),MAX_WSSPM_LOCAL_STAT_TABLE, MAX_WSSPM_LOCAL_STAT_TABLE,0 },
{ "tWssPMInfo", "MAX_WSSPM_PMINFO_TABLE", sizeof(tWssPMInfo),MAX_WSSPM_PMINFO_TABLE, MAX_WSSPM_PMINFO_TABLE,0 },
{ "tWtpStatTimerInfo", "MAX_WSSPM_STATTIMER_TABLE", sizeof(tWtpStatTimerInfo),MAX_WSSPM_STATTIMER_TABLE, MAX_WSSPM_STATTIMER_TABLE,0 },
{ "tWssPmWLANBSSIDStatsProcessRBDB", "MAX_WSSPM_WLANBSSID_TABLE", sizeof(tWssPmWLANBSSIDStatsProcessRBDB),MAX_WSSPM_WLANBSSID_TABLE, MAX_WSSPM_WLANBSSID_TABLE,0 },
{ "tWssPmWTPIDRadIDStatsProcessRBDB", "MAX_WSSPM_WTPIDRADID_TABLE", sizeof(tWssPmWTPIDRadIDStatsProcessRBDB),MAX_WSSPM_WTPIDRADID_TABLE, MAX_WSSPM_WTPIDRADID_TABLE,0 },
{ "tWssPmWTPIDStatsProcessRBDB", "MAX_WSSPM_WTPID_TABLE", sizeof(tWssPmWTPIDStatsProcessRBDB),MAX_WSSPM_WTPID_TABLE, MAX_WSSPM_WTPID_TABLE,0 },
{ "tWssPmCapwATPStatsProcessRBDB", "NUM_OF_AP_SUPPORTED", sizeof(tWssPmCapwATPStatsProcessRBDB),NUM_OF_AP_SUPPORTED, NUM_OF_AP_SUPPORTED,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSPMSZ_C  */
extern tFsModSizingParams FsWSSPMSizingParams [];
#endif /*  _WSSPMSZ_C  */


