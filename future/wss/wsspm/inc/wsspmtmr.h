/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsspmtmr.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the timer related data structures in
 *        WSSPM module*
 *******************************************************************/
#ifndef _WSSPM_WLC_TMR_H_
#define _WSSPM_WLC_TMR_H_

typedef struct _WSSPMWTPSTATS_TIMER {
tTmrBlk        WssPmWtpStatsTmrBlk;
UINT1          u1Status;
UINT1          au1pad[3];
}tWssPmWtpStatsTmr;

INT4 wssPmStatsTmrInit(VOID);
INT4 pmWtpStatsTmrStart PROTO ((UINT1 ,UINT4));
VOID WssPmWtpStatsTmrExp PROTO((VOID *));
#endif

