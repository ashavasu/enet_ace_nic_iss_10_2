#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# | $Id: make.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                    |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

BCNMGR_BASE_DIR = ${BASE_DIR}/wss/bcnmgr
BCNMGR_INC_DIR  = ${BCNMGR_BASE_DIR}/inc
BCNMGR_SRC_DIR  = ${BCNMGR_BASE_DIR}/src
BCNMGR_OBJ_DIR  = ${BCNMGR_BASE_DIR}/obj
WSSIF_INC_DIR  = ${BASE_DIR}/wss/wssif/inc
RADIOIF_INC_DIR  = ${BASE_DIR}/wss/radioif/inc
WSSWLAN_INC_DIR  = ${BASE_DIR}/wss/wsswlan/inc
WSSMAC_INC_DIR  = ${BASE_DIR}/wss/wssmac/inc
APHDLR_INC_DIR = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR = ${BASE_DIR}/wss/wlchdlr/inc
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
WSSAUTH_INC_DIR = ${BASE_DIR}/wss/wssauth/inc
WSSSTA_INC_DIR = ${BASE_DIR}/wss/wsssta/inc
CFA_INC_DIR = ${BASE_DIR}/cfa2/inc


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${CFA_INC_DIR}	-I${BCNMGR_INC_DIR}	-I${WSSMAC_INC_DIR}	-I${WSSIF_INC_DIR}	-I${WSSWLAN_INC_DIR}	-I${RADIOIF_INC_DIR}	-I${APHDLR_INC_DIR}	-I${WLCHDLR_INC_DIR}	-I${CAPWAP_INC_DIR}	-I${WSSAUTH_INC_DIR}	-I${WSSSTA_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
