/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: bcnmgrmain.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 * Description: This file contains the BCNMGR Main Task Functions.
 *
 ******************************************************************************/
#ifndef __BCNMGRMAIN_C__
#define __BCNMGRMAIN_C__

#include "bcnmgrinc.h"

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrMainTask                                            *
 *                                                                          *
 * Description  : Main function of BCNMGR task.                             *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BcnMgrMainTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;
    BCNMGR_FN_ENTRY ();

    UNUSED_PARAM (pi1Arg);

    if (BcnMgrMainTaskInit () == OSIX_FAILURE)
    {
        BcnMgrMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gBcnMgrGlobalInfo.MainTaskId, BCNMGR_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* This routine handles the events and process them. */
            BcnMgrMainProcessEvent (u4Events);
        }
    }
    BCNMGR_FN_EXIT ();
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrMainTaskInit                                        *
 *                                                                          *
 * Description  : Init function of BCNMGR task.                             *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : OSIX_FAILURE | OSIX_SUCCESS                               *
 *                                                                          *
 ****************************************************************************/
INT4
BcnMgrMainTaskInit (VOID)
{
    MEMSET (&gBcnMgrGlobalInfo, 0, sizeof (tBcnMgrGlobalInfo));
    BCNMGR_FN_ENTRY ();

    /* BCNMGR Semaphore */
    if (OsixSemCrt (BCNMGR_SEM_NAME, &(gBcnMgrGlobalInfo.SemId)) ==
        OSIX_FAILURE)
    {
        BCNMGR_TRC (OS_RESOURCE_TRC | BCNMGR_CRITICAL_TRC,
                    "BcnMgrMainTaskInit: BCNMGR Semaphore creation FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. 
     * So GiveSem is called before using the semaphore. */
    OsixSemGive (gBcnMgrGlobalInfo.SemId);

    /* BCNMGR Task Q */
    if (OsixQueCrt ((UINT1 *) BCNMGR_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    BCNMGR_MAX_Q_DEPTH, &(gBcnMgrGlobalInfo.TaskQId))
        == OSIX_FAILURE)
    {
        BCNMGR_TRC (OS_RESOURCE_TRC | BCNMGR_CRITICAL_TRC,
                    "BcnMgrMainTaskInit: BCNMGR Task Queue creation FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

    if (OsixGetTaskId (SELF, BCNMGR_TASK_NAME, &(gBcnMgrGlobalInfo.MainTaskId))
        == OSIX_FAILURE)
    {
        BCNMGR_TRC (OS_RESOURCE_TRC | BCNMGR_CRITICAL_TRC,
                    "BcnMgrMainTaskInit: BCNMGR Task Creation FAILED!!!\r\n");
        return OSIX_FAILURE;
    }

    if (BcnMgrModuleStart () == OSIX_FAILURE)
    {
        BCNMGR_TRC (OS_RESOURCE_TRC | BCNMGR_CRITICAL_TRC,
                    "BcnMgrMainTaskInit: BCNMGR Module Start FAILED!!!\r\n");
        return OSIX_FAILURE;
    }
    BCNMGR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrMainTaskDeInit                                      *
 *                                                                          *
 * Description  : This function de-inits global data structures of          *
 *                Beacon Manager.                                           *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BcnMgrMainTaskDeInit (VOID)
{
    BCNMGR_FN_ENTRY ();
    BcnMgrModuleShutDown ();

    if (gBcnMgrGlobalInfo.TaskQId != 0)
    {
        OsixQueDel (gBcnMgrGlobalInfo.TaskQId);
        gBcnMgrGlobalInfo.TaskQId = 0;
    }

    if (gBcnMgrGlobalInfo.SemId != 0)
    {
        OsixSemDel (gBcnMgrGlobalInfo.SemId);
        gBcnMgrGlobalInfo.SemId = 0;
    }
    BCNMGR_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrModuleStart                                         *
 *                                                                          *
 * Description  : This function allocates memory pools and inits global     *
 *                data structures of Beacon Manager.                        *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : OSIX_FAILURE | OSIX_SUCCESS                               *
 *                                                                          *
 ****************************************************************************/
INT4
BcnMgrModuleStart (VOID)
{
    UINT1               u1TmrType = 0;
    UINT4               u4TmrInterval = 0;

    BCNMGR_FN_ENTRY ();
    /* Initialize Database Mempools */
    if (BcnmgrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        BCNMGR_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "BcnMgrModuleStart: Memory Initialization FAILED!!!\r\n");
        BcnmgrSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
    /*Assigning the respective mempools */
    BcnMgrAssignMempoolIds ();

    /* Timer Initialization */
    if (BcnMgrTmrInit () == OSIX_FAILURE)
    {
        BCNMGR_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "BcnMgrModuleStart: Timer Initialization FAILED!!!\r\n");
        BcnmgrSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    /* Beacon manager Initialization */
    BeaconMgrInit ();

    gBcnMgrGlobalInfo.u1BcnTaskInitialized = OSIX_TRUE;

#if 0
    BcnMgrTmrStart (u1TmrType, u4TmrInterval);
#endif

    BCNMGR_FN_EXIT ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : BeaconMgrInit                                             *
 *                                                                          *
 * Description  : This function Initializes beacon manager with default     * 
 *                values.                                                   *
 *                                                                          *
 * Input        : gBeaconMacFrame                                           *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BeaconMgrInit ()
{
    BCNMGR_FN_ENTRY ();
    MEMSET (&gBeaconMacFrame, 0, sizeof (tDot11BeaconMacFrame));

    MEMSET (&gBcnMgrBcnParams, 0, sizeof (tBcnMgrBcnParams));

    MEMCPY (gBeaconMacFrame.TimeStamp.au1MacFrameTimestamp,
            BCNMGR_DEF_TIMESTAMP, STRLEN (BCNMGR_DEF_TIMESTAMP));
    gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt = BCNMGR_DEF_BEACON_INT;
    gBeaconMacFrame.Capability.u2MacFrameCapability = BCNMGR_DEF_CAPABILITY;

    gBeaconMacFrame.Ssid.u1MacFrameElemId = WSSMAC_SSID_ELEMENT_ID;
    gBeaconMacFrame.Ssid.u1MacFrameElemLen = (UINT1) STRLEN (BCNMGR_DEF_SSID);
    MEMCPY (gBeaconMacFrame.Ssid.au1MacFrameSSID, BCNMGR_DEF_SSID,
            STRLEN (BCNMGR_DEF_SSID));

    gBeaconMacFrame.SuppRate.u1MacFrameElemId = WSSMAC_SUPP_RATE_ELEMENT_ID;
    gBeaconMacFrame.SuppRate.u1MacFrameElemLen = BCNMGR_SUPP_RATE_ELEM_LEN;
    MEMCPY (gBeaconMacFrame.SuppRate.au1MacFrameSuppRate, BCNMGR_DEF_SUPP_RATE,
            STRLEN (BCNMGR_DEF_SUPP_RATE));

    gBeaconMacFrame.DSParamSet.u1MacFrameElemId = WSSMAC_DS_PARAM_ELEMENT_ID;
    gBeaconMacFrame.DSParamSet.u1MacFrameElemLen = 1;
    gBeaconMacFrame.DSParamSet.u1MacFrameDS_CurChannel = BCNMGR_DS_CURCHN;

    gBeaconMacFrame.Country.u1MacFrameElemId = WSSMAC_COUNTRY_ELEMENT_ID;
    gBeaconMacFrame.Country.u1MacFrameElemLen = BCNMGR_COUNTRTY_ELEM_LEN;
    MEMCPY (gBeaconMacFrame.Country.au1MacFrameCountryStr, BCNMGR_DEF_COUNTRY,
            STRLEN (BCNMGR_DEF_COUNTRY));
    gBeaconMacFrame.Country.u1MacFrameFirChanlNum_RegExtnId = 1;
    gBeaconMacFrame.Country.u1MacFrameNumOfChanl_RegClass = 1;
    gBeaconMacFrame.Country.u1MacFrameMaxTxPowLev_CovClass = 1;

    gBeaconMacFrame.PowConstraint.u1MacFrameElemId = WSSMAC_POWCONST_ELEMENT_ID;
    gBeaconMacFrame.PowConstraint.u1MacFrameElemLen = 1;
    gBeaconMacFrame.PowConstraint.u1MacFrameLocPowConst = 1;

    gBeaconMacFrame.EDCAParam.u1MacFrameElemId = WSSMAC_EDCA_PARAM_ELEMENT_ID;
    gBeaconMacFrame.EDCAParam.u1MacFrameElemLen = BCNMGR_EDCA_PARAM_ELEM_LEN;
    gBeaconMacFrame.EDCAParam.u1MacFrameEDCA_QosInfo = 1;

    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ACI_AFSN = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BE.
        u1MacFrameEDCA_AC_ECWMin_Max = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BE.
        u2MacFrameEDCA_AC_TxOpLimit = 1;

    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ACI_AFSN = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BK.
        u1MacFrameEDCA_AC_ECWMin_Max = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BK.
        u2MacFrameEDCA_AC_TxOpLimit = 1;

    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ACI_AFSN = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VI.
        u1MacFrameEDCA_AC_ECWMin_Max = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VI.
        u2MacFrameEDCA_AC_TxOpLimit = 1;

    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ACI_AFSN = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VO.
        u1MacFrameEDCA_AC_ECWMin_Max = 1;
    gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VO.
        u2MacFrameEDCA_AC_TxOpLimit = 1;

    BCNMGR_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrMainProcessEvent                                    *
 *
 * Description  : This function processes the events and calls the          * 
 *                event handler routines of BCNMGR module.                  *
 *                                                                          *
 * Input        : u4Event - Events                                          *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BcnMgrMainProcessEvent (UINT4 u4Events)
{
    BCNMGR_FN_ENTRY ();
    BcnMgrLock ();

    if (u4Events & BCNMGR_TIMER_EXP_EVENT)
    {
        BCNMGR_TRC (CONTROL_PLANE_TRC,
                    "BcnMgrMainProcessEvent: Recvd Tmr Exp Event\r\n");
        BcnMgrTmrExpHandler ();
    }

    if (u4Events & BCNMGR_QMSG_EVENT)
    {
        BCNMGR_TRC (CONTROL_PLANE_TRC,
                    "BcnMgrMainProcessEvent: Recvd Q Event\r\n");
        BcnMgrQueMsgHandler ();
    }

    BcnMgrUnlock ();

    BCNMGR_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrModuleShutDown                                      *
 *                                                                          *
 * Description  : This function shuts down the BCNMGR module.               *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BcnMgrModuleShutDown (VOID)
{
    tBcnMgrQMsg        *pBcnMgrMsg = NULL;

    BCNMGR_FN_ENTRY ();
    BcnMgrLock ();

    if (gBcnMgrGlobalInfo.u1BcnTaskInitialized == OSIX_FALSE)
    {
        BcnMgrUnlock ();
        return;
    }

    gBcnMgrGlobalInfo.u1BcnTaskInitialized = OSIX_FALSE;

    while (OsixQueRecv (gBcnMgrGlobalInfo.TaskQId, (UINT1 *) &pBcnMgrMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        BcnMgrMainReleaseQMemory (pBcnMgrMsg);
    }

    if (BcnMgrTmrDeInit () == OSIX_FAILURE)
    {
        BCNMGR_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    "BcnMgrModuleShutDown: Timer DeInit FAILED !!!\r\n");
    }

    BcnmgrSizingMemDeleteMemPools ();

    BcnMgrUnlock ();

    BCNMGR_FN_EXIT ();

    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrAssignMempoolIds                                    *
 *                                                                          *
 * Description  : This function assigns memory pool IDs.                    *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BcnMgrAssignMempoolIds (VOID)
{
    BCNMGR_FN_ENTRY ();
    gBcnMgrGlobalInfo.QMsgPoolId = BCNMGRMemPoolIds[MAX_BCNMGR_Q_MSG_SIZING_ID];
    BCNMGR_FN_EXIT ();
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrMainReleaseQMemory                                  *
 *                                                                          *
 * Description  : This function releases the Q memory pools.                *
 *                                                                          *
 * Input        : pBcnMgrMsg - Queue Memory                                 *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
BcnMgrMainReleaseQMemory (tBcnMgrQMsg * pBcnMgrMsg)
{
    BCNMGR_FN_ENTRY ();
    if (pBcnMgrMsg->unMsgParam.BcnMacFrame.pBcnMacPdu != NULL)
    {
        /* Release the CRU buffer of the Beacon PDU enqueued by WSS MAC */
        CRU_BUF_Release_MsgBufChain
            (pBcnMgrMsg->unMsgParam.BcnMacFrame.pBcnMacPdu, FALSE);
    }

    MemReleaseMemBlock (gBcnMgrGlobalInfo.QMsgPoolId, (UINT1 *) pBcnMgrMsg);
    BCNMGR_FN_EXIT ();
    return;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrLock                                                *
 *                                                                          *
 * Description  : This function is to take BcnMgr mutex semaphore.          *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
BcnMgrLock (VOID)
{
    BCNMGR_FN_ENTRY ();
    if (OsixSemTake (gBcnMgrGlobalInfo.SemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BCNMGR_FN_EXIT ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrUnlock                                              *
 *                                                                          *
 * Description  : This function is to release BcnMgr mutex semaphore.       *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
BcnMgrUnlock (VOID)
{
    BCNMGR_FN_ENTRY ();

    if (OsixSemGive (gBcnMgrGlobalInfo.SemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    BCNMGR_FN_EXIT ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : BcnMgrEnqueCtrlPkts                                       *
 *                                                                          *
 * Description  : This function is to enque control packets to beacon mgr   *
 *                task                                                      *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
INT4
BcnMgrEnqueCtrlPkts (tBcnMgrMsgStruct * pBcnMgrMsg)
{

    INT1                i4Status = OSIX_SUCCESS;
    tBcnMgrQMsg        *pBcnMgrQMsg = NULL;

    BCNMGR_FN_ENTRY ();

    pBcnMgrQMsg = (tBcnMgrQMsg *) MemAllocMemBlk (gBcnMgrGlobalInfo.QMsgPoolId);
    if (pBcnMgrQMsg == NULL)
    {
        BCNMGR_TRC (BCNMGR_FAILURE_TRC, "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }
    pBcnMgrQMsg->u4MsgType = BCNMGR_RECV_CONF_UPDATE;
    MEMCPY (&(pBcnMgrQMsg->unMsgParam.BcnParams),
            &(pBcnMgrMsg->unBcn.BcnMgrBcnParams), sizeof (tBcnMgrBcnParams));

    switch (pBcnMgrQMsg->u4MsgType)
    {
        case BCNMGR_RECV_CONF_UPDATE:
        {
            if (OsixQueSend (gBcnMgrGlobalInfo.TaskQId,
                             (UINT1 *) &pBcnMgrQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                            "Failed to send the message to CTRL Rx queue \r\n");
                MemReleaseMemBlock (gBcnMgrGlobalInfo.QMsgPoolId,
                                    (UINT1 *) pBcnMgrQMsg);
                return OSIX_FAILURE;
            }
            /*post event to Service Task */
            if (OsixEvtSend (gBcnMgrGlobalInfo.MainTaskId,
                             BCNMGR_QMSG_EVENT) == OSIX_FAILURE)
            {
                BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                            "BcnMgrEnqueCtrlPkts: Failed to send Event \r\n");
                i4Status = OSIX_FAILURE;
            }
        }
            break;
        default:
            break;
    }

    BCNMGR_FN_EXIT ();

    return i4Status;
}

#endif /* BCNMGRMAIN_C */
