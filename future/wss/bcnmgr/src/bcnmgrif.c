/*****************************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Description: This file contains the BCNMGR Sizing Functions.
* $Id: bcnmgrif.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
******************************************************************************/

#ifndef _BCNMGR_IF_C
#define _BCNMGR_IF_C

#include "bcnmgrinc.h"

/************************************************************************/ 
/*  Function Name   : BcnMgrProcessWssIfMsg                             */ 
/*  Description     : The function processes the configuration updates  */ 
/*                    from Radio/WLAN modules                           */
/*  Input(s)        : eMsgType - Message Type                           */ 
/*                    pBcnMgrMsg - Pointer to the message to be enqueued*/
/*  Output(s)       : None.                                             */ 
/*  Returns         : OSIX_FAILURE | OSIX_SUCCESS                       */ 
/************************************************************************/
INT4
BcnMgrProcessWssIfMsg (INT4 eMsgType, tBcnMgrMsgStruct *pBcnMgrMsg)
{
    INT1 i4Status = OSIX_SUCCESS;
    BCNMGR_FN_ENTRY();

    switch (eMsgType)
    {
        case BCNMGR_RECV_CONF_UPDATE:
            {
                if (BcnMgrEnqueCtrlPkts (pBcnMgrMsg) != OSIX_SUCCESS)
                {
                    BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                            "BcnMgrEnqueCtrlPkts: Failed to Enque control"
                            "packets \r\n");
                    i4Status = OSIX_FAILURE;
                }
                break;
            }
        default:
            break;
    }
    BCNMGR_FN_EXIT();
    return i4Status;
}

#endif /* _BCNMGR_IF_C */

