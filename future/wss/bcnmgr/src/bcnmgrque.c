
#ifndef _BCNMGR_QUE_C_
#define _BCNMGR_QUE_C_

#include "bcnmgrinc.h"

/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrque.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains beacon manager queue management routines. 
 * 
 *****************************************************************************/

/***************************************************************************
 * FUNCTION NAME    : BcnMgrQueMsgHandler                                  *
 *                                                                         *
 * DESCRIPTION      : Function is used to process the BCNMGR Q msgs.       *
 *                                                                         *
 * INPUT            : NONE                                                 *
 *                                                                         *
 * OUTPUT           : NONE                                                 *
 *                                                                         *
 * RETURNS          : NONE                                                 *
 *                                                                         *
 ***************************************************************************/
VOID
BcnMgrQueMsgHandler (VOID)
{
    tBcnMgrQMsg        *pMsg = NULL;
    BCNMGR_FN_ENTRY ();

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gBcnMgrGlobalInfo.TaskQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->u4MsgType)
        {
            case BCNMGR_RECV_BEACON_FRAME:
            {
                BcnMgrQueHandleBcnFrame (pMsg);
                CRU_BUF_Release_MsgBufChain (pMsg->unMsgParam.BcnMacFrame.
                                             pBcnMacPdu, FALSE);
            }
                break;
            case BCNMGR_RECV_CONF_UPDATE:
            {
                BcnMgrQueHandleConfUpdate (pMsg);
            }
                break;
            default:
            {
                BCNMGR_TRC (CONTROL_PLANE_TRC,
                            "BcnMgrQueMsgHandler: Unknown message type"
                            "received\r\n");
            }
                break;
        }

        /* Release the buffer to pool */
        if (MemReleaseMemBlock (gBcnMgrGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            == MEM_FAILURE)
        {
            BCNMGR_TRC (BCNMGR_CRITICAL_TRC | ALL_FAILURE_TRC,
                        "BcnMgrQueMsgHandler: Free MemBlock QMsg FAILED!!!\r\n");
            return;
        }
    }

    BCNMGR_FN_EXIT ();

    return;
}

/***************************************************************************
 * FUNCTION NAME    : BcnMgrQueHandleBcnFrame
 *
 * DESCRIPTION      : Function is used to process the Rx Beacon Frame.
 *
 * INPUT            : tBcnMgrQMsg *pMsg
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
VOID
BcnMgrQueHandleBcnFrame (tBcnMgrQMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    BCNMGR_TRC (CONTROL_PLANE_TRC,
                "BcnMgrQueHandleBcnFrame: Received Beacon Frame from MAC."
                "Ignoring it for now. \r\n");

    /* In Phase1, we just ignore the received Beacon Frame.
     * We need to process the same when RRM is handled.
     */
}

/***************************************************************************
 * FUNCTION NAME    : BcnMgrQueHandleConfUpdate
 *
 * DESCRIPTION      : Function is used to process the Configuration
 *                    update message.
 *
 * INPUT            : tBcnMgrQMsg *pMsg
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
VOID
BcnMgrQueHandleConfUpdate (tBcnMgrQMsg * pMsg)
{
    UINT2               u2Count = 0;
    UINT1               u1TmrType = 0;
    UINT4               u4TmrInterval = 0;
    UINT2               u2Len = 0;
    tWssMacMsgStruct    BeaconFrameBuf;
    unApHdlrMsgStruct   ApHdlrMsgStruct;

    BCNMGR_FN_ENTRY ();

    BCNMGR_TRC (CONTROL_PLANE_TRC,
                "BcnMgrQueHandleConfUpdate: Received Conf Update Msg.\r\n");

    /* gBcnMgrBcnParams contains the values of Beacon Parameters.
     * This should be used to verify if there is any difference in the value of the current update
     * verus the value of already existing.  Only if there is a change this Beacon Manager shall proceed to use 
     * the incoming values */

    if (MEMCMP
        (&gBcnMgrBcnParams, &(pMsg->unMsgParam.BcnParams),
         sizeof (tBcnMgrBcnParams)) != 0)
    {

        MEMSET (&BeaconFrameBuf, 0, sizeof (tWssMacMsgStruct));
        MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));

        MEMCPY (gBeaconMacFrame.MacMgmtFrmHdr.u1BssId,
                pMsg->unMsgParam.BcnParams.u1Bssid, WSSMAC_MAC_ADDR_LEN);

        gBeaconMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl = BCNMGR_FRAME_CONTROL;

        gBeaconMacFrame.Capability.u2MacFrameCapability
            = pMsg->unMsgParam.BcnParams.u2Capability;

        gBeaconMacFrame.Ssid.u1MacFrameElemId = WSSMAC_SSID_ELEMENT_ID;
        gBeaconMacFrame.Ssid.u1MacFrameElemLen
            = (UINT1) STRLEN (pMsg->unMsgParam.BcnParams.au1Ssid);
        MEMCPY (gBeaconMacFrame.Ssid.au1MacFrameSSID,
                pMsg->unMsgParam.BcnParams.au1Ssid,
                STRLEN (pMsg->unMsgParam.BcnParams.au1Ssid));

        gBeaconMacFrame.TIMParamSet.u1MacFrameElemId = WSSMAC_TIM_ELEMENT_ID;
        gBeaconMacFrame.TIMParamSet.u1MacFrameElemLen = WSSMAC_TIM_LEN;
        gBeaconMacFrame.TIMParamSet.u1MacFrameTIM_DtimPer
            = pMsg->unMsgParam.BcnParams.u1DtimPeriod;

        gBeaconMacFrame.PowConstraint.u1MacFrameElemId =
            WSSMAC_POWCONST_ELEMENT_ID;
        gBeaconMacFrame.PowConstraint.u1MacFrameElemLen = 1;
        gBeaconMacFrame.PowConstraint.u1MacFrameLocPowConst
            = pMsg->unMsgParam.BcnParams.u1LocPowConstraint;

        gBeaconMacFrame.EDCAParam.u1MacFrameEDCA_QosInfo
            = pMsg->unMsgParam.BcnParams.u1EdcaQosInfo;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BE.u1MacFrameEDCA_AC_ACI_AFSN
            = pMsg->unMsgParam.BcnParams.u1Edca_AC_BE_AciAfsn;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BE.
            u1MacFrameEDCA_AC_ECWMin_Max =
            pMsg->unMsgParam.BcnParams.u1Edca_AC_BE_EcwMinMax;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BE.
            u2MacFrameEDCA_AC_TxOpLimit =
            pMsg->unMsgParam.BcnParams.u2Edca_AC_BE_TxOpLimit;

        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BK.u1MacFrameEDCA_AC_ACI_AFSN
            = pMsg->unMsgParam.BcnParams.u1Edca_AC_BK_AciAfsn;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BK.
            u1MacFrameEDCA_AC_ECWMin_Max =
            pMsg->unMsgParam.BcnParams.u1Edca_AC_BK_EcwMinMax;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_BK.
            u2MacFrameEDCA_AC_TxOpLimit =
            pMsg->unMsgParam.BcnParams.u2Edca_AC_BK_TxOpLimit;

        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VI.u1MacFrameEDCA_AC_ACI_AFSN
            = pMsg->unMsgParam.BcnParams.u1Edca_AC_VI_AciAfsn;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VI.
            u1MacFrameEDCA_AC_ECWMin_Max =
            pMsg->unMsgParam.BcnParams.u1Edca_AC_VI_EcwMinMax;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VI.
            u2MacFrameEDCA_AC_TxOpLimit =
            pMsg->unMsgParam.BcnParams.u2Edca_AC_VI_TxOpLimit;

        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VO.u1MacFrameEDCA_AC_ACI_AFSN
            = pMsg->unMsgParam.BcnParams.u1Edca_AC_VO_AciAfsn;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VO.
            u1MacFrameEDCA_AC_ECWMin_Max =
            pMsg->unMsgParam.BcnParams.u1Edca_AC_VO_EcwMinMax;
        gBeaconMacFrame.EDCAParam.MacFrameEDCA_AC_VO.
            u2MacFrameEDCA_AC_TxOpLimit =
            pMsg->unMsgParam.BcnParams.u2Edca_AC_VO_TxOpLimit;

        gBeaconMacFrame.QosCap.u1MacFrameQosInfo
            = pMsg->unMsgParam.BcnParams.u1QosInfo;

        gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt
            = pMsg->unMsgParam.BcnParams.u2BeaconPeriod;

        gBeaconMacFrame.SuppRate.u1MacFrameElemId = WSSMAC_SUPP_RATE_ELEMENT_ID;
        gBeaconMacFrame.SuppRate.u1MacFrameElemLen
            = (UINT1) STRLEN (pMsg->unMsgParam.BcnParams.au1SuppRate);
        MEMCPY (gBeaconMacFrame.SuppRate.au1MacFrameSuppRate,
                pMsg->unMsgParam.BcnParams.au1SuppRate,
                STRLEN (pMsg->unMsgParam.BcnParams.au1SuppRate));

        gBeaconMacFrame.DSParamSet.u1MacFrameElemId =
            WSSMAC_DS_PARAM_ELEMENT_ID;
        gBeaconMacFrame.DSParamSet.u1MacFrameElemLen = 1;
        gBeaconMacFrame.DSParamSet.u1MacFrameDS_CurChannel
            = pMsg->unMsgParam.BcnParams.u1DsCurChannel;

        gBeaconMacFrame.Country.u1MacFrameElemId = WSSMAC_COUNTRY_ELEMENT_ID;
        gBeaconMacFrame.Country.u1MacFrameElemLen = 6;
        u2Count = 0;
        while (u2Count < WSSMAC_MAX_COUNTRY_STR_LEN)
        {
            gBeaconMacFrame.Country.au1MacFrameCountryStr[u2Count]
                = pMsg->unMsgParam.BcnParams.au1CountryStr[u2Count];
            u2Count++;
        }
        gBeaconMacFrame.Country.u1MacFrameFirChanlNum_RegExtnId
            = pMsg->unMsgParam.BcnParams.u1FirstChanlNum;
        gBeaconMacFrame.Country.u1MacFrameMaxTxPowLev_CovClass
            = pMsg->unMsgParam.BcnParams.u1MaxTxPowerLevel;
        gBeaconMacFrame.Country.u1MacFrameNumOfChanl_RegClass
            = pMsg->unMsgParam.BcnParams.u1NumOfChanls;

        if (pMsg->unMsgParam.BcnParams.u1ExtSupport ==
            WSSMAC_EXT_SUPPORT_ENABLE)
        {
            gBeaconMacFrame.ExtSuppRates.u1MacFrameElemId =
                WSSMAC_EXT_SUPP_RATE_ELEMENTID;
            gBeaconMacFrame.ExtSuppRates.u1MacFrameElemLen
                = (UINT1) STRLEN (pMsg->unMsgParam.BcnParams.au1ExtSuppRates);
            MEMCPY (gBeaconMacFrame.ExtSuppRates.au1MacFrameExtSuppRates,
                    pMsg->unMsgParam.BcnParams.au1ExtSuppRates,
                    STRLEN (pMsg->unMsgParam.BcnParams.au1ExtSuppRates));
        }

        if (pMsg->unMsgParam.BcnParams.u1Dot11Support == DOT11N_SUPPORT_ENABLE)
        {
            gBeaconMacFrame.HTCapabilities.u1MacFrameElemId =
                WSSMAC_HT_CAPAB_ELEMENT_ID;
            gBeaconMacFrame.HTCapabilities.u1MacFrameElemLen
                = WSSMAC_HT_CAPAB_ELEMENT_LEN;
            MEMCPY (&gBeaconMacFrame.HTCapabilities.u2HTCapInfo,
                    &pMsg->unMsgParam.BcnParams.HTCapabilities.u2HTCapInfo,
                    sizeof (pMsg->unMsgParam.BcnParams.HTCapabilities.
                            u2HTCapInfo));
            MEMCPY (&gBeaconMacFrame.HTCapabilities.u1AMPDUParam,
                    &pMsg->unMsgParam.BcnParams.HTCapabilities.u1AMPDUParam,
                    sizeof (pMsg->unMsgParam.BcnParams.HTCapabilities.
                            u1AMPDUParam));
            MEMCPY (gBeaconMacFrame.HTCapabilities.au1SuppMCSSet,
                    pMsg->unMsgParam.BcnParams.HTCapabilities.au1SuppMCSSet,
                    WSSMAC_SUPP_MCS_SET);
            MEMCPY (&gBeaconMacFrame.HTCapabilities.u2HTExtCap,
                    &pMsg->unMsgParam.BcnParams.HTCapabilities.u2HTExtCap,
                    sizeof (pMsg->unMsgParam.BcnParams.HTCapabilities.
                            u2HTExtCap));
            MEMCPY (&gBeaconMacFrame.HTCapabilities.u4TranBeamformCap,
                    &pMsg->unMsgParam.BcnParams.HTCapabilities.
                    u4TranBeamformCap,
                    sizeof (pMsg->unMsgParam.BcnParams.HTCapabilities.
                            u4TranBeamformCap));
            MEMCPY (&gBeaconMacFrame.HTCapabilities.u1ASELCap,
                    &pMsg->unMsgParam.BcnParams.HTCapabilities.u1ASELCap,
                    sizeof (pMsg->unMsgParam.BcnParams.HTCapabilities.
                            u1ASELCap));

            gBeaconMacFrame.HTOperation.u1MacFrameElemId =
                WSSMAC_HT_OPE_ELEMENT_ID;
            gBeaconMacFrame.HTOperation.u1MacFrameElemLen
                = WSSMAC_HT_OPE_ELEMENT_LEN;
            MEMCPY (&gBeaconMacFrame.HTOperation.u1PrimaryCh,
                    &pMsg->unMsgParam.BcnParams.HTOperation.u1PrimaryCh,
                    sizeof (pMsg->unMsgParam.BcnParams.HTOperation.
                            u1PrimaryCh));
            MEMCPY (gBeaconMacFrame.HTOperation.au1HTOpeInfo,
                    pMsg->unMsgParam.BcnParams.HTOperation.au1HTOpeInfo,
                    WSSMAC_HTOPE_INFO);
            MEMCPY (gBeaconMacFrame.HTOperation.au1BasicMCSSet,
                    pMsg->unMsgParam.BcnParams.HTOperation.au1BasicMCSSet,
                    WSSMAC_BASIC_MCS_SET);
        }

        if (pMsg->unMsgParam.BcnParams.u1KeyStatus == 1)
        {
            gBeaconMacFrame.RSNInfo.u1KeyStatus = 1;
            gBeaconMacFrame.RSNInfo.u1MacFrameElemId = WSSMAC_RSN_ELEMENT_ID;
            gBeaconMacFrame.RSNInfo.u2MacFrameRSN_Version
                = pMsg->unMsgParam.BcnParams.u2RSN_Version;

#ifdef QORIQ_WANTED
            WSSMAC_SWAP_2BYTES (gBeaconMacFrame.RSNInfo.u2MacFrameRSN_Version);
#endif

            u2Len = u2Len + sizeof (pMsg->unMsgParam.BcnParams.u2RSN_Version);
            gBeaconMacFrame.RSNInfo.u4MacFrameRSN_GrpCipherSuit
                = pMsg->unMsgParam.BcnParams.u4RSN_GrpCipherSuit;
            u2Len =
                u2Len + sizeof (pMsg->unMsgParam.BcnParams.u4RSN_GrpCipherSuit);
            gBeaconMacFrame.RSNInfo.u2MacFrameRSN_PairCiphSuitCnt =
                pMsg->unMsgParam.BcnParams.u2RSN_PairCiphSuitCnt;

#ifdef QORIQ_WANTED
            WSSMAC_SWAP_2BYTES (gBeaconMacFrame.RSNInfo.
                                u2MacFrameRSN_PairCiphSuitCnt);
#endif

            u2Len =
                u2Len +
                sizeof (pMsg->unMsgParam.BcnParams.u2RSN_PairCiphSuitCnt);
            u2Count = 0;
            while (u2Count < WSSMAC_MAX_CIPH_SUIT_CNT)
            {
                MEMCPY (gBeaconMacFrame.RSNInfo.
                        aMacFrameRSN_PairCiphSuitList[u2Count].
                        au1MacFrameRSN_CiphSuit_OUI,
                        pMsg->unMsgParam.BcnParams.
                        aRSN_PairCiphSuitList[u2Count].
                        au1MacFrameRSN_CiphSuit_OUI, WSSMAC_MAX_OUI_SIZE);
                u2Len = u2Len + sizeof (tWssMacRSN_CiphSuit);
                gBeaconMacFrame.RSNInfo.aMacFrameRSN_PairCiphSuitList[u2Count].
                    u1MacFrameRSN_CiphSuit_Type =
                    pMsg->unMsgParam.BcnParams.aRSN_PairCiphSuitList[u2Count].
                    u1MacFrameRSN_CiphSuit_Type;

                u2Count++;
            }
            gBeaconMacFrame.RSNInfo.u2MacFrameRSN_AKMSuitCnt
                = pMsg->unMsgParam.BcnParams.u2RSN_AKMSuitCnt;

#ifdef QORIQ_WANTED
            WSSMAC_SWAP_2BYTES (gBeaconMacFrame.RSNInfo.
                                u2MacFrameRSN_AKMSuitCnt);
#endif

            u2Len =
                u2Len + sizeof (pMsg->unMsgParam.BcnParams.u2RSN_AKMSuitCnt);
            MEMCPY (gBeaconMacFrame.RSNInfo.aMacFrameRSN_AKMSuitList,
                    pMsg->unMsgParam.BcnParams.au4RSN_AKMSuitList,
                    sizeof (pMsg->unMsgParam.BcnParams.au4RSN_AKMSuitList));
            u2Len =
                u2Len + sizeof (pMsg->unMsgParam.BcnParams.au4RSN_AKMSuitList);

            gBeaconMacFrame.RSNInfo.u2MacFrameRSN_Capability
                = pMsg->unMsgParam.BcnParams.u2RSN_Capability;
            u2Len =
                u2Len + sizeof (pMsg->unMsgParam.BcnParams.u2RSN_Capability);

#ifdef QORIQ_WANTED
            WSSMAC_SWAP_2BYTES (gBeaconMacFrame.RSNInfo.
                                u2MacFrameRSN_Capability);
#endif

/*        gBeaconMacFrame.RSNInfo.u2MacFrameRSN_PMKIdCnt
           = pMsg->unMsgParam.BcnParams.u2RSN_PMKIdCnt;
*/
/*
#ifdef QORIQ_WANTED
        WSSMAC_SWAP_2BYTES (gBeaconMacFrame.RSNInfo.u2MacFrameRSN_PMKIdCnt);
#endif
*/

/*    u2Len = u2Len +sizeof(pMsg->unMsgParam.BcnParams.u2RSN_PMKIdCnt);*/
            gBeaconMacFrame.RSNInfo.u1MacFrameElemLen = u2Len;
        }
        else
        {
            gBeaconMacFrame.RSNInfo.u1MacFrameElemId = 0;
        }

        BcnMgrAssembleBeaconFrame (&BeaconFrameBuf);

        if (BeaconFrameBuf.unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
        {
            ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
                pDot11MacPdu =
                BeaconFrameBuf.unMacMsg.MacDot11PktBuf.pDot11MacPdu;
            ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
                BeaconFrameBuf.unMacMsg.MacDot11PktBuf.u4IfIndex;
            ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u1MacType =
                LOCAL_ROUTING_DISABLED;
            if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &ApHdlrMsgStruct)
                != WSSMAC_SUCCESS)
            {
                return;
            }
        }
        /* Store the Configured Beacon Value */
        MEMCPY (&gBcnMgrBcnParams, &(pMsg->unMsgParam.BcnParams),
                sizeof (tBcnMgrBcnParams));
    }

    BCNMGR_FN_EXIT ();
}

#endif
