/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: bcnmgrtmr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 * Description: This file contains the BCNMGR Timer related Functions.
 *
 ******************************************************************************/
#ifndef __BCNMGRTMR_C__
#define __BCNMGRTMR_C__

#include "bcnmgrinc.h"

/*****************************************************************************
 *
 * Function     : BcnMgrTmrInit
 *
 * Description  : This function creates a timer list for the timers
 *                in BCNMGR module.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
INT4
BcnMgrTmrInit (VOID)
{
    BCNMGR_FN_ENTRY ();

    if (TmrCreateTimerList ((CONST UINT1 *) BCNMGR_TASK_NAME,
                            BCNMGR_TIMER_EXP_EVENT, NULL,
                            (tTimerListId *) & (gBcnMgrGlobalInfo.bcnPerTmrId))
        == TMR_FAILURE)
    {
        BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                    "BcnMgrTmrInit: Failed to create the Beacon Manager"
                    "Timer List");
        return OSIX_FAILURE;
    }

    BcnMgrTmrInitTmrDesc ();

    BCNMGR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : BcnMgrTmrDeInit
 *
 * Description  : This function deletes the timer list for the timers
 *                in BCNMGR module.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
INT4
BcnMgrTmrDeInit (VOID)
{
    BCNMGR_FN_ENTRY ();

    if (TmrDeleteTimerList (gBcnMgrGlobalInfo.bcnPerTmrId) == TMR_FAILURE)
    {
        BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                    "BcnMgrTmrInit: Failed to Delete the Beacon Manager"
                    "Timer List");
        return OSIX_FAILURE;
    }

    MEMSET (gBcnMgrGlobalInfo.aBcnPerTmrDesc, 0,
            (sizeof (tTmrDesc) * BCNMGR_MAX_TMR));

    BCNMGR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : BcnMgrTmrInitTmrDesc
 *
 * Description  : This function initializes the timer desc for the timers
 *                in BCNMGR module.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
VOID
BcnMgrTmrInitTmrDesc (VOID)
{
    BCNMGR_FN_ENTRY ();

    gBcnMgrGlobalInfo.aBcnPerTmrDesc[BCNMGR_BCN_PER_TMR].TmrExpFn
        = BcnMgrBcnPerTmrExp;
    gBcnMgrGlobalInfo.aBcnPerTmrDesc[BCNMGR_BCN_PER_TMR].i2Offset = -1;

    BCNMGR_FN_EXIT ();
    return;
}

/*****************************************************************************
 *
 * Function     : BcnMgrBcnPerTmrExp
 *
 * Description  : This function defines the BcnMgr Timer Expiry Function.
 *
 * Input        : VOID *pArg
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
VOID
BcnMgrBcnPerTmrExp (VOID *pArg)
{
    UINT1               u1TmrType = 0;
    UINT4               u4TmrInterval = 0;
    tWssMacMsgStruct    BeaconFrameBuf;
    unApHdlrMsgStruct   ApHdlrMsgStruct;

    MEMSET (&BeaconFrameBuf, 0, sizeof (tWssMacMsgStruct));
    MEMSET (&ApHdlrMsgStruct, 0, sizeof (unApHdlrMsgStruct));

    UNUSED_PARAM (pArg);
    BCNMGR_FN_ENTRY ();

    BCNMGR_TRC (BCNMGR_MGMT_TRC,
                "BcnMgrBcnPerTmrExp: Beacon Period Timer Expired");

    BcnMgrAssembleBeaconFrame (&BeaconFrameBuf);

    if (BeaconFrameBuf.unMacMsg.MacDot11PktBuf.pDot11MacPdu != NULL)
    {
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.pDot11MacPdu =
            BeaconFrameBuf.unMacMsg.MacDot11PktBuf.pDot11MacPdu;
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.u4IfIndex =
            BeaconFrameBuf.unMacMsg.MacDot11PktBuf.u4IfIndex;
        ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.MacDot11PktBuf.
            u1MacType = LOCAL_ROUTING_DISABLED;
        if (WssIfProcessApHdlrMsg (WSS_APHDLR_CFA_TX_BUF, &ApHdlrMsgStruct) !=
            WSSMAC_SUCCESS)
        {
            return;
        }
    }

#if 0
    u1TmrType = BCNMGR_BCN_PER_TMR;
    u4TmrInterval = (UINT4) gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt;

    BcnMgrTmrStart (u1TmrType, u4TmrInterval);
#endif

    BCNMGR_FN_EXIT ();
    return;
}

/*****************************************************************************
 *
 * Function     : BcnMgrTmrExpHandler
 *
 * Description  : This function is called whenever a timer expiry message is
 *                received by Beacon Manager Task.
 *
 * Input        : None
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
VOID
BcnMgrTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset;

    BCNMGR_FN_ENTRY ();

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gBcnMgrGlobalInfo.bcnPerTmrId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        if (u1TimerId < BCNMGR_MAX_TMR)
        {
            i2Offset = gBcnMgrGlobalInfo.aBcnPerTmrDesc[u1TimerId].i2Offset;

            BCNMGR_TRC2 (BCNMGR_MGMT_TRC, "BcnTimerId: %d\tOffset: %d\r\n",
                         u1TimerId, i2Offset);

            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter */
                (*(gBcnMgrGlobalInfo.aBcnPerTmrDesc[u1TimerId].TmrExpFn))
                    (NULL);
            }
            else
            {
                (*(gBcnMgrGlobalInfo.aBcnPerTmrDesc[u1TimerId].TmrExpFn))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }

    BCNMGR_FN_EXIT ();
    return;
}

/* ***************************************************************************
 *                                                                           *
 * Function     : BcnMgrAssembleBeaconFrame                                  *
 *                                                                           *
 * Description  : Assemble probe response structure                          *
 *                                                                           *
 * Input(s)     : Union of struct with probe response structure and char     *
 *                buffer                                                     *
 *                                                                           *
 * Output(s)    : Probe response Char buffer                                 *
 *                                                                           *
 * Returns      : NONE                                                       *
 *                                                                           *
 * ***************************************************************************/
VOID
BcnMgrAssembleBeaconFrame (tWssMacMsgStruct * pBeaconFrameBuf)
{
    UINT1              *pu1ReadPtr = NULL;
    UINT4               u4Len = 0;
    UINT1               pBuf[WSSMAC_MAX_PKT_LEN];
    UINT2               u2Count = 0, u2Len = 0;
    UINT2               u2MacFrameBeaconIntTemp = 0;
    UINT1               u1ElementId = 0;
    tWssWlanDB          WssWlanDBMsg;
    UINT1               DA[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    UINT1              *np = NULL;
    BCNMGR_FN_ENTRY ();
    MEMSET (pBuf, 0, WSSMAC_MAX_PKT_LEN);
    MEMSET (&WssWlanDBMsg, 0, sizeof (tWssWlanDB));

    pu1ReadPtr = pBuf;
    MEMCPY (gBeaconMacFrame.MacMgmtFrmHdr.u1DA, DA, WSSMAC_MAC_ADDR_LEN);

    /* Put the 802.11 MAC Header */
    /* Put Frame control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       gBeaconMacFrame.MacMgmtFrmHdr.u2MacFrameCtrl);
    /* Put Duration ID field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       gBeaconMacFrame.MacMgmtFrmHdr.u2MacFrameDurId);
    /* Put Addr1 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, gBeaconMacFrame.MacMgmtFrmHdr.u1DA,
                       WSSMAC_MAC_ADDR_LEN);
    /* Put Addr2 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, gBeaconMacFrame.MacMgmtFrmHdr.u1BssId,
                       WSSMAC_MAC_ADDR_LEN);
    /* Put Addr3 field */
    WSSMAC_PUT_NBYTES (pu1ReadPtr, gBeaconMacFrame.MacMgmtFrmHdr.u1BssId,
                       WSSMAC_MAC_ADDR_LEN);
    /* Put Sequence control field */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, gBeaconMacFrame.MacMgmtFrmHdr.
                       u2MacFrameSeqCtrl);

    /* Put the 802.11 MAC Frame Body */
    /* Put Timestamp */
    MEMSET (gBeaconMacFrame.TimeStamp.au1MacFrameTimestamp, 0, 8);
    WSSMAC_PUT_NBYTES (pu1ReadPtr,
                       gBeaconMacFrame.TimeStamp.au1MacFrameTimestamp,
                       WSSMAC_TIMESTAMP_LEN);
    np = pu1ReadPtr;
    /* Put Beacon Interval */
#ifdef QORIQ_WANTED
    u2MacFrameBeaconIntTemp = gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt;
    WSSMAC_SWAP_2BYTES (u2MacFrameBeaconIntTemp);
#endif

#if 0
    WSSMAC_PUT_2BYTES (pu1ReadPtr,
                       gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt);
#endif
#ifdef QORIQ_WANTED
    MEMCPY (pu1ReadPtr, &u2MacFrameBeaconIntTemp, sizeof (UINT2));
#else
    MEMCPY (pu1ReadPtr, &gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt,
            sizeof (UINT2));
#endif

/*           *((UINT2 *) ((VOID*)pu1ReadPtr)) = gBeaconMacFrame.BeaconInt.u2MacFrameBeaconInt;*/
    pu1ReadPtr += 2;

    /* Put Capability info */
    WSSMAC_PUT_2BYTES (pu1ReadPtr, gBeaconMacFrame.Capability.
                       u2MacFrameCapability);

    /* Put SSID */
    u1ElementId = gBeaconMacFrame.Ssid.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_SSID_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.Ssid.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.Ssid.u1MacFrameElemLen);
        u2Len = gBeaconMacFrame.Ssid.u1MacFrameElemLen;
        WSSMAC_PUT_NBYTES (pu1ReadPtr, gBeaconMacFrame.Ssid.au1MacFrameSSID,
                           u2Len);
    }
    /* Put Supported rates */
    u1ElementId = gBeaconMacFrame.SuppRate.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_SUPP_RATE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.SuppRate.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.SuppRate.u1MacFrameElemLen);
        u2Len = gBeaconMacFrame.SuppRate.u1MacFrameElemLen;
        u2Count = 0;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.SuppRate.
                              au1MacFrameSuppRate[u2Count]);
            u2Count++;
        }
    }

    /* Put DS parameters */
    u1ElementId = gBeaconMacFrame.DSParamSet.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_DS_PARAM_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.DSParamSet.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.DSParamSet.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.DSParamSet.u1MacFrameDS_CurChannel);
    }
    /* DTIM Element */
    u1ElementId = gBeaconMacFrame.TIMParamSet.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_TIM_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.TIMParamSet.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.TIMParamSet.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.TIMParamSet.u1MacFrameTIM_DtimCnt);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.TIMParamSet.u1MacFrameTIM_DtimPer);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.TIMParamSet.u1MacFrameTIM_BitCtrl);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           gBeaconMacFrame.TIMParamSet.
                           au1MacFrameTIM_PartVirtBitmap, 3);
    }

    /* Put Country info */
    u1ElementId = gBeaconMacFrame.Country.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_COUNTRY_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.Country.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.Country.u1MacFrameElemLen);
        u2Count = 0;
        while (u2Count < WSSMAC_MAX_COUNTRY_STR_LEN)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              gBeaconMacFrame.Country.
                              au1MacFrameCountryStr[u2Count]);
            u2Count++;
        }
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.Country.
                          u1MacFrameFirChanlNum_RegExtnId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.Country.
                          u1MacFrameNumOfChanl_RegClass);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.Country.
                          u1MacFrameMaxTxPowLev_CovClass);
    }

    /* Ext Support rate */
    u1ElementId = gBeaconMacFrame.ExtSuppRates.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_EXT_SUPP_RATE_ELEMENTID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.ExtSuppRates.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.ExtSuppRates.u1MacFrameElemLen);
        u2Len = gBeaconMacFrame.ExtSuppRates.u1MacFrameElemLen;
        u2Count = 0;
        while (u2Count < u2Len)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.ExtSuppRates.
                              au1MacFrameExtSuppRates[u2Count]);
            u2Count++;
        }
    }

    /* HT capabilities for 802.11n */
    u1ElementId = gBeaconMacFrame.HTCapabilities.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_HT_CAPAB_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.HTCapabilities.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.HTCapabilities.u1MacFrameElemLen);
#ifdef QORIQ_WANTED
        WSSMAC_SWAP_2BYTES (gBeaconMacFrame.HTCapabilities.u2HTCapInfo);
        MEMCPY (pu1ReadPtr, &gBeaconMacFrame.HTCapabilities.u2HTCapInfo,
                sizeof (UINT2));
#else
        MEMCPY (pu1ReadPtr, &gBeaconMacFrame.HTCapabilities.u2HTCapInfo,
                sizeof (UINT2));
#endif
        pu1ReadPtr += 2;
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.HTCapabilities.u1AMPDUParam);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           gBeaconMacFrame.HTCapabilities.au1SuppMCSSet,
                           WSSMAC_SUPP_MCS_SET);
#ifdef QORIQ_WANTED
        WSSMAC_SWAP_2BYTES (gBeaconMacFrame.HTCapabilities.u2HTExtCap);
        MEMCPY (pu1ReadPtr, &gBeaconMacFrame.HTCapabilities.u2HTExtCap,
                sizeof (UINT2));
#else
        MEMCPY (pu1ReadPtr, &gBeaconMacFrame.HTCapabilities.u2HTExtCap,
                sizeof (UINT2));
#endif
        pu1ReadPtr += 2;
        WSSMAC_PUT_4BYTES (pu1ReadPtr,
                           gBeaconMacFrame.HTCapabilities.u4TranBeamformCap);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.HTCapabilities.u1ASELCap);
    }
    /* HT Operation for 802.11n */
    u1ElementId = gBeaconMacFrame.HTOperation.u1MacFrameElemId;
    if (u1ElementId == WSSMAC_HT_OPE_ELEMENT_ID)
    {
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.HTOperation.u1MacFrameElemId);
        WSSMAC_PUT_1BYTE (pu1ReadPtr,
                          gBeaconMacFrame.HTOperation.u1MacFrameElemLen);
        WSSMAC_PUT_1BYTE (pu1ReadPtr, gBeaconMacFrame.HTOperation.u1PrimaryCh);
        WSSMAC_PUT_NBYTES (pu1ReadPtr, gBeaconMacFrame.HTOperation.au1HTOpeInfo,
                           WSSMAC_HTOPE_INFO);
        WSSMAC_PUT_NBYTES (pu1ReadPtr,
                           gBeaconMacFrame.HTOperation.au1BasicMCSSet,
                           WSSMAC_BASIC_MCS_SET);
    }

    /* Rsna Elements */
    if (gBeaconMacFrame.RSNInfo.u1KeyStatus == 1)
    {

        u1ElementId = gBeaconMacFrame.RSNInfo.u1MacFrameElemId;
        if (u1ElementId == WSSMAC_RSN_ELEMENT_ID)
        {
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              gBeaconMacFrame.RSNInfo.u1MacFrameElemId);
            WSSMAC_PUT_1BYTE (pu1ReadPtr,
                              gBeaconMacFrame.RSNInfo.u1MacFrameElemLen);
            WSSMAC_PUT_2BYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.u2MacFrameRSN_Version);
            WSSMAC_PUT_NBYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.
                               u4MacFrameRSN_GrpCipherSuit, 4);
            WSSMAC_PUT_2BYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.
                               u2MacFrameRSN_PairCiphSuitCnt);
            WSSMAC_PUT_NBYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.
                               aMacFrameRSN_PairCiphSuitList, 4);
            WSSMAC_PUT_2BYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.
                               u2MacFrameRSN_AKMSuitCnt);
            WSSMAC_PUT_NBYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.
                               aMacFrameRSN_AKMSuitList, 4);
            WSSMAC_PUT_2BYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.
                               u2MacFrameRSN_Capability);
            WSSMAC_PUT_2BYTES (pu1ReadPtr,
                               gBeaconMacFrame.RSNInfo.u2MacFrameRSN_PMKIdCnt);
        }

    }

    u4Len = (UINT4) (pu1ReadPtr - pBuf);

    /* Allocate memory for packet linear buffer */
    pBeaconFrameBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu =
        WSSMAC_ALLOCATE_CRU_BUF (u4Len);

    if (pBeaconFrameBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu == NULL)
    {
        WSSMAC_TRC (WSSMAC_FAILURE_TRC, "Memory Allocation Failed \r\n");
        return;
    }

    WSSMAC_COPY_TO_BUF (pBeaconFrameBuf->unMacMsg.MacDot11PktBuf.pDot11MacPdu,
                        pBuf, 0, u4Len);

    MEMCPY (WssWlanDBMsg.WssWlanAttributeDB.BssId,
            gBeaconMacFrame.MacMgmtFrmHdr.u1BssId, WSSMAC_MAC_ADDR_LEN);
    WssWlanDBMsg.WssWlanIsPresentDB.bWlanIfIndex = WSSMAC_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY,
                                  &WssWlanDBMsg) != WSSMAC_SUCCESS)
    {
        return;
    }

    pBeaconFrameBuf->unMacMsg.MacDot11PktBuf.u4IfIndex =
        WssWlanDBMsg.WssWlanAttributeDB.u4WlanIfIndex;

    BCNMGR_FN_EXIT ();

    return;
}

/*****************************************************************************
 *
 * Function     : BcnMgrTmrStart
 *
 * Description  : This function starts the Beacon Manager Timer.
 *
 * Input        : u4TmrInterval - Time interval for which timer must run
 *               (in seconds)
 *               u1TmrType - Indicates which timer to start.
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
INT4
BcnMgrTmrStart (UINT1 u1TmrType, UINT4 u4TmrInterval)
{
    BCNMGR_FN_ENTRY ();

    switch (u1TmrType)
    {
        case BCNMGR_BCN_PER_TMR:
        {
            if (TmrStart (gBcnMgrGlobalInfo.bcnPerTmrId,
                          &(gBcnMgrGlobalInfo.bcnPerTimer),
                          u1TmrType, 0, u4TmrInterval) == TMR_FAILURE)
            {
                BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                            "BcnMgrTmrStart: Beacon Period Timer Start"
                            "FAILED.\r\n");
                return OSIX_FAILURE;
            }
            break;
        }
        default:
        {
            BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                        "BcnMgrTmrStart: INVALID Timer Type !!!\r\n");
            return OSIX_FAILURE;
        }
    }

    BCNMGR_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : BcnMgrTmrStop
 *
 * Description  : This function stops the Beacon Manager Timer.
 *
 * Input        : u1TmrType - Indicates which timer to stop
 *
 * Output       : None
 *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *
 *****************************************************************************/
INT4
BcnMgrTmrStop (UINT1 u1TmrType)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4TmrRetVal = TMR_SUCCESS;

    BCNMGR_FN_ENTRY ();

    switch (u1TmrType)
    {
        case BCNMGR_BCN_PER_TMR:
        {
            u4TmrRetVal =
                TmrGetRemainingTime (gBcnMgrGlobalInfo.bcnPerTmrId,
                                     &(gBcnMgrGlobalInfo.bcnPerTimer.TimerNode),
                                     &u4RemainingTime);

            if (u4TmrRetVal != TMR_FAILURE)
            {
                if (TmrStop (gBcnMgrGlobalInfo.bcnPerTmrId,
                             &(gBcnMgrGlobalInfo.bcnPerTimer)) != TMR_SUCCESS)
                {
                    BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                                "BcnMgrTmrStop: Failure to Stop Beacon"
                                "Period Timer \r\n");
                    return OSIX_FAILURE;
                }
            }
            break;
        }
        default:
        {
            BCNMGR_TRC (BCNMGR_FAILURE_TRC,
                        "BcnMgrTmrStop: INVALID Timer Type !!!\r\n");
            return OSIX_FAILURE;
        }
    }

    BCNMGR_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
