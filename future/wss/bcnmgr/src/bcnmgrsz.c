/*****************************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
* $Id: bcnmgrsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
* Description: This file contains the BCNMGR Sizing Functions.
*
******************************************************************************/

#define _BCNMGRSZ_C
#include "bcnmgrinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
BcnmgrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BCNMGR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsBCNMGRSizingParams[i4SizingId].u4StructSize,
                              FsBCNMGRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(BCNMGRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            BcnmgrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
BcnmgrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsBCNMGRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, BCNMGRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
BcnmgrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < BCNMGR_MAX_SIZING_ID; i4SizingId++)
    {
        if (BCNMGRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (BCNMGRMemPoolIds[i4SizingId]);
            BCNMGRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
