/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: bcnmgrtdfs.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 * Description: This file contains all the typedefs used by the
 *              BCNMGR Module
 *******************************************************************/         

#ifndef __BCNMGR_TDFS_H__
#define __BCNMGR_TDFS_H__


typedef enum {
        BCNMGR_BCN_PER_TMR = 0,
        BCNMGR_MAX_TMR
}tBcnMgrTmrType;

typedef struct {
        tOsixTaskId     MainTaskId;     /* Main Task ID */
        tOsixQId        TaskQId;        /* BCNMGR Task Queue ID */
        tOsixSemId      SemId;          /* BCNMGR Task Semaphore ID */
        tMemPoolId      QMsgPoolId;     /* Mempool Id for the Task Queue */

        tTimerListId    bcnPerTmrId;    /* Beacon Period Timer Id */
        tTmrDesc        aBcnPerTmrDesc[BCNMGR_MAX_TMR]; /*BeaconPeriod TmrDesc*/
        tTmrBlk         bcnPerTimer;    /* Beacon Period Timer Block */

        UINT1           u1BcnTaskInitialized;

        UINT1           au1Pad[3];

}tBcnMgrGlobalInfo;

#endif /* BCNMGR_TDFS_H */
