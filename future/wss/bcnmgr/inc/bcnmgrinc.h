#ifndef __BCNMGR_INC_H__
#define __BCNMGR_INC_H__
/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrinc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the include files for Beacon Manager. 
 * 
 *****************************************************************************/

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "trace.h"

#include "wssmacinc.h"

#include "bcnmgrconst.h"
#include "bcnmgrtrc.h"
#include "bcnmgrtdfs.h"
#include "bcnmgrsz.h"
#include "bcnmgrproto.h"

#include "bcnmgr.h"

#ifdef __BCNMGRMAIN_C__
#include "bcnmgrglob.h"
#else
#include "bcnmgrextn.h"
#endif


#endif /* BCNMGR_INC_H */
