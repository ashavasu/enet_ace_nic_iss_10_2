
#ifndef  __BCNMGR_CONST_H__
#define  __BCNMGR_CONST_H__

/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrconst.h,v 1.2 2017/05/23 14:16:47 siva Exp $ 
 * 
 * Description: This file contains the Beacon Manager constants definition. 
 * 
 *****************************************************************************/

/* General */
#define BCNMGR_TASK_NAME                ((UINT1 *)"BCNMGRT")
#define BCNMGR_TASK_QUEUE_NAME          ((UINT1 *)"BCNMGRQ")
#define BCNMGR_SEM_NAME                 ((UINT1 *)"BCNMGRS")
#define MAX_BCNMGR_Q_MSG    128

/* Sizeable Parameters */
#define BCNMGR_MAX_Q_DEPTH              50   /* TBD */

/* Message Types */
#define BCNMGR_RECV_BEACON_FRAME        1

/* Event List*/
#define BCNMGR_TIMER_EXP_EVENT   0x01
#define BCNMGR_QMSG_EVENT        0x02
#define BCNMGR_ALL_EVENTS        ( BCNMGR_TIMER_EXP_EVENT | BCNMGR_QMSG_EVENT )

/* Trace Level*/
#define BCNMGR_MGMT_TRC                 0x00000001
#define BCNMGR_INIT_TRC                 0x00000002
#define BCNMGR_ENTRY_TRC                0x00000004
#define BCNMGR_EXIT_TRC                 0x00000008
#define BCNMGR_FAILURE_TRC              0x00000010
#define BCNMGR_BUF_TRC                  0x00000020
#define BCNMGR_SESS_TRC                 0x00000040
#define BCNMGR_CRITICAL_TRC             0x00000100

#define BCNMGR_SUCCESS                  1
#define BCNMGR_FAILURE                  0

#define BCNMGR_DEF_BEACON_INT           120
#define BCNMGR_DEF_CAPABILITY           12
#define BCNMGR_DEF_SSID                 ((UINT1 *)"WSS") 
#define BCNMGR_SUPP_RATE_ELEM_LEN       3 
#define BCNMGR_DEF_SUPP_RATE            ((UINT1 *)"128") 
#define BCNMGR_DS_CURCHN                36
#define BCNMGR_COUNTRTY_ELEM_LEN        6 
#define BCNMGR_DEF_COUNTRY              ((UINT1 *)"IN") 
#define BCNMGR_EDCA_PARAM_ELEM_LEN      18 
#define BCNMGR_DEF_TIMESTAMP            "00000000"
#define BCNMGR_FRAME_CONTROL            0x8000

#define BCNMGR_UNUSED_CODE              0

#endif 
