#ifndef __BCNMGR_PROTO_H__
#define __BCNMGR_PROTO_H__

/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrproto.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description:This file contains the beacon manager module function prototypes.
 * 
 *****************************************************************************/

#include "bcnmgr.h"

/******** bcnmgrmain.c ------ BcnMgr Main Task Functions ******/
INT4 BcnMgrMainTaskInit (VOID);
VOID BcnMgrMainTaskDeInit (VOID);
INT4 BcnMgrModuleStart (VOID);
VOID BcnMgrMainProcessEvent (UINT4);
VOID BcnMgrModuleShutDown (VOID);
VOID BcnMgrAssignMempoolIds (VOID);
VOID BcnMgrMainReleaseQMemory (tBcnMgrQMsg *);
INT4 BcnMgrLock (VOID);
INT4 BcnMgrUnlock (VOID);
VOID BeaconMgrInit(VOID);
INT4 BcnMgrEnqueCtrlPkts (tBcnMgrMsgStruct *);

/******** bcnmgrque.c ------ BcnMgr Queue Related Functions ******/
VOID BcnMgrQueMsgHandler (VOID);
VOID BcnMgrQueHandleBcnFrame (tBcnMgrQMsg *);
VOID BcnMgrQueHandleConfUpdate (tBcnMgrQMsg *);

/******** bcnmgrtmr.c ------ BcnMgr Timer Related Functions ******/
INT4 BcnMgrTmrInit (VOID);
INT4 BcnMgrTmrDeInit (VOID);
VOID BcnMgrTmrInitTmrDesc (VOID);
VOID BcnMgrBcnPerTmrExp (VOID *);
VOID BcnMgrTmrExpHandler (VOID);
INT4 BcnMgrTmrStart (UINT1, UINT4);
INT4 BcnMgrTmrStop (UINT1);
VOID BcnMgrAssembleBeaconFrame (tWssMacMsgStruct *);

/******** bcnmgrif.c ------ BcnMgr WSS Interface Functions ******/
INT4 BcnMgrProcessWssIfMsg (INT4, tBcnMgrMsgStruct *);

#endif
