
#ifndef _BCNMGRGLOB_H_
#define _BCNMGRGLOB_H_
/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrglob.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the Beacon Manager global variables. 
 * 
 *****************************************************************************/

#include "wssmac.h"

tBcnMgrGlobalInfo gBcnMgrGlobalInfo;
tDot11BeaconMacFrame gBeaconMacFrame;

/* The below datastructure is to hold the content of the Becon Elements for an SSID.
 * Currently FreeScale is supported only for the single SSID.  Hence let us have the below one.
 * Once the support for Multiple SSID is done, the below data structure needs to revisited */

tBcnMgrBcnParams  gBcnMgrBcnParams;

#endif /* BCNMGRGLOB_H */
