#ifndef _BCNMGR_EXTN_H_
#define _BCNMGR_EXTN_H_

/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrextn.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the Beacon Manager external variable that can
 * be used by other modules. 
 * 
 *****************************************************************************/

extern tBcnMgrGlobalInfo    gBcnMgrGlobalInfo;
extern tDot11BeaconMacFrame gBeaconMacFrame;

extern tBcnMgrBcnParams  gBcnMgrBcnParams;

#endif /* BCNMGR_EXTN_H */
