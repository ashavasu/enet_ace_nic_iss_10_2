
#ifndef __BCNMGR_TRC_H__
#define __BCNMGR_TRC_H__

/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: bcnmgrtrc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains the beacon manager module trace and debug
 *              definitions. 
 * 
 *****************************************************************************/

#define  BCNMGR_MOD                ((const char *)"BCNMGR")

#define BCNMGR_MASK                BCNMGR_MGMT_TRC | BCNMGR_FAILURE_TRC

/* Trace and debug flags */

#define BCNMGR_FN_ENTRY() MOD_FN_ENTRY (BCNMGR_MASK,BCNMGR_ENTRY_TRC,BCNMGR_MOD)

#define BCNMGR_FN_EXIT() MOD_FN_EXIT (BCNMGR_MASK, BCNMGR_EXIT_TRC,BCNMGR_MOD)

#define BCNMGR_TRC(mask, fmt)\
         MOD_TRC(BCNMGR_MASK, mask, BCNMGR_MOD, fmt)

#define BCNMGR_TRC1(mask,fmt,arg1)\
         MOD_TRC_ARG1(BCNMGR_MASK,mask,BCNMGR_MOD,fmt,arg1)

#define BCNMGR_TRC2(mask,fmt,arg1,arg2)\
         MOD_TRC_ARG2(BCNMGR_MASK,mask,BCNMGR_MOD,fmt,arg1,arg2)

#define BCNMGR_TRC3(mask,fmt,arg1,arg2,arg3)\
         MOD_TRC_ARG3(BCNMGR_MASK,mask,BCNMGR_MOD,fmt,arg1,arg2,arg3)

#define BCNMGR_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
         MOD_TRC_ARG4(BCNMGR_MASK,mask,BCNMGR_MOD,fmt,arg1,arg2,arg3,arg4)

#endif
