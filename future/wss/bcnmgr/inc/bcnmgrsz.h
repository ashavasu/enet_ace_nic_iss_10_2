/*****************************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
* $Id: bcnmgrsz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
* Description: This file contains the BCNMGR Sizing Definitions.
*
******************************************************************************/

enum {
    MAX_BCNMGR_Q_MSG_SIZING_ID,
    BCNMGR_MAX_SIZING_ID
};


#ifdef  _BCNMGRSZ_C
tMemPoolId BCNMGRMemPoolIds[ BCNMGR_MAX_SIZING_ID];
INT4  BcnmgrSizingMemCreateMemPools(VOID);
VOID  BcnmgrSizingMemDeleteMemPools(VOID);
INT4  BcnmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _BCNMGRSZ_C  */
extern tMemPoolId BCNMGRMemPoolIds[ ];
extern INT4  BcnmgrSizingMemCreateMemPools(VOID);
extern VOID  BcnmgrSizingMemDeleteMemPools(VOID);
#endif /*  _BCNMGRSZ_C  */


#ifdef  _BCNMGRSZ_C
tFsModSizingParams FsBCNMGRSizingParams [] = {
{ "tBcnMgrQMsg", "MAX_BCNMGR_Q_MSG", sizeof(tBcnMgrQMsg),MAX_BCNMGR_Q_MSG, MAX_BCNMGR_Q_MSG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _BCNMGRSZ_C  */
extern tFsModSizingParams FsBCNMGRSizingParams [];
#endif /*  _BCNMGRSZ_C  */


