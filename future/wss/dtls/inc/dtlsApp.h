/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsApp.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of dtls App module.
 *******************************************************************/
#ifndef _DTLS_APP_H_
#define _DTLS_APP_H_

#include "lr.h"
#include "dtlsCommon.h"

#define DTLS_APP_0_EVENT 0x0
#define DTLS_APP_1_EVENT 0x1
#define DTLS_APP_2_EVENT 0x2
#define DTLS_APP_3_EVENT 0x4
#define DTLS_APP_4_EVENT 0x8

#define DTLS_EVENT 10 

#define DTLS_QUEUE_POOL_ID      gaDTLSMemPoolIds[MAX_DTLS_QUEUE_SIZE_ID]
#define DTLS_APP_PKTS_POOL_ID   gaDTLSMemPoolIds[MAX_DTLS_APP_PKTS_SIZE_ID]


extern tMemPoolId gaDTLSMemPoolIds[MAX_DTLS_SIZING_ID];
 
#endif
