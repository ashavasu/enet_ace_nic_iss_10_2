/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsCommon.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of dtls Cmn module.
 *******************************************************************/
#ifndef _DTLS_COMMON_H_
#define _DTLS_COMMON_H_

#include "lr.h"
#include "dtls.h"

#define MAX_APP_NO             10
#define MAX_QUEUE_MSG          10

/* lock for management tables */
extern tOsixSemId dtlsTblAccessSemId;

/* management table */
typedef struct
{
    tRBNodeEmbd        nextPeerEntry;
    UINT4 u4Id;
    UINT4 u4IpAddr;
    VOID  *pDtlsCtxt;
    UINT4 u4Fd;
}tPeerManageTbl;

typedef struct
{
    tRBNodeEmbd        nextAppEntry;
    UINT4 u4Fd;
    UINT4 u4Port;
    VOID (*dtlsSessionEstNotify)(UINT4, UINT4, UINT4);
    VOID (*dtlsPeerDisconnectNotify)(UINT4, UINT4, UINT4);
    VOID (*dtlsAppCryptoNotify)(tQueData*);
    UINT4 u4DtlsState;
}tAppManageTbl;

enum {
    MAX_DTLS_APP_TBL_SIZE_ID,
    MAX_DTLS_PEER_TBL_SIZE_ID,
    MAX_DTLS_QUEUE_SIZE_ID,
    MAX_DTLS_APP_PKTS_SIZE_ID,
    MAX_DTLS_SIZING_ID
};


#endif
