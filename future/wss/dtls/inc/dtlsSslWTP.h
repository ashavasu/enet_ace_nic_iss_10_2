/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsSslWTP.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of dtlsSslWTP module.
 *******************************************************************/
#ifndef _DTLS_SSLWLC_H_
#define _DTLS_SSLWLC_H_

#include "lr.h"
INT4 DtlsWlcConnect (UINT4, UINT4, UINT4 (*callbck) (VOID*,INT4,INT4));
INT4 DtlsSslRead (VOID*, INT1*, INT4);
INT4 DtlsSslWrite (VOID*, INT1*, INT4);

#endif
