
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsLib.h,v 1.2 2017/05/23 14:16:50 siva Exp $
 *
 * Description: This file contains include files of dtlsLib module.
 *******************************************************************/
#ifndef _DTLS_LIB_H_
#define _DTLS_LIB_H

#include "pthread.h"
#include "dtlsCommon.h"

#define DTLS_MAX_APP_TBL_SIZE  sizeof (tAppManageTbl)
#define DTLS_MAX_PEER_TBL_SIZE sizeof (tPeerManageTbl)
#define DTLS_MAX_QUEUE_SIZE    sizeof(tQueData)
#define DTLS_MAX_APP_TBL_NODES      5
#define DTLS_MAX_PEER_TBL_NODES     10
#define DTLS_MAX_QUEUE_NODES        30
#define DTLS_APP_TBL_POOL_ID    gaDTLSMemPoolIds[MAX_DTLS_APP_TBL_SIZE_ID]
#define DTLS_PEER_TBL_POOL_ID   gaDTLSMemPoolIds[MAX_DTLS_PEER_TBL_SIZE_ID]

#endif
