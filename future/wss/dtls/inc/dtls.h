
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtls.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of dtls module.
 *******************************************************************/
#ifndef _DTLS_H_
#define _DTLS_H_

#include "lr.h"

#define CAPWAP_DTLS_UDP_PORT        9999
#define DTLS_PORT_NO                5341
#define DTLS_HDR_FORMAT             {0x01, 0x00,0x00,0x00}
#define DTLS_HDR_SIZE               4
#define DTLS_CTRL_CHANNEL           1
#define DTLS_DATA_CHANNEL           2

typedef enum
{
    /* function types */
    DTLS_OPEN,
    DTLS_ACCEPT,
    DTLS_CONNECT,
    DTLS_ENABLE,
    DTLS_DISABLE,
    DTLS_SESSION_SHUTDOWN
}eFuncSelect;

typedef enum
{
    DATA_ENCRYPT,
    DATA_DECRYPT
}eDataHandle;

typedef struct QueData{
    eDataHandle   eQueDataType;
    UINT4         u4Id; 
    UINT4         u4IpAddr;
    UINT4         u4PortNo;
    UINT4         u4BufSize;
    UINT4         u4Channel;
    VOID          *pBuffer;
}tQueData;

typedef struct
{
    VOID (*SeesionEstdNotify) (UINT4, UINT4, UINT4);
    VOID (*PeerDisconnectNotify) (UINT4, UINT4, UINT4);
    VOID (*DataExchangeNotify) (tQueData*);
    UINT4 u4Fd;
    UINT4 u4Port;
    UINT4 u4IpAddress;
    UINT4 u4Id;
} tDtlsParams;

INT4 CapwapSetCapwapBaseFailedDTLSAuthFailureCount PROTO ((VOID));
INT4 CapwapSetCapwapBaseFailedDTLSSessionCount PROTO ((VOID));

#endif
