
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsProtWLC.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of dtlsProtWLC module.
 *******************************************************************/
#ifndef _DTLS_PROT_H_
#define _DTLS_PROT_H_

#include "lr.h"
#include "dtlsApp.h"
#include "dtlsCommon.h"
#include "dtlsLib.h"

#define DTLS_SEM_NAME   "DMTS"
#define DTLS_APP_TASK_NAME  "DAPT"
#define DTLS_PEER_TASK_NAME     "DPET"

#define MAX_NAME              10
#define DTLS_MAX_MTU_SIZE     1500 
#define DTLS_MAX_APP_PKTS_SIZE  DTLS_MAX_MTU_SIZE
#define DTLS_MAX_APP_PKTS     20
#define DTLS_TASK_PRIORITY    31

#define DTLS_FD 100

typedef VOID (*pfPtr)(tQueData *);

/* DTLS Library Function prototypes */
UINT4 DtlsInit (VOID);
UINT4 DtlsFnSelect(eFuncSelect , tDtlsParams *);
UINT4 DtlsConnect (UINT4 ,UINT4 );
UINT4 DtlsCheckFdValid (UINT4 );
UINT4 DtlsDataEncryptDecryptNotify(UINT4 , tQueData *);
VOID* DtlsGetContextFromPeerTbl (UINT4);
pfPtr DtlsGetAppCBApiFromAppTbl (UINT4);
UINT4 DtlsCheckIfEnabled (UINT4);
VOID  DtlsNotifyPeerConnected (UINT4, UINT4, UINT4, UINT4);
tPeerManageTbl* DtlsAddToPeerManageTree (UINT4, VOID *, UINT4, UINT4);


/* Applicaiton and peer thread prototypes */
VOID AppDtlsThread (INT1*);
VOID DtlsPeerThread (INT1*);

/* RB tree callback function prototypes */
INT4 DtlsComparePeerRBTree (tRBElem * , tRBElem *);
INT4 DtlsCompareAppRBTree (tRBElem * , tRBElem *);
INT4 DtlsPeerDestroyFn (tRBElem *, UINT4 );
INT4 DtlsAppDestroyFn (tRBElem *, UINT4 );

/* prototype for unique id generation */
UINT4 DtlsGetUniqueId (UINT4);

/* SSL callback function */
INT4 dtlsDataEncryptDecryptCallback (INT1*, INT4, INT4);

#endif
