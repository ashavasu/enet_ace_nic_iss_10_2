/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsSslWLC.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 *
 * Description: This file contains include files of capwap module.
 *******************************************************************/
#ifndef _DTLS_SSLWLC_H_
#define _DTLS_SSLWLC_H_

#include "lr.h"

VOID   DtlsInitOpenSSL (VOID);
VOID*  DtlsSetupServerCtxt (VOID);
VOID*  DtlsCreateNewBio (UINT4);
INT4   DtlsDoBioAccept(VOID*);
VOID*  DtlsBioPop (VOID*);
VOID*  DtlsNewSsl (VOID*, VOID*);
INT4   DtlsSslAccept (VOID*);
UINT4  DtlsGetIp (VOID*);
UINT4  DtlsGetPeerPort (VOID*);
INT4   DtlsSslRead (VOID*, INT1*, INT4);
INT4   DtlsSslWrite (VOID*, INT1*, INT4);
VOID   DtlsSslSetCallback (VOID*,INT4 (*callbck) (INT1 *, INT4, INT4));
INT4   DtlsGetCN (VOID*, INT1*);
void   DtlsSslSetAuthType (unsigned int authtype);
#endif
