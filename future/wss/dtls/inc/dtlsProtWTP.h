
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtlsProtWTP.h,v 1.2 2017/05/23 14:16:50 siva Exp $
 *
 * Description: This file contains include files of dtlsProt module.
 *******************************************************************/
#ifndef _DTLS_PROT_H_
#define _DTLS_PROT_H_

#include "lr.h"
#include "dtls.h"

#define DTLS_APP_TASK_NAME    "dtlsApptsk"
#define DTLS_MAX_QUEUE_SIZE    sizeof(tQueData)
#define MAX_QUEUE_NODES        30
#define MAX_QUEUE_MSG          10
#define DTLS_QUEUE_POOL_ID      gaDTLSMemPoolIds[MAX_DTLS_QUEUE_SIZE_ID]
#define DTLS_APP_PKTS_POOL_ID   gaDTLSMemPoolIds[MAX_DTLS_APP_PKTS_SIZE_ID]
#define DTLS_APP_PKTS_MAX_MTU_SIZE 1500
#define DTLS_MAX_APP_PKTS_SIZE  DTLS_APP_PKTS_MAX_MTU_SIZE
#define DTLS_MAX_NUM_PKTS       20
#define DTLS_TASK_PRIORITY      31

#define MAX_MTU_SIZE 1500

UINT4 DtlsConnect (UINT4 ,UINT4 );
UINT4 DtlsFnSelect(eFuncSelect , tDtlsParams *);
UINT4 DtlsDataEncryptDecryptNotify(tQueData *);

/* SSL callback function */
UINT4 dtlsDataEncryptDecryptCallback (VOID*, INT4, INT4);

VOID AppWTPDtlsThread (INT1*);
#endif
