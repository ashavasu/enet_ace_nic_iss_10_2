/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: dtlsPeerWTP.c,v 1.2 2017/05/23 14:16:50 siva Exp $                                                                          *
 * Description: This file contains the implementation for                    *
 *  DTLS Peer thread implementation                                          *
 * interface.                                                                *
 *                                                                           *
 *****************************************************************************/
#ifndef _DTLS_PEER_C
#define _DTLS_PEER_C

#include "dtlsSslWTP.h"
#include "dtlsProtWTP.h"
#include "dtlsCommon.h"
#include "dtls.h"

extern tMemPoolId   gaDTLSMemPoolIds[MAX_DTLS_SIZING_ID];

static tOsixTaskId  gu4dtlsAppTaskId;
tOsixQId            dtlsWTPDataEncDecQue;
UINT1              *gapQueueNames[MAX_APP_NO] =
    { (UINT1 *) "APP1", (UINT1 *) "APP2",
    (UINT1 *) "APP3", (UINT1 *) "APP4",
    (UINT1 *) "APP5"
};

UINT1               gau1AppDataCallbckBuff[MAX_MTU_SIZE];
UINT4               gu4CallbackBuffSize;

extern VOID         (*gpAppEncDecCallbackPtr) (tQueData *);

/*****************************************************************************/
/* Function Name      : dtlsDataEncryptDecryptNotify                         */
/*                                                                           */
/* Description        : This function put the data sent by the appliation    */
/*                      into the queue and sends an event to app thread      */
/*                                                                           */
/* Input(s)           : ptQueueAppData - Application Data                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
UINT4
DtlsDataEncryptDecryptNotify (tQueData * ptQueueAppData)
{
    tQueData           *ptQueueData = NULL;
    UINT4               u4Fd = 1;
    UINT1              *pu1Temp = NULL;
    UINT1              *pAppData = NULL;

    ptQueueData = (tQueData *) MemAllocMemBlk (DTLS_QUEUE_POOL_ID);
    if ((ptQueueData == NULL) || (ptQueueAppData == NULL))
    {
        return OSIX_FAILURE;
    }

    pAppData = (UINT1 *) MemAllocMemBlk (DTLS_APP_PKTS_POOL_ID);
    if (pAppData == NULL)
    {
        MemReleaseMemBlock (DTLS_QUEUE_POOL_ID, (UINT1 *) ptQueueData);
        return OSIX_FAILURE;
    }

    memset (ptQueueData, 0, sizeof (tQueData));

    /* copy CRU buffer into a local linear buffer */
    CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) ptQueueAppData->
                               pBuffer, pAppData, 0, ptQueueAppData->u4BufSize);

    ptQueueData->eQueDataType = ptQueueAppData->eQueDataType;
    ptQueueData->u4Id = ptQueueAppData->u4Id;
    ptQueueData->u4BufSize = ptQueueAppData->u4BufSize;
    ptQueueData->pBuffer = pAppData;
    ptQueueData->u4IpAddr = ptQueueAppData->u4IpAddr;
    ptQueueData->u4PortNo = ptQueueAppData->u4PortNo;
    ptQueueData->u4Channel = ptQueueAppData->u4Channel;

    /* put queueData in the dtlsDataEncDecQue[u4Fd - 1] queue */
    pu1Temp = (UINT1 *) ptQueueData;
    if (OsixQueSend (dtlsWTPDataEncDecQue, (UINT1 *) &pu1Temp,
                     OSIX_MAX_Q_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (DTLS_QUEUE_POOL_ID, (UINT1 *) ptQueueData);
        MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID, (UINT1 *) pAppData);
        return OSIX_FAILURE;
    }

    /* send event to application thread */
    if (OsixEvtSend (gu4dtlsAppTaskId, (UINT4) (1 << (u4Fd - 1))) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AppWTPDtlsThread                                     */
/*                                                                           */
/* Description        : This function create DTLS Application Thread         */
/*                                                                           */
/* Input(s)           : pi1Params - Input Parameters                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AppWTPDtlsThread (INT1 *pi1Params)
{

    UINT1               au1AppDataSslBuff[MAX_MTU_SIZE] = { 0 };
    UINT4               u4BytesRead = 0;
    UINT4               u4EventReceived = 10;
    tQueData           *ptQueueAppData = NULL;

    UNUSED_PARAM (pi1Params);

    /* get task id */
    if (OsixTskIdSelf (&gu4dtlsAppTaskId) == OSIX_FAILURE)
    {
        return;
    }

    /* Create 'CTRL RX Q' queue */
    if (OsixQueCrt
        ((UINT1 *) "WTPDtlsQue", OSIX_MAX_Q_MSG_LEN, 30 /* Q depth */ ,
         &(dtlsWTPDataEncDecQue)) != OSIX_SUCCESS)
    {
        printf ("AppWTPDtlsThread:: Queue Creation failed\n");
        return;
    }

    while (1)
    {

        /* receive events from all applications */
        OsixEvtRecv (gu4dtlsAppTaskId,
                     0x1 | 0x2 | 0x4 | 0x8 | 0x0, OSIX_WAIT, &u4EventReceived);

        /* receive data from intended queue */
        while (OsixQueRecv (dtlsWTPDataEncDecQue,
                            (UINT1 *) &ptQueueAppData,
                            OSIX_MAX_Q_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {

            if (ptQueueAppData->eQueDataType == DATA_DECRYPT)
            {
                /* decrypt the data */
                memcpy (&gau1AppDataCallbckBuff[0], ptQueueAppData->pBuffer,
                        ptQueueAppData->u4BufSize);

                gu4CallbackBuffSize = ptQueueAppData->u4BufSize;

                MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                    ptQueueAppData->pBuffer);

                /* read data from callback buffer and decrypt */
                if ((u4BytesRead =
                     (UINT4) DtlsSslRead (NULL, (INT1 *) &au1AppDataSslBuff[0],
                                          MAX_MTU_SIZE)) <= 0)
                {
                    MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                        (UINT1 *) ptQueueAppData);
                    continue;
                }

                /* create a CRU buffer and put decrypted data into it */
                ptQueueAppData->pBuffer =
                    (VOID *) CRU_BUF_Allocate_MsgBufChain (MAX_MTU_SIZE, 0);

                CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *)
                                           ptQueueAppData->pBuffer,
                                           &au1AppDataSslBuff[0], 0,
                                           u4BytesRead);

                /* fill the size in queue structure */
                ptQueueAppData->u4BufSize = u4BytesRead;

            }
            else if (ptQueueAppData->eQueDataType == DATA_ENCRYPT)
            {

                /* encrypt the data */
                /* encrypt data and put in callback buffer */
                if (DtlsSslWrite (NULL, ptQueueAppData->pBuffer,
                                  (int) ptQueueAppData->u4BufSize) <= 0)
                {
                    printf ("DtlsSslWrite failed :(\n");
                    MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                        (UINT1 *) ptQueueAppData->pBuffer);
                    MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                        (UINT1 *) ptQueueAppData);
                    continue;
                }

                MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                    (UINT1 *) ptQueueAppData->pBuffer);
                /* create a CRU buffer and put decrypted data into it */
                ptQueueAppData->pBuffer =
                    (VOID *) CRU_BUF_Allocate_MsgBufChain (MAX_MTU_SIZE, 0);
                /* copy callback buffer containing encrypted code in CRU buffer */
                CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *)
                                           ptQueueAppData->pBuffer,
                                           &gau1AppDataCallbckBuff[0], 0,
                                           gu4CallbackBuffSize);

                /* fill the size in queue structure */
                ptQueueAppData->u4BufSize = gu4CallbackBuffSize;

            }
            /* call the callback */
            gpAppEncDecCallbackPtr (ptQueueAppData);
            MemReleaseMemBlock (DTLS_QUEUE_POOL_ID, (UINT1 *) ptQueueAppData);

        }
    }
}

/*****************************************************************************/
/* Function Name      : dtlsDataEncryptDecryptCallback                       */
/*                                                                           */
/* Description        : This function is the callback for encrypt/decrypt    */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pi1Buff - Input Buffer                               */
/*                      i4Size - Size                                        */
/*                      i4OpType - Operation Type                            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0/1                                                  */
/*****************************************************************************/
UINT4
dtlsDataEncryptDecryptCallback (void *pi1Buff, INT4 i4Size, INT4 i4OpType)
{
    if (0 == i4OpType)            /* write */
    {
        memcpy (&gau1AppDataCallbckBuff[0], (INT1 *) pi1Buff, (size_t) i4Size);
        gu4CallbackBuffSize = (UINT4) i4Size;
        return 0;
    }
    else if (1 == i4OpType)        /* read */
    {
        memcpy ((INT1 *) pi1Buff, &gau1AppDataCallbckBuff[0],
                gu4CallbackBuffSize);
        memset (gau1AppDataCallbckBuff, 0, MAX_MTU_SIZE);
        return gu4CallbackBuffSize;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : DtlsConnect                                          */
/*                                                                           */
/* Description        : This function is called from AP (WTP) to connect     */
/*                      to AC(WLC)                                           */
/*                                                                           */
/* Input(s)           : u4ipAddress - Ip Address                             */
/*                      u4port - U4Port No                                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
UINT4
DtlsConnect (UINT4 u4ipAddress, UINT4 u4port)
{
    /* connect to specified port and ip address */
    if (DtlsWlcConnect (u4ipAddress, u4port, &dtlsDataEncryptDecryptCallback) <
        0)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif /* _DTLS_PEER_C */
