/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: dtlsLibraryWTP.c,v 1.3 2017/05/23 14:16:50 siva Exp $                                                                          *
 * Description: This file contains the implementation for                    *
 *  DTLS Library  implementation  for WLC                                    *
 * interface.                                                                *
 *                                                                           *
 *****************************************************************************/

#ifndef _DTLS_LIBRARY_C
#define _DTLS_LIBRARY_C

#include "dtlsProtWTP.h"
#include "dtlsCommon.h"

static tOsixTaskId  gu4tappTaskId;
static UINT4        gau4FdArray[MAX_QUEUE_MSG];

tMemPoolId          gaDTLSMemPoolIds[MAX_DTLS_SIZING_ID];

tFsModSizingParams  gaFsDTLSSizingParams[] = {
    {"\0", "\0", 0, 0, 0, 0},
    {"\0", "\0", 0, 0, 0, 0},
    {"DTLS_MAX_QUEUE_SIZE", "MAX_QUEUE_NODES", DTLS_MAX_QUEUE_SIZE,
     MAX_QUEUE_NODES, MAX_QUEUE_NODES, 0},
    {"DTLS_MAX_APP_PKTS_SZ", "DTLS_MAX_NUM_PKTS", DTLS_MAX_APP_PKTS_SIZE,
     DTLS_MAX_NUM_PKTS, DTLS_MAX_NUM_PKTS, 0},
    {"\0", "\0", 0, 0, 0, 0}
};

VOID                (*gpAppEncDecCallbackPtr) (tQueData *);

extern UINT1       *gapQueueNames[MAX_APP_NO];

static UINT4        DtlsSizingMemCreateMemPools (VOID);
static UINT4        DtlsOpen (VOID (*)(tQueData *));

/*****************************************************************************/
/* Function Name      : dtlsFnSelect                                         */
/*                                                                           */
/* Description        : This function called by application calls the        */
/*                      appropriate DTLS function based on input             */
/*                                                                           */
/* Input (s)          : eFunction - Element Function                         */
/*                      pDtlsParams - Dtls Parameter                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/

UINT4
DtlsFnSelect (eFuncSelect eFunction, tDtlsParams * pDtlsParams)
{
    switch (eFunction)
    {
        case DTLS_OPEN:
            return (DtlsOpen (pDtlsParams->DataExchangeNotify));
            break;
        case DTLS_ACCEPT:
            return (OSIX_FAILURE);
            break;
        case DTLS_CONNECT:
            return (DtlsConnect (pDtlsParams->u4IpAddress,
                                 pDtlsParams->u4Port));
            break;
        case DTLS_ENABLE:
            return (OSIX_FAILURE);
            break;
        case DTLS_DISABLE:
            return (OSIX_FAILURE);
            break;
        case DTLS_SESSION_SHUTDOWN:
            return (OSIX_FAILURE);
            break;
        default:

            break;

            return OSIX_FAILURE;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DtlsOpen                                             */
/*                                                                           */
/* Description        : This function creates a FD to the application and    */
/*                      creates message queue                                */
/*                                                                           */
/* Input(s)           : Fn Pointer                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
static UINT4
DtlsOpen (VOID      (*DtlsAppCryptoNotify) (tQueData *))
{
    UINT4               u4Fd = 0;
    UINT4               u4Count = 0;

    /* check if no function pointer is NULL */
    if (DtlsAppCryptoNotify == NULL)
    {
        PRINTF ("dtlsOpen failed for param validation\n");
        return OSIX_FAILURE;
    }

    /* store callback enc dec pointer */
    gpAppEncDecCallbackPtr = DtlsAppCryptoNotify;

    /* create a new fd */
    for (u4Count = 0; u4Count < MAX_APP_NO; u4Count++)
    {
        if (gau4FdArray[u4Count] == 0)
        {
            u4Fd = u4Count + 1;
            gau4FdArray[u4Count] = u4Count + 1;
            break;
        }
    }

    /* create the application management thread */
    if (OsixCreateTask ((const UINT1 *) DTLS_APP_TASK_NAME,
                        DTLS_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        AppWTPDtlsThread,
                        NULL,
                        OSIX_DEFAULT_TASK_MODE, &gu4tappTaskId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* create mem pools for queues */
    if (DtlsSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return u4Fd;
}

/*****************************************************************************/
/* Function Name      : DtlsSizingMemCreateMemPools                          */
/*                                                                           */
/* Description        : This function creates a FD to the application and    */
/*                      creates message queue                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS                                         */
/*****************************************************************************/
static UINT4
DtlsSizingMemCreateMemPools (VOID)
{
    INT4                i4RetVal = 0;
    INT4                i4SizingId = 0;

    for (i4SizingId = MAX_DTLS_QUEUE_SIZE_ID; i4SizingId < MAX_DTLS_SIZING_ID;
         i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (gaFsDTLSSizingParams[i4SizingId].
                                     u4StructSize,
                                     gaFsDTLSSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(gaDTLSMemPoolIds[i4SizingId]));
    }
    return i4RetVal;
}

#endif
