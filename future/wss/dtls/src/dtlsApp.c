/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: dtlsApp.c,v 1.2 2017/05/23 14:16:50 siva Exp $                                                                           *
 * Description: This file contains the implementation for                    *
 *  DTLS Application thread implementation                                   *
 * interface.                                                                *
 *                                                                           *
 *****************************************************************************/

#ifndef _DTLS_APP_C
#define _DTLS_APP_C

#include "dtlsSslWLC.h"
#include "dtlsProtWLC.h"
#include "dtlsApp.h"
#include "dtls.h"
#include "dtlsCommon.h"
#include "lr.h"

#define MAX_MTU_SIZE 1500

static tOsixTaskId  gu4dtlsAppTaskId;
tOsixQId            gadtlsDataEncDecQue[MAX_APP_NO];
UINT1              *gapQueueNames[MAX_APP_NO] =
    { (UINT1 *) "APP1", (UINT1 *) "APP2",
    (UINT1 *) "APP3", (UINT1 *) "APP4",
    (UINT1 *) "APP5"
};

UINT1               gau1AppDataCallbckBuff[MAX_MTU_SIZE];
UINT4               gu4CallbackBuffSize;

/*****************************************************************************/
/* Function Name      : dtlsDataEncryptDecryptNotify                         */
/*                                                                           */
/* Description        : This function put the data sent by the appliation    */
/*                      into the queue and sends an event to app thread      */
/*                                                                           */
/* Input (s)          : u4Fd - File Descriptor                               */
/*                      pQueueData - Queue Data                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCSS                             */
/*****************************************************************************/
UINT4
DtlsDataEncryptDecryptNotify (UINT4 u4Fd, tQueData * pQueueData)
{
    tQueData           *ptQueueData = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1              *pAppPktBuffer = NULL;

    /* Kloc Fix Start */
    if (pQueueData == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Kloc Fix Ends */
    ptQueueData = (tQueData *) MemAllocMemBlk (DTLS_QUEUE_POOL_ID);
    if (ptQueueData == NULL)
    {
        return OSIX_FAILURE;
    }

    pAppPktBuffer = (UINT1 *) MemAllocMemBlk (DTLS_APP_PKTS_POOL_ID);

    if (pAppPktBuffer == NULL)
    {
        MemReleaseMemBlock (DTLS_QUEUE_POOL_ID, (UINT1 *) ptQueueData);
        return OSIX_FAILURE;
    }

    memset (ptQueueData, 0, sizeof (tQueData));

    /* copy CRU buffer into a local linear buffer */
    CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) pQueueData->pBuffer,
                               pAppPktBuffer, 0, pQueueData->u4BufSize);

    ptQueueData->eQueDataType = pQueueData->eQueDataType;
    ptQueueData->u4Id = pQueueData->u4Id;
    ptQueueData->u4IpAddr = pQueueData->u4IpAddr;
    ptQueueData->u4PortNo = pQueueData->u4PortNo;
    ptQueueData->u4BufSize = pQueueData->u4BufSize;
    ptQueueData->u4Channel = pQueueData->u4Channel;
    ptQueueData->pBuffer = pAppPktBuffer;

    /* check if FD is valid */
    if (DtlsCheckFdValid (u4Fd) != u4Fd)
    {
        MemReleaseMemBlock (DTLS_QUEUE_POOL_ID, (UINT1 *) ptQueueData);
        MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID, (UINT1 *) pAppPktBuffer);
        return OSIX_FAILURE;
    }

    /* put queueData in the gadtlsDataEncDecQue[u4Fd - 1] queue */
    pu1Temp = (UINT1 *) ptQueueData;
    if (OsixQueSend (gadtlsDataEncDecQue[u4Fd - 1], (UINT1 *) &pu1Temp,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (DTLS_QUEUE_POOL_ID, (UINT1 *) ptQueueData);
        MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID, (UINT1 *) pAppPktBuffer);
        return OSIX_FAILURE;
    }

    /* send event to application thread */
    if (OsixEvtSend (gu4dtlsAppTaskId, (UINT4) (1 << (u4Fd - 1))) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : dtlsDataEncryptDecryptCallback                       */
/*                                                                           */
/* Description        : This function is the callback for encrypt/decrypt    */
/*                                                                           */
/* Input (s)          : pi1Buff - BufferPointer                              */
/*                      i4Size - Size                                        */
/*                      i4OpType - OpType                                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0/BufSize                                            */
/*****************************************************************************/
INT4
dtlsDataEncryptDecryptCallback (INT1 *pi1Buff, INT4 i4Size, INT4 i4OpType)
{
    if (0 == i4OpType)            /* write */
    {
        memcpy (&gau1AppDataCallbckBuff[0], pi1Buff, (size_t) i4Size);
        gu4CallbackBuffSize = (UINT4) i4Size;
        return 0;
    }
    else if (1 == i4OpType)        /* read */
    {
        memcpy (pi1Buff, &gau1AppDataCallbckBuff[0], gu4CallbackBuffSize);
        memset (gau1AppDataCallbckBuff, 0, MAX_MTU_SIZE);
        return (INT4) gu4CallbackBuffSize;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : appDtlsThread                                        */
/*                                                                           */
/* Description        : Application thread that performs encryption or       */
/*                      decryption on the data given by application          */
/*                                                                           */
/* Input(s)           pi1Params - Input Parameters                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AppDtlsThread (INT1 *pi1Params)
{
    UINT4               u4Count = 0;
    VOID                (*fpAppCB) (tQueData *) = NULL;
    UINT4               u4EventReceived = DTLS_EVENT;
    UINT4               u4Fd = 0;
    UINT4               u4Id = 0;
    VOID               *pCtxt = NULL;
    tQueData           *ptQueueAppData = NULL;
    UINT1               au1AppDataSslBuff[MAX_MTU_SIZE] = { 0 };
    UINT4               u4BytesRead = 0;

    UNUSED_PARAM (pi1Params);
    /* get task id */
    if (OsixTskIdSelf (&gu4dtlsAppTaskId) == OSIX_FAILURE)
    {
        return;
    }

    while (1)
    {
        /* receive events from all applications */
        OsixEvtRecv (gu4dtlsAppTaskId,
                     DTLS_APP_0_EVENT | DTLS_APP_1_EVENT | DTLS_APP_2_EVENT |
                     DTLS_APP_3_EVENT | DTLS_APP_4_EVENT,
                     OSIX_WAIT, &u4EventReceived);

        /* right shift  u4EventRecevied untill we get 1 and get the number of 
         * right shift count */
        /* this count + 1 is the FD and the applicaiton  */

        while (1)
        {
            if (u4EventReceived == 1)
            {
                u4Fd = u4Count + 1;
                break;
            }
            u4Count++;
            u4EventReceived = u4EventReceived >> 1;
        }

        /* receive data from intended queue */
        while (OsixQueRecv (gadtlsDataEncDecQue[u4Fd - 1],
                            (UINT1 *) &ptQueueAppData,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {

            /* check if DTLS is enabled for the application */
            if (DtlsCheckIfEnabled (u4Fd) == 0)
            {
                MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                    (UINT1 *) ptQueueAppData->pBuffer);
                MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                    (UINT1 *) ptQueueAppData);
                continue;
            }

            if (ptQueueAppData->eQueDataType == DATA_DECRYPT)
            {

                /* from id get the context */
                u4Id = ptQueueAppData->u4Id;

                if ((pCtxt = DtlsGetContextFromPeerTbl (u4Id)) == NULL)
                {
                    MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                        (UINT1 *) ptQueueAppData->pBuffer);
                    MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                        (UINT1 *) ptQueueAppData);
                    continue;
                }

                /* decrypt the data */

                memcpy (&gau1AppDataCallbckBuff[0],
                        (UINT1 *) ptQueueAppData->pBuffer,
                        ptQueueAppData->u4BufSize);

                gu4CallbackBuffSize = ptQueueAppData->u4BufSize;

                MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                    (UINT1 *) ptQueueAppData->pBuffer);

                /* read data from callback buffer and decrypt */
                u4BytesRead =
                    (UINT4) DtlsSslRead (pCtxt, (INT1 *) &au1AppDataSslBuff[0],
                                         MAX_MTU_SIZE);

                /* create a CRU buffer and put decrypted data into it */
                ptQueueAppData->pBuffer =
                    (VOID *) CRU_BUF_Allocate_MsgBufChain (MAX_MTU_SIZE, 0);

                CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *)
                                           ptQueueAppData->pBuffer,
                                           &au1AppDataSslBuff[0], 0,
                                           u4BytesRead);

                /* fill the size in queue structure */
                ptQueueAppData->u4BufSize = u4BytesRead;

                /* call application data provided API to send data to capwap */
                fpAppCB = DtlsGetAppCBApiFromAppTbl (u4Fd);

                if (fpAppCB != NULL)
                {
                    fpAppCB (ptQueueAppData);
                }
                MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                    (UINT1 *) ptQueueAppData);

            }
            else if (ptQueueAppData->eQueDataType == DATA_ENCRYPT)
            {
                /* from id get the context */
                u4Id = ptQueueAppData->u4Id;
                if ((pCtxt = DtlsGetContextFromPeerTbl (u4Id)) == NULL)
                {
                    MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                        (UINT1 *) ptQueueAppData->pBuffer);
                    MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                        (UINT1 *) ptQueueAppData);
                    continue;
                }

                /* encrypt data and put in callback buffer */
                DtlsSslWrite (pCtxt, (INT1 *) ptQueueAppData->pBuffer,
                              (INT4) ptQueueAppData->u4BufSize);

                MemReleaseMemBlock (DTLS_APP_PKTS_POOL_ID,
                                    (UINT1 *) ptQueueAppData->pBuffer);

                /* create a CRU buffer and put decrypted data into it */
                ptQueueAppData->pBuffer =
                    (VOID *) CRU_BUF_Allocate_MsgBufChain (MAX_MTU_SIZE, 0);

                /* copy callback buffer containing encrypted code in CRU buffer */
                CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *)
                                           ptQueueAppData->pBuffer,
                                           &gau1AppDataCallbckBuff[0], 0,
                                           gu4CallbackBuffSize);

                /* fill the size in queue structure */
                ptQueueAppData->u4BufSize = gu4CallbackBuffSize;

                /* call capwap provided API to send event to capwap */
                fpAppCB = DtlsGetAppCBApiFromAppTbl (u4Fd);

                if (fpAppCB != NULL)
                {
                    fpAppCB (ptQueueAppData);
                }

                MemReleaseMemBlock (DTLS_QUEUE_POOL_ID,
                                    (UINT1 *) ptQueueAppData);
            }
        }
    }
}

#endif /* _DTLS_APP_C */
