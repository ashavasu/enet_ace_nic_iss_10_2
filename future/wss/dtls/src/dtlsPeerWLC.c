/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: dtlsPeerWLC.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $                                                                          *
 * Description: This file contains the implementation for                    *
 *  DTLS Peer WLC implementation                                             *
 * interface.                                                                *
 * *****************************************************************************/
#ifndef _DTLS_PEER_C
#define _DTLS_PEER_C

#include "dtlsProtWLC.h"
#include "dtlsCommon.h"
#include "dtls.h"
#include "dtlsSslWLC.h"

extern void         DtlsServerSetCipher (void *);

/*****************************************************************************/
/* Function Name      : dtlsPeerThread                                       */
/*                                                                           */
/* Description        : This thread listens for peer connection. After       */
/*                      establishing connections it updateds peer            */
/*                      information into management table and notifies       */
/*                      application.                                         */
/*                                                                           */
/* Input(s)           : pi1Params - Input Parameters                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
VOID
DtlsPeerThread (INT1 *pi1Params)
{

    UINT4               u4PortNum = *((UINT4 *) (void *) pi1Params + 1);
    UINT4               u4Fd = *((UINT4 *) (void *) pi1Params + 2);
    UINT4               u4IpAddr = 0;
    UINT4               u4PeerPort = 0;
    UINT4               u4Id = 0;

    /* BIO pointers */
    VOID               *pBioAccept = NULL;
    VOID               *pBioClient = NULL;
    /* SSL pointer */
    VOID               *pSsl = NULL;
    /* SSL context pointer */
    VOID               *pCtx = NULL;

    DtlsInitOpenSSL ();
    if ((pCtx = DtlsSetupServerCtxt ()) == NULL)
    {
        if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
        {
            PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
        }

        PRINTF ("PEER THREAD : server setup failed\n");
    }

    pBioAccept = DtlsCreateNewBio (u4PortNum);
    if (NULL == pBioAccept)
    {
        if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
        {
            PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
        }

        PRINTF ("PEER THREAD : Error creating server socket\n");
    }

    if (DtlsDoBioAccept (pBioAccept) <= 0)
    {
        if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
        {
            PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
        }

        PRINTF ("PEER THREAD : Error binding server socket\n");
    }

    for (;;)
    {
        if (DtlsDoBioAccept (pBioAccept) <= 0)
        {
            if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
            {
                PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
            }

            PRINTF ("PEER THREAD : Error accepting connection\n");
            continue;
/*            exit(0);*/
        }

        pBioClient = DtlsBioPop (pBioAccept);

        /* Get IP address from context */
        u4IpAddr = DtlsGetIp (pBioClient);
        u4IpAddr = OSIX_NTOHL (u4IpAddr);

        pSsl = DtlsNewSsl (pCtx, pBioClient);
        if (!pSsl)
        {
            if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
            {
                PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
            }

            PRINTF ("PEER THREAD : Error creating SSL context\n");
            continue;
        }

        DtlsServerSetCipher (pSsl);
        if (DtlsSslAccept (pSsl) <= 0)
        {
            PRINTF ("PEER THREAD : Error accepting SSL connection\n");
            if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
            {
                PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
            }

            if (CapwapSetCapwapBaseFailedDTLSAuthFailureCount () !=
                OSIX_SUCCESS)
            {
                PRINTF ("PEER THREAD : Auth Fail Count Updation Failed\n");
            }
            continue;
        }
        /* set callback and route SSL read and SSL write to callback */
        DtlsSslSetCallback (pSsl, dtlsDataEncryptDecryptCallback);

        /* generate an unique ID */
        u4Id = DtlsGetUniqueId ((UINT4) pSsl);

        /* update management table with the peer addition */
        if (DtlsAddToPeerManageTree (u4Fd, (VOID *) pSsl, u4IpAddr, u4Id) ==
            NULL)
        {
            if (CapwapSetCapwapBaseFailedDTLSSessionCount () != OSIX_SUCCESS)
            {
                PRINTF ("PEER THREAD : Session Fail Count Updation Failed\n");
            }

            PRINTF ("PEER THREAD : Error in adding peer to table\n");
        }

        DtlsNotifyPeerConnected (u4Fd, u4Id, u4IpAddr, u4PeerPort);

    }

    return;
}
#endif /* _DTLS_PEER_C */
