/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 * $Id: dtlsLibraryWLC.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $                                                                          *
 * Description: This file contains the implementation for                    *
 *  DTLS Library thread implementation                                       *
 * interface.                                                                *
 *                                                                           *
 *****************************************************************************/
#ifndef _DTLS_LIBRARY_C
#define _DTLS_LIBRARY_C

#include "dtlsProtWLC.h"
#include "dtlsApp.h"
#include "dtls.h"
#include "dtlsCommon.h"
#include "fsutil.h"
#include "wssifcapdb.h"

/* task ids of peer threads */
tOsixTaskId         gatPeerTaskId[MAX_APP_NO + 1];
tOsixTaskId         gu4test;

/* lock for management tables */
tOsixSemId          dtlsMgmtTblSemId = NULL;

/* peer and application tree IDs*/
static tRBTree      dtlsAppManageTable;
static tRBTree      dtlsPeerManageTable;

/* memory pool IDs*/
tMemPoolId          gaDTLSMemPoolIds[MAX_DTLS_SIZING_ID];

/* array to store FDs */
static UINT4        gau4FdArray[MAX_APP_NO];

/* peer task name array*/
static UINT1        gau1PeerTaskName[MAX_APP_NO][MAX_NAME] =
    { "AP1", "AP2", "AP3", "AP4", "AP5", "AP6" };

tFsModSizingParams  gaFsDTLSSizingParams[] = {
    {"DTLS_MAX_APP_TBL_SIZE", "DTLS_MAX_APP_TBL_NODES",
     DTLS_MAX_APP_TBL_SIZE, DTLS_MAX_APP_TBL_NODES, DTLS_MAX_APP_TBL_NODES, 0},
    {"DTLS_MAX_PEER_TBL_SIZE", "DTLS_MAX_PEER_TBL_NODES",
     DTLS_MAX_PEER_TBL_SIZE, DTLS_MAX_PEER_TBL_NODES, DTLS_MAX_PEER_TBL_NODES,
     0},
    {"DTLS_MAX_QUEUE_SIZE", "DTLS_MAX_QUEUE_NODES",
     DTLS_MAX_QUEUE_SIZE, DTLS_MAX_QUEUE_NODES, DTLS_MAX_QUEUE_NODES, 0},
    {"DTLS_MAX_APP_PKTS_SZ", "MAX_NUM_PKTS",
     DTLS_MAX_APP_PKTS_SIZE, DTLS_MAX_APP_PKTS, DTLS_MAX_APP_PKTS, 0},
    {"\0", "\0", 0, 0, 0, 0}
};

extern tOsixQId     gadtlsDataEncDecQue[MAX_APP_NO];
extern UINT1       *gapQueueNames[MAX_APP_NO];

/* prototypes for DTLS functionalities */
static UINT4        DtlsOpen (VOID (*)(UINT4, UINT4, UINT4), VOID (*)(UINT4,
                                                                      UINT4,
                                                                      UINT4),
                              VOID (*)(tQueData *));
static UINT4        DtlsAccept (UINT4, UINT4);
static UINT4        DtlsEnable (UINT4);
static UINT4        DtlsDisable (UINT4);
static UINT4        DtlsSessionShutdown (UINT4);

/* prototypes for trees and mem pools */
static UINT4        DtlsSizingMemCreateMemPools (VOID);
static VOID         DtlsSizingMemDeleteMemPools (VOID);
static tAppManageTbl *DtlsAddToAppManageTree (UINT4, VOID (*)(UINT4, UINT4,
                                                              UINT4),
                                              VOID (*)(UINT4, UINT4, UINT4),
                                              VOID (*)(tQueData *), UINT4);
static UINT4        DtlsAddRBTreeToPeerTbl (tPeerManageTbl *);
static UINT4        DtlsAddRBTreeToAppTbl (tAppManageTbl *);
static UINT4        DtlsCheckIdPresentInPeerTbl (UINT4);
static UINT4        DtlsCheckFdPresentInAppTbl (UINT4);
static tPeerManageTbl *DtlsGetPeerTbl (UINT4 u4Id);
static tAppManageTbl *DtlsGetAppTbl (UINT4 u4Fd);
static UINT4        DtlsRemPeerNodeFromTbl (tPeerManageTbl *);

/*****************************************************************************/
/* Function Name      : dtlsInit                                             */
/*                                                                           */
/* Description        : This function initializes the dtls Library.          */
/*                      It inlcudes RB tree creation for management tables   */
/*                      memory pool creation, semaphore creation and         */
/*                      application thread creation.                         */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/

UINT4
DtlsInit (VOID)
{
    tOsixTaskId         u4appTaskId = 0;
    static UINT4        u4Init = 0;

    if (1 == u4Init)
    {
        return OSIX_SUCCESS;
    }

    /* create the two management tables */
    /* Create the RB Tree */
    dtlsAppManageTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tAppManageTbl,
                                              nextAppEntry)),
                              DtlsCompareAppRBTree);
    if (NULL == dtlsAppManageTable)
    {
        return OSIX_FAILURE;
    }

    /* Create the RB Tree */
    dtlsPeerManageTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tPeerManageTbl,
                                              nextPeerEntry)),
                              DtlsComparePeerRBTree);
    if (NULL == dtlsPeerManageTable)
    {
        return OSIX_FAILURE;
    }

    /* Initialize the lock to access management table */
    if (OsixCreateSem ((UINT1 *) DTLS_SEM_NAME, 1, 0, &dtlsMgmtTblSemId) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* create mem pools for application and peer tables */
    if (DtlsSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* create the application management thread */
    if (OsixCreateTask ((const UINT1 *) DTLS_APP_TASK_NAME,
                        DTLS_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        AppDtlsThread,
                        NULL,
                        OSIX_DEFAULT_TASK_MODE, &u4appTaskId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4Init = 1;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : dtlsFnSelect                                         */
/*                                                                           */
/* Description        : This function called by application calls the        */
/*                      appropriate DTLS function based on input             */
/*                                                                           */
/* Input(s)           : eFunction - FunctionSelect                           */
/*                      pDtlsParams - Dtls Parameters                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/

UINT4
DtlsFnSelect (eFuncSelect eFunction, tDtlsParams * pDtlsParams)
{
    switch (eFunction)
    {
        case DTLS_OPEN:
            return (DtlsOpen (pDtlsParams->SeesionEstdNotify,
                              pDtlsParams->PeerDisconnectNotify,
                              pDtlsParams->DataExchangeNotify));
            break;
        case DTLS_ACCEPT:
            return (DtlsAccept (pDtlsParams->u4Fd, pDtlsParams->u4Port));
            break;
        case DTLS_CONNECT:
            return (OSIX_FAILURE);
            break;
        case DTLS_ENABLE:
            return (DtlsEnable (pDtlsParams->u4Fd));
            break;
        case DTLS_DISABLE:
            return (DtlsDisable (pDtlsParams->u4Fd));
            break;
        case DTLS_SESSION_SHUTDOWN:
            return (DtlsSessionShutdown (pDtlsParams->u4Id));
            break;
        default:

            break;

            return OSIX_FAILURE;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : dtlsCheckFdValid                                     */
/*                                                                           */
/* Description        : This function checks for FDs validity                */
/*                                                                           */
/* Input(s)            u4Fd - File Descriptor                                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
UINT4
DtlsCheckFdValid (UINT4 u4Fd)
{
    return (DtlsCheckFdPresentInAppTbl (u4Fd));
}

/*****************************************************************************/
/* Function Name      : DtlsOpen                                             */
/*                                                                           */
/* Description        : This function adds a FD to the app manage tree and   */
/*                      registers the callback for the application, creates  */
/*                      message queue                                        */
/*                                                                           */
/* Inputs(s)          : u4Id - Id                                            */
/*                      IpAddr - Ip Address                                  */
/*                      u4PeerPort - Peer port number                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
static UINT4
DtlsOpen (VOID      (*DtlsSessionEstNotify) (UINT4 u4Id, UINT4 IpAddr,
                                             UINT4 u4PeerPort),
          VOID (*DtlsPeerDisconnectNotify) (UINT4 u4Id, UINT4 IpAddr,
                                            UINT4 u4PeerPort),
          VOID (*DtlsAppCryptoNotify) (tQueData *))
{
    UINT4               u4Fd = 0;
    UINT4               u4Count = 0;

    /* check if no function pointer is NULL */
    if ((DtlsSessionEstNotify == NULL) ||
        (DtlsPeerDisconnectNotify == NULL) || (DtlsAppCryptoNotify == NULL))
    {
        PRINTF ("dtlsOpen failed for param validation\n");
        return OSIX_FAILURE;
    }

    /* create a new fd */
    u4Count = 0;
    while (u4Count < MAX_APP_NO)
    {
        if (gau4FdArray[u4Count] == 0)
        {
            u4Fd = u4Count + 1;
            gau4FdArray[u4Count] = u4Count + 1;
            break;
        }
        u4Count++;
    }

    /* create a new entry in application management table */
    if (DtlsAddToAppManageTree (u4Fd,
                                DtlsSessionEstNotify,
                                DtlsPeerDisconnectNotify,
                                DtlsAppCryptoNotify, 1) == NULL)
    {
        return OSIX_FAILURE;
    }

    /* create message queues for the FD generated */
    if (OsixQueCrt ((UINT1 *) gapQueueNames[u4Fd - 1], OSIX_MAX_Q_MSG_LEN,
                    MAX_QUEUE_MSG,
                    &gadtlsDataEncDecQue[u4Fd - 1]) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return u4Fd;
}

/*****************************************************************************/
/* Function Name      : DtlsAccept                                           */
/*                                                                           */
/* Description        : creates peer thread for the application called with  */
/*                      FD and port number as parameters                     */
/*                                                                           */
/* Input(s)           : u4Fd - File Descriptor                               */
/*                      u4Port - Port Number                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
static UINT4
DtlsAccept (UINT4 u4Fd, UINT4 u4Port)
{
    tAppManageTbl      *pAppManageTbl = NULL;

    UINT4              *pu4Params = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pu4Params, OSIX_FAILURE)
/* check if fd is valid */
        if (DtlsCheckFdPresentInAppTbl (u4Fd) != u4Fd)
    {
        PRINTF ("dtlsAccept wrong FD\n");
        return OSIX_FAILURE;
    }

    /* check if port is unique and not present in table for the given fd 
     * (to avoid duplicate calls) */
    pAppManageTbl = DtlsGetAppTbl (u4Fd);
    if (pAppManageTbl == NULL)
    {
        return OSIX_FAILURE;
    }

    /* get the lock */
    OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0);

    if (pAppManageTbl->u4Port == u4Port)
    {
        /* release the lock */
        OsixGiveSem (0, (const UINT1 *) DTLS_SEM_NAME);

        return OSIX_SUCCESS;
    }
    else
    {
        /* fill port number in management table for the fd */
        pAppManageTbl->u4Port = u4Port;

        /* release the lock */
        OsixGiveSem (0, (const UINT1 *) DTLS_SEM_NAME);
    }

    /* create peer thread with port number as parameter */
    *(pu4Params + 1) = u4Port;
    *(pu4Params + 2) = u4Fd;

    if (OsixCreateTask ((const UINT1 *) &gau1PeerTaskName[u4Fd],
                        DTLS_TASK_PRIORITY,
                        OSIX_DEFAULT_STACK_SIZE,
                        DtlsPeerThread,
                        (INT1 *) pu4Params,
                        OSIX_DEFAULT_TASK_MODE,
                        &gatPeerTaskId[u4Fd]) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pu4Params);
        return OSIX_FAILURE;
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pu4Params);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DtlsEnable                                           */
/*                                                                           */
/* Description        : Enable DTLS for called application                   */
/*                                                                           */
/* Input(s)           : u4Fd - File Descriptor                               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
static UINT4
DtlsEnable (UINT4 u4Fd)
{
    tAppManageTbl      *pAppManageTbl = NULL;

    /* check if fd is valid */
    if (DtlsCheckFdPresentInAppTbl (u4Fd) != u4Fd)
    {
        return OSIX_FAILURE;
    }

    /* check if enable field is set */
    pAppManageTbl = DtlsGetAppTbl (u4Fd);

    if (pAppManageTbl != NULL)
    {
        /* get the lock */
        OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0);

        if (pAppManageTbl->u4DtlsState == 1)
        {
            /* release the lock */
            OsixGiveSem (0, (const UINT1 *) DTLS_SEM_NAME);

            PRINTF ("dtlsEnable exited\n");
            return OSIX_SUCCESS;
        }

        pAppManageTbl->u4DtlsState = 1;

        /* release the lock */
        OsixGiveSem (0, (const UINT1 *) DTLS_SEM_NAME);

        /* create the peer thread */
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DtlsDisable                                          */
/*                                                                           */
/* Description        : Disable DTLS for called application.                 */
/* Input(s)           : u4Fd - File Descriptor                               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
static UINT4
DtlsDisable (UINT4 u4Fd)
{
    tAppManageTbl      *pAppManageTbl = NULL;

    /* check if fd is valid */
    if (DtlsCheckFdPresentInAppTbl (u4Fd) != u4Fd)
    {
        return OSIX_FAILURE;
    }

    /* check if enable field is set */
    pAppManageTbl = DtlsGetAppTbl (u4Fd);

    if (pAppManageTbl != NULL)
    {
        /* get the lock */
        OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0);

        if (pAppManageTbl->u4DtlsState == 0)
        {
            /* release the lock */
            OsixGiveSem (0, (const UINT1 *) DTLS_SEM_NAME);

            PRINTF ("dtlsiDisable exited\n");
            return OSIX_SUCCESS;
        }

        pAppManageTbl->u4DtlsState = 0;

        /* release the lock */
        OsixGiveSem (0, (const UINT1 *) DTLS_SEM_NAME);

        /* delete the peer thread */

        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : DtlsSessionShutdown                                  */
/*                                                                           */
/* Description        : hutdown session for specified peer                   */
/*                                                                           */
/* Input(s)           : u4Id - Input Identifier                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
static UINT4
DtlsSessionShutdown (UINT4 u4Id)
{
    tPeerManageTbl     *pPeerManageTbl = NULL;

    /* check if id is valid */
    if (DtlsCheckIdPresentInPeerTbl (u4Id) != u4Id)
    {
        return OSIX_FAILURE;
    }

    /* remove the ID entry */
    pPeerManageTbl = DtlsGetPeerTbl (u4Id);
    if (pPeerManageTbl != NULL)
    {
        return (DtlsRemPeerNodeFromTbl (pPeerManageTbl));
    }

    return OSIX_FAILURE;

}

/*****************************************************************************/
/* Function Name      : CompareAppRBTree                                     */
/*                                                                           */
/* Description        : This function compares the two application RB Tree's */
/*                      based on the based on the FD                         */
/*                                                                           */
/* Input(s)           : e1/e2 - RB Tree Element                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : i4RetVal - Return value of MEMCMP                    */
/*****************************************************************************/
INT4
DtlsCompareAppRBTree (tRBElem * e1, tRBElem * e2)
{
    tAppManageTbl      *pNode1 = e1;
    tAppManageTbl      *pNode2 = e2;

    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (&pNode1->u4Fd, &pNode2->u4Fd, sizeof (UINT4));

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : dtlsComparePeerRBTree                                */
/*                                                                           */
/* Description        : This function compares the two peer RB Tree's        */
/*                      based on the based on the peer ID                    */
/* Input(s)           : e1/e2 - RB Tree Element                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : i4RetVal - Return value of MEMCMP                    */
/*****************************************************************************/
INT4
DtlsComparePeerRBTree (tRBElem * e1, tRBElem * e2)
{
    tPeerManageTbl     *pNode1 = e1;
    tPeerManageTbl     *pNode2 = e2;

    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (&pNode1->u4Id, &pNode2->u4Id, sizeof (UINT4));

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : DtlsSizingMrmCreateMemPools                          */
/*                                                                           */
/* Description        : create necessary memory pools.                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/OSIX_SUCCESS                            */
/*****************************************************************************/
static UINT4
DtlsSizingMemCreateMemPools ()
{
    INT4                i4RetVal = 0;
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < MAX_DTLS_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (gaFsDTLSSizingParams[i4SizingId].
                                     u4StructSize,
                                     gaFsDTLSSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(gaDTLSMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DtlsSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DtlsSizingMemDeleteMemPools                          */
/*                                                                           */
/* Description        : Delete created memory pools                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                     */
/*****************************************************************************/
static VOID
DtlsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = 0;

    for (i4SizingId = 0; i4SizingId < MAX_DTLS_SIZING_ID; i4SizingId++)
    {
        if (gaDTLSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (gaDTLSMemPoolIds[i4SizingId]);
            gaDTLSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : DtlsAddToAppManageTree                               */
/*                                                                           */
/* Description        : Add a entry to application manage tree.              */
/*                                                                           */
/* Input(s)            u4Fd - File Descriptor                                */
/*                     u4DtlsState - DTLS State                              */
/* Output(s)          : Tree pointer                                         */
/*                                                                           */
/* Return Value(s)    : NULL/Tree Pointer                                    */
/*****************************************************************************/
static tAppManageTbl *
DtlsAddToAppManageTree (UINT4 u4Fd,
                        VOID (*dtlsSessionEstNotify) (UINT4 u4Id, UINT4 IpAddr,
                                                      UINT4 u4Port),
                        VOID (*dtlsPeerDisconnectNotify) (UINT4 u4Id,
                                                          UINT4 IpAddr,
                                                          UINT4 u4Port),
                        VOID (*dtlsAppCryptoNotify) (tQueData *),
                        UINT4 u4DtlsState)
{
    tAppManageTbl      *pAppManageTblNode = NULL;

    pAppManageTblNode = (tAppManageTbl *) MemAllocMemBlk (DTLS_APP_TBL_POOL_ID);
    if (pAppManageTblNode == NULL)
    {
        return NULL;
    }
    memset (pAppManageTblNode, 0, sizeof (tAppManageTbl));

    pAppManageTblNode->u4Fd = u4Fd;
    pAppManageTblNode->dtlsSessionEstNotify = dtlsSessionEstNotify;
    pAppManageTblNode->dtlsPeerDisconnectNotify = dtlsPeerDisconnectNotify;
    pAppManageTblNode->dtlsAppCryptoNotify = dtlsAppCryptoNotify;
    pAppManageTblNode->u4DtlsState = u4DtlsState;

    if (DtlsAddRBTreeToAppTbl (pAppManageTblNode) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (DTLS_APP_TBL_POOL_ID, (UINT1 *) pAppManageTblNode);
        return NULL;
    }
    return pAppManageTblNode;
}

/*****************************************************************************/
/* Function Name      : DtlsAddToPeerManageTree                              */
/*                                                                           */
/* Description        : Add a entry to peer manage tree                      */
/*                                                                           */
/* Input(s)           : u4Fd - File Descriptor                               */
/*                      pDtlsCtxt - Dtls Context                             */
/*                      u4IpAddr - IP Address                                */
/*                      u4Id - Identifier                                    */
/* Output(s)          : Tree pointer                                         */
/*                                                                           */
/* Return Value(s)    : NULL/Tree pointer                                    */
/*****************************************************************************/
tPeerManageTbl     *
DtlsAddToPeerManageTree (UINT4 u4Fd,
                         VOID *pDtlsCtxt, UINT4 u4IpAddr, UINT4 u4Id)
{
    tPeerManageTbl     *pPeerManageTblNode = NULL;

    pPeerManageTblNode =
        (tPeerManageTbl *) MemAllocMemBlk (DTLS_PEER_TBL_POOL_ID);
    if (pPeerManageTblNode == NULL)
    {
        return NULL;
    }
    memset (pPeerManageTblNode, 0, sizeof (tPeerManageTbl));
    pPeerManageTblNode->u4Fd = u4Fd;
    pPeerManageTblNode->pDtlsCtxt = pDtlsCtxt;
    pPeerManageTblNode->u4IpAddr = u4IpAddr;
    pPeerManageTblNode->u4Id = u4Id;

    if (DtlsAddRBTreeToPeerTbl (pPeerManageTblNode) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (DTLS_PEER_TBL_POOL_ID,
                            (UINT1 *) pPeerManageTblNode);
        return NULL;
    }

    return pPeerManageTblNode;
}

/*****************************************************************************/
/* Function Name      : DtlsAddRBTreeToAppTbl                                */
/*                                                                           */
/* Description        : Add to RB tree.                                      */
/*                                                                           */
/* Input              : pAppManageTblNode- App Mgmt Table                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
static UINT4
DtlsAddRBTreeToAppTbl (tAppManageTbl * pAppManageTblNode)
{

    /* acquire lock */
    if (OsixTakeSem (0, (const UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    if (RBTreeAdd (dtlsAppManageTable, pAppManageTblNode) != RB_SUCCESS)
    {
        /* release lock */
        OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

        return OSIX_FAILURE;
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DtlsAddRBTreeToPeerTbl                               */
/*                                                                           */
/* Description        : add to RB tree.                                      */
/*                                                                           */
/* Input (s)          : pPeerManageTblNode - PeerManageTable                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
static UINT4
DtlsAddRBTreeToPeerTbl (tPeerManageTbl * pPeerManageTblNode)
{
    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    if (RBTreeAdd (dtlsPeerManageTable, pPeerManageTblNode) != RB_SUCCESS)
    {
        /* release lock */
        OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

        return OSIX_FAILURE;
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DtlsGetAppTbl                                        */
/*                                                                           */
/* Description        : Get app table entry for specified FD.                */
/*                                                                           */
/* Input(s)           : u4Fd - File Descriptor                               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Application Mgmt Table                               */
/*****************************************************************************/
static tAppManageTbl *
DtlsGetAppTbl (UINT4 u4Fd)
{
    tAppManageTbl       AppManageTbl;
    tAppManageTbl      *pAppManageTbl = NULL;

    MEMSET (&AppManageTbl, 0, sizeof (tAppManageTbl));

    AppManageTbl.u4Fd = u4Fd;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return NULL;
    }

    pAppManageTbl = (tAppManageTbl *)
        RBTreeGet (dtlsAppManageTable, &AppManageTbl);

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return pAppManageTbl;
}

/*****************************************************************************/
/* Function Name      : DtlsGetPeerTbl                                       */
/*                                                                           */
/* Description        : Get peer table entry for specified peer ID.          */
/*                                                                           */
/* Input(s)             u4Id - Identifier                                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NULL/pPeerManageTbl                                  */
/*****************************************************************************/
static tPeerManageTbl *
DtlsGetPeerTbl (UINT4 u4Id)
{
    tPeerManageTbl      PeerManageTbl;
    tPeerManageTbl     *pPeerManageTbl = NULL;

    memset (&PeerManageTbl, 0, sizeof (tPeerManageTbl));

    PeerManageTbl.u4Id = u4Id;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return NULL;
    }

    pPeerManageTbl = (tPeerManageTbl *)
        RBTreeGet (dtlsPeerManageTable, &PeerManageTbl);

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return pPeerManageTbl;
}

/*****************************************************************************/
/* Function Name      : DtlsCheckFdPresentInAppTbl                           */
/*                                                                           */
/* Description        : Cehck if a entry is present for specified FD.        */
/*                                                                           */
/* Input              : u4Fd - File Descriptor                               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0/DTLS_FD                                            */
/*****************************************************************************/
static UINT4
DtlsCheckFdPresentInAppTbl (UINT4 u4Fd)
{
    tAppManageTbl      *pAppManageTbl = NULL;
    tAppManageTbl       AppManageTbl;

    memset (&AppManageTbl, 0, sizeof (tAppManageTbl));

    AppManageTbl.u4Fd = u4Fd;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return 0;
    }

    pAppManageTbl = (tAppManageTbl *)
        RBTreeGet (dtlsAppManageTable, &AppManageTbl);

    if (pAppManageTbl != NULL)
    {
        if (pAppManageTbl->u4Fd == u4Fd)
        {
            /* release lock */
            OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

            return u4Fd;
        }
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return DTLS_FD;
}

/*****************************************************************************/
/* Function Name      : DtlsCheckIdPresentInPeerTbl                          */
/*                                                                           */
/* Description        : check if entry present for specified peer ID.        */
/* Input              : u4Fd - File Descriptor                               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0/u4Id                                               */
/*****************************************************************************/
static UINT4
DtlsCheckIdPresentInPeerTbl (UINT4 u4Id)
{
    tPeerManageTbl     *pPeerManageTbl = NULL;
    tPeerManageTbl      PeerManageTbl;

    memset (&PeerManageTbl, 0, sizeof (tPeerManageTbl));

    memcpy ((void *) &PeerManageTbl.u4Id, (const void *) &u4Id, sizeof (UINT4));

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return 0;
    }

    pPeerManageTbl = (tPeerManageTbl *)
        RBTreeGet (dtlsPeerManageTable, &PeerManageTbl);

    if (pPeerManageTbl != NULL)
    {
        if (pPeerManageTbl->u4Id == u4Id)
        {
            /* release lock */
            OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

            return u4Id;
        }
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return 0;
}

/*****************************************************************************/
/* Function Name      : DtlsRemPeerNodeFromTbl                               */
/*                                                                           */
/* Description        : Remove a node from peer manage table.                */
/*                                                                           */
/* Input(s)           : tPeerManageTbl - Peer Management Table               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
static UINT4
DtlsRemPeerNodeFromTbl (tPeerManageTbl * pPeerManageTblNode)
{
    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    if (RBTreeRemove (dtlsPeerManageTable, pPeerManageTblNode) == RB_FAILURE)
    {
        /* release lock */
        OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

        return OSIX_FAILURE;
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    MemReleaseMemBlock (DTLS_PEER_TBL_POOL_ID, (UINT1 *) pPeerManageTblNode);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : dtlsCheckIfEnabled                                   */
/*                                                                           */
/* Description        : This function checks if DTLS is enabled for given FD */
/*                                                                           */
/* Input(s)            u4Fd - File Descriptor                                */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0/1                                                  */
/*****************************************************************************/
UINT4
DtlsCheckIfEnabled (UINT4 u4Fd)
{
    tAppManageTbl      *pAppManageTbl = NULL;
    tAppManageTbl       AppManageTbl;

    memset (&AppManageTbl, 0, sizeof (tAppManageTbl));

    AppManageTbl.u4Fd = u4Fd;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return 0;
    }

    pAppManageTbl = (tAppManageTbl *)
        RBTreeGet (dtlsAppManageTable, &AppManageTbl);

    if (pAppManageTbl != NULL)
    {
        if (pAppManageTbl->u4DtlsState == 1)
        {
            /* release lock */
            OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

            return 1;
        }
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return 0;
}

/*****************************************************************************/
/* Function Name      : dtlsNotifyPeerConnected                              */
/*                                                                           */
/* Description        : This function notifies application of a new peer     */
/*                                                                           */
/* Input(s)           : u4Fd - File Descriptor                               */
/*                      u4Id - Identifier                                    */
/*                      u4IpAddr - IP Address                                */
/*                      u4PeerPort - Peer Port Number                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
DtlsNotifyPeerConnected (UINT4 u4Fd,
                         UINT4 u4Id, UINT4 u4IpAddr, UINT4 u4PeerPort)
{
    tAppManageTbl      *pAppManageTbl = NULL;
    tAppManageTbl       AppManageTbl;
    VOID                (*pSessionEstdNotify) (UINT4, UINT4, UINT4) = NULL;

    memset (&AppManageTbl, 0, sizeof (tAppManageTbl));

    AppManageTbl.u4Fd = u4Fd;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return;
    }

    pAppManageTbl = (tAppManageTbl *)
        RBTreeGet (dtlsAppManageTable, &AppManageTbl);

    if (pAppManageTbl != NULL)
    {
        pSessionEstdNotify = pAppManageTbl->dtlsSessionEstNotify;
        pSessionEstdNotify (u4Id, u4IpAddr, u4PeerPort);

        /* release lock */
        OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return;

}

/*****************************************************************************/
/* Function Name      : dtlsGetContextFromPeerTbl                            */
/*                                                                           */
/* Description        : This function returns context from unique ID         */
/*                                                                           */
/* Input(s)           : u4Id - Identifier                                    */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0/Context                                            */
/*****************************************************************************/
VOID               *
DtlsGetContextFromPeerTbl (UINT4 u4Id)
{
    VOID               *pCtxt = NULL;
    tPeerManageTbl     *pPeerManageTbl = NULL;
    tPeerManageTbl      PeerManageTbl;

    memset (&PeerManageTbl, 0, sizeof (tPeerManageTbl));

    PeerManageTbl.u4Id = u4Id;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return 0;
    }

    pPeerManageTbl = (tPeerManageTbl *)
        RBTreeGet (dtlsPeerManageTable, &PeerManageTbl);

    if (pPeerManageTbl != NULL)
    {
        if (pPeerManageTbl->u4Id == u4Id)
        {
            pCtxt = pPeerManageTbl->pDtlsCtxt;

            /* release lock */
            OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

            return pCtxt;
        }
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return NULL;
}

/*****************************************************************************/
/* Function Name      : dtlsGetAppCBApiFromAppTbl                            */
/*                                                                           */
/* Description        : This fucntion returns application callback for       */
/*                      encrypt/decrypt from FD provided                     */
/*                                                                           */
/* Input(s)           : u4Fd - File Descriptor                               */
/* Output(s)          : Application callback                                 */
/*                                                                           */
/* Return Value(s)    : 0/Callback                                           */
/*****************************************************************************/
pfPtr
DtlsGetAppCBApiFromAppTbl (UINT4 u4Fd)
{
    tAppManageTbl      *pAppManageTbl = NULL;
    tAppManageTbl       AppManageTbl;
    void                (*pAppCB) (tQueData *) = NULL;

    memset (&AppManageTbl, 0, sizeof (tAppManageTbl));

    AppManageTbl.u4Fd = u4Fd;

    /* acquire lock */
    if (OsixTakeSem (0, (UINT1 *) DTLS_SEM_NAME, 0, 0) != OSIX_SUCCESS)
    {
        return 0;
    }

    pAppManageTbl = (tAppManageTbl *)
        RBTreeGet (dtlsAppManageTable, &AppManageTbl);

    if (pAppManageTbl != NULL)
    {
        if (pAppManageTbl->u4Fd == u4Fd)
        {
            pAppCB = pAppManageTbl->dtlsAppCryptoNotify;

            /* release lock */
            OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

            return (pAppCB);
        }
    }

    /* release lock */
    OsixGiveSem (0, (UINT1 *) DTLS_SEM_NAME);

    return NULL;
}

/*****************************************************************************/
/* Function Name      : dtlsGetUniqueId                                      */
/*                                                                           */
/* Description        : This function returns ID from context                */
/*                                                                           */
/* Input(s)           : u4Context - Context id                               */
/* Output(s)          : unique ID                                            */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
UINT4
DtlsGetUniqueId (UINT4 u4Context)
{
    UINT4               u4Id = 0;

    u4Id = u4Context;

    return u4Id;
}

#endif /* _DTLS_LIBRARY_C */
