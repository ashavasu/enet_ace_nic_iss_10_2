/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbicsem.h,v 1.2 2016/06/24 09:44:04 siva Exp $
*
* Description: Data structure used by Redundancy manager State event 
*              machine.
*********************************************************************/
#ifndef _HB_SEM_H_
#define _HB_SEM_H_

#ifdef ICCH_WANTED 

enum {
    ICCH0 = 0,
    ICCH1,
    ICCH2,
    ICCH3,
    ICCH4,
    ICCH5,
    ICCH6,
    ICCH7,
    ICCH8,
    ICCH9,
    ICCH10,
    MAX_ICCH_FN_PTRS
};
typedef void (*tHbIcchActionProc) (VOID);

/* ICCH SEM function pointers */
tHbIcchActionProc HbIcchActionProc [MAX_ICCH_FN_PTRS] =
{
   HbIcchSemEvntIgnore,                   /* ICCH0 */
   HbIcchSem1WayRcvdInInitState,          /* ICCH1 */
   HbIcchSem2WayRcvdInInitState,          /* ICCH2 */
   HbIcchSemMasterElctdInInitState,       /* ICCH3 */
   HbIcchSemPeerDownRcvdInInitState,      /* ICCH4 */
   HbIcchSemPeerDownRcvdInMasterState,    /* ICCH5 */
   HbIcchSemPeerDownRcvdInSlaveState,     /* ICCH6 */
   HbIcchSem2WayRcvdInMasterState,        /* ICCH7 */
   HbIcchSem1WayRcvdInMasterState,        /* ICCH8 */
   HbIcchSem1WayRcvdInSlaveState,         /* ICCH9 */
   HbIcchSem2WayRcvdInSlaveState          /* ICCH10 */
   
};

/* ICCH State Event Table */
const UINT1 au1HbIcchSem[HB_MAX_EVENTS][HB_ICCH_MAX_STATES] =
/*  ICCH     ICCH    ICCH    ICCH
 *  INIT     MASTER  SLAVE   TRANSITION
 *                           IN PROGRESS  */ /* States */
{  
   {ICCH1,   ICCH8,  ICCH9,  ICCH0  },    /* 1WAY_RCVD */ /* Events */
   {ICCH2,   ICCH7,  ICCH10, ICCH0  },    /* 2WAY_RCVD */     
   {ICCH3,   ICCH0,  ICCH0,  ICCH0  },    /* MASTER_ELCTD */
   {ICCH4,   ICCH5,  ICCH6,  ICCH0  },    /* PEER_DOWN */
   {ICCH0,   ICCH0,  ICCH0,  ICCH0  },    /* IGNORE_PKT */
};
const CHR1 *au1HbIcchStateStr[HB_ICCH_MAX_STATES] = {
   "INIT",
   "MASTER",
   "SLAVE",
   "TRANSITION_IN_PROGRESS"
};

const CHR1 *au1HbIcchEvntStr[HB_ICCH_MAX_EVENTS] = {
   "1WAY_RCVD",
   "2WAY_RCVD",
   "MASTER_ELCTD",
   "PEER_DOWN",
   "IGNORE_PKT",
};
#endif 
#endif /* _HB_SEM_H_ */
