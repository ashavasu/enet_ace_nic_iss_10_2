/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: fshbwr.h,v 1.3 2015/02/05 11:32:27 siva Exp $
 *
 * Description: Proto types for wrapper function
 *********************************************************************/
#ifndef _FSHBWR_H
#define _FSHBWR_H

VOID RegisterFSHB(VOID);

VOID UnRegisterFSHB(VOID);
INT4 FsHbIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsHbPeerDeadIntMultiplierGet(tSnmpIndex *, tRetVal *);
INT4 FsHbTrcLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsHbStatsEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsHbClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsHbIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsHbPeerDeadIntMultiplierSet(tSnmpIndex *, tRetVal *);
INT4 FsHbTrcLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsHbStatsEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsHbClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsHbIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHbPeerDeadIntMultiplierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHbTrcLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHbStatsEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHbClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHbIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHbPeerDeadIntMultiplierDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHbTrcLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHbStatsEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHbClearStatsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsHbStatsMsgTxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsHbStatsMsgTxFailedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsHbStatsMsgRxCountGet(tSnmpIndex *, tRetVal *);
INT4 FsHbStatsMsgRxProcCountGet(tSnmpIndex *, tRetVal *);
INT4 FsHbStatsRxFailedCountGet(tSnmpIndex *, tRetVal *);
#endif /* _FSHBWR_H */
