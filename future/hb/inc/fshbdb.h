/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fshbdb.h,v 1.4 2015/11/11 08:40:57 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSHBDB_H
#define _FSHBDB_H


UINT4 fshb [] ={1,3,6,1,4,1,29601,2,93};
tSNMP_OID_TYPE fshbOID = {9, fshb};


UINT4 FsHbInterval [ ] ={1,3,6,1,4,1,29601,2,93,0,1};
UINT4 FsHbPeerDeadIntMultiplier [ ] ={1,3,6,1,4,1,29601,2,93,0,2};
UINT4 FsHbTrcLevel [ ] ={1,3,6,1,4,1,29601,2,93,0,3};
UINT4 FsHbStatsEnable [ ] ={1,3,6,1,4,1,29601,2,93,0,4};
UINT4 FsHbClearStats [ ] ={1,3,6,1,4,1,29601,2,93,0,5};
UINT4 FsHbStatsMsgTxCount [ ] ={1,3,6,1,4,1,29601,2,93,1,1};
UINT4 FsHbStatsMsgTxFailedCount [ ] ={1,3,6,1,4,1,29601,2,93,1,2};
UINT4 FsHbStatsMsgRxCount [ ] ={1,3,6,1,4,1,29601,2,93,1,3};
UINT4 FsHbStatsMsgRxProcCount [ ] ={1,3,6,1,4,1,29601,2,93,1,4};
UINT4 FsHbStatsRxFailedCount [ ] ={1,3,6,1,4,1,29601,2,93,1,5};




tMbDbEntry fshbMibEntry[]= {

{{11,FsHbInterval}, NULL, FsHbIntervalGet, FsHbIntervalSet, FsHbIntervalTest, FsHbIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "500"},

{{11,FsHbPeerDeadIntMultiplier}, NULL, FsHbPeerDeadIntMultiplierGet, FsHbPeerDeadIntMultiplierSet, FsHbPeerDeadIntMultiplierTest, FsHbPeerDeadIntMultiplierDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{11,FsHbTrcLevel}, NULL, FsHbTrcLevelGet, FsHbTrcLevelSet, FsHbTrcLevelTest, FsHbTrcLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsHbStatsEnable}, NULL, FsHbStatsEnableGet, FsHbStatsEnableSet, FsHbStatsEnableTest, FsHbStatsEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsHbClearStats}, NULL, FsHbClearStatsGet, FsHbClearStatsSet, FsHbClearStatsTest, FsHbClearStatsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsHbStatsMsgTxCount}, NULL, FsHbStatsMsgTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsHbStatsMsgTxFailedCount}, NULL, FsHbStatsMsgTxFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsHbStatsMsgRxCount}, NULL, FsHbStatsMsgRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsHbStatsMsgRxProcCount}, NULL, FsHbStatsMsgRxProcCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsHbStatsRxFailedCount}, NULL, FsHbStatsRxFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fshbEntry = { 10, fshbMibEntry };

#endif /* _FSHBDB_H */

