/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbglob.h,v 1.8 2017/10/04 14:26:58 siva Exp $
*
* Description: This file contains the global variable declarations  
*              used in HB Module
*********************************************************************/
#ifndef _HBGLOB_H
#define _HBGLOB_H
#ifdef _HBMAIN_C_
tHbInfo         gHbInfo;
tHbStat         gHbStat;
UINT1           gu1KeepAliveFlag = FALSE;
UINT1           gu1IcchKeepAliveFlag = FALSE;
UINT1           gu1HbIcchInitComplete = OSIX_FALSE;
UINT1           gu1StandbyTmrRestartCount;
UINT2           gu2PortMaskValue = 0;
UINT1           gu1HbMclagDisableInProgress = OSIX_FALSE;
#else
extern tHbInfo         gHbInfo;
extern tHbStat         gHbStat;
extern UINT1           gu1KeepAliveFlag;
extern UINT1           gu1IcchKeepAliveFlag;
extern UINT1           gu1HbIcchInitComplete;
extern UINT1           gu1StandbyTmrRestartCount;
extern UINT2           gu2PortMaskValue;
extern UINT1           gu1HbMclagDisableInProgress;
#endif /* _HBMAIN_C_ */
extern UINT4                   gu4RmStackingInterfaceType;
#endif /* _HBGLOB_H */
