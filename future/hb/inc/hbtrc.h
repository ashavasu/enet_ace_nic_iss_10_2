/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbtrc.h,v 1.4 2016/02/24 09:55:25 siva Exp $
*
* Description: Macros related to Trace of _HB_. 
*********************************************************************/
#ifdef TRACE_WANTED
#define HB_TRC_FLAG   gHbInfo.u4HbTrc
#define RM_HB_NAME   "RM_HB"

#define RM_HB_MAX_TRC_NAME_LEN       60

#define HB_PKT_DUMP(TraceType, pBuf, Length, Str) \
{\
  if (HB_TRC_FLAG & (TraceType))\
  {\
     UtlTrcLog(HB_TRC_FLAG,TraceType,  (CONST CHR1 *)"HB", Str); \
     DumpPkt(pBuf, Length);\
  }\
}


#define HB_TRC(Value, Fmt)\
{\
    if (HB_TRC_FLAG & (Value))\
    {\
        UtlTrcLog(HB_TRC_FLAG, Value, (CONST CHR1 *) "HB", \
                  (CONST CHR1 *) Fmt);\
    }\
}

#define HB_TRC1(Value, Fmt, arg1)\
{\
    if (HB_TRC_FLAG & (Value))\
    {\
        UtlTrcLog(HB_TRC_FLAG, Value, (CONST CHR1 *) "HB", \
                  (CONST CHR1 *) Fmt, arg1);\
    }\
}

#define HB_TRC2(Value, Fmt, arg1, arg2)\
{\
    if (HB_TRC_FLAG & (Value))\
    {\
        UtlTrcLog(HB_TRC_FLAG, Value, (CONST CHR1 *) "HB", \
                  (CONST CHR1 *) Fmt, arg1, arg2);\
    }\
}
#else

#define HB_PKT_DUMP(TraceType, pBuf, Length, Str)
#define HB_TRC(Value, Fmt) 
#define HB_TRC1(Value, Fmt, arg1) 
#define HB_TRC2(Value, Fmt, arg1, arg2) 
#endif /* TRACE_WANTED */
