/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbdefn.h,v 1.18 2017/10/04 14:26:58 siva Exp $
*
* Description: Data structure used by Redundancy manager module. 
*********************************************************************/
#ifndef HB_DEFN_H
#define HB_DEFN_H

enum
{
    HEART_BEAT_MSG,    /* 0 - heart beat msg exchgd between peers */
};

#define HB_PROTO_SEM_NAME  (CONST UINT1 *) "RMHS"
#define HB_TASK_NAME   "HBTS"

#define HB_DEF_PRIORITY 1
#define HB_STANDBY_MAX_TMR_RESTART 10

/* ACTIVE side max wait time for the
 *  * force switchover ack from peer */
#define HB_MAX_FSW_ACK_TIME  (2 * HB_GET_PEER_DEAD_INTERVAL()) 

/* RM protocol number 250 is used (which is un-used in /etc/protocols). */
#define HB_PROTO          250
#define HB_ICCH_PROTO     251

/* RM  Multicast ip addr used is 224.0.0.250 */
#define HB_MCAST_IPADDR   0xE00000FA

/* ICCH  Multicast ip addr used is 224.0.0.251 */
#define HB_ICCH_MCAST_IPADDR   0xE00000FB

/* Allocate CRU buf and set the valid offset to point to 0 */
#define HB_ALLOC_RX_BUF(size)    CRU_BUF_Allocate_MsgBufChain (size, 0)
#define HB_ALLOC_TX_BUF(size)    \
            CRU_BUF_Allocate_MsgBufChain (size+HB_HDR_LENGTH, HB_HDR_LENGTH)
#define HB_COPY_TO_LINEAR_BUF(CruBuf, LinBuf, offset, Len) \
        CRU_BUF_Copy_FromBufChain (CruBuf, (UINT1 *)LinBuf, offset,Len)

#define HB_VERSION_NUM  1

#define HB_CALC_CKSUM UtlIpCSumCruBuf 
#define HB_LINEAR_CALC_CKSUM UtlIpCSumLinBuf 

/* Macro to prepend to HB Hdr to data */
#define HB_PREPEND_BUF(pBuf, pSrc, u4Size) \
        CRU_BUF_Prepend_BufChain(pBuf, pSrc, u4Size) 
   
/*Release the HB buffer */
#define HB_FREE(size)     CRU_BUF_Release_MsgBufChain (size, FALSE)

/*Get data from HB buffer*/
#define HB_GET_DATA_1_BYTE(pMsg, u4Offset, u1Value) \
        CRU_BUF_Copy_FromBufChain(pMsg, (UINT1 *)&u1Value, u4Offset, 1)

#define HB_GET_DATA_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS ((UINT2)u2Value);\
}



#define HB_GET_DATA_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL ((UINT4)u4Value);\
}

#define HB_GET_DATA_N_BYTE(pMsg, pdst, u4Offset, u4size) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) pdst), u4Offset, u4size);\
}

#define HB_COPY(CruBuf, LinBuf, size)  \
        CRU_BUF_Copy_OverBufChain (CruBuf, LinBuf, 0, size)

#define HB_COPY_TO_OFFSET(dest, src, offset, size) \
        CRU_BUF_Copy_OverBufChain (dest, (UINT1 *) src, offset, size)

/*Moving to data in buffer*/
#define HB_PKT_MOVE_TO_DATA(pBuf, u4HLen) \
       { \
        CRU_BUF_Move_ValidOffset (pBuf, u4HLen);\
       }

#define HB_MCLAG_ENABLED LA_MCLAG_ENABLED
#define HB_MCLAG_DISABLED LA_MCLAG_DISABLED
/* Max. read/write buffer size */
#define  HB_MAX_BUF_SIZE     100

#define  HB_IF_NAME_LEN  16

#define  HB_DEF_MSG_LEN            OSIX_DEF_MSG_LEN

#define  MAX_IP_HB_PKT_LEN 200

/* Rm Heart Beat message format */
typedef struct {

   /* MsgType - HEART_BEAT */
   UINT4 u4MsgType;

   /* Heart Beat Interval */
   UINT4 u4HbInterval;

   /* Peer Dead Interval */
   UINT4 u4PeerDeadInterval;

   /* Peer Dead Interval Multiplier */
   UINT4 u4PeerDeadIntMultiplier;
#ifdef RM_WANTED
   /* Active Node IP addr */
   UINT4 u4ActiveNode;

   /* Self node IP addr */
   UINT4 u4SelfNode;

   /* Peer node IP addr */
   UINT4 u4PeerNode;
#endif

#ifdef ICCH_WANTED
   /* Master Node IP addr */
   UINT4 u4MasterNode;

   /* Self node IP addr */
   UINT4 u4IcchSelfNode;

   /* Self node subnet mask */
   UINT4 u4IcchSelfMask;

   /* Peer node IP addr */
   UINT4 u4IcchPeerNode;
#endif

#ifdef RM_WANTED  
   /* Self node Priority */
   UINT4 u4SelfNodePriority;

   /* Peer node Priority */
   UINT4 u4PeerNodePriority;
    
#endif
   /* Bitmap - Bit 0 is currently used for force-switchover */   
   UINT4 u4Flags;
   UINT2  u2MclagPortMaskValue;/*This mask value will be used for 
                               *masking the MCLAG port number to the
                               *peer node.MC-LAG should have some logical port
                               *as Actor port in LACP which can distinguish
                               * both MC-LAG nodes. */


   /* String to store the ISS software version.
    * This field will be used when ISSU is enabled and
    * incase of Non disruptive mode if software versions are
    * different hb packets needs to be dropped, to ensure the 
    * stacking link is down */
    UINT1  au1IssVersion[ISS_STR_LEN];
} tHbData;

typedef struct {
   UINT4 u4IpAddr;
   UINT4 u4NetMask;
}tHbNodeInfo;

/* RM HB Timer Types */
enum
{
    HB_HEARTBEAT_TIMER              = 1, /* Heart Beat Timer */
    HB_PEER_DEAD_TIMER              = 2, /* Peer Dead Timer */
    HB_FSW_ACK_TIMER                = 3, /* Force Switchover Ack Timer */
    HB_ICCH_HEARTBEAT_TIMER         = 4, /* Heart beat Timer for ICCH */
    HB_ICCH_PEER_DEAD_TIMER         = 5, /* Peer Dead Timer for ICCH */
    HB_MCLAG_DISABLE_TIMER          = 6, /* MC-LAG disable timer*/
    HB_MAX_TIMER_TYPES              = 7  /* Max Timer types */
};

/****************************************************************************/
enum{
        HB_STATS_ENABLE  =1,
        HB_STATS_DISABLE,
        HB_CLEAR_STATS_TRUE,
        HB_CLEAR_STATS_FALSE
};

typedef struct HbStat{
    UINT4 u4MsgTxCount; /* Number of HB msgs successfully sent */
    UINT4 u4MsgTxFailedCount; /* Number of HB msgs failed while sending*/
    UINT4 u4MsgRxCount; /* Number of HB msgs received */
    UINT4 u4MsgRxProcCount; /* Number of HB msgs received and processed */
    UINT4 u4RxFailedCount; /* Number of HB msgs failed while processing */
} tHbStat;

/* HB Registeration information */
typedef struct {
    UINT4 u4AppId;
}tHbRegInfo;

typedef struct {
    tOsixSemId  HbProtoSemId; /* HB protocol Semaphore Identifier */
    tOsixTaskId HbTaskId; /* Trace value */
    tOsixQId     RmHbQueueId; /* Queue to Process RM HB Messages
                                 received on Stacking Port */
    tOsixQId     HbIcchQueueId; /* Queue to Process ICCH HB Messages
                                   received on ICCL Port */
    /* Timers*/
    tTmrDesc    aTmrDesc[HB_MAX_TIMER_TYPES];
                          /* Timer data struct that contains
                             func ptrs for timer handling and
                             offsets to identify the data
                             struct containing timer block.
                             Timer ID is the index to this
                             data structure */
    /* RM HB Timer List ID */
    tTimerListId    TmrListId;
    /* RM HB Timer block to hold the timer id */
    tTmrBlk    HbTmrBlk[HB_MAX_TIMER_TYPES];
    tHbRegInfo HbRegInfo[HB_MAX_APPS];
    UINT4  u4HbTrc; /* Trace value */
    UINT4  u4HbTrcBkp;
    UINT4  u4HbInterval; /* Hear Beat msg interval */
    UINT4  u4PeerDeadInterval; /* Peer node dead interval */
    UINT4  u4PeerDeadIntMultiplier; /* Peer node dead interval Multiplier */
    UINT4  u4Flags; /* Flag to identify the bit set in rcvd msg */
#ifdef RM_WANTED
    UINT4  u4PrevNodeState; /* Previous State of the node */
    UINT4  u4NodeState; /* State of the node */
    UINT4  u4ActiveNode; /* Ip addr of ACTIVE node */
    UINT4  u4SelfNode; /* Ip addr of this node */
    UINT4  u4SelfNodeMask; /* Subnet Mask of this node */
    UINT4  u4PeerNode; /* Ip addr of peer node */
    UINT4  u4SelfNodePriority;  /* Self node priority */
    UINT4  u4PeerNodePriority; /* Peer node priority */
    UINT4  u4PrevNotifNodeState;
#endif /* RM_WANTED */
    UINT4  u4IcchNodeState; /* State of the node */
    UINT4  u4IcchMasterNode; /* Ip addr of MASTER node */
    UINT4  u4IcchPeerNode; /* Ip addr of peer node */
    UINT4  u4IcchSelfNode; /* Ip addr of MC-LAG node */
    UINT4  u4IcchSelfNodeMask; /* Subnet Mask of MC-LAG node */
    UINT4  u4IcchPrevNodeState; /* Previous State of the node */
    UINT4  u4IcchPrevNotifNodeState; /* Previously notified state of Node */
    UINT4  u4IcchFlags; /* Used toconvey the node state information to the peer node, while sending HB packets. */ 
    UINT4  u4HwInOctets; /* HB received Byte count fetched from NP. */ 
    UINT4  u4HwInPauseFrame; /* HB received pause frame count fetched from NP. */ 
    UINT4  u4HwOutPauseFrames; /* HB transmitted pause frame count fetched from NP. */ 
    INT4   i4IcchTcpSockFd; /* TCP socket to send/rcv ICCH Sync msgs */
    INT4   i4IcchRawSockFd; /* RAW socket to send/rcv HB & other msgs */
    INT4   i4RawSockFd; /* RAW socket to send/rcv HB & other msgs */
    INT4   i4ClearStats;
    UINT1  au1SysMac[MAC_ADDR_LEN];
    UINT1  au1DestMac[MAC_ADDR_LEN];
    UINT1  u1IcchPeerNodeState; /* State of the peer node */
    BOOL1  bHbIcchPeerNewlyArrived; /* This is set when peer is newly arrived */
#ifdef RM_WANTED
    UINT1  u1PeerNodeState; /* State of the peer node */
    UINT1  u1ForceSwitchOverFlag;
    UINT1  u1StdbySoftRbt;
    UINT1  au1RmPad [1];
#endif
    UINT1  u1StatsEnable; /* Enable or disable Statistics */
   /* String to store the ISS software version.
    * This field will be used when ISSU is enabled and
    * incase of Non disruptive mode if software versions are
    * different hb packets needs to be dropped, to ensure the
    * stacking link is down */
    UINT1  au1IssVersion[ISS_STR_LEN];
    UINT1  au1HbPad [3];
} tHbInfo;

#define HB_ICCH_TCP_SRV_SOCK_FD() (gHbInfo.i4IcchTcpSockFd)
#define HB_GET_MSG_TX_COUNT() (gHbStat.u4MsgTxCount)
#define HB_GET_MSG_TX_FCOUNT() (gHbStat.u4MsgTxFailedCount)
#define HB_GET_MSG_RX_COUNT() (gHbStat.u4MsgRxCount)
#define HB_GET_MSG_RX_PCOUNT() (gHbStat.u4MsgRxProcCount)
#define HB_GET_MSG_RX_FCOUNT() (gHbStat.u4RxFailedCount)
#define HB_INCR_MSG_TX_COUNT() \
{ \
    if (HB_GET_STATS_ENABLE () == HB_STATS_ENABLE) \
    { \
        (gHbStat.u4MsgTxCount) ++; \
    }\
}
#define HB_INCR_MSG_TX_FCOUNT() \
{ \
    if (HB_GET_STATS_ENABLE () == HB_STATS_ENABLE) \
    { \
        (gHbStat.u4MsgTxFailedCount) ++; \
    }\
}
#define HB_INCR_MSG_RX_COUNT() \
{ \
    if (HB_GET_STATS_ENABLE () == HB_STATS_ENABLE) \
    { \
        (gHbStat.u4MsgRxCount) ++; \
    }\
}
#define HB_INCR_MSG_RX_PCOUNT() \
{ \
    if (HB_GET_STATS_ENABLE () == HB_STATS_ENABLE) \
    { \
        (gHbStat.u4MsgRxProcCount) ++; \
    }\
}
#define HB_INCR_MSG_RX_FCOUNT() \
{ \
    if (HB_GET_STATS_ENABLE () == HB_STATS_ENABLE) \
    { \
        (gHbStat.u4RxFailedCount) ++; \
    }\
}
#define HB_GET_STATS_ENABLE() (gHbInfo.u1StatsEnable)
#define HB_GET_SELF_NODE_PRIORITY()    (gHbInfo.u4SelfNodePriority)
#define HB_GET_PEER_NODE_PRIORITY()    (gHbInfo.u4PeerNodePriority)
#define HB_GET_HB_INTERVAL()     (gHbInfo.u4HbInterval)
#define HB_GET_PEER_DEAD_INTERVAL()     (gHbInfo.u4PeerDeadInterval)
#define HB_GET_PEER_DEAD_INT_MULTIPLIER() (gHbInfo.u4PeerDeadIntMultiplier)
#define HB_GET_FLAGS()           (gHbInfo.u4Flags)
#define HB_PROTO_SEM()           (gHbInfo.HbProtoSemId)
#define HB_TASK_ID            (gHbInfo.HbTaskId)
#define HB_RAW_SOCK_FD()         (gHbInfo.i4RawSockFd)
#define HB_ICCH_RAW_SOCK_FD()         (gHbInfo.i4IcchRawSockFd)
#define HB_QUEUE_ID         (gHbInfo.RmHbQueueId)
#define HB_ICCH_GET_FLAGS()           (gHbInfo.u4IcchFlags)
#define HB_ICCH_GET_MASTER_NODE_ID()  (gHbInfo.u4IcchMasterNode)
#define HB_ICCH_GET_SELF_NODE_ID()    (gHbInfo.u4IcchSelfNode)
#define HB_ICCH_GET_SELF_NODE_MASK()  (gHbInfo.u4IcchSelfNodeMask)
#define HB_ICCH_GET_PEER_NODE_ID()    (gHbInfo.u4IcchPeerNode)
#define HB_ICCH_GET_NODE_STATE()      (gHbInfo.u4IcchNodeState)
#define HB_ICCH_GET_PREV_NOTIF_NODE_STATE()      (gHbInfo.u4IcchPrevNotifNodeState)
#define HB_ICCH_GET_PREV_NODE_STATE() (gHbInfo.u4IcchPrevNodeState)
#define HB_ICCH_GET_PEER_NODE_STATE() (gHbInfo.u1IcchPeerNodeState)
#define HB_ICCH_QUEUE_ID         (gHbInfo.HbIcchQueueId)
#define HB_ICCH_PEER_NEWLY_ARRIVED gHbInfo.bHbIcchPeerNewlyArrived
#ifdef RM_WANTED
#define HB_GET_ACTIVE_NODE_ID()  (gHbInfo.u4ActiveNode)
#define HB_GET_SELF_NODE_ID()    (gHbInfo.u4SelfNode)
#define HB_GET_SELF_NODE_MASK()  (gHbInfo.u4SelfNodeMask)
#define HB_GET_PEER_NODE_ID()    (gHbInfo.u4PeerNode)
#define HB_GET_NODE_STATE()      (gHbInfo.u4NodeState)
#define HB_GET_PREV_NOTIF_NODE_STATE()      (gHbInfo.u4PrevNotifNodeState)
#define HB_GET_PREV_NODE_STATE() (gHbInfo.u4PrevNodeState)
#define HB_GET_PEER_NODE_STATE() (gHbInfo.u1PeerNodeState)
#endif
#define HB_GET_TRC_LEVEL()       (gHbInfo.u4HbTrc)
#define HB_FORCE_SWITCHOVER_FLAG() (gHbInfo.u1ForceSwitchOverFlag)

#define MAX_HB_CTRL_MSG_NODES             1000
#define HB_MCLAG_DISABLE_TIME             5000 /*in milli seconds*/
/*Invalid socket descriptor*/
#define HB_INV_SOCK_FD -1

/* RM states */
enum {
   HB_RM_INIT = 0,
   HB_RM_ACTIVE,
   HB_RM_STANDBY,
   HB_RM_TRANSITION_IN_PROGRESS, /* Node transition in progress */
   HB_RM_MAX_STATES /* Count of states. If any new states, add before this  */
};

/*TRACE LEVEL*/
#define HB_DEFAULT_TRC      HB_CRITICAL_TRC 

#define HB_SET_TRC_LEVEL(TrcLevel) \
                                 (gHbInfo.u4HbTrc = TrcLevel)
#define HB_TAKE_TRC_BACKUP() \
    (gHbInfo.u4HbTrcBkp = HB_GET_TRC_LEVEL())
#define HB_RESTORE_TRC_BACKUP() \
    (HB_SET_TRC_LEVEL(gHbInfo.u4HbTrcBkp))

#define HB_SET_NODE_STATE(state) \
{\
    if (HB_RM_TRANSITION_IN_PROGRESS == state) \
    {\
        HB_TRC_START_SWITCHOVER();          \
    }\
    else if (HB_RM_TRANSITION_IN_PROGRESS == HB_GET_PREV_NODE_STATE()) \
    {\
        HB_TRC_FINISH_SWITCHOVER(); \
    }\
    gHbInfo.u4NodeState = state; \
}

#ifdef ICCH_WANTED
#define HB_ICCH_SET_NODE_STATE(state) \
{\
    gHbInfo.u4IcchNodeState = state; \
}
#endif

/* Call this whenever RM_IS_NODE_TRANSITION_IN_PROGRESS is SET */
#define HB_TRC_START_SWITCHOVER() \
{\
    if ((HB_GET_TRC_LEVEL() & HB_SWITCHOVER_TRC)) \
    {\
        HB_TAKE_TRC_BACKUP();         \
        HB_TRC (HB_SWITCHOVER_TRC, "---SwitchOver Started[HB]---\n");\
        HB_SET_TRC_LEVEL(HB_ALL_TRC); \
    }\
}
    
/* Call this whenever RM_IS_NODE_TRANSITION_IN_PROGRESS is RESET */
#define HB_TRC_FINISH_SWITCHOVER() \
{\
    if ((HB_GET_TRC_LEVEL() & HB_SWITCHOVER_TRC)) \
    {\
        HB_TRC (HB_SWITCHOVER_TRC, "---SwitchOver Finished[HB]---\n");\
        HB_RESTORE_TRC_BACKUP();       \
    }\
}

#define HB_LOCK()              HbLock ()
#define HB_UNLOCK()            HbUnLock ()

#define HB_SET_SELF_NODE_PRIORITY(Pri) \
                             (gHbInfo.u4SelfNodePriority = Pri)
#define HB_SET_PEER_NODE_PRIORITY(Pri) \
                             (gHbInfo.u4PeerNodePriority = Pri)
#define HB_SET_HB_INTERVAL(HbInt) \
{\
    gHbInfo.u4HbInterval = HbInt; \
    HB_SET_PEER_DEAD_INTERVAL(gHbInfo.u4HbInterval * \
                              HB_GET_PEER_DEAD_INT_MULTIPLIER()); \
}
#define HB_SET_PEER_DEAD_INTERVAL(PeerDeadInt) \
                             (gHbInfo.u4PeerDeadInterval = PeerDeadInt)

#define HB_SET_PEER_DEAD_INT_MULTIPLIER(PeerDeadIntMultiplier) \
{\
    gHbInfo.u4PeerDeadIntMultiplier = PeerDeadIntMultiplier; \
    HB_SET_PEER_DEAD_INTERVAL(HB_GET_HB_INTERVAL() * \
                                 gHbInfo.u4PeerDeadIntMultiplier);\
}

#define HB_SET_ACTIVE_NODE_ID(NodeIp) \
                             (gHbInfo.u4ActiveNode = NodeIp)

#ifdef ICCH_WANTED
#define HB_ICCH_SET_MASTER_NODE_ID(NodeIp) \
                             (gHbInfo.u4IcchMasterNode = NodeIp)
#define HB_ICCH_SET_PEER_NODE_ID(NodeIp) \
                             (gHbInfo.u4IcchPeerNode = NodeIp)

#define HB_ICCH_SET_PREV_NOTIF_NODE_STATE(state) \
                             (gHbInfo.u4IcchPrevNotifNodeState = state)
#define HB_ICCH_SET_PREV_NODE_STATE(state) \
                             (gHbInfo.u4IcchPrevNodeState = state)
#define HB_ICCH_SET_PEER_NODE_STATE(state) \
                             (gHbInfo.u1IcchPeerNodeState = state)
#define HB_ICCH_SET_FLAGS(flags) \
                             (gHbInfo.u4IcchFlags |= flags)
#define HB_ICCH_SET_PREV_NOTIF_NODE_STATE(state) \
                             (gHbInfo.u4IcchPrevNotifNodeState = state)
#define HB_ICCH_RESET_FLAGS(flags) \
                             (gHbInfo.u4IcchFlags &= (UINT4) ~flags)
#endif
#define HB_SET_PEER_NODE_ID(NodeIp) \
                             (gHbInfo.u4PeerNode = NodeIp)

#define HB_SET_PREV_NOTIF_NODE_STATE(state) \
                             (gHbInfo.u4PrevNotifNodeState = state)
#define HB_SET_PREV_NODE_STATE(state) \
                             (gHbInfo.u4PrevNodeState = state)
#define HB_SET_PEER_NODE_STATE(state) \
                             (gHbInfo.u1PeerNodeState = state)
#define HB_SET_FLAGS(flags) \
                             (gHbInfo.u4Flags |= flags)

#define HB_RESET_FLAGS(flags) \
                             (gHbInfo.u4Flags &= (UINT4) ~flags)

#define HB_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define HB_DATA_PUT_2_BYTE(pMsg, u4Offset, u2Value, i4RetVal) \
{\
   UINT2  LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   i4RetVal = \
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &LinearBuf), u4Offset, 2);\
}
#define HB_SET_STATS_ENABLE(i4SetValFsHbStatsEnable) \
    (gHbInfo.u1StatsEnable= (UINT1) i4SetValFsHbStatsEnable)

#define HB_GET_TASK_ID           OsixGetTaskId
#define HB_SEND_EVENT            OsixEvtSend
#define HB_RECV_EVENT            OsixEvtRecv
#define HB_CREATE_QUEUE          OsixQueCrt
#define HB_DELETE_QUEUE            OsixQueDel
#define HB_DELETE_SEMAPHORE(semid) OsixSemDel(semid)
#define HB_SEND_TO_QUEUE         OsixQueSend 
#define HB_RECEIVE_FROM_QUEUE    OsixQueRecv
#define HB_Q_NAME                "RMHQ"
#define HB_ICCH_Q_NAME             "ICHQ"
/* RM ACTIVE node discovery state machine - STATES & EVENTS */
/* RM events */
enum {
   HB_1WAY_RCVD = 0,
   HB_2WAY_RCVD,
   HB_ACTV_ELCTD,
   HB_PEER_DOWN,
   HB_PRIORITY_CHG,
   HB_IGNORE_PKT,
   HB_FSW_RCVD,
   HB_FSW_ACK_RCVD,
   HB_MAX_EVENTS  /* Count of events. If any new events, add before this  */
};

/* RM software reboot states */
enum
{
    HB_STDBY_SOFT_RBT_NOT_OCCURED = 0,
    HB_STDBY_SOFT_RBT_NODE_DOWN
};

#define HB_ARP_INVALID ARP_INVALID
#define HB_ARP_ENET_V2_ENCAP ARP_ENET_V2_ENCAP
#define HB_ARP_STATIC ARP_STATIC
#define HB_MASK_VALUE_1  0x8000
#define HB_MASK_VALUE_2  0x4000
#define HB_ZERO  0
#define HB_ONE   1

#ifdef ICCH_WANTED 
/* ICCH states */
enum {
   HB_ICCH_INIT = 0,
   HB_ICCH_MASTER,
   HB_ICCH_SLAVE,
   HB_ICCH_TRANSITION_IN_PROGRESS, /* Node transition in progress */
   HB_ICCH_MAX_STATES /* Total number of states. Add new events before this  */
};

/* ICCH events */
enum {
   HB_ICCH_1WAY_RCVD = 0,
   HB_ICCH_2WAY_RCVD,
   HB_ICCH_MASTER_ELCTD,
   HB_ICCH_PEER_DOWN,
   HB_ICCH_IGNORE_PKT,
   HB_ICCH_MAX_EVENTS  /* Total number of events. Add new events before this  */
};
#endif

#endif /* HB_DEFN_H */
