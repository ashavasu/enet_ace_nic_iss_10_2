/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: fshblw.h,v 1.3 2015/02/05 11:32:27 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsHbInterval ARG_LIST((UINT4 *));

INT1
nmhGetFsHbPeerDeadIntMultiplier ARG_LIST((UINT4 *));

INT1
nmhGetFsHbTrcLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsHbStatsEnable ARG_LIST((INT4 *));

INT1
nmhGetFsHbClearStats ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsHbInterval ARG_LIST((UINT4 ));

INT1
nmhSetFsHbPeerDeadIntMultiplier ARG_LIST((UINT4 ));

INT1
nmhSetFsHbTrcLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsHbStatsEnable ARG_LIST((INT4 ));

INT1
nmhSetFsHbClearStats ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsHbInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsHbPeerDeadIntMultiplier ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsHbTrcLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsHbStatsEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsHbClearStats ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsHbInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsHbPeerDeadIntMultiplier ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsHbTrcLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsHbStatsEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsHbClearStats ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsHbStatsMsgTxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsHbStatsMsgTxFailedCount ARG_LIST((UINT4 *));

INT1
nmhGetFsHbStatsMsgRxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsHbStatsMsgRxProcCount ARG_LIST((UINT4 *));

INT1
nmhGetFsHbStatsRxFailedCount ARG_LIST((UINT4 *));
