/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbsem.h,v 1.4 2016/07/30 10:41:31 siva Exp $
*
* Description: Data structure used by Redundancy manager State event 
*              machine.
*********************************************************************/
#ifndef _HB_SEM_H_
#define _HB_SEM_H_

#ifdef RM_WANTED
enum {
    HB0 = 0,
    HB1,
    HB2,
    HB3,
    HB4,
    HB5,
    HB6,
    HB7,
    HB8,
    HB9,
    HB10,
    HB11,
    HB12,
    HB13,
    MAX_HB_FN_PTRS
};

typedef void (*tHbRmActionProc) (VOID);
/* HB SEM function pointers */
tHbRmActionProc HbRmActionProc [MAX_HB_FN_PTRS] = 
{
   HbRmSemEvntIgnore,                   /* HB0 */
   HbRmSem1WayRcvdInInitState,          /* HB1 */
   HbRmSem2WayRcvdInInitState,          /* HB2 */
   HbRmSemActvElctdInInitState,         /* HB3 */
   HbRmSemPeerDownRcvdInInitState,      /* HB4 */ 
   HbRmSemPeerDownRcvdInActiveState,    /* HB5 */ 
   HbRmSemPeerDownRcvdInStandbyState,   /* HB6 */ 
   HbRmSemPeerDownRcvdInTrgsInPrgs,     /* HB7 */ 
   HbRmSemPriorityChg,                  /* HB8 */
   HbRmSem2WayRcvdInActvState,          /* HB9 */
   HbRmSemFSWRcvdInActiveState,        /* HB10 */
   HbRmSemFSWRcvdInStandbyState,        /* HB11 */
   HbRmSemFSWAckRcvdInTrgsInPrgsState,  /* HB12 */
   HbRmSem1WayRcvdInStandbyState        /* HB13 */    
};
/* Rm State Event Table */
const UINT1 au1HbRmSem[HB_MAX_EVENTS][HB_RM_MAX_STATES] =
/*  HB     HB    HB    HB
 *  INIT   ACTV  STNBY TRANSITION
 *                     IN PROGRESS  */ /* States */
{  
   {HB1,   HB0,  HB13,  HB0  },  /* HB_1WAY_RCVD */ /* Events */
   {HB2,   HB9,  HB0,  HB0  },  /* HB_2WAY_RCVD */
   {HB3,   HB0,  HB0,  HB0  },  /* HB_ACTV_ELCTD */
   {HB4,   HB5,  HB6,  HB7  },  /* HB_PEER_DOWN */
   {HB8,   HB8,  HB8,  HB0  },  /* HB_PRIORITY_CHG */
   {HB0,   HB0,  HB0,  HB0  },  /* HB_IGNORE_PKT */
   {HB0,   HB10, HB11, HB0  },  /* HB_FSW_RCVD */
   {HB0,   HB0,  HB0,  HB12 }   /* HB_FSW_ACK_RCVD */
};
const CHR1 *au1HbRmStateStr[HB_RM_MAX_STATES] = {
   "INIT",
   "ACTIVE",
   "STANDBY",
   "TRANSITION_IN_PROGRESS"
};

const CHR1 *au1HbRmEvntStr[HB_MAX_EVENTS] = {
   "1WAY_RCVD",
   "2WAY_RCVD",
   "ACTV_ELCTD",
   "PEER_DOWN",
   "PRIORITY_CHG",
   "IGNORE_PKT",
   "RM_FSW_RCVD",
   "RM_FSW_ACK_RCVD"
};
#endif
#endif /* _HB_SEM_H_ */
