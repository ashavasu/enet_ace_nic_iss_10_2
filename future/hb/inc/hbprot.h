/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbprot.h,v 1.16 2017/10/04 14:26:58 siva Exp $
*
* Description: HB module function prototypes. 
*********************************************************************/
/* Function Prototypes */

/* hbmain.c */
VOID  HbProcessRcvEvt (VOID);
UINT4 HbTaskInit (VOID);
UINT4 HbInit (VOID);
VOID  HbProcessMsg (tHbMsg * pHbData, UINT4 u4DataLen, 
                      UINT4 u4PeerAddr);
VOID  HbProcessMsgRcvdOnStkPort (VOID);
UINT4 HbProcessNodeId (tHbData * pHbData, UINT1 *pu1Event);
UINT4 HbProcessNodePriority (tHbData * pHbData, UINT1 *pu1Event);
UINT4 HbFormAndSendMsg (VOID);
UINT4 HbFormMsg (tHbData * pHbData);
INT4  HbWaitForOtherModulesBootUp (VOID);
UINT4 HbPrependHbHdr (tHbMsg * pHbMsg, UINT4 u4Size);
UINT4 HbSendMsg (tHbMsg * pHbMsg, UINT4 u4DestAddr);
UINT4 HbSendMsgToPeer (UINT4 u4Msg);
VOID HbIcchProcessRcvEvt (VOID); 
VOID HbProcessIcchMsg (tHbMsg * pHbData);

#ifdef RM_WANTED
VOID  HbHandleFSWEvent (VOID);
VOID  HbHandleRxBufProcessedEvt (VOID);
VOID  HbRmUpdateForceStandbyState (VOID);
#endif
VOID  HbChgNodeStateToTransInPrgs (VOID);
VOID  HbPostSelfAttachEvent(VOID);
INT4  HbWaitForSelfAttachCompletion (VOID);
PUBLIC VOID  HbTxTsk (VOID);

#ifdef ICCH_WANTED
VOID HbIcchInitiateHBMsg (VOID);
UINT4 HbIcchFormAndSendMsg (VOID);
UINT4 HbIcchSendMsgToPeer (UINT4 u4Msg);
UINT4 HbIcchFormMsg (tHbData * pHbData);
UINT4 HbIcchSendMsg (tHbMsg * pHbMsg, UINT4 u4DestAddr);
UINT4 HbIcchPrependHbHdr (tHbMsg * pHbMsg, UINT4 u4Size);
UINT4 HbIcchProcessNodeId (tHbData * pHbData, UINT1 *pu1Event);
VOID HbIcchSem1WayRcvdInMasterState (VOID);
VOID HbIcchSem1WayRcvdInSlaveState (VOID);
VOID HbTxIcchTsk (VOID);
VOID HbProcessMsgRcvdOnICCLPort (VOID);

#endif
/* hbtmr.c */
UINT4 HbTmrInit (VOID);
UINT4 HbTmrDeInit (VOID);
VOID  HbTmrExpHandler (VOID);
UINT4 HbTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal);
UINT4 HbTmrStopTimer (UINT1 u1TmrId);
UINT4 HbTmrRestartTimer (UINT1 u1TmrId, UINT4 u4TmrVal);
VOID  HbTmrHdlHbTmrExp (VOID *pArg);
VOID  HbTmrHdlPeerDeadTmrExp (VOID *pArg);
VOID  HbTmrHdlFswAckTmrExp (VOID *pArg);
INT4  HbResumeProtocolOperation (VOID);
VOID  HbIcchSetMclagEnableFlag (VOID *pArg);

VOID  HbIcchTmrHdlHbTmrExp (VOID *pArg);
VOID  HbIcchTmrHdlPeerDeadTmrExp (VOID *pArg);

/* hbsock.c */
INT4  HbRawRmSockInit (INT4 *pi4HbSockFd);
VOID  HbRawPktRcvd (INT4 i4SockFd);
UINT4 HbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, 
                       UINT4 u4DestAddr);
INT4  HbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, 
                      UINT4 *pu4PeerAddr);
UINT4 HbGetIfIndexFromName (UINT1 *pu1IfName);
UINT4 HbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo);
INT4  HbCloseSocket (INT4 i4SockFd);

/* hbsem.c */
VOID HbStateMachine (UINT1 u1Event);
VOID HbRmSem1WayRcvdInInitState (VOID);
VOID HbRmSem2WayRcvdInInitState (VOID);
VOID HbRmSem2WayRcvdInActvState (VOID);
VOID HbRmSemActvElctdInInitState (VOID);
VOID HbRmSemPeerDownRcvdInInitState (VOID);
VOID HbRmSemPeerDownRcvdInActiveState (VOID);
VOID HbRmSemPeerDownRcvdInStandbyState (VOID);
VOID HbRmSemPeerDownRcvdInTrgsInPrgs (VOID);
VOID HbRmSemFSWRcvdInActiveState (VOID);
VOID HbRmSemFSWRcvdInStandbyState (VOID);
VOID HbRmSemFSWAckRcvdInTrgsInPrgsState (VOID);
VOID HbRmSemEvntIgnore (VOID);
VOID HbRmSemPriorityChg (VOID);
VOID HbRmStartElection (VOID);
VOID HbRmUpdateState (UINT1 );
VOID HbRmSem1WayRcvdInStandbyState(VOID);

#ifdef ICCH_WANTED
VOID  HbIcchRawPktRcvd (INT4 i4SockFd);

VOID HbIcchStateMachine (UINT1 u1Event);
VOID HbIcchStartElection (VOID);
VOID HbIcchSemEvntIgnore (VOID);
VOID HbIcchSem1WayRcvdInInitState (VOID);
VOID HbIcchSem2WayRcvdInInitState (VOID);
VOID HbIcchSemPeerDownRcvdInInitState (VOID);
VOID HbIcchSemPeerDownRcvdInMasterState (VOID);
VOID HbIcchSemPeerDownRcvdInSlaveState (VOID);
VOID HbIcchSem2WayRcvdInMasterState (VOID);
VOID HbIcchSem2WayRcvdInSlaveState (VOID);
VOID HbIcchSemMasterElctdInInitState (VOID);
VOID HbIcchUpdateState (UINT1);
UINT4 HbIcchNotifyPeerInfo (UINT4 u4PeerAddr, UINT1 u1Event);


INT4  HbRawIcchSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, 
                      UINT4 *pu4PeerAddr);
UINT4 HbRawIcchSockSend (UINT1 *pu1Data, UINT2 u2PktLen, 
                       UINT4 u4DestAddr);
VOID  HbRawIcchSockInit (VOID);
#endif

/* hbapi.c */
UINT4 HbNotifyPeerInfo (UINT4 u4PeerAddr, UINT1 u1Event);
VOID  HbNotifyProtocols (UINT1 u1Event);
VOID  HbHandleAppInitCompleted (VOID);

/*rmclr.c */
VOID  HbHandleShutAndStartCompleted (VOID);
VOID  HbIcchHandleRxBufProcessedEvt (VOID);      

/* hbicutl.c */
INT4
HbUtilAddEthHeader (UINT1 *pu1DataBuf, UINT4 u4PktSize);
UINT4
HbIcchNpPortWrite (tHbMsg * pHbMsg, UINT4 u4DestAddr);
VOID
HbIcchTerminateHBMsg (VOID);

/* hbport.c */
UINT1 HbLaGetMCLAGSystemStatus (VOID);
INT1 HbAstDisableStpOnPort (UINT4 u4IfIndex);
VOID HbPortGetIpPortFrmIndex (UINT4 u4IfIndex, UINT4 *pu4Port);
VOID HbVlanConfigStaticUcastEntry (tConfigStUcastInfo *pStUcastInfo,
                                   UINT1 u1Action,
                                   UINT1 *pu1Error);
VOID HbVlanHwDelFdbEntry (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId);
VOID HbArpModifyWithIndex (UINT4 u4IfIndex, UINT4 u4PeerAddr, INT1 i1State);
INT4 HbArpAddArpEntry PROTO ((tARP_DATA));
UINT4
HbLaDisableLldpOnIcclPorts (UINT4 u4AggIndex);
VOID
HbLaIcchInitProperties (UINT4 u4IcclIfIndex);
VOID
HbLaNotifyMclagShutdownComplete (VOID);
