####################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.2 2015/02/17 12:33:59 siva Exp $
#
# Description: Linux make.h for HB.
####################################################

MODULE_NAME = FutureHB

HB_BASE_DIR = $(BASE_DIR)/hb
HB_SRC_DIR = $(HB_BASE_DIR)/src
HB_INC_DIR = $(HB_BASE_DIR)/inc
HB_OBJ_DIR = $(HB_BASE_DIR)/obj


HB_INCLUDE_FILES = $(HB_INC_DIR)/hbdefn.h \
                   $(HB_INC_DIR)/hbsem.h  \
                   $(HB_INC_DIR)/hbtrc.h  \
                   $(HB_INC_DIR)/fshblw.h \
                   $(HB_INC_DIR)/fshbwr.h \
                   $(HB_INC_DIR)/hbprot.h \
                   $(HB_INC_DIR)/hbglob.h \
                   $(HB_INC_DIR)/hbincs.h

HB_INCLUDE_DIRS = -I$(HB_INC_DIR) \
                   $(COMMON_INCLUDE_DIRS)
                   

ifneq (${TARGET_OS},OS_VXWORKS)
ifeq (${L2RED},YES)
ifeq (${RM_LNXIP},YES)
HB_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                           -DIS_SLI_WRAPPER_MODULE
endif
endif
endif

HB_DEPENDENCIES = $(COMMON_DEPENDENCIES)  \
               $(HB_INCLUDE_FILES)     \
               $(HB_HB_INCLUDE_FILES)  \
               $(HB_BASE_DIR)/Makefile \
               $(HB_BASE_DIR)/make.h 
