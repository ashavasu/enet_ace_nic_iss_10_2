/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbapi.c,v 1.16.2.1 2018/03/16 13:43:24 siva Exp $
*
* Description: API related functions of Heart Beat module.
*********************************************************************/
#ifndef _HBAPI_C_
#define _HBAPI_C_

#include "hbincs.h"

/******************************************************************************
 * Function                   : HbApiSendEventToHb
 * Action                     : Exported API to send RM core module 
 *                              actions to be done to RM HB Module.
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS or HB_FAILURE  
 ******************************************************************************/
UINT4
HbApiSendRmEventToHb (UINT4 u4Event)
{
    HB_TRC (HB_CTRL_PATH_TRC, "HbApiSendEventToHb: ENTRY \r\n");

    if (RM_HB_MODE == ISS_RM_HB_MODE_EXTERNAL)
    {
        HB_TRC (HB_CTRL_PATH_TRC, "HbApiSendEventToHb: HB Mode External \r\n");
        return HB_SUCCESS;
    }
    /* Event handled by this Heart Beat API *
     * 1. HB_FSW_EVENT            -> Initiate the Force-switchover
     * 2. HB_SYNC_MSG_PROCESSED   -> The sync messages which were 
     *                               pending are processed. 
     * 3. HB_CHG_TO_TRANS_IN_PRGS -> Change the node state to 
     *                               transition in progress as node is 
     *                               triggered the force-switchover, during
     *                               election is not entertained.
     * 4. HB_APP_INIT_COMPLETED   -> RM enabled modules 
     *                               shutdown and start completed.
     */
    if (HB_SEND_EVENT (HB_TASK_ID, u4Event) == OSIX_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbApiSendEventToHb: Send Event "
                "Failed\n");
        return HB_FAILURE;
    }
    HB_TRC (HB_CTRL_PATH_TRC, "HbApiSendEventToHb: EXIT \r\n");
    return HB_SUCCESS;
}

/******************************************************************************
 * Function                   : HbApiSendEventToHb
 * Action                     : Exported API to send RM core module
 *                              actions to be done to RM HB Module.
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS or HB_FAILURE
 ******************************************************************************/
UINT4
HbApiSendIcchEventToHb (UINT4 u4Event)
{
    /* Event handled by this Heart Beat API 
     * 1. HB_ICCH_SYNC_MSG_PROCESSED -> The sync messages which were
     *                                  pending are processed.
     */
    if (HB_SEND_EVENT (HB_TASK_ID, u4Event) == OSIX_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbApiSendEventToHb: Send Event "
                "Failed \r\n");
        return HB_FAILURE;
    }
    return HB_SUCCESS;
}

#ifdef RM_WANTED
/******************************************************************************
 * Function                   : HbApiSetFlagAndTxHbMsg          
 * Action                     : This API sets the given flag flag in the
 *                          HB messages sent to the peer standby node.
 * Input(s)                     : u4Flag - The flag that has to be set in the Hb messages.
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS or HB_FAILURE 
 ******************************************************************************/
UINT4
HbApiSetFlagAndTxHbMsg (UINT4 u4Flag)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_EXTERNAL)
    {
        HB_TRC (HB_CTRL_PATH_TRC,
                "HbApiSetFlagAndTxHbMsg: HB Mode External \r\n");
        return HB_SUCCESS;
    }

    HB_LOCK ();

    HB_SET_FLAGS (u4Flag);

    if (HbFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "RmHbApiHandleStandbyReboot: "
                "Sending standby reboot to peer failed !!!\r\n");
        HB_UNLOCK ();
        return HB_FAILURE;
    }

    HB_UNLOCK ();

    return HB_SUCCESS;
}
#endif /* RM_WANTED */
/******************************************************************************
 * Function                   : HbApiSetKeepAliveFlag          
 * Action                     : This API sets the given flag and the flag is set
 *                              whenever the node receives any Message from Peer
 * Input(s)                   : u1RmKeepAliveFlag - The flag which indicates 
 *                              whether node receives rm messages from peer or 
 *                              not.
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None 
 ******************************************************************************/
VOID
HbApiSetKeepAliveFlag (UINT1 u1RmKeepAliveFlag)
{
    gu1KeepAliveFlag = u1RmKeepAliveFlag;
    return;
}

#ifdef ICCH_WANTED
/******************************************************************************
 * Function           : HbApiSetKeepAliveFlag
 * Input(s)           : u1IcchKeepAliveFlag - The flag which indicates whether node
 *                      receives ICCH messages from peer or not.
 * Output(s)          : None
 * Returns            : None
 * Action             : This API sets the given flag and the flag is set whenever
 *                      the node receives any Message from Peer.
 * ******************************************************************************/
VOID
HbApiSetIcchKeepAliveFlag (UINT1 u1IcchKeepAliveFlag)
{
    gu1IcchKeepAliveFlag = u1IcchKeepAliveFlag;
    return;

}
#endif

/******************************************************************************
 * Function                   : HbRegisterApps
 * Action                     : This API is used by external modules
 *                              to register with HB module.
 * Input(s)                   : AppId - Application Identifier.
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None.
 ******************************************************************************/

VOID
HbRegisterApps (UINT4 u4AppId)
{
    gHbInfo.HbRegInfo[u4AppId - 1].u4AppId = u4AppId;
    return;
}

/******************************************************************************
 * Function                   : HbSetHbInterval
 * Action                     : This API is used to set the heartbeat interval
 * Input(s)                   : u4HbInterval - Heart beat interval
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS or HB_FAILURE
 ******************************************************************************/
INT1
HbSetHbInterval (UINT4 u4HbInterval)
{
    tRegPeriodicTxInfo  RegnInfo;
    INT1                i1RetVal = HB_FAILURE;

    MEMSET (&RegnInfo, 0, sizeof (tRegPeriodicTxInfo));

    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        HB_LOCK ();
        if (HbGetHbInterval () != u4HbInterval)
        {
            if ((HbGetHbInterval ()) == HB_MIN_INTERVAL)
            {
#ifdef RM_WANTED
                CfaDeRegisterPeriodicPktTx (CFA_APP_RM);
#endif /* RM_WANTED */
            }

            HB_SET_HB_INTERVAL (u4HbInterval);
            if (u4HbInterval == HB_MIN_INTERVAL)
            {
#ifdef RM_WANTED
                RegnInfo.SendToApplication = HbTxTsk;
                CfaRegisterPeriodicPktTx (CFA_APP_RM, &RegnInfo);
#endif /* RM_WANTED */
            }

            /* Since peer dead interval is also
             * changed(inside HB_SET_HB_INTERVAL), restart the peer dead
             * interval timer */
#ifdef RM_WANTED
            if (HbTmrRestartTimer (HB_PEER_DEAD_TIMER,
                                   HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "!!! Peer Dead restart timer Failure !!! \r\n");
            }
#endif /*RM_WANTED */
#ifdef ICCH_WANTED
            if (HbLaGetMCLAGSystemStatus () == HB_MCLAG_ENABLED)
            {
                if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                                       HB_GET_PEER_DEAD_INTERVAL ()) ==
                    HB_FAILURE)
                {
                    HB_TRC (HB_ALL_FAILURE_TRC,
                            "!!! Peer Dead restart timer Failure !!! \r\n");
                }
            }
#endif /*ICCH_WANTED */

            /* Call Heart-Beat timer expiry routine, as it
             * takes care of restarting the timer and sending
             * the heart-beat message (to notify the peer about
             * the modified hb-interval) */
#ifdef RM_WANTED
            if ((HbGetHbInterval ()) != HB_MIN_INTERVAL)
            {
                HbTmrHdlHbTmrExp (NULL);
            }
#endif /* RM_WANTED */
#ifdef ICCH_WANTED
            if (HbLaGetMCLAGSystemStatus () == HB_MCLAG_ENABLED)
            {
                if ((HbGetHbInterval ()) != HB_MIN_INTERVAL)
                {
                    HbIcchTmrHdlHbTmrExp (NULL);
                }
            }
#endif /* ICCH_WANTED */
        }
        HB_UNLOCK ();

        i1RetVal = HB_SUCCESS;
    }
    else
    {
        UNUSED_PARAM (u4HbInterval);
        i1RetVal = HB_SUCCESS;
    }
    return i1RetVal;

}

/******************************************************************************
 * Function                   : HbSetHbPeerDeadInterval
 * Action                     : This API is used to set the HB peer dead 
 *                              interval.
 * Input(s)                   : u4HbPeerDeadInterval - peer dead interval of HB
 * Output(s)                  : HB_SUCCESS or HB_FAILURE
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : value of global variable
 ******************************************************************************/
INT1
HbSetHbPeerDeadInterval (UINT4 u4HbPeerDeadInterval)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        HB_LOCK ();

        if (HB_GET_PEER_DEAD_INTERVAL () != u4HbPeerDeadInterval)
        {
            HB_SET_PEER_DEAD_INTERVAL (u4HbPeerDeadInterval);
            /* Form and send heart-beat msg to notify the change in peer-dead
             * interval to the peer node */
#ifdef RM_WANTED
            if (HbFormAndSendMsg () == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "nmhSetFsRmPeerDeadInterval - RmFormAndSendHbMsg "
                        "Failure !!! \r\n");
            }

            if (HbTmrRestartTimer (HB_PEER_DEAD_TIMER,
                                   HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "!!! Peer Dead restart timer Failure !!! \r\n");
            }
#endif /* RM_WANTED */
            HB_TRC (HB_CRITICAL_TRC,
                    "\r\n\r\nWarning:peer-dead-interval(fsRmPeerDeadInterval)"
                    " is maintained only for backward compatability."
                    " peer-dead-interval-multiplier(fsRmPeerDeadIntMultiplier)"
                    " must be used for configuring peer dead interval.\r\n");
        }
        HB_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (u4HbPeerDeadInterval);
    }
    return HB_SUCCESS;
}

/******************************************************************************
 * Function                   : HbSetHbPeerDeadIntMultiplier
 * Action                     : This API sets the peer dead interval multiplier 
 *                              of system
 * Input(s)                   : u4HbPeerDeadIntMultiplier - Peer dead interval
 *                              multiplier value
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS or HB_FAILURE
 ******************************************************************************/
INT1
HbSetHbPeerDeadIntMultiplier (UINT4 u4HbPeerDeadIntMultiplier)
{
    if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
    {
        HB_LOCK ();

        if (HB_GET_PEER_DEAD_INT_MULTIPLIER () != u4HbPeerDeadIntMultiplier)
        {
            HB_SET_PEER_DEAD_INT_MULTIPLIER (u4HbPeerDeadIntMultiplier);
            /* Form and send heart-beat msg to notify the change in peer-dead
             * interval multiplier to the peer node */
#ifdef RM_WANTED
            if (HbFormAndSendMsg () == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "nmhSetFsRmPeerDeadIntMultiplier "
                        "- HbFormAndSendMsg Failure !!! \r\n");
            }

            if (HbTmrRestartTimer (HB_PEER_DEAD_TIMER,
                                   HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "!!! Peer Dead restart timer Failure !!! \r\n");
            }
#endif /* RM_WANTED */
#ifdef ICCH_WANTED
            if (HbLaGetMCLAGSystemStatus () == HB_MCLAG_DISABLED)
            {
                HB_UNLOCK ();
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "nmhSetFsRmPeerDeadIntMultiplier "
                        "- MCLAG is disabled !!! \r\n");
                return HB_SUCCESS;
            }
            if (HbIcchFormAndSendMsg () == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "nmhSetFsRmPeerDeadIntMultiplier "
                        "- HbIcchFormAndSendMsg Failure !!! \r\n");
            }

            if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                                   HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "!!! Peer Dead restart timer Failure !!! \r\n");
            }
#endif /* ICCH_WANTED */

        }
        HB_UNLOCK ();
    }
    else
    {
        UNUSED_PARAM (u4HbPeerDeadIntMultiplier);
    }
    return HB_SUCCESS;
}

/******************************************************************************
 * Function                   : HbGetHbInterval
 * Action                     : This API gets the hb interval of system
 * Input(s)                   : None
 * Output(s)                  : None 
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Returns the HB interval
 ******************************************************************************/
UINT4
HbGetHbInterval (VOID)
{
    return HB_GET_HB_INTERVAL ();
}

/******************************************************************************
 * Function                   : HbGetHbPeerDeadInterval
 * Action                     : This API gets the peer dead interval of system
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Returns the peer dead interval
 ******************************************************************************/
UINT4
HbGetHbPeerDeadInterval (VOID)
{
    return HB_GET_PEER_DEAD_INTERVAL ();
}

/******************************************************************************
 * Function                   : HbGetHbPeerDeadIntMultiplier
 * Action                     : This API gets the peer dead int multiplier of 
 *                              system
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Returns the peer dead interval multiplier
 ******************************************************************************/
UINT4
HbGetHbPeerDeadIntMultiplier (VOID)
{
    return HB_GET_PEER_DEAD_INT_MULTIPLIER ();
}

#ifdef RM_WANTED
/******************************************************************************
 * Function                   : HbGetActiveNodeId
 * Action                     : This API gets the active node id of system
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Returns the node ID of Active node
 ******************************************************************************/
UINT4
HbGetActiveNodeId (VOID)
{
    return HB_GET_ACTIVE_NODE_ID ();
}

/******************************************************************************
 * Function                   : HbGetPeerNodeId
 * Action                     : This API gets the self node mask of system
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Returns the peer node ID
 ******************************************************************************/
UINT4
HbGetPeerNodeId (VOID)
{
    return HB_GET_PEER_NODE_ID ();
}

/******************************************************************************
 * Function                   : HbGetNodeState
 * Action                     : This API gets the state of system
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global variables Modified  : None
 * Global Variables Referred  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Retuens the state of the Node
 ******************************************************************************/
UINT4
HbGetNodeState (VOID)
{
    return HB_GET_NODE_STATE ();
}

/******************************************************************************
 * Function                   : HbSetTrcLevel
 * Action                     : This API sets the trace level
 * Input(s)                   : u4TrcLevel - level of trace
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None
 ******************************************************************************/
VOID
HbSetTrcLevel (UINT4 u4TrcLevel)
{
    HB_SET_TRC_LEVEL (u4TrcLevel);
    return;
}
#endif /* RM_WANTED */

/******************************************************************************
 * Function                   : HbApiSendEvtFromMCLAG
 * Action                     : This API sets the trace level
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None
 ******************************************************************************/
VOID
HbApiSendEvtFromMCLAG (UINT4 u4Event)
{
    if (HB_SEND_EVENT (HB_TASK_ID, u4Event) != OSIX_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "SendEvent failed in "
                "HbApiSendEvtFromMCLAG \r\n");
    }
    return;
}

/*****************************************************************************
 * Function Name              : HbIcchPostHBPktToIcch
 * Description                : This function is invoked whenever ICCH HB packet
 *                              is received in the driver. This will take care 
 *                              of posting the packet in CRU buffer format to
 *                              ICCH
 * Input(s)                   : pBuf - pointer to the ICCH HB Data
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
INT4
HbIcchPostHBPktToIcch (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4RetVal = OSIX_FAILURE;

    /* post the buffer to ICCH queue */
    u4RetVal =
        HB_SEND_TO_QUEUE (HB_ICCH_QUEUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN);

    if (u4RetVal != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    /* trigger the interuppt event for ICCH */
    if (OsixEvtSend (HB_TASK_ID, HB_PKT_RCVD_ON_ICCL_PORT) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function           : HbApiIssuForceStandBy
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : HB_SUCCESS or HB_FAILURE
 * Action             : This API is used to post force standby event to HB queue
 ******************************************************************************/

UINT4
HbApiIssuForceStandBy (VOID)
{
    /* Post GO_STANDBY event to HB queue */
    if (HB_SEND_EVENT (HB_TASK_ID, HB_ISSU_FORCE_STANDBY) == OSIX_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "SendEvent failed in HbApiIssuForceStandBy\n");
        return HB_FAILURE;
    }

    return HB_SUCCESS;
}

#ifdef ICCH_WANTED
/******************************************************************************
 * Function                   : HbUpdateIcclFdbEntryForVlan
 * Action                     : This API creates or deletes FDB entry for the 
 *                              given VLAN with HB peer MAC and ICCL interface.
 * Input(s)                   : u2VlanId - VLAN ID
 *                              u1Action - Decision whether to create or delete
 *                                         FDB entry
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global variables Modified  : None
 * Global Variables Referred  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None
 ******************************************************************************/
VOID
HbUpdateIcclFdbEntryForVlan (UINT2 u2VlanId, UINT1 u1Action)
{
    tMacAddr            DestMacAddr;
    UINT4               u4IfIndex = 0;

    MEMSET (DestMacAddr, 0, sizeof (tMacAddr));

    /* Incase if peer MAC is zero, skip this for now */
    if (0 == MEMCMP (DestMacAddr, gHbInfo.au1DestMac, MAC_ADDR_LEN))
    {
        return;
    }

    MEMCPY (DestMacAddr, gHbInfo.au1DestMac, sizeof (tMacAddr));

    IcchGetIcclIfIndex (&u4IfIndex);
    if (u1Action == HB_FDB_ADD)
    {
        /* Add the static MAC-Address for ICCL interface */
        VlanHwAddFdbEntry (u4IfIndex, DestMacAddr, u2VlanId, VLAN_PERMANENT);
    }
    else
    {
        /* Delete the static MAC-Address for ICCL interface */
        VlanHwDelFdbEntry (u4IfIndex, DestMacAddr, u2VlanId);
    }
    return;
}
#endif /* ICCH_WANTED */
/******************************************************************************
 * Function           : HbApiIssuHandleTimer
 * Input(s)           : u1Flag - OSIX_TRUE/OSIX_FALSE to STOP/START timers
 * Output(s)          : None
 * Returns            : HB_FAILURE/HB_SUCCESS
 * Action             : This API is used to stop/start the peer_dead timer and 
 *                      Heart_beat timer at the time of issu Loadversion 
 *                      process to avoid HB periodic packet sending.  
 ******************************************************************************/
INT1
HbApiIssuHandleTimer (UINT1 u1Flag)
{
    HB_LOCK ();
    if (u1Flag == OSIX_TRUE)
    {
        if (HbTmrStartTimer (HB_PEER_DEAD_TIMER, HbGetHbPeerDeadInterval ()) ==
            HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "ISSU start HB_PEER_DEAD_TIMER failed in HbApiIssuStopTimer\n");
            HB_UNLOCK ();
            return HB_FAILURE;
        }
        if (HbTmrStartTimer (HB_HEARTBEAT_TIMER, HbGetHbInterval ()) ==
            HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "ISSU start HB_HEARTBEAT_TIMER failed in HbApiIssuStopTimer\n");
            HB_UNLOCK ();
            return HB_FAILURE;
        }
    }
    else
    {
        if (HbTmrStopTimer (HB_PEER_DEAD_TIMER) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "ISSU stop HB_PEER_DEAD_TIMER failed in HbApiIssuStopTimer\n");
            HB_UNLOCK ();
            return HB_FAILURE;
        }
        if (HbTmrStopTimer (HB_HEARTBEAT_TIMER) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "ISSU stop HB_HEARTBEAT_TIMER failed in HbApiIssuStopTimer\n");
            HB_UNLOCK ();
            return HB_FAILURE;
        }
    }
    HB_UNLOCK ();

    return HB_SUCCESS;
}

/*****************************************************************************
 * Function Name              : HbApiGetPortMaskValue
 * Description                : This function returns port mask value
 *                              of the node.Mask values of both MC-LAG nodes 
 *                              will differ to achieve unique logical port number.
 *                              This unique number will be stored in 
 *                              gu2PortMaskValue.This function will be called 
 *                              in LA while sending MCLAG ports LACPDU to mask 
 *                              the port number  
 * Input(s)                   : None
 * Output(s)                  : None
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : gu2PortMaskValue
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : gu2PortMaskValue - Mask value 
 *****************************************************************************/
UINT2
HbApiGetPortMaskValue ()
{
    return gu2PortMaskValue;
}

#endif /* _HBAPI_C_ */
