/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbicsem.c,v 1.13 2016/06/24 09:44:04 siva Exp $
*
* Description: Heart Beat State event machine for ICCH.
*********************************************************************/
#ifndef _HBICSEM_C_
#define _HBICSEM_C_
#include "hbincs.h"
#include "hbicsem.h"


/****************************************************************************
 * Function           	      : HbIcchStateMachine
 * Action             	      : This procedure indexes into the state event 
 *                              table using the event and the state to get an 
 *                              integer. This integer when indexed into an 
 *                              array of function pointers gives the action 
 *                              routine to be called. 
 * Input(s)           	      : u1Event - Event 
 * Output(s)          	      : None
 * <OPTIONAL Fields>          : None                                      
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns        	      : None
 ***************************************************************************/
VOID
HbIcchStateMachine (UINT1 u1Event)
{
    UINT1               u1ActionProcIdx = 0;
    UINT1               u1CurState = 0;

    u1CurState = (UINT1) HB_ICCH_GET_NODE_STATE ();

    if (u1CurState < HB_ICCH_MAX_STATES)
    {
        HB_TRC2 (HB_EVENT_TRC, "FSM Event: %s  State: %s\r\n",
                    au1HbIcchEvntStr[u1Event], au1HbIcchStateStr[u1CurState]);
    }
    /* Get the index of the action procedure */
    u1ActionProcIdx = au1HbIcchSem[u1Event][HB_ICCH_GET_NODE_STATE ()];

    /* call the corresponding function pointer */
    (*HbIcchActionProc[u1ActionProcIdx]) ();
    return;
}

/****************************************************************************
 * Function           	     : HbIcchSemEvntIgnore
 * Action             	     : This procedure is invoked when an invalid event 
 * 	                       occurs from ICCH
 * Input(s)           	     : None.
 * Output(s)          	     : None.
 * <OPTIONAL Fields>         : None
 * Global Variables Referred : None
 * Global variables Modified : None
 * Exceptions or Operating 
 * System Error Handling     : None
 * Use of Recursion          : None
 * Returns    	             : None.
 ****************************************************************************/
VOID
HbIcchSemEvntIgnore (VOID)
{
    UINT1               u1CurState = 0;

    u1CurState = (UINT1) HB_ICCH_GET_NODE_STATE ();
    if (u1CurState < HB_ICCH_MAX_STATES)
    {
        HB_TRC1 (HB_EVENT_TRC, "Unexpected event to ICCH SEM. State:%s\r\n",
                    au1HbIcchStateStr[u1CurState]);
    }
    return;
}

/******************************************************************************
 * Function           	      : HbIcchSem1WayRcvdInMasterState
 * Action             	      : This procedure is invoked when the peer node has
 *				not learnt about this node, but this node has
 *				learnt about peer (i.e. HeartBeat is one-way 
 *				rcvd).
 * Input(s)           	      : None
 * Output(s)          	      : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns            	      : None
 ******************************************************************************/
VOID
HbIcchSem1WayRcvdInMasterState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSem1WayRcvdInMasterState\r\n");
    HbIcchStartElection ();
    return;
}
/******************************************************************************
 * Function                   : HbIcchSem1WayRcvdInSlaveState
 * Action                     : This procedure is invoked when the peer node has
 *              not learnt about this node, but this node has
 *              learnt about peer (i.e. HeartBeat is one-way
 *              rcvd).
 * Input(s)                   : None
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None
 ******************************************************************************/
VOID
HbIcchSem1WayRcvdInSlaveState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSem1WayRcvdInSlaveState\r\n");
    HbIcchStartElection ();
    return;
}

/******************************************************************************
 * Function                   : HbIcchSem2WayRcvdInSlaveState
 * Action                     : This procedure is invoked when the peer node has
 *              learnt about this node, and this node has also
 *              learnt about peer (i.e. HeartBeat is one-way
 *              rcvd).
 * Input(s)                   : None
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : None
 ******************************************************************************/
VOID
HbIcchSem2WayRcvdInSlaveState (VOID)
{

    UINT1               u1SelfNodeState = 0;
    UINT1               u1PeerNodeState = 0;

    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSem2WayRcvdInSlaveState\r\n");

    /* Get the remote MC-LAG node state*/
    if ((HB_ICCH_GET_FLAGS ()) & HB_ICCH_INIT_STATE)
    {
        u1PeerNodeState = ICCH_INIT;
    }
    else if ((HB_ICCH_GET_FLAGS ()) & HB_ICCH_MASTER_STATE)
    {
        u1PeerNodeState = ICCH_MASTER;
    }
    else if ((HB_ICCH_GET_FLAGS ()) & HB_ICCH_SLAVE_STATE)
    {
        u1PeerNodeState = ICCH_SLAVE;
    }

    u1SelfNodeState = (UINT1) (HB_ICCH_GET_NODE_STATE ());

    if ((u1SelfNodeState == ICCH_SLAVE) &&
        (u1PeerNodeState == ICCH_SLAVE))
    {
        HbIcchStartElection ();
    }
    return;
}


/******************************************************************************
 * Function           	       : HbIcchSem1WayRcvdInInitState
 * Action             	       : This procedure is invoked when the peer node 
 * 				 has not learnt about this node, but this node 
 * 				 has learnt about peer (i.e. HeartBeat is 
 * 				 one-way rcvd).
 * Input(s)           	       : None
 * Output(s)          	       : None.
 * <OPTIONAL Fields>           : None
 * Global Variables Referred   : None
 * Global variables Modified   : None
 * Exceptions or Operating 
 * System Error Handling       : None
 * Use of Recursion            : None
 * Returns            	       : None.
 ******************************************************************************/
VOID
HbIcchSem1WayRcvdInInitState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSem1WayRcvdInInitState\r\n");
    return;
}

/******************************************************************************
 * Function           	       : HbIcchSem2WayRcvdInInitState
 * Action             	       : This procedure is invoked when the peer node 
 * 				 has learnt about this node and this node has 
 * 				 also learnt about peer node (i.e., HeartBeat 
 * 				 msg is 2-way rcvd).
 * Input(s)           	       : None.
 * Output(s)          	       : None.
 * <OPTIONAL Fields>           : None
 * Global Variables Referred   : None
 * Global variables Modified   : None
 * Exceptions or Operating 
 * System Error Handling       : None
 * Use of Recursion            : None
 * Returns           	       : None.
 ******************************************************************************/
VOID
HbIcchSem2WayRcvdInInitState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSem2WayRcvdInInitState\r\n");
    HbIcchStartElection ();
    return;
}
/******************************************************************************
 * Function           	       : HbIcchStartElection 
 * Action             	       : This procedure performs the election to elect 
 * 				 MASTER and SLAVE node.
 * Input(s)           	       : None.
 * Output(s)          	       : None.
 * <OPTIONAL Fields>           : None
 * Global Variables Referred   : None
 * Global variables Modified   : None
 * Exceptions or Operating 
 * System Error Handling       : None
 * Use of Recursion            : None
 * Returns            	       : None.
 ******************************************************************************/
VOID
HbIcchStartElection (VOID)
{
    tUtlInAddr          HbNodeId;
    UINT1               u1NodeState = HB_ICCH_MAX_STATES;
    UINT1               u1SelfNodeState = 0;
    UINT1               u1PeerNodeState = 0;
 
    MEMSET (&HbNodeId, 0, sizeof (tUtlInAddr));

    HB_TRC (HB_INIT_SHUT_TRC, "HbIcchStartElection: ENTRY\r\n");
    
    /* Get the remote MC-LAG node state*/    
    if ((HB_ICCH_GET_FLAGS ()) & HB_ICCH_INIT_STATE)
    {
        u1PeerNodeState = ICCH_INIT;
    }
    else if ((HB_ICCH_GET_FLAGS ()) & HB_ICCH_MASTER_STATE)
    {
        u1PeerNodeState = ICCH_MASTER;
    }
    else if ((HB_ICCH_GET_FLAGS ()) & HB_ICCH_SLAVE_STATE)
    {
        u1PeerNodeState = ICCH_SLAVE;
    }
    
    u1SelfNodeState = (UINT1) (HB_ICCH_GET_NODE_STATE ());

    /*-------------------------------------------------------------------*/
    /* Self State  | Peer State  | Action                                */
    /*-------------------------------------------------------------------*/
    /* SLAVE       | MASTER      |                                       */
    /*----------------------------                                       */
    /* MASTER      | SLAVE       |  Do nothing. Notify the peer node     */
    /*----------------------------  state to ICCH                        */
    /* MASTER      | INIT        |                                       */
    /*-------------------------------------------------------------------*/
    /* SLAVE       | SLAVE       |                                       */
    /*----------------------------                                       */
    /* MASTER      | MASTER      |                                       */
    /*----------------------------  Start election based on the node ID. */
    /* INIT        | SLAVE       |  Lower IP node is Master and higher   */
    /*----------------------------  IP node is Slave.                    */
    /* SLAVE       | INIT        |                                       */
    /*----------------------------                                       */
    /* INIT        | INIT        |                                       */
    /*-------------------------------------------------------------------*/
    /* INIT        | MASTER      |  Make the node state as Slave         */
    /*-------------------------------------------------------------------*/
    

    if (((u1PeerNodeState == ICCH_MASTER) && (u1SelfNodeState == ICCH_SLAVE)) ||
        ((u1PeerNodeState == ICCH_SLAVE) && (u1SelfNodeState == ICCH_MASTER)) ||
        ((u1PeerNodeState == ICCH_INIT) && (u1SelfNodeState == ICCH_MASTER)))
    {
        HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_UP);
        return;
    }
    else if (((u1PeerNodeState == ICCH_SLAVE) && 
			(u1SelfNodeState == ICCH_SLAVE)) ||
             ((u1PeerNodeState == ICCH_MASTER) && 
			(u1SelfNodeState == ICCH_MASTER)) ||
             ((u1PeerNodeState == ICCH_SLAVE) && 
			(u1SelfNodeState == ICCH_INIT)) ||
             ((u1PeerNodeState == ICCH_INIT) && 
			(u1SelfNodeState == ICCH_SLAVE)) ||
             ((u1PeerNodeState == ICCH_INIT) && 
			(u1SelfNodeState == ICCH_INIT)))
    {

        /* Elect the node with lower IP address as the MASTER node */
        if (HB_ICCH_GET_SELF_NODE_ID () < HB_ICCH_GET_PEER_NODE_ID ())
        {
            HB_ICCH_SET_MASTER_NODE_ID (HB_ICCH_GET_SELF_NODE_ID ());
            u1NodeState = HB_ICCH_MASTER;
            HbNodeId.u4Addr = OSIX_NTOHL (HB_ICCH_GET_MASTER_NODE_ID ());
            HB_TRC1 (HB_NOTIFICATION_TRC, "MASTER NODE is elected: %s\r\n",
                     INET_NTOA (HbNodeId));
        }
        else if (HB_ICCH_GET_SELF_NODE_ID () > HB_ICCH_GET_PEER_NODE_ID ())
        {
            HB_ICCH_SET_MASTER_NODE_ID (HB_ICCH_GET_PEER_NODE_ID ());
            u1NodeState = HB_ICCH_SLAVE;
        }
    }
    else if ((u1PeerNodeState == ICCH_MASTER) && (u1SelfNodeState == ICCH_INIT))
    {
        HB_ICCH_SET_MASTER_NODE_ID (HB_ICCH_GET_PEER_NODE_ID ());
        u1NodeState = HB_ICCH_SLAVE;
    }
    if (HB_ICCH_GET_MASTER_NODE_ID () == 0)
    {
        HB_TRC (HB_NOTIFICATION_TRC, "HbIcchStartElection: "
                                     "Master node id is zero...!!! \r\n");
        return;
    }
    if (u1NodeState == HB_ICCH_SLAVE)
    {
        HbIcchUpdateState (u1NodeState);
        HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_UP);
    }
    else                        /* NODE STATE is MASTER */
    {
        HbIcchUpdateState (u1NodeState);
        HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_UP); 
        LaApiMCLAGMasterInit ();
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbIcchStartElection: EXIT\r\n");
    return;
}

/******************************************************************************
 * Function           	       : HbIcchUpdateState
 * Action             	       : This function updates the current state 
 *                               of the node 
 * Input(s)           	       : u1State - State of the MC-LAG Node.
 * Output(s)          	       : None.
 * <OPTIONAL Fields>           : None
 * Global Variables Referred   : None
 * Global variables Modified   : None
 * Exceptions or Operating 
 * System Error Handling       : None 
 * Use of Recursion            : None
 * Returns  	     	       : None.
 ******************************************************************************/
VOID
HbIcchUpdateState (UINT1 u1State)
{
    tIcchHbMsg          IcchHbMsg;
    UINT1               u1PrevState = 0;
    UINT1               u1Event = HB_ICCH_INIT;
    UINT1               u1CurState = 0;

    MEMSET(&IcchHbMsg, 0, sizeof(tIcchHbMsg));

    if ((IcchApiGetPeerAddress () != 0) &&
        (u1State == HB_ICCH_GET_NODE_STATE ()))
    {
        /* no change in node state */
        return;
    }

    HB_TRC1 (HB_EVENT_TRC, " HbIcchUpdateState = %d\r\n", u1State);

    u1PrevState = (UINT1) HB_ICCH_GET_NODE_STATE ();
    HB_ICCH_SET_PREV_NODE_STATE (u1PrevState);
    HB_ICCH_SET_NODE_STATE (u1State);

    if (HB_ICCH_GET_PREV_NOTIF_NODE_STATE () == HB_ICCH_GET_NODE_STATE ())
    {
        /* No need to notify the same status again to ICCH core module */
        return;
    }

    HB_ICCH_SET_PREV_NOTIF_NODE_STATE (HB_ICCH_GET_NODE_STATE ());
    IcchHbMsg.u4Evt = u1Event;

    if (HB_ICCH_GET_NODE_STATE () == HB_ICCH_MASTER)
    {
        u1Event = GO_MASTER;
    }
    else if (HB_ICCH_GET_NODE_STATE () == HB_ICCH_SLAVE)
    {
        u1Event = GO_SLAVE;
    }
    else 
    {
        u1Event = GO_INIT;
    }

    if ((u1Event == GO_MASTER) || (u1Event == GO_SLAVE) || (u1Event == GO_INIT))
    {
        HB_ICCH_SET_PREV_NOTIF_NODE_STATE (HB_ICCH_GET_NODE_STATE ());
        IcchHbMsg.u4Evt = u1Event;
        IcchApiSendHbEvtToIcch (&IcchHbMsg);
    }

    u1CurState = (UINT1) HB_ICCH_GET_NODE_STATE ();
    if (u1CurState < HB_ICCH_MAX_STATES)
    {
        HB_TRC1 (HB_NOTIFICATION_TRC, "FSM State Changed to: %s\r\n",
                au1HbIcchStateStr[u1CurState]);
    }
    return;
}

/*****************************************************************************
 * Function           	      : HbIcchSemMasterElctdInInitState
 * Action             	      : Master node is already elected and this node has
 * 				to update its MASTER node list and change its 
 * 				state to SLAVE.
 * Input(s)           	      : None.
 * Output(s)          	      : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None 
 * Returns           	      : None.
 *****************************************************************************/
VOID
HbIcchSemMasterElctdInInitState (VOID)
{
    UINT1               u1State = HB_ICCH_MAX_STATES;

    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSemMasterElctdInInitState\r\n");

    /* Master node is already elected and this is the second node
     * to come up. Do not start election again. MASTER NodeId is already
     * updated in global data struct and now set the state accordingly. */
    if (HB_ICCH_GET_MASTER_NODE_ID () == HB_ICCH_GET_SELF_NODE_ID ())
    {
        u1State = HB_ICCH_MASTER;
    }
    else if (HB_ICCH_GET_MASTER_NODE_ID () == HB_ICCH_GET_PEER_NODE_ID ())
    {
        u1State = HB_ICCH_SLAVE;
    }

    if ((u1State == HB_ICCH_MASTER) || (u1State == HB_ICCH_SLAVE))
    {
        HbIcchUpdateState (u1State);
    }
    return;
}



/*****************************************************************************
 * Function           	      : HbIcchSemPeerDownRcvdInInitState
 * Action             	      : This function is invoked when Peer dead timer 
 * 				expires i.e., Heart Beat msg is not received 
 * 				for a duration of peer dead interval.
 * Input(s)           	      : None.
 * Output(s)          	      : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns	              : None.
 *****************************************************************************/
VOID
HbIcchSemPeerDownRcvdInInitState (VOID)
{
    HbIcchUpdateState (HB_ICCH_MASTER);
    HB_TRC (HB_NOTIFICATION_TRC, "HbIcchSemPeerDownRcvdInInitState: " 
                                 "MASTER notified\r\n");
    return;
}

/*****************************************************************************
 * Function           	      : HbIcchSemPeerDownRcvdInMasterState
 * Action             	      : This function is invoked when MASTER node
 *                     	        Peer dead timer expires i.e., Heart Beat msg 
 *                     	        is not received for a duration of peer dead 
 *                     	        interval.
 * Input(s)           	      : None.
 * Output(s)          	      : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns            	      : None.
 *****************************************************************************/
VOID
HbIcchSemPeerDownRcvdInMasterState (VOID)
{
    tMacAddr            DestMacAddr;
    UINT4		u4IfIndex = 0;
    UINT4               u4PeerAddr = 0;
    tVlanId		VlanId = 0;
    /* Delete the Static ARP and FDB added for ICCL Peer */
    MEMSET (DestMacAddr, 0, sizeof (tMacAddr));

    IcchGetIcclL3IfIndex (&u4IfIndex);
    MEMCPY (&u4PeerAddr, &(gHbInfo.u4IcchPeerNode), sizeof(u4PeerAddr));
    HbArpModifyWithIndex (u4IfIndex, u4PeerAddr, HB_ARP_INVALID);
    u4IfIndex = 0;

    MEMCPY(DestMacAddr, gHbInfo.au1DestMac, sizeof(tMacAddr));
    VlanId = (tVlanId) (IcchApiGetIcclVlanId (ICCH_DEFAULT_INST));
    IcchGetIcclIfIndex (&u4IfIndex);

    /* Delete the static MAC Programmed for ICCL interface */
    HbVlanHwDelFdbEntry (u4IfIndex, DestMacAddr, VlanId);
    /* Delete the peer redirect filter */
    HbVlanHwDelFdbEntry (u4IfIndex, DestMacAddr, VLAN_WILD_CARD_ID);

    HB_TRC (HB_NOTIFICATION_TRC, "HbIcchSemPeerDownRcvdInMasterState: " 
                                 "SLAVE is down\r\n");
    if (HB_ICCH_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_DOWN);
    }

    MEMSET (gHbInfo.au1DestMac,0,sizeof(tMacAddr));
    /* Set the peer node id to zero */
    HB_ICCH_SET_PEER_NODE_ID (0);
    return;
}
/*****************************************************************************
 * Function           	      : HbIcchSemPeerDownRcvdInSlaveState
 * Action             	      : This function is invoked when Peer dead timer 
 * 				expires i.e., Heart Beat msg is not rcvd for a 
 * 				duration of peer dead interval.
 * Input(s)           	      : None.
 * Output(s)          	      : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns	              : None.
 *****************************************************************************/
VOID
HbIcchSemPeerDownRcvdInSlaveState (VOID)
{
    tMacAddr            DestMacAddr;
    UINT4		u4IfIndex = 0;
    UINT4               u4PeerAddr = 0;
    tVlanId		VlanId = 0;
    /* Delete the Static ARP and FDB added for ICCL Peer */
    MEMSET (DestMacAddr, 0, sizeof (tMacAddr));

    IcchGetIcclL3IfIndex (&u4IfIndex);
    MEMCPY (&u4PeerAddr, &(gHbInfo.u4IcchPeerNode),sizeof(u4PeerAddr));
    HbArpModifyWithIndex (u4IfIndex, u4PeerAddr, HB_ARP_INVALID);
    u4IfIndex = 0;

    MEMCPY(DestMacAddr, gHbInfo.au1DestMac, sizeof(tMacAddr));
    VlanId = (tVlanId) (IcchApiGetIcclVlanId (ICCH_DEFAULT_INST));
    IcchGetIcclIfIndex (&u4IfIndex);

    /* Delete the static MAC Programmed for ICCL interface */
    HbVlanHwDelFdbEntry (u4IfIndex, DestMacAddr, VlanId);
    /* Delete the peer redirect filter */
    HbVlanHwDelFdbEntry (u4IfIndex, DestMacAddr, VLAN_WILD_CARD_ID);

    if (HB_ICCH_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_DOWN);
    }

    MEMSET (gHbInfo.au1DestMac,0,sizeof(tMacAddr));
    /* This peer node id set to zero should be present before
     * returning in current node = transition in progress and
     * previous state is slave OR INIT check, otherwise after completing
     * pending operation at ICCH core module, peer down
     * will not be detected and GO_MASTER will not be sent to
     * ICCH core module*/
    HB_ICCH_SET_PEER_NODE_ID (0);
    HB_ICCH_SET_PREV_NODE_STATE (HB_ICCH_SLAVE);
    HB_ICCH_SET_MASTER_NODE_ID (HB_ICCH_GET_SELF_NODE_ID ());
        /* 1. In Failover case, this event is received after processing the 
         * Rx buffer messages to give GO_MASTER to ICCH core module.
         */
    HbIcchUpdateState (HB_ICCH_MASTER);


    HB_TRC (HB_NOTIFICATION_TRC, "HbIcchSemPeerDownRcvdInSlaveState: " 
                     " Master node dead, "
            	     "Pending RX buffer processing event triggered\r\n");
    return;
}

/*****************************************************************************
 * Function           	      : HbIcchSem2WayRcvdInMasterState
 * Action             	      : This procedure is invoked when a heart-beat msg 
 * 				is rcvd in 2-way rcvd state.
 * Input(s)           	      : None.
 * Output(s)          	      : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns 	              : None.
 *****************************************************************************/
VOID
HbIcchSem2WayRcvdInMasterState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbIcchSem2WayRcvdInMasterState\r\n");
    HbIcchStartElection ();
    return;
}
#endif
