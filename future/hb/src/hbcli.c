/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: hbcli.c,v 1.11 2016/06/24 09:44:04 siva Exp $
 *
 * Description: Action routines of HB module specific CLI commands
 *********************************************************************/
#ifndef __HBCLI_C__
#define __HBCLI_C__

#include "hbincs.h"
#include "hbcli.h"
#include "fshbcli.h"
/******************************************************************************
 * Function           : CliProcessHbCmd
 * Input(s)           : CliHandle - CLI handler
 *                      u4Command - Command identifier
 * Output(s)          : None.
 * Returns            : CLI_FAILURE or CLI_SUCCESS 
 * Action             : This function takes variable no. of arguments and
 *                      process the cli commands for HB module.
 ******************************************************************************/
INT4
CliProcessHbCmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *au1Args[HB_CLI_MAX_ARGS];
    INT4                i4Argno = 0;
    UINT4               u4rc = 0;
    UINT4               u4ErrCode = 0;
    va_start (ap, u4Command);

    while (1)
    {
        au1Args[i4Argno++] = va_arg (ap, UINT4 *);
        if (i4Argno == HB_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);
    switch (u4Command)
    {
        case HB_CLI_HB_INT:
            u4rc = HbCliSetHbInterval (CliHandle, (UINT4) *au1Args[0]);
            break;

        case HB_CLI_PDI_MULTIPLIER:
            u4rc = HbCliSetHbPeerDeadIntMultiplier (CliHandle, 
                   (UINT4) *au1Args[0]);
            break;
        case HB_CLI_SHOW_HB_INFO:
            u4rc = HbCliShowHbInfo (CliHandle); 
            break;
        case HB_CLI_TRC_DBG:
            u4rc = HbCliHbDebugEnableDisable (CliHandle,
                   (UINT4) CLI_PTR_TO_U4(au1Args[0]), (UINT1) u4Command); 
            break;
        case HB_CLI_TRC_NO_DBG:
            u4rc = HbCliHbDebugEnableDisable (CliHandle,
                   (UINT4) CLI_PTR_TO_U4(au1Args[0]), (UINT1) u4Command); 
            break;
        case HB_CLI_SHOW_DBG_INFO:
            u4rc = HbCliHbShowDebugInfo (CliHandle);
            break;
        case HB_CLI_HB_STATS:
            u4rc = HbCliStatsEnableDisable(CliHandle, 
                    (INT4) CLI_PTR_TO_U4(au1Args[0]));
            break;
        case HB_CLI_SHOW_HB_COUNTERS:
            u4rc = HbCliShowStatsInfo (CliHandle);
            break;
        case HB_CLI_CLEAR_STATS:
            u4rc = HbCliHbClearStatistics (CliHandle);
            break;
        default:
            break;

    }
    if ((u4rc == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_HB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", HbCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (u4rc);

    return ((INT4) u4rc); 
}

/******************************************************************************
 * Function           : HbCliSetHbInterval
 * Input(s)           : CliHandle - CLI handle
 *                      u4HbInt - Heart-beat interval to be set
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS or CLI_FAILURE
 * Action             : This function sets the heart-beat interval.
 ******************************************************************************/
UINT4
HbCliSetHbInterval (tCliHandle CliHandle, UINT4 u4HbInt)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsHbInterval (&u4ErrCode, u4HbInt) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsHbInterval (u4HbInt) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbCliSetHbPeerDeadIntMultiplier
 * Input(s)           : CliHandle - CLI handle
 *                      u4PeerDeadIntMultiplier - Peer-Dead interval
 *                                                multiplier to be set
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS or CLI_FAILURE
 * Action             : This function sets the peer-dead interval multiplier.
 ******************************************************************************/
UINT4
HbCliSetHbPeerDeadIntMultiplier (tCliHandle CliHandle,
                               UINT4 u4PeerDeadIntMultiplier)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsHbPeerDeadIntMultiplier (&u4ErrCode, u4PeerDeadIntMultiplier)
            != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsHbPeerDeadIntMultiplier (u4PeerDeadIntMultiplier)
            != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }


    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbCliShowHbInfo
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to show the HB informations
 *          
 ******************************************************************************/
UINT4
HbCliShowHbInfo (tCliHandle CliHandle)
{
    UINT4 u4HbInterval = 0;
    UINT4 u4HbPeerDeadIntMultiplier = 0;
    if (nmhGetFsHbInterval(&u4HbInterval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhGetFsHbPeerDeadIntMultiplier(&u4HbPeerDeadIntMultiplier) 
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Hb Interval               : %u msecs\r\n",
               u4HbInterval);
    CliPrintf (CliHandle, "Hb Interval Multiplier    : %u\r\n",
               u4HbPeerDeadIntMultiplier);

return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbCliShowStatsInfo
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to show the HB Statistics informations
 *
*******************************************************************************/


UINT4
HbCliShowStatsInfo (tCliHandle CliHandle)
{
   UINT4 u4HbStatsMsgCount = 0;
   INT4 i4HbStatsEnable = 0;

   if (nmhGetFsHbStatsEnable(&i4HbStatsEnable) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }
   if (i4HbStatsEnable == HB_CLI_COUNTER_DISABLE)
   {
       CliPrintf (CliHandle, "HB statistics collection is disabled\r\n");
   }
   if (nmhGetFsHbStatsMsgTxCount(&u4HbStatsMsgCount) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }
   CliPrintf (CliHandle, "Hb Statistics Message Tx Count                        : %u\r\n", 
           u4HbStatsMsgCount);
   if (nmhGetFsHbStatsMsgTxFailedCount(&u4HbStatsMsgCount) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }
   CliPrintf (CliHandle, "Hb Statistics Message Tx Failed Count                 : %u\r\n",
           u4HbStatsMsgCount);
   if (nmhGetFsHbStatsMsgRxCount(&u4HbStatsMsgCount) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }
   CliPrintf (CliHandle, "Hb Statistics Message Rx Count                        : %u\r\n",
           u4HbStatsMsgCount); 
   if (nmhGetFsHbStatsRxFailedCount(&u4HbStatsMsgCount) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }
   CliPrintf (CliHandle, "Hb Statistics Message Rx Failed Count                 : %u\r\n", 
           u4HbStatsMsgCount);
   if (nmhGetFsHbStatsMsgRxProcCount(&u4HbStatsMsgCount) != SNMP_SUCCESS)
   {
       return CLI_FAILURE;
   }
   CliPrintf (CliHandle, "Hb Statistics Message Rx Proc Count                   : %u\r\n", 
           u4HbStatsMsgCount);
   return CLI_SUCCESS;

}

 
/******************************************************************************
 * Function           : HbCliHbDebugEnableDisable
 * Input(s)           : CliHandle - CLI handler
 *                      u4TrcVal - Trace Value 
 *                      u1DbgFlag - Enable/Disable HB Debug
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to set the HB debug level
 *
 ******************************************************************************/
UINT4
HbCliHbDebugEnableDisable (tCliHandle CliHandle, 
                           UINT4 u4TrcVal, 
                           UINT1 u1DbgFlag)
{
    UINT4 u4ErrorCode = 0;
    UINT4 u4TraceValue = 0;

    if (u1DbgFlag == HB_CLI_TRC_NO_DBG)
    {
        u4TraceValue = HB_GET_TRC_LEVEL() & (~u4TrcVal);
    }
    else if (u1DbgFlag == HB_CLI_TRC_DBG)
    {
        u4TraceValue = HB_GET_TRC_LEVEL() | u4TrcVal;
    }

    if (nmhTestv2FsHbTrcLevel(&u4ErrorCode , u4TraceValue) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsHbTrcLevel(u4TraceValue) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbCliHbShowDebugInfo
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function shows the HB debug Info
 *
 ******************************************************************************/
UINT4
HbCliHbShowDebugInfo (tCliHandle CliHandle)
{
    UINT4   u4HbTrcLevel = 0;

    nmhGetFsHbTrcLevel(&u4HbTrcLevel);
    
    if ( 0 == u4HbTrcLevel)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\nHB: \r\n");

    if ((u4HbTrcLevel & HB_ALL_TRC) == HB_ALL_TRC)
    {
        CliPrintf (CliHandle, "  All HB Traces are on\r\n");
    }
    else
    {
        if (u4HbTrcLevel & HB_INIT_SHUT_TRC)
        {
            CliPrintf (CliHandle, "  HB Init-Shut Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_MGMT_TRC)
        {
            CliPrintf (CliHandle, "  HB Management Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_CTRL_PATH_TRC)
        {
            CliPrintf (CliHandle, "  HB Control path Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_CRITICAL_TRC)
        {
            CliPrintf (CliHandle, "  HB Critical Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_PEER_DISC_SM_TRC)
        {
            CliPrintf (CliHandle, "  HB Peer Discovery state machine Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_SOCKET_API_TRC)
        {
            CliPrintf (CliHandle, "  HB Socket APIs Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_NOTIFICATION_TRC)
        {
            CliPrintf (CliHandle, "  HB Notification Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_ALL_FAILURE_TRC)
        {
            CliPrintf (CliHandle, "  HB All failure Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_BUFFER_TRC)
        {
            CliPrintf (CliHandle, "  HB Buffer Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_EVENT_TRC)
        {
            CliPrintf (CliHandle, "  HB Event Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_PKT_DUMP_TRC)
        {
            CliPrintf (CliHandle, "  HB Packet dump Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_SNMP_TRC)
        {
            CliPrintf (CliHandle, "  HB SNMP Trace is on\r\n");
        }
        if (u4HbTrcLevel & HB_SWITCHOVER_TRC)
        {
            CliPrintf (CliHandle, "  HB Switchover Trace is on\r\n");
        }
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbCliStatsEnableDisable
 * Input(s)           : CliHandle - CLI handler
                        i4HbStatsEnable
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function is to sets the statistics status
*******************************************************************************/
UINT4
HbCliStatsEnableDisable (tCliHandle CliHandle, INT4 i4HbStatsEnable)
{

    UINT4 u4ErrorCode = 0;

    if (nmhTestv2FsHbStatsEnable(&u4ErrorCode , i4HbStatsEnable) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    
    if (nmhSetFsHbStatsEnable (i4HbStatsEnable) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbCliHbClearStatistics
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS/CLI_FAILURE
 * Action             : This function clears the statistics
 *
 ******************************************************************************/
UINT4
HbCliHbClearStatistics (tCliHandle CliHandle)
{
    UINT4 u4ErrorCode = 0;

    if (nmhTestv2FsHbClearStats (&u4ErrorCode, HB_CLEAR_STATS_TRUE) 
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsHbClearStats (HB_CLEAR_STATS_TRUE) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function           : HbShowRunningConfig
 * Input(s)           : CliHandle - CLI handler
 * Output(s)          : None.
 * Returns            : CLI_SUCCESS or CLI_FAILURE
 * Action             : This function displays the Heart beat module's
 *                       current configuration details.
 ******************************************************************************/
UINT4
HbShowRunningConfig (tCliHandle CliHandle)
{
    UINT4               u4RetVal = 0;

    nmhGetFsHbInterval (&u4RetVal);
    if (u4RetVal != HB_DEFAULT_INTERVAL)
    {
        CliPrintf (CliHandle, "heartbeat interval %d\r\n", u4RetVal);
    }

    nmhGetFsHbPeerDeadIntMultiplier (&u4RetVal);
    if (u4RetVal != HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER)
    {
        CliPrintf (CliHandle, "peer-dead interval multiplier %d\r\n", u4RetVal);
    }
    nmhGetFsHbStatsEnable ((INT4*) &u4RetVal);
    if (u4RetVal != HB_STATS_ENABLE)
    {
        CliPrintf (CliHandle, "heartbeat counters disable \r\n");
    }
    return CLI_SUCCESS;
}

#endif
