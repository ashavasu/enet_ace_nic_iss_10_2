/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: fshblw.c,v 1.8 2015/11/03 11:00:24 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "hbincs.h" 
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsHbInterval
 Input       :  The Indices

                The Object 
                retValFsHbInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbInterval(UINT4 *pu4RetValFsHbInterval)
{
    *pu4RetValFsHbInterval = HB_GET_HB_INTERVAL ();

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsHbPeerDeadIntMultiplier
 Input       :  The Indices

                The Object 
                retValFsHbPeerDeadIntMultiplier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbPeerDeadIntMultiplier(UINT4 *pu4RetValFsHbPeerDeadIntMultiplier)
{
    *pu4RetValFsHbPeerDeadIntMultiplier = HB_GET_PEER_DEAD_INT_MULTIPLIER ();

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsHbTrcLevel
 Input       :  The Indices

                The Object 
                retValFsHbTrcLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbTrcLevel(UINT4 *pu4RetValFsHbTrcLevel)
{
    *pu4RetValFsHbTrcLevel = HB_GET_TRC_LEVEL ();
    
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsHbStatsEnable
 Input       :  The Indices

                The Object
                retValFsHbStatsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbStatsEnable(INT4 *pi4RetValFsHbStatsEnable)
{
     *pi4RetValFsHbStatsEnable = HB_GET_STATS_ENABLE();

     return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsHbClearStats
 Input       :  The Indices

                The Object
                retValFsHbClearStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbClearStats(INT4 *pi4RetValFsHbClearStats)
{
    *pi4RetValFsHbClearStats = gHbInfo.i4ClearStats;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsHbInterval
 Input       :  The Indices

                The Object 
                setValFsHbInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsHbInterval(UINT4 u4SetValFsHbInterval)
{
    if (HbSetHbInterval (u4SetValFsHbInterval) == HB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsHbPeerDeadIntMultiplier
 Input       :  The Indices

                The Object 
                setValFsHbPeerDeadIntMultiplier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsHbPeerDeadIntMultiplier(UINT4 u4SetValFsHbPeerDeadIntMultiplier)
{
    if (HbSetHbPeerDeadIntMultiplier(u4SetValFsHbPeerDeadIntMultiplier)
        == HB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsHbTrcLevel
 Input       :  The Indices

                The Object 
                setValFsHbTrcLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsHbTrcLevel(UINT4 u4SetValFsHbTrcLevel)
{
    HB_SET_TRC_LEVEL (u4SetValFsHbTrcLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsHbStatsEnable
 Input       :  The Indices

                The Object
                setValFsHbStatsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsHbStatsEnable(INT4 i4SetValFsHbStatsEnable)
{

     HB_SET_STATS_ENABLE(i4SetValFsHbStatsEnable);

#ifdef RM_WANTED
	 if(i4SetValFsHbStatsEnable == HB_CLI_COUNTER_ENABLE )
	 {
		HbSendMsgToPeer(HB_COUNTER_ENABLE);
	 } 
	 else
     {
		HbSendMsgToPeer(HB_COUNTER_DISABLE);
	 }
#endif 
		
     return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsHbClearStats
 Input       :  The Indices

                The Object
                setValFsHbClearStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsHbClearStats(INT4 i4SetValFsHbClearStats)
{
    if (i4SetValFsHbClearStats == HB_CLEAR_STATS_TRUE)
    {
        gHbInfo.i4ClearStats = HB_CLEAR_STATS_TRUE;
        gHbStat.u4MsgTxCount = 0;
        gHbStat.u4MsgTxFailedCount = 0;
        gHbStat.u4MsgRxCount = 0;
        gHbStat.u4MsgRxProcCount = 0;
        gHbStat.u4RxFailedCount = 0;
        gHbInfo.i4ClearStats = HB_CLEAR_STATS_FALSE;
        return SNMP_SUCCESS;
    }
    else
    {
        /* No change as gIcchInfo.i4ClearStats is HB_CLEAR_STATS_FALSE
           by default */
        return SNMP_SUCCESS; 
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsHbInterval
 Input       :  The Indices

                The Object 
                testValFsHbInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsHbInterval(UINT4 *pu4ErrorCode , UINT4 u4TestValFsHbInterval)
{
    if ((u4TestValFsHbInterval < HB_MIN_INTERVAL) ||
            (u4TestValFsHbInterval > HB_MAX_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_HB_INVALID_HB_INTERVAL);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsHbPeerDeadIntMultiplier
 Input       :  The Indices

                The Object 
                testValFsHbPeerDeadIntMultiplier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsHbPeerDeadIntMultiplier(UINT4 *pu4ErrorCode , UINT4 u4TestValFsHbPeerDeadIntMultiplier)
{
    if ((u4TestValFsHbPeerDeadIntMultiplier <
         HB_MIN_PEER_DEAD_INT_MULTIPLIER) ||
        (u4TestValFsHbPeerDeadIntMultiplier >
         HB_MAX_PEER_DEAD_INT_MULTIPLIER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_HB_INVALID_MULTIPLIER);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsHbTrcLevel
 Input       :  The Indices

                The Object 
                testValFsHbTrcLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsHbTrcLevel(UINT4 *pu4ErrorCode , UINT4 u4TestValFsHbTrcLevel)
{
    if ((0 != u4TestValFsHbTrcLevel) &&
    (!(HB_ALL_TRC & u4TestValFsHbTrcLevel)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_HB_INVALID_TRACE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsHbStatsEnable
 Input       :  The Indices

                The Object
                testValFsHbStatsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsHbStatsEnable(UINT4 *pu4ErrorCode , INT4 i4TestValFsHbStatsEnable)
{
    if((i4TestValFsHbStatsEnable != HB_STATS_ENABLE) && 
        (i4TestValFsHbStatsEnable != HB_STATS_DISABLE))
    
    {	
        *pu4ErrorCode=SNMP_ERR_WRONG_VALUE;
	    return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsHbClearStats
 Input       :  The Indices

                The Object
                testValFsHbClearStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsHbClearStats(UINT4 *pu4ErrorCode , INT4 i4TestValFsHbClearStats)
{
    if ((i4TestValFsHbClearStats != HB_CLEAR_STATS_TRUE) &&
        (i4TestValFsHbClearStats != HB_CLEAR_STATS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsHbInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsHbInterval(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsHbPeerDeadIntMultiplier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsHbPeerDeadIntMultiplier(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsHbTrcLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsHbTrcLevel(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsHbStatsEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsHbStatsEnable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsHbClearStats
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsHbClearStats(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsHbStatsMsgTxCount
 Input       :  The Indices

                The Object 
                retValFsHbStatsMsgTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbStatsMsgTxCount(UINT4 *pu4RetValFsHbStatsMsgTxCount)
{
	*pu4RetValFsHbStatsMsgTxCount = HB_GET_MSG_TX_COUNT();

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsHbStatsMsgTxFailedCount
 Input       :  The Indices

                The Object 
                retValFsHbStatsMsgTxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbStatsMsgTxFailedCount(UINT4 *pu4RetValFsHbStatsMsgTxFailedCount)
{
	*pu4RetValFsHbStatsMsgTxFailedCount= HB_GET_MSG_TX_FCOUNT();

	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsHbStatsMsgRxCount
 Input       :  The Indices

                The Object 
                retValFsHbStatsMsgRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbStatsMsgRxCount(UINT4 *pu4RetValFsHbStatsMsgRxCount)
{
        *pu4RetValFsHbStatsMsgRxCount= HB_GET_MSG_RX_COUNT();

	return SNMP_SUCCESS;	
}

/****************************************************************************
 Function    :  nmhGetFsHbStatsMsgRxProcCount
 Input       :  The Indices

                The Object 
                retValFsHbStatsMsgRxProcCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbStatsMsgRxProcCount(UINT4 *pu4RetValFsHbStatsMsgRxProcCount)
{
	*pu4RetValFsHbStatsMsgRxProcCount= HB_GET_MSG_RX_PCOUNT();

	return SNMP_SUCCESS;	
}
/****************************************************************************
 Function    :  nmhGetFsHbStatsRxFailedCount
 Input       :  The Indices

                The Object 
                retValFsHbStatsRxFailedCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsHbStatsRxFailedCount(UINT4 *pu4RetValFsHbStatsRxFailedCount)
{
	*pu4RetValFsHbStatsRxFailedCount= HB_GET_MSG_RX_FCOUNT();

	return SNMP_SUCCESS;
}

