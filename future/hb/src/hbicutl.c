/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: hbicutl.c,v 1.17 2017/10/04 14:27:00 siva Exp $
*
* Description: Heart Beat Utility file where ICCH message
*              handling is done
*********************************************************************/

#include "hbincs.h"
/******************************************************************************
 * Function                   : HbIcchSendMsgToPeer
 * Action                     : Routine to send message to peer.
 * Input(s)                   : u4Msg.
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS/HB_FAILURE
 ******************************************************************************/
UINT4
HbIcchSendMsgToPeer (UINT4 u4Msg)
{
    tHbData             HbData;
    tHbMsg             *pHbMsg = NULL;
    UINT4               u4RetVal = HB_SUCCESS;

    MEMSET (&HbData, 0, sizeof (tHbData));

    UNUSED_PARAM (u4Msg);
    /* Allocate memory for ICCH message. */
    if ((pHbMsg = HB_ALLOC_TX_BUF (sizeof (tHbData))) == NULL)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "ICCH alloc failed !!! \r\n");
        return (HB_FAILURE);
    }

    /* Form HeartBeat message */
    HbIcchFormMsg (&HbData);

    /* Copy HB data to the appropriate offset by leaving space for HB hdr */
    if ((HB_COPY_TO_OFFSET (pHbMsg, &HbData, 0, sizeof (tHbData)))
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HB copy to CRU buf failed !!! \r\n");
        u4RetVal = HB_FAILURE;
    }
    if (u4RetVal != HB_FAILURE)
    {
        u4RetVal = HbIcchPrependHbHdr (pHbMsg, sizeof (tHbData));
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* Send the pkt out to peer thru' RAW socket */
        if (HbIcchSendMsg (pHbMsg, HB_ICCH_MCAST_IPADDR) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "!!! HbIcchSendMsg -- Failed !!!\n");
            u4RetVal = HB_FAILURE;
        }
    }
    else
    {
        HB_FREE (pHbMsg);
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbIcchFormAndSendMsg
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE
 * Action             : Routine to form heart-beat message and send it to the
 *                      peer.
 ******************************************************************************/
UINT4
HbIcchFormAndSendMsg (VOID)
{
    tHbData             HbData;
    tHbMsg             *pHbMsg = NULL;
    UINT4               u4RetVal = HB_SUCCESS;

    MEMSET (&HbData, 0, sizeof (tHbData));

    if (HbLaGetMCLAGSystemStatus () == HB_MCLAG_DISABLED)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Unable to send HB packets\
                        since MCLAG is in disabled state!!!\r\n");
        return HB_FAILURE;
    }

    /* Allocate memory for ICCH message. */
    if ((pHbMsg = HB_ALLOC_TX_BUF (sizeof (tHbData))) == NULL)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbIcchFormAndSendMsg: HB alloc "
                "failed !!!\r\n");
        return (HB_FAILURE);
    }

    /* Form HeartBeat message */
    HbIcchFormMsg (&HbData);

    /* Set the Node state in flag and send it to remote MC-LAG node. */
    /* Peer node does the election basd on the state */

    if (HB_ICCH_GET_NODE_STATE () == ICCH_INIT)
    {
        HB_ICCH_RESET_FLAGS (HB_ICCH_MASTER_STATE);
        HB_ICCH_RESET_FLAGS (HB_ICCH_SLAVE_STATE);
        HB_ICCH_SET_FLAGS (HB_ICCH_INIT_STATE);
    }
    else if (HB_ICCH_GET_NODE_STATE () == ICCH_MASTER)
    {
        HB_ICCH_RESET_FLAGS (HB_ICCH_INIT_STATE);
        HB_ICCH_RESET_FLAGS (HB_ICCH_SLAVE_STATE);
        HB_ICCH_SET_FLAGS (HB_ICCH_MASTER_STATE);
    }
    else if (HB_ICCH_GET_NODE_STATE () == ICCH_SLAVE)
    {
        HB_ICCH_RESET_FLAGS (HB_ICCH_INIT_STATE);
        HB_ICCH_RESET_FLAGS (HB_ICCH_MASTER_STATE);
        HB_ICCH_SET_FLAGS (HB_ICCH_SLAVE_STATE);
    }
    HbData.u4Flags = OSIX_HTONL (HB_ICCH_GET_FLAGS ());

    /* Copy HB data to the appropriate offset by leaving space for HB hdr */
    if ((HB_COPY_TO_OFFSET (pHbMsg, &HbData, 0, sizeof (tHbData)))
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbIcchFormAndSendMsg: HB copy to CRU buf failed !!!\r\n");
        u4RetVal = HB_FAILURE;
    }
    if (u4RetVal != HB_FAILURE)
    {
        u4RetVal = HbIcchPrependHbHdr (pHbMsg, sizeof (tHbData));
    }

    if (u4RetVal != HB_FAILURE)
    {
#ifdef NPAPI_WANTED
        /* Send the pkt out to peer by directly writing it on ICCL port */
        if (HbIcchNpPortWrite (pHbMsg, HB_ICCH_MCAST_IPADDR) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "HbIcchFormAndSendMsg: HbIcchNpPortWrite -- Failed !!!\r\n");
            u4RetVal = HB_FAILURE;
        }
#else
        /* Send the pkt out to peer thru' RAW socket */
        if (HbIcchSendMsg (pHbMsg, HB_ICCH_MCAST_IPADDR) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "HbIcchFormAndSendMsg: HbIcchSendMsg -- Failed !!!\r\n");
            u4RetVal = HB_FAILURE;
        }
#endif
    }
    else
    {
        HB_FREE (pHbMsg);
    }

    if (u4RetVal != HB_FAILURE)
    {
        HB_INCR_MSG_TX_COUNT ();
    }
    else
    {
        HB_INCR_MSG_TX_FCOUNT ();
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbIcchFormMsg
 * Input(s)           : pHbData - ptr to RmHBMsg
 * Output(s)          : None.
 * Returns            : HB_SUCCESS
 * Action             : Routine to form the HeartBeat message.
 ******************************************************************************/
UINT4
HbIcchFormMsg (tHbData * pHbData)
{
    MEMSET (pHbData, 0, sizeof (tHbData));

    pHbData->u4MsgType = OSIX_HTONL (HEART_BEAT_MSG);
    pHbData->u4HbInterval = OSIX_HTONL (HB_GET_HB_INTERVAL ());
    pHbData->u4PeerDeadInterval = OSIX_HTONL (HB_GET_PEER_DEAD_INTERVAL ());
    pHbData->u4PeerDeadIntMultiplier =
        OSIX_HTONL (HB_GET_PEER_DEAD_INT_MULTIPLIER ());
    pHbData->u4MasterNode = OSIX_HTONL (HB_ICCH_GET_MASTER_NODE_ID ());
    pHbData->u4IcchSelfNode = OSIX_HTONL (HB_ICCH_GET_SELF_NODE_ID ());
    pHbData->u4IcchSelfMask = OSIX_HTONL (HB_ICCH_GET_SELF_NODE_MASK ());
    pHbData->u4IcchPeerNode = OSIX_HTONL (HB_ICCH_GET_PEER_NODE_ID ());
    pHbData->u2MclagPortMaskValue = gu2PortMaskValue;

    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbIcchNpPortWrite
 * Input(s)           : pHbMsg - Message to be sent.
 *                      u4DestAddr - Dest addr to which the message has to be sent
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE
 * Action             : Routine to send out the HB messages (which are arrived from
 *                      applications) to the peer.
*******************************************************************************/
UINT4
HbIcchNpPortWrite (tHbMsg * pHbMsg, UINT4 u4DestAddr)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               au2ActivePorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT4               u4RetVal = HB_SUCCESS;
    UINT4               u4IfIndex = 0;
    UINT1               au1Data[HB_ICCH_PKT_LEN];
    UINT4               u4HbPktLen = 0;
    UINT4               u4TotLen = 0;
    t_IP_HEADER        *pIpHdr = NULL;
    UINT2               u2Cksum = 0;
    UINT2               u2Port = 0;
    UINT2               u2NumPorts = 0;

    MEMSET (au2ActivePorts, 0, (sizeof (UINT2) * L2IWF_MAX_PORTS_PER_CONTEXT));

    /* Get the length of HB pkt to be sent */
    HB_GET_DATA_4_BYTE (pHbMsg, HB_HDR_OFF_TOT_LENGTH, u4HbPktLen);

    /* Total length of pkt to be sent includes IP hdr length */
    u4TotLen = u4HbPktLen + IP_HDR_LEN + HB_MSG_ETH_HEADER_SIZE;

    if (u4TotLen > HB_ICCH_PKT_LEN)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HB packt length exceeds defined "
                "length\r\n");
        HB_FREE (pHbMsg);
        return HB_FAILURE;
    }
    /* Form IP hdr as IP_HDRINCL option is set for the socket. */
    pIpHdr = (t_IP_HEADER *) (VOID *) (au1Data + HB_MSG_ETH_HEADER_SIZE);

    pIpHdr->u1Ver_hdrlen = (UINT1) IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = 0;
    pIpHdr->u2Totlen = OSIX_HTONS (u4TotLen);
    pIpHdr->u2Id = 0;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = 1;
    pIpHdr->u1Proto = HB_PROTO;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u4Src = OSIX_HTONL (HB_ICCH_GET_SELF_NODE_ID ());
    pIpHdr->u4Dest = OSIX_HTONL (u4DestAddr);
    u2Cksum = HB_LINEAR_CALC_CKSUM ((CONST INT1 *) pIpHdr, IP_HDR_LEN);
    pIpHdr->u2Cksum = OSIX_HTONS (u2Cksum);
    /* Copy the HB info to the pkt. */
    /* Copy the CRU buf to linear buf and free the CRU buf */
    if (HB_COPY_TO_LINEAR_BUF (pHbMsg, (au1Data + IP_HDR_LEN +
                                        HB_MSG_ETH_HEADER_SIZE), 0, u4HbPktLen)
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Copy from CRU buf to linear buf "
                "failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    IcchGetIcclIfIndex (&u4IfIndex);

    if (u4RetVal != HB_FAILURE)
    {
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((u4HbPktLen + IP_HDR_LEN),
                                                  0)) == NULL)
        {
            return HB_FAILURE;
        }
        CRU_BUF_Copy_OverBufChain (pBuf, au1Data, 0, u4HbPktLen + IP_HDR_LEN);

        if (L2IwfGetActivePortsForPortChannel ((UINT2) u4IfIndex,
                                               au2ActivePorts,
                                               &u2NumPorts) != L2IWF_FAILURE)
        {
            if (u2NumPorts > 0)
            {
                u2Port = au2ActivePorts[0];
                u4RetVal = (UINT4) (HbUtilAddEthHeader (au1Data, u4TotLen));
#ifdef NPAPI_WANTED
                if (CfaNpPortWrite (au1Data, u2Port, u4TotLen) == FNP_FAILURE)
                {
                    HB_TRC (HB_ALL_FAILURE_TRC,
                            "Writing to ICCL interface failed in "
                            "HbIcchNpPortWrite\r\n");
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    HB_FREE (pHbMsg);
                    return HB_FAILURE;
                }
                HB_PKT_DUMP (HB_PKT_DUMP_TRC, pHbMsg, u4TotLen,
                             "Dumping HB packets on transmission\r\n");
            }
#else
                UNUSED_PARAM (u2Port);
            }
#endif
        }
        else
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "Getting port number with AggIndex failed in "
                    "HbIcchNpPortWrite\r\n");
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "copying to linear buffer failed in HbIcchNpPortWrite\r\n");
    }

    HB_FREE (pHbMsg);
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbIcchSendMsg
 * Input(s)           : pHbMsg - Message to be sent.
 *                      u4DestAddr - Dest addr to which the message has to be sent
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE
 * Action             : Routine to send out the HB messages (which are arrived from
 *                      applications) to the peer.
*******************************************************************************/
UINT4
HbIcchSendMsg (tHbMsg * pHbMsg, UINT4 u4DestAddr)
{
    UINT4               u4RetVal = HB_SUCCESS;
    UINT1               au1Data[HB_SYNC_PKT_LEN + IP_HDR_LEN];
    UINT4               u4HbPktLen = 0;
    UINT4               u4TotLen = 0;
    t_IP_HEADER        *pIpHdr;
    UINT2               u2Cksum = 0;

    /* Get the length of HB pkt to be sent */
    HB_GET_DATA_4_BYTE (pHbMsg, HB_HDR_OFF_TOT_LENGTH, u4HbPktLen);

    /* Total length of pkt to be sent includes IP hdr length */
    u4TotLen = u4HbPktLen + IP_HDR_LEN;

    /* Form IP hdr as IP_HDRINCL option is set for the socket. */
    pIpHdr = (t_IP_HEADER *) (VOID *) (au1Data);

    pIpHdr->u1Ver_hdrlen = (UINT1) IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = 0;
    pIpHdr->u2Totlen = OSIX_HTONS (u4TotLen);
    pIpHdr->u2Id = 0;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = 1;
    pIpHdr->u1Proto = HB_PROTO;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u4Src = OSIX_HTONL (HB_ICCH_GET_SELF_NODE_ID ());
    pIpHdr->u4Dest = OSIX_HTONL (u4DestAddr);
    u2Cksum = HB_LINEAR_CALC_CKSUM ((CONST INT1 *) pIpHdr, IP_HDR_LEN);
    pIpHdr->u2Cksum = OSIX_HTONS (u2Cksum);
    /* Copy the HB info to the pkt. */
    /* Copy the CRU buf to linear buf and free the CRU buf */
    if (HB_COPY_TO_LINEAR_BUF (pHbMsg, au1Data + IP_HDR_LEN, 0, u4HbPktLen)
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Copy from CRU buf to linear buf "
                "failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    HB_FREE (pHbMsg);

    if (u4RetVal != HB_FAILURE)
    {
        /* ICCH Heart beat message channel should be decided based on ICCL Port
         * Type. If it is out of Band port, HB messages will be tranmitted
         * through socket on OOB port running over Linux IP.
         * If it is an Inband Port,HB messages will be tranmitted through
         * socket on stacking port running over Aricent IP */
        if (HbRawIcchSockSend (au1Data, (UINT2) u4TotLen, u4DestAddr) ==
            HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "!!! RmIcchSockSend - failure !!!\r\n");
            u4RetVal = HB_FAILURE;
        }
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbIcchPrependHbHdr
 * Input(s)           : u4Size.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE.
 * Action             : Routine to form HB header and prepend it to the
 *                      HB message.
 ******************************************************************************/
UINT4
HbIcchPrependHbHdr (tHbMsg * pHbMsg, UINT4 u4Size)
{
    tHbHdr              HbHdr;
    UINT2               u2Cksum = 0;
    UINT4               u4RetVal = HB_SUCCESS;
    INT4                i4RetVal = CRU_FAILURE;

    /* From HB hdr */
    MEMSET (&HbHdr, 0, sizeof (tHbHdr));

    HbHdr.u2Version = OSIX_HTONS (HB_VERSION_NUM);
    HbHdr.u2Chksum = OSIX_HTONS (0);
    HbHdr.u4TotLen = OSIX_HTONL ((HB_HDR_LENGTH + u4Size));
    HbHdr.u4MsgType = OSIX_HTONS (0);    /* REQ/RESP - to be filled while 
                                         * implementing re-transmission */
    HbHdr.u4SrcEntId = OSIX_HTONL (ICCH_APP_ID);
    HbHdr.u4DestEntId = OSIX_HTONL (ICCH_APP_ID);
    HbHdr.u4SeqNo = OSIX_HTONS (0);

    /* Prepend HB hdr to HB data */
    if ((HB_PREPEND_BUF (pHbMsg, (UINT1 *) &HbHdr, sizeof (tHbHdr))) !=
        CRU_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Prepend HB hdr - failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    /* Calculate the checksum and fill-in here */
    u2Cksum = HB_CALC_CKSUM (pHbMsg, (HB_HDR_LENGTH + u4Size), 0);

    HB_DATA_PUT_2_BYTE (pHbMsg, HB_HDR_OFF_CKSUM, u2Cksum, i4RetVal);
    if (i4RetVal != CRU_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Update Cksum in HB hdr - failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbIcchProcessNodeId
 * Input(s)           : pHbData - Rcvd HB message,
 * Output(s)          : u1Event - Event to be set based on the message.
 * Returns            : HB_SUCCESS/HB_FAILURE.
 * Action             : Routine to process the NodeId information of 
 *                      HeartBeat message.
 ******************************************************************************/
UINT4
HbIcchProcessNodeId (tHbData * pHbData, UINT1 *pu1Event)
{
    tIcchHbMsg          IcchHbMsg;
    tUtlInAddr          HbNodeId;

    MEMSET (&HbNodeId, 0, sizeof (tUtlInAddr));
    MEMSET (&IcchHbMsg, 0, sizeof (tIcchHbMsg));

    /* This node doesn't know about MASTER node. */
    if ((HB_ICCH_GET_MASTER_NODE_ID () != pHbData->u4MasterNode) &&
        (pHbData->u4MasterNode != 0))
    {
        /* This node doesn't know about MASTER node. 
         * Peer doesn't know about MASTER node.
         * Master node is not yet elected. Start the election */
        *pu1Event = HB_ICCH_2WAY_RCVD;
    }
    else
    {
        if ((pHbData->u4MasterNode != 0) &&
            (HB_ICCH_GET_NODE_STATE () == HB_ICCH_INIT))
        {
            HB_ICCH_SET_MASTER_NODE_ID (pHbData->u4MasterNode);
            HbNodeId.u4Addr = OSIX_NTOHL (HB_ICCH_GET_MASTER_NODE_ID ());
            HB_TRC1 (HB_NOTIFICATION_TRC, "MASTER NODE elected: %s\r\n",
                     INET_NTOA (HbNodeId));

            HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_UP);
            *pu1Event = HB_ICCH_MASTER_ELCTD;
        }
        else if (((0 == IcchApiGetPeerAddress ()) &&
                  (HB_ICCH_GET_PEER_NODE_ID () != 0)) ||
                 (HB_ICCH_PEER_NEWLY_ARRIVED == OSIX_TRUE))
        {
            HbIcchStartElection ();
            if (HB_ICCH_GET_NODE_STATE () == HB_ICCH_MASTER)
            {
                IcchHbMsg.u4Evt = GO_MASTER;
                IcchHbMsg.u4PeerAddr = HB_ICCH_GET_PEER_NODE_ID ();
                IcchApiSendHbEvtToIcch (&IcchHbMsg);
                HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (),
                                      ICCH_PEER_UP);
            }
        }
        else
        {
            *pu1Event = HB_IGNORE_PKT;
        }
    }
    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbUtilAddEthHeader
 * Input(s)           : pu1DataBuf - Message in which ethernet header to be added.
 *                      u4PktSize - Size of the message before adding header
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE
 * Action             : Routine to add ethernet header to the heartbeat messages
 *                      before writing pkt to port.
*******************************************************************************/
INT4
HbUtilAddEthHeader (UINT1 *pu1DataBuf, UINT4 u4PktSize)
{
    UINT1               au1DstMac[CFA_ENET_ADDR_LEN] =
        { 0x01, 0x00, 0x5E, 0x00, 0x00, 0xFA };
    tVlanId             IcclVlanId;
    UINT2               u2EtherType = OSIX_HTONS (HB_MSG_ETHERTYPE);
    UINT2               u2VlanProtoId = OSIX_HTONS (VLAN_PROTOCOL_ID);
    UINT2               u2Tag = 0;

    UNUSED_PARAM (u4PktSize);

    IcclVlanId = (tVlanId) (IcchApiGetIcclVlanId (ICCH_DEFAULT_INST));
    /* allocate CRU buffer */

    /* Copy the Destination Mac */
    MEMCPY (pu1DataBuf, au1DstMac, CFA_ENET_ADDR_LEN);
    MEMCPY (pu1DataBuf + CFA_ENET_ADDR_LEN, gHbInfo.au1SysMac,
            CFA_ENET_ADDR_LEN);
    MEMCPY (pu1DataBuf + HB_MSG_VLAN_PROTOID_OFFSET,
            &u2VlanProtoId, HB_MSG_ETHERTYPE_LEN);

    u2Tag = HB_MSG_VLAN_HIGHEST_PRIORITY;

    u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);

    u2Tag = (UINT2) (u2Tag | IcclVlanId);

    u2Tag = (UINT2) (OSIX_HTONS (u2Tag));

    MEMCPY (pu1DataBuf + HB_MSG_VLAN_TAG_OFFSET, &u2Tag, HB_MSG_ETHERTYPE_LEN);
    MEMCPY (pu1DataBuf + HB_MSG_ETHERTYPE_OFFSET,
            (UINT1 *) &u2EtherType, HB_MSG_ETHERTYPE_LEN);
    return HB_SUCCESS;
}
