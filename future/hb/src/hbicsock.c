/********************************************************************
 *  Copyright (C) 2014 Aricent Inc . All Rights Reserved
 * 
 *  $Id: hbicsock.c,v 1.3 2015/11/04 11:22:16 siva Exp $
 * 
 *  Description: ICCH heartbeat socket interface functions 
 * ********************************************************************/

#include "hbincs.h"
#include "fssocket.h"

/******************************************************************************
 * Function           	      : HbRawIcchSockInit 
 * Action                     : Routine to create a RAW IP socket, set socket 
 * 			        options for  sending and receiving RM multicast
 * 			        packets.
 * Input(s)                   : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)          	      : RM HB Raw socket descriptor.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating 
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns  	              : HB_SUCCESS/HB_FAILURE 
 ******************************************************************************/
VOID
HbRawIcchSockInit (VOID)
{
    struct in_addr      SelfNodeAddr;
    UINT4               u4RetVal = HB_SUCCESS;
    INT4                i4SockFd = HB_INV_SOCK_FD;
    INT4                i4Flags = -1;
    INT4                i4OptnVal = TRUE;

    MEMSET (&SelfNodeAddr, 0, sizeof (struct in_addr));
    HB_TRC (HB_INIT_SHUT_TRC, "HbRawIcchSockInit: "
               "Raw socket creation entry...\r\n");
    /* Open a raw socket for heart beat messages */
    i4SockFd = socket (AF_INET, SOCK_RAW, HB_PROTO);
    if (i4SockFd < 0)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbRawIcchSockInit: Raw Socket "
                   "Creation Failure !!!\r\n");
        u4RetVal = HB_FAILURE;
    }

    /* Get current socket flags */
    if (u4RetVal != HB_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "HbRawIcchSockInit: Fcntl GET "
                       "Failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
        { 
            HB_TRC (HB_ALL_FAILURE_TRC, "HbRawIcchSockInit: Fcntl SET "
                       "Failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        } 
    }

    if (u4RetVal != HB_FAILURE)
    {
        SelfNodeAddr.s_addr = OSIX_HTONL (HB_ICCH_GET_SELF_NODE_ID ());

        /* Set socket option to send multicast IP pkt */
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_MULTICAST_IF,
			(char *) &SelfNodeAddr, sizeof (struct in_addr)) < 0)
        {  
	   perror ("HbRawIcchSockInit: IP_MULTICAST_IF setsockopt failure\r\n");
           HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawIcchSockInit: setsockopt "
                       "IP_MULTICAST_IF - failure !!!\r\n");
           close (i4SockFd);
           u4RetVal = HB_FAILURE;
        }
    }
    if (u4RetVal != HB_FAILURE)
    {
        if ((setsockopt (i4SockFd, IPPROTO_IP, IP_HDRINCL, (&i4OptnVal),
                         sizeof (INT4))) < 0)
        {
            perror ("HbRawIcchSockInit: IP_HDRINCL setsockopt failure\r\n");
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawIcchSockInit: setsockopt IP_HDRINCL "
                       "- failure !!!\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_PKTINFO, &i4OptnVal,
                    sizeof (INT4)) < 0)
        {
            perror ("HbRawIcchSockInit: IP_PKTINFO setsockopt failure\r\n");
            close (i4SockFd); 
	    u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        if (SelAddFd (i4SockFd, HbIcchRawPktRcvd) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawIcchSockInit: SelAddFd Failure "
                       "while adding RM raw socket\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }
    if (u4RetVal == HB_FAILURE)
    {
       return;
    }
    HB_ICCH_RAW_SOCK_FD ()  = i4SockFd;
    HB_TRC (HB_INIT_SHUT_TRC, "HbRawIcchSockInit: "
               "Raw socket created successfully - exit...\r\n");
    return;
}

/*******************************************************************************
 * Function           	       : HbRawIcchSockSend
 * Action             	       : Routine to send the packet out through RAW IP 
 * 				 socket.This socket is specifically used by RM 
 * 				 module to send the Heart Beat messages destined
 * 				 to the multicast ip address 224.0.0.250.
 * Input(s)           	       : pu1Data - pointer to the data to be sent out
 *                               u2PktLen - Length of the data
 * Output(s)          	       : None.
 * <OPTIONAL Fields>           : None
 * Global Variables Referred   : None
 * Global variables Modified   : None
 * Exceptions or Operating 
 * System Error Handling       : None
 * Use of Recursion            : None
 * Returns             	       : HB_SUCCESS/HB_FAILURE
 ******************************************************************************/
UINT4
HbRawIcchSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    struct sockaddr_in  DestAddr;
    UINT4               u4RetVal = HB_SUCCESS;
    INT4                i4WrBytes = 0;

    MEMSET (&DestAddr, 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestAddr.sin_port = 0;

    i4WrBytes = sendto (gHbInfo.i4IcchRawSockFd, pu1Data, u2PktLen, 0,
                        (struct sockaddr *) &DestAddr,
                        sizeof (struct sockaddr_in));
    if (i4WrBytes < 0)
    {
        perror ("HbSend failure\r\n");
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! HbSendTo - failure !!!\r\n");
        u4RetVal = HB_FAILURE;
    }
    return (u4RetVal);
}


/******************************************************************************
 * Function           		: HbRawIcchSockRcv
 * Action             		: Routine to receive RM protocol packets.
 * Input(s)           		: pu1Data - pointer to rcv the packet
 *                      	  u2BufSize - size of the buffer to be received.
 * Output(s)          		: pu1Data - pointer to the packet rcvd.
 *                      	  pu4PeerAddr - IP addr of the peer from which 
 *                      	  the pkt is rcvd.
 * <OPTIONAL Fields>           	: None
 * Global Variables Referred   	: None
 * Global variables Modified   	: None
 * Exceptions or Operating 
 * System Error Handling 	: None
 * Use of Recursion            	: None
 * Returns            	       	: i4Len - Length of the packet received.
 ******************************************************************************/
INT4
HbRawIcchSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    struct sockaddr_in  PeerAddr;
    INT4                i4AddrLen = sizeof (PeerAddr);
    INT4                i4Len = 0;

    i4Len = recvfrom (gHbInfo.i4IcchRawSockFd, pu1Data,
                      u2BufSize, 0, (struct sockaddr *) &PeerAddr,
                      (socklen_t *) & i4AddrLen);
   *pu4PeerAddr = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
    return (i4Len);
}


