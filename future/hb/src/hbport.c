/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: hbport.c,v 1.8.2.1 2018/03/16 13:43:25 siva Exp $
*
* Description:  Heartbeat module portable functions 
***********************************************************************/
#ifndef _HBPORT_C_
#define _HBPORT_C_

#include "hbincs.h"

/*****************************************************************************/
/* Function Name      : HbLaGetMCLAGSystemStatus                             */
/*                                                                           */
/* Description        : This function is used to get the MCLAG system status */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : HB_MCLAG_ENABLED or HB_MCLAG_DISABLED                */
/*                                                                           */
/*****************************************************************************/
UINT1
HbLaGetMCLAGSystemStatus (VOID)
{
#ifdef LA_WANTED
    return LaGetMCLAGSystemStatus ();
#else
    return HB_MCLAG_DISABLED;
#endif
}

/*****************************************************************************/
/* Function Name      : HbLaNotifyMclagShutdownComplete                      */
/*                                                                           */
/* Description        : This function is used to notify the MCLAG shutdown   */
/*                      completion to LA module                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
HbLaNotifyMclagShutdownComplete (VOID)
{
#ifdef LA_WANTED
    LaNotifyMclagShutdownComplete ();
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : HbAstDisableStpOnPort                                */
/*                                                                           */
/* Description        : This function calls the AST module to disable STP on */
/*                      a given port                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index on which STP is to be    */
/*                      disabled.                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT1
HbAstDisableStpOnPort (UINT4 u4IfIndex)
{
#ifdef RSTP_WANTED
    if (AstDisableStpOnPort (u4IfIndex) == OSIX_FAILURE)
    {
        return HB_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    return HB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : HbPortGetIpPortFrmIndex                              */
/*                                                                           */
/* Description        : This function gets IP port from  L3 IfIndex          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index on which STP is to be    */
/*                      disabled.                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                             */
/*                                                                           */
/*****************************************************************************/
VOID
HbPortGetIpPortFrmIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    *pu4Port = (UINT4) CfaGetIfIpPort (u4IfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : HbArpModifyWithIndex                                 */
/*                                                                           */
/* Description        : This function is used to delete the ARP Added in the */
/*                learnt on the Index with the mentioned PeerAddr.     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                 */
/*                      u4PeerAddr - IP Address of peer               */
/*            i1State - State                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
HbArpModifyWithIndex (UINT4 u4IfIndex, UINT4 u4PeerAddr, INT1 i1State)
{

#ifdef ARP_WANTED
    ArpModifyWithIndex (u4IfIndex, u4PeerAddr, i1State);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PeerAddr);
    UNUSED_PARAM (i1State);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : HbArpAddArpEntry                                     */
/*                                                                           */
/* Description        : This function is used to add ARP Entry.              */
/*                                                                           */
/* Input(s)           : tArpData - ARP Datastructure                   */
/*                      pDataBuff - CRU Buff Pointer                  */
/*            u1ModId - Mode Id                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
HbArpAddArpEntry (tARP_DATA ArpData)
{
#ifdef ARP_WANTED
    if (ARP_SUCCESS == ArpAddArpEntry (ArpData, NULL, 0))
    {
        return HB_SUCCESS;
    }
    else
    {
        return HB_FAILURE;
    }
#else
    UNUSED_PARAM (ArpData);
    return HB_FAILURE;
#endif

}

/*****************************************************************************/
/* Function Name      : HbVlanConfigStaticUcastEntry                         */
/*                                                                           */
/* Description        : This function configures the static MAC Address      */
/*                      for ICCL interface  IfIndex                          */
/*                                                                           */
/* Input(s)           : pStUcastInfo - Static Unicast Info                   */
/*                      u1Action - VLAN_DELETE/VLAN_ADD                      */
/*                      pu1Error - Error Code                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
HbVlanConfigStaticUcastEntry (tConfigStUcastInfo * pStUcastInfo,
                              UINT1 u1Action, UINT1 *pu1Error)
{
#ifdef VLAN_WANTED
    VlanConfigStaticUcastEntry (pStUcastInfo, u1Action, pu1Error);
#else
    UNUSED_PARAM (pStUcastInfo);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (pu1Error);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : HbLaDisableLldpOnIcclPorts                           */
/*                                                                           */
/* Description        : This function disables LLDP on ICCL interfce         */
/*                                                                           */
/* Input(s)           : u4AggIndex - Agg. index of ICCL port channel         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : HB_SUCCESS/HB_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
HbLaDisableLldpOnIcclPorts (UINT4 u4AggIndex)
{
#ifdef LA_WANTED
    if (LA_TRUE == (LaApiDisableLldpOnIcclPorts (u4AggIndex)))
    {
        return HB_SUCCESS;
    }
#else
    UNUSED_PARAM (u4AggIndex);
#endif
    return HB_FAILURE;
}

/* ***************************************************************************/
/* Function Name      : HbVlanHwDelFdbEntry                                  */
/*                                                                           */
/* Description        : This function flushes the fdb entry with the IfIndex */
/*                      learnt on VlanId with the Mac-Addr.                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      MacAddr - MacAddress to be learnt                    */
/*                      VlanId  - VLAN Id                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
HbVlanHwDelFdbEntry (UINT4 u4IfIndex, tMacAddr MacAddr, tVlanId VlanId)
{
#ifdef VLAN_WANTED
    VlanHwDelFdbEntry (u4IfIndex, MacAddr, VlanId);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : HbLaIcchInitProperties                               */
/*                                                                           */
/* Description        : This function is used to initialize ICCL properties  */
/*                      Sets the ICCL LACP mode as Manual mode               */
/*                                                                           */
/* Input(s)           : u4IcclIfIndex - ICCL interface index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
HbLaIcchInitProperties (UINT4 u4IcclIfIndex)
{
#ifdef LA_WANTED
    LaIcchInitProperties (u4IcclIfIndex);
#else
    UNUSED_PARAM (u4IcclIfIndex);
#endif /* LA_WANTED */
    return;
}

#endif /* _HBPORT_C_ */
/* End of File */
