/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbtmr.c,v 1.25.2.1 2018/03/16 13:43:25 siva Exp $
*
* Description: Timer related functions of Heart Beat module.
*********************************************************************/
#include "hbincs.h"

PRIVATE VOID        HbTmrInitTmrDesc (VOID);
#ifdef ICCH_WANTED
#ifdef NPAPI_WANTED
static UINT4        u4HbTmrCount;
#endif
static UINT4        u4HbTolerance;
#endif
/******************************************************************************
 * Function           : HbTmrInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to create RM HB timer.
 ******************************************************************************/
UINT4
HbTmrInit (VOID)
{
    UINT4               u4RetVal = HB_SUCCESS;

    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrInit: ENTRY\r\n");
    if (TmrCreateTimerList ((CONST UINT1 *) HB_TASK_NAME,
                            HB_TMR_EXPIRY_EVENT, NULL,
                            (tTimerListId *) & (gHbInfo.TmrListId))
        != TMR_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbTmrInit: Heart Beat timer list creation Failure !!!\r\n");
        u4RetVal = HB_FAILURE;
    }
    HbTmrInitTmrDesc ();
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrInit: EXIT\r\n");
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbTmrDeInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to delete RM HB timer list
 ******************************************************************************/
UINT4
HbTmrDeInit (VOID)
{
    UINT4               u4RetVal = HB_SUCCESS;

    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrDeInit: ENTRY\r\n");
    if (gHbInfo.TmrListId == NULL)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbTmrDeInit: " "Timer list is deleted already !!!\r\n");
        return (u4RetVal);
    }
    if (TmrDeleteTimerList (gHbInfo.TmrListId) != TMR_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbTmrDeInit: "
                "Heart Beat timer list deletion Failure !!!\r\n");
        u4RetVal = HB_FAILURE;
    }
    gHbInfo.TmrListId = NULL;
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrDeInit: EXIT\r\n");
    return (u4RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : HbTmrInitTmrDesc
 *    
 *    DESCRIPTION      : This function intializes the timer description for all
 *                       the timers in RM HB module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
HbTmrInitTmrDesc (VOID)
{
#ifdef RM_WANTED
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrInitTmrDesc: ENTRY\r\n");
    gHbInfo.aTmrDesc[HB_HEARTBEAT_TIMER].TmrExpFn = HbTmrHdlHbTmrExp;
    gHbInfo.aTmrDesc[HB_PEER_DEAD_TIMER].TmrExpFn = HbTmrHdlPeerDeadTmrExp;
    gHbInfo.aTmrDesc[HB_FSW_ACK_TIMER].TmrExpFn = HbTmrHdlFswAckTmrExp;
#endif /* RM_WANTED */
#ifdef ICCH_WANTED
    gHbInfo.aTmrDesc[HB_ICCH_HEARTBEAT_TIMER].TmrExpFn = HbIcchTmrHdlHbTmrExp;
    gHbInfo.aTmrDesc[HB_ICCH_PEER_DEAD_TIMER].TmrExpFn =
        HbIcchTmrHdlPeerDeadTmrExp;
#endif /* ICCH_WANTED */
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrInitTmrDesc: EXIT\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : HbTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       message is received by RM HB task. Different timer
 *                       expiry handlers are called based on the timer type.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
HbTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;

    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrExpHandler: ENTRY\r\n");
    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gHbInfo.TmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

        HB_TRC1 (HB_NOTIFICATION_TRC, "HbTmrExpHandler: "
                 "Timer Id=%d expired\r\n", u1TimerId);
        /* The timer function does not take any parameter. */
        (*(gHbInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrExpHandler: EXIT\r\n");
}

/******************************************************************************
 * Function           : HbTmrStartTimer
 * Input(s)           : u1TmrId - Timer id
                        u4TmrVal - Time duration of the timer 
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to start the RM HB module timers
 ******************************************************************************/
UINT4
HbTmrStartTimer (UINT1 u1TmrId, UINT4 u4TmrVal)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrStartTimer: ENTRY\r\n");

    if (TmrStart (gHbInfo.TmrListId,
                  &(gHbInfo.HbTmrBlk[u1TmrId]),
                  u1TmrId, 0, u4TmrVal) != TMR_FAILURE)
    {
        HB_TRC1 (HB_NOTIFICATION_TRC, "HbTmrStartTimer: Timer Id=%d "
                 "started successfully - EXIT\r\n", u1TmrId);
        return HB_SUCCESS;
    }
    HB_TRC (HB_ALL_FAILURE_TRC, "HbTmrStartTimer: Timer start failed !!!\r\n");
    return HB_FAILURE;
}

/******************************************************************************
 * Function           : HbTmrStopTimer
 * Input(s)           : u1TmrId - Timer id
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to stop the RM HB module timers
 ******************************************************************************/
UINT4
HbTmrStopTimer (UINT1 u1TmrId)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrStopTimer: ENTRY\r\n");

    if (TmrStop (gHbInfo.TmrListId,
                 &(gHbInfo.HbTmrBlk[u1TmrId])) != TMR_FAILURE)
    {
        HB_TRC1 (HB_NOTIFICATION_TRC, "HbTmrStopTimer: Timer Id=%d "
                 "stopped successfully - EXIT\r\n", u1TmrId);
        return HB_SUCCESS;
    }
    HB_TRC (HB_ALL_FAILURE_TRC, "HbTmrStopTimer: Timer stopping "
            "failed !!!\r\n");
    return HB_FAILURE;
}

/******************************************************************************
 * Function           : HbTmrRestartTimer
 * Input(s)           : u1TmrId - Timer id
                        u4TmrVal - Time duration of the timer 
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to restart the RM HB module timers
 ******************************************************************************/
UINT4
HbTmrRestartTimer (UINT1 u1TmrId, UINT4 u4TmrVal)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrRestartTimer: ENTRY\r\n");

    /* If issu loadversion triggerd on node then do not restart the
     * HP timers til reboot the system */
    if (IssuApiIsLoadVersionTriggered () == OSIX_TRUE)
    {
        HB_TRC1 (HB_NOTIFICATION_TRC, "HbTmrRestartTimer: Timer Id=%d "
                 "not started due to issu loadversion triggerd"
                 "- EXIT\r\n", u1TmrId);
        return HB_SUCCESS;
    }
    if (TmrRestart (gHbInfo.TmrListId, &(gHbInfo.HbTmrBlk[u1TmrId]),
                    u1TmrId, 0, u4TmrVal) != TMR_FAILURE)
    {
        HB_TRC1 (HB_NOTIFICATION_TRC, "HbTmrRestartTimer: Timer Id=%d "
                 "started successfully - EXIT\r\n", u1TmrId);
        return HB_SUCCESS;
    }
    HB_TRC (HB_ALL_FAILURE_TRC, "HbTmrRestartTimer: Timer start "
            "failed !!!\r\n");
    return HB_FAILURE;
}

#ifdef RM_WANTED
/******************************************************************************
 * Function           : HbTmrHdlHbTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine handle Heart Beat timer expiry. Sends the 
 *                      HeartBeat message after HB Tmr expiry and restarts 
 *                      the timer.  
 ******************************************************************************/
VOID
HbTmrHdlHbTmrExp (VOID *pArg)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrHdlHbTmrExp: ENTRY \r\n");
    UNUSED_PARAM (pArg);
    if (HbFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbTmrHdlHbTmrExp: "
                "HbFormAndSendMsg -- Failure !!!\r\n");
    }
    if (HB_GET_HB_INTERVAL () != HB_MIN_INTERVAL)
    {
        if (HbTmrRestartTimer
            (HB_HEARTBEAT_TIMER, HB_GET_HB_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "HbTmrHdlHbTmrExp: "
                    "Heart Beat re-start timer Failure !!!\r\n");
        }
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrHdlHbTmrExp: EXIT \r\n");
}

/******************************************************************************
 * Function           : HbTmrHdlPeerDeadTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine handle PeerDead timer expiry. Sets the current
 *                      node in ACTIVE state and declares PEER DOWN.  
 ******************************************************************************/
VOID
HbTmrHdlPeerDeadTmrExp (VOID *pArg)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrHdlPeerDeadTmrExp: ENTRY \r\n");
    UNUSED_PARAM (pArg);

    /* If HBs are received at NP layer, gu1KeepAliveFlag is set TRUE and 
     * the PEER_DOWN event is prevented and peer dead timer is restarted ,
     * except for the following case : HB packets are received but they are 
     * ignored on comparing the Software version. This will happen during
     * Maintenance mode and ISSU mode is in-compatible.
     * In case of a node which is forced to come up as STANDBY, PEER_DOWN event 
     * is prevented even if HBs are not received. The node will be maintained in
     * INIT state, until HB from an ACTIVE node with same software version 
     * is received or until the HB timer is restarted for
     * HB_STANDBY_MAX_TMR_RESTART times */

    if (((gu1KeepAliveFlag == RM_TRUE) &&
         (((IssuGetIssuMode () != ISSU_INCOMPATIBLE_MODE) ||
           (IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_DISABLE) ||
           (MEMCMP (gHbInfo.au1IssVersion, IssGetSoftwareVersion (),
                    ISS_STR_LEN) == 0)))) ||
        ((IssGetIssuNodeRole () == ISSU_NODE_ROLE_STANDBY) &&
         (HB_GET_NODE_STATE () == HB_RM_INIT) &&
         (gu1StandbyTmrRestartCount < HB_STANDBY_MAX_TMR_RESTART)))
    {
        if (HbTmrRestartTimer (HB_PEER_DEAD_TIMER,
                               HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Peer Dead restart timer Failure !!!\r\n");
        }
        gu1StandbyTmrRestartCount++;
        gu1KeepAliveFlag = RM_FALSE;
        return;
    }
    HB_RESET_FLAGS (HB_STANDBY_REBOOT);

    /* When the PEER node has gone down, STANDBY_SOFT_REBOOT
     * and STANDBY_PEER_UP flags used to inform the state to
     * the peer node can be reset. */
    HB_RESET_FLAGS (HB_STDBY_PROTOCOL_RESTART);
    HB_RESET_FLAGS (HB_STDBY_PEER_UP);

    /* Heart beat msgs are not rcvd for 4*HBInterval.
     * Peer down. If this node can participate in election (priority!=0),
     * change the state of this node to ACTIVE */
    if (HB_GET_SELF_NODE_PRIORITY () != 0)
    {
        HB_SET_ACTIVE_NODE_ID (HB_GET_SELF_NODE_ID ());
        HB_TRC1 (HB_NOTIFICATION_TRC, "ACTIVE NODE elected: %0x\n",
                 HB_GET_ACTIVE_NODE_ID ());
        HbStateMachine (HB_PEER_DOWN);
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrHdlPeerDeadTmrExp: EXIT \r\n");
}

/******************************************************************************
 * Function           : HbTmrHdlFswAckTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Handles the Force switchover ack timer expiry - 
 *                      Sends an event to RM core module to allow 
 *                      the static and dynamic sync. messages.
 ******************************************************************************/
VOID
HbTmrHdlFswAckTmrExp (VOID *pArg)
{
    tRmNotificationMsg  NotifMsg;

    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrHdlFswAckTmrExp: "
            "FSW Ack timer expired... ENTRY\n");

    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
    NotifMsg.u4NodeId = HB_GET_SELF_NODE_ID ();
    NotifMsg.u4State = RM_ACTIVE;
    NotifMsg.u4AppId = RM_APP_ID;
    NotifMsg.u4Operation = RM_NOTIF_SWITCHOVER;
    NotifMsg.u4Status = RM_FAILED;
    NotifMsg.u4Error = RM_NONE;
    UtlGetTimeStr (NotifMsg.ac1DateTime);
    SPRINTF (NotifMsg.ac1ErrorStr,
             "Force-Switchover ACK is not received from standby in %d milli secs.",
             HB_MAX_FSW_ACK_TIME);
    RmApiTrapSendNotifications (&NotifMsg);
    HB_TRC (HB_INIT_SHUT_TRC, "HbTmrHdlFswAckTmrExp: "
            "FSW Ack timer expired... EXIT\n");
    UNUSED_PARAM (pArg);
}

/******************************************************************************
 * Function           : HbResumeProtocolOperation
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : This routine is used to resume RM protocol operations 
 *                      once initialization of other modules is completed.
 ******************************************************************************/
INT4
HbResumeProtocolOperation (VOID)
{
    tRegPeriodicTxInfo  RegnInfo;
    HB_TRC (HB_INIT_SHUT_TRC, "HbResumeProtocolOperation: ENTRY\r\n");

    MEMSET (&RegnInfo, 0, sizeof (tRegPeriodicTxInfo));

    /* Start the Heart Beat Timer */
    CfaDeRegisterPeriodicPktTx (CFA_APP_RM);
    if (HbTmrStartTimer (HB_HEARTBEAT_TIMER,
                         HB_GET_HB_INTERVAL ()) != HB_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! Heart Beat start timer Failure !!!\n");
        return HB_FAILURE;
    }

    /* Initial HB message to detect the peer is rebooted */
    if (HbFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbResumeProtocolOperation: "
                "HbFormAndSendMsg - Failure !!!\r\n");
    }
    /* Start the Peer Dead Timer */
    if (HbTmrStartTimer (HB_PEER_DEAD_TIMER,
                         HB_GET_PEER_DEAD_INTERVAL ()) != HB_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Peer Dead start timer Failure !!!\r\n");
        return HB_FAILURE;
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbResumeProtocolOperation: EXIT\r\n");
    return HB_SUCCESS;
}

#endif /* RM_WANTED */

#ifdef ICCH_WANTED
/******************************************************************************
 * Function           : HbIcchResumeProtocolOperation
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : This routine is used to resume RM protocol operations 
 *                      once initialization of other modules is completed.
 ******************************************************************************/
INT4
HbIcchResumeProtocolOperation (VOID)
{
    tMacAddr            au1SrcMac;
    UINT4               u4TotalInst = 0;
    UINT4               u4IcclIfIndex = 0;
    UINT2               u2IcchInstanceIndex = 0;
    INT4                i4SockFd = 0;

    MEMSET (&au1SrcMac, 0, sizeof (tMacAddr));

    HB_TRC (HB_INIT_SHUT_TRC, "HbResumeProtocolOperation: ENTRY\r\n");

    /* Initialize self node information */
    gHbInfo.u4IcchSelfNode = IcchApiGetIcclIpAddr (ICCH_DEFAULT_INST);
    gHbInfo.u4IcchSelfNodeMask = IcchApiGetIcclIpMask (ICCH_DEFAULT_INST);

    /* Initializing SysMac info with system mac-address retirieved from CFA */
    CfaGetSysMacAddress (au1SrcMac);
    MEMCPY (gHbInfo.au1SysMac, au1SrcMac, MAC_ADDR_LEN);

    u4TotalInst = IcchApiGetTotalNumOfInst ();

    /*Create ICCL interface for hb packet exchange on ICCL */
    for (u2IcchInstanceIndex = 0; u2IcchInstanceIndex < (UINT2) u4TotalInst;
         u2IcchInstanceIndex++)
    {
        if (CfaCreateIcchIPInterface (u2IcchInstanceIndex) == CFA_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "HbIcchResumeProtocolOperation: Failed to create ICCH "
                    "IP interface !!!\r\n");
            return HB_FAILURE;
        }
    }
#ifndef NPAPI_WANTED
    HbRawIcchSockInit ();
#endif

    IcchApiTcpSrvSockInit (&i4SockFd);

    HB_ICCH_TCP_SRV_SOCK_FD () = i4SockFd;

    IcchGetIcclIfIndex (&u4IcclIfIndex);
    /* Initialize properties of ICCL port channel */
    HbLaIcchInitProperties (u4IcclIfIndex);

    if (VlanApiSetIcchPortProperties () != VLAN_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbIcchResumeProtocolOperation: VlanApiSetIcchPortProperties "
                "Failure !!!\r\n");
        return HB_FAILURE;
    }

    if (IcchApiSyslogRegister () != ICCH_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "ICCH Registration with syslog failed\r\n");
    }
    if (HbLaGetMCLAGSystemStatus () == HB_MCLAG_ENABLED)
    {
        HbIcchInitiateHBMsg ();
    }
    gu1HbIcchInitComplete = OSIX_TRUE;

    if (HbAstDisableStpOnPort (u4IcclIfIndex) != OSIX_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!!HbIcchResumeProtocolOperation: Disabling STP on port Failed!!!\n");
        return HB_FAILURE;
    }

    if (HB_SUCCESS != (HbLaDisableLldpOnIcclPorts (u4IcclIfIndex)))
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbIcchResumeProtocolOperation: Disabling LLDP on port is "
                "Failed!!!\r\n");
        return HB_FAILURE;
    }

    HB_TRC (HB_INIT_SHUT_TRC, "HbResumeProtocolOperation: EXIT\r\n");
    return HB_SUCCESS;
}

/******************************************************************************
 * Function           : HbIcchTmrHdlHbTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine handle Heart Beat timer expiry for ICCH module. 
 *                      Sends the HeartBeat message after HB Timer expiry and 
 *                      restarts the timer.  
 ******************************************************************************/
VOID
HbIcchTmrHdlHbTmrExp (VOID *pArg)
{
    UINT4               u4IcclIfIndex = 0;

    IcchGetIcclIfIndex (&u4IcclIfIndex);
    HB_TRC (HB_INIT_SHUT_TRC, "HbIcchTmrHdlHbTmrExp: ENTRY \r\n");
    UNUSED_PARAM (pArg);
    if (HbIcchFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbIcchTmrHdlHbTmrExp: "
                "HbFormAndSendMsg -- Failure !!!\n");
    }

#ifdef NPAPI_WANTED
    if (u4HbTmrCount == 0)
    {
        FsHwGetStat (u4IcclIfIndex, NP_STAT_DOT3_OUT_PAUSE_FRAMES,
                     &gHbInfo.u4HwOutPauseFrames);
        FsHwGetStat (u4IcclIfIndex, NP_STAT_DOT3_IN_PAUSE_FRAMES,
                     &gHbInfo.u4HwInPauseFrame);
        FsHwGetStat (u4IcclIfIndex, NP_STAT_IF_IN_OCTETS,
                     &gHbInfo.u4HwInOctets);
    }
    u4HbTmrCount++;
    if (u4HbTmrCount == HB_GET_PEER_DEAD_INT_MULTIPLIER () - 1)
    {
        u4HbTmrCount = 0;
    }
#endif
    if (HbTmrRestartTimer
        (HB_ICCH_HEARTBEAT_TIMER, HB_GET_HB_INTERVAL ()) == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbIcchTmrHdlHbTmrExp: "
                "Heart Beat re-start timer Failure !!!\n");
    }

    HB_TRC (HB_INIT_SHUT_TRC, "HbIcchTmrHdlHbTmrExp: EXIT \r\n");
}

/******************************************************************************
 * Function           : HbIcchTmrHdlPeerDeadTmrExp
 * Input(s)           : pArg - Argument
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine handle PeerDead timer expiry. Sets the current
 *                      node in MASTER state and declares PEER DOWN.
 ******************************************************************************/
VOID
HbIcchTmrHdlPeerDeadTmrExp (VOID *pArg)
{
    tUtlInAddr          HbNodeId;
    UINT4               u4IcclIfIndex = 0;
    UINT4               u4HwInPauseFrame = 0;
    UINT4               u4HwOutPauseFrames = 0;
    UINT4               u4HwInOctets = 0;
    UINT1               u1OperStatus = 0;

    MEMSET (&HbNodeId, 0, sizeof (tUtlInAddr));

    IcchGetIcclIfIndex (&u4IcclIfIndex);
    HB_TRC (HB_INIT_SHUT_TRC, "HbIcchTmrHdlPeerDeadTmrExp: ENTRY \r\n");
    UNUSED_PARAM (pArg);

    CfaGetIfOperStatus (u4IcclIfIndex, &u1OperStatus);
#ifdef NPAPI_WANTED
    FsHwGetStat (u4IcclIfIndex, NP_STAT_DOT3_IN_PAUSE_FRAMES,
                 &u4HwInPauseFrame);
    FsHwGetStat (u4IcclIfIndex, NP_STAT_DOT3_OUT_PAUSE_FRAMES,
                 &u4HwOutPauseFrames);
    FsHwGetStat (u4IcclIfIndex, NP_STAT_IF_IN_OCTETS, &u4HwInOctets);
#endif
    if ((((gHbInfo.u4HwInPauseFrame < u4HwInPauseFrame) ||
          (gHbInfo.u4HwOutPauseFrames < u4HwOutPauseFrames) ||
          (gHbInfo.u4HwInOctets < u4HwInOctets))) &&
        (HB_ICCH_GET_PEER_NODE_ID () != 0) && (u1OperStatus == CFA_IF_UP))
    {
        gu1IcchKeepAliveFlag = ICCH_TRUE;
        u4HbTolerance = 0;
        if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                               HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Peer Dead restart timer Failure !!!\n");
        }
        gu1IcchKeepAliveFlag = ICCH_FALSE;
        return;
    }

    if ((((gHbInfo.u4HwInPauseFrame == u4HwInPauseFrame) ||
          (gHbInfo.u4HwOutPauseFrames == u4HwOutPauseFrames) ||
          ((gHbInfo.u4HwInOctets == u4HwInOctets))) &&
         (u1OperStatus == CFA_IF_UP)) &&
        (HB_ICCH_GET_PEER_NODE_ID () != 0) &&
        (u4HbTolerance < HB_GET_PEER_DEAD_INT_MULTIPLIER ()))
    {
        if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                               HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Peer Dead restart timer Failure !!!\n");
        }
        u4HbTolerance++;
        return;
    }

    if (u4HbTolerance > (HB_GET_PEER_DEAD_INT_MULTIPLIER ()))
    {
        u4HbTolerance = 0;
    }
    if (gu1IcchKeepAliveFlag == ICCH_TRUE)
    {
        if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                               HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Peer Dead restart timer Failure !!!\n");
        }
        gu1IcchKeepAliveFlag = ICCH_FALSE;
        return;
    }

    /* Heart beat msgs are not rcvd for 4*HBInterval.
     * Peer down. If this node can participate in election,
     * change the state of this node to MASTER */

    HB_ICCH_SET_MASTER_NODE_ID (HB_ICCH_GET_SELF_NODE_ID ());
    HbNodeId.u4Addr = OSIX_NTOHL (HB_ICCH_GET_MASTER_NODE_ID ());
    HB_TRC1 (HB_NOTIFICATION_TRC, "MASTER node is elected: %s\r\n",
             INET_NTOA (HbNodeId));
    HbIcchStateMachine (HB_ICCH_PEER_DOWN);
    HB_TRC (HB_INIT_SHUT_TRC, "HbIcchTmrHdlPeerDeadTmrExp: EXIT \r\n");
}

#endif /* ICCH_WANTED */
