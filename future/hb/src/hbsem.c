/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbsem.c,v 1.5 2016/07/30 10:41:31 siva Exp $
*
* Description: Heart Beat State event machine.
*********************************************************************/
#ifndef _HBSEM_C
#define _HBSEM_C
#include "hbincs.h"
#include "hbsem.h"

/******************************************************************************
 * Function           : HbStateMachine
 * Input(s)           : u1Event - Event 
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure indexes into the state event table using 
 *                      the event and the state to get an interger. This integer
 *                      when indexed into an array of function pointers gives 
 *                      the action routine to be called. 
 ******************************************************************************/
VOID
HbStateMachine (UINT1 u1Event)
{
    UINT1               u1ActionProcIdx;
    UINT1               u1CurState = 0;

    u1CurState = (UINT1) HB_GET_NODE_STATE ();

    if (u1CurState < HB_RM_MAX_STATES)
    {
        HB_TRC2 (HB_EVENT_TRC, "FSM Event: %s  State: %s\n",
                    au1HbRmEvntStr[u1Event], au1HbRmStateStr[u1CurState]);
    }
    /* Get the index of the action procedure */
    u1ActionProcIdx = au1HbRmSem[u1Event][HB_GET_NODE_STATE ()];

    /* call the corresponding function pointer */
    (*HbRmActionProc[u1ActionProcIdx]) ();
    return;
}

/******************************************************************************
 * Function           : HbRmSem1WayRcvdInInitState 
 * Input(s)           : VOID
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when the peer node has not 
 *                      learnt about this node, but this node has learnt 
 *                      about peer (i.e. HeartBeat is one-way rcvd). 
 ******************************************************************************/
VOID
HbRmSem1WayRcvdInInitState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbRmSem1WayRcvdInInitState\n");
}

/******************************************************************************
 * Function           : HbRmSem2WayRcvdInInitState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when the peer node has
 *                      learnt about this node and this node has also learnt 
 *                      about peer node (i.e., HeartBeat msg is 2-way rcvd). 
 ******************************************************************************/
VOID
HbRmSem2WayRcvdInInitState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbRmSem2WayRcvdInInitState\n");

    HbRmStartElection ();
}

/******************************************************************************
 * Function           : HbRmSem2WayRcvdInActvState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a heart-beat msg is 
 *                      rcvd in 2-way rcvd state.  
 ******************************************************************************/
VOID
HbRmSem2WayRcvdInActvState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbRmSem2WayRcvdInActvState\n");
}

/******************************************************************************
 * Function           : HbRmSemActvElctdInInitState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Active node is already elected and this node has to
 *                      update its ACTIVE node list and change its state to
 *                      STANDBY.
 ******************************************************************************/
VOID
HbRmSemActvElctdInInitState (VOID)
{
    UINT1               u1State = HB_RM_MAX_STATES;

    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbRmSemActvElctdInInitState\n");

    /* Active node is already elected and this is the second node
     * to come up. Do not start election again. ACTIVE NodeId is already 
     * updated in global data struct and now set the state accordingly. */
    if (HB_GET_ACTIVE_NODE_ID () == HB_GET_SELF_NODE_ID ())
    {
        u1State = HB_RM_ACTIVE;
    }
    else if (HB_GET_ACTIVE_NODE_ID () == HB_GET_PEER_NODE_ID ())
    {
        u1State = HB_RM_STANDBY;
    }

    if ((u1State == HB_RM_ACTIVE) || (u1State == HB_RM_STANDBY))
    {
        HbRmUpdateState (u1State);
    }
}

/******************************************************************************
 * Function           : HbRmSemPeerDownRcvdInInitState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when Peer dead timer expires
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
HbRmSemPeerDownRcvdInInitState (VOID)
{
    HbRmUpdateState (HB_RM_ACTIVE);
    HB_TRC (HB_CRITICAL_TRC, "HbRmSemPeerDownRcvdInInitState: ACTIVE notified\n");
}

/******************************************************************************
 * Function           : HbRmSemPeerDownRcvdInActiveState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when ACTIVE node 
 *                      Peer dead timer expires 
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
HbRmSemPeerDownRcvdInActiveState (VOID)
{
    HB_TRC (HB_CRITICAL_TRC, "HbRmSemPeerDownRcvdInActiveState: Standby is down\n");
    if (HB_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_DOWN);
    }
    /* Set the peer node id to zero */
    HB_SET_PEER_NODE_ID (0);
   /* In case of standby doesnt know about the peer down in active node,
    * sending HB with peer id with '0' will ensure the stanby to take
    * necessary action on Receiving this message 
    */
    if (HbFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_CRITICAL_TRC,
                "HbRmSemPeerDownRcvdInActiveState: "
                "Sending HBMsg to peer failed !!!\n");
    }
}

/******************************************************************************
 * Function           : HbRmSemPeerDownRcvdInStandbyState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when Peer dead timer expires
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
HbRmSemPeerDownRcvdInStandbyState (VOID)
{
#ifdef RM_WANTED
    tRmHbMsg            RmHbMsg;

    if (HB_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_DOWN);
    }
    /* This peer node id set to zero should be present before 
     * returning in curr node = transition in progress and 
     * prev state is standby OR INIT check, otherwise after completing 
     * pending operation at RM core module, peer down 
     * will not be detected and GO_ACTIVE will not be sent to 
     * RM core module*/
    HB_SET_PEER_NODE_ID (0);

    /* Until we get a event from RM core module for RX buffer 
     * messages are processed, RM SEM will be in 
     * HB_RM_TRANSITION_IN_PROGRESS state */
    HB_SET_PREV_NODE_STATE (HB_RM_STANDBY);
    HB_SET_NODE_STATE (HB_RM_TRANSITION_IN_PROGRESS);
    HB_SET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
    RmHbMsg.u4Evt = RM_PROC_PENDING_SYNC_MSG;
    RmApiSendHbEvtToRm (&RmHbMsg);
    HB_TRC (HB_CRITICAL_TRC, "RmSemPeerDownRcvdInStandbyState: Active node dead, "
               "Pending RX buffer processing event triggered\n");
#endif
}

/******************************************************************************
 * Function           : HbRmSemPeerDownRcvdInTrgsInPrgs
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function is invoked when Peer dead timer expires
 *                      i.e., Heart Beat msg is not rcvd for a duration of 
 *                      peer dead interval. 
 ******************************************************************************/
VOID
HbRmSemPeerDownRcvdInTrgsInPrgs (VOID)
{
#ifdef RM_WANTED
    tRmHbMsg            RmHbMsg;

    if (HB_GET_PREV_NODE_STATE () == HB_RM_ACTIVE)
    {
        HB_RESET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
        if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
        {
            /* Standby node dead before getting the FSW ACK. */
            HB_RESET_FLAGS (HB_FORCE_SWITCHOVER);
            HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
            /* FSW ACK timer should be greater than the peer dead timer value, 
             * so FSW ACK should be stopped here. */
            HbTmrStopTimer (HB_FSW_ACK_TIMER);
            RmHbMsg.u4Evt = RM_FSW_FAILED_IN_ACTIVE;
            RmApiSendHbEvtToRm (&RmHbMsg);
            HB_TRC (HB_CRITICAL_TRC,
                       "HbRmSemPeerDownRcvdInTrgsInPrgs: Standby node dead "
                       "while waiting for FSW ACK HB message\n");
        }
        else
        {
            /* Failover case */
            /* Standby node dead, ACTIVE side RM core module 
             * does not have the RX buffer to process. 
             * So, special action is not required here.*/
            HB_TRC (HB_CRITICAL_TRC,
                       "HbRmSemPeerDownRcvdInTrgsInPrgs: Standby node dead\n");
        }
    }
    else if (HB_GET_PREV_NODE_STATE () == HB_RM_STANDBY)
    {
        if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
        {
            /* Active node dead before getting the FSW ACK. */
            HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
            HB_TRC (HB_CRITICAL_TRC,
                       "HbRmSemPeerDownRcvdInTrgsInPrgs: Active node dead "
                       "before sending the FSW ACK HB message\n");
        }
        else                    /* Failover case */
        {
            /* Failover case.
             * Ignore this peer down as RM core module is processing the 
             * RX buffer messages*/
            HB_TRC (HB_CRITICAL_TRC,
                       "HbRmSemPeerDownRcvdInTrgsInPrgs: Active node dead "
                       "while RX buffer processing at STANDBY\n");
        }
    }

    if (HB_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_DOWN);
    }
    /* This peer node id set to zero should be present before 
     * returning in curr node = transition in progress and 
     * prev state is standby OR INIT check, otherwise after completing 
     * pending operation at RM core module, peer down 
     * will not be detected and GO_ACTIVE will not be sent to 
     * RM core module*/
    HB_SET_PEER_NODE_ID (0);
    /* 1. RM core module is processing the RX buffer messages now, 
     * so don't give GO_ACTIVE, RM core module notifies once the RX buffer 
     * messages are processed and then give GO_ACTIVE indication 
     * from RM HB module.
     * 2. Prev state = RM_INIT and curr state = HB_RM_TRANSITION_IN_PROGRESS 
     * is applicable only for the connection lost and restored case.
     * So, while RM core task doing the shut and start 
     * operation peer down is received. Just ignore this event now, 
     * after completing the shut and start operation, 
     * GO_ACTIVE will be given.
     * */
    if ((HB_GET_PREV_NODE_STATE () == HB_RM_STANDBY) ||
        (HB_GET_PREV_NODE_STATE () == HB_RM_INIT))
    {
        /* 1. RM core module is processing the RX buffer messages now, 
         * so don't give GO_ACTIVE, RM core module notifies once the RX buffer 
         * messages are processed and then give GO_ACTIVE indication 
         * from RM HB module.
         * 2. Prev state = HB_RM_INIT and curr state = HB_RM_TRANSITION_IN_PROGRESS 
         * is applicable only for the connection lost and restored case.
         * So, while RM core task doing the shut and start 
         * operation peer down is received. Just ignore this event now, 
         * after completing the shut and start operation, 
         * GO_ACTIVE will be given.
         * */
        return;
    }
    HbRmUpdateState (HB_RM_ACTIVE);
    HB_TRC (HB_CRITICAL_TRC, "HbRmSemPeerDownRcvdInTrgsInPrgs: ACTIVE indicated\n");
#endif
}

/******************************************************************************
 * Function           : HbRmSemFSWRcvdInActiveState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a peer node does not 
 *                      receive the FSW ACK and here FSW ACK retransmission 
 *                      takes place.
 ******************************************************************************/
VOID
HbRmSemFSWRcvdInActiveState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbRmSemFSWRcvdInActiveState: ENTRY\n");
    if (HbSendMsgToPeer (HB_FORCE_SWITCHOVER_ACK) == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                   "!!! Sending fore-switchover-ack to peer failed !!!\n");
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbRmSemFSWRcvdInActiveState: EXIT\n");
}

/******************************************************************************
 * Function           : HbRmSemFSWRcvdInStandbyState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a peer node is desired
 *                      to go standby.
 ******************************************************************************/
VOID
HbRmSemFSWRcvdInStandbyState (VOID)
{
#ifdef RM_WANTED
    tRmHbMsg            RmHbMsg;

    HB_TRC (HB_INIT_SHUT_TRC, "RmSemFSWRcvdInStandbyState: ENTRY\n");
    if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_NOT_OCCURED)
    {
        HB_SET_PREV_NODE_STATE (HB_GET_NODE_STATE ());
        HB_SET_NODE_STATE (HB_RM_TRANSITION_IN_PROGRESS);
        HB_SET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
        HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_OCCURED;
        RmHbMsg.u4Evt = RM_PROC_PENDING_SYNC_MSG;
        RmApiSendHbEvtToRm (&RmHbMsg);
        HB_TRC (HB_EVENT_TRC, "RmSemFSWRcvdInStandbyState: STANDBY side "
                   "pending RX buffer processing event triggered\n");
    }
    HB_TRC (HB_INIT_SHUT_TRC, "RmSemFSWRcvdInStandbyState: EXIT\n");
#endif
}

/******************************************************************************
 * Function           : HbRmSemFSWAckRcvdInTrgsInPrgsState 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a active 
 *                      to go to standby.
 ******************************************************************************/
VOID
HbRmSemFSWAckRcvdInTrgsInPrgsState (VOID)
{
    /* Since force switchover acknowledgement is recieved, reset 
     * the flag.
     */
    HB_TRC (HB_INIT_SHUT_TRC, "HbRmSemFSWAckRcvdInTrgsInPrgsState: ENTRY\n");
    if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
    {
        HB_RESET_FLAGS (HB_FORCE_SWITCHOVER);
        HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
        HB_RESET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
        HbTmrStopTimer (HB_FSW_ACK_TIMER);
        HB_SET_ACTIVE_NODE_ID (HB_GET_PEER_NODE_ID ());
        HbRmUpdateState (HB_RM_STANDBY);
        HB_TRC (HB_CRITICAL_TRC, "HbRmSemFSWAckRcvdInTrgsInPrgsState: "
                   "FSW ACK is received from peer\n");
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbRmSemFSWAckRcvdInTrgsInPrgsState: EXIT\n");
}

/******************************************************************************
 * Function           : HbRmUpdateState
 * Input(s)           : State.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This function updates the current state 
 *                      of the node and does the shut and start 
 *                      when init to standby transition desired from active.
 ******************************************************************************/
VOID
HbRmUpdateState (UINT1 u1State)
{
    UINT1               u1PrevState = 0;
    UINT1               u1Event = HB_RM_INIT;
    UINT1               u1CurState = 0;
#ifdef RM_WANTED
    tRmHbMsg            RmHbMsg;
#endif
    if (u1State == HB_GET_NODE_STATE ())
    {
        /* no change in node state */
        return;
    }

    HB_TRC1 (HB_CRITICAL_TRC, "HbRmUpdateState = %s\n",(u1State == HB_RM_ACTIVE)?"ACTIVE":"STANDBY");

    if ((HB_GET_PREV_NODE_STATE () == HB_RM_ACTIVE) &&
        (HB_GET_NODE_STATE () == RM_INIT) && (u1State == HB_RM_STANDBY))
    {
#ifdef RM_WANTED
        HB_SET_PREV_NODE_STATE (HB_GET_NODE_STATE ());
        HB_SET_NODE_STATE (HB_RM_TRANSITION_IN_PROGRESS);
        HB_SET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
        RmHbMsg.u4Evt = RM_COMM_LOST_AND_RESTORED;
        RmApiSendHbEvtToRm (&RmHbMsg);
#endif
        return;
    }
    else if ((HB_GET_PREV_NODE_STATE () == HB_RM_ACTIVE) &&
             (HB_GET_NODE_STATE () == HB_RM_INIT) &&
             (u1State == HB_RM_ACTIVE) &&
             (HB_GET_PREV_NOTIF_NODE_STATE () == HB_RM_ACTIVE))
    {
        /* RM level state transition, 
         * no need to update this status to RM enabled protolcols as their 
         * status is ACTIVE */
        u1PrevState = (UINT1) HB_GET_NODE_STATE ();
        HB_SET_PREV_NODE_STATE (u1PrevState);
        HB_SET_NODE_STATE (u1State);
        HB_TRC (HB_CRITICAL_TRC, " RM INIT->ACTIVE transition\n");
        return;
    }
    u1PrevState = (UINT1) HB_GET_NODE_STATE ();
    HB_SET_PREV_NODE_STATE (u1PrevState);
    HB_SET_NODE_STATE (u1State);

    if (HB_GET_PREV_NOTIF_NODE_STATE () == HB_GET_NODE_STATE ())
    {
        /* No need to notify the same status again to RM core module */
        return;
    }

    if (HB_GET_NODE_STATE () == HB_RM_STANDBY)
    {
        u1Event = GO_STANDBY;
    }
    if (HB_GET_NODE_STATE () == HB_RM_ACTIVE)
    {
        u1Event = GO_ACTIVE;
    }

#ifdef RM_WANTED
    if ((u1Event == GO_ACTIVE) || (u1Event == GO_STANDBY))
    {
        HB_SET_PREV_NOTIF_NODE_STATE (HB_GET_NODE_STATE ());
        RmHbMsg.u4Evt = u1Event;
        RmApiSendHbEvtToRm (&RmHbMsg);
    }
#endif

    u1CurState = (UINT1) HB_GET_NODE_STATE ();

#ifdef RM_WANTED
    if (u1CurState < HB_RM_MAX_STATES)
    {
        HB_TRC1 (HB_CRITICAL_TRC, "FSM State Changed to: %s\n",
                    au1HbRmStateStr[u1CurState]);
    }
#endif
}

/******************************************************************************
 * Function           : HbRmSemEvntIgnore 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when an invalid event occurs.
 ******************************************************************************/
VOID
HbRmSemEvntIgnore (VOID)
{
    UINT1               u1CurState = 0;

    u1CurState = (UINT1) HB_GET_NODE_STATE ();
#ifdef RM_WANTED
    if (u1CurState < HB_RM_MAX_STATES)
    {
        HB_TRC1 (HB_EVENT_TRC, "Unexpected event to RM SEM. State:%s\n",
                    au1HbRmStateStr[u1CurState]);
    }
#endif
    return;
}

/******************************************************************************
 * Function           : HbRmSemPriorityChg 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked whenever the priority of the
 *                      node changes.
 ******************************************************************************/
VOID
HbRmSemPriorityChg (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "Entered HbRmSemPriorityChg\n");
    HbRmStartElection ();
}

/******************************************************************************
 * Function           : HbRmStartElection 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure performs the election to elect ACTIVE
 *                      and STANDBY node.
 ******************************************************************************/
VOID
HbRmStartElection (VOID)
{
    UINT1               u1NodeState = HB_RM_MAX_STATES;

    HB_TRC (HB_INIT_SHUT_TRC, "HbRmStartElection: ENTRY\n");

    if (HB_GET_SELF_NODE_PRIORITY () > HB_GET_PEER_NODE_PRIORITY ())
    {
        HB_SET_ACTIVE_NODE_ID (HB_GET_SELF_NODE_ID ());
        u1NodeState = HB_RM_ACTIVE;
        HB_TRC1 (HB_CRITICAL_TRC, "ACTIVE NODE elected: %0x\n",
                    HB_GET_ACTIVE_NODE_ID ());
    }
    else if (HB_GET_SELF_NODE_PRIORITY () < HB_GET_PEER_NODE_PRIORITY ())
    {
        u1NodeState = HB_RM_STANDBY;
    }
    else
        /* Priorities are equal. In the event of tie, NodeId is used for election */
    {
        /* Elect the node with higher IP address as the ACTIVE node */
        if (HB_GET_SELF_NODE_ID () > HB_GET_PEER_NODE_ID ())
        {
            HB_SET_ACTIVE_NODE_ID (HB_GET_SELF_NODE_ID ());
            u1NodeState = HB_RM_ACTIVE;
            HB_TRC1 (HB_CRITICAL_TRC, "ACTIVE NODE elected: %0x\n",
                        HB_GET_ACTIVE_NODE_ID ());
        }
        else if (HB_GET_SELF_NODE_ID () < HB_GET_PEER_NODE_ID ())
        {
            u1NodeState = HB_RM_STANDBY;
        }
    }
    if (HB_GET_ACTIVE_NODE_ID () == 0)
    {
        HB_TRC (HB_CRITICAL_TRC, "HbRmStartElection: "
                   "Active node id is zero...!!! \r\n");
        return;
    }
    if (u1NodeState == HB_RM_STANDBY)
    {
        if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
        {
            HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_UP);
        }
        HbRmUpdateState (u1NodeState);
    }
    else                        /* NODE STATE is ACTIVE */
    {
        HbRmUpdateState (u1NodeState);

        if (RM_HB_MODE == ISS_RM_HB_MODE_INTERNAL)
        {
            HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_UP);
        }
    }
    HB_TRC (HB_INIT_SHUT_TRC, "HbRmStartElection: EXIT\n");
}
/******************************************************************************
 * Function           : HbRmSem1WayRcvdInStandbyState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This procedure is invoked when a 1Way event recived 
 *                      in standby node                      
 ******************************************************************************/
VOID
HbRmSem1WayRcvdInStandbyState (VOID)
{
    HB_TRC (HB_INIT_SHUT_TRC, "HbRmSemFSWAckRcvdInTrgsInPrgsState: ENTRY\n");
    /*Active Node has lost peer node details, but standby knows about Active Node.
     *Standby should go for reboot to avoid sync problems 
     */
    if ((IssuGetMaintModeOperation() != OSIX_TRUE ) &&
                  (RmGetNodeState() == RM_STANDBY))
    { 
        HB_TRC (HB_CRITICAL_TRC,
                "!!! HB . 1way received  in standby Node, Active node lost peerID"
                " hence rebooting the node\n");
        RmApiSystemRestart ();
    } 

    HB_TRC (HB_INIT_SHUT_TRC, "HbRmSem1WayRcvdInStandbyState: EXIT\n");
 }
#endif
