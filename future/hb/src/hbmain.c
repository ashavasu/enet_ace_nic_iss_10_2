/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbmain.c,v 1.43.2.1 2018/03/16 13:43:24 siva Exp $
*
* Description: Heart Beat functionality .
***********************************************************************/
#ifndef _HBMAIN_C_
#define _HBMAIN_C_

#include "hbincs.h"
#include "hbglob.h"
#ifdef NPAPI_WANTED
#include "l2iwf.h"
#include "nputil.h"
#include "cfanp.h"
#endif
#ifdef ICCH_WANTED
#include "fssocket.h"
#endif /* ICCH_WANTED */
/******************************************************************************
 * Function           : HbTaskMain
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : HB Task main routine to initialize HB task parameters, 
 *                      main loop processing.
 ******************************************************************************/
VOID
HbTaskMain (INT1 *pArg)
{
    UINT4               u4Event = 0;
#ifdef RM_WANTED
    UINT4               u4StackingModel = 0;
#endif /* RM_WANTED */
    UNUSED_PARAM (pArg);

    if (OsixTskIdSelf (&HB_TASK_ID) != OSIX_SUCCESS)
    {
        HB_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (HbTaskInit () == HB_FAILURE)
    {
        /* Indicate the status of initialization to main routine */
        HB_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    HB_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef RM_WANTED
    if (HbWaitForOtherModulesBootUp () != HB_SUCCESS)
    {
        HbCloseSocket (HB_RAW_SOCK_FD ());
        OsixSemDel (HB_PROTO_SEM ());
        HbTmrDeInit ();
        return;
    }

    u4StackingModel = (UINT4) ISS_GET_STACKING_MODEL ();
    if (u4StackingModel == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        HbPostSelfAttachEvent ();

        if (HbWaitForSelfAttachCompletion () != HB_SUCCESS)
        {
            HbCloseSocket (HB_RAW_SOCK_FD ());
            OsixSemDel (HB_PROTO_SEM ());
            HbTmrDeInit ();
            return;
        }

    }
    /* If RM runs over FSIP, SLI Task has to be initialized 
       for creating Sockets . Hence initializing RM TCP Socket
       after all modules are initialized */
    RmApiTcpSrvSockInit ();
#endif
#ifdef SNMP_2_WANTED
    /* Register MIBs if there are any */
    RegisterFSHB ();
#endif /* SNMP_2_WANTED */
    while (1)
    {
        HB_RECV_EVENT (HB_TASK_ID, HB_MODULE_EVENTS, OSIX_WAIT, &u4Event);

        HB_LOCK ();

#ifdef ICCH_WANTED
        if (u4Event & HB_ICCH_PKT_RCVD)
        {
            /* ICCH pkt rcvd in raw socket - 
             * processing the msgs are handled here */
            HbIcchProcessRcvEvt ();
            if (SelAddFd (gHbInfo.i4IcchRawSockFd, HbIcchRawPktRcvd) ==
                HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "Unable to add HB Raw socket with select -- Failure\r\n");
            }
        }

        if (u4Event & HB_ICCH_SYNC_MSG_PROCESSED)
        {
            /* ICCH core module sends this event after proc pending RX buffer */
            HbIcchHandleRxBufProcessedEvt ();
        }

        if (u4Event & HB_PKT_RCVD_ON_ICCL_PORT)
        {
            HbProcessMsgRcvdOnICCLPort ();
        }

        if (u4Event & HB_MCLAG_START_HB_MSG)
        {
            if (gu1HbIcchInitComplete == OSIX_TRUE)
            {
                /* HB module timers expiry handling */
                HbIcchInitiateHBMsg ();
            }
        }
        if (u4Event & HB_MCLAG_STOP_HB_MSG)
        {
            /* HB module timers expiry handling */
            HbIcchTerminateHBMsg ();
        }

        if (u4Event & HB_MCLAG_SHUTDOWN)
        {
            HbLaNotifyMclagShutdownComplete ();
        }

#endif /* ICCH_WANTED */
#ifdef RM_WANTED
        if (u4Event & HB_PKT_RCVD)
        {
            /* Rm/ICCH pkt rcvd in raw socket - 
             * processing the msgs are handled here */
            HbProcessRcvEvt ();
            if (SelAddFd (gHbInfo.i4RawSockFd, HbRawPktRcvd) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "Unable to add HB Raw socket with select -- Failure\r\n");
            }
        }

        if (u4Event & HB_CHG_TO_TRANS_IN_PRGS)
        {
            /* RM core module sends this event when FSW is given */
            HbChgNodeStateToTransInPrgs ();
        }
        if (u4Event & HB_ISSU_FORCE_STANDBY)
        {
            /* ISSU module sends this event */
            HbRmUpdateForceStandbyState ();
        }

        if (u4Event & HB_FSW_EVENT)
        {
            /* RM core module sends this event after txing pending tx buffer */
            HbHandleFSWEvent ();
        }

        if (u4Event & HB_SYNC_MSG_PROCESSED)
        {
            /* RM core module sends this event after proc pending RX buffer */
            HbHandleRxBufProcessedEvt ();
        }

        if (u4Event & HB_APP_INIT_COMPLETED)
        {
            /* After backplane communication lost and restored,
             * the election determined that this node to go STANDBY.
             * Now the shutdown and start of RED enabled modules done
             * Further it determines the node state based on the
             * peer existance i.e.
             * 1. GO_ACTIVE -> If peer is dead
             * 2. GO_STANDBY -> If peer is alive */
            HbHandleAppInitCompleted ();
        }
        if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
        {
            /* Rm/Icch pkt rcvd in Ethernet port - 
             * processing the msgs are handled here */
            if (u4Event & HB_PKT_RCVD_ON_ETH_PORT)
            {
                HbProcessMsgRcvdOnStkPort ();
            }
        }

#endif /* RM_WANTED */
        if (u4Event & HB_TMR_EXPIRY_EVENT)
        {
            /* HB module timers expiry handling */
            HbTmrExpHandler ();
        }

        HB_UNLOCK ();
    }
}

/******************************************************************************
 * Function           : HbTaskInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_FAILURE/HB_SUCCESS 
 * Action             : Initialises the HB task related variables
 ******************************************************************************/
UINT4
HbTaskInit (VOID)
{
    UINT4               u4RetVal = HB_SUCCESS;
#if (defined RM_WANTED)
    INT4                i4SockFd = HB_INV_SOCK_FD;
#endif

    /* Initialize the default value of the global struct */
    if (HbInit () == HB_FAILURE)
    {
        u4RetVal = HB_FAILURE;
    }

    /* Open a socket connection and set the socket options to 
     * communicate with peer node */

#ifdef RM_WANTED
    /* Heart beat messages will be transmiited over a socket only if RM 
       Stacking Interface is OOB . If Interface is an Inband ethernet port,
       Heart beat messages will be tranmitted as ethernet packets.
       So socket initializations for HB Messages  are done only if the
       stacking port is an oob port */
    if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_OOB)
    {
        if (u4RetVal != HB_FAILURE)
        {
            if (HbRawRmSockInit (&i4SockFd) == HB_FAILURE)
            {
                OsixSemDel (HB_PROTO_SEM ());
                u4RetVal = HB_FAILURE;
            }
            else
            {
                HB_RAW_SOCK_FD () = i4SockFd;
            }
        }
    }
#endif
    /* Create timers for HB */
    if (u4RetVal != HB_FAILURE)
    {
        if (HbTmrInit () == HB_FAILURE)
        {
            HbCloseSocket (HB_RAW_SOCK_FD ());
            HbCloseSocket (HB_ICCH_RAW_SOCK_FD ());
            OsixSemDel (HB_PROTO_SEM ());
            u4RetVal = HB_FAILURE;
        }
    }

    gHbInfo.u1StatsEnable = HB_STATS_ENABLE;
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbInit
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to initialize HB global data structure. 
 ******************************************************************************/
UINT4
HbInit (VOID)
{
#ifdef RM_WANTED
    tHbNodeInfo         SelfNodeInfo;
#endif
    tOsixTaskId         TempTskId;

    /* Before clearing the global info.
     * Store the HB task id in temporary variable
     * then move it to task id variable */
    TempTskId = gHbInfo.HbTaskId;

    MEMSET (&gHbInfo, 0, sizeof (tHbInfo));

#ifdef RM_WANTED
    gu4RmStackingInterfaceType = (UINT4) IssGetRMTypeFromNvRam ();
#endif
    gHbInfo.HbTaskId = TempTskId;
    gHbInfo.i4RawSockFd = HB_INV_SOCK_FD;
    gHbInfo.u4HbTrc = HB_DEFAULT_TRC;
    gHbInfo.u4HbTrcBkp = gHbInfo.u4HbTrc;
    gHbInfo.u4HbInterval = HB_DEFAULT_INTERVAL;
    gHbInfo.u4PeerDeadInterval = HB_DEFAULT_PEER_DEAD_INTERVAL;
    gHbInfo.u4PeerDeadIntMultiplier = HB_DEFAULT_PEER_DEAD_INT_MULTIPLIER;
#ifdef RM_WANTED
    gHbInfo.u4PrevNodeState = HB_RM_INIT;
    gHbInfo.u4NodeState = HB_RM_INIT;
    gHbInfo.u1PeerNodeState = HB_RM_INIT;
    /* Active node is yet to be identified */
    gHbInfo.u4ActiveNode = 0;

    gHbInfo.u4SelfNodePriority = HB_DEF_PRIORITY;
    gHbInfo.u4PeerNodePriority = HB_DEF_PRIORITY;
    /* Peer node Ip addr is not yet learnt */
    gHbInfo.u4PeerNode = 0;
    /* Make the FSW flag RM_FSW_NOT_OCCURED */
    gHbInfo.u1ForceSwitchOverFlag = RM_FSW_NOT_OCCURED;
#endif /* RM_WANTED */

    /* Initialize the statistics counters */
    HB_GET_MSG_TX_COUNT () = 0;
    HB_GET_MSG_TX_FCOUNT () = 0;
    HB_GET_MSG_RX_COUNT () = 0;
    HB_GET_MSG_RX_PCOUNT () = 0;
    HB_GET_MSG_RX_FCOUNT () = 0;

    gHbInfo.u4Flags = 0;
    /* In Control plane stacking Model,Heart Beat Messages will be 
       transmitted through connecting Ethernet Port .
       A separate  Queue is created to Handle Heart Beat Messages */
    if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        if (HB_CREATE_QUEUE ((UINT1 *) HB_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                             MAX_HB_CTRL_MSG_NODES,
                             &(HB_QUEUE_ID)) != OSIX_SUCCESS)
        {
            HB_TRC (HB_CRITICAL_TRC, "Creation of HB Q Failed in HbInit\r\n");
            return HB_FAILURE;
        }
    }
#ifdef ICCH_WANTED
    HB_ICCH_PEER_NEWLY_ARRIVED = OSIX_FALSE;
    gHbInfo.u4IcchFlags = 0;
    MEMSET (gHbInfo.au1SysMac, 0, MAC_ADDR_LEN);
    if (HB_CREATE_QUEUE ((UINT1 *) HB_ICCH_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                         MAX_HB_CTRL_MSG_NODES,
                         &(HB_ICCH_QUEUE_ID)) != OSIX_SUCCESS)
    {
        HB_TRC (HB_CRITICAL_TRC, "Creation of HB Q Failed in HbInit\r\n");
        return HB_FAILURE;
    }
#endif
    if (OsixCreateSem (HB_PROTO_SEM_NAME, 1, 0,
                       (tOsixSemId *) & (HB_PROTO_SEM ())) != OSIX_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HB Protocol Sem Creation failed in HbInit\n");
        return HB_FAILURE;
    }
#ifdef RM_WANTED
    /* If RM Stacking Interface type is oob, RM Interface read from issnvram
       should be used */
    if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_OOB)
    {
        /* Get self node ip addr and initialize in data struct */
        if (HbGetIfIpInfo
            ((UINT1 *) IssGetRmIfNameFromNvRam (), &SelfNodeInfo) != HB_FAILURE)
        {
            gHbInfo.u4SelfNode = SelfNodeInfo.u4IpAddr;
            gHbInfo.u4SelfNodeMask = SelfNodeInfo.u4NetMask;
        }
        else
        {
            OsixSemDel (HB_PROTO_SEM ());
            return HB_FAILURE;
        }
    }
    /* If RM Stacking Interface is Inband, RM Stacking Interface read from 
       NodeID should be used for communication. */

    /* IP Address and Subnet Mask to be used should be read from Nodeid file */
    else
    {
        gHbInfo.u4SelfNode = (UINT4) IssGetRMIpAddress ();
        gHbInfo.u4SelfNodeMask = (UINT4) IssGetRMSubnetMask ();

    }
#endif
    return HB_SUCCESS;
}

/******************************************************************************
 * Function           : HbWaitForSelfAttachCompletion 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : This routine is used to hold the HB protocol operations
 *                      till Self Attach is handled by MBSM
 ******************************************************************************/
INT4
HbWaitForSelfAttachCompletion (VOID)
{
    UINT4               u4Event = 0;

    while (1)
    {
        HB_RECV_EVENT (HB_TASK_ID, HB_RESUME_EVENT, OSIX_WAIT, &u4Event);

        if (u4Event & HB_RESUME_EVENT)
        {
            break;
        }
    }
    return HB_SUCCESS;
}

/******************************************************************************
 * Function           : HbInformOtherModulesBootUp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : This routine is used to inform HB module about the 
 *                      completion of other modules intialization.
 ******************************************************************************/
VOID
HbInformOtherModulesBootUp (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);
    if (HB_SEND_EVENT (HB_TASK_ID, HB_RESUME_EVENT) != OSIX_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "SendEvent failed in HbInformOtherModulesBootUp\r\n");
    }
    /* Indicate the status of initialization to main routine */
    return;
}

/******************************************************************************
 * Function           : HbPrependHbHdr
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to form RM header and prepend it to the 
 *                      RM msg.
 ******************************************************************************/
UINT4
HbPrependHbHdr (tHbMsg * pHbMsg, UINT4 u4Size)
{
    tHbHdr              HbHdr;
    UINT2               u2Cksum = 0;
    UINT4               u4RetVal = HB_SUCCESS;
    INT4                i4RetVal = CRU_FAILURE;

    /* From HB hdr */
    MEMSET (&HbHdr, 0, sizeof (tHbHdr));

    HbHdr.u2Version = OSIX_HTONS (HB_VERSION_NUM);
    HbHdr.u2Chksum = OSIX_HTONS (0);
    HbHdr.u4TotLen = OSIX_HTONL ((HB_HDR_LENGTH + u4Size));
    HbHdr.u4MsgType = OSIX_HTONS (0);    /* REQ/RESP - to be filled while 
                                         * implementing re-transmission */
    HbHdr.u4SrcEntId = OSIX_HTONL (RM_APP_ID);
    HbHdr.u4DestEntId = OSIX_HTONL (RM_APP_ID);
    HbHdr.u4SeqNo = OSIX_HTONS (0);

    /* Prepend HB hdr to HB data */
    if ((HB_PREPEND_BUF (pHbMsg, (UINT1 *) &HbHdr, sizeof (tHbHdr))) !=
        CRU_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Prepend HB hdr - failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    /* Calculate the checksum and fill-in here */
    u2Cksum = HB_CALC_CKSUM (pHbMsg, (HB_HDR_LENGTH + u4Size), 0);

    HB_DATA_PUT_2_BYTE (pHbMsg, HB_HDR_OFF_CKSUM, u2Cksum, i4RetVal);
    if (i4RetVal != CRU_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Update Cksum in HB hdr - failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbWaitForOtherModulesBootUp
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : This routine is used to hold the HB protocol operations
 *                      till other modules completes its initialization.
 ******************************************************************************/
INT4
HbWaitForOtherModulesBootUp (VOID)
{
    UINT4               u4Event = 0;

    while (1)
    {
        HB_RECV_EVENT (HB_TASK_ID, HB_RESUME_EVENT, OSIX_WAIT, &u4Event);

        if (u4Event & HB_RESUME_EVENT)
        {
#ifdef RM_WANTED
            if (HbResumeProtocolOperation () != HB_SUCCESS)
            {
                return HB_FAILURE;
            }
#endif /* RM_WANTED */

            break;
        }
    }
    return HB_SUCCESS;
}

#ifdef ICCH_WANTED
/******************************************************************************
 * Function           : HbIcchInitiateHBMsg
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None                  
 * Action             : This routine is used to hold the HB protocol operations
 *                      till other modules completes its initialization.
 ******************************************************************************/
VOID
HbIcchInitiateHBMsg (VOID)
{
    /* Start the Heart Beat Timer */
    /* HB Timer to be started only when hb interval is > 10 ms */
    if (HbTmrStartTimer (HB_ICCH_HEARTBEAT_TIMER,
                         HB_GET_HB_INTERVAL ()) != HB_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! Heart Beat start timer Failure !!!\n");
        return;
    }

    /* Initial HB message to detect the peer is rebooted */
    if (HbIcchFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbResumeProtocolOperation: "
                "HbFormAndSendMsg -- Failure !!!\r\n");
    }
    /* Start the Peer Dead Timer */
    if (HbTmrStartTimer (HB_ICCH_PEER_DEAD_TIMER,
                         HB_GET_PEER_DEAD_INTERVAL ()) != HB_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! Peer Dead start timer "
                "Failure !!!\r\n");
        return;
    }
    return;
}

/******************************************************************************
 * Function           : HbIcchTerminateHBMsg
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None                  
 * Action             : This routine is used to stop Heartbeat messages exchange
 * ****************************************************************************/
VOID
HbIcchTerminateHBMsg (VOID)
{
    tMacAddr            DestMacAddr;
    UINT4               u4IfIndex = 0;
    UINT4               u4PeerAddr = 0;
    tVlanId             VlanId = 0;

    /* Delete the Static ARP and FDB added for ICCL Peer */
    MEMSET (DestMacAddr, 0, sizeof (tMacAddr));

    IcchGetIcclL3IfIndex (&u4IfIndex);
    MEMCPY (&u4PeerAddr, &(gHbInfo.u4IcchPeerNode), sizeof (u4PeerAddr));
    HbArpModifyWithIndex (u4IfIndex, u4PeerAddr, HB_ARP_INVALID);
    u4IfIndex = 0;

    MEMCPY (DestMacAddr, gHbInfo.au1DestMac, sizeof (tMacAddr));
    VlanId = (tVlanId) (IcchApiGetIcclVlanId (ICCH_DEFAULT_INST));
    IcchGetIcclIfIndex (&u4IfIndex);
    HbVlanHwDelFdbEntry (u4IfIndex, DestMacAddr, VlanId);

    MEMSET (gHbInfo.au1DestMac, 0, sizeof (tMacAddr));

    if (HB_ICCH_GET_PEER_NODE_ID () != 0)
    {
        /* Update the PEER node state and notify the protocols */
        HbIcchNotifyPeerInfo (HB_ICCH_GET_PEER_NODE_ID (), ICCH_PEER_DOWN);
    }
    /* This peer node id set to zero should be present before
     * returning in curr node = transition in progress and
     * prev state is slave OR INIT check, otherwise after completing
     * pending operation at ICCH core module, peer down
     * will not be detected and GO_MASTER will not be sent to
     * ICCH core module*/

    HB_ICCH_SET_PEER_NODE_ID (0);
    HB_ICCH_SET_PREV_NODE_STATE (HB_ICCH_GET_NODE_STATE ());
    HB_ICCH_SET_MASTER_NODE_ID (0);

    /* 1. In Failover case, this event is received after processing the
     * Rx buffer messages to give GO_MASTER to ICCH core module.
     */
    HbIcchUpdateState (HB_ICCH_INIT);

    if ((HbTmrStopTimer (HB_ICCH_HEARTBEAT_TIMER)) != HB_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! Heart Beat stop timer Failure !!!\r\n");
        return;
    }

    /* Start the Peer Dead Timer */
    if ((HbTmrStopTimer (HB_ICCH_PEER_DEAD_TIMER)) != HB_SUCCESS)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! Peer Dead start timer "
                "Failure !!!\r\n");
        return;
    }
    return;
}

/******************************************************************************
 * Function           : HbProcessMsgRcvdOnICCLPort
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the received HB messages in the 
 *                      connecting ICCL port.
 ******************************************************************************/
VOID
HbProcessMsgRcvdOnICCLPort (VOID)
{
    tHbMsg             *pPkt = NULL;
    tARP_DATA           ArpData;
    tMacAddr            DestMacAddr;
    UINT4               u4PeerAddr = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4IpHdrLen = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;
    UINT4               u4TotLen = 0;
    UINT4               u4Port = 0;
    UINT4               u4L3IfIndex = 0;
    tVlanId             VlanId = 0;
    UINT2               u2Cksum = 0;
    UINT1               u1IpVerHdr = 0;

    MEMSET (&ArpData, 0, sizeof (tARP_DATA));

    while (HB_RECEIVE_FROM_QUEUE (HB_ICCH_QUEUE_ID, (UINT1 *) &pPkt,
                                  HB_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (HbLaGetMCLAGSystemStatus () == HB_MCLAG_DISABLED)
        {
            /*Do not process heart beat packets from remote, till
             *MC-LAG is enabled in the system.
             */
            HB_TRC (HB_ALL_FAILURE_TRC, "Heart-beat packets cannot be processed\
                              since MC-LAG is disabled in the system\r\n");
            HB_FREE (pPkt);
            continue;
        }
        /* Peer's MAC and ARP are statically programmed so as to communicate
         * with peer This is done because MAC-Learning is disabled on ICCL
         * interface */
        MEMSET (DestMacAddr, 0, sizeof (DestMacAddr));
        HB_GET_DATA_N_BYTE (pPkt, DestMacAddr, MAC_ADDR_LEN, MAC_ADDR_LEN);
        if (HB_ZERO != MEMCMP (DestMacAddr, gHbInfo.au1DestMac, MAC_ADDR_LEN))
        {
            IcchGetIcclIfIndex (&u4IfIndex);
            VlanId = (tVlanId) (IcchApiGetIcclVlanId (ICCH_DEFAULT_INST));
            /* Add the static MAC-Address for ICCL interface */
            VlanHwAddFdbEntry (u4IfIndex, DestMacAddr, VlanId, VLAN_PERMANENT);
            /* Create PEER Redirect Filter and Filter for lifting ARP Pkts */
            VlanHwAddFdbEntry (u4IfIndex, DestMacAddr, VLAN_WILD_CARD_ID,
                               VLAN_PERMANENT);
            MEMCPY (gHbInfo.au1DestMac, DestMacAddr, sizeof (DestMacAddr));
        }

        VlanId = 0;
        HB_GET_DATA_2_BYTE (pPkt, HB_MSG_VLAN_PROTOID_OFFSET, VlanId);
        if (VlanId == VLAN_PROVIDER_PROTOCOL_ID)
        {
            CRU_BUF_Move_ValidOffset (pPkt, HB_MSG_ETH_DOUBLETAG_HEADER_SIZE);
        }
        else
        {
            CRU_BUF_Move_ValidOffset (pPkt, HB_MSG_ETH_HEADER_SIZE);
        }

        HB_GET_DATA_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1IpVerHdr);
        u4IpHdrLen = (UINT4) ((u1IpVerHdr & 0x0F) * 4);

        HB_GET_DATA_4_BYTE (pPkt, IP_PKT_OFF_SRC, u4PeerAddr);

        /* Update Peer Node Ip Addr */
        if (HB_ICCH_GET_PEER_NODE_ID () == 0)
        {
            /* If the peer node ip address is zero already, 
               then new peer information is arrived */
            HB_ICCH_PEER_NEWLY_ARRIVED = OSIX_TRUE;
        }
        HB_ICCH_SET_PEER_NODE_ID (u4PeerAddr);

        /* Programming static ARP for peer node */
        if (HB_ZERO != MEMCMP (DestMacAddr, gHbInfo.au1DestMac, MAC_ADDR_LEN))
        {
            ArpData.i2Hardware = ARP_ENET_TYPE;
            ArpData.u4IpAddr = gHbInfo.u4IcchPeerNode;

            /* Get L3 Interface index */
            IcchGetIcclL3IfIndex (&u4L3IfIndex);
            /* Get port number using L3 interface index */
            HbPortGetIpPortFrmIndex (u4L3IfIndex, &u4Port);
            ArpData.u2Port = (UINT2) u4Port;

            ArpData.i1Hwalen = (INT1) MAC_ADDR_LEN;
            MEMCPY (ArpData.i1Hw_addr, DestMacAddr, ArpData.i1Hwalen);
            ArpData.u1EncapType = HB_ARP_ENET_V2_ENCAP;
            ArpData.i1State = (INT1) HB_ARP_STATIC;
            ArpData.u1RowStatus = ACTIVE;
            if (ARP_FAILURE == ArpAddArpEntry (ArpData, NULL, 0))
            {
                HB_TRC (HB_CRITICAL_TRC,
                        "!!! Static ARP Addition Failed.... !!!\r\n");
            }
            else
            {
                MEMCPY (gHbInfo.au1DestMac, DestMacAddr, sizeof (DestMacAddr));
            }
        }

        /* Received pkt has [IP Hdr + Hb Hdr + Hb Data].
         * Strip off the IP header and extract the HbMsg from the rcvd pkt */
        HB_PKT_MOVE_TO_DATA (pPkt, u4IpHdrLen);

        /* IP hdr is already stripped off. Now the pkt has [Hb hdr + HB data].
         * Extract the HB hdr info.  Verify checksum */
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_TOT_LENGTH, u4TotLen);

        HB_PKT_DUMP (HB_PKT_DUMP_TRC, pPkt, u4TotLen,
                     "Dumping HB packets on reception\r\n");
        u2Cksum = HB_CALC_CKSUM (pPkt, u4TotLen, 0);
        if (u2Cksum != 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Pkt rcvd with invalid cksum. Discarding. !!!\r\n");
            HB_FREE (pPkt);
            continue;
        }

        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_SRC_ENTID, u4SrcEntId);
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_DEST_ENTID, u4DestEntId);

        /* Now the pkt has [HB Hdr + HB data]. Strip off HB header */
        HB_PKT_MOVE_TO_DATA (pPkt, HB_HDR_LENGTH);

        /* Send HB data to appls. based on the dest entity id. for processing */
        if (u4DestEntId == ICCH_APP_ID)
        {
            /* Get the message type to distinguish between the packets
             * received */
            HB_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

            if (u4MsgType == HEART_BEAT_MSG)
            {
                HbProcessIcchMsg (pPkt);
            }
            else
            {
                HB_TRC (HB_ALL_FAILURE_TRC, "DestEntId invalid in rcvd pkt\n");
                HB_FREE (pPkt);
            }
        }
        else
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "DestEntId invalid in rcvd pkt\n");
            HB_FREE (pPkt);
        }
    }
    return;
}
#endif

#ifdef RM_WANTED
/******************************************************************************
 * Function           : HbPostSelfAttachEvent
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 *
 * Action             : This function is used to post RM_INIT Event to all
 *                      modules registered with RM. MBSM module on processing
 *                      this event provides Self Attach Indication.Other modules
 *                      does not handles this Event
 *
 * Returns            : None
 ******************************************************************************/
VOID
HbPostSelfAttachEvent (VOID)
{
    tRmHbMsg            RmHbMsg;

    MEMSET (&RmHbMsg, 0, sizeof (tRmHbMsg));

    RmHbMsg.u4Evt = RM_TRIGGER_SELF_ATTACH;
    RmApiSendHbEvtToRm (&RmHbMsg);
    return;

}

/******************************************************************************
 * Function           : HbTxTsk
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Call back function invoked by cfa to tx rm hb msgs
 ******************************************************************************/
VOID
HbTxTsk (VOID)
{
    if (HbFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbTxTsk: " "Sending hb msg to peer failed !!!\r\n");
    }
}

/******************************************************************************
 * Function           : HbProcessRcvEvt
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the received HB messages.
 ******************************************************************************/
VOID
HbProcessRcvEvt (VOID)
{
    UINT4               u4PeerAddr = 0;
    UINT4               u4MsgType = 0;
    UINT1               u1IpVerHdr = 0;
    UINT4               u4IpHdrLen = 0;
    UINT1               au1RcvPkt[MAX_IP_HB_PKT_LEN];
    tHbMsg             *pPkt = NULL;
    INT4                i4RecvBytes = 0;
    UINT4               u4HbDataLen = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;
    UINT4               u4TotLen = 0;
    UINT2               u2Cksum = 0;

    while ((i4RecvBytes =
            HbRawSockRcv (au1RcvPkt, MAX_IP_HB_PKT_LEN, &u4PeerAddr)) > 0)
    {
        /* Abort if i4RecvBytes > MAX_IP_HB_PKT_LEN */
        FSAP_ASSERT (i4RecvBytes <= MAX_IP_HB_PKT_LEN);

        /* Compare the SrcIp of the rcvd pkt and the SelfNodeIp. 
         * Drop the self-originated packets. */
        if (u4PeerAddr == HB_GET_SELF_NODE_ID ())
        {
            continue;
        }

        /* Discard the pkts if the peer node IP and self node IP 
         * are not in same subnet. This is especially added to 
         * test the exe on Linux environment. i.e., to run more 
         * than 2 exes on the same m/c. 
         * Therefore configure the backplane interface IP with
         * the same subnet. 
         * N11: 127.1.0.1, N12: 127.1.0.2 - Chassis 1
         * N21: 127.2.0.1, N22: 127.2.0.2 - Chassis 2 */
        if ((u4PeerAddr & HB_GET_SELF_NODE_MASK ()) !=
            (HB_GET_SELF_NODE_ID () & HB_GET_SELF_NODE_MASK ()))
        {
            continue;
        }

        /* Allocate memory for CRU buffer. This mem will be freed by appls. 
         * after processing. So, do not free this mem here. */
        if ((pPkt = HB_ALLOC_RX_BUF ((UINT4) i4RecvBytes)) == NULL)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "EXIT: Unable to allocate buf\r\n");
            break;
        }

        /* copy the linear buffer to CRU buffer */
        if (HB_COPY (pPkt, au1RcvPkt, (UINT4) i4RecvBytes) == CRU_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "EXIT: Unable to copy linear to CRU buf\r\n");
            HB_FREE (pPkt);
            break;
        }

        HB_GET_DATA_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1IpVerHdr);
        u4IpHdrLen = (UINT4) ((u1IpVerHdr & 0x0F) * 4);

        /* Received pkt has [IP Hdr + Hb Hdr + Hb Data].
         * Strip off the IP header and extract the HbMsg from the rcvd pkt */
        HB_PKT_MOVE_TO_DATA (pPkt, u4IpHdrLen);

        /* IP hdr is already stripped off. Now the pkt has [HB hdr + HB data].
         * Extract the HB hdr info.  Verify checksum */
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_TOT_LENGTH, u4TotLen);

        u2Cksum = HB_CALC_CKSUM (pPkt, u4TotLen, 0);
        if (u2Cksum != 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Pkt rcvd with invalid cksum. Discarding. !!!\r\n");
            HB_FREE (pPkt);
            continue;
        }

        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_SRC_ENTID, u4SrcEntId);
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_DEST_ENTID, u4DestEntId);

        /* Now the pkt has [HB Hdr + HB data]. Strip off HB header */
        HB_PKT_MOVE_TO_DATA (pPkt, HB_HDR_LENGTH);

        u4HbDataLen = u4TotLen - HB_HDR_LENGTH;

        if (u4DestEntId >= RM_MAX_APPS)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "DestEntId invalid in rcvd pkt\r\n");
            HB_FREE (pPkt);
            continue;
        }

        /* Send RM data to appls. based on the dest entity id. for processing */
        if (u4DestEntId == RM_APP_ID)
        {
            /* Get the message type to distinguish between the packets
             * received */
            HB_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

            if (u4MsgType == HEART_BEAT_MSG)
            {
                HbProcessMsg (pPkt, u4HbDataLen, u4PeerAddr);
            }
            else
            {
                HB_FREE (pPkt);
            }

        }

        else
        {
            HB_FREE (pPkt);
        }
    }

}

/******************************************************************************
 * Function           : HbProcessMsgRcvdOnStkPort
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the received HB messages in the 
                        connecting Stack port.
 ******************************************************************************/
VOID
HbProcessMsgRcvdOnStkPort (VOID)
{
    tHbMsg             *pPkt = NULL;
    UINT4               u4PeerAddr = 0;
    UINT4               u4MsgType = 0;
    UINT4               u4IpHdrLen = 0;
    UINT4               u4HbDataLen = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;
    UINT4               u4TotLen = 0;
    UINT2               u2Cksum = 0;
    UINT1               u1IpVerHdr = 0;

    while (HB_RECEIVE_FROM_QUEUE (HB_QUEUE_ID, (UINT1 *) &pPkt,
                                  HB_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        HB_GET_DATA_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1IpVerHdr);
        u4IpHdrLen = (UINT4) ((u1IpVerHdr & 0x0F) * 4);

        HB_GET_DATA_4_BYTE (pPkt, IP_PKT_OFF_SRC, u4PeerAddr);

        /* Received pkt has [IP Hdr + Hb Hdr + Hb Data].
         * Strip off the IP header and extract the HbMsg from the rcvd pkt */
        HB_PKT_MOVE_TO_DATA (pPkt, u4IpHdrLen);

        /* IP hdr is already stripped off. Now the pkt has [Hb hdr + HB data].
         * Extract the HB hdr info.  Verify checksum */
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_TOT_LENGTH, u4TotLen);

        u2Cksum = HB_CALC_CKSUM (pPkt, u4TotLen, 0);
        if (u2Cksum != 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Pkt rcvd with invalid cksum. Discarding. !!!\r\n");
            HB_FREE (pPkt);
            continue;
        }

        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_SRC_ENTID, u4SrcEntId);
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_DEST_ENTID, u4DestEntId);

        /* Now the pkt has [HB Hdr + HB data]. Strip off HB header */
        HB_PKT_MOVE_TO_DATA (pPkt, HB_HDR_LENGTH);

        u4HbDataLen = u4TotLen - HB_HDR_LENGTH;

        if (u4DestEntId >= RM_MAX_APPS)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "DestEntId invalid in rcvd pkt\n");
            HB_FREE (pPkt);
            continue;
        }

        /* Send HB data to appls. based on the dest entity id. for processing */
        if (u4DestEntId == RM_APP_ID)
        {
            /* Get the message type to distinguish between the packets
             * received */
            HB_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

            if (u4MsgType == HEART_BEAT_MSG)
            {
                HbProcessMsg (pPkt, u4HbDataLen, u4PeerAddr);
            }
            else
            {
                HB_FREE (pPkt);
            }

        }

        else
        {
            HB_FREE (pPkt);
        }
    }

}

/******************************************************************************
 * Function           : HbProcessMsg
 * Input(s)           : pHbData - Rcvd HB msg.
 *                      u4DataLen - Length of pHbData
 *                      u4PeerAddr - Addr of the peer from which the msg arrived
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the HeartBeat msg received from peer.
 ******************************************************************************/
VOID
HbProcessMsg (tHbMsg * pHbData, UINT4 u4DataLen, UINT4 u4PeerAddr)
{
    UINT1               u1Event = HB_MAX_EVENTS;    /* initialized to invalid 
                                                     * value */
    UINT4               u4SelfNodeIp = 0;
    UINT4               u4SelfNodePri = 0;
    UINT4               u4PeerNodePri = 0;
    BOOL1               bRestartPeerDeadTmr = OSIX_FALSE;
    tHbData             HbData;
    tRmHbMsg            RmHbMsg;
    tRegPeriodicTxInfo  RegnInfo;

    UNUSED_PARAM (u4DataLen);

    MEMSET (&RegnInfo, 0, sizeof (tRegPeriodicTxInfo));
    MEMSET (&HbData, 0, sizeof (tHbData));
    MEMSET (&RmHbMsg, 0, sizeof (tRmHbMsg));

    /* Copy the CRU buf content to linear buffer and free the CRU buf. */
    if (HB_COPY_TO_LINEAR_BUF (pHbData, &HbData, 0, sizeof (tHbData))
        == CRU_FAILURE)
    {
        HB_TRC (HB_CRITICAL_TRC, "CRU to linear copy failure\r\n");
        HB_FREE (pHbData);
        HB_INCR_MSG_RX_FCOUNT ();
        return;
    }

    HB_FREE (pHbData);

    /* Convert the rcvd msg from NETWORK byte order to HOST byte order */
    HbData.u4MsgType = OSIX_NTOHL (HbData.u4MsgType);
    HbData.u4HbInterval = OSIX_NTOHL (HbData.u4HbInterval);
    HbData.u4PeerDeadInterval = OSIX_NTOHL (HbData.u4PeerDeadInterval);
    HbData.u4PeerDeadIntMultiplier =
        OSIX_NTOHL (HbData.u4PeerDeadIntMultiplier);
    HbData.u4ActiveNode = OSIX_NTOHL (HbData.u4ActiveNode);
    HbData.u4SelfNode = OSIX_NTOHL (HbData.u4SelfNode);
    HbData.u4PeerNode = OSIX_NTOHL (HbData.u4PeerNode);
    HbData.u4SelfNodePriority = OSIX_NTOHL (HbData.u4SelfNodePriority);
    HbData.u4PeerNodePriority = OSIX_NTOHL (HbData.u4PeerNodePriority);
    HbData.u4Flags = OSIX_NTOHL (HbData.u4Flags);

    HB_INCR_MSG_RX_COUNT ();

    if (IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_ENABLE)
    {
        if (IssuGetIssuMode () == ISSU_INCOMPATIBLE_MODE)
        {
            /* Copy the peer Software version in the Hbdata, this will be used
             * in case of ISSU incompatible during the peer dead timer expiry.
             * If both node versions are same peer dead timer needs
             * to be restarted */
            MEMCPY (gHbInfo.au1IssVersion, HbData.au1IssVersion,
                    (ISS_STR_LEN - 1));

            /* Check whether the peer software version and current node
             * software version is same, if it is not matched drop the packet */
            if (MEMCMP (HbData.au1IssVersion, IssGetSoftwareVersion (),
                        ISS_STR_LEN) != 0)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "!!! Software version is mismatched !!!\r\n");
                HB_INCR_MSG_RX_FCOUNT ();
                return;
            }
        }
        else
        {
            /* If issu loadversion triggerd on node then do not proccess the 
             * HP message til reboot the system */
            if (IssuApiIsLoadVersionTriggered () == OSIX_TRUE)
            {
                HB_INCR_MSG_RX_FCOUNT ();
                return;
            }
        }
    }

    /* If there is a change in HeartBeat/PeerDead interval in the HB msg rcvd 
     * from ACTIVE node, change the values accordingly in peer node 
     * to be in sync. */
    /* Allow to change the HB interval and peer dead interval 
     * only when the proper values (refer the below "if" check) 
     * are obtained in the HB message. */
    if ((HB_GET_NODE_STATE () != HB_RM_ACTIVE) &&
        (!((HbData.u4ActiveNode == 0) &&
           (HbData.u4PeerNode /* 0 */  != HB_GET_SELF_NODE_ID ()) &&
           (HbData.u4SelfNode == HB_GET_PEER_NODE_ID ()) &&
           (HB_GET_NODE_STATE () == HB_RM_STANDBY))))
    {
        if (HbData.u4HbInterval != HB_GET_HB_INTERVAL ())
        {
            HB_TRC1 (HB_CRITICAL_TRC,
                     "!!! Heart Beat Interval of ACTIVE node is modified. "
                     "Updating the Heart Beat Interval to %dms ...\r\n",
                     HbData.u4HbInterval);
            HB_SET_HB_INTERVAL (HbData.u4HbInterval);

            /* HB Timer to be restarted only when hb interval is > 10 ms */
            /* HB module to register with CFA only when hb interval is = 10 ms */
            if (HB_GET_HB_INTERVAL () == HB_MIN_INTERVAL)
            {
                RegnInfo.SendToApplication = HbTxTsk;
                CfaRegisterPeriodicPktTx (CFA_APP_RM, &RegnInfo);
            }
            else
            {
                CfaDeRegisterPeriodicPktTx (CFA_APP_RM);
                if (HbTmrRestartTimer (HB_HEARTBEAT_TIMER,
                                       HB_GET_HB_INTERVAL ()) == HB_FAILURE)
                {
                    HB_TRC (HB_ALL_FAILURE_TRC,
                            "!!! Heart Beat re-start timer Failure !!!\r\n");
                }
            }
        }
        if (HbData.u4PeerDeadInterval != HB_GET_PEER_DEAD_INTERVAL ())
        {
            HB_TRC1 (HB_CRITICAL_TRC,
                     "!!! Peer Dead Interval of ACTIVE node is modified. "
                     "Updating the Peer Dead Interval to %dms \r\n",
                     HbData.u4PeerDeadInterval);
            HB_SET_PEER_DEAD_INTERVAL (HbData.u4PeerDeadInterval);
            bRestartPeerDeadTmr = OSIX_TRUE;
        }

        if (HbData.u4PeerDeadIntMultiplier !=
            HB_GET_PEER_DEAD_INT_MULTIPLIER ())
        {
            HB_TRC1 (HB_CRITICAL_TRC,
                     "!!! Peer Dead Interval Multiplier of ACTIVE node is "
                     "modified. Updating the Peer Dead Interval "
                     "Multiplier to %d ...\r\n",
                     HbData.u4PeerDeadIntMultiplier);
            HB_SET_PEER_DEAD_INT_MULTIPLIER (HbData.u4PeerDeadIntMultiplier);
            bRestartPeerDeadTmr = OSIX_TRUE;
        }
    }
    if (bRestartPeerDeadTmr == OSIX_TRUE)
    {
        if (HbTmrRestartTimer (HB_PEER_DEAD_TIMER,
                               HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Peer Dead restart timer Failure !!!\r\n");
            HB_INCR_MSG_RX_FCOUNT ();
            return;
        }
    }

    /* ACTIVE NODE CHECK,  
     * Below 'if' check is added to detect 
     * whether the peer is dead or not when the STANDBY node reboots 
     * in less than peer dead time. */
    if ((HbData.u4ActiveNode == 0) &&
        (HbData.u4PeerNode /* 0 */  != HB_GET_SELF_NODE_ID ()) &&
        (HbData.u4SelfNode == HB_GET_PEER_NODE_ID ()) &&
        (HB_GET_NODE_STATE () == HB_RM_ACTIVE))
    {
        /* STANDBY node is rebooted before ACTIVE detects 
         * the peer is dead */
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! STANDBY node is rebooted before ACTIVE detects "
                "the peer is dead !!!\r\n");
        HbTmrHdlPeerDeadTmrExp (NULL);
    }
    /* Update the Peer node ip addr. i.e., IpAddr of the node 
     * from which the pkt is rcvd */
    HB_SET_PEER_NODE_ID (u4PeerAddr);

    /* Read the NodeId and Priority information. */
    u4SelfNodeIp = HB_GET_SELF_NODE_ID ();
    u4SelfNodePri = HB_GET_SELF_NODE_PRIORITY ();
    u4PeerNodePri = HB_GET_PEER_NODE_PRIORITY ();

    /* Below 'if' check is added to detect 
     * whether the peer is dead or not when the ACTIVE node reboots 
     * in less than peer dead time. */
    if ((HbData.u4ActiveNode == 0) &&
        (HbData.u4PeerNode /* 0 */  != HB_GET_SELF_NODE_ID ()) &&
        (HbData.u4SelfNode == HB_GET_PEER_NODE_ID ()) &&
        (HB_GET_NODE_STATE () == HB_RM_STANDBY))
    {
        /* STANDBY NODE CHECK 
         * ACTIVE node is rebooted before STANDBY detects the 
         * peer is dead*/
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! ACTIVE node is rebooted before STANDBY detects "
                "the peer is dead !!!\r\n");
        HbTmrHdlPeerDeadTmrExp (NULL);
        u1Event = HB_IGNORE_PKT;
    }
    else if (HbData.u4PeerNode == 0)
    {
        /* Peer node has not learnt about me. 
         * But this node has seen the peer node */
        u1Event = HB_1WAY_RCVD;
    }
    else if (HbData.u4Flags & HB_FORCE_SWITCHOVER)
    {
        /* If there is force-switchover msg from peer, force this
         *          * node to become ACTIVE. */
        u1Event = HB_FSW_RCVD;
    }
    else if (HbData.u4Flags & HB_COUNTER_ENABLE)
    {
        HB_SET_STATS_ENABLE (HB_STATS_ENABLE);
    }
    else if (HbData.u4Flags & HB_COUNTER_DISABLE)
    {
        HB_SET_STATS_ENABLE (HB_STATS_DISABLE);
    }
    else if (HbData.u4Flags & HB_FORCE_SWITCHOVER_ACK)
    {
        u1Event = HB_FSW_ACK_RCVD;
    }
    else if ((HbData.u4Flags & HB_TRANS_IN_PROGRESS_FLAG) ||
             (HB_GET_NODE_STATE () == HB_RM_TRANSITION_IN_PROGRESS))
    {
        /* Don't allow any operation when HB message with 
         * "transition in progress" flag is received */
        u1Event = HB_IGNORE_PKT;
    }
    else if ((HbData.u4Flags & HB_STANDBY_REBOOT) &&
             (HB_GET_NODE_STATE () != HB_RM_ACTIVE))
    {
        RmApiSystemRestart ();
    }
    else if (HbData.u4Flags & HB_STDBY_PROTOCOL_RESTART)
    {
        /* When the active node receives a HB_STDBY_PROTOCOL_RESTART flag,
         * the active node internally sends a peer down event to its
         * protocols so that they stop sending the static and dynamic
         * update messages. This is done because the standby node cannot
         * process these update messages, as the protocols are restarting
         * and will obtain all the configurations in the bulk update 
         * messages after the protocols have restarted.
         * Also, the active node sends a acknowledge message to the standby node
         * When the standby node receives a HB_STDBY_PROTOCOL_RESTART flag,
         * the active node would already stopped all the sync msg 
         * transmission. Hence, send a acknowledge message and start
         * the standby software reboot. */
        if (HB_GET_NODE_STATE () == HB_RM_ACTIVE)
        {
            if (gHbInfo.u1StdbySoftRbt == HB_STDBY_SOFT_RBT_NOT_OCCURED)
            {
                RmHbMsg.u4PeerAddr = HB_GET_PEER_NODE_ID ();
                RmHbMsg.u4Evt = RM_PEER_DOWN;
                RmApiSendHbEvtToRm (&RmHbMsg);
                gHbInfo.u1StdbySoftRbt = HB_STDBY_SOFT_RBT_NODE_DOWN;
            }
            if (HbSendMsgToPeer (HB_STDBY_PROTOCOL_RESTART_ACK) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC, "Sending Standby node "
                        "software reboot flag failed !!!\r\n");
            }
        }
        else
        {
            if (HbSendMsgToPeer (HB_STDBY_PROTOCOL_RESTART_ACK) == HB_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC, "Sending Standby node software "
                        "reboot flag failed !!!\r\n");
            }
            if (gHbInfo.u1StdbySoftRbt == HB_STDBY_SOFT_RBT_NOT_OCCURED)
            {
                gHbInfo.u1StdbySoftRbt = HB_STDBY_SOFT_RBT_NODE_DOWN;
                /* As the active node initiated the standby node's soft reboot
                 * increment the reboot counter  and then initiate the protocol
                 * restart */
                RmApiIncrementRebootCntr ();
                RmApiAllProtocolRestart ();
            }
        }
    }
    else if (HbData.u4Flags & HB_STDBY_PROTOCOL_RESTART_ACK)
    {
        /* When the STANDBY_SOFT_REBOOT acknowledge is received, 
         * reset the STANDBY_SOFT_REBOOT flag in HB message and stop
         * the timer. In addition, the standby node starts its
         * Software reboot. */
        HB_RESET_FLAGS (HB_STDBY_PROTOCOL_RESTART);

        if (gHbInfo.u1StdbySoftRbt == HB_STDBY_SOFT_RBT_NOT_OCCURED)
        {
            if (HB_GET_NODE_STATE () != HB_RM_ACTIVE)
            {
                RmApiAllProtocolRestart ();
            }
            gHbInfo.u1StdbySoftRbt = HB_STDBY_SOFT_RBT_NODE_DOWN;
        }
    }
    else if ((HbData.u4Flags & HB_STDBY_PEER_UP) &&
             (HB_GET_NODE_STATE () == HB_RM_ACTIVE))
    {
        /* Active received a standby peer up message. Send the acknowledge */
        if (gHbInfo.u1StdbySoftRbt == HB_STDBY_SOFT_RBT_NODE_DOWN)
        {
            RmHbMsg.u4PeerAddr = HB_GET_PEER_NODE_ID ();
            RmHbMsg.u4Evt = RM_PEER_UP;
            RmApiSendHbEvtToRm (&RmHbMsg);
            gHbInfo.u1StdbySoftRbt = HB_STDBY_SOFT_RBT_NOT_OCCURED;
        }
        if (HbSendMsgToPeer (HB_STDBY_PEER_UP_ACK) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "Sending Standby node software "
                    "reboot flag failed !!!\r\n");
        }
    }
    else if ((HbData.u4Flags & HB_STDBY_PEER_UP_ACK) &&
             (HB_GET_NODE_STATE () != HB_RM_ACTIVE))
    {
        if (gHbInfo.u1StdbySoftRbt == HB_STDBY_SOFT_RBT_NODE_DOWN)
        {
            RmApiInitiateBulkRequest ();
            gHbInfo.u1StdbySoftRbt = HB_STDBY_SOFT_RBT_NOT_OCCURED;
        }
        HB_RESET_FLAGS (HB_STDBY_PEER_UP);
    }
    else if (HbData.u4PeerNode == u4SelfNodeIp)
    {
        /* Rcvd pkt's peer node has my ip addr. Therefore, 
         * peer node has learnt about me & I have seen the peer node. */

        /* If both the nodes priorities are same, and are not 0, 
         * go for normal election based on Priority/NodeId.
         * Node with Priority = 0, is not eligible for election. */
        if ((HbData.u4SelfNodePriority == u4PeerNodePri) &&
            (HbData.u4PeerNodePriority == u4SelfNodePri))
        {
            /* There is no change in node priority. */
            if (u4SelfNodePri != 0)
            {
                HbProcessNodeId (&HbData, &u1Event);
            }
        }
        else
        {
            /* There is a change in node priority. */
            HbProcessNodePriority (&HbData, &u1Event);
        }
    }
    else
    {
        /* Discard this packet */
        u1Event = HB_IGNORE_PKT;
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! HB rcvd from invalid Peer. Discarding.... !!!\r\n");
        HB_INCR_MSG_RX_FCOUNT ();
        return;
    }

    HB_TRC2 (HB_EVENT_TRC, "FSM Event: %d  State: %d\r\n",
             u1Event, HB_GET_NODE_STATE ());
    /* StateMachine is initiated only if the event is valid */
    if (u1Event != HB_MAX_EVENTS)
    {
        HbStateMachine (u1Event);
    }
    if (HbTmrRestartTimer (HB_PEER_DEAD_TIMER,
                           HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! Peer Dead restart timer "
                "Failure !!!\r\n");
    }

    HB_INCR_MSG_RX_PCOUNT ();
    return;
}

/******************************************************************************
 * Function           : HbProcessNodeId
 * Input(s)           : pHbData - Rcvd HB msg,
 * Output(s)          : u1Event - Event to be set based on the msg.
 * Returns            : HB_SUCCESS/HB_FAILURE.
 * Action             : Routine to process the NodeId information of 
 *                      HeartBeat msg.
 ******************************************************************************/
UINT4
HbProcessNodeId (tHbData * pHbData, UINT1 *pu1Event)
{
    tUtlInAddr          HbNodeId;

    MEMSET (&HbNodeId, 0, sizeof (tUtlInAddr));

    if (HB_GET_ACTIVE_NODE_ID () == 0)
    {
        /* This node doesn't know about ACTIVE node. */
        if (pHbData->u4ActiveNode == 0)
        {
            /* This node doesn't know about ACTIVE node. 
             * Peer doesn't know about ACTIVE node.
             * Active node is not yet elected. Start the election */
            *pu1Event = HB_2WAY_RCVD;
        }
        else
        {
            /* If the active node id is not known and 
             * the received HB message has self node id as active node id 
             * then skip the HB message. 
             * This situation comes only when the STANDBY node 
             * does not detect the peer is dead */
            if (HB_GET_NODE_STATE () == HB_RM_INIT)
            {
                if (HB_GET_SELF_NODE_ID () == pHbData->u4ActiveNode)
                {
                    HB_TRC (HB_CRITICAL_TRC,
                            "ACTIVE node is rebooted before STANDBY detects "
                            "the peer is dead !!!\r\n");
                    *pu1Event = HB_IGNORE_PKT;
                    return (HB_SUCCESS);
                }
            }
            /* This node deosn't know about ACTIVE node. 
             * Peer knows about Active node. Do not do the election.
             * Reuse the same ACTIVE node. */
            HB_SET_ACTIVE_NODE_ID (pHbData->u4ActiveNode);
            HbNodeId.u4Addr = OSIX_NTOHL (HB_GET_ACTIVE_NODE_ID ());
            HB_TRC1 (HB_CRITICAL_TRC, "ACTIVE NODE elected: %0x\n",
                     INET_NTOA (HbNodeId));

            HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_UP);
            *pu1Event = HB_ACTV_ELCTD;
        }
    }
    else                        /* ghbInfo.u4ActiveNode != 0 */
    {
        /* This node knows the ACTIVE Node. */
        if (pHbData->u4ActiveNode == 0)
        {
            /* This node knows about ACTIVE node. 
             * Peer doesn't know about ACTIVE. */
            *pu1Event = HB_2WAY_RCVD;
        }
        else if (HB_GET_ACTIVE_NODE_ID () != pHbData->u4ActiveNode)
        {
            /* rcvd pkts active node != current active node, i.e.,
             * backplane communication is lost for a while and so both
             * the nodes declare themselves as 'active'. Now, when the backplane
             * communication comes up, the active node will not be the same. 
             * Start re-election here. */

            HB_SET_ACTIVE_NODE_ID (0);
            HB_SET_PEER_NODE_ID (0);
            HB_SET_PREV_NODE_STATE (HB_GET_NODE_STATE ());
            HB_SET_NODE_STATE (HB_RM_INIT);
            *pu1Event = HB_1WAY_RCVD;
        }
        else
        {
            /* Peer node has learnt the active node info. */

            /* Update the PEER node state and notify the protocols */
            HbNotifyPeerInfo (HB_GET_PEER_NODE_ID (), RM_PEER_UP);
            *pu1Event = HB_2WAY_RCVD;
        }
    }
    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbProcessNodePriority
 * Input(s)           : pHbData - Rcvd RM msg,
 * Output(s)          : u1Event - Event to be set based on the msg.
 * Returns            : HB_SUCCESS/HB_FAILURE.
 * Action             : Routine to process the NodePriority information of 
 *                      HeartBeat msg.
 ******************************************************************************/
UINT4
HbProcessNodePriority (tHbData * pHbData, UINT1 *pu1Event)
{
    UINT4               u4SelfNodePri = HB_GET_SELF_NODE_PRIORITY ();
    UINT4               u4PeerNodePri = HB_GET_PEER_NODE_PRIORITY ();

    if (u4SelfNodePri == 0)
    {
        /* Do not participate in election. Do not process HeartBeat
         * messages, stay in INIT state. */
        HB_SET_ACTIVE_NODE_ID (0);
        HB_SET_NODE_STATE (HB_RM_INIT);
        *pu1Event = HB_IGNORE_PKT;
    }
    else if (u4PeerNodePri != pHbData->u4SelfNodePriority)
    {
        /* There is a change in node priority */
        /* Update the priority of peer node */
        HB_SET_PEER_NODE_PRIORITY (pHbData->u4SelfNodePriority);

        /* Start re-election */
        HB_SET_ACTIVE_NODE_ID (0);
        *pu1Event = HB_PRIORITY_CHG;
    }
    else if (u4SelfNodePri != pHbData->u4PeerNodePriority)
    {
        *pu1Event = HB_PRIORITY_CHG;
    }

    /* Update the priority of peer node */
    HB_SET_PEER_NODE_PRIORITY (pHbData->u4SelfNodePriority);

    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbFormAndSendMsg 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to form heart-beat message and send it to the 
 *                      peer. 
 ******************************************************************************/
UINT4
HbFormAndSendMsg (VOID)
{
    tHbData             HbData;
    tHbMsg             *pHbMsg;
    UINT4               u4RetVal = HB_SUCCESS;

    MEMSET (&HbData, 0, sizeof (tHbData));
    /* Allocate memory for HB msg. */
    if ((pHbMsg = HB_ALLOC_TX_BUF (sizeof (tHbData))) == NULL)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbFormAndSendMsg: HB alloc "
                "failed !!!\r\n");
        return (HB_FAILURE);
    }

    /* Form HeartBeat msg */
    HbFormMsg (&HbData);

    HbData.u4Flags = OSIX_HTONL (HB_GET_FLAGS ());
    /* Copy HB data to the appropriate offset by leaving space for HB hdr */
    if ((HB_COPY_TO_OFFSET (pHbMsg, &HbData, 0, sizeof (tHbData)))
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbFormAndSendMsg: HB copy to CRU buf failed !!!\r\n");
        u4RetVal = HB_FAILURE;
    }

    if (u4RetVal != HB_FAILURE)
    {
        u4RetVal = HbPrependHbHdr (pHbMsg, sizeof (tHbData));
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* Send the pkt out to peer thru' RAW socket */
        if (HbSendMsg (pHbMsg, HB_MCAST_IPADDR) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! HbFormAndSendMsg: HbSendMsg -- Failed !!!\r\n");
            u4RetVal = HB_FAILURE;
        }
    }
    else
    {
        HB_FREE (pHbMsg);
    }

    if (u4RetVal != HB_FAILURE)
    {
        HB_INCR_MSG_TX_COUNT ();
    }
    else
    {
        HB_INCR_MSG_TX_FCOUNT ();
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbFormMsg
 * Input(s)           : pHbData - ptr to RmHBMsg
 * Output(s)          : None. 
 * Returns            : HB_SUCCESS/HB_FAILURE.
 * Action             : Routine to form the HeartBeat msg.
 ******************************************************************************/
UINT4
HbFormMsg (tHbData * pHbData)
{
    MEMSET (pHbData, 0, sizeof (tHbData));

    pHbData->u4MsgType = OSIX_HTONL (HEART_BEAT_MSG);
    pHbData->u4HbInterval = OSIX_HTONL (HB_GET_HB_INTERVAL ());
    pHbData->u4PeerDeadInterval = OSIX_HTONL (HB_GET_PEER_DEAD_INTERVAL ());
    pHbData->u4PeerDeadIntMultiplier =
        OSIX_HTONL (HB_GET_PEER_DEAD_INT_MULTIPLIER ());
    pHbData->u4ActiveNode = OSIX_HTONL (HB_GET_ACTIVE_NODE_ID ());
    pHbData->u4SelfNode = OSIX_HTONL (HB_GET_SELF_NODE_ID ());
    pHbData->u4PeerNode = OSIX_HTONL (HB_GET_PEER_NODE_ID ());
    pHbData->u4SelfNodePriority = OSIX_HTONL (HB_GET_SELF_NODE_PRIORITY ());
    pHbData->u4PeerNodePriority = OSIX_HTONL (HB_GET_PEER_NODE_PRIORITY ());

    if ((IssuGetIssuMode () == ISSU_INCOMPATIBLE_MODE) &&
        (IssuGetMaintenanceMode () == ISSU_MAINTENANCE_MODE_ENABLE))
    {
        MEMCPY (pHbData->au1IssVersion, IssGetSoftwareVersion (),
                STRLEN (IssGetSoftwareVersion ()));
    }

    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbSendMsg
 * Input(s)           : pHbMsg - Msg to be sent.
 *                      u4DestAddr - Dest addr to which the msg has to be sent
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to send out the HB msgs (which are arrived from 
 *                      applications) to the peer.
*******************************************************************************/
UINT4
HbSendMsg (tHbMsg * pHbMsg, UINT4 u4DestAddr)
{
    UINT4               u4RetVal = HB_SUCCESS;
    UINT1               au1Data[HB_SYNC_PKT_LEN + IP_HDR_LEN];
    UINT4               u4HbPktLen = 0;
    UINT4               u4TotLen = 0;
    t_IP_HEADER        *pIpHdr;
#ifdef NPAPI_WANTED
    tHwInfo             HwInfo;
#endif
    UINT2               u2Cksum = 0;

    /* Get the length of HB pkt to be sent */
    HB_GET_DATA_4_BYTE (pHbMsg, HB_HDR_OFF_TOT_LENGTH, u4HbPktLen);

    /* Total length of pkt to be sent includes IP hdr length */
    u4TotLen = u4HbPktLen + IP_HDR_LEN;

    /* Form IP hdr as IP_HDRINCL option is set for the socket. */
    pIpHdr = (t_IP_HEADER *) (VOID *) au1Data;

    pIpHdr->u1Ver_hdrlen = (UINT1) IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = 0;
    pIpHdr->u2Totlen = OSIX_HTONS (u4TotLen);
    pIpHdr->u2Id = 0;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = 1;
    pIpHdr->u1Proto = HB_PROTO;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u4Src = OSIX_HTONL (HB_GET_SELF_NODE_ID ());
    pIpHdr->u4Dest = OSIX_HTONL (u4DestAddr);
    u2Cksum = HB_LINEAR_CALC_CKSUM ((CONST INT1 *) pIpHdr, IP_HDR_LEN);
    pIpHdr->u2Cksum = OSIX_HTONS (u2Cksum);

    /* Copy the HB info to the pkt. */
    /* Copy the CRU buf to linear buf and free the CRU buf */
    if (HB_COPY_TO_LINEAR_BUF (pHbMsg, au1Data + IP_HDR_LEN, 0, u4HbPktLen)
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "Copy from CRU buf to linear buf "
                "failed\r\n");
        u4RetVal = HB_FAILURE;
    }

    HB_FREE (pHbMsg);

    if (u4RetVal != HB_FAILURE)
    {
#if (defined NPAPI_WANTED && defined RM_WANTED)
        /* RM Heart beat message channel should be decided based on RM Port
         * Type. If it is out of Band port, HB messages will be tranmitted 
         * through socket on OOB port running over Linux IP.
         * If it is an Inband Port,HB messages will be tranmitted through 
         * socket on stacking port running over Aricent IP */
        if (gu4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_INBAND)
        {
            MEMSET (&HwInfo, 0, sizeof (tHwInfo));
            HwInfo.u4MessageType = RM_HB_PKT;
            HwInfo.uHwMsgInfo.HwGenMsgInfo.pu1Data = au1Data;
            HwInfo.u4PktSize = u4TotLen;

            if (FsCfaHwSendIPCMsg (&HwInfo) == FNP_FAILURE)
            {
                HB_TRC (HB_ALL_FAILURE_TRC,
                        "!!! FsCfaHwSendIPCMsg - failure !!!\r\n");
                u4RetVal = HB_FAILURE;
            }

            return (u4RetVal);

        }
#endif
        if (HbRawSockSend (au1Data, (UINT2) u4TotLen, u4DestAddr) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "!!! RmSockSend - failure !!!\r\n");
            u4RetVal = HB_FAILURE;
        }
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbSendMsgToPeer
 * Input(s)           : u4Msg.
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE 
 * Action             : Routine to send message to peer.
 *                      e.g, force-switchover command on ACTIVE node, 
 *                      should force the ACTIVE node to STANDBY state and 
 *                      the STANDBY node to ACTIVE state.
 *                      Active node will send a msg to STANDBY to switch to
 *                      ACTIVE state.
 ******************************************************************************/
UINT4
HbSendMsgToPeer (UINT4 u4Msg)
{
    tHbData             HbData;
    tHbMsg             *pHbMsg;
    UINT4               u4RetVal = HB_SUCCESS;

    MEMSET (&HbData, 0, sizeof (tHbData));

    UNUSED_PARAM (u4Msg);
    /* Allocate memory for RM msg. */
    if ((pHbMsg = HB_ALLOC_TX_BUF (sizeof (tHbData))) == NULL)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "RM alloc failed !!!\r\n");
        return (HB_FAILURE);
    }

    /* Form HeartBeat msg */
    HbFormMsg (&HbData);

    /* Set the bitmap to indicate that this is a force-switchover msg */
    HbData.u4Flags = OSIX_HTONL (u4Msg);

    /* Copy HB data to the appropriate offset by leaving space for HB hdr */
    if ((HB_COPY_TO_OFFSET (pHbMsg, &HbData, 0, sizeof (tHbData)))
        == CRU_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HB copy to CRU buf failed !!!\r\n");
        u4RetVal = HB_FAILURE;
    }

    if (u4RetVal != HB_FAILURE)
    {
        u4RetVal = HbPrependHbHdr (pHbMsg, sizeof (tHbData));
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* Send the pkt out to peer thru' RAW socket */
        if (HbSendMsg (pHbMsg, HB_MCAST_IPADDR) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "!!! HbSendMsg -- Failed !!!\r\n");
        }
    }
    else
    {
        HB_FREE (pHbMsg);
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function           : HbHandleFSWEvent
 * Input(s)           : None 
 * Output(s)          : None 
 * Returns            : None 
 * Action             : Sends the HB message with HB_FORCE_SWITCHOVER flag set. 
 ******************************************************************************/
VOID
HbHandleFSWEvent ()
{
    tRmNotificationMsg  NotifMsg;
    tRmHbMsg            RmHbMsg;

    MEMSET (&RmHbMsg, 0, sizeof (tRmHbMsg));
    MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));

    /* ACTIVE side processing */
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleFSWEvent ENTRY\r\n");
    HB_TRC (HB_CTRL_PATH_TRC,
            "HbHandleFSWEvent: Force switchover event from "
            "ACTIVE RM core module\r\n");

    if ((HB_GET_NODE_STATE () != HB_RM_TRANSITION_IN_PROGRESS) &&
        (HB_GET_PREV_NODE_STATE () != HB_RM_ACTIVE))
    {
        MEMSET (&NotifMsg, 0, sizeof (tRmNotificationMsg));
        NotifMsg.u4NodeId = HB_GET_SELF_NODE_ID ();
        NotifMsg.u4State = HB_RM_ACTIVE;
        NotifMsg.u4AppId = RM_APP_ID;
        NotifMsg.u4Operation = RM_NOTIF_SWITCHOVER;
        NotifMsg.u4Status = RM_FAILED;
        NotifMsg.u4Error = RM_NONE;
        UtlGetTimeStr (NotifMsg.ac1DateTime);
        STRCPY (NotifMsg.ac1ErrorStr,
                "Force-Switchover is failed as RM HB node state "
                "is not in transition in progress");
        RmApiTrapSendNotifications (&NotifMsg);
        RmHbMsg.u4Evt = RM_FSW_FAILED_IN_ACTIVE;
        RmApiSendHbEvtToRm (&RmHbMsg);
        return;
    }
    if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
    {
        HB_TRC (HB_CTRL_PATH_TRC,
                "HbHandleFSWEvent: " "Force switchover in progress state\r\n");
        return;
    }
    if (HB_GET_PEER_NODE_ID () == 0)
    {
        /* Peer is dead but Force switchover event is received 
         * from RM core module */
        HB_TRC (HB_CTRL_PATH_TRC, "HbHandleFSWEvent: " "STANDBY is dead\r\n");
        return;
    }
    HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_OCCURED;
    HB_SET_FLAGS (HB_FORCE_SWITCHOVER);

    if (HbFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbHandleFSWEvent: "
                "Sending force-switchover-ack to peer failed !!!\r\n");
    }
    if (HbTmrStartTimer (HB_FSW_ACK_TIMER, HB_MAX_FSW_ACK_TIME) == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbHandleFSWEvent: FSW ACK timer start failed\r\n");
        return;
    }
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleFSWEvent EXIT\r\n");
}

/******************************************************************************
 * Function           : HbHandleRxBufProcessedEvt 
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine performs the following
 *                      1. Force Switchover handle after processing RX messages
 *                      2. Failover handle after processing RX messages
 *                      finally changes the node state to ACTIVE.
 ******************************************************************************/
VOID
HbHandleRxBufProcessedEvt (VOID)
{
    tRmHbMsg            RmHbMsg;

    MEMSET (&RmHbMsg, 0, sizeof (tRmHbMsg));
    /* STANDBY side processing */
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleRxBufProcessedEvt ENTRY\r\n");

    if ((HB_GET_NODE_STATE () != HB_RM_TRANSITION_IN_PROGRESS) &&
        (HB_GET_PREV_NODE_STATE () != HB_RM_STANDBY))
    {
        HB_TRC (HB_CRITICAL_TRC, "RM HB module is not in "
                "HB_RM_TRANSITION_IN_PROGRESS state after processing "
                "RX buffer message in RM core module\r\n");
        return;
    }
    HB_RESET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);

    HB_SET_ACTIVE_NODE_ID (HB_GET_SELF_NODE_ID ());
    if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_OCCURED)
    {
        HB_FORCE_SWITCHOVER_FLAG () = RM_FSW_NOT_OCCURED;
        RmHbMsg.u4Evt = RM_FSW_RCVD_IN_STANDBY;
        RmApiSendHbEvtToRm (&RmHbMsg);
        if (HbSendMsgToPeer (HB_FORCE_SWITCHOVER_ACK) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Sending fore-switchover-ack to "
                    "peer failed !!!\r\n");
        }
    }
    else if (HB_FORCE_SWITCHOVER_FLAG () == RM_FSW_NOT_OCCURED)
    {
        /* 1. In Failover case, this event is received after processing the 
         * Rx buffer messages to give GO_ACTIVE to RM core module.
         * 2. When FSW flag is received in HB msg and indication to 
         * RM core module given for RX buffer processing, 
         * before getting the Rx buffer processed event from
         * RM core module, peer dead OR FSW RX buffer processing timer expiry 
         * might have set 
         * HB_FORCE_SWITCHOVER_FLAG () as RM_FSW_NOT_OCCURED */
    }
    HbRmUpdateState (HB_RM_ACTIVE);
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleRxBufProcessedEvt EXIT\r\n");
}

/******************************************************************************
 * Function           : HbHandleAppInitCompleted
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Routine to handle RM shut and start completed.
 ******************************************************************************/
VOID
HbHandleAppInitCompleted (VOID)
{
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleAppInitCompleted: ENTRY \r\n");

    if (HB_GET_PEER_NODE_ID () == 0)
    {
#ifdef CLI_WANTED
        /*CLI will be waiting for me to complete.. Let him resume. 
         *Standby case this will be done after static configuration
         *restoration */
        CliInformRestorationComplete ();
#endif
        HbRmUpdateState (HB_RM_ACTIVE);
    }
    else
    {
        HbRmUpdateState (HB_RM_STANDBY);
    }
    HB_RESET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleAppInitCompleted: EXIT \r\n");
    return;
}

/******************************************************************************
 * Function           : HbChgNodeStateToTransInPrgs
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None 
 * Action             : Routine to change RM HB level node state to 
 *                      HB_RM_TRANSITION_IN_PROGRESS.
 ******************************************************************************/
VOID
HbChgNodeStateToTransInPrgs (VOID)
{
    HB_TRC (HB_CTRL_PATH_TRC, "HbChgNodeStateToTransInPrgs: ENTRY \r\n");
    if (HB_GET_PEER_NODE_ID () != 0)
    {
        HB_SET_PREV_NODE_STATE (HB_GET_NODE_STATE ());
        HB_SET_NODE_STATE (HB_RM_TRANSITION_IN_PROGRESS);
        HB_SET_FLAGS (HB_TRANS_IN_PROGRESS_FLAG);
    }
    HB_TRC (HB_CTRL_PATH_TRC, "HbChgNodeStateToTransInPrgs: EXIT \r\n");
}

/******************************************************************************
 * Function           : HbRmUpdateForceStandbyState
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None 
 * Action             : Routine to change RM HB level node state to 
 *                      HB_RM_STANDBY forcefully.
 ******************************************************************************/
VOID
HbRmUpdateForceStandbyState (VOID)
{
#ifdef RM_WANTED
    tRmHbMsg            RmHbMsg;
#endif
    HB_TRC (HB_CTRL_PATH_TRC, "HbRmUpdateForceStandbyState: ENTRY \r\n");

    HB_SET_PREV_NODE_STATE (HB_GET_NODE_STATE ());
    HB_SET_NODE_STATE (HB_RM_STANDBY);
    HB_TRC1 (HB_EVENT_TRC, " HbRmUpdateForceStandbyState = %d\r\n",
             HB_GET_NODE_STATE ());
#ifdef RM_WANTED
    RmHbMsg.u4Evt = GO_STANDBY;
    RmApiSendHbEvtToRm (&RmHbMsg);
#endif
    HB_TRC (HB_CTRL_PATH_TRC, "HbRmUpdateForceStandbyState: EXIT \r\n");
}

#endif

/******************************************************************************
 * Function           : HbNotifyPeerInfo
 * Input(s)           : u1Event - Events like RM_STANDBY_UP/RM_STANDBY_DOWN.
 * Output(s)          : None.
 * Returns            : Nome. 
 * Action             : Routine used by RM to notify Standby state and 
 *                      peer address to protocols.
 ******************************************************************************/
UINT4
HbNotifyPeerInfo (UINT4 u4PeerAddr, UINT1 u1Event)
{
#ifdef RM_WANTED
    tRmHbMsg            RmHbMsg;

    /* Peer count need to maintained irrespective of the Node status */
    if (HB_GET_PEER_NODE_STATE () != u1Event)
    {
        HB_SET_PEER_NODE_STATE (u1Event);
        HB_TRC2 (HB_CTRL_PATH_TRC, "HbNotifyPeerInfo: Peer id=%x "
                 "and status=%d\r\n", u4PeerAddr, u1Event);
        RmHbMsg.u4PeerAddr = u4PeerAddr;
        RmHbMsg.u4Evt = u1Event;
        RmApiSendHbEvtToRm (&RmHbMsg);
        /* Send peer up/down indication to RM task */
    }
#else
    UNUSED_PARAM (u4PeerAddr);
    UNUSED_PARAM (u1Event);
#endif
    return HB_SUCCESS;
}

#ifdef ICCH_WANTED
/******************************************************************************
 * Function           : HbIcchHandleRxBufProcessedEvt 
 * Input(s)           : None 
 * Output(s)          : None
 * Returns            : None
 * Action             : This routine performs the following
 *                       Failover handle after processing RX messages
 *                      finally changes the node state to MASTER.
 ******************************************************************************/
VOID
HbIcchHandleRxBufProcessedEvt (VOID)
{
    tIcchHbMsg          IcchHbMsg;

    MEMSET (&IcchHbMsg, 0, sizeof (tIcchHbMsg));
    HB_TRC (HB_CTRL_PATH_TRC, "HbIcchHandleRxBufProcessedEvt ENTRY\r\n");

    if ((HB_ICCH_GET_NODE_STATE () != HB_ICCH_TRANSITION_IN_PROGRESS) &&
        (HB_ICCH_GET_PREV_NODE_STATE () != HB_ICCH_SLAVE))
    {
        HB_TRC (HB_CRITICAL_TRC, "RM HB module is not in "
                "HB_ICCH_RM_TRANSITION_IN_PROGRESS state after processing "
                "RX buffer message in ICCH core module\r\n");
        return;
    }

    HB_ICCH_SET_MASTER_NODE_ID (HB_ICCH_GET_SELF_NODE_ID ());
    /* 1. In Failover case, this event is received after processing the 
     * Rx buffer messages to give GO_MASTER to ICCH core module.
     */
    HbIcchUpdateState (HB_ICCH_MASTER);
    HB_TRC (HB_CTRL_PATH_TRC, "HbHandleRxBufProcessedEvt EXIT\r\n");
    return;
}

/******************************************************************************
 * Function           : HbProcessIcchMsg
 * Input(s)           : pHbData - Rcvd HB msg.
 *                      u4DataLen - Length of pHbData
 *                      u4PeerAddr - Addr of the peer from which the msg arrived
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the HeartBeat msg received from peer.
 ******************************************************************************/
VOID
HbProcessIcchMsg (tHbMsg * pHbData)
{
    UINT1               u1Event = HB_MAX_EVENTS;    /* initialized to invalid value */
    UINT4               u4SelfNodeIp = 0;
    UINT4               u4Maxmask = HB_ICCH_GET_SELF_NODE_MASK ();
    BOOL1               bRestartPeerDeadTmr = OSIX_FALSE;
    tHbData             HbData;
    tIcchHbMsg          IcchHbMsg;
    tRegPeriodicTxInfo  RegnInfo;

    MEMSET (&RegnInfo, 0, sizeof (tRegPeriodicTxInfo));
    MEMSET (&IcchHbMsg, 0, sizeof (tIcchHbMsg));
    MEMSET (&HbData, 0, sizeof (tHbData));

    /* Copy the CRU buf content to linear buffer and free the CRU buf. */
    if (HB_COPY_TO_LINEAR_BUF (pHbData, &HbData, 0, sizeof (tHbData))
        == CRU_FAILURE)
    {
        HB_TRC (HB_CRITICAL_TRC, "CRU to linear copy failure\r\n");
        HB_INCR_MSG_RX_FCOUNT ();
        HB_FREE (pHbData);
        return;
    }

    HB_FREE (pHbData);

    /* Convert the rcvd msg from NETWORK byte order to HOST byte order */
    HbData.u4MsgType = OSIX_NTOHL (HbData.u4MsgType);
    HbData.u4HbInterval = OSIX_NTOHL (HbData.u4HbInterval);
    HbData.u4PeerDeadInterval = OSIX_NTOHL (HbData.u4PeerDeadInterval);
    HbData.u4PeerDeadIntMultiplier =
        OSIX_NTOHL (HbData.u4PeerDeadIntMultiplier);
    HbData.u4MasterNode = OSIX_NTOHL (HbData.u4MasterNode);
    HbData.u4IcchSelfNode = OSIX_NTOHL (HbData.u4IcchSelfNode);
    HbData.u4IcchSelfMask = OSIX_NTOHL (HbData.u4IcchSelfMask);
    HbData.u4IcchPeerNode = OSIX_NTOHL (HbData.u4IcchPeerNode);
    HbData.u4Flags = OSIX_NTOHL (HbData.u4Flags);

    if (HbData.u4IcchSelfMask > HB_ICCH_GET_SELF_NODE_MASK ())
    {
        u4Maxmask = HbData.u4IcchSelfMask;
    }
    /*The mask value will be initially zero for both MC-LAG nodes.It will be exchanged as part of HB message.
     * When HB message is processed, check if the mask value is zero for local node and in received HB message.
     * This indicates both nodes have not started using mask value.

     * If it is true then check node state ,if it is master use MASK_VALUE_1.It will be communicated 
     * to next node via HB message.*/
    if ((gu2PortMaskValue == 0))
    {
        if ((HbData.u2MclagPortMaskValue == 0) &&
            (HB_ICCH_GET_NODE_STATE () == HB_ICCH_MASTER))
        {

            gu2PortMaskValue = HB_MASK_VALUE_1;
        }
        /* If the other node is using a mask value then use the next mask value 
         * Check if the mask value is zero for local node and received HB message mask value is MASK_VALUE_1.
         * This indicated MASK_VALUE_1 is used by other MC-LAG node.Hence use MASK_VALUE_2*/

        if (HbData.u2MclagPortMaskValue == HB_MASK_VALUE_1)
        {
            gu2PortMaskValue = HB_MASK_VALUE_2;
        }

        /* Check if the mask value is zero for local node and received HB message mask value is MASK_VALUE_2.
         * This indicated MASK_VALUE_2 is used by other MC-LAG node.Hence use MASK_VALUE_1.*/

        if (HbData.u2MclagPortMaskValue == HB_MASK_VALUE_2)
        {
            gu2PortMaskValue = HB_MASK_VALUE_1;
        }
    }

    if ((HbData.u4IcchSelfNode == HB_ICCH_GET_SELF_NODE_ID ()) ||
        ((HbData.u4IcchSelfNode & u4Maxmask) !=
         ((HB_ICCH_GET_SELF_NODE_ID ()) & u4Maxmask)))
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "Self Node and Peer node network differs or "
                "Self Node and Peer Node has same IP address\r\n");
        HB_INCR_MSG_RX_FCOUNT ();

        return;
    }

    if (HbData.u4Flags & HB_ICCH_INIT_STATE)
    {
        HB_ICCH_RESET_FLAGS (HB_ICCH_MASTER_STATE);
        HB_ICCH_RESET_FLAGS (HB_ICCH_SLAVE_STATE);
        HB_ICCH_SET_FLAGS (HB_ICCH_INIT_STATE);
    }
    else if (HbData.u4Flags & HB_ICCH_MASTER_STATE)
    {
        HB_ICCH_RESET_FLAGS (HB_ICCH_INIT_STATE);
        HB_ICCH_RESET_FLAGS (HB_ICCH_SLAVE_STATE);
        HB_ICCH_SET_FLAGS (HB_ICCH_MASTER_STATE);
    }
    else if (HbData.u4Flags & HB_ICCH_SLAVE_STATE)
    {
        HB_ICCH_RESET_FLAGS (HB_ICCH_INIT_STATE);
        HB_ICCH_RESET_FLAGS (HB_ICCH_MASTER_STATE);
        HB_ICCH_SET_FLAGS (HB_ICCH_SLAVE_STATE);
    }
    HB_INCR_MSG_RX_COUNT ();

    /* If there is a change in HeartBeat/PeerDead interval in the HB msg rcvd 
     * from ACTIVE node, change the values accordingly in peer node 
     * to be in sync. */
    /* Allow to change the HB interval and peer dead interval 
     * only when the proper values (refer the below "if" check) 
     * are obtained in the HB message. */
    if ((HB_ICCH_GET_NODE_STATE () != HB_ICCH_MASTER) &&
        (!((HbData.u4MasterNode == 0) &&
           (HbData.u4IcchPeerNode /* 0 */  != HB_ICCH_GET_SELF_NODE_ID ()) &&
           (HbData.u4IcchSelfNode == HB_ICCH_GET_PEER_NODE_ID ()) &&
           (HB_ICCH_GET_NODE_STATE () == HB_ICCH_SLAVE))))
    {
        if (HbData.u4HbInterval != HB_GET_HB_INTERVAL ())
        {
            HB_TRC1 (HB_CRITICAL_TRC,
                     "!!! Heart Beat Interval of ACTIVE node is modified. "
                     "Updating the Heart Beat Interval to %dms ...\r\n",
                     HbData.u4HbInterval);
            HB_SET_HB_INTERVAL (HbData.u4HbInterval);

            /* HB Timer to be restarted only when hb interval is > 10 ms */
            /* HB module to register with CFA only when hb interval is = 10 ms */
            if (HB_GET_HB_INTERVAL () == HB_MIN_INTERVAL)
            {
#ifdef ICCH_WANTED
                RegnInfo.SendToApplication = HbTxIcchTsk;
#endif
            }
            else
            {
                if (HbTmrRestartTimer (HB_ICCH_HEARTBEAT_TIMER,
                                       HB_GET_HB_INTERVAL ()) == HB_FAILURE)
                {
                    HB_TRC (HB_ALL_FAILURE_TRC,
                            "!!! Heart Beat re-start timer Failure !!!\r\n");
                }
            }
        }
        if (HbData.u4PeerDeadInterval != HB_GET_PEER_DEAD_INTERVAL ())
        {
            HB_TRC1 (HB_CRITICAL_TRC,
                     "!!! Peer Dead Interval of ACTIVE node is modified. "
                     "Updating the Peer Dead Interval to %dms ...\r\n",
                     HbData.u4PeerDeadInterval);
            HB_SET_PEER_DEAD_INTERVAL (HbData.u4PeerDeadInterval);
            bRestartPeerDeadTmr = OSIX_TRUE;
        }

        if (HbData.u4PeerDeadIntMultiplier !=
            HB_GET_PEER_DEAD_INT_MULTIPLIER ())
        {
            HB_TRC1 (HB_CRITICAL_TRC,
                     "!!! Peer Dead Interval Multiplier of ACTIVE node is "
                     "modified. Updating the Peer Dead Interval "
                     "Multiplier to %d ...\r\n",
                     HbData.u4PeerDeadIntMultiplier);
            HB_SET_PEER_DEAD_INT_MULTIPLIER (HbData.u4PeerDeadIntMultiplier);
            bRestartPeerDeadTmr = OSIX_TRUE;
        }
    }
    if (bRestartPeerDeadTmr == OSIX_TRUE)
    {
        if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                               HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Peer Dead restart timer Failure !!!\r\n");
            HB_INCR_MSG_RX_FCOUNT ();
            return;
        }
    }

    /* MASTER NODE CHECK,  
     * Below 'if' check is added to detect 
     * whether the peer is dead or not when the SLAVE node reboots 
     * in less than peer dead time. */
    if ((HbData.u4MasterNode == 0) &&
        (HbData.u4IcchPeerNode /* 0 */  != HB_ICCH_GET_SELF_NODE_ID ()) &&
        (HbData.u4IcchSelfNode == HB_ICCH_GET_PEER_NODE_ID ()) &&
        (HB_ICCH_GET_NODE_STATE () == HB_ICCH_MASTER))
    {
        /* SLAVE node is rebooted before MASTER detects 
         * the peer is dead */
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! Slave node is rebooted before MASTER detects "
                "the peer is dead !!!\r\n");
        HbIcchTmrHdlPeerDeadTmrExp (NULL);
    }

    /* Read the NodeId and Priority information. */
    u4SelfNodeIp = HB_ICCH_GET_SELF_NODE_ID ();

    /* Below 'if' check is added to detect 
     * whether the peer is dead or not when the MASTER node reboots 
     * in less than peer dead time. */
    if ((HbData.u4MasterNode == 0) &&
        (HbData.u4IcchPeerNode /* 0 */  != HB_ICCH_GET_SELF_NODE_ID ()) &&
        (HbData.u4IcchSelfNode == HB_ICCH_GET_PEER_NODE_ID ()) &&
        (HB_ICCH_GET_NODE_STATE () == HB_ICCH_SLAVE))
    {
        /* SLAVE NODE CHECK 
         * MASTER node is rebooted before STANDBY detects the 
         * peer is dead*/
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! MASTER node is rebooted before SLAVE detects "
                "the peer is dead !!!\r\n");
        HbIcchTmrHdlPeerDeadTmrExp (NULL);
        u1Event = HB_IGNORE_PKT;
    }
    else if (HbData.u4IcchPeerNode == 0)
    {
        /* Peer node has not learnt about me. 
         * But this node has seen the peer node */
        u1Event = HB_1WAY_RCVD;
    }
    else if ((HbData.u4Flags & HB_SLAVE_PEER_UP) &&
             (HB_ICCH_GET_NODE_STATE () == HB_ICCH_MASTER))
    {
        IcchHbMsg.u4PeerAddr = HB_ICCH_GET_PEER_NODE_ID ();
        IcchHbMsg.u4Evt = ICCH_PEER_UP;
        IcchApiSendHbEvtToIcch (&IcchHbMsg);
        if (HbIcchSendMsgToPeer (HB_SLAVE_PEER_UP_ACK) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "Sending Slave node "
                    "peer up flag failed !!!\r\n");
        }
    }
    else if ((HbData.u4Flags & HB_SLAVE_PEER_UP_ACK) &&
             (HB_ICCH_GET_NODE_STATE () != HB_ICCH_MASTER))
    {
        IcchApiInitiateBulkRequest ();
        HB_RESET_FLAGS (HB_SLAVE_PEER_UP);
    }
    else if (HbData.u4IcchPeerNode == u4SelfNodeIp)
    {
        /* Rcvd pkt's peer node has my ip addr. Therefore, 
         * peer node has learnt about me & I have seen the peer node. */
        /* Go for normal election based on NodeId.
         */
        HbIcchProcessNodeId (&HbData, &u1Event);
    }
    else
    {
        /* Discard this packet */
        u1Event = HB_IGNORE_PKT;
        HB_TRC (HB_ALL_FAILURE_TRC,
                "!!! HB rcvd from invalid Peer. Discarding.... !!!\r\n");
        HB_INCR_MSG_RX_FCOUNT ();
        return;
    }
    if (HB_ICCH_PEER_NEWLY_ARRIVED == OSIX_TRUE)
    {
        HB_ICCH_PEER_NEWLY_ARRIVED = OSIX_FALSE;
    }

    HB_TRC2 (HB_EVENT_TRC, "FSM Event: %d  State: %d\r\n",
             u1Event, HB_ICCH_GET_NODE_STATE ());
    /* StateMachine is initiated only if the event is valid */
    if (u1Event < HB_ICCH_MAX_EVENTS)
    {
        HbIcchStateMachine (u1Event);
    }
    if (HbTmrRestartTimer (HB_ICCH_PEER_DEAD_TIMER,
                           HB_GET_PEER_DEAD_INTERVAL ()) == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! Peer Dead restart timer "
                "Failure !!!\r\n");
    }
    HB_INCR_MSG_RX_PCOUNT ();

    return;
}

/******************************************************************************
 * Function           : HbIcchProcessRcvEvt
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Routine to process the received HB messages.
 ******************************************************************************/
VOID
HbIcchProcessRcvEvt (VOID)
{
    tHbMsg             *pPkt = NULL;
    UINT4               u4PeerAddr = 0;
    UINT4               u4MsgType = 0;
    UINT1               u1IpVerHdr = 0;
    UINT4               u4IpHdrLen = 0;
    UINT1               au1RcvPkt[MAX_IP_HB_PKT_LEN];
    INT4                i4RecvBytes = 0;
    UINT4               u4SrcEntId = 0;
    UINT4               u4DestEntId = 0;
    UINT4               u4TotLen = 0;
    UINT2               u2Cksum = 0;

    while ((i4RecvBytes =
            HbRawIcchSockRcv (au1RcvPkt, MAX_IP_HB_PKT_LEN, &u4PeerAddr)) > 0)
    {
        /* Abort if i4RecvBytes > MAX_IP_HB_PKT_LEN */
        FSAP_ASSERT (i4RecvBytes <= MAX_IP_HB_PKT_LEN);

        /* Compare the SrcIp of the rcvd pkt and the SelfNodeIp. 
         * Drop the self-originated packets. */
        if (u4PeerAddr == HB_ICCH_GET_SELF_NODE_ID ())
        {
            continue;
        }

        /* Discard the pkts if the peer node IP and self node IP 
         * are not in same subnet. This is especially added to 
         * test the exe on Linux environment. i.e., to run more 
         * than 2 exes on the same m/c. 
         * Therefore configure the backplane interface IP with
         * the same subnet. 
         * N11: 127.1.0.1, N12: 127.1.0.2 - Chassis 1
         * N21: 127.2.0.1, N22: 127.2.0.2 - Chassis 2 */
        if ((u4PeerAddr & HB_ICCH_GET_SELF_NODE_MASK ()) !=
            (HB_ICCH_GET_SELF_NODE_ID () & HB_ICCH_GET_SELF_NODE_MASK ()))
        {
            continue;
        }

        /* Allocate memory for CRU buffer. This mem will be freed by appls. 
         * after processing. So, do not free this mem here. */
        if ((pPkt = HB_ALLOC_RX_BUF ((UINT4) (i4RecvBytes))) == NULL)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "EXIT: Unable to allocate buf\r\n");
            break;
        }

        /* copy the linear buffer to CRU buffer */
        if (HB_COPY (pPkt, au1RcvPkt, (UINT4) (i4RecvBytes)) == CRU_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "EXIT: Unable to copy linear to CRU buf\r\n");
            HB_FREE (pPkt);
            break;
        }

        HB_GET_DATA_1_BYTE (pPkt, IP_PKT_OFF_HLEN, u1IpVerHdr);
        u4IpHdrLen = ((UINT4) ((u1IpVerHdr & 0x0F) * 4));

        /* Received pkt has [IP Hdr + Hb Hdr + Hb Data].
         * Strip off the IP header and extract the HbMsg from the rcvd pkt */
        HB_PKT_MOVE_TO_DATA (pPkt, u4IpHdrLen);

        /* IP hdr is already stripped off. Now the pkt has [HB hdr + HB data].
         * Extract the HB hdr info.  Verify checksum */
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_TOT_LENGTH, u4TotLen);

        u2Cksum = HB_CALC_CKSUM (pPkt, u4TotLen, 0);
        if (u2Cksum != 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                    "!!! Pkt rcvd with invalid cksum. Discarding. !!!\r\n");
            HB_FREE (pPkt);
            continue;
        }

        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_SRC_ENTID, u4SrcEntId);
        HB_GET_DATA_4_BYTE (pPkt, HB_HDR_OFF_DEST_ENTID, u4DestEntId);

        /* Now the pkt has [HB Hdr + HB data]. Strip off HB header */
        HB_PKT_MOVE_TO_DATA (pPkt, HB_HDR_LENGTH);

        if (u4DestEntId >= ICCH_MAX_APPS)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "DestEntId invalid in rcvd pkt\r\n");
            HB_FREE (pPkt);
            continue;
        }

        /* Send RM data to appls. based on the dest entity id. for processing */
        if (u4DestEntId == ICCH_APP_ID)
        {
            /* Get the message type to distinguish between the packets
             * received */
            HB_GET_DATA_4_BYTE (pPkt, 0, u4MsgType);

            if (u4MsgType == HEART_BEAT_MSG)
            {
                HbProcessIcchMsg (pPkt);
            }
            else
            {
                HB_FREE (pPkt);
            }

        }

        else
        {
            HB_FREE (pPkt);
        }
    }
}

/******************************************************************************
 * Function           : HbTxIcchTsk
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Call back function invoked by cfa to tx icch hb msgs
 ******************************************************************************/
VOID
HbTxIcchTsk (VOID)
{
    if (HbIcchFormAndSendMsg () == HB_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC,
                "HbTxTsk: " "Sending hb msg to peer failed !!!\r\n");
    }
}

/******************************************************************************
 * Function           : HbIcchNotifyPeerInfo
 * Input(s)           : u1Event - Events like ICCH_STANDBY_UP/ICCH_STANDBY_DOWN.
 *                      u4PeerAddr
 * Output(s)          : None.
 * Returns            : HB_SUCCESS/HB_FAILURE. 
 * Action             : Routine used by ICCH to notify Standby state and 
 *                      peer address to protocols.
 ******************************************************************************/
UINT4
HbIcchNotifyPeerInfo (UINT4 u4PeerAddr, UINT1 u1Event)
{
    tIcchHbMsg          IcchHbMsg;
    UINT4               u4IfIndex = 0;

    MEMSET (&IcchHbMsg, 0, sizeof (tIcchHbMsg));

    IcchGetIcclIfIndex (&u4IfIndex);
    if (u1Event == ICCH_PEER_UP)
    {
        /* Create FDB entry with ICCL If index for all the available VLANs */
        CfaUpdateIcclFdbEntry (gHbInfo.au1DestMac, u4IfIndex, HB_FDB_ADD);
    }
    else if (u1Event == ICCH_PEER_DOWN)
    {
        /* Delete FDB entry with ICCL If index for all the available VLANs */
        CfaUpdateIcclFdbEntry (gHbInfo.au1DestMac, u4IfIndex, HB_FDB_DEL);
    }

    /* Peer count need to maintained irrespective of the Node status */
    if ((HB_ICCH_GET_PEER_NODE_STATE () != u1Event) ||
        (IcchApiGetPeerAddress () == 0))
    {
        HB_ICCH_SET_PEER_NODE_STATE (u1Event);
        HB_TRC2 (HB_CTRL_PATH_TRC, "HbIcchNotifyPeerInfo: Peer id=%x "
                 "and status=%d\r\n", u4PeerAddr, u1Event);
        IcchHbMsg.u4PeerAddr = u4PeerAddr;
        IcchHbMsg.u4Evt = u1Event;
        IcchApiSendHbEvtToIcch (&IcchHbMsg);
        /* Send peer up/down indication to ICCH task */
    }
    return HB_SUCCESS;
}

/******************************************************************************
 * Function           : HbIcchRawPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a HB raw socket packet 
                        arrives.
 ******************************************************************************/
VOID
HbIcchRawPktRcvd (INT4 i4SockFd)
{
    HB_TRC (HB_SOCKET_API_TRC, "HbIcchRawPktRcvd: Packet on "
            "raw socket entry...\r\n");
    UNUSED_PARAM (i4SockFd);
    if (HB_SEND_EVENT (HB_TASK_ID, HB_ICCH_PKT_RCVD) == OSIX_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbIcchRawPktRcvd: Send Event HB_PKT_RCVD "
                "-- Failed\r\n");
        return;
    }
    HB_TRC (HB_SOCKET_API_TRC, "HbIcchRawPktRcvd: Packet on "
            "raw socket exit...\r\n");
}

#endif

/*****************************************************************************/
/* Function Name      : RmHbLock                                             */
/*                                                                           */
/* Description        : This function is used to take the RM HB mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : HB_SUCCESS or HB_FAILURE                             */
/*                                                                           */
/* Called By          : HB, Protocols registered with HB, SNMP & CLI         */
/*****************************************************************************/
INT4
HbLock (VOID)
{
    if (OsixSemTake (HB_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        HB_TRC (HB_CRITICAL_TRC, "Failed to Take HB Protocol "
                "Mutual Exclusion Sema4 \r\n");
        return HB_FAILURE;
    }

    return HB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RmHbUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the RM HB mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : HB_SUCCESS or HB_FAILURE                             */
/*                                                                           */
/* Called By          : HB, Protocols registered with HB, SNMP & CLI         */
/*****************************************************************************/
INT4
HbUnLock (VOID)
{
    if (OsixSemGive (HB_PROTO_SEM ()) != OSIX_SUCCESS)
    {
        HB_TRC (HB_CRITICAL_TRC, "Failed to Give HB Protocol "
                "Mutual Exclusion Sema4 \r\n");
        return HB_FAILURE;
    }

    return HB_SUCCESS;
}

/******************************************************************************
 * Function           : HbRawPktRcvd
 * Input(s)           : i4SockFd - socket descriptor.
 * Output(s)          : None.
 * Returns            : None. 
 * Action             : Callback function registered to SELECT library.
 *                      This function is invoked when a HB raw socket packet arrives. 
 ******************************************************************************/
VOID
HbRawPktRcvd (INT4 i4SockFd)
{
    HB_TRC (HB_SOCKET_API_TRC, "HbRawPktRcvd: Packet on "
            "raw socket entry...\r\n");
    UNUSED_PARAM (i4SockFd);
    if (HB_SEND_EVENT (HB_TASK_ID, HB_PKT_RCVD) == OSIX_FAILURE)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbRawPktRcvd: Send Event HB_PKT_RCVD "
                "-- Failed\r\n");
        return;
    }
    HB_TRC (HB_SOCKET_API_TRC, "HbRawPktRcvd: Packet on "
            "raw socket exit...\r\n");
}

/********************************************************************************
 *    Function Name      : RmHbCallBackFn
 *
 *    Description        : This function is invoked whenever RM HB packet is 
 *                         received in the driver. This will take care of 
 *                         posting the packet in CruBuffer format to RM
 *
 *    Input(s)           : pBuf - pointer to the RM HB Data
 *                           
 *                            
 *
 *    Output(s)          : None
 *
 *    Returns            : OSIX_SUCCESS / OSIX_FAILURE
 ********************************************************************************/

INT4
RmHbCallBackFn (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4RetVal = OSIX_FAILURE;

    /* post the buffer to RM queue */
    u4RetVal =
        HB_SEND_TO_QUEUE (HB_QUEUE_ID, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN);

    if (u4RetVal != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    /* trigger the interuppt event for RM */
    if (OsixEvtSend (HB_TASK_ID, HB_PKT_RCVD_ON_ETH_PORT) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
#endif /* _HBMAIN_C_ */
