/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbvxsock.c,v 1.2 2015/11/03 10:59:11 siva Exp $
*
* Description: Heart beat module socket interface functions.
*********************************************************************/

#include "hbincs.h"
#ifdef RM_WANTED
/****************************************************************************
 * Function           : HbRawRmSockInit 
 * Input(s)           : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)          : RM HB Raw socket descriptor.
 * Returns            : RM_SUCCESS/RM_FAILURE 
 * Action             : Routine to create a RAW IP socket, set socket options 
 *                      for  sending and receiving RM multicast packets.
 ****************************************************************************/
INT4
HbRawRmSockInit (INT4 *pi4HbSockFd)
{
    UNUSED_PARAM (pi4HbSockFd);
    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbRawSockSend 
 * Input(s)           : pu1Data - pointer to the data to be sent out
 *                      u2PktLen - Length of the data
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send the packet out thru' RAW IP socket.
 *                      This is socket is specifically used by RM module to
 *                      send the Heart Beat messages destined to the 
 *                      multicast ip address 224.0.0.250.
 ******************************************************************************/
UINT4
HbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2PktLen);
    UNUSED_PARAM (u4DestAddr);
    return (HB_SUCCESS);
}

/******************************************************************************
 * Function           : HbRawSockRcv 
 * Input(s)           : pu1Data - pointer to rcv the packet
 *                      u2BufSize - size of the buffer to be received.
 * Output(s)          : pu1Data - pointer to the packet rcvd. 
 *                      pu4PeerAddr - IP addr of the peer from which the 
 *                                    pkt is rcvd.   
 * Returns            : Length of the packet received.
 * Action             : Routine to receive RM protocol packets.
 ******************************************************************************/
INT4
HbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufSize);
    UNUSED_PARAM (pu4PeerAddr);
    return (0);
}

/******************************************************************************
 * Function           : RmHbCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for RM.
 ******************************************************************************/
INT4
RmHbCloseSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    return (-1);
}

/******************************************************************************
 * Function           : RmHbGetIfIpInfo
 * Input(s)           : pu1IfName - Interface name
 * Output(s)          : pNodeInfo - Node information.
 * Returns            : RM_SUCCESS or RM_FAILURE.
 * Action             : Routine to get the ip addr of this node.
 *******************************************************************************/
UINT4
RmHbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo)
{
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (pNodeInfo);
    return (RM_SUCCESS);
}


/****************************************************************************
 * Function           : RmHbRawSockInit
 * Input(s)           : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)          : RM HB Raw socket descriptor.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to create a RAW IP socket, set socket options
 *                     for  sending and receiving RM multicast packets.
 *****************************************************************************/
INT4
RmHbRawSockInit (INT4 *pi4HbSockFd)
{
    UNUSED_PARAM (pi4HbSockFd);
    return (RM_SUCCESS);
}


/******************************************************************************
 * Function           : RmHbRawSockSend
 * Input(s)           : pu1Data - pointer to the data to be sent out
 *                      u2PktLen - Length of the data
 * Output(s)          : None.
 * Returns            : RM_SUCCESS/RM_FAILURE
 * Action             : Routine to send the packet out thru' RAW IP socket.
 *                      This is socket is specifically used by RM module to
 *                      send the Heart Beat messages destined to the
 *                      multicast ip address 224.0.0.250.
 *******************************************************************************/
UINT4
RmHbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2PktLen);
    UNUSED_PARAM (u4DestAddr);
    return (RM_SUCCESS);
}


/******************************************************************************
 * Function           : RmHbRawSockRcv
 * Input(s)           : pu1Data - pointer to rcv the packet
 *                      u2BufSize - size of the buffer to be received.
 * Output(s)          : pu1Data - pointer to the packet rcvd.
 *                      pu4PeerAddr - IP addr of the peer from which the
 *                                    pkt is rcvd.
 * Returns            : Length of the packet received.
 * Action             : Routine to receive RM protocol packets.
 *******************************************************************************/
INT4
RmHbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufSize);
    UNUSED_PARAM (pu4PeerAddr);
    return (0);
}



#endif

/******************************************************************************
 * Function           : HbCloseSocket
 * Input(s)           : i4SockFd - Socket descriptor.
 * Output(s)          : None.
 * Returns            : 0 on success & -1 on failure
 * Action             : Routine to close the socket created for RM.
 *       ******************************************************************************/
INT4
HbCloseSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    return (-1);

}


/******************************************************************************
 * Function           : HbGetIfIndexFromName
 * Input(s)           : pu1IfName - Interface name.
 * Output(s)          : None.
 * Returns            : IfIndex
 * Action             : Routine to get the Interface index of the given
 *                      Interface name.
 * ******************************************************************************/
UINT4
HbGetIfIndexFromName (UINT1 *pu1IfName)
{
    UNUSED_PARAM (pu1IfName);
    return (0);

}

/******************************************************************************
 * Function           : HbGetIfIpInfo
 * Input(s)           : pu1IfName - Interface name
 * Output(s)          : pNodeInfo - Node information.
 * Returns            : HB_SUCCESS or HB_FAILURE.
 * Action             : Routine to get the ip addr of this node.
 *******************************************************************************/
UINT4
HbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo)
{

    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (pNodeInfo);
    return (HB_SUCCESS);

}


#ifdef ICCH_WANTED


/******************************************************************************
 * Function                   : HbRawIcchSockInit
 * Action                     : Routine to create a RAW IP socket, set socket
 *                              options for  sending and receiving RM multicast
 *                              packets.
 * Input(s)                   : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)                  : RM HB Raw socket descriptor.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS/HB_FAILURE
 ******************************************************************************/
VOID
HbRawIcchSockInit (VOID)
{
    return;
}


/*******************************************************************************
 * Function                    : HbRawIcchSockSend
 * Action                      : Routine to send the packet out through RAW IP
 *                               socket.This socket is specifically used by RM
 *                               module to send the Heart Beat messages destined
 *                               to the multicast ip address 224.0.0.250.
 * Input(s)                    : pu1Data - pointer to the data to be sent out
 *                                u2PktLen - Length of the data
 * utput(s)                   : None.
 * <OPTIONAL Fields>           : None
 * Global Variables Referred   : None
 * Global variables Modified   : None
 * Exceptions or Operating
 * System Error Handling       : None
 * Use of Recursion            : None
 * Returns                     : HB_SUCCESS/HB_FAILURE
 *******************************************************************************/
UINT4
HbRawIcchSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2PktLen);
    UNUSED_PARAM (u4DestAddr);
    return (HB_SUCCESS);
}

/******************************************************************************
 * Function                     : HbRawIcchSockRcv
 * Action                       : Routine to receive RM protocol packets.
 * Input(s)                     : pu1Data - pointer to rcv the packet
 *                                u2BufSize - size of the buffer to be received.
 * Output(s)                    : pu1Data - pointer to the packet rcvd.
 *                                pu4PeerAddr - IP addr of the peer from which
 *                                the pkt is rcvd.
 * <OPTIONAL Fields>            : None
 * Global Variables Referred    : None
 * Global variables Modified    : None
 * Exceptions or Operating
 * System Error Handling        : None
 * Use of Recursion             : None
 * Returns                      : i4Len - Length of the packet received.
 *******************************************************************************/
INT4
HbRawIcchSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufSize);
    UNUSED_PARAM (pu4PeerAddr);
    return (0);
}

#endif

