/********************************************************************
 * * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fshbwr.c,v 1.3 2015/02/05 11:32:28 siva Exp $
 * *
 * * Description: Protocol Low Level Routines
 * *********************************************************************/
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fshblw.h"
# include  "fshbwr.h"
# include  "fshbdb.h"


VOID RegisterFSHB ()
{
	SNMPRegisterMib (&fshbOID, &fshbEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fshbOID, (const UINT1 *) "fshb");
}



VOID UnRegisterFSHB ()
{
	SNMPUnRegisterMib (&fshbOID, &fshbEntry);
	SNMPDelSysorEntry (&fshbOID, (const UINT1 *) "fshb");
}

INT4 FsHbIntervalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsHbInterval(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbPeerDeadIntMultiplierGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsHbPeerDeadIntMultiplier(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbTrcLevelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsHbTrcLevel(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbStatsEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbStatsEnable(&(pMultiData->i4_SLongValue)));
}
INT4 FsHbClearStatsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbClearStats(&(pMultiData->i4_SLongValue)));
}
INT4 FsHbIntervalSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsHbInterval(pMultiData->u4_ULongValue));
}


INT4 FsHbPeerDeadIntMultiplierSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsHbPeerDeadIntMultiplier(pMultiData->u4_ULongValue));
}


INT4 FsHbTrcLevelSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsHbTrcLevel(pMultiData->u4_ULongValue));
}

INT4 FsHbStatsEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsHbStatsEnable(pMultiData->i4_SLongValue));
}


INT4 FsHbClearStatsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsHbClearStats(pMultiData->i4_SLongValue));
}

INT4 FsHbIntervalTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsHbInterval(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsHbPeerDeadIntMultiplierTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsHbPeerDeadIntMultiplier(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsHbTrcLevelTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsHbTrcLevel(pu4Error, pMultiData->u4_ULongValue));
}

INT4 FsHbStatsEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FsHbStatsEnable(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsHbClearStatsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FsHbClearStats(pu4Error, pMultiData->i4_SLongValue));
}

INT4 FsHbIntervalDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsHbInterval(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsHbPeerDeadIntMultiplierDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsHbPeerDeadIntMultiplier(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsHbTrcLevelDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsHbTrcLevel(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsHbStatsEnableDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsHbStatsEnable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsHbClearStatsDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsHbClearStats(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsHbStatsMsgTxCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbStatsMsgTxCount(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbStatsMsgTxFailedCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbStatsMsgTxFailedCount(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbStatsMsgRxCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbStatsMsgRxCount(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbStatsMsgRxProcCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbStatsMsgRxProcCount(&(pMultiData->u4_ULongValue)));
}
INT4 FsHbStatsRxFailedCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsHbStatsRxFailedCount(&(pMultiData->u4_ULongValue)));
}
