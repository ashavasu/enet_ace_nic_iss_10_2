/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved
*
* $Id: hbsock.c,v 1.4 2015/11/20 10:19:07 siva Exp $
*
* Description: Redundancy manager socket interface functions.
*********************************************************************/
#include "hbincs.h"

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>

#ifdef write
#undef write
#endif
#ifdef read
#undef read
#endif
#include <unistd.h>

#ifdef ICCH_WANTED
UINT1               gIcchMcastGroupAddr[4] = { 224, 0, 0, 251 };
#endif /* ICCH_WANTED */
#ifdef RM_WANTED
/* RM  Multicast ip addr used is 224.0.0.250 */
UINT1               gRmMcastGroupAddr[4] = { 224, 0, 0, 250 };

/****************************************************************************
 * Function                   : HbRawSockInit 
 * Action                     : Routine to create a RAW IP socket, set socket 
 *                              options for  sending and receiving RM multicast
 *                              packets
 * Input(s)           	      : pi4HbSockFd - Pointer to HB raw socked fd
 * Output(s)          	      : RM HB Raw socket descriptor.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS/HB_FAILURE
 ****************************************************************************/
INT4
HbRawRmSockInit (INT4 *pi4HbSockFd)
{
    struct ip_mreqn     mreqn;
    struct in_addr      SelfNodeAddr;
    UINT4               u4RetVal = HB_SUCCESS;
    UINT4               u4IfIndex = 0;
    INT4                i4SockFd = HB_INV_SOCK_FD;
    INT4                i4Flags = -1;
    INT4                i4OptnVal = TRUE;
    INT4                i4MCloopVal = 0;    /*disable receiving mcast packets
                                             *sent by us*/
    INT4                i4precedence = 6;   /* precedence specified to give high
                                             * priority RM HB Packets */

    MEMSET (&mreqn, 0, sizeof (struct ip_mreqn));    
    MEMSET (&SelfNodeAddr, 0, sizeof (struct in_addr));

    HB_TRC (HB_INIT_SHUT_TRC, "HbRawSockInit: "
               "Raw socket creation entry...\r\n");
    /* Open a raw socket for heart beat messages */
    i4SockFd = socket (AF_INET, SOCK_RAW, HB_PROTO);
    if (i4SockFd < 0)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "HbRawSockInit: Raw Socket "
                   "Creation Failure !!!\r\n");
        u4RetVal = HB_FAILURE;
    }

    /* Get current socket flags */
    if (u4RetVal != HB_FAILURE)
    {
        if ((i4Flags = fcntl (i4SockFd, F_GETFL, 0)) < 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "HbRawSockInit: Fcntl GET "
                       "Failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4SockFd, F_SETFL, i4Flags) < 0)
        {
            HB_TRC (HB_ALL_FAILURE_TRC, "HbRawSockInit: Fcntl SET "
                       "Failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        SelfNodeAddr.s_addr = OSIX_HTONL (HB_GET_SELF_NODE_ID ());

        /* Set socket option to send multicast IP pkt */
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_MULTICAST_IF,
                        (char *) &SelfNodeAddr, sizeof (struct in_addr)) < 0)
        {
            perror ("HbRawSockInit: IP_MULTICAST_IF setsockopt failure\r\n");
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawSockInit: setsockopt "
                       "IP_MULTICAST_IF - failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* This option allows the user to form their own IP hdr instead
         * of kernel including its IP hdr.
         * While using loopback interface lo:1 & lo:2, the ip pkt sent
         * out has incorrect src ip. Kernel fills up the src IP in
         * IP hdr as the interface's primary IP addr (which may not
         * be correct in case of an interface having multiple ip addr). */
        if ((setsockopt (i4SockFd, IPPROTO_IP, IP_HDRINCL, (&i4OptnVal),
                         sizeof (INT4))) < 0)
        {
            perror ("HbRawSockInit: IP_HDRINCL setsockopt failure\r\n");
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawSockInit: setsockopt IP_HDRINCL "
                       "- failure !!! \r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        MEMSET (&mreqn, 0, sizeof (mreqn));
        MEMCPY ((char *) &mreqn.imr_multiaddr.s_addr,
                (char *) &gRmMcastGroupAddr, 4);

        u4IfIndex = HbGetIfIndexFromName ((UINT1 *) IssGetRmIfNameFromNvRam ());
        mreqn.imr_ifindex = (INT4) u4IfIndex;

        /* Socket option to receive the multicast IP packet */
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                        (void *) &mreqn, sizeof (mreqn)) < 0)
        {
            perror ("HbRawSockInit: Setsockopt Failed for "
                    "IP_ADD_MEMBERSHIP. \r\n");
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawSockInit: setsockopt IP_ADD_MEMBERSHIP "
                       "- failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }
    if (u4RetVal != HB_FAILURE)
    {
        if (setsockopt (i4SockFd, IPPROTO_IP, IP_MULTICAST_LOOP,
                        &i4MCloopVal, sizeof (INT4)) < 0)
        {
            perror ("HbRawSockInit: Setsockopt Failed "
                    "for IP_MULTICAST_LOOP. \r\n");
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawSockInit: setsockopt "
                       "IP_MULTICAST_LOOP - failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        /* Priority Given to  RM-Hb messages since they are very vital
         * This is done so as to ensure that the RM-Hb Packets are not lost */
        if (setsockopt (i4SockFd, SOL_SOCKET, SO_PRIORITY,
                        (char *) &i4precedence, sizeof (i4precedence)) < 0)
        {
            perror ("HbRawSockInit : setsockopt  Failed  for SO_PRIORITY)");
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawSockInit: setsockopt "
                       "SO_PRIORITY - failure !!!\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }

    if (u4RetVal != HB_FAILURE)
    {
        if (SelAddFd (i4SockFd, HbRawPktRcvd) == HB_FAILURE)
        {
            HB_TRC (HB_ALL_FAILURE_TRC,
                       "HbRawSockInit: SelAddFd Failure "
                       "while adding RM raw socket\r\n");
            close (i4SockFd);
            u4RetVal = HB_FAILURE;
        }
    }
    if (u4RetVal == HB_FAILURE)
    {
        return (HB_FAILURE);
    }
    *pi4HbSockFd = i4SockFd;
    HB_TRC (HB_INIT_SHUT_TRC, "HbRawSockInit: "
               "Raw socket created successfully - exit...\r\n");
    return (HB_SUCCESS);
}

/******************************************************************************
 * Function                   : HbRawSockSend 
 * Action                     : Routine to send the packet out thru' RAW IP 
 *                              socket. This is socket is specifically used by 
 *                              RM module to send the Heart Beat messages 
 *                              destined to the multicast ip address 224.0.0.250
 * Input(s)                   : pu1Data - pointer to the data to be sent out
 *                              u2PktLen - Length of the data
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS/HB_FAILURE
 ******************************************************************************/
UINT4
HbRawSockSend (UINT1 *pu1Data, UINT2 u2PktLen, UINT4 u4DestAddr)
{
    struct sockaddr_in  DestAddr;
    UINT4               u4RetVal = HB_SUCCESS;
    INT4                i4WrBytes = 0;

    MEMSET (&DestAddr, 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestAddr.sin_port = 0;

    i4WrBytes = sendto (gHbInfo.i4RawSockFd, pu1Data, u2PktLen, 0,
                        (struct sockaddr *) &DestAddr,
                        sizeof (struct sockaddr_in));
    if (i4WrBytes < 0)
    {
        perror ("HbSend failure\r\n");
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! HbSendTo - failure !!! \r\n");
        u4RetVal = HB_FAILURE;
    }
    return (u4RetVal);
}

/******************************************************************************
 * Function                   : HbRawSockRcv 
 * Action                     : Routine to receive RM protocol packets.
 * Input(s)                   : pu1Data - pointer to rcv the packet
 *                              u2BufSize - size of the buffer to be received.
 * Output(s)                  : pu1Data - pointer to the packet rcvd. 
 *                              pu4PeerAddr - IP addr of the peer from which the
 *                              pkt is rceived.   
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : Length of the packet received.
 ******************************************************************************/
INT4
HbRawSockRcv (UINT1 *pu1Data, UINT2 u2BufSize, UINT4 *pu4PeerAddr)
{
    struct sockaddr_in  PeerAddr;
    INT4                i4Len = 0;
    INT4                i4AddrLen = sizeof (PeerAddr);

    i4Len = recvfrom (gHbInfo.i4RawSockFd, pu1Data,
                      u2BufSize, 0, (struct sockaddr *) &PeerAddr,
                      (socklen_t *) & i4AddrLen);
    *pu4PeerAddr = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
    return (i4Len);
}


#endif /* RM_WANTED */
/******************************************************************************
 * Function                   : HbCloseSocket
 * Action                     : Routine to close the socket created for RM.
 * Input(s)                   : i4SockFd - Socket descriptor.
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : 0 on success & -1 on failure
 ******************************************************************************/
INT4
HbCloseSocket (INT4 i4SockFd)
{
    INT4                i4RetVal = 0;

    i4RetVal = close (i4SockFd);
    if (i4RetVal < 0)
    {
        HB_TRC (HB_ALL_FAILURE_TRC, "!!! socket close Failure !!! \r\n");
    }
    return (i4RetVal);
}

/******************************************************************************
 * Function           	      : HbGetIfIndexFromName
 * Action                     : Routine to get the Interface index of the given 
 *                              Interface name.
 * Input(s)                   : pu1IfName - Interface name.
 * Output(s)                  : None.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : IfIndex
 ******************************************************************************/
UINT4
HbGetIfIndexFromName (UINT1 *pu1IfName)
{
    UINT4               u4IfIndex = 0;

    u4IfIndex = if_nametoindex ((const CHR1 *) pu1IfName);
    return (u4IfIndex);
}

/******************************************************************************
 * Function                   : HbGetIfIpInfo
 * Action                     : Routine to get the ip addr of this node.
 * Input(s)                   : pu1IfName - Interface name
 * Output(s)                  : pNodeInfo - Node information.
 * <OPTIONAL Fields>          : None
 * Global Variables Referred  : None
 * Global variables Modified  : None
 * Exceptions or Operating
 * System Error Handling      : None
 * Use of Recursion           : None
 * Returns                    : HB_SUCCESS or HB_FAILURE.
 ******************************************************************************/
UINT4
HbGetIfIpInfo (UINT1 *pu1IfName, tHbNodeInfo * pNodeInfo)
{
    struct ifreq        ifreq;
    struct sockaddr_in *saddr=NULL;
    UINT4               u4RetVal = HB_SUCCESS;
    INT4                i4SockFd = -1;

    MEMSET (&ifreq, 0, sizeof (struct ifreq));

    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (i4SockFd >= 0)
    {
        STRNCPY (ifreq.ifr_name, pu1IfName, IFNAMSIZ);
        ifreq.ifr_name[IFNAMSIZ - 1] = '\0';

        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) < 0)
        {
            perror ("Error in getting RM interface\r\n");
            u4RetVal = HB_FAILURE;
        }
        else
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            pNodeInfo->u4IpAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
        }

        if (ioctl (i4SockFd, SIOCGIFNETMASK, &ifreq) < 0)
        {
            u4RetVal = HB_FAILURE;
        }
        else
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_netmask;
            pNodeInfo->u4NetMask = OSIX_NTOHL (saddr->sin_addr.s_addr);
        }

        close (i4SockFd);
    }
    else
    {
        u4RetVal = HB_FAILURE;

    }
    if (u4RetVal == HB_FAILURE)
    {
        return (HB_FAILURE);
    }

    return (HB_SUCCESS);
}
