/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripport.h,v 1.26 2016/07/16 11:13:48 siva Exp $
 *
 * Description:This file contains all the related           
 *             definitions for porting RIP to various target
 *             OS's.                                        
 * 
 *******************************************************************/
#ifndef   __RIP_PORT_H__
#define   __RIP_PORT_H__

           /********************************************************
           * The following definitions are related to intertask    *
           * definitions of macros and functions.                  *
           ********************************************************/

#define   RIP_NO_OF_PEERS    gRipRtr.RipSystemSize.u4RipMaxPeerEntries


#define   RIP_IS_ADDR_CLASS_D(u4Addr)    IP_IS_ADDR_CLASS_D(u4Addr)
#define   RIP_IS_ADDR_CLASS_E(u4Addr)    IP_IS_ADDR_CLASS_E(u4Addr)

           /********************************************************
           * The folowing definitions are related to the timer     *
           * level and need to be ported.                          *
           ********************************************************/

#define   RIP_TICKS_PER_SEC    1

           /********************************************************
           * The folowing definitions are related to trap          *
           * notification to manager through SNMP.                 *
           ********************************************************/
/* Uncomment this line if TRAP support is provided in SNMP */
/*
#define     RIP_SEND_TRAP_FOR_MEM_ALLOC_FAIL(TrapSubRef)                \
            {                                                           \
            tRIP_TRAP_PARM  TrapParams;                                 \
            RIP_TRAP_PROTOCOL_TYPE(TrapParams) = RIP_TRAP_PROTOCOL_IP;  \
            RIP_TRAP_SUBREF(TrapParams) = TrapSubRef;                   \
            SNMP_RIF_Notify_Trap(RIP_NO_MORE_BUFFERS, TrapParams);  \
            }

#define     RIP_SEND_TRAP_FOR_TABLE_OVFLOW(TrapSubRef)                  \
            {                                                           \
            tRIP_TRAP_PARM  TrapParams;                                 \
            RIP_TRAP_PROTOCOL_TYPE(TrapParams) = RIP_TRAP_PROTOCOL_IP;  \
            RIP_TRAP_SUBREF(TrapParams) = TrapSubRef;                   \
            SNMP_RIF_Notify_Trap(RIP_TABLE_OVERFLOW, TrapParams);   \
            }
*/
#define   RIP_SEND_TRAP_FOR_MEM_ALLOC_FAIL(TrapSubRef)    
#define   RIP_SEND_TRAP_FOR_TABLE_OVFLOW(TrapSubRef)      

typedef struct RipMemory
{
    tIpMemPoolId  RtEntryQId;
    tIpMemPoolId  LocalRtEntryQId;
    tIpMemPoolId  SumEntryQId;
    tIpMemPoolId  RtNodeQId;
    tIpMemPoolId  RtQMsgId;
    tIpMemPoolId  IfQId;
    tIpMemPoolId  IfAggTblRootId;
    tIpMemPoolId  IfAggRtRecId;
    tIpMemPoolId  CxtPoolId;
    tIpMemPoolId  RespPoolId;
    tIpMemPoolId  ImportRtPoolId;
    tIpMemPoolId  UnicastNbrPoolId;
    tIpMemPoolId  AuthKeyPoolId;
    tIpMemPoolId  CryptoAuthKeyPoolId;
    tIpMemPoolId  RMapFilterPoolId;
    tIpMemPoolId  RMMemPoolId;
} tRipMemory;

#define   RIP_TASK_NAME         (UINT1 *)"RIP"

/* RIP Input Packet arrival Q info */
#define   RIP_INPUT_Q_NODE_ID   SELF
#define   RIP_INPUT_Q_NAME      (const UINT1 *)"RIPQ"
#define   RIP_RESP_Q_NAME      (const UINT1 *)"RESQ"
#define   RIP_RM_PKT_QUEUE      "RIPRMQ"

#define   RIP_TASK_NODE_ID      SELF


/* RIP Packet arrival from RRD - Q info */
#define   RIP_RTM_Q_NODE_ID    SELF
#define   RIP_Q_NAME_FOR_RTM   (const UINT1 *)"RRDQ"

/* RIP Packet arrival Q info */
#define   RIP_INPUT_Q_DEPTH     50          
#define   RIP_INPUT_Q_MODE      OSIX_GLOBAL 

/* RIP Packet arrival Q info */
#define   RIP_RTM_Q_DEPTH      50          
#define   RIP_RTM_Q_MODE       OSIX_GLOBAL 
#define   RIP_RM_QUE_DEPTH      10
#define   RIP_RESP_MSG_NUM     RTM_MAX_PROTOMASK_ROUTES

#define   RIP_SEM_NAME      ((UINT1 *) "RIPS" )                
#define   RIP_SEM_COUNT     1                     
#define   RIP_SEM_FLAGS     OSIX_DEFAULT_SEM_MODE 
#define   RIP_SEM_NODE_ID   SELF                  

#define   RIP_RTMRT_LIST_SEM    ((UINT1 *) "RIPRTMS")                

#define   RIP_TIMER_EVENT              0x00000001
#define   RIP_INPUT_QUEUE_EVENT        0x00000002
#define   RIP_PKT_ARRIVAL_EVENT        0x00000004
#define   RIP_CONTROL_MSG_EVENT        0x00000008
#define   RIP_RTM_RTMSG_EVENT          0x00000010
#define   RIP_RM_PKT_EVENT             0x00000020
#define   RIP_RM_PEND_RT_SYNC_EVENT    0x00000040
#define   RIP_RM_START_TIMER_EVENT     0x00000080

#define   RIP_EVENT_WAIT_FLAGS     (OSIX_WAIT | OSIX_EV_ANY) 
#define   RIP_EVENT_WAIT_TIMEOUT   0                         
#define   RIP_SHUTDOWN_INTERVAL    3                        

#define RIP_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define RIP_L3IPVLAN_TYPE  CFA_L3IPVLAN
#endif /* End of #ifndef __RIP_PORT_H__ Block Checking */

