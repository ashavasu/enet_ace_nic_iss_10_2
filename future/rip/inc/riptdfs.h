/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: riptdfs.h,v 1.27 2016/06/18 11:46:29 siva Exp $
 *
 * Description:This file contains some general type         
 *             definitions that the RIP module uses.        
 * 
 *******************************************************************/
#ifndef   __RIP_TDFS_H__
#define   __RIP_TDFS_H__

          /********************************************************
           * The following definitions pertain to the general      *
           * definitions that the RIP module make use of.          *
           ********************************************************/

#define   RIP_VERSION_0               0
#define   RIP_VERSION_1               1
#define   RIP_VERSION_2               2
#define   RIP_VERSION_ANY          0xff
#define RIP_MD5_ALLOW_SEQUENCE_NO   100

#define   RIP_HDR_LEN                 4    /* Length, in terms of bytes    */
#define   RIP_AUTH_HDR_LEN            4    /* Length, in terms of bytes    */
#define   RIP_INFO_LEN               20

#define   RIP_UNLINK_TIMERS           0
#define   RIP_LINK_TIMERS             1

#define   RIP_DEF_METRIC              0
#define   RIP_INITIAL_TTL_VALUE       1
#define   RIP_INITIAL_BROADCAST_VALUE 0
#define   RIP_UPDATE_DATAGRAM_LEN         \
          (RIP_HDR_LEN + (RIP_INFO_LEN * RIP_MAX_ROUTES_PER_PKT))

#define   RIP_HDR_OFFSET           0

#define   RIP_RT_MASK_EXIST        1
#define   RIP_RT_MASK_NOT_EXIST    0

#define   RIP_DEFAULT_ROUTE        1
#define   RIP_HOST_ROUTE           2

#define   RIP_INCR_BAD_PACKETS     1

#define   RIP_AUTH_NEEDED          1
#define   RIP_AUTH_NOT_NEEDED      0

#define   RIP_MCAST_ON             1
#define   RIP_MCAST_OFF            0

#define   RIP_CLASS_A_NETMASK      0xff000000
#define   RIP_INVALID_CLASS_A_NET  0
/* Neighbor Unicast Feature */
#define   RIP_UNICAST_REQUIRED                   1
#define   RIP_UNICAST_NOT_REQUIRED               0

#define   RIP_BROADCAST_MASK            0xFFFFFFFF

#define   RIP_DATABASE_CHANGED                   2
#define   RIP_DATABASE_NOT_CHANGED               3

#define   RIP_TRIGGER_UPDATE                     1
#define   RIP_ROUTE_ADDITION_NO_NEED             1

#define   RIP_RESET_TIMER_ON                     1
#define   RIP_RESET_TIMER_OFF                    0

#define   RIP_SHUTDOWN_UPDATE_COUNT              4

#define   RIP_NULL_RTAG   '\0'        
#define   ForEver         for ( ; ; ) 

           /********************************************************
           * The following definitions are related to the operation*
           * of triggered update limitation                        *
           ********************************************************/

#define   RIP_TRIG_TIMER_INACTIVE        0
#define   RIP_TRIG_TIMER_ACTIVE          1
#define   RIP_TRIG_UPD_NOT_DESIRED       0
#define   RIP_TRIG_UPD_DESIRED           1

#define   RIP_POIS_REV_NOT_APPLICABLE    0
#define   RIP_POIS_REV_APPLICABLE        1
#define   RIP_NO_SPLIT_HOR_APPLICABLE    2

           /********************************************************
           * The following definitions defines the address family  *
           * related definitions that RIP module uses              *
           ********************************************************/

#define   INET_ADDRESS_FAMILY             2    /* The AFI for IP as specified
                                                * in rfc1058
                                                */
#define   RIP_AUTH_ADDRESS_FAMILY    0xFFFF    /* As defined in rfc1723        
                                                */

           /********************************************************
           * The following definitions defines the address related *
           * definitions that RIP module uses                      *
           ********************************************************/

#define     RIP_BROADCAST_ADDRESS           0xFFFFFFFF
                                            /* The multicast address with which
                                             * RIP versions above 1, may send
                                             * updates to reduce the traffic  */
#define     RIP_MULTICAST_ADDRESS           0xE0000009
                                            /* rfc1058 defines the class-A
                                             * address for loopback address   */
#define     RIP_LOOPBACK_NET_ADDRESS        0x7F000001
#define     IP_ADDR_LENGTH                  4

           /********************************************************
           * Following defines the possible commands that a RIP    *
           * packet can have                                       *
           ********************************************************/

#define   RIP_REQUEST               1
#define   RIP_RESPONSE              2
#define   RIP_UPDATE_REQUEST        9
#define   RIP_UPDATE_RESPONSE      10
#define   RIP_UPDATE_ACK           11

#define   RIP_FLUSH_FLAG            1
#define   RIP_WAN_TYPE              2
#define   RIP_POLL_INVL            10
#define   RIP_MAX_LEN             576
#define   RIP_RUNIN_TMR             2
#define   RIP_TRIP_TRIG_INTVL      10
#define   RIP_DOMAIN_LENGTH         2

#define RIP_ALLOC_LIST_ENTRY(pListNode) \
            (pListNode = (tRipRtNode *)IP_ALLOCATE_MEM_BLOCK\
             (IpRipMemory.RtNodeQId))

#define RIP_FREE_LIST_ENTRY(pListNode) \
            IP_RELEASE_MEM_BLOCK(IpRipMemory.RtNodeQId, pListNode)

#define RIP_ALLOC_RESP_ENTRY(pRespMsg) \
        (pRespMsg = (tRipRespQMsg *)IP_ALLOCATE_MEM_BLOCK\
         (IpRipMemory.RespPoolId))

#define RIP_FREE_RESP_ENTRY(pRespMsg) \
            IP_RELEASE_MEM_BLOCK(IpRipMemory.RespPoolId, pRespMsg)


           /********************************************************
           * The following definitions are related to the mode for *
           * rip send operation, whether it is due to a response to*
           * a request etc.                                        *
           ********************************************************/

#define   RIP_RES_FOR_REQ          0
#define   RIP_REG_UPDATE           1
#define   RIP_TRIG_UPDATE          2
#define   RIP_SPACE_UPDATE         3
#define   RIP_LAST_SPACE_UPDATE    4
#define   RIP_UNICAST_UPDATE       5

           /********************************************************
           * Maximum number of packets that can be present in a    *
           * datagram.                                             *
           ********************************************************/

#define   RIP_MAX_ROUTES_PER_PKT    25

           /********************************************************
           * The following definitions defines the timer           *
           * identifiers that are used for RIP protocol operation  *
           ********************************************************/

#define   RIP_RT_TIMER_ID          1
#define   RIP_UPDATE_TIMER_ID      2
#define   RIP_SPACE_TIMER_ID       4

#define   RIP_TRIG_UPD_TIMER_ID    3
#define   RIP_RES_RETX_TIMER       5
#define   RIP_REQ_RETX_TIMER       6
#define   RIP_SUBSCRIP_TIMER       7
#define   RIP_TRIP_TRIG_TMR        8

#define   RIP_PASS_UPD_TIMER_ID    9
           /********************************************************
           *      Definitions related to the RIP peer records      *
           ********************************************************/

#define   RIP_PEER_RECORD_OLD    0
#define   RIP_PEER_RECORD_NEW    1

           /********************************************************
           *         Type of local route table  for RIP            *
           ********************************************************/
#define RIP_ROUTE_TABLE    4

           /********************************************************
           *   Definitions related to global statistics structure  *
           ********************************************************/

typedef struct RIP_GBL_STATS
{
    UINT4   u4GlobalRouteChanges; /* This specifies the number of route
                                   * changes made to the RIP route
                                   * database and does not include the
                                   * refresh of a route's age.          
                                   */
    UINT4   u4GlobalQueries;      /* This specifies the number of
                                   * responses sent to RIP queries from
                                   * the other systems.                 
                                   */
} tRipGblStats;

typedef struct
{
    tTMO_APP_TIMER Timer_node;
    UINT1          u1Id;             /* To demultiplex between handlers */
    UINT1          u1AlignmentByte;
    UINT2          u2AlignmentByte;
} t_RIP_TIMER;

typedef struct
{
    tIP_APP_TIMER  TimerNode;
    UINT1          u1Id;        /* Identifier to demultiplex between the
                                 * timers (Update (Or) Age)           
                                 */
    UINT1          u1Data;
    UINT2          u2If;
} tRipUpdateTimer;

typedef struct
{
    INT4     i4CxtId;
    UINT4    u4DstAddr;
    UINT4    u4DstMask;
} tRipRtMarker;

typedef struct _RipRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tRipRmCtrlMsg;

typedef struct _RipRmMsg
{
     tRipRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tRipRmMsg;

typedef struct _sRipRedGblInfo{
    UINT1 u1BulkUpdStatus; /* bulk update status
                            * RIP_HA_UPD_NOT_STARTED 0
                            * RIP_HA_UPD_COMPLETED 1
                            * RIP_HA_UPD_IN_PROGRESS 2,
                            * RIP_HA_UPD_ABORTED 3 */
    UINT1 u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1 u1NumPeersUp;     /* Indicates number of standby nodes
                               that are up. */
    UINT1 bBulkReqRcvd;     /* To check whether bulk request recieved
                               from standby before RM_STANDBY_UP event. */
}tRipRedGblInfo;


           /********************************************************
           * The following definition finds out whether an address *
           * valid or not.                                         *
           ********************************************************/

#define    RIP_IS_ADDRESS_INVALID(Addr)     \
           ((Addr < (UINT4) RIP_MIN_NET_NUMBER) ||      \
                      (RIP_IS_ADDR_CLASS_D( Addr ) != FALSE) ||    \
           (RIP_IS_ADDR_CLASS_E( Addr ) != FALSE) ||    \
           (Addr == RIP_LOOPBACK_NET_ADDRESS))

#ifdef  ARG_LIST
#undef  ARG_LIST
#endif

#define   ARG_LIST(x)    x

           /********************************************************
           *            Extern Declaration Variables               *
           ********************************************************/

#define   RIP_ENABLE                   1

/*****************************************************************************/
/*              CRU Related definitions                                      */

#define   RIP_BUF_COPY_OVER_CHAIN(pBuf, RipPkt, u4_Offset, u2Len) \
          CRU_BUF_Copy_OverBufChain(pBuf, (UINT1 *) RipPkt, u4_Offset, \
                                 (UINT4) u2Len)

#define  RIP_BUF_COPY_FROM_CHAIN(pBuf, pDst, u4Offset, u4Size) \
         CRU_BUF_Copy_FromBufChain(pBuf, (UINT1 *)(pDst), u4Offset, \
         u4Size)
   
#define   RIP_BUF_ALLOCATE_CHAIN(x, y) CRU_BUF_Allocate_MsgBufChain(x, y)

#endif /* End of #ifdef __RIP_TDFS_H__ Block Checking */

/* ************** Rip local routing table related *************/
#define RIP_ROUTE_IFINDEX       1
#define RIP_ROUTE_RTTYPE        2
#define RIP_ROUTE_PROTO         3
#define RIP_ROUTE_CHGTIME       4
#define RIP_ROUTE_METRIC        5
#define RIP_ROUTE_ROWSTATUS     6
#define RIP_ROUTE_GATEWAY       7
