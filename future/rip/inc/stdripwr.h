
#ifndef _STDRIPWRAP_H 
#define _STDRIPWRAP_H 
VOID RegisterSTDRIP(VOID);
INT4 Rip2GlobalRouteChangesGet (tSnmpIndex *, tRetVal *);
INT4 Rip2GlobalQueriesGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatAddressGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatRcvBadPacketsGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatRcvBadRoutesGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatSentUpdatesGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Rip2IfStatStatusSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfStatStatusGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAddressGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfDomainTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfDomainSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfDomainGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAuthTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAuthTypeSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAuthTypeGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAuthKeyTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAuthKeySet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfAuthKeyGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfSendTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfSendSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfSendGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfReceiveTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfReceiveSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfReceiveGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfDefaultMetricTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfDefaultMetricSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfDefaultMetricGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfStatusSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfStatusGet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfSrcAddressTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Rip2IfConfSrcAddressSet (tSnmpIndex *, tRetVal *);
INT4 Rip2IfConfSrcAddressGet (tSnmpIndex *, tRetVal *);
INT4 Rip2PeerAddressGet (tSnmpIndex *, tRetVal *);
INT4 Rip2PeerDomainGet (tSnmpIndex *, tRetVal *);
INT4 Rip2PeerLastUpdateGet (tSnmpIndex *, tRetVal *);
INT4 Rip2PeerVersionGet (tSnmpIndex *, tRetVal *);
INT4 Rip2PeerRcvBadPacketsGet (tSnmpIndex *, tRetVal *);
INT4 Rip2PeerRcvBadRoutesGet (tSnmpIndex *, tRetVal *);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexRip2PeerTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexRip2IfConfTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexRip2IfStatTable( tSnmpIndex *, tSnmpIndex *);
#endif
