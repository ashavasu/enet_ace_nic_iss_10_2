/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: ripmiproto.h,v 1.22 2017/03/10 12:43:03 siva Exp $
 *******************************************************************/

#ifndef _RIP_MI_PROTO_H_
#define _RIP_MI_PROTO_H_

#define RIP_MGMT_CXT    gRipRtr.pRipMgmtCxt
#define RIP_TIMER_ID    gRipRtr.RipTimerListId
#define RIP_TRC_GLOBAL_FLAG gRipRtr.u4RipGlobalTrcFlag
#define RIP_TRC_CXT_FLAG (RIP_MGMT_CXT)->u4RipTrcFlag
#define RIP_MGMT_CXT_ID (RIP_MGMT_CXT)->i4CxtId

#define RIP_RT_TYPE_IN_CXT(pRipCxt)\
                  (RIP_ROUTE_TABLE | (((UINT4)pRipCxt->i4CxtId) << 16))

#define RIP_AG_TYPE(u4IfIndex) (((UINT2)u4IfIndex) | (((UINT4)RIP_SIZING_CONTEXT_COUNT) << 16))
INT4 RipLocalRtTblGetObject ARG_LIST((UINT4, UINT4, INT4,
                                      UINT4, UINT4 *, INT4, tRipCxt *));
INT1 RipIfAggTblInit PROTO ((UINT4 u4IfIndex, tRipCxt *pRipCxtEntry));

VOID RipIfAggTblShut PROTO ((UINT4 u4IfIndex, tRipCxt *pRipCxtEntry));

VOID * RipGetIfAggTblRoot PROTO ((UINT2 u2IfIndex, tRipCxt *pRipCxtEntry));
INT1 RipCheckIfAggLimit PROTO ((UINT2 u2IfIndex, tRipCxt *pRipCxtEntry));

tRipIfAggRtInfo *RipCheckIfAgg PROTO ((UINT2 u2IfIndex, UINT4 u4Address, 
                                       UINT4 u4Mask, tRipCxt *pRipCxtEntry));
INT1 RipCheckSinkRt PROTO ((UINT4 u4Address, UINT4 u4Mask, tRipCxt *pRipCxtEntry));
tRipIfAggRtInfo *RipCheckRtInIfAggTbl PROTO ((UINT2 u2IfIndex, UINT4 u4Address, 
                                       UINT4 u4Mask, tRipCxt *pRipCxtEntry));
tRipIfAggRtInfo *RipAddIfAggRtToTable PROTO ((UINT4 u4IfIndex, UINT4 u4Address, 
                                UINT4 u4Mask, tRipCxt *pRipCxtEntry));

VOID RipUpdateIfAggRt PROTO ((tRipIfAggRtInfo *pRipIfAggRtInfo, UINT2 u2Port));

INT1 RipDeleteIfAggRt PROTO ((UINT4 u4IfIndex, UINT4 u4Address, UINT4 u4Mask, tRipCxt *pRipCxtEntry));

INT1 RipDeleteIfAgg PROTO ((tRipIfaceRec *pRipIfRec));

INT1 RipUpdateIfAggRtAsSinkRt PROTO ((tRipIfAggRtInfo *pRipIfAggRtInfo));
INT1 RipDeleteIfAggTbl PROTO ((UINT2 u2IfIndex, tRipCxt *pRipCxtEntry));

VOID RipTrieDeleteCallBack PROTO ((tDeleteOutParams *pInput));

INT1 RipUpdateRtmForSummaryDisable PROTO ((tRipCxt *pRipCxtEntry));
INT1 RipUpdateRtmForSummaryEnable PROTO ((tRipCxt *pRipCxtEntry));
INT1 RipGenerateUpdateForSummaryEnable PROTO ((UINT1 u1Version, 
                                               UINT1 u1BcastFlag,
                                               UINT2 u2If,
                                               tRipInfoBlk *pRipInfoBlk,
                                               UINT2 *pu2NoOfRoutes,
                                               UINT4 u4DestNet, 
                                               UINT2 u2DestUdpPort,
                                               tRipCxt *pRipCxtEntry,
            UINT1 u1UpdateSendFlag));

VOID RipAddRipRtToUpdateForSummaryEnable PROTO ((tRipTxInfo *pRipTxInfo,
                                                 UINT4 u4Address, UINT4 u4Mask,
                                    tRipInfoBlk *pRipInfoBlk, 
                                    UINT2 *pu2NoOfRoutes, tRipCxt *pRipCxtEntry));
tRipRtEntry *RipAddSummaryRoute PROTO ((UINT4 u4RtAddr, tRipCxt *pRipCxtEntry));
INT1 RipSetAggRtSplitHorizonStatus PROTO ((tRipInfo *pRipInfo, UINT2 u2If, tRipCxt *pRipCxtEntry));
INT1 RipUpdateIfAggRtStatus PROTO ((UINT2 u2IfIndex, tRipCxt *pRipCxtEntry));
tRipRtEntry *RipCheckSummaryAgg PROTO ((UINT4 u4Address, UINT4 u4Mask, tRipCxt *pRipCxtEntry));
VOID RipCheckAggRtSplitHorizonStatus PROTO ((UINT4 u4Address, UINT4 u4Mask,
                                             UINT2 u2IfIndex,
                                             INT1 *pi1SendSummryFlag, tRipCxt *pRipCxtEntry));
VOID RipUpdateAllAggForSummaryEnable PROTO ((tRipCxt *pRipCxtEntry));

VOID RipAddDefRtToUpdate PROTO ((tRipInfoBlk * pRipInfoBlk, UINT2 *pu2NoOfRoutes,
                                 UINT2 u2If, tRipCxt *pRipCxtEntry));
EXPORT INT4 rip_check_recv_status ARG_LIST ((UINT1, UINT2, UINT4, UINT4, tRipCxt *));
EXPORT INT4 rip_check_authentication ARG_LIST ((tRipInfo *, UINT1 *,
                                                UINT4, UINT2, UINT2 *, UINT4 *,
                                                UINT1 *, tRipCxt *));
EXPORT BOOLEAN rip_check_for_our_own_address ARG_LIST ((UINT4, tRipCxt *));
EXPORT INT4 rip_check_send_status ARG_LIST ((UINT1, UINT2, tRipCxt *));
EXPORT INT4 rip_get_send_status ARG_LIST ((UINT2, UINT1 *, UINT1 *, tRipCxt *));

EXPORT tRipPeerRec *rip_retrieve_record_for_peer ARG_LIST ((UINT4, UINT1 *, tRipCxt *));

EXPORT INT2 rip_get_if_index ARG_LIST ((UINT4 u4IfAddr, UINT2 *pu2If, tRipCxt *pRipCxtEntry));
EXPORT INT4 rip_construct_auth_info ARG_LIST ((UINT1 *, UINT2 *, UINT2, tRipCxt *));

EXPORT INT4         rip_update_peer_record
ARG_LIST ((UINT2, UINT4, UINT1 *, UINT1, UINT2, UINT2, UINT4, UINT1, tRipCxt *));
EXPORT VOID rip_if_update_periodic_timer ARG_LIST ((UINT1, UINT2, tRipCxt *));
EXPORT VOID rip_triggered_timer_expiry_handler ARG_LIST ((tRipCxt *));
VOID rip_copy_valid_entries (tRipInfoBlk * pRipInfoBlk,
                        UINT2 *pu2NoOfRoutes, int u2If, VOID *pRoot, tRipCxt *pRipCxtEntry);
EXPORT INT4         rip_generate_update_message
ARG_LIST ((UINT1, UINT1, UINT4, UINT2, UINT2, tRipCxt *));
EXPORT INT4         rip_send_update_datagram
ARG_LIST ((UINT1, UINT1, UINT4, UINT2, tRipInfoBlk *, UINT2, UINT2, tRipCxt *));
EXPORT INT4 rip_process_input ARG_LIST ((UINT2, UINT1 *,
                                         UINT2, UINT4, UINT4, UINT2, tRipCxt *));

EXPORT INT1        RipDeleteLocalRoute (tRipIfaceRec *);

EXPORT INT4         RipSendSpacedRouteInfo
ARG_LIST ((UINT1, UINT2, tRipInfoBlk *, UINT2 *, UINT4, UINT2, INT1, tRipCxt *));
EXPORT INT4 RipIsActiveInAnyInterface ARG_LIST ((tRipCxt *));
EXPORT UINT2 rip_get_routes_to_compose ARG_LIST ((UINT2, tRipCxt *pRipCxtEntry));
EXPORT tRipIfaceRec *rip_if_create ARG_LIST ((tRIP_INTERFACE IfId, tRipCxt *pRipCxtEntry));
EXPORT UINT1 RipUpdateRoutesThroIf ARG_LIST ((UINT2, UINT1, UINT4, tRipCxt *));
EXPORT INT1 RipGetNeighborPosition ARG_LIST ((INT4, tRipCxt *));
EXPORT BOOLEAN RipCheckForValidNeighbor ARG_LIST ((UINT4,tRipCxt *));
EXPORT VOID RipInitNeighborList ARG_LIST ((tRipCxt *));
EXPORT UINT4 RipGetNeighborListStatus ARG_LIST ((tRipCxt *));
EXPORT VOID RipIncNbrListCheckNoPktsDropped ARG_LIST ((tRipCxt *));
EXPORT INT4 RipSendUnicastDatagram PROTO ((tRipInfoBlk *, UINT2, tRipCxt *));

EXPORT INT4 RipInitWanInterface ARG_LIST ((tRipIfaceRec *));
EXPORT INT4 RipGenerateUpdateResponse ARG_LIST ((tRipInfo *, UINT2, UINT2 *, tRipCxt *,UINT1));
EXPORT VOID RipAddLeafToList ARG_LIST ((tLeafNode *, tRipCxt *));
EXPORT VOID RipMoveLeafToListEnd ARG_LIST ((void *, tRipIfaceRec *, tRipCxt *));
EXPORT VOID RipDeleteLeafFromList ARG_LIST ((void *, BOOLEAN, tRipIfaceRec *, tRipCxt *));
EXPORT tMd5AuthKeyInfo *RipMd5GetKeyToUseForSend ARG_LIST ((UINT2, tRipCxt *));
EXPORT tMd5AuthKeyInfo *RipMd5GetKeyToUseForReceive ARG_LIST ((UINT2, UINT1, tRipCxt *));
EXPORT INT4 rip_task_udp_send ARG_LIST ((UINT1 *,
                                         UINT2, UINT4, UINT2, UINT2, UINT1,
                                         UINT2, UINT1, UINT4, tRipCxt *));
INT4 RipConfigInterface ARG_LIST ((UINT2, tRipCxt *));
INT4 RipAddRtmRoute ARG_LIST ((tNetIpv4RtInfo *pRtmRoute, tRipCxt *pRipCxtEntry));
INT4                RipProcessRtChgNotification
ARG_LIST ((tNetIpv4RtInfo *pRtmRoute, tRipCxt *pRipCxtEntry));
INT4 RipSendRtmRedistDisableMsg ARG_LIST ((INT2, tRipCxt *));
INT4 RipDeleteImportRts ARG_LIST ((UINT1, tRipCxt *));
tRipRtEntry        *IsRouteFoundInRipTable (UINT4 u4DestinationNetworkAddress,
                                            UINT4 u4DestinationNetworkMask,
                                            UINT4 u4NextHop,
                                            UINT4 u4Tos,
                                            UINT1 u1SourceProtocolId,tRipCxt *pRipCxtEntry);
tRipIfaceRec  *RipGetIfRec PROTO ((UINT4, tRipCxt *));
tRipIfaceRec  *RipGetIfRecFromIndex PROTO ((UINT4));
tRipIfaceRec  *RipGetIfRecFromAddr PROTO ((UINT4, tRipCxt *));
tRipIfaceRec  *RipGetUtilIfRecForPrimSecAddr PROTO ((UINT4, tRipCxt *));
UINT2  RipGetUtilNoOfRoutes (UINT2 u2Len, UINT2 u2If,tRipCxt * pRipCxtEntry);
VOID RipUtilRecalculateDigest(UINT1 *pu1RipPkt, UINT2 u2AuthType,
                             UINT2 u2LenToSend,
                             tCryptoAuthKeyInfo * pCryptoKeyInfo);
INT4 RipDeleteIfRecord (UINT4 u4IfIndex,UINT4 u4IpAddr , UINT4 u4Flag);
VOID RipHandleDeleteIfRecEvent (tProtoQMsg * pMsg);
INT1 RipSetIfConfStatus (UINT4 u4Rip2IfConfAddress,INT4 i4SetValRip2IfConfStatus);
INT4 RipCheckNetworkIsPresent (UINT4 u4IpAddr, UINT4 u4SubNet);


tRipCxt   *RipGetCxtInfoRecFrmIface (UINT2);
INT4      RipCompareCxtFn (tRBElem *, tRBElem *);
INT4      RipAddVcEntry (VOID *);
VOID      RipDelVcEntry (VOID *);
INT4      RipGetFirstCxtId(INT4 *);
INT4      RipGetNextCxtId(INT4 ,INT4 *) ;
tRipCxt   *RipGetCxtInfoRec(INT4);
INT4      RipCxtExists(INT4);
INT4      RipValidateCxtId (INT4);
EXPORT    INT4 rip_rt_init_in_cxt (tRipCxt *);
INT4      RipCxtIsExistInVcm(INT4);
VOID      UtlRipTrcLog   ARG_LIST ((UINT4 ,INT4,UINT4 , CONST char *,
                            CONST char *, ...));
INT4      RipClearCxtInfo (tRipCxt *);
INT4      RipGetVcmContextInfoFromIfIndex (UINT4 , UINT4 *, UINT2 *);
VOID      RipTrieRootDel(tDeleteOutParams   *);
INT4      RipDelCxt(INT4);
VOID      RipIfMapChngAndCxtChngHdlr(UINT4, UINT4, UINT1);
INT4      RipSendRespPktToQ(tRip *, tRipInfo *,UINT2 , UINT4 , UINT2 ,
                            UINT4 , UINT1 , tRipCxt *);
INT4      RipSendRouteMapUpdateMsg (UINT1*, UINT4);
UINT1     RipFilterRouteSourceInCxt (tRipCxt * pRipCxtEntry, tRipInfo * pRipInfo);
INT4 RipGetVcmSystemMode (UINT2 u2ProtocolId);
INT4 RipGetVcmSystemModeExt (UINT2 u2ProtocolId);
INT1 RipIsDemandCircuit (UINT2 u2SendStatus);
#endif 
