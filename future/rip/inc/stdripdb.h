/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdripdb.h,v 1.4 2008/08/28 10:58:55 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDRIPDB_H
#define _STDRIPDB_H

UINT1 Rip2IfStatTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Rip2IfConfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Rip2PeerTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_FIXED_LENGTH_OCTET_STRING ,2};

UINT4 stdrip [] ={1,3,6,1,2,1,23};
tSNMP_OID_TYPE stdripOID = {7, stdrip};


UINT4 Rip2GlobalRouteChanges [ ] ={1,3,6,1,2,1,23,1,1};
UINT4 Rip2GlobalQueries [ ] ={1,3,6,1,2,1,23,1,2};
UINT4 Rip2IfStatAddress [ ] ={1,3,6,1,2,1,23,2,1,1};
UINT4 Rip2IfStatRcvBadPackets [ ] ={1,3,6,1,2,1,23,2,1,2};
UINT4 Rip2IfStatRcvBadRoutes [ ] ={1,3,6,1,2,1,23,2,1,3};
UINT4 Rip2IfStatSentUpdates [ ] ={1,3,6,1,2,1,23,2,1,4};
UINT4 Rip2IfStatStatus [ ] ={1,3,6,1,2,1,23,2,1,5};
UINT4 Rip2IfConfAddress [ ] ={1,3,6,1,2,1,23,3,1,1};
UINT4 Rip2IfConfDomain [ ] ={1,3,6,1,2,1,23,3,1,2};
UINT4 Rip2IfConfAuthType [ ] ={1,3,6,1,2,1,23,3,1,3};
UINT4 Rip2IfConfAuthKey [ ] ={1,3,6,1,2,1,23,3,1,4};
UINT4 Rip2IfConfSend [ ] ={1,3,6,1,2,1,23,3,1,5};
UINT4 Rip2IfConfReceive [ ] ={1,3,6,1,2,1,23,3,1,6};
UINT4 Rip2IfConfDefaultMetric [ ] ={1,3,6,1,2,1,23,3,1,7};
UINT4 Rip2IfConfStatus [ ] ={1,3,6,1,2,1,23,3,1,8};
UINT4 Rip2IfConfSrcAddress [ ] ={1,3,6,1,2,1,23,3,1,9};
UINT4 Rip2PeerAddress [ ] ={1,3,6,1,2,1,23,4,1,1};
UINT4 Rip2PeerDomain [ ] ={1,3,6,1,2,1,23,4,1,2};
UINT4 Rip2PeerLastUpdate [ ] ={1,3,6,1,2,1,23,4,1,3};
UINT4 Rip2PeerVersion [ ] ={1,3,6,1,2,1,23,4,1,4};
UINT4 Rip2PeerRcvBadPackets [ ] ={1,3,6,1,2,1,23,4,1,5};
UINT4 Rip2PeerRcvBadRoutes [ ] ={1,3,6,1,2,1,23,4,1,6};

tMbDbEntry stdripMibEntry[]= {

{{9,Rip2GlobalRouteChanges}, NULL, Rip2GlobalRouteChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Rip2GlobalQueries}, NULL, Rip2GlobalQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Rip2IfStatAddress}, GetNextIndexRip2IfStatTable, Rip2IfStatAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Rip2IfStatTableINDEX, 1, 0, 0, NULL},

{{10,Rip2IfStatRcvBadPackets}, GetNextIndexRip2IfStatTable, Rip2IfStatRcvBadPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Rip2IfStatTableINDEX, 1, 0, 0, NULL},

{{10,Rip2IfStatRcvBadRoutes}, GetNextIndexRip2IfStatTable, Rip2IfStatRcvBadRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Rip2IfStatTableINDEX, 1, 0, 0, NULL},

{{10,Rip2IfStatSentUpdates}, GetNextIndexRip2IfStatTable, Rip2IfStatSentUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Rip2IfStatTableINDEX, 1, 0, 0, NULL},

{{10,Rip2IfStatStatus}, GetNextIndexRip2IfStatTable, Rip2IfStatStatusGet, Rip2IfStatStatusSet, Rip2IfStatStatusTest, Rip2IfStatTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Rip2IfStatTableINDEX, 1, 0, 1, NULL},

{{10,Rip2IfConfAddress}, GetNextIndexRip2IfConfTable, Rip2IfConfAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Rip2IfConfTableINDEX, 1, 0, 0, NULL},

{{10,Rip2IfConfDomain}, GetNextIndexRip2IfConfTable, Rip2IfConfDomainGet, Rip2IfConfDomainSet, Rip2IfConfDomainTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, "0"},

{{10,Rip2IfConfAuthType}, GetNextIndexRip2IfConfTable, Rip2IfConfAuthTypeGet, Rip2IfConfAuthTypeSet, Rip2IfConfAuthTypeTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, "1"},

{{10,Rip2IfConfAuthKey}, GetNextIndexRip2IfConfTable, Rip2IfConfAuthKeyGet, Rip2IfConfAuthKeySet, Rip2IfConfAuthKeyTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, "0"},

{{10,Rip2IfConfSend}, GetNextIndexRip2IfConfTable, Rip2IfConfSendGet, Rip2IfConfSendSet, Rip2IfConfSendTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, "3"},

{{10,Rip2IfConfReceive}, GetNextIndexRip2IfConfTable, Rip2IfConfReceiveGet, Rip2IfConfReceiveSet, Rip2IfConfReceiveTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, "3"},

{{10,Rip2IfConfDefaultMetric}, GetNextIndexRip2IfConfTable, Rip2IfConfDefaultMetricGet, Rip2IfConfDefaultMetricSet, Rip2IfConfDefaultMetricTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, NULL},

{{10,Rip2IfConfStatus}, GetNextIndexRip2IfConfTable, Rip2IfConfStatusGet, Rip2IfConfStatusSet, Rip2IfConfStatusTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 1, NULL},

{{10,Rip2IfConfSrcAddress}, GetNextIndexRip2IfConfTable, Rip2IfConfSrcAddressGet, Rip2IfConfSrcAddressSet, Rip2IfConfSrcAddressTest, Rip2IfConfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Rip2IfConfTableINDEX, 1, 0, 0, NULL},

{{10,Rip2PeerAddress}, GetNextIndexRip2PeerTable, Rip2PeerAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Rip2PeerTableINDEX, 2, 0, 0, NULL},

{{10,Rip2PeerDomain}, GetNextIndexRip2PeerTable, Rip2PeerDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Rip2PeerTableINDEX, 2, 0, 0, NULL},

{{10,Rip2PeerLastUpdate}, GetNextIndexRip2PeerTable, Rip2PeerLastUpdateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Rip2PeerTableINDEX, 2, 0, 0, NULL},

{{10,Rip2PeerVersion}, GetNextIndexRip2PeerTable, Rip2PeerVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Rip2PeerTableINDEX, 2, 0, 0, NULL},

{{10,Rip2PeerRcvBadPackets}, GetNextIndexRip2PeerTable, Rip2PeerRcvBadPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Rip2PeerTableINDEX, 2, 0, 0, NULL},

{{10,Rip2PeerRcvBadRoutes}, GetNextIndexRip2PeerTable, Rip2PeerRcvBadRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Rip2PeerTableINDEX, 2, 0, 0, NULL},
};
tMibData stdripEntry = { 22, stdripMibEntry };
#endif /* _STDRIPDB_H */

