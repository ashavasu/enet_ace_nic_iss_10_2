/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripextn.h,v 1.22 2015/02/06 11:58:58 siva Exp $
 *
 * Description: Contains common includes used by RIP submodule.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/*  FILE NAME             : ripextn.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : IP                                               */
/*  MODULE NAME           : RARP                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 10'th January 2000                               */
/*  DESCRIPTION           : Contains common includes used by RIP submodule.  */
/*****************************************************************************/

#ifndef _RIP_EXTN_H
#define _RIP_EXTN_H

extern tRipMemory     IpRipMemory;
extern INT1 nmhGetIpForwarding PROTO ((INT4 *));
extern INT1 nmhGetIfIpAddr PROTO ((INT4, UINT4 *));
extern DBL8 UtilCeil  (DBL8);
extern INT4 CfaGetIfAlias PROTO((UINT4 u4IfIndex, UINT1 *au1IfAlias));
extern tRipRedGblInfo   gRipRedGblInfo;
extern tDbTblDescriptor gRipDynInfoList;
extern tRipRtMarker     gRipRedRtMarker;
extern tOsixQId         gRipRmPktQId;
extern UINT4            gu4RipSysLogId;
extern tDbDataDescInfo  gaRipDynDataDescList[RIP_MAX_DYN_INFO_TYPE];
extern UINT1            gu1RmStConfReq;
#endif /* _RIP_EXTN_H */
