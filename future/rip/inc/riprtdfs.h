/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: riprtdfs.h,v 1.28 2016/07/08 11:58:35 siva Exp $
 *
 * Description:This file contains structure definitions.    
 *             common definitions about the RIP routes and  
 *             RIP dynamic route table.                     
 * 
 *******************************************************************/
#ifndef   __RIP_RT_DFS_H
#define   __RIP_RT_DFS_H

           /********************************************************
           * The following definitions pertain to the status of    *
           * each route (Except PENDING status, other status are   *
           * taken from MIB-2 - rfc1213)                           *
           ********************************************************/

#define   RIP_RT_INDIRECT    0x04    /* Indicates, the routes that are
                                      * dynamically learnt from the
                                      * peers and are not direct 
                                      * routes                      
                                      */
#define   RIP_RT_PENDING     0x05    /* Indicates, the routes that are
                                      * aged out due to some reason, 
                                      * and being put in a garbage
                                      * collection time interval    
                                      */
#define   RIP_RT_COMPLETE   1
#define   RIP_RT_SUSPEND    2

           /********************************************************
           * The following definition defines a timer structure    *
           * which is used for every RIP route timer operation and *
           * is adopted from generic timer structure used by Future*
           * IP module.                                            *
           ********************************************************/

typedef struct
{
    tIP_APP_TIMER  TimerNode;
    UINT1          u1Id;             /* This is used to demultiplex between 
                                      * timer
                                      */ 
    UINT1          u1AlignmentByte;  /* Byte for Four Byte word alignment */
    UINT2          u2AlignmentByte;  /* Byte for Four Byte word alignment */
    VOID           *pRt;             /* A pointer to the route information */
} tRipRtTimer;

           /********************************************************
           * The following structure corresponds to the routing    *
           * database information for each and every destination   *
           ********************************************************/

           /* DO NOT CHANGE THIS DATA STRUCTURE
            * BEFORE READING DESIGN DOCUMENT
            */
typedef struct
{
    UINT4     u4DestNet;        /* The destination address                   */
    UINT4     u4DestMask;       /* The net mask to be applied to get the
                                 * non-host portion for the destination      
                                 */
    UINT4     u4Tos;            /* always zero for RIP                       */
    UINT4     u4NextHop;        /* The nexthop address to which any datagrams
                                 * destined to the destination, to be
                                 * forwarded. (In some special cases)        
                                 */
    UINT4     u4RtIfIndx;        /* The interface through which the route is
                                  * learnt                                    
                                  */
    UINT2     u2RtType;          /* The route status                          */
    UINT2     u2RtProto;         /* The protocol id   == RIP (8 - 1)here      */
    UINT4     u4ChgTime;         /* The time when the route is installed      */
    UINT4     u4RtNxtHopAS;      /* 2 LSB contains route tag of RIP           */
    INT4      i4Metric1;         /* The reachability cost for the destination */
    UINT4     u4RowStatus;       /* Row status                                */
    UINT4     u4Gw;              /* The gateway address to which any datagrams
                                  * destined to the destination, to be 
                                  * forwarded                                 
                                  */
    tRipRtTimer Timer;           /* The timer associated with this route
                                  * information used for age timer purpose    
                                  */
    void     *pListNode;
    UINT2     u2IfCounter;       /* A counter associated with each destination
                                  * and is significant at the time of applying
                                  * reverse poison for this                   
                                  */
    UINT1     u1Status;          /* changed route /unchanged route */
    UINT1     u1SinkRtFlag;      /* flag the specify if the summary route have
                                    been added as sink route to rtm or not*/

    /*-----------------------------------------------------------------------*/

    /* The dynamic entries that are to be synced should be added after the
     * RouteDbNode. And both the halves should be padded accordingly */

    tDbTblNode  RouteDbNode;               /* Data Base node to sync RIP route
                                            * information.*/

    UINT4     u4TempDestNet;        /* The destination address               */
    UINT4     u4TempDestMask;       /* The net mask to be applied to get the
                                     * non-host portion for the destination      
                                     */
    UINT4     u4TempTos;            /* always zero for RIP                   */
    UINT4     u4TempNextHop;        /* The nexthop address to which any datagrams
                                 * destined to the destination, to be
                                 * forwarded. (In some special cases)        
                                 */
    UINT4     u4TempRtIfIndx;        /* The interface through which the route is
                                  * learnt                                    
                                  */
    UINT4     u4TempGw;              /* The gateway address to which any datagrams
                                  * destined to the destination, to be 
                                  * forwarded                                 
                                  */
    UINT4     u4TempRtNxtHopAS;      /* 2 LSB contains route tag of RIP     */
    INT4      i4TempMetric1;         /* The reachability cost for the 
                                    destination */
    INT4      i4CxtId;           /* The Context id to be used for summary rts */
    UINT1     u1Distance;        /* Default distance */
    UINT1     u1Operation;       /* Either add route or delete route - used for
                                  * HA sync-up */
    UINT1     u1Level;           /* ISIS level-1 or level-2*/
    UINT1     au1Pad[1];         /* Padding */
    UINT4     u4SrcAddr;        /* Source address from which route received*/

} tRipRtInfo;

           /********************************************************
       The following structure is defined for easy route access by Trie.
           ********************************************************/

typedef struct
{
    void       *pNext;                /* used by trie library only */
    void       *pNextAlternatepath;   
    tRipRtInfo  RtInfo;               /* The route information structure, which
                                       * is described above 
                                       */ 
    tTMO_DLL_NODE RipIfNode;           /*
     *Node for Interface route entry.
     * */
    tTMO_DLL_NODE RipCxtRtNode;        /*
     *Node for global route entry.
     * */
 UINT4     u4RipCxtId; 
    UINT1          u1Preference; /* Contains preference value*/
    UINT1          u1RouteStatusInRtm; /* This flag indicates this
                                        * route added or not in RTM */
    UINT1          au1PaddingBytes[2]; /* Padding bytes to keep alignment */
} tRipRtEntry;

typedef struct
{
    tTMO_SLL_NODE       Next;
    tRipRtEntry        *pRt;
}
tRipImportList;

           /********************************************************
           * Some of the definitions related to the route          *
           * information for easy accessing of the fields          *
           ********************************************************/

                  /*  Age timer related macro definitions  */

#define   RIP_ROUTE_TIMER_NODE(pRoute)        ((pRoute)->RtInfo.Timer.TimerNode)
#define   RIP_ROUTE_TIMER_ID(pRoute)          ((pRoute)->RtInfo.Timer.u1Id)
#define   RIP_ROUTE_TIMER_ROUTE(pRoute)       ((pRoute)->RtInfo.Timer.pRt)
#define   RIP_ROUTE_TIMER_ROUTE_TAB(pRoute)   ((pRoute)->RtInfo.Timer.pHtab)

/* tag related macros */

#define COPY_RT_TAG(pRt, pu1RtTag)\
    (pRt)->RtInfo.u4RtNxtHopAS = (pu1RtTag)[0];\
    (pRt)->RtInfo.u4RtNxtHopAS =\
       ((pRt)->RtInfo.u4RtNxtHopAS << 8) + (pu1RtTag)[1];

#define COPY_RT_TAG_FOR_RRD(pRt, pu1RtTag)\
   (pRt)->u4RouteTag = (pu1RtTag)[0];\
          (pRt)->u4RouteTag =\
   ((pRt)->u4RouteTag << 8) + (pu1RtTag)[1];\
          (pRt)->u4RouteTag = ((pRt)->u4RouteTag<< 16)+gRipRRDGblCfg.u2ASNumber;

#define COPY_RIP_ROUTE_TO_IP_ROUTE(IpRoute, pRipRoute) \
        (IpRoute).pNextRtInfo        = NULL; \
        (IpRoute).pNextAlternatepath = NULL; \
        (IpRoute).u4DestNet          = (pRipRoute)->RtInfo.u4DestNet; \
        (IpRoute).u4DestMask         = (pRipRoute)->RtInfo.u4DestMask; \
        (IpRoute).u4Tos              = (pRipRoute)->RtInfo.u4Tos; \
        (IpRoute).u4NextHop          = (pRipRoute)->RtInfo.u4NextHop; \
        (IpRoute).u4RtIfIndx         = (pRipRoute)->RtInfo.u4RtIfIndx; \
        (IpRoute).u2RtType           = (pRipRoute)->RtInfo.u2RtType; \
        (IpRoute).u2RtProto          = (pRipRoute)->RtInfo.u2RtProto; \
        (IpRoute).u4RtAge            = (pRipRoute)->RtInfo.u4ChgTime; \
        (IpRoute).u4RtNxtHopAS       = (pRipRoute)->RtInfo.u4RtNxtHopAS; \
        (IpRoute).i4Metric1          = (pRipRoute)->RtInfo.i4Metric1; \
        (IpRoute).u4RowStatus        = (pRipRoute)->RtInfo.u4RowStatus
 

#define   RIP_RT_CHANGED_VAL     0x80
#define   RIP_RT_CHANGED_MASK    0x7f

#define   RIP_RT_CHANGED_STATUS(pRt)  \
          (pRt->RtInfo.u1Status & (UINT1)RIP_RT_CHANGED_VAL)

#define   RIP_RT_SET_CHANGED_STATUS(pRt)  \
          (pRt->RtInfo.u1Status = pRt->RtInfo.u1Status | (UINT1)RIP_RT_CHANGED_VAL)

#define   RIP_RT_RESET_CHANGED_STATUS(pRt)    \
          (pRt->RtInfo.u1Status = pRt->RtInfo.u1Status & (UINT1)RIP_RT_CHANGED_MASK)

#define   RIP_RT_STATUS(pRt)          \
          (pRt->RtInfo.u1Status & (UINT1)RIP_RT_CHANGED_MASK)

           /********************************************************
           * Definition to initialise a set of default values for a*
           * route structure at the start-up                       *
           ********************************************************/

#define     RIP_INITIALIZE_ROUTE_DEFAULTS(pRt)  \
                    { \
                    MEMSET(pRt, 0, sizeof(tRipRtEntry));\
                    pRt->RtInfo.i4Metric1    = RIP_DEF_METRIC; \
                    pRt->RtInfo.u2RtType     = CIDR_REMOTE_ROUTE_TYPE;\
                    pRt->RtInfo.u4RowStatus  = (UINT4) IPFWD_NOT_IN_SERVICE; \
                    pRt->RtInfo.u2RtProto    = (UINT2) RIP_ID;\
                    }

           /********************************************************
           * Acc, rfc1812, we need to implement a strategy of      *
           * applying poisoned reverse for a route in a limited    *
           * manner. The following definitions are used for        *
           * implementing this strategy.                           *
           ********************************************************/

                           /* The number of consecutive updates that have to be
                            * lost or fail to mention a route before RIP deletes
                            * the route                                       */

#define     RIP_UPDATE_LOSS             (RIP_DEF_ROUTE_AGE /            \
                                         RIP_DEF_UPDATE_INTERVAL) + 1

                           /* The value to set the ifcounter to when a
                            * destination is newly learned.                   */

#define     RIP_NEW_DESTN_IFCOUNTER     RIP_UPDATE_LOSS -               \
                                        (RIP_DEF_GARBAGE_COLCT_INTERVAL /  \
                                             RIP_DEF_UPDATE_INTERVAL)

#define   RIP_RRD_DEF_RT_METRIC                   3

#endif /* End of #ifdef __RIP_RT_DFS_H_ Block Checking */
