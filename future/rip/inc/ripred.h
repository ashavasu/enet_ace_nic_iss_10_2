/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: ripred.h,v 1.2 2014/04/01 12:19:24 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for RIP Server module.
 *              
 *******************************************************************/
#ifndef __RIP_RED_H
#define __RIP_RED_H


#define RIP_RED_BULK_UPD_CXT_MESSAGE 4

enum{
    RIP_RT_DYN_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as RIP Route table info. */
    RIP_PEER_DYN_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as RIP Peer table info. */
    RIP_MAX_DYN_INFO_TYPE
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
};

/* Represents the message types encoded in the update messages */
typedef enum {
    RIP_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    RIP_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG
}eRipRedRmMsgType;

typedef enum{
    RIP_HA_UPD_NOT_STARTED = 1,/* 1 */
    RIP_HA_UPD_IN_PROGRESS,    /* 2 */
    RIP_HA_UPD_COMPLETED,      /* 3 */
    RIP_HA_UPD_ABORTED,        /* 4 */
    RIP_HA_MAX_BLK_UPD_STATUS
} eRipHaBulkUpdStatus;

enum{
    RIP_RED_DYN_RT_INFO = RIP_RT_DYN_INFO,
    RIP_RED_DYN_PEER_INFO = RIP_PEER_DYN_INFO,
    RIP_RED_DYN_BULK_TAIL = RIP_RED_BULK_UPD_TAIL_MESSAGE,
    RIP_RED_DYN_BULK_CXT = RIP_RED_BULK_UPD_CXT_MESSAGE
};


/* Macro Definitions for RIP Server Redundancy */

#define MAX_RIP_SYNCUP_ROUTES 100

#define RIP_PROCESS_ONE_MSG 0
#define RIP_PROCESS_ALL_MSG 1

#define RIP_RM_GET_NUM_STANDBY_NODES_UP() \
          gRipRedGblInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define RIP_NUM_STANDBY_NODES() gRipRedGblInfo.u1NumPeersUp

#define RIP_RM_BULK_REQ_RCVD() gRipRedGblInfo.bBulkReqRcvd

#define RIP_IS_STANDBY_UP() \
          ((gRipRedGblInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

#define RIP_INITIATE_BULK_UPDATES L2_INITIATE_BULK_UPDATES
/* RM wanted */

#define RIP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define RIP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define RIP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define RIP_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define RIP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define RIP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define RIP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define RIP_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define RIP_RED_MAX_MSG_SIZE        1500
#define RIP_RED_TYPE_FIELD_SIZE     1
#define RIP_RED_LEN_FIELD_SIZE      2

#define RIP_ONE_BYTE                1
#define RIP_TWO_BYTES               2
#define RIP_FOUR_BYTES              4

#define RIP_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define RIP_RED_BULK_REQ_MSG_SIZE            3
#define RIP_RED_BULK_UPD_CXT_MSG_SIZE        3

#define RIP_RED_BULQ_REQ_SIZE       3

#define RIP_RED_ADD_CACHE            1
#define RIP_RED_DEL_CACHE            2

#define RIP_RED_CACHE_DEL         1
#define RIP_RED_CACHE_DONT_DEL    0

/* Function prototypes for RIP Redundancy */

#endif /* __RIP_RED_H */
