/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistdb.h,v 1.5 2013/05/23 12:44:50 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMISTRIPDB_H
#define _FSMISTRIPDB_H

UINT1 FsMIStdRip2GlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdRip2IfStatTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdRip2IfConfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdRip2PeerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsmistrip [] ={1,3,6,1,4,1,2076,152};
tSNMP_OID_TYPE fsmistripOID = {8, fsmistrip};


UINT4 FsMIStdRip2GlobalRouteChanges [ ] ={1,3,6,1,4,1,2076,152,1,1,1,1};
UINT4 FsMIStdRip2GlobalQueries [ ] ={1,3,6,1,4,1,2076,152,1,1,1,2};
UINT4 FsMIStdRip2IfStatAddress [ ] ={1,3,6,1,4,1,2076,152,2,1,1};
UINT4 FsMIStdRip2IfStatRcvBadPackets [ ] ={1,3,6,1,4,1,2076,152,2,1,2};
UINT4 FsMIStdRip2IfStatRcvBadRoutes [ ] ={1,3,6,1,4,1,2076,152,2,1,3};
UINT4 FsMIStdRip2IfStatSentUpdates [ ] ={1,3,6,1,4,1,2076,152,2,1,4};
UINT4 FsMIStdRip2IfStatStatus [ ] ={1,3,6,1,4,1,2076,152,2,1,5};
UINT4 FsMIStdRip2IfStatPeriodicUpdates [ ] ={1,3,6,1,4,1,2076,152,2,1,6};
UINT4 FsMIStdRip2IfStatRcvBadAuthPackets [ ] ={1,3,6,1,4,1,2076,152,2,1,7};
UINT4 FsMIStdRip2IfConfAddress [ ] ={1,3,6,1,4,1,2076,152,3,1,1};
UINT4 FsMIStdRip2IfConfDomain [ ] ={1,3,6,1,4,1,2076,152,3,1,2};
UINT4 FsMIStdRip2IfConfAuthType [ ] ={1,3,6,1,4,1,2076,152,3,1,3};
UINT4 FsMIStdRip2IfConfAuthKey [ ] ={1,3,6,1,4,1,2076,152,3,1,4};
UINT4 FsMIStdRip2IfConfSend [ ] ={1,3,6,1,4,1,2076,152,3,1,5};
UINT4 FsMIStdRip2IfConfReceive [ ] ={1,3,6,1,4,1,2076,152,3,1,6};
UINT4 FsMIStdRip2IfConfDefaultMetric [ ] ={1,3,6,1,4,1,2076,152,3,1,7};
UINT4 FsMIStdRip2IfConfStatus [ ] ={1,3,6,1,4,1,2076,152,3,1,8};
UINT4 FsMIStdRip2IfConfSrcAddress [ ] ={1,3,6,1,4,1,2076,152,3,1,9};
UINT4 FsMIStdRip2PeerAddress [ ] ={1,3,6,1,4,1,2076,152,4,1,1};
UINT4 FsMIStdRip2PeerDomain [ ] ={1,3,6,1,4,1,2076,152,4,1,2};
UINT4 FsMIStdRip2PeerLastUpdate [ ] ={1,3,6,1,4,1,2076,152,4,1,3};
UINT4 FsMIStdRip2PeerVersion [ ] ={1,3,6,1,4,1,2076,152,4,1,4};
UINT4 FsMIStdRip2PeerRcvBadPackets [ ] ={1,3,6,1,4,1,2076,152,4,1,5};
UINT4 FsMIStdRip2PeerRcvBadRoutes [ ] ={1,3,6,1,4,1,2076,152,4,1,6};
UINT4 FsMIStdRip2PeerInUseKey [ ] ={1,3,6,1,4,1,2076,152,4,1,7};


tMbDbEntry fsmistripMibEntry[]= {

{{12,FsMIStdRip2GlobalRouteChanges}, GetNextIndexFsMIStdRip2GlobalTable, FsMIStdRip2GlobalRouteChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2GlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIStdRip2GlobalQueries}, GetNextIndexFsMIStdRip2GlobalTable, FsMIStdRip2GlobalQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2GlobalTableINDEX, 1, 0, 0, NULL},

{{11,FsMIStdRip2IfStatAddress}, GetNextIndexFsMIStdRip2IfStatTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdRip2IfStatTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfStatRcvBadPackets}, GetNextIndexFsMIStdRip2IfStatTable, FsMIStdRip2IfStatRcvBadPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2IfStatTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfStatRcvBadRoutes}, GetNextIndexFsMIStdRip2IfStatTable, FsMIStdRip2IfStatRcvBadRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2IfStatTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfStatSentUpdates}, GetNextIndexFsMIStdRip2IfStatTable, FsMIStdRip2IfStatSentUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2IfStatTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfStatStatus}, GetNextIndexFsMIStdRip2IfStatTable, FsMIStdRip2IfStatStatusGet, FsMIStdRip2IfStatStatusSet, FsMIStdRip2IfStatStatusTest, FsMIStdRip2IfStatTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdRip2IfStatTableINDEX, 2, 0, 1, NULL},

{{11,FsMIStdRip2IfStatPeriodicUpdates}, GetNextIndexFsMIStdRip2IfStatTable, FsMIStdRip2IfStatPeriodicUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2IfStatTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfStatRcvBadAuthPackets}, GetNextIndexFsMIStdRip2IfStatTable, FsMIStdRip2IfStatRcvBadAuthPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2IfStatTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfConfAddress}, GetNextIndexFsMIStdRip2IfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfConfDomain}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfDomainGet, FsMIStdRip2IfConfDomainSet, FsMIStdRip2IfConfDomainTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, "0"},

{{11,FsMIStdRip2IfConfAuthType}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfAuthTypeGet, FsMIStdRip2IfConfAuthTypeSet, FsMIStdRip2IfConfAuthTypeTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, "1"},

{{11,FsMIStdRip2IfConfAuthKey}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfAuthKeyGet, FsMIStdRip2IfConfAuthKeySet, FsMIStdRip2IfConfAuthKeyTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, "0"},

{{11,FsMIStdRip2IfConfSend}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfSendGet, FsMIStdRip2IfConfSendSet, FsMIStdRip2IfConfSendTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, "3"},

{{11,FsMIStdRip2IfConfReceive}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfReceiveGet, FsMIStdRip2IfConfReceiveSet, FsMIStdRip2IfConfReceiveTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, "3"},

{{11,FsMIStdRip2IfConfDefaultMetric}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfDefaultMetricGet, FsMIStdRip2IfConfDefaultMetricSet, FsMIStdRip2IfConfDefaultMetricTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2IfConfStatus}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfStatusGet, FsMIStdRip2IfConfStatusSet, FsMIStdRip2IfConfStatusTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 1, NULL},

{{11,FsMIStdRip2IfConfSrcAddress}, GetNextIndexFsMIStdRip2IfConfTable, FsMIStdRip2IfConfSrcAddressGet, FsMIStdRip2IfConfSrcAddressSet, FsMIStdRip2IfConfSrcAddressTest, FsMIStdRip2IfConfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIStdRip2IfConfTableINDEX, 2, 0, 0, NULL},

{{11,FsMIStdRip2PeerAddress}, GetNextIndexFsMIStdRip2PeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdRip2PeerDomain}, GetNextIndexFsMIStdRip2PeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdRip2PeerLastUpdate}, GetNextIndexFsMIStdRip2PeerTable, FsMIStdRip2PeerLastUpdateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdRip2PeerVersion}, GetNextIndexFsMIStdRip2PeerTable, FsMIStdRip2PeerVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdRip2PeerRcvBadPackets}, GetNextIndexFsMIStdRip2PeerTable, FsMIStdRip2PeerRcvBadPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdRip2PeerRcvBadRoutes}, GetNextIndexFsMIStdRip2PeerTable, FsMIStdRip2PeerRcvBadRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},

{{11,FsMIStdRip2PeerInUseKey}, GetNextIndexFsMIStdRip2PeerTable, FsMIStdRip2PeerInUseKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdRip2PeerTableINDEX, 3, 0, 0, NULL},
};
tMibData fsmistripEntry = { 25, fsmistripMibEntry };

#endif /* _FSMISTRIPDB_H */

