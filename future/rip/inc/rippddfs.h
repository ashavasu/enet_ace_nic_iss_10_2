/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rippddfs.h,v 1.20 2016/07/16 11:13:48 siva Exp $
 *
 * Description:This file contains definitions of the route  
 *             information structure, the authentication    
 *             structure and a typical RIP message format.  
 * 
 *******************************************************************/

#ifndef   __RIP_PD_DFS_H__
#define   __RIP_PD_DFS_H__

          /********************************************************
           * The following defines the components constituting RIP *
           * message header                                        *
           ********************************************************/

typedef struct
{
    UINT1  u1Command;   /* Denotes either a RIP request or a RIP response  */
    UINT1  u1Version;   /* Denotes version that RIP message corresponds to */
    UINT2  u2Reserved;  /* Reserved field, as specified in rfc1723 and
                         * should be always zero.                     
                         */
} tRip;

           /********************************************************
           * Following defines the authentication structure which  *
           * is a per message function                             *
           ********************************************************/

typedef struct
{
    /* 
     * Field u2Family distinguishes between the authentication information and
     * the route information. Depending on this, one of the structures in the
     * union "tRipMesg" is accessed.
     */
    UINT2     u2Family;

    /* The authentication type denoting which type
     * of authentication that the RIP message
     * corresponds to.                            */
    UINT2     u2AuthType;
    /* The authentication string in octets and in
     * left-justified                             */
    UINT1     au1AuthKey[MAX_AUTH_KEY_LENGTH];
} tAuth;

           /********************************************************
           * The route information structure in a RIP message is   *
           * described below                                       *
           ********************************************************/

typedef struct
{
    /* 
     * Field u2Family distinguishes between the authentication information and
     * the route information. Depending on this, one of the structures in the
     * union "tRipMesg" is accessed.
     */
    UINT2   u2Family;
    UINT1   au1RouteTag[2];   /* The route domain field, which is assigned to
                               * a route which must be preserved and 
                               */
    UINT4   u4DestNet;        /* The destination network whose reachability is
                               * contained in this route information        
                               */
    UINT4   u4SubnetMask;     /* This contains the subnet mask which is
                               * applied to the IP address to yield the
                               * non-host portion of the address            
                               */
    UINT4   u4NextHop;        /* The immediate next hop IP address to which
                               * packets to the destination network specified
                               * by this route entry should be forwarded    
                               */
    UINT4   u4Metric;         /* The cost associated with this route entry  */
} tRoute;

           /********************************************************
           * A typical message format that a RIP packet can have   *
           * on a per route basis is described below               *
           ********************************************************/

typedef struct
{
    union
    {
        tAuth     Auth;
        tRoute    Route;
    } RipMesg;
} tRipInfo;

           /********************************************************
           * For easy accessing the elements of tRipInfo structure*
           * we define the following set of macros                 *
           ********************************************************/

#define     RIP_ADDRESS_FAMILY(pRipInfo)    \
            pRipInfo->RipMesg.Route.u2Family

              /* Accessing Authentication Information Structure */

#define   RIP_AUTH_TYPE(pRipInfo)    pRipInfo->RipMesg.Auth.u2AuthType
#define   RIP_AUTH_KEY(pRipInfo)     pRipInfo->RipMesg.Auth.au1AuthKey

                  /* Accessing Route Information Structure */

#define   RIP_ROUTE_TAG(pRipInfo)      (pRipInfo)->RipMesg.Route.au1RouteTag
#define   RIP_DEST_NET(pRipInfo)       (pRipInfo)->RipMesg.Route.u4DestNet
#define   RIP_SUBNET_MASK(pRipInfo)    (pRipInfo)->RipMesg.Route.u4SubnetMask
#define   RIP_NEXT_HOP(pRipInfo)       (pRipInfo)->RipMesg.Route.u4NextHop
#define   RIP_METRIC(pRipInfo)         (pRipInfo)->RipMesg.Route.u4Metric

           /********************************************************
           * A typical RIP message packet is being defined below.  *
           * This is used to enhance the speed in construction and *
           * processing of the RIP packet                          *
           ********************************************************/

typedef struct
{
    tRip      RipHdr;
    tRipInfo  aRipInfo[RIP_MAX_ROUTES_PER_PKT];
} tRipPkt;

typedef struct UPD_HDR
{
    UINT1     u1Version;
    UINT1     u1Flush;
    UINT2     u2SeqNo;
} tUpdHdr;

typedef struct
{
    tRip      RipHdr;
    tUpdHdr   UpdHdr;
    tRipInfo  aRipInfo[RIP_MAX_ROUTES_PER_PKT];
} tTrigRipPkt;

           /********************************************************
           * We need a structure as an interface between RIP and IP*
           * static route management module for all the necessary  *
           * information.                                          *
           ********************************************************/

typedef struct
{
    UINT2     u2If;
    UINT2     u2IfCounter; 
    tRoute    Route;
} tRipInfoBlk;

typedef struct MD5_INFO
{
    UINT2     u2RipPktLen;
    UINT1     u1KeyId;
    UINT1     u1AuthDataLen;
    UINT4     u4SeqNo;
    UINT4     u4Reserved1;
    UINT4     u4Reserved2;
} tMd5Info;

typedef tMd5Info tCryptoAuthInfo;

typedef struct RIP_PROTO_QMSG
{
    UINT4  u4MsgType;
    union {
  tCRU_BUF_CHAIN_HEADER *SnmpParms ;
  tCRU_BUF_CHAIN_HEADER *RtmParms ;
  tCRU_BUF_CHAIN_HEADER *RipParms ;
                tCRU_BUF_CHAIN_HEADER *RMapParms ;
  tCRU_BUF_CHAIN_HEADER *IfDeleteParms ;
   }msg; 
}tProtoQMsg;
typedef struct RIP_RESPONSE_QMSG
{
    tRipInfo aRipRtInfo[RIP_MAX_ROUTES_PER_PKT];/*Rip Route Information.*/
    UINT2  u2Len;       /* Length of the Rip Packet*/
    UINT1  u1Version;   /* Denotes version that RIP message corresponds to */
    UINT1  au1Pad[1];  
    UINT4  u4SrcAddr; /*Source Address*/
    UINT4  u4SeqNo;   /*Sequence Number.*/
    UINT4  u4CxtId;   /*Context Id Value.*/
    UINT2  u2IfIndex; /*Interface Index  value.*/
    UINT1  u1KeyId;   /*Authentication Key Id value.*/
    UINT1  u1Reserve;
}tRipRespQMsg;

#define RIP_GENERATE_REQUEST_HDR(RipPkt, pRipIfRec) \
           { \
              (RipPkt).RipHdr.u1Command  = (UINT1)RIP_UPDATE_REQUEST; \
              (RipPkt).RipHdr.u1Version  = (UINT1)(pRipIfRec->RipIfaceCfg.u2RipSendStatus & 3); \
              (RipPkt).RipHdr.u2Reserved = 0; \
              (RipPkt).UpdHdr.u1Version  = (UINT1)RIP_VERSION_1; \
              (RipPkt).UpdHdr.u1Flush    = 0; \
              (RipPkt).UpdHdr.u2SeqNo    = 0; \
           }

#define RIP_GENERATE_RESPONSE_HDR(RipPkt, pRipIfRec, flush) \
           { \
              (RipPkt).RipHdr.u1Command  = (UINT1)RIP_UPDATE_RESPONSE; \
              (RipPkt).RipHdr.u1Version  = (UINT1)(pRipIfRec->RipIfaceCfg.u2RipSendStatus & 3); \
              (RipPkt).RipHdr.u2Reserved = 0; \
              (RipPkt).UpdHdr.u1Version  = (UINT1)RIP_VERSION_1; \
              (RipPkt).UpdHdr.u1Flush    = flush; \
              (RipPkt).UpdHdr.u2SeqNo    = IP_HTONS((UINT2)(pRipIfRec->RipIfaceStats.u4SentTrigUpdates)); \
           }

#define DEMAND_RIP_GENERATE_REQUEST_HDR(pRouteEntry) \
           { \
           pRouteEntry->u2Family = 0; \
           pRouteEntry->au1RouteTag[0] = 0; \
           pRouteEntry->au1RouteTag[1] = 0; \
           pRouteEntry->u4DestNet = 0; \
           pRouteEntry->u4SubnetMask = 0; \
           pRouteEntry->u4NextHop = 0; \
           pRouteEntry->u4Metric = RIP_HTONL ((UINT4) RIP_INFINITY); \
           }

#define RIP_SNMP_MSG       1
#define RIP_RRD_MSG        2
#define RIP_INPUT_Q_MSG    3
#define RIP_VCM_MSG        4
#define RIP_ROUTEMAP_UPDATE_MSG 5
#define RIP_DELETE_IFREC_MSG 6
#endif /* End of #ifdef __RIP_PD_DFS_H__ Block Checking */
