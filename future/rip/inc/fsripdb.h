/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsripdb.h,v 1.12 2013/06/19 13:31:11 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRIPDB_H
#define _FSRIPDB_H

UINT1 FsRip2NBRTrustListTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRip2IfConfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRipMd5AuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRipCryptoAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRip2NBRUnicastListTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRip2LocalRoutingTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRipAggTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRip2PeerTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsRip2IfStatTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRipDistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsrip [] ={1,3,6,1,4,1,2076,75};
tSNMP_OID_TYPE fsripOID = {8, fsrip};


UINT4 FsRip2Security [ ] ={1,3,6,1,4,1,2076,75,1,1};
UINT4 FsRip2Peers [ ] ={1,3,6,1,4,1,2076,75,1,2};
UINT4 FsRip2TrustNBRListEnable [ ] ={1,3,6,1,4,1,2076,75,1,3};
UINT4 FsRip2NumberOfDroppedPkts [ ] ={1,3,6,1,4,1,2076,75,1,4};
UINT4 FsRip2SpacingEnable [ ] ={1,3,6,1,4,1,2076,75,1,5};
UINT4 FsRip2AutoSummaryStatus [ ] ={1,3,6,1,4,1,2076,75,1,6};
UINT4 FsRip2RetransTimeoutInt [ ] ={1,3,6,1,4,1,2076,75,1,7};
UINT4 FsRip2MaxRetransmissions [ ] ={1,3,6,1,4,1,2076,75,1,8};
UINT4 FsRip2OverSubscriptionTimeout [ ] ={1,3,6,1,4,1,2076,75,1,9};
UINT4 FsRip2Propagate [ ] ={1,3,6,1,4,1,2076,75,1,10};
UINT4 FsRip2MaxRoutes [ ] ={1,3,6,1,4,1,2076,75,1,11};
UINT4 FsRipTrcFlag [ ] ={1,3,6,1,4,1,2076,75,1,12};
UINT4 FsRip2TrustNBRIpAddr [ ] ={1,3,6,1,4,1,2076,75,1,13,1,1};
UINT4 FsRip2TrustNBRRowStatus [ ] ={1,3,6,1,4,1,2076,75,1,13,1,2};
UINT4 FsRip2IfConfAddress [ ] ={1,3,6,1,4,1,2076,75,1,14,1,1};
UINT4 FsRip2IfAdminStat [ ] ={1,3,6,1,4,1,2076,75,1,14,1,2};
UINT4 FsRip2IfConfOperState [ ] ={1,3,6,1,4,1,2076,75,1,14,1,3};
UINT4 FsRip2IfConfUpdateTmr [ ] ={1,3,6,1,4,1,2076,75,1,14,1,4};
UINT4 FsRip2IfConfGarbgCollectTmr [ ] ={1,3,6,1,4,1,2076,75,1,14,1,5};
UINT4 FsRip2IfConfRouteAgeTmr [ ] ={1,3,6,1,4,1,2076,75,1,14,1,6};
UINT4 FsRip2IfSplitHorizonStatus [ ] ={1,3,6,1,4,1,2076,75,1,14,1,7};
UINT4 FsRip2IfConfDefRtInstall [ ] ={1,3,6,1,4,1,2076,75,1,14,1,8};
UINT4 FsRip2IfConfSpacingTmr [ ] ={1,3,6,1,4,1,2076,75,1,14,1,9};
UINT4 FsRip2IfConfAuthType [ ] ={1,3,6,1,4,1,2076,75,1,14,1,10};
UINT4 FsRip2IfConfInUseKey [ ] ={1,3,6,1,4,1,2076,75,1,14,1,11};
UINT4 FsRip2IfConfAuthLastKeyStatus [ ] ={1,3,6,1,4,1,2076,75,1,14,1,12};
UINT4 FsRipMd5AuthAddress [ ] ={1,3,6,1,4,1,2076,75,1,15,1,1};
UINT4 FsRipMd5AuthKeyId [ ] ={1,3,6,1,4,1,2076,75,1,15,1,2};
UINT4 FsRipMd5AuthKey [ ] ={1,3,6,1,4,1,2076,75,1,15,1,3};
UINT4 FsRipMd5KeyStartTime [ ] ={1,3,6,1,4,1,2076,75,1,15,1,4};
UINT4 FsRipMd5KeyExpiryTime [ ] ={1,3,6,1,4,1,2076,75,1,15,1,5};
UINT4 FsRipMd5KeyRowStatus [ ] ={1,3,6,1,4,1,2076,75,1,15,1,6};
UINT4 FsRipCryptoAuthIfIndex [ ] ={1,3,6,1,4,1,2076,75,1,20,1,1};
UINT4 FsRipCryptoAuthAddress [ ] ={1,3,6,1,4,1,2076,75,1,20,1,2};
UINT4 FsRipCryptoAuthKeyId [ ] ={1,3,6,1,4,1,2076,75,1,20,1,3};
UINT4 FsRipCryptoAuthKey [ ] ={1,3,6,1,4,1,2076,75,1,20,1,4};
UINT4 FsRipCryptoKeyStartAccept [ ] ={1,3,6,1,4,1,2076,75,1,20,1,5};
UINT4 FsRipCryptoKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,75,1,20,1,6};
UINT4 FsRipCryptoKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,75,1,20,1,7};
UINT4 FsRipCryptoKeyStopAccept [ ] ={1,3,6,1,4,1,2076,75,1,20,1,8};
UINT4 FsRipCryptoKeyStatus [ ] ={1,3,6,1,4,1,2076,75,1,20,1,9};
UINT4 FsRip2NBRUnicastIpAddr [ ] ={1,3,6,1,4,1,2076,75,1,16,1,1};
UINT4 FsRip2NBRUnicastNBRRowStatus [ ] ={1,3,6,1,4,1,2076,75,1,16,1,2};
UINT4 FsRip2DestNet [ ] ={1,3,6,1,4,1,2076,75,1,17,1,1};
UINT4 FsRip2DestMask [ ] ={1,3,6,1,4,1,2076,75,1,17,1,2};
UINT4 FsRip2Tos [ ] ={1,3,6,1,4,1,2076,75,1,17,1,3};
UINT4 FsRip2NextHop [ ] ={1,3,6,1,4,1,2076,75,1,17,1,4};
UINT4 FsRip2RtIfIndex [ ] ={1,3,6,1,4,1,2076,75,1,17,1,5};
UINT4 FsRip2RtType [ ] ={1,3,6,1,4,1,2076,75,1,17,1,6};
UINT4 FsRip2Proto [ ] ={1,3,6,1,4,1,2076,75,1,17,1,7};
UINT4 FsRip2ChgTime [ ] ={1,3,6,1,4,1,2076,75,1,17,1,8};
UINT4 FsRip2Metric [ ] ={1,3,6,1,4,1,2076,75,1,17,1,9};
UINT4 FsRip2RowStatus [ ] ={1,3,6,1,4,1,2076,75,1,17,1,10};
UINT4 FsRip2Gateway [ ] ={1,3,6,1,4,1,2076,75,1,17,1,11};
UINT4 FsRipIfIndex [ ] ={1,3,6,1,4,1,2076,75,1,18,1,1};
UINT4 FsRipAggAddress [ ] ={1,3,6,1,4,1,2076,75,1,18,1,2};
UINT4 FsRipAggAddressMask [ ] ={1,3,6,1,4,1,2076,75,1,18,1,3};
UINT4 FsRipAggStatus [ ] ={1,3,6,1,4,1,2076,75,1,18,1,4};
UINT4 FsRipAdminStatus [ ] ={1,3,6,1,4,1,2076,75,1,19};
UINT4 FsRip2PeerInUseKey [ ] ={1,3,6,1,4,1,2076,75,1,21,1,1};
UINT4 FsRip2LastAuthKeyLifetimeStatus [ ] ={1,3,6,1,4,1,2076,75,1,22};
UINT4 FsRip2IfStatRcvBadAuthPackets [ ] ={1,3,6,1,4,1,2076,75,1,23,1,1};
UINT4 FsRipRtCount [ ] ={1,3,6,1,4,1,2076,75,1,24};
UINT4 FsRipRRDGlobalStatus [ ] ={1,3,6,1,4,1,2076,75,2,1};
UINT4 FsRipRRDSrcProtoMaskEnable [ ] ={1,3,6,1,4,1,2076,75,2,2};
UINT4 FsRipRRDSrcProtoMaskDisable [ ] ={1,3,6,1,4,1,2076,75,2,3};
UINT4 FsRipRRDRouteTagType [ ] ={1,3,6,1,4,1,2076,75,2,4};
UINT4 FsRipRRDRouteTag [ ] ={1,3,6,1,4,1,2076,75,2,5};
UINT4 FsRipRRDRouteDefMetric [ ] ={1,3,6,1,4,1,2076,75,2,6};
UINT4 FsRipRRDRouteMapEnable [ ] ={1,3,6,1,4,1,2076,75,2,7};
UINT4 FsRipDistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,75,3,1,1,1};
UINT4 FsRipDistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,75,3,1,1,3};
UINT4 FsRipDistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,75,3,1,1,4};
UINT4 FsRipDistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,75,3,1,1,5};
UINT4 FsRipPreferenceValue [ ] ={1,3,6,1,4,1,2076,75,4,1};
UINT4 FsRipAuthIfIndex [ ] ={1,3,6,1,4,1,2076,75,5,1};
UINT4 FsRipAuthKeyId [ ] ={1,3,6,1,4,1,2076,75,5,2};
UINT4 FsRipPeerAddress [ ] ={1,3,6,1,4,1,2076,75,5,3};




tMbDbEntry fsripMibEntry[]= {

{{10,FsRip2Security}, NULL, FsRip2SecurityGet, FsRip2SecuritySet, FsRip2SecurityTest, FsRip2SecurityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRip2Peers}, NULL, FsRip2PeersGet, FsRip2PeersSet, FsRip2PeersTest, FsRip2PeersDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRip2TrustNBRListEnable}, NULL, FsRip2TrustNBRListEnableGet, FsRip2TrustNBRListEnableSet, FsRip2TrustNBRListEnableTest, FsRip2TrustNBRListEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRip2NumberOfDroppedPkts}, NULL, FsRip2NumberOfDroppedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRip2SpacingEnable}, NULL, FsRip2SpacingEnableGet, FsRip2SpacingEnableSet, FsRip2SpacingEnableTest, FsRip2SpacingEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRip2AutoSummaryStatus}, NULL, FsRip2AutoSummaryStatusGet, FsRip2AutoSummaryStatusSet, FsRip2AutoSummaryStatusTest, FsRip2AutoSummaryStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsRip2RetransTimeoutInt}, NULL, FsRip2RetransTimeoutIntGet, FsRip2RetransTimeoutIntSet, FsRip2RetransTimeoutIntTest, FsRip2RetransTimeoutIntDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FsRip2MaxRetransmissions}, NULL, FsRip2MaxRetransmissionsGet, FsRip2MaxRetransmissionsSet, FsRip2MaxRetransmissionsTest, FsRip2MaxRetransmissionsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "36"},

{{10,FsRip2OverSubscriptionTimeout}, NULL, FsRip2OverSubscriptionTimeoutGet, FsRip2OverSubscriptionTimeoutSet, FsRip2OverSubscriptionTimeoutTest, FsRip2OverSubscriptionTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "180"},

{{10,FsRip2Propagate}, NULL, FsRip2PropagateGet, FsRip2PropagateSet, FsRip2PropagateTest, FsRip2PropagateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRip2MaxRoutes}, NULL, FsRip2MaxRoutesGet, FsRip2MaxRoutesSet, FsRip2MaxRoutesTest, FsRip2MaxRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4000"},

{{10,FsRipTrcFlag}, NULL, FsRipTrcFlagGet, FsRipTrcFlagSet, FsRipTrcFlagTest, FsRipTrcFlagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsRip2TrustNBRIpAddr}, GetNextIndexFsRip2NBRTrustListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRip2NBRTrustListTableINDEX, 1, 0, 0, NULL},

{{12,FsRip2TrustNBRRowStatus}, GetNextIndexFsRip2NBRTrustListTable, FsRip2TrustNBRRowStatusGet, FsRip2TrustNBRRowStatusSet, FsRip2TrustNBRRowStatusTest, FsRip2NBRTrustListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip2NBRTrustListTableINDEX, 1, 0, 1, NULL},

{{12,FsRip2IfConfAddress}, GetNextIndexFsRip2IfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRip2IfConfTableINDEX, 1, 0, 0, NULL},

{{12,FsRip2IfAdminStat}, GetNextIndexFsRip2IfConfTable, FsRip2IfAdminStatGet, FsRip2IfAdminStatSet, FsRip2IfAdminStatTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, NULL},

{{12,FsRip2IfConfOperState}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRip2IfConfTableINDEX, 1, 0, 0, "2"},

{{12,FsRip2IfConfUpdateTmr}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfUpdateTmrGet, FsRip2IfConfUpdateTmrSet, FsRip2IfConfUpdateTmrTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "30"},

{{12,FsRip2IfConfGarbgCollectTmr}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfGarbgCollectTmrGet, FsRip2IfConfGarbgCollectTmrSet, FsRip2IfConfGarbgCollectTmrTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "120"},

{{12,FsRip2IfConfRouteAgeTmr}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfRouteAgeTmrGet, FsRip2IfConfRouteAgeTmrSet, FsRip2IfConfRouteAgeTmrTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "180"},

{{12,FsRip2IfSplitHorizonStatus}, GetNextIndexFsRip2IfConfTable, FsRip2IfSplitHorizonStatusGet, FsRip2IfSplitHorizonStatusSet, FsRip2IfSplitHorizonStatusTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "2"},

{{12,FsRip2IfConfDefRtInstall}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfDefRtInstallGet, FsRip2IfConfDefRtInstallSet, FsRip2IfConfDefRtInstallTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "2"},

{{12,FsRip2IfConfSpacingTmr}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfSpacingTmrGet, FsRip2IfConfSpacingTmrSet, FsRip2IfConfSpacingTmrTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "0"},

{{12,FsRip2IfConfAuthType}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfAuthTypeGet, FsRip2IfConfAuthTypeSet, FsRip2IfConfAuthTypeTest, FsRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip2IfConfTableINDEX, 1, 0, 0, "1"},

{{12,FsRip2IfConfInUseKey}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfInUseKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2IfConfTableINDEX, 1, 0, 0, NULL},

{{12,FsRip2IfConfAuthLastKeyStatus}, GetNextIndexFsRip2IfConfTable, FsRip2IfConfAuthLastKeyStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRip2IfConfTableINDEX, 1, 0, 0, "2"},

{{12,FsRipMd5AuthAddress}, GetNextIndexFsRipMd5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRipMd5AuthTableINDEX, 2, 0, 0, NULL},

{{12,FsRipMd5AuthKeyId}, GetNextIndexFsRipMd5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRipMd5AuthTableINDEX, 2, 0, 0, NULL},

{{12,FsRipMd5AuthKey}, GetNextIndexFsRipMd5AuthTable, FsRipMd5AuthKeyGet, FsRipMd5AuthKeySet, FsRipMd5AuthKeyTest, FsRipMd5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRipMd5AuthTableINDEX, 2, 0, 0, NULL},

{{12,FsRipMd5KeyStartTime}, GetNextIndexFsRipMd5AuthTable, FsRipMd5KeyStartTimeGet, FsRipMd5KeyStartTimeSet, FsRipMd5KeyStartTimeTest, FsRipMd5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRipMd5AuthTableINDEX, 2, 0, 0, NULL},

{{12,FsRipMd5KeyExpiryTime}, GetNextIndexFsRipMd5AuthTable, FsRipMd5KeyExpiryTimeGet, FsRipMd5KeyExpiryTimeSet, FsRipMd5KeyExpiryTimeTest, FsRipMd5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRipMd5AuthTableINDEX, 2, 0, 0, NULL},

{{12,FsRipMd5KeyRowStatus}, GetNextIndexFsRipMd5AuthTable, FsRipMd5KeyRowStatusGet, FsRipMd5KeyRowStatusSet, FsRipMd5KeyRowStatusTest, FsRipMd5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRipMd5AuthTableINDEX, 2, 0, 1, NULL},

{{12,FsRip2NBRUnicastIpAddr}, GetNextIndexFsRip2NBRUnicastListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRip2NBRUnicastListTableINDEX, 1, 0, 0, NULL},

{{12,FsRip2NBRUnicastNBRRowStatus}, GetNextIndexFsRip2NBRUnicastListTable, FsRip2NBRUnicastNBRRowStatusGet, FsRip2NBRUnicastNBRRowStatusSet, FsRip2NBRUnicastNBRRowStatusTest, FsRip2NBRUnicastListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip2NBRUnicastListTableINDEX, 1, 0, 1, NULL},

{{12,FsRip2DestNet}, GetNextIndexFsRip2LocalRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2DestMask}, GetNextIndexFsRip2LocalRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2Tos}, GetNextIndexFsRip2LocalRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2NextHop}, GetNextIndexFsRip2LocalRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2RtIfIndex}, GetNextIndexFsRip2LocalRoutingTable, FsRip2RtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2RtType}, GetNextIndexFsRip2LocalRoutingTable, FsRip2RtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2Proto}, GetNextIndexFsRip2LocalRoutingTable, FsRip2ProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2ChgTime}, GetNextIndexFsRip2LocalRoutingTable, FsRip2ChgTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2Metric}, GetNextIndexFsRip2LocalRoutingTable, FsRip2MetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2RowStatus}, GetNextIndexFsRip2LocalRoutingTable, FsRip2RowStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRip2Gateway}, GetNextIndexFsRip2LocalRoutingTable, FsRip2GatewayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsRip2LocalRoutingTableINDEX, 4, 0, 0, NULL},

{{12,FsRipIfIndex}, GetNextIndexFsRipAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRipAggTableINDEX, 3, 0, 0, NULL},

{{12,FsRipAggAddress}, GetNextIndexFsRipAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRipAggTableINDEX, 3, 0, 0, NULL},

{{12,FsRipAggAddressMask}, GetNextIndexFsRipAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRipAggTableINDEX, 3, 0, 0, NULL},

{{12,FsRipAggStatus}, GetNextIndexFsRipAggTable, FsRipAggStatusGet, FsRipAggStatusSet, FsRipAggStatusTest, FsRipAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRipAggTableINDEX, 3, 0, 1, NULL},

{{10,FsRipAdminStatus}, NULL, FsRipAdminStatusGet, FsRipAdminStatusSet, FsRipAdminStatusTest, FsRipAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsRipCryptoAuthIfIndex}, GetNextIndexFsRipCryptoAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoAuthAddress}, GetNextIndexFsRipCryptoAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoAuthKeyId}, GetNextIndexFsRipCryptoAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoAuthKey}, GetNextIndexFsRipCryptoAuthTable, FsRipCryptoAuthKeyGet, FsRipCryptoAuthKeySet, FsRipCryptoAuthKeyTest, FsRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoKeyStartAccept}, GetNextIndexFsRipCryptoAuthTable, FsRipCryptoKeyStartAcceptGet, FsRipCryptoKeyStartAcceptSet, FsRipCryptoKeyStartAcceptTest, FsRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoKeyStartGenerate}, GetNextIndexFsRipCryptoAuthTable, FsRipCryptoKeyStartGenerateGet, FsRipCryptoKeyStartGenerateSet, FsRipCryptoKeyStartGenerateTest, FsRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoKeyStopGenerate}, GetNextIndexFsRipCryptoAuthTable, FsRipCryptoKeyStopGenerateGet, FsRipCryptoKeyStopGenerateSet, FsRipCryptoKeyStopGenerateTest, FsRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoKeyStopAccept}, GetNextIndexFsRipCryptoAuthTable, FsRipCryptoKeyStopAcceptGet, FsRipCryptoKeyStopAcceptSet, FsRipCryptoKeyStopAcceptTest, FsRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRipCryptoKeyStatus}, GetNextIndexFsRipCryptoAuthTable, FsRipCryptoKeyStatusGet, FsRipCryptoKeyStatusSet, FsRipCryptoKeyStatusTest, FsRipCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRipCryptoAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsRip2PeerInUseKey}, GetNextIndexFsRip2PeerTable, FsRip2PeerInUseKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRip2PeerTableINDEX, 2, 0, 0, NULL},

{{10,FsRip2LastAuthKeyLifetimeStatus}, NULL, FsRip2LastAuthKeyLifetimeStatusGet, FsRip2LastAuthKeyLifetimeStatusSet, FsRip2LastAuthKeyLifetimeStatusTest, FsRip2LastAuthKeyLifetimeStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsRip2IfStatRcvBadAuthPackets}, GetNextIndexFsRip2IfStatTable, FsRip2IfStatRcvBadAuthPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsRip2IfStatTableINDEX, 1, 0, 0, NULL},

{{10,FsRipRtCount}, NULL, FsRipRtCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRipRRDGlobalStatus}, NULL, FsRipRRDGlobalStatusGet, FsRipRRDGlobalStatusSet, FsRipRRDGlobalStatusTest, FsRipRRDGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRipRRDSrcProtoMaskEnable}, NULL, FsRipRRDSrcProtoMaskEnableGet, FsRipRRDSrcProtoMaskEnableSet, FsRipRRDSrcProtoMaskEnableTest, FsRipRRDSrcProtoMaskEnableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRipRRDSrcProtoMaskDisable}, NULL, FsRipRRDSrcProtoMaskDisableGet, FsRipRRDSrcProtoMaskDisableSet, FsRipRRDSrcProtoMaskDisableTest, FsRipRRDSrcProtoMaskDisableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRipRRDRouteTagType}, NULL, FsRipRRDRouteTagTypeGet, FsRipRRDRouteTagTypeSet, FsRipRRDRouteTagTypeTest, FsRipRRDRouteTagTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsRipRRDRouteTag}, NULL, FsRipRRDRouteTagGet, FsRipRRDRouteTagSet, FsRipRRDRouteTagTest, FsRipRRDRouteTagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRipRRDRouteDefMetric}, NULL, FsRipRRDRouteDefMetricGet, FsRipRRDRouteDefMetricSet, FsRipRRDRouteDefMetricTest, FsRipRRDRouteDefMetricDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,FsRipRRDRouteMapEnable}, NULL, FsRipRRDRouteMapEnableGet, FsRipRRDRouteMapEnableSet, FsRipRRDRouteMapEnableTest, FsRipRRDRouteMapEnableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsRipDistInOutRouteMapName}, GetNextIndexFsRipDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRipDistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRipDistInOutRouteMapType}, GetNextIndexFsRipDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRipDistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRipDistInOutRouteMapValue}, GetNextIndexFsRipDistInOutRouteMapTable, FsRipDistInOutRouteMapValueGet, FsRipDistInOutRouteMapValueSet, FsRipDistInOutRouteMapValueTest, FsRipDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRipDistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRipDistInOutRouteMapRowStatus}, GetNextIndexFsRipDistInOutRouteMapTable, FsRipDistInOutRouteMapRowStatusGet, FsRipDistInOutRouteMapRowStatusSet, FsRipDistInOutRouteMapRowStatusTest, FsRipDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRipDistInOutRouteMapTableINDEX, 2, 0, 1, NULL},

{{10,FsRipPreferenceValue}, NULL, FsRipPreferenceValueGet, FsRipPreferenceValueSet, FsRipPreferenceValueTest, FsRipPreferenceValueDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "121"},

{{10,FsRipAuthIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRipAuthKeyId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsRipPeerAddress}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsripEntry = { 78, fsripMibEntry };

#endif /* _FSRIPDB_H */

