/* $Id: fsmistwr.h,v 1.4 2013/05/23 12:44:50 siva Exp $  */
#ifndef _FSMISTWR_H
#define _FSMISTWR_H
INT4 GetNextIndexFsMIStdRip2GlobalTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMISTRIP(VOID);
INT4 FsMIStdRip2GlobalRouteChangesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2GlobalQueriesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsMIStdRip2IfStatTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdRip2IfStatRcvBadPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatRcvBadRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatSentUpdatesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatPeriodicUpdatesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatRcvBadAuthPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfStatTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdRip2IfConfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdRip2IfConfDomainGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfAuthTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfAuthKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfSendGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfReceiveGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfDefaultMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfSrcAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfDomainSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfAuthTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfAuthKeySet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfSendSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfReceiveSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfDefaultMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfSrcAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfDomainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfAuthTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfAuthKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfSendTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfReceiveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfDefaultMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfSrcAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2IfConfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIStdRip2PeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIStdRip2PeerLastUpdateGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2PeerVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2PeerRcvBadPacketsGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2PeerRcvBadRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIStdRip2PeerInUseKeyGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMISTWR_H */
