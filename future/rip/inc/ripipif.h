/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripipif.h,v 1.27 2017/09/20 13:07:49 siva Exp $
 *
 * Description:This file contains the routines for initiali-
 *             -zing the RIP data structures, Timer handler  
 *             routines, shutdown routines              
 *
 *******************************************************************/
#ifndef   __RIP_IPIF_H__
#define   __RIP_IPIF_H__

/*************************************************************************
 Functional Interfaces Expected from IP: 
**************************************************************************/
#define RIPIF_GLBTAB_ADDR_OF(u2Port)         RipIfGetAddrFromPort (u2Port)
#define RIPIF_GLBTAB_MASK_OF(u2Port)         RipIfGetMaskFromPort (u2Port)
#define RIPIF_GLBTAB_OPER_OF(u2Port)        RipIfGetOperStatusFromPort (u2Port)
#define RIP_DEFAULT_NET_MASK(u4DestNet)      RipIfGetDflNetMask (u4DestNet)
#define RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT(u4CxtId, u4Addr) \
                                             RipIfGetIndexFromAddrInCxt (u4CxtId, u4Addr) 
#define RIPIF_GET_PORT_FROM_INDEX(u4IfIndex, pu2Port) \
                                             RipIfGetIpPortFromIndex (u4IfIndex, pu2Port)
#define RIPIF_GET_IFID_FROM_INDEX(u2IfInd, pIfId) \
                                             RipIfGetIfIdFromIfIndex (u2IfInd, pIfId)
#define RIPIF_IS_UNNUMBERED(u2Port) RipIfIsUnnumbered(u2Port)


#define RIPIF_GET_IFCONFIG_RECORD(u2Port, IpRecord) \
{\
    if (IpGetIfConfigRecord (u2Port, &IpRecord) != IP_SUCCESS)\
    {\
       return RIP_FAILURE;\
    }\
}
/*************************************************************************
Macros to be locally defined in RIP
**************************************************************************/

#define   RIP_MEM_SUCCESS    MEM_SUCCESS /* future/ip/inc/cruport.h */
#define   RIP_ADDRESS_ABOVE_CLASS_C(u4Addr)    (u4Addr > 0xdfffffff) 

#define RIP_ALLOC_IF_REC_MEM(pu1Block) \
   (pu1Block = (tRipIfaceRec *)(MemAllocMemBlk ((tMemPoolId)(IpRipMemory.IfQId))))

#define RIP_RELEASE_IF_REC_MEM(ppu1Block) \
   MemReleaseMemBlock ((tMemPoolId)(IpRipMemory.IfQId) ,(UINT1 *) ppu1Block)

/*CLI constants need for dynamic sync audit implementation*/
#define RIP_AUDIT_FILE_ACTIVE        "/tmp/rip_output_file_active"
#define RIP_AUDIT_FILE_STDBY         "/tmp/rip_output_file_stdby"
#define RIP_DYN_MAX_CMDS             2
#define RIP_CLI_EOF                  2
#define RIP_CLI_NO_EOF               1
#define RIP_CLI_RDONLY               OSIX_FILE_RO
#define RIP_CLI_WRONLY               OSIX_FILE_WO
#define RIP_CLI_MAX_GROUPS_LINE_LEN  200

/********************
 CRU Related Macros 
********************/

#define   RIPIF_IS_INVALID_ID(IfId) \
          (RIP_GET_IFACE_TYPE((IfId)) == IP_IF_TYPE_INVALID)

#define RIP_GET_IFACE_TYPE(tInterface)                  \
   CRU_BUF_Get_Interface_Type((tInterface))

#define RIP_GET_IFACE_NUM(tInterface)                   \
   CRU_BUF_Get_Interface_Num((tInterface))

#define RIP_GET_IFACE_SUBREF(tInterface)                \
   CRU_BUF_Get_Interface_SubRef((tInterface))

#define RIP_GET_IFACE_INDEX(tInterface)                \
   CRU_BUF_Get_IfIndex((tInterface))


/* Interface ID Macros */


#define   tRIP_TRAP_PARM     tCRU_TRAP_PARM       
#define   tRIP_INTERFACE     tCRU_INTERFACE       

#define   RIP_HTONL(u4Key)   OSIX_HTONL(u4Key)     
#define   RIP_NTOHL(u4Key)   OSIX_NTOHL(u4Key)     
#define   RIP_NTOHS(x)       OSIX_NTOHS(x)         
#define   RIP_HTONS(x)       OSIX_HTONS(x)         

#define   tRIP_BUF_CHAIN_HEADER              tCRU_BUF_CHAIN_HEADER

#define   RIP_GET_MODULE_DATA_PTR(pBuf)    IP_GET_MODULE_DATA_PTR(pBuf)

#define   RIP_ALLOC_PEER_REC_MEM(pRipCxtEntry,x)  \
          (x = (tRipPeerRec *)IP_ALLOCATE_MEM_BLOCK(RIPMemPoolIds[MAX_RIP_PEERS_SIZING_ID]))

#define   RIP_FREE_PEER_REC_MEM(pRipCxtEntry,x)  \
          IP_RELEASE_MEM_BLOCK(RIPMemPoolIds[MAX_RIP_PEERS_SIZING_ID], x)

#define   RIP_ALLOC_IMPORT_ROUTE(pRipCxtEntry, x)  \
          (x =(tRipImportList *) IP_ALLOCATE_MEM_BLOCK(IpRipMemory.ImportRtPoolId))

#define   RIP_FREE_IMPORT_ROUTE(pRipCxtEntry, x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.ImportRtPoolId, x)

#define   RIP_ALLOC_NBR_NODE(pRipIfRec,x)  \
          (x = (tRipUnicastNBRS *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.UnicastNbrPoolId))

#define   RIP_FREE_NBR_NODE(pRipIfRec, x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.UnicastNbrPoolId, x)

#define   RIP_ALLOC_AUTH_KEY_NODE(pRipIfRec,x)  \
          (x = (tMd5AuthKeyInfo *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.AuthKeyPoolId))

#define   RIP_ALLOC_CRYPTO_AUTH_KEY_NODE(pRipIfRec,x)  \
          (x = (tCryptoAuthKeyInfo *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.CryptoAuthKeyPoolId))

#define   RIP_FREE_AUTH_KEY_NODE(pRipIfRec, x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.AuthKeyPoolId, x)

#define   RIP_FREE_CRYPTO_AUTH_KEY_NODE(pRipIfRec, x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.CryptoAuthKeyPoolId, x)

#define   RIP_ALLOC_RMAP_FILTER_NODE(pRipCxtEntry,x)  \
          (x = (tFilteringRMap *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.RMapFilterPoolId))

#define   RIP_FREE_RMAP_FILTER_NODE(pRipCxtEntry, x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.RMapFilterPoolId, x)

#define   RIP_ALLOC_RTM_ROUTE(x)  \
          (x =(tExpRtNode *) IP_ALLOCATE_MEM_BLOCK(IpRipMemory.RespPoolId))

#define   RIP_FREE_RTM_ROUTE(x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.RespPoolId, x)

#define   RIP_ALLOC_ROUTE_ENTRY(x)  \
          (x= (tRipRtEntry *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.RtEntryQId))

#define   RIP_ALLOC_SUMMARY_ENTRY(x)  \
          (x= (tRipRtEntry *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.SumEntryQId))

#define   RIP_ALLOC_QMSG_ENTRY(x)  \
          (x = (tProtoQMsg *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.RtQMsgId))

#define   RIP_ALLOC_LOCAL_ROUTE_ENTRY(x)  \
          (x= (tRipRtEntry *)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.LocalRtEntryQId))

#define   RIP_FREE_LOCAL_ROUTE_ENTRY(pRt)  \
           if(pRt != 0)\
           (IP_RELEASE_MEM_BLOCK(IpRipMemory.LocalRtEntryQId, pRt))



#define   RIP_FREE_QMSG_ENTRY(x)  \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.RtQMsgId, x)

#define   RIP_ROUTE_FREE(pRt) \
   if(pRt != 0)\
   (IP_RELEASE_MEM_BLOCK(IpRipMemory.RtEntryQId, pRt))
          
#define   RIP_SUMMARY_FREE(pRt) \
          IP_RELEASE_MEM_BLOCK(IpRipMemory.SumEntryQId, pRt)

#define   RIP_ALLOC_CXT_ENTRY(x)\
          (x = (tRipCxt*)IP_ALLOCATE_MEM_BLOCK(IpRipMemory.CxtPoolId))

#define   RIP_FREE_CXT_ENTRY(x)\
          IP_RELEASE_MEM_BLOCK (IpRipMemory.CxtPoolId, x)

#define   RIP_MEMORY_ALLOCATION_FAILED        CRU_MEMORY_ALLOCATION_FAILED


#define   RIP_TRAP_TABLE                 CRU_RIP_TABLE                 

#define   RIPIF_INVALIDATE_IF_ID(IfId)   \
          (RIP_GET_IFACE_TYPE((IfId)) = IP_IF_TYPE_INVALID)

#ifdef L2RED_WANTED
#define RIP_GET_NODE_STATUS()  gRipRedGblInfo.u1NodeStatus
#else
#define RIP_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define RIP_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define RIP_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#define RIPHA_ADD_ROUTE   1
#define RIPHA_DEL_ROUTE   2
#define RIPHA_ADD_SUM_ROUTE   3
#define RIPHA_DEL_SUM_ROUTE   4

#define RIP_TWO 2

/********************
 TMO Related Macros 
********************/

#define   RIP_SLL_Init(x)               TMO_SLL_Init(x) 
#define   RIP_SLL_Add(x, y)             TMO_SLL_Add(x, y) 
#define   RIP_SLL_Scan(x, y, z)         TMO_SLL_Scan(x, y, z) 
#define   RIP_SLL_FreeNodes(x)          TMO_SLL_FreeNodes(x ,0)
#define   tRIP_SLL                      tTMO_SLL 
#define   tRIP_SLL_NODE                 tTMO_SLL_NODE
#define   RIP_SLL_First(x)              TMO_SLL_First(x) 
#define   RIP_SLL_Next(pList, pNode)    TMO_SLL_Next(pList, pNode)
#define   RIP_SLL_Delete(pList,pNode)   TMO_SLL_Delete(pList,pNode)

/***********************
 TIMER Related Macros 
***********************/
#define   RIP_SLL_Count(x)              TMO_SLL_Count(x)

/***********************
 TIMER Related Macros 
***********************/
typedef tTimerListId tRipTmrListId;

#define RIP_CREATE_TMR_LIST(au1TaskName, u4Event, pTmrListId)  \
       TmrCreateTimerList((au1TaskName), (u4Event), NULL, (pTmrListId))

#define RIP_START_TIMER(TimerListId, pTmrRef, u4Duration)  \
    if (RIP_GET_NODE_STATUS () == RM_ACTIVE)\
       TmrStartTimer((TimerListId), (pTmrRef), \
           SYS_NUM_OF_TIME_UNITS_IN_A_SEC * (u4Duration))

#define RIP_STOP_TIMER(TimerListId, pTmrRef) \
       TmrStopTimer((TimerListId), (pTmrRef))

#define RIP_RESTART_TIMER(TimerListId, pTmrRef, u4Duration)  \
       if (RIP_GET_NODE_STATUS () == RM_ACTIVE)\
       {\
           TmrStopTimer((TimerListId), (pTmrRef)); \
           TmrStartTimer((TimerListId), (pTmrRef), \
           SYS_NUM_OF_TIME_UNITS_IN_A_SEC * (u4Duration)); \
       }

#define RIP_GET_EXPIRED_TIMERS(TimerListId, ppTmrRef)   \
        TmrGetExpiredTimers((TimerListId), (ppTmrRef))

#define RIP_NEXT_TIMER(TimerListId)  \
       TmrGetNextExpiredTimer((TimerListId))

#define RIP_GET_REMAINING_TIME(TimerListId, pTmrRef, pu4Time)   \
        TmrGetRemainingTime((TimerListId), (pTmrRef), (pu4Time))

/*************************************************************************
Ensure Consistency:
**************************************************************************/

#define RIP_RELEASE_BUF(pBuf, u1ForceFlag)                      \
   CRU_BUF_Release_MsgBufChain((pBuf), (u1ForceFlag))

#define RIP_ALLOCATE_BUF(u4Size, u4ValidOffset)                 \
   CRU_BUF_Allocate_MsgBufChain((u4Size), (u4ValidOffset))


#define  RIP_SUCCESS               0
#define  RIP_FAILURE              -1
#define  RIP_EXIT_LOOP            -2
#define  RIP_CONTINUE_LOOP        -3
#define  RIP_RTM_ACK_NOT_RECEIVED -4
#define  RIP_BUF_COPY_FAILURE     -5
#define  RIP_NO_ROOM              -6
#define  RIP_DUPLICATE_ROUTE      -7
#define  RIP_UNKNOWN_VALUE        -8
#define  RIP_ROUTE_NOT_FOUND      -9
#define  RIP_UPDATE_NOT_SEND       1
#define  RIP_UPDATE_SEND           2
#define  RIP_SPACE_START           1
#define RIP_NO_SEMAPHORE          -10
#define RIP_NO_QUEUE              -11
#define RIP_NO_TIMER_LIST         -12
#define RIP_NO_MEMORY             -13

#define   RIP_NAME       "RIP"    
#define   RIP_MOD_TRC      0x00080000

#define   RIP_TRC_FLAG_MIN  0
#define   RIP_TRC_FLAG_MAX  0xffff

#define   MUL_MASK  0xe0000009
#define   CLASSA_MASK 0xff000000 
#define   CLASSB_MASK 0xffff0000 
#define   CLASSC_MASK 0xffffff00 

#define  RIP_CONTEXT_INFO(x, CxtId) "Context-%d:" x, CxtId


#define RIP_TRC_INV_CXT_ID(cmod, trcflag,cxtid,mask,mod,fmt) \
        UtlTrcLog(trcflag, \
                      mask, \
                      mod,  \
                      fmt)


#define RIP_TRC(cmod, trcflag,cxtid,mask,mod,fmt) \
if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, RIP_CONTEXT_INFO (fmt, cxtid));\
 }\
 else\
 {\
        UtlTrcLog(trcflag, \
                     mask, \
                     mod,  \
                     RIP_CONTEXT_INFO (fmt, cxtid));\
 }

#define RIP_TRC_ARG1(cmod, trcflag,cxtid, mask,  mod, fmt, arg1) \
{ \
     if ((UINT4)cxtid == RIP_INVALID_CXT_ID)\
     {\
         if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
         {\
             UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, fmt, arg1);\
         }\
         else\
         {\
        UtlTrcLog(trcflag, \
                      mask, \
                      mod,  \
                      fmt, arg1);\
     }\
     }\
     else \
     { \
         if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
          {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, RIP_CONTEXT_INFO (fmt, cxtid), arg1);\
          }\
     else \
     { \
        UtlTrcLog(trcflag, \
                     mask, \
                     mod,  \
                     RIP_CONTEXT_INFO (fmt, cxtid), arg1);\
     }\
     }\
}

#define RIP_TRC_ARG2( cmod,trcflag,cxtid,mask, mod, fmt,arg1,arg2) \
{ \
     if ((UINT4)cxtid == RIP_INVALID_CXT_ID)\
     {\
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, fmt, arg1, arg2);\
        }\
        else\
        {\
        UtlTrcLog(trcflag, \
                      mask, \
                      mod,  \
                      fmt, arg1, arg2);\
     }\
    }\
    else \
    { \
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2);\
        }\
     else \
     { \
        UtlTrcLog(trcflag, \
                     mask, \
                     mod,  \
                     RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2);\
     }\
    }\
}

#define RIP_TRC_ARG3( cmod,trcflag,cxtid, mask, mod, fmt,arg1,arg2,arg3) \
{ \
     if ((UINT4)cxtid == RIP_INVALID_CXT_ID)\
     {\
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, fmt, arg1, arg2, arg3);\
        }\
        else\
        {\
        UtlTrcLog(trcflag, \
                      mask, \
                      mod,  \
                      fmt, arg1, arg2, arg3);\
     }\
    }\
    else \
    { \
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2, arg3);\
        }\
     else \
     { \
        UtlTrcLog(trcflag, \
                     mask, \
                     mod,  \
                     RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2, arg3);\
     }\
    }\
}

#define RIP_TRC_ARG4( cmod, trcflag,cxtid,mask, mod, fmt,arg1,arg2,arg3,arg4) \
{ \
     if ((UINT4)cxtid == RIP_INVALID_CXT_ID)\
     {\
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, fmt, arg1, arg2, arg3, arg4);\
        }\
        else\
        {\
        UtlTrcLog(trcflag, \
                      mask, \
                      mod,  \
                      fmt, arg1, arg2, arg3, arg4);\
     }\
    }\
    else \
    { \
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2, arg3, arg4);\
        }\
     else \
     { \
        UtlTrcLog(trcflag, \
                     mask, \
                     mod,  \
                     RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2, arg3, arg4);\
     }\
    }\
}
            
#define RIP_TRC_ARG5( cmod, trcflag,cxtid,mask, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
{ \
     if ((UINT4)cxtid == RIP_INVALID_CXT_ID)\
     {\
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, fmt, arg1, arg2, arg3, arg4, arg5);\
        }\
        else\
        {\
        UtlTrcLog(trcflag, \
                      mask, \
                      mod,  \
                      fmt, arg1, arg2, arg3, arg4, arg5);\
     }\
    }\
    else \
    { \
        if (((mask) & ALL_FAILURE_TRC) || ((mask) & RIP_CRITICAL_TRC ))\
        {\
            UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, trcflag, mask, mod, mod, RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2, arg3, arg4, arg5);\
        }\
     else \
     { \
        UtlTrcLog(trcflag, \
                     mask, \
                     mod,  \
                     RIP_CONTEXT_INFO (fmt, cxtid), arg1, arg2, arg3, arg4, arg5);\
     }\
    }\
}           
#endif
/*************Added a macro for calculating minimum of two values********/
#define RIP_MIN(a,b)   (a < b) ? a : b

