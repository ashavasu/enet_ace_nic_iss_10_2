/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripsz.h,v 1.6 2016/07/16 11:13:49 siva Exp $
 *
 * Description:
 *             
 *******************************************************************/
 enum {
    MAX_RIP_AGG_RT_ENTRIES_SIZING_ID,
    MAX_RIP_CONTEXT_SIZING_ID,
    MAX_RIP_CRYPTO_AUTH_KEYS_SIZING_ID,
    MAX_RIP_INTERFACES_SIZING_ID,
    MAX_RIP_IPIF_LOGICAL_IFACES_SIZING_ID,
    MAX_RIP_MD5_AUTH_KEYS_SIZING_ID,
    MAX_RIP_NBRS_SIZING_ID,
    MAX_RIP_PEERS_SIZING_ID,
    MAX_RIP_QUE_DEPTH_SIZING_ID,
    MAX_RIP_REDISTRIBUTE_ROUTES_SIZING_ID,
    MAX_RIP_RESP_MSG_NUM_SIZING_ID,
    MAX_RIP_RMAP_FILTERS_SIZING_ID,
    MAX_RIP_ROUTE_ENTRIES_SIZING_ID,
    MAX_RIP_ROUTE_NODES_SIZING_ID,
    MAX_RIP_SUMMARY_ENTRIES_SIZING_ID,
    MAX_RIP_RM_QUE_DEPTH_SIZING_ID,
    MAX_RIP_LOCAL_ROUTE_ENTRIES_SIZING_ID,
    RIP_MAX_SIZING_ID
};


#ifdef  _RIPSZ_C
tMemPoolId RIPMemPoolIds[ RIP_MAX_SIZING_ID];
INT4  RipSizingMemCreateMemPools(VOID);
VOID  RipSizingMemDeleteMemPools(VOID);
INT4  RipSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RIPSZ_C  */
extern tMemPoolId RIPMemPoolIds[ ];
extern INT4  RipSizingMemCreateMemPools(VOID);
extern VOID  RipSizingMemDeleteMemPools(VOID);
extern INT4  RipSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif /*  _RIPSZ_C  */


#ifdef  _RIPSZ_C
tFsModSizingParams FsRIPSizingParams [] = {
{ "tRipIfAggRtInfo", "MAX_RIP_AGG_RT_ENTRIES", sizeof(tRipIfAggRtInfo),MAX_RIP_AGG_RT_ENTRIES, MAX_RIP_AGG_RT_ENTRIES,0 },
{ "tRipCxt", "MAX_RIP_CONTEXT", sizeof(tRipCxt),MAX_RIP_CONTEXT, MAX_RIP_CONTEXT,0 },
{ "tCryptoAuthKeyInfo", "MAX_RIP_CRYPTO_AUTH_KEYS", sizeof(tCryptoAuthKeyInfo),MAX_RIP_CRYPTO_AUTH_KEYS, MAX_RIP_CRYPTO_AUTH_KEYS,0 },
{ "tRipIfaceRec", "MAX_RIP_INTERFACES", sizeof(tRipIfaceRec),MAX_RIP_INTERFACES, MAX_RIP_INTERFACES,0 },
{ "tRipIfAggTblRoot", "MAX_RIP_IPIF_LOGICAL_IFACES", sizeof(tRipIfAggTblRoot),MAX_RIP_IPIF_LOGICAL_IFACES, MAX_RIP_IPIF_LOGICAL_IFACES,0 },
{ "tMd5AuthKeyInfo", "MAX_RIP_MD5_AUTH_KEYS", sizeof(tMd5AuthKeyInfo),MAX_RIP_MD5_AUTH_KEYS, MAX_RIP_MD5_AUTH_KEYS,0 },
{ "tRipUnicastNBRS", "MAX_RIP_NBRS", sizeof(tRipUnicastNBRS),MAX_RIP_NBRS, MAX_RIP_NBRS,0 },
{ "tRipPeerRec", "MAX_RIP_PEERS", sizeof(tRipPeerRec),MAX_RIP_PEERS, MAX_RIP_PEERS,0 },
{ "tProtoQMsg", "MAX_RIP_QUE_DEPTH", sizeof(tProtoQMsg),MAX_RIP_QUE_DEPTH, MAX_RIP_QUE_DEPTH,0 },
{ "tRipImportList", "MAX_RIP_REDISTRIBUTE_ROUTES", sizeof(tRipImportList),MAX_RIP_REDISTRIBUTE_ROUTES, MAX_RIP_REDISTRIBUTE_ROUTES,0 },
{ "tRipRespQMsg", "MAX_RIP_RESP_MSG_NUM", sizeof(tRipRespQMsg),MAX_RIP_RESP_MSG_NUM, MAX_RIP_RESP_MSG_NUM,0 },
{ "tFilteringRMap", "MAX_RIP_RMAP_FILTERS", sizeof(tFilteringRMap),MAX_RIP_RMAP_FILTERS, MAX_RIP_RMAP_FILTERS,0 },
{ "tRipRtEntry", "MAX_RIP_ROUTE_ENTRIES", sizeof(tRipRtEntry),MAX_RIP_ROUTE_ENTRIES, MAX_RIP_ROUTE_ENTRIES,0 },
{ "tRipRtNode", "MAX_RIP_ROUTE_NODES", sizeof(tRipRtNode),MAX_RIP_ROUTE_NODES, MAX_RIP_ROUTE_NODES,0 },
{ "tRipRtEntry", "MAX_RIP_SUMMARY_ENTRIES", sizeof(tRipRtEntry),MAX_RIP_SUMMARY_ENTRIES, MAX_RIP_SUMMARY_ENTRIES,0 },
{ "tRipRmMsg", "MAX_RIP_RM_QUE_DEPTH", sizeof(tRipRmMsg),MAX_RIP_RM_QUE_DEPTH, MAX_RIP_RM_QUE_DEPTH,0 },
{ "tRipRtEntry", "MAX_RIP_LOCAL_ROUTE_ENTRIES", sizeof(tRipRtEntry),MAX_RIP_LOCAL_ROUTE_ENTRIES,MAX_RIP_LOCAL_ROUTE_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RIPSZ_C  */
extern tFsModSizingParams FsRIPSizingParams [];
#endif /*  _RIPSZ_C  */


