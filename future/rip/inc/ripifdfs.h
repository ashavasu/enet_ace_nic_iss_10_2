/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripifdfs.h,v 1.27 2016/09/13 12:57:12 siva Exp $
 *
 * Description:This file contains the definitions, data     
 *             structures related to RIP interfaces.        
 *                               
 *
 *******************************************************************/
#ifndef   __RIP_IF_DFS_H
#define   __RIP_IF_DFS_H

           /********************************************************
           * The following definitions corresponds to the general  *
           * interface level definitions that RIP module uses      *
           ********************************************************/

#define   RIPIF_INVALID_MEMBER    0xffff
#define   RIPIF_INVALID_INDEX     0xffff
#define RIP_SIZING_CONTEXT_COUNT    FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].u4PreAllocatedUnits

#define   RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES   0x00ffffff

/* This is used to send the RIP update packets on passive interfaces.
 * When we use RIPIF_INVALID_INDEX as parameter for 
 * rip_generate_update_message for sending passive updates, the Metric for 
 * local routes will become 15 (RIP_INFINITY-1) in rip_copy_valid_entries.
 * To avoid this problem, any value other than RIPIF_INVALID_INDEX can be
 * used.
 */

#define   RIPIF_PASSIVE_INDEX    0xefff

           /********************************************************
           * The following defines the statistics structure that   *
           * RIP module uses for the interfaces.                   *
           ********************************************************/

typedef struct
{
    UINT4   u4InBadPackets;    /* This specifies the number of incoming 
                                * packets that are discarded.           
                                */
    UINT4   u4InBadPackets1;    /* This specifies the number of incoming
                                * packets that are discarded due to junk
                                * received in the rip packet.
                                */
    UINT4   u4InBadPackets2;    /* This specifies the number of incoming
                                * packets that are discarded due to Invalid
                                * sending HOST.
                                */
    UINT4   u4InBadPackets3;    /* This specifies the number of incoming
                                * packets that are discarded due to Failure
                                * of RIP response/update.
                                */
    UINT4   u4InBadPackets4;    /* This specifies the number of incoming
                                * packets that are discarded.
                                */
    UINT4   u4InBadRoutes;     /* This specifies the number of routes that
                                * are discarded in the packets.         
                                */
    UINT4   u4InBadRoutes1;     /* This specifies the number of routes that
                                * are discarded in the packets.
                                */
    UINT4   u4InBadRoutes2;     /* This specifies the number of routes that
                                * are discarded in the packets.
                                */
    UINT4   u4SentTrigUpdates; /* This specifies the number of triggered
                                * updates that we have sent over this
                                * interface.                            
                                */

/* new member PeriodicUpdates */
    UINT4   u4SentPeriodicUpdates;   /* Number of periodic updates */
    UINT4   u4InBadAuthPackets;  /* Specifies the number of packets
                                 * discarded due to authentication failure */
    UINT4   u4InRipRequestReceived; /*count to track packets received in rip*/

    UINT4   u4InRipResponseProcessed;

    UINT4   u4RipUpdatesRequestReceived;

    UINT4   u4RipUpdatesResponseReceived;

    UINT4   u4RipUpdatesAcknowledged;

    UINT4   u4RipFailCount;
/* new member authentication failures*/
    UINT4   u4RipV2NoAuthFailCount;
    UINT4   u4RipV1SimplePasswdFailCount;
    UINT4   u4RipV2UnAuthenticatedPktFailCount;
    UINT4   u4RipV2SimplePasswdFailCount;
    UINT4   u4RipV1Md5AuthenticatedPktFailCount;
    UINT4   u4RipV2Md5UnAuthenticatedPktFailCount;
    UINT4   u4RipV2Md5InvalidLengthPktFailCount;
    UINT4   u4RipV2Md5InvalidKeyFailCount;
    UINT4   u4RipV2Md5InvalidDigestFailCount;
    UINT4   u4RipBadUpdateHeaderFailCount;
    UINT4   u4RipValidatePacketFailCount;
    UINT4   u4RipTrailingJunkFailCount;
    UINT4   u4RipValidateHost1FailCount;
    UINT4   u4RipValidateHost2FailCount;
    UINT4   u4RipValidateHost3FailCount;
    UINT4   u4RipValidatePkt1FailCount;
    UINT4   u4RipValidatePkt2FailCount;
    UINT4   u4RipValidatePkt3FailCount;
    UINT4   u4RipProcessRequest1FailCount;
    UINT4   u4RipProcessRequest2FailCount;
    UINT4   u4RipProcessRequest3FailCount;
    UINT4   u4RipProcessRequest4FailCount;
    UINT4   u4RipFailUpdatePeerCount;
    UINT4   u4RipValidateRouteInfoFailCount;
    UINT4   u4RipSendUpdateAndStartSpaceTmrFailCount;
    /*more error counters during send agg updates*/
    UINT4   u4RipAggTblInitFailCount;
    UINT4   u4RipDelIfAggNullEntryCount;
    UINT4   u4RipAggDelTrieFailCount;
    UINT4   u4RipAggIntfSpecificDelTrieFailCount;
    UINT4   u4RipAggChkLimitFailCount;
    UINT4   u4RipDelIfAggRouteFailCount;
    UINT4   u4RipDelIfAggTbl1FailCount;
    UINT4   u4RipDelIfAggTbl2FailCount;
    UINT4   u4RipSplitHorizonFailCount;
    UINT4   u4RipIfAggRtNullEntryCount;
    UINT4   u4RipGenerateUpdate1FailCount;
    UINT4   u4RipGenerateUpdate2FailCount;
    UINT4   u4RipGenerateUpdate3FailCount;
    UINT4   u4RipGenerateUpdate4FailCount;
    UINT4   u4RipSentUpdate1FailCount;
    UINT4   u4RipSentUpdate2FailCount;
    UINT4   u4RipSentUpdate3FailCount;
    UINT4   u4RipSentUpdate4FailCount;
    UINT4   u4RipSentUpdate5FailCount;
    UINT4   u4RipUpdateRipPktToSend1FailCount;
    UINT4   u4RipUpdateRipPktToSend2FailCount;
    UINT4   u4RipUpdateRipPktToSend3FailCount;
    UINT4   u4RipUpdateRipPktToSend4FailCount;
    UINT4   u4RipSendUnicastDatagram1FailCount;
    UINT4   u4RipSendUnicastDatagram2FailCount;
    UINT4   u4RipUpdateRequestFailCount;
    UINT4   u4RipUpdateAckFailCount;
    UINT4   u4RipValidateRouteInfo1FailCount;
    UINT4   u4RipValidateRouteInfo2FailCount;
    UINT4   u4RipValidateRouteInfo3FailCount;
    UINT4   u4RipValidateRouteInfo4FailCount;
    UINT4   u4RipValidateRouteInfo5FailCount;
    UINT4   u4RipValidateRouteInfo6FailCount;
    UINT4   u4RipValidateRouteInfo7FailCount;
    UINT4   u4RipConstructAuthInfoFailCount;
    UINT4   u4RipGetOperStatusFailCount;
    UINT4   u4RipGetSendStatusFailCount;
    UINT4   u4RipFailRouteAddCount;
} tRipIfaceStats;

           /********************************************************
           * The following defines the configurable parameters in  *
           * a structure which RIP modules uses for interfaces     *
           ********************************************************/

#define   MAX_AUTH_KEY_LENGTH    16    /* This defines the maximum length
                                        * of an authentication key that can
                                        * be used in terms of 8 bit bytes 
                                        */
#define   RIP_MAX_RESET_COUNT    6
#define   RIP_SPACING_ENABLE     1
#define   RIP_SPACING_DISABLE    2

typedef struct
{
    UINT4  u4SrcAddr;             /* This specifies the IP address that the
                                   * system will use as a source address on
                                   * this interface.                        
                                   */
    UINT2  u2UpdateInterval;      /* This specifies the time gap with
                                   * which regular updates from this
                                   * interface needs to be sent.      
                                   */
    UINT2  u2RouteAgeInterval;    /* This specifies the age timer value
                                   * after which the routes would be
                                   * invalidated.                     
                                   */
    UINT2  u2GarbageCollectionInterval;  /* This specifies the time after which
                                          * the routes are purged.           
                                          */
    UINT2  u2AuthType;        /* This specifies the authentication  
                               * type that need to be used for the
                               */
    UINT2  u2RipSendStatus;
    /* This specifies the operation of RIP over this interface
       and has various controls like,
       1. Rip operation is stopped from sending.
       2. Rip version 1 is sent.
       3. Broadcasting of Rip2 updates under RFC1058 rules.
       4. Multicasting of Rip2 packets.
       5. Demand Rip with Rip1 operation.
       6. Demand Rip with Rip2 operation.                    */
    UINT2  u2RipRecvStatus;
    /* This specifies the status of RIP operation as to whether
       it can receive,
       1. rip1 packets,
       2. rip2 packets,
       3. rip1Orrip2,
       4. Stopped from receiving any RIP packets.     */
    UINT2  u2AdminStatus;    /* This specifies the status of the 
                              * interface whether the RIP operation is
                              * enabled/disabled/invalidated over this 
                              * interface.                             
                              */
    UINT2  u2OperStatus;       /* This specifies the operational status of
                                * the interface and is controlled from IP 
                                */
    UINT2  u2DefaultMetric;    /* This specifies the default value of the 
                                * metric to be used for the default route 
                                * entry in RIP updates.                  
                                */
    INT1   au1RouteDomain[2];    /* This specifies the route tag value
                                  * which needs to be put into the
                                  * route packets that are sent out
                                  * thro this interface.             
                                  */
    UINT1  au1AuthKey[MAX_AUTH_KEY_LENGTH];
    /* This specifies the authentication key to be used if at 
       all the authentication type is of type other than 
       "No Authentication".                                  */
    UINT1  u1IfCurMask;          /* This specifies the status mask for the
                                  * interface and reflects the current status
                                  * of the interface                       
                                  */
    UINT1  u1RipAdminFlag;
    UINT2  u2SplitHorizonOperStatus;
    UINT2  u1DefRtInstallStatus;  /* This specifies the status for installation
                                     of default learned over this interface*/
    UINT2  u2SpacingTmrVal; /*Spaing timer interval between the update packets.*/
    UINT1  u1KeyIdInUse;    /* Auth Key currently used in this interface */
    BOOL1  b1LastKeyStatus; /* flag to indicate whether last key expired or not
                               true - last key expired, false - last key not expired*/
    UINT1  au1Pad[2];
} tRipIfaceCfg;

           /********************************************************
           * The following structure depicts the update timer      *
           * which is used on a per interface basis                *
           ********************************************************/


           /********************************************************
           * The following definitions relate to the peer address  *
           * and some of the statistics related to them            *
           ********************************************************/

typedef struct
{
    tRIP_SLL_NODE  Link;
    UINT4          u4PeerLastUpdateTime;
    UINT4          u4PeerRcvBadPackets;
    UINT4          u4PeerRcvBadRoutes;

    /*-----------------------------------------------------------------------*/

    /* The dynamic entries that are to be synced should be added after the
     * PeerDbNode. And both the halves should be padded accordingly */

    tDbTblNode     PeerDbNode;           /* Data Base node to sync RIP route
                                          * information.*/

    UINT4          u4PeerAddr;
    UINT2          u2IfIndex;
    UINT1          au1PeerRouteTag[2];  /* It's use is currently depracated
                                         * and should always be 0 
                                         */
    UINT4          u4RecvSeqNo;
    UINT2          u2PeerVersion;       /* The version of the last RIP packe
                                         * received from this peer        
                                         */
    /* This is used for MD5 authentication */
    UINT1          u1KeyIdInUse;
    UINT1          u1AlignmentByte;
} tRipPeerRec;

           /********************************************************
           * The following definitions defines the interface       *
           * record which comprises the statistics and configurable*
           * elements                                              *
           ********************************************************/

           /***********************************************************
           * tRipRtNode helps the routes in the database to be linked *
           * by a doubly linked list in the chronological order.      *
           ***********************************************************/

typedef struct
{
    tTMO_DLL_NODE  DllLink;
    tLeafNode      *pLeafNode;
} tRipRtNode;

typedef struct
{
 tTMO_HASH_NODE      nextIfNode;
 /* Pointer to the next and the previous interface
  * elements hashed on to the same hash index value
  */
 tRIP_INTERFACE   IfaceId;
 tRipIfaceStats   RipIfaceStats;
 tRipIfaceCfg     RipIfaceCfg;
 tRipUpdateTimer  RipUpdateTimer;
 tRipUpdateTimer  RipSpacingTimer;
 tRipUpdateTimer  RipSubscrTimer;    /* Over-Subscription Timer */
 tRipRtNode      *pReTxFirstRoute;
 tRipRtNode      *pReTxLastRoute;
 tRIP_SLL         RipPeerList;       /* RIP peers are organized as a
          * singly linked list of peer
          * records                        
          */
 tRIP_SLL         RipMd5KeyList;
 tRIP_SLL         RipCryptoAuthKeyList; /* List of crypto authentication
                                           keys for this interface */
 UINT4            u4Addr;            /* The IP address of this interface
          * associated with a subnet.      
          */
 UINT4            u4NetMask;         /* The mask of this interface
          * associated with a subnet.
          */
 tRIP_SLL         RipUnicastNBRS;    /* List of neighbors to whom unicast 
          * packet has to be sent. 
          */
 UINT4            u4PeerAddress;
        VOID             *pRipNextSendRt;   /* This is used for spacing. The 
          * next route node to be  send in next space
       * update is stored here. 
       */
 UINT2            u2NoOfPktsPerSpace;    /* Number packets to be sent per 
       * space. This is calculated from 
       * the number of routes and the 
       * number of spaces. 
       */
 UINT2            u2NextIfaceIndex;      /* This denotes the next valid 
       * member in the series of interface
       * records                        
       */
 UINT2            u2ReservedWord;
 INT2             i2ResetCount;          /* Reset Timer in rfc 1812 is 
       * simulated with this count and 
       * the update timer 
       */
 UINT1            u1ReTxCounter;
 UINT1            u1Persistence;
 UINT1            i1Align[2];
 tRipCxt          *pRipCxt;
 tTMO_DLL         RipIfRtList;
    tTMO_DLL_NODE    RipIfRecNode; /*Interface record Node */
} tRipIfaceRec;

typedef struct
{
    UINT4   u4RipV2NoAuthFailCount;
    UINT4   u4RipV1SimplePasswdFailCount;
    UINT4   u4RipV2UnAuthenticatedPktFailCount;
    UINT4   u4RipV2SimplePasswdFailCount;
    UINT4   u4RipV1Md5AuthenticatedPktFailCount;
    UINT4   u4RipV2Md5UnAuthenticatedPktFailCount;
    UINT4   u4RipV2Md5InvalidLengthPktFailCount;
    UINT4   u4RipV2Md5InvalidKeyFailCount;
    UINT4   u4RipV2Md5InvalidDigestFailCount;
    UINT4   u4RipBadUpdateHeaderFailCount;
    UINT4   u4RipValidatePacketFailCount;
    UINT4   u4RipTrailingJunkFailCount;
    UINT4   u4RipValidateHost1FailCount;
    UINT4   u4RipValidateHost2FailCount;
    UINT4   u4RipValidateHost3FailCount;
    UINT4   u4RipValidatePkt1FailCount;
    UINT4   u4RipValidatePkt2FailCount;
    UINT4   u4RipValidatePkt3FailCount;
    UINT4   u4RipProcessRequest1FailCount;
    UINT4   u4RipProcessRequest2FailCount;
    UINT4   u4RipProcessRequest3FailCount;
    UINT4   u4RipProcessRequest4FailCount;
    UINT4   u4RipFailUpdatePeerCount;
    UINT4   u4RipValidateRouteInfoFailCount;
    UINT4   u4RipSendUpdateAndStartSpaceTmrFailCount;
    UINT4   u4RipFailCount;
    UINT4   u4RipAggTblInitFailCount;
    UINT4   u4RipDelIfAggNullEntryCount;
    UINT4   u4RipAggDelTrieFailCount;
    UINT4   u4RipAggIntfSpecificDelTrieFailCount;
    UINT4   u4RipAggChkLimitFailCount;
    UINT4   u4RipDelIfAggRouteFailCount;
    UINT4   u4RipDelIfAggTbl1FailCount;
    UINT4   u4RipDelIfAggTbl2FailCount;
    UINT4   u4RipSplitHorizonFailCount;
    UINT4   u4RipIfAggRtNullEntryCount;
    UINT4   u4RipGenerateUpdate1FailCount;
    UINT4   u4RipGenerateUpdate2FailCount;
    UINT4   u4RipGenerateUpdate3FailCount;
    UINT4   u4RipGenerateUpdate4FailCount;
    UINT4   u4RipSentUpdate1FailCount;
    UINT4   u4RipSentUpdate2FailCount;
    UINT4   u4RipSentUpdate3FailCount;
    UINT4   u4RipSentUpdate4FailCount;
    UINT4   u4RipSentUpdate5FailCount;
    UINT4   u4RipUpdateRipPktToSend1FailCount;
    UINT4   u4RipUpdateRipPktToSend2FailCount;
    UINT4   u4RipUpdateRipPktToSend3FailCount;
    UINT4   u4RipUpdateRipPktToSend4FailCount;
    UINT4   u4RipSendUnicastDatagram1FailCount;
    UINT4   u4RipSendUnicastDatagram2FailCount;
    UINT4   u4RipUpdateRequestFailCount;
    UINT4   u4RipUpdateAckFailCount;
    UINT4   u4RipValidateRouteInfo1FailCount;
    UINT4   u4RipValidateRouteInfo2FailCount;
    UINT4   u4RipValidateRouteInfo3FailCount;
    UINT4   u4RipValidateRouteInfo4FailCount;
    UINT4   u4RipValidateRouteInfo5FailCount;
    UINT4   u4RipValidateRouteInfo6FailCount;
    UINT4   u4RipValidateRouteInfo7FailCount;
    UINT4   u4RipConstructAuthInfoFailCount;
    UINT4   u4RipGetOperStatusFailCount;
    UINT4   u4RipGetSendStatusFailCount;
    UINT4   u4RipFailRouteAddCount;
}tRipInFailCount;

/* Structure for MD5 Authentication */
typedef struct MD5_AUTH_KEY_INFO
{
    tRIP_SLL_NODE  NextNode;
    UINT1          au1AuthKey[MAX_AUTH_KEY_LENGTH];
    INT4           i4KeyStartTime;
    INT4           i4KeyExpiryTime;
    UINT4          u4SentSeqNo;
    UINT1          u1AuthKeyId;
    UINT1          u1KeyEntryStatus;
    UINT2          u2AlignmentWord;
} tMd5AuthKeyInfo;

/* Structure for Crypto Authentication (MD5,SHA1 & SHA2)*/
typedef struct CRYPTO_AUTH_KEY_INFO
{
    tRIP_SLL_NODE  NextNode;
    UINT1          au1AuthKey[MAX_AUTH_KEY_LENGTH];
    UINT4          u4KeyStartGenerate;
    UINT4          u4KeyStopGenerate;
    UINT4          u4KeyStartAccept;
    UINT4          u4KeyStopAccept;
    UINT4          u4SentSeqNo;
    UINT1          u1AuthKeyId;
    UINT1          u1KeyEntryStatus;
    UINT1          u1AuthKeyLength;
    UINT1          au1Pad[1];
} tCryptoAuthKeyInfo;

/* Constants for HMAC - SHA functions */
/* RIP SHA version constants should match 
 * with the values defined in SHA.H */

typedef enum RIPSHAversion {
    RIP_AR_SHA1_ALGO,
    RIP_AR_SHA224_ALGO,
    RIP_AR_SHA256_ALGO,
    RIP_AR_SHA384_ALGO,
    RIP_AR_SHA512_ALGO
}eRipShaVersion;

typedef struct RipUnicastNBRS
{
    tRIP_SLL_NODE  NextNode;
    UINT4          u4UnicastNBR;
    UINT4          u4UnicastNBRRowStatus;
} tRipUnicastNBRS;


typedef struct RipTxInfo
{
    UINT2    u2IfIndex;
    UINT2    u2RtsToCompose;
    UINT2    u2DestUdpPort;
    UINT1    u1ResponseVersion;
    UINT1    u1BcastFlag;
    UINT4    u4DestNet;
}tRipTxInfo;
           /********************************************************
           *    Definitions for the RIP interface update timers    *
           ********************************************************/

#define     RIPIF_FILL_RES_RETX_TIMER(pRipIfRec, val) \
              {\
                 pRipIfRec->RipUpdateTimer.u2If = \
                       (UINT2)RIP_GET_IFACE_INDEX(pRipIfRec->IfaceId);\
                 pRipIfRec->RipUpdateTimer.u1Id = RIP_RES_RETX_TIMER;\
                 pRipIfRec->RipUpdateTimer.u1Data = val; \
              }

#define     RIPIF_FILL_REQ_RETX_TIMER(pRipIfRec, val) \
              {\
                 pRipIfRec->RipSpacingTimer.u2If = \
                          (UINT2)RIP_GET_IFACE_INDEX(pRipIfRec->IfaceId);\
                 pRipIfRec->RipSpacingTimer.u1Id = RIP_REQ_RETX_TIMER;\
                 pRipIfRec->RipSpacingTimer.u1Data = val; \
              }

#define    RIPIF_FILL_SUBSCRIP_TIMER(pRipIfRec, val) \
             {\
                pRipIfRec->RipSubscrTimer.u2If = \
                           (UINT2)RIP_GET_IFACE_INDEX(pRipIfRec->IfaceId);\
                pRipIfRec->RipSubscrTimer.u1Id = RIP_SUBSCRIP_TIMER;\
                pRipIfRec->RipSubscrTimer.u1Data = val; \
             }

           /********************************************************
           *    Definitions for the various values of the RIP      *
           *    interface configuration structure parameters       *
           ********************************************************/

/* Domain Values */
#define   RIPIF_RT_DOMAIN    0    /* Always a 0 value               */

/* Authentication Types */
#define   RIPIF_NO_AUTHENTICATION       1
#define   RIPIF_SIMPLE_PASSWORD         2
#define   RIPIF_MD5_AUTHENTICATION      3
#define   RIPIF_SHA1_AUTHENTICATION     4
#define   RIPIF_SHA256_AUTHENTICATION   5
#define   RIPIF_SHA384_AUTHENTICATION   6
#define   RIPIF_SHA512_AUTHENTICATION   7

#define   RIP_MD5_DIGEST_LEN            16
#define   RIP_SHA1_DIGEST_LEN           20
#define   RIP_SHA256_DIGEST_LEN         32
#define   RIP_SHA384_DIGEST_LEN         48
#define   RIP_SHA512_DIGEST_LEN         64
#define   MAX_DIGEST_LENGTH             64

#define   RIP_CRYPTO_MIN_AUTH_TYPE      1
#define   RIP_CRYPTO_MAX_AUTH_TYPE      5

/* Authentication Key */
#define   RIPIF_DEF_AUTH_KEY    ""

/* Send Configuration Values */
#define   RIPIF_DO_NOT_SEND        1
#define   RIPIF_VERSION_1_SND      2
#define   RIPIF_RIP1_COMPATIBLE    3
#define   RIPIF_VERSION_2_SND      4
#define   RIPIF_V1_DEMAND          5
#define   RIPIF_V2_DEMAND          6

/* Recv Configuration Values */
#define   RIPIF_VERSION_1_RCV    1
#define   RIPIF_VERSION_2_RCV    2
#define   RIPIF_1_OR_2           3
#define   RIPIF_DO_NOT_RECV      4

/* Row Status Values */
#define   RIPIF_ADMIN_NOT_EXIST    0
#define   RIPIF_ADMIN_ACTIVE       ACTIVE

/* This indicates the interface is ready for operation   */
#define   RIPIF_ADMIN_NOT_IN_SERVICE    NOT_IN_SERVICE     

/* This indicates the interface is not available for operation  */
#define   RIPIF_ADMIN_NOT_READY    NOT_READY          

/* This indicates the interface is not exactly ready and need some input  */
#define   RIPIF_ADMIN_CREATE_AND_GO    CREATE_AND_GO      

/* This indicates the interface is ready for operation                      */
#define   RIPIF_ADMIN_CREATE_AND_WAIT    CREATE_AND_WAIT    

/* This indicates the interface instance is created but unavailable for
 * operation                          
 */
#define   RIPIF_ADMIN_DESTROY    DESTROY            

/* This indicates the interface is to be deleted                            */
/* Oper Status Values */
#define   RIPIF_OPER_ENABLE        1
#define   RIPIF_OPER_DISABLE       2

#define   FIRST_NODE               1
#define   RIP_ROUTE_TUPLE_SIZE    20

/* TRAPS */
#define   RIP_AUTH_FAILURE_TRAP_ID    1
#define   RIP_AUTH_LAST_KEY_TRAP_ID   2
#define   SNMP_V2_TRAP_OID_LEN        15

#define   RIP_MAX_BUFFER           256
#define   RIP_ZERO                 0
#define   RIP_ONE                  1
#define   RIP_TWO                  2
#define   RIP_NUMBER_TEN           10
#define   RIP_MAX_HEX_SINGLE_DIGIT 0xf
#define   RIP_INVALID              -1

           /********************************************************
           *    A definition to determine the activeness of RIP    *
           ********************************************************/

#define     RIP_SET_DATA(pBufChain,pu1_UserData, NumByte) \
            MEMCPY((pBufChain)->ModuleData.au1_UserData, pu1_UserData, NumByte)

#define     RIP_GET_DATA(pBufChain,pu1_UserData, NumByte) \
            MEMCPY(pu1_UserData,(pBufChain)->ModuleData.au1_UserData, NumByte)

           /********************************************************
           *    vishu - For Unnumbered Interface checkings.        *
           ********************************************************/

   /* Macros added for MD5 Authentication */

#define   RIP_MD5_AUTH_KEY_ID(pNode)   pNode->u1AuthKeyId      
#define   RIP_MD5_AUTH_KEY(pNode)      &(pNode->au1AuthKey)    
#define   RIP_MD5_START_TIME(pNode)    pNode->i4KeyStartTime   
#define   RIP_MD5_EXPIRY_TIME(pNode)   pNode->i4KeyExpiryTime  
#define   RIP_MD5_ROW_STATUS(pNode)    pNode->u1KeyEntryStatus 
#define   RIP_MD5_SEQUENCE_NO(pNode)   pNode->u4SentSeqNo      

   /* Macros added for Crypto Authentication */

#define   RIP_CRYPTO_AUTH_KEY_ID(pNode)        pNode->u1AuthKeyId      
#define   RIP_CRYPTO_AUTH_KEY(pNode)           &(pNode->au1AuthKey)    
#define   RIP_CRYPTO_AUTH_KEY_LEN(pNode)       pNode->u1AuthKeyLength   
#define   RIP_CRYPTO_START_GEN_TIME(pNode)     pNode->u4KeyStartGenerate   
#define   RIP_CRYPTO_START_ACCEPT_TIME(pNode)  pNode->u4KeyStartAccept
#define   RIP_CRYPTO_STOP_GEN_TIME(pNode)      pNode->u4KeyStopGenerate 
#define   RIP_CRYPTO_STOP_ACCEPT_TIME(pNode)   pNode->u4KeyStopAccept  
#define   RIP_CRYPTO_STATUS(pNode)             pNode->u1KeyEntryStatus 
#define   RIP_CRYPTO_SEQUENCE_NO(pNode)        pNode->u4SentSeqNo      

/* Time Buffer related macros */

#define     RIP_TMP_NUM_STR     10
#define     RIP_INVALID_YEAR    1
#define     RIP_INVALID_MONTH   2
#define     RIP_INVALID_DATE    3
#define     RIP_INVALID_HOUR    4
#define     RIP_INVALID_MIN     5
#define     RIP_INVALID_SEC     6
#define     RIP_MAX_SEC         59
#define     RIP_MAX_MIN         59
#define     RIP_MAX_HOUR        23
#define     RIP_MAX_MONTH       12
#define     RIP_MAX_MONTH_DAYS  31
#define     RIP_MAX_LEAP_FEB_DAYS  29
#define     RIP_MIN_LEAP_FEB_DAYS  28
#define     RIP_DST_TIME_LEN       20
   /* Macros added for peer record updation */

#define   RIP_PEER_LAST_SEQ_NO(pPeerRec)   (pPeerRec)->u4RecvSeqNo  
#define   RIP_PEER_KEY_IN_USE(pPeerRec)    (pPeerRec)->u1KeyIdInUse 

#define   RIP_NODE_NOT_FOUND                     1
#define   RIP_NODE_FOUND                         2

#define   RIP_TRAILER_AUTH_TYPE    0x0001
#define   RIP_OFFSET_TO_KEY             4
    
#define     RIP_IS_ACTIVE(pRipIfRec)       \
            ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag == (UINT1) RIP_ADMIN_ENABLE) && \
             (pRipIfRec->RipIfaceCfg.u2AdminStatus ==                             \
                                              (UINT2) RIPIF_ADMIN_ACTIVE) &&   \
             (pRipIfRec->RipIfaceCfg.u2OperStatus  == (UINT2) RIPIF_OPER_ENABLE))

#endif /* End of #ifdef __RIP_IF_DFS_H block checking */
