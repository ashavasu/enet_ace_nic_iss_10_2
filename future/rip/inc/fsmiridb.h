/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmiridb.h,v 1.8 2013/06/19 13:31:11 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIRIDB_H
#define _FSMIRIDB_H

UINT1 FsMIRip2GlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRip2NBRTrustListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIRip2IfConfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIRipMd5AuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRipCryptoAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRip2NBRUnicastListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIRip2LocalRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIRipAggTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIRipRRDGlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRipDistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRipPreferenceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmiri [] ={1,3,6,1,4,1,2076,151};
tSNMP_OID_TYPE fsmiriOID = {8, fsmiri};


UINT4 FsMIRipContextId [ ] ={1,3,6,1,4,1,2076,151,1,1,1,1};
UINT4 FsMIRip2Security [ ] ={1,3,6,1,4,1,2076,151,1,1,1,2};
UINT4 FsMIRip2Peers [ ] ={1,3,6,1,4,1,2076,151,1,1,1,3};
UINT4 FsMIRip2TrustNBRListEnable [ ] ={1,3,6,1,4,1,2076,151,1,1,1,4};
UINT4 FsMIRip2NumberOfDroppedPkts [ ] ={1,3,6,1,4,1,2076,151,1,1,1,5};
UINT4 FsMIRip2SpacingEnable [ ] ={1,3,6,1,4,1,2076,151,1,1,1,6};
UINT4 FsMIRip2AutoSummaryStatus [ ] ={1,3,6,1,4,1,2076,151,1,1,1,7};
UINT4 FsMIRip2RetransTimeoutInt [ ] ={1,3,6,1,4,1,2076,151,1,1,1,8};
UINT4 FsMIRip2MaxRetransmissions [ ] ={1,3,6,1,4,1,2076,151,1,1,1,9};
UINT4 FsMIRip2OverSubscriptionTimeout [ ] ={1,3,6,1,4,1,2076,151,1,1,1,10};
UINT4 FsMIRip2Propagate [ ] ={1,3,6,1,4,1,2076,151,1,1,1,11};
UINT4 FsMIRipTrcFlag [ ] ={1,3,6,1,4,1,2076,151,1,1,1,12};
UINT4 FsMIRipRowStatus [ ] ={1,3,6,1,4,1,2076,151,1,1,1,13};
UINT4 FsMIRipAdminStatus [ ] ={1,3,6,1,4,1,2076,151,1,1,1,14};
UINT4 FsMIRip2LastAuthKeyLifetimeStatus [ ] ={1,3,6,1,4,1,2076,151,1,1,1,15};
UINT4 FsMIRipRtCount [ ] ={1,3,6,1,4,1,2076,151,1,1,1,16};
UINT4 FsMIRipGlobalTrcFlag [ ] ={1,3,6,1,4,1,2076,151,1,2};
UINT4 FsMIRip2TrustNBRIpAddr [ ] ={1,3,6,1,4,1,2076,151,1,3,1,1};
UINT4 FsMIRip2TrustNBRRowStatus [ ] ={1,3,6,1,4,1,2076,151,1,3,1,2};
UINT4 FsMIRip2IfConfAddress [ ] ={1,3,6,1,4,1,2076,151,1,4,1,1};
UINT4 FsMIRip2IfAdminStat [ ] ={1,3,6,1,4,1,2076,151,1,4,1,2};
UINT4 FsMIRip2IfConfOperState [ ] ={1,3,6,1,4,1,2076,151,1,4,1,3};
UINT4 FsMIRip2IfConfUpdateTmr [ ] ={1,3,6,1,4,1,2076,151,1,4,1,4};
UINT4 FsMIRip2IfConfGarbgCollectTmr [ ] ={1,3,6,1,4,1,2076,151,1,4,1,5};
UINT4 FsMIRip2IfConfRouteAgeTmr [ ] ={1,3,6,1,4,1,2076,151,1,4,1,6};
UINT4 FsMIRip2IfSplitHorizonStatus [ ] ={1,3,6,1,4,1,2076,151,1,4,1,7};
UINT4 FsMIRip2IfConfDefRtInstall [ ] ={1,3,6,1,4,1,2076,151,1,4,1,8};
UINT4 FsMIRip2IfConfSpacingTmr [ ] ={1,3,6,1,4,1,2076,151,1,4,1,9};
UINT4 FsMIRip2IfConfInUseKey [ ] ={1,3,6,1,4,1,2076,151,1,4,1,10};
UINT4 FsMIRip2IfConfAuthLastKeyStatus [ ] ={1,3,6,1,4,1,2076,151,1,4,1,11};
UINT4 FsMIRipMd5AuthAddress [ ] ={1,3,6,1,4,1,2076,151,1,5,1,1};
UINT4 FsMIRipMd5AuthKeyId [ ] ={1,3,6,1,4,1,2076,151,1,5,1,2};
UINT4 FsMIRipMd5AuthKey [ ] ={1,3,6,1,4,1,2076,151,1,5,1,3};
UINT4 FsMIRipMd5KeyStartTime [ ] ={1,3,6,1,4,1,2076,151,1,5,1,4};
UINT4 FsMIRipMd5KeyExpiryTime [ ] ={1,3,6,1,4,1,2076,151,1,5,1,5};
UINT4 FsMIRipMd5KeyRowStatus [ ] ={1,3,6,1,4,1,2076,151,1,5,1,6};
UINT4 FsMIRipCryptoAuthIfIndex [ ] ={1,3,6,1,4,1,2076,151,1,9,1,1};
UINT4 FsMIRipCryptoAuthAddress [ ] ={1,3,6,1,4,1,2076,151,1,9,1,2};
UINT4 FsMIRipCryptoAuthKeyId [ ] ={1,3,6,1,4,1,2076,151,1,9,1,3};
UINT4 FsMIRipCryptoAuthKey [ ] ={1,3,6,1,4,1,2076,151,1,9,1,4};
UINT4 FsMIRipCryptoKeyStartAccept [ ] ={1,3,6,1,4,1,2076,151,1,9,1,5};
UINT4 FsMIRipCryptoKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,151,1,9,1,6};
UINT4 FsMIRipCryptoKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,151,1,9,1,7};
UINT4 FsMIRipCryptoKeyStopAccept [ ] ={1,3,6,1,4,1,2076,151,1,9,1,8};
UINT4 FsMIRipCryptoKeyStatus [ ] ={1,3,6,1,4,1,2076,151,1,9,1,9};
UINT4 FsMIRip2NBRUnicastIpAddr [ ] ={1,3,6,1,4,1,2076,151,1,6,1,1};
UINT4 FsMIRip2NBRUnicastNBRRowStatus [ ] ={1,3,6,1,4,1,2076,151,1,6,1,2};
UINT4 FsMIRip2DestNet [ ] ={1,3,6,1,4,1,2076,151,1,7,1,1};
UINT4 FsMIRip2DestMask [ ] ={1,3,6,1,4,1,2076,151,1,7,1,2};
UINT4 FsMIRip2Tos [ ] ={1,3,6,1,4,1,2076,151,1,7,1,3};
UINT4 FsMIRip2NextHop [ ] ={1,3,6,1,4,1,2076,151,1,7,1,4};
UINT4 FsMIRip2RtIfIndex [ ] ={1,3,6,1,4,1,2076,151,1,7,1,5};
UINT4 FsMIRip2RtType [ ] ={1,3,6,1,4,1,2076,151,1,7,1,6};
UINT4 FsMIRip2Proto [ ] ={1,3,6,1,4,1,2076,151,1,7,1,7};
UINT4 FsMIRip2ChgTime [ ] ={1,3,6,1,4,1,2076,151,1,7,1,8};
UINT4 FsMIRip2Metric [ ] ={1,3,6,1,4,1,2076,151,1,7,1,9};
UINT4 FsMIRip2RowStatus [ ] ={1,3,6,1,4,1,2076,151,1,7,1,10};
UINT4 FsMIRip2Gateway [ ] ={1,3,6,1,4,1,2076,151,1,7,1,11};
UINT4 FsMIRipIfIndex [ ] ={1,3,6,1,4,1,2076,151,1,8,1,1};
UINT4 FsMIRipAggAddress [ ] ={1,3,6,1,4,1,2076,151,1,8,1,2};
UINT4 FsMIRipAggAddressMask [ ] ={1,3,6,1,4,1,2076,151,1,8,1,3};
UINT4 FsMIRipAggStatus [ ] ={1,3,6,1,4,1,2076,151,1,8,1,4};
UINT4 FsMIRipRRDGlobalStatus [ ] ={1,3,6,1,4,1,2076,151,2,1,1,1};
UINT4 FsMIRipRRDSrcProtoMaskEnable [ ] ={1,3,6,1,4,1,2076,151,2,1,1,2};
UINT4 FsMIRipRRDSrcProtoMaskDisable [ ] ={1,3,6,1,4,1,2076,151,2,1,1,3};
UINT4 FsMIRipRRDRouteTagType [ ] ={1,3,6,1,4,1,2076,151,2,1,1,4};
UINT4 FsMIRipRRDRouteTag [ ] ={1,3,6,1,4,1,2076,151,2,1,1,5};
UINT4 FsMIRipRRDRouteDefMetric [ ] ={1,3,6,1,4,1,2076,151,2,1,1,6};
UINT4 FsMIRipRRDRouteMapEnable [ ] ={1,3,6,1,4,1,2076,151,2,1,1,7};
UINT4 FsMIRipDistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,151,3,1,1,1};
UINT4 FsMIRipDistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,151,3,1,1,2};
UINT4 FsMIRipDistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,151,3,1,1,3};
UINT4 FsMIRipDistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,151,3,1,1,4};
UINT4 FsMIRipPreferenceValue [ ] ={1,3,6,1,4,1,2076,151,4,1,1,1};
UINT4 FsMIRip2ContextId [ ] ={1,3,6,1,4,1,2076,151,5,1};
UINT4 FsMIRipAuthIfIndex [ ] ={1,3,6,1,4,1,2076,151,5,2};
UINT4 FsMIRipAuthKeyId [ ] ={1,3,6,1,4,1,2076,151,5,3};
UINT4 FsMIRipPeerAddress [ ] ={1,3,6,1,4,1,2076,151,5,4};




tMbDbEntry fsmiriMibEntry[]= {

{{12,FsMIRipContextId}, GetNextIndexFsMIRip2GlobalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRip2GlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRip2Security}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2SecurityGet, FsMIRip2SecuritySet, FsMIRip2SecurityTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRip2Peers}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2PeersGet, FsMIRip2PeersSet, FsMIRip2PeersTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "5"},

{{12,FsMIRip2TrustNBRListEnable}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2TrustNBRListEnableGet, FsMIRip2TrustNBRListEnableSet, FsMIRip2TrustNBRListEnableTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRip2NumberOfDroppedPkts}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2NumberOfDroppedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIRip2GlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRip2SpacingEnable}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2SpacingEnableGet, FsMIRip2SpacingEnableSet, FsMIRip2SpacingEnableTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRip2AutoSummaryStatus}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2AutoSummaryStatusGet, FsMIRip2AutoSummaryStatusSet, FsMIRip2AutoSummaryStatusTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRip2RetransTimeoutInt}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2RetransTimeoutIntGet, FsMIRip2RetransTimeoutIntSet, FsMIRip2RetransTimeoutIntTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "5"},

{{12,FsMIRip2MaxRetransmissions}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2MaxRetransmissionsGet, FsMIRip2MaxRetransmissionsSet, FsMIRip2MaxRetransmissionsTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "36"},

{{12,FsMIRip2OverSubscriptionTimeout}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2OverSubscriptionTimeoutGet, FsMIRip2OverSubscriptionTimeoutSet, FsMIRip2OverSubscriptionTimeoutTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "180"},

{{12,FsMIRip2Propagate}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2PropagateGet, FsMIRip2PropagateSet, FsMIRip2PropagateTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRipTrcFlag}, GetNextIndexFsMIRip2GlobalTable, FsMIRipTrcFlagGet, FsMIRipTrcFlagSet, FsMIRipTrcFlagTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRipRowStatus}, GetNextIndexFsMIRip2GlobalTable, FsMIRipRowStatusGet, FsMIRipRowStatusSet, FsMIRipRowStatusTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 1, NULL},

{{12,FsMIRipAdminStatus}, GetNextIndexFsMIRip2GlobalTable, FsMIRipAdminStatusGet, FsMIRipAdminStatusSet, FsMIRipAdminStatusTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRip2LastAuthKeyLifetimeStatus}, GetNextIndexFsMIRip2GlobalTable, FsMIRip2LastAuthKeyLifetimeStatusGet, FsMIRip2LastAuthKeyLifetimeStatusSet, FsMIRip2LastAuthKeyLifetimeStatusTest, FsMIRip2GlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2GlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRipRtCount}, GetNextIndexFsMIRip2GlobalTable, FsMIRipRtCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2GlobalTableINDEX, 1, 0, 0, NULL},

{{10,FsMIRipGlobalTrcFlag}, NULL, FsMIRipGlobalTrcFlagGet, FsMIRipGlobalTrcFlagSet, FsMIRipGlobalTrcFlagTest, FsMIRipGlobalTrcFlagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsMIRip2TrustNBRIpAddr}, GetNextIndexFsMIRip2NBRTrustListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRip2NBRTrustListTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRip2TrustNBRRowStatus}, GetNextIndexFsMIRip2NBRTrustListTable, FsMIRip2TrustNBRRowStatusGet, FsMIRip2TrustNBRRowStatusSet, FsMIRip2TrustNBRRowStatusTest, FsMIRip2NBRTrustListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2NBRTrustListTableINDEX, 2, 0, 1, NULL},

{{12,FsMIRip2IfConfAddress}, GetNextIndexFsMIRip2IfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRip2IfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRip2IfAdminStat}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfAdminStatGet, FsMIRip2IfAdminStatSet, FsMIRip2IfAdminStatTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRip2IfConfOperState}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRip2IfConfTableINDEX, 2, 0, 0, "2"},

{{12,FsMIRip2IfConfUpdateTmr}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfUpdateTmrGet, FsMIRip2IfConfUpdateTmrSet, FsMIRip2IfConfUpdateTmrTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, "30"},

{{12,FsMIRip2IfConfGarbgCollectTmr}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfGarbgCollectTmrGet, FsMIRip2IfConfGarbgCollectTmrSet, FsMIRip2IfConfGarbgCollectTmrTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, "120"},

{{12,FsMIRip2IfConfRouteAgeTmr}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfRouteAgeTmrGet, FsMIRip2IfConfRouteAgeTmrSet, FsMIRip2IfConfRouteAgeTmrTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, "180"},

{{12,FsMIRip2IfSplitHorizonStatus}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfSplitHorizonStatusGet, FsMIRip2IfSplitHorizonStatusSet, FsMIRip2IfSplitHorizonStatusTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, "2"},

{{12,FsMIRip2IfConfDefRtInstall}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfDefRtInstallGet, FsMIRip2IfConfDefRtInstallSet, FsMIRip2IfConfDefRtInstallTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, "2"},

{{12,FsMIRip2IfConfSpacingTmr}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfSpacingTmrGet, FsMIRip2IfConfSpacingTmrSet, FsMIRip2IfConfSpacingTmrTest, FsMIRip2IfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRip2IfConfTableINDEX, 2, 0, 0, "0"},

{{12,FsMIRip2IfConfInUseKey}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfInUseKeyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2IfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRip2IfConfAuthLastKeyStatus}, GetNextIndexFsMIRip2IfConfTable, FsMIRip2IfConfAuthLastKeyStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRip2IfConfTableINDEX, 2, 0, 0, "2"},

{{12,FsMIRipMd5AuthAddress}, GetNextIndexFsMIRipMd5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRipMd5AuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipMd5AuthKeyId}, GetNextIndexFsMIRipMd5AuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRipMd5AuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipMd5AuthKey}, GetNextIndexFsMIRipMd5AuthTable, FsMIRipMd5AuthKeyGet, FsMIRipMd5AuthKeySet, FsMIRipMd5AuthKeyTest, FsMIRipMd5AuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipMd5AuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipMd5KeyStartTime}, GetNextIndexFsMIRipMd5AuthTable, FsMIRipMd5KeyStartTimeGet, FsMIRipMd5KeyStartTimeSet, FsMIRipMd5KeyStartTimeTest, FsMIRipMd5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipMd5AuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipMd5KeyExpiryTime}, GetNextIndexFsMIRipMd5AuthTable, FsMIRipMd5KeyExpiryTimeGet, FsMIRipMd5KeyExpiryTimeSet, FsMIRipMd5KeyExpiryTimeTest, FsMIRipMd5AuthTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipMd5AuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipMd5KeyRowStatus}, GetNextIndexFsMIRipMd5AuthTable, FsMIRipMd5KeyRowStatusGet, FsMIRipMd5KeyRowStatusSet, FsMIRipMd5KeyRowStatusTest, FsMIRipMd5AuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRipMd5AuthTableINDEX, 3, 0, 1, NULL},

{{12,FsMIRip2NBRUnicastIpAddr}, GetNextIndexFsMIRip2NBRUnicastListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRip2NBRUnicastListTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRip2NBRUnicastNBRRowStatus}, GetNextIndexFsMIRip2NBRUnicastListTable, FsMIRip2NBRUnicastNBRRowStatusGet, FsMIRip2NBRUnicastNBRRowStatusSet, FsMIRip2NBRUnicastNBRRowStatusTest, FsMIRip2NBRUnicastListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRip2NBRUnicastListTableINDEX, 2, 0, 1, NULL},

{{12,FsMIRip2DestNet}, GetNextIndexFsMIRip2LocalRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2DestMask}, GetNextIndexFsMIRip2LocalRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2Tos}, GetNextIndexFsMIRip2LocalRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2NextHop}, GetNextIndexFsMIRip2LocalRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2RtIfIndex}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2RtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2RtType}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2RtTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2Proto}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2ProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2ChgTime}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2ChgTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2Metric}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2MetricGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2RowStatus}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2RowStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRip2Gateway}, GetNextIndexFsMIRip2LocalRouteTable, FsMIRip2GatewayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIRip2LocalRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRipIfIndex}, GetNextIndexFsMIRipAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRipAggTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipAggAddress}, GetNextIndexFsMIRipAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRipAggTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipAggAddressMask}, GetNextIndexFsMIRipAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRipAggTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipAggStatus}, GetNextIndexFsMIRipAggTable, FsMIRipAggStatusGet, FsMIRipAggStatusSet, FsMIRipAggStatusTest, FsMIRipAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRipAggTableINDEX, 4, 0, 1, NULL},

{{12,FsMIRipCryptoAuthIfIndex}, GetNextIndexFsMIRipCryptoAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoAuthAddress}, GetNextIndexFsMIRipCryptoAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoAuthKeyId}, GetNextIndexFsMIRipCryptoAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoAuthKey}, GetNextIndexFsMIRipCryptoAuthTable, FsMIRipCryptoAuthKeyGet, FsMIRipCryptoAuthKeySet, FsMIRipCryptoAuthKeyTest, FsMIRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoKeyStartAccept}, GetNextIndexFsMIRipCryptoAuthTable, FsMIRipCryptoKeyStartAcceptGet, FsMIRipCryptoKeyStartAcceptSet, FsMIRipCryptoKeyStartAcceptTest, FsMIRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoKeyStartGenerate}, GetNextIndexFsMIRipCryptoAuthTable, FsMIRipCryptoKeyStartGenerateGet, FsMIRipCryptoKeyStartGenerateSet, FsMIRipCryptoKeyStartGenerateTest, FsMIRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoKeyStopGenerate}, GetNextIndexFsMIRipCryptoAuthTable, FsMIRipCryptoKeyStopGenerateGet, FsMIRipCryptoKeyStopGenerateSet, FsMIRipCryptoKeyStopGenerateTest, FsMIRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoKeyStopAccept}, GetNextIndexFsMIRipCryptoAuthTable, FsMIRipCryptoKeyStopAcceptGet, FsMIRipCryptoKeyStopAcceptSet, FsMIRipCryptoKeyStopAcceptTest, FsMIRipCryptoAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipCryptoKeyStatus}, GetNextIndexFsMIRipCryptoAuthTable, FsMIRipCryptoKeyStatusGet, FsMIRipCryptoKeyStatusSet, FsMIRipCryptoKeyStatusTest, FsMIRipCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRipCryptoAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIRipRRDGlobalStatus}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDGlobalStatusGet, FsMIRipRRDGlobalStatusSet, FsMIRipRRDGlobalStatusTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRipRRDSrcProtoMaskEnable}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDSrcProtoMaskEnableGet, FsMIRipRRDSrcProtoMaskEnableSet, FsMIRipRRDSrcProtoMaskEnableTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRipRRDSrcProtoMaskDisable}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDSrcProtoMaskDisableGet, FsMIRipRRDSrcProtoMaskDisableSet, FsMIRipRRDSrcProtoMaskDisableTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRipRRDRouteTagType}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDRouteTagTypeGet, FsMIRipRRDRouteTagTypeSet, FsMIRipRRDRouteTagTypeTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIRipRRDRouteTag}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDRouteTagGet, FsMIRipRRDRouteTagSet, FsMIRipRRDRouteTagTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRipRRDRouteDefMetric}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDRouteDefMetricGet, FsMIRipRRDRouteDefMetricSet, FsMIRipRRDRouteDefMetricTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, "3"},

{{12,FsMIRipRRDRouteMapEnable}, GetNextIndexFsMIRipRRDGlobalTable, FsMIRipRRDRouteMapEnableGet, FsMIRipRRDRouteMapEnableSet, FsMIRipRRDRouteMapEnableTest, FsMIRipRRDGlobalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIRipRRDGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRipDistInOutRouteMapName}, GetNextIndexFsMIRipDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIRipDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipDistInOutRouteMapType}, GetNextIndexFsMIRipDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRipDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipDistInOutRouteMapValue}, GetNextIndexFsMIRipDistInOutRouteMapTable, FsMIRipDistInOutRouteMapValueGet, FsMIRipDistInOutRouteMapValueSet, FsMIRipDistInOutRouteMapValueTest, FsMIRipDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRipDistInOutRouteMapRowStatus}, GetNextIndexFsMIRipDistInOutRouteMapTable, FsMIRipDistInOutRouteMapRowStatusGet, FsMIRipDistInOutRouteMapRowStatusSet, FsMIRipDistInOutRouteMapRowStatusTest, FsMIRipDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRipDistInOutRouteMapTableINDEX, 3, 0, 1, NULL},

{{12,FsMIRipPreferenceValue}, GetNextIndexFsMIRipPreferenceTable, FsMIRipPreferenceValueGet, FsMIRipPreferenceValueSet, FsMIRipPreferenceValueTest, FsMIRipPreferenceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRipPreferenceTableINDEX, 1, 0, 0, "0"},

{{10,FsMIRip2ContextId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsMIRipAuthIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsMIRipAuthKeyId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{10,FsMIRipPeerAddress}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsmiriEntry = { 78, fsmiriMibEntry };

#endif /* _FSMIRIDB_H */

