/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirilw.h,v 1.8 2013/06/19 13:31:11 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIRip2GlobalTable. */
INT1
nmhValidateIndexInstanceFsMIRip2GlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRip2GlobalTable  */

INT1
nmhGetFirstIndexFsMIRip2GlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRip2GlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRip2Security ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2Peers ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2TrustNBRListEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2NumberOfDroppedPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRip2SpacingEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2AutoSummaryStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2RetransTimeoutInt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2MaxRetransmissions ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2OverSubscriptionTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2Propagate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipTrcFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRip2LastAuthKeyLifetimeStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRtCount ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRip2Security ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2Peers ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2TrustNBRListEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2SpacingEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2AutoSummaryStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2RetransTimeoutInt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2MaxRetransmissions ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2OverSubscriptionTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2Propagate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipTrcFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRip2LastAuthKeyLifetimeStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRip2Security ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2Peers ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2TrustNBRListEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2SpacingEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2AutoSummaryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2RetransTimeoutInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2MaxRetransmissions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2OverSubscriptionTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2Propagate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipTrcFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2LastAuthKeyLifetimeStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRip2GlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipGlobalTrcFlag ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipGlobalTrcFlag ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipGlobalTrcFlag ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipGlobalTrcFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRip2NBRTrustListTable. */
INT1
nmhValidateIndexInstanceFsMIRip2NBRTrustListTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRip2NBRTrustListTable  */

INT1
nmhGetFirstIndexFsMIRip2NBRTrustListTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRip2NBRTrustListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRip2TrustNBRRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRip2TrustNBRRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRip2TrustNBRRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRip2NBRTrustListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRip2IfConfTable. */
INT1
nmhValidateIndexInstanceFsMIRip2IfConfTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRip2IfConfTable  */

INT1
nmhGetFirstIndexFsMIRip2IfConfTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRip2IfConfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRip2IfAdminStat ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfOperState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfUpdateTmr ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfGarbgCollectTmr ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfRouteAgeTmr ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfSplitHorizonStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfDefRtInstall ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfSpacingTmr ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfInUseKey ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2IfConfAuthLastKeyStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRip2IfAdminStat ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRip2IfConfUpdateTmr ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRip2IfConfGarbgCollectTmr ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRip2IfConfRouteAgeTmr ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRip2IfSplitHorizonStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRip2IfConfDefRtInstall ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRip2IfConfSpacingTmr ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRip2IfAdminStat ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2IfConfUpdateTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2IfConfGarbgCollectTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2IfConfRouteAgeTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2IfSplitHorizonStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2IfConfDefRtInstall ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRip2IfConfSpacingTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRip2IfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRipMd5AuthTable. */
INT1
nmhValidateIndexInstanceFsMIRipMd5AuthTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRipMd5AuthTable  */

INT1
nmhGetFirstIndexFsMIRipMd5AuthTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRipMd5AuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipMd5AuthKey ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRipMd5KeyStartTime ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIRipMd5KeyExpiryTime ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIRipMd5KeyRowStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipMd5AuthKey ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRipMd5KeyStartTime ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIRipMd5KeyExpiryTime ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIRipMd5KeyRowStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipMd5AuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRipMd5KeyStartTime ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipMd5KeyExpiryTime ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipMd5KeyRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipMd5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRipCryptoAuthTable. */
INT1
nmhValidateIndexInstanceFsMIRipCryptoAuthTable ARG_LIST((INT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRipCryptoAuthTable  */

INT1
nmhGetFirstIndexFsMIRipCryptoAuthTable ARG_LIST((INT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRipCryptoAuthTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipCryptoAuthKey ARG_LIST((INT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRipCryptoKeyStartAccept ARG_LIST((INT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRipCryptoKeyStartGenerate ARG_LIST((INT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRipCryptoKeyStopGenerate ARG_LIST((INT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRipCryptoKeyStopAccept ARG_LIST((INT4  , INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRipCryptoKeyStatus ARG_LIST((INT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipCryptoAuthKey ARG_LIST((INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRipCryptoKeyStartAccept ARG_LIST((INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRipCryptoKeyStartGenerate ARG_LIST((INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRipCryptoKeyStopGenerate ARG_LIST((INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRipCryptoKeyStopAccept ARG_LIST((INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIRipCryptoKeyStatus ARG_LIST((INT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipCryptoAuthKey ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRipCryptoKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRipCryptoKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRipCryptoKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRipCryptoKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIRipCryptoKeyStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipCryptoAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRip2NBRUnicastListTable. */
INT1
nmhValidateIndexInstanceFsMIRip2NBRUnicastListTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRip2NBRUnicastListTable  */

INT1
nmhGetFirstIndexFsMIRip2NBRUnicastListTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRip2NBRUnicastListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRip2NBRUnicastNBRRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRip2NBRUnicastNBRRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRip2NBRUnicastNBRRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRip2NBRUnicastListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRip2LocalRouteTable. */
INT1
nmhValidateIndexInstanceFsMIRip2LocalRouteTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRip2LocalRouteTable  */

INT1
nmhGetFirstIndexFsMIRip2LocalRouteTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRip2LocalRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRip2RtIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2RtType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2Proto ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2ChgTime ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2Metric ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2RowStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRip2Gateway ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIRipAggTable. */
INT1
nmhValidateIndexInstanceFsMIRipAggTable ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRipAggTable  */

INT1
nmhGetFirstIndexFsMIRipAggTable ARG_LIST((INT4 * , INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRipAggTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipAggStatus ARG_LIST((INT4  , INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipAggStatus ARG_LIST((INT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipAggStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipAggTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRipRRDGlobalTable. */
INT1
nmhValidateIndexInstanceFsMIRipRRDGlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRipRRDGlobalTable  */

INT1
nmhGetFirstIndexFsMIRipRRDGlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRipRRDGlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipRRDGlobalStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRRDSrcProtoMaskEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRRDSrcProtoMaskDisable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRRDRouteTagType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRRDRouteTag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRRDRouteDefMetric ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRipRRDRouteMapEnable ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipRRDGlobalStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRRDSrcProtoMaskEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRRDSrcProtoMaskDisable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRRDRouteTagType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRRDRouteTag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRRDRouteDefMetric ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRipRRDRouteMapEnable ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipRRDGlobalStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRRDSrcProtoMaskEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRRDSrcProtoMaskDisable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRRDRouteTagType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRRDRouteTag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRRDRouteDefMetric ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRipRRDRouteMapEnable ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipRRDGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRipDistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsMIRipDistInOutRouteMapTable ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ));

/* Proto Type for Low Level GET FIRST fn for FsMIRipDistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsMIRipDistInOutRouteMapTable ARG_LIST((
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4    *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRipDistInOutRouteMapTable ARG_LIST((
    INT4    , 
    INT4    * , 
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4    , 
    INT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipDistInOutRouteMapValue ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ,
    INT4    *));

INT1
nmhGetFsMIRipDistInOutRouteMapRowStatus ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipDistInOutRouteMapValue ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

INT1
nmhSetFsMIRipDistInOutRouteMapRowStatus ARG_LIST((
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipDistInOutRouteMapValue ARG_LIST((UINT4 *  ,
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

INT1
nmhTestv2FsMIRipDistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,
    INT4     , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipDistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRipPreferenceTable. */
INT1
nmhValidateIndexInstanceFsMIRipPreferenceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRipPreferenceTable  */

INT1
nmhGetFirstIndexFsMIRipPreferenceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRipPreferenceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRipPreferenceValue ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRipPreferenceValue ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRipPreferenceValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRipPreferenceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
