/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsriplw.h,v 1.10 2013/06/19 13:31:11 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2Security ARG_LIST((INT4 *));

INT1
nmhGetFsRip2Peers ARG_LIST((INT4 *));

INT1
nmhGetFsRip2TrustNBRListEnable ARG_LIST((INT4 *));

INT1
nmhGetFsRip2NumberOfDroppedPkts ARG_LIST((UINT4 *));

INT1
nmhGetFsRip2SpacingEnable ARG_LIST((INT4 *));

INT1
nmhGetFsRip2AutoSummaryStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRip2RetransTimeoutInt ARG_LIST((INT4 *));

INT1
nmhGetFsRip2MaxRetransmissions ARG_LIST((INT4 *));

INT1
nmhGetFsRip2OverSubscriptionTimeout ARG_LIST((INT4 *));

INT1
nmhGetFsRip2Propagate ARG_LIST((INT4 *));

INT1
nmhGetFsRip2MaxRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsRipTrcFlag ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip2Security ARG_LIST((INT4 ));

INT1
nmhSetFsRip2Peers ARG_LIST((INT4 ));

INT1
nmhSetFsRip2TrustNBRListEnable ARG_LIST((INT4 ));

INT1
nmhSetFsRip2SpacingEnable ARG_LIST((INT4 ));

INT1
nmhSetFsRip2AutoSummaryStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRip2RetransTimeoutInt ARG_LIST((INT4 ));

INT1
nmhSetFsRip2MaxRetransmissions ARG_LIST((INT4 ));

INT1
nmhSetFsRip2OverSubscriptionTimeout ARG_LIST((INT4 ));

INT1
nmhSetFsRip2Propagate ARG_LIST((INT4 ));

INT1
nmhSetFsRip2MaxRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsRipTrcFlag ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip2Security ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2Peers ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2TrustNBRListEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2SpacingEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2AutoSummaryStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2RetransTimeoutInt ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2MaxRetransmissions ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2OverSubscriptionTimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2Propagate ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip2MaxRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipTrcFlag ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip2Security ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2Peers ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2TrustNBRListEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2SpacingEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2AutoSummaryStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2RetransTimeoutInt ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2MaxRetransmissions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2OverSubscriptionTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2Propagate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip2MaxRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipTrcFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip2NBRTrustListTable. */
INT1
nmhValidateIndexInstanceFsRip2NBRTrustListTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRip2NBRTrustListTable  */

INT1
nmhGetFirstIndexFsRip2NBRTrustListTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip2NBRTrustListTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2TrustNBRRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip2TrustNBRRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip2TrustNBRRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip2NBRTrustListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip2IfConfTable. */
INT1
nmhValidateIndexInstanceFsRip2IfConfTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRip2IfConfTable  */

INT1
nmhGetFirstIndexFsRip2IfConfTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip2IfConfTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2IfAdminStat ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfOperState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfUpdateTmr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfGarbgCollectTmr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfRouteAgeTmr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfSplitHorizonStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfDefRtInstall ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfSpacingTmr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfAuthType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfInUseKey ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsRip2IfConfAuthLastKeyStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip2IfAdminStat ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfConfUpdateTmr ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfConfGarbgCollectTmr ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfConfRouteAgeTmr ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfSplitHorizonStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfConfDefRtInstall ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfConfSpacingTmr ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsRip2IfConfAuthType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip2IfAdminStat ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfConfUpdateTmr ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfConfGarbgCollectTmr ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfConfRouteAgeTmr ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfSplitHorizonStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfConfDefRtInstall ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfConfSpacingTmr ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsRip2IfConfAuthType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip2IfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRipMd5AuthTable. */
INT1
nmhValidateIndexInstanceFsRipMd5AuthTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRipMd5AuthTable  */

INT1
nmhGetFirstIndexFsRipMd5AuthTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRipMd5AuthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipMd5AuthKey ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRipMd5KeyStartTime ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsRipMd5KeyExpiryTime ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsRipMd5KeyRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipMd5AuthKey ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRipMd5KeyStartTime ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsRipMd5KeyExpiryTime ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsRipMd5KeyRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipMd5AuthKey ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRipMd5KeyStartTime ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsRipMd5KeyExpiryTime ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsRipMd5KeyRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipMd5AuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRipCryptoAuthTable. */
INT1
nmhValidateIndexInstanceFsRipCryptoAuthTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRipCryptoAuthTable  */

INT1
nmhGetFirstIndexFsRipCryptoAuthTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRipCryptoAuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipCryptoAuthKey ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRipCryptoKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRipCryptoKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRipCryptoKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRipCryptoKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRipCryptoKeyStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipCryptoAuthKey ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRipCryptoKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRipCryptoKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRipCryptoKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRipCryptoKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsRipCryptoKeyStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipCryptoAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRipCryptoKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRipCryptoKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRipCryptoKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRipCryptoKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsRipCryptoKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipCryptoAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip2NBRUnicastListTable. */
INT1
nmhValidateIndexInstanceFsRip2NBRUnicastListTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRip2NBRUnicastListTable  */

INT1
nmhGetFirstIndexFsRip2NBRUnicastListTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip2NBRUnicastListTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2NBRUnicastNBRRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip2NBRUnicastNBRRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip2NBRUnicastNBRRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip2NBRUnicastListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip2LocalRoutingTable. */
INT1
nmhValidateIndexInstanceFsRip2LocalRoutingTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRip2LocalRoutingTable  */

INT1
nmhGetFirstIndexFsRip2LocalRoutingTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip2LocalRoutingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2RtIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRip2RtType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRip2Proto ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRip2ChgTime ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRip2Metric ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRip2RowStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRip2Gateway ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsRipAggTable. */
INT1
nmhValidateIndexInstanceFsRipAggTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRipAggTable  */

INT1
nmhGetFirstIndexFsRipAggTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRipAggTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipAggStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipAggStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipAggStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipAggTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRipRtCount ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip2PeerTable. */
INT1
nmhValidateIndexInstanceFsRip2PeerTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsRip2PeerTable  */

INT1
nmhGetFirstIndexFsRip2PeerTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip2PeerTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2PeerInUseKey ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2LastAuthKeyLifetimeStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip2LastAuthKeyLifetimeStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip2LastAuthKeyLifetimeStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip2LastAuthKeyLifetimeStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip2IfStatTable. */
INT1
nmhValidateIndexInstanceFsRip2IfStatTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRip2IfStatTable  */

INT1
nmhGetFirstIndexFsRip2IfStatTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip2IfStatTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip2IfStatRcvBadAuthPackets ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipRRDGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRipRRDSrcProtoMaskEnable ARG_LIST((INT4 *));

INT1
nmhGetFsRipRRDSrcProtoMaskDisable ARG_LIST((INT4 *));

INT1
nmhGetFsRipRRDRouteTagType ARG_LIST((INT4 *));

INT1
nmhGetFsRipRRDRouteTag ARG_LIST((INT4 *));

INT1
nmhGetFsRipRRDRouteDefMetric ARG_LIST((INT4 *));

INT1
nmhGetFsRipRRDRouteMapEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipRRDGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRipRRDSrcProtoMaskEnable ARG_LIST((INT4 ));

INT1
nmhSetFsRipRRDSrcProtoMaskDisable ARG_LIST((INT4 ));

INT1
nmhSetFsRipRRDRouteTagType ARG_LIST((INT4 ));

INT1
nmhSetFsRipRRDRouteTag ARG_LIST((INT4 ));

INT1
nmhSetFsRipRRDRouteDefMetric ARG_LIST((INT4 ));

INT1
nmhSetFsRipRRDRouteMapEnable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipRRDGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipRRDSrcProtoMaskEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipRRDSrcProtoMaskDisable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipRRDRouteTagType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipRRDRouteTag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipRRDRouteDefMetric ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRipRRDRouteMapEnable ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipRRDGlobalStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipRRDSrcProtoMaskEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipRRDSrcProtoMaskDisable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipRRDRouteTagType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipRRDRouteTag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipRRDRouteDefMetric ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRipRRDRouteMapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRipDistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsRipDistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRipDistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsRipDistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRipDistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipDistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsRipDistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipDistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsRipDistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipDistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsRipDistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipDistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRipPreferenceValue ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRipPreferenceValue ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRipPreferenceValue ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRipPreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
