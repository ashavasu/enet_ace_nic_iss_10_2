
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripmitdfs.h,v 1.12 2015/11/09 09:40:08 siva Exp $
 *
 * Description:This file contains Mutiple Instances         
 *             definitions that the RIP module uses.        
 * 
 *******************************************************************/
#ifndef   __RIP_MI_TDFS_H__
#define   __RIP_MI_TDFS_H__

#define RIP_CXT_EQUAL  0
#define RIP_CXT_LESS  -1
#define RIP_CXT_GREAT  1

typedef struct RipCxt{
    INT4             i4CxtId;
    UINT4            u4RipTrcFlag;
    tRipGblCfg       RipGblCfg; /* contains the  Global Configuration information of the  Context*/
    tRipRRDGblCfg    RipRRDGblCfg; /*Global Redistribution Configuration Information*/
    tRipGblStats     RipGblStats;/*RipGlobal Statistics*/
    tRipNbrListCfg   RipNbrListCfg; /*NeighborListConfiguration*/
    tTMO_SLL         RipIfAggTblRootLst ;/*Points To the AggrgationTableRoot*/
    tTMO_SLL         aRipImportList[MAX_ROUTING_PROTOCOLS];/*Redistributed Route List from other Protocols.*/
    tTMO_DLL         RipRtList;
    tTMO_DLL         RipCxtRtList; /*global route entry List*/
    tTMO_DLL         RipCxtSummaryList; /*Summary entry List*/
    UINT4            u4NumOfPassNeighbors; /*Number of Passive neighbors.*/
    t_RIP_TIMER      RipPassiveUpdTimer; /*Ponter to the Paasive Update Timer.*/
    t_RIP_TIMER      RipTrigUpdTimer;      /* Trigger Update Timer.*/
    tRipUpdateTimer  TripTrigTmr; /*Route change Timer.Won't send trigger Updation for every change. */
    VOID            *pRipRoot; /*Pointer to the TrieNode*/
    tTMO_DLL         RipIfSpaceLst; /*Interface records running space timer*/
    tFilteringRMap  *pDistributeInFilterRMap;  /* Config for Distribute in filtering feature */
    tFilteringRMap  *pDistributeOutFilterRMap; /* Config for Distribute out filtering feature */
    tFilteringRMap  *pDistanceFilterRMap;      /* Config for Distance filtering feature */
    UINT4            u4RipPeriodicUpdates;
    UINT4            u4TotalEntryCount; /*Total Number Of Existing Entries.*/
    UINT4            u4TotalSummaryCount; /*Total Number Of Existing Summary Entries.*/
    INT1             i1RipId; /*RIP Id for the Trie Node.*/
    UINT1            u1RipRouteDBInitialized;
    UINT1            u1CxtStatus; /* The status of this context  entry*/
    UINT1            u1AdminStatus;
    UINT1            u1Distance;
    UINT1            u1PrevDistance;
    BOOL1            b1LastKeyLifeTimeStatus;
    UINT1            u1Align[1];
}tRipCxt;

typedef struct{
 UINT4          u4SysUpInSecs; /*SysUp time in Secs.*/
 INT4           i4RipSd; /*RIP Socket Id*/
 tOsixSemId     RipSemId; /*Rip Sempahore Id.*/
 tOsixSemId     RipRtmLstSemId; /*RIP RTM semaphore Id.*/
        tRipTmrListId  RipTimerListId; /*Timer List Id*/
        tRipCxt        *pRipMgmtCxt;
        tRipCxt        *apRipCxt[SYS_DEF_MAX_NUM_CONTEXTS];
        UINT4          u4RipGlobalTrcFlag;
        tTMO_SLL       RtmRtLst;
                            /* SLL that contains the Routes from RTM till
                             * RIP Process them */
        INT4           i4RtCal; /*Route Calculation Completed or Not*/
    tTMO_HASH_TABLE *pGIfHashTbl;
}tRipRtr;
typedef struct{
 INT4   i4CxtId;
 UINT4  u4IfIdx;
 UINT1  u1Chg;
 UINT1  u1Align[3];
}tRipParams;

typedef struct __RipTrapInfo {
    INT4         i4CxtId;
    UINT4        u4PeerAddr;
    UINT4        u4IfIndex;
    INT4         i4AuthKeyId;
} tRipTrapInfo;

#endif /*_RIP_MI_TDFS_H__*/

