/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripsndfs.h,v 1.13 2016/06/18 11:46:29 siva Exp $
 *
 * Description:This file contains the definitions that are  
 *             needed for network management support.       
 *
 *******************************************************************/
#ifndef   __RIP_SN_DFS_H
#define   __RIP_SN_DFS_H

           /********************************************************
           * The following definitions corresponds to the bit mask *
           * definitions that are needed for management purposes   *
           ********************************************************/

            /* Dependency Bit Mask Definitions for ripIfConfTable */

#define   RIP_CONF_DEF_BIT             0x00    /* At start up            */
#define   RIP_DEF_METRIC_BIT           0x01
#define   RIP_STAT_BIT                 0x02
#define   RIP_SRC_ADDR_BIT             0x04
#define   RIPIF_CFG_STAT_MASK          0x07

#define   RIP_ROW_NOT_EXIST_CREATED       1
#define   RIP_ROW_ALREADY_EXIST           2
#define   RIP_ROW_NOT_EXIST               3

            /********************************************************
            * The following flag definitions correspond to the      *
            * settable parameters of rip2IfConfTable in the MIB for *
            * identification of the variable                        *
            ********************************************************/

#define   RIPIF_DOMAIN            0
#define   RIPIF_AUTHTYPE          1
#define   RIPIF_AUTHKEY           2
#define   RIPIF_SEND              3
#define   RIPIF_RECV              4
#define   RIPIF_DEF_MET           5
#define   RIPIF_ROW_STAT          6
#define   RIPIF_SRC_ADDR          7
#define   RIPIF_UPD_TMR           8
#define   RIPIF_GARB_TMR          9
#define   RIPIF_AGE_TMR          10
#define   RIPIF_ADMIN_FLAG       11
#define   RIPIF_SPLIT_HORIZON    12
#define   RIPIF_DEF_RT_INSTALL   13
#define   RIPIF_SPACE_TMR_VAL    14

           /********************************************************
           * The following definitions corresponds to the general  *
           * interface level definitions that RIP management sub   *
           * module uses.                                          *
           ********************************************************/

#define   RIP_MIN_NET_NUMBER    0x00000001
#define   RIP_MAX_NET_NUMBER    0xFFFFFFFF

#define   RIP_SET                        1
#define   RIP_VALIDATE                   2

 /*Marco for Primary/Secondary Ip record found for specific source address*/
#define   RIP_FLAG_SET                   1

#define   RIP_INV_NET_NUM       0xFFFFFFFF
#define   FROM_SNMP_TO_RIP      15
#define   RIP_INT_MAX           0x7FFFFFFF

#endif /*  End of #ifndef   __RIP_SN_DFS_H block Checking  */
