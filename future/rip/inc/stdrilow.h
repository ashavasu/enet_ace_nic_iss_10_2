
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRip2GlobalRouteChanges ARG_LIST((UINT4 *));

INT1
nmhGetRip2GlobalQueries ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Rip2IfStatTable. */
INT1
nmhValidateIndexInstanceRip2IfStatTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Rip2IfStatTable  */

INT1
nmhGetFirstIndexRip2IfStatTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRip2IfStatTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRip2IfStatRcvBadPackets ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetRip2IfStatRcvBadRoutes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetRip2IfStatSentUpdates ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetRip2IfStatStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetRip2IfStatStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Rip2IfStatStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Rip2IfStatTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Rip2IfConfTable. */
INT1
nmhValidateIndexInstanceRip2IfConfTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Rip2IfConfTable  */

INT1
nmhGetFirstIndexRip2IfConfTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRip2IfConfTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRip2IfConfDomain ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetRip2IfConfAuthType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetRip2IfConfAuthKey ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetRip2IfConfSend ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetRip2IfConfReceive ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetRip2IfConfDefaultMetric ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetRip2IfConfStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetRip2IfConfSrcAddress ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetRip2IfConfDomain ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetRip2IfConfAuthType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetRip2IfConfAuthKey ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetRip2IfConfSend ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetRip2IfConfReceive ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetRip2IfConfDefaultMetric ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetRip2IfConfStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetRip2IfConfSrcAddress ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Rip2IfConfDomain ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Rip2IfConfAuthType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Rip2IfConfAuthKey ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Rip2IfConfSend ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Rip2IfConfReceive ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Rip2IfConfDefaultMetric ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Rip2IfConfStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Rip2IfConfSrcAddress ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Rip2IfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Rip2PeerTable. */
INT1
nmhValidateIndexInstanceRip2PeerTable ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Rip2PeerTable  */

INT1
nmhGetFirstIndexRip2PeerTable ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexRip2PeerTable ARG_LIST((UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetRip2PeerLastUpdate ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetRip2PeerVersion ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetRip2PeerRcvBadPackets ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetRip2PeerRcvBadRoutes ARG_LIST((UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));
