/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: riprrdfs.h,v 1.16 2007/08/31 07:09:58 iss Exp $
 *
 * Description: Data structures defined in RIP for RRD
 *
 *******************************************************************/
/*#include "riprtdfs.h"*/
typedef struct
{
    UINT1  au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT1               u1RipRRDGblStatus;
    UINT1               u1RipRRDRouteTagType;
    UINT2               u2RipRRDRtTag;
    UINT2               u2RipRRDDefMetric;
    UINT2               u2ASNumber;
    UINT4               u4RouterId;
    UINT2               u2SrcProtMskEnable;
    UINT2               u2SrcProtMskDisable;
    UINT4               u4MaxMessageSize;
}
tRipRRDGblCfg;


#define   RTM_HDR_LEN          sizeof(tRtmMsgHdr)
#define   RTM_REG_MSG_LEN      sizeof(tRtmRegnMsg)
#define   RTM_DEREG_MSG_LEN    sizeof(tRtmRegnMsg)
#define   RTM_UPDATE_MSG_LEN   sizeof(tNetIpv4RtInfo)
#define   RRD_REG_MSG          1

#define   RIP_RRD_GBL_STATUS           3
#define   RIP_RRD_GBL_STAT_ENABLE      1
#define   RIP_RRD_GBL_STAT_DISABLE     2

/*** allowed values for route tag type  ***/

#define   RIP_RTM_NXTHOP    RIP_NULL_RTAG

#define   MANUAL                                  1
#define   AUTOMATIC                               2
#define   RIP_RRD_DEF_RT_TAG                      0
#define   RIP_RRD_DEF_SRC_PRO_ENA_MSK        0x0000
#define   RIP_RRD_DEF_SRC_PRO_DIS_MSK        0x0000
#define   RIP_SET_RT_CHG_NOTIFY                0x80
#define   RT_STATUS_DESTROY                       6


/*** message type in RIP->RTM messages ***/
#define   RIP_RTM_REDIST_ENA_MSG   RTM_REDISTRIBUTE_ENABLE_MESSAGE
#define   RIP_RTM_REDIST_DIS_MSG   RTM_REDISTRIBUTE_DISABLE_MESSAGE

/*** message type in RTM->RIP messages ***/
#define   RIP_RTM_RT_UPDATE        RTM_ROUTE_UPDATE_MESSAGE
#define   RIP_RTM_RT_CHG           RTM_ROUTE_CHANGE_NOTIFY_MESSAGE
#define   RIP_RTM_ACK_MSG          RTM_REGISTRATION_ACK_MESSAGE

/****************************************************************************/
/***        rip definitions related to SNMP                             *****/

#define   SNMP_BIT_RRD_STATUS           0x02    /* bit 1 */
#define   SNMP_BIT_SRC_PROTO_MSK_ENA    0x04    /* bit 2 */
#define   SNMP_BIT_SRC_PROTO_MSK_DIS    0x08    /* bit 3 */
#define   SNMP_BIT_RIP_ADMIN_STATUS     0x01    /* bit 0 */

/*****************************************************************************/

/*****************************************************************************/
/*             rtm related definitions                ******/
#define   RTM_Q_NODE_ID               RTM_MESSAGE_ARRIVAL_Q_NODE_ID
#define   RIP_RTM_INPUT_Q_NAME        RTM_MESSAGE_ARRIVAL_Q_NAME
#define   RIP_RTM_TASK_NODE_ID        SELF
#define   RIP_RTM_TASK_NAME           RTM_MAIN_TASK_NAME
#define   RTM_INPUT_EVENT             0x00000200


