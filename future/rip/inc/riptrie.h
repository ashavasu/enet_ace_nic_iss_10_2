/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: riptrie.h,v 1.10 2007/02/01 15:03:43 iss Exp $
 *
 * Description:This file contains structure definitions     
 *             related to trie library.                     
 * 
 *******************************************************************/
#ifndef   __RIP_TRIE_H
#define   __RIP_TRIE_H

          /********************************************************
           * This structure is exclusively defined to handle       *
           * trie library needs.                                   *
           * The fields of this structure are not appropriately    *
           * named as at different time it can hold different value*
           ********************************************************/

typedef struct _RipOutParams
{
    tScanOutParams  ScanParams;
    void            *pRipInfoBlk;
    void            *pu2NoOfRoutes;
    UINT4           u4DestNet;
    UINT2           u2If;
    UINT2           u2DestUdpPort;
    UINT2           u2RtsToCompose;
    UINT1           u1BcstFlag;
    UINT1           u1Changed;
} tRipOutParams;

#endif /* __RIP_TRIE_H */
