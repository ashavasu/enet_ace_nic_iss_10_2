/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistlw.h,v 1.5 2013/05/23 12:44:50 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIStdRip2GlobalTable. */
INT1
nmhValidateIndexInstanceFsMIStdRip2GlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdRip2GlobalTable  */

INT1
nmhGetFirstIndexFsMIStdRip2GlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdRip2GlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdRip2GlobalRouteChanges ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdRip2GlobalQueries ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdRip2IfStatTable. */
INT1
nmhValidateIndexInstanceFsMIStdRip2IfStatTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdRip2IfStatTable  */

INT1
nmhGetFirstIndexFsMIStdRip2IfStatTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdRip2IfStatTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdRip2IfStatRcvBadPackets ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdRip2IfStatRcvBadRoutes ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdRip2IfStatSentUpdates ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdRip2IfStatStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdRip2IfStatPeriodicUpdates ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdRip2IfStatRcvBadAuthPackets ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdRip2IfStatStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdRip2IfStatStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdRip2IfStatTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdRip2IfConfTable. */
INT1
nmhValidateIndexInstanceFsMIStdRip2IfConfTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdRip2IfConfTable  */

INT1
nmhGetFirstIndexFsMIStdRip2IfConfTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdRip2IfConfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdRip2IfConfDomain ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdRip2IfConfAuthType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdRip2IfConfAuthKey ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdRip2IfConfSend ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdRip2IfConfReceive ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdRip2IfConfDefaultMetric ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdRip2IfConfStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdRip2IfConfSrcAddress ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdRip2IfConfDomain ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdRip2IfConfAuthType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdRip2IfConfAuthKey ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdRip2IfConfSend ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdRip2IfConfReceive ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdRip2IfConfDefaultMetric ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdRip2IfConfStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdRip2IfConfSrcAddress ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdRip2IfConfDomain ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdRip2IfConfAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdRip2IfConfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdRip2IfConfSend ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdRip2IfConfReceive ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdRip2IfConfDefaultMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdRip2IfConfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdRip2IfConfSrcAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdRip2IfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdRip2PeerTable. */
INT1
nmhValidateIndexInstanceFsMIStdRip2PeerTable ARG_LIST((INT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdRip2PeerTable  */

INT1
nmhGetFirstIndexFsMIStdRip2PeerTable ARG_LIST((INT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdRip2PeerTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdRip2PeerLastUpdate ARG_LIST((INT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdRip2PeerVersion ARG_LIST((INT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdRip2PeerRcvBadPackets ARG_LIST((INT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdRip2PeerRcvBadRoutes ARG_LIST((INT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdRip2PeerInUseKey ARG_LIST((INT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
