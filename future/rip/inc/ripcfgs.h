/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripcfgs.h,v 1.30 2016/07/16 11:13:48 siva Exp $
 *
 * Description:This file contains some of the definitions   
 *             related to the configuration parameters in   
 *             RIP.
 *
 *******************************************************************/
#ifndef   __RIP_CFGS_H__
#define   __RIP_CFGS_H__

          /********************************************************
           * The following definitions pertain to general          *
           * parameters that can be configurable.                  *
           ********************************************************/

#define   RIP_INVALID_MAX_ROUTE_ENTRIES 10000

#define   RIP_MIN_UPD_TIME          1    /* Needs to change to 5 */
#define   RIP_INFINITY             16
#define   RIP_LEAST_METRIC          1
#define   RIP_IP_DEF_TTL            1

#define   RIP_MIN_TRIG_UPD_TIME     5

#define   RIP_TRIE_NUM_ENTRIES     25
#define   RIP_MAX_IF_AGG           20

#define   RIP_JITTER               5
#define   RIP_MAX_JITTER           25
#define   RIP_DIV_FACTOR           180
#define   RIP_MIN_TIMER_VAL        3600
#define   RIP_DEL                  2
#define   RIP_DEL_ALL              3





           /********************************************************
           * The following definitions are related to the control  *
           * of RIP Propogate Static Route flag                    *
           ********************************************************/

#define   RIP_PROP_STATIC_ENABLE     1
#define   RIP_PROP_STATIC_DISABLE    2

           /********************************************************
           * The following definitions are related to the security *
           * aspect of RIP protocol on a global basis              *
           ********************************************************/

#define   RIP_MINIMUM_SECURITY    1
#define   RIP_MAXIMUM_SECURITY    2

           /********************************************************
           * The following definitions are related to the split    *
           * horizon operational status on a global basis.         *
           ********************************************************/

#define   RIP_SPLIT_HORIZON               1
#define   RIP_SPLIT_HORZ_WITH_POIS_REV    2
#define   RIP_NO_SPLIT_HORIZON            3


          /**********************************************************
          * Defintions related installing default route             *
          **********************************************************/

#define   RIP_NO_DEF_RT_ORIGINATE         0
#define   RIP_INSTALL_DEF_RT              1
#define   RIP_DONT_INSTALL_DEF_RT         2

          /**********************************************************
          * Defintions related to the number of peers in the system *
          **********************************************************/

#define   RIP_MIN_PEER    1
#define   RIP_MAX_PEERS   65535
#define   RIP_DEF_PEER    5
#define   RIP_MAX_PEER(pRipCxtEntry)    pRipCxtEntry->RipGblCfg.u2TotalNoOfPeers

           /********************************************************
           * The following definition is related to the interface  *
           * specific configuration information                    *
           ********************************************************/

#define   RIPIF_MAX_LOGICAL_IFACES    IPIF_MAX_LOGICAL_IFACES

           /********************************************************
           * The following definitions are related to the          *
           * configuration information of various timers that are  *
           * needed for RIP protocol operation                     *
           ********************************************************/

#define   RIP_DEF_ROUTE_AGE                 180
#define   RIP_MAX_ROUTE_AGE                 500
#define   RIP_MIN_ROUTE_AGE                  30

#define   RIP_DEF_GARBAGE_COLCT_INTERVAL    120
#define   RIP_MAX_GARBAGE_COLCT_INTERVAL    180
#define   RIP_MIN_GARBAGE_COLCT_INTERVAL    120

#define   RIP_DEF_UPDATE_INTERVAL            30
 
 /*  This 15 is based on the default update timer 30 (30/2). Change in
  *  RIP_DEF_UPDATE_INTERVAL should always change the RIP_DEF_TOTAL_NUM_SPACE
  */
#define   RIP_DEF_TOTAL_NUM_SPACE      15
#define   RIP_DEF_SPACE_INTERVAL        1
#define   RIP_NUM_PACKETS_PER_SPACE    10
#define   RIP_MAX_UPDATE_INTERVAL    3600
#define   RIP_MIN_UPDATE_INTERVAL      10
#define   RIP_IF_IP_ADDR_CHG           4
#define   RIP_MIN_SPACE_INTERVAL       0
#define   RIP_MAX_SPACE_INTERVAL     ((10 * RIP_MAX_UPDATE_INTERVAL) /100)
#define   RIP_OFFSET(x,y)           FSAP_OFFSETOF(x,y)

           /********************************************************
           * The following defines the global configuration        *
           * structure related to route propagation that are       *
           * configurable at IP level but a copy is maintained in  *
           * RIP also, and also some general parameters that need  *
           * to be configured at global level.                     *
           ********************************************************/

typedef struct
{
    INT1   i1PropagateStatic;  /* Indicates whether static routes are */
    UINT1  u1MaximumSecurity;  /* Indicates the security level */
    UINT1  u1TrigUpdFlag;      /* This flag is used for reflecting
                                * the triggered update protocol timer                                           * operation
                                */ 
    UINT1  u1TrigDesired;      /* This flag is used for reflecting
                                * the triggered update protocol
                                */   
    UINT1  u1SpacingEnable;    /* Spacing enabled/disabled Status */
    UINT1  u1AutoSummaryStatus;/* Auto-summary enabled/disabled status*/
    UINT2  u2MaxRetxCount;
    UINT2  u2RetxTimeout;
    UINT2  u2SubscrTimeout;
    UINT2  u2ReservedWord;
    UINT2  u2TotalNoOfPeers;   /* This reflects total no of peers
                                * that can exist in the RIP system 
                                */
} tRipGblCfg;

/*  A general macro definition */
#define   RIP_TTL(Flag)    (Flag == RIP_RES_FOR_REQ) ? 0 : RIP_INITIAL_TTL_VALUE

           /********************************************************
           * The following defines the global configuration        *
           * structure related to RIP neighbor list                *
           ********************************************************/
#define   RIP_MAX_NUM_NBRS              16
#define   RIP_NBR_LIST_ENABLE            1
#define   RIP_NBR_LIST_DISABLE           2

#define   RIP_NBR_LIST_ACTIVE            1
#define   RIP_NBR_LIST_NOT_IN_SERVICE    2 
#define   RIP_NBR_LIST_NOT_READY         3 
#define   RIP_NBR_LIST_CREATE_AND_GO     4
#define   RIP_NBR_LIST_CREATE_AND_WAIT   5 
#define   RIP_NBR_LIST_DESTROY           6

#define   RIP_NBR_NOT_FOUND             -1


typedef struct _aNbrTrustList
{
    UINT4     u4FsRip2TrustNBRIpAddr;

    UINT4     u4FsRip2TrustNBRRowStatus;

} tRipNbrList ;


typedef struct _RipNbrListCfg
{
    UINT4     u4NumAuthorizedNbrs;         /* The number of authorized neighbors
                                            * in the List.  
                                            */
    UINT4     u4NumPktsDropped;            /* Number of update packets dropped 
                                            * due to neighbor list check.
                                            */
    UINT1     u1RipNbrListStatus;          /* This specificies the status of the
                                            * Neighbor List whether Enabled or 
                                            * Disabled.  
                                            */
    UINT1     u1AlignmentByte;
    UINT2     u2AlignmentWord;

    tRipNbrList   aNbrList[RIP_MAX_NUM_NBRS];  /* This holds list of all neighbors 
                                            * authorized to exchange RIP updates
                                            */
} tRipNbrListCfg ;


           /********************************************************
           *         Extern Declarations of the variables          *
           ********************************************************/


#define   RIP_SPACING_STATUS(pRipCxtEntry)         pRipCxtEntry->RipGblCfg.u1SpacingEnable 
#define   RIP_CFG_MAX_RETX(pRipCxtEntry)           pRipCxtEntry->RipGblCfg.u2MaxRetxCount  
#define   RIP_CFG_GET_RETX_TIMEOUT(pRipCxtEntry)   pRipCxtEntry->RipGblCfg.u2RetxTimeout   
#define   RIP_CFG_SUBSCRIP_TIME(pRipCxtEntry)      pRipCxtEntry->RipGblCfg.u2SubscrTimeout 
#define   RIP_ROUTE_INFO_PTR_FROM_RTLST(pListNode,i2OffSet) \
                       (tRipRtEntry *)(VOID *)(((UINT1*)pListNode) - i2OffSet)
#define   RIP_DEF_RETX_TIMEOUT      5
#define   RIP_DEF_RETX_COUNT       36
#define   RIP_DEF_SUBSCRIP_TIME    180

#define   RIP_PURGE_ROUTES          1
#define   RIP_START_DBASE_TIMER     2
#define   RIP_STATIC_DEF_METRIC    10

#endif /* End of #ifdef __RIP_CFGS_H__ Block Checking */
