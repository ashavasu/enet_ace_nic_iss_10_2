/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripproto.h,v 1.47 2016/09/13 12:57:12 siva Exp $
 *
 * Description: This file contains global prototypes of RIP 
 *              functions
 *
 *******************************************************************/
#ifndef   __RIP_PROTO_H__
#define   __RIP_PROTO_H__


EXPORT VOID rip_initialize_if_to_defaults ARG_LIST ((tRipIfaceRec *pRipIfRec));


EXPORT UINT4 rip_task_ip_get_net_mask ARG_LIST ((UINT2, UINT4, INT1));
EXPORT INT4 rip_ip_get_oper_status ARG_LIST ((UINT2));
EXPORT INT4 rip_get_rt ARG_LIST ((UINT4, INT1, UINT2 *, UINT4 *));
EXPORT VOID rip_ip_join_mcast_group ARG_LIST ((UINT2));
EXPORT VOID rip_ip_leave_mcast_group ARG_LIST ((UINT2));
EXPORT INT4 rip_timer_expiry_handler ARG_LIST ((VOID));
EXPORT VOID rip_reset_timer_expiry_handler ARG_LIST ((VOID));
EXPORT VOID rip_shutdown ARG_LIST ((VOID));
INT4 rip_is_poison_reverse_applicable ARG_LIST ((tRipInfoBlk *, UINT2, tRipCxt *));
/* 2.0.1.1 Fix 1 15-09-99 --START-- */


/* 2.0.1.1 Fix 1 15-09-99 --END-- */
EXPORT INT4 rip_rt_init ARG_LIST ((VOID));
/*#ifdef _RTMTDFS_H*/

EXPORT INT1        RipAddLocalRoute (tRipIfaceRec *pRipIfRec);

EXPORT INT4         rip_add_route
ARG_LIST ((UINT4, UINT1 *, UINT4, UINT2, UINT4, UINT4, UINT4, INT1, UINT1, UINT4, tRipCxt *));
EXPORT INT4 rip_add_new_route ARG_LIST ((tRipRtEntry * pRt, tRipCxt *));
EXPORT VOID         rip_calculate_ifcounter_for_route
ARG_LIST ((UINT2, tRipRtEntry *, tRipRtEntry *, UINT4));
EXPORT INT4 rip_rt_timeout_handler ARG_LIST ((VOID *, tRipCxt *));
EXPORT VOID rip_decrement_ifcounter_for_routes ARG_LIST ((tRipCxt *));
EXPORT VOID        *rip_rt_get_info
ARG_LIST ((UINT4 u4DestAddr, UINT4 u4NetMask, tRipCxt *));
/* 2.0.1.1 Fix 1 15-09-99 --START-- */

EXPORT INT4         rip_send_rip_rt_info
ARG_LIST ((UINT1, UINT1, UINT2, tRipInfoBlk *, UINT2 *, UINT4, UINT2, INT1, tRipCxt *));

/* 2.0.1.1 Fix 1 15-09-99 --END-- */

EXPORT VOID rip_if_enable ARG_LIST ((tRipIfaceRec *pRipIfRe));
EXPORT VOID rip_if_disable ARG_LIST ((tRipIfaceRec *pRipIfRec));
EXPORT VOID rip_if_destroy ARG_LIST ((tRipIfaceRec *pRipIfRec));
INT4   RipDeleteRtTbl ARG_LIST ((tRipCxt *));
EXPORT VOID rip_clear_route_table ARG_LIST ((VOID));
EXPORT INT1 rip_get_rip2GlobalRouteChanges ARG_LIST ((UINT4 *));
EXPORT INT1 rip_get_rip2GlobalQueries ARG_LIST ((UINT4 *));
EXPORT INT1 rip_get_rip2Security ARG_LIST ((INT4 *));
EXPORT INT1 rip_get_rip2SplitHorizonStatus ARG_LIST ((INT4 *));
EXPORT INT1 rip_get_rip2Peers ARG_LIST ((UINT4 *));
EXPORT INT1 rip_validate_rip2AdminStat ARG_LIST ((INT4));
EXPORT INT1 rip_validate_rip2Security ARG_LIST ((INT4));
EXPORT INT1 rip_validate_rip2SplitHorizonStatus ARG_LIST ((INT4));
EXPORT INT1 rip_validate_rip2Peers ARG_LIST ((INT4));
EXPORT INT1 rip_set_rip2AdminStat ARG_LIST ((INT4));
EXPORT INT1 rip_set_rip2Security ARG_LIST ((INT4));
EXPORT INT1 rip_set_rip2SplitHorizonStatus ARG_LIST ((INT4));
EXPORT INT1 rip_set_rip2Peers ARG_LIST ((INT4));
EXPORT INT4 rip_reset_protocol ARG_LIST ((VOID));
EXPORT UINT4 RipIfGetAddrFromPort ARG_LIST ((UINT4));
EXPORT UINT4 RipIfGetMaskFromPort ARG_LIST ((UINT4));
EXPORT UINT4 RipIfGetBaddrFromPort ARG_LIST ((UINT4));
EXPORT INT4 RipIfGetIndexFromAddrInCxt ARG_LIST ((UINT4, UINT4));
EXPORT UINT1 RipIfGetOperStatusFromPort ARG_LIST ((UINT4));
EXPORT UINT4 RipIfGetDflNetMask ARG_LIST ((UINT4));
EXPORT INT4 RipIfGetIfIdFromIfIndex ARG_LIST ((UINT4, tIP_INTERFACE *));
EXPORT UINT1 RipIfIsUnnumbered ARG_LIST ((UINT2));
EXPORT INT4 RipIfGetIpForUnnumIf ARG_LIST ((tRipCxt *, UINT4 *));
EXPORT INT4 RipIfGetConfAuthKey ARG_LIST ((UINT4  , tSNMP_OCTET_STRING_TYPE * ));
EXPORT INT4 RipIfGetValidPeerIpAddress ARG_LIST ((tRipIfaceRec *));

VOID RipIfStateChgHdlr ARG_LIST ((tNetIpv4IfInfo *, UINT4));


INT4  RipProcessRtmRts  ARG_LIST ((VOID));

VOID  RipClearIfInfo ARG_LIST ((tRipIfaceRec *pRipIfRec));
/* Added new routine to get PeriodicUpdates stats */
INT4 RipGetIfStatsPeriodicUpdates ARG_LIST ((UINT4 , UINT4 *, tRipCxt *));

INT4 RipProcessPktFromRtm ARG_LIST ((tCRU_BUF_CHAIN_HEADER *));
VOID RipInitImportList ARG_LIST ((tRipCxt *));
VOID RipUpdateImportTbl ARG_LIST ((tRipRtEntry *, UINT1, UINT1, tRipCxt *));
INT4 RipSendRtmRedistEnableMsgInCxt ARG_LIST ((tRipCxt *, INT2, UINT1 *));
INT1
               UpdateRipRoutingTable (tNetIpv4RtInfo RtUpdate, tRipRtEntry * pRt, tRipCxt *);

INT4                RipSetRrdGblStatusEnable (INT4);
INT4                RipSetRrdGblStatusDisable (INT4);


INT1 RipAddRouteToForwardingTableInCxt (tRipCxt *, tRipRtEntry *);
INT1 RipModifyRouteToForwardingTableInCxt (tRipCxt *,tRipRtEntry *);
INT1 RipDeleteRouteFromForwardingTableInCxt (tRipCxt *,tRipRtEntry *);
INT1 RipApplyInOutFilter( tFilteringRMap* pFilterRMap, tRipRtInfo * pRtInfo, UINT4 u4Src );

#ifdef FUTURE_SNMP_WANTED

INT4 RegisterStdRIPwithFutureSNMP PROTO ((void));
INT4 RegisterFSRIPwithFutureSNMP PROTO ((void));

#endif /* FUTURE_SNMP_WANTED */

EXPORT UINT4 RipSendToQ(void *, UINT4);

INT4 RipCidrCompareIndics PROTO ((UINT4 u4Addr1,
                                 UINT4 u4Mask1, UINT4 u4Addr2, UINT4 u4Mask2));
VOID RipIfAggrTblInit PROTO ((UINT4 u4IfIndex));
UINT4 RipGetIfHashIndex PROTO ((UINT4 u4Port));
INT4 RipCreateCxtEntry(tRipCxt *);
INT4 RipDelCxtEntry(tRipCxt *);
INT4 RipGetVrfNameFrmCxtId(INT4, UINT1 *);
VOID RipPktRcvdOnSocket(INT4);
INT4 RipDeRegisterWithOthersInCxt (INT4 i4CxtId);
INT4 RipDelTrieRootEntry(tRipCxt *);
INT1 RipSendTriggeredUpdate(tRipCxt *);
UINT4 RipGetJitterVal (UINT4 , UINT4);
INT4  RipProcessRespMessages (VOID);
INT4 RipDisableAdminStat(tRipCxt *);
INT4 RipEnableAdminStat(tRipCxt *);
VOID RipUpdateRouteEntry (tRipRtEntry *, tRipCxt *);
VOID RipUpdateIfaceNextRtSend (tRipCxt *, tRipRtEntry *);
INT4
RipSendUpdateAndStartSpaceTmr(UINT1, UINT1,UINT4, UINT2, tRipInfoBlk *, UINT2,UINT2 *, UINT2 *,
         UINT2 *, tRipIfaceRec *, tRipCxt *, tTMO_DLL_NODE *);
EXPORT tRipRtEntry *
RipGetBestRoute (UINT4 u4DestAddr,UINT4 u4DestMask,tRipCxt *pRipCxtEntry);

tRipRtEntry *
RipGetBestRouteForUpdateToRtm(UINT4 u4DestAddr,UINT4 u4DestMask,
                              tRipCxt *pRipCxtEntry);
VOID
RipAddNewBestRoutesToRtm(tRipRtEntry *pTempRt,tRipCxt *pRipCxtEntry);
INT4 RipGetIfStatsRequestReceived ARG_LIST ((UINT4 , UINT4 *, tRipCxt *));
INT4 RipGetIfStatsResponseReceived ARG_LIST ((UINT4 , UINT4 *, tRipCxt *));
INT4 RipGetIfStatsUpdateRequestReceived ARG_LIST ((UINT4 , UINT4 *, tRipCxt *));
INT4 RipGetIfStatsUpdateResponseReceived ARG_LIST ((UINT4 , UINT4 *, tRipCxt *));
INT4 RipGetIfStatsUpdatesAcknowledged ARG_LIST ((UINT4 , UINT4 *, tRipCxt *));
INT4 RipGetIfStatsRipFailcount ARG_LIST ((UINT4 , tRipInFailCount *, tRipCxt *));

INT4    RipGetPortFromIfIndex PROTO ((INT4 i4IfIndex, UINT4 *pu4Port));
INT4    RipGetIfIndexFromPort PROTO ((UINT4 u4Port, UINT4 *pu4IfIndex));
UINT4   RipUtilGetSecondsSinceBase PROTO ((tUtlTm utlTm));
INT4    RipUtilConvertTime  PROTO ((UINT1 *pu1TimeStr, tUtlTm * tm));
VOID    RipUtilGetKeyTime PROTO ((UINT4 u4Secs, UINT1 *pAuthKeyTime));
VOID    RipUtilGetShaDigest PROTO ((UINT1 *pu1RipPkt, UINT2 u2LenToSend, UINT2 u2AuthType,
                            tCryptoAuthKeyInfo *pCryptoKeyInfo, UINT1 *pdigest));
VOID    RipUtilHandleAuthentication PROTO ((UINT1 *pu1RipPkt, UINT2 u2AuthType, 
                                            UINT2 *pu2LenToSend,
                                            tCryptoAuthKeyInfo *pCryptoKeyInfo));
INT4    RipUtilValidateAuthentication PROTO ((UINT1 *pu1RipPkt, UINT2 *pu2Len,
                                            tCryptoAuthKeyInfo *pCryptoKeyInfo, UINT2 u2AuthType));
VOID    RipSortInsertAuthKey PROTO ((tTMO_SLL * pCryptoAuthkeyList, tCryptoAuthKeyInfo *pCryptoKeyInfo));
tCryptoAuthKeyInfo * 
RipCryptoGetKeyToUseForSend PROTO ((UINT2 u2IfId, tRipCxt * pRipCxtEntry));
tCryptoAuthKeyInfo * 
RipCryptoGetKeyToUseForReceive PROTO ((UINT2 u2IfId, UINT1 u1KeyId, tRipCxt * pRipCxtEntry));

tSNMP_OID_TYPE     *
RipUtilMakeObjIdFromDotNew (INT1 *pi1TxtStr);
INT4
RipUtilParseSubIdNew (UINT1 **ppu1TempPtr);
VOID
RipUtilSnmpIfSendTrap (UINT1 u1TrapId, tRipTrapInfo *pTrapInfo);
INT4  RipDeleteRtEntry (tRipRtEntry *, tRipCxt *);
INT4  RipSendInitialRequest (UINT2, UINT4, tRipCxt *);

VOID RipRedDynDataDescInit (VOID);
VOID RipRedDescrTblInit (VOID);
VOID RipRedDbNodeInit (tDbTblNode * pDBNode, UINT4 u4Type);
INT4 RipRedInitGlobalInfo (VOID);
INT4 RipRedDeInitGlobalInfo (VOID);
INT4 RipRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
VOID RipRedHandleRmEvents (UINT1 u1Flag);
VOID RipRedHandleGoActive (VOID);
VOID RipRedHandleGoStandby (tRipRmMsg * pMsg);
VOID RipRedHandleIdleToActive (VOID);
VOID RipRedHandleIdleToStandby (VOID);
VOID RipRedHandleStandbyToActive (VOID);
VOID RipRedHandleActiveToStandby (VOID);
VOID RipRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen);
VOID RipRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen);
INT4 RipRedRmReleaseMemoryForMsg (UINT1 *pu1Block);
INT4 RipRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length);
VOID RipRedSendBulkReqMsg (VOID);
VOID RipRedDbUtilAddRtInfo (tRipRtEntry *pRipRt);
VOID RipRedDbUtilAddTblNode (tDbTblDescriptor * pRipDataDesc,
                             tDbTblNode * pRipDbNode);
VOID RipRedSyncDynInfo (VOID);
VOID RipRedAddAllPeerNodeInDbTbl (VOID);
VOID RipRedAddAllRouteNodeInDbTbl (VOID);
VOID RipRedSendBulkDefCxtInfo (VOID);
VOID RipRedSendBulkUpdMsg (VOID);
VOID RipRedSendBulkUpdTailMsg (VOID);
VOID RipRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID RipRedProcessDynamicRtInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID RipRedProcessDynamicPeerInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
INT4 RipRedStartTimers (VOID);
INT4 RipRedNotifyRestartRTM (UINT4  u4CxtId);

INT4
RipRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));
INT4
RipGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum);
INT4
RipCliGetShowCmdOutputToFile (UINT1 *);
INT4
RipCliCalcSwAudCheckSum (UINT1 *, UINT2 *);
INT1
RipCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);
VOID RipRedHandleDynSyncAudit (VOID);
VOID RipExecuteCmdAndCalculateChkSum  (VOID);
INT4 RipRedStopTimersOnStandby (tRipCxt * pRipCxtEntry);
#ifdef ROUTEMAP_WANTED
VOID RipUpdateRtInfo (tRtMapInfo * pInfo, tRipRtInfo * pRtInfo);
#endif /* ROUTEMAP_WANTED */

#endif /*  __RIP_PROTO_H__ */
