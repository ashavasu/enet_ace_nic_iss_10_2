
#ifndef   __RIP_AGGR_H__
#define   __RIP_AGGR_H__
typedef struct _RipIfAggTblRoot
{
    tTMO_SLL_NODE pNext;
    UINT4         u4IfIndex;       /* the interface index */
    VOID         *pRipIfAggTblPtr; /* root of the trie that holds the 
                                      aggregated routes for this interface*/
}tRipIfAggTblRoot;

typedef struct _RipIfAggRtInfo
{
    VOID *pNextApp;
    VOID *pNextAlternatepath;
    UINT4 u4AggRtAddr;   /*The aggregated route address*/
    UINT4 u4AggRtMask;   /*The aggregated route mask*/
    UINT2 u2Count;       /* number of sub net routes currently available ,
                            falling under this aggregation.*/
    UINT2 u2Metric;      /*Metric of this aggregated route*/
    UINT2 u2Status;      /* Route change status*/
    UINT1 u1SinkRtFlag;  /* This flag specify if the route is added to rtm as 
                            sink route or not*/
    UINT1 u1AggRtStatus; /* The status of this aggregated route entry*/
    tRipCxt *pRipCxt;
}tRipIfAggRtInfo;

#define RIP_SUMMARY_MASK               0xff000000

#define RIP_GET_SUMMARY_ADDRESS(u4RtAddr) \
        (u4RtAddr & RIP_SUMMARY_MASK)

#define RIP_ALLOC_IF_AGG_REC_MEM(pu1Block) \
   (pu1Block = (tRipIfAggRtInfo *)(MemAllocMemBlk ((tMemPoolId)(IpRipMemory.IfAggRtRecId))))

#define RIP_RELEASE_IF_AGG_REC_MEM(ppu1Block) \
       MemReleaseMemBlock ((tMemPoolId)(IpRipMemory.IfAggRtRecId) ,(UINT1 *) ppu1Block)

#define RIP_ALLOC_IF_AGG_TBL_ROOT_MEM(pu1Block) \
   (pu1Block = (tRipIfAggTblRoot *)(MemAllocMemBlk ((tMemPoolId)(IpRipMemory.IfAggTblRootId))))

#define RIP_RELEASE_IF_AGG_TBL_ROOT_MEM(ppu1Block) \
       MemReleaseMemBlock ((tMemPoolId)(IpRipMemory.IfAggTblRootId) ,(UINT1 *) ppu1Block)

#define   RIP_IF_AGG_RT_DEF_STATUS     0
#define   RIP_IF_AGG_RT_CHANGED_VAL    1 
#define   RIP_IF_AGG_RT_UPDATED_VAL    2

#define   RIP_IF_AGG_RT_STATUS(pRt)  (pRt->u2Status)

#define RIP_IF_AGG_DEF_METRIC          16
#define RIP_IF_AGG_DEF_SUBNET_COUNT    0
#define RIP_INSTALL_SINK_RT            1
#define RIP_NO_INSTALL_SINK_RT         2

#define RIP_IF_AGG_NEXTHOP             0
#define RIP_FALSE                      0
#define RIP_TRUE                       1

#define RIP_AUTO_SUMMARY_ENABLE        1
#define RIP_AUTO_SUMMARY_DISABLE       2
#define RIP_AUTO_SUMMARY_STATUS(pRipCxtEntry)  pRipCxtEntry->RipGblCfg.u1AutoSummaryStatus 

#define RIP_SEND_SUMMARY              1 
#define RIP_NO_SEND_SUMMARY           2

#define RIP_MIN_METRIC(u2AggMetric, u2RtMetric) \
         (u2AggMetric >= u2RtMetric) ? u2RtMetric : u2AggMetric

#define RIP_INIT_IF_AGG_RT_DEFAULTS(pAggRt) \
                    {\
                    (pAggRt)->u2Metric       = RIP_IF_AGG_DEF_METRIC;\
                    (pAggRt)->u2Count        = RIP_IF_AGG_DEF_SUBNET_COUNT;\
                    (pAggRt)->u1SinkRtFlag   = RIP_NO_INSTALL_SINK_RT;\
                    }

/* function proto types*/



INT1 RipUpdateSinkRtInCxt PROTO ((tRipCxt *pRipCxtEntry, UINT4 u4DestNet, 
                                  UINT4 u4DestMask, UINT2 u2Metric,
                             UINT1 u1UpdateType));

INT1 RipDeleteIfAggRtAsSinkRt PROTO ((tRipIfAggRtInfo *pRipIfAggRtInfo));

INT1 RipUpdateSummaryRtAsSinkRt PROTO ((tRipRtEntry *pRipSummryRt, tRipCxt *));

INT1 RipDeleteSummaryRtAsSinkRt PROTO ((tRipRtEntry *pRipSummryRt));

VOID RipFindNumOfPktsPerSpace PROTO ((UINT2 u2IfId, UINT2 u2NumRoutesPerPkt,
                                     tRipCxt *pRipCxtEntry));

VOID RipAddAggRtToUpdateMsg PROTO ((tRipInfoBlk * pRipInfoBlk,
                                 UINT2 *pu2NoOfRoutes, tRipRtEntry *pRt,
                                    tRipCxt *pRipCxtEntry));

VOID RipDeleteSummaryRt PROTO ((tRipRtEntry *pSummryRt, tRipCxt *));

VOID RipAddRipRtToUpdateMessage PROTO ((tRipInfoBlk * pRipInfoBlk,
                                 UINT2 *pu2NoOfRoutes, tRipRtEntry *pRt));

VOID RipUpdateSummaryAggRoute PROTO ((tRipRtEntry *pSummaryRt, tRipCxt *));


INT1 RipComposeRipRtFromIfAggRt PROTO ((tRipIfAggRtInfo *pRipIfAggRtInfo,
                                 tRipRtEntry **pRipRt));




INT1 RipDeleteSummarySinkRt PROTO ((tRipRtEntry *pRipSummryRt, tRipCxt *));

INT1 RipDeleteIfAggSinkRt PROTO ((tRipIfAggRtInfo *pRipIfAggRtInfo));

INT1 RipAddIfAggRts PROTO ((UINT2 u2IfIndex, tRipInfoBlk *pRipInfoBlk,
                     UINT2 *pu2NoOfRoutes, UINT2 u2RtsToCompose,
                     UINT1 u1Version, UINT1 u1BcastFlag, UINT4 u4DestNet,
                     UINT2 u2DestUdpPort));

INT1 RipSendUpdateOverInterface PROTO ((UINT2 u2IfIndex, 
                                        UINT2 u2NoOfValidRoutes,
                                        UINT1 u1BcastFlag,
                                        tRipPkt *pRipPkt,
                                        UINT4 u4DestNet, UINT2 u2DestUdpPort,
                                        INT4 *pi4UpdStat,tRipCxt *));

INT1 RipUpdateRipPktToSend PROTO ((UINT1 , UINT2,
                                   tRipInfo *, 
                                   tRipInfoBlk *,tRipCxt *));

VOID RipAddRipRtsForSummaryEnabledTrigUpdate PROTO ((UINT4 u4Address, 
                                                     UINT4 u4Mask,
                                                     tRipInfo *pRipInfo, 
                                                     UINT2 *pu2NoOfValidRoutes,
                                                     UINT2 u2IfIndex,
                                                     UINT1 u1BcastFlag, 
                                                     tRipPkt *pRipPkt,
                                                     UINT4 u4DestNet,
                                                     UINT2 u2DestUdpPort,
                                                     INT4 *pi4UpdStat,tRipCxt *));

VOID RipUpdateAllAggRtForRtChange PROTO ((tRipRtEntry *pRt, tRipCxt *));

#endif    
    
    
    
    
    
    
    
    

    
