#####################################################################
##                                                                 ##
## $RCSfile: Makefile,v $
##                                                                 ##
## $Date: 2013/06/19 13:31:10 $                                     
##                                                                 ##
## $Revision: 1.28 $                                
##                                                                 ##
#####################################################################

#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                          ####
#####################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                      |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Srinivasan. E.S.               |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  IP RIP                         |##
##|                                                               |##
##|    MODULE NAME             ::  RIP Version 2                  |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  26-May-1996                    |##
##|                                                               |##
##|###############################################################|##
#### CHANGE RECORD ::                                            ####

include ../LR/make.h
include ../LR/make.rule
include make.h
#Compilation

INCLUDES       = ${GLOBAL_INCLUDES}

LOCAL_OPNS     = ${STUB_OPNS} ${ST_ENH_OPNS} ${GBL_OPNS}

TOTAL_OPNS     = ${LOCAL_OPNS} ${GLOBAL_OPNS}

C_FLAGS         = ${CC_FLAGS} ${CC_DEBUG_OPTIONS} ${TOTAL_OPNS} ${INCLUDES}

#Rip Dependencies List.

RIPDPNDS = ${COMMON_DEPENDENCIES} \
            ${RIP_BASE_DIR}/make.h \
            ${RIP_BASE_DIR}/Makefile \
            ${COMN_INCL_DIR}/lr.h \
            ${COMN_INCL_DIR}/ip.h \
            ${COMN_INCL_DIR}/rip.h \
            ${RIPINCD}/fsriplw.h \
            ${RIPINCD}/ripcfgs.h \
            ${RIPINCD}/ripipif.h \
            ${RIPINCD}/ripextn.h \
            ${RIPINCD}/ripifdfs.h \
            ${RIPINCD}/ripinc.h \
            ${RIPINCD}/rippddfs.h \
            ${RIPINCD}/ripport.h \
            ${RIPINCD}/ripproto.h \
            ${RIPINCD}/riprrdfs.h \
            ${RIPINCD}/ripmitdfs.h \
            ${RIPINCD}/riprtdfs.h \
            ${RIPINCD}/ripsndfs.h \
            ${RIPINCD}/riptdfs.h \
            ${RIPINCD}/riptrie.h \
            ${RIPINCD}/stdrilow.h \
            ${RIPINCD}/ripaggr.h \
            ${RIPINCD}/ripmiglob.h \
            ${RIPINCD}/ripmiproto.h \
            ${RIPINCD}/ripsz.h \
            ${RIPINCD}/ripred.h \
            ${RIPD}/Makefile

RIPOBJ = ${RIPOBJD}/FutureRIP.o

RIPOBJS = \
        ${RIPOBJD}/ripmiutl.o \
        ${RIPOBJD}/ripifutl.o \
        ${RIPOBJD}/ripipif.o \
        ${RIPOBJD}/ripmain.o \
        ${RIPOBJD}/ripprpdu.o \
        ${RIPOBJD}/ripprrt.o \
        ${RIPOBJD}/ripsncfg.o \
        ${RIPOBJD}/ripsnif.o \
        ${RIPOBJD}/riptskmg.o \
        ${RIPOBJD}/ripudpif.o \
        ${RIPOBJD}/ripmd5sn.o \
        ${RIPOBJD}/ripshasn.o \
	${RIPOBJD}/riprrd.o \
        ${RIPOBJD}/fsriplow.o \
        ${RIPOBJD}/fsripagglw.o \
        ${RIPOBJD}/ripagg.o \
	${RIPOBJD}/rrdlow.o\
	${RIPOBJD}/ripsz.o\
	${RIPOBJD}/fsmirilw.o\
	${RIPOBJD}/fsmistlw.o\
	${RIPOBJD}/ripred.o

ifeq (DRIP_TEST_WANTED, $(findstring DRIP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
RIP_TEST_BASE_DIR = ${RIP_BASE_DIR}/test
RIP_TEST_OBJ_DIR  = ${RIP_BASE_DIR}/test/obj
RIPOBJS += \
	${RIP_TEST_OBJ_DIR}/FutureRipTest.o	
endif

ifeq (${CLI}, YES)
RIPOBJS  += \
	${RIPOBJD}/ripcli.o 
endif

ifeq (${SNMP}, YES)
RIPOBJS  += \
        ${RIPOBJD}/fsripmid.o \
        ${RIPOBJD}/stdrimid.o
endif

ifeq (${SNMP_2}, YES)
RIPOBJS  += \
        ${RIPOBJD}/fsripwr.o \
        ${RIPOBJD}/stdripwr.o\
	${RIPOBJD}/fsmiriwr.o\
	${RIPOBJD}/fsmistwr.o
endif

ifeq (${TEST_RRD}, YES)
SYSTEM_COMPILATION_SWITCHES  += -DTEST_RRD_WANTED
endif

ifeq (${NPAPI}, YES)
RIPOBJS  += \
	${RIPOBJD}/ripnpapi.o 
endif

${RIPOBJ} : obj ${RIPOBJS}
	${LD} ${LD_FLAGS} -o ${RIPOBJ} ${RIPOBJS} ${CC_COMMON_OPTIONS}

obj:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) obj
endif

ifeq (DRIP_TEST_WANTED, $(findstring DRIP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
${RIP_TEST_OBJ_DIR}/FutureRipTest.o: FORCE
	${MAKE} -C ${RIP_TEST_BASE_DIR} -f Makefile
endif

FORCE:

${RIPOBJD}/ripmiutl.o: ${RIPSRCD}/ripmiutl.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripmiutl.c -o $@

${RIPOBJD}/ripifutl.o: ${RIPSRCD}/ripifutl.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripifutl.c -o $@

${RIPOBJD}/ripipif.o: ${RIPSRCD}/ripipif.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripipif.c -o $@

${RIPOBJD}/ripmain.o: ${RIPSRCD}/ripmain.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripmain.c -o $@

${RIPOBJD}/ripprpdu.o: ${RIPSRCD}/ripprpdu.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripprpdu.c -o $@

${RIPOBJD}/ripprrt.o: ${RIPSRCD}/ripprrt.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripprrt.c -o $@

${RIPOBJD}/ripsncfg.o: ${RIPSRCD}/ripsncfg.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripsncfg.c -o $@

${RIPOBJD}/ripsnif.o: ${RIPSRCD}/ripsnif.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripsnif.c -o $@

${RIPOBJD}/riptskmg.o: ${RIPSRCD}/riptskmg.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/riptskmg.c -o $@

${RIPOBJD}/ripudpif.o: ${RIPSRCD}/ripudpif.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripudpif.c -o $@

${RIPOBJD}/ripmd5sn.o: ${RIPSRCD}/ripmd5sn.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripmd5sn.c -o $@

${RIPOBJD}/ripshasn.o: ${RIPSRCD}/ripshasn.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripshasn.c -o $@

${RIPOBJD}/riprrd.o: ${RIPSRCD}/riprrd.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/riprrd.c -o $@

${RIPOBJD}/rrdlow.o: ${RIPSRCD}/rrdlow.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/rrdlow.c -o $@

${RIPOBJD}/fsripwr.o: ${RIPSRCD}/fsripwr.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsripwr.c -o $@

${RIPOBJD}/stdripwr.o: ${RIPSRCD}/stdripwr.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/stdripwr.c -o $@

${RIPOBJD}/fsriplow.o: ${RIPSRCD}/fsriplow.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsriplow.c -o $@

${RIPOBJD}/fsripagglw.o: ${RIPSRCD}/fsripagglw.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsripagglw.c -o $@

${RIPOBJD}/ripcli.o: ${RIPSRCD}/ripcli.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripcli.c -o $@

${RIPOBJD}/ripfscli.o: ${RIPSRCD}/ripfscli.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripfscli.c -o $@

${RIPOBJD}/ripagg.o: ${RIPSRCD}/ripagg.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripagg.c -o $@
${RIPOBJD}/fsmirilw.o: ${RIPSRCD}/fsmirilw.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsmirilw.c -o $@
${RIPOBJD}/fsmistlw.o: ${RIPSRCD}/fsmistlw.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsmistlw.c -o $@
${RIPOBJD}/fsmiriwr.o: ${RIPSRCD}/fsmiriwr.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsmiriwr.c -o $@
${RIPOBJD}/fsmistwr.o: ${RIPSRCD}/fsmistwr.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/fsmistwr.c -o $@
${RIPOBJD}/ripsz.o: ${RIPSRCD}/ripsz.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripsz.c -o $@
${RIPOBJD}/ripnpapi.o: ${RIPSRCD}/ripnpapi.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripnpapi.c -o $@
${RIPOBJD}/ripred.o: ${RIPSRCD}/ripred.c \
        ${RIPDPNDS}
	${CC} ${C_FLAGS} ${RIPSRCD}/ripred.c -o $@
clean  :
	$(RM) $(RM_FLAGS) ${RIPOBJS} ${RIPOBJ}
ifeq (DRIP_TEST_WANTED, $(findstring DRIP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
	make clean -C ${RIP_TEST_BASE_DIR} -f Makefile
endif

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/rip.tgz -T ${BASE_DIR}/rip/FILES.NEW;\
        cd ${CUR_PWD};

