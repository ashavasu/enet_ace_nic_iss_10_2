#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :                                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 19th October 1999                             |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Cleanall option                            |
# |                            3. (Sample) Packaging (eg. make review)       |
# |                            4. Project/Module Backup (eg. daily, weekly)  |
# |                            5. Project FTP options (internal releases,    |
# |                               external releases if possible)             |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################
# This is used to specify the compiler option
COMPILER_TYPE = -DANSI -DUNIX

GLOBAL_OPNS = ${TRACE_OPNS} ${DEBUG_OPNS} ${PROTOCOL_OPNS} \
              ${CONFIG_OPNS} ${DUMP_OPNS} ${COMPILER_TYPE} \
              ${PROT_TASK_OPNS} \
              ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}


############################################################################
#                         Directories                                      #
############################################################################

RIP_BASE_DIR      = ${BASE_DIR}/rip
RIPD      = ${RIP_BASE_DIR}/
RIPINCD   = ${RIP_BASE_DIR}/inc
RIPSRCD   = ${RIP_BASE_DIR}/src
RIPOBJD   = ${RIP_BASE_DIR}/obj

TRIEINCD  = ${BASE_DIR}/util/trie
############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################


RIP_GLOBAL_INCLUDES  = -I${RIPINCD} -I${TRIEINCD} 

GLOBAL_INCLUDES = $(RIP_GLOBAL_INCLUDES) \
                    $(COMMON_INCLUDE_DIRS)
#############################################################################

TEST_RRD         = YES
