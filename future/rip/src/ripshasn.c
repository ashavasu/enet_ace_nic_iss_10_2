
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripshasn.c,v 1.9 2016/06/18 11:46:30 siva Exp $
 *
 * Description:Contains low level routines for SHA authen-  
 *             cation in RIP.                              
 *
 *******************************************************************/

#include "ripinc.h"
#include "ripcli.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"

/* LOW LEVEL Routines for Table : FsRipCryptoAuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRipCryptoAuthTable
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsRipCryptoAuthTable
    (INT4 i4FsRipCryptoAuthIfIndex,
     UINT4 u4FsRipCryptoAuthAddress, INT4 i4FsRipCryptoAuthKeyId)
{
    tCryptoAuthKeyInfo *pNodeInfo = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }
    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList),
                  pNodeInfo, tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNodeInfo) == i4FsRipCryptoAuthKeyId)
        {
            /* this is a valid entry */
            return SNMP_SUCCESS;
        }
    }
    /* Entry is not found - Invalid index */

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRipCryptoAuthTable
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsRipCryptoAuthTable
    (INT4 *pi4FsRipCryptoAuthIfIndex,
     UINT4 *pu4FsRipCryptoAuthAddress, INT4 *pi4FsRipCryptoAuthKeyId)
{
    tCryptoAuthKeyInfo *pNodeInfo = NULL;
    UINT4               u4IfAddr = 0;
    UINT4               u4IfAddrNext = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IfIndexNext = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipCryptoRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetFirstIndexRip2IfConfTable (&u4IfAddr) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n", u4IfAddr);
        return SNMP_FAILURE;
    }

    RipGetIfIndexFromPort (pRipIfRec->IfaceId.u4IfIndex, &u4IfIndex);

    pNodeInfo = (tCryptoAuthKeyInfo *)
        RIP_SLL_First (&(pRipIfRec->RipCryptoAuthKeyList));
    if (pNodeInfo != NULL)
    {
        *pi4FsRipCryptoAuthIfIndex = (INT4) u4IfIndex;
        *pu4FsRipCryptoAuthAddress = u4IfAddr;
        *pi4FsRipCryptoAuthKeyId = (INT4) RIP_CRYPTO_AUTH_KEY_ID (pNodeInfo);
        return SNMP_SUCCESS;
    }
    else
    {
        while (nmhGetNextIndexRip2IfConfTable (u4IfAddr, &u4IfAddrNext) !=
               SNMP_FAILURE)
        {
            pRipCryptoRec = RipGetIfRecFromAddr (u4IfAddrNext, pRipCxtEntry);
            if (pRipCryptoRec == NULL)
            {
                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              RIP_TRC_CXT_FLAG,
                              RIP_MGMT_CXT_ID,
                              MGMT_TRC, RIP_NAME,
                              "Invalid address %x \n", u4IfAddrNext);
                return SNMP_FAILURE;
            }

            RipGetIfIndexFromPort (pRipCryptoRec->IfaceId.u4IfIndex,
                                   &u4IfIndexNext);
            pNodeInfo = (tCryptoAuthKeyInfo *)
                RIP_SLL_First (&(pRipCryptoRec->RipCryptoAuthKeyList));
            if (pNodeInfo != NULL)
            {
                *pi4FsRipCryptoAuthIfIndex = (INT4) u4IfIndexNext;
                *pu4FsRipCryptoAuthAddress = u4IfAddrNext;
                *pi4FsRipCryptoAuthKeyId =
                    (INT4) RIP_CRYPTO_AUTH_KEY_ID (pNodeInfo);
                return SNMP_SUCCESS;
            }
            else
            {
                u4IfAddr = u4IfAddrNext;
            }
        }

        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRipCryptoAuthTable
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                nextFsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                nextFsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId
                nextFsRipCryptoAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsRipCryptoAuthTable
    (INT4 i4FsRipCryptoAuthIfIndex,
     INT4 *pi4NextFsRipCryptoAuthIfIndex,
     UINT4 u4FsRipCryptoAuthAddress,
     UINT4 *pu4NextFsRipCryptoAuthAddress,
     INT4 i4FsRipCryptoAuthKeyId, INT4 *pi4NextFsRipCryptoAuthKeyId)
{
    tCryptoAuthKeyInfo *pNodeInfo = NULL;
    tCryptoAuthKeyInfo *pNodeInfoCur = NULL;
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipIfCryptoRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4IfAddrNext = 0;
    UINT4               u4IfAddr = 0;
    UINT4               u4IfIndex = (UINT4) i4FsRipCryptoAuthIfIndex;
    UINT4               u4IfIndexNext = 0;
    UINT4               u4Port = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* If the first index for the table is given as 0, then
     * return the first entry of the table */
    if (i4FsRipCryptoAuthIfIndex == 0)
    {
        if (nmhGetFirstIndexFsRipCryptoAuthTable
            (pi4NextFsRipCryptoAuthIfIndex,
             pu4NextFsRipCryptoAuthAddress,
             pi4NextFsRipCryptoAuthKeyId) != SNMP_FAILURE)
        {
            return SNMP_SUCCESS;
        }
    }

    /*we will first find if there is any next node for the current index
       if there is any key present then we will return the next node else
       we will return the first key for the next interface */

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

     u4IfAddr =  u4FsRipCryptoAuthAddress;
     pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n", u4IfAddr);
        return SNMP_FAILURE;
    }

    if (pRipIfRec != NULL)
    {
        pNodeInfoCur =
            (tCryptoAuthKeyInfo *) RIP_SLL_First
            (&(pRipIfRec->RipCryptoAuthKeyList));
        if (pNodeInfoCur != NULL)
        {
            RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                          tCryptoAuthKeyInfo *)
            {
                if (RIP_CRYPTO_AUTH_KEY_ID (pNode) > i4FsRipCryptoAuthKeyId)
                {
                    if (pNode != NULL)
                    {
                        *pi4NextFsRipCryptoAuthIfIndex = (INT4) u4IfIndex;
                        *pu4NextFsRipCryptoAuthAddress = u4IfAddr;
                        *pi4NextFsRipCryptoAuthKeyId =
                            (INT4) RIP_CRYPTO_AUTH_KEY_ID (pNode);
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }

    /* For Unnumbered interface Ifindex is given */
    if (u4IfAddr == RIP_ZERO)
    {
        u4IfAddr = u4IfIndex;
    }

    while (nmhGetNextIndexRip2IfConfTable (u4IfAddr, &u4IfAddrNext) !=
           SNMP_FAILURE)
    {
        pRipIfCryptoRec = RipGetIfRecFromAddr (u4IfAddrNext, pRipCxtEntry);
        if (pRipIfCryptoRec == NULL)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          RIP_TRC_CXT_FLAG,
                          RIP_MGMT_CXT_ID,
                          MGMT_TRC, RIP_NAME,
                          "Invalid address %x \n", u4IfAddrNext);
            return SNMP_FAILURE;
        }

        RipGetIfIndexFromPort (pRipIfCryptoRec->IfaceId.u4IfIndex,
                               &u4IfIndexNext);
        pNodeInfo =
            (tCryptoAuthKeyInfo *) RIP_SLL_First
            (&(pRipIfCryptoRec->RipCryptoAuthKeyList));

        if (pNodeInfo != NULL)
        {
            *pi4NextFsRipCryptoAuthIfIndex = (INT4) u4IfIndexNext;
            *pu4NextFsRipCryptoAuthAddress = u4IfAddrNext;
            *pi4NextFsRipCryptoAuthKeyId =
                (INT4) RIP_CRYPTO_AUTH_KEY_ID (pNodeInfo);
            return SNMP_SUCCESS;
        }
        else
        {
            u4IfAddr = u4IfAddrNext;
        }
    }

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRipCryptoAuthKey
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                retValFsRipCryptoAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipCryptoAuthKey (INT4 i4FsRipCryptoAuthIfIndex,
                          UINT4 u4FsRipCryptoAuthAddress,
                          INT4 i4FsRipCryptoAuthKeyId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsRipCryptoAuthKey)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               au1AuthKey[MAX_AUTH_KEY_LENGTH];

    MEMSET (au1AuthKey, 0, MAX_AUTH_KEY_LENGTH);

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            if ((MsrGetSaveStatus () == ISS_TRUE) 
#ifdef RM_WANTED
                ||
                (gu1RmStConfReq == RM_TRUE)
#endif
                )
            {
                MEMCPY (pRetValFsRipCryptoAuthKey->pu1_OctetList,
                        RIP_CRYPTO_AUTH_KEY (pNode),
                        RIP_CRYPTO_AUTH_KEY_LEN (pNode));
                pRetValFsRipCryptoAuthKey->i4_Length =
                    RIP_CRYPTO_AUTH_KEY_LEN (pNode);
            }
            else
            {
                MEMCPY (au1AuthKey, "****************", MAX_AUTH_KEY_LENGTH);
                au1AuthKey[MAX_AUTH_KEY_LENGTH - 1] = '\0';
                /* Prevention of Auth key from display */
                MEMCPY (pRetValFsRipCryptoAuthKey->pu1_OctetList,
                        au1AuthKey, MAX_AUTH_KEY_LENGTH);
                pRetValFsRipCryptoAuthKey->i4_Length =
                    (INT4) STRLEN (au1AuthKey);
            }
            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRipCryptoKeyStartAccept
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                retValFsRipCryptoKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipCryptoKeyStartAccept (INT4 i4FsRipCryptoAuthIfIndex,
                                 UINT4 u4FsRipCryptoAuthAddress,
                                 INT4 i4FsRipCryptoAuthKeyId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsRipCryptoKeyStartAccept)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               au1StartAcceptTime[RIP_DST_TIME_LEN];

    MEMSET (au1StartAcceptTime, 0, sizeof (au1StartAcceptTime));

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilGetKeyTime (RIP_CRYPTO_START_ACCEPT_TIME (pNode),
                               au1StartAcceptTime);
            MEMCPY (pRetValFsRipCryptoKeyStartAccept->pu1_OctetList,
                    &au1StartAcceptTime, STRLEN (au1StartAcceptTime));
            pRetValFsRipCryptoKeyStartAccept->i4_Length =
                (INT4) STRLEN (au1StartAcceptTime);
            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRipCryptoKeyStartGenerate
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                retValFsRipCryptoKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipCryptoKeyStartGenerate (INT4 i4FsRipCryptoAuthIfIndex,
                                   UINT4 u4FsRipCryptoAuthAddress,
                                   INT4 i4FsRipCryptoAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsRipCryptoKeyStartGenerate)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               au1StartGenerateTime[RIP_DST_TIME_LEN];

    MEMSET (au1StartGenerateTime, 0, sizeof (au1StartGenerateTime));

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilGetKeyTime (RIP_CRYPTO_START_GEN_TIME (pNode),
                               au1StartGenerateTime);
            MEMCPY (pRetValFsRipCryptoKeyStartGenerate->pu1_OctetList,
                    &au1StartGenerateTime, STRLEN (au1StartGenerateTime));
            pRetValFsRipCryptoKeyStartGenerate->i4_Length =
                (INT4) STRLEN (au1StartGenerateTime);
            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRipCryptoKeyStopGenerate
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                retValFsRipCryptoKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipCryptoKeyStopGenerate (INT4 i4FsRipCryptoAuthIfIndex,
                                  UINT4 u4FsRipCryptoAuthAddress,
                                  INT4 i4FsRipCryptoAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsRipCryptoKeyStopGenerate)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               au1StopGenerateTime[RIP_DST_TIME_LEN];

    MEMSET (au1StopGenerateTime, 0, sizeof (au1StopGenerateTime));

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilGetKeyTime (RIP_CRYPTO_STOP_GEN_TIME (pNode),
                               au1StopGenerateTime);
            MEMCPY (pRetValFsRipCryptoKeyStopGenerate->pu1_OctetList,
                    &au1StopGenerateTime, STRLEN (au1StopGenerateTime));
            pRetValFsRipCryptoKeyStopGenerate->i4_Length =
                (INT4) STRLEN (au1StopGenerateTime);
            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRipCryptoKeyStopAccept
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                retValFsRipCryptoKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipCryptoKeyStopAccept (INT4 i4FsRipCryptoAuthIfIndex,
                                UINT4 u4FsRipCryptoAuthAddress,
                                INT4 i4FsRipCryptoAuthKeyId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsRipCryptoKeyStopAccept)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               au1StopAcceptTime[RIP_DST_TIME_LEN];

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilGetKeyTime (RIP_CRYPTO_STOP_ACCEPT_TIME (pNode),
                               au1StopAcceptTime);
            MEMCPY (pRetValFsRipCryptoKeyStopAccept->pu1_OctetList,
                    &au1StopAcceptTime, STRLEN (au1StopAcceptTime));
            pRetValFsRipCryptoKeyStopAccept->i4_Length =
                (INT4) STRLEN (au1StopAcceptTime);
            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRipCryptoKeyStatus
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                retValFsRipCryptoKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipCryptoKeyStatus (INT4 i4FsRipCryptoAuthIfIndex,
                            UINT4 u4FsRipCryptoAuthAddress,
                            INT4 i4FsRipCryptoAuthKeyId,
                            INT4 *pi4RetValFsRipCryptoKeyStatus)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            *pi4RetValFsRipCryptoKeyStatus = (INT4) RIP_CRYPTO_STATUS (pNode);
            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipCryptoAuthKeyId);

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRipCryptoAuthKey
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                setValFsRipCryptoAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipCryptoAuthKey (INT4 i4FsRipCryptoAuthIfIndex,
                          UINT4 u4FsRipCryptoAuthAddress,
                          INT4 i4FsRipCryptoAuthKeyId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsRipCryptoAuthKey)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    UINT4               u4StartTime = 0;
    tUtlTm              tm;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1OctetList[RIP_DST_TIME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE  ValFsRipCryptoKeyAccept;
    tSNMP_OCTET_STRING_TYPE *pValFsRipCryptoKeyAccept = NULL;
#endif
    UINT4               u4SeqNum = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

#ifdef SNMP_3_WANTED
    MEMSET (&au1OctetList, 0, (RIP_DST_TIME_LEN + 1));
    MEMSET (&ValFsRipCryptoKeyAccept, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    pValFsRipCryptoKeyAccept = &ValFsRipCryptoKeyAccept;
    pValFsRipCryptoKeyAccept->pu1_OctetList = &au1OctetList[0];
#endif

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }


   pRipIfRec = RipGetIfRecFromAddr (u4FsRipCryptoAuthAddress, pRipCxtEntry);

   if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipCryptoAuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            u1NodeFound = RIP_NODE_FOUND;
            break;
        }
    }

    /* Node is not present */
    if (u1NodeFound == RIP_NODE_NOT_FOUND)
    {
        RIP_ALLOC_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pNode);
        if (pNode == NULL)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          RIP_TRC_CXT_FLAG,
                          RIP_MGMT_CXT_ID,
                          MGMT_TRC, RIP_NAME, "Memory Alloc failure for %d\n",
                          i4FsRipCryptoAuthKeyId);

            return SNMP_FAILURE;
        }
        /* Getting the system time for u4StartTime */
        UtlGetTime (&tm);
        u4StartTime = RipUtilGetSecondsSinceBase (tm);

        RIP_CRYPTO_AUTH_KEY_ID (pNode) = (UINT1) i4FsRipCryptoAuthKeyId;
        RIP_CRYPTO_SEQUENCE_NO (pNode) = 0;
        RIP_CRYPTO_START_GEN_TIME (pNode) = u4StartTime;
        RIP_CRYPTO_START_ACCEPT_TIME (pNode) = u4StartTime;
        RIP_CRYPTO_STOP_GEN_TIME (pNode) = (UINT4) -1;    /* infinite time */
        RIP_CRYPTO_STOP_ACCEPT_TIME (pNode) = (UINT4) -1;    /* infinite time */
        RIP_CRYPTO_STATUS (pNode) = RIP_AUTHKEY_STATUS_VALID;

        MEMSET (RIP_CRYPTO_AUTH_KEY (pNode), 0, MAX_AUTH_KEY_LENGTH);
        RipSortInsertAuthKey (&(pRipIfRec->RipCryptoAuthKeyList), pNode);
    }

    MEMSET (RIP_CRYPTO_AUTH_KEY (pNode), 0, MAX_AUTH_KEY_LENGTH);
    MEMCPY (RIP_CRYPTO_AUTH_KEY (pNode),
            pSetValFsRipCryptoAuthKey->pu1_OctetList,
            MEM_MAX_BYTES (pSetValFsRipCryptoAuthKey->i4_Length,
                           MAX_AUTH_KEY_LENGTH));
    RIP_CRYPTO_AUTH_KEY_LEN (pNode) =
        (UINT1) (MEM_MAX_BYTES (pSetValFsRipCryptoAuthKey->i4_Length,
                                MAX_AUTH_KEY_LENGTH));

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoAuthKey;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipCryptoAuthKey) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 4;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                      pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                      u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                      pSetValFsRipCryptoAuthKey));

    /* To sync StartAccept Time */
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    RipUtilGetKeyTime (RIP_CRYPTO_START_ACCEPT_TIME (pNode),
                                               au1OctetList);

    pValFsRipCryptoKeyAccept->i4_Length = (INT4) STRLEN (au1OctetList);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStartAccept;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipCryptoKeyStartAccept) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 4;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                      pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                      u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                      pValFsRipCryptoKeyAccept));


    /* To synce StartGenerate Time */
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (&au1OctetList, 0, (RIP_DST_TIME_LEN + 1));

    RipUtilGetKeyTime (RIP_CRYPTO_START_GEN_TIME (pNode),
                                            au1OctetList);

    pValFsRipCryptoKeyAccept->i4_Length = (INT4) STRLEN (au1OctetList);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStartGenerate;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipCryptoKeyStartGenerate) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 4;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                      pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                      u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                      pValFsRipCryptoKeyAccept));
#endif
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    KW_FALSEPOSITIVE_FIX (pNode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRipCryptoKeyStartAccept
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                setValFsRipCryptoKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipCryptoKeyStartAccept (INT4 i4FsRipCryptoAuthIfIndex,
                                 UINT4 u4FsRipCryptoAuthAddress,
                                 INT4 i4FsRipCryptoAuthKeyId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsRipCryptoKeyStartAccept)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT4               u4StartAccept = 0;
    tUtlTm              tm;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilConvertTime
                (pSetValFsRipCryptoKeyStartAccept->pu1_OctetList, &tm);
            u4StartAccept = RipUtilGetSecondsSinceBase (tm);
            RIP_CRYPTO_START_ACCEPT_TIME (pNode) = u4StartAccept;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStartAccept;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipCryptoKeyStartAccept) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                              pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                              u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                              pSetValFsRipCryptoKeyStartAccept));
#endif
            return SNMP_SUCCESS;
        }
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRipCryptoKeyStartGenerate
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                setValFsRipCryptoKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipCryptoKeyStartGenerate (INT4 i4FsRipCryptoAuthIfIndex,
                                   UINT4 u4FsRipCryptoAuthAddress,
                                   INT4 i4FsRipCryptoAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsRipCryptoKeyStartGenerate)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT4               u4StartGenerate = 0;
    tUtlTm              tm;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilConvertTime
                (pSetValFsRipCryptoKeyStartGenerate->pu1_OctetList, &tm);
            u4StartGenerate = RipUtilGetSecondsSinceBase (tm);
            RIP_CRYPTO_START_GEN_TIME (pNode) = u4StartGenerate;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStartGenerate;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipCryptoKeyStartGenerate) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                              pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                              u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                              pSetValFsRipCryptoKeyStartGenerate));
#endif
            return SNMP_SUCCESS;
        }
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRipCryptoKeyStopGenerate
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                setValFsRipCryptoKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipCryptoKeyStopGenerate (INT4 i4FsRipCryptoAuthIfIndex,
                                  UINT4 u4FsRipCryptoAuthAddress,
                                  INT4 i4FsRipCryptoAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsRipCryptoKeyStopGenerate)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT4               u4StopGenerate = 0;
    tUtlTm              tm;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilConvertTime
                (pSetValFsRipCryptoKeyStopGenerate->pu1_OctetList, &tm);
            u4StopGenerate = RipUtilGetSecondsSinceBase (tm);
            RIP_CRYPTO_STOP_GEN_TIME (pNode) = u4StopGenerate;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStopGenerate;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipCryptoKeyStopGenerate) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                              pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                              u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                              pSetValFsRipCryptoKeyStopGenerate));
#endif
            return SNMP_SUCCESS;
        }
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRipCryptoKeyStopAccept
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                setValFsRipCryptoKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipCryptoKeyStopAccept (INT4 i4FsRipCryptoAuthIfIndex,
                                UINT4 u4FsRipCryptoAuthAddress,
                                INT4 i4FsRipCryptoAuthKeyId,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsRipCryptoKeyStopAccept)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT4               u4StopAccept = 0;
    tUtlTm              tm;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            RipUtilConvertTime
                (pSetValFsRipCryptoKeyStopAccept->pu1_OctetList, &tm);
            u4StopAccept = RipUtilGetSecondsSinceBase (tm);
            RIP_CRYPTO_STOP_ACCEPT_TIME (pNode) = u4StopAccept;

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStopAccept;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipCryptoKeyStopAccept) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %s",
                              pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                              u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                              pSetValFsRipCryptoKeyStopAccept));
#endif
            return SNMP_SUCCESS;
        }
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRipCryptoKeyStatus
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                setValFsRipCryptoKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipCryptoKeyStatus (INT4 i4FsRipCryptoAuthIfIndex,
                            UINT4 u4FsRipCryptoAuthAddress,
                            INT4 i4FsRipCryptoAuthKeyId,
                            INT4 i4SetValFsRipCryptoKeyStatus)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipCryptoAuthAddress, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipCryptoAuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            switch (i4SetValFsRipCryptoKeyStatus)
            {
                case RIP_AUTHKEY_STATUS_VALID:
                    RIP_CRYPTO_STATUS (pNode) =
                        (UINT1) i4SetValFsRipCryptoKeyStatus;
                    break;

                case RIP_AUTHKEY_STATUS_DELETE:
                    RIP_SLL_Delete (&(pRipIfRec->RipCryptoAuthKeyList),
                                    &(pNode->NextNode));
                    RIP_FREE_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pNode);
                    break;

                default:
                    break;
            }
            break;
        }
    }
    if (RIP_SLL_Count (&(pRipIfRec->RipCryptoAuthKeyList)) == 0)
    {
        /* If no keys are present interface auth type is set as No Auth */
        nmhSetRip2IfConfAuthType (pRipIfRec->u4Addr, RIPIF_NO_AUTHENTICATION);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipCryptoKeyStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipCryptoKeyStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 4;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %i %i",
                      pRipCxtEntry->i4CxtId, i4FsRipCryptoAuthIfIndex,
                      u4FsRipCryptoAuthAddress, i4FsRipCryptoAuthKeyId,
                      i4SetValFsRipCryptoKeyStatus));
#endif
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRipCryptoAuthKey
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                testValFsRipCryptoAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipCryptoAuthKey (UINT4 *pu4ErrorCode,
                             INT4 i4FsRipCryptoAuthIfIndex,
                             UINT4 u4FsRipCryptoAuthAddress,
                             INT4 i4FsRipCryptoAuthKeyId,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsRipCryptoAuthKey)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT1               au1Key[MAX_AUTH_KEY_LENGTH];
    UINT4               u4Port = 0;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return SNMP_FAILURE;
    }

    MEMSET (au1Key, 0, MAX_AUTH_KEY_LENGTH);
    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (pRipIfRec->RipIfaceCfg.u2AuthType < RIPIF_MD5_AUTHENTICATION)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_WRONG_VALUE);
        return SNMP_FAILURE;
    }

    if ((pTestValFsRipCryptoAuthKey->i4_Length < 0) ||
        (pTestValFsRipCryptoAuthKey->i4_Length > MAX_AUTH_KEY_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            break;
        }
    }

    if (!MEMCMP (au1Key, pTestValFsRipCryptoAuthKey->pu1_OctetList,
                 MEM_MAX_BYTES (sizeof (au1Key),
                                (UINT4) pTestValFsRipCryptoAuthKey->i4_Length)))
    {
        /* Configured key should not be zero */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipCryptoKeyStartAccept
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                testValFsRipCryptoKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipCryptoKeyStartAccept (UINT4 *pu4ErrorCode,
                                    INT4 i4FsRipCryptoAuthIfIndex,
                                    UINT4 u4FsRipCryptoAuthAddress,
                                    INT4 i4FsRipCryptoAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsRipCryptoKeyStartAccept)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tCryptoAuthKeyInfo *pPrevNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    tUtlTm              tm;
    INT4                i4CheckVal = 0;
    UINT4               u4StartTime = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            u1NodeFound = RIP_NODE_FOUND;
            break;
        }
        pPrevNode = pNode;
    }

    if (u1NodeFound != RIP_NODE_FOUND)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    if (pTestValFsRipCryptoKeyStartAccept->i4_Length >= RIP_DST_TIME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    i4CheckVal = RipUtilConvertTime
        (pTestValFsRipCryptoKeyStartAccept->pu1_OctetList, &tm);
    if (i4CheckVal != RIP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    u4StartTime = RipUtilGetSecondsSinceBase (tm);

    if (pPrevNode != NULL)
    {
        if (u4StartTime > RIP_CRYPTO_STOP_ACCEPT_TIME (pPrevNode))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
            return SNMP_FAILURE;
        }
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipCryptoKeyStartGenerate
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                testValFsRipCryptoKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipCryptoKeyStartGenerate (UINT4 *pu4ErrorCode,
                                      INT4 i4FsRipCryptoAuthIfIndex,
                                      UINT4 u4FsRipCryptoAuthAddress,
                                      INT4 i4FsRipCryptoAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsRipCryptoKeyStartGenerate)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tCryptoAuthKeyInfo *pPrevNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    tUtlTm              tm;
    INT4                i4CheckVal = 0;
    UINT4               u4StartTime = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            u1NodeFound = RIP_NODE_FOUND;
            break;
        }
        pPrevNode = pNode;
    }

    if (u1NodeFound != RIP_NODE_FOUND)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    if (pTestValFsRipCryptoKeyStartGenerate->i4_Length >= RIP_DST_TIME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    i4CheckVal = RipUtilConvertTime
        (pTestValFsRipCryptoKeyStartGenerate->pu1_OctetList, &tm);
    if (i4CheckVal != RIP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    u4StartTime = RipUtilGetSecondsSinceBase (tm);

    if (pPrevNode != NULL)
    {
        if (u4StartTime > RIP_CRYPTO_STOP_GEN_TIME (pPrevNode))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
            return SNMP_FAILURE;
        }
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipCryptoKeyStopGenerate
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                testValFsRipCryptoKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipCryptoKeyStopGenerate (UINT4 *pu4ErrorCode,
                                     INT4 i4FsRipCryptoAuthIfIndex,
                                     UINT4 u4FsRipCryptoAuthAddress,
                                     INT4 i4FsRipCryptoAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsRipCryptoKeyStopGenerate)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    tUtlTm              tm;
    INT4                i4CheckVal = 0;
    UINT4               u4StopTime = 0;
    UINT4               u4CurrentTime = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            u1NodeFound = RIP_NODE_FOUND;
            break;
        }
    }

    if (u1NodeFound != RIP_NODE_FOUND)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    if (pTestValFsRipCryptoKeyStopGenerate->i4_Length >= RIP_DST_TIME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    i4CheckVal = RipUtilConvertTime
        (pTestValFsRipCryptoKeyStopGenerate->pu1_OctetList, &tm);
    if (i4CheckVal != RIP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    u4StopTime = RipUtilGetSecondsSinceBase (tm);

    MEMSET (&tm, 0, sizeof (tUtlTm));
    /* Getting the system time for u4CurrentTime */
    UtlGetTime (&tm);
    u4CurrentTime = RipUtilGetSecondsSinceBase (tm);

    if (u4StopTime < u4CurrentTime)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    if (u4StopTime < RIP_CRYPTO_START_GEN_TIME (pNode))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipCryptoKeyStopAccept
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                testValFsRipCryptoKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipCryptoKeyStopAccept (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRipCryptoAuthIfIndex,
                                   UINT4 u4FsRipCryptoAuthAddress,
                                   INT4 i4FsRipCryptoAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsRipCryptoKeyStopAccept)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    tUtlTm              tm;
    INT4                i4CheckVal = 0;
    UINT4               u4StopTime = 0;
    UINT4               u4CurrentTime = 0;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRec (u4Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid interface index %d \n",
                      i4FsRipCryptoAuthIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            u1NodeFound = RIP_NODE_FOUND;
            break;
        }
    }

    if (u1NodeFound != RIP_NODE_FOUND)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    if (pTestValFsRipCryptoKeyStopAccept->i4_Length >= RIP_DST_TIME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    i4CheckVal = RipUtilConvertTime
        (pTestValFsRipCryptoKeyStopAccept->pu1_OctetList, &tm);
    if (i4CheckVal != RIP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    u4StopTime = RipUtilGetSecondsSinceBase (tm);

    MEMSET (&tm, 0, sizeof (tUtlTm));
    /* Getting the system time for u4CurrentTime */
    UtlGetTime (&tm);
    u4CurrentTime = RipUtilGetSecondsSinceBase (tm);

    if (u4StopTime < u4CurrentTime)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }

    if (u4StopTime < RIP_CRYPTO_START_ACCEPT_TIME (pNode))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipCryptoKeyStatus
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId

                The Object 
                testValFsRipCryptoKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipCryptoKeyStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsRipCryptoAuthIfIndex,
                               UINT4 u4FsRipCryptoAuthAddress,
                               INT4 i4FsRipCryptoAuthKeyId,
                               INT4 i4TestValFsRipCryptoKeyStatus)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Port = 0;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (RipGetPortFromIfIndex (i4FsRipCryptoAuthIfIndex,
                               &u4Port) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipCryptoAuthAddress, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipCryptoAuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == i4FsRipCryptoAuthKeyId)
        {
            u1NodeFound = RIP_NODE_FOUND;
            break;
        }
    }

    if (u1NodeFound != RIP_NODE_FOUND)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRipCryptoKeyStatus != RIP_AUTHKEY_STATUS_VALID) &&
        (i4TestValFsRipCryptoKeyStatus != RIP_AUTHKEY_STATUS_DELETE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4FsRipCryptoAuthAddress);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRipCryptoAuthTable
 Input       :  The Indices
                FsRipCryptoAuthIfIndex
                FsRipCryptoAuthAddress
                FsRipCryptoAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipCryptoAuthTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RipCryptoGetKeyToUseForSend
 Description :  Gets the key in use for the given interface index.
 Input       :  u2IfId - Inderface index for which the key in use should be 
                found.
                
 Output      :  None.
 Returns     :  pAuthInfo or NULL
****************************************************************************/
tCryptoAuthKeyInfo *
RipCryptoGetKeyToUseForSend (UINT2 u2IfId, tRipCxt * pRipCxtEntry)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tCryptoAuthKeyInfo *pExpiredNode = NULL;
    tCryptoAuthKeyInfo *pKeyTouse = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipTrapInfo        RipTrapInfo;
    tRipCxt            *pRipCxt = NULL;
    UINT4               u4newDiff = 0;
    UINT4               u4KeyStartGenDiff = (UINT4) -1;
    UINT4               u4CurrentTime = 0;
    tUtlTm              tm;

    MEMSET (&RipTrapInfo, 0, sizeof (tRipTrapInfo));
    pRipIfRec = RipGetIfRec ((UINT4) u2IfId, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return NULL;
    }

    /* Getting the current system time */
    UtlGetTime (&tm);
    u4CurrentTime = RipUtilGetSecondsSinceBase (tm);

    /* Scan through the entries for this interface */
    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        /* Scan for a active key */
        if (RIP_CRYPTO_STATUS (pNode) == RIP_AUTHKEY_STATUS_VALID)
        {
            if (RIP_CRYPTO_START_GEN_TIME (pNode) <= u4CurrentTime)
            {
                /* If atlease one active key whose starttime is less than 
                   system time is present, then expired keys can be deleted */
                if (pExpiredNode != NULL)
                {
                    TMO_SLL_Delete (&(pRipIfRec->RipCryptoAuthKeyList),
                                    &(pExpiredNode->NextNode));
                    RIP_FREE_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pExpiredNode);
                }
                if ((RIP_CRYPTO_STOP_GEN_TIME (pNode) < u4CurrentTime) &&
                    (RIP_CRYPTO_STOP_ACCEPT_TIME (pNode) < u4CurrentTime))
                {
                    pExpiredNode = pNode;
                }
                else if ((RIP_CRYPTO_STOP_GEN_TIME (pNode) == 0) ||
                         (RIP_CRYPTO_STOP_GEN_TIME (pNode) > u4CurrentTime))
                {
                    /* If more than one unexpired key is available then use
                       most recent key having start time maximum */

                    u4newDiff =
                        u4CurrentTime - RIP_CRYPTO_START_GEN_TIME (pNode);
                    if (u4newDiff < u4KeyStartGenDiff)
                    {
                        u4KeyStartGenDiff = u4newDiff;
                        pKeyTouse = pNode;
                    }
                }
            }
        }
    }

    /* According to RFC 2082, sec 4.3, if the last key is expired, that key 
       can be used, until the new key is configured  based on the configuration */
    if ((pKeyTouse == NULL) && (pExpiredNode != NULL))
    {
        pRipIfRec->RipIfaceCfg.b1LastKeyStatus = TRUE;
        pRipCxt = RipGetCxtInfoRecFrmIface (u2IfId);
        if (pRipCxt != NULL)
        {
            RipTrapInfo.i4CxtId = pRipCxt->i4CxtId;
            RipTrapInfo.u4IfIndex = (UINT4) u2IfId;
            RipTrapInfo.i4AuthKeyId = (INT4) pExpiredNode->u1AuthKeyId;
            RipUtilSnmpIfSendTrap (RIP_AUTH_LAST_KEY_TRAP_ID, &RipTrapInfo);
        }
        if (pRipCxtEntry->b1LastKeyLifeTimeStatus == TRUE)
        {
            RIP_CRYPTO_STOP_GEN_TIME (pExpiredNode) = (UINT4) -1;    /* infinite time */
            RIP_CRYPTO_STOP_ACCEPT_TIME (pExpiredNode) = (UINT4) -1;    /* infinite time */
            return pExpiredNode;
        }
        else
        {
            TMO_SLL_Delete (&(pRipIfRec->RipCryptoAuthKeyList),
                            &(pExpiredNode->NextNode));
            RIP_FREE_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pExpiredNode);
            pRipIfRec->RipIfaceCfg.u2AuthType = RIPIF_NO_AUTHENTICATION;
        }
    }
    return pKeyTouse;
}

/****************************************************************************
 Function    :  RipCryptoGetKeyToUseForReceive
 Description :  Gets the key in use for the given interface index.
 Input       :  u2IfId - Inderface index for which the key in use should be 
                found.
                u1KeyId - Key identifier.
                
 Output      :  None.
 Returns     :  pAuthInfo or NULL
****************************************************************************/
tCryptoAuthKeyInfo *
RipCryptoGetKeyToUseForReceive (UINT2 u2IfId, UINT1 u1KeyId,
                                tRipCxt * pRipCxtEntry)
{
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRec ((UINT4) u2IfId, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return NULL;
    }

    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {

        /* Scan for a active key */

        if (RIP_CRYPTO_AUTH_KEY_ID (pNode) == u1KeyId)
        {
            /* we found the key with the same key id */
            return pNode;
        }

    }

    /* No key ID matches with the received key ID */

    return NULL;
}
