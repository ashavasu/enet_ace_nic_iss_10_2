/*********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripagg.c,v 1.51 2017/09/20 13:08:00 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
# include "ripinc.h"

/**************************************************************************/
/* Function Name     :  RipIfAggTblInit                                   */
/*                                                                        */
/* Description       :  This function create a trie to store the interface*/
/*                      specific aggregations configured over an interface*/
/*                      It also adds the created trie root to an SLL List */
/*                      that holds the trie root for all the interfaces   */
/*                                                                        */
/* Input(s)          :  u4IfIndex : The CFA interface index for which     */
/*                                  trie is being created                 */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  SUCCESS/FAILURE.                                  */
/**************************************************************************/

INT1
RipIfAggTblInit (UINT4 u4IfIndex, tRipCxt * pRipCxtEntry)
{
    tCreateParams       IfAggRtCreate;
    tRipIfAggTblRoot   *pRipIfAggTableRoot = NULL;
    tRipIfAggTblRoot   *pPrevNode = NULL;
    tRipIfAggTblRoot   *pNode = NULL;
    VOID               *pRipIfTrieRoot = NULL;
    INT1                i1RipIfAggRtId = 0;
    UINT4               u4Port = 0;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4GetPortIfIn;
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec (u4IfIndex, pRipCxtEntry);

    IfAggRtCreate.u1KeySize = 2 * sizeof (UINT4);    /* key is address + mask */
    /*Conflicting with RTM Trie Entries */
    IfAggRtCreate.u4Type = RIP_AG_TYPE (u4IfIndex);
    IfAggRtCreate.u1AppId = OTHERS_ID;

    /*Create a trie for this interface */
    i1RipIfAggRtId = (INT1) TrieCreate (&(IfAggRtCreate), &(pRipIfTrieRoot));

    if (i1RipIfAggRtId >= 0)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC,
                 RIP_NAME,
                 "TrieCreate Success For Interface Aggregation Trie\n");
        if (RIP_ALLOC_IF_AGG_TBL_ROOT_MEM (pRipIfAggTableRoot) != NULL)
        {
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC,
                     RIP_NAME,
                     "Mem Alloc Success For Interface Aggregation TrieSLL\n");
            i4GetPortIfIn = NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port);
            UNUSED_PARAM (i4GetPortIfIn);
            TMO_SLL_Init_Node (&(pRipIfAggTableRoot->pNext));
            pRipIfAggTableRoot->u4IfIndex = u4Port;
            pRipIfAggTableRoot->pRipIfAggTblPtr = pRipIfTrieRoot;

            /* Add the structure that holds the created trie root ptr and the 
             * interface index to the sll list*/
            TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pNode,
                          tRipIfAggTblRoot *)
            {
                NetIpv4GetCfaIfIndexFromPort (pNode->u4IfIndex, &u4CfaIfIndex);
                if (u4CfaIfIndex > u4IfIndex)
                {
                    pPrevNode = (tRipIfAggTblRoot *)
                        TMO_SLL_Previous (&pRipCxtEntry->RipIfAggTblRootLst,
                                          &(pNode->pNext));
                    TMO_SLL_Insert (&pRipCxtEntry->RipIfAggTblRootLst,
                                    &(pPrevNode->pNext),
                                    &(pRipIfAggTableRoot->pNext));
                    break;
                }
            }

            if ((TMO_SLL_Count (&pRipCxtEntry->RipIfAggTblRootLst) == 0) ||
                (pNode == NULL))
            {
                TMO_SLL_Add (&pRipCxtEntry->RipIfAggTblRootLst,
                             &(pRipIfAggTableRoot->pNext));
            }
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC,
                     RIP_NAME,
                     "Success returned after trie creation and sll addition\n");
            return (RIP_SUCCESS);
        }
        else
        {
            /*TODO delete the created trie instance */
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId, ALL_FAILURE_TRC,
                     RIP_NAME,
                     "Mem Alloc Failed For Interface Aggregation TrieSLL\n");
        }
    }
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             ALL_FAILURE_TRC,
             RIP_NAME, "TrieCreate Failed For Interface Aggregation \n");
    if (pRipIfRec != NULL)
    {
        (pRipIfRec->RipIfaceStats.u4RipAggTblInitFailCount)++;
    }
    return RIP_FAILURE;
}

/**************************************************************************/
/* Function Name     :  RipIfAggTblShut                                   */
/*                                                                        */
/* Description       :  This function delete the trie that store the      */
/*                      interface specific aggregations configured over   */
/*                      an interface only if all configured entries have  */
/*                      been deleted and also delete the created trie root */
/*                      from the SLL List that holds the trie root for    */
/*                      all the interfaces                                */
/*                                                                        */
/* Input(s)          :  u4IfIndex : IP Port no for which trie is          */
/*                                  being deleted                         */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  None.                                             */
/**************************************************************************/

VOID
RipIfAggTblShut (UINT4 u4IfIndex, tRipCxt * pRipCxtEntry)
{
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;

    /* Delete the trie if no more aggregation exists in the trie */
    if (RipDeleteIfAggTbl ((UINT2) u4IfIndex, pRipCxtEntry) == RIP_SUCCESS)
    {
        /*Delete the structure that holds the trie pointer */
        TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                      tRipIfAggTblRoot *)
        {
            if (pRipIfAggTblRoot->u4IfIndex == u4IfIndex)
            {
                TMO_SLL_Delete (&pRipCxtEntry->RipIfAggTblRootLst,
                                &(pRipIfAggTblRoot->pNext));
                RIP_RELEASE_IF_AGG_TBL_ROOT_MEM (pRipIfAggTblRoot);
                break;
            }
        }
    }
}

/**************************************************************************/
/* Function Name     :  RipDeleteIfAgg                                    */
/*                                                                        */
/* Description       :  This function delete the all the aggregation      */
/*                      configured over the specified inteface and delete */
/*                      the trie for that interface.                      */
/*                                                                        */
/* Input(s)          :  pRipIfRec : The interface record                  */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS                                       */
/**************************************************************************/
INT1
RipDeleteIfAgg (tRipIfaceRec * pRipIfRec)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tDeleteOutParams    DeleteOutParams;
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    UINT4               au4Indx[2];
    UINT4               u4Address = 0;
    UINT4               u4Mask = 0;
    UINT2               u2IfIndex = 0;
    VOID               *pTrieRoot = NULL;
    tRipCxt            *pRipCxtEntry = NULL;

    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }
    MEMSET (&DeleteOutParams, 0, sizeof (tDeleteOutParams));

    u2IfIndex = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);
    pRipCxtEntry = pRipIfRec->pRipCxt;
    if (pRipCxtEntry == NULL)
    {
        (pRipIfRec->RipIfaceStats.u4RipDelIfAggNullEntryCount)++;
        return RIP_FAILURE;
    }
    pTrieRoot = RipGetIfAggTblRoot (u2IfIndex, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        return (RIP_SUCCESS);
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;
    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    while (TrieGetNextEntry ((&InParams), (&OutParams)) == TRIE_SUCCESS)
    {
        pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;

        u4Address = pRipIfAggRtInfo->u4AggRtAddr;
        u4Mask = pRipIfAggRtInfo->u4AggRtMask;
        /*Delete the trie entry */
        if (RipDeleteIfAggRt
            ((UINT4) u2IfIndex, u4Address, u4Mask, pRipCxtEntry) == RIP_FAILURE)
        {
            (pRipIfRec->RipIfaceStats.u4RipAggDelTrieFailCount)++;
            RIP_TRC_ARG1 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC, RIP_NAME,
                          "Failed To Delete Trie Entry  %x \n", u4Address);
        }
    }

    /* No more entries are present in the trie.We can delete the 
     * trie*/
    i4OutCome = TrieDelete ((&InParams), RipTrieDeleteCallBack,
                            (&DeleteOutParams));

    if (i4OutCome != TRIE_SUCCESS)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC,
                 RIP_NAME,
                 "Failed to Delete Interface Specific Aggregation Trie \n");
        (pRipIfRec->RipIfaceStats.u4RipAggIntfSpecificDelTrieFailCount)++;
        return RIP_FAILURE;
    }

    /*Delete the structure that holds the trie pointer from sll list */
    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        if (pRipIfAggTblRoot->u4IfIndex == (UINT4) u2IfIndex)
        {
            TMO_SLL_Delete (&pRipCxtEntry->RipIfAggTblRootLst,
                            &(pRipIfAggTblRoot->pNext));
            RIP_RELEASE_IF_AGG_TBL_ROOT_MEM (pRipIfAggTblRoot);
            break;
        }
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipGetIfAggTblRoot                                */
/*                                                                        */
/* Description       :  This function returns the trie root for the       */
/*                      specified interface                               */
/*                                                                        */
/* Input(s)          :  u2Port : The interface index for which trie       */
/*                                  root is returned                      */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  Trie root / NULL                                  */
/**************************************************************************/
VOID               *
RipGetIfAggTblRoot (UINT2 u2Port, tRipCxt * pRipCxtEntry)
{
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;

    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        if (pRipIfAggTblRoot->u4IfIndex == (UINT4) u2Port)
        {
            break;
        }
    }
    if (pRipIfAggTblRoot == NULL)
    {
        return (NULL);
    }
    else
    {
        return (pRipIfAggTblRoot->pRipIfAggTblPtr);
    }
}

/**************************************************************************/
/* Function Name     :  RipCheckIfAggLimit                                */
/*                                                                        */
/* Description       :  This function checks if the maximum number of     */
/*                      aggregations have been configured over the        */
/*                      interface                                         */
/*                                                                        */
/* Input(s)          :  u2Port : the interface index                      */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_FAILURE / RIP_SUCCESS                         */
/**************************************************************************/
INT1
RipCheckIfAggLimit (UINT2 u2Port, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    INT1                i1RetVal = RIP_SUCCESS;
    UINT2               u2Count = 0;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */
    VOID               *pTrieRoot = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec (u2Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    pTrieRoot = RipGetIfAggTblRoot (u2Port, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        return i1RetVal;
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    while (TrieGetNextEntry (&(InParams), &(OutParams)) == TRIE_SUCCESS)
    {
        pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
        if (pRipIfAggRtInfo != NULL)
        {
            au4Indx[0] = RIP_HTONL (pRipIfAggRtInfo->u4AggRtAddr);
            au4Indx[1] = RIP_HTONL (pRipIfAggRtInfo->u4AggRtMask);
            u2Count++;
        }
    }
    if (u2Count >= RIP_MAX_IF_AGG)
    {
        (pRipIfRec->RipIfaceStats.u4RipAggChkLimitFailCount)++;
        i1RetVal = RIP_FAILURE;
    }
    return i1RetVal;
}

/**************************************************************************/
/* Function Name     :  RipCheckIfAgg                                     */
/*                                                                        */
/* Description       :  This function checks if any aggregation exists    */
/*                      over an interface for the inputted ip/mask(route) */
/*                      If exist, returns the aggregated route info       */
/*                                                                        */
/* Input(s)          :  u2IfIndex : the interface index over which        */
/*                                  the availability of aggregated route  */
/*                                  need to be checked.                   */
/*                      u4Address : network address                       */
/*                      u4Mask    : mask                                  */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  Aggregated route info / NULL                      */
/**************************************************************************/
tRipIfAggRtInfo    *
RipCheckIfAgg (UINT2 u2IfIndex, UINT4 u4Address, UINT4 u4Mask,
               tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    INT4                i4OutCome = TRIE_FAILURE;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */
    VOID               *pTrieRoot = NULL;

    pTrieRoot = RipGetIfAggTblRoot (u2IfIndex, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        return NULL;
    }

    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) au4Indx;
    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieLookupEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pRipIfAggRtInfo = (tRipIfAggRtInfo *) (OutParams.pAppSpecInfo);
        if (pRipIfAggRtInfo != NULL)
        {
            /* Since Look up returns the nearest match we need to ensure 
             * that the entry returned is the proper aggregation route for 
             * inputted address.*/
            if (pRipIfAggRtInfo->u4AggRtAddr !=
                (u4Address & pRipIfAggRtInfo->u4AggRtMask))
            {
                return (NULL);
            }
            if ((pRipIfAggRtInfo->u1AggRtStatus == ACTIVE) ||
                (pRipIfAggRtInfo->u1AggRtStatus == NOT_IN_SERVICE))
            {
                return ((tRipIfAggRtInfo *) (OutParams.pAppSpecInfo));
            }
        }
    }
    return (NULL);
}

/**************************************************************************/
/* Function Name     :  RipCheckSummaryAgg                                */
/*                                                                        */
/* Description       :  This function checks if any summary exists        */
/*                      over an interface for the inputted ip/mask(route) */
/*                      If exist, returns the aggregated route info       */
/*                                                                        */
/* Input(s)          :  u4Address : ip address                            */
/*                      u4Mask    : mask                                  */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  Aggregated route info / NULL                      */
/**************************************************************************/
tRipRtEntry        *
RipCheckSummaryAgg (UINT4 u4Address, UINT4 u4Mask, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRtEntry = NULL;
    INT4                i4OutCome = TRIE_FAILURE;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */

    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieLookupEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pRipRtEntry = (tRipRtEntry *) OutParams.pAppSpecInfo;

        if (pRipRtEntry->RtInfo.u4RowStatus == ACTIVE)
        {
            return (pRipRtEntry);
        }
        if ((pRipRtEntry->RtInfo.u4RowStatus == NOT_IN_SERVICE) &&
            (RIP_RT_CHANGED_STATUS (pRipRtEntry) != 0))
        {
            return (pRipRtEntry);
        }
    }
    return (NULL);
}

/**************************************************************************/
/* Function Name     :  RipCheckSinkRt                                    */
/*                                                                        */
/* Description       :  This function checks if the aggregate route       */
/*                      (ip address + mask)should be added as sink route  */
/*                      to rtm or not.Return RIP_SUCESS if sink route     */
/*                      should be added                                   */
/*                                                                        */
/* Input(s)          :  u4Address : ip address                            */
/*                      u4Mask    : mask                                  */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipCheckSinkRt (UINT4 u4Address, UINT4 u4Mask, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRt = NULL;
    INT4                i4OutCome = TRIE_FAILURE;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];

    /* HTONL is needed as we store in trie in the network byte order */
    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    /* The aggregated route is not installed as a sink route if
       1.  There is any route present in the rip database (excluding summary 
       route) that is same as the aggregated route.
       2 . There is any route in the rip database (excluding summary route)
       under which the aggregated route can fall.

       If the above two condition is satisfied , the aggregated route
       should not be installed as sink route to rtm.
       Else it should be installed as sink route to rtm */

    i4OutCome = TrieLookupEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* Match is found.Now verify if the match is the summary route or 
         * not.If the best match found is the summary route, then sink 
         * route can be insatlled.Else sink route must not be installed 
         * in rtm.*/
        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if ((((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry]
                 != NULL) && (i4AppEntry != (OTHERS_ID - 1)))
            {
                pRipRt =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                /* Since look up returns the nearest match, we need to ensure 
                 * that the entry returned is the proper aggregation route for 
                 * the inputted address.*/
                if (pRipRt->RtInfo.u4DestNet !=
                    (u4Address & pRipRt->RtInfo.u4DestMask))
                {
                    return RIP_SUCCESS;
                }

                if (pRipRt->RtInfo.u4RowStatus == IPFWD_ACTIVE)
                {
                    /* Matching route is not the summary route.So sink route 
                     * should not be installed*/

                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Sink Route Should Not Be Added\n");
                    return RIP_FAILURE;
                }
            }
        }
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipUpdateSinkRtInCxt                              */
/*                                                                        */
/* Description       :  This function install / delete/modify the         */
/*                      aggregated route as sink route to/from  rtm       */
/*                                                                        */
/* Input(s)          :  u4DestNet : The aggregated route address          */
/*                      u4DestMask : Mask                                 */
/*                      i4Metric : The aggregated route metric            */
/*                      u1UpdateType : This specify whether route is      */
/*                      installed as sink route to rtm or  deleted from   */
/*                      rtm or modified.Thus can have three input values  */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipUpdateSinkRtInCxt (tRipCxt * pRipCxtEntry, UINT4 u4DestNet, UINT4 u4DestMask,
                      UINT2 u2Metric, UINT1 u1UpdateType)
{
    tNetIpv4RtInfo      NetRtInfo;

    MEMSET ((UINT1 *) &NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4DestNet = u4DestNet;
    NetRtInfo.u4DestMask = u4DestMask;
    NetRtInfo.u4NextHop = RIP_IF_AGG_NEXTHOP;
    NetRtInfo.u4RtIfIndx = IPIF_INVALID_INDEX;
    NetRtInfo.i4Metric1 = (INT4) u2Metric;
    NetRtInfo.u2RtProto = OTHERS_ID;
    NetRtInfo.u2RtType = REJECT;
    NetRtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
    NetRtInfo.u2ChgBit = (UINT2) IP_BIT_ALL;
    NetRtInfo.u4ContextId = (UINT4) pRipCxtEntry->i4CxtId;

    if (NetIpv4LeakRoute (u1UpdateType, &NetRtInfo) == NETIPV4_FAILURE)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC, RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "Failed To Update Forwarding Table For Sink Route\n");
        return (RIP_FAILURE);
    }
    return (RIP_SUCCESS);
}

/**************************************************************************/
/* Function Name     :  RipCheckRtInIfAggTbl                              */
/*                                                                        */
/* Description       :  This function check if the inputted ip/mask route */
/*                      exist in the aggregation table for the specified  */
/*                      interface.If present, it returns the aggregated   */
/*                      route.Else NULL                                   */
/*                                                                        */
/* Input(s)          :  u2IfIndex : the interface index over which        */
/*                                  the availability of aggregated route  */
/*                                  need to be checked.                   */
/*                      u4Address : ip address                            */
/*                      u4Mask    : mask                                  */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  aggregated route info / NULL                      */
/**************************************************************************/
tRipIfAggRtInfo    *
RipCheckRtInIfAggTbl (UINT2 u2IfIndex, UINT4 u4Address, UINT4 u4Mask,
                      tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    INT4                i4OutCome;
    UINT4               au4Indx[2];
    VOID               *pTrieRoot = NULL;

    /* Get the interface specific aggregation table's root from the SLL */
    pTrieRoot = RipGetIfAggTblRoot (u2IfIndex, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        return (NULL);
    }

    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME, "Aggregation Route Available \n");
        return (pRipIfAggRtInfo);
    }
    return (NULL);
}

/**************************************************************************/
/* Function Name     :  RipAddIfAggRtToTable                              */
/*                                                                        */
/* Description       :  This function adds the  configured                */
/*                      aggregation route to the specified interface trie */
/*                                                                        */
/* Input(s)          :  u4IfIndex : the interface index over which the    */
/*                                  the aggregation is configured         */
/*                      u4Address : ip address                            */
/*                      u4Mask    : mask                                  */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  Aggregated route / NULL                           */
/**************************************************************************/
tRipIfAggRtInfo    *
RipAddIfAggRtToTable (UINT4 u4IfIndex, UINT4 u4Address, UINT4 u4Mask,
                      tRipCxt * pRipCxtEntry)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[2];
    UINT4               u4Port = 0;
    VOID               *pTrieRoot = NULL;
    INT4                i4GetPortIfIn;

    i4GetPortIfIn = NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port);
    UNUSED_PARAM (i4GetPortIfIn);
    pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME, "No Trie Avialable \n");
        return (NULL);
    }

    if (RIP_ALLOC_IF_AGG_REC_MEM (pRipIfAggRtInfo) == NULL)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC,
                 RIP_NAME,
                 "Failed To Allocate Memory For Interface Specific Aggregated,"
                 "Route \n");
        return (NULL);
    }

    MEMSET (pRipIfAggRtInfo, 0, sizeof (tRipIfAggRtInfo));
    pRipIfAggRtInfo->u4AggRtAddr = u4Address;
    pRipIfAggRtInfo->u4AggRtMask = u4Mask;
    RIP_INIT_IF_AGG_RT_DEFAULTS (pRipIfAggRtInfo);
    pRipIfAggRtInfo->u1AggRtStatus = NOT_READY;
    pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;
    pRipIfAggRtInfo->pRipCxt = pRipCxtEntry;

    /*Now add this route entry to the trie specific for this interface. */
    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;

    if (TrieAddEntry (&(InParams), (VOID *) pRipIfAggRtInfo, &(OutParams))
        == TRIE_SUCCESS)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC,
                 RIP_NAME,
                 "Successfully Added Interface Specific Aggregated\n");
        return (pRipIfAggRtInfo);
    }
    else
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC,
                 RIP_NAME, "Failed To Add Interface Specific Aggregated \n");
        return (NULL);
    }
}

/**************************************************************************/
/* Function Name     :  RipDeleteIfAggRt                                  */
/*                                                                        */
/* Description       :  This function delete the aggregation route from   */
/*                      the specified interface trie                      */
/*                                                                        */
/* Input(s)          :  u4IfIndex : IP Port over which the                */
/*                                 the aggregation is configured          */
/*                      u4Address : ip address                            */
/*                      u4Mask    : mask                                  */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS                                       */
/**************************************************************************/
INT1
RipDeleteIfAggRt (UINT4 u4IfIndex, UINT4 u4Address, UINT4 u4Mask,
                  tRipCxt * pRipCxtEntry)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome = RIP_SUCCESS;
    UINT4               au4Indx[2];
    VOID               *pTrieRoot = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec ((UINT2) u4IfIndex, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4IfIndex, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        return (RIP_SUCCESS);
    }

    /*Now delete this route entry from the specified trie. */
    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieDeleteEntry (&(InParams), &(OutParams), RIP_IF_AGG_NEXTHOP);
    if (i4OutCome == TRIE_SUCCESS)
    {
        /* Freeing the allocated memory */
        pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
        i4OutCome = (INT4) RIP_RELEASE_IF_AGG_REC_MEM (pRipIfAggRtInfo);
        if (i4OutCome == RIP_MEM_SUCCESS)
        {
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC,
                     RIP_NAME,
                     "Successfully Deleted The Interface Specific Aggregated ,"
                     "Route\n");
            return (RIP_SUCCESS);
        }
    }
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             ALL_FAILURE_TRC,
             RIP_NAME,
             "Failed to Delete The Interface Specific Aggregated Route\n");
    (pRipIfRec->RipIfaceStats.u4RipDelIfAggRouteFailCount)++;
    return RIP_FAILURE;
}

/**************************************************************************/
/* Function Name     :  RipUpdateIfAggRtAsSinkRt                          */
/*                                                                        */
/* Description       :  This function add the aggregated route as sink    */
/*                      or delete or modify the added sink route          */
/*                                                                        */
/* Input(s)          :  pRipIfAggRtInfo : aggregated route info           */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS                                       */
/**************************************************************************/
INT1
RipUpdateIfAggRtAsSinkRt (tRipIfAggRtInfo * pRipIfAggRtInfo)
{
    INT4                i4RetVal = RIP_SUCCESS;
    INT4                i4SinkRtStatus = RIP_FAILURE;
    tRipCxt            *pRipCxtEntry = NULL;

    pRipCxtEntry = pRipIfAggRtInfo->pRipCxt;

    /* Check whether the route need to be added as a sink route or not. */
    i4SinkRtStatus = RipCheckSinkRt (pRipIfAggRtInfo->u4AggRtAddr,
                                     pRipIfAggRtInfo->u4AggRtMask,
                                     pRipCxtEntry);

    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) == RIP_AUTO_SUMMARY_ENABLE)
    {
        if (pRipIfAggRtInfo->u1SinkRtFlag == RIP_INSTALL_SINK_RT)
        {
            /* Interface aggregation is installed as sink route.
             * It should be deleted from rtm as auto summary is enabled*/
            i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                             pRipIfAggRtInfo->u4AggRtAddr,
                                             pRipIfAggRtInfo->u4AggRtMask,
                                             pRipIfAggRtInfo->u2Metric,
                                             NETIPV4_DELETE_ROUTE);
            if (i4RetVal == RIP_SUCCESS)
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC, RIP_NAME,
                         "Sink Route Deleted From Forwarding Table\n");
                pRipIfAggRtInfo->u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
            }
        }
    }
    else                        /* Summary disable */
    {
        if (i4SinkRtStatus == RIP_SUCCESS)
        {
            /* Sink route should be installed */
            if (pRipIfAggRtInfo->u1SinkRtFlag == RIP_INSTALL_SINK_RT)
            {
                /* Already installed.No need to add it again as sink route.
                 * Do only updation*/
                i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                                 pRipIfAggRtInfo->u4AggRtAddr,
                                                 pRipIfAggRtInfo->u4AggRtMask,
                                                 pRipIfAggRtInfo->u2Metric,
                                                 NETIPV4_MODIFY_ROUTE);
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC, RIP_NAME, "Sink Route Modified\n");
            }
            else
            {
                /* Install the route as sink route to rtm */
                i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                                 pRipIfAggRtInfo->u4AggRtAddr,
                                                 pRipIfAggRtInfo->u4AggRtMask,
                                                 pRipIfAggRtInfo->u2Metric,
                                                 NETIPV4_ADD_ROUTE);
                if (i4RetVal == RIP_SUCCESS)
                {
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Sink Route Added To Forwarding Table\n");
                    pRipIfAggRtInfo->u1SinkRtFlag = RIP_INSTALL_SINK_RT;
                }
            }
        }
        else
        {
            /* Sink route should not be installed */
            if (pRipIfAggRtInfo->u1SinkRtFlag == RIP_INSTALL_SINK_RT)
            {
                i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                                 pRipIfAggRtInfo->u4AggRtAddr,
                                                 pRipIfAggRtInfo->u4AggRtMask,
                                                 pRipIfAggRtInfo->u2Metric,
                                                 NETIPV4_DELETE_ROUTE);
                if (i4RetVal == RIP_SUCCESS)
                {
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Sink Route Deleted From Forwarding Table\n");
                    pRipIfAggRtInfo->u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
                }
            }
        }
    }
    return ((INT1) i4RetVal);
}

/**************************************************************************/
/* Function Name     :  RipDeleteIfAggSinkRt                              */
/*                                                                        */
/* Description       :  This function deletes the interface aggregation   */
/*                      which was added as sink from rtm.                 */
/*                                                                        */
/* Input(s)          :  pRipIfAggRtInfo : aggregated route info           */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipDeleteIfAggSinkRt (tRipIfAggRtInfo * pRipIfAggRtInfo)
{
    tRipCxt            *pRipCxtEntry = pRipIfAggRtInfo->pRipCxt;

    if (pRipIfAggRtInfo->u1SinkRtFlag == RIP_NO_INSTALL_SINK_RT)
    {
        /* Route to be deleted is not installed.So no need to delete it */
        return RIP_SUCCESS;
    }
    else
    {
        if (RipUpdateSinkRtInCxt (pRipCxtEntry, pRipIfAggRtInfo->u4AggRtAddr,
                                  pRipIfAggRtInfo->u4AggRtMask,
                                  pRipIfAggRtInfo->u2Metric,
                                  NETIPV4_DELETE_ROUTE) == RIP_FAILURE)
        {
            return (RIP_FAILURE);
        }
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC,
                 RIP_NAME, "Sink Route Deleted From Forwarding Table\n");

        pRipIfAggRtInfo->u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
    }
    return (RIP_SUCCESS);
}

/**************************************************************************/
/* Function Name     :  RipUpdateSummaryRtAsSinkRt                        */
/*                                                                        */
/* Description       :  This function add summray route as sink route     */
/*                      or delete / modify the added sink route in the    */
/*                      rtm                                               */
/*                                                                        */
/* Input(s)          :  pRipSummryRt : summary route info                 */
/*                      pRipCxtEntry : Context Info                       */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS                                       */
/**************************************************************************/
INT1
RipUpdateSummaryRtAsSinkRt (tRipRtEntry * pRipSummryRt, tRipCxt * pRipCxtEntry)
{

    INT4                i4SinkRtStatus = RIP_FAILURE;
    INT4                i4RetVal = RIP_SUCCESS;

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }

    /* Check if the summary route needs to be added as sink route or not */
    i4SinkRtStatus = RipCheckSinkRt (pRipSummryRt->RtInfo.u4DestNet,
                                     pRipSummryRt->RtInfo.u4DestMask,
                                     pRipCxtEntry);

    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) == RIP_AUTO_SUMMARY_DISABLE)
    {
        if (pRipSummryRt->RtInfo.u1SinkRtFlag == RIP_INSTALL_SINK_RT)
        {
            /* Delete the installed sink route from the rtm */
            i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                             pRipSummryRt->RtInfo.u4DestNet,
                                             pRipSummryRt->RtInfo.u4DestMask,
                                             (UINT2) pRipSummryRt->RtInfo.
                                             i4Metric1, NETIPV4_DELETE_ROUTE);
            if (i4RetVal == RIP_SUCCESS)
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC, RIP_NAME,
                         "Sink Route Deleted From Forwarding Table\n");
                pRipSummryRt->RtInfo.u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
            }
        }
    }
    else                        /* Summary enable */
    {
        if (i4SinkRtStatus == RIP_SUCCESS)
        {
            /* Sink route should be installed */
            if (pRipSummryRt->RtInfo.u1SinkRtFlag == RIP_INSTALL_SINK_RT)
            {
                /* Its already installed.No need to install it again.
                 * Only modify it*/
                i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                                 pRipSummryRt->RtInfo.u4DestNet,
                                                 pRipSummryRt->RtInfo.
                                                 u4DestMask,
                                                 (UINT2) pRipSummryRt->RtInfo.
                                                 i4Metric1,
                                                 NETIPV4_MODIFY_ROUTE);
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC, RIP_NAME, "Sink Route Modified\n");
            }
            else
            {
                /* Install sink route */
                i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                                 pRipSummryRt->RtInfo.u4DestNet,
                                                 pRipSummryRt->RtInfo.
                                                 u4DestMask,
                                                 (UINT2) pRipSummryRt->RtInfo.
                                                 i4Metric1, NETIPV4_ADD_ROUTE);
                if (i4RetVal == RIP_SUCCESS)
                {
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Sink Route Added To Forwarding Table\n");
                    pRipSummryRt->RtInfo.u1SinkRtFlag = RIP_INSTALL_SINK_RT;
                }
            }
        }
        else
        {
            /* Sink route should not be installed */
            if (pRipSummryRt->RtInfo.u1SinkRtFlag == RIP_INSTALL_SINK_RT)
            {
                /* Delete the installed sink route from the rtm */
                i4RetVal = RipUpdateSinkRtInCxt (pRipCxtEntry,
                                                 pRipSummryRt->RtInfo.u4DestNet,
                                                 pRipSummryRt->RtInfo.
                                                 u4DestMask,
                                                 (UINT2) pRipSummryRt->RtInfo.
                                                 i4Metric1,
                                                 NETIPV4_DELETE_ROUTE);
                if (i4RetVal == RIP_SUCCESS)
                {
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Sink Route Deleted From Forwarding Table\n");
                    pRipSummryRt->RtInfo.u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
                }
            }
        }
    }
    return ((INT1) i4RetVal);
}

/**************************************************************************/
/* Function Name     :  RipDeleteSummarySinkRt                            */
/*                                                                        */
/* Description       :  This function deletes the specified summary route */
/*                      that was added as sink route from rtm and reset   */
/*                      the flag                                          */
/*                                                                        */
/* Input(s)          :  pRipSummryRt : summary route info                 */
/*                      pRipCxtEntry: Pointer to the context Info         */
/*                                                                        */
/* Output(s)         :  none                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS                                       */
/**************************************************************************/
INT1
RipDeleteSummarySinkRt (tRipRtEntry * pRipSummryRt, tRipCxt * pRipCxtEntry)
{
    if (pRipSummryRt->RtInfo.u1SinkRtFlag == RIP_NO_INSTALL_SINK_RT)
    {
        /* The route is not installed.so no need to delete it. */
        return RIP_SUCCESS;
    }
    else
    {
        if (RipUpdateSinkRtInCxt (pRipCxtEntry,
                                  pRipSummryRt->RtInfo.u4DestNet,
                                  pRipSummryRt->RtInfo.u4DestMask,
                                  (UINT2) pRipSummryRt->RtInfo.i4Metric1,
                                  NETIPV4_DELETE_ROUTE) == RIP_FAILURE)
        {
            return (RIP_FAILURE);
        }
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC,
                 RIP_NAME, "Sink Route Deleted From Forwarding Table\n");

        pRipSummryRt->RtInfo.u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
    }

    return (RIP_SUCCESS);
}

/**************************************************************************/
/* Function Name     :  RipUpdateIfAggRt                                  */
/*                                                                        */
/* Description       :  This function traverse the rip database and find  */
/*                      number of routes that fall under this aggregation.*/
/*                      It also update the mertic with the minimum of all */
/*                      the subnet routes falling under this aggregation. */
/*                                                                        */
/* Input(s)          :  pRipIfAggRtInfo : aggregated route info that      */
/*                                        should be updated               */
/*                      u2Port : The interface index over which the       */
/*                               aggregation was configured               */
/*                                                                        */
/* Output(s)         :  Updated aggregated route                          */
/*                                                                        */
/* Returns           :  RIP_SUCCESS                                       */
/**************************************************************************/
VOID
RipUpdateIfAggRt (tRipIfAggRtInfo * pRipIfAggRtInfo, UINT2 u2Port)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRt = NULL;
    tRipIfAggRtInfo    *pAggRt = NULL;
    INT4                i4OutCome;
    INT4                i4AppEntry;
    INT4                i4NewMetric = RIP_INFINITY;
    UINT4               au4Indx[2];
    UINT4               u4AggAddress;
    UINT4               u4AggMask;
    UINT4               u4RtAddress;
    UINT4               u4RtMask;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRipCxt            *pRipCxtEntry = NULL;

    /*Getting the aggregation address and mask  */
    u4AggAddress = pRipIfAggRtInfo->u4AggRtAddr;
    u4AggMask = pRipIfAggRtInfo->u4AggRtMask;

    pRipCxtEntry = pRipIfAggRtInfo->pRipCxt;

    /* Initialising the count before updating it */
    pRipIfAggRtInfo->u2Count = 0;

    /* Traverse the rip database route by route starting from the 
     * aggrgated route range */

    au4Indx[0] = RIP_HTONL (u4AggAddress);
    au4Indx[1] = RIP_HTONL (u4AggMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = NULL;

    /*Check whether route same as aggregation route exists */
    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
    if (i4OutCome == TRIE_SUCCESS)
    {
        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if ((((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry]
                 != NULL) && (i4AppEntry != (OTHERS_ID - 1)))
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                break;
            }
        }

        if ((pRipRt != NULL) && (pRipRt->RtInfo.u4RowStatus == IPFWD_ACTIVE))
        {
            /* A route exist in rip database whose destination network and 
             * mask is same as the interface aggregation.Update the 
             * aggregated route metric and count */
            i4NewMetric =
                RIP_MIN_METRIC (i4NewMetric, pRipRt->RtInfo.i4Metric1);
            pRipIfAggRtInfo->u2Count++;
        }
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
    }

    /* Scan all the subnets under the given network */
    i4OutCome = TRIE_SUCCESS;
    while (i4OutCome == TRIE_SUCCESS)
    {
        pRipRt = NULL;

        /* Get the next entry from the rip database */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                if (i4AppEntry != (OTHERS_ID - 1))
                {
                    break;
                }
            }
        }

        if (pRipRt == NULL)
        {
            break;
        }
        else
        {
            /* Get the destination route and mask of the rip route entry */
            u4RtAddress = pRipRt->RtInfo.u4DestNet;
            u4RtMask = pRipRt->RtInfo.u4DestMask;
            if (pRipRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                au4Indx[0] = RIP_HTONL (u4RtAddress);
                au4Indx[1] = RIP_HTONL (u4RtMask);
                MEMSET (apAppSpecInfo, 0,
                        MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
                continue;
            }
            /* Check if we have traversed all the routes in rip database 
             * falling under the aggregation range */
            if (u4AggAddress != (u4RtAddress & u4AggMask))
            {
                break;
            }

            /* Check if any aggregation exists for this rip entry in the 
             * interface specific aggregation table*/
            pAggRt =
                RipCheckIfAgg (u2Port, u4RtAddress, u4RtMask, pRipCxtEntry);
            /* If the aggregation exist and if aggregation is same as the 
             * the inputted aggregated entry , update the metric and 
             * count.Else do not update as the aggregation can be 
             * an overlapping aggregation */
            if ((pAggRt != NULL) && (pAggRt->u4AggRtAddr == u4AggAddress))
            {
                i4NewMetric = RIP_MIN_METRIC (i4NewMetric,
                                              pRipRt->RtInfo.i4Metric1);
                pRipIfAggRtInfo->u2Count++;
            }
            au4Indx[0] = RIP_HTONL (u4RtAddress);
            au4Indx[1] = RIP_HTONL (u4RtMask);
            MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
        }
    }

    if (i4NewMetric != (INT4) pRipIfAggRtInfo->u2Metric)
    {
        /* Set the route change status bit */
        RIP_IF_AGG_RT_STATUS (pRipIfAggRtInfo) = RIP_IF_AGG_RT_CHANGED_VAL;
    }
    else
    {
        pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_UPDATED_VAL;
    }

    pRipIfAggRtInfo->u2Metric = (UINT2) i4NewMetric;
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME,
             "Successfully Updated The Interface Specific Aggregated Route\n");
}

/**************************************************************************/
/* Function Name     :  RipDeleteIfAggTbl                                 */
/*                                                                        */
/* Description       :  This function deletes the trie created for        */
/*                      holding the aggregations configured over the      */
/*                      inputted interface if no more                     */
/*                      aggregation exists .If no more configured         */
/*                      aggregations exist over this interface, the trie  */
/*                      will be deleted.                                  */
/*                                                                        */
/* Input(s)          :  u4IfIndex : The interface index over which the    */
/*                                  the aggregated route is configured    */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS if no entry available and trie is     */
/*                      deleted. Else  RIP_FAILURE                        */
/**************************************************************************/
INT1
RipDeleteIfAggTbl (UINT2 u2IfIndex, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tDeleteOutParams    DeleteOutParams;
    INT4                i4OutCome;
    UINT4               au4Indx[2];
    VOID               *pTrieRoot = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    pTrieRoot = RipGetIfAggTblRoot (u2IfIndex, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        return (RIP_SUCCESS);
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;
    MEMSET (&OutParams, 0, sizeof (tOutputParams));
    MEMSET (&DeleteOutParams, 0, sizeof (tDeleteOutParams));

    i4OutCome = TrieGetNextEntry ((&InParams), (&OutParams));
    if (i4OutCome == TRIE_FAILURE)
    {
        /* No entries available in the interface's aggregation trie.
         * So delete the trie */
        i4OutCome = TrieDelete ((&InParams), RipTrieDeleteCallBack,
                                (&DeleteOutParams));
        if (i4OutCome != TRIE_SUCCESS)
        {
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId, ALL_FAILURE_TRC,
                     RIP_NAME,
                     "Failed to Delete Interface Specific Aggregation Trie \n");
            (pRipIfRec->RipIfaceStats.u4RipDelIfAggTbl1FailCount)++;
            return RIP_FAILURE;
        }
        return RIP_SUCCESS;
    }
    (pRipIfRec->RipIfaceStats.u4RipDelIfAggTbl2FailCount)++;
    return RIP_FAILURE;
}

/**************************************************************************/
/* Function Name     : RipTrieDeleteCallBack                              */
/*                                                                        */
/* Description       : This is a call back function for TRIE Delete       */
/*                                                                        */
/* Input(s)          : pInput - Input Parameter.                          */
/*                                                                        */
/* Output(s)         : None.                                              */
/*                                                                        */
/* Return(s)         : None.                                              */
/**************************************************************************/
VOID
RipTrieDeleteCallBack (tDeleteOutParams * pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

/**************************************************************************/
/* Function Name     :  RipUpdateRtmForSummaryDisable                     */
/*                                                                        */
/* Description       : This function is called when summary status is     */
/*                     being set to disable from enable.Function does the */
/*                     following                                          */
/*                     1: delete each and every summary route that have   */
/*                        been added to rtm as sink route.Reset the sink  */
/*                        flag.                                           */
/*                     2: for each and every interface specific route,    */
/*                        add the aggregated route to rtm if it needs to  */
/*                        be established as sink route.Set the sink route */
/*                        flag                                            */
/*                                                                        */
/* Input(s)          :  None                                              */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipUpdateRtmForSummaryDisable (tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pSummaryRt = NULL;
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    tRipIfAggRtInfo    *pAggRt = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    UINT4               au4Indx[2];
    UINT4               u4Port = 0;
    VOID               *pTrieRoot = NULL;

    /* Serach the rip database for summary routes */
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    while (i4OutCome == TRIE_SUCCESS)
    {
        /* Get the next entry from the rip database */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        pSummaryRt = (tRipRtEntry *) OutParams.pAppSpecInfo;

        /* Check if the summary route was added as a sink route. */
        if (pSummaryRt->RtInfo.u1SinkRtFlag == RIP_INSTALL_SINK_RT)
        {
            i4OutCome = RipDeleteSummarySinkRt (pSummaryRt, pRipCxtEntry);
            if (i4OutCome == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed to Delete Already Added Sink Route From,"
                         "Forwarding Table \n");
            }
        }
        au4Indx[0] = (UINT4) RIP_HTONL (pSummaryRt->RtInfo.u4DestNet);
        au4Indx[1] = (UINT4) RIP_HTONL (pSummaryRt->RtInfo.u4DestMask);
        MEMSET (&OutParams, 0, sizeof (tOutputParams));
    }

    /* Add all the interface specific configured aggregations to rtm as 
     * sink route if the route need to be added*/
    i4OutCome = TRIE_SUCCESS;
    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        /* For each route in each interface, check if sink route should be 
         * installed or not. If sink route needs to be installed, do the 
         * same.Set the snk flg*/
        u4Port = pRipIfAggTblRoot->u4IfIndex;
        pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry);
        if (pTrieRoot == NULL)
        {
            return RIP_FAILURE;
        }
        au4Indx[0] = 0;
        au4Indx[1] = 0;
        InParams.pRoot = pTrieRoot;
        InParams.i1AppId = (OTHERS_ID - 1);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        OutParams.Key.pKey = NULL;
        MEMSET (&OutParams, 0, sizeof (tOutputParams));

        while (i4OutCome == TRIE_SUCCESS)
        {
            /* Get the next interface specific aggregated route */
            i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
            if (i4OutCome == TRIE_FAILURE)
            {
                break;
            }

            pAggRt = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
            if (pAggRt->u1AggRtStatus == ACTIVE)
            {
                if (RipUpdateIfAggRtAsSinkRt (pAggRt) == RIP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC, RIP_NAME,
                             "Failed to Add/Update The Sink Route \n");
                }
            }

            au4Indx[0] = (UINT4) RIP_HTONL (pAggRt->u4AggRtAddr);
            au4Indx[1] = (UINT4) RIP_HTONL (pAggRt->u4AggRtMask);
            MEMSET (&OutParams, 0, sizeof (tOutputParams));
        }
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipUpdateRtmForSummaryEnable                      */
/*                                                                        */
/* Description       : This function is called when summary status is     */
/*                     being set to enable from disable.Function does the */
/*                     following                                          */
/*                     1: delete each and every interface aggregated      */
/*                        route that have been added to rtm as sink       */
/*                        route.Reset the sink flag.                      */
/*                     2: for each and every summary  route, add to rtm if*/
/*                        it needs to be established as sink route.Set    */
/*                        the sink route flag                             */
/*                                                                        */
/* Input(s)          :  None                                              */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipUpdateRtmForSummaryEnable (tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pSummaryRt = NULL;
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    tRipIfAggRtInfo    *pAggRt = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    UINT4               au4Indx[2];
    UINT4               u4Port = 0;
    VOID               *pTrieRoot = NULL;

    /* Delete all the interface specific configured aggregations from rtm as 
     * sink route if the route have been added earlier*/
    /* Get the interface indices over which aggregation is configured. */
    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        u4Port = pRipIfAggTblRoot->u4IfIndex;
        /* For each aggregated route in each interface, check if sink route is 
         * installed or not. If sink route is installed, delete it from rtm.
         * Reset the snk flg*/
        pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry);
        if (pTrieRoot == NULL)
        {
            return RIP_FAILURE;
        }
        au4Indx[0] = 0;
        au4Indx[1] = 0;
        InParams.pRoot = pTrieRoot;
        InParams.i1AppId = (OTHERS_ID - 1);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        OutParams.Key.pKey = NULL;
        MEMSET (&OutParams, 0, sizeof (tOutputParams));

        while (i4OutCome == TRIE_SUCCESS)
        {
            /* Get the next interface specific aggregated route */
            i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
            if (i4OutCome == TRIE_FAILURE)
            {
                break;
            }

            pAggRt = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
            if (RipDeleteIfAggSinkRt (pAggRt) == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed to Delete Already Added Sink Route From,"
                         "Forwarding Table \n");
            }

            au4Indx[0] = RIP_HTONL (pAggRt->u4AggRtAddr);
            au4Indx[1] = RIP_HTONL (pAggRt->u4AggRtMask);
            MEMSET (&OutParams, 0, sizeof (tOutputParams));
        }
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    i4OutCome = TRIE_SUCCESS;

    while (i4OutCome == TRIE_SUCCESS)
    {
        /* Get the next entry from the rip database */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        pSummaryRt = (tRipRtEntry *) OutParams.pAppSpecInfo;

        /* Add summary route  to rtm */
        if (pSummaryRt->RtInfo.u4RowStatus == ACTIVE)
        {
            if (RipUpdateSummaryRtAsSinkRt (pSummaryRt, pRipCxtEntry))
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed to Add/Update The Sink Route \n");
            }
        }

        au4Indx[0] = RIP_HTONL (pSummaryRt->RtInfo.u4DestNet);
        au4Indx[1] = RIP_HTONL (pSummaryRt->RtInfo.u4DestMask);
        MEMSET (&OutParams, 0, sizeof (tOutputParams));
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipGenerateUpdateForSummaryEnable                 */
/*                                                                        */
/* Description       :  This function is called to generate update        */
/*                      message if summary enable.                        */
/*                                                                        */
/* Input(s)          :  u1Version -  Version Number.                      */
/*                      u1BcastFlag - Broadcasf Flag.                     */
/*                      u2If - Interface Number.                          */
/*                      pRipInfoBlk - Pointer to RipInformation SendPacket*/
/*                      pu2NoOfRoutes - Number Of route in packet.        */
/*                      u2DestNet - Destination Address.                  */
/*                      u2DestUdpPort - Destionation Port.                */
/*                      pRipCxtEntry - Pointer to the context entry.      */
/*                      u1UpdateSendFlag - Update Send or not.            */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipGenerateUpdateForSummaryEnable (UINT1 u1Version, UINT1 u1BcastFlag,
                                   UINT2 u2If,
                                   tRipInfoBlk * pRipInfoBlk,
                                   UINT2 *pu2NoOfRoutes,
                                   UINT4 u4DestNet, UINT2 u2DestUdpPort,
                                   tRipCxt * pRipCxtEntry,
                                   UINT1 u1UpdateSendFlag)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pSummaryRt = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    INT4                i4RetStat;
    INT4                i4AppEntry = 0;
    UINT4               au4Indx[2] = { 0, 0 };
    UINT4               u4IfAddr = 0;
    UINT4               u4DestMask = 0;
    UINT2               u2NoOfPktsSend = 0;
    UINT2               u2RtsToCompose = 0;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRipRtEntry        *pRtSend = NULL, *pRtTemp = NULL;
    INT2                i2OffSet =
        (INT2) RIP_OFFSET (tRipRtEntry, RipCxtRtNode);
    INT1                i1DefRtExist = FALSE;

    if (u2If != RIPIF_INVALID_INDEX)
    {
        pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);
        if (pRipIfRec == NULL)
        {
            return RIP_FAILURE;
        }
        u4IfAddr = pRipIfRec->u4Addr;
    }

    u2RtsToCompose = rip_get_routes_to_compose (u2If, pRipCxtEntry);
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME, "Generating Updates For Summary Enabled \n");

    switch (u1BcastFlag)
    {
        case RIP_REG_UPDATE:
        case RIP_SPACE_UPDATE:
        case RIP_RES_FOR_REQ:
        case RIP_UNICAST_UPDATE:
            if ((u1BcastFlag == RIP_REG_UPDATE) &&
                (RIP_SPACING_STATUS (pRipCxtEntry) == RIP_SPACING_ENABLE))
            {
                RipFindNumOfPktsPerSpace (u2If, u2RtsToCompose, pRipCxtEntry);
            }
            /*
             * In the spacing enable case send the update packet 
             * from the last send route entry
             * Otherwise Send from the starting.
             *
             * */
            if (u1BcastFlag == RIP_SPACE_UPDATE)
            {
                if (pRipIfRec != NULL)
                {
                    pRtSend = (tRipRtEntry *) pRipIfRec->pRipNextSendRt;
                }
            }
            else
            {
                /*
                 *checking default route entry exists in the List.
                 *Get the default route from route entry list.
                 * */
                pRtSend = (tRipRtEntry *)
                    TMO_DLL_First (&pRipCxtEntry->RipCxtRtList);
                while (pRtSend != NULL)
                {
                    pRtSend =
                        (tRipRtEntry *) ((FS_ULONG) pRtSend - (UINT2) i2OffSet);
                    if ((u2If != RIPIF_INVALID_INDEX)
                        && (pRipIfRec->RipIfaceCfg.u2DefaultMetric !=
                            RIP_NO_DEF_RT_ORIGINATE) && (i1DefRtExist == FALSE))
                    {
                        if ((pRtSend->RtInfo.u4DestNet == 0) &&
                            (pRtSend->RtInfo.u4RowStatus == IPFWD_ACTIVE))
                        {
                            rip_copy_valid_entries (pRipInfoBlk, pu2NoOfRoutes,
                                                    u2If, pRtSend,
                                                    pRipCxtEntry);
                            (*pu2NoOfRoutes)++;
                            i1DefRtExist = TRUE;
                        }
                        else
                        {
                            /* Default route is not present in the list.But 
                             * default route origination is enabled.So 
                             * add a default route explicitly for 
                             * advertising.*/
                            RipAddDefRtToUpdate (pRipInfoBlk, pu2NoOfRoutes,
                                                 u2If, pRipCxtEntry);
                            (*pu2NoOfRoutes)++;
                            i1DefRtExist = TRUE;
                        }

                        pRtSend = (tRipRtEntry *)
                            TMO_DLL_Next (&pRipCxtEntry->RipCxtRtList,
                                          (tTMO_DLL_NODE *) & pRtSend->
                                          RipCxtRtNode);
                        if (*pu2NoOfRoutes == u2RtsToCompose)
                        {
                            i4RetStat =
                                RipSendUpdateAndStartSpaceTmr (u1Version,
                                                               u1BcastFlag,
                                                               u4DestNet,
                                                               u2DestUdpPort,
                                                               pRipInfoBlk,
                                                               u2If,
                                                               &u2RtsToCompose,
                                                               &u2NoOfPktsSend,
                                                               pu2NoOfRoutes,
                                                               pRipIfRec,
                                                               pRipCxtEntry,
                                                               &(pRtSend->
                                                                 RipCxtRtNode));
                            /*
                             *If the space timer started break the 
                             * */
                            if (i4RetStat == RIP_SPACE_START)
                            {
                                break;
                            }
                            else if (i4RetStat == RIP_FAILURE)
                            {
                                (pRipIfRec->RipIfaceStats.
                                 u4RipSendUpdateAndStartSpaceTmrFailCount)++;
                                return ((INT1) i4RetStat);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                pRtSend = (tRipRtEntry *)
                    TMO_DLL_First (&pRipCxtEntry->RipCxtSummaryList);
            }
            while (pRtSend != NULL)
            {
                pRtSend =
                    (tRipRtEntry *) ((FS_ULONG) pRtSend - (UINT2) i2OffSet);
                /*
                 *Route entries are fall under Interface 
                 *Network will be added to update list.
                 *
                 * */
                if ((pRipIfRec != NULL) && (pRtSend->RtInfo.u4DestNet ==
                                            (u4IfAddr & pRtSend->RtInfo.
                                             u4DestMask)))
                {
                    if (u1BcastFlag == RIP_SPACE_UPDATE)
                    {
                        pRtTemp = pRtSend;
                    }
                    else
                    {
                        pRtTemp = (tRipRtEntry *)
                            TMO_DLL_First (&pRipCxtEntry->RipCxtRtList);
                    }
                    while (pRtTemp != NULL)
                    {
                        pRtTemp =
                            (tRipRtEntry *) ((FS_ULONG) pRtTemp -
                                             (UINT2) i2OffSet);
                        /*
                         *Interface address net different from 
                         *summary route, route information on the same interface
                         *has to send summary route info on other routes it has to
                         *send summary and normal route info.
                         */
                        u4DestMask = pRtSend->RtInfo.u4DestMask;
                        if ((pRtTemp->RtInfo.u4DestNet & u4DestMask) ==
                            (pRtSend->RtInfo.u4DestNet & pRtSend->RtInfo.
                             u4DestMask))
                        {
                            rip_copy_valid_entries (pRipInfoBlk, pu2NoOfRoutes,
                                                    u2If, pRtTemp,
                                                    pRipCxtEntry);
                            (*pu2NoOfRoutes)++;
                            if (*pu2NoOfRoutes == u2RtsToCompose)
                            {
                                i4RetStat =
                                    RipSendUpdateAndStartSpaceTmr (u1Version,
                                                                   u1BcastFlag,
                                                                   u4DestNet,
                                                                   u2DestUdpPort,
                                                                   pRipInfoBlk,
                                                                   u2If,
                                                                   &u2RtsToCompose,
                                                                   &u2NoOfPktsSend,
                                                                   pu2NoOfRoutes,
                                                                   pRipIfRec,
                                                                   pRipCxtEntry,
                                                                   &(pRtTemp->
                                                                     RipCxtRtNode));
                                /*
                                 *If the space timer started break the 
                                 * */
                                if (i4RetStat == RIP_SPACE_START)
                                {
                                    break;
                                }
                                else if (i4RetStat == RIP_FAILURE)
                                {
                                    if (pRipIfRec != NULL)
                                    {
                                        (pRipIfRec->RipIfaceStats.
                                         u4RipSendUpdateAndStartSpaceTmrFailCount)++;
                                    }
                                    return ((INT1) i4RetStat);
                                }
                            }
                        }
                        pRtTemp = (tRipRtEntry *)
                            TMO_DLL_Next (&pRipCxtEntry->RipCxtRtList,
                                          (tTMO_DLL_NODE *) & pRtTemp->
                                          RipCxtRtNode);
                    }
                }
                else
                {
                    RipAddAggRtToUpdateMsg (pRipInfoBlk, pu2NoOfRoutes,
                                            pRtSend, pRipCxtEntry);
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Added Summary Route To Update \n");
                    (*pu2NoOfRoutes)++;
                }
                pRtSend = (tRipRtEntry *)
                    TMO_DLL_Next (&pRipCxtEntry->RipCxtSummaryList,
                                  (tTMO_DLL_NODE *) & pRtSend->RipCxtRtNode);
                /*
                 * One datagram is full,
                 * Send it over the specified interface(s).
                 */
                if (*pu2NoOfRoutes == u2RtsToCompose)
                {
                    i4RetStat =
                        RipSendUpdateAndStartSpaceTmr (u1Version, u1BcastFlag,
                                                       u4DestNet, u2DestUdpPort,
                                                       pRipInfoBlk, u2If,
                                                       &u2RtsToCompose,
                                                       &u2NoOfPktsSend,
                                                       pu2NoOfRoutes, pRipIfRec,
                                                       pRipCxtEntry,
                                                       (tTMO_DLL_NODE *) (VOID
                                                                          *)
                                                       pRtSend);
                    /*
                     *If the space timer started break the 
                     * */
                    if (i4RetStat == RIP_SPACE_START)
                    {
                        break;
                    }
                    else if (i4RetStat == RIP_FAILURE)
                    {
                        if (pRipIfRec != NULL)
                        {
                            (pRipIfRec->RipIfaceStats.
                             u4RipSendUpdateAndStartSpaceTmrFailCount)++;
                        }
                        return ((INT1) i4RetStat);
                    }
                }
            }

            break;
        case RIP_TRIG_UPDATE:
            RipUpdateAllAggForSummaryEnable (pRipCxtEntry);
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC,
                     RIP_NAME, "Updated All The Summary Routes \n");

            InParams.pRoot = pRipCxtEntry->pRipRoot;
            InParams.i1AppId = ALL_ROUTING_PROTOCOL;
            InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
            MEMSET (apAppSpecInfo, 0,
                    MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
            OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
            OutParams.Key.pKey = NULL;

            i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
            if (i4OutCome == TRIE_SUCCESS)
            {
                /* Default route */
                for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS;
                     i4AppEntry++)
                {
                    if (((tRipRtEntry **)
                         (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
                    {
                        (pSummaryRt) = ((tRipRtEntry **)
                                        (OutParams.pAppSpecInfo))[i4AppEntry];
                    }
                }
                if ((pSummaryRt != NULL) &&
                    RIP_RT_CHANGED_STATUS (pSummaryRt) != 0)
                {
                    /* Default route addition have caused 
                     * triggered update.So add the 
                     * default route to update message since 
                     * interface specific 
                     * information is not available here*/
                    if (u1UpdateSendFlag != RIP_UPDATE_NOT_SEND)
                    {
                        rip_copy_valid_entries (pRipInfoBlk,
                                                pu2NoOfRoutes, u2If,
                                                pSummaryRt, pRipCxtEntry);
                        (*pu2NoOfRoutes)++;
                    }
                    RIP_RT_RESET_CHANGED_STATUS (pSummaryRt);
                }
            }
            i4OutCome = TRIE_SUCCESS;
            InParams.i1AppId = (OTHERS_ID - 1);
            MEMSET (&OutParams, 0, sizeof (tOutputParams));
            while (i4OutCome == TRIE_SUCCESS)
            {
                /* Get the next entry from the rip database */
                i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
                if (i4OutCome == TRIE_FAILURE)
                {
                    break;
                }

                pSummaryRt = (tRipRtEntry *) OutParams.pAppSpecInfo;

                /* Triggered update */
                if (RIP_RT_CHANGED_STATUS (pSummaryRt) != 0)
                {
                    /* Add the summary route */
                    if (u1UpdateSendFlag != RIP_UPDATE_NOT_SEND)
                    {
                        RipAddAggRtToUpdateMsg (pRipInfoBlk, pu2NoOfRoutes,
                                                pSummaryRt, pRipCxtEntry);
                        (*pu2NoOfRoutes)++;
                    }
                    RIP_RT_RESET_CHANGED_STATUS (pSummaryRt);
                }
                else
                {
                    au4Indx[0] = RIP_HTONL (pSummaryRt->RtInfo.u4DestNet);
                    au4Indx[1] = RIP_HTONL (pSummaryRt->RtInfo.u4DestMask);
                    MEMSET (&OutParams, 0, sizeof (tOutputParams));
                    continue;
                }
                /*
                 * One datagram is full,
                 * Send it over the specified interface(s).
                 */
                if ((u1UpdateSendFlag != RIP_UPDATE_NOT_SEND) &&
                    (*pu2NoOfRoutes == u2RtsToCompose))
                {
                    rip_send_update_datagram (u1Version, u1BcastFlag,
                                              u4DestNet, u2DestUdpPort,
                                              pRipInfoBlk, u2If,
                                              u2RtsToCompose, pRipCxtEntry);
                    (*pu2NoOfRoutes) = 0;
                }

                au4Indx[0] = RIP_HTONL (pSummaryRt->RtInfo.u4DestNet);
                au4Indx[1] = RIP_HTONL (pSummaryRt->RtInfo.u4DestMask);
                if (pSummaryRt->RtInfo.u2IfCounter == 0)
                {
                    /* delete the summary route from 
                     * forwarding table
                     * and from trie as there is no 
                     * rip route falling under this 
                     * aggregation*/
                    RipDeleteSummarySinkRt (pSummaryRt, pRipCxtEntry);
                    RipDeleteSummaryRt (pSummaryRt, pRipCxtEntry);
                }
                MEMSET (&OutParams, 0, sizeof (tOutputParams));
            }
            break;
        default:
            break;
    }
    return RIP_SUCCESS;

}

/**************************************************************************/
/* Function Name     :  RipAddDefRtToUpdate                               */
/*                                                                        */
/* Description       :  This function adds a default route to the update  */
/*                      being sent over an interface                     */
/*                                                                        */
/* Input(s)          :  pRipInfoBlk: a pointer to the route information   */
/*                      block which has route structure, interface        */
/*                      through which the route has been learnt,Ifcounter */
/*                      value for that route.                             */
/*                      pu2NoOfRoutes: the address of the variable which  */
/*                      is holding the current number of routes in the    */
/*                      packet.                                           */
/*                                                                        */
/* Output(s)         :  pRipInfoBlk                                       */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/
VOID
RipAddDefRtToUpdate (tRipInfoBlk * pRipInfoBlk, UINT2 *pu2NoOfRoutes,
                     UINT2 u2If, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4DefRtInstallStatus;

    pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.au1RouteTag[0]
        = (UINT1) (0xffffffff & 0x000000ff);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.au1RouteTag[1]
        = (UINT1) (0xffffffff & 0x000000ff);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4DestNet = 0;

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4SubnetMask = 0;

    if (u2If != RIPIF_INVALID_INDEX)
    {
        if (pRipIfRec != NULL)
        {
            u4DefRtInstallStatus = (UINT4) (pRipIfRec->RipIfaceCfg.
                                            u1DefRtInstallStatus);
            (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric
                = (UINT4) RIP_HTONL (u4DefRtInstallStatus);
            UNUSED_PARAM (u4DefRtInstallStatus);
        }
    }
    else
    {
        (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric
            = RIP_HTONL (RIP_DEF_METRIC);
    }

    /*
     * We need to store the interface index also for the
     * application of split horizon while sending.
     */

    (pRipInfoBlk[*pu2NoOfRoutes]).u2If = RIPIF_INVALID_INDEX;
    (pRipInfoBlk[*pu2NoOfRoutes]).u2IfCounter = 0;

}

/**************************************************************************/
/* Function Name     :  RipCheckAggRtSplitHorizonStatus                   */
/*                                                                        */
/* Description       :  This function check if all the routes falling     */
/*                      under the inputted summary are learnt over the    */
/*                      interface over which regular update is to be sent.*/
/*                      It set a flag if atlest one route is learnt over  */
/*                      a different interface and return from the function*/
/*                                                                        */
/* Input(s)          :  pSummaryRt : Summary route                        */
/*                      u2If       : interface over which th update need  */
/*                                   to be sent                           */
/*                                                                        */
/* Output(s)         :  pi1SendSummryFlag                                 */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/
VOID
RipCheckAggRtSplitHorizonStatus (UINT4 u4Address, UINT4 u4Mask,
                                 UINT2 u2IfIndex, INT1 *pi1SendSummryFlag,
                                 tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRt = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */
    UINT4               u4IfIndex;
    UINT4               u4RtAddr;
    UINT4               u4RtMask;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];

    NetIpv4GetCfaIfIndexFromPort (u2IfIndex, &u4IfIndex);
    *pi1SendSummryFlag = RIP_NO_SEND_SUMMARY;

    /* HTONL is needed as we store in trie in the network byte order */
    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry ((&InParams), (&OutParams));
    if (i4OutCome != TRIE_FAILURE)
    {
        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if ((((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry]
                 != NULL) && (i4AppEntry != (OTHERS_ID - 1)))
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                break;
            }
        }

        if ((pRipRt != NULL) && (pRipRt->RtInfo.u4RowStatus == ACTIVE))
        {
            if (pRipRt->RtInfo.u4RtIfIndx != u2IfIndex)
            {
                *pi1SendSummryFlag = RIP_SEND_SUMMARY;
                return;
            }
        }
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    }
    i4OutCome = TRIE_SUCCESS;
    while (i4OutCome == TRIE_SUCCESS)
    {
        i4OutCome = TrieGetNextEntry ((&InParams), (&OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                if (i4AppEntry != (OTHERS_ID - 1))
                {
                    break;
                }
            }
        }
        if (pRipRt != NULL)
        {
            u4RtAddr = pRipRt->RtInfo.u4DestNet;
            u4RtMask = pRipRt->RtInfo.u4DestMask;
            if (pRipRt->RtInfo.u2RtProto == (OTHERS_ID - 1))
            {
                au4Indx[0] = RIP_HTONL (u4RtAddr);
                au4Indx[1] = RIP_HTONL (u4RtMask);
                MEMSET (apAppSpecInfo, 0,
                        MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
                continue;
            }

            /* We have exited the aggregation range */
            if (u4Address != (u4RtAddr & u4Mask))
            {
                break;
            }

            if (pRipRt->RtInfo.u4RowStatus == IPFWD_ACTIVE)
            {
                if (pRipRt->RtInfo.u4RtIfIndx != u2IfIndex)
                {
                    *pi1SendSummryFlag = RIP_SEND_SUMMARY;
                    return;
                }
            }
            au4Indx[0] = RIP_HTONL (u4RtAddr);
            au4Indx[1] = RIP_HTONL (u4RtMask);
            MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
        }
    }
    return;
}

/**************************************************************************/
/* Function Name     :  RipAddRipRtToUpdateForSummaryEnable               */
/*                                                                        */
/* Description       :  This function adds the rip routes that fall under */
/*                      under the summary route(inputted ip/mask) to      */
/*                      the update message                                */
/*                                                                        */
/* Input(s)          :  u4Address  : summary route address                */
/*                      u4Mask     : summary mask                         */
/*                      pRipInfoBlk: a pointer to the route information   */
/*                      block which has route structure, interface        */
/*                      through which the route has been learnt,Ifcounter */
/*                      value for that route.                             */
/*                      u2If : The interface index                        */
/*                      pu2NoOfRoutes: the address of the variable which  */
/*                      is holding the current number of routes in the    */
/*                      packet.                                           */
/*                                                                        */
/* Output(s)         :  pRipInfoBlk                                       */
/*                      pu2NoOfRoutes                                     */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/

VOID
RipAddRipRtToUpdateForSummaryEnable (tRipTxInfo * pRipTxInfo,
                                     UINT4 u4Address, UINT4 u4Mask,
                                     tRipInfoBlk * pRipInfoBlk,
                                     UINT2 *pu2NoOfRoutes,
                                     tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRt = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];    /* Index for Trie is made of 2 members */
    UINT4               u4RtAddr;
    UINT4               u4RtMask;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];

    /* HTONL is needed as we store in trie in the network byte order */
    au4Indx[0] = RIP_HTONL (u4Address);
    au4Indx[1] = RIP_HTONL (u4Mask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry ((&InParams), (&OutParams));

    if (i4OutCome != TRIE_FAILURE)
    {
        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL
                && (i4AppEntry != (OTHERS_ID - 1)))
            {
                pRipRt =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                break;
            }
        }

        if ((pRipRt != NULL) && (pRipRt->RtInfo.u4RowStatus == ACTIVE))
        {
            rip_copy_valid_entries (pRipInfoBlk, pu2NoOfRoutes,
                                    pRipTxInfo->u2IfIndex, (VOID *) pRipRt,
                                    pRipCxtEntry);
            (*pu2NoOfRoutes)++;
        }
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
    }

    i4OutCome = TRIE_SUCCESS;
    while (i4OutCome == TRIE_SUCCESS)
    {
        if ((*pu2NoOfRoutes) == (pRipTxInfo->u2RtsToCompose - 1))
        {
            /* One datagram is full, Send it over the specified interface(s). */
            if (rip_send_update_datagram (pRipTxInfo->u1ResponseVersion,
                                          pRipTxInfo->u1BcastFlag,
                                          pRipTxInfo->u4DestNet,
                                          pRipTxInfo->u2DestUdpPort,
                                          pRipInfoBlk,
                                          pRipTxInfo->u2IfIndex,
                                          pRipTxInfo->u2RtsToCompose,
                                          pRipCxtEntry) == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed To Send The Generated Update Over The ,"
                         "Specified Interface  \n");
                return;
            }
            (*pu2NoOfRoutes) = 0;
            RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC,
                     RIP_NAME,
                     "Update Successfully Send Over The Specified Interface\n");
        }
        i4OutCome = TrieGetNextEntry ((&InParams), (&OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                if (i4AppEntry != (OTHERS_ID - 1))
                {
                    break;
                }
            }
        }

        if (pRipRt != NULL)
        {
            u4RtAddr = pRipRt->RtInfo.u4DestNet;
            u4RtMask = pRipRt->RtInfo.u4DestMask;
            if (pRipRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                au4Indx[0] = RIP_HTONL (u4RtAddr);
                au4Indx[1] = RIP_HTONL (u4RtMask);
                MEMSET (apAppSpecInfo, 0,
                        MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
                continue;
            }
            /* We have exited the aggregation range */
            if (u4Address != (u4RtAddr & u4Mask))
            {
                break;
            }

            if (pRipRt->RtInfo.u4RowStatus == IPFWD_ACTIVE)
            {
                rip_copy_valid_entries (pRipInfoBlk, pu2NoOfRoutes,
                                        pRipTxInfo->u2IfIndex, (VOID *) pRipRt,
                                        pRipCxtEntry);
                (*pu2NoOfRoutes)++;
            }
            au4Indx[0] = RIP_HTONL (u4RtAddr);
            au4Indx[1] = RIP_HTONL (u4RtMask);
            MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
        }
        else
        {
            break;
        }
    }
}

/**************************************************************************/
/* Function Name     :  RipAddAggRtToUpdateMsg                        */
/*                                                                        */
/* Description       :  This function add the summary route to update     */
/*                      message that will be send                         */
/*                                                                        */
/* Input(s)          :  pRipInfoBlk : The block to which summary route is */
/*                                    added                               */
/*                      pu2NoOfRoutes : Index of current route in the blk */
/*                      pRt : The summary route that is added to blk      */
/*                                                                        */
/* Output(s)         :  pRipInfoBlk                                      */
/*                                                                        */
/* Returns           :  none                                              */
/**************************************************************************/

VOID
RipAddAggRtToUpdateMsg (tRipInfoBlk * pRipInfoBlk,
                        UINT2 *pu2NoOfRoutes, tRipRtEntry * pRt,
                        tRipCxt * pRipCxtEntry)
{
    UINT2               u2RtTag = 0;
    tRipRtInfo          TempRt;

    MEMSET (&TempRt, 0, sizeof (tRipRtInfo));

    TempRt.u4DestNet = pRt->RtInfo.u4DestNet;
    TempRt.u4DestMask = pRt->RtInfo.u4DestMask;
    TempRt.u2RtType = pRt->RtInfo.u2RtType;
    TempRt.u4NextHop = pRt->RtInfo.u4NextHop;
    TempRt.u4RtIfIndx = pRt->RtInfo.u4RtIfIndx;
    TempRt.i4Metric1 = pRt->RtInfo.i4Metric1;
    TempRt.u4RtNxtHopAS = pRt->RtInfo.u4RtNxtHopAS;
    TempRt.u4NextHop = pRt->RtInfo.u4NextHop;

    if (RipApplyInOutFilter (pRipCxtEntry->pDistributeOutFilterRMap,
                             &TempRt, 0) != RIP_SUCCESS)
    {
        /* Stop processing this route. */
        return;
    }
    UNUSED_PARAM (pRipCxtEntry);
    u2RtTag = (UINT2) ((TempRt.u4RtNxtHopAS) & 0x0000ffff);
    u2RtTag = RIP_HTONS (u2RtTag);
    MEMCPY (pRipInfoBlk[*pu2NoOfRoutes].Route.au1RouteTag, &u2RtTag,
            sizeof (UINT2));

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4DestNet
        = RIP_HTONL (TempRt.u4DestNet);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4SubnetMask
        = RIP_HTONL (TempRt.u4DestMask);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric
        = RIP_HTONL ((UINT4) TempRt.i4Metric1);

    (pRipInfoBlk[*pu2NoOfRoutes]).u2IfCounter = 0;

    (pRipInfoBlk[*pu2NoOfRoutes]).u2If = (UINT2) TempRt.u4RtIfIndx;

}

/**************************************************************************/
/* Function Name     :  RipUpdateAllAggRtForRtChange                      */
/*                                                                        */
/* Description       :  This function initializes all the aggregated      */
/*                      routes and summary routes and update them with    */
/*                      the proper values.This function is mainly used    */
/*                      when a static or redistributed route gets added,  */
/*                      deleted or updated                                */
/*                                                                        */
/* Input(s)          :  pRt : The route that got added or deleted or      */
/*                            updated                                     */
/*                      pRipCxtEntry: Pointer to the context Info         */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  none                                              */
/**************************************************************************/
VOID
RipUpdateAllAggRtForRtChange (tRipRtEntry * pRt, tRipCxt * pRipCxtEntry)
{
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tRipRtEntry        *pSummaryRt = NULL;
    UINT4               u4Address = 0;
    UINT4               u4Mask = 0;
    tRipRtEntry        *pRtTemp = NULL;
    tTMO_DLL_NODE      *pListNode = NULL;
    INT2                i2OffSet =
        (INT2) RIP_OFFSET (tRipRtEntry, RipCxtRtNode);

    u4Address = pRt->RtInfo.u4DestNet;
    u4Mask = pRt->RtInfo.u4DestMask;

    if (u4Address == 0)
    {
        /* Default route, so no need to add summary route for the same */
        return;
    }

    /* Get the interface index over which aggregated routes have been 
     * configured*/
    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        pRipIfAggRtInfo = RipCheckIfAgg ((UINT2) pRipIfAggTblRoot->u4IfIndex,
                                         u4Address, u4Mask, pRipCxtEntry);
        if (pRipIfAggRtInfo != NULL)
        {
            RipUpdateIfAggRt (pRipIfAggRtInfo,
                              (UINT2) pRipIfAggTblRoot->u4IfIndex);
            RipUpdateIfAggRtAsSinkRt (pRipIfAggRtInfo);
            pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;
        }
    }

    /* Updating the summary route */
    pSummaryRt = RipCheckSummaryAgg (u4Address, u4Mask, pRipCxtEntry);
    if (pSummaryRt == NULL)
    {
        /* No summary route exist currently for the specified route address
         * and mask.So add a new summary route for the same */
        pSummaryRt = RipAddSummaryRoute (u4Address, pRipCxtEntry);
        if (pSummaryRt == NULL)
        {
            return;
        }
    }
    else
    {
        RipUpdateSummaryAggRoute (pSummaryRt, pRipCxtEntry);
        TMO_DLL_Scan (&(pRipCxtEntry->RipCxtSummaryList),
                      pListNode, tTMO_DLL_NODE *)
        {
            pRtTemp = RIP_ROUTE_INFO_PTR_FROM_RTLST (pListNode, i2OffSet);
            if (pRtTemp->RtInfo.u4DestNet == pSummaryRt->RtInfo.u4DestNet)
            {
                pRtTemp->RtInfo.i4Metric1 = pSummaryRt->RtInfo.i4Metric1;
            }
        }
        if (pSummaryRt->RtInfo.u2IfCounter == 0)
        {
            RipDeleteSummarySinkRt (pSummaryRt, pRipCxtEntry);
            RipDeleteSummaryRt (pSummaryRt, pRipCxtEntry);
            return;
        }
        RipUpdateSummaryRtAsSinkRt (pSummaryRt, pRipCxtEntry);
    }
    if (RIP_RT_CHANGED_STATUS (pRt) == 0)
    {
        pSummaryRt->RtInfo.u1Status = 0;
    }
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME,
             "Update Successfully Both Summary and Interface Specific ,"
             "Aggregation For A Route Change\n");
}

/**************************************************************************/
/* Function Name     :  RipUpdateSummaryAggRoute                          */
/*                                                                        */
/* Description       :  This function update the summary routes with      */
/*                      proper values of metric, count                    */
/*                                                                        */
/* Input(s)          :  pSummryRt : the summary route for which  will be  */
/*                                  deleted from rip database             */
/*                      pRipCxtEntry: Pointer to the context Info         */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  none                                              */
/**************************************************************************/
VOID
RipUpdateSummaryAggRoute (tRipRtEntry * pSummaryRt, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRt = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];
    UINT4               u4RtMask = 0;
    UINT4               u4RtAddr = 0;
    UINT4               u4AggAddress;
    UINT4               u4AggMask;
    INT4                i4NewMetric = RIP_INFINITY;
    UINT4               u4NewTag = 0;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    UINT1               u1AggRangeExeeded = RIP_FALSE;

    /* Initialising the number of subnet routes falling under this aggregation
     * as zero*/
    pSummaryRt->RtInfo.u2IfCounter = 0;
    u4AggAddress = pSummaryRt->RtInfo.u4DestNet;
    u4AggMask = pSummaryRt->RtInfo.u4DestMask;

    /* Traverse the rip trie and get each route */
    au4Indx[0] = RIP_HTONL (u4AggAddress);
    au4Indx[1] = RIP_HTONL (u4AggMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
    if (i4OutCome != TRIE_FAILURE)
    {
        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if ((((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry]
                 != NULL) && (i4AppEntry != (OTHERS_ID - 1)))
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];

                if ((pRipRt != NULL) && (pRipRt->RtInfo.u4RowStatus == ACTIVE))
                {
                    i4NewMetric = RIP_MIN_METRIC (i4NewMetric,
                                                  pRipRt->RtInfo.i4Metric1);
                    u4NewTag = pRipRt->RtInfo.u4RtNxtHopAS;
                    pSummaryRt->RtInfo.u2IfCounter++;
                }

            }
        }
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    }

    i4OutCome = TRIE_SUCCESS;

    while (i4OutCome == TRIE_SUCCESS)
    {
        /* get the next entry from the rip database */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];

                if (pRipRt == NULL)
                {
                    continue;
                }
                u4RtAddr = pRipRt->RtInfo.u4DestNet;
                u4RtMask = pRipRt->RtInfo.u4DestMask;

                if (i4AppEntry != (OTHERS_ID - 1))
                {

                    /* We have exited the aggregation range */
                    if (u4AggAddress != (u4RtAddr & u4AggMask))
                    {
                        u1AggRangeExeeded = RIP_TRUE;
                        break;
                    }

                    if ((pRipRt != NULL)
                        && (pRipRt->RtInfo.u4RowStatus == ACTIVE))
                    {
                        i4NewMetric = RIP_MIN_METRIC (i4NewMetric,
                                                      pRipRt->RtInfo.i4Metric1);
                        u4NewTag = pRipRt->RtInfo.u4RtNxtHopAS;
                        pSummaryRt->RtInfo.u2IfCounter++;
                    }
                }
            }
        }

        if (u1AggRangeExeeded == RIP_TRUE)
        {
            break;
        }
        au4Indx[0] = RIP_HTONL (u4RtAddr);
        au4Indx[1] = RIP_HTONL (u4RtMask);
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
    }

    if (pSummaryRt->RtInfo.u2IfCounter == 0)
    {
        pSummaryRt->RtInfo.i4Metric1 = RIP_INFINITY;
        RIP_RT_SET_CHANGED_STATUS (pSummaryRt);
        /* No subnet route exist in the rip trie that falls under this 
         * aggregation. So delete this summary aggregation*/
        /* Delete the sink route if it was added earlier as a sink route */
        /* RipDeleteSummarySinkRt (pSummaryRt);
           RipDeleteSummaryRt (&pSummaryRt); */
        return;
    }

    if (i4NewMetric != pSummaryRt->RtInfo.i4Metric1)
    {
        RIP_RT_SET_CHANGED_STATUS (pSummaryRt);
        pSummaryRt->RtInfo.u4RowStatus = ACTIVE;
    }
    if (u4NewTag != pSummaryRt->RtInfo.u4RtNxtHopAS)
    {
        RIP_RT_SET_CHANGED_STATUS (pSummaryRt);
        pSummaryRt->RtInfo.u4RowStatus = ACTIVE;
    }
    pSummaryRt->RtInfo.i4Metric1 = i4NewMetric;
    pSummaryRt->RtInfo.u4RtNxtHopAS = u4NewTag;
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME, "Summary Route Updated Successfully\n");
}

/**************************************************************************/
/* Function Name     :  RipUpdateAllAggForSummaryEnable                   */
/*                                                                        */
/* Description       :  This function update the all summary routes with  */
/*                      proper values of metric, count for any route      */
/*                      change                                            */
/*                                                                        */
/* Input(s)          :  None                                              */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  none                                              */
/**************************************************************************/
VOID
RipUpdateAllAggForSummaryEnable (tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRipRt = NULL;
    tRipRtEntry        *pSummaryRt = NULL;
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    INT4                i4OutCome = TRIE_SUCCESS;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];
    UINT4               u4RtMask;
    UINT4               u4RtAddr;
    UINT4               u4SummaryAddr = 0;
    UINT4               u4IfIndex = 0;
    VOID               *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];

    /* Traverse the rip trie and get each route  for which */
    /* changed flag is set */
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = NULL;

    i4OutCome = TRIE_SUCCESS;
    while (i4OutCome == TRIE_SUCCESS)
    {
        /* get the next entry from the rip database */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }

        for (i4AppEntry = 0; i4AppEntry < MAX_ROUTING_PROTOCOLS; i4AppEntry++)
        {
            if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry] != NULL)
            {
                (pRipRt) =
                    ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4AppEntry];
                if (i4AppEntry != (OTHERS_ID - 1))
                {
                    break;
                }
            }
        }

        if (pRipRt == NULL)
        {
            break;
        }
        else
        {
            u4RtAddr = pRipRt->RtInfo.u4DestNet;
            u4RtMask = pRipRt->RtInfo.u4DestMask;
            if (pRipRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                au4Indx[0] = RIP_HTONL (u4RtAddr);
                au4Indx[1] = RIP_HTONL (u4RtMask);
                MEMSET (apAppSpecInfo, 0,
                        MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
                continue;
            }

            if (RIP_RT_CHANGED_STATUS (pRipRt) != 0)
            {
                if (u4SummaryAddr != (u4RtAddr & RIP_SUMMARY_MASK))
                {
                    /* The summary route is not updated.Do the updation */
                    /* Get the summary route under which this route falls */
                    pSummaryRt =
                        RipCheckSummaryAgg (u4RtAddr, u4RtMask, pRipCxtEntry);
                    if (pSummaryRt == NULL)
                    {
                        /* Summary route does not exist for this route.
                         * Add a new summary aggregation for the same*/
                        pSummaryRt =
                            RipAddSummaryRoute (u4RtAddr, pRipCxtEntry);
                        /*
                         * when Trie Entry Not allocated returns NULL.
                         * */
                        if (pSummaryRt != NULL)
                        {
                            if (pSummaryRt->RtInfo.u2IfCounter == 0)
                            {
                                RipDeleteSummaryRt (pSummaryRt, pRipCxtEntry);
                                RipDeleteSummarySinkRt (pSummaryRt,
                                                        pRipCxtEntry);
                            }
                            u4SummaryAddr = pSummaryRt->RtInfo.u4DestNet;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        RipUpdateSummaryAggRoute (pSummaryRt, pRipCxtEntry);

                        RipUpdateRouteEntry (pSummaryRt, pRipCxtEntry);
                        if (pSummaryRt->RtInfo.u2IfCounter != 0)
                        {
                            RipUpdateSummaryRtAsSinkRt (pSummaryRt,
                                                        pRipCxtEntry);
                        }
                        u4SummaryAddr = pSummaryRt->RtInfo.u4DestNet;
                    }
                }
                /* Update the interface specific aggregation with the
                 * proper metric, count */

                /* Get the interface index over which aggregated routes 
                 * have been  configured*/
                TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst,
                              pRipIfAggTblRoot, tRipIfAggTblRoot *)
                {
                    u4IfIndex = pRipIfAggTblRoot->u4IfIndex;
                    /* Updating the interface specific routes */
                    pRipIfAggRtInfo =
                        RipCheckIfAgg ((UINT2) u4IfIndex, u4RtAddr, u4RtMask,
                                       pRipCxtEntry);
                    if (pRipIfAggRtInfo != NULL)
                    {
                        if (RIP_IF_AGG_RT_STATUS (pRipIfAggRtInfo) ==
                            RIP_IF_AGG_RT_DEF_STATUS)
                        {
                            RipUpdateIfAggRt (pRipIfAggRtInfo,
                                              (UINT2) u4IfIndex);
                            RipUpdateIfAggRtAsSinkRt (pRipIfAggRtInfo);
                        }
                    }
                }
            }
            au4Indx[0] = RIP_HTONL (u4RtAddr);
            au4Indx[1] = RIP_HTONL (u4RtMask);
            MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (VOID *));
        }
    }
    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        u4IfIndex = pRipIfAggTblRoot->u4IfIndex;
        /* All the interface specific aggregations have been 
         * updated over this interface have been updated.
         * Set their status from updated to default status*/
        RipUpdateIfAggRtStatus ((UINT2) u4IfIndex, pRipCxtEntry);
    }
}

/**************************************************************************/
/* Function Name     :  RipAddSummaryRoute                                */
/*                                                                        */
/* Description       :  This function adds a new summary route to the rip */
/*                      database and update the route with the proper     */
/*                      metric, count and sink rt flag.If summary is      */
/*                      enabled, add the summary route to rtm as sink     */
/*                      route according to the flag set                   */
/*                                                                        */
/* Input(s)          :  u4RtAddr : the rip route address for which        */
/*                                 summary route is to be added           */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  Added summary route / NULL                        */
/**************************************************************************/
tRipRtEntry        *
RipAddSummaryRoute (UINT4 u4RtAddr, tRipCxt * pRipCxtEntry)
{
    tRipRtEntry        *pSummryRt = NULL;
    UINT4               u4SummryAddr;

    /* Get the summary address */
    u4SummryAddr = RIP_GET_SUMMARY_ADDRESS (u4RtAddr);

    /* Add the summary route entry to the rip database */
    RIP_ALLOC_SUMMARY_ENTRY (pSummryRt);

    if (pSummryRt == NULL)
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC,
                 RIP_NAME, "Failed To Allocate Memory For Summary Route \n");
        return (NULL);
    }

    MEMSET (pSummryRt, 0, sizeof (tRipRtEntry));
    (pSummryRt)->RtInfo.i4Metric1 = RIP_INFINITY;
    (pSummryRt)->RtInfo.u2RtType = REJECT;
    (pSummryRt)->RtInfo.u2RtProto = (UINT2) OTHERS_ID;
    (pSummryRt)->RtInfo.u4DestMask = RIP_SUMMARY_MASK;
    (pSummryRt)->RtInfo.u4DestNet = u4SummryAddr;
    (pSummryRt)->RtInfo.u4NextHop = RIP_IF_AGG_NEXTHOP;
    (pSummryRt)->RtInfo.u1SinkRtFlag = RIP_NO_INSTALL_SINK_RT;
    (pSummryRt)->RtInfo.u4RtIfIndx = RIPIF_INVALID_INDEX;
    (pSummryRt)->RtInfo.i4CxtId = pRipCxtEntry->i4CxtId;

    RipRedDbNodeInit (&(pSummryRt->RtInfo.RouteDbNode), RIP_RED_DYN_RT_INFO);
    pSummryRt->RtInfo.u4TempDestMask = pSummryRt->RtInfo.u4DestMask;
    pSummryRt->RtInfo.u4TempDestNet = pSummryRt->RtInfo.u4DestNet;

    if (pRipCxtEntry->RipRRDGblCfg.u1RipRRDRouteTagType == MANUAL)
    {
        (pSummryRt)->RtInfo.u4RtNxtHopAS = 0xFFFF0000;
        pSummryRt->RtInfo.u4RtNxtHopAS |=
            (UINT4) (pRipCxtEntry->RipRRDGblCfg.u2RipRRDRtTag);
    }
    else
    {
        (pSummryRt)->RtInfo.u4RtNxtHopAS = 0xFFFFFFFF;
    }
    (pSummryRt)->RtInfo.u2IfCounter = 0;

    /* Update the summary route metric, count */
    RipUpdateSummaryAggRoute (pSummryRt, pRipCxtEntry);

    /* Add the summary route to rip database */
    (pSummryRt)->RtInfo.u4RowStatus = ACTIVE;
    if (rip_add_new_route (pSummryRt, pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pSummryRt)));
        RIP_SUMMARY_FREE ((pSummryRt));
        return (NULL);
    }
    pSummryRt->RtInfo.u1Operation = RIPHA_ADD_SUM_ROUTE;
    RipRedDbUtilAddRtInfo (pSummryRt);
    RipRedSyncDynInfo ();

    /* Add the summary route as sink route to RTM */
    if (RipUpdateSummaryRtAsSinkRt (pSummryRt, pRipCxtEntry) == RIP_FAILURE)
    {
        return (NULL);
    }
    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME, "Summary Route Successfully Added To Rip Trie\n");
    return (pSummryRt);
}

/**************************************************************************/
/* Function Name     :  RipDeleteSummaryRt                                */
/*                                                                        */
/* Description       :  This function deletes the summary route from rip  */
/*                      database                                          */
/*                                                                        */
/* Input(s)          :  pSummryRt : the summary route for which  will be  */
/*                                  deleted from rip database             */
/*                      pRipCxtEntry: Pointer to the context Info         */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/
VOID
RipDeleteSummaryRt (tRipRtEntry * pSummryRt, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome;
    UINT4               au4Indx[2];    /* Index of Trie is made-up of
                                       address and mask */

    au4Indx[0] = RIP_HTONL (pSummryRt->RtInfo.u4DestNet);
    au4Indx[1] = RIP_HTONL (pSummryRt->RtInfo.u4DestMask);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (INT1) (pSummryRt->RtInfo.u2RtProto - 1);

    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieDeleteEntry (&(InParams), &(OutParams),
                                 pSummryRt->RtInfo.u4NextHop);

    if (i4OutCome == TRIE_SUCCESS)
    {
        pSummryRt->RtInfo.u1Operation = RIPHA_DEL_SUM_ROUTE;
        RipRedDbUtilAddRtInfo (pSummryRt);
        RipRedSyncDynInfo ();

        if (pSummryRt->RtInfo.u2RtProto == OTHERS_ID)
        {
            pRipCxtEntry->u4TotalSummaryCount--;
        }
        else
        {
            pRipCxtEntry->u4TotalEntryCount--;
        }

        TMO_DLL_Delete (&(pRipCxtEntry->RipCxtSummaryList),
                        (tTMO_DLL_NODE *) & pSummryRt->RipCxtRtNode);

        RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pSummryRt)));
        RIP_SUMMARY_FREE ((pSummryRt));
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC,
                 RIP_NAME,
                 "Summary Route Successfully Deleted From Rip Trie\n");
    }
    else
    {
        RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag, pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC,
                 RIP_NAME, "Failed To Delete Summary Route From Rip Trie\n");
    }
}

/**************************************************************************/
/* Function Name     :  RipComposeRipRtFromIfAggRt                        */
/*                                                                        */
/* Description       :  This function compose rip route from interface    */
/*                      specific route                                    */
/*                                                                        */
/* Input(s)          :  pRipIfAggRtInfo : the interface specific route    */
/*                                                                        */
/* Output(s)         :  pRipRt : composed rip route                       */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipComposeRipRtFromIfAggRt (tRipIfAggRtInfo * pRipIfAggRtInfo,
                            tRipRtEntry ** pRipRt)
{
    tRipRtEntry        *pRipRoute = NULL;

    RIP_ALLOC_ROUTE_ENTRY (pRipRoute);
    if (pRipRoute == NULL)
    {
        return RIP_FAILURE;
    }
    MEMSET (pRipRoute, 0, sizeof (tRipRtEntry));
    RipRedDbNodeInit (&(pRipRoute->RtInfo.RouteDbNode), RIP_RED_DYN_RT_INFO);
    pRipRoute->RtInfo.i4Metric1 = (INT4) (pRipIfAggRtInfo->u2Metric);
    pRipRoute->RtInfo.u2RtType = REJECT;
    pRipRoute->RtInfo.u4RowStatus = (UINT4) pRipIfAggRtInfo->u1AggRtStatus;
    pRipRoute->RtInfo.u2RtProto = (UINT2) OTHERS_ID;
    pRipRoute->RtInfo.u4DestMask = pRipIfAggRtInfo->u4AggRtMask;
    pRipRoute->RtInfo.u4DestNet = pRipIfAggRtInfo->u4AggRtAddr;
    pRipRoute->RtInfo.u4NextHop = RIP_IF_AGG_NEXTHOP;
    pRipRoute->RtInfo.u4RtIfIndx = RIPIF_INVALID_INDEX;
    pRipRoute->RtInfo.u1SinkRtFlag = pRipIfAggRtInfo->u1SinkRtFlag;
    pRipRoute->RtInfo.u2IfCounter = pRipIfAggRtInfo->u2Count;

    *pRipRt = pRipRoute;
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipSetAggRtSplitHorizonStatus                     */
/*                                                                        */
/* Description       :  This function sets the metric of the aggregated   */
/*                      route based on split horizon checking.            */
/*                                                                        */
/* Input(s)          :  pRipInfo : Route over which split horizon check   */
/*                                 is done                                */
/*                      u2If : The interface index                        */
/*                                                                        */
/* Output(s)         :  pRipInfo : The updated route after split horizon  */
/*                                 check                                  */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipSetAggRtSplitHorizonStatus (tRipInfo * pRipInfo, UINT2 u2If,
                               tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4Address;
    UINT4               u4Mask;
    INT1                i1SendSummryFlag = RIP_SEND_SUMMARY;
    UINT2               u2SplitHorizonStatus;

    u4Address = RIP_NTOHL (RIP_DEST_NET (pRipInfo));
    u4Mask = RIP_NTOHL (RIP_SUBNET_MASK (pRipInfo));

    /* Check if atleat one route in the aggregation range is learnt over a
     * different interface */
    RipCheckAggRtSplitHorizonStatus (u4Address, u4Mask, u2If,
                                     &i1SendSummryFlag, pRipCxtEntry);

    pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }
    u2SplitHorizonStatus = pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus;

    /* All subnet routes under aggregation are learnt over this 
     * interface itself */
    if (i1SendSummryFlag != RIP_SEND_SUMMARY)
    {
        /* Split Horizon is enabled on the interface */
        if (u2SplitHorizonStatus == RIP_SPLIT_HORIZON)
        {
            /* Do not send this route over the. */
            (pRipIfRec->RipIfaceStats.u4RipSplitHorizonFailCount)++;
            return RIP_FAILURE;
        }
        else if (u2SplitHorizonStatus == RIP_SPLIT_HORZ_WITH_POIS_REV)
        {
            /* Set the route with metrc 16 */
            if ((pRipIfRec->u4Addr & pRipIfRec->u4NetMask) == u4Address)
            {
                RIP_METRIC (pRipInfo) = RIP_HTONL (RIP_INFINITY);
            }
        }
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  RipUpdateIfAggRtStatus                            */
/*                                                                        */
/* Description       :  This function traverse the interface specific     */
/*                      trie and update the route change status           */
/*                                                                        */
/* Input(s)          :  u2IfIndex : The interface index                   */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  RIP_SUCCESS / RIP_FAILURE                         */
/**************************************************************************/
INT1
RipUpdateIfAggRtStatus (UINT2 u2IfIndex, tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    INT4                i4OutCome = RIP_SUCCESS;
    UINT4               au4Indx[2];
    VOID               *pTrieRoot = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    /* Get the interface specific aggregation table's root from the SLL */
    pTrieRoot = RipGetIfAggTblRoot (u2IfIndex, pRipCxtEntry);
    if (pTrieRoot == NULL)
    {
        (pRipIfRec->RipIfaceStats.u4RipIfAggRtNullEntryCount)++;
        return RIP_FAILURE;
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    while (i4OutCome == TRIE_SUCCESS)
    {
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));

        if (i4OutCome == TRIE_SUCCESS)
        {
            pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
            if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) ==
                RIP_AUTO_SUMMARY_ENABLE)
            {
                pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;
            }
            else
            {
                if (pRipIfAggRtInfo->u2Status == RIP_IF_AGG_RT_UPDATED_VAL)
                {
                    pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;
                }
            }
            au4Indx[0] = RIP_HTONL (pRipIfAggRtInfo->u4AggRtAddr);
            au4Indx[1] = RIP_HTONL (pRipIfAggRtInfo->u4AggRtMask);
        }
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/* Function Name : RipFilterRouteSourceInCxt                              */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. pRipCxtEntry - RIP Context Pointer                  */
/*               : 2. pRipInfo - RIP route info                           */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
RipFilterRouteSourceInCxt (tRipCxt * pRipCxtEntry, tRipInfo * pRipInfo)
{
    UINT1               u1Distance = pRipCxtEntry->u1Distance;
#ifdef ROUTEMAP_WANTED

    if (pRipCxtEntry->pDistanceFilterRMap != NULL
        && pRipCxtEntry->pDistanceFilterRMap->u1Status == FILTERNIG_STAT_ENABLE)
    {
        tRtMapInfo          MapInfo;
        tRoute              TempRouteInfo;
        MEMSET (&MapInfo, 0, sizeof (MapInfo));
        MEMSET (&TempRouteInfo, 0, sizeof (TempRouteInfo));

        TempRouteInfo.u4DestNet =
            OSIX_NTOHL (pRipInfo->RipMesg.Route.u4DestNet);
        TempRouteInfo.u4NextHop = RIP_NTOHL (pRipInfo->RipMesg.Route.u4NextHop);
        TempRouteInfo.u4SubnetMask =
            OSIX_NTOHL (pRipInfo->RipMesg.Route.u4SubnetMask);
        IPVX_ADDR_INIT_FROMV4 (MapInfo.DstXAddr, TempRouteInfo.u4DestNet);
        IPVX_ADDR_INIT_FROMV4 (MapInfo.NextHopXAddr, TempRouteInfo.u4NextHop);
        IPV4_MASK_TO_MASKLEN (MapInfo.u2DstPrefixLen,
                              TempRouteInfo.u4SubnetMask);
        MapInfo.u4RouteTag = pRipInfo->RipMesg.Route.au1RouteTag[0];
        MapInfo.u4RouteTag =
            (MapInfo.u4RouteTag << 8) + pRipInfo->RipMesg.Route.au1RouteTag[1];
        if (RMapApplyRule2
            (&MapInfo,
             pRipCxtEntry->pDistanceFilterRMap->au1DistInOutFilterRMapName) ==
            RMAP_ENTRY_MATCHED)
        {
            u1Distance = pRipCxtEntry->pDistanceFilterRMap->u1RMapDistance;
        }
    }

#else
    UNUSED_PARAM (pRipInfo);
#endif
    return u1Distance;
}
