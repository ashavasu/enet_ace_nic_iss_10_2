/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripmiutl.c,v 1.21 2014/12/09 12:51:58 siva Exp $
 *
 * Description:This file contains Multiple Instance utility  
 *             routines.             
 *
 *******************************************************************/
#include "ripinc.h"
#include "fsmiriwr.h"

/**************************************************************************
 *Function Name     :  RipGetCxtInfoRecFrmIface.            
 *Description       :  This function returns the pointer to contextEntry     
 *Input(s)          :  u2IfaceId: Interface Index value. 
 *Output(s)         :  None                                     
 *Returns           :  pRipCxtEntry
**************************************************************************/
tRipCxt            *
RipGetCxtInfoRecFrmIface (UINT2 u2IfaceId)
{
    tRipCxt            *pRipCxtEntry = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    if (RipGetVcmSystemMode (RIP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        pRipIfRec = RipGetIfRecFromIndex ((UINT4) u2IfaceId);
        if (pRipIfRec == NULL)
        {
            return NULL;
        }
        pRipCxtEntry = pRipIfRec->pRipCxt;
        return pRipCxtEntry;
    }
    else
    {
        pRipCxtEntry = gRipRtr.apRipCxt[RIP_DEFAULT_CXT];
        return pRipCxtEntry;
    }
}

/**************************************************************************
* Function Name     :  RipSetContext.            
* Description       :  Sets the Global Context pointer to the Current context.
* Input(s)          :  i4CxtId - Context Id Value.
* Output(s)         :  None.                                     
* Returns           :  SNMP_SUCCESS - on Success otherwise SNMP_FAILURE.
**************************************************************************/
INT4
RipSetContext (UINT4 u4CxtId)
{
    if (RipGetVcmSystemMode (RIP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (gRipRtr.apRipCxt[u4CxtId] == NULL)
        {
            return SNMP_FAILURE;
        }
        RIP_MGMT_CXT = gRipRtr.apRipCxt[u4CxtId];

    }
    else
    {
        RIP_MGMT_CXT = gRipRtr.apRipCxt[RIP_DEFAULT_CXT];
    }
    return SNMP_SUCCESS;
}

/**************************************************************************
* Function Name     :  RipReleaseContext.            
* Description       :  Resets the Global Context pointer.
* Input(s)          :  None
* Output(s)         :  None.                                     
* Returns           :  None.
**************************************************************************/
INT4
RipReleaseContext ()
{
    RipResetContext ();
    return RIP_SUCCESS;
}

/**************************************************************************
* Function Name     :  RipResetContext.            
* Description       :  Resets the Global Context pointer.
* Input(s)          :  None
* Output(s)         :  None.                                     
* Returns           :  None.
**************************************************************************/

VOID
RipResetContext ()
{
    if (RipGetVcmSystemModeExt (RIP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        RIP_MGMT_CXT = NULL;
    }
}

/**************************************************************************
* Function Name     :  RipGetFirstCxtId.            
* Description       :  Gets the first context Id value.
* Input(s)          :  Pionter to the Context Id value.
* Output(s)         :  None.                                     
* Returns           :  RIP_SUCCESS on Success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipGetFirstCxtId (INT4 *pi4FsMIRipContextId)
{
    INT4                i4CxtId;
    tRipCxt            *pRipCxtEntry = NULL;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    for (i4CxtId = 0; i4CxtId < i4MaxCxt; i4CxtId++)
    {
        pRipCxtEntry = gRipRtr.apRipCxt[i4CxtId];
        if (pRipCxtEntry != NULL)
        {
            *pi4FsMIRipContextId = pRipCxtEntry->i4CxtId;
            return RIP_SUCCESS;
        }
    }
    *pi4FsMIRipContextId = (INT4) RIP_INVALID_CXT_ID;
    return RIP_FAILURE;
}

/**************************************************************************
* Function Name     :  RipGetNextCxtId.            
* Description       :  Gets the Next Context Id value of the given context Id.
* Input(s)          :  i4CxtId - Context Id value.
*                      pi4NextCxtId - Pointer to the Next Context Id.
* Output(s)         :  None.                                     
* Returns           :  RIP_SUCCESS on Success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipGetNextCxtId (INT4 i4CxtId, INT4 *pi4NextCxtId)
{
    tRipCxt            *pRipCxtEntry = NULL;
    INT4                i4NxtCxtId = 0;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    for (i4NxtCxtId = i4CxtId + 1; i4NxtCxtId < i4MaxCxt; i4NxtCxtId++)
    {
        pRipCxtEntry = gRipRtr.apRipCxt[i4NxtCxtId];
        if (pRipCxtEntry != NULL)
        {
            *pi4NextCxtId = pRipCxtEntry->i4CxtId;
            return RIP_SUCCESS;
        }
    }
    *pi4NextCxtId = (INT4) RIP_INVALID_CXT_ID;
    return RIP_FAILURE;
}

/**************************************************************************
* Function Name     :  RipGetCxtInfoRec.            
* Description       :  Returns the pointer to the context entry of the given 
                       context id.
* Input(s)          :  i4CxtId - Context Id value.
* Output(s)         :  None.                                     
* Returns           :  Pointer to the Context Entry.
**************************************************************************/

tRipCxt            *
RipGetCxtInfoRec (INT4 i4CxtId)
{
    return gRipRtr.apRipCxt[i4CxtId];

}

/**************************************************************************
* Function Name     :  RipGetCxtExists.            
* Description       :  Returns the pointer to the context entry of the given 
                       context id.
* Input(s)          :  i4CxtId - Context Id value.
* Output(s)         :  None.                                     
* Returns           :  Pointer to the Context Entry.
**************************************************************************/
INT4
RipCxtExists (INT4 i4CxtId)
{
    tRipCxt            *pRipCxtEntry = NULL;

    pRipCxtEntry = gRipRtr.apRipCxt[i4CxtId];

    if (pRipCxtEntry != NULL)
    {
        return RIP_SUCCESS;
    }
    return RIP_FAILURE;
}

/**************************************************************************
* Function Name     :  RipValidateCxtId.            
* Description       :  Validates the given Context Id value. 
* Input(s)          :  i4CxtId - Context Id value.
* Output(s)         :  None.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipValidateCxtId (INT4 i4CxtId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);
    if ((i4CxtId < RIP_DEFAULT_CXT) || (i4CxtId >= i4MaxCxt))
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/**************************************************************************
* Function Name     :  RipCxtIsExistInVcm.            
* Description       :  Checks the Context is created in VCM. 
* Input(s)          :  i4CxtId - Context Id value.
* Output(s)         :  None.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipCxtIsExistInVcm (INT4 i4CxtId)
{
    if (VcmIsL3VcExist ((UINT4) i4CxtId) == VCM_FALSE)
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/**************************************************************************
* Function Name     :  RipClearCxtInfo.            
* Description       :  Deletes all the Context related information. 
* Input(s)          :  Pinter to the context entry.
* Output(s)         :  Deletes all the context related inforamtion.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipClearCxtInfo (tRipCxt * pRipCxtEntry)
{
    tRipImportList     *pRipImportRt = NULL;
    tInputParams        InParams;
    tDeleteOutParams    DeleteOutParams;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    UINT4               u4Neighbors;
    INT2                i2SrcProto;

    /*
     *Deleting Trusted Neighbors.
     *
     * */
    for (u4Neighbors = 0;
         u4Neighbors < ((pRipCxtEntry->RipNbrListCfg).u4NumAuthorizedNbrs);
         u4Neighbors++)
    {
        nmhSetFsMIRip2TrustNBRRowStatus (pRipCxtEntry->i4CxtId,
                                         pRipCxtEntry->RipNbrListCfg.
                                         aNbrList[u4Neighbors].
                                         u4FsRip2TrustNBRIpAddr,
                                         RIP_NBR_LIST_DESTROY);
    }

    /*
     *Deleting Redistributed Routes From ImportList.
     *
     * */
    for (i2SrcProto = 0; i2SrcProto < MAX_ROUTING_PROTOCOLS; i2SrcProto++)
    {
        while ((pRipImportRt = (tRipImportList *)
                TMO_SLL_Get (&pRipCxtEntry->aRipImportList[i2SrcProto])) !=
               NULL)
        {
            RIP_FREE_IMPORT_ROUTE (pRipCxtEntry, pRipImportRt);
        }
    }

    /*
     *Stop All the timers.
     * */
    RIP_STOP_TIMER (RIP_TIMER_ID,
                    &((pRipCxtEntry->RipPassiveUpdTimer).Timer_node));
    RIP_STOP_TIMER (RIP_TIMER_ID,
                    &((pRipCxtEntry->RipTrigUpdTimer).Timer_node));
    RIP_STOP_TIMER (RIP_TIMER_ID, &((pRipCxtEntry->TripTrigTmr).TimerNode));

    if (pRipCxtEntry->pDistanceFilterRMap != NULL)
    {
        RIP_FREE_RMAP_FILTER_NODE (pRipCxtEntry,
                                   pRipCxtEntry->pDistanceFilterRMap);
        pRipCxtEntry->pDistanceFilterRMap = NULL;
    }
    if (pRipCxtEntry->pDistributeInFilterRMap != NULL)
    {
        RIP_FREE_RMAP_FILTER_NODE (pRipCxtEntry,
                                   pRipCxtEntry->pDistributeInFilterRMap);
        pRipCxtEntry->pDistributeInFilterRMap = NULL;
    }
    if (pRipCxtEntry->pDistributeOutFilterRMap != NULL)
    {
        RIP_FREE_RMAP_FILTER_NODE (pRipCxtEntry,
                                   pRipCxtEntry->pDistributeOutFilterRMap);
        pRipCxtEntry->pDistributeOutFilterRMap = NULL;
    }
    if (pRipCxtEntry->pRipRoot != NULL)
    {
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = pRipCxtEntry->i1RipId;
        DeleteOutParams.pKey = NULL;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
        DeleteOutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
        DeleteOutParams.u4NumEntries = MAX_ROUTING_PROTOCOLS;

        if (TrieDelete (&InParams, RipTrieRootDel, (VOID *) &DeleteOutParams) ==
            TRIE_FAILURE)
        {
            return RIP_FAILURE;
        }
    }
    return RIP_SUCCESS;
}

VOID
RipTrieRootDel (tDeleteOutParams * pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

/**************************************************************************
* Function Name     :  RipDelCxt.            
* Description       :  Deletes the Context related information. 
* Input(s)          :  i4CxtId - Context Id.
* Output(s)         :  Deletes  the context related inforamtion.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipDelCxt (INT4 i4CxtId)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsMIRipRowStatus (&u4ErrorCode, i4CxtId, DESTROY)
        == SNMP_FAILURE)
    {
        return (RIP_FAILURE);
    }

    if (nmhSetFsMIRipRowStatus (i4CxtId, DESTROY) == SNMP_FAILURE)
    {
        return (RIP_FAILURE);
    }
    return RIP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipGetVcmSystemMode                                */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
INT4
RipGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipGetVcmSystemModeExt                             */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE / VCM_SI_MODE                          */
/*                                                                           */
/*****************************************************************************/
INT4
RipGetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssHealthChkClearCtrRip                                    */
/*                                                                           */
/* Description    This function clears the rip counters.                     */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
RipHealthChkClearCtr ()
{

    tRipCxt            *pRipCxtEntry = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipPeerRec        *pRipCurPeerRec = NULL;
    INT4                i4MaxCxt = 0;
    INT4                i4ContextId = 0;
    UINT4               u4Rip2IfStatAddress = 0;
    UINT4               u4Rip2PrevIfStatAddress = 0;
    UINT4               u4Rip2PeerAddress = 0;
    UINT4               u4Rip2PrevPeerAddress = 0;
    UINT1              *pu1Domain = NULL;
    UINT1               au1PeerInfo[64];
    tSNMP_OCTET_STRING_TYPE Rip2PeerDomain;
    tSNMP_OCTET_STRING_TYPE Rip2PrevPeerDomain;

    MEMSET (au1PeerInfo, 0, sizeof (au1PeerInfo));
    Rip2PeerDomain.pu1_OctetList = au1PeerInfo;
    Rip2PrevPeerDomain.pu1_OctetList = au1PeerInfo;
    Rip2PeerDomain.i4_Length = 0;
    Rip2PrevPeerDomain.i4_Length = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams
                        [MAX_RIP_CONTEXT_SIZING_ID].u4PreAllocatedUnits);

    for (i4ContextId = 0; i4ContextId < i4MaxCxt; i4ContextId++)
    {
        if (gRipRtr.apRipCxt[i4ContextId] == NULL)
        {
            continue;
        }
        RipLock ();
        RIP_MGMT_CXT = gRipRtr.apRipCxt[i4ContextId];
        pRipCxtEntry = RIP_MGMT_CXT;
        pRipCxtEntry->RipGblStats.u4GlobalRouteChanges = 0;
        pRipCxtEntry->RipGblStats.u4GlobalQueries = 0;
        pRipCxtEntry->RipNbrListCfg.u4NumPktsDropped = 0;

        if (nmhGetFirstIndexRip2IfStatTable (&u4Rip2IfStatAddress) !=
            SNMP_FAILURE)
        {
            do
            {
                pRipIfRec =
                    RipGetIfRecFromAddr (u4Rip2IfStatAddress, pRipCxtEntry);
                if (pRipIfRec == NULL)
                {
                    RipUnLock ();
                    return;
                }
                pRipIfRec->RipIfaceStats.u4InBadPackets = 0;
                pRipIfRec->RipIfaceStats.u4InBadRoutes = 0;
                pRipIfRec->RipIfaceStats.u4SentTrigUpdates = 0;
                pRipIfRec->RipIfaceStats.u4SentPeriodicUpdates = 0;
                pRipIfRec->RipIfaceStats.u4InBadAuthPackets = 0;

                u4Rip2PrevIfStatAddress = u4Rip2IfStatAddress;
            }
            while (nmhGetNextIndexRip2IfStatTable (u4Rip2PrevIfStatAddress,
                                                   &u4Rip2IfStatAddress) !=
                   SNMP_FAILURE);

        }

        if (nmhGetFirstIndexRip2PeerTable (&u4Rip2PeerAddress,
                                           &Rip2PeerDomain) != SNMP_FAILURE)
        {
            do
            {
                pu1Domain = Rip2PeerDomain.pu1_OctetList;
                if ((pRipCurPeerRec =
                     rip_retrieve_record_for_peer (u4Rip2PeerAddress, pu1Domain,
                                                   pRipCxtEntry)) ==
                    (tRipPeerRec *) NULL)
                {
                    RipUnLock ();
                    return;
                }
                pRipCurPeerRec->u4PeerRcvBadPackets = 0;
                pRipCurPeerRec->u4PeerRcvBadRoutes = 0;

                u4Rip2PrevPeerAddress = u4Rip2PeerAddress;
                Rip2PrevPeerDomain.i4_Length = Rip2PeerDomain.i4_Length;
                MEMCPY (Rip2PrevPeerDomain.pu1_OctetList,
                        Rip2PeerDomain.pu1_OctetList, Rip2PeerDomain.i4_Length);

            }
            while (nmhGetNextIndexRip2PeerTable (u4Rip2PrevPeerAddress,
                                                 &u4Rip2PeerAddress,
                                                 &Rip2PrevPeerDomain,
                                                 &Rip2PeerDomain) !=
                   SNMP_FAILURE);

        }
        RipUnLock ();
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RipIsDemandCircuit                                         */
/*                                                                           */
/* Description    This function checks whether sendStatus is set to the      */
/*                following values and returns RIP_SUCCESS,                  */ 
/*			ripVersion1 (2),                                     */
/*                      rip1Compatible (3),                                  */
/*                      ripVersion2 (4)                                      */
/*                otherwise RIP_FAILURE.                                     */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RIP_SUCCESS / RIP_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT1
RipIsDemandCircuit (UINT2 u2SendStatus)
{
    if ((u2SendStatus >= 2 ) && (u2SendStatus <= 4))
    {
        /* i1IsDC will be set to FALSE at calling place */
        return RIP_SUCCESS;
    }
    else
    {
        /* i1IsDC will be set to TRUE at calling place */
        return RIP_FAILURE;
    }
}
