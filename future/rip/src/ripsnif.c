/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripsnif.c,v 1.74 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains the LOW level routines for
 *             the interface table, includes the routines   
 *             for both the IfStat and IfConf tables defined
 *             in RIP-2 MIB (rfc 1724) and contains the low 
 *             level routines for rip peer table.           
 *
 *******************************************************************/
#include "ripinc.h"
#include "ripcli.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"

PRIVATE tRipPkt     gRipPacket;

        /*|**********************************************************|**
         *** <<<<      local prototypes are delared here         >>>> ***
         **|**********************************************************|* */

PRIVATE INT1 rip_validate_row_status ARG_LIST ((UINT4, INT4, tRipCxt *));
PRIVATE INT1 rip_set_if_parameter ARG_LIST ((UINT4, VOID *, UINT1));
PRIVATE INT1        rip_get_if_index_and_row_status
ARG_LIST ((UINT4, UINT2 *, UINT1, tRipCxt *));
PRIVATE INT4 rip_get_if_instance ARG_LIST ((tRIP_INTERFACE, UINT4, tRipCxt *));
PRIVATE INT4        rip_create_if_instance
ARG_LIST ((UINT2, tRIP_INTERFACE, UINT4, tRipCxt *));
PRIVATE INT4        rip_action_for_iface_row_status
ARG_LIST ((UINT2, UINT4, UINT2, tRipCxt *));

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceRip2IfStatTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceRip2IfStatTable (UINT4 u4Rip2IfStatAddress)
{
    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfStatAddress))
    {
        /***  $$TRACE_LOG (EXIT, "Validation for domain, Failure \n"
                "Address is invalid \n");  ***/
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRip2PeerTable
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceRip2PeerTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceRip2PeerTable (UINT4 u4Rip2PeerAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pRip2PeerDomain)
{

    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /***  $$TRACE_LOG (ENTRY, "Entering Validation for domain. \n");  ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4Rip2PeerAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for domain, Failure \n"
                "Address is invalid \n");  ***/
        return SNMP_FAILURE;
    }

    /* Check the domain value */

    if (pRip2PeerDomain->i4_Length != RIP_DOMAIN_LENGTH)
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Domain, Failure \n");  ***/
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (EXIT, "Validation for Domain, Success \n");  ***/

    return SNMP_SUCCESS;

}

  /***************************************************************************
  *    "Get" routines for the object rip2IfStatTable are presented below     *
  ***************************************************************************/

/****************************************************************************
 Function    :  nmhGetFirstIndexRip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstRip2IfStatTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexRip2IfStatTable (UINT4 *pu4Rip2IfStatAddress)
{
    if (nmhGetNextIndexRip2IfStatTable (0, pu4Rip2IfStatAddress)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
                nextRip2IfStatAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextRip2IfStatTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexRip2IfStatTable (UINT4 u4Rip2IfStatAddress,
                                UINT4 *pu4NextRip2IfStatAddress)
{
    UINT4               u4IfStAddr = RIP_INV_NET_NUM;
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
                       "rip_get_next_index_rip2IfStatTable \n, Get Next"
                       "Request for net->%x \n", u4Rip2IfStatAddress);  ***/

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            if (pRipIfRec->u4Addr > u4Rip2IfStatAddress)
            {
                if (u4IfStAddr > pRipIfRec->u4Addr)
                {

                    u4IfStAddr = pRipIfRec->u4Addr;
                }
            }
        }
    }                            /* End Of If Hash Table Scan */

    if (u4IfStAddr == RIP_INV_NET_NUM)
    {

        /***  TRACE_LOG (EXIT, "Failure for next index retrievel \n");  ***/
        return SNMP_FAILURE;
    }

    *pu4NextRip2IfStatAddress = u4IfStAddr;

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_next_index_rip2IfStatTable"
            "\n Next Index = %d", *pu4NextRip2IfStatAddress);  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfStatRcvBadPackets
 Input       :  The Indices
                Rip2IfStatAddress

                The Object
                retValRip2IfStatRcvBadPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfStatRcvBadPackets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfStatRcvBadPackets (UINT4 u4Rip2IfStatAddress,
                               UINT4 *pu4RetValRip2IfStatRcvBadPackets)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfStatRcvBadPackets"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfStatAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValRip2IfStatRcvBadPackets =
        (UINT4) (pRipIfRec->RipIfaceStats.u4InBadPackets +
                 pRipIfRec->RipIfaceStats.u4InBadPackets1 +
                 pRipIfRec->RipIfaceStats.u4InBadPackets2 +
                 pRipIfRec->RipIfaceStats.u4InBadPackets3 +
                 pRipIfRec->RipIfaceStats.u4InBadPackets4);
    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfStatRcvBadPackets"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfStatRcvBadRoutes
 Input       :  The Indices
                Rip2IfStatAddress

                The Object
                retValRip2IfStatRcvBadRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfStatRcvBadRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfStatRcvBadRoutes (UINT4 u4Rip2IfStatAddress,
                              UINT4 *pu4RetValRip2IfStatRcvBadRoutes)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfStatRcvBadRoutes"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfStatAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValRip2IfStatRcvBadRoutes =
        (UINT4) (pRipIfRec->RipIfaceStats.u4InBadRoutes +
                 pRipIfRec->RipIfaceStats.u4InBadRoutes1 +
                 pRipIfRec->RipIfaceStats.u4InBadRoutes2);
    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfStatRcvBadRoutes"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfStatSentUpdates
 Input       :  The Indices
                Rip2IfStatAddress

                The Object
                retValRip2IfStatSentUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfStatSentUpdates ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfStatSentUpdates (UINT4 u4Rip2IfStatAddress,
                             UINT4 *pu4RetValRip2IfStatSentUpdates)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfStatSentUpdates"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfStatAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValRip2IfStatSentUpdates =
        (UINT4) (pRipIfRec->RipIfaceStats.u4SentTrigUpdates);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfStatSentUpdates"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfStatStatus
 Input       :  The Indices
                Rip2IfStatAddress

                The Object
                retValRip2IfStatStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfStatStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfStatStatus (UINT4 u4Rip2IfStatAddress,
                        INT4 *pi4RetValRip2IfStatStatus)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfStatStatus \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfStatAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2IfStatStatus = (INT4) (pRipIfRec->RipIfaceCfg.u2AdminStatus);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfStatStatus"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

  /***************************************************************************
  *  The following set of `validate` routines implement the finite state     *
  *  on how to act for a table with RowStatus variable in rip2IfStatTable.   *
  ***************************************************************************/
/****************************************************************************
 Function    :  nmhTestv2Rip2IfStatStatus
 Input       :  The Indices
                Rip2IfStatAddress

                The Object
                testValRip2IfStatStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfStatStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfStatStatus (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfStatAddress,
                           INT4 i4TestValRip2IfStatStatus)
{
    INT4                i4RetVal;
    UINT1               u1IfType;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, rowStatus, critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Stat Status. \n"); ***/

    /*  For unnumbered u4Rip2IfStatAddress will not have an IP address
     *  but insted the IfIndex of the Unnumbered interface, So we definately
     *  know that it is going to fail, So let us hack this out and return
     *  SUCCESS.
     *  There is one more hack here, we need to limit number of unnumbered
     *  ifaces to a value such that it is less than the least CLASS A IP address
     *  we can have, i.e., less than 1.0.0.1 = 0x1000001 = 16777217 Unnumbered
     *  interfaces we can have, right now we can limit it to a decimal value of
     *  say 100 as below. This additional check is to avoid SEG fault..
     */

    if (u4Rip2IfStatAddress <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES)
    {
        if ((i4RetVal =
             RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                                 u4Rip2IfStatAddress)) ==
            RIP_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }

        u1IfType = RIPIF_IS_UNNUMBERED ((UINT2) i4RetVal);
        if (u1IfType == FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    if (rip_validate_row_status (u4Rip2IfStatAddress,
                                 i4TestValRip2IfStatStatus,
                                 pRipCxtEntry) == RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

       /***  $$TRACE_LOG (EXIT, "Validation for row status success in"
               "rip2IfStatTable \n");  ***/
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Rip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Rip2IfStatTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

  /***************************************************************************
  *  The following routine implements the SET operation for rip2ifStatTable  *
  *  object.                                                                 *
  ***************************************************************************/

/****************************************************************************
 Function    :  nmhSetRip2IfStatStatus
 Input       :  The Indices
                Rip2IfStatAddress

                The Object
                setValRip2IfStatStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfStatStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfStatStatus (UINT4 u4Rip2IfStatAddress,
                        INT4 i4SetValRip2IfStatStatus)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }

    /*
     * Set operation for non-index, RowStatus variable, Critical variable.
     */

    if (rip_set_if_parameter
        (u4Rip2IfStatAddress, (VOID *) &i4SetValRip2IfStatStatus,
         RIPIF_ROW_STAT) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Row Status , Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Row Status, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfStatStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdRip2IfStatStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfStatAddress, i4SetValRip2IfStatStatus));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceRip2IfConfTable
 Input       :  The Indices
                Rip2IfConfAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceRip2IfConfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceRip2IfConfTable (UINT4 u4Rip2IfConfAddress)
{
    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for domain, Failure \n"
                "Address is invalid \n");  ***/
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

  /***************************************************************************
  *    "Get" routines for the object rip2IfConfTable are presented below     *
  ***************************************************************************/

/****************************************************************************
 Function    :  nmhGetFirstIndexRip2IfConfTable
 Input       :  The Indices
                Rip2IfConfAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstRip2IfConfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexRip2IfConfTable (UINT4 *pu4Rip2IfConfAddress)
{
    if (nmhGetNextIndexRip2IfConfTable (0, pu4Rip2IfConfAddress)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRip2IfConfTable
 Input       :  The Indices
                Rip2IfConfAddress
                nextRip2IfConfAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextRip2IfConfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexRip2IfConfTable (UINT4 u4Rip2IfConfAddress,
                                UINT4 *pu4NextRip2IfConfAddress)
{
    UINT4               u4IfStAddr = RIP_INV_NET_NUM;
    UINT4               u4HashIndex = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
                       "nmhGetNextIndexRip2IfConfTable \n, Get Next"
                       "Request for net->%x \n", u4Rip2IfConfAddress);  ***/

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {

            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            if (pRipIfRec->u4Addr > u4Rip2IfConfAddress)
            {
                if (u4IfStAddr > pRipIfRec->u4Addr)
                {

                    u4IfStAddr = pRipIfRec->u4Addr;
                }
            }
        }

    }                            /* End Of If Hash Table Scan */

    if (u4IfStAddr == RIP_INV_NET_NUM)
    {

        /***  TRACE_LOG (EXIT, "Failure for next index retrievel \n");  ***/
        return SNMP_FAILURE;
    }

    *pu4NextRip2IfConfAddress = u4IfStAddr;

    /***  $$TRACE_LOG (EXIT, "Exiting fn, nmhGetNextIndexRip2IfConfTable"
            "\n Next Index = %d", *pu4NextRip2IfConfAddress);  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfDomain
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfDomain ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfDomain (UINT4 u4Rip2IfConfAddress,
                        tSNMP_OCTET_STRING_TYPE * pRetValRip2IfConfDomain)
{
    UINT1              *pu1Domain = pRetValRip2IfConfDomain->pu1_OctetList;
    INT4               *pi4Length = &(pRetValRip2IfConfDomain->i4_Length);
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfDomain \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Fill the string */

    *pu1Domain++ = (UINT1) pRipIfRec->RipIfaceCfg.au1RouteDomain[0];
    *pu1Domain = (UINT1) pRipIfRec->RipIfaceCfg.au1RouteDomain[1];

    /* Fill in the length of the string also */

    *pi4Length = 2;                /* Domain Length */

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfDomain"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfAuthType
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfAuthType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfAuthType (UINT4 u4Rip2IfConfAddress,
                          INT4 *pi4RetValRip2IfConfAuthType)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*** $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfAuthType \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2IfConfAuthType = (INT4) (pRipIfRec->RipIfaceCfg.u2AuthType);

    if (*pi4RetValRip2IfConfAuthType >= RIPIF_MD5_AUTHENTICATION)
    {
        /* Rip2IfConfAuthType can have only 3 Auth type
         * if SHA Authentication is configured then it can be retrieved 
         * by fsRip2IfconfAuthType */
        *pi4RetValRip2IfConfAuthType = RIPIF_MD5_AUTHENTICATION;
    }
    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfAuthType"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfAuthKey
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfAuthKey ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfAuthKey (UINT4 u4Rip2IfConfAddress,
                         tSNMP_OCTET_STRING_TYPE * pRetValRip2IfConfAuthKey)
{
    UINT2               u2If;
    INT4               *pi4Length = &(pRetValRip2IfConfAuthKey->i4_Length);
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tRipIfaceRec       *pRipIfRec = NULL;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfAuthKey \n"); ***/

    if (rip_get_if_index (u4Rip2IfConfAddress, &u2If, pRipCxtEntry) ==
        RIP_FAILURE)
    {

        /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfAuthKey"
                "with SNMP_FAILURE status \n");  ***/
        return SNMP_FAILURE;
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return RIP_FAILURE;
    }

    /* Should not fill the string.If the key value is filled, a get operation
     * on the object  or walk on the table will display the key value 
     * thus bypassing authentication.This is against the mib description for 
     * the object. So key value is not returned, empty string is returned.
     * So set the string length to zero */
    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        MEMCPY (pRetValRip2IfConfAuthKey->pu1_OctetList,
                (INT1 *) pRipIfRec->RipIfaceCfg.au1AuthKey,
                STRLEN (pRipIfRec->RipIfaceCfg.au1AuthKey));
        pRetValRip2IfConfAuthKey->i4_Length =
            (INT4) STRLEN (pRipIfRec->RipIfaceCfg.au1AuthKey);
    }
    else
    {
        *pi4Length = 0;
    }

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfAuthKey"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfSend
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfSend
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfSend ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfSend (UINT4 u4Rip2IfConfAddress, INT4 *pi4RetValRip2IfConfSend)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfSend \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2IfConfSend = (INT4) (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfSend"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfReceive
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfReceive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfReceive ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfReceive (UINT4 u4Rip2IfConfAddress,
                         INT4 *pi4RetValRip2IfConfReceive)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfReceive \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2IfConfReceive =
        (INT4) (pRipIfRec->RipIfaceCfg.u2RipRecvStatus);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfReceive"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfDefaultMetric
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfDefaultMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfDefaultMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfDefaultMetric (UINT4 u4Rip2IfConfAddress,
                               INT4 *pi4RetValRip2IfConfDefaultMetric)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfDefaultMetric"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2IfConfDefaultMetric =
        (INT4) (pRipIfRec->RipIfaceCfg.u2DefaultMetric);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfDefaultMetric"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfStatus
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfStatus (UINT4 u4Rip2IfConfAddress,
                        INT4 *pi4RetValRip2IfConfStatus)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfStatus \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2IfConfStatus = (INT4) (pRipIfRec->RipIfaceCfg.u2AdminStatus);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfStatus"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2IfConfSrcAddress
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                retValRip2IfConfSrcAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2IfConfSrcAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2IfConfSrcAddress (UINT4 u4Rip2IfConfAddress,
                            UINT4 *pu4RetValRip2IfConfSrcAddress)
{

    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfSrcAddress"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValRip2IfConfSrcAddress = pRipIfRec->RipIfaceCfg.u4SrcAddr;

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfSrcAddress"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

  /***************************************************************************
  *  The following set of `validate` routines implement the finite state     *
  *  on how to act for a table with RowStatus variable.                      *
  ***************************************************************************/

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfDomain
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfDomain ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfDomain (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                           tSNMP_OCTET_STRING_TYPE * pTestValRip2IfConfDomain)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /***  $$TRACE_LOG (ENTRY, "Entering Validation for domain. \n");  ***/

    /* Validation for the net address */
    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for domain, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check the domain value */

    if (pTestValRip2IfConfDomain->i4_Length != RIP_DOMAIN_LENGTH)
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Domain, Failure \n");  ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (EXIT, "Validation for Domain, Success \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfAuthType
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfAuthType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfAuthType (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                             INT4 i4TestValRip2IfConfAuthType)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    tMd5AuthKeyInfo    *pNode = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /***  $$TRACE_LOG (ENTRY, "Entering Validation for AuthType. \n");  ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for auth type, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check for the auth type */

    switch (i4TestValRip2IfConfAuthType)
    {

        case RIPIF_NO_AUTHENTICATION:
        case RIPIF_SIMPLE_PASSWORD:

            /*** $$TRACE_LOG (EXIT, "Validation for AuthType, Success \n"); ***/
            return SNMP_SUCCESS;

        case RIPIF_MD5_AUTHENTICATION:
            /* trying to set RIP MD5 Authentication for this interface */

            pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
            if (pRipIfRec == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
            {
                if (RIP_MD5_ROW_STATUS (pNode) == ACTIVE)
                {
                    /* active key is there */
                    /* check the start time */
                    if ((UINT4) RIP_MD5_START_TIME (pNode) <=
                        OsixGetSysUpTime ())
                    {
                        /* a key is configured to use  */
                        return SNMP_SUCCESS;
                    }
                }
            }

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        default:

            /* We are not supportng MD5 authentication */

            /*** $$TRACE_LOG (EXIT, "Validation for AuthType, Failure \n"); ***/
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfAuthKey
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfAuthKey ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfAuthKey (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                            tSNMP_OCTET_STRING_TYPE * pTestValRip2IfConfAuthKey)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /***  $$TRACE_LOG (ENTRY, "Entering Validation for AuthKey. \n");  ***/

    /* Validation for the net address */

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for auth key, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check the auth key */

    if ((pTestValRip2IfConfAuthKey->i4_Length < 0) ||
        (pTestValRip2IfConfAuthKey->i4_Length > MAX_AUTH_KEY_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (EXIT, "Validation for AuthKey, Success \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfSend
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfSend
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfSend ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfSend (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                         INT4 i4TestValRip2IfConfSend)
{
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4RetIf;
    INT1                i1IsDC = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index & non-rowStatus, non-critical variable.
     */

    /***  $$TRACE_LOG (ENTRY, "Entering Validation for Send. \n");  ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Send, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4RetIf =
         RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                             u4Rip2IfConfAddress)) ==
        RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec ((UINT4) i4RetIf, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return SNMP_FAILURE;
    }
    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
    /* Check for the send value */
    if (u4Rip2IfConfAddress <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES)
    {
        if (i4TestValRip2IfConfSend == RIPIF_VERSION_2_SND)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_RIP_INVALID_UNNUM_VERSION);
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValRip2IfConfSend)
    {

        case RIPIF_VERSION_1_SND:
        case RIPIF_RIP1_COMPATIBLE:
        case RIPIF_VERSION_2_SND:
            if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                && (i1IsDC != RIP_SUCCESS))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                break;
            }

        case RIPIF_DO_NOT_SEND:
            /***  $$TRACE_LOG (EXIT, "Validation for Send, Success \n");  ***/
            i4RetVal = SNMP_SUCCESS;
            break;

        case RIPIF_V1_DEMAND:
        case RIPIF_V2_DEMAND:
            if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                && (i1IsDC == RIP_SUCCESS))
            {
                i4RetVal = SNMP_SUCCESS;
            }
            else
            {
                if (pRipIfRec->u1Persistence != RIP_WAN_TYPE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_RIP_DEMAND_TYPE_ERR);
                    return SNMP_FAILURE;
                }
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_RIP_DEMAND_VERSION_ERR);
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;

    }
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfReceive
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfReceive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfReceive ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfReceive (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                            INT4 i4TestValRip2IfConfReceive)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /***  $$TRACE_LOG (ENTRY, "Entering Validation for Recv. \n");  ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Recv, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check for the recv value */
    if (u4Rip2IfConfAddress <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES)
    {
        if (i4TestValRip2IfConfReceive == RIPIF_VERSION_2_RCV)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_RIP_INVALID_UNNUM_VERSION);
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValRip2IfConfReceive)
    {

        case RIPIF_VERSION_1_RCV:
        case RIPIF_VERSION_2_RCV:
        case RIPIF_1_OR_2:
        case RIPIF_DO_NOT_RECV:

            /***  $$TRACE_LOG (EXIT, "Validation for Recv, Success \n");  ***/
            return SNMP_SUCCESS;

        default:

            /***  $$TRACE_LOG (EXIT, "Validation for Recv, Failure \n");  ***/
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfDefaultMetric
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfDefaultMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfDefaultMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfDefaultMetric (UINT4 *pu4ErrorCode,
                                  UINT4 u4Rip2IfConfAddress,
                                  INT4 i4TestValRip2IfConfDefaultMetric)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, non-rowStatus, critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Default Metric. \n"); ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Def. Metric, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValRip2IfConfDefaultMetric < 0) ||
        (i4TestValRip2IfConfDefaultMetric >= RIP_INFINITY))
    {

        /*** $$TRACE_LOG (EXIT, "Validation for DefaultMetric, Failure\n"); ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (EXIT, "Validation for DefaultMetric, Success \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfStatus
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfStatus (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                           INT4 i4TestValRip2IfConfStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT4                i4IfIndex;
    UINT4               u4RipCxtId;
    tNetIpv4IfInfo      NetIpIfInfo;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i4IfIndex =
        RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                            u4Rip2IfConfAddress);

    if (i4IfIndex == RIP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (RIPIF_IS_UNNUMBERED ((UINT2) i4IfIndex) == FALSE)
    {
        if (NetIpv4GetIfInfo ((UINT4) i4IfIndex, &NetIpIfInfo)
            == NETIPV4_FAILURE)
        {
            CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }
    if (NetIpv4GetCxtId ((UINT4) i4IfIndex, &u4RipCxtId) == NETIPV4_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pRipCxtEntry->i4CxtId != (INT4) u4RipCxtId)
    {
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, rowStatus, critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Conf Status. \n"); ***/
    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Row Status, Failure \n"
                "Address is invalid \n");  ***/
        CLI_SET_ERR (CLI_RIP_INVALID_IP_ADDR);
        return RIP_FAILURE;
    }

    if (rip_validate_row_status
        (u4Rip2IfConfAddress, i4TestValRip2IfConfStatus,
         pRipCxtEntry) == RIP_FAILURE)
    {

        /***  $$TRACE_LOG (EXIT, "Validation for row status in rip2IfConfTable"
                "fails \n");  ***/
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (EXIT, "Validation for row status success in"
            "rip2IfConfTable \n");  ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Rip2IfConfSrcAddress
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                testValRip2IfConfSrcAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2IfConfSrcAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Rip2IfConfSrcAddress (UINT4 *pu4ErrorCode, UINT4 u4Rip2IfConfAddress,
                               UINT4 u4TestValRip2IfConfSrcAddress)
{
    INT4                i4RetVal;
    UINT1               u1IfType;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4Addr;
    /*
     * Validate operation for non-index, non-rowStatus, critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Src Address. \n"); ***/

    /* Validation for the net address */
    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (RIP_IS_ADDRESS_INVALID (u4Rip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Conf. Src Addr, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /*
     * The following thing is applicable only for Numbered ifaces and is subject
     * to change for UnNumbered ifaces.
     *  We need to override this check for IP Unnumbered. Hay My dear 
     *  young friends of FS this may cause SEG Fault be careful when u give an 
     *  MMI command.
     */

    if ((i4RetVal =
         RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                             u4Rip2IfConfAddress)) ==
        RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u1IfType = RIPIF_IS_UNNUMBERED ((UINT2) i4RetVal);
    if ((u4TestValRip2IfConfSrcAddress != 0) && u1IfType == FALSE)
    {
        /* then it is unnumberd interface */
        if ((u4TestValRip2IfConfSrcAddress != u4Rip2IfConfAddress))
        {

        /*** $$TRACE_LOG (EXIT, "Validation for SrcAddress, Failure \n"); ***/
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (u1IfType == TRUE)
    {
        /*this is an unnumbered interface */
        pRipIfRec = RipGetIfRec ((UINT4) i4RetVal, pRipCxtEntry);
        if (pRipIfRec == NULL)
        {
            return SNMP_FAILURE;
        }
        pRipCxtEntry = pRipIfRec->pRipCxt;
        if (u4TestValRip2IfConfSrcAddress == 0)
        {
            i4RetVal = RipIfGetIpForUnnumIf (pRipCxtEntry, &u4Addr);
            if (i4RetVal == RIP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_NO_VALID_SOURCE);
                return SNMP_FAILURE;
            }
        }
        else
        {
            i4RetVal =
                RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->
                                                    i4CxtId,
                                                    u4TestValRip2IfConfSrcAddress);
            if (i4RetVal == RIP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    /***  $$TRACE_LOG (EXIT, "Validation for SrcAddress, Success \n");  ***/

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Rip2IfConfTable
 Input       :  The Indices
                Rip2IfConfAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Rip2IfConfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

  /***************************************************************************
  *  The following set of `set` routines implement the finite state machine  *
  *  on how to act for a table with RowStatus variable. The action for       *
  *  `set` operation for any variable is same and mentioned below. Any set   *
  *  routine will call a procedure which is responsible to create a new      *
  *  instance if one is not available, assign the default parameters with    *
  *  the values, manage the linked list of interfaces. If the instance is    *
  *  already available, the variable is set and the status variable for      *
  *  that row is taken care accordingly.                                     *
  ***************************************************************************/
/****************************************************************************
 Function    :  nmhSetRip2IfConfDomain
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfDomain ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfDomain (UINT4 u4Rip2IfConfAddress,
                        tSNMP_OCTET_STRING_TYPE * pSetValRip2IfConfDomain)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index, non-rowStatus, non-critical variable.
     */

    if (rip_set_if_parameter
        (u4Rip2IfConfAddress, (VOID *) pSetValRip2IfConfDomain->pu1_OctetList,
         RIPIF_DOMAIN) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Domain, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfDomain;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdRip2IfConfDomain) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %s", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, pSetValRip2IfConfDomain));
#endif
     /***  $$TRACE_LOG (EXIT, "Set for Domain, Success \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetRip2IfConfAuthType
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfAuthType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfAuthType (UINT4 u4Rip2IfConfAddress,
                          INT4 i4SetValRip2IfConfAuthType)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tCryptoAuthKeyInfo *pCryptoNode = NULL;
    tCryptoAuthKeyInfo *pCryptoNextNode = NULL;
    tMd5AuthKeyInfo    *pMd5Node = NULL;
    tMd5AuthKeyInfo    *pMd5NextNode = NULL;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index & non-rowStatus, non-critical variable.
     */

    if (rip_set_if_parameter (u4Rip2IfConfAddress,
                              (VOID *) &i4SetValRip2IfConfAuthType,
                              RIPIF_AUTHTYPE) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Auth Type, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }
    /* For simple text & no authentication, the crypto key entries
     * if present needs to be deleted. */
    if (i4SetValRip2IfConfAuthType <= 2)
    {
        /* Deleting the keys from Md5 Key list */
        pMd5Node = (tMd5AuthKeyInfo *)
            RIP_SLL_First (&(pRipIfRec->RipMd5KeyList));
        while (pMd5Node != NULL)
        {
            pMd5NextNode = RIP_SLL_Next (&(pRipIfRec->RipMd5KeyList),
                                         &(pMd5Node->NextNode));
            RIP_SLL_Delete (&(pRipIfRec->RipMd5KeyList), &(pMd5Node->NextNode));
            RIP_FREE_AUTH_KEY_NODE (pRipIfRec, pMd5Node);

            pMd5Node = pMd5NextNode;
        }

        /* Deleting the keys from Crypto Key list */
        pCryptoNode = (tCryptoAuthKeyInfo *)
            RIP_SLL_First (&(pRipIfRec->RipCryptoAuthKeyList));
        while (pCryptoNode != NULL)
        {
            pCryptoNextNode = (tCryptoAuthKeyInfo *)
                RIP_SLL_Next (&(pRipIfRec->RipCryptoAuthKeyList),
                              &(pCryptoNode->NextNode));
            RIP_SLL_Delete (&(pRipIfRec->RipCryptoAuthKeyList),
                            &(pCryptoNode->NextNode));
            RIP_FREE_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pCryptoNode);

            pCryptoNode = pCryptoNextNode;
        }
    }

     /***  $$TRACE_LOG (EXIT, "Set for Auth Type, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfAuthType;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIStdRip2IfConfAuthType) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, i4SetValRip2IfConfAuthType));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetRip2IfConfAuthKey
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfAuthKey ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfAuthKey (UINT4 u4Rip2IfConfAddress,
                         tSNMP_OCTET_STRING_TYPE * pSetValRip2IfConfAuthKey)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index & non-rowStatus, non-critical variable.
     */

    if (rip_set_if_parameter (u4Rip2IfConfAddress,
                              (VOID *) pSetValRip2IfConfAuthKey,
                              RIPIF_AUTHKEY) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Auth Key, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Auth Key, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfAuthKey;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIStdRip2IfConfAuthKey) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %s", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, pSetValRip2IfConfAuthKey));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetRip2IfConfSend
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfSend
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfSend ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfSend (UINT4 u4Rip2IfConfAddress, INT4 i4SetValRip2IfConfSend)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index & non-rowStatus, non-critical variable.
     */

    if (rip_set_if_parameter (u4Rip2IfConfAddress,
                              (VOID *) &i4SetValRip2IfConfSend,
                              RIPIF_SEND) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Send value, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Send value, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfSend;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdRip2IfConfSend) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, i4SetValRip2IfConfSend));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetRip2IfConfReceive
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfReceive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfReceive ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfReceive (UINT4 u4Rip2IfConfAddress,
                         INT4 i4SetValRip2IfConfReceive)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index & non-rowStatus, non-critical variable.
     */

    if (rip_set_if_parameter (u4Rip2IfConfAddress,
                              (VOID *) &i4SetValRip2IfConfReceive,
                              RIPIF_RECV) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Recv value, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Recv value, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfReceive;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIStdRip2IfConfReceive) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, i4SetValRip2IfConfReceive));
#endif
    return SNMP_SUCCESS;
}

/***************************************************************************
 Function    :  nmhSetRip2IfConfDefaultMetric
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfDefaultMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfDefaultMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfDefaultMetric (UINT4 u4Rip2IfConfAddress,
                               INT4 i4SetValRip2IfConfDefaultMetric)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index & non-rowStatus variable.
     */

    if (rip_set_if_parameter (u4Rip2IfConfAddress,
                              (VOID *) &i4SetValRip2IfConfDefaultMetric,
                              RIPIF_DEF_MET) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Default Metric, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Default Metric, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfDefaultMetric;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIStdRip2IfConfDefaultMetric) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, i4SetValRip2IfConfDefaultMetric));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetRip2IfConfStatus
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfStatus (UINT4 u4Rip2IfConfAddress,
                        INT4 i4SetValRip2IfConfStatus)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipNextIfRec = NULL;
    UINT4               u4HashIndex = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index, RowStatus variable.
     */

    if (rip_set_if_parameter
        (u4Rip2IfConfAddress, (VOID *) &i4SetValRip2IfConfStatus,
         RIPIF_ROW_STAT) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Row Status , Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/
        if (i4SetValRip2IfConfStatus == DESTROY)
        {
            if (RIP_GET_NODE_STATUS () == RM_STANDBY)
            {
                TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
                {
                    TMO_HASH_DYN_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                              pRipIfRec, pRipNextIfRec,
                                              tRipIfaceRec *)
                    {
                        if (u4Rip2IfConfAddress == pRipIfRec->u4Addr)
                        {
                            rip_if_destroy (pRipIfRec);
                        }
                    }
                }
            }
        }

        return SNMP_FAILURE;
    }
     /***  $$TRACE_LOG (EXIT, "Set for Row Status, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdRip2IfConfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, i4SetValRip2IfConfStatus));
#endif
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhSetRip2IfConfSrcAddress
 Input       :  The Indices
                Rip2IfConfAddress

                The Object
                setValRip2IfConfSrcAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2IfConfSrcAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetRip2IfConfSrcAddress (UINT4 u4Rip2IfConfAddress,
                            UINT4 u4SetValRip2IfConfSrcAddress)
{
    INT4                i4IfIndex;
    UINT1               u1IfType;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * Set operation for non-index, non-rowStatus & critical variable.
     */

    /*  Here we need to trick RIP, Allow MMI not return bad value if
     *  interface level ifindex and Ifaddr are different, temporarily configure
     *  it as 0.0.0.0 and HERE check for 0.0.0.0, and if so it is unnumbered
     *  interface and
     *  then we  can use the IP address of ifindex as the source address. All
     *  we have to do is make MMI conf come up to low level then we can do all
     *  the tricks. 
     *  e.g.,   /rip/if/0.0.0.2/IfAddr 0.0.0.0, right now MMI doesent allow
     *  this having different value, override this check to allow it. and once
     *  the call come to this func, we can handle it.
     *
     *  RFC - 1724 : rip2IfConfSrcAddress OBJECT-TYPE 
     *         "The IP Address this system will use as a source 
     *          address on this interface.  If it is a numbered 
     *          interface, this MUST be the same value as 
     *          rip2IfConfAddress.  On unnumbered interfaces, 
     *          it must be the value of rip2IfConfAddress for 
     *          some interface on the system." 
     *
     *  For avoiding Seg Fault here the MMI user has to be care full 
     *  to give IfIndex in place of IP Address. If it is unnumberd interface
     *  so set the address to router-id.
     */

    i4IfIndex =
        RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                            u4Rip2IfConfAddress);

    if (i4IfIndex == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u1IfType = RIPIF_IS_UNNUMBERED ((UINT2) i4IfIndex);

    if ((u4SetValRip2IfConfSrcAddress == 0) && ((u1IfType) == TRUE))
    {
        pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfConfAddress, pRipCxtEntry);
        if (pRipIfRec == NULL)
        {
            return SNMP_FAILURE;
        }
        pRipCxtEntry = pRipIfRec->pRipCxt;
        /*fetch some valid ip address for the unnumbered interface to use as source */
        if (RipIfGetIpForUnnumIf (pRipCxtEntry, &u4SetValRip2IfConfSrcAddress)
            == RIP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    if (rip_set_if_parameter (u4Rip2IfConfAddress,
                              (VOID *) &u4SetValRip2IfConfSrcAddress,
                              RIPIF_SRC_ADDR) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Src Address, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Src Address, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfSrcAddress;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIStdRip2IfConfSrcAddress) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %u", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, u4SetValRip2IfConfSrcAddress));
#endif
    return SNMP_SUCCESS;
}

  /***************************************************************************
  *    "Get" routines for the object rip2PeerTable are presented below       *
  ***************************************************************************/
/****************************************************************************
 Function    :  nmhGetFirstIndexRip2PeerTable
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstRip2PeerTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexRip2PeerTable (UINT4 *pu4Rip2PeerAddress,
                               tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain)
{
    UINT1               au1Domain[2] = "zz";    /* Maximum Value, Initiallly */
    UINT4               u4PeerAddr = RIP_INV_NET_NUM;
    tRipPeerRec        *pRipCurPeerRec = NULL;
    UINT1              *pu1Domain = pRip2PeerDomain->pu1_OctetList;
    INT4               *pi4Length = &(pRip2PeerDomain->i4_Length);
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_first_index_rip2PeerTable \n");  ***/

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {

            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            /* Scan the list of peer list for the specified interface */

            RIP_SLL_Scan (&(pRipIfRec->RipPeerList), pRipCurPeerRec,
                          tRipPeerRec *)
            {

                if (pRipCurPeerRec->u4PeerAddr < u4PeerAddr)
                {

                    u4PeerAddr = pRipCurPeerRec->u4PeerAddr;
                    MEMCPY ((VOID *) au1Domain,
                            (VOID *) (pRipCurPeerRec->au1PeerRouteTag),
                            sizeof (au1Domain));
                }

                else if (pRipCurPeerRec->u4PeerAddr > u4PeerAddr)
                {
                    continue;
                }

                else
                {

                    /* 
                     * Both the Peer Indices are equal, check for their route
                     * domains.
                     */

                    if (MEMCMP (pRipCurPeerRec->au1PeerRouteTag, au1Domain,
                                sizeof (au1Domain)) < 0)
                    {

                        MEMCPY ((VOID *) au1Domain,
                                (VOID *) (pRipCurPeerRec->au1PeerRouteTag),
                                sizeof (au1Domain));
                    }
                }
            }                    /* End of RIP_SLL_Scan () */

        }
    }                            /* End of Interface Hash Table Scan */

    /*
     * Ideally speaking, we should also be checking for route tag as the start &
     * invalid one, but since the RFC doesn't prescribe any value as invalid for
     * the route tag, we just leave that now for future consideration.
     */

    if (u4PeerAddr == RIP_INV_NET_NUM)
    {

        /***  TRACE_LOG (EXIT, "Failure for first index retrievel \n");  ***/
        return SNMP_FAILURE;
    }

    *pu4Rip2PeerAddress = u4PeerAddr;

    /* Fill the string */

    MEMCPY (pu1Domain, au1Domain, sizeof (au1Domain));

    /* Fill in the length of the string also */

    *pi4Length = 2;                /* Length of the domain */

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_first_index_rip2PeerTable"
            "\n First Index, Peer addr = %x, \n Domain = %c%c \n", *pu4Rip2PeerAddress,
            pu1Domain[0], pu1Domain[1]);  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRip2PeerTable
 Input       :  The Indices
                Rip2PeerAddress
                nextRip2PeerAddress
                Rip2PeerDomain
                nextRip2PeerDomain
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextRip2PeerTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexRip2PeerTable (UINT4 u4Rip2PeerAddress,
                              UINT4 *pu4NextRip2PeerAddress,
                              tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                              tSNMP_OCTET_STRING_TYPE * pNextRip2PeerDomain)
{
    UINT1               au1NextDomain[2] = "��",    /* Maximum Value, initiallly */
        *pu1NextDomain = pNextRip2PeerDomain->pu1_OctetList;
    UINT4               u4NextPeerAddr = RIP_INV_NET_NUM;
    tRipPeerRec        *pRipCurPeerRec = NULL;
    INT4               *pi4NextLength = &(pNextRip2PeerDomain->i4_Length);
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_next_index_rip2PeerTable \n, Get next request->"
            "Peer = %x", u4Rip2PeerAddress );  ***/

    /* Makefile Changes - unused parameter */
    if (pRip2PeerDomain == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            /* Scan the list of peer list for the specified interface */

            RIP_SLL_Scan (&(pRipIfRec->RipPeerList), pRipCurPeerRec,
                          tRipPeerRec *)
            {

                if (pRipCurPeerRec->u4PeerAddr > u4Rip2PeerAddress)
                {

                    if (u4NextPeerAddr > pRipCurPeerRec->u4PeerAddr)

                    {
                        u4NextPeerAddr = pRipCurPeerRec->u4PeerAddr;
                        MEMCPY (au1NextDomain,
                                pRipCurPeerRec->au1PeerRouteTag,
                                sizeof (au1NextDomain));
                    }

                    else if (u4NextPeerAddr < pRipCurPeerRec->u4PeerAddr)
                    {
                        continue;
                    }
                    else
                    {

                        /*
                         * Both the peer indices are equal, and so check
                         * the next index, namely, `Domain`.
                         */

                        if (MEMCMP (pRipCurPeerRec->au1PeerRouteTag,
                                    au1NextDomain, sizeof (au1NextDomain)) < 0)
                        {

                            MEMCPY (au1NextDomain,
                                    pRipCurPeerRec->au1PeerRouteTag,
                                    sizeof (au1NextDomain));
                        }
                    }
                }                /* End of if (pRipCurPeerRec->u4PeerAddr > 
                                 * u4Rip2PeerAddress) */

                else if (pRipCurPeerRec->u4PeerAddr < u4Rip2PeerAddress)
                {
                    continue;
                }

                else
                {

                    /*
                     * The peer addresses are equal, and so we have to check 
                     * for the domains.
                     */

                    if (MEMCMP (pRipCurPeerRec->au1PeerRouteTag,
                                au1NextDomain, sizeof (au1NextDomain)) > 0)
                    {

                        if (u4NextPeerAddr > pRipCurPeerRec->u4PeerAddr)
                        {

                            u4NextPeerAddr = pRipCurPeerRec->u4PeerAddr;
                            MEMCPY (au1NextDomain,
                                    pRipCurPeerRec->au1PeerRouteTag,
                                    sizeof (au1NextDomain));
                        }

                        else if (u4NextPeerAddr < pRipCurPeerRec->u4PeerAddr)
                        {
                            continue;
                        }

                        else
                        {

                            /*
                             * Peer addresses are equal, check for domain.
                             */

                            if (MEMCMP (au1NextDomain,
                                        pRipCurPeerRec->au1PeerRouteTag,
                                        sizeof (au1NextDomain)) > 0)
                            {

                                MEMCPY (au1NextDomain,
                                        pRipCurPeerRec->au1PeerRouteTag,
                                        sizeof (au1NextDomain));
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }            /* End of if (MEMCMP () ) */

                }                /* End of else part of 
                                 * if(pRipCurPeerRec->u4PeerAddr>u4Rip2PeerAddress) 
                                 */

            }                    /* End of RIP_SLL_Scan () */

        }                        /* End of TMO_HASH_Scan_Bucket */

    }                            /* End of Interface Hash Table Scan */

    /*
     * Refer FirstIndex retrieval comment at this place as far as the
     * RouteDomain is concerned.
     */

    if (u4NextPeerAddr == RIP_INV_NET_NUM)
    {

        /***  TRACE_LOG (EXIT, "Failure for Next index retrievel \n");  ***/
        return SNMP_FAILURE;
    }

    *pu4NextRip2PeerAddress = u4NextPeerAddr;
    MEMCPY (pu1NextDomain, au1NextDomain, sizeof (au1NextDomain));

    /* Fill in the length of the string also */
    *pi4NextLength = 2;            /* Length of the route tag */

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_next_index_rip2PeerTable"
            "\n Second Index, Peer addr = %x, \n Domain = %c%c \n",
            *pu4NextRip2PeerAddress, pu1NextDomain[0], pu1NextDomain[1]);  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetRip2PeerLastUpdate
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain

                The Object
                retValRip2PeerLastUpdate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2PeerLastUpdate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2PeerLastUpdate (UINT4 u4Rip2PeerAddress,
                          tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                          UINT4 *pu4RetValRip2PeerLastUpdate)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    UINT1              *pu1Domain = pRip2PeerDomain->pu1_OctetList;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_rip2PeerLastUpdate \n, Get request->"
            "Peer = %x, Domain = %c%c", u4Rip2PeerAddress, pRip2PeerDomain[0], pRip2PeerDomain[1]
            );  ***/

    if ((pRipCurPeerRec = rip_retrieve_record_for_peer (u4Rip2PeerAddress,
                                                        pu1Domain,
                                                        pRipCxtEntry)) !=
        (tRipPeerRec *) NULL)
    {

        *pu4RetValRip2PeerLastUpdate = pRipCurPeerRec->u4PeerLastUpdateTime;

        /***  $$TRACE_LOG (INTMD, "Exiting function, rip_get_rip2PeerLastUpdate"
                "\n Last Update = %d \n", *pu4RetValRip2PeerLastUpdate);  ***/
        return SNMP_SUCCESS;
    }

    /***  $$TRACE_LOG (INTMD, "Exiting function, rip_get_rip2PeerLastUpdate"
            "\n Failure in GET on rip2PeerLastUpdate \n");  ***/

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRip2PeerVersion
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain

                The Object
                retValRip2PeerVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2PeerVersion ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2PeerVersion (UINT4 u4Rip2PeerAddress,
                       tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                       INT4 *pi4RetValRip2PeerVersion)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    UINT1              *pu1Domain = pRip2PeerDomain->pu1_OctetList;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_rip2PeerVersion \n, Get request->"
            "Peer = %x, Domain = %c%c", u4Rip2PeerAddress, pRip2PeerDomain[0], pRip2PeerDomain[1]
            );  ***/

    if ((pRipCurPeerRec = rip_retrieve_record_for_peer (u4Rip2PeerAddress,
                                                        pu1Domain,
                                                        pRipCxtEntry)) !=
        (tRipPeerRec *) NULL)
    {

        *pi4RetValRip2PeerVersion = (INT4) (pRipCurPeerRec->u2PeerVersion);

        /***  $$TRACE_LOG (INTMD, "Exiting function, rip_get_rip2PeerVersion"
                "\n Last Version = %d \n", *pi4RetValRip2PeerVersion);  ***/

        return SNMP_SUCCESS;
    }

    /***  $$TRACE_LOG (INTMD, "Exiting function, rip_get_rip2PeerVersion"
            "\n Failure in GET on rip2PeerVersion \n");  ***/

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRip2PeerRcvBadPackets
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain

                The Object
                retValRip2PeerRcvBadPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2PeerRcvBadPackets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2PeerRcvBadPackets (UINT4 u4Rip2PeerAddress,
                             tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                             UINT4 *pu4RetValRip2PeerRcvBadPackets)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    UINT1              *pu1Domain = pRip2PeerDomain->pu1_OctetList;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_rip2PeerRcvBadPackets \n, Get request->"
            "Peer = %x, Domain = %c%c", u4Rip2PeerAddress, pRip2PeerDomain->pu1_OctetList[0], pRip2PeerDomain->pu1_OctetList[1]
            );  ***/

    if ((pRipCurPeerRec = rip_retrieve_record_for_peer (u4Rip2PeerAddress,
                                                        pu1Domain,
                                                        pRipCxtEntry)) !=
        (tRipPeerRec *) NULL)
    {

        *pu4RetValRip2PeerRcvBadPackets = pRipCurPeerRec->u4PeerRcvBadPackets;

        /***  $$TRACE_LOG (INTMD, "Exiting function"
                "rip_get_rip2PeerRcvBadPackets \n NoOfBadPkts = %d \n",
                *pu4RetValRip2PeerRcvBadPackets);  ***/

        return SNMP_SUCCESS;
    }

    /***  $$TRACE_LOG (INTMD, "Exiting function, rip_get_rip2PeerRcvBadPackets"
            "\n Failure in GET on rip2PeerRcvBadPackets \n");  ***/

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetRip2PeerRcvBadRoutes
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain

                The Object
                retValRip2PeerRcvBadRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2PeerRcvBadRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2PeerRcvBadRoutes (UINT4 u4Rip2PeerAddress,
                            tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                            UINT4 *pu4RetValRip2PeerRcvBadRoutes)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    UINT1              *pu1Domain = pRip2PeerDomain->pu1_OctetList;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_rip2PeerRcvBadRoutes \n, Get request->"
            "Peer = %x, Domain = %c%c", u4Rip2PeerAddress, pRip2PeerDomain[0], pRip2PeerDomain[1]
            );  ***/

    if ((pRipCurPeerRec = rip_retrieve_record_for_peer (u4Rip2PeerAddress,
                                                        pu1Domain,
                                                        pRipCxtEntry)) !=
        (tRipPeerRec *) NULL)
    {

        *pu4RetValRip2PeerRcvBadRoutes = pRipCurPeerRec->u4PeerRcvBadRoutes;

        /***  $$TRACE_LOG (INTMD, "Exiting function"
                "rip_get_rip2PeerRcvBadRoutes \n NoOfBadRoutes = %d \n",
                *pu4RetValRip2PeerRcvBadRoutes);  ***/

        return SNMP_SUCCESS;
    }

    /***  $$TRACE_LOG (INTMD, "Exiting function, rip_get_rip2PeerRcvBadRoutes"
            "\n Failure in GET on rip2PeerRcvBadRoutes \n");  ***/

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsRip2PeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip2PeerTable
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRip2PeerTable (UINT4 u4Rip2PeerAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRip2PeerDomain)
{
    if (nmhValidateIndexInstanceRip2PeerTable (u4Rip2PeerAddress,
                                               pRip2PeerDomain) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip2PeerTable
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRip2PeerTable (UINT4 *pu4Rip2PeerAddress,
                                 tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain)
{
    if (nmhGetFirstIndexRip2PeerTable (pu4Rip2PeerAddress,
                                       pRip2PeerDomain) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip2PeerTable
 Input       :  The Indices
                Rip2PeerAddress
                nextRip2PeerAddress
                Rip2PeerDomain
                nextRip2PeerDomain
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRip2PeerTable (UINT4 u4Rip2PeerAddress,
                                UINT4 *pu4NextRip2PeerAddress,
                                tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                                tSNMP_OCTET_STRING_TYPE * pNextRip2PeerDomain)
{
    if (nmhGetNextIndexRip2PeerTable (u4Rip2PeerAddress,
                                      pu4NextRip2PeerAddress,
                                      pRip2PeerDomain,
                                      pNextRip2PeerDomain) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip2PeerInUseKey
 Input       :  The Indices
                Rip2PeerAddress
                Rip2PeerDomain

                The Object 
                retValFsRip2PeerInUseKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2PeerInUseKey (UINT4 u4Rip2PeerAddress,
                          tSNMP_OCTET_STRING_TYPE * pRip2PeerDomain,
                          INT4 *pi4RetValFsRip2PeerInUseKey)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    UINT1              *pu1Domain = pRip2PeerDomain->pu1_OctetList;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering function"
            "rip_get_rip2PeerInUseKeyId \n, Get request->"
            "Peer = %x, Domain = %c%c", u4Rip2PeerAddress, pRip2PeerDomain[0], pRip2PeerDomain[1]
            );  ***/

    if ((pRipCurPeerRec = rip_retrieve_record_for_peer (u4Rip2PeerAddress,
                                                        pu1Domain,
                                                        pRipCxtEntry)) !=
        (tRipPeerRec *) NULL)
    {

        *pi4RetValFsRip2PeerInUseKey = (INT4) (pRipCurPeerRec->u1KeyIdInUse);

        /***  $$TRACE_LOG (INTMD, "Exiting function, "rip_get_rip2PeerInUseKeyId"
                "\n Last Version = %d \n", *pi4RetValFsRip2PeerInUseKey);  ***/

        return SNMP_SUCCESS;
    }

    /***  $$TRACE_LOG (INTMD, "Exiting function, "rip_get_rip2PeerInUseKeyId"
            "\n Failure in GET on Rip2PeerInUseKey \n");  ***/

    return SNMP_FAILURE;

}

/* LOW LEVEL Routines for Table : FsRip2IfStatTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRip2IfStatTable (UINT4 u4Rip2IfStatAddress)
{
    if (nmhValidateIndexInstanceRip2IfStatTable (u4Rip2IfStatAddress)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRip2IfStatTable (UINT4 *pu4Rip2IfStatAddress)
{
    if (nmhGetFirstIndexRip2IfStatTable (pu4Rip2IfStatAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip2IfStatTable
 Input       :  The Indices
                Rip2IfStatAddress
                nextRip2IfStatAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRip2IfStatTable (UINT4 u4Rip2IfStatAddress,
                                  UINT4 *pu4NextRip2IfStatAddress)
{
    if (nmhGetNextIndexRip2IfStatTable
        (u4Rip2IfStatAddress, pu4NextRip2IfStatAddress) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip2IfStatRcvBadAuthPackets
 Input       :  The Indices
                Rip2IfStatAddress

                The Object 
                retValFsRip2IfStatRcvBadAuthPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2IfStatRcvBadAuthPackets (UINT4 u4Rip2IfStatAddress,
                                     UINT4
                                     *pu4RetValFsRip2IfStatRcvBadAuthPackets)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfStatRcvBadPackets"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4Rip2IfStatAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsRip2IfStatRcvBadAuthPackets =
        (UINT4) (pRipIfRec->RipIfaceStats.u4InBadAuthPackets);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfStatRcvBadAuthPackets"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

        /***************************************************************
        >>>>>>>>> PRIVATE Routines of this Module Start Here <<<<<<<<<<
        ***************************************************************/

/*******************************************************************************

    Function            :   rip_validate_row_status.

    Description         :   This function vaildates the critical variable
                            "Status" which is the RowStatus parameter in the
                            rip2ConfIfTable and rip2IfStatTable.

    Input parameters    :   u4IfAddr, which is the index for the table
                            rip2IfConfTable,
                            i4StatVal.

    Output parameters   :   None.

    Global variables
    Affected            :   None.

    Return value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_validate_row_status
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE INT1
rip_validate_row_status (UINT4 u4IfAddr, INT4 i4StatVal, tRipCxt * pRipCxtEntry)
{
    INT1                i1RowStat, i1RetStat = (INT1) RIP_SUCCESS;
    UINT2               u2If;
    tRipIfaceRec       *pRipIfRec = NULL;

    /***  $$TRACE_LOG (ENTRY, "Entering function, rip_validate_row_status"
            "\n");  ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4IfAddr))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Row Status, Failure \n"
                "Address is invalid \n");  ***/
        return RIP_FAILURE;
    }

    /* Validation for row status and it's applicability */

    i1RowStat =
        rip_get_if_index_and_row_status (u4IfAddr, &u2If, RIP_VALIDATE,
                                         pRipCxtEntry);

    switch (i4StatVal)
    {

        case RIPIF_ADMIN_CREATE_AND_GO:

            i1RetStat = (INT1) RIP_FAILURE;
            break;

        case RIPIF_ADMIN_CREATE_AND_WAIT:

            i1RetStat =
                (INT1) (i1RowStat ==
                        RIP_ROW_NOT_EXIST) ? RIP_SUCCESS : RIP_FAILURE;
            break;

        case RIPIF_ADMIN_ACTIVE:
        case RIPIF_ADMIN_NOT_IN_SERVICE:

            if (i1RowStat == RIP_ROW_NOT_EXIST || i1RowStat == RIP_FAILURE)
            {

                i1RetStat = (INT1) RIP_FAILURE;
                break;
            }
            /* Get the Interface Record from Hash Table */
            pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
            if (pRipIfRec == NULL)
            {
                /*Interface Record Not Found */
                return RIP_FAILURE;
            }

            if ((pRipIfRec->RipIfaceCfg.u2AdminStatus ==
                 RIPIF_ADMIN_NOT_IN_SERVICE) ||
                (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_ACTIVE))
                break;            /* Success */

            if ((pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_NOT_READY)
                && (pRipIfRec->RipIfaceCfg.u1IfCurMask == RIPIF_CFG_STAT_MASK))
                break;            /* Success */
            else
                i1RetStat = (INT1) RIP_FAILURE;    /* Inconsistent Value  */
            break;

        case RIPIF_ADMIN_DESTROY:
            break;                /* Success */

        default:

            i1RetStat = (INT1) RIP_FAILURE;
            break;

    }                            /* End of switch ( i4StatVal ) */

    /***  $$TRACE_LOG (EXIT, "Validation for RowStatus \n");  ***/

    return i1RetStat;
}

/*******************************************************************************

    Function            :   rip_set_if_parameter.

    Description         :   This function does 4 primary things.
                            1. Retrieves an instance corresponding to the
                               address either after creation of one (If at all,
                               a possibility exists) or an existing one.
                            2. Assigns the value of the parameter to the actual
                               variable.
                            3. Manipulates the row status.
                            4. Manages the interface list.

    Input Parameters    :   1. u4IfAddr, the interface address.
                            2. pvVal, the value to set.
                            3. u1ValFlag, a flag to denote the variable to be
                               set.

    Output Parameters   :   None.

    Global Variables
    Affected            :   RipIfGblRec, the interface record.

    Return Value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_set_if_parameter
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE INT1
rip_set_if_parameter (UINT4 u4IfAddr, VOID *pvVal, UINT1 u1ValFlag)
{
    INT1                i1RetStat;
    UINT2               u2IfInd;

    UINT4               u4Val;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT1               u1OperStatus = 0;
    INT1                i1IsDC = RIP_SUCCESS;

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }
    u4Val = *((UINT4 *) pvVal);

    if ((i1RetStat = rip_get_if_index_and_row_status (u4IfAddr, &u2IfInd,
                                                      (UINT1) RIP_SET,
                                                      pRipCxtEntry)) ==
        RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Error in getting index and row status\n");
        return RIP_FAILURE;        /* Iface would not have been
                                   configured at IP level          */
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return RIP_FAILURE;
    }

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
    /*
     * Instance would anyway be existing now. Proceed with modifying the value
     * of the variable and then we will worry about the state of the row. Take
     * care to set the dependency mask for critical variables.
     */

    switch (u1ValFlag)
    {

        case RIPIF_DOMAIN:

            /***  $TRACE_LOG (INTMD, "Set for RIP_DOMAIN  \n");  ***/

            pRipIfRec->RipIfaceCfg.au1RouteDomain[0] =
                (INT1) ((UINT1 *) pvVal)[0];
            pRipIfRec->RipIfaceCfg.au1RouteDomain[1] =
                (INT1) ((UINT1 *) pvVal)[1];
            break;

        case RIPIF_AUTHTYPE:

            /***  $TRACE_LOG (INTMD, "Set for RIP_AUTHTYPE  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2AuthType = (UINT2) u4Val;
            break;

        case RIPIF_AUTHKEY:

            /***  $TRACE_LOG (INTMD, "Set for RIP_AUTHKEY  \n");  ***/

            MEMSET ((VOID *) pRipIfRec->RipIfaceCfg.au1AuthKey, '\0',
                    MAX_AUTH_KEY_LENGTH);
            MEMCPY ((INT1 *) pRipIfRec->RipIfaceCfg.au1AuthKey,
                    ((tSNMP_OCTET_STRING_TYPE *) pvVal)->pu1_OctetList,
                    ((tSNMP_OCTET_STRING_TYPE *) pvVal)->i4_Length);
            break;

        case RIPIF_SEND:

            /***  $TRACE_LOG (INTMD, "Set for RIP_SEND  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2RipSendStatus = (UINT2) u4Val;
            break;

        case RIPIF_RECV:
        {

            UINT2               u2RecvStat = (UINT2) u4Val;

            /***  $TRACE_LOG (INTMD, "Set for RIP_RECV  \n");  ***/

            /*
             * Multicast mgmt needs to be taken care 
             */

            if (i1RetStat == RIP_ROW_NOT_EXIST_CREATED)
            {

                if ((u2RecvStat != RIPIF_VERSION_2_RCV) ||
                    (u2RecvStat != RIPIF_1_OR_2))
                {

                    /* Leave the mcast registry group */
                    rip_ip_leave_mcast_group (u2IfInd);
                }
                /* Registry is done during the creation */
            }

            else
            {

                /* Only the following cases suffice */

                if (((pRipIfRec->RipIfaceCfg.u2RipRecvStatus !=
                      RIPIF_VERSION_2_RCV)
                     || (pRipIfRec->RipIfaceCfg.u2RipRecvStatus !=
                         RIPIF_1_OR_2)) && ((u2RecvStat == RIPIF_1_OR_2)
                                            || (u2RecvStat ==
                                                RIPIF_VERSION_2_RCV)))
                {

                    /* Join the mcast registry group */
                    rip_ip_join_mcast_group (u2IfInd);
                }

                else if (((pRipIfRec->RipIfaceCfg.u2RipRecvStatus ==
                           RIPIF_VERSION_2_RCV)
                          || (pRipIfRec->RipIfaceCfg.u2RipRecvStatus ==
                              RIPIF_1_OR_2)) && ((u2RecvStat != RIPIF_1_OR_2)
                                                 || (u2RecvStat !=
                                                     RIPIF_VERSION_2_RCV)))
                {

                    /* Leave the mcast registry group */
                    rip_ip_leave_mcast_group (u2IfInd);
                }
            }
            /* Join the mcast registry group */

            pRipIfRec->RipIfaceCfg.u2RipRecvStatus = u2RecvStat;

            break;
        }

        case RIPIF_DEF_MET:

            /***  $TRACE_LOG (INTMD, "Set for RIP_DEF_METRIC  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2DefaultMetric = (UINT2) u4Val;
            pRipIfRec->RipIfaceCfg.u1IfCurMask |= RIP_DEF_METRIC_BIT;
            break;

        case RIPIF_ROW_STAT:

            /*
             * we need to take the appropriate actions for setting the row
             * status to the appropriate value.
             */
            rip_action_for_iface_row_status (u2IfInd, u4IfAddr, (UINT2) u4Val,
                                             pRipCxtEntry);
            /*
             * if the RowStatus has been "Destroy", we can as well return from
             * here as we have no further work to do.
             */

            if (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_NOT_EXIST)
            {
                return RIP_SUCCESS;
            }

            break;

        case RIPIF_SRC_ADDR:

            /***  $TRACE_LOG (INTMD, "Set for RIP_SRC_ADDR  \n");  ***/

            pRipIfRec->RipIfaceCfg.u4SrcAddr = *((UINT4 *) pvVal);
            pRipIfRec->RipIfaceCfg.u1IfCurMask |= RIP_SRC_ADDR_BIT;
            break;

        case RIPIF_UPD_TMR:

            /***  $TRACE_LOG (INTMD, "Set for RIP_UPDATE_TMR  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2UpdateInterval = (UINT2) u4Val;

            break;

        case RIPIF_GARB_TMR:

            /***  $TRACE_LOG (INTMD, "Set for RIP_GARBAGE_TIMER  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval = (UINT2) u4Val;
            break;

        case RIPIF_AGE_TMR:

            /***  $TRACE_LOG (INTMD, "Set for RIP_AGE_TIMER  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2RouteAgeInterval = (UINT2) u4Val;
            break;
        case RIPIF_ADMIN_FLAG:
            /* Following checks are made for Neighbor unicast update.
             * To send the Unicast packet on the passive interface,
             * we have a global timer. The timer is based on the number
             * of neighbors for the passive interfaces.
             */

            if ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag != RIP_ADMIN_PASSIVE) &&
                (u4Val == RIP_ADMIN_PASSIVE))
            {
                if ((pRipCxtEntry->u4NumOfPassNeighbors == 0) &&
                    RIP_SLL_Count (&(pRipIfRec->RipUnicastNBRS)) != 0)
                {
                    pRipCxtEntry->RipPassiveUpdTimer.u1Id =
                        RIP_PASS_UPD_TIMER_ID;
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &((pRipCxtEntry->RipPassiveUpdTimer).
                                       Timer_node), RIP_DEF_UPDATE_INTERVAL);
                }
                pRipCxtEntry->u4NumOfPassNeighbors =
                    pRipCxtEntry->u4NumOfPassNeighbors +
                    RIP_SLL_Count (&(pRipIfRec->RipUnicastNBRS));
            }
            else if ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag ==
                      RIP_ADMIN_PASSIVE) && (u4Val != RIP_ADMIN_PASSIVE))
            {
                pRipCxtEntry->u4NumOfPassNeighbors =
                    pRipCxtEntry->u4NumOfPassNeighbors -
                    RIP_SLL_Count (&(pRipIfRec->RipUnicastNBRS));
                if (pRipCxtEntry->u4NumOfPassNeighbors == 0)
                {
                    RIP_STOP_TIMER (RIP_TIMER_ID,
                                    &((pRipCxtEntry->RipPassiveUpdTimer).
                                      Timer_node));
                }
            }

            switch ((UINT1) u4Val)
            {

                case RIP_ADMIN_ENABLE:

                    u1OperStatus = RipIfGetOperStatusFromPort (u2IfInd);
                    if ((u1OperStatus == IPIF_OPER_ENABLE) &&
                        (pRipCxtEntry->u1AdminStatus != RIP_ADMIN_DISABLE))
                    {
                        pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_ENABLE;
                    }
                    /* Link the timer only previous state the flag is not 
                     * admin enable 
                     */
                    if ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag !=
                         RIP_ADMIN_ENABLE)
                        && (pRipIfRec->RipIfaceCfg.u2OperStatus ==
                            RIPIF_OPER_ENABLE))
                    {
                        pRipIfRec->RipIfaceCfg.u1RipAdminFlag =
                            RIP_ADMIN_ENABLE;
                        rip_if_enable (pRipIfRec);
                    }
                    break;

                case RIP_ADMIN_DISABLE:
                    rip_if_disable (pRipIfRec);
                    break;
                case RIP_ADMIN_PASSIVE:
                    if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag ==
                        RIP_ADMIN_ENABLE)
                    {
                        if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE)
                            || (i1IsDC == RIP_SUCCESS))
                        {
                            rip_if_update_periodic_timer (RIP_UNLINK_TIMERS,
                                                          u2IfInd,
                                                          pRipCxtEntry);
                        }
                        else
                        {
                            RIP_STOP_TIMER (RIP_TIMER_ID,
                                            &pRipIfRec->RipSpacingTimer.
                                            TimerNode);
                            RIP_STOP_TIMER (RIP_TIMER_ID,
                                            &pRipIfRec->RipUpdateTimer.
                                            TimerNode);
                            pRipIfRec->RipIfaceCfg.u2RipSendStatus =
                                RIPIF_DO_NOT_SEND;
                        }
                        pRipIfRec->RipIfaceCfg.u1KeyIdInUse = RIP_ZERO;
                    }

                    break;

                default:
                    return RIP_FAILURE;

            }

            pRipIfRec->RipIfaceCfg.u1RipAdminFlag = (UINT1) u4Val;
            break;

        case RIPIF_SPLIT_HORIZON:

            /***  $TRACE_LOG (INTMD, "Set for RIP_SPLIT_HORIZON  \n");  ***/

            pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus = (UINT2) u4Val;
            break;

        case RIPIF_DEF_RT_INSTALL:
            /* Set the default route install status */
            pRipIfRec->RipIfaceCfg.u1DefRtInstallStatus = (UINT1) u4Val;
            break;
        case RIPIF_SPACE_TMR_VAL:
            pRipIfRec->RipIfaceCfg.u2SpacingTmrVal = (UINT2) u4Val;
            break;

        default:
            return RIP_FAILURE;

    }                            /* End of switch (u1ValFlag) */

    /*
     * We should be doing the following action, if the row was created OR the
     * admin status of the iface is equal to the "NotReady" if the row was
     * present.
     */

    if ((i1RetStat == RIP_ROW_NOT_EXIST_CREATED)
        || (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_NOT_READY))
    {

        pRipIfRec->RipIfaceCfg.u2AdminStatus =
            ((pRipIfRec->RipIfaceCfg.u1IfCurMask == RIPIF_CFG_STAT_MASK)
             ? RIPIF_ADMIN_NOT_IN_SERVICE : RIPIF_ADMIN_NOT_READY);
    }

    /*
     * We organize the list of interfaces from the valid interface ending with
     * the invalid one, let us do that.
     */

    return RIP_SUCCESS;
}

/*******************************************************************************

    Function            :   rip_get_if_index_and_row_status.

    Description         :   This function returns the interface index of the RIP
                            confirutation table. This is called from two places.
                            One from "Set" Operation of settable variable, and
                            another from "Validate" operation of any settable
                            variable. It behaves differently both the times.
                            When it is for "Set" operation, it creates a new
                            instance if one is unavailable and accordingly
                            manages the RowStatus for that instance and returns
                            the id. When it is for "Validate" option, it doesn't
                            create a new instance but it returns an index, if
                            one is available or a meaningful message of row
                            status not present etc.
                          
    Input parameters    :   u4IfAddr, which is the index for the table
                            rip2IfConfTable,
                            pu2IfInd, the interface index address.
                            u1OperFlag, denotes the operation either as "Set" or
                            "Validate".

    Output parameters   :   pu2IfInd.

    Global variables
    Affected            :   None.

    Return value        :   Rip_row_already_exist | Rip_row_not_exist_created |
                            Rip_row_not_exist | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_if_index_and_row_status
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE INT1
rip_get_if_index_and_row_status (UINT4 u4IfAddr, UINT2 *pu2IfInd,
                                 UINT1 u1OperFlag, tRipCxt * pRipCxtEntry)
{
    INT4                i4RetIfIns, i4RetIf;
    INT1                i1RetStat = RIP_ROW_ALREADY_EXIST;
    tRIP_INTERFACE      IfId;
    tRipIfaceRec       *pRipIfRec = NULL;

    /*
     * As a first step, identify the logical interface index by queryig IP.
     * This will now return IfIndex for Unnumbered.
     */

    if ((i4RetIf =
         RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                             u4IfAddr)) == RIP_FAILURE)
    {
        /* the L3 interface itself is not configured */
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return RIP_FAILURE;
    }

    /*
     * Retrieve the interface Id (with subreference) from the interface index.
     */

    RIPIF_GET_IFID_FROM_INDEX ((UINT4) i4RetIf, &IfId);

    /*
     * Then, check to see whether any instance exists with the the interface id
     * in our interface database.
     */
    /* ******************************************************************************
     * 1. Routine rip_get_ifinstance () is updated with new prototypes
     *    so that it fetches the perfect record based on IP address.
     * 2. This will be useful when there are many IP addresses configured
     *    for a single interface.
     *************************************************************************/

    if ((i4RetIfIns =
         rip_get_if_instance (IfId, u4IfAddr, pRipCxtEntry)) == RIP_FAILURE)
    {
        *pu2IfInd = (UINT2) i4RetIf;
        if (u1OperFlag == RIP_SET)
        {

            /*
             * We shall behave as mentioned in the RFC 1903, on RowStatus. We
             * won't be returning any error value, instead the instance of the
             * status column is created with that of IPModule's interface index.
             */

            if (rip_create_if_instance (*pu2IfInd, IfId, u4IfAddr, pRipCxtEntry)
                == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "Failed while creating new instance\n");

                /***  $$TRACE_LOG (EXIT, "Creation of new instance failure"
                        " \n");  ***/
                return RIP_FAILURE;
            }

            /*
             * Let us set the row status bit as we are creating the row instance
             * out here.
             */

            /***  $TRACE_LOG (INTMD, "Set for RIP_ROW_STAT  \n");  ***/

            /* Get the Interface Record from Hash Table */
            pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
            if (pRipIfRec == NULL)
            {
                /*Interface Record Not Found */
                return SNMP_FAILURE;
            }

            pRipIfRec->RipIfaceCfg.u1IfCurMask |= RIP_STAT_BIT;

            i1RetStat = RIP_ROW_NOT_EXIST_CREATED;
        }
        else
        {
            /* For Validation */
            i1RetStat = RIP_ROW_NOT_EXIST;
        }

    }                            /* End of if ( (i4RetIfIns = rip_get_if_instance ) ) */

    else
    {

        /* Row is already present, assign the interface id and pack up */

        *pu2IfInd = (UINT2) i4RetIfIns;
    }

    return (i1RetStat);
}

/*******************************************************************************

    Function            :   rip_get_if_instance.

    Description         :   This returns either the interface index if one is
                            available or a failure message, for an interface ID.
                          
    Input parameters    :   pIfId, the interface id of type tRIP_INTERFACE *.

    Output parameters   :   None.

    Global variables
    Affected            :   None.

    Return value        :  The interface index | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_if_instance
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE INT4
rip_get_if_instance (tRIP_INTERFACE IfId, UINT4 u4IpAddr,
                     tRipCxt * pRipCxtEntry)
{
    UINT2               u2IfIndex;
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_if_instance \n, the IfId"
            "is %d", *( (UINT4*) &IfId ));  ***/

    /*
     * Now, having got the interface id, we scan through the list of interfaces
     * and compare with the current one and return the index or status
     * accordingly.
     */

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {

            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            if ((RIP_GET_IFACE_TYPE (pRipIfRec->IfaceId) ==
                 RIP_GET_IFACE_TYPE (IfId)) &&
                (RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId) ==
                 RIP_GET_IFACE_INDEX (IfId)) &&
                (RIP_GET_IFACE_SUBREF (pRipIfRec->IfaceId) ==
                 RIP_GET_IFACE_SUBREF (IfId)))
            {

                u2IfIndex = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);
                /*
                 * Interface is a valid one, and we have the row instance that has
                 * been created.
                 */

                 /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_if_instance, \n, Got
                         the iface index = %d \n", u2IfIndex);  ***/
                /* Check for the IP and match withe the input */
                if (pRipIfRec->u4Addr == u4IpAddr)
                {
                    return (INT4) u2IfIndex;
                }
            }
        }
    }                            /* End Of Interface Hash Table Scan */

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_if_instance, \n, No instance
            found \n");  ***/

    return RIP_FAILURE;
}

/*******************************************************************************

    Function            :   rip_get_if_index.

    Description         :   This returns either the interface index if one is
                            available or a failure message, for the interface
                            address.
                          
    Input parameters    :   1. u4IfAddr, the interface address of type UINT4.
                            2. pu2If, the interface index.

    Output parameters   :   pu2If.

    Global variables
    Affected            :   None.

    Return value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_if_index
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

INT2
rip_get_if_index (UINT4 u4IfAddr, UINT2 *pu2If, tRipCxt * pRipCxtEntry)
{
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_if_index, \n For the"
            "address %x \n", u4IfAddr);  ***/

    /*
     *  Now, having got the interface address, we scan through the list of
     *  interfaces and compare with the current one and return the index or
     *  status accordingly.
     *  This function will be called by almost all Get Operations.
     */

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            if (pRipIfRec->u4Addr == u4IfAddr)
            {

                /*
                 * Interface is a valid one, and we have the row instance that has
                 * been created.
                 */

                *pu2If = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

                /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_if_index, \n, Got
                        the iface index = %d \n", *pu2If);  ***/

                return RIP_SUCCESS;
            }

        }

    }                            /* end of If Hash Table Scan */

    RIP_TRC (RIP_MOD_TRC,
             pRipCxtEntry->u4RipTrcFlag,
             pRipCxtEntry->i4CxtId,
             ALL_FAILURE_TRC, RIP_NAME,
             "Exiting fn,Failure in getting iface index");
    /***  $TRACE_LOG (EXIT, "Exiting fn, rip_get_if_index, \n, Failure in'
           "getting the iface index \n");  ***/

    return RIP_FAILURE;
}

/*******************************************************************************

    Function            :   rip_create_if_instance.

    Description         :   This creates a new interface instance and assigns
                            the default parameters for the circuit to be
                            operational.
                          
    Input parameters    :   1. u2IfInd, the new iface index with which the
                               instance needs to be associated.
                            2. IfId, the interface index.
                            3. u4IfAddr, the interface address.

    Output parameters   :   None.

    Global variables
    Affected            :   RipIfGblRec.

    Return value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_create_if_instance
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE INT4
rip_create_if_instance (UINT2 u2IfInd, tRIP_INTERFACE IfId, UINT4 u4IfAddr,
                        tRipCxt * pRipCxtEntry)
{
    INT4                i4OperState = IPIF_OPER_DISABLE;
    tNetIpv4IfInfo      NetIpIfInfo;

    tIpBuf             *pBuf = NULL;
    tIpParms           *pParms = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pTempRipIfRec = NULL;
    tCryptoAuthKeyInfo *pNode = NULL;
    tCryptoAuthKeyInfo *pTempNode = NULL;
    UINT4               u4CfaIfIndex;
    UINT4               u4TempNetmask;
    INT4                i4RetVal = 0;

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_create_if_instance, \n The"
            "Iface Index = %d, \n Iface Id = %d, \n Iface Addr = %x, \n",
            u2IfInd, *( (UINT4*) &IfId ), u4IfAddr);  ***/

    /* check if there is already any rip record present with this interface id
     * We need to copy the details of interface config if the record is present
     * for secondary/primary address in the same L3 vlan interface*/

    pTempRipIfRec = RipGetIfRecFromIndex (u2IfInd);

    /* Allocate Mem Block for the Interface Record */
    pRipIfRec = rip_if_create (IfId, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    pRipIfRec->u4Addr = u4IfAddr;

    if (NetIpv4GetIfInfo ((UINT4) u2IfInd, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return RIP_FAILURE;
    }

    /*get CfaIfIndex from interface index */
    i4RetVal = NetIpv4GetCfaIfIndexFromPort (u2IfInd, &u4CfaIfIndex);

    if (RIPIF_IS_UNNUMBERED (u2IfInd) == TRUE)
    {
        pRipIfRec->u4NetMask = 0xFFFFFFFF;
    }
    /* check if the interface adddress is secondary Address
     *   * for secondary address mask is got from querying CFA*/
    else if (CfaIpIfIsSecLocalNetOnInterface
             (u4CfaIfIndex, u4IfAddr, &u4TempNetmask) == RIP_SUCCESS)
    {
        pRipIfRec->u4NetMask = u4TempNetmask;
    }
    else
    {
        pRipIfRec->u4NetMask = NetIpIfInfo.u4NetMask;
    }

    /* already a rip record exist in this index hence copy the record contents to the new record */
    /* the rest of the operations on the index is already done during first initialization */
    if (pTempRipIfRec != NULL)
    {

        pRipIfRec->RipIfaceCfg.u2DefaultMetric =
            pTempRipIfRec->RipIfaceCfg.u2DefaultMetric;
        pRipIfRec->RipIfaceCfg.u2UpdateInterval =
            pTempRipIfRec->RipIfaceCfg.u2UpdateInterval;
        pRipIfRec->RipIfaceCfg.u2RouteAgeInterval =
            pTempRipIfRec->RipIfaceCfg.u2RouteAgeInterval;
        pRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval =
            pTempRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval;
        pRipIfRec->RipIfaceCfg.u2SpacingTmrVal =
            pTempRipIfRec->RipIfaceCfg.u2SpacingTmrVal;
        pRipIfRec->RipIfaceCfg.au1RouteDomain[0] =
            pTempRipIfRec->RipIfaceCfg.au1RouteDomain[0];
        pRipIfRec->RipIfaceCfg.au1RouteDomain[1] =
            pTempRipIfRec->RipIfaceCfg.au1RouteDomain[1];
        pRipIfRec->RipIfaceCfg.u2AuthType =
            pTempRipIfRec->RipIfaceCfg.u2AuthType;
        STRNCPY (pRipIfRec->RipIfaceCfg.au1AuthKey,
                 pTempRipIfRec->RipIfaceCfg.au1AuthKey, MAX_AUTH_KEY_LENGTH);
        pRipIfRec->RipIfaceCfg.u2RipSendStatus =
            pTempRipIfRec->RipIfaceCfg.u2RipSendStatus;
        pRipIfRec->RipIfaceCfg.u2RipRecvStatus =
            pTempRipIfRec->RipIfaceCfg.u2RipRecvStatus;
        pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus =
            pTempRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus;
        pRipIfRec->RipIfaceCfg.u1DefRtInstallStatus =
            pTempRipIfRec->RipIfaceCfg.u1DefRtInstallStatus;
        pRipIfRec->RipIfaceCfg.u2OperStatus =
            pTempRipIfRec->RipIfaceCfg.u2OperStatus;
        pRipIfRec->RipIfaceCfg.u1RipAdminFlag =
            pTempRipIfRec->RipIfaceCfg.u1RipAdminFlag;
        pRipIfRec->RipIfaceCfg.u2AdminStatus =
            pTempRipIfRec->RipIfaceCfg.u2AdminStatus;
        pRipIfRec->RipIfaceCfg.u1KeyIdInUse =
            pTempRipIfRec->RipIfaceCfg.u1KeyIdInUse;

        /* allocate for cyptoallocate list */
        if (RIP_SLL_Count (&(pTempRipIfRec->RipCryptoAuthKeyList)) != 0)
        {
            RIP_SLL_Scan (&(pTempRipIfRec->RipCryptoAuthKeyList), pTempNode,
                          tCryptoAuthKeyInfo *)
            {
                if (pTempNode != NULL)
                {
                    RIP_ALLOC_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pNode);
                    if (pNode == NULL)
                    {
                        return RIP_FAILURE;
                    }

                    /*copy not oly key but all the values required for ppde */

                    RIP_CRYPTO_AUTH_KEY_ID (pNode) =
                        RIP_CRYPTO_AUTH_KEY_ID (pTempNode);
                    RIP_CRYPTO_SEQUENCE_NO (pNode) =
                        RIP_CRYPTO_SEQUENCE_NO (pTempNode);
                    RIP_CRYPTO_START_GEN_TIME (pNode) =
                        RIP_CRYPTO_START_GEN_TIME (pTempNode);
                    RIP_CRYPTO_START_ACCEPT_TIME (pNode) =
                        RIP_CRYPTO_START_ACCEPT_TIME (pTempNode);
                    RIP_CRYPTO_STOP_GEN_TIME (pNode) =
                        RIP_CRYPTO_STOP_GEN_TIME (pTempNode);
                    RIP_CRYPTO_STOP_ACCEPT_TIME (pNode) =
                        RIP_CRYPTO_STOP_ACCEPT_TIME (pTempNode);
                    RIP_CRYPTO_STATUS (pNode) = RIP_CRYPTO_STATUS (pTempNode);

                    MEMSET (RIP_CRYPTO_AUTH_KEY (pNode), 0,
                            MAX_AUTH_KEY_LENGTH);
                    MEMCPY (RIP_CRYPTO_AUTH_KEY (pNode),
                            RIP_CRYPTO_AUTH_KEY (pTempNode),
                            RIP_CRYPTO_AUTH_KEY_LEN (pTempNode));
                    RIP_CRYPTO_AUTH_KEY_LEN (pNode) =
                        RIP_CRYPTO_AUTH_KEY_LEN (pTempNode);
                    RipSortInsertAuthKey (&(pRipIfRec->RipCryptoAuthKeyList),
                                          pNode);
                }
            }
        }
    }
    else
    {
        /*
         * Initialize the default value variables.
         */

        pRipIfRec->RipIfaceCfg.u2DefaultMetric = RIP_NO_DEFAULT_ROUTE_METRIC;
        pRipIfRec->RipIfaceCfg.u2UpdateInterval =
            (UINT2) RIP_DEF_UPDATE_INTERVAL;
        pRipIfRec->RipIfaceCfg.u2RouteAgeInterval = (UINT2) RIP_DEF_ROUTE_AGE;
        pRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval =
            (UINT2) RIP_DEF_GARBAGE_COLCT_INTERVAL;
        pRipIfRec->RipIfaceCfg.u2SpacingTmrVal = (UINT2) RIP_DEF_SPACE_INTERVAL;

        pRipIfRec->RipIfaceCfg.au1RouteDomain[0] = RIP_NULL_RTAG;
        pRipIfRec->RipIfaceCfg.au1RouteDomain[1] = RIP_NULL_RTAG;
        pRipIfRec->RipIfaceCfg.u2AuthType = (UINT2) RIPIF_NO_AUTHENTICATION;
        /* The padded bytes will any way be NULL in the key      */
        if (STRLEN (RIPIF_DEF_AUTH_KEY) != 0)
        {
            STRNCPY (pRipIfRec->RipIfaceCfg.au1AuthKey, RIPIF_DEF_AUTH_KEY,
                     STRLEN (RIPIF_DEF_AUTH_KEY));
        }
        pRipIfRec->RipIfaceCfg.au1AuthKey[STRLEN (RIPIF_DEF_AUTH_KEY)] = '\0';

        if (RIPIF_IS_UNNUMBERED (u2IfInd) == TRUE)
        {
            pRipIfRec->RipIfaceCfg.u2RipSendStatus =
                (UINT2) RIPIF_VERSION_2_SND;
            pRipIfRec->RipIfaceCfg.u2RipRecvStatus =
                (UINT2) RIPIF_VERSION_2_RCV;
        }
        else
        {
            pRipIfRec->RipIfaceCfg.u2RipSendStatus =
                (UINT2) RIPIF_RIP1_COMPATIBLE;
            pRipIfRec->RipIfaceCfg.u2RipRecvStatus = (UINT2) RIPIF_1_OR_2;
        }

        pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus =
            (UINT2) RIP_SPLIT_HORZ_WITH_POIS_REV;
        pRipIfRec->RipIfaceCfg.u1DefRtInstallStatus =
            (UINT2) RIP_DONT_INSTALL_DEF_RT;
        /* The above assignment is in accordance
         * with the newly release draft MIB   */

        /*
         * In ideal cases, IGMP support is needed to selectively send & receive
         * Multicast packets on a per interface basis. Here, just a workaround is
         * done during the interface creation and if at all receive status is either
         * rip1OrRip2 (Or) rip2, the multicast registry is done.
         */

        rip_ip_join_mcast_group (u2IfInd);

        if (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE)
        {
            pRipIfRec->RipIfaceCfg.u2OperStatus = IPIF_OPER_DISABLE;
            return RIP_SUCCESS;
        }
        /*
         * We query the IP module for setting the oper status .
         */

        if ((i4OperState = rip_ip_get_oper_status (u2IfInd)) == RIP_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME, "Oper state get Failure \n");
            (pRipIfRec->RipIfaceStats.u4RipGetOperStatusFailCount)++;
        /***  $TRACE_LOG (EXIT, "Oper state get failure \n");  ***/
            return RIP_FAILURE;
        }
        else
        {
            pRipIfRec->RipIfaceCfg.u2OperStatus = (UINT2) i4OperState;
        }
        if (i4OperState == IPIF_OPER_ENABLE)

        {
            pBuf = RIP_ALLOCATE_BUF (sizeof (UINT4), 0);
            if (pBuf == NULL)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC, RIP_NAME,
                         "rip_create_if_instance: Buffer allocation failed. \n");
                return RIP_FAILURE;
            }

            pParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pBuf);
            pParms->u2Port = u2IfInd;

            pParms->u1Cmd = IPIF_OPER_ENABLE;

            if (RipSendToQ (pBuf, RIP_SNMP_MSG) != RIP_SUCCESS)
            {
                RIP_RELEASE_BUF (pBuf, FALSE);
                return RIP_FAILURE;
            }
        }

    }
    UNUSED_PARAM (i4RetVal);
    /*RIP_SECONDARY_IP_CHANGES */
    return RIP_SUCCESS;
}

/*******************************************************************************

    Function            :   rip_action_for_iface_row_status.

    Description         :   This function implements the finite state machine
                            for RowStatus (rfc 1903), which is given in this
                            file for easy reference. When the action is to make
                            to the row as "ACTIVE", it adds the network address
                            for this interface to the global route database as
                            a direct network.
                          
    Input parameters    :   u2IfInd, the interface index.
                            u2StatVal, denotes the operation for row status.

    Output parameters   :   None.

    Global variables
    Affected            :   1. RipRtTabList, the global route database may get
                               affected, when the action is to make ACTIVE.
                            2. RipGblStats.u4GlobalRouteChanges.

    Return value        :   Success | Failure.
                            

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_action_for_iface_row_status
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE INT4
rip_action_for_iface_row_status (UINT2 u2IfInd, UINT4 u4IfAddr, UINT2 u2StatVal,
                                 tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    INT1                i1IsDC = RIP_SUCCESS;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return SNMP_FAILURE;
    }

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
    switch (u2StatVal)
    {                            /* Based on the input value */

        case RIPIF_ADMIN_CREATE_AND_WAIT:

            /* if for Primary Ip addres the passive interface is enabled 
             * and now the network command is given for Secondary IP. 
             *The Interface record of the primary will be copied to the 
             *secondary .If passive interface is enabled already donot
             * change it during row create*/

            if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag != RIP_ADMIN_PASSIVE)
            {
                pRipIfRec->RipIfaceCfg.u1RipAdminFlag = RIP_ADMIN_ENABLE;
            }
            pRipIfRec->RipIfaceCfg.u2AdminStatus =
                ((pRipIfRec->RipIfaceCfg.u1IfCurMask == RIPIF_CFG_STAT_MASK)
                 ? RIPIF_ADMIN_NOT_IN_SERVICE : RIPIF_ADMIN_NOT_READY);
            break;

        case RIPIF_ADMIN_ACTIVE:

            /*
             * We need to check whether the interface is already "Active", If
             * so, we don't need to proceed further.
             */
            if (RIPIF_ADMIN_ACTIVE != pRipIfRec->RipIfaceCfg.u2AdminStatus)
            {
                /* The interface is being made active. Hence we need to set
                 * the Adminstrative status and if the Operational status is
                 * up RIP should be enabled.
                 */

                pRipIfRec->RipIfaceCfg.u2AdminStatus = RIPIF_ADMIN_ACTIVE;

                if ((pRipIfRec->RipIfaceCfg.u2OperStatus == RIPIF_OPER_ENABLE)
                    && (pRipCxtEntry->u1AdminStatus != RIP_ADMIN_DISABLE))
                {
                    rip_if_enable (pRipIfRec);
                    if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE)
                        || (i1IsDC == RIP_SUCCESS))
                    {
                        return (RipSendInitialRequest
                                (u2IfInd, u4IfAddr, pRipCxtEntry));
                    }
                    else
                    {
                        return RIP_SUCCESS;
                    }
                }
            }

            break;

        case RIPIF_ADMIN_NOT_IN_SERVICE:

            /*
             * When we are not in service, we should ideally be deleting all the
             * corresponding routes for this interface.
             */
            rip_if_disable (pRipIfRec);
            pRipIfRec->RipIfaceCfg.u2AdminStatus = RIPIF_ADMIN_NOT_IN_SERVICE;
            break;

        case RIPIF_ADMIN_DESTROY:

            /*
             * We need to do the delete operation for the current row indexed by
             * the u2IfInd.
             */

            rip_if_destroy (pRipIfRec);
            break;
        default:
            break;

    }                            /* End of switch (u2Value) */

    return RIP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRip2IfConfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip2IfConfTable
 Input       :  The Indices
                FutRip2IfConfAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsRip2IfConfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsRip2IfConfTable (UINT4 u4FutRip2IfConfAddress)
{
   /*** $$TRACE_LOG (ENTRY,"FutRip2IfConfAddress = %u\n", u4FutRip2IfConfAddress); ***/
    return nmhValidateIndexInstanceRip2IfConfTable (u4FutRip2IfConfAddress);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip2IfConfTable
 Input       :  The Indices
                FutRip2IfConfAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRip2IfConfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsRip2IfConfTable (UINT4 *pu4FutRip2IfConfAddress)
{
   /*** $$TRACE_LOG (ENTRY,"FutRip2IfConfAddress = %u\n", *pu4FutRip2IfConfAddress); ***/
    return nmhGetFirstIndexRip2IfConfTable (pu4FutRip2IfConfAddress);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip2IfConfTable
 Input       :  The Indices
                FutRip2IfConfAddress
                nextFutRip2IfConfAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRip2IfConfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsRip2IfConfTable (UINT4 u4FutRip2IfConfAddress,
                                  UINT4 *pu4NextFutRip2IfConfAddress)
{
   /*** $$TRACE_LOG (ENTRY,"FutRip2IfConfAddress = %u\n", *pu4FutRip2IfConfAddress); ***/
    return nmhGetNextIndexRip2IfConfTable (u4FutRip2IfConfAddress,
                                           pu4NextFutRip2IfConfAddress);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip2IfAdminStat
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                retValFutRip2IfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2IfAdminStat ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2IfAdminStat (UINT4 u4FutRip2IfConfAddress,
                         INT4 *pi4RetValFutRip2IfAdminStat)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRecFromAddr (u4FutRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutRip2IfAdminStat =
        (INT4) (pRipIfRec->RipIfaceCfg.u1RipAdminFlag);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfAdminStatus"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfOperState
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                retValFutRip2IfConfOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2IfConfOperState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2IfConfOperState (UINT4 u4FutRip2IfConfAddress,
                             INT4 *pi4RetValFutRip2IfConfOperState)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfOperState \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4FutRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutRip2IfConfOperState =
        (INT4) (pRipIfRec->RipIfaceCfg.u2OperStatus);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfOperState"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfUpdateTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                retValFutRip2IfConfUpdateTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2IfConfUpdateTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2IfConfUpdateTmr (UINT4 u4FutRip2IfConfAddress,
                             INT4 *pu4RetValFutRip2IfConfUpdateTmr)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfUpdateTmr \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4FutRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFutRip2IfConfUpdateTmr =
        (UINT4) (pRipIfRec->RipIfaceCfg.u2UpdateInterval);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfUpdateTmr"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfGarbgCollectTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                retValFutRip2IfConfGarbgCollectTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2IfConfGarbgCollectTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2IfConfGarbgCollectTmr (UINT4 u4FutRip2IfConfAddress,
                                   INT4 *pu4RetValFutRip2IfConfGarbgCollectTmr)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***   $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfGarbgCollectTmr"
             "\n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4FutRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFutRip2IfConfGarbgCollectTmr =
        (UINT4) (pRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfGarbgCollectTmr"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfRouteAgeTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                retValFutRip2IfConfRouteAgeTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2IfConfRouteAgeTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2IfConfRouteAgeTmr (UINT4 u4FutRip2IfConfAddress,
                               INT4 *pu4RetValFutRip2IfConfRouteAgeTmr)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfConfRouteAgeTmr \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4FutRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFutRip2IfConfRouteAgeTmr =
        (UINT4) (pRipIfRec->RipIfaceCfg.u2RouteAgeInterval);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfConfRouteAgeTmr"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfSplitHorizonStatus
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                retValFutRip2IfSplitHorizonStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2IfSplitHorizonStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2IfSplitHorizonStatus (UINT4 u4FutRip2IfConfAddress,
                                  INT4 *pi4RetValFutRip2IfSplitHorizonStatus)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (ENTRY, "Entering fn, rip_get_rip2IfSplitHorizonStatus \n"); ***/

    pRipIfRec = RipGetIfRecFromAddr (u4FutRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutRip2IfSplitHorizonStatus =
        (INT4) (pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus);

    /***  $$TRACE_LOG (EXIT, "Exiting fn, rip_get_rip2IfSplitHorizonStatus"
            "with SNMP_SUCCESS status \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfDefRtInstall
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object
                retValFsRip2IfConfDefRtInstall
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2IfConfDefRtInstall (UINT4 u4FsRip2IfConfAddress,
                                INT4 *pi4RetValFsRip2IfConfDefRtInstall)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRip2IfConfDefRtInstall =
        (INT4) (pRipIfRec->RipIfaceCfg.u1DefRtInstallStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfSpacingTmr
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                retValFsRip2IfConfSpacingTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2IfConfSpacingTmr (UINT4 u4FsRip2IfConfAddress,
                              INT4 *pi4RetValFsRip2IfConfSpacingTmr)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRip2IfConfSpacingTmr =
        (UINT4) (pRipIfRec->RipIfaceCfg.u2SpacingTmrVal);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfAuthType
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                retValFsRip2IfConfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2IfConfAuthType (UINT4 u4FsRip2IfConfAddress,
                            INT4 *pi4RetValFsRip2IfConfAuthType)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Crypto Authentication which includes MD5, SHA1, SHA 256, SHA 384,
     * SHA 512 are displayed in this nmhGetFsRip2IfConfAuthType object 
     * So the Auth type obtained is incremented by 2 to display Crypto type*/
    *pi4RetValFsRip2IfConfAuthType =
        (INT4) ((pRipIfRec->RipIfaceCfg.u2AuthType) - 2);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfInUseKey
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                retValFsRip2IfConfInUseKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2IfConfInUseKey (UINT4 u4FsRip2IfConfAddress,
                            INT4 *pi4RetValFsRip2IfConfInUseKey)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRip2IfConfInUseKey =
        (INT4) (pRipIfRec->RipIfaceCfg.u1KeyIdInUse);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2IfConfAuthLastKeyStatus
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                retValFsRip2IfConfAuthLastKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2IfConfAuthLastKeyStatus (UINT4 u4FsRip2IfConfAddress,
                                     INT4
                                     *pi4RetValFsRip2IfConfAuthLastKeyStatus)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRip2IfConfAuthLastKeyStatus =
        (INT4) (pRipIfRec->RipIfaceCfg.b1LastKeyStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRip2IfAdminStat
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                setValFutRip2IfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2IfAdminStat ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2IfAdminStat (UINT4 u4FutRip2IfConfAddress,
                         INT4 i4SetValFutRip2IfAdminStat)
{
    /*
     * Set operation for non-index, non-rowStatus & non-critical variable.
     */
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FutRip2IfConfAddress,
                              (VOID *) &i4SetValFutRip2IfAdminStat,
                              RIPIF_ADMIN_FLAG) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set admin flag ," 
                 "Could be due to the fact that at IP level, the interface "
                 "would not have been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for admin flag, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfAdminStat;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRip2IfAdminStat) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FutRip2IfConfAddress, i4SetValFutRip2IfAdminStat));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfConfUpdateTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                setValFutRip2IfConfUpdateTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2IfConfUpdateTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2IfConfUpdateTmr (UINT4 u4FutRip2IfConfAddress,
                             INT4 i4SetValFutRip2IfConfUpdateTmr)
{
    /*
     * Set operation for non-index & non-rowStatus variable.
     */
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FutRip2IfConfAddress,
                              (VOID *) &i4SetValFutRip2IfConfUpdateTmr,
                              RIPIF_UPD_TMR) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set update timer, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for update timer, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfConfUpdateTmr;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRip2IfConfUpdateTmr) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FutRip2IfConfAddress, i4SetValFutRip2IfConfUpdateTmr));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfConfGarbgCollectTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                setValFutRip2IfConfGarbgCollectTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2IfConfGarbgCollectTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2IfConfGarbgCollectTmr (UINT4 u4FutRip2IfConfAddress,
                                   INT4 i4SetValFutRip2IfConfGarbgCollectTmr)
{
    /*
     * Set operation for non-index, non-rowStatus, non-critical variable.
     */
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FutRip2IfConfAddress,
                              (VOID *) &i4SetValFutRip2IfConfGarbgCollectTmr,
                              RIPIF_GARB_TMR) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set garbage collect timer, Could"
                 "be due to the fact that at IP level, the interface would not"
                 "have been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for Garbage Colct timer, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfConfGarbgCollectTmr;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2IfConfGarbgCollectTmr) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FutRip2IfConfAddress,
                      i4SetValFutRip2IfConfGarbgCollectTmr));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfConfRouteAgeTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                setValFutRip2IfConfRouteAgeTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2IfConfRouteAgeTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2IfConfRouteAgeTmr (UINT4 u4FutRip2IfConfAddress,
                               INT4 i4SetValFutRip2IfConfRouteAgeTmr)
{
    /*
     * Set operation for non-index, non-rowStatus & non-critical variable.
     */
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FutRip2IfConfAddress,
                              (VOID *) &i4SetValFutRip2IfConfRouteAgeTmr,
                              RIPIF_AGE_TMR) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set age timer, Could be due"
                 "to the fact that at IP level, the interface would not have"
                 "been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for age timer, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfConfRouteAgeTmr;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2IfConfRouteAgeTmr) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FutRip2IfConfAddress,
                      i4SetValFutRip2IfConfRouteAgeTmr));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfSplitHorizonStatus
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                setValFutRip2IfSplitHorizonStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2IfSplitHorizonStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2IfSplitHorizonStatus (UINT4 u4FutRip2IfConfAddress,
                                  INT4 i4SetValFutRip2IfSplitHorizonStatus)
{
    /*
     * Set operation for non-index, non-rowStatus & non-critical variable.
     */
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FutRip2IfConfAddress,
                              (VOID *) &i4SetValFutRip2IfSplitHorizonStatus,
                              RIPIF_SPLIT_HORIZON) == RIP_FAILURE)
    {
         /***  $$TRACE_LOG (EXIT, "Failure to set Split horizon status ," 
                 "Could be due to the fact that at IP level, the interface "
                 "would not have been configured \n");  ***/

        return SNMP_FAILURE;
    }

     /***  $$TRACE_LOG (EXIT, "Set for split horizon status, Success \n");  ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfSplitHorizonStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2IfSplitHorizonStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FutRip2IfConfAddress,
                      i4SetValFutRip2IfSplitHorizonStatus));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfConfDefRtInstall
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object
                setValFsRip2IfConfDefRtInstall
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip2IfConfDefRtInstall (UINT4 u4FsRip2IfConfAddress,
                                INT4 i4SetValFsRip2IfConfDefRtInstall)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FsRip2IfConfAddress,
                              (VOID *) &i4SetValFsRip2IfConfDefRtInstall,
                              RIPIF_DEF_RT_INSTALL) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfConfDefRtInstall;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2IfConfDefRtInstall) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FsRip2IfConfAddress, i4SetValFsRip2IfConfDefRtInstall));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfConfSpacingTmr
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                setValFsRip2IfConfSpacingTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip2IfConfSpacingTmr (UINT4 u4FsRip2IfConfAddress,
                              INT4 i4SetValFsRip2IfConfSpacingTmr)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (rip_set_if_parameter (u4FsRip2IfConfAddress,
                              (VOID *) &i4SetValFsRip2IfConfSpacingTmr,
                              RIPIF_SPACE_TMR_VAL) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2IfConfSpacingTmr;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2IfConfSpacingTmr) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FsRip2IfConfAddress, i4SetValFsRip2IfConfSpacingTmr));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2IfConfAuthType
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                setValFsRip2IfConfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip2IfConfAuthType (UINT4 u4FsRip2IfConfAddress,
                            INT4 i4SetValFsRip2IfConfAuthType)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Authentication type for MD5,SHA1,SHA256,SHA384,SHA512
     * can only be configured via this object, so the value is 
     * incremented and stored to retain the other Auth types 
     * configured via Rip2IfConfAuthType */
    i4SetValFsRip2IfConfAuthType = i4SetValFsRip2IfConfAuthType + 2;

    if (rip_set_if_parameter (u4FsRip2IfConfAddress,
                              (VOID *) &i4SetValFsRip2IfConfAuthType,
                              RIPIF_AUTHTYPE) == RIP_FAILURE)
    {
        /***  $$TRACE_LOG (EXIT, "Failure to set Auth Type, Could be due"
         * "to the fact that at IP level, the interface would not have"
         * "been configured \n");  ***/
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfAuthType;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIStdRip2IfConfAuthType) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FsRip2IfConfAddress, i4SetValFsRip2IfConfAuthType));
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfAdminStat
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                testValFutRip2IfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2IfAdminStat ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2IfAdminStat (UINT4 *pu4ErrorCode, UINT4 u4FutRip2IfConfAddress,
                            INT4 i4TestValFutRip2IfAdminStat)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (RIP_IS_ADDRESS_INVALID (u4FutRip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Admin flag, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /*** $TRACE_LOG (ENTRY, "Entering fn, rip_validate_rip2AdminStat \n"); ***/

    switch (i4TestValFutRip2IfAdminStat)
    {

        case RIP_ADMIN_ENABLE:
        case RIP_ADMIN_DISABLE:
        case RIP_ADMIN_PASSIVE:

        /***  $TRACE_LOG (EXIT, "Exiting fn, rip_validate_rip2AdminStat \n"
                  "Validation Success \n");     ***/
            return SNMP_SUCCESS;

        default:

        /***  $TRACE_LOG (EXIT, "Admin Value Failure \n");    ***/
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }                            /* End of switch ( i4TestValRip2AdminStat ) */
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfConfUpdateTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                testValFutRip2IfConfUpdateTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2IfConfUpdateTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2IfConfUpdateTmr (UINT4 *pu4ErrorCode,
                                UINT4 u4FutRip2IfConfAddress,
                                INT4 i4TestValFutRip2IfConfUpdateTmr)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Update Timer. \n"); ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4FutRip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Upd. Timer, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check for the update timer value */

    if ((i4TestValFutRip2IfConfUpdateTmr < RIP_MIN_UPDATE_INTERVAL) ||
        (i4TestValFutRip2IfConfUpdateTmr > RIP_MAX_UPDATE_INTERVAL))
    {

        /*** $$TRACE_LOG (EXIT, "Validation for Update Timer, Failure\n"); ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /***  $$TRACE_LOG (EXIT, "Validation for Update Timer, Success \n");  ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfConfGarbgCollectTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                testValFutRip2IfConfGarbgCollectTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2IfConfGarbgCollectTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2IfConfGarbgCollectTmr (UINT4 *pu4ErrorCode,
                                      UINT4 u4FutRip2IfConfAddress,
                                      INT4
                                      i4TestValFutRip2IfConfGarbgCollectTmr)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, non-rowStatus,  non-critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Garb Colct Tmr. \n"); ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4FutRip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Garb. Colct timer, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check for the garbage collect timer */

    if ((i4TestValFutRip2IfConfGarbgCollectTmr < RIP_MIN_GARBAGE_COLCT_INTERVAL)
        || (i4TestValFutRip2IfConfGarbgCollectTmr >
            RIP_MAX_GARBAGE_COLCT_INTERVAL))
    {

        /*** $$TRACE_LOG (EXIT, "Validation for Garb Colct Tmr, Failed\n"); ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*** $$TRACE_LOG (EXIT, "Validation for Garb Colect Tmr, Success \n"); ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfConfRouteAgeTmr
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                testValFutRip2IfConfRouteAgeTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2IfConfRouteAgeTmr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2IfConfRouteAgeTmr (UINT4 *pu4ErrorCode,
                                  UINT4 u4FutRip2IfConfAddress,
                                  INT4 i4TestValFutRip2IfConfRouteAgeTmr)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /*
     * Validate operation for non-index, non-rowStatus, non-critical variable.
     */

    /*** $$TRACE_LOG (ENTRY, "Entering Validation for Rt Age Tmr. \n"); ***/

    /* Validation for the net address */

    if (RIP_IS_ADDRESS_INVALID (u4FutRip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for Rt. Age timer, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check for the route age timer */

    if ((i4TestValFutRip2IfConfRouteAgeTmr < RIP_MIN_ROUTE_AGE) ||
        (i4TestValFutRip2IfConfRouteAgeTmr > RIP_MAX_ROUTE_AGE))
    {

        /*** $$TRACE_LOG (EXIT, "Validation for Rt Age Tmr, Failure\n"); ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfSplitHorizonStatus
 Input       :  The Indices
                FutRip2IfConfAddress

                The Object 
                testValFutRip2IfSplitHorizonStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2IfSplitHorizonStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2IfSplitHorizonStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FutRip2IfConfAddress,
                                     INT4 i4TestValFutRip2IfSplitHorizonStatus)
{
    INT4                i4RetIf;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT1                i1IsDC = RIP_SUCCESS;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (RIP_IS_ADDRESS_INVALID (u4FutRip2IfConfAddress))
    {

        /***  $$TRACE_LOG (EXIT, "Validation for SplitHorizonStatus, Failure \n"
                "Address is invalid \n");  ***/
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4RetIf =
         RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                             u4FutRip2IfConfAddress)) ==
        RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec ((UINT4) i4RetIf, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return SNMP_FAILURE;
    }

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
    /*** $TRACE_LOG (ENTRY, "Entering fn, rip_validate_rip2SplitHorizonStatus"
          "\n"); ***/

    switch (i4TestValFutRip2IfSplitHorizonStatus)
    {

        case RIP_SPLIT_HORIZON:
        case RIP_NO_SPLIT_HORIZON:
            if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                && (i1IsDC != RIP_SUCCESS))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            /* fall through */
        case RIP_SPLIT_HORZ_WITH_POIS_REV:

        /***  $TRACE_LOG (EXIT, "Exiting fn, rip_validate_rip2SplitHorizonStatus
                               \n" "Validation Success \n");  ***/

            return SNMP_SUCCESS;

        default:

            /***  $TRACE_LOG (EXIT, "SplitHorizon value Failure \n");  ***/
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }                            /* End of switch ( i4TestValFutRip2IfSplitHorizonStatus ) */
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfConfDefRtInstall
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object
                testValFsRip2IfConfDefRtInstall
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip2IfConfDefRtInstall (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsRip2IfConfAddress,
                                   INT4 i4TestValFsRip2IfConfDefRtInstall)
{
    INT4                i4RetIf;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (RIP_IS_ADDRESS_INVALID (u4FsRip2IfConfAddress))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4RetIf =
         RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                             u4FsRip2IfConfAddress)) ==
        RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec ((UINT4) i4RetIf, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return SNMP_FAILURE;
    }

    if (i4TestValFsRip2IfConfDefRtInstall != RIP_INSTALL_DEF_RT
        && i4TestValFsRip2IfConfDefRtInstall != RIP_DONT_INSTALL_DEF_RT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfConfSpacingTmr
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                testValFsRip2IfConfSpacingTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip2IfConfSpacingTmr (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsRip2IfConfAddress,
                                 INT4 i4TestValFsRip2IfConfSpacingTmr)
{
    INT4                i4RetIf;
    INT4                i4SpacingStatus;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (RIP_IS_ADDRESS_INVALID (u4FsRip2IfConfAddress))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4RetIf =
         RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                             u4FsRip2IfConfAddress)) ==
        RIP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec ((UINT4) i4RetIf, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        /*Interface Record Not Found */
        return SNMP_FAILURE;
    }
    if (nmhGetFsRip2SpacingEnable (&i4SpacingStatus) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4SpacingStatus == RIP_SPACING_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRip2IfConfSpacingTmr < RIP_MIN_SPACE_INTERVAL) ||
        (i4TestValFsRip2IfConfSpacingTmr > RIP_MAX_SPACE_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2IfConfAuthType
 Input       :  The Indices
                FsRip2IfConfAddress

                The Object 
                testValFsRip2IfConfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip2IfConfAuthType (UINT4 *pu4ErrorCode,
                               UINT4 u4FsRip2IfConfAddress,
                               INT4 i4TestValFsRip2IfConfAuthType)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tRipIfaceRec       *pRipIfRec = NULL;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Validation for the net address */
    if (RIP_IS_ADDRESS_INVALID (u4FsRip2IfConfAddress))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRip2IfConfAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Validation for the Authentication type(crypto) */
    if ((i4TestValFsRip2IfConfAuthType < RIP_CRYPTO_MIN_AUTH_TYPE) ||
        (i4TestValFsRip2IfConfAuthType > RIP_CRYPTO_MAX_AUTH_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip2IfConfTable
 Input       :  The Indices
                FsRip2IfConfAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2IfConfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT4
RipConfigInterface (UINT2 u2Port, tRipCxt * pRipCxtEntry)
{
    UINT4               u4SrcAddress;
    UINT4               u4RipIfAddr;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT1                i1IsDC = RIP_SUCCESS;
#ifdef IP_WANTED
    tIfConfigRecord     IpInfo;
    MEMSET (&IpInfo, 0, sizeof (tIfConfigRecord));
#endif

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2Port, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return RIP_FAILURE;
    }

    u4SrcAddress = RIPIF_GLBTAB_ADDR_OF (u2Port);
    u4RipIfAddr = pRipIfRec->u4Addr;
    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    if ((u4RipIfAddr) && (u4SrcAddress != u4RipIfAddr))

    {
        /*RIP conf row status is explicitly deleted here,when there is a change 
         *in the interface IP address and the row status is created with the 
         * new ip address */
        rip_action_for_iface_row_status (u2Port, u4SrcAddress,
                                         (UINT2) RIPIF_ADMIN_DESTROY,
                                         pRipCxtEntry);
    }
    if (rip_validate_row_status (u4SrcAddress,
                                 RIPIF_ADMIN_CREATE_AND_WAIT,
                                 pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "FAILED while validating row status");
        return RIP_FAILURE;
    }
    nmhSetRip2IfConfStatus (u4SrcAddress, RIPIF_ADMIN_CREATE_AND_WAIT);

    if (RIP_IS_ADDRESS_INVALID (u4SrcAddress))
    {
        return RIP_FAILURE;
    }
    nmhSetRip2IfConfSrcAddress (u4SrcAddress, u4SrcAddress);
    nmhSetRip2IfConfDefaultMetric (u4SrcAddress, RIP_DEFAULT_METRIC);

#ifdef IP_WANTED
    RIPIF_GET_IFCONFIG_RECORD (u2Port, IpInfo);
    pRipIfRec->u1Persistence = IpInfo.u1Persistence;
    pRipIfRec->u4PeerAddress = IpInfo.u4PeerAddress;
#endif

    if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE) && (i1IsDC != RIP_SUCCESS))
    {
        pRipIfRec->RipIfaceCfg.u2RipSendStatus = RIPIF_V2_DEMAND;
    }

    return RIP_SUCCESS;
}

/****************************************************************************
Function    : RipSendInitialRequest
Input        : u4RipIfAddr, i4RipIfSendType
Output        : This routine sends an initial RIP_REQUEST packet out of the 
              RIP enabled interface.
Returns     : RIP_SUCCESS or RIP_FAILURE.
****************************************************************************/

INT4
RipSendInitialRequest (UINT2 u2IfIndex, UINT4 u4SrcAddr, tRipCxt * pRipCxtEntry)
{
    tRip                RipHdr;
    tRoute              RouteEntry;
    UINT4               u4DestAddr;
    INT4                i4Status;
    UINT2               u2Len;
    UINT1               u1Version, u1McastFlag;
    tRipIfaceRec       *pRipIfRec = NULL;

    MEMSET (&RipHdr, 0, sizeof (tRip));
    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return RIP_FAILURE;
    }

    RipHdr.u1Command = RIP_REQUEST;
    if ((i4Status = rip_get_send_status (u2IfIndex,
                                         &u1Version,
                                         &u1McastFlag,
                                         pRipCxtEntry)) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "FAILURE in getting send status of an interface");
        (pRipIfRec->RipIfaceStats.u4RipGetSendStatusFailCount)++;
        return RIP_FAILURE;
    }
    if (RIP_MCAST_ON == u1McastFlag)
    {
        u4DestAddr = RIP_MULTICAST_ADDRESS;
    }
    else
    {
        u4DestAddr = RipIfGetBaddrFromPort (u2IfIndex);
    }
    if (0 == u4DestAddr)
    {
        u4DestAddr = RIP_BROADCAST_ADDRESS;
    }
    RipHdr.u1Version = u1Version;
    RipHdr.u2Reserved = 0;
    RouteEntry.u2Family = 0;
    RouteEntry.au1RouteTag[0] = 0;
    RouteEntry.au1RouteTag[1] = 0;
    RouteEntry.u4DestNet = 0;
    RouteEntry.u4SubnetMask = 0;
    RouteEntry.u4NextHop = 0;
    RouteEntry.u4Metric = RIP_HTONL ((UINT4) RIP_INFINITY);

    u2Len = (UINT2) (sizeof (tRip) + sizeof (tRoute));

    MEMSET (&gRipPacket, 0, sizeof (tRipPkt));
    MEMCPY (&gRipPacket, (UINT1 *) &RipHdr, sizeof (tRip));
    MEMCPY ((VOID *) (&(gRipPacket.aRipInfo[0].RipMesg.Route)),
            (VOID *) &RouteEntry, sizeof (tRoute));

    if (rip_task_udp_send ((UINT1 *) &gRipPacket, UDP_RIP_PORT, u4DestAddr,
                           UDP_RIP_PORT, u2Len, RIP_INITIAL_TTL_VALUE,
                           u2IfIndex, RIP_INITIAL_BROADCAST_VALUE, u4SrcAddr,
                           pRipCxtEntry) != RIP_SUCCESS)
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipSetIfConfStatus                                 */
/*   Description     : This function sends sync message to standby to     */
/*                     clear the interface record                         */
/*   Input(s)        : u4Rip2IfConfAddress - network address              */
/*                     i4SetValRip2IfConfStatus - Row status              */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT1
RipSetIfConfStatus (UINT4 u4Rip2IfConfAddress, INT4 i4SetValRip2IfConfStatus)
{
    INT4                i4RetVal = SNMP_SUCCESS;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIStdRip2IfConfStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIStdRip2IfConfStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4Rip2IfConfAddress, i4SetValRip2IfConfStatus));
#endif
    UNUSED_PARAM (u4Rip2IfConfAddress);
    UNUSED_PARAM (i4SetValRip2IfConfStatus);
    UNUSED_PARAM (i4RetVal);
    return RIP_SUCCESS;

}
