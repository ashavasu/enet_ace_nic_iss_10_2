/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripmain.c,v 1.41 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains the routines for initiali-
 *             -zing the RIP data structures, Timer handler  
 *             routines, shutdown routines              
 *
 *******************************************************************/
#include "ripinc.h"

/* Makefile Changes - no previous prototype for function */

    /*|**********************************************************|**
     *** <<<<     Global Variable Declarations Start Here >>>> ***
     **|**********************************************************|* */
tRipMemory          IpRipMemory;
tDbTblDescriptor    gRipDynInfoList;
tDbDataDescInfo     gaRipDynDataDescList[RIP_MAX_DYN_INFO_TYPE];
tRipRedGblInfo      gRipRedGblInfo;
tRipRtMarker        gRipRedRtMarker;
tOsixQId            gRipRmPktQId;
UINT4               gu4RipSysLogId;

    /***************************************************************
    >>>>>>>>> PUBLIC Routines of this Module Start Here <<<<<<<<<<
    ***************************************************************/

        /****************************************************
        * The timer handler routines are all present here    *
        ****************************************************/

/*******************************************************************************

    Function        :   rip_timer_expiry_handler().

    Description        :   This function handles the expiry of timers. Both the
                update timers and the route timers are handled. It
                traverses through the timer list and processes the
                expired timers. For each timer, it checks for
                whether the timer corresponds to update timer or
                route timer. If the timer expired, corresponds to
                route timer, it calls a routine to handle the same.
                It this relates to update timer expiry, then,
                generates a regular update and then resets the
                update timer.

    Input parameters    :   None.

    Output parameters    :   None.

    Global variables
    Affected        :   RipTimerList, the global timer list is affected.

    Return value    :   RIP_DATABASE_CHANGED | RIP_DATABASE_NOT_CHANGED.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_timer_expiry_handler
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

EXPORT INT4
rip_timer_expiry_handler ()
{
    UINT1               u1Id;
    INT4                i4Status = RIP_SUCCESS;

    tTrigRipPkt         RipPkt;

    tTMO_APP_TIMER     *pHeadTimer = NULL;
    tTMO_APP_TIMER     *pCurTimer = NULL;

    /*
     * Let us get the expired list of timers from our main timer list.
     */

    RIP_GET_EXPIRED_TIMERS (RIP_TIMER_ID, &pHeadTimer);

    while (pHeadTimer != NULL)
    {

        /*
         * Go through individual timers and process it.
         */

        pCurTimer = pHeadTimer;

        pHeadTimer = RIP_NEXT_TIMER (RIP_TIMER_ID);

        u1Id = ((t_RIP_TIMER *) pCurTimer)->u1Id;

        MEMSET (&RipPkt, 0, sizeof (tTrigRipPkt));

        switch (u1Id)
        {

            case RIP_RT_TIMER_ID:
            {
                tRipRtEntry        *pRtEntry = NULL;
                tRipCxt            *pRipCxtEntry = NULL;

                pRtEntry = (tRipRtEntry *) ((tRipRtTimer *) pCurTimer)->pRt;

                pRipCxtEntry = RipGetCxtInfoRec ((INT4) pRtEntry->u4RipCxtId);
                if (pRipCxtEntry == NULL)
                {
                    break;
                }
                i4Status = rip_rt_timeout_handler (((tRipRtTimer *)
                                                    pCurTimer)->pRt,
                                                   pRipCxtEntry);

                if (i4Status == RIP_TRIGGER_UPDATE)
                {

                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC, RIP_NAME,
                             "Triggered update is on, "
                             "some of routes have been changed   \n");
                    RipSendTriggeredUpdate (pRipCxtEntry);
                }

                break;
            }
            case RIP_UPDATE_TIMER_ID:
            {
                UINT2               u2If =
                    ((tRipUpdateTimer *) pCurTimer)->u2If;
                tRipIfaceRec       *pRipIfRec = NULL;
                tRipCxt            *pRipCxtEntry = NULL;
                UINT4               u4Interval;

                pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2If);
                if (pRipCxtEntry == NULL)
                {
                    break;
                }

                pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);
                if (pRipIfRec == NULL)
                {
                    break;
                }
                if (pRipIfRec->i2ResetCount < RIP_MAX_RESET_COUNT + 1)
                {
                    pRipIfRec->i2ResetCount++;
                }
                /*
                 * Send an update over this interface, when we are globally
                 * active, admin, oper status active in this interface.
                 */
                if (RIP_IS_ACTIVE (pRipIfRec))
                {

                    RIP_TRC_ARG1 (RIP_MOD_TRC,
                                  pRipCxtEntry->u4RipTrcFlag,
                                  pRipCxtEntry->i4CxtId,
                                  CONTROL_PLANE_TRC, RIP_NAME,
                                  "Sending regular Update over this "
                                  "interface %d \n", u2If);

                    /* 2.0.1.1 Fix 1 15-09-99 --START-- */

                    if ((i4Status =
                         rip_generate_update_message (RIP_VERSION_ANY,
                                                      RIP_REG_UPDATE,
                                                      IP_GEN_BCAST_ADDR,
                                                      (UINT2) UDP_RIP_PORT,
                                                      u2If,
                                                      pRipCxtEntry)) ==
                        RIP_FAILURE)
                    {

                        RIP_TRC (RIP_MOD_TRC,
                                 pRipCxtEntry->u4RipTrcFlag,
                                 pRipCxtEntry->i4CxtId,
                                 ALL_FAILURE_TRC, RIP_NAME,
                                 "Error in sending update. "
                                 "Anyway, proceeding to process "
                                 "the remaining timers, if any. \n");

                    }

                    /* 2.0.1.1 Fix 1 15-09-99 --END-- */
                }

                u4Interval =
                    RipGetJitterVal (pRipIfRec->RipIfaceCfg.u2UpdateInterval,
                                     RIP_JITTER);
                RIP_START_TIMER (RIP_TIMER_ID,
                                 &(pRipIfRec->RipUpdateTimer.TimerNode),
                                 u4Interval);

                break;
            }
            case RIP_SPACE_TIMER_ID:
            {
                UINT2               u2If =
                    ((tRipUpdateTimer *) pCurTimer)->u2If;
                tRipCxt            *pRipCxtEntry = NULL;
                tRipIfaceRec       *pRipIfRec = NULL;
                pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2If);

                if (pRipCxtEntry == NULL)
                {
                    break;
                }
                pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);
                if (pRipIfRec == NULL)
                {
                    break;
                }

                TMO_DLL_Delete (&pRipCxtEntry->RipIfSpaceLst,
                                (tTMO_DLL_NODE *) & pRipIfRec->RipIfRecNode);
                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              " Sending spaced Update over this interface %d \n",
                              u2If);

                if ((i4Status =
                     rip_generate_update_message (RIP_VERSION_ANY,
                                                  RIP_SPACE_UPDATE,
                                                  IP_GEN_BCAST_ADDR,
                                                  (UINT2) UDP_RIP_PORT,
                                                  u2If,
                                                  pRipCxtEntry)) == RIP_FAILURE)
                {

                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC,
                             RIP_NAME,
                             "Error in sending update. "
                             "Anyway, proceeding to process remaining timers, "
                             "if any. \n");

                }

                break;
            }
                /* One shot timer is not available. So the triggered timer was added to the
                 * list for FSAP2 conversion.
                 */
            case RIP_TRIG_UPD_TIMER_ID:
            {
                INT2                i2OffSet = 0;
                tRipCxt            *pRipCxtEntry = NULL;
                i2OffSet = (INT2) RIP_OFFSET (tRipCxt, RipTrigUpdTimer);

                pRipCxtEntry = (tRipCxt *) (VOID *) ((UINT1 *)
                                                     pCurTimer - i2OffSet);
                if (pRipCxtEntry == NULL)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             RIP_TRC_GLOBAL_FLAG,
                             RIP_INVALID_CXT_ID,
                             ALL_FAILURE_TRC, RIP_NAME,
                             "ERROR in filling pRipCxtEntry");
                    return RIP_FAILURE;
                }
                rip_triggered_timer_expiry_handler (pRipCxtEntry);
                break;
            }
            case RIP_PASS_UPD_TIMER_ID:
            {
                INT2                i2OffSet = 0;
                tRipCxt            *pRipCxtEntry = NULL;
                i2OffSet = (INT2) RIP_OFFSET (tRipCxt, RipPassiveUpdTimer);

                pRipCxtEntry = (tRipCxt *) (VOID *) ((UINT1 *)
                                                     pCurTimer - i2OffSet);

                if ((i4Status = rip_generate_update_message (RIP_VERSION_ANY,
                                                             RIP_UNICAST_UPDATE,
                                                             IP_GEN_BCAST_ADDR,
                                                             (UINT2)
                                                             UDP_RIP_PORT,
                                                             RIPIF_INVALID_INDEX,
                                                             pRipCxtEntry)) ==
                    RIP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC,
                             RIP_NAME,
                             "Error in sending update on Passive timer expiry ");
                }

                RIP_START_TIMER (RIP_TIMER_ID,
                                 &((pRipCxtEntry->RipPassiveUpdTimer).
                                   Timer_node), RIP_DEF_UPDATE_INTERVAL);
                break;
            }
            case RIP_RES_RETX_TIMER:
            {

                UINT2               u2Len = 0;
                tRipRtNode         *pListNode = NULL;
                tRipIfaceRec       *pRipIfRec = NULL;
                UINT2               u2IfIndex =
                    ((tRipUpdateTimer *) pCurTimer)->u2If;
                tRipCxt            *pRipCxtEntry = NULL;

                pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2IfIndex);
                if (pRipCxtEntry == NULL)
                {
                    break;
                }

                pRipIfRec = RipGetIfRec ((UINT4) u2IfIndex, pRipCxtEntry);
                if (pRipIfRec == NULL)
                {
                    break;
                }

                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Sending Update response with flush flag "
                              "on interface %d \n", u2IfIndex);
                if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_DO_NOT_SEND)
                {
                    return RIP_DATABASE_NOT_CHANGED;
                }
                if (pRipIfRec->u1ReTxCounter >= RIP_CFG_MAX_RETX (pRipCxtEntry))
                {
                    /* max retx failures. stop sending update response */
                    if (pRipIfRec->RipSpacingTimer.u1Data == 0)
                    {
                        /* purge routes learnt via this interface */
                        RipUpdateRoutesThroIf (u2IfIndex, RIP_PURGE_ROUTES,
                                               pRipIfRec->RipIfaceCfg.u4SrcAddr,
                                               pRipCxtEntry);
                        /* start polling timer */
                        RIP_START_TIMER (RIP_TIMER_ID,
                                         &(pRipIfRec->RipSpacingTimer.
                                           TimerNode), RIP_POLL_INVL);
                    }
                    /* when the link comes up next time and the database
                     * changes we will send the unacked routes again.
                     */
                    pListNode = pRipIfRec->pReTxFirstRoute;

                    if (pListNode != NULL)
                    {

                        pListNode =
                            (tRipRtNode *) TMO_DLL_Previous (&pRipCxtEntry->
                                                             RipRtList,
                                                             &(pListNode->
                                                               DllLink));
                        pRipIfRec->pReTxLastRoute = pListNode;
                        pRipIfRec->pReTxFirstRoute = NULL;
                    }
                    break;
                }
                pRipIfRec->u1ReTxCounter =
                    (UINT1) (pRipIfRec->u1ReTxCounter + 1);
                if (pRipIfRec->RipUpdateTimer.u1Data == RIP_FLUSH_FLAG)
                {
                    /* retx-mitting update response with flush flag set */
                    RIP_GENERATE_RESPONSE_HDR (RipPkt, pRipIfRec,
                                               RIP_FLUSH_FLAG);
                    u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr)
                                     + sizeof (tRoute));
                }
                else if (pRipIfRec->pReTxFirstRoute != NULL)
                {
                    /* reconstructing and (re)tx-mitting update response */
                    RIP_GENERATE_RESPONSE_HDR (RipPkt, pRipIfRec, 0);

                    u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));
                    if (RipGenerateUpdateResponse
                        ((tRipInfo *) & (RipPkt.aRipInfo), u2IfIndex,
                         &u2Len, pRipCxtEntry, RIP_REG_UPDATE) != RIP_SUCCESS)
                    {
                        break;
                    }
                }
                if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                    RIPIF_NO_AUTHENTICATION)

                {
                    if (rip_construct_auth_info
                        ((UINT1 *) &RipPkt, &u2Len, u2IfIndex,
                         pRipCxtEntry) != RIP_SUCCESS)
                    {
                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC, RIP_NAME,
                                      "Failed while filling authentication structure other than simple password for an index %d.\n",
                                      (u2IfIndex));
                        if (pRipIfRec != NULL)
                        {
                            (pRipIfRec->RipIfaceStats.
                             u4RipConstructAuthInfoFailCount)++;
                        }
                        return RIP_FAILURE;
                    }
                }
                if ((pRipIfRec->u4PeerAddress != 0) ||
                    (RipIfGetValidPeerIpAddress (pRipIfRec) == RIP_SUCCESS))
                {
                    rip_task_udp_send ((UINT1 *) &RipPkt, UDP_RIP_PORT,
                                       pRipIfRec->u4PeerAddress, UDP_RIP_PORT,
                                       u2Len, RIP_INITIAL_TTL_VALUE, u2IfIndex,
                                       RIP_INITIAL_BROADCAST_VALUE,
                                       0, pRipCxtEntry);
                }
                RIP_START_TIMER (RIP_TIMER_ID,
                                 &(pRipIfRec->RipUpdateTimer.TimerNode),
                                 RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry));

                break;
            }

            case RIP_REQ_RETX_TIMER:
            {
                UINT2               u2IfIndex =
                    ((tRipUpdateTimer *) pCurTimer)->u2If;

                tRoute             *pRouteEntry =
                    &(RipPkt.aRipInfo[0].RipMesg.Route);

                UINT2               u2Len = 0;
                tRipIfaceRec       *pRipIfRec = NULL;
                tRipCxt            *pRipCxtEntry = NULL;

                pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2IfIndex);

                if (pRipCxtEntry == NULL)
                {
                    break;
                }

                pRipIfRec = RipGetIfRec ((UINT4) u2IfIndex, pRipCxtEntry);
                if (pRipIfRec == NULL)
                {
                    break;
                }

                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Sending Update request on interface %d \n",
                              u2IfIndex);
                if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_DO_NOT_SEND)
                {
                    return RIP_DATABASE_NOT_CHANGED;
                }
                if ((pRipIfRec->RipSpacingTimer.u1Data != RIP_POLL_INVL) &&
                    (pRipIfRec->u1ReTxCounter >=
                     RIP_CFG_MAX_RETX (pRipCxtEntry)))

                {
                    /* max retx failures. retx req at a reduced frequency */
                    RIPIF_FILL_REQ_RETX_TIMER (pRipIfRec, RIP_POLL_INVL);
                    /* purge routes learnt via this interface */
                    RipUpdateRoutesThroIf (u2IfIndex, RIP_PURGE_ROUTES,
                                           pRipIfRec->RipIfaceCfg.u4SrcAddr,
                                           pRipCxtEntry);
                }
                RIP_GENERATE_REQUEST_HDR (RipPkt, pRipIfRec);
                u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));
                if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                    (UINT2) RIPIF_NO_AUTHENTICATION)
                {
                    pRouteEntry = &(RipPkt.aRipInfo[1].RipMesg.Route);
                    DEMAND_RIP_GENERATE_REQUEST_HDR (pRouteEntry);
                    u2Len = (UINT2) (u2Len + (UINT2) sizeof (tRoute));
                    if (rip_construct_auth_info
                        ((UINT1 *) &RipPkt, &u2Len,
                         u2IfIndex, pRipCxtEntry) != RIP_SUCCESS)
                    {
                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC, RIP_NAME,
                                      "Failed while filling authentication structure other than simple password for an index %d.\n",
                                      (u2IfIndex));
                        if (pRipIfRec != NULL)
                        {
                            (pRipIfRec->RipIfaceStats.
                             u4RipConstructAuthInfoFailCount)++;
                        }
                        return RIP_FAILURE;
                    }

                }
                else
                {
                    DEMAND_RIP_GENERATE_REQUEST_HDR (pRouteEntry);
                    u2Len = (UINT2) (u2Len + sizeof (tRoute));

                }

                if ((pRipIfRec->u4PeerAddress != 0) ||
                    (RipIfGetValidPeerIpAddress (pRipIfRec) == RIP_SUCCESS))
                {

                    rip_task_udp_send ((UINT1 *) &RipPkt, UDP_RIP_PORT,
                                       pRipIfRec->u4PeerAddress, UDP_RIP_PORT,
                                       u2Len, RIP_INITIAL_TTL_VALUE, u2IfIndex,
                                       RIP_INITIAL_BROADCAST_VALUE, 0,
                                       pRipCxtEntry);
                }

                /* use the same time-out value as in the timer structure */
                RIP_START_TIMER (RIP_TIMER_ID,
                                 &(pRipIfRec->RipSpacingTimer.TimerNode),
                                 pRipIfRec->RipSpacingTimer.u1Data);

                break;
            }
            case RIP_SUBSCRIP_TIMER:
            {
                UINT2               u2IfIndex =
                    ((tRipUpdateTimer *) pCurTimer)->u2If;
                tRipIfaceRec       *pRipIfRec = NULL;
                tRipCxt            *pRipCxtEntry = NULL;

                pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2IfIndex);
                if (pRipCxtEntry == NULL)
                {
                    break;
                }

                pRipIfRec = RipGetIfRec ((UINT4) u2IfIndex, pRipCxtEntry);
                if (pRipIfRec == NULL)
                {
                    break;
                }

                pRipIfRec->RipSubscrTimer.u1Data = 0;

                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Over subscription timer fired "
                              "for interface %d \n", u2IfIndex);

                /* circuit is down, purge all routes learnt thro that iface */
                rip_if_disable (pRipIfRec);
                break;
            }

            case RIP_TRIP_TRIG_TMR:
            {
                UINT2               u2IfIndex;

                UINT2               u2Len;
                tRipIfaceRec       *pRipIfRec = NULL;
                UINT4               u4HashIndex = 0;
                INT2                i2OffSet = 0;
                tRipCxt            *pRipCxtEntry = NULL;
                INT1                i1IsDC = RIP_SUCCESS;

                i2OffSet = (INT2) RIP_OFFSET (tRipCxt, TripTrigTmr);

                pRipCxtEntry = (tRipCxt *) (VOID *) ((UINT1 *)
                                                     pCurTimer - i2OffSet);

                (pRipCxtEntry->TripTrigTmr).u1Data = RIP_TRIG_TIMER_INACTIVE;

                TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
                {
                    TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl,
                                          u4HashIndex, pRipIfRec,
                                          tRipIfaceRec *)
                    {
                        if (pRipCxtEntry != pRipIfRec->pRipCxt)
                        {
                            continue;
                        }
                        i1IsDC =
                            RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.
                                                u2RipSendStatus);
                        /*
                         * update is sent only when 
                         * RIP is running on that interface.
                         * It is a WAN interface.
                         * RIP is not passive on that interface.
                         * Atleast one route is there in the database.
                         * First retx route of the interface should be NULL
                         * And ipForwarding is enabled. RFC 1812
                         */
                        u2IfIndex =
                            (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);
                        if ((pRipIfRec->RipIfaceCfg.u2AdminStatus ==
                             RIPIF_ADMIN_ACTIVE)
                            && ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                                && (i1IsDC != RIP_SUCCESS))
                            && (pRipIfRec->RipIfaceCfg.u2RipSendStatus !=
                                RIPIF_DO_NOT_SEND)
                            && (TMO_DLL_First (&(pRipCxtEntry->RipRtList)) !=
                                NULL) && (pRipIfRec->pReTxFirstRoute == NULL))

                        {
                            /*
                             * Construct the update response and 
                             * check if authentication is required and send out the packet
                             */
                            pRipIfRec->RipIfaceStats.u4SentTrigUpdates += 1;
                            RIP_GENERATE_RESPONSE_HDR (RipPkt, pRipIfRec, 0);

                            u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));
                            if (RipGenerateUpdateResponse
                                ((tRipInfo *) & (RipPkt.aRipInfo), u2IfIndex,
                                 &u2Len, pRipCxtEntry,
                                 RIP_TRIG_UPDATE) != RIP_SUCCESS)
                            {
                                RIP_TRC_ARG1 (RIP_MOD_TRC,
                                              pRipCxtEntry->u4RipTrcFlag,
                                              pRipCxtEntry->i4CxtId,
                                              CONTROL_PLANE_TRC, RIP_NAME,
                                              "RipGenerateUpdateResponse() for "
                                              "interface %d - returned FAILURE\n",
                                              u2IfIndex);
                                return RIP_FAILURE;
                            }
                            if (pRipIfRec->RipIfaceCfg.u2AuthType !=
                                RIPIF_NO_AUTHENTICATION)

                            {
                                if (rip_construct_auth_info
                                    ((UINT1 *) &RipPkt, &u2Len,
                                     u2IfIndex, pRipCxtEntry) != RIP_SUCCESS)
                                {
                                    pRipIfRec->RipIfaceStats.
                                        u4SentTrigUpdates -= 1;
                                    RIP_TRC_ARG1 (RIP_MOD_TRC,
                                                  pRipCxtEntry->u4RipTrcFlag,
                                                  pRipCxtEntry->i4CxtId,
                                                  ALL_FAILURE_TRC, RIP_NAME,
                                                  "Failed while filling authentication structure other than simple password for an index %d.\n",
                                                  (u2IfIndex));
                                    if (pRipIfRec != NULL)
                                    {
                                        (pRipIfRec->RipIfaceStats.
                                         u4RipConstructAuthInfoFailCount)++;
                                    }

                                    return RIP_FAILURE;
                                }
                            }
                            if ((pRipIfRec->u4PeerAddress != 0) ||
                                (RipIfGetValidPeerIpAddress (pRipIfRec) ==
                                 RIP_SUCCESS))
                            {

                                rip_task_udp_send ((UINT1 *) &RipPkt,
                                                   UDP_RIP_PORT,
                                                   pRipIfRec->u4PeerAddress,
                                                   UDP_RIP_PORT, u2Len,
                                                   RIP_INITIAL_TTL_VALUE,
                                                   u2IfIndex,
                                                   RIP_INITIAL_BROADCAST_VALUE,
                                                   0, pRipCxtEntry);
                            }
                            /* start the retransmission timer for that interface */
                            RIPIF_FILL_RES_RETX_TIMER (pRipIfRec, 0);
                            RIP_START_TIMER (RIP_TIMER_ID,
                                             &(pRipIfRec->RipUpdateTimer.
                                               TimerNode),
                                             RIP_CFG_GET_RETX_TIMEOUT
                                             (pRipCxtEntry));

                        }
                    }
                }
                break;
            }
            default:
            {
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                                    RIP_NAME, "??? Unknown timer here ??? \n");
                break;
            }

        }                        /* End of switch (u1Id) */

    }                            /* End of while (pHeadTimer != NULL) */
    return RIP_SUCCESS;
}

/*******************************************************************************

    Function        :   rip_triggered_timer_expiry_handler.

    Description        :   This timer is used for limiting the triggered
                updates to a minimum extent so that the network is
                not flooded with a volume of updates.
                This function handles the expiry condition as
                mentioned in rfc1812.
                It resets both the triggered update related flags
                and then sends an update all the routing table by
                calling a procedure.

    Input parameters    :   None.

    Output parameters    :   None.

    Global variables
    Affected        :   RipGblCfg.u1TrigDesired, RipGblCfg.u1TrigUpdFlag.

    Return value    :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_triggered_timer_expiry_handler
    $$TRACE_PROCEDURE_LEVEL = MAIN
*******************************************************************************/

EXPORT VOID
rip_triggered_timer_expiry_handler (tRipCxt * pRipCxtEntry)
{

    pRipCxtEntry->RipGblCfg.u1TrigUpdFlag = RIP_TRIG_TIMER_INACTIVE;    /* Reset the
                                                                           active flag  */
    if (pRipCxtEntry->RipGblCfg.u1TrigDesired == RIP_TRIG_UPD_DESIRED)
    {

        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME,
                 "Triggered timer desired flag is set for "
                 "Sending triggered update once again \n");

        /* 2.0.1.1 Fix 1 15-09-99 --START-- */

        if (rip_generate_update_message (RIP_VERSION_ANY,
                                         RIP_TRIG_UPDATE,
                                         IP_GEN_BCAST_ADDR,
                                         UDP_RIP_PORT,
                                         RIPIF_INVALID_INDEX,
                                         pRipCxtEntry) == RIP_FAILURE)
        {

            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "Failure in the generation of update message, Exiting \n");

        }
        pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_NOT_DESIRED;
        /* Clear off.. */

        /* 2.0.1.1 Fix 1 15-09-99 --END-- */
    }                            /* If rip trig. update is desired */

}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipAddLeafToList                                    |
*|                                                                     |
*| Description   : Adds the leaf to the RIP Route list.                |  
*|                                                                     |  
*| Input         : pointer to the leaf node.                           |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : None.                                               |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
VOID
RipAddLeafToList (tLeafNode * pLeaf, tRipCxt * pRipCxtEntry)
{
    tRipRtNode         *pListNode = NULL;

    RIP_ALLOC_LIST_ENTRY (pListNode);
    if (pListNode != NULL)
    {
        /* Link leaf to list */
        TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pListNode->DllLink);
        pListNode->pLeafNode = pLeaf;
        pLeaf->pListNode = (void *) pListNode;
        TMO_DLL_Add (&(pRipCxtEntry->RipRtList),
                     (tTMO_DLL_NODE *) & pListNode->DllLink);

        /* The check for gTripTrigTmr not equal to zero is required
         * since local routes are added to the database even before
         * the RIP task is spawned.
         */
        if ((pRipCxtEntry->TripTrigTmr.u1Data == RIP_TRIG_TIMER_INACTIVE) &&
            (RIP_TIMER_ID != 0))
        {
            /* database changed start timer so that we dont sent 
             * a update for every route added 
             */
            pRipCxtEntry->TripTrigTmr.u1Data = RIP_TRIG_TIMER_ACTIVE;
            pRipCxtEntry->TripTrigTmr.u1Id = RIP_TRIP_TRIG_TMR;
            RIP_START_TIMER (RIP_TIMER_ID,
                             &((pRipCxtEntry->TripTrigTmr).TimerNode),
                             RIP_TRIP_TRIG_INTVL);
        }
    }
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipMoveLeafToListEnd                                |
*|                                                                     |
*| Description   : When a route content changes the route is unlinked  |  
*|                 from the route list and added at the end of the     |  
*|                 list.                                               |  
*|                                                                     |  
*| Input         : pointer to the route node which is linked in the    |
*|                 dll                                                 |  
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : None.                                               |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
VOID
RipMoveLeafToListEnd (void *pNode, tRipIfaceRec * pRipIfRec,
                      tRipCxt * pRipCxtEntry)
{
    tRipRtNode         *pListNode = (tRipRtNode *) pNode;

    /* check if any of the interfaces has this route node as
     * the first or last retx route, if so we have to alter their 
     * pReTxFirstRoute or pReTxLastRoute.
     */
    if (pRipIfRec != NULL)
    {
        if (pRipIfRec->pReTxFirstRoute == pListNode)
        {
            pRipIfRec->pReTxFirstRoute = (tRipRtNode *)
                TMO_DLL_Next (&(pRipCxtEntry->RipRtList),
                              (tTMO_DLL_NODE *) & pListNode->DllLink);
        }
        else if (pRipIfRec->pReTxLastRoute == pListNode)
        {
            pRipIfRec->pReTxLastRoute = (tRipRtNode *)
                TMO_DLL_Previous (&(pRipCxtEntry->RipRtList),
                                  (tTMO_DLL_NODE *) & pListNode->DllLink);
        }
    }

    TMO_DLL_Delete (&(pRipCxtEntry->RipRtList),
                    (tTMO_DLL_NODE *) & pListNode->DllLink);
    TMO_DLL_Add (&(pRipCxtEntry->RipRtList),
                 (tTMO_DLL_NODE *) & pListNode->DllLink);
    if (pRipCxtEntry->TripTrigTmr.u1Data == RIP_TRIG_TIMER_INACTIVE)
    {
        /* database changed start timer so that we dont sent 
         * a update for every route added 
         */
        pRipCxtEntry->TripTrigTmr.u1Data = RIP_TRIG_TIMER_ACTIVE;
        pRipCxtEntry->TripTrigTmr.u1Id = RIP_TRIP_TRIG_TMR;
        RIP_START_TIMER (RIP_TIMER_ID,
                         &(pRipCxtEntry->TripTrigTmr.TimerNode),
                         RIP_TRIP_TRIG_INTVL);
    }
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipDeleteLeafFromList                               |
*|                                                                     |
*| Description   : When a route gets deleted it is also removed from   |  
*|                 the dll.                                            |  
*|                                                                     |  
*| Input         : pointer to the route node which is linked in the    |
*|                 dll                                                 |  
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : None.                                               |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
VOID
RipDeleteLeafFromList (void *pNode, BOOLEAN bLeafFlag,
                       tRipIfaceRec * pRipIfRec, tRipCxt * pRipCxtEntry)
{
    tRipRtNode         *pListNode = (tRipRtNode *) pNode;

    /* check if any of the interfaces has this route node as
     * the first or last retx route, if so we have to alter their 
     * pReTxFirstRoute or pReTxLastRoute.
     */

    if (pRipIfRec != NULL)
    {
        if (pRipIfRec->pReTxFirstRoute == pListNode)
        {
            pRipIfRec->pReTxFirstRoute = (tRipRtNode *)
                TMO_DLL_Next (&(pRipCxtEntry->RipRtList),
                              (tTMO_DLL_NODE *) & pListNode->DllLink);
        }
        else if (pRipIfRec->pReTxLastRoute == pListNode)
        {
            pRipIfRec->pReTxLastRoute = (tRipRtNode *)
                TMO_DLL_Previous (&(pRipCxtEntry->RipRtList),
                                  (tTMO_DLL_NODE *) & pListNode->DllLink);
        }
    }

    if (bLeafFlag == TRUE)
    {
        pListNode->pLeafNode->pListNode = NULL;
    }
    TMO_DLL_Delete (&(pRipCxtEntry->RipRtList),
                    (tTMO_DLL_NODE *) & pListNode->DllLink);
    RIP_FREE_LIST_ENTRY (pListNode);
}

#ifdef FUTURE_SNMP_WANTED
INT4
RegisterStdRIPwithFutureSNMP (VOID)
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) & stdrip_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) & stdrip_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType *) & stdrip_FMAS_MIBObjectTable,
         stdrip_FMAS_Global_data, (INT4) stdrip_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return FAILURE;
    }
    return SUCCESS;
}

INT4
RegisterFSRIPwithFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *) & fsrip_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *) & fsrip_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *) &
                              fsrip_FMAS_MIBObjectTable,
                              fsrip_FMAS_Global_data,
                              (INT4) fsrip_MAX_OBJECTS) != SNMP_SUCCESS)
        return FAILURE;
    return SUCCESS;
}
#endif
/*****************************************************************************/
/*                                                                           */
/* Function     : RipGetJitterVal                                                */
/*                                                                           */
/* Description  : Determines the jitter value based on the jitter percent    */
/*                and adds it to the value to be jittered.                   */
/*                                                                           */
/* Input        : u4Value            :  value to be jittered                */
/*                u4JitterPcent     :  jitter percent                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Modified value after jittering                             */
/*                                                                           */
/*****************************************************************************/

UINT4
RipGetJitterVal (UINT4 u4Value, UINT4 u4JitterPcent)
{
    UINT4               u4Temp;
    static UINT4        su4Next = 1;

    if (u4Value < RIP_MIN_TIMER_VAL)
    {
        u4JitterPcent =
            RIP_MAX_JITTER - (UINT4) (UtilCeil (u4Value / RIP_DIV_FACTOR));
    }

    su4Next = su4Next * 1103515245 + 12345;
    u4Temp =
        (((100
           - u4JitterPcent) +
          ((su4Next / 65536) % u4JitterPcent)) * (u4Value)) / 100;
    if (u4Temp == 0)
    {
        u4Temp = 1;

    }
    return (u4Temp);
}

/*****************************************************************************/
/* Function     : RipGetBestRoute                                            */
/* Description  : This function will search the best route in network        */
/*                (u4DestAddr & u4DestMask)                                  */
/* Input        : u4DestAddr                                                 */
/*                u4DestMask                                                 */
/*                pRipCxtEntry                                               */
/* Output       : None                                                       */
/* Returns      : tRipRtEntry                                                */
/*****************************************************************************/
tRipRtEntry        *
RipGetBestRoute (UINT4 u4DestAddr, UINT4 u4DestMask, tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome = 0;
    INT4                i4Metric = 0xFFFF;
    INT4                i4count = 0;
    UINT4               au4Indx[2];    /* Index for Trie is madeup of 2 members */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    tRipRtEntry        *pNewBestRt = NULL;
    tRipRtEntry        *pTempRt = NULL;

    au4Indx[0] = RIP_HTONL (u4DestAddr & u4DestMask);
    au4Indx[1] = RIP_HTONL (u4DestMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
        {
            if ((apAppSpecInfo[i4count] != NULL)
                && (i4count != (OTHERS_ID - 1)) &&
                (i4count != MAX_ROUTING_PROTOCOLS))
            {
                pTempRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
                if ((i4count == (CIDR_LOCAL_ID - 1)) ||
                    (i4count == (CIDR_STATIC_ID - 1)))
                {
                    return pTempRt;
                }
                /* Lowest metric is highest priority */
                if (i4Metric > pTempRt->RtInfo.i4Metric1)
                {
                    i4Metric = pTempRt->RtInfo.i4Metric1;
                    pNewBestRt = pTempRt;
                }
            }
        }
    }
    return pNewBestRt;
}

/*****************************************************************************/
/* Function     : RipGetBestRouteForUpdateToRtm                              */
/* Description  : This function will search the best route in rip learned    */
/*                routes from neighbor for update to RTM.                    */
/* Input        : u4DestAddr                                                 */
/*                u4DestMask                                                 */
/*                pRipCxtEntry                                               */
/* Output       : None                                                       */
/* Returns      : tRipRtEntry                                                */
/*****************************************************************************/
tRipRtEntry        *
RipGetBestRouteForUpdateToRtm (UINT4 u4DestAddr, UINT4 u4DestMask,
                               tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome = 0;
    INT4                i4Metric = 0xFFFF;
    UINT4               au4Indx[2];    /* Index for Trie is madeup of 2 members */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    tRipRtEntry        *pNewBestRt = NULL;
    tRipRtEntry        *pTempRt = NULL;

    au4Indx[0] = RIP_HTONL (u4DestAddr & u4DestMask);
    au4Indx[1] = RIP_HTONL (u4DestMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pTempRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[RIP_ID - 1];
        while (pTempRt != NULL)
        {
            /* Lowest metric is highest priority */
            if (i4Metric > pTempRt->RtInfo.i4Metric1)
            {
                i4Metric = pTempRt->RtInfo.i4Metric1;
                pNewBestRt = pTempRt;
            }
            pTempRt = pTempRt->pNextAlternatepath;
        }                        /* while */
    }
    return pNewBestRt;
}

/*****************************************************************************/
/* Function     : RipAddNewBestRoutesToRtm                                      */
/* Description  : This function will search the next best route and          */
/*                update to RTM                                              */
/* Input        : pTempRt                                                    */
/*                pRipCxtEntry                                               */
/* Output       : None                                                       */
/* Returns      : tRipRtEntry                                                */
/*****************************************************************************/
VOID
RipAddNewBestRoutesToRtm (tRipRtEntry * pTempRt, tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome = 0;
    INT4                i4Metric1 = 0;
    UINT4               au4Indx[2];    /* Index for Trie is madeup of 2 members */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    tRipRtEntry        *pRt = NULL;

    /* pTempRt is not Best route and no need to update 
     * again best routes to RTM */
    if (pTempRt->u1RouteStatusInRtm == OSIX_FALSE)
    {
        return;
    }
    else
    {
        pTempRt->u1RouteStatusInRtm = OSIX_FALSE;
    }

    au4Indx[0] =
        RIP_HTONL (pTempRt->RtInfo.u4DestNet & pTempRt->RtInfo.u4DestMask);
    au4Indx[1] = RIP_HTONL (pTempRt->RtInfo.u4DestMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[RIP_ID - 1];

        while (pRt != NULL)
        {
            if (pRt == pTempRt)
            {
                /* If the same route is found in the list, just skip it */
                pRt = pRt->pNextAlternatepath;
                continue;
            }

            /* Same metric routes no need update to RTM 
             * because ECMP best routes already update to RTM */
            if ((pRt != pTempRt) && (pRt->u1RouteStatusInRtm == OSIX_TRUE))
            {
                /* Already ECMP routes, so the deletion of this does not 
                 * require new route best route selection */
                return;
            }
            pRt = pRt->pNextAlternatepath;
        }                        /* while */

        pRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[RIP_ID - 1];

        if (pRt != NULL)
        {
            i4Metric1 = pRt->RtInfo.i4Metric1;
        }

        while (pRt != NULL)
        {
            if (pRt == pTempRt)
            {
                /* If the same route is found in the list, just skip it */
                pRt = pRt->pNextAlternatepath;
                continue;
            }

            if (pRt->RtInfo.i4Metric1 == RIP_INFINITY)
            {
                break;
            }

            if ((pRt->RtInfo.i4Metric1 == i4Metric1) &&
                (pRt->u1RouteStatusInRtm == OSIX_FALSE))
            {
                RipAddRouteToForwardingTableInCxt (pRipCxtEntry, pRt);
                pRt->u1RouteStatusInRtm = OSIX_TRUE;
            }
            pRt = pRt->pNextAlternatepath;
        }                        /* while */

    }
    return;
}
