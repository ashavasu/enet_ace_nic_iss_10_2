/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: riprrd.c,v 1.62 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains the functions to handle  
 *             packets from RTM.                           
 *
 *******************************************************************/

#include "ripinc.h"

        /***************************************************************
        >>>>>>>>> PUBLIC Routines of this Module Start Here <<<<<<<<<<
        ***************************************************************/

/*******************************************************************************
Function            :   RipProcessPktFromRtm.

Description         :   This function is called from riptskmg.c whenever it
                        receives an event indicating a packet from RTM.

Input Parameters    :   pBuf Pointer to a message buffer containing information
                        from RTM. 

Output Parameters   :   None.

Global Variables
Affected            :   gRipRRDGblCfg 

Return Value        :   Success | Failure | Rip_trigger_update .

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipProcessPktFromRtm 
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/
INT4
RipProcessPktFromRtm (tRIP_BUF_CHAIN_HEADER * pBuf)
{
    tRtmMsgHdr          RtmHdr;
    tRtmRegnAckMsg      AckMsg;
    INT4                i4RetVal = RIP_SUCCESS;

    /* get the message type */

    UINT1               u1MsgType;

    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "RipProPktRtm");
    MEMCPY (&RtmHdr, IP_GET_MODULE_DATA_PTR (pBuf), sizeof (tRtmMsgHdr));

    u1MsgType = RtmHdr.u1MessageType;

    switch (u1MsgType)
    {
        case RIP_RTM_ACK_MSG:
            /* store in global structure */
            if (RIP_BUF_COPY_FROM_CHAIN (pBuf, &AckMsg, 0,
                                         sizeof (tRtmRegnAckMsg)) !=
                sizeof (tRtmRegnAckMsg))
            {
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    BUFFER_TRC, RIP_NAME,
                                    "Error while extracting from buffer");
                i4RetVal = RIP_FAILURE;
            }
            else
            {
                /*
                 *It should be revised.routerId not used in RIP.
                 */
                /* It should be uncommented once RTM supports MI 
                   pRipCxtEntry = RipGetCxtInfoRec (RIP_DEFAULT_CXT);
                   pRipCxtEntry->RipRRDGblCfg.u2ASNumber = AckMsg.u2ASNumber;
                   pRipCxtEntry->RipRRDGblCfg.u4RouterId = AckMsg.u4RouterId;
                   pRipCxtEntry->RipRRDGblCfg.u4MaxMessageSize =
                   AckMsg.u4MaxMessageSize; */
            }
            RIP_RELEASE_BUF (pBuf, FALSE);
            break;

        default:
            i4RetVal = RIP_FAILURE;
            RIP_RELEASE_BUF (pBuf, FALSE);
            break;

    }/*** end of switch ***/
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : RipProcessRtmRts                                        */
/*                                                                           */
/* Description     : This procedure Process the Routes from RTM              */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
RipProcessRtmRts ()
{
    tExpRtNode         *pRtmRouteMsg = NULL;
    tNetIpv4RtInfo     *pRtInfo = NULL;
    INT4                i4ReturnValue = 0;
    tRipCxt            *pRipCxtEntry = NULL;
    INT1                i1TriggerUpdate = 0;
    CHR1               *pu1DestNetString = NULL;

    OsixSemTake (gRipRtr.RipRtmLstSemId);
    while ((pRtmRouteMsg = (tExpRtNode *)
            TMO_SLL_First (&gRipRtr.RtmRtLst)) != NULL)
    {
        pRtInfo = &(pRtmRouteMsg->RtInfo);

        if ((pRipCxtEntry =
             RipGetCxtInfoRec ((INT4) pRtInfo->u4ContextId)) == NULL)
        {
            break;
        }

        /* Updation for the Route already Distributed */
        if ((pRtInfo->u4RowStatus == RTM_DESTROY) ||
            ((pRtInfo->u4RowStatus == RTM_ACTIVE)
             && (pRtInfo->u2ChgBit != IP_BIT_ALL)))
        {
            i4ReturnValue = RipProcessRtChgNotification (pRtInfo, pRipCxtEntry);
        }
        else
        {
            i4ReturnValue = RipAddRtmRoute (pRtInfo, pRipCxtEntry);
        }

        if (i4ReturnValue == RIP_TRIGGER_UPDATE)
        {
            i1TriggerUpdate = RIP_TRIGGER_UPDATE;
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1DestNetString, pRtInfo->u4DestNet);

        RIP_TRC_ARG2 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC, RIP_NAME,
                      "Route newly learnt through re-distribution has "
                      "Destination Network = %s, Destination Mask = %d\r\n",
                      pu1DestNetString, CliGetMaskBits (pRtInfo->u4DestMask));

        TMO_SLL_Delete (&gRipRtr.RtmRtLst, &(pRtmRouteMsg->NextRt));
        RIP_FREE_RTM_ROUTE (pRtmRouteMsg);
    }
    OsixSemGive (gRipRtr.RipRtmLstSemId);

    /* Release the DataLock for accessing RTM Route List */
    if ((i1TriggerUpdate == RIP_TRIGGER_UPDATE) && (pRipCxtEntry != NULL))
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC,
                 RIP_NAME,
                 "RipSendTriggeredUpdate Initiated for redistributed routes\n");
        if (RipSendTriggeredUpdate (pRipCxtEntry) == RIP_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC,
                     RIP_NAME, "Failure in RipSendTriggeredUpdate.......\n");
        }
    }
    return i4ReturnValue;
}

/*******************************************************************************
Function            :   RipAddRtmRoute.

Description         :   This function is called whenever RIP Processes the
                        the  route update message from RTM.                      
Input Parameters    :   pRtmRoute - Route Information from RTM

Output Parameters   :   None.

Global Variables
Affected            :   None.
Return Value        :   Success | Failure .
*******************************************************************************/
INT4
RipAddRtmRoute (tNetIpv4RtInfo * pRtmRoute, tRipCxt * pRipCxtEntry)
{
    UINT4               u4Dest;
    UINT4               u4Mask;
    UINT1               u1SrcProto = 0;
    tRipRtEntry        *pRt = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT4                i4ReturnValue = RIP_SUCCESS;

    u4Dest = pRtmRoute->u4DestNet;
    u4Mask = pRtmRoute->u4DestMask;
    u1SrcProto = (UINT1) pRtmRoute->u2RtProto;

    pRt = IsRouteFoundInRipTable (u4Dest, u4Mask, pRtmRoute->u4NextHop,
                                  pRtmRoute->u4Tos, u1SrcProto, pRipCxtEntry);
    if (pRt != NULL)
    {
        /*
         * If the Entry is local type and rip is enabled over the interface, 
         * don't update
         *
         * */
        pRipIfRec = RipGetIfRec (pRt->RtInfo.u4RtIfIndx, pRipCxtEntry);

        if ((pRipIfRec != NULL) && (pRt->RtInfo.u2RtProto == CIDR_LOCAL_ID))
        {
            if (CfaIpIfIsLocalNetInCxt
                (pRtmRoute->u4ContextId, pRtmRoute->u4DestNet) != CFA_SUCCESS)
            {
                return i4ReturnValue;
            }
        }

        if (pRtmRoute->i4Metric1 == RIP_INFINITY
            && pRt->RtInfo.i4Metric1 == RIP_INFINITY)
        {
            return i4ReturnValue;
        }
        if (pRtmRoute->i4Metric1 != RIP_INFINITY
            && pRt->RtInfo.i4Metric1 == RIP_INFINITY)
        {
            /* Stop the timer running, if any */
            RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRt)));
        }
        /* Update RIP routing table */
        if (UpdateRipRoutingTable ((*pRtmRoute), pRt, pRipCxtEntry) ==
            RIP_SUCCESS)
        {
            RipUpdateRouteEntry (pRt, pRipCxtEntry);
            pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
            i4ReturnValue = RIP_TRIGGER_UPDATE;
            /* Update Import Table */
            RipUpdateImportTbl (pRt, u1SrcProto, 1, pRipCxtEntry);
        }
        return i4ReturnValue;
    }                            /* pRt != NULL */

    /*  Update Message should be converted into routing entry in local
     *  route table */
    RIP_ALLOC_ROUTE_ENTRY (pRt);
    if (pRt == NULL)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME,
                 "RipAddRtmRoute: RIP_ALLOC_ROUTE_ENTRY FAILURE");
        /* Send traps to the net manager for further actions, if required. */
        RIP_SEND_TRAP_FOR_MEM_ALLOC_FAIL (RIP_MEMORY_ALLOCATION_FAILED);
        i4ReturnValue = RIP_FAILURE;
        return (i4ReturnValue);
    }

    /*
     * We need to initialise the default values for some of the 
     * elements in the route structure. 
     */
    MEMSET (pRt, 0, sizeof (tRipRtEntry));
    RipRedDbNodeInit (&(pRt->RtInfo.RouteDbNode), RIP_RED_DYN_RT_INFO);
    pRt->RtInfo.u4DestMask = u4Mask;
    pRt->RtInfo.u4DestNet = (u4Dest & u4Mask);
    pRt->RtInfo.u4NextHop = pRtmRoute->u4NextHop;
    pRt->RtInfo.u2RtType = pRtmRoute->u2RtType;
    pRt->RtInfo.u4RowStatus = pRtmRoute->u4RowStatus;
    pRt->RtInfo.u2RtProto = (UINT2) u1SrcProto;
    pRt->RtInfo.u1Level = pRtmRoute->u1MetricType;

    pRt->RtInfo.u4RtNxtHopAS = pRtmRoute->u4RtNxtHopAs;
    /* Left shift the next hop AS number and fill the route tag given 
     * by RRD, into the lower 16 bits of u4RtNxtHopAS */
    pRt->RtInfo.u4RtNxtHopAS = pRt->RtInfo.u4RtNxtHopAS << 16;
    /*
     *NextHopAs last 2 bytes for Route Tag.
     *If Route Tag type is manual,filling the 
     *configured tag value otherwise generated value.
     * */
    if ((pRipCxtEntry->RipRRDGblCfg.u1RipRRDRouteTagType == MANUAL)
        && (pRtmRoute->u4RouteTag == 0))
    {
        pRt->RtInfo.u4RtNxtHopAS |=
            (UINT2) (pRipCxtEntry->RipRRDGblCfg.u2RipRRDRtTag);
    }
    else
    {
        pRt->RtInfo.u4RtNxtHopAS |= (UINT2) (pRtmRoute->u4RouteTag);
    }
    pRt->RtInfo.u4RtIfIndx = pRtmRoute->u4RtIfIndx;
    pRt->RtInfo.u4Tos = (UINT4) pRtmRoute->u4Tos;

    /* Triggered update should not be send for new routes. 
     * RIP advertises routes learnt from RTM with default metric set by 
     *  administrator.
     */
    pRt->RtInfo.i4Metric1 = pRipCxtEntry->RipRRDGblCfg.u2RipRRDDefMetric;
    pRt->u4RipCxtId = (UINT4) pRipCxtEntry->i4CxtId;
    if (rip_add_new_route (pRt, pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRt)));
        RIP_ROUTE_FREE (pRt);
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while addind new route to the route database");
        return RIP_FAILURE;

    }
     /*** update import table ***/
    RIP_RT_SET_CHANGED_STATUS (pRt);
    i4ReturnValue = RIP_TRIGGER_UPDATE;
    return i4ReturnValue;
}

/*******************************************************************************
Function            :   RipInitImportList.

Description         :   This functon initializes all SLLs of import list.

Input Parameters    :   None.

Output Parameters   :   None.

Global Variables
Affected            :   tRipImportList.

Return Value        :   None .

*******************************************************************************/
VOID
RipInitImportList (tRipCxt * pRipCxtEntry)
{
    INT2                i2Index;

    for (i2Index = 0; i2Index < MAX_ROUTING_PROTOCOLS; i2Index++)
    {
        TMO_SLL_Init (&pRipCxtEntry->aRipImportList[i2Index]);
    }                            /* end of for  */
}                                /* end of RipInitImp */

/*******************************************************************************
Function            :   RipUpdateImportTbl.

Description         :   This table is maintained in order to have easy and
                        faster access to redistributed routes learnt from
                        RTM. This function is called whenever RIP receives
                        route update message from RTM.
                        Called from RipAddRtmRoute.

Input Parameters    :   pRt, Route entry to be added in the import tablle. 
                        u1SrcProto, The source protocol from which routes are
                        learnt.
                        pRipCxtEntry: Pointer to the context Info         
Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   None .

*******************************************************************************/
VOID
RipUpdateImportTbl (tRipRtEntry * pRt, UINT1 u1SrcProto, UINT1 u1Flag,
                    tRipCxt * pRipCxtEntry)
{
    tRipImportList     *pRipImportRt = NULL;
    BOOL1               bRipImportFlag = RIP_FALSE;
    UINT1               u1IsisIndex = ISIS_ID - 1;

    if (pRipCxtEntry == NULL)
    {
        return;
    }

    if (u1Flag)
    {
        RIP_ALLOC_IMPORT_ROUTE (pRipCxtEntry, pRipImportRt);
        if (pRipImportRt == NULL)
        {
            return;
        }
        TMO_SLL_Init_Node (&(pRipImportRt->Next));
        pRipImportRt->pRt = pRt;
        bRipImportFlag = RIP_TRUE;
    }

    switch (u1SrcProto)
    {
        case CIDR_STATIC_ID:
            if (u1Flag)
            {
                TMO_SLL_Add (&
                             (pRipCxtEntry->aRipImportList[CIDR_STATIC_ID - 1]),
                             &(pRipImportRt->Next));
                bRipImportFlag = RIP_FALSE;
            }
            else
            {
                TMO_SLL_Scan (&
                              (pRipCxtEntry->
                               aRipImportList[CIDR_STATIC_ID - 1]),
                              pRipImportRt, tRipImportList *)
                {
                    if (pRipImportRt->pRt == pRt)
                    {
                        break;
                    }
                }
                if (pRipImportRt != NULL)
                {
                    TMO_SLL_Delete (&
                                    (pRipCxtEntry->
                                     aRipImportList[CIDR_STATIC_ID - 1]),
                                    &(pRipImportRt->Next));
                }
            }
            break;

        case CIDR_LOCAL_ID:

            if (u1Flag)
            {
                TMO_SLL_Add (&(pRipCxtEntry->aRipImportList[CIDR_LOCAL_ID - 1]),
                             &(pRipImportRt->Next));
                bRipImportFlag = RIP_FALSE;
            }
            else
            {
                TMO_SLL_Scan (&
                              (pRipCxtEntry->aRipImportList[CIDR_LOCAL_ID - 1]),
                              pRipImportRt, tRipImportList *)
                {
                    if (pRipImportRt->pRt == pRt)
                    {
                        break;
                    }
                }
                if (pRipImportRt != NULL)
                {
                    TMO_SLL_Delete (&
                                    (pRipCxtEntry->
                                     aRipImportList[CIDR_LOCAL_ID - 1]),
                                    &(pRipImportRt->Next));
                }
            }
            break;

        case OSPF_ID:
            if (u1Flag)
            {
                TMO_SLL_Add (&(pRipCxtEntry->aRipImportList[OSPF_ID - 1]),
                             &(pRipImportRt->Next));
                bRipImportFlag = RIP_FALSE;
            }
            else
            {
                TMO_SLL_Scan (&(pRipCxtEntry->aRipImportList[OSPF_ID - 1]),
                              pRipImportRt, tRipImportList *)
                {
                    if (pRipImportRt->pRt == pRt)
                    {
                        break;
                    }
                }
                if (pRipImportRt != NULL)
                {
                    TMO_SLL_Delete (&
                                    (pRipCxtEntry->aRipImportList[OSPF_ID - 1]),
                                    &(pRipImportRt->Next));
                }
            }
            break;

        case BGP_ID:
            if (u1Flag)
            {
                TMO_SLL_Add (&(pRipCxtEntry->aRipImportList[BGP_ID - 1]),
                             &(pRipImportRt->Next));
                bRipImportFlag = RIP_FALSE;
            }
            else
            {
                TMO_SLL_Scan (&(pRipCxtEntry->aRipImportList[BGP_ID - 1]),
                              pRipImportRt, tRipImportList *)
                {
                    if (pRipImportRt->pRt == pRt)
                    {
                        break;
                    }
                }
                if (pRipImportRt != NULL)
                {
                    TMO_SLL_Delete (&(pRipCxtEntry->aRipImportList[BGP_ID - 1]),
                                    &(pRipImportRt->Next));
                }
            }

            break;
        case ISIS_ID:
            if (pRt->RtInfo.u1Level == IP_ISIS_LEVEL1)
            {
                u1IsisIndex = ISIS_ID - 1;    /*ISIS_ID is 9 there is no protocol with                                               ID as 10 these can be used to level1 & level2. 
                                               for level1 aRipImportList Index 8
                                             */
            }
            else
            {
                u1IsisIndex = ISIS_ID;    /*for Level-2 aRipImportList Index 9 */
            }
            if (u1Flag)
            {
                TMO_SLL_Add (&(pRipCxtEntry->aRipImportList[u1IsisIndex]),
                             &(pRipImportRt->Next));
                bRipImportFlag = RIP_FALSE;
            }
            else
            {
                TMO_SLL_Scan (&(pRipCxtEntry->aRipImportList[u1IsisIndex]),
                              pRipImportRt, tRipImportList *)
                {
                    if (pRipImportRt->pRt == pRt)
                    {
                        break;
                    }
                }
                if (pRipImportRt != NULL)
                {
                    TMO_SLL_Delete (&
                                    (pRipCxtEntry->aRipImportList[u1IsisIndex]),
                                    &(pRipImportRt->Next));
                }
            }

            break;
        default:
            break;

    }/*** end of switch ***/

    /* Update the summary and interface specific aggegation routes for 
     * route change*/

    if ((!u1Flag) || (bRipImportFlag == RIP_TRUE))
    {
        if (pRipImportRt != NULL)
        {
            RIP_FREE_IMPORT_ROUTE (pRipCxtEntry, pRipImportRt);
        }
    }
}

/*******************************************************************************
Function            :   RipProcessRtChgNotification.

Description         :   This function is called whenever RIP Processes route
                        change notification from RTM. Route is deleted from 
                        the routing table if the bitmask is set to change 
                        in rowsattus and rowstatus is set to destroy.
                        Nothing is done in all other cases. 

Input Parameters    :   pRtmRoute - Route from RTM

Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   Success | Failure .
*******************************************************************************/
INT4
RipProcessRtChgNotification (tNetIpv4RtInfo * pRtmRoute, tRipCxt * pRipCxtEntry)
{
   /*** extract routes one by one ***/
    INT4                i4OutCome = 0;
    INT4                i4RetVal = RIP_FAILURE;
    UINT4               u4Dest;
    UINT4               u4Mask;
    UINT4               au4Indx[2];
    UINT1               u1BitMask = 0x00;
    UINT1               u1SrcProto;

    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtInfo         *apAppSpecInfo[16];
    tRipRtEntry        *pRt = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    u4Dest = pRtmRoute->u4DestNet;
    u4Mask = pRtmRoute->u4DestMask;
    u1BitMask = (UINT1) pRtmRoute->u2ChgBit;
    u1SrcProto = (UINT1) pRtmRoute->u2RtProto;

    /* If rowstatus is destroy and Bit mask received is for rowstatus we 
       will have to change the metric as infinity for that route and 
       start the garbage collect timer. Once that timer fires the timer 
       handler will take care of deleting that route 
       from the routing table. */

    if (((u1BitMask & RTM_ROW_STATUS_MASK) == RTM_ROW_STATUS_MASK) &&
        (pRtmRoute->u4RowStatus == RT_STATUS_DESTROY))
    {

        au4Indx[0] = RIP_HTONL (u4Dest & u4Mask);
        au4Indx[1] = RIP_HTONL (u4Mask);
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = (INT1) (u1SrcProto - 1);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        OutParams.Key.pKey = NULL;
        MEMSET (apAppSpecInfo, 0, 16 * sizeof (tRtInfo *));
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
        i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_SUCCESS)
        {

            pRt = ((tRipRtEntry *) OutParams.pAppSpecInfo);

            if (pRt != NULL)
            {

                pRt->RtInfo.i4Metric1 = (INT4) RIP_INFINITY;
                RIP_RT_SET_CHANGED_STATUS (pRt);
                /* When ever route change happens with metric
                 * we should update Trie such that best route will
                 * be adjusted again based on metric.
                 * Otherwise TrieSearch will give Old entry as
                 * the best route which gives chance to wrong
                 * behaviour like loop creation.
                 */
                OutParams.Key.pKey = NULL;
                MEMSET (apAppSpecInfo, 0, 16 * sizeof (tRtInfo *));
                OutParams.pObject1 = NULL;
                OutParams.pObject2 = NULL;
                OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

                i4OutCome = TrieUpdate (&(InParams), pRt, RIP_INFINITY,
                                        &OutParams);
                if (i4OutCome == TRIE_SUCCESS)
                {
                    if (pRt->RtInfo.u2RtProto == RIP_ID)
                    {
                        pRt->RtInfo.u1Operation = RIPHA_ADD_ROUTE;
                        RipRedDbUtilAddRtInfo (pRt);
                        RipRedSyncDynInfo ();
                    }

                    pRipIfRec =
                        RipGetIfRec (pRt->RtInfo.u4RtIfIndx, pRipCxtEntry);
                    if ((pRipIfRec != NULL)
                        && (pRt->RtInfo.u2RtProto != CIDR_LOCAL_ID)
                        && (pRt->RtInfo.u2RtProto != OTHERS_ID))
                    {
                        if (TMO_DLL_Is_Node_In_List
                            ((tTMO_DLL_NODE *) & pRt->RipIfNode) == TRUE)
                        {

                            TMO_DLL_Delete (&(pRipIfRec->RipIfRtList),
                                            (tTMO_DLL_NODE *) & pRt->RipIfNode);
                        }
                    }

                    /*
                     * The timer structure for RouteAge calculation that need
                     * to be initialised to garbage collect interval.
                     */
                    RIP_ROUTE_TIMER_ROUTE (pRt) = (VOID *) pRt;
                    RIP_ROUTE_TIMER_ID (pRt) = (UINT1) RIP_RT_TIMER_ID;
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &(RIP_ROUTE_TIMER_NODE (pRt)),
                                     RIP_DEF_GARBAGE_COLCT_INTERVAL);
                    pRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;

                /*** for sending triggered updates.   ***/
                    RipUpdateRouteEntry (pRt, pRipCxtEntry);
                    RipUpdateImportTbl (pRt, u1SrcProto, 0, pRipCxtEntry);

                    i4RetVal = RIP_TRIGGER_UPDATE;

                }

            }
        }
    }
    return i4RetVal;
}

/*******************************************************************************
Function            :   RipSendRtmRedistEnableMsgInCxt.

Description         :   This function is called when ever administrator sets    
                        RipRRDGblStatus  to enable.
                        or when ever administrator wants a particular protocol  
                        learnt routes to be redistributed. This is done by       
                        setting SrcProtoMsk Enable to the particular protocol     
                        from which he wants to learn routes.

Input Parameters    :   u1SrcProto , This is Source protocol mask identifying    
                        the protocol from which Rip wants redistribute routes.

Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   Success | Failure .

*******************************************************************************/
/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipSendRtmRedistEnableMsgInCxt.
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/
INT4
RipSendRtmRedistEnableMsgInCxt (tRipCxt * pRipCxtEntry,
                                INT2 i2SrcProtoMask, UINT1 *pu1RMapName)
{
    UINT4               u4Size;
    tRIP_BUF_CHAIN_HEADER *pBuf = NULL;
    tRtmMsgHdr         *pHdr = NULL;
    tRtmMsgHdr          RtmHdr;
    INT4                i4OutCome;

    pHdr = &RtmHdr;
    u4Size = sizeof (UINT4);    /* message size is hdr+ size of
                                 *  SrcProtoMask Length   */
    pBuf = (tRIP_BUF_CHAIN_HEADER *) RIP_BUF_ALLOCATE_CHAIN ((u4Size +
                                                              RMAP_MAX_NAME_LEN),
                                                             0);
    if (pBuf == NULL)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            BUFFER_TRC, RIP_NAME,
                            "Error while Allocating memory to buffer");
        return RIP_FAILURE;

    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "RipSndRtmRed");
   /*** copy message to be sent to buffer ***/
    pHdr = (tRtmMsgHdr *) RIP_GET_MODULE_DATA_PTR (pBuf);

    pHdr->u1MessageType = RIP_RTM_REDIST_ENA_MSG;
    pHdr->RegnId.u2ProtoId = RIP_ID;
    pHdr->RegnId.u4ContextId = (UINT4) pRipCxtEntry->i4CxtId;
    pHdr->u2MsgLen = sizeof (UINT4) + RMAP_MAX_NAME_LEN;

   /*** copy SrcProtoMask   ***/
    if ((i4OutCome =
         RIP_BUF_COPY_OVER_CHAIN (pBuf, &i2SrcProtoMask, 0,
                                  sizeof (UINT2))) != CRU_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            BUFFER_TRC, RIP_NAME,
                            "Error while copying to buffer");
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RIP_FAILURE;
    }

    if ((i4OutCome =
         RIP_BUF_COPY_OVER_CHAIN (pBuf, pu1RMapName, sizeof (UINT2),
                                  RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            BUFFER_TRC, RIP_NAME,
                            "Error while copying to buffer");
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RIP_FAILURE;
    }

   /*** post to RTM queue ***/
    if (RpsEnqueuePktToRtm (pBuf) == RTM_SUCCESS)
    {
        return RIP_SUCCESS;
    }
    else
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while Enqueing the pkt to RTM");
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RIP_FAILURE;
    }
} /*** end of function rip_send_rrd_msg_to_rtm ***/

/*******************************************************************************
Function            :   RipSendRtmRedistDisableMsg.

Description         :   This function is called when ever rip doesn't want to   
                        learn routes from other protocols by setting global     
                        variable RipRRDGblstatus to disable or it wants to stop 
                        a specific protocol learned routes. In any case this    
                        function is called.

Input Parameters    :   u1SrcProto , This is Source protocol mask identifying
                        the protocol from which Rip wants to stop redistribution
  

Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   Success | Failure .

*******************************************************************************/
/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipSendRtmRedistDisableMsg.
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

INT4
RipSendRtmRedistDisableMsg (INT2 i2SrcProtoMask, tRipCxt * pRipCxtEntry)
{
    tRIP_BUF_CHAIN_HEADER *pBuf = NULL;
    tRtmMsgHdr         *pHdr = NULL;
    UINT4               u4Bitflag = 1;
    UINT1               u1Count;
    INT4                i4ReturnValue = RIP_SUCCESS;

    pBuf = (tRIP_BUF_CHAIN_HEADER *) RIP_BUF_ALLOCATE_CHAIN (sizeof (INT4) +
                                                             RMAP_MAX_NAME_LEN,
                                                             0);

    if (pBuf == NULL)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 BUFFER_TRC, RIP_NAME,
                 "Error while Allocating memory to buffer");
        return RIP_FAILURE;
    }
   /*** copy message to be sent to buffer ***/

    pHdr = (tRtmMsgHdr *) RIP_GET_MODULE_DATA_PTR (pBuf);

    pHdr->u1MessageType = RIP_RTM_REDIST_DIS_MSG;
    pHdr->RegnId.u2ProtoId = RIP_ID;
    pHdr->RegnId.u4ContextId = (UINT4) pRipCxtEntry->i4CxtId;
    pHdr->u2MsgLen = sizeof (UINT4);

   /*** copy SrcProtoMask   ***/
    if ((RIP_BUF_COPY_OVER_CHAIN (pBuf, &i2SrcProtoMask, 0,
                                  sizeof (UINT2))) == CRU_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 BUFFER_TRC, RIP_NAME, "Error while copying to buffer");
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RIP_FAILURE;
    }

    if ((RIP_BUF_COPY_OVER_CHAIN (pBuf,
                                  pRipCxtEntry->RipRRDGblCfg.au1RMapName,
                                  sizeof (UINT2), RMAP_MAX_NAME_LEN))
        == CRU_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 BUFFER_TRC, RIP_NAME, "Error while copying to buffer");
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RIP_FAILURE;
    }

    /*** post to RTM queue ***/
    if (RpsEnqueuePktToRtm (pBuf) == RIP_SUCCESS)
    {
        for (u1Count = 0; u1Count < MAX_ROUTING_PROTOCOLS; u1Count++)

        {
            if ((INT2) u4Bitflag & i2SrcProtoMask)
            {
                if (u4Bitflag & RTM_ISISL1_MASK)
                {
                    if (RipDeleteImportRts ((UINT1) ISIS_ID, pRipCxtEntry)
                        == RIP_TRIGGER_UPDATE)
                    {
                        i4ReturnValue = RIP_TRIGGER_UPDATE;
                    }
                }
                else if (u4Bitflag & RTM_ISISL2_MASK)
                {
                    if (RipDeleteImportRts ((UINT1) (ISIS_ID + 1), pRipCxtEntry)
                        == RIP_TRIGGER_UPDATE)
                    {
                        i4ReturnValue = RIP_TRIGGER_UPDATE;
                    }
                }
                else
                {
                    if (RipDeleteImportRts ((UINT1) (u1Count + 1), pRipCxtEntry)
                        == RIP_TRIGGER_UPDATE)
                    {
                        i4ReturnValue = RIP_TRIGGER_UPDATE;
                    }
                }
            }
            u4Bitflag = u4Bitflag << 1;
        }
        return i4ReturnValue;
    }
    else
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 CONTROL_PLANE_TRC | ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while Enqueing the pkt to RTM");
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RIP_FAILURE;
    }
}

/*******************************************************************************
Function            :   RipDeleteImportRts.

Description         :   This function is called when rip receives RtRedist      
                        disable from RTM for a protocol. 
                        so the routes redistributed from protocol we would have 
                        already advertised into our domain. Now any changes for 
                        those routes can not be known . So those routes are now 
                        unreachable. so we delete those routes from routing     
                        table and flush the routes from the import table.  

Input Parameters    :   u1SrcProto , This is Source protocol mask identifying
                        the protocol whose routes are to be deleted. 

Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   Success | Failure .

*******************************************************************************/
/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipDeleteImportRts.
    $$TRACE_PROCEDURE_LEVEL = INTMD

******************************************************************************/
INT4
RipDeleteImportRts (UINT1 u1SrcProto, tRipCxt * pRipCxtEntry)
{
    tRipImportList     *pTempRt = NULL;
    tRipRtEntry        *pRt = NULL;
    UINT4               u4Count;
    UINT4               u4Temp;
    UINT1               u1RipEnabledOnInterface = FALSE;
    tRipIfaceRec       *pRipIfRec = NULL;

    if ((u1SrcProto > MAX_ROUTING_PROTOCOLS) || (u1SrcProto <= 0))
    {
        return RIP_FAILURE;
    }
    u4Count = TMO_SLL_Count (&pRipCxtEntry->aRipImportList[u1SrcProto - 1]);
   /*** extract nodes one by one and start the deletion process ***/
    if (u4Count)
    {
        for (u4Temp = 0; u4Temp < u4Count; u4Temp++)
        {
      /*** Get the first node in the list corresponding to the specified         
       *   protocol  */

            pTempRt =
                (tRipImportList *)
                TMO_SLL_Get (&pRipCxtEntry->aRipImportList[u1SrcProto - 1]);
            if (pTempRt == NULL)
            {
                continue;
            }
            pRt = pTempRt->pRt;
      /*** extract the key parms from the entry                  
       *   and Initialise and start the timers . this is because 
       *   redistributed routes doesn't get any periodical updates
       *   untill we get any route change notification routes valid
       */

            /* Get the Interface Record from Hash Table */
            pRipIfRec = RipGetIfRec (pRt->RtInfo.u4RtIfIndx, pRipCxtEntry);

            if (pRipIfRec != NULL)
            {
                if ((pRt->RtInfo.u2RtProto == CIDR_LOCAL_ID) &&
                    (pRipIfRec->RipIfaceCfg.u2AdminStatus
                     == RIPIF_ADMIN_ACTIVE) &&
                    ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag
                      == RIP_ADMIN_ENABLE) ||
                     (pRipIfRec->RipIfaceCfg.u1RipAdminFlag
                      == RIP_ADMIN_PASSIVE)))
                {
                    /* We set the metric to infinity and initiate
                     * garbage collection only if RIP is not enabled
                     * on the interface in case this is a Local route
                     */
                    u1RipEnabledOnInterface = TRUE;
                }
            }

            if ((u1RipEnabledOnInterface == FALSE) &&
                (pRt->RtInfo.i4Metric1 != RIP_INFINITY))
            {
                RIP_ROUTE_TIMER_ROUTE (pRt) = (VOID *) pRt;
                RIP_ROUTE_TIMER_ID (pRt) = (UINT1) RIP_RT_TIMER_ID;

                pRt->RtInfo.i4Metric1 = RIP_INFINITY;
                RIP_RT_SET_CHANGED_STATUS (pRt);
                if ((pRipIfRec != NULL)
                    && (pRt->RtInfo.u2RtProto != CIDR_LOCAL_ID)
                    && (pRt->RtInfo.u2RtProto != OTHERS_ID))
                {
                    if (TMO_DLL_Is_Node_In_List
                        ((tTMO_DLL_NODE *) & pRt->RipIfNode) == TRUE)
                    {

                        TMO_DLL_Delete (&(pRipIfRec->RipIfRtList),
                                        (tTMO_DLL_NODE *) & pRt->RipIfNode);
                    }
                }
                RipUpdateRouteEntry (pRt, pRipCxtEntry);
                RIP_START_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRt)),
                                 RIP_DEF_GARBAGE_COLCT_INTERVAL);
                pRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;

            }                    /* u1RipEnabledOnInterface == FALSE */

            RIP_FREE_IMPORT_ROUTE (pRipCxtEntry, pTempRt);
            u1RipEnabledOnInterface = FALSE;

        }
     /*** end of for ***/

        return RIP_TRIGGER_UPDATE;
    }
    return RIP_SUCCESS;
} /*** end of rip_delete ***/

/*******************************************************************************
Function            :   IsRouteFoundInRipTable

Description         :   This function is to check whether the route is found in
                        the rip routing table or not.

Input Parameters    :   u4DestinationNetworkAddress, u4DestinationNetworkMask
                        and u1SourceProtocolId

Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   Returns the pointer to route, if the route exists 
                        else NULL.

*******************************************************************************/
/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = IsRouteFoundInRipTable.
    $$TRACE_PROCEDURE_LEVEL = INTMD

******************************************************************************/
tRipRtEntry        *
IsRouteFoundInRipTable (UINT4 u4DestinationNetworkAddress,
                        UINT4 u4DestinationNetworkMask,
                        UINT4 u4NextHop, UINT4 u4Tos, UINT1 u1SourceProtocolId,
                        tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome = 0;
    UINT4               au4Indx[2];
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pRt = NULL;
    tRipRtInfo         *apAppSpecInfo[16];

    /* TrieUpdate may send pObject1 as uninitialized. Initialize it now. */
    memset (&OutParams, 0, sizeof (tOutputParams));
    au4Indx[0] = RIP_HTONL (u4DestinationNetworkAddress &
                            u4DestinationNetworkMask);
    au4Indx[1] = RIP_HTONL (u4DestinationNetworkMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (INT1) (u1SourceProtocolId - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, 16 * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pRt = ((tRipRtEntry *) OutParams.pAppSpecInfo);
        for (; pRt; pRt = pRt->pNextAlternatepath)
        {
            if (pRt->RtInfo.u4NextHop == u4NextHop &&
                pRt->RtInfo.u4Tos == u4Tos)
            {
                break;
            }
        }
    }

    return pRt;

}                                /* End of IsFoundRouteInRipTable */

/*******************************************************************************
Function            :   UpdateRipRoutingTable.

Description         :   This function is to update the RIP Trie instance for
                        a route given.

Input Parameters    :   RtUpdate-given by RTM, pRt - pointer to the route entry in the rip
                        routing table, pRipCxtEntry- Pointer to the context Info         

Output Parameters   :   None.

Global Variables
Affected            :   None.

Return Value        :   Returns the pointer to route, if the route exists 
                        else NULL.

*******************************************************************************/
/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = UpdateRipRoutingTable.
    $$TRACE_PROCEDURE_LEVEL = INTMD

******************************************************************************/
INT1
UpdateRipRoutingTable (tNetIpv4RtInfo RtUpdate, tRipRtEntry * pRt,
                       tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome = RIP_SUCCESS;
    UINT4               au4Indx[2];
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfaceRec       *pRipIfRec = NULL;
    pRipIfRec = RipGetIfRec (pRt->RtInfo.u4RtIfIndx, pRipCxtEntry);

    /* TrieUpdate may send pObject1 as uninitialized. Initialize it now. */
    memset (&OutParams, 0, sizeof (tOutputParams));

    au4Indx[0] = RIP_HTONL (RtUpdate.u4DestNet & RtUpdate.u4DestMask);
    au4Indx[1] = RIP_HTONL (RtUpdate.u4DestMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (INT1) (pRt->RtInfo.u2RtProto - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    OutParams.pAppSpecInfo = NULL;
    /* Copy the changed fields and update the trie */
    pRt->RtInfo.u4RtIfIndx = RtUpdate.u4RtIfIndx;
    pRt->RtInfo.u2RtType = RtUpdate.u2RtType;
    pRt->RtInfo.u4RtNxtHopAS = RtUpdate.u4RtNxtHopAs;
    pRt->RtInfo.u4RowStatus = RtUpdate.u4RowStatus;
    pRt->RtInfo.u1Level = RtUpdate.u1MetricType;
    if (RtUpdate.i4Metric1 != pRt->RtInfo.i4Metric1)
    {
        /*while redsitribute all is given in rip , the current design considers the all the rip enabled interfaces as 
           connected routes and assigns a metric as 3 and update in RIP database. But when only network is enabled without 
           redistribution connected the metric will be 2 in the rip database. The fix is made such that only for rip enable 
           interfaces the metric will always remain 2 irrespective of it redistribution is enabled or not
         */
        if (RipCheckNetworkIsPresent (pRt->RtInfo.u4DestNet,
                                      pRt->RtInfo.u4DestMask) == RIP_SUCCESS)
        {
            pRt->RtInfo.i4Metric1 = RIP_DEFAULT_METRIC;
        }
        else
        {
            pRt->RtInfo.i4Metric1 =
                pRipCxtEntry->RipRRDGblCfg.u2RipRRDDefMetric;
        }
        i4OutCome =
            TrieUpdate (&InParams, pRt, (UINT4) pRt->RtInfo.i4Metric1,
                        &OutParams);
        if (i4OutCome == TRIE_SUCCESS)
        {

            if ((pRipIfRec != NULL) && (RtUpdate.i4Metric1 == RIP_INFINITY) &&
                (pRt->RtInfo.u2RtProto != CIDR_LOCAL_ID) &&
                (pRt->RtInfo.u2RtProto != OTHERS_ID))
            {
                if (TMO_DLL_Is_Node_In_List ((tTMO_DLL_NODE *) & pRt->RipIfNode)
                    == TRUE)
                {
                    TMO_DLL_Delete (&(pRipIfRec->RipIfRtList),
                                    (tTMO_DLL_NODE *) & pRt->RipIfNode);
                }
            }
            if (OutParams.pObject1 != NULL)
            {
                RipMoveLeafToListEnd (OutParams.pObject1, pRipIfRec,
                                      pRipCxtEntry);
            }
            else if (OutParams.pObject2 != NULL)
            {
                RipDeleteLeafFromList (OutParams.pObject2,
                                       (BOOLEAN) OutParams.u1LeafFlag,
                                       pRipIfRec, pRipCxtEntry);
            }
        }
    }

    return ((INT1) i4OutCome);

}                                /* End of UpdateRipRoutingTable */
