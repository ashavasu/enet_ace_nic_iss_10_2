/********************************************************************
 * $Id: ripsncfg.c,v 1.38 2016/03/26 09:59:47 siva Exp $
 *
 * Description:This file contains the routines for RIP task 
 *             configurations at global level and also the  
 *             related SNMP low level routines.          
 *
 *******************************************************************/
#include "ripinc.h"
#include "ripcli.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"

    /*|**********************************************************|**
     *** <<<<     Global Variable Declarations Start Here >>>> ***
     **|**********************************************************|* */
    /*|**********************************************************|**
     *** <<<<      local prototypes are delared here      >>>> ***
     **|**********************************************************|* */
PRIVATE VOID RipUpdateNeighborList ARG_LIST ((INT1, tRipCxt *));
PRIVATE UINT1 RipGetNeighborIndex ARG_LIST ((INT4, tRipCxt *));
    /***************************************************************
    >>>>>>>>> PUBLIC Routines of this Module Start Here <<<<<<<<<<
    ***************************************************************/

    /***************************************************************
    * The following functions act as low level GET routines for the*
    * scalar variables in the rip2 management information base.    *
    ***************************************************************/
/****************************************************************************
 Function    :    nmhGetRip2GlobalRouteChanges
 Input         :    The Indices

        The Object
        retValRip2GlobalRouteChanges
 Output         :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2GlobalRouteChanges ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2GlobalRouteChanges (UINT4 *pu4RetValRip2GlobalRouteChanges)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    /***  $$TRACE_LOG (ENTRY, "Entering Function,"
               "rip_get_rip2GlobalRouteChanges \n");  ***/
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValRip2GlobalRouteChanges =
        pRipCxtEntry->RipGblStats.u4GlobalRouteChanges;

    /***  $$TRACE_LOG (EXIT, "Exiting Function,"
               "rip_get_rip2GlobalRouteChanges \n");  ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetRip2GlobalQueries
 Input         :    The Indices

        The Object
        retValRip2GlobalQueries
 Output         :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2GlobalQueries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetRip2GlobalQueries (UINT4 *pu4RetValRip2GlobalQueries)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    /***  $$TRACE_LOG (ENTRY, "Entering Function,"
               "rip_get_rip2GlobalQueries \n");     ***/
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValRip2GlobalQueries = pRipCxtEntry->RipGblStats.u4GlobalQueries;

    /***  $$TRACE_LOG (EXIT, "Exiting Function,"
               "rip_get_rip2GlobalQueries \n");     ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFsRip2Security
 Input         :    The Indices

        The Object
        retValRip2Security
 Output         :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2Security ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2Security (INT4 *pi4RetValRip2Security)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    /***  $$TRACE_LOG (ENTRY, "Entering Function,"
               "rip_get_rip2Security \n");  ***/
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2Security = (INT4) (pRipCxtEntry->RipGblCfg.u1MaximumSecurity);

    /***  $$TRACE_LOG (EXIT, "Exiting Function,"
               "rip_get_rip2Security \n");  ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFsRip2Peers
 Input         :    The Indices

        The Object
        retValRip2Peers
 Output         :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2Peers ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2Peers (INT4 *pi4RetValRip2Peers)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    /***  $$TRACE_LOG (ENTRY, "Entering Function,"
               "rip_get_rip2Peers \n");     ***/
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValRip2Peers = (UINT4) (pRipCxtEntry->RipGblCfg.u2TotalNoOfPeers);

    /***  $$TRACE_LOG (EXIT, "Exiting Function,"
               "rip_get_rip2Peers \n");     ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2SpacingEnable
 Input       :  The Indices

                The Object 
                retValRip2SpacingEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2SpacingEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2SpacingEnable (INT4 *pi4RetValRip2SpacingEnable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValRip2SpacingEnable = RIP_SPACING_STATUS (pRipCxtEntry);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsRip2AutoSummaryStatus
 Input       :  The Indices

                The Object
                retValFsRip2AutoSummaryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip2AutoSummaryStatus (INT4 *pi4RetValFsRip2AutoSummaryStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2AutoSummaryStatus =
        (INT4) RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2TrustNBRListEnable
 Input       :  The Indices

                The Object 
                retValFsRip2TrustNBRListEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2TrustNBRListEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2TrustNBRListEnable (INT4 *pi4RetValFsRip2TrustNBRListEnable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2TrustNBRListEnable =
        (INT4) pRipCxtEntry->RipNbrListCfg.u1RipNbrListStatus;

  /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhGetFsRip2NumberOfDroppedPkts
 Input         :    The Indices

        The Object
        retValRip2NumberOfDroppedPkts
 Output         :    The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetRip2NumberOfDroppedPkts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2NumberOfDroppedPkts (UINT4 *pu4RetValRip2NumberOfDroppedPkts)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
   /***     $$TRACE_LOG (ENTRY, "Entering nmhGetRip2NumberOfDroppedPkts()..  \n");  ***/

    *pu4RetValRip2NumberOfDroppedPkts =
        pRipCxtEntry->RipNbrListCfg.u4NumPktsDropped;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/* get routines for TRIG_RIP */
/****************************************************************************
 Function    :  nmhGetFsRip2RetransTimeoutInt
 Input       :  The Indices

                The Object 
                retValFsRip2RetransTimeoutInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2RetransTimeoutInt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2RetransTimeoutInt (INT4 *pi4RetValFsRip2RetransTimeoutInt)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2RetransTimeoutInt = RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2MaxRetransmissions
 Input       :  The Indices

                The Object 
                retValFsRip2MaxRetransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2MaxRetransmissions ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2MaxRetransmissions (INT4 *pi4RetValFsRip2MaxRetransmissions)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2MaxRetransmissions = RIP_CFG_MAX_RETX (pRipCxtEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2OverSubscriptionTimeout
 Input       :  The Indices

                The Object 
                retValFsRip2OverSubscriptionTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2OverSubscriptionTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2OverSubscriptionTimeout (INT4
                                     *pi4RetValFsRip2OverSubscriptionTimeout)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2OverSubscriptionTimeout =
        RIP_CFG_SUBSCRIP_TIME (pRipCxtEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRip2MaxRoutes
 Input       :  The Indices

                The Object
                retValFsRip2MaxRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2MaxRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2MaxRoutes (INT4 *pi4RetValFsRip2MaxRoutes)
{
    UNUSED_PARAM (*pi4RetValFsRip2MaxRoutes);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip2Propagate 
 Input       :  The Indices

                The Object
                retValFsRip2Propagate 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2Propagate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2Propagate (INT4 *pi4RetValFsRip2Propagate)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRip2Propagate = pRipCxtEntry->RipGblCfg.i1PropagateStatic;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipTrcFlag
 Input       :  The Indices

                The Object
                retValFsRipTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipTrcFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipTrcFlag (INT4 *pi4RetValFsRipTrcFlag)
{
    if (RIP_MGMT_CXT == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipTrcFlag = (INT4) RIP_TRC_CXT_FLAG;
    return SNMP_SUCCESS;
}

    /***************************************************************
    * The following functions act as low level VALIDATE routines   *
    * for the scalar variables in the rip2 management information  *
    * base.                                   *
    ***************************************************************/
/****************************************************************************
 Function    :    nmhTestv2FsRip2Security
 Input         :    The Indices

        The Object
        testValRip2Security
 Output         :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2Security ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2Security (UINT4 *pu4ErrorCode, INT4 i4TestValRip2Security)
{
    /*** $TRACE_LOG (ENTRY, "Entering fn, rip_validate_rip2Security \n"); ***/
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValRip2Security != (INT4) RIP_MINIMUM_SECURITY &&
        i4TestValRip2Security != (INT4) RIP_MAXIMUM_SECURITY)
    {

    /***  $TRACE_LOG (EXIT, "Security Value Failure \n");  ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /***  $TRACE_LOG (EXIT, "Exiting fn, rip_validate_rip2Security \n"
              "Validation Success \n");     ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhTestv2FsRip2Peers
 Input         :    The Indices

        The Object
        testValRip2Peers
 Output         :    The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2Peers ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2Peers (UINT4 *pu4ErrorCode, INT4 i4TestValRip2Peers)
{
    /*** $TRACE_LOG (ENTRY, "Entering fn, rip_validate_rip2Peers \n"); ***/

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValRip2Peers < RIP_MIN_PEER || i4TestValRip2Peers > RIP_MAX_PEERS)
    {

    /***  $TRACE_LOG (EXIT, "Total no of peers value Failure \n");    ***/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /***  $TRACE_LOG (EXIT, "Exiting fn, rip_validate_rip2Peers \n"
              "Validation Success \n");     ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2SpacingEnable
 Input       :  The Indices

                The Object 
                testValRip2SpacingEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Rip2SpacingEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2SpacingEnable (UINT4 *pu4ErrorCode,
                              INT4 i4TestValRip2SpacingEnable)
{
   /*** $$TRACE_LOG (ENTRY, "Rip2SpacingEnable = %d\n", i4TestValRip2SpacingEnable); ***/
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValRip2SpacingEnable == RIP_SPACING_ENABLE) ||
        (i4TestValRip2SpacingEnable == RIP_SPACING_DISABLE))
    {
      /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
    /*** $$TRACE_LOG (ABN_EXIT,"SNMP Failure\n"); ***/
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsRip2AutoSummaryStatus
 Input       :  The Indices

                The Object
                testValFsRip2AutoSummaryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip2AutoSummaryStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsRip2AutoSummaryStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRip2AutoSummaryStatus == RIP_AUTO_SUMMARY_ENABLE) ||
        (i4TestValFsRip2AutoSummaryStatus == RIP_AUTO_SUMMARY_DISABLE))
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2TrustNBRListEnable
 Input       :  The Indices

                The Object 
                testValFsRip2TrustNBRListEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2TrustNBRListEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2TrustNBRListEnable (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsRip2TrustNBRListEnable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRip2TrustNBRListEnable == RIP_NBR_LIST_ENABLE) ||
        (i4TestValFsRip2TrustNBRListEnable == RIP_NBR_LIST_DISABLE))
    {
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ABN_EXIT,"nmhTestv2Rip2TrustNeighborsListEnable - FAILED \n"); ***/
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* test routines for TRIG_RIP */
/****************************************************************************
 Function    :  nmhTestv2FsRip2RetransTimeoutInt
 Input       :  The Indices

                The Object 
                testValFsRip2RetransTimeoutInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2RetransTimeoutInt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2RetransTimeoutInt (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsRip2RetransTimeoutInt)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValFsRip2RetransTimeoutInt < 5 ||
        i4TestValFsRip2RetransTimeoutInt > 10)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2MaxRetransmissions
 Input       :  The Indices

                The Object 
                testValFsRip2MaxRetransmissions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2MaxRetransmissions ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2MaxRetransmissions (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsRip2MaxRetransmissions)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValFsRip2MaxRetransmissions < 10 ||
        i4TestValFsRip2MaxRetransmissions > 40)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2OverSubscriptionTimeout
 Input       :  The Indices

                The Object 
                testValFsRip2OverSubscriptionTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2OverSubscriptionTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2OverSubscriptionTimeout (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsRip2OverSubscriptionTimeout)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValFsRip2OverSubscriptionTimeout < 100 ||
        i4TestValFsRip2OverSubscriptionTimeout > 300)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2MaxRoutes
 Input       :  The Indices

                The Object
                testValFsRip2MaxRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2MaxRoutes  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2MaxRoutes (UINT4 *pu4ErrorCode, INT4 i4TestValFsRip2MaxRoutes)
{
    UNUSED_PARAM (i4TestValFsRip2MaxRoutes);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2Propagate
 Input       :  The Indices

                The Object
                testValFsRip2Propagate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2Propagate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2Propagate (UINT4 *pu4ErrorCode, INT4 i4TestValFsRip2Propagate)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRip2Propagate != RIP_PROP_STATIC_ENABLE &&
         i4TestValFsRip2Propagate != RIP_PROP_STATIC_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRipTrcFlag
 Input       :  The Indices

                The Object
                testValFsRipTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipTrcFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipTrcFlag (UINT4 *pu4ErrorCode, INT4 i4TestValFsRipTrcFlag)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipTrcFlag = %d\n", i4TestValFsRipTrcFlag); ***/
    if (i4TestValFsRipTrcFlag < RIP_TRC_FLAG_MIN ||
        i4TestValFsRipTrcFlag > RIP_TRC_FLAG_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip2Security
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2Security (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2Peers
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2Peers (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2TrustNBRListEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2TrustNBRListEnable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2SpacingEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2SpacingEnable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2AutoSummaryStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2AutoSummaryStatus (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2RetransTimeoutInt
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2RetransTimeoutInt (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2MaxRetransmissions
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2MaxRetransmissions (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2OverSubscriptionTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2OverSubscriptionTimeout (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2Propagate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2Propagate (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip2MaxRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2MaxRoutes (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipTrcFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipTrcFlag (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

    /***************************************************************
    * The following functions act as low level SET routines for the*
    * scalar variables in the rip2 management information base.    *
    ***************************************************************/

/****************************************************************************
 Function    :    nmhSetFsRip2Security
 Input         :    The Indices

        The Object
        setValRip2Security
 Output         :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2Security ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2Security (INT4 i4SetValRip2Security)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipCxtEntry->RipGblCfg.u1MaximumSecurity = (UINT1) i4SetValRip2Security;

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2Security;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRip2Security) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValRip2Security));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :    nmhSetFsRip2Peers
 Input         :    The Indices

        The Object
        setValRip2Peers
 Output         :    The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
 Returns     :    SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2Peers ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2Peers (INT4 i4SetValRip2Peers)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipCxtEntry->RipGblCfg.u2TotalNoOfPeers = (UINT2) i4SetValRip2Peers;
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2Peers;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRip2Peers) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValRip2Peers));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2SpacingEnable
 Input       :  The Indices

                The Object 
                setValRip2SpacingEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetRip2SpacingEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2SpacingEnable (INT4 i4SetValRip2SpacingEnable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "Rip2SpacingEnable = %d\n", i4SetValRip2SpacingEnable); ***/
    RIP_SPACING_STATUS (pRipCxtEntry) = (UINT1) i4SetValRip2SpacingEnable;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2SpacingEnable;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRip2SpacingEnable) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValRip2SpacingEnable));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2AutoSummaryStatus
 Input       :  The Indices

                The Object
                setValFsRip2AutoSummaryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip2AutoSummaryStatus (INT4 i4SetValFsRip2AutoSummaryStatus)
{
    /*if summary status is being set from enable to disable do the following
       1 delete each and every summary route that have been added to rtm as 
       sink route.Reset the sink flag.
       2 for each and every interface specific route, add the aggregated route to
       rtm if it needs to be established as sink route.Set the sink route flag
     */
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) == RIP_AUTO_SUMMARY_ENABLE)
    {
        if (i4SetValFsRip2AutoSummaryStatus == RIP_AUTO_SUMMARY_DISABLE)
        {
            RipUpdateRtmForSummaryDisable (pRipCxtEntry);
        }
    }

    /*if summary status is being set from disable to enable do the following
       1 For each and every interface route, delete the route from rtm if it 
       was added as sink route to rtm.Reset the sink flag.
       2 For each summary route , add the route to rtm if the route can be added
       as sink route.Set the sink route flag.
     */
    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) == RIP_AUTO_SUMMARY_DISABLE)
    {
        if (i4SetValFsRip2AutoSummaryStatus == RIP_AUTO_SUMMARY_ENABLE)
        {
            RipUpdateRtmForSummaryEnable (pRipCxtEntry);
        }
    }

    RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) =
        (UINT1) i4SetValFsRip2AutoSummaryStatus;
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2AutoSummaryStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2AutoSummaryStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2AutoSummaryStatus));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2TrustNBRListEnable
 Input       :  The Indices

                The Object 
                setValFsRip2TrustNBRListEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2TrustNBRListEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2TrustNBRListEnable (INT4 i4SetValFsRip2TrustNBRListEnable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipCxtEntry->RipNbrListCfg.u1RipNbrListStatus =
        (UINT1) i4SetValFsRip2TrustNBRListEnable;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2TrustNBRListEnable;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2TrustNBRListEnable) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2TrustNBRListEnable));
#endif

    return (SNMP_SUCCESS);
}

/* set routines for TRIG_RIP */
/****************************************************************************
 Function    :  nmhSetFsRip2RetransTimeoutInt
 Input       :  The Indices

                The Object 
                setValFsRip2RetransTimeoutInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2RetransTimeoutInt ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2RetransTimeoutInt (INT4 i4SetValFsRip2RetransTimeoutInt)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry) =
        (UINT2) i4SetValFsRip2RetransTimeoutInt;

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2RetransTimeoutInt;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2RetransTimeoutInt) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2RetransTimeoutInt));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2MaxRetransmissions
 Input       :  The Indices

                The Object 
                setValFsRip2MaxRetransmissions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2MaxRetransmissions ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2MaxRetransmissions (INT4 i4SetValFsRip2MaxRetransmissions)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    RIP_CFG_MAX_RETX (pRipCxtEntry) = (UINT2) i4SetValFsRip2MaxRetransmissions;
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2MaxRetransmissions;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2MaxRetransmissions) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2MaxRetransmissions));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2OverSubscriptionTimeout
 Input       :  The Indices

                The Object 
                setValFsRip2OverSubscriptionTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2OverSubscriptionTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2OverSubscriptionTimeout (INT4 i4SetValFsRip2OverSubscriptionTimeout)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    RIP_CFG_SUBSCRIP_TIME (pRipCxtEntry) =
        (UINT2) i4SetValFsRip2OverSubscriptionTimeout;

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2OverSubscriptionTimeout;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2OverSubscriptionTimeout) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2OverSubscriptionTimeout));
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip2MaxRoutes
 Input       :  The Indices

                The Object
                setValFsRip2MaxRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2MaxRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2MaxRoutes (INT4 i4SetValFsRip2MaxRoutes)
{
    UNUSED_PARAM (i4SetValFsRip2MaxRoutes);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsRip2Propagate 
 Input       :  The Indices

                The Object
                setValFsRip2Propagate 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2Propagate ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2Propagate (INT4 i4SetValFsRip2Propagate)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipCxtEntry->RipGblCfg.i1PropagateStatic = (INT1) i4SetValFsRip2Propagate;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2Propagate;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRip2Propagate) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRip2Propagate));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRipTrcFlag
 Input       :  The Indices

                The Object
                setValFsRipTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipTrcFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipTrcFlag (INT4 i4SetValFsRipTrcFlag)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
   /*** $$TRACE_LOG (ENTRY, "FsRipTrcFlag = %d\n", i4SetValFsRipTrcFlag); ***/
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    RIP_TRC_CXT_FLAG = (UINT4) i4SetValFsRipTrcFlag;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = FsMIRipTrcFlag;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipTrcFlag) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRipTrcFlag));

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table :  */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip2NBRTrustListTable
 Input       :  The Indices
                FsRip2TrustNBRIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsRip2NBRTrustListTable 
***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsRip2NBRTrustListTable (UINT4 u4FsRip2TrustNBRIpAddr)
{
   /*** $$TRACE_LOG (ENTRY,"FsRip2TrustNBRIpAddr = %u\n", u4FsRip2TrustNBRIpAddr); ***/
    if (RIP_ADDRESS_ABOVE_CLASS_C (u4FsRip2TrustNBRIpAddr) ||
        (u4FsRip2TrustNBRIpAddr == 0) ||
        (u4FsRip2TrustNBRIpAddr == IP_GEN_BCAST_ADDR))
    {
      /*** $$TRACE_LOG (ABN_EXIT,"Neighbor Address is INVALID - FAILED \n"); 
                        ***/
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip2NBRTrustListTable
 Input       :  The Indices
                FsRip2TrustNBRIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRip2NBRTrustListTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsRip2NBRTrustListTable (UINT4 *pu4FsRip2TrustNBRIpAddr)
{
   /*** $$TRACE_LOG (ENTRY,"FsRip2TrustNBRIpAddr = %u\n", *
pu4FsRip2TrustNBRIpAddr); ***/
    return nmhGetNextIndexFsRip2NBRTrustListTable (0, pu4FsRip2TrustNBRIpAddr);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip2NBRTrustListTable
 Input       :  The Indices
                FsRip2TrustNBRIpAddr
                nextFsRip2TrustNBRIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRip2NBRTrustListTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsRip2NBRTrustListTable (UINT4 u4FsRip2TrustNBRIpAddr,
                                        UINT4 *pu4NextFsRip2TrustNBRIpAddr)
{
    UINT1               u1Index;
    UINT4               u4NeighborAddr;
    UINT4               u4NumAuthorizedNbrs;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    u4NeighborAddr = u4FsRip2TrustNBRIpAddr;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u4NumAuthorizedNbrs = pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs;
    if (u4NumAuthorizedNbrs != 0)
    {
        for (u1Index = 0; u1Index < u4NumAuthorizedNbrs; u1Index++)
        {

            u4NeighborAddr =
                ((pRipCxtEntry->RipNbrListCfg.aNbrList[u1Index].
                  u4FsRip2TrustNBRIpAddr > u4FsRip2TrustNBRIpAddr)
                 && ((u4NeighborAddr == u4FsRip2TrustNBRIpAddr)
                     || (pRipCxtEntry->RipNbrListCfg.aNbrList[u1Index].
                         u4FsRip2TrustNBRIpAddr <
                         u4NeighborAddr))) ? pRipCxtEntry->RipNbrListCfg.
                aNbrList[u1Index].u4FsRip2TrustNBRIpAddr : u4NeighborAddr;
        }

        if (u4NeighborAddr != u4FsRip2TrustNBRIpAddr)
        {
            *pu4NextFsRip2TrustNBRIpAddr = u4NeighborAddr;
            return SNMP_SUCCESS;
        }

    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsRip2TrustNBRRowStatus
 Input       :  The Indices
                FsRip2TrustNBRIpAddr

                The Object 
                retValFsRip2TrustNBRRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2TrustNBRRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2TrustNBRRowStatus (UINT4 u4FsRip2TrustNBRIpAddr,
                               INT4 *pi4RetValFsRip2TrustNBRRowStatus)
{
    INT1                u1RetStatus;
    UINT1               u1NumNeighborIndex;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1RetStatus =
        RipGetNeighborPosition ((INT4) u4FsRip2TrustNBRIpAddr, pRipCxtEntry);

    if (u1RetStatus != RIP_NBR_NOT_FOUND)
    {
        u1NumNeighborIndex =
            RipGetNeighborIndex ((INT4) u4FsRip2TrustNBRIpAddr, pRipCxtEntry);
        if (u1NumNeighborIndex >= RIP_MAX_NUM_NBRS)
        {
            return SNMP_FAILURE;
        }
        *pi4RetValFsRip2TrustNBRRowStatus =
            (INT4) pRipCxtEntry->RipNbrListCfg.aNbrList[u1NumNeighborIndex].
            u4FsRip2TrustNBRRowStatus;
      /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (ABN_EXIT," nmhGetRip2NeighborRowStatus() - Failed\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRip2TrustNBRRowStatus
 Input       :  The Indices
                FsRip2TrustNBRIpAddr

                The Object 
                setValFsRip2TrustNBRRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2TrustNBRRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2TrustNBRRowStatus (UINT4 u4FsRip2TrustNBRIpAddr, INT4
                               i4SetValFsRip2TrustNBRRowStatus)
{
    UINT4               u4NumNeighbors;
    UINT1               u1NumNeighborIndex;
    INT1                i1RetValue = 0;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2TrustNBRRowStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2TrustNBRRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FsRip2TrustNBRIpAddr, i4SetValFsRip2TrustNBRRowStatus));
#endif
   /*** $$TRACE_LOG (ENTRY,"Rip2NeighborIpAddr = %u\n", u4Rip2NeighborIpAddr);
             ***/

   /*** $$TRACE_LOG (ENTRY,"Rip2NeighborRowStatus = %d\n", 
            i4SetValRip2NeighborRowStatus); ***/

    u4NumNeighbors = pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs;

    if (i4SetValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_CREATE_AND_GO)
    {
        pRipCxtEntry->RipNbrListCfg.aNbrList[u4NumNeighbors].
            u4FsRip2TrustNBRIpAddr = u4FsRip2TrustNBRIpAddr;

        pRipCxtEntry->RipNbrListCfg.aNbrList[u4NumNeighbors].
            u4FsRip2TrustNBRRowStatus = RIP_NBR_LIST_ACTIVE;
        pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs++;
        return SNMP_SUCCESS;
    }

    else if (i4SetValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_CREATE_AND_WAIT)
    {
        pRipCxtEntry->RipNbrListCfg.aNbrList[u4NumNeighbors].
            u4FsRip2TrustNBRIpAddr = u4FsRip2TrustNBRIpAddr;

        pRipCxtEntry->RipNbrListCfg.aNbrList[u4NumNeighbors].
            u4FsRip2TrustNBRRowStatus = RIP_NBR_LIST_NOT_IN_SERVICE;
        pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs++;
        return SNMP_SUCCESS;
    }

    else if (i4SetValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_DESTROY)
    {
        i1RetValue =
            RipGetNeighborPosition ((INT4) u4FsRip2TrustNBRIpAddr,
                                    pRipCxtEntry);
        RipUpdateNeighborList (i1RetValue, pRipCxtEntry);
        return SNMP_SUCCESS;
    }

    else if ((i4SetValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_NOT_IN_SERVICE) ||
             (i4SetValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_ACTIVE))
    {
        u1NumNeighborIndex =
            RipGetNeighborIndex ((INT4) u4FsRip2TrustNBRIpAddr, pRipCxtEntry);
        if (u1NumNeighborIndex >= RIP_MAX_NUM_NBRS)
        {
            return SNMP_FAILURE;
        }
        pRipCxtEntry->RipNbrListCfg.aNbrList[u1NumNeighborIndex].
            u4FsRip2TrustNBRRowStatus = (UINT4) i4SetValFsRip2TrustNBRRowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip2TrustNBRRowStatus
 Input       :  The Indices
                FsRip2TrustNBRIpAddr

                The Object 
                testValFsRip2TrustNBRRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2TrustNBRRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2TrustNBRRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsRip2TrustNBRIpAddr,
                                  INT4 i4TestValFsRip2TrustNBRRowStatus)
{
    INT1                i1RetValue = 0;
    UINT4               u4NumNeighbors;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((CFA_IS_LOOPBACK_RANGE (u4FsRip2TrustNBRIpAddr))
        || (IP_IS_ZERO_NETWORK (u4FsRip2TrustNBRIpAddr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
        return SNMP_FAILURE;
    }

    if (pRipCxtEntry->RipNbrListCfg.u1RipNbrListStatus == RIP_NBR_LIST_ENABLE)
    {

        if (i4TestValFsRip2TrustNBRRowStatus < RIP_NBR_LIST_ACTIVE
            || i4TestValFsRip2TrustNBRRowStatus > RIP_NBR_LIST_DESTROY)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (NetIpv4IfIsOurAddressInCxt ((UINT4) pRipCxtEntry->i4CxtId,
                                        u4FsRip2TrustNBRIpAddr) ==
            NETIPV4_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }

        i1RetValue =
            RipGetNeighborPosition ((INT4) u4FsRip2TrustNBRIpAddr,
                                    pRipCxtEntry);

        if ((i4TestValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_CREATE_AND_GO) ||
            (i4TestValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_CREATE_AND_WAIT))

        {
            u4NumNeighbors = pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs;

            /* Add it after the last one. Last Index is found from number of 
             * neighbors present-1. Should check whether we can add any more 
             * or already maximum entries exist. 
             */
            if (i1RetValue != RIP_NBR_NOT_FOUND)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (u4NumNeighbors != RIP_MAX_NUM_NBRS)
            {
            /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
        }

        else if ((i4TestValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_ACTIVE) ||
                 (i4TestValFsRip2TrustNBRRowStatus ==
                  RIP_NBR_LIST_NOT_IN_SERVICE)
                 || (i4TestValFsRip2TrustNBRRowStatus == RIP_NBR_LIST_DESTROY))
        {
            if (i1RetValue != RIP_NBR_NOT_FOUND)
            {
            /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip2NBRTrustListTable
 Input       :  The Indices
                FsRip2TrustNBRIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2NBRTrustListTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRip2NBRUnicastListTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip2NBRUnicastListTable
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = 
nmhValidateIndexInstanceFsRip2NBRUnicastListTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsRip2NBRUnicastListTable (UINT4
                                                   u4FsRip2NBRUnicastIpAddr)
{
    UINT2               u2Val = FALSE;
    UINT4               u4Addr;
    UINT4               u4Mask;
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipUnicastNBRS    *pRipUnicastNBR = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            u4Addr = pRipIfRec->u4Addr;
            u4Mask = pRipIfRec->u4NetMask;

            if ((u4Addr & u4Mask) == (u4FsRip2NBRUnicastIpAddr & u4Mask))
            {
                u2Val = TRUE;
                break;
            }
        }
        if (u2Val == TRUE)
        {
            break;
        }
    }                            /* end of Interface Hash Table Scan */

    if (pRipIfRec == NULL)
    {
        /* Interface Record Not Found */
        return SNMP_FAILURE;
    }

    RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pRipUnicastNBR,
                  tRipUnicastNBRS *)
    {
        if (pRipUnicastNBR->u4UnicastNBR == u4FsRip2NBRUnicastIpAddr)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip2NBRUnicastListTable
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRip2NBRUnicastListTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsRip2NBRUnicastListTable (UINT4 *pu4FsRip2NBRUnicastIpAddr)
{

    UINT4               u4HashIndex;
    tRipUnicastNBRS    *pRipUnicastNBR = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            /* Scan the list of unicast neighbors for the specified interface */

            if ((pRipUnicastNBR =
                 (tRipUnicastNBRS
                  *) (RIP_SLL_First (&(pRipIfRec->RipUnicastNBRS)))) != NULL)

            {
                *pu4FsRip2NBRUnicastIpAddr = pRipUnicastNBR->u4UnicastNBR;
                return SNMP_SUCCESS;
            }

        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip2NBRUnicastListTable
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr
                nextFsRip2NBRUnicastIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRip2NBRUnicastListTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsRip2NBRUnicastListTable (UINT4 u4FsRip2NBRUnicastIpAddr,
                                          UINT4 *pu4NextFsRip2NBRUnicastIpAddr)
{
    UINT1               u1LastIndexFound = FALSE;
    UINT4               u4HashIndex;
    tRipUnicastNBRS    *pRipUnicastNBR = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            /* Scan the list of unicast neighbors for the specified interface */

            RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pRipUnicastNBR,
                          tRipUnicastNBRS *)
            {
                if (u1LastIndexFound == TRUE)
                {
                    *pu4NextFsRip2NBRUnicastIpAddr =
                        pRipUnicastNBR->u4UnicastNBR;
                    return SNMP_SUCCESS;
                }

                if (pRipUnicastNBR->u4UnicastNBR == u4FsRip2NBRUnicastIpAddr)
                {
                    u1LastIndexFound = TRUE;
                }
            }                    /* End of RIP_SLL_Scan */

        }
    }                            /* End Interface Hash Table Scan */

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip2NBRUnicastNBRRowStatus
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr

                The Object 
                retValFsRip2NBRUnicastNBRRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRip2NBRUnicastNBRRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRip2NBRUnicastNBRRowStatus (UINT4 u4FsRip2NBRUnicastIpAddr, INT4
                                    *pi4RetValFsRip2NBRUnicastNBRRowStatus)
{
    UINT2               u2Val = FALSE;
    tRipUnicastNBRS    *pNode = NULL;
    UINT4               u4Addr;
    UINT4               u4Mask;
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            u4Addr = pRipIfRec->u4Addr;
            u4Mask = pRipIfRec->u4NetMask;

            if ((u4Addr & u4Mask) == (u4FsRip2NBRUnicastIpAddr & u4Mask))
            {
                RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pNode,
                              tRipUnicastNBRS *)
                {
                    if (pNode->u4UnicastNBR == u4FsRip2NBRUnicastIpAddr)
                    {
                        *pi4RetValFsRip2NBRUnicastNBRRowStatus =
                            (INT4) pNode->u4UnicastNBRRowStatus;
                        return SNMP_SUCCESS;
                    }
                }
                u2Val = TRUE;
                break;
            }
        }
        if (u2Val == TRUE)
        {
            break;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRip2NBRUnicastNBRRowStatus
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr

                The Object 
                setValFsRip2NBRUnicastNBRRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRip2NBRUnicastNBRRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRip2NBRUnicastNBRRowStatus (UINT4 u4FsRip2NBRUnicastIpAddr, INT4
                                    i4SetValFsRip2NBRUnicastNBRRowStatus)
{
    UINT2               u2Val = FALSE;
    tRipUnicastNBRS    *pNode = NULL;
    UINT4               u4Addr;
    UINT4               u4Mask;
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Given Neighbor address is not local to our Router.
     * Don't Accept it.
     */
    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            u4Addr = pRipIfRec->u4Addr;
            u4Mask = pRipIfRec->u4NetMask;

            if ((u4Addr & u4Mask) == (u4FsRip2NBRUnicastIpAddr & u4Mask))
            {
                u2Val = TRUE;
                break;
            }
        }
        if (u2Val == TRUE)
        {
            break;
        }
    }                            /* End Interface Hash Table Scan */

    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4SetValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_CREATE_AND_WAIT)
        || (i4SetValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_CREATE_AND_GO))
    {
        if (i4SetValFsRip2NBRUnicastNBRRowStatus ==
            RIP_NBR_LIST_CREATE_AND_WAIT)
        {
            RIP_ALLOC_NBR_NODE (pRipIfRec, pNode);
            if (pNode == NULL)
            {
                return SNMP_FAILURE;
            }
            pNode->u4UnicastNBR = u4FsRip2NBRUnicastIpAddr;
            pNode->u4UnicastNBRRowStatus = RIP_NBR_LIST_NOT_IN_SERVICE;
        }
        else if (i4SetValFsRip2NBRUnicastNBRRowStatus ==
                 RIP_NBR_LIST_CREATE_AND_GO)
        {
            RIP_ALLOC_NBR_NODE (pRipIfRec, pNode);
            if (pNode == NULL)
            {
                return SNMP_FAILURE;
            }
            pNode->u4UnicastNBR = u4FsRip2NBRUnicastIpAddr;
            pNode->u4UnicastNBRRowStatus = RIP_NBR_LIST_ACTIVE;
        }

        RIP_SLL_Add (&(pRipIfRec->RipUnicastNBRS), &(pNode->NextNode));
        /* Passive Interface does not have the Timer associated with it.
         * We need to start a single timer for sending packets in the 
         * passive interfaces.If already started, should not restart it.
         */
        if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_PASSIVE)
        {
            if (pRipCxtEntry->u4NumOfPassNeighbors == 0)
            {
                pRipCxtEntry->RipPassiveUpdTimer.u1Id = RIP_PASS_UPD_TIMER_ID;
                RIP_START_TIMER (RIP_TIMER_ID,
                                 &((pRipCxtEntry->RipPassiveUpdTimer).
                                   Timer_node), RIP_DEF_UPDATE_INTERVAL);
            }
            pRipCxtEntry->u4NumOfPassNeighbors++;
        }
    }
    else if (i4SetValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_DESTROY)
    {
        RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pNode, tRipUnicastNBRS *)
        {
            if (pNode->u4UnicastNBR == u4FsRip2NBRUnicastIpAddr)
            {
                break;
            }
        }
        if (pNode == NULL)
        {
            return SNMP_FAILURE;
        }
        RIP_SLL_Delete (&(pRipIfRec->RipUnicastNBRS), &(pNode->NextNode));
        RIP_FREE_NBR_NODE (pRipIfRec, pNode);
        if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_PASSIVE)
        {
            pRipCxtEntry->u4NumOfPassNeighbors--;
            if (pRipCxtEntry->u4NumOfPassNeighbors == 0)
            {
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &((pRipCxtEntry->RipPassiveUpdTimer).
                                  Timer_node));
            }
        }
    }
    else if ((i4SetValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_ACTIVE) ||
             (i4SetValFsRip2NBRUnicastNBRRowStatus ==
              RIP_NBR_LIST_NOT_IN_SERVICE))
    {
        RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pNode, tRipUnicastNBRS *)
        {
            if (pNode->u4UnicastNBR == u4FsRip2NBRUnicastIpAddr)
                break;
        }
        if (pNode == NULL)
        {
            return SNMP_FAILURE;
        }
        pNode->u4UnicastNBRRowStatus =
            (UINT4) i4SetValFsRip2NBRUnicastNBRRowStatus;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRip2NBRUnicastNBRRowStatus;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRip2NBRUnicastNBRRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i", pRipCxtEntry->i4CxtId,
                      u4FsRip2NBRUnicastIpAddr,
                      i4SetValFsRip2NBRUnicastNBRRowStatus));
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRip2NBRUnicastNBRRowStatus
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr

                The Object 
                testValFsRip2NBRUnicastNBRRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRip2NBRUnicastNBRRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRip2NBRUnicastNBRRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsRip2NBRUnicastIpAddr,
                                       INT4
                                       i4TestValFsRip2NBRUnicastNBRRowStatus)
{

    UINT1               u1Available = FALSE;
    UINT2               u2Val = FALSE;
    UINT4               u4Addr;
    UINT4               u4Mask;
    UINT4               u4HashIndex;
    UINT4               u4NumOfUcastNbr = 0;
    tRipUnicastNBRS    *pRipUnicastNBR = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tRipIfaceRec       *pSelRipIf = NULL;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Scan the Logical Interface and check whether the given address is
     * Local or not.
     */

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {

            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            if ((i4TestValFsRip2NBRUnicastNBRRowStatus ==
                 RIP_NBR_LIST_CREATE_AND_WAIT)
                || (i4TestValFsRip2NBRUnicastNBRRowStatus ==
                    RIP_NBR_LIST_CREATE_AND_GO))
            {
                RIP_SLL_Scan (&(pRipIfRec->RipUnicastNBRS), pRipUnicastNBR,
                              tRipUnicastNBRS *)
                {
                    u4NumOfUcastNbr++;
                }
                if (u4NumOfUcastNbr >= RIP_MAX_NUM_NBRS)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    return SNMP_FAILURE;
                }
            }

            u4Addr = pRipIfRec->u4Addr;
            u4Mask = pRipIfRec->u4NetMask;

            if ((u4Addr & u4Mask) == (u4FsRip2NBRUnicastIpAddr & u4Mask))
            {
                pSelRipIf = pRipIfRec;
                u2Val = TRUE;
                if ((i4TestValFsRip2NBRUnicastNBRRowStatus !=
                     RIP_NBR_LIST_CREATE_AND_WAIT)
                    && (i4TestValFsRip2NBRUnicastNBRRowStatus !=
                        RIP_NBR_LIST_CREATE_AND_GO))
                {
                    break;
                }
            }
        }
        if (u2Val == TRUE)
        {
            break;
        }
    }                            /* End Of Interface Hash Table Scan */

    if (u2Val != TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_NOT_READY)
        || (i4TestValFsRip2NBRUnicastNBRRowStatus > RIP_NBR_LIST_DESTROY)
        || (i4TestValFsRip2NBRUnicastNBRRowStatus < RIP_NBR_LIST_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RIP_SLL_Scan (&(pSelRipIf->RipUnicastNBRS), pRipUnicastNBR,
                  tRipUnicastNBRS *)
    {
        if (pRipUnicastNBR->u4UnicastNBR == u4FsRip2NBRUnicastIpAddr)
        {
            u1Available = TRUE;
            break;
        }
    }

    if (u1Available != TRUE)
    {
        if ((i4TestValFsRip2NBRUnicastNBRRowStatus ==
             RIP_NBR_LIST_CREATE_AND_GO)
            || (i4TestValFsRip2NBRUnicastNBRRowStatus ==
                RIP_NBR_LIST_CREATE_AND_WAIT))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    /* If entry is available, it can be destroyed. */
    if (u1Available == TRUE)
    {
        if ((i4TestValFsRip2NBRUnicastNBRRowStatus ==
             RIP_NBR_LIST_NOT_IN_SERVICE)
            || (i4TestValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_ACTIVE)
            || (i4TestValFsRip2NBRUnicastNBRRowStatus == RIP_NBR_LIST_DESTROY))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip2NBRUnicastListTable
 Input       :  The Indices
                FsRip2NBRUnicastIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip2NBRUnicastListTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

EXPORT INT1
RipGetNeighborPosition (INT4 i4NeighborAddr, tRipCxt * pRipCxtEntry)
{
    INT1                u1GetIndex;

    /* Checks whether the given neighbor is available in the List */
    for (u1GetIndex = 0; u1GetIndex <
         (INT1) pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs; u1GetIndex++)
    {
        /* Makefile Changes - comparison btwn signed and unsigned */
        if (pRipCxtEntry->RipNbrListCfg.aNbrList[u1GetIndex].
            u4FsRip2TrustNBRIpAddr == (UINT4) i4NeighborAddr)
        {
            return u1GetIndex;
        }
    }

    return RIP_NBR_NOT_FOUND;
}

PRIVATE UINT1
RipGetNeighborIndex (INT4 i4NeighborAddr, tRipCxt * pRipCxtEntry)
{
    UINT1               u1GetIndex;

    /* Checks whether the given neighbor is available in the List */
    for (u1GetIndex = 0; u1GetIndex <
         pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs; u1GetIndex++)
    {
        /* Makefile Changes - comparison btwn signed and unsigned */
        if (pRipCxtEntry->RipNbrListCfg.aNbrList[u1GetIndex].
            u4FsRip2TrustNBRIpAddr == (UINT4) i4NeighborAddr)
            break;
    }
    return u1GetIndex;
}

PRIVATE VOID
RipUpdateNeighborList (INT1 i1FoundIndex, tRipCxt * pRipCxtEntry)
{
    UINT1               u1UpdateIndex;
    UINT1               u1NextIndex = 0;
    u1UpdateIndex = 0;

    /* Delete the contents in the given index update the array */

    for (u1UpdateIndex = (UINT1) i1FoundIndex; u1UpdateIndex <
         pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs; u1UpdateIndex++)
    {
        u1NextIndex = (UINT1) (u1UpdateIndex + 1);
        if (u1NextIndex >= RIP_MAX_NUM_NBRS)
        {
            continue;
        }

        pRipCxtEntry->RipNbrListCfg.aNbrList[u1UpdateIndex].
            u4FsRip2TrustNBRIpAddr =
            pRipCxtEntry->RipNbrListCfg.aNbrList[u1NextIndex].
            u4FsRip2TrustNBRIpAddr;
    }

    if (u1NextIndex < RIP_MAX_NUM_NBRS)
    {
        pRipCxtEntry->RipNbrListCfg.aNbrList[u1NextIndex].
            u4FsRip2TrustNBRIpAddr = 0;
        /* Update the number of valid neighbors count */

        pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs--;
    }
    return;
}

EXPORT BOOLEAN
RipCheckForValidNeighbor (UINT4 u4SrcAddr, tRipCxt * pRipCxtEntry)
{
    INT1                i1RetStatus;

    i1RetStatus = RipGetNeighborPosition ((INT4) u4SrcAddr, pRipCxtEntry);

    if (i1RetStatus == RIP_NBR_NOT_FOUND)
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

EXPORT VOID
RipInitNeighborList (tRipCxt * pRipCxtEntry)
{

    MEMSET ((VOID *) &pRipCxtEntry->RipNbrListCfg, 0, sizeof (tRipNbrListCfg));

    pRipCxtEntry->RipNbrListCfg.u1RipNbrListStatus = RIP_NBR_LIST_DISABLE;

}

EXPORT UINT4
RipGetNeighborListStatus (tRipCxt * pRipCxtEntry)
{
    return (pRipCxtEntry->RipNbrListCfg.u1RipNbrListStatus);
}

EXPORT VOID
RipIncNbrListCheckNoPktsDropped (tRipCxt * pRipCxtEntry)
{
    pRipCxtEntry->RipNbrListCfg.u4NumPktsDropped++;
}
