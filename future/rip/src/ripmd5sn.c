/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripmd5sn.c,v 1.31 2016/06/18 11:46:29 siva Exp $
 *
 * Description:Contains low level routines for MD5 authen-  
 *             cation in RIP.                              
 *
 *******************************************************************/

#include "ripinc.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"

/* INT4            RipGetNodeValues(tMd5AuthKeyInfo* pNode); */
/* LOW LEVEL Routines for Table : FsRipMd5AuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRipMd5AuthTable
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsRipMd5AuthTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsRipMd5AuthTable (UINT4 u4FsRipMd5AuthAddress,
                                           INT4 i4FsRipMd5AuthKeyId)
{

    tMd5AuthKeyInfo    *pNodeInfo = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {

        /* RIP is not enabled for this interface */

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNodeInfo, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNodeInfo) == i4FsRipMd5AuthKeyId)
        {
            /* this is a valid entry */
            return SNMP_SUCCESS;
        }
    }

    /* Entry is not found - Invalid index */

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id %x \n",
                  i4FsRipMd5AuthKeyId);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRipMd5AuthTable
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRipMd5AuthTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsRipMd5AuthTable (UINT4 *pu4FsRipMd5AuthAddress,
                                   INT4 *pi4FsRipMd5AuthKeyId)
{

    tMd5AuthKeyInfo    *pNodeInfo = NULL;
    UINT4               u4IfAddr, u4IfAddrNext;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipMd5Rec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetFirstIndexRip2IfConfTable (&u4IfAddr) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n", u4IfAddr);
        return SNMP_FAILURE;
    }

    pNodeInfo = (tMd5AuthKeyInfo *) RIP_SLL_First (&(pRipIfRec->RipMd5KeyList));
    if (pNodeInfo != NULL)
    {
        *pu4FsRipMd5AuthAddress = u4IfAddr;
        *pi4FsRipMd5AuthKeyId = (INT4) RIP_MD5_AUTH_KEY_ID (pNodeInfo);
        return SNMP_SUCCESS;
    }
    else
    {
        while (nmhGetNextIndexRip2IfConfTable (u4IfAddr, &u4IfAddrNext) !=
               SNMP_FAILURE)
        {
            pRipMd5Rec = RipGetIfRecFromAddr (u4IfAddrNext, pRipCxtEntry);
            if (pRipMd5Rec == NULL)
            {
                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              RIP_TRC_CXT_FLAG,
                              RIP_MGMT_CXT_ID,
                              MGMT_TRC, RIP_NAME,
                              "Invalid address %x \n", u4IfAddrNext);
                return SNMP_FAILURE;
            }

            pNodeInfo =
                (tMd5AuthKeyInfo *)
                RIP_SLL_First (&(pRipMd5Rec->RipMd5KeyList));
            if (pNodeInfo != NULL)
            {
                *pu4FsRipMd5AuthAddress = u4IfAddrNext;
                *pi4FsRipMd5AuthKeyId = (INT4) RIP_MD5_AUTH_KEY_ID (pNodeInfo);
                return SNMP_SUCCESS;
            }
            else
            {
                u4IfAddr = u4IfAddrNext;
            }
        }
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRipMd5AuthTable
 Input       :  The Indices
                FsRipMd5AuthAddress
                nextFsRipMd5AuthAddress
                FsRipMd5AuthKeyId
                nextFsRipMd5AuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRipMd5AuthTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsRipMd5AuthTable (UINT4 u4FsRipMd5AuthAddress,
                                  UINT4 *pu4NextFsRipMd5AuthAddress,
                                  INT4 i4FsRipMd5AuthKeyId,
                                  INT4 *pi4NextFsRipMd5AuthKeyId)
{

    tMd5AuthKeyInfo    *pNodeInfo = NULL;
    tMd5AuthKeyInfo    *pNextNode = NULL;
    tMd5AuthKeyInfo    *pNodeInfoCur = NULL;
    tMd5AuthKeyInfo    *pNode = NULL;
    UINT4               u4IfAddrNext;
    UINT4               u4IfAddr = u4FsRipMd5AuthAddress;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipIfMd5Rec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*we will first find if there is any next node for the current index
       if there is any key present then we will return the next node else
       we will return the first key for the next interface */
    pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n", u4IfAddr);
        return SNMP_FAILURE;
    }
    pNodeInfoCur =
        (tMd5AuthKeyInfo *) RIP_SLL_First (&(pRipIfRec->RipMd5KeyList));
    if (pNodeInfoCur != NULL)
    {
        RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
        {
            if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
            {
                pNextNode =
                    (tMd5AuthKeyInfo *)
                    RIP_SLL_Next (&(pRipIfRec->RipMd5KeyList),
                                  &(pNode->NextNode));

                if (pNextNode != NULL)
                {
                    *pu4NextFsRipMd5AuthAddress = u4IfAddr;
                    *pi4NextFsRipMd5AuthKeyId =
                        (INT4) RIP_MD5_AUTH_KEY_ID (pNextNode);
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    while (nmhGetNextIndexRip2IfConfTable (u4IfAddr, &u4IfAddrNext) !=
           SNMP_FAILURE)
    {
        pRipIfMd5Rec = RipGetIfRecFromAddr (u4IfAddrNext, pRipCxtEntry);
        if (pRipIfMd5Rec == NULL)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          RIP_TRC_CXT_FLAG,
                          RIP_MGMT_CXT_ID,
                          MGMT_TRC, RIP_NAME,
                          "Invalid address %x \n", u4IfAddrNext);
            return SNMP_FAILURE;
        }
        pNodeInfo =
            (tMd5AuthKeyInfo *) RIP_SLL_First (&(pRipIfMd5Rec->RipMd5KeyList));

        if (pNodeInfo != NULL)
        {
            *pu4NextFsRipMd5AuthAddress = u4IfAddrNext;
            *pi4NextFsRipMd5AuthKeyId = (INT4) RIP_MD5_AUTH_KEY_ID (pNodeInfo);
            return SNMP_SUCCESS;
        }
        else
        {
            u4IfAddr = u4IfAddrNext;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRipMd5AuthKey
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                retValFsRipMd5AuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipMd5AuthKey ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipMd5AuthKey (UINT4 u4FsRipMd5AuthAddress, INT4
                       i4FsRipMd5AuthKeyId,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsRipMd5AuthKey)
{
    tMd5AuthKeyInfo    *pNode;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {

            MEMCPY (pRetValFsRipMd5AuthKey->pu1_OctetList,
                    RIP_MD5_AUTH_KEY (pNode), MAX_AUTH_KEY_LENGTH);

            pRetValFsRipMd5AuthKey->i4_Length = MAX_AUTH_KEY_LENGTH;

            return SNMP_SUCCESS;
        }
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  RIP_TRC_CXT_FLAG,
                  RIP_MGMT_CXT_ID,
                  MGMT_TRC, RIP_NAME, "Invalid key id index %x \n",
                  i4FsRipMd5AuthKeyId);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsRipMd5KeyStartTime
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                retValFsRipMd5KeyStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipMd5KeyStartTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipMd5KeyStartTime (UINT4 u4FsRipMd5AuthAddress, INT4
                            i4FsRipMd5AuthKeyId,
                            INT4 *pi4RetValFsRipMd5KeyStartTime)
{
    tMd5AuthKeyInfo    *pNode;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {

            *pi4RetValFsRipMd5KeyStartTime = RIP_MD5_START_TIME (pNode);

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRipMd5KeyExpiryTime
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                retValFsRipMd5KeyExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipMd5KeyExpiryTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipMd5KeyExpiryTime (UINT4 u4FsRipMd5AuthAddress, INT4
                             i4FsRipMd5AuthKeyId,
                             INT4 *pi4RetValFsRipMd5KeyExpiryTime)
{
    tMd5AuthKeyInfo    *pNode;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {

            *pi4RetValFsRipMd5KeyExpiryTime = RIP_MD5_EXPIRY_TIME (pNode);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsRipMd5KeyRowStatus
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                retValFsRipMd5KeyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipMd5KeyRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipMd5KeyRowStatus (UINT4 u4FsRipMd5AuthAddress, INT4
                            i4FsRipMd5AuthKeyId,
                            INT4 *pi4RetValFsRipMd5KeyRowStatus)
{
    tMd5AuthKeyInfo    *pNode;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {

            *pi4RetValFsRipMd5KeyRowStatus = (INT4) RIP_MD5_ROW_STATUS (pNode);

            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRipMd5AuthKey
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                setValFsRipMd5AuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipMd5AuthKey ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipMd5AuthKey (UINT4 u4FsRipMd5AuthAddress, INT4
                       i4FsRipMd5AuthKeyId,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsRipMd5AuthKey)
{
    tMd5AuthKeyInfo    *pNode;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            /* configured kay may be less than the Authentication 
               key length ie. 16 bytes. SO left justify the 
               key with NULL's on the right */

            MEMSET (RIP_MD5_AUTH_KEY (pNode), 0, MAX_AUTH_KEY_LENGTH);
            MEMCPY (RIP_MD5_AUTH_KEY (pNode),
                    pSetValFsRipMd5AuthKey->pu1_OctetList,
                    MEM_MAX_BYTES (pSetValFsRipMd5AuthKey->i4_Length,
                                   MAX_AUTH_KEY_LENGTH));

            /* Check whether this row can be moved in to NOT_IN_SERVICE state */

            if ((RIP_MD5_START_TIME (pNode) != 0) &&
                (RIP_MD5_EXPIRY_TIME (pNode) != 0))
            {

                RIP_MD5_ROW_STATUS (pNode) = RIPIF_ADMIN_NOT_READY;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipMd5AuthKey;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipMd5AuthKey) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 3;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %s",
                              pRipCxtEntry->i4CxtId, u4FsRipMd5AuthAddress,
                              i4FsRipMd5AuthKeyId, pSetValFsRipMd5AuthKey));
#endif
            return SNMP_SUCCESS;
        }
    }
    /******** SCAN ___SLL ******/
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsRipMd5KeyStartTime
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                setValFsRipMd5KeyStartTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipMd5KeyStartTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipMd5KeyStartTime (UINT4 u4FsRipMd5AuthAddress, INT4
                            i4FsRipMd5AuthKeyId,
                            INT4 i4SetValFsRipMd5KeyStartTime)
{
    tMd5AuthKeyInfo    *pNode;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }
    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {

        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            {
                RIP_MD5_START_TIME (pNode) = i4SetValFsRipMd5KeyStartTime;
                RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
                SnmpNotifyInfo.pu4ObjectId = FsMIRipMd5KeyStartTime;
                SnmpNotifyInfo.u4OidLen =
                    sizeof (FsMIRipMd5KeyStartTime) / sizeof (UINT4);
                SnmpNotifyInfo.u1RowStatus = FALSE;
                SnmpNotifyInfo.pLockPointer = RipLock;
                SnmpNotifyInfo.pUnLockPointer = RipUnLock;
                SnmpNotifyInfo.u4Indices = 3;
                SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;

                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                                  pRipCxtEntry->i4CxtId, u4FsRipMd5AuthAddress,
                                  i4FsRipMd5AuthKeyId,
                                  i4SetValFsRipMd5KeyStartTime));
#endif
                return SNMP_SUCCESS;
            }
        }
    }
    /******** SCAN ___SLL ******/

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsRipMd5KeyExpiryTime
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                setValFsRipMd5KeyExpiryTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipMd5KeyExpiryTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipMd5KeyExpiryTime (UINT4 u4FsRipMd5AuthAddress, INT4
                             i4FsRipMd5AuthKeyId,
                             INT4 i4SetValFsRipMd5KeyExpiryTime)
{
    tMd5AuthKeyInfo    *pNode;
    UINT1               au1Key[MAX_AUTH_KEY_LENGTH];
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1Key, 0, MAX_AUTH_KEY_LENGTH);

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            if (i4SetValFsRipMd5KeyExpiryTime != 0)
            {
                RIP_MD5_EXPIRY_TIME (pNode) = i4SetValFsRipMd5KeyExpiryTime;
            }
            else
            {
                RIP_MD5_EXPIRY_TIME (pNode) = i4SetValFsRipMd5KeyExpiryTime;
            }

            if (!MEMCMP (RIP_MD5_AUTH_KEY (pNode), au1Key, MAX_AUTH_KEY_LENGTH))
            {

                /* all this entries available */
                RIP_MD5_ROW_STATUS (pNode) = RIPIF_ADMIN_NOT_READY;

            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipMd5KeyExpiryTime;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipMd5KeyExpiryTime) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 3;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                              pRipCxtEntry->i4CxtId, u4FsRipMd5AuthAddress,
                              i4FsRipMd5AuthKeyId,
                              i4SetValFsRipMd5KeyExpiryTime));
#endif
            return SNMP_SUCCESS;
        }
    }
    /******** SCAN ___SLL ******/

    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsRipMd5KeyRowStatus
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                setValFsRipMd5KeyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipMd5KeyRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipMd5KeyRowStatus (UINT4 u4FsRipMd5AuthAddress, INT4
                            i4FsRipMd5AuthKeyId,
                            INT4 i4SetValFsRipMd5KeyRowStatus)
{
    tMd5AuthKeyInfo    *pNode, *pNextNode;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4IfIndex = 0;
    tSNMP_OCTET_STRING_TYPE StartTime;
    tSNMP_OCTET_STRING_TYPE ExpiryTime;
    tSNMP_OCTET_STRING_TYPE AuthKey;
    UINT1               au1StartTime[RIP_DST_TIME_LEN];
    UINT1               au1ExpiryTime[RIP_DST_TIME_LEN];

    MEMSET (au1StartTime, 0, sizeof (au1StartTime));
    MEMSET (au1ExpiryTime, 0, sizeof (au1ExpiryTime));

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME, "Invalid address %x \n",
                      u4FsRipMd5AuthAddress);

        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {

            u1NodeFound = RIP_NODE_FOUND;

            break;
        }
    }
    /******** SCAN___SLL ******/

    if (u1NodeFound == RIP_NODE_NOT_FOUND)
    {
        /* Node is not there */
        /* Check whether the row status is Create and Wait */

        if ((i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_CREATE_AND_WAIT)
            || (i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_CREATE_AND_GO))
        {
            /* add a node */

            RIP_ALLOC_AUTH_KEY_NODE (pRipIfRec, pNode);
            if (pNode == NULL)
            {
                return SNMP_FAILURE;
            }

            RIP_MD5_AUTH_KEY_ID (pNode) = (UINT1) i4FsRipMd5AuthKeyId;

            RIP_MD5_SEQUENCE_NO (pNode) = 0;

            RIP_MD5_START_TIME (pNode) = -1;

            RIP_MD5_EXPIRY_TIME (pNode) = -1;

            MEMSET (RIP_MD5_AUTH_KEY (pNode), 0, MAX_AUTH_KEY_LENGTH);
            RIP_SLL_Add (&(pRipIfRec->RipMd5KeyList), &(pNode->NextNode));

            if (i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_CREATE_AND_GO)
            {
                RIP_MD5_ROW_STATUS (pNode) = RIPIF_ADMIN_ACTIVE;

                /* Filling the crypto authentication table for MD5 */
                RipGetIfIndexFromPort (pRipIfRec->IfaceId.u4IfIndex,
                                       &u4IfIndex);

                i4IfIndex = (INT4) u4IfIndex;
                AuthKey.pu1_OctetList = pNode->au1AuthKey;
                AuthKey.i4_Length = (INT4) STRLEN (pNode->au1AuthKey);
                nmhSetFsRipCryptoAuthKey (i4IfIndex, u4FsRipMd5AuthAddress,
                                          i4FsRipMd5AuthKeyId, &AuthKey);

                RipUtilGetKeyTime ((UINT4) RIP_MD5_START_TIME (pNode),
                                   au1StartTime);
                StartTime.pu1_OctetList = au1StartTime;
                StartTime.i4_Length = (INT4) STRLEN (au1StartTime);

                nmhSetFsRipCryptoKeyStartAccept (i4IfIndex, 0,
                                                 i4FsRipMd5AuthKeyId,
                                                 &StartTime);
                nmhGetFsRipCryptoKeyStartGenerate (i4IfIndex, 0,
                                                   i4FsRipMd5AuthKeyId,
                                                   &StartTime);

                RipUtilGetKeyTime ((UINT4) RIP_MD5_EXPIRY_TIME (pNode),
                                   au1ExpiryTime);
                ExpiryTime.pu1_OctetList = au1ExpiryTime;
                ExpiryTime.i4_Length = (INT4) STRLEN (au1ExpiryTime);

                nmhSetFsRipCryptoKeyStopGenerate (i4IfIndex, 0,
                                                  i4FsRipMd5AuthKeyId,
                                                  &ExpiryTime);
                nmhSetFsRipCryptoKeyStopAccept (i4IfIndex, 0,
                                                i4FsRipMd5AuthKeyId,
                                                &ExpiryTime);
                nmhSetFsRipCryptoKeyStatus (i4IfIndex, u4FsRipMd5AuthAddress,
                                            i4FsRipMd5AuthKeyId,
                                            RIP_AUTHKEY_STATUS_VALID);
            }
            else if (i4SetValFsRipMd5KeyRowStatus ==
                     RIPIF_ADMIN_CREATE_AND_WAIT)
            {
                RIP_MD5_ROW_STATUS (pNode) = RIPIF_ADMIN_NOT_IN_SERVICE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipMd5KeyRowStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipMd5KeyRowStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 3;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                              pRipCxtEntry->i4CxtId, u4FsRipMd5AuthAddress,
                              i4FsRipMd5AuthKeyId,
                              i4SetValFsRipMd5KeyRowStatus));
#endif
            return SNMP_SUCCESS;
        }
        else
        {
            /* row status is not craete and wait and the node is not there */

            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Node already present */

        if ((i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_DESTROY) ||
            (i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_NOT_IN_SERVICE))
        {
            /* Request for deleting a node */
            /* Check whether this key's expiry time is zero */
            /* and the next key's start time is zero - taken care */
            /* in test routine */

            /* we have to set the next keys start and end life time */
            /* appropriately */
            pNextNode =
                (tMd5AuthKeyInfo *) RIP_SLL_Next (&(pRipIfRec->RipMd5KeyList),
                                                  &(pNode->NextNode));

            if (pNextNode != NULL)
            {
                RIP_MD5_START_TIME (pNextNode) = (INT4) OsixGetSysUpTime ();

                RIP_MD5_EXPIRY_TIME (pNextNode) =
                    RIP_MD5_START_TIME (pNextNode) +
                    RIP_MD5_EXPIRY_TIME (pNextNode);
            }
            if (i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_DESTROY)
            {
                RIP_SLL_Delete (&(pRipIfRec->RipMd5KeyList),
                                &(pNode->NextNode));

                RIP_FREE_AUTH_KEY_NODE (pRipIfRec, pNode);
                nmhSetFsRipCryptoKeyStatus (i4IfIndex, u4FsRipMd5AuthAddress,
                                            i4FsRipMd5AuthKeyId,
                                            RIP_AUTHKEY_STATUS_DELETE);
            }

            else if (i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_NOT_IN_SERVICE)
            {
                RIP_MD5_ROW_STATUS (pNode) =
                    (UINT1) i4SetValFsRipMd5KeyRowStatus;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipMd5KeyRowStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipMd5KeyRowStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 3;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                              pRipCxtEntry->i4CxtId, u4FsRipMd5AuthAddress,
                              i4FsRipMd5AuthKeyId,
                              i4SetValFsRipMd5KeyRowStatus));
#endif
            return SNMP_SUCCESS;

        }

        if (i4SetValFsRipMd5KeyRowStatus == RIPIF_ADMIN_ACTIVE)
        {
            if (pNode != NULL)
            {
                RIP_MD5_ROW_STATUS (pNode) =
                    (UINT1) i4SetValFsRipMd5KeyRowStatus;

                /* Filling the Crypto Authentication table */

                RipGetIfIndexFromPort (pRipIfRec->IfaceId.u4IfIndex,
                                       &u4IfIndex);

                i4IfIndex = (INT4) u4IfIndex;
                AuthKey.pu1_OctetList = pNode->au1AuthKey;
                AuthKey.i4_Length = (INT4) STRLEN (pNode->au1AuthKey);
                nmhSetFsRipCryptoAuthKey (i4IfIndex, u4FsRipMd5AuthAddress,
                                          i4FsRipMd5AuthKeyId, &AuthKey);

                RipUtilGetKeyTime ((UINT4) RIP_MD5_START_TIME (pNode),
                                   au1StartTime);
                StartTime.pu1_OctetList = au1StartTime;
                StartTime.i4_Length = (INT4) STRLEN (au1StartTime);

                nmhSetFsRipCryptoKeyStartAccept (i4IfIndex, 0,
                                                 i4FsRipMd5AuthKeyId,
                                                 &StartTime);
                nmhGetFsRipCryptoKeyStartGenerate (i4IfIndex, 0,
                                                   i4FsRipMd5AuthKeyId,
                                                   &StartTime);

                RipUtilGetKeyTime ((UINT4) RIP_MD5_EXPIRY_TIME (pNode),
                                   au1ExpiryTime);
                ExpiryTime.pu1_OctetList = au1ExpiryTime;
                ExpiryTime.i4_Length = (INT4) STRLEN (au1ExpiryTime);

                nmhSetFsRipCryptoKeyStopGenerate (i4IfIndex, 0,
                                                  i4FsRipMd5AuthKeyId,
                                                  &ExpiryTime);
                nmhSetFsRipCryptoKeyStopAccept (i4IfIndex, 0,
                                                i4FsRipMd5AuthKeyId,
                                                &ExpiryTime);
                nmhSetFsRipCryptoKeyStatus (i4IfIndex, u4FsRipMd5AuthAddress,
                                            i4FsRipMd5AuthKeyId,
                                            RIP_AUTHKEY_STATUS_VALID);
                RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
                SnmpNotifyInfo.pu4ObjectId = FsMIRipMd5KeyRowStatus;
                SnmpNotifyInfo.u4OidLen =
                    sizeof (FsMIRipMd5KeyRowStatus) / sizeof (UINT4);
                SnmpNotifyInfo.u1RowStatus = TRUE;
                SnmpNotifyInfo.pLockPointer = RipLock;
                SnmpNotifyInfo.pUnLockPointer = RipUnLock;
                SnmpNotifyInfo.u4Indices = 3;
                SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                SnmpNotifyInfo.u4SeqNum = u4SeqNum;

                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u %i %i",
                                  pRipCxtEntry->i4CxtId, u4FsRipMd5AuthAddress,
                                  i4FsRipMd5AuthKeyId,
                                  i4SetValFsRipMd5KeyRowStatus));
#endif
                return SNMP_SUCCESS;
            }
        }

    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRipMd5AuthKey
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                testValFsRipMd5AuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipMd5AuthKey ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipMd5AuthKey (UINT4 *pu4ErrorCode, UINT4 u4FsRipMd5AuthAddress,
                          INT4 i4FsRipMd5AuthKeyId,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsRipMd5AuthKey)
{
    tMd5AuthKeyInfo    *pNode;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    UINT1               au1Key[MAX_AUTH_KEY_LENGTH];
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    MEMSET (au1Key, 0, MAX_AUTH_KEY_LENGTH);

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME,
                      "Invalid address for index %x in to RIP MD5 table \n",
                      u4FsRipMd5AuthAddress);
        return SNMP_FAILURE;
    }

    if (pTestValFsRipMd5AuthKey->i4_Length < 0
        || pTestValFsRipMd5AuthKey->i4_Length > MAX_AUTH_KEY_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            /* Node present */

            u1NodeFound = RIP_NODE_FOUND;

            break;

        }
    }
    /******** SCAN___SLL ******/

    if ((u1NodeFound == RIP_NODE_NOT_FOUND) ||
        (!MEMCMP
         (au1Key, pTestValFsRipMd5AuthKey->pu1_OctetList,
          MEM_MAX_BYTES (sizeof (au1Key),
                         (UINT4) pTestValFsRipMd5AuthKey->i4_Length))))
    {
        /* Configured key should not be zero */

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME,
                      "Invalid key index %d in to RIP MD5 table \n",
                      i4FsRipMd5AuthKeyId);

        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRipMd5KeyStartTime
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                testValFsRipMd5KeyStartTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipMd5KeyStartTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipMd5KeyStartTime (UINT4 *pu4ErrorCode,
                               UINT4 u4FsRipMd5AuthAddress,
                               INT4 i4FsRipMd5AuthKeyId,
                               INT4 i4TestValFsRipMd5KeyStartTime)
{
    tMd5AuthKeyInfo    *pNode, *pPrevNode = NULL;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;

    UINT4               u4NodeCount;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME,
                      "Invalid address for index %x in to RIP MD5 table \n",
                      u4FsRipMd5AuthAddress);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            /* Node present */

            u1NodeFound = RIP_NODE_FOUND;

            break;

        }
        pPrevNode = pNode;
    }
    /******** SCAN___SLL ******/

    if (u1NodeFound == RIP_NODE_FOUND)
    {
        /* Node already present */
        /* we have to check whether this is the first node for that */
        /* interface or not */
        u4NodeCount = RIP_SLL_Count (&(pRipIfRec->RipMd5KeyList));

        /* this is checked because only the first key can have a zero 
         * start time or the previous key's expiry time should be zero - 
         * ie infinity 
         The Maximum value 2147483647 is 2 to the power 31 minus 1 which is 
         maximum positive value in 4 byte integer. 
         */
        if (i4TestValFsRipMd5KeyStartTime >= -1
            && i4TestValFsRipMd5KeyStartTime < RIP_INT_MAX)
        {
            if (i4TestValFsRipMd5KeyStartTime == 0)
            {
                if ((u4NodeCount == FIRST_NODE) ||
                    (RIP_MD5_EXPIRY_TIME (pNode) == 0) ||
                    (RIP_MD5_EXPIRY_TIME (pNode) < (INT4) OsixGetSysUpTime ()))
                {
                    /* Start time can b zero for the first node */
                    /* if this is not the first node 
                       surely a previous node will be present */

                    return SNMP_SUCCESS;
                }
            }                    /* start time zero */

            else
            {
                /* start time is not zero */
                /* the start time of the key should be less than the expiry 
                 * time of the previous key. and  must be greater than 
                 * the start time  of the previous key 
                 */

                if (pPrevNode != NULL)
                {
                    if (((i4TestValFsRipMd5KeyStartTime +
                          (INT4) OsixGetSysUpTime ()) <
                         RIP_MD5_EXPIRY_TIME (pPrevNode)) &&
                        ((i4TestValFsRipMd5KeyStartTime +
                          (INT4) OsixGetSysUpTime ()) >
                         RIP_MD5_START_TIME (pPrevNode)))
                    {
                        return SNMP_SUCCESS;
                    }
                }
                else
                {
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    RIP_TRC (RIP_MOD_TRC,
             RIP_TRC_CXT_FLAG,
             RIP_MGMT_CXT_ID,
             MGMT_TRC, RIP_NAME, "Invalid index or bad start time value");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipMd5KeyExpiryTime
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                testValFsRipMd5KeyExpiryTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipMd5KeyExpiryTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipMd5KeyExpiryTime (UINT4 *pu4ErrorCode,
                                UINT4 u4FsRipMd5AuthAddress,
                                INT4 i4FsRipMd5AuthKeyId,
                                INT4 i4TestValFsRipMd5KeyExpiryTime)
{
    tMd5AuthKeyInfo    *pNode;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME,
                      "Invalid address for index %x in to RIP MD5 table \n",
                      u4FsRipMd5AuthAddress);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            /* Node present */

            u1NodeFound = RIP_NODE_FOUND;

            break;

        }
    }
    /******** SCAN___SLL ******/

    if (u1NodeFound == RIP_NODE_NOT_FOUND)
    {
        /* node is not there */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME,
                      "Bad key identifier %d . \n", i4FsRipMd5AuthKeyId);

        return SNMP_FAILURE;
    }

    /* The Maximum value 2147483647 is 2 to the power 31 minus 1 which is 
       maximum positive value in 4 byte integer. */

    if (i4TestValFsRipMd5KeyExpiryTime >= -1
        && i4TestValFsRipMd5KeyExpiryTime < RIP_INT_MAX)
    {
        if (i4TestValFsRipMd5KeyExpiryTime == 0)
        {
            /*
             * Configuring a key with infinite lifetime.
             */
            return SNMP_SUCCESS;
        }

        /* Expiry time should be set only after setting the start time and the
           expiry time of a key should be surely greater than the start time
           of the key. */

        if ((i4TestValFsRipMd5KeyExpiryTime + (INT4) OsixGetSysUpTime ())
            < RIP_MD5_START_TIME (pNode))
        {
            /* Expiry time is less than the key start time */

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_CXT_FLAG,
                     RIP_MGMT_CXT_ID,
                     MGMT_TRC, RIP_NAME, "Bad expiry time value. \n");

            return SNMP_FAILURE;

        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_CXT_FLAG,
                 RIP_MGMT_CXT_ID,
                 MGMT_TRC, RIP_NAME, "Bad expiry time value. \n");

        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipMd5KeyRowStatus
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId

                The Object 
                testValFsRipMd5KeyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipMd5KeyRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipMd5KeyRowStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4FsRipMd5AuthAddress,
                               INT4 i4FsRipMd5AuthKeyId,
                               INT4 i4TestValFsRipMd5KeyRowStatus)
{
    tMd5AuthKeyInfo    *pNode, *pNextNode = NULL;
    UINT1               u1NodeFound = RIP_NODE_NOT_FOUND;
    UINT1               au1Key[MAX_AUTH_KEY_LENGTH];
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    MEMSET (au1Key, 0, MAX_AUTH_KEY_LENGTH);

    pRipIfRec = RipGetIfRecFromAddr (u4FsRipMd5AuthAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_CXT_FLAG,
                      RIP_MGMT_CXT_ID,
                      MGMT_TRC, RIP_NAME,
                      "Invalid address for index %x in to RIP MD5 table \n",
                      u4FsRipMd5AuthAddress);
        return SNMP_FAILURE;
    }

    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        if (RIP_MD5_AUTH_KEY_ID (pNode) == i4FsRipMd5AuthKeyId)
        {
            /* Node present */

            u1NodeFound = RIP_NODE_FOUND;

            break;

        }

    }
    /******** SCAN___SLL ******/

    if (u1NodeFound == RIP_NODE_NOT_FOUND)
    {
        /* Creation of a new key */

        if ((i4TestValFsRipMd5KeyRowStatus == RIPIF_ADMIN_CREATE_AND_GO) ||
            (i4TestValFsRipMd5KeyRowStatus == RIPIF_ADMIN_CREATE_AND_WAIT))
        {
            /* alloc and add */

            return SNMP_SUCCESS;
        }
    }
    else
    {
        /* Node already exists */

        if ((i4TestValFsRipMd5KeyRowStatus == RIPIF_ADMIN_DESTROY)
            || (i4TestValFsRipMd5KeyRowStatus == RIPIF_ADMIN_NOT_IN_SERVICE))

        {
            /* For the manual deletion of a key */
            pNextNode =
                (tMd5AuthKeyInfo *) RIP_SLL_Next (&(pRipIfRec->RipMd5KeyList),
                                                  &(pNode->NextNode));

            if ((RIP_MD5_EXPIRY_TIME (pNode) == 0) &&
                (pNextNode &&
                 (RIP_MD5_START_TIME (pNextNode) == 0) &&
                 (RIP_MD5_ROW_STATUS (pNextNode) == ACTIVE)))
            {

                /* Delete a key with infinite lifetime and use the next key with 
                   the zero start time */

                return SNMP_SUCCESS;

            }
        }
        if (i4TestValFsRipMd5KeyRowStatus == RIPIF_ADMIN_ACTIVE)
        {
            /* Check whether all info available */

            if (MEMCMP (RIP_MD5_AUTH_KEY (pNode), au1Key, MAX_AUTH_KEY_LENGTH)
                && (RIP_MD5_EXPIRY_TIME (pNode) != -1)
                && (RIP_MD5_START_TIME (pNode) != -1))
            {
                return SNMP_SUCCESS;
            }
        }
        else if (i4TestValFsRipMd5KeyRowStatus == RIPIF_ADMIN_NOT_READY)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

    }
    RIP_TRC (RIP_MOD_TRC,
             RIP_TRC_CXT_FLAG,
             RIP_MGMT_CXT_ID, MGMT_TRC, RIP_NAME, "Bad row status value. \n");

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRipMd5AuthTable
 Input       :  The Indices
                FsRipMd5AuthAddress
                FsRipMd5AuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipMd5AuthTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RipMd5GetKeyToUseForSend
 Description :  Gets the key in use for the given interface index.
 Input       :  u2IfId - Inderface index for which the key in use should be 
                found.
                
 Output      :  None.
 Returns     :  pAuthInfo or NULL
****************************************************************************/
tMd5AuthKeyInfo    *
RipMd5GetKeyToUseForSend (UINT2 u2IfId, tRipCxt * pRipCxtEntry)
{
    tMd5AuthKeyInfo    *pNode = NULL;
    tMd5AuthKeyInfo    *pExpiredNode = NULL;
    tMd5AuthKeyInfo    *pKeyTouse = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4newDiff;
    UINT4               u4KeyStartGenDiff = (UINT4) -1;

    pRipIfRec = RipGetIfRec ((UINT4) u2IfId, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return NULL;
    }

    /* Scan through the entries for this interface */
    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {
        /* Scan for a active key */
        if (RIP_MD5_ROW_STATUS (pNode) == RIPIF_ADMIN_ACTIVE)
        {
            if ((UINT4) RIP_MD5_START_TIME (pNode) <= OsixGetSysUpTime ())
            {
                /* If atlease one active key whose starttime is less than 
                   system time is present, then expired keys can be deleted */
                if (pExpiredNode != NULL)
                {
                    TMO_SLL_Delete (&(pRipIfRec->RipMd5KeyList),
                                    &(pExpiredNode->NextNode));
                }
                if ((UINT4) RIP_MD5_EXPIRY_TIME (pNode) < OsixGetSysUpTime ())
                {
                    pExpiredNode = pNode;
                }
                else if ((RIP_MD5_EXPIRY_TIME (pNode) == 0) ||
                         ((UINT4) RIP_MD5_EXPIRY_TIME (pNode) >
                          OsixGetSysUpTime ()))
                {
                    /* If more than one unexpired key is available then use
                       most recent key having start time maximum */

                    u4newDiff =
                        OsixGetSysUpTime () -
                        (UINT4) RIP_MD5_START_TIME (pNode);
                    if (u4newDiff < u4KeyStartGenDiff)
                    {
                        u4KeyStartGenDiff = u4newDiff;
                        pKeyTouse = pNode;
                    }
                }
            }
        }
    }

    /* According to RFC 2082, sec 4.3, if the last key is expired, that key 
       can be used, until the new key is configured  */
    if ((pKeyTouse == NULL) && (pExpiredNode != NULL))
    {
        return pExpiredNode;
    }
    return pKeyTouse;
}

/****************************************************************************
 Function    :  RipMd5GetKeyToUseForReceive
 Description :  Gets the key in use for the given interface index.
 Input       :  u2IfId - Inderface index for which the key in use should be 
                found.
                u1KeyId - Key identifier.
                
 Output      :  None.
 Returns     :  pAuthInfo or NULL
****************************************************************************/
tMd5AuthKeyInfo    *
RipMd5GetKeyToUseForReceive (UINT2 u2IfId, UINT1 u1KeyId,
                             tRipCxt * pRipCxtEntry)
{
    tMd5AuthKeyInfo    *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRec ((UINT4) u2IfId, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return NULL;
    }

    /* Scan through the entries for this interface */

    /* the key id should be unique with in an interface */
    RIP_SLL_Scan (&(pRipIfRec->RipMd5KeyList), pNode, tMd5AuthKeyInfo *)
    {

        /* Scan for a active key */

        if (RIP_MD5_AUTH_KEY_ID (pNode) == u1KeyId)
        {
            /* we found the key with the same key id */
            return pNode;
        }

    }

    /* No key ID matches with the received key ID */

    return NULL;
}
