/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: fsripwr.c,v 1.11 
 *
 * Description: Wrapper routines for fsrip
 *
 *****************************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsriplw.h"
# include  "fsripwr.h"
# include  "fsripdb.h"
#include   "rip.h"

VOID
RegisterFSRIP ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsripOID, &fsripEntry, RipLock,
                                         RipUnLock, RipSetContext,
                                         RipResetContext, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsripOID, (const UINT1 *) "fsrip");
}

VOID
UnRegisterFSRIP ()
{
    SNMPUnRegisterMib (&fsripOID, &fsripEntry);
    SNMPDelSysorEntry (&fsripOID, (const UINT1 *) "fsrip");
}

INT4
FsRip2SecurityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2Security (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2PeersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2Peers (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2TrustNBRListEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2TrustNBRListEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2NumberOfDroppedPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2NumberOfDroppedPkts (&(pMultiData->u4_ULongValue)));
}

INT4
FsRip2SpacingEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2SpacingEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2AutoSummaryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2AutoSummaryStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2RetransTimeoutIntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2RetransTimeoutInt (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2MaxRetransmissionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2MaxRetransmissions (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2OverSubscriptionTimeoutGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2OverSubscriptionTimeout (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2PropagateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2Propagate (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2MaxRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2MaxRoutes (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipTrcFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipTrcFlag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2SecuritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2Security (pMultiData->i4_SLongValue));
}

INT4
FsRip2PeersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2Peers (pMultiData->i4_SLongValue));
}

INT4
FsRip2TrustNBRListEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2TrustNBRListEnable (pMultiData->i4_SLongValue));
}

INT4
FsRip2SpacingEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2SpacingEnable (pMultiData->i4_SLongValue));
}

INT4
FsRip2AutoSummaryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2AutoSummaryStatus (pMultiData->i4_SLongValue));
}

INT4
FsRip2RetransTimeoutIntSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2RetransTimeoutInt (pMultiData->i4_SLongValue));
}

INT4
FsRip2MaxRetransmissionsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2MaxRetransmissions (pMultiData->i4_SLongValue));
}

INT4
FsRip2OverSubscriptionTimeoutSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2OverSubscriptionTimeout (pMultiData->i4_SLongValue));
}

INT4
FsRip2PropagateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2Propagate (pMultiData->i4_SLongValue));
}

INT4
FsRip2MaxRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2MaxRoutes (pMultiData->i4_SLongValue));
}

INT4
FsRipTrcFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipTrcFlag (pMultiData->i4_SLongValue));
}

INT4
FsRip2SecurityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2Security (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2PeersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2Peers (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2TrustNBRListEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2TrustNBRListEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2SpacingEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2SpacingEnable (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2AutoSummaryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2AutoSummaryStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2RetransTimeoutIntTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2RetransTimeoutInt
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2MaxRetransmissionsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2MaxRetransmissions
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2OverSubscriptionTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2OverSubscriptionTimeout
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2PropagateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2Propagate (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2MaxRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2MaxRoutes (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipTrcFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipTrcFlag (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2SecurityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2Security (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2PeersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2Peers (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2TrustNBRListEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2TrustNBRListEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2SpacingEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2SpacingEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2AutoSummaryStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2AutoSummaryStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2RetransTimeoutIntDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2RetransTimeoutInt
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2MaxRetransmissionsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2MaxRetransmissions
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2OverSubscriptionTimeoutDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2OverSubscriptionTimeout
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2PropagateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2Propagate (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRip2MaxRoutesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2MaxRoutes (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipTrcFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipTrcFlag (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRip2NBRTrustListTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRip2NBRTrustListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRip2NBRTrustListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRip2TrustNBRRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2NBRTrustListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2TrustNBRRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2TrustNBRRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2TrustNBRRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRip2TrustNBRRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2TrustNBRRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRip2NBRTrustListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2NBRTrustListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRip2IfConfTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRip2IfConfTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRip2IfConfTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRip2IfAdminStatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfAdminStat (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfOperState (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfUpdateTmrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfUpdateTmr (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfGarbgCollectTmrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfGarbgCollectTmr
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfRouteAgeTmrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfRouteAgeTmr (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfSplitHorizonStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfSplitHorizonStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfDefRtInstallGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfDefRtInstall
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfSpacingTmrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfSpacingTmr (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfAuthType (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfInUseKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfInUseKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfConfAuthLastKeyStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfConfAuthLastKeyStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2IfAdminStatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfAdminStat (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfUpdateTmrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfConfUpdateTmr (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfGarbgCollectTmrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfConfGarbgCollectTmr
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfRouteAgeTmrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfConfRouteAgeTmr (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRip2IfSplitHorizonStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfSplitHorizonStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfDefRtInstallSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfConfDefRtInstall
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfSpacingTmrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfConfSpacingTmr (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2IfConfAuthType (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRip2IfAdminStatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfAdminStat (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfUpdateTmrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfConfUpdateTmr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfGarbgCollectTmrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfConfGarbgCollectTmr (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfRouteAgeTmrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfConfRouteAgeTmr (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRip2IfSplitHorizonStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfSplitHorizonStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfDefRtInstallTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfConfDefRtInstall (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfSpacingTmrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfConfSpacingTmr (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2IfConfAuthType (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRip2IfConfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2IfConfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRipMd5AuthTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRipMd5AuthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRipMd5AuthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRipMd5AuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipMd5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipMd5AuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsRipMd5KeyStartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipMd5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipMd5KeyStartTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRipMd5KeyExpiryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipMd5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipMd5KeyExpiryTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRipMd5KeyRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipMd5AuthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipMd5KeyRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRipMd5AuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipMd5AuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
FsRipMd5KeyStartTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipMd5KeyStartTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRipMd5KeyExpiryTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipMd5KeyExpiryTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRipMd5KeyRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipMd5KeyRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRipMd5AuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRipMd5AuthKey (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsRipMd5KeyStartTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRipMd5KeyStartTime (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRipMd5KeyExpiryTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRipMd5KeyExpiryTime (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRipMd5KeyRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRipMd5KeyRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRipMd5AuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipMd5AuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRipCryptoAuthTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRipCryptoAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRipCryptoAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRipCryptoAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipCryptoAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStartAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipCryptoKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStartGenerateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipCryptoKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStopGenerateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipCryptoKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStopAcceptGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipCryptoKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipCryptoKeyStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRipCryptoAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipCryptoAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStartAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipCryptoKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStartGenerateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipCryptoKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStopGenerateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipCryptoKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStopAcceptSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipCryptoKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipCryptoKeyStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRipCryptoAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsRipCryptoAuthKey (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsRipCryptoKeyStartAccept (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStartGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRipCryptoKeyStartGenerate (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsRipCryptoKeyStopGenerate (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRipCryptoKeyStopAccept (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
FsRipCryptoKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRipCryptoKeyStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRipCryptoAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipCryptoAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRip2NBRUnicastListTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRip2NBRUnicastListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRip2NBRUnicastListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRip2NBRUnicastNBRRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2NBRUnicastListTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2NBRUnicastNBRRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2NBRUnicastNBRRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRip2NBRUnicastNBRRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRip2NBRUnicastNBRRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRip2NBRUnicastNBRRowStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsRip2NBRUnicastListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2NBRUnicastListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRip2LocalRoutingTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRip2LocalRoutingTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRip2LocalRoutingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRip2RtIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2RtIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2RtTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2RtType (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2ProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2Proto (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].i4_SLongValue,
                               pMultiIndex->pIndex[3].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2ChgTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2ChgTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2MetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2Metric (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2RowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2RowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2GatewayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2LocalRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2Gateway (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiIndex->pIndex[2].i4_SLongValue,
                                 pMultiIndex->pIndex[3].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexFsRipAggTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRipAggTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRipAggTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRipAggStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipAggTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipAggStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRipAggStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipAggStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiIndex->pIndex[2].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRipAggStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsRipAggStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsRipAggTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipAggTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRtCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRtCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipAdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsRipAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipAdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipAdminStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRip2PeerTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRip2PeerTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRip2PeerTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRip2PeerInUseKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2PeerInUseKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRip2LastAuthKeyLifetimeStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRip2LastAuthKeyLifetimeStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsRip2LastAuthKeyLifetimeStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRip2LastAuthKeyLifetimeStatus (pMultiData->i4_SLongValue));
}

INT4
FsRip2LastAuthKeyLifetimeStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRip2LastAuthKeyLifetimeStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRip2LastAuthKeyLifetimeStatusDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRip2LastAuthKeyLifetimeStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRip2IfStatTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRip2IfStatTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRip2IfStatTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRip2IfStatRcvBadAuthPacketsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRip2IfStatTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRip2IfStatRcvBadAuthPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsRipRRDGlobalStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDGlobalStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRRDSrcProtoMaskEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDSrcProtoMaskEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRRDSrcProtoMaskDisableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDSrcProtoMaskDisable (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRRDRouteTagTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDRouteTagType (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRRDRouteTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDRouteTag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRRDRouteDefMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDRouteDefMetric (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipRRDRouteMapEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipRRDRouteMapEnable (pMultiData->pOctetStrValue));
}

INT4
FsRipRRDGlobalStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDGlobalStatus (pMultiData->i4_SLongValue));
}

INT4
FsRipRRDSrcProtoMaskEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDSrcProtoMaskEnable (pMultiData->i4_SLongValue));
}

INT4
FsRipRRDSrcProtoMaskDisableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDSrcProtoMaskDisable (pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteTagTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDRouteTagType (pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDRouteTag (pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteDefMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDRouteDefMetric (pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteMapEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipRRDRouteMapEnable (pMultiData->pOctetStrValue));
}

INT4
FsRipRRDGlobalStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDGlobalStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipRRDSrcProtoMaskEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDSrcProtoMaskEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipRRDSrcProtoMaskDisableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDSrcProtoMaskDisable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteTagTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDRouteTagType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDRouteTag (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteDefMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDRouteDefMetric
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipRRDRouteMapEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipRRDRouteMapEnable
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsRipRRDGlobalStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDGlobalStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipRRDSrcProtoMaskEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDSrcProtoMaskEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipRRDSrcProtoMaskDisableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDSrcProtoMaskDisable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipRRDRouteTagTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDRouteTagType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipRRDRouteTagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDRouteTag (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipRRDRouteDefMetricDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDRouteDefMetric
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipRRDRouteMapEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipRRDRouteMapEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));

}

INT4
GetNextIndexFsRipDistInOutRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRipDistInOutRouteMapTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRipDistInOutRouteMapTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRipDistInOutRouteMapValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipDistInOutRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipDistInOutRouteMapValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRipDistInOutRouteMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRipDistInOutRouteMapTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRipDistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRipDistInOutRouteMapValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRipDistInOutRouteMapValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRipDistInOutRouteMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsRipDistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRipDistInOutRouteMapValueTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsRipDistInOutRouteMapValue (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsRipDistInOutRouteMapRowStatusTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsRipDistInOutRouteMapRowStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsRipDistInOutRouteMapTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipDistInOutRouteMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRipPreferenceValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRipPreferenceValue (&(pMultiData->i4_SLongValue)));
}

INT4
FsRipPreferenceValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRipPreferenceValue (pMultiData->i4_SLongValue));
}

INT4
FsRipPreferenceValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRipPreferenceValue
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRipPreferenceValueDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRipPreferenceValue
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
