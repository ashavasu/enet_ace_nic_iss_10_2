/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: ripred.c,v 1.24 2017/09/20 13:08:00 siva Exp $
 *
 * Description: This file contains RIP Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __RIPRED_C
#define __RIPRED_C

#include "ripinc.h"

tDbOffsetTemplate   gaRipRtOffsetTbl[] =
    { {(FSAP_OFFSETOF (tRipRtInfo, u4TempDestNet) -
        FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, u4TempDestMask) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, u4TempTos) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, u4TempNextHop) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, u4TempRtIfIndx) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, u4TempGw) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, u4TempRtNxtHopAS) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, i4TempMetric1) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipRtInfo, i4CxtId) -
  FSAP_OFFSETOF (tRipRtInfo, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{-1, 0}
};

tDbOffsetTemplate   gaRipPeerOffsetTbl[] =
    { {(FSAP_OFFSETOF (tRipPeerRec, u4PeerAddr) -
        FSAP_OFFSETOF (tRipPeerRec, PeerDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipPeerRec, u2IfIndex) -
  FSAP_OFFSETOF (tRipPeerRec, PeerDbNode) - sizeof (tDbTblNode)), 2}
,
{(FSAP_OFFSETOF (tRipPeerRec, au1PeerRouteTag) -
  FSAP_OFFSETOF (tRipPeerRec, PeerDbNode) - sizeof (tDbTblNode)), 2}
,
{(FSAP_OFFSETOF (tRipPeerRec, u4RecvSeqNo) -
  FSAP_OFFSETOF (tRipPeerRec, PeerDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRipPeerRec, u2PeerVersion) -
  FSAP_OFFSETOF (tRipPeerRec, PeerDbNode) - sizeof (tDbTblNode)), 2}
,
{-1, 0}
};

#ifdef L2RED_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize rip dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pRipRtDynDataDesc = NULL;
    tDbDataDescInfo    *pRipPeerDynDataDesc = NULL;

    pRipRtDynDataDesc = &(gaRipDynDataDescList[RIP_RED_DYN_RT_INFO]);

    pRipRtDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pRipRtDynDataDesc->u4DbDataSize =
        sizeof (tRipRtInfo) - FSAP_OFFSETOF (tRipRtInfo, u4TempDestNet);

    pRipRtDynDataDesc->pDbDataOffsetTbl = gaRipRtOffsetTbl;

    pRipPeerDynDataDesc = &(gaRipDynDataDescList[RIP_RED_DYN_PEER_INFO]);

    pRipPeerDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pRipPeerDynDataDesc->u4DbDataSize =
        sizeof (tRipPeerRec) - FSAP_OFFSETOF (tRipPeerRec, u4PeerAddr);

    pRipPeerDynDataDesc->pDbDataOffsetTbl = gaRipPeerOffsetTbl;
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Rip descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDescrTblInit (VOID)
{
    tDbDescrParams      RipDescrParams;
    MEMSET (&RipDescrParams, RIP_ZERO, sizeof (tDbDescrParams));

    RipDescrParams.u4ModuleId = RM_RIP_APP_ID;

    RipDescrParams.pDbDataDescList = gaRipDynDataDescList;

    DbUtilTblInit (&gRipDynInfoList, &RipDescrParams);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Rip Oper db node. */
/*                                                                           */
/*    Input(s)            : pRipDbTblNode - pointer to Rip Db Entry.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDbNodeInit (tDbTblNode * pDBNode, UINT4 u4Type)
{
    DbUtilNodeInit (pDBNode, u4Type);
    return;
}

/************************************************************************
 *  Function Name   : RipRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the RIP module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
RipRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedInitGlobalInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedInitGlobalInfo"));

    /* Create the RM Packet arrival Q */

    if (OsixQueCrt ((UINT1 *) RIP_RM_PKT_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    RIP_RM_QUE_DEPTH, &gRipRmPktQId) != OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME, "INIT: Queue Creation Failed\n");
        return OSIX_FAILURE;
    }

    MEMSET (&RmRegParams, RIP_ZERO, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_RIP_APP_ID;
    RmRegParams.pFnRcvPkt = RipRedRmCallBack;

    /* Registering RIP with RM */
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedInitGlobalInfo: Registration with RM failed\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedInitGlobalInfo: Registration with RM failed"));
        return OSIX_FAILURE;
    }
    RIP_GET_NODE_STATUS () = RM_INIT;
    RIP_NUM_STANDBY_NODES () = RIP_ZERO;
    gRipRedGblInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedInitGlobalInfo"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedInitGlobalInfo\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : RipRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the RIP module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register RIP with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
RipRedDeInitGlobalInfo (VOID)
{
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedDeInitGlobalInfo\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedDeInitGlobalInfo"));
    /* Deregister from SYSLOG */

    if (gRipRmPktQId != 0)
    {
        OsixQueDel (gRipRmPktQId);
        gRipRmPktQId = IP_ZERO;
    }

    if (RmDeRegisterProtocols (RM_RIP_APP_ID) == RM_FAILURE)
    {

        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedDeInitGlobalInfo: De-Registration with RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedDeInitGlobalInfo: De-Registration with RM failed"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedDeInitGlobalInfo"));
    SYS_LOG_DEREGISTER (gu4RipSysLogId);
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedDeInitGlobalInfo\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RIP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RipRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tRipRmMsg          *pMsg = NULL;
    UINT1               u1Flag = RIP_PROCESS_ONE_MSG;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedRmCallBack\r \n");

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedRmCallBack:This event is not associated with RM \r\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedRmCallBack: Queue Message associated with the event "
                            "is not sent by RM \r\n");
        return OSIX_FAILURE;
    }

    pMsg = (tRipRmMsg *) MemAllocMemBlk ((tMemPoolId) IpRipMemory.RMMemPoolId);
    if (pMsg == NULL)
    {
        RmApiSetNoLock ();
        RipLock ();
        RipRedHandleRmEvents (u1Flag);
        RipUnLock ();
        RmApiSetLock ();
        pMsg = (tRipRmMsg *)
            MemAllocMemBlk ((tMemPoolId) IpRipMemory.RMMemPoolId);
    }

    if (pMsg != NULL)
    {
        MEMSET (pMsg, RIP_ZERO, sizeof (tRipRmMsg));

        pMsg->RmCtrlMsg.pData = pData;
        pMsg->RmCtrlMsg.u1Event = u1Event;
        pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

        /* Send the message associated with the event to
         * RIP module in a queue */
        if (OsixQueSend (gRipRmPktQId,
                         (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            /* Send queue will not fail, as we processed the queue message
             * and the queue is free.
             * Even then if the queue send fails, it is critical.
             * It is reported to the syslog manager. 
             */
            MemReleaseMemBlock ((tMemPoolId) IpRipMemory.RMMemPoolId,
                                (UINT1 *) pMsg);

            if (u1Event == RM_MESSAGE)
            {
                /* RM CRU Buffer Memory Release */
                RM_FREE (pData);
            }
            else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
            {
                /* Call RM API to release memory of data of these events */
                RipRedRmReleaseMemoryForMsg ((UINT1 *) pData);
            }

            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                ALL_FAILURE_TRC,
                                RIP_NAME,
                                "RipRedRmCallBack: Q send failure\r \n");
            return OSIX_FAILURE;
        }

    }

    /* Post a event to RIP to process RM events */
    OsixSendEvent (RIP_TASK_NODE_ID, RIP_TASK_NAME, RIP_RM_PKT_EVENT);

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedRmCallBack\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : RipRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the RIP module to 
 *                      process all the events and messages posted by 
 *                      the RM module.
 *                      In case if it is called by the RM thread,
 *                      then process only one message from the queue
 *                      and post the message to the queue. 
 *                      If the function is called from the RIP thread
 *                      then all the messages are processed from the queue                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the RIP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
RipRedHandleRmEvents (UINT1 u1Flag)
{
    tRipRmMsg          *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = RIP_ZERO;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleRmEvents\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleRmEvents"));
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    do
    {
        if (OsixQueRecv (gRipRmPktQId,
                         (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_FAILURE)
        {
            break;
        }

        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    CONTROL_PLANE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents:Received GO_ACTIVE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents: Received GO_ACTIVE"
                              " event"));
                RipRedHandleGoActive ();
                break;
            case GO_STANDBY:
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    CONTROL_PLANE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents:Received GO_STANDBY event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents:Received GO_STANDBY event"));
                RipRedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;
            case RM_STANDBY_UP:
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    CONTROL_PLANE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents: Received RM_STANDBY_UP"
                                    " event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents: Received RM_STANDBY_UP"
                              " event"));
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RIP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                RipRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (RIP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    RIP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_NOT_STARTED;
                    RipRedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    CONTROL_PLANE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents: Received RM_STANDBY_DOWN "
                                    "event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents: Received RM_STANDBY_DOWN "
                              "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RIP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                RipRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    CONTROL_PLANE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents:Received RM_MESSAGE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents:Received RM_MESSAGE event"));
                ProtoAck.u4AppId = RM_RIP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gRipRedGblInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    RipRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                  pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gRipRedGblInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    RipRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                        RIP_TRC_GLOBAL_FLAG,
                                        RIP_INVALID_CXT_ID,
                                        CONTROL_PLANE_TRC,
                                        RIP_NAME,
                                        "RipRedHandleRmEvents: Sync-up message received"
                                        " at Idle Node!!!!\r\n");
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                                  "RipRedHandleRmEvents: Sync-up message "
                                  "received at Idle Node"));
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the RIP status is changed 
                 * from idle to standby */
                if (gRipRedGblInfo.u1NodeStatus == RM_INIT)
                {
                    if (RIP_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        RipRedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case RIP_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    CONTROL_PLANE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents: Received "
                                    "L2_INITIATE_BULK_UPDATES \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents: Received "
                              "L2_INITIATE_BULK_UPDATES"));
                RipRedSendBulkReqMsg ();
                break;
            case RM_DYNAMIC_SYNCH_AUDIT:
                RipRedHandleDynSyncAudit ();
                break;
            default:
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    ALL_FAILURE_TRC,
                                    RIP_NAME,
                                    "RipRedHandleRmEvents:Invalid RM event received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                              "RipRedHandleRmEvents: Invalid RM event received"));
                break;

        }
        MemReleaseMemBlock (IpRipMemory.RMMemPoolId, (UINT1 *) pMsg);
    }
    while (u1Flag);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleRmEvents"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleRmEvents\r \n");
    return;
}

/************************************************************************
 * Function Name      : RipRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the RIP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
RipRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleGoActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleGoActive"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_NOT_STARTED;

    if (RIP_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedHandleGoActive: GO_ACTIVE event reached when node "
                            "is already active \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                      "RipRedHandleGoActive: GO_ACTIVE event reached when "
                      "node is already active"));
        return;
    }
    else if (RIP_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedHandleGoActive: Idle to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                      "RipRedHandleGoActive: Idle to Active transition"));

        RipRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (RIP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedHandleGoActive: Standby to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                      "RipRedHandleGoActive: Standby to Active transition"));
        RipRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (RIP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        RIP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_NOT_STARTED;
        RipRedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleGoActive"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleGoActive\r \n");
    return;
}

/************************************************************************
 * Function Name      : RipRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the RIP upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
RipRedHandleGoStandby (tRipRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleGoStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleGoStandby"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    MemReleaseMemBlock (IpRipMemory.RMMemPoolId, (UINT1 *) pMsg);
    gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_NOT_STARTED;

    if (RIP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedHandleGoStandby: GO_STANDBY event reached when "
                            "node is already in standby \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                      "RipRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already in standby"));
        return;
    }
    if (RIP_GET_NODE_STATUS () == RM_INIT)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedHandleGoStandby: GO_STANDBY event reached when node "
                            "is already idle \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                      "RipRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already idle"));

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedHandleGoStandby: Active to Standby transition..\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                      "RipRedHandleGoStandby: Active to Standby transition"));
        RipRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleGoStandby"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleGoStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleIdleToActive (VOID)
{
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleIdleToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleIdleToActive"));

    /* Node status is set to active and the number of standby nodes 
     * are updated */
    RIP_GET_NODE_STATUS () = RM_ACTIVE;
    RIP_RM_GET_NUM_STANDBY_NODES_UP ();

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleIdleToActive"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleIdleToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleIdleToStandby (VOID)
{
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleIdleToStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleIdleToStandby"));

    /* the node status is set to standby and no of standby nodes is set to 0 */
    RIP_GET_NODE_STATUS () = RM_STANDBY;
    RIP_NUM_STANDBY_NODES () = RIP_ZERO;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME,
                        "RipRedHandleIdleToStandby: Node Status Idle to Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "RipRedHandleIdleToStandby: Node Status Idle to Standby"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleIdleToStandby"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleIdleToStandby\r \n");

    return;
}

/************************************************************************/
/* Function Name      : RipRedStartTimers                               */
/*                                                                      */
/* Description        : This is a will start all the necessary timers   */
/*                      for all the RIP instances. It will walk        */
/*                      array and start the timers. If the RIP state   */
/*                      is Master, then Advertisement timer is started. */
/*                      If the state is Backup, then Master-Down timer  */
/*                      is started.                                     */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP_SUCCESS/RIP_FAILURE                         */
/************************************************************************/

PUBLIC INT4
RipRedStartTimers ()
{
    tRipCxt            *pRipCxtEntry = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipRtEntry        *pRtEntry = NULL;
    tTMO_DLL_NODE      *pListNode = NULL;
    UINT4               u4Interval = RIP_ZERO;
    UINT4               u4HashIndex = RIP_ZERO;
    INT4                i4RipCxtId = RIP_ZERO;
    INT4                i4NextRipCxtId = RIP_ZERO;
    UINT2               u2If = RIP_ZERO;
    INT2                i2OffSet = RIP_ZERO;
    INT4                i4RipGetNextCxtId;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedStartTimers\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedStartTimers"));

    /* Sending Update message on all the interface */

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            u2If = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

            pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2If);
            if (pRipCxtEntry == NULL)
            {
                continue;
            }

            /* If the interface is configured as a passive interface,
             * then start the Passive update timer on that interface */

            if ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_PASSIVE) &&
                (pRipCxtEntry->u4NumOfPassNeighbors == RIP_ZERO))
            {
                pRipCxtEntry->RipPassiveUpdTimer.u1Id = RIP_PASS_UPD_TIMER_ID;
                RIP_START_TIMER (RIP_TIMER_ID,
                                 &((pRipCxtEntry->RipPassiveUpdTimer).
                                   Timer_node), RIP_DEF_UPDATE_INTERVAL);
                continue;
            }

            /*
             * Send initial request on all the interface and 
             * send an update over this interface, when we are globally
             * active, admin, oper status active in this interface.
             */
            rip_if_enable (pRipIfRec);
            RipSendInitialRequest (u2If, pRipIfRec->RipIfaceCfg.u4SrcAddr,
                                   pRipCxtEntry);
            if (RIP_IS_ACTIVE (pRipIfRec))
            {
                rip_generate_update_message (RIP_VERSION_ANY,
                                             RIP_REG_UPDATE, IP_GEN_BCAST_ADDR,
                                             (UINT2) UDP_RIP_PORT, u2If,
                                             pRipCxtEntry);
            }

            u4Interval =
                RipGetJitterVal (pRipIfRec->RipIfaceCfg.u2UpdateInterval,
                                 RIP_JITTER);
            RIP_START_TIMER (RIP_TIMER_ID,
                             &(pRipIfRec->RipUpdateTimer.TimerNode),
                             u4Interval);
        }
    }

    RipGetFirstCxtId (&i4RipCxtId);

    while (i4RipCxtId != (INT4) RIP_INVALID_CXT_ID)
    {
        pRipCxtEntry = RipGetCxtInfoRec (i4RipCxtId);

        /* Start the triggered update timer */
        pRipCxtEntry->RipTrigUpdTimer.u1Id = RIP_TRIG_UPD_TIMER_ID;

        RIP_START_TIMER (RIP_TIMER_ID,
                         &((pRipCxtEntry->RipTrigUpdTimer).Timer_node),
                         RIP_MIN_TRIG_UPD_TIME);

        /* Update the global trig on status flag */

        pRipCxtEntry->RipGblCfg.u1TrigUpdFlag = RIP_TRIG_TIMER_ACTIVE;
        pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_DESIRED;

        TMO_DLL_Scan (&(pRipCxtEntry->RipCxtRtList), pListNode, tTMO_DLL_NODE *)
        {
            i2OffSet = (INT2) RIP_OFFSET (tRipRtEntry, RipCxtRtNode);
            pRtEntry = RIP_ROUTE_INFO_PTR_FROM_RTLST (pListNode, i2OffSet);
            pRipIfRec = RipGetIfRec (pRtEntry->RtInfo.u4RtIfIndx, pRipCxtEntry);
            if (pRipIfRec == NULL)
            {
                continue;
            }
            while (pRtEntry != NULL)
            {
                /* If the route is un reachable then start the garbage 
                 * collection timer.
                 * If the route is reachable and the interface is not a 
                 * passive interface then start update timer on that 
                 * interface 
                 */
                if (pRtEntry->RtInfo.i4Metric1 == RIP_INFINITY)
                {
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &(RIP_ROUTE_TIMER_NODE (pRtEntry)),
                                     pRipIfRec->RipIfaceCfg.
                                     u2GarbageCollectionInterval);
                }
                else
                {
                    if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag ==
                        RIP_ADMIN_PASSIVE)
                    {
                        pRtEntry = pRtEntry->pNextAlternatepath;
                        continue;
                    }
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &(RIP_ROUTE_TIMER_NODE (pRtEntry)),
                                     pRipIfRec->RipIfaceCfg.u2RouteAgeInterval);
                }
                pRtEntry = pRtEntry->pNextAlternatepath;
            }
        }
        i4RipGetNextCxtId = RipGetNextCxtId (i4RipCxtId, &i4NextRipCxtId);
        UNUSED_PARAM (i4RipGetNextCxtId);
        i4RipCxtId = i4NextRipCxtId;
    }
    return RIP_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedNotifyRestartRTM                          */
/*                                                                      */
/* Description        : This function will notify RTM to mark all RIP   */
/*                      routes as STALE. It will send an event to RTN   */
/*                      for this. On receiving the event, RTM will      */
/*                      mark all the routes as stale and start a timer  */
/*                      for Timeout value. On the expiry of this timer  */
/*                      if there are any stale routes still present     */
/*                      those routes are deleted from the RTM and also  */
/*                      from the hardware.                              */
/*                                                                      */
/* Input(s)           : u4CxtId - Context ID                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP_SUCCESS/RIP_FAILURE                         */
/************************************************************************/

INT4
RipRedNotifyRestartRTM (UINT4 u4CxtId)
{
    tOsixMsg           *pRtmMsg = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;
    UINT4               u4RestartTime = RIP_DEF_ROUTE_AGE;

    /* Notifies to RTMv4 about RIP restart with restart interval */
    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtmMsgHdr) + sizeof (UINT4)), RIP_ZERO)) == NULL)

    {
        return RIP_FAILURE;
    }
    MEMSET (pRtmMsg->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pRtmMsg, "RipRedNtfRTM");

    pRtmMsgHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_GR_NOTIFY_MSG;
    pRtmMsgHdr->RegnId.u2ProtoId = (UINT2) RIP_ID;
    pRtmMsgHdr->RegnId.u4ContextId = u4CxtId;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP_COPY_TO_BUF (pRtmMsg, &u4RestartTime,
                        RIP_ZERO, sizeof (UINT4)) == IP_BUF_FAILURE)
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return RIP_FAILURE;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) != IP_SUCCESS)
    {
        IP_RELEASE_BUF (pRtmMsg, RIP_ZERO);
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleStandbyToActive (VOID)
{
    INT4                i4RipCxtId = RIP_ZERO;
    INT4                i4NextRipCxtId = RIP_ZERO;
    INT4                i4RipGetNextCxtId;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleStandbyToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleStandbyToActive"));

    RIP_GET_NODE_STATUS () = RM_ACTIVE;
    RIP_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Timers to be started, check the dynamic arp entry. 
     * If in pending/ageout state, then start the arp age out 
     * timer and send the arp request. If in dynamic state,
     * start the arp cache timer. */

    RipGetFirstCxtId (&i4RipCxtId);

    while (i4RipCxtId != (INT4) RIP_INVALID_CXT_ID)
    {
        RipRedNotifyRestartRTM ((UINT4) i4RipCxtId);
        i4RipGetNextCxtId = RipGetNextCxtId (i4RipCxtId, &i4NextRipCxtId);
        UNUSED_PARAM (i4RipGetNextCxtId);
        i4RipCxtId = i4NextRipCxtId;
    }
    OsixSendEvent (RIP_TASK_NODE_ID, RIP_TASK_NAME, RIP_RM_START_TIMER_EVENT);

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME,
                        "RipRedHandleStandbyToActive: Node Status Standby to "
                        "Active\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "RipRedHandleStandbyToActive: Node Status Standby to Active"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleStandbyToActive"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleStandbyToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleActiveToStandby (VOID)
{
    INT4                i4RipCxtId = RIP_ZERO;
    INT4                i4NextRipCxtId = RIP_ZERO;
    tRipCxt            *pRipCxtEntry = NULL;
    INT4                i4RipGetNextCxtId;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedHandleActiveToStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedHandleActiveToStandby"));

    /*update the statistics */
    RIP_GET_NODE_STATUS () = RM_STANDBY;
    RIP_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Stop the running timers in the active node. Check whether any timers are 
     * running, if running, stop those timers. If there are any messages in the 
     * queue, discard those messages. Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */

    RipGetFirstCxtId (&i4RipCxtId);

    pRipCxtEntry = RipGetCxtInfoRec (i4RipCxtId);
    if (pRipCxtEntry != NULL)
    {
        if (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_ENABLE)
        {

            while (i4RipCxtId != (INT4) RIP_INVALID_CXT_ID)
            {
                RipRedStopTimersOnStandby (pRipCxtEntry);

                i4RipGetNextCxtId =
                    RipGetNextCxtId (i4RipCxtId, &i4NextRipCxtId);
                i4RipCxtId = i4NextRipCxtId;
                pRipCxtEntry = RipGetCxtInfoRec (i4RipCxtId);
            }

        }
    }
    UNUSED_PARAM (i4RipGetNextCxtId);

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME,
                        "RipRedHandleActiveToStandby: Node Status Active to "
                        "Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "RipRedHandleActiveToStandby: Node Status Active to Standby"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedHandleActiveToStandby"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedHandleActiveToStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP_ZERO;
    UINT2               u2Length = RIP_ZERO;
    UINT1               u1MsgType = RIP_ZERO;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedProcessPeerMsgAtActive\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedProcessPeerMsgAtActive"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;

    RIP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    RIP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != RIP_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == RIP_RED_BULK_REQ_MESSAGE)
    {
        gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_NOT_STARTED;
        if (!RIP_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gRipRedGblInfo.bBulkReqRcvd = OSIX_TRUE;

            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                CONTROL_PLANE_TRC,
                                RIP_NAME,
                                "RipRedProcessPeerMsgAtActive:Bulk request message "
                                "before RM_STANDBY_UP\r \n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                          "RipRedProcessPeerMsgAtActive:Bulk request message "
                          "before RM_STANDBY_UP"));
            return;
        }
        RipRedSendBulkUpdMsg ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedProcessPeerMsgAtActive"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedProcessPeerMsgAtActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP_ZERO;
    UINT2               u2Length = RIP_ZERO;
    UINT2               u2ExtractMsgLen = RIP_ZERO;
    UINT2               u2MinLen = RIP_ZERO;
    UINT1               u1MsgType = RIP_ZERO;
    INT4                i4RipContextId = RIP_ZERO;
    tRipCxt            *pRipCxtEntry = NULL;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME,
                        "Entering RipRedProcessPeerMsgAtStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedProcessPeerMsgAtStandby"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    u2MinLen = RIP_RED_TYPE_FIELD_SIZE + RIP_RED_LEN_FIELD_SIZE;

    while (u4OffSet < u2DataLen)
    {
        u2ExtractMsgLen = (UINT2) u4OffSet;
        RIP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

        RIP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u4OffSet += u2Length;
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen = (UINT2) (u2ExtractMsgLen + u2Length);

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given 
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            RmApiHandleProtocolEvent (&ProtoEvt);
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                ALL_FAILURE_TRC,
                                RIP_NAME,
                                "RipRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                          "RipRedProcessPeerMsgAtStandby:RM_PROCESS Failure"));
            return;
        }

        switch (u1MsgType)
        {
            case RIP_RED_DYN_BULK_TAIL:
                RipRedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;
            case RIP_RED_DYN_RT_INFO:
                RipRedProcessDynamicRtInfo (pMsg, &u4OffSet);
                break;
            case RIP_RED_DYN_PEER_INFO:
                RipRedProcessDynamicPeerInfo (pMsg, &u4OffSet);
                break;
            case RIP_RED_DYN_BULK_CXT:
                pRipCxtEntry = RipGetCxtInfoRec (i4RipContextId);
                if (pRipCxtEntry != NULL)
                {
                    RipDelCxtEntry (pRipCxtEntry);
                }
                break;
            default:
                break;
        }
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedProcessPeerMsgAtStandby"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedProcessPeerMsgAtStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the RIP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RipRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedRmReleaseMemoryForMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedRmReleaseMemoryForMsg"));
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {

        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedRmReleaseMemoryForMsg:Failure in releasing allocated"
                            " memory\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedRmReleaseMemoryForMsg:Failure in releasing "
                      "allocated memory"));
        return OSIX_FAILURE;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedRmReleaseMemoryForMsg"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedRmReleaseMemoryForMsg\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RipRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = RIP_ZERO;
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedSendMsgToRm\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedSendMsgToRm"));
    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_RIP_APP_ID, RM_RIP_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendMsgToRm:Freememory due to message send "
                            "failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendMsgToRm:Freememory due to message send"
                      " failure"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedSendMsgToRm"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedSendMsgToRm\r \n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RipRmEnqChkSumMsgToRm                               */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RIP_SUCCESS/RIP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RipRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef L2RED_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return RIP_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return RIP_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = RIP_ZERO;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedSendBulkReqMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedSendBulkReqMsg"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RIP Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RIP_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkReqMsg: RM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkReqMsg: RM Memory allocation failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    RIP_RM_PUT_1_BYTE (pMsg, &u4OffSet, RIP_RED_BULK_REQ_MESSAGE);
    RIP_RM_PUT_2_BYTE (pMsg, &u4OffSet, RIP_RED_BULK_REQ_MSG_SIZE);

    if (RipRedSendMsgToRm (pMsg, (UINT2) u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkReqMsg: Send message to RM failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkReqMsg: Send message to RM failed"));
        return;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedSendBulkReqMsg"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedSendBulkReqMsg\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : RipRedHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RipRedHandleDynSyncAudit ()
{
    /*On receiving this event, Rip should execute show cmd and calculate checksum */
    RipExecuteCmdAndCalculateChkSum ();
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDbUtilAddRtInfo                            */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic route */
/*                          info after copying the information               */
/*                                                                           */
/*    Input(s)            : pRipRtInfo - Rip Route information             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDbUtilAddRtInfo (tRipRtEntry * pRipRtInfo)
{
    if (pRipRtInfo->RtInfo.u2RtProto == CIDR_LOCAL_ID)
    {
        pRipRtInfo->RtInfo.u4TempDestNet = pRipRtInfo->RtInfo.u4DestNet;
        pRipRtInfo->RtInfo.u4TempDestMask = pRipRtInfo->RtInfo.u4DestMask;
    }
    pRipRtInfo->RtInfo.u4TempTos = pRipRtInfo->RtInfo.u4Tos;
    pRipRtInfo->RtInfo.u4TempNextHop = pRipRtInfo->RtInfo.u4NextHop;
    pRipRtInfo->RtInfo.u4TempRtIfIndx = pRipRtInfo->RtInfo.u4RtIfIndx;
    pRipRtInfo->RtInfo.u4TempGw = pRipRtInfo->RtInfo.u4Gw;
    pRipRtInfo->RtInfo.u4TempRtNxtHopAS = pRipRtInfo->RtInfo.u4RtNxtHopAS;
    pRipRtInfo->RtInfo.i4TempMetric1 = pRipRtInfo->RtInfo.i4Metric1;

#ifdef RIP_FT_TEST_WANTED
    if (gi4DynUpdTestStatus == RIP_TWO)
    {
        /* Skiping the dynamic update */
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            CONTROL_PLANE_TRC,
                            RIP_NAME,
                            "RipRedDbUtilAddRtInfo: Skiping the dynamic update\r\n");
        return;
    }
#endif
    RipRedDbUtilAddTblNode (&gRipDynInfoList,
                            &(pRipRtInfo->RtInfo.RouteDbNode));
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pRipDataDesc - This is Rip sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pRipDbNode - This is db node defined in the RIP*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDbUtilAddTblNode (tDbTblDescriptor * pRipDataDesc,
                        tDbTblNode * pRipDbNode)
{
    if ((RIP_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gRipRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pRipDataDesc, pRipDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate RIP dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedSyncDynInfo (VOID)
{
    if ((RIP_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gRipRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleNodes (&gRipDynInfoList);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedAddPeerAllNodeInDbTbl                      */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
RipRedAddAllPeerNodeInDbTbl (VOID)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            /* Scan the list of peer list for the specified interface */
            RIP_SLL_Scan (&(pRipIfRec->RipPeerList), pRipCurPeerRec,
                          tRipPeerRec *)
            {
                RipRedDbUtilAddTblNode (&gRipDynInfoList,
                                        &(pRipCurPeerRec->PeerDbNode));
            }
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedSendBulkDefCxtInfo                         */
/*                                                                           */
/*    Description         : This function will initiate bulk syncup in the   */
/*                          scenario when default context is deleted         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
RipRedSendBulkDefCxtInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP_ZERO;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedSendBulkDefCxtInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedSendBulkDefCxtInfo"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_IN_PROGRESS;
    if ((pMsg = RM_ALLOC_TX_BUF (RIP_RED_BULK_UPD_CXT_MSG_SIZE)) == NULL)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkDefCxtInfo: RM Memory allocation"
                            " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkDefCxtInfo: RM Memory allocation "
                      "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u4OffSet = RIP_ZERO;

    RIP_RM_PUT_1_BYTE (pMsg, &u4OffSet, RIP_RED_DYN_BULK_CXT);
    RIP_RM_PUT_2_BYTE (pMsg, &u4OffSet, (UINT2) RIP_RED_BULK_UPD_CXT_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (RipRedSendMsgToRm (pMsg, (UINT2) u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkDefCxtInfo:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkDefCxtInfo:Send message to RM failed"));
        return;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedSendBulkDefCxtInfo"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedSendBulkDefCxtInfo\r \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedAddRouteAllNodeInDbTbl                     */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedAddAllRouteNodeInDbTbl (VOID)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtMarker       *pRipRtMarker = NULL;
    tRipRtEntry        *pRt = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    tRipRtInfo         *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    UINT4               u4RtCount = RIP_ZERO;
    UINT4               au4Indx[2];
    INT4                i4Entry = RIP_ZERO;
    INT4                i4RipContextId = RIP_ZERO;
    INT4                i4NextRipCxtId = RIP_ZERO;
    INT4                i4OutCome = RIP_ZERO;
    INT4                i4RipGetNextCxtId;
#ifdef RIP_FT_TEST_WANTED
    INT4               *pDummy = NULL;
#endif

    pRipRtMarker = &(gRipRedRtMarker);
    i4RipContextId = pRipRtMarker->i4CxtId;

    if (i4RipContextId < 0)
    {
        gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_COMPLETED;
        RipRedSendBulkUpdTailMsg ();
        return;
    }

    pRipCxtEntry = RipGetCxtInfoRec (i4RipContextId);
    if (pRipCxtEntry == NULL)
    {
        gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_COMPLETED;
        RipRedSendBulkUpdTailMsg ();
        return;
    }

    gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_IN_PROGRESS;

    au4Indx[0] = pRipRtMarker->u4DstAddr;
    au4Indx[1] = pRipRtMarker->u4DstMask;
    while (i4RipContextId != (INT4) RIP_INVALID_CXT_ID)
    {
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
        OutParams.Key.pKey = NULL;

        i4OutCome = TRIE_SUCCESS;
        while (i4OutCome == TRIE_SUCCESS)
        {
            /* Get the next entry from the rip database */
            InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
            MEMSET (apAppSpecInfo, RIP_ZERO,
                    MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
            i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
            if (i4OutCome == TRIE_FAILURE)
            {
                break;
            }

            /* Traverse the route entry for any rip routes or symmary routes.
             * The traversal is done in the opposite direction, because
             * the summary route will not be added before a rip route or a 
             * static route is already present. So first the rip route is
             * added to the db node and then the summary route is added */

            for (i4Entry = (MAX_ROUTING_PROTOCOLS - 1); i4Entry >= 0; i4Entry--)
            {
                pRt = NULL;
                if (((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4Entry] !=
                    NULL)
                {
                    (pRt) =
                        ((tRipRtEntry **) (OutParams.pAppSpecInfo))[i4Entry];
                    au4Indx[0] = RIP_HTONL (pRt->RtInfo.u4DestNet);
                    au4Indx[1] = RIP_HTONL (pRt->RtInfo.u4DestMask);
                    if ((i4Entry != (OTHERS_ID - 1)) &&
                        (i4Entry != (RIP_ID - 1)))
                    {
                        continue;
                    }
                }
                while (pRt != NULL)
                {
                    /* If the number of routes is greater than 
                     * MAX_RIP_SYNCUP_ROUTES, then an event is sent to RIP 
                     * to send the remaining routes in the next bunch. This is
                     * done here to avoid a tight loop while sending bulk 
                     * update.
                     */
                    if (u4RtCount < MAX_RIP_SYNCUP_ROUTES)
                    {
                        pRt->RtInfo.i4CxtId = i4RipContextId;
                        RipRedDbUtilAddRtInfo (pRt);
                        u4RtCount += 1;
                        pRipRtMarker->i4CxtId = i4RipContextId;
                        pRipRtMarker->u4DstAddr =
                            RIP_HTONL (pRt->RtInfo.u4TempDestNet);
                        pRipRtMarker->u4DstMask =
                            RIP_HTONL (pRt->RtInfo.u4TempDestMask);
                    }
                    else
                    {
                        RipRedSyncDynInfo ();
                        OsixSendEvent (RIP_TASK_NODE_ID, RIP_TASK_NAME,
                                       RIP_RM_PEND_RT_SYNC_EVENT);
                        return;
                    }
                    pRt = pRt->pNextAlternatepath;
                }
            }
        }

        au4Indx[0] = RIP_ZERO;
        au4Indx[1] = RIP_ZERO;
        MEMSET (&OutParams, 0, sizeof (tOutputParams));
        i4RipGetNextCxtId = RipGetNextCxtId (i4RipContextId, &i4NextRipCxtId);
        UNUSED_PARAM (i4RipGetNextCxtId);
        i4RipContextId = i4NextRipCxtId;
        pRipCxtEntry = RipGetCxtInfoRec (i4RipContextId);
        if (pRipCxtEntry == NULL)
        {
            break;
        }
    }

    if (u4RtCount != 0)
    {
#ifdef RIP_FT_TEST_WANTED
        if (gi4BulkUpdTestStatus == RIP_TWO)
        {
            /* CRASHING */
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                CONTROL_PLANE_TRC,
                                RIP_NAME,
                                "EXE Crashing during Bulk Update\r\n");
            *pDummy = 10;
        }
#endif
        RipRedSyncDynInfo ();
    }

    gRipRedGblInfo.u1BulkUpdStatus = RIP_HA_UPD_COMPLETED;
    RipRedSendBulkUpdTailMsg ();
    MEMSET (&gRipRedRtMarker, 0, sizeof (tRipRtMarker));
    RipGetFirstCxtId (&gRipRedRtMarker.i4CxtId);

    return;
}

/************************************************************************/
/* Function Name      : RipRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
PUBLIC VOID
RipRedSendBulkUpdMsg (VOID)
{

    INT4                i4RipContextId = RIP_ZERO;
    tRipCxt            *pRipCxtEntry = NULL;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedSendBulkUpdMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedSendBulkUpdMsg"));

    pRipCxtEntry = RipGetCxtInfoRec (i4RipContextId);

    if (RIP_IS_STANDBY_UP () == OSIX_FALSE)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkUpMsg:RIP Standby up failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkUpMsg:RIP stand by up failure"));
        return;
    }

    if (gRipRedGblInfo.u1BulkUpdStatus == RIP_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        MEMSET (&gRipRedRtMarker, RIP_ZERO, sizeof (tRipRtMarker));
        RipGetFirstCxtId (&gRipRedRtMarker.i4CxtId);
        RipRedAddAllPeerNodeInDbTbl ();
        if (pRipCxtEntry == NULL)
        {
            RipRedSendBulkDefCxtInfo ();
        }
        RipRedAddAllRouteNodeInDbTbl ();

    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedSendBulkUpdMsg"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedSendBulkUpdMsg\r \n");
    return;

}

/************************************************************************/
/* Function Name      : RipRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RIP offers an IP address         */
/*                      to the RIP client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRipBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
RipRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP_ZERO;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedSendBulkUpdTailMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedSendBulkUpdTailMsg"));
    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * RIP_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RIP_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkUpdTailMsg: RM Memory allocation"
                            " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkUpdTailMsg: RM Memory allocation "
                      "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u4OffSet = RIP_ZERO;

    RIP_RM_PUT_1_BYTE (pMsg, &u4OffSet, RIP_RED_DYN_BULK_TAIL);
    RIP_RM_PUT_2_BYTE (pMsg, &u4OffSet, (UINT2) RIP_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (RipRedSendMsgToRm (pMsg, (UINT2) u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RipSysLogId,
                      "RipRedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedSendBulkUpdTailMsg"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedSendBulkUpdTailMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RIP_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedProcessBulkTailMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedProcessBulkTailMsg"));

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        ALL_FAILURE_TRC,
                        RIP_NAME,
                        "RipRedProcessBulkTailMsg: Bulk Update Tail Message received"
                        " at Standby node.\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "RipRedProcessBulkTailMsg: Bulk Update Tail Message"
                  " received at Standby node"));

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedProcessBulkTailMsg"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedProcessBulkTailMsg\r \n");

    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessDynamicRtInfo                      */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the route information   */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RipRedProcessDynamicRtInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRipCxt            *pRipCxtEntry = NULL;
    tRipRtEntry        *pRipRt = NULL;
    tRipRtEntry        *pSumRt = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipRtInfo          TempRtInfo;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    UINT4               au4Indx[2];
    UINT4               u4Metric = RIP_ZERO;
    UINT4               u4RipCxtId = RIP_ZERO;
    INT4                i4ReturnValue = RIP_SUCCESS;
    INT4                i4Entry = RIP_ZERO;
    UINT2               u2AsNo = RIP_ZERO;
    UINT1               au1RtTag[2];
    UINT1               u1RipNetworkEnabled = OSIX_FALSE;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedProcessDynamicRtInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedProcessDynamicRtInfo"));

    MEMSET (&TempRtInfo, 0, sizeof (tRipRtInfo));
    MEMSET (au1RtTag, 0, 2);

    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4TempDestNet);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4TempDestMask);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4Tos);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4NextHop);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4RtIfIndx);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4Gw);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4RtNxtHopAS);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Metric);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4RipCxtId);
    RIP_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1Distance);
    RIP_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1Operation);
    RIP_RM_GET_N_BYTE (pMsg, pu4OffSet, TempRtInfo.au1Pad, RIP_TWO);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4SrcAddr);

    TempRtInfo.i4Metric1 = (INT4) u4Metric;
    TempRtInfo.i4CxtId = (INT4) u4RipCxtId;
    if ((TempRtInfo.u1Operation == RIPHA_ADD_ROUTE) ||
        (TempRtInfo.u1Operation == RIPHA_DEL_ROUTE))
    {
        pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) TempRtInfo.u4RtIfIndx);
        if (pRipCxtEntry == NULL)
        {
            return;
        }

        if (TempRtInfo.u4SrcAddr != RIP_ZERO)
        {
            pRipIfRec =
                RipGetIfRecFromAddr (TempRtInfo.u4SrcAddr, pRipCxtEntry);
            u1RipNetworkEnabled = OSIX_TRUE;
        }
        else
        {
            pRipIfRec = RipGetIfRec (TempRtInfo.u4RtIfIndx, pRipCxtEntry);
        }

    }
    else
    {
        pRipCxtEntry = RipGetCxtInfoRec (TempRtInfo.i4CxtId);
        if (pRipCxtEntry == NULL)
        {
            return;
        }
    }

    u2AsNo = (UINT2) TempRtInfo.u4RtNxtHopAS;
    MEMCPY (au1RtTag, &u2AsNo, 2);

    au4Indx[0] = RIP_HTONL (TempRtInfo.u4TempDestNet);
    au4Indx[1] = RIP_HTONL (TempRtInfo.u4TempDestMask);

    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) au4Indx;

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, RIP_ZERO, 16 * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    i4ReturnValue = TrieSearchEntry (&InParams, &OutParams);

    if (i4ReturnValue == TRIE_SUCCESS)
    {
        for (i4Entry = RIP_ZERO; i4Entry < 16; i4Entry++)
        {
            if (apAppSpecInfo[i4Entry] != NULL)
            {
                /* Get the rip route and the summary route */
                if (i4Entry != (OTHERS_ID - 1))
                {
                    pRipRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4Entry];
                    break;
                }
                else if (i4Entry == (OTHERS_ID - 1))
                {
                    pSumRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4Entry];
                }
            }
        }
    }

    if (TempRtInfo.u1Operation == RIPHA_ADD_ROUTE)
    {
        TempRtInfo.u4Gw = TempRtInfo.u4NextHop;
        /* If the route is not present and the metric is infinity,
         * directly add the route entry */
        if ((pRipRt == NULL) && (TempRtInfo.i4Metric1 == RIP_INFINITY))
        {
            RIP_ALLOC_ROUTE_ENTRY (pRipRt);
            if (pRipRt == NULL)
            {
                return;
            }

            RIP_INITIALIZE_ROUTE_DEFAULTS (pRipRt);
            RipRedDbNodeInit (&(pRipRt->RtInfo.RouteDbNode),
                              RIP_RED_DYN_RT_INFO);
            pRipRt->RtInfo.u4DestMask = TempRtInfo.u4TempDestMask;
            pRipRt->RtInfo.u4DestNet = TempRtInfo.u4TempDestNet;
            pRipRt->RtInfo.u4NextHop = TempRtInfo.u4NextHop;
            pRipRt->RtInfo.i4Metric1 = TempRtInfo.i4Metric1;
            pRipRt->RtInfo.u4RtIfIndx = TempRtInfo.u4RtIfIndx;
            pRipRt->RtInfo.u2RtProto = (UINT2) RIP_ID;
            pRipRt->RtInfo.u4TempDestMask = pRipRt->RtInfo.u4DestMask;
            pRipRt->RtInfo.u4TempDestNet = pRipRt->RtInfo.u4DestNet;
            pRipRt->RtInfo.u1Distance = TempRtInfo.u1Distance;
            pRipRt->RtInfo.u4SrcAddr = TempRtInfo.u4SrcAddr;
            pRipRt->u1Preference = TempRtInfo.u1Distance;

            if (rip_add_new_route (pRipRt, pRipCxtEntry) == RIP_FAILURE)
            {
                RIP_ROUTE_FREE (pRipRt);
                return;
            }
            COPY_RT_TAG (pRipRt, au1RtTag);

            OsixGetSysTime (&pRipRt->RtInfo.u4ChgTime);

            pRipRt->RtInfo.u4Gw = TempRtInfo.u4Gw;

            pRipRt->RtInfo.u1Status = (UINT1) RIP_RT_PENDING;
            pRipRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;
        }
        /* If the route entry is already present or
         * if the route entry's metric is not 16 and it is a new route
         * add the route through rip_add_route call */
        else
        {
            rip_add_route (TempRtInfo.u4TempDestNet, au1RtTag,
                           TempRtInfo.u4TempDestMask,
                           (UINT2) TempRtInfo.u4RtIfIndx, TempRtInfo.u4Gw,
                           TempRtInfo.u4NextHop, (UINT4) TempRtInfo.i4Metric1,
                           RIP_RT_INDIRECT, TempRtInfo.u1Distance,
                           TempRtInfo.u4SrcAddr, pRipCxtEntry);
        }
    }
    else if (TempRtInfo.u1Operation == RIPHA_DEL_ROUTE)
    {
        while (pRipRt != NULL)
        {
            if ((pRipRt->RtInfo.u4NextHop == TempRtInfo.u4NextHop) &&
                (pRipRt->RtInfo.u4Gw == TempRtInfo.u4Gw))
            {
                /* Required route exists */
                break;
            }
            pRipRt = pRipRt->pNextAlternatepath;
        }

        if (pRipRt == NULL)
        {
            return;
        }
        RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry, pRipRt);
        RipUpdateIfaceNextRtSend (pRipCxtEntry, pRipRt);
        au4Indx[0] = RIP_HTONL (pRipRt->RtInfo.u4DestNet);
        au4Indx[1] = RIP_HTONL (pRipRt->RtInfo.u4DestMask);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = (INT1) (pRipRt->RtInfo.u2RtProto - 1);

        OutParams.pAppSpecInfo = NULL;
        OutParams.Key.pKey = NULL;
        OutParams.pObject1 = NULL;
        OutParams.pObject2 = NULL;
        /* Delete the route now */

        i4ReturnValue = TrieDeleteEntry (&(InParams), &(OutParams),
                                         TempRtInfo.u4NextHop);

        if (i4ReturnValue == TRIE_SUCCESS)
        {
            if (OutParams.pObject1 != NULL)
            {
                RipMoveLeafToListEnd (OutParams.pObject1, pRipIfRec,
                                      pRipCxtEntry);
            }
            else if (OutParams.pObject2 != NULL)
            {
                RipDeleteLeafFromList (OutParams.pObject2,
                                       (BOOLEAN) OutParams.u1LeafFlag,
                                       pRipIfRec, pRipCxtEntry);
            }
            TMO_DLL_Delete (&(pRipCxtEntry->RipCxtRtList),
                            (tTMO_DLL_NODE *) & pRipRt->RipCxtRtNode);

            if (pRipRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                pRipCxtEntry->u4TotalSummaryCount--;
                RIP_ROUTE_FREE (OutParams.pAppSpecInfo);
            }
            else if ((u1RipNetworkEnabled == OSIX_TRUE)
                     && (pRipRt->RtInfo.u2RtProto == CIDR_LOCAL_ID))
            {
                pRipCxtEntry->u4TotalEntryCount--;
                RIP_FREE_LOCAL_ROUTE_ENTRY (OutParams.pAppSpecInfo);
            }
            else
            {
                pRipCxtEntry->u4TotalEntryCount--;
                RIP_ROUTE_FREE (OutParams.pAppSpecInfo);
            }

        }
    }
    else if (TempRtInfo.u1Operation == RIPHA_ADD_SUM_ROUTE)
    {
        /* If it is a summary route then summary route addition 
         * is called */
        if (pSumRt != NULL)
        {
            return;
        }
        RipAddSummaryRoute (TempRtInfo.u4TempDestNet, pRipCxtEntry);
    }
    else if (TempRtInfo.u1Operation == RIPHA_DEL_SUM_ROUTE)
    {
        /* if it is a summary route deletion, then the summary route is
         * deleted from the data structure */
        if (pSumRt != NULL)
        {
            RipDeleteSummarySinkRt (pSumRt, pRipCxtEntry);
            RipDeleteSummaryRt (pSumRt, pRipCxtEntry);
        }
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedProcessDynamicRtInfo"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedProcessDynamicRtInfo\r \n");
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessDynamicPeerInfo                    */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the peer information    */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RipRedProcessDynamicPeerInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRipCxt            *pRipCxtEntry = NULL;
    tRipPeerRec         TempPeerInfo;

    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Entering RipRedProcessDynamicPeerInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Entering RipRedProcessDynamicPeerInfo"));

    MEMSET (&TempPeerInfo, 0, sizeof (tRipPeerRec));

    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempPeerInfo.u4PeerAddr);
    RIP_RM_GET_2_BYTE (pMsg, pu4OffSet, TempPeerInfo.u2IfIndex);
    RIP_RM_GET_N_BYTE (pMsg, pu4OffSet, TempPeerInfo.au1PeerRouteTag, 2);
    RIP_RM_GET_4_BYTE (pMsg, pu4OffSet, TempPeerInfo.u4RecvSeqNo);
    RIP_RM_GET_2_BYTE (pMsg, pu4OffSet, TempPeerInfo.u2PeerVersion);
    RIP_RM_GET_1_BYTE (pMsg, pu4OffSet, TempPeerInfo.u1KeyIdInUse);
    RIP_RM_GET_1_BYTE (pMsg, pu4OffSet, TempPeerInfo.u1AlignmentByte);

    pRipCxtEntry = RipGetCxtInfoRecFrmIface (TempPeerInfo.u2IfIndex);
    if (pRipCxtEntry == NULL)
    {
        return;
    }
    /* Peer deletion is taken care as part of static sync-up.
     * Peer is deleted only when RIP is disabled on that interface.
     * So peer addition/modificaiton is handled here */

    rip_update_peer_record (TempPeerInfo.u2IfIndex, TempPeerInfo.u4PeerAddr,
                            TempPeerInfo.au1PeerRouteTag,
                            (UINT1) TempPeerInfo.u2PeerVersion,
                            0, 0, TempPeerInfo.u4RecvSeqNo,
                            TempPeerInfo.u1KeyIdInUse, pRipCxtEntry);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4RipSysLogId,
                  "Exiting RipRedProcessDynamicPeerInfo"));
    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                        RIP_TRC_GLOBAL_FLAG,
                        RIP_INVALID_CXT_ID,
                        CONTROL_PLANE_TRC,
                        RIP_NAME, "Exiting RipRedProcessDynamicPeerInfo\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : RipExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RipExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_RIP_APP_ID;
    UINT2               u2ChkSum = 0;

    RipUnLock ();
#ifdef CLI_WANTED
    if (RipGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 CONTROL_PLANE_TRC,
                 RIP_NAME, "Checksum of calculation failed for RIP\n");
        RipLock ();
        return;
    }
#else
    UNUSED_PARAM (u2ChkSum);
#endif

    if (RipRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC, RIP_NAME, "Sending checkum to RM failed\n");
        RipLock ();
        return;
    }
    RipLock ();
    return;
}

/************************************************************************/
/* Function Name      : RipRedStopTimersOnStandby                        */
/*                                                                      */
/* Description        : This function will stop timers for              */
/*                     all rip enabled interfaces on standby           */
/*                                                                      */
/* Input(s)           : u4CxtId - Context ID                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP_SUCCESS/RIP_FAILURE                         */
/************************************************************************/
INT4
RipRedStopTimersOnStandby (tRipCxt * pRipCxtEntry)
{

    UINT4               u4HashIndex = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4RemainingTime = 0;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }

            /*Unlink the timer associated with this interface, if the iface
             *is already active. Even for WAN circuits timers needs to be
             unlinked */
            if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE)
            {
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(pRipIfRec->RipUpdateTimer.TimerNode));
                if ((RIP_GET_REMAINING_TIME
                     (RIP_TIMER_ID, &(pRipIfRec->RipSpacingTimer.TimerNode),
                      &u4RemainingTime)) == TMR_SUCCESS)
                {
                    RIP_STOP_TIMER (RIP_TIMER_ID,
                                    &(pRipIfRec->RipSpacingTimer.TimerNode));
                    TMO_DLL_Delete (&pRipCxtEntry->RipIfSpaceLst,
                                    &pRipIfRec->RipIfRecNode);
                }
                /*Delete the interface record from Space Interface list */
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(pRipIfRec->RipSubscrTimer.TimerNode));

                pRipIfRec->RipUpdateTimer.u1Data = 0;
                pRipIfRec->RipSpacingTimer.u1Data = 0;
                pRipIfRec->RipSubscrTimer.u1Data = 0;
            }
            /*clear context related timers also */
            /* Stop the timers before clearing the context */
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &((pRipCxtEntry->RipPassiveUpdTimer).Timer_node));
            /* Initialize the triggered timer limiting variables to their startup 
             *values*/
            pRipCxtEntry->RipGblCfg.u1TrigUpdFlag = RIP_TRIG_TIMER_INACTIVE;
            pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_NOT_DESIRED;
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &((pRipCxtEntry->RipTrigUpdTimer).Timer_node));
            pRipCxtEntry->TripTrigTmr.u1Data = RIP_TRIG_TIMER_INACTIVE;
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &((pRipCxtEntry->TripTrigTmr).TimerNode));

        }
    }
    return RIP_SUCCESS;
}

#else

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize rip dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Rip descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Rip Oper db node. */
/*                                                                           */
/*    Input(s)            : pRipOperInfo - pointer to Rip Oper Entry.      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDbNodeInit (tDbTblNode * pRipOperDb, UINT4 u4Type)
{
    UNUSED_PARAM (pRipOperDb);
    UNUSED_PARAM (u4Type);
    return;
}

/************************************************************************
 *  Function Name   : RipRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the RIP module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
RipRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : RipRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the RIP module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register RIP with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
RipRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RIP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RipRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : RipRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the RIP module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the RIP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
RipRedHandleRmEvents (UINT1 u1Flag)
{
    UNUSED_PARAM (u1Flag);
    return;
}

/************************************************************************
 * Function Name      : RipRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the RIP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
RipRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************
 * Function Name      : RipRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the RIP upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
RipRedHandleGoStandby (tRipRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : RipRedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RipRedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RipRedStartTimers                               */
/*                                                                      */
/* Description        : This is a will start all the necessary timers   */
/*                      for all the RIP instances. It will walk        */
/*                      array and start the timers. If the RIP state   */
/*                      is Master, then Advertisement timer is started. */
/*                      If the state is Backup, then Master-Down timer  */
/*                      is started.                                     */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP_SUCCESS/RIP_FAILURE                         */
/************************************************************************/

PUBLIC INT4
RipRedStartTimers ()
{
    return RIP_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RipRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);

    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);

    return;
}

/************************************************************************/
/* Function Name      : RipRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the RIP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RipRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RipRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RipRedSendBulkReqMsg                            */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedSendBulkReqMsg (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDbUtilAddRtInfo                            */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic route */
/*                          info after copying the information               */
/*                                                                           */
/*    Input(s)            : pRipRtInfo - Rip Route information             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDbUtilAddRtInfo (tRipRtEntry * pRipRtInfo)
{
    UNUSED_PARAM (pRipRtInfo);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pRipDataDesc - This is Rip sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pRipDbNode - This is db node defined in the RIP*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedDbUtilAddTblNode (tDbTblDescriptor * pRipDataDesc,
                        tDbTblNode * pRipDbNode)
{
    UNUSED_PARAM (pRipDataDesc);
    UNUSED_PARAM (pRipDbNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate RIP dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedSyncDynInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedAddPeerAllNodeInDbTbl                      */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedAddAllPeerNodeInDbTbl (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedSendBulkDefCxtInfo                         */
/*                                                                           */
/*    Description         : This function will initiate bulk syncup in the   */
/*                          scenario when default context is deleted         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/
PUBLIC VOID
RipRedSendBulkDefCxtInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : RipRedAddRouteAllNodeInDbTbl                     */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
RipRedAddAllRouteNodeInDbTbl (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RipRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedSendBulkUpdMsg (VOID)
{
    return;

}

/************************************************************************/
/* Function Name      : RipRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RIP offers an IP address         */
/*                      to the RIP client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRipBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
RipRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RipRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessDynamicRtInfo                      */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the route information   */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RipRedProcessDynamicRtInfo (tRmMsg * pMsg, UINT4 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : RipRedProcessDynamicPeerInfo                    */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the peer information    */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RipRedProcessDynamicPeerInfo (tRmMsg * pMsg, UINT4 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : RipRedNotifyRestartRTM                          */
/*                                                                      */
/* Description        : This function will notify RTM to mark all RIP   */
/*                      routes as STALE. It will send an event to RTN   */
/*                      for this. On receiving the event, RTM will      */
/*                      mark all the routes as stale and start a timer  */
/*                      for Timeout value. On the expiry of this timer  */
/*                      if there are any stale routes still present     */
/*                      those routes are deleted from the RTM and also  */
/*                      from the hardware.                              */
/*                                                                      */
/* Input(s)           : u4CxtId - Context ID                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP_SUCCESS/RIP_FAILURE                         */
/************************************************************************/

INT4
RipRedNotifyRestartRTM (UINT4 u4CxtId)
{
    UNUSED_PARAM (u4CxtId);
    return RIP_SUCCESS;
}

#endif /* L2RED_WANTED */

#endif /* __RIPRED_C */
