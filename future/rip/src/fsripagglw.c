/* $Id: fsripagglw.c,v 1.13 2017/09/20 13:07:59 siva Exp $ */

#include "ripinc.h"
#include "ripcli.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"

/* LOW LEVEL Routines for Table : FsRipAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRipAggTable
 Input       :  The Indices
                FsRipIfIndex
                FsRipAggAddress
                FsRipAggAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRipAggTable (INT4 i4FsRipIfIndex,
                                       UINT4 u4FsRipAggAddress,
                                       UINT4 u4FsRipAggAddressMask)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    UINT4               u4Port = 0;
    VOID               *pTrieRoot = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT4                i4GetPortIfIn;

    i4GetPortIfIn = NetIpv4GetPortFromIfIndex ((UINT4) i4FsRipIfIndex, &u4Port);
    UNUSED_PARAM (i4GetPortIfIn);

    /* get the trie that hold the aggregated routes configured over this 
     * interface */
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry);

    if (pTrieRoot == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRipIfAggRtInfo = RipCheckRtInIfAggTbl ((UINT2) u4Port, u4FsRipAggAddress,
                                            u4FsRipAggAddressMask,
                                            pRipCxtEntry);
    if (pRipIfAggRtInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRipAggTable
 Input       :  The Indices
                FsRipIfIndex
                FsRipAggAddress
                FsRipAggAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRipAggTable (INT4 *pi4FsRipIfIndex,
                               UINT4 *pu4FsRipAggAddress,
                               UINT4 *pu4FsRipAggAddressMask)
{
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome = TRIE_FAILURE;
    UINT4               u4Port = 0;
    UINT4               au4Indx[2];
    UINT4               u4IfIndex = 0;
    VOID               *pTrieRoot = NULL;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pRipIfAggTblRoot =
        (tRipIfAggTblRoot *) TMO_SLL_First (&pRipCxtEntry->RipIfAggTblRootLst);
    if (pRipIfAggTblRoot == NULL)
    {
        return SNMP_FAILURE;
    }
    u4Port = pRipIfAggTblRoot->u4IfIndex;

    if ((pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry)) == NULL)
    {
        return (SNMP_FAILURE);
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pTrieRoot;
    InParams.i1AppId = (OTHERS_ID - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
    if (i4OutCome == TRIE_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;

    NetIpv4GetCfaIfIndexFromPort (u4Port, &u4IfIndex);
    *pi4FsRipIfIndex = (INT4) u4IfIndex;
    *pu4FsRipAggAddress = pRipIfAggRtInfo->u4AggRtAddr;
    *pu4FsRipAggAddressMask = pRipIfAggRtInfo->u4AggRtMask;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRipAggTable
 Input       :  The Indices
                FsRipIfIndex
                nextFsRipIfIndex
                FsRipAggAddress
                nextFsRipAggAddress
                FsRipAggAddressMask
                nextFsRipAggAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRipAggTable (INT4 i4FsRipIfIndex, INT4 *pi4NextFsRipIfIndex,
                              UINT4 u4FsRipAggAddress,
                              UINT4 *pu4NextFsRipAggAddress,
                              UINT4 u4FsRipAggAddressMask,
                              UINT4 *pu4NextFsRipAggAddressMask)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tRipIfAggTblRoot   *pRipIfAggTblRoot = NULL;
    INT4                i4OutCome = TRIE_FAILURE;
    UINT4               au4Indx[2];
    UINT4               u4Port;
    UINT4               u4CfaIfIndex = 0;
    VOID               *pTrieRoot = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT4                i4GetPortIfIn;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4GetPortIfIn = NetIpv4GetPortFromIfIndex ((UINT4) i4FsRipIfIndex, &u4Port);
    UNUSED_PARAM (i4GetPortIfIn);

    if ((pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry)) != NULL)
    {
        au4Indx[0] = RIP_HTONL (u4FsRipAggAddress);
        au4Indx[1] = RIP_HTONL (u4FsRipAggAddressMask);
        InParams.pRoot = pTrieRoot;
        InParams.i1AppId = (OTHERS_ID - 1);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        OutParams.Key.pKey = NULL;
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));

        if (i4OutCome == TRIE_SUCCESS)
        {
            pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
            *pi4NextFsRipIfIndex = i4FsRipIfIndex;
            *pu4NextFsRipAggAddress = pRipIfAggRtInfo->u4AggRtAddr;
            *pu4NextFsRipAggAddressMask = pRipIfAggRtInfo->u4AggRtMask;
            return SNMP_SUCCESS;
        }
    }

    TMO_SLL_Scan (&pRipCxtEntry->RipIfAggTblRootLst, pRipIfAggTblRoot,
                  tRipIfAggTblRoot *)
    {
        NetIpv4GetCfaIfIndexFromPort (pRipIfAggTblRoot->u4IfIndex,
                                      &u4CfaIfIndex);
        if (u4CfaIfIndex > (UINT4) i4FsRipIfIndex)
        {
            break;
        }
    }
    if (pRipIfAggTblRoot != NULL)
    {
        au4Indx[0] = 0;
        au4Indx[1] = 0;
        InParams.pRoot = pRipIfAggTblRoot->pRipIfAggTblPtr;
        InParams.i1AppId = (OTHERS_ID - 1);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        OutParams.Key.pKey = NULL;
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));

        if (i4OutCome == TRIE_SUCCESS)
        {
            pRipIfAggRtInfo = (tRipIfAggRtInfo *) OutParams.pAppSpecInfo;
            *pi4NextFsRipIfIndex = (INT4) u4CfaIfIndex;
            *pu4NextFsRipAggAddress = pRipIfAggRtInfo->u4AggRtAddr;
            *pu4NextFsRipAggAddressMask = pRipIfAggRtInfo->u4AggRtMask;
            return (SNMP_SUCCESS);
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRipAggStatus
 Input       :  The Indices
                FsRipIfIndex
                FsRipAggAddress
                FsRipAggAddressMask

                The Object 
                retValFsRipAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipAggStatus (INT4 i4FsRipIfIndex, UINT4 u4FsRipAggAddress,
                      UINT4 u4FsRipAggAddressMask,
                      INT4 *pi4RetValFsRipAggStatus)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    UINT4               u4Port = 0;
    VOID               *pTrieRoot = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT4                i4GetPortIfIn;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4GetPortIfIn = NetIpv4GetPortFromIfIndex ((UINT4) i4FsRipIfIndex, &u4Port);
    UNUSED_PARAM (i4GetPortIfIn);

    if ((pTrieRoot = RipGetIfAggTblRoot ((UINT2) u4Port, pRipCxtEntry)) == NULL)
    {
        return (SNMP_FAILURE);
    }
    UNUSED_PARAM (pTrieRoot);

    pRipIfAggRtInfo = RipCheckRtInIfAggTbl ((UINT2) u4Port, u4FsRipAggAddress,
                                            u4FsRipAggAddressMask,
                                            pRipCxtEntry);
    if (pRipIfAggRtInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsRipAggStatus = pRipIfAggRtInfo->u1AggRtStatus;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRipAggStatus
 Input       :  The Indices
                FsRipIfIndex
                FsRipAggAddress
                FsRipAggAddressMask

                The Object 
                setValFsRipAggStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipAggStatus (INT4 i4FsRipIfIndex, UINT4 u4FsRipAggAddress,
                      UINT4 u4FsRipAggAddressMask, INT4 i4SetValFsRipAggStatus)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tRipIfAggRtInfo    *pRipOverlapAggRt = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Address = 0;
    UINT4               u4Mask = 0;
    UINT4               u4Port = 0;
    UINT2               u2Port = 0;
    VOID               *pTrieRoot = NULL;
    UINT2               u2Metric = 0;
    UINT2               u2Count = 0;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4FsRipIfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u2Port = (UINT2) u4Port;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pTrieRoot = RipGetIfAggTblRoot (u2Port, pRipCxtEntry);

    u4Address = u4FsRipAggAddress;
    u4Mask = u4FsRipAggAddressMask;

    switch (i4SetValFsRipAggStatus)
    {
        case ACTIVE:

            /* get the aggregation entry */
            pRipIfAggRtInfo =
                RipCheckRtInIfAggTbl (u2Port, u4Address, u4Mask, pRipCxtEntry);
            if (pRipIfAggRtInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            /* check if any overlapping aggreation exist for this 
             * aggregated route */
            pRipOverlapAggRt = RipCheckIfAgg (u2Port,
                                              pRipIfAggRtInfo->u4AggRtAddr,
                                              pRipIfAggRtInfo->u4AggRtMask,
                                              pRipCxtEntry);

            /* set the row status as active */
            pRipIfAggRtInfo->u1AggRtStatus = (UINT1) i4SetValFsRipAggStatus;

            /*update the aggregated route entry with the proper metric
             * and count*/
            RipUpdateIfAggRt (pRipIfAggRtInfo, u2Port);
            pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;

            if (pRipOverlapAggRt != NULL)
            {
                RipUpdateIfAggRt (pRipOverlapAggRt, u2Port);
                pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;

            }

            /* If the number of subnet count is greater than zero 
             * and if summary is disabled , install this aggregated route as
             * sink route */
            if (pRipIfAggRtInfo->u2Count != 0)
            {
                if (RipUpdateIfAggRtAsSinkRt (pRipIfAggRtInfo) == RIP_FAILURE)
                {
                    return (SNMP_FAILURE);
                }
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipAggStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipAggStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %u %i",
                              pRipCxtEntry->i4CxtId, i4FsRipIfIndex,
                              u4FsRipAggAddress, u4FsRipAggAddressMask,
                              i4SetValFsRipAggStatus));
#endif

            return (SNMP_SUCCESS);

        case NOT_IN_SERVICE:

            pRipIfAggRtInfo =
                RipCheckRtInIfAggTbl (u2Port, u4Address, u4Mask, pRipCxtEntry);
            if (pRipIfAggRtInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            pRipIfAggRtInfo->u1AggRtStatus = (UINT1) i4SetValFsRipAggStatus;
            if (pRipIfAggRtInfo->u1SinkRtFlag == RIP_INSTALL_SINK_RT)
            {
                /* delete the entry from rtm as it is made not in service */
                if (RIP_FAILURE == RipDeleteIfAggSinkRt (pRipIfAggRtInfo))
                {
                    return SNMP_FAILURE;
                }
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipAggStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipAggStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %u %i",
                              pRipCxtEntry->i4CxtId, i4FsRipIfIndex,
                              u4FsRipAggAddress, u4FsRipAggAddressMask,
                              i4SetValFsRipAggStatus));
#endif

            return (SNMP_SUCCESS);

        case CREATE_AND_GO:

            if (pTrieRoot == NULL)
            {
                if (RipIfAggTblInit ((UINT4) i4FsRipIfIndex, pRipCxtEntry) ==
                    RIP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC, RIP_TRC_CXT_FLAG, RIP_MGMT_CXT_ID,
                             ALL_FAILURE_TRC,
                             RIP_NAME, "RipIfAggTblInit Failed\n");

                    return (SNMP_FAILURE);
                }
            }
            /* check if any overlapping aggreation exist for this 
             * aggregated route.*/
            pRipOverlapAggRt =
                RipCheckIfAgg (u2Port, u4Address, u4Mask, pRipCxtEntry);

            pRipIfAggRtInfo =
                RipAddIfAggRtToTable ((UINT4) i4FsRipIfIndex, u4Address,
                                      u4Mask, pRipCxtEntry);
            if (pRipIfAggRtInfo == NULL)
            {
                RIP_TRC (RIP_MOD_TRC, RIP_TRC_CXT_FLAG, RIP_MGMT_CXT_ID,
                         ALL_FAILURE_TRC,
                         RIP_NAME, "RipAddIfAggRtToTbl Failed\n");
                return (SNMP_FAILURE);
            }

            /* set the row status of the configured aggegation entry as 
             * active*/
            pRipIfAggRtInfo->u1AggRtStatus = ACTIVE;

            /*update the aggregated route entry with the proper metric
             * and count*/
            RipUpdateIfAggRt (pRipIfAggRtInfo, u2Port);
            pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;

            if (pRipOverlapAggRt != NULL)
            {
                RipUpdateIfAggRt (pRipOverlapAggRt, u2Port);
                pRipIfAggRtInfo->u2Status = RIP_IF_AGG_RT_DEF_STATUS;
            }

            /* If the number of subnet count is greater than zero 
             * and if summary is disabled , install this aggregated route 
             * sink route */
            if (pRipIfAggRtInfo->u2Count != 0)
            {
                if (RipUpdateIfAggRtAsSinkRt (pRipIfAggRtInfo) == RIP_FAILURE)
                {
                    return (SNMP_FAILURE);
                }
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipAggStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipAggStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %u %i",
                              pRipCxtEntry->i4CxtId, i4FsRipIfIndex,
                              u4FsRipAggAddress, u4FsRipAggAddressMask,
                              i4SetValFsRipAggStatus));
#endif
            return SNMP_SUCCESS;

        case CREATE_AND_WAIT:

            if (pTrieRoot == NULL)
            {
                if (RipIfAggTblInit ((UINT4) i4FsRipIfIndex, pRipCxtEntry) ==
                    RIP_FAILURE)
                {
                    return (SNMP_FAILURE);
                }
            }

            pRipIfAggRtInfo =
                RipAddIfAggRtToTable ((UINT4) i4FsRipIfIndex, u4Address,
                                      u4Mask, pRipCxtEntry);

            if (pRipIfAggRtInfo == NULL)
            {
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipAggStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipAggStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %u %i",
                              pRipCxtEntry->i4CxtId, i4FsRipIfIndex,
                              u4FsRipAggAddress, u4FsRipAggAddressMask,
                              i4SetValFsRipAggStatus));
#endif
            return SNMP_SUCCESS;

        case DESTROY:

            /* get the aggregation entry and set the row status as not in service */
            pRipIfAggRtInfo =
                RipCheckRtInIfAggTbl (u2Port, u4Address, u4Mask, pRipCxtEntry);
            if (pRipIfAggRtInfo == NULL)
            {
                return SNMP_SUCCESS;
            }
            pRipIfAggRtInfo->u1AggRtStatus = NOT_IN_SERVICE;
            u2Metric = pRipIfAggRtInfo->u2Metric;
            u2Count = pRipIfAggRtInfo->u2Count;

            /* delete the aggregated route from rtm */
            if (RIP_FAILURE == RipDeleteIfAggSinkRt (pRipIfAggRtInfo))
            {
                return SNMP_FAILURE;
            }

            /* delete the entry from trie. */
            RipDeleteIfAggRt (u4Port, u4Address, u4Mask, pRipCxtEntry);

            /* delete the trie if there is no other aggregated entry 
             * configured over the interface */
            /* Also delete the node from the sll */
            RipIfAggTblShut (u4Port, pRipCxtEntry);
            /* check if any overlapping aggreation exist for this 
             * aggregated route.If so update the metric and count for that 
             * route before deleting the entry */
            pRipOverlapAggRt =
                RipCheckIfAgg ((UINT2) u4Port, u4Address, u4Mask, pRipCxtEntry);
            if (pRipOverlapAggRt != NULL)
            {
                pRipOverlapAggRt->u2Count =
                    (UINT2) (pRipOverlapAggRt->u2Count + u2Count);
                pRipOverlapAggRt->u2Metric =
                    RIP_MIN_METRIC (pRipOverlapAggRt->u2Metric, u2Metric);

            }
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipAggStatus;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipAggStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 4;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u %u %i",
                              pRipCxtEntry->i4CxtId, i4FsRipIfIndex,
                              u4FsRipAggAddress, u4FsRipAggAddressMask,
                              i4SetValFsRipAggStatus));
#endif
            return SNMP_SUCCESS;

        default:
            return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRipAggStatus
 Input       :  The Indices
                FsRipIfIndex
                FsRipAggAddress
                FsRipAggAddressMask

                The Object 
                testValFsRipAggStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipAggStatus (UINT4 *pu4ErrorCode, INT4 i4FsRipIfIndex,
                         UINT4 u4FsRipAggAddress,
                         UINT4 u4FsRipAggAddressMask,
                         INT4 i4TestValFsRipAggStatus)
{
    tRipIfAggRtInfo    *pRipIfAggRtInfo = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    UINT4               u4Address = 0;
    UINT4               u4Mask = 0;
    UINT4               u4Port = 0;
    UINT2               u2Port = 0;
    VOID               *pTrieRoot = NULL;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4FsRipIfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    u2Port = (UINT2) u4Port;
    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    u4Address = u4FsRipAggAddress;
    u4Mask = u4FsRipAggAddressMask;

    switch (i4TestValFsRipAggStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:

            pRipIfAggRtInfo =
                RipCheckRtInIfAggTbl (u2Port, u4Address, u4Mask, pRipCxtEntry);
            if (pRipIfAggRtInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            /* entry exist in aggregation table */
            return (SNMP_SUCCESS);

        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            pRipIfAggRtInfo =
                RipCheckRtInIfAggTbl (u2Port, u4Address, u4Mask, pRipCxtEntry);
            if (pRipIfAggRtInfo != NULL)
            {
                /*such an entry already exist */
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }

            pTrieRoot = RipGetIfAggTblRoot (u2Port, pRipCxtEntry);
            if (pTrieRoot != NULL)
            {
                /* Check if maximum allowed aggregations have been configured
                 * or not*/
                if (RipCheckIfAggLimit (u2Port, pRipCxtEntry) == RIP_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    return SNMP_FAILURE;
                }
            }
            return (SNMP_SUCCESS);

        case DESTROY:

            return (SNMP_SUCCESS);

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
    }
}

/****************************************************************************
Function Name    :  nmhDepv2FsRipAggTable
 Input       :  The Indices
                FsRipIfIndex
                FsRipAggAddress
                FsRipAggAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipAggTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
