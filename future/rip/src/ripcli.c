/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripcli.c,v 1.127 2018/01/03 11:32:33 siva Exp $
 *
 * Description:This file contains the routine for RIP task
 *             and event management.
 *
 *******************************************************************/

/* SOURCE FILE  :
 *
 * ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |                                                                           |
 * |  FILE NAME                   :  Ripcli.c                                  |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  Rip                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  Rip                                       |
 * |                                                                           |
 * |  LANGUAGE                    :   C                                        |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :   Linux                                    |
 * |                                                                           |
 * |  AUTHOR                      :                                            |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE       :                                            | 
 * |                                                                           |
 * |  DESCRIPTION                 :  this file has the handle                  |
 * |                                 routines for cli SET/GET destined         |
 * |                                 for RIP Module                            |
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __RIPCLI_C__
#define __RIPCLI_C__

/********************************************************/
/*            HEADER FILES                              */
/********************************************************/

#include "ripinc.h"
#include "cli.h"
#include "ripcli.h"
#include "extern.h"
#include "stdripwr.h"
#include "fsripwr.h"

extern UINT4        gu4RipPeriodicUpdates;
PRIVATE void        RipPrintCxtName (tCliHandle CliHandle, INT4 i4CxtId);

INT4
cli_process_rip_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RIP_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT1              *pu1RipCxtName = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4IfIndex = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4CxtId = (INT4) RIP_INVALID_CXT_ID;
    INT4                i4AdminStatus;
    UINT2               u2ShowCmdFlag = RIP_FALSE;
    UINT1               u1DefMetric = 0;

    CliRegisterLock (CliHandle, RipLock, RipUnLock);
    RipLock ();

    if (RipCliGetContext (CliHandle, u4Command, &i4CxtId, &i4IfaceIndex,
                          &u2ShowCmdFlag) == RIP_FAILURE)
    {
        RipUnLock ();
        CliUnRegisterLock (CliHandle);
        CliPrintf (CliHandle, "\r %% RIP is not enabled on the interface\n\r");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfIndex = va_arg (ap, INT4);

    /* If u2ShowCmdFlag is set and for router enable/disable the third 
     * argument is always context name */
    if ((u4Command == CLI_RIP_NO_ROUTER) || (u4Command == CLI_RIP_ROUTER)
        || (u2ShowCmdFlag == RIP_TRUE) || (u4Command == CLI_RIP_DEBUG) ||
        (u4Command == CLI_RIP_AUTHENTICATION_LAST_KEY))
    {
        pu1RipCxtName = va_arg (ap, UINT1 *);
    }

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == RIP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);
    if (u2ShowCmdFlag == RIP_TRUE)
    {
        switch (u4Command)
        {

            case CLI_RIP_SHOW_RIP:
                /* args[0] - command identifier 
                 * args[1] - destination ip address
                 * args[2] - destination ip mask
                 */
                if (pu1RipCxtName != NULL)
                {
                    i4RetStatus =
                        RipCliGetCxtIdFrmVrfName ((UINT1 *) pu1RipCxtName,
                                                  &i4CxtId);
                    if (i4RetStatus != CLI_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r %% Invalid RIP Context Id\n\r");
                        RipUnLock ();
                        CliUnRegisterLock (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                if (CLI_PTR_TO_U4 (args[0]) == CLI_RIP_SHOW_DATABASE)
                {
                    i4RetStatus = RipShowDatabaseCxt (CliHandle, i4CxtId);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RIP_SHOW_IPADDR)
                {
                    i4RetStatus = RipShowIpAddrInCxt (CliHandle,
                                                      *args[1],
                                                      *args[2], i4CxtId);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RIP_SHOW_STATISTICS)
                {
                    i4RetStatus = RipShowStatisticsInCxt (CliHandle, i4CxtId);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RIP_SHOW_AUTHENTICATION)
                {
                    i4RetStatus =
                        RipShowAuthenticationInCxt (CliHandle, i4CxtId);
                }
                break;

            case CLI_RIP_SHOW_PEER_INFO:
                i4RetStatus = RipShowPeerInformation (CliHandle, i4CxtId);
                break;

            default:
                CliPrintf (CliHandle, "\r% Invalid Command\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
        }
    }
    else
    {
        if ((u4Command == CLI_RIP_ROUTER) || (u4Command == CLI_RIP_NO_ROUTER)
            || (u4Command == CLI_RIP_DEBUG) ||
            (u4Command == CLI_RIP_AUTHENTICATION_LAST_KEY))
        {
            if (pu1RipCxtName != NULL)
            {
                i4RetStatus =
                    RipCliGetCxtIdFrmVrfName (pu1RipCxtName, &i4CxtId);
            }
            else
            {
                i4CxtId = RIP_DEFAULT_CXT;
            }
        }
        if ((i4RetStatus == CLI_FAILURE)
            || ((u4Command != CLI_RIP_ROUTER)
                && (RipSetContext ((UINT4) i4CxtId) == SNMP_FAILURE)))
        {
            CliPrintf (CliHandle, "\r %% Invalid RIP Context Id\n\r");
            RipUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
        if ((u4Command != CLI_RIP_ROUTER) &&
            (nmhGetFsRipAdminStatus (&i4AdminStatus) == SNMP_SUCCESS) &&
            (i4AdminStatus == RIP_ADMIN_DISABLE))
        {
            CliPrintf (CliHandle, "\r %% Context AdminStatus is Disabled\n\r");
            RipUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
        switch (u4Command)
        {

            case CLI_RIP_DEBUG:
                /* args[0] - CLI_ENABLE / CLI_DISABLE 
                 * args[1] - debug level to set
                 */
                i4RetStatus =
                    RipSetDebugLevel (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                      (INT4) CLI_PTR_TO_U4 (args[1]));
                break;

            case CLI_RIP_ROUTER:
                i4RetStatus = RipEnableRouter (CliHandle, i4CxtId);
                break;

            case CLI_RIP_NO_ROUTER:
                i4RetStatus = RipDisableRouter (CliHandle, i4CxtId);
                break;

            case CLI_RIP_AUTHENTICATION_LAST_KEY:
                i4RetStatus =
                    RipCtrlLastAuthenticationKey (CliHandle, i4CxtId,
                                                  CLI_PTR_TO_I4 (args[0]));
                break;

            case CLI_RIP_SECURITY:
                /* args[0] - security level */
                i4RetStatus =
                    RipSetSecurity (CliHandle, CLI_PTR_TO_I4 (args[0]));
                break;

            case CLI_RIP_NO_SECURITY:
                /* args[0] - security level */
                i4RetStatus = RipSetSecurity (CliHandle, CLI_RIP_SECURITY_MAX);
                break;

            case CLI_RIP_RETRANSMIT:
                /* args[0] - command identifier
                 * args[1] - timeout or retries value
                 */
                if (CLI_PTR_TO_U4 (args[0]) == RIP_RETRANSMIT_INTERVAL)
                {
                    RipSetRetransmissionInt (CliHandle, *(INT4 *) args[1]);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == RIP_RETRANSMIT_RETRIES)
                {
                    RipSetRetransmissionRetries (CliHandle, *(INT4 *) args[1]);
                }
                break;

            case CLI_RIP_NO_RETRANSMIT:
                /* args[0] - command identifier
                 */
                if (CLI_PTR_TO_U4 (args[0]) == RIP_RETRANSMIT_INTERVAL)
                {
                    RipSetRetransmissionInt (CliHandle,
                                             RIP_DEF_RETRANSMISSION_INT);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == RIP_RETRANSMIT_RETRIES)
                {
                    RipSetRetransmissionRetries (CliHandle,
                                                 RIP_DEF_RETRANSMISSION_RETRIES);
                }
                break;

            case CLI_RIP_NETWORK:
                /* args[1] - ip address of the IP network */
                i4RetStatus =
                    RipSetNetwork (CliHandle, *args[0],
                                   CLI_PTR_TO_U4 (args[1]));
                break;

            case CLI_RIP_NO_NETWORK:
                /* args[0] - ip address of the IP network */
                i4RetStatus =
                    RipSetNoNetwork (CliHandle, *args[0],
                                     CLI_PTR_TO_U4 (args[1]));
                break;

            case CLI_RIP_NEIGHBOR:
                /* args[0] - IP address of the neighbor 
                 */
                i4RetStatus = RipSetNeighbor (CliHandle, *args[0]);
                break;

            case CLI_RIP_NO_NEIGHBOR:
                /* args[0] - IP address of the neighbor 
                 */
                i4RetStatus = RipSetNoNeighbor (CliHandle, *args[0]);
                break;

            case CLI_RIP_PASSIVE_INTF:
                i4RetStatus = RipSetPassiveIntf (CliHandle, i4IfIndex,
                                                 (INT4) RIP_ADMIN_PASSIVE);
                break;

            case CLI_RIP_NO_PASSIVE_INTF:
                i4RetStatus = RipSetPassiveIntf (CliHandle, i4IfIndex,
                                                 (INT4) RIP_ADMIN_ENABLE);
                break;

            case CLI_RIP_OUTPUT_DELAY:
                i4RetStatus = RipSetSpacing (CliHandle, RIP_SPACING_ENABLE);
                break;

            case CLI_RIP_NO_OUTPUT_DELAY:
                i4RetStatus = RipSetSpacing (CliHandle, RIP_SPACING_DISABLE);
                break;

            case CLI_RIP_ROUTE_REDISTRIBUTE:
                /* args[0] - protocol identifier 
                 */
                i4RetStatus =
                    RipSetRouteRedistribute (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                             (UINT1 *) args[1]);
                break;

            case CLI_RIP_NO_ROUTE_REDISTRIBUTE:
                /* args[0] - protocol identifier 
                 */
                i4RetStatus =
                    RipSetNoRouteRedistribute (CliHandle,
                                               CLI_PTR_TO_I4 (args[0]),
                                               (UINT1 *) args[1]);

                break;

            case CLI_RIP_DISTRIBUTE_LIST:
                i4RetStatus = RipCliSetDistribute (CliHandle, (UINT1 *) args[0],
                                                   (UINT1)
                                                   CLI_PTR_TO_U4 (args[1]),
                                                   (UINT1)
                                                   CLI_PTR_TO_U4 (args[2]));
                break;

            case CLI_RIP_SEND_VERSION:
                /* args[0] - send version */
                i4RetStatus = RipSetSendVersion (CliHandle, i4IfaceIndex,
                                                 CLI_PTR_TO_I4 (args[0]));
                break;

            case CLI_RIP_RECEIVE_VERSION:
                /* args[0] - receive version */
                i4RetStatus = RipSetReceiveVersion (CliHandle, i4IfaceIndex,
                                                    CLI_PTR_TO_I4 (args[0]));
                break;

            case CLI_RIP_SEND_RECV_VERSION:
                i4RetStatus = RipSetSendAndRecvVersion (CliHandle,
                                                        CLI_PTR_TO_I4 (args[0]),
                                                        CLI_PTR_TO_I4 (args
                                                                       [1]));
                break;

            case CLI_RIP_AUTHENTICATION:
                /* args[0] - authentication  mode
                 * args[1] - authentication key
                 */
                i4RetStatus = RipSetAuthentication (CliHandle, i4IfaceIndex,
                                                    CLI_PTR_TO_I4 (args[0]),
                                                    (UINT1 *) args[1]);
                break;

            case CLI_RIP_AUTHENTICATION_KEY:
                /* 
                 * args[0] - authentication key
                 */
                i4RetStatus = RipSetAuthenticationKey (CliHandle, i4IfaceIndex,
                                                       (UINT1 *) args[0]);
                break;

            case CLI_RIP_NO_AUTHENTICATION:
                /* The action routine should disable authentication so set the
                 * authentication mode to CLI_RIP_AUTH_NONE
                 */
                i4RetStatus = RipSetNoAuthentication (CliHandle, i4IfaceIndex);
                break;

            case CLI_RIP_CRYPTO_AUTHENTICATION:
                /* args[0] - authentication type
                 */
                i4RetStatus = RipSetCryptoAuthentication (CliHandle, i4CxtId,
                                                          i4IfaceIndex,
                                                          CLI_PTR_TO_I4 (args
                                                                         [0]));
                break;

            case CLI_RIP_CRYPTO_AUTH_KEY:
                /* args[0] - authentication key ID
                 * args[1] - authentication key
                 */
                i4RetStatus = RipSetCryptoAuthenticationKey
                    (CliHandle, i4CxtId,
                     i4IfaceIndex, *(INT4 *) (args[0]), (UINT1 *) args[1]);
                break;

            case CLI_RIP_DEL_CRYPTO_AUTH_KEY:
                /* args[0] - authentication key ID
                 */
                i4RetStatus = RipSetDeleteCryptoAuthKey (CliHandle, i4CxtId,
                                                         i4IfaceIndex,
                                                         *(INT4 *) (args[0]));
                break;

            case CLI_RIP_AUTH_KEY_START_ACCEPT:
                /* args[0] - authentication key ID
                 * args[1] - start accept time
                 */
                i4RetStatus = RipSetCryptoAuthKeyStartAccept
                    (CliHandle, i4CxtId,
                     i4IfaceIndex, *(INT4 *) (args[0]), (UINT1 *) args[1]);
                break;

            case CLI_RIP_AUTH_KEY_STOP_ACCEPT:
                /* args[0] - authentication key ID
                 * args[1] - stop accept time
                 */
                i4RetStatus = RipSetCryptoAuthKeyStopAccept
                    (CliHandle, i4CxtId,
                     i4IfaceIndex, *(INT4 *) (args[0]), (UINT1 *) args[1]);
                break;

            case CLI_RIP_AUTH_KEY_START_GEN:
                /* args[0] - authentication key ID
                 * args[1] - start generate time
                 */
                i4RetStatus = RipSetCryptoAuthKeyStartGenerate
                    (CliHandle, i4CxtId,
                     i4IfaceIndex, *(INT4 *) (args[0]), (UINT1 *) args[1]);
                break;

            case CLI_RIP_AUTH_KEY_STOP_GEN:
                /* args[0] - authentication key ID
                 * args[1] - stop generate time
                 */
                i4RetStatus = RipSetCryptoAuthKeyStopGenerate
                    (CliHandle, i4CxtId,
                     i4IfaceIndex, *(INT4 *) (args[0]), (UINT1 *) args[1]);
                break;

            case CLI_RIP_SPLIT_HORIZON:
                /* args[0] - poisson */
                i4RetStatus = RipSetSplitHorizon (CliHandle, i4IfaceIndex,
                                                  (args[0] !=
                                                   NULL) ?
                                                  RIP_SPLIT_HORZ_WITH_POIS_REV :
                                                  RIP_SPLIT_HORIZON);
                break;

            case CLI_RIP_NO_SPLIT_HORIZON:
                i4RetStatus = RipSetSplitHorizon (CliHandle, i4IfaceIndex,
                                                  RIP_NO_SPLIT_HORIZON);
                break;

            case CLI_RIP_DEF_METRIC:
                /* args[0] - metric value */
                u1DefMetric = *(UINT1 *) args[0];
                i4RetStatus = RipSetDefMetric (CliHandle, u1DefMetric);
                break;

            case CLI_RIP_NO_DEF_METRIC:
                i4RetStatus =
                    RipSetDefMetric (CliHandle, CLI_RIP_DEF_METRIC_VALUE);
                break;

            case CLI_RIP_INSTALL_DEF_RT:
                i4RetStatus =
                    RipSetDefRtInstallStatus (CliHandle, i4IfaceIndex,
                                              RIP_INSTALL_DEF_RT);
                break;

            case CLI_RIP_NO_INSTALL_DEF_RT:
                i4RetStatus =
                    RipSetDefRtInstallStatus (CliHandle, i4IfaceIndex,
                                              RIP_DONT_INSTALL_DEF_RT);
                break;

            case CLI_RIP_TIMERS_BASIC:
                /* args[0] - update timer
                 * args[1] - hold timer
                 * args[2] - garbage timer
                 */
                i4RetStatus = RipSetTimers (CliHandle, i4IfaceIndex,
                                            (UINT4) (*(INT4 *) args[0]),
                                            (UINT4) (*(INT4 *) args[1]),
                                            (UINT4) (*(INT4 *) args[2]));
                break;

            case CLI_RIP_NO_TIMERS_BASIC:
                /* args[0] - update timer
                 * args[1] - hold timer
                 * args[2] - garbage timer
                 */
                i4RetStatus = RipSetTimers (CliHandle, i4IfaceIndex,
                                            CLI_RIP_DEF_UPDATE_TMR,
                                            CLI_RIP_DEF_HOLD_TMR,
                                            CLI_RIP_DEF_GARBAGE_TMR);
                break;

            case CLI_RIP_DEF_ROUTE_ORIGINATE:
                /* args[0] - metric with which default roue will be
                 * propagated */
                i4RetStatus = RipDefaultRouteOriginate (CliHandle, i4IfaceIndex,
                                                        *(INT4 *) args[0]);
                break;

            case CLI_RIP_NO_DEF_ROUTE_ORIGINATE:
                /* Setting metric value of zero disables origination of
                 * default route over the interface */
                i4RetStatus = RipDefaultRouteOriginate (CliHandle, i4IfaceIndex,
                                                        CLI_RIP_RESTRICT_DEF_ROUTE);
                break;

            case CLI_RIP_SUMMARY_ADDRESS:
                /* args[0] - aggregated address 
                 * args[1] - mask for subnet for which aggregation is done */
                i4RetStatus = RipSummaryAddress (CliHandle, i4IfaceIndex,
                                                 *args[0], *args[1]);
                break;

            case CLI_RIP_NO_SUMMARY_ADDRESS:
                /* args[0] - aggregated address 
                 * args[1] - mask for  subnet for which aggregation is done */
                i4RetStatus = RipNoSummaryAddress (CliHandle, i4IfaceIndex,
                                                   *args[0], *args[1]);
                break;

            case CLI_RIP_ENABLE_AUTO_SUMMARY:
                i4RetStatus = RipSetAutoSummary (CliHandle,
                                                 RIP_AUTO_SUMMARY_ENABLE);
                break;

            case CLI_RIP_DISABLE_AUTO_SUMMARY:
                i4RetStatus = RipSetAutoSummary (CliHandle,
                                                 RIP_AUTO_SUMMARY_DISABLE);
                break;

            case CLI_RIP_ROUTE_DISTANCE:
            {
                /* args[0] - distance value (1-255)
                 * args[1] - route map name (optional)
                 */
                i4RetStatus =
                    RipSetRouteDistance (CliHandle, *(INT4 *) args[0],
                                         (UINT1 *) args[1]);
            }
                break;

            case CLI_RIP_NO_ROUTE_DISTANCE:
            {
                /* args[0] - route map name (optional)
                 */
                i4RetStatus =
                    RipSetNoRouteDistance (CliHandle, (UINT1 *) args[0]);
            }
                break;

            default:
                CliPrintf (CliHandle, "\r% Invalid Command\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
        }
        RipResetContext ();
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RIP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", RipCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    RipUnLock ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}

/**************************************************************************/
/*   Function Name :ShowRipProtocolInfoInCxt                              */
/*                                                                        */
/*   Description   :To display IP RIP protocol information                */
/*                                                                        */
/*   Input(s)      : Context Id                                           */
/*   Output(s)     : .If Success none else Error                          */
/*   Return Values : CLI_SUCCESS/CLI_FAILURE                              */
/* ************************************************************************/
INT4
ShowRipProtocolInfoInCxt (tCliHandle CliHandle, INT4 i4ContextId)
{

    INT4                i4Val;
    UINT4               u4RipIfAddr;
    UINT4               u4PrevRipIfAddr = 0;
    UINT4               u4RipIfGarbgTmr;
    UINT4               u4RipIfRouteAgeTmr;
    UINT4               u4RipIfUpdateTmr;
    UINT4               u4Mask;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4Port;
    INT4                i4RipIfAuthType;
    INT4                i4RipIfSendType;
    INT4                i4RipIfRecvType;
    INT4                i4Redistribute;
    INT4                i4Horizon;
    INT4                i4DefRtInstall;
    INT4                i4IfAdminStatus;
    INT4                i4DefaultMetric;
    INT4                i4NxtCxtId = 0;
    INT4                i4AdminStat = 0;
    UINT4               u4Neighbors;
    UINT4               u4RetValue;
    UINT4               u4RipPrevIfAddr;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pu1IpAddr = NULL;
    tNetIpv4IfInfo      pNetIpIfInfo;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET ((UINT1 *) &pNetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    if ((nmhGetFsMIRipAdminStatus (i4ContextId, &i4AdminStat) ==
         SNMP_FAILURE) || (i4AdminStat == RIP_ADMIN_DISABLE))
    {
        return CLI_SUCCESS;
    }

    if ((RipGetVrfNameFrmCxtId (i4ContextId, au1RipCxtName)) == RIP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, " \r\nVrf  %s\r\n", au1RipCxtName);

    /* Display the global RIP configuration information */
    nmhGetFsMIRip2Security (i4ContextId, &i4Val);
    CliPrintf (CliHandle, " RIP2 security level is %s\r\n",
               (i4Val == RIP_MINIMUM_SECURITY) ? "Minimum" : "Maximum");

    /* Display route redistribution */
    CliPrintf (CliHandle, " Redistributing : rip");

    nmhGetFsMIRipRRDSrcProtoMaskEnable (i4ContextId, &i4Redistribute);
    if ((i4Redistribute & RIP_RRD_BGP_MASK) == RIP_RRD_BGP_MASK)
    {
        CliPrintf (CliHandle, ", bgp");
    }
    if ((i4Redistribute & RIP_RRD_DIRECT_MASK) == RIP_RRD_DIRECT_MASK)
    {
        CliPrintf (CliHandle, ", connected");
    }
    if ((i4Redistribute & RIP_RRD_OSPF_MASK) == RIP_RRD_OSPF_MASK)
    {
        CliPrintf (CliHandle, ", ospf");
    }
    if ((i4Redistribute & RIP_RRD_STATIC_MASK) == RIP_RRD_STATIC_MASK)
    {
        CliPrintf (CliHandle, ", static");
    }

    CliPrintf (CliHandle, "\r\n");

    nmhGetFsMIRip2SpacingEnable (i4ContextId, &i4Val);
    CliPrintf (CliHandle, " Output Delay is %s\r\n",
               (i4Val == RIP_SPACING_ENABLE) ? "enabled" : "disabled");

    /* Display the retransmission timeout interval */
    nmhGetFsMIRip2RetransTimeoutInt (i4ContextId, &i4Val);
    CliPrintf (CliHandle,
               " Retransmission timeout interval is %d seconds\r\n", i4Val);

    /* Display the number of retransmission retries */
    nmhGetFsMIRip2MaxRetransmissions (i4ContextId, &i4Val);
    CliPrintf (CliHandle, " Number of retransmission retries is %d \r\n",
               i4Val);

    /* Display the default metric */
    nmhGetFsMIRipRRDRouteDefMetric (i4ContextId, &i4Val);
    CliPrintf (CliHandle, " Default metric is %d \r\n", i4Val);

    nmhGetFsMIRip2AutoSummaryStatus (i4ContextId, &i4Val);
    if (i4Val == RIP_AUTO_SUMMARY_DISABLE)
    {
        CliPrintf (CliHandle, " Auto-Summarization of routes is disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Auto-Summarization of routes is enabled\r\n");
    }
    /* Display routing for networks */
    /* Walk through all interfaces on which RIP is enabled, obtain the routing 
     * for networks by ANDING the ipaddress with the subnet mask configured for
     * that interface 
     */
    CliPrintf (CliHandle, " Routing for Networks :\r\n");

    u4PrevRipIfAddr = 0;
    i4NxtCxtId = 0;

    while (nmhGetNextIndexFsMIRip2IfConfTable
           (i4ContextId, &i4NxtCxtId, u4PrevRipIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4ContextId != i4NxtCxtId)
        {
            break;
        }
        /* Get the if table entry for this IP address. Then obtain the 
         * subnet mask and AND it with IP address and display
         */
        if ((i4Port = RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NxtCxtId,
                                                          u4RipIfAddr)) !=
            RIP_FAILURE)
        {
            u4RetValue = u4RipIfAddr;
            pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) i4Port);
            if (pRipCxtEntry == NULL)
            {
                break;
            }
            pRipIfRec = RipGetIfRec ((UINT4) i4Port, pRipCxtEntry);
            if (pRipIfRec == NULL)
            {
                break;
            }
            if (pRipIfRec != NULL)
            {
                u4Mask = pRipIfRec->u4NetMask;
                u4RetValue = u4RetValue & u4Mask;
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RetValue);
                CliPrintf (CliHandle, "   %s\r\n", pu1IpAddr);
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
            return (CLI_FAILURE);
        }
        u4PrevRipIfAddr = u4RipIfAddr;

    }

    /* Display the RIP Neighbor information */
    CliPrintf (CliHandle, " Routing Information Sources :\r\n");
    ShowRipIfAggregationCxt (CliHandle, i4ContextId);
    pRipCxtEntry = RipGetCxtInfoRec (i4ContextId);
    if (pRipCxtEntry != NULL)
    {
        CliPrintf (CliHandle, " Trusted Neighbors :\r \n");
        for (u4Neighbors = 0;
             u4Neighbors <
             ((pRipCxtEntry->RipNbrListCfg).u4NumAuthorizedNbrs); u4Neighbors++)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                       pRipCxtEntry->RipNbrListCfg.
                                       aNbrList[u4Neighbors].
                                       u4FsRip2TrustNBRIpAddr);
            CliPrintf (CliHandle, "  %s\r\n", pu1IpAddr);
        }
    }

    /* Display interface specific RIP configuration information */
    i4NxtCxtId = 0;
    u4RipPrevIfAddr = 0;

    while (nmhGetNextIndexFsMIRip2IfConfTable
           (i4ContextId, &i4NxtCxtId, u4RipPrevIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4ContextId != i4NxtCxtId)
        {
            break;
        }

        /* Get the if table entry for this IP address. 
         */
        if ((i4Port = RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NxtCxtId,
                                                          u4RipIfAddr)) !=
            RIP_FAILURE)
        {

            if ((NetIpv4GetIfInfo ((UINT4) i4Port, &pNetIpIfInfo) ==
                 NETIPV4_FAILURE) ||
                (NetIpv4GetCfaIfIndexFromPort ((UINT4) i4Port, &u4CfaIfIndex) ==
                 NETIPV4_FAILURE))

            {
                CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
                return (CLI_FAILURE);
            }

            CfaCliGetIfName (u4CfaIfIndex, (INT1 *) au1IfName);
            CliPrintf (CliHandle, " Interface %s\r\n", au1IfName);
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
            CliPrintf (CliHandle, " RIP Network :\r \n");
            CliPrintf (CliHandle, "   %s\r\n", pu1IpAddr);

            nmhGetFsMIRip2IfConfUpdateTmr (i4ContextId, u4RipIfAddr,
                                           (INT4 *) &u4RipIfUpdateTmr);

            CliPrintf (CliHandle,
                       "  Sending updates every %d seconds\r\n",
                       u4RipIfUpdateTmr);

            nmhGetFsMIRip2IfConfRouteAgeTmr (i4ContextId, u4RipIfAddr,
                                             (INT4 *) &u4RipIfRouteAgeTmr);

            CliPrintf (CliHandle, "  Invalid after %d seconds\r\n",
                       u4RipIfRouteAgeTmr);

            nmhGetFsMIRip2IfConfGarbgCollectTmr (i4ContextId, u4RipIfAddr,
                                                 (INT4 *) &u4RipIfGarbgTmr);
            CliPrintf (CliHandle, "  Flushed after %d seconds\r\n",
                       u4RipIfGarbgTmr);

            nmhGetFsMIStdRip2IfConfSend (i4ContextId, u4RipIfAddr,
                                         &i4RipIfSendType);

            nmhGetFsMIStdRip2IfConfReceive (i4ContextId, u4RipIfAddr,
                                            &i4RipIfRecvType);

            CliPrintf (CliHandle, "  Send version is ");
            switch (i4RipIfSendType)
            {
                case RIPIF_DO_NOT_SEND:
                    CliPrintf (CliHandle, "none, ");
                    break;

                case RIPIF_VERSION_1_SND:
                    CliPrintf (CliHandle, "1, ");
                    break;

                case RIPIF_RIP1_COMPATIBLE:
                    CliPrintf (CliHandle, "1 2, ");
                    break;

                case RIPIF_VERSION_2_SND:
                    CliPrintf (CliHandle, "2, ");
                    break;
                default:
                    break;
            }

            CliPrintf (CliHandle, "receive version is ");

            switch (i4RipIfRecvType)
            {
                case RIPIF_VERSION_1_RCV:
                    CliPrintf (CliHandle, "1\r\n");
                    break;

                case RIPIF_VERSION_2_RCV:
                    CliPrintf (CliHandle, "2\r\n");
                    break;

                case RIPIF_1_OR_2:
                    CliPrintf (CliHandle, "1 2\r\n");
                    break;

                case RIPIF_DO_NOT_RECV:
                    CliPrintf (CliHandle, "none\r\n");
                    break;
                default:
                    break;
            }

            nmhGetFsMIStdRip2IfConfAuthType (i4ContextId, u4RipIfAddr,
                                             &i4RipIfAuthType);

            /* Display the authentication type */

            switch (i4RipIfAuthType)
            {
                case RIPIF_NO_AUTHENTICATION:
                    CliPrintf (CliHandle, "  Authentication type is none\r\n");
                    break;

                case RIPIF_SIMPLE_PASSWORD:
                    CliPrintf (CliHandle,
                               "  Authentication type is simple text\r\n");
                    break;

                case RIPIF_MD5_AUTHENTICATION:
                    CliPrintf (CliHandle, "  Authentication type is md5\r\n");
                    break;

                case RIPIF_SHA1_AUTHENTICATION:
                    CliPrintf (CliHandle, "  Authentication type is sha1\r\n");
                    break;

                case RIPIF_SHA256_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "  Authentication type is sha256\r\n");
                    break;

                case RIPIF_SHA384_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "  Authentication type is sha384\r\n");
                    break;

                case RIPIF_SHA512_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "  Authentication type is sha512\r\n");
                    break;

                default:
                    break;
            }

            nmhGetFsMIRip2IfAdminStat (i4ContextId, u4RipIfAddr,
                                       &i4IfAdminStatus);
            if (i4IfAdminStatus == RIP_ADMIN_PASSIVE)
            {
                CliPrintf (CliHandle, "  Passive interface is enabled\r\n");
            }

            nmhGetFsMIRip2IfSplitHorizonStatus (i4ContextId, u4RipIfAddr,
                                                &i4Horizon);
            CliPrintf (CliHandle, "  Split Horizon ");
            switch (i4Horizon)
            {
                case RIP_SPLIT_HORIZON:
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, "is enabled\r\n");
                    break;

                case RIP_NO_SPLIT_HORIZON:
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, "is disabled\r\n");
                    break;

                case RIP_SPLIT_HORZ_WITH_POIS_REV:
                    u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                        "with poissoned reverse is enabled\r\n");
                    break;
                default:
                    break;
            }

            nmhGetFsMIRip2IfConfDefRtInstall (i4ContextId, u4RipIfAddr,
                                              &i4DefRtInstall);

            if (i4DefRtInstall == RIP_INSTALL_DEF_RT)
            {
                CliPrintf (CliHandle, "  Installs default route received\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Restricts default route installation\r\n ");
            }

            nmhGetFsMIStdRip2IfConfDefaultMetric (i4ContextId, u4RipIfAddr,
                                                  &i4DefaultMetric);
            if (i4DefaultMetric != RIP_NO_DEF_RT_ORIGINATE)
            {
                CliPrintf (CliHandle, "  Originate default route\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Restricts default route origination\r\n");
            }
            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt */
                break;
            }

        }
        else
        {
            CliPrintf (CliHandle, "%% Invalid interface\r\n");
        }

        u4RipPrevIfAddr = u4RipIfAddr;
    }

    return (CLI_SUCCESS);
}

/**************************************************************************/
/*   Function Name :ShowRipProtocolInfo                                   */
/*                                                                        */
/*   Description   :To display IP RIP protocol information                */
/*                                                                        */
/*   Input(s)      : u4RipIfAddr - Contains IP Addr                       */
/*   Output(s)     : .If Success none else Error                          */
/*   Return Values : CLI_SUCCESS/CLI_FAILURE                              */
/* ************************************************************************/
INT4
ShowRipProtocolInfo (tCliHandle CliHandle)
{
    INT4                i4Val;
    UINT4               u4RipIfAddr;
    UINT4               u4PrevRipIfAddr = 0;
    UINT4               u4RipIfGarbgTmr;
    UINT4               u4RipIfRouteAgeTmr;
    UINT4               u4RipIfUpdateTmr;
    UINT4               u4Mask;
    INT4                i4Port;
    INT4                i4RipIfAuthType;
    INT4                i4RipIfSendType;
    INT4                i4RipIfRecvType;
    INT4                i4Redistribute;
    INT4                i4Horizon;
    INT4                i4DefRtInstall;
    INT4                i4IfAdminStatus;
    INT4                i4DefaultMetric;
    INT4                i4PrevCxtId = 0;
    INT4                i4NxtCxtId = 0;
    INT4                i4ContextId = 0;
    INT4                i4AdminStat = 0;
    UINT4               u4Neighbors;
    UINT4               u4RetValue;
    UINT4               u4RipPrevIfAddr;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4CfaIfIndex = 0;
    CHR1               *pu1IpAddr = NULL;
    tNetIpv4IfInfo      pNetIpIfInfo;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    MEMSET ((UINT1 *) &pNetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    /* Display the global RIP configuration information */

    if (nmhGetFirstIndexFsMIRip2GlobalTable (&i4ContextId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if ((nmhGetFsMIRipAdminStatus (i4ContextId, &i4AdminStat) ==
             SNMP_FAILURE) || (i4AdminStat == RIP_ADMIN_DISABLE))
        {
            continue;
        }

        CliPrintf (CliHandle, "\r\n\nRouting Protocol is rip\r\n");

        if ((RipGetVrfNameFrmCxtId (i4ContextId, au1RipCxtName)) == RIP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, " \r\nVrf  %s\r\n", au1RipCxtName);

        nmhGetFsMIRip2Security (i4ContextId, &i4Val);
        CliPrintf (CliHandle, " RIP2 security level is %s\r\n",
                   (i4Val == RIP_MINIMUM_SECURITY) ? "Minimum" : "Maximum");

        /* Display route redistribution */
        CliPrintf (CliHandle, " Redistributing : rip");

        nmhGetFsMIRipRRDSrcProtoMaskEnable (i4ContextId, &i4Redistribute);
        if ((i4Redistribute & RIP_RRD_BGP_MASK) == RIP_RRD_BGP_MASK)
        {
            CliPrintf (CliHandle, ", bgp");
        }
        if ((i4Redistribute & RIP_RRD_DIRECT_MASK) == RIP_RRD_DIRECT_MASK)
        {
            CliPrintf (CliHandle, ", connected");
        }
        if ((i4Redistribute & RIP_RRD_OSPF_MASK) == RIP_RRD_OSPF_MASK)
        {
            CliPrintf (CliHandle, ", ospf");
        }
        if ((i4Redistribute & RIP_RRD_STATIC_MASK) == RIP_RRD_STATIC_MASK)
        {
            CliPrintf (CliHandle, ", static");
        }

        CliPrintf (CliHandle, "\r\n");

        nmhGetFsMIRip2SpacingEnable (i4ContextId, &i4Val);
        CliPrintf (CliHandle, " Output Delay is %s\r\n",
                   (i4Val == RIP_SPACING_ENABLE) ? "enabled" : "disabled");

        /* Display the retransmission timeout interval */
        nmhGetFsMIRip2RetransTimeoutInt (i4ContextId, &i4Val);
        CliPrintf (CliHandle,
                   " Retransmission timeout interval is %d seconds\r\n", i4Val);

        /* Display the number of retransmission retries */
        nmhGetFsMIRip2MaxRetransmissions (i4ContextId, &i4Val);
        CliPrintf (CliHandle, " Number of retransmission retries is %d \r\n",
                   i4Val);

        /* Display the default metric */
        nmhGetFsMIRipRRDRouteDefMetric (i4ContextId, &i4Val);
        CliPrintf (CliHandle, " Default metric is %d \r\n", i4Val);

        nmhGetFsMIRip2AutoSummaryStatus (i4ContextId, &i4Val);
        if (i4Val == RIP_AUTO_SUMMARY_DISABLE)
        {
            CliPrintf (CliHandle,
                       " Auto-Summarization of routes is disabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Auto-Summarization of routes is enabled\r\n");
        }

        /* Display routing for networks */
        /* Walk through all interfaces on which RIP is enabled, obtain the routing 
         * for networks by ANDING the ipaddress with the subnet mask configured for
         * that interface 
         */
        CliPrintf (CliHandle, " Routing for Networks :\r\n");

        u4PrevRipIfAddr = 0;
        i4NxtCxtId = 0;

        while (nmhGetNextIndexFsMIRip2IfConfTable
               (i4ContextId, &i4NxtCxtId, u4PrevRipIfAddr,
                &u4RipIfAddr) == SNMP_SUCCESS)
        {
            if (i4ContextId != i4NxtCxtId)
            {
                break;
            }
            /* Get the if table entry for this IP address. Then obtain the 
             * subnet mask and AND it with IP address and display
             */
            if ((i4Port =
                 RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NxtCxtId,
                                                     u4RipIfAddr)) !=
                RIP_FAILURE)
            {
                u4RetValue = u4RipIfAddr;
                pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) i4Port);
                if (pRipCxtEntry == NULL)
                {
                    break;
                }
                if ((pRipIfRec =
                     RipGetIfRec ((UINT4) i4Port, pRipCxtEntry)) != NULL)
                {
                    u4Mask = pRipIfRec->u4NetMask;
                    u4RetValue = u4RetValue & u4Mask;
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RetValue);
                    CliPrintf (CliHandle, "   %s\r\n", pu1IpAddr);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
                return (CLI_FAILURE);
            }
            u4PrevRipIfAddr = u4RipIfAddr;

        }

        /* Display the RIP Neighbor information */
        CliPrintf (CliHandle, " Routing Information Sources :\r\n");
        ShowRipIfAggregationCxt (CliHandle, i4ContextId);
        pRipCxtEntry = RipGetCxtInfoRec (i4ContextId);
        if (pRipCxtEntry != NULL)
        {
            CliPrintf (CliHandle, " Trusted Neighbors :\r \n");
            for (u4Neighbors = 0;
                 u4Neighbors <
                 ((pRipCxtEntry->RipNbrListCfg).u4NumAuthorizedNbrs);
                 u4Neighbors++)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                           pRipCxtEntry->RipNbrListCfg.
                                           aNbrList[u4Neighbors].
                                           u4FsRip2TrustNBRIpAddr);
                CliPrintf (CliHandle, "  %s\r\n", pu1IpAddr);
            }
        }

        /* Display interface specific RIP configuration information */

        i4NxtCxtId = 0;
        u4RipPrevIfAddr = 0;

        while (nmhGetNextIndexFsMIRip2IfConfTable
               (i4ContextId, &i4NxtCxtId, u4RipPrevIfAddr,
                &u4RipIfAddr) == SNMP_SUCCESS)
        {
            if (i4ContextId != i4NxtCxtId)
            {
                break;
            }

            /* Get the if table entry for this IP address. 
             */
            if ((i4Port =
                 RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NxtCxtId,
                                                     u4RipIfAddr)) !=
                RIP_FAILURE)
            {

                if ((NetIpv4GetIfInfo ((UINT4) i4Port, &pNetIpIfInfo) ==
                     NETIPV4_FAILURE) ||
                    (NetIpv4GetCfaIfIndexFromPort
                     ((UINT4) i4Port, &u4CfaIfIndex) == NETIPV4_FAILURE))
                {
                    CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
                    return (CLI_FAILURE);
                }

                CfaCliGetIfName (u4CfaIfIndex, (INT1 *) au1IfName);
                CliPrintf (CliHandle, " Interface %s\r\n", au1IfName);
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle, " RIP Network :\r \n");
                CliPrintf (CliHandle, "   %s\r\n", pu1IpAddr);

                nmhGetFsMIRip2IfConfUpdateTmr (i4ContextId, u4RipIfAddr,
                                               (INT4 *) &u4RipIfUpdateTmr);

                CliPrintf (CliHandle,
                           "  Sending updates every %d seconds\r\n",
                           u4RipIfUpdateTmr);

                nmhGetFsMIRip2IfConfRouteAgeTmr (i4ContextId, u4RipIfAddr,
                                                 (INT4 *) &u4RipIfRouteAgeTmr);

                CliPrintf (CliHandle, "  Invalid after %d seconds\r\n",
                           u4RipIfRouteAgeTmr);

                nmhGetFsMIRip2IfConfGarbgCollectTmr (i4ContextId, u4RipIfAddr,
                                                     (INT4 *) &u4RipIfGarbgTmr);
                CliPrintf (CliHandle, "  Flushed after %d seconds\r\n",
                           u4RipIfGarbgTmr);

                nmhGetFsMIStdRip2IfConfSend (i4ContextId, u4RipIfAddr,
                                             &i4RipIfSendType);

                nmhGetFsMIStdRip2IfConfReceive (i4ContextId, u4RipIfAddr,
                                                &i4RipIfRecvType);

                CliPrintf (CliHandle, "  Send version is ");
                switch (i4RipIfSendType)
                {
                    case RIPIF_DO_NOT_SEND:
                        CliPrintf (CliHandle, "none, ");
                        break;

                    case RIPIF_VERSION_1_SND:
                        CliPrintf (CliHandle, "1, ");
                        break;

                    case RIPIF_RIP1_COMPATIBLE:
                        CliPrintf (CliHandle, "1 2, ");
                        break;

                    case RIPIF_VERSION_2_SND:
                        CliPrintf (CliHandle, "2, ");
                        break;
                    default:
                        break;
                }

                CliPrintf (CliHandle, "receive version is ");

                switch (i4RipIfRecvType)
                {
                    case RIPIF_VERSION_1_RCV:
                        CliPrintf (CliHandle, "1\r\n");
                        break;

                    case RIPIF_VERSION_2_RCV:
                        CliPrintf (CliHandle, "2\r\n");
                        break;

                    case RIPIF_1_OR_2:
                        CliPrintf (CliHandle, "1 2\r\n");
                        break;

                    case RIPIF_DO_NOT_RECV:
                        CliPrintf (CliHandle, "none\r\n");
                        break;
                    default:
                        break;
                }

                nmhGetFsMIStdRip2IfConfAuthType (i4ContextId, u4RipIfAddr,
                                                 &i4RipIfAuthType);

                /* Display the authentication type */

                switch (i4RipIfAuthType)
                {
                    case RIPIF_NO_AUTHENTICATION:
                        CliPrintf (CliHandle,
                                   "  Authentication type is none\r\n");
                        break;

                    case RIPIF_SIMPLE_PASSWORD:
                        CliPrintf (CliHandle,
                                   "  Authentication type is simple text\r\n");
                        break;

                    case RIPIF_MD5_AUTHENTICATION:
                        CliPrintf (CliHandle,
                                   "  Authentication type is md5\r\n");
                        break;

                    case RIPIF_SHA1_AUTHENTICATION:
                        CliPrintf (CliHandle,
                                   "  Authentication type is sha1\r\n");
                        break;

                    case RIPIF_SHA256_AUTHENTICATION:
                        CliPrintf (CliHandle,
                                   "  Authentication type is sha256\r\n");
                        break;

                    case RIPIF_SHA384_AUTHENTICATION:
                        CliPrintf (CliHandle,
                                   "  Authentication type is sha384\r\n");
                        break;

                    case RIPIF_SHA512_AUTHENTICATION:
                        CliPrintf (CliHandle,
                                   "  Authentication type is sha512\r\n");
                        break;

                    default:
                        break;
                }

                nmhGetFsMIRip2IfAdminStat (i4ContextId, u4RipIfAddr,
                                           &i4IfAdminStatus);
                if (i4IfAdminStatus == RIP_ADMIN_PASSIVE)
                {
                    CliPrintf (CliHandle, "  Passive interface is enabled\r\n");
                }

                nmhGetFsMIRip2IfSplitHorizonStatus (i4ContextId, u4RipIfAddr,
                                                    &i4Horizon);
                CliPrintf (CliHandle, "  Split Horizon ");
                switch (i4Horizon)
                {
                    case RIP_SPLIT_HORIZON:
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, "is enabled\r\n");
                        break;

                    case RIP_NO_SPLIT_HORIZON:
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, "is disabled\r\n");
                        break;

                    case RIP_SPLIT_HORZ_WITH_POIS_REV:
                        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                            "with poissoned reverse is enabled\r\n");
                        break;
                    default:
                        break;
                }

                nmhGetFsMIRip2IfConfDefRtInstall (i4ContextId, u4RipIfAddr,
                                                  &i4DefRtInstall);

                if (i4DefRtInstall == RIP_INSTALL_DEF_RT)
                {
                    CliPrintf (CliHandle,
                               "  Installs default route received\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "  Restricts default route installation\r\n ");
                }

                nmhGetFsMIStdRip2IfConfDefaultMetric (i4ContextId, u4RipIfAddr,
                                                      &i4DefaultMetric);
                if (i4DefaultMetric != RIP_NO_DEF_RT_ORIGINATE)
                {
                    CliPrintf (CliHandle, "  Originate default route\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "  Restricts default route origination\r\n");
                }
                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User pressed 'q' at more prompt */
                    break;
                }

            }
            else
            {
                CliPrintf (CliHandle, "%% Invalid interface\r\n");
            }

            u4RipPrevIfAddr = u4RipIfAddr;
        }
        i4PrevCxtId = i4ContextId;
    }
    while ((nmhGetNextIndexFsMIRip2GlobalTable (i4PrevCxtId, &i4ContextId) ==
            SNMP_SUCCESS));

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : ShowRipIfAggregationCxt                                  */
/*                                                                         */
/*   Description   : Displays the interface specific aggregation configured*/
/*                   over each interface                                   */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
ShowRipIfAggregationCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4IfIndex = 0;
    UINT4               u4AggAddr = 0;
    UINT4               u4AggMask = 0;
    UINT4               u4MaskBits = 0;
    UINT4               u4Port = 0;
    INT4                i4NxtCxtId = 0;
    CHR1               *pu1IpAddr = NULL;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4GetPortIfIn;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    CliPrintf (CliHandle, "\r Interface Specific Address Summarization : \r\n");

    if (nmhGetFirstIndexFsMIRipAggTable (&i4NxtCxtId, &i4IfIndex, &u4AggAddr,
                                         &u4AggMask) == SNMP_SUCCESS)
    {
        do
        {
            if (i4CxtId == i4NxtCxtId)
            {
                u4MaskBits = CliGetMaskBits (u4AggMask);
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4AggAddr);
                i4GetPortIfIn =
                    NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port);
                UNUSED_PARAM (i4GetPortIfIn);
                CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1IfName);
                if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) != NETIPV4_FAILURE)
                {
                    CliPrintf (CliHandle, "\r   %s/%-2u   for %s\r\n",
                               pu1IpAddr, u4MaskBits, au1IfName);
                }
            }
        }
        while ((nmhGetNextIndexFsMIRipAggTable (i4NxtCxtId,
                                                &i4NxtCxtId,
                                                i4IfIndex,
                                                &i4IfIndex,
                                                u4AggAddr,
                                                &u4AggAddr,
                                                u4AggMask,
                                                &u4AggMask) == SNMP_SUCCESS) &&
               (i4NxtCxtId <= i4CxtId));
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*   Function Name : RipShowAuthenticationInCxt                           */
/*                                                                        */
/*   Description   :To display IP RIP protocol crypto authentication info */
/*                                                                        */
/*   Input(s)      : CliHandle - CLI Context Id.                          */
/*                   i4CxtId - RIP Context Id                             */
/*   Output(s)     : If Success none else Error                           */
/*                                                                        */
/*   Return Values : .CLI_SUCCESS/CLI_FAILURE                             */
/* ************************************************************************/
INT4
RipShowAuthenticationInCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    tSNMP_OCTET_STRING_TYPE AuthInfo;
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    INT4                i4NxtCxtId = 0;
    INT4                i4AllCxt = RIP_FALSE;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    UINT4               u4AuthAddress = 0;
    INT4                i4KeyId = 0;
    INT4                i4InUseKeyId = 0;
    INT4                i4LastKeyStatus = 0;
    INT4                i4AuthType = 0;
    INT4                i4RetVal = 0;
    UINT1               au1IfName[10];
    UINT1               au1AuthInfo[RIP_DST_TIME_LEN + 1];
    INT4                i4VrfNameFrmCxtId;

    MEMSET (au1AuthInfo, 0, sizeof (au1AuthInfo));
    AuthInfo.pu1_OctetList = au1AuthInfo;
    AuthInfo.i4_Length = 0;

    if (i4CxtId == (INT4) RIP_INVALID_CXT_ID)
    {
        i4AllCxt = RIP_TRUE;
        if (nmhGetFirstIndexFsMIRipCryptoAuthTable
            (&i4NxtCxtId, &i4IfIndex, &u4AuthAddress, &i4KeyId) != SNMP_SUCCESS)
        {

            RipShowSimpleAuthenticationInCxt (CliHandle, i4CxtId);
            return (CLI_SUCCESS);
        }
    }
    else
    {
        i4RetVal = (nmhGetNextIndexFsMIRipCryptoAuthTable
                    (i4CxtId, &i4NxtCxtId,
                     i4PrevIfIndex, &i4IfIndex,
                     u4AuthAddress, &u4AuthAddress, i4KeyId, &i4KeyId));
        if ((i4NxtCxtId != i4CxtId) || (i4RetVal != SNMP_SUCCESS))
        {
            RipShowSimpleAuthenticationInCxt (CliHandle, i4CxtId);
            return CLI_SUCCESS;
        }
    }

    do
    {
        if ((i4NxtCxtId != i4CxtId) && (i4AllCxt == RIP_FALSE))
        {
            break;
        }

        if (i4PrevIfIndex != i4IfIndex)
        {
            i4VrfNameFrmCxtId =
                RipGetVrfNameFrmCxtId (i4NxtCxtId, au1RipCxtName);
            UNUSED_PARAM (i4VrfNameFrmCxtId);
            CliPrintf (CliHandle, "\n\rVrf %s\n", au1RipCxtName);

            CfaGetIfAlias ((UINT4) i4IfIndex, au1IfName);
            CliPrintf (CliHandle,
                       "Interface Name                   %s\r\n", au1IfName);

            nmhGetFsMIStdRip2IfConfAuthType (i4NxtCxtId, u4AuthAddress,
                                             &i4AuthType);

            /* Display the authentication type */

            switch (i4AuthType)
            {
                case RIPIF_NO_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "Authentication type is           none\r\n");
                    break;

                case RIPIF_SIMPLE_PASSWORD:
                    CliPrintf (CliHandle,
                               "Authentication type is           simple text\r\n");
                    break;

                case RIPIF_MD5_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "Authentication type is           md5\r\n");
                    break;

                case RIPIF_SHA1_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "Authentication type is           sha1\r\n");
                    break;

                case RIPIF_SHA256_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "Authentication type is           sha256\r\n");
                    break;

                case RIPIF_SHA384_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "Authentication type is           sha384\r\n");
                    break;

                case RIPIF_SHA512_AUTHENTICATION:
                    CliPrintf (CliHandle,
                               "Authentication type is           sha512\r\n");
                    break;
                default:
                    break;

            }

            if (i4AuthType >= RIPIF_MD5_AUTHENTICATION)
            {
                nmhGetFsMIRip2IfConfInUseKey (i4NxtCxtId, u4AuthAddress,
                                              &i4InUseKeyId);
                CliPrintf (CliHandle,
                           "Authentication KeyId in use:     %d\r\n",
                           i4InUseKeyId);
                nmhGetFsMIRip2IfConfAuthLastKeyStatus (i4NxtCxtId,
                                                       u4AuthAddress,
                                                       &i4LastKeyStatus);
                if (i4LastKeyStatus == 1)
                {
                    CliPrintf (CliHandle,
                               "Authentication Last key status:  true\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Authentication Last key status:  false\r\n");
                }
            }

            MEMSET (au1AuthInfo, 0, sizeof (au1AuthInfo));
            CliPrintf (CliHandle, "\r\nRIP Authentication Key Info:\r\n");
            CliPrintf (CliHandle, "----------------------------\r\n");
            CliPrintf (CliHandle, "Authentication KeyId        %d\r\n",
                       i4KeyId);
            nmhGetFsMIRipCryptoKeyStartAccept (i4NxtCxtId, i4IfIndex,
                                               u4AuthAddress, i4KeyId,
                                               &AuthInfo);
            CliPrintf (CliHandle, "Start Accept Time           %s\r\n",
                       AuthInfo.pu1_OctetList);
            nmhGetFsMIRipCryptoKeyStartGenerate (i4NxtCxtId, i4IfIndex,
                                                 u4AuthAddress, i4KeyId,
                                                 &AuthInfo);
            CliPrintf (CliHandle, "Start Generate Time         %s\r\n",
                       AuthInfo.pu1_OctetList);
            nmhGetFsMIRipCryptoKeyStopGenerate (i4NxtCxtId, i4IfIndex,
                                                u4AuthAddress, i4KeyId,
                                                &AuthInfo);
            CliPrintf (CliHandle, "Stop Generate Time          %s\r\n",
                       AuthInfo.pu1_OctetList);
            nmhGetFsMIRipCryptoKeyStopAccept (i4NxtCxtId, i4IfIndex,
                                              u4AuthAddress, i4KeyId,
                                              &AuthInfo);
            CliPrintf (CliHandle, "Stop Accept Time            %s\r\n",
                       AuthInfo.pu1_OctetList);
            CliPrintf (CliHandle, "\r\n");
        }

        i4PrevIfIndex = i4IfIndex;
    }
    while (nmhGetNextIndexFsMIRipCryptoAuthTable
           (i4NxtCxtId, &i4NxtCxtId,
            i4PrevIfIndex, &i4IfIndex,
            u4AuthAddress, &u4AuthAddress, i4KeyId, &i4KeyId) == SNMP_SUCCESS);

    RipShowSimpleAuthenticationInCxt (CliHandle, i4CxtId);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/**************************************************************************/
/*   Function Name : RipShowStatisticsInCxt                               */
/*                                                                        */
/*   Description   :To display IP RIP protocol statistics                 */
/*                                                                        */
/*   Input(s)      : CliHandle - CLI Context Id.                          */
/*                   i4CxtId - RIP Context Id                            */
/*   Output(s)     : If Success none else Error                          */
/*                                                                        */
/*   Return Values : .CLI_SUCCESS/CLI_FAILURE                             */
/* ************************************************************************/
INT4
RipShowStatisticsInCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    UINT4               u4GlobalRouteChange;
    UINT4               u4GlobalQueries;
    UINT4               u4NoOfDroppedPkts;
    UINT4               u4BadAuthPkts = 0;
    UINT4               u4RipIfAddr = 0;
    UINT4               u4BadRoutes;
    UINT4               u4SentUpdates;
    INT4                i4RowStatus;
    INT4                i4AllCxt = RIP_FALSE;
    INT4                i4CurCxtId = 0, i4PrevCxtId = (INT4) RIP_INVALID_CXT_ID;
    UINT4               u4PeriodicUpdates;
    UINT4               u4BadPkts;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pu1IpAddr = NULL;
    UINT1               au1AdminStatus[RIP_MAX_NAME_LENGTH];
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    UINT4               u4RequestReceived;
    UINT4               u4ResponseReceived;
    UINT4               u4UpdateRequestReceived;
    UINT4               u4UpdateResponseReceived;
    UINT4               u4UpdatesAcknowledged;
    tRipCxt            *pRipCxtEntry = NULL;
    tRipInFailCount     InFailcount;
    tRipInBadRoutes     InBadRoutes;
    tRipInBadPkts       InBadPkts;
    tRipIfaceRec       *pRipIfRec = NULL;
    UNUSED_PARAM (u4BadPkts);
    UNUSED_PARAM (u4BadRoutes);
    MEMSET (&InBadPkts, 0, sizeof (tRipInBadPkts));
    MEMSET (&InBadRoutes, 0, sizeof (tRipInBadRoutes));
    MEMSET (&InFailcount, 0, sizeof (tRipInFailCount));

    if (i4CxtId == (INT4) RIP_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIRip2GlobalTable (&i4CxtId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n");
            return (CLI_SUCCESS);
        }
        i4AllCxt = RIP_TRUE;
    }

    if (nmhGetNextIndexFsMIStdRip2IfStatTable (i4CxtId, &i4CurCxtId,
                                               u4RipIfAddr,
                                               &u4RipIfAddr) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {

        if (i4CxtId != i4CurCxtId && i4AllCxt == RIP_FALSE)
        {
            break;
        }
        /* if secondary IP address is enabled in CFA 
         * we need to display the RIP Interface Record for the following else skip it*/
        if (CfaIpIfIsLocalNetInCxt ((UINT4) i4CurCxtId, u4RipIfAddr) !=
            CFA_SUCCESS)
        {
            continue;
        }

        if (i4PrevCxtId != i4CurCxtId)
        {
            if (RipGetVrfNameFrmCxtId (i4CurCxtId, au1RipCxtName) ==
                RIP_FAILURE)
            {
                return CLI_SUCCESS;
            }
            CliPrintf (CliHandle, "\n\rVrf %s\n", au1RipCxtName);

            nmhGetFsMIStdRip2GlobalRouteChanges (i4CurCxtId,
                                                 &u4GlobalRouteChange);
            nmhGetFsMIRip2NumberOfDroppedPkts (i4CurCxtId, &u4NoOfDroppedPkts);
            nmhGetFsMIStdRip2GlobalQueries (i4CurCxtId, &u4GlobalQueries);

            CliPrintf (CliHandle, "\r\nRIP Global Statistics:\r\n");
            CliPrintf (CliHandle, "----------------------\r\n");

            CliPrintf (CliHandle, " Total number of route changes is %u\r\n",
                       u4GlobalRouteChange);

            CliPrintf (CliHandle,
                       " Total number of queries responded is %u\r\n",
                       u4GlobalQueries);

            CliPrintf (CliHandle, " Total number of dropped packets is %u\r\n",
                       u4NoOfDroppedPkts);

            i4PrevCxtId = i4CurCxtId;
        }
        pRipCxtEntry = RipGetCxtInfoRec (i4CurCxtId);
        if (pRipCxtEntry == NULL)
        {
            break;
        }
        pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);
        if (pRipIfRec == NULL)
        {
            return RIP_FAILURE;
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);

        nmhGetFsMIStdRip2IfStatPeriodicUpdates (i4CurCxtId, u4RipIfAddr,
                                                &u4PeriodicUpdates);

        nmhGetFsMIStdRip2IfStatRcvBadRoutes (i4CurCxtId, u4RipIfAddr,
                                             &u4BadRoutes);
        nmhGetFsMIStdRip2IfStatSentUpdates (i4CurCxtId, u4RipIfAddr,
                                            &u4SentUpdates);
        nmhGetFsMIStdRip2IfStatRcvBadPackets (i4CurCxtId, u4RipIfAddr,
                                              &u4BadPkts);
        nmhGetFsMIStdRip2IfStatRcvBadAuthPackets (i4CurCxtId, u4RipIfAddr,
                                                  &u4BadAuthPkts);
        nmhGetFsMIRip2IfAdminStat (i4CurCxtId, u4RipIfAddr, &i4RowStatus);

        RipGetIfStatsRequestReceived (u4RipIfAddr, &u4RequestReceived,
                                      pRipCxtEntry);
        RipGetIfStatsResponseReceived (u4RipIfAddr, &u4ResponseReceived,
                                       pRipCxtEntry);
        RipGetIfStatsUpdateRequestReceived (u4RipIfAddr,
                                            &u4UpdateRequestReceived,
                                            pRipCxtEntry);
        RipGetIfStatsUpdateResponseReceived (u4RipIfAddr,
                                             &u4UpdateResponseReceived,
                                             pRipCxtEntry);
        RipGetIfStatsUpdatesAcknowledged (u4RipIfAddr, &u4UpdatesAcknowledged,
                                          pRipCxtEntry);
        RipGetIfStatsRipFailcount (u4RipIfAddr, &InFailcount, pRipCxtEntry);

        MEMSET (&au1AdminStatus[0], 0, RIP_MAX_NAME_LENGTH);

        switch (i4RowStatus)
        {
            case RIP_ADMIN_ENABLE:
                STRNCPY (au1AdminStatus, "Enabled", STRLEN ("Enabled"));
                au1AdminStatus[STRLEN ("Enabled")] = '\0';
                break;

            case RIP_ADMIN_DISABLE:
                STRNCPY (au1AdminStatus, "Disabled", STRLEN ("Disabled"));
                au1AdminStatus[STRLEN ("Disabled")] = '\0';
                break;

            case RIP_ADMIN_PASSIVE:
                STRNCPY (au1AdminStatus, "Passive", STRLEN ("Passive"));
                au1AdminStatus[STRLEN ("Passive")] = '\0';
                break;
            default:
                break;

        }
        /* Print the header for interface specific statistics */
        CliPrintf (CliHandle, "\r\nRIP Interface Statistics:\r\n");
        CliPrintf (CliHandle, "-------------------------\r\n");
        CliPrintf (CliHandle,
                   "Interface         Periodic       BadRoutes   Triggered      BadPackets   BadAuthPkts   Admin\r\n");

        CliPrintf (CliHandle,
                   "IP Address        Updates Sent   Received    Updates Sent   Received     Received      Status\r\n");

        CliPrintf (CliHandle,
                   "-----------       ------------   ---------   ------------   ----------   --------      --------\r\n");

        u4PagingStatus =
            (UINT4) CliPrintf (CliHandle,
                               "%-22s%-14lu%-12lu%-14lu%-10lu%-15u%-12s\r\n",
                               pu1IpAddr, u4PeriodicUpdates, u4BadRoutes,
                               u4SentUpdates, u4BadPkts, u4BadAuthPkts,
                               au1AdminStatus);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Periodic Updates sent    : %u\r\n",
                   u4PeriodicUpdates);
        CliPrintf (CliHandle, "TOTAL BADROUTES RECEIVED       : %u\r\n",
                   u4BadRoutes);
        CliPrintf (CliHandle, "--------------------------\r\n");
        CliPrintf (CliHandle,
                   "  BadRoutes Received1(Invalid Route Info)            : %u\r\n",
                   pRipIfRec->RipIfaceStats.u4InBadRoutes1);
        CliPrintf (CliHandle,
                   "  BadRoutes Received2(Processing Routes Fails)       : %u\r\n",
                   pRipIfRec->RipIfaceStats.u4InBadRoutes2);
        CliPrintf (CliHandle, "Triggered updates sent   : %u\r\n",
                   u4SentUpdates);
        CliPrintf (CliHandle, "TOTAL BADPACKETS RECEIVED      : %u\r\n",
                   u4BadPkts);
        CliPrintf (CliHandle, "--------------------------\r\n");
        CliPrintf (CliHandle,
                   "  BadPackets Received1(Trailing Junk Received) : %u\r\n",
                   pRipIfRec->RipIfaceStats.u4InBadPackets1);
        CliPrintf (CliHandle,
                   "  BadPackets Received2(Invalid Sending Host)   : %u\r\n",
                   pRipIfRec->RipIfaceStats.u4InBadPackets2);
        CliPrintf (CliHandle,
                   "  BadPackets Received3(Invalid RIP Pkt)        : %u\r\n",
                   pRipIfRec->RipIfaceStats.u4InBadPackets3);
        CliPrintf (CliHandle,
                   "  BadPackets Received4(Peer Updation)          : %u\r\n",
                   pRipIfRec->RipIfaceStats.u4InBadPackets4);
        CliPrintf (CliHandle,
                   "  BadAuthentication Packet Received            : %u\r\n",
                   u4BadAuthPkts);
        CliPrintf (CliHandle, "Admin Status             : %s\r\n",
                   au1AdminStatus);
        CliPrintf (CliHandle, "Request Received         : %u\r\n",
                   u4RequestReceived);
        CliPrintf (CliHandle, "Response Received        : %u\r\n",
                   u4ResponseReceived);
        CliPrintf (CliHandle, "Update Request Received  : %u\r\n",
                   u4UpdateRequestReceived);
        CliPrintf (CliHandle, "Update Response Received : %u\r\n",
                   u4UpdateResponseReceived);
        CliPrintf (CliHandle, "Updates Acknowledged     : %u\r\n",
                   u4UpdatesAcknowledged);
        CliPrintf (CliHandle, "FAILURE COUNTERS\r\n");
        CliPrintf (CliHandle, "---------------------------\r\n");
        if (InFailcount.u4RipV2NoAuthFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2NoAuthFailCount                      : %u\r\n",
                       InFailcount.u4RipV2NoAuthFailCount);
        }
        if (InFailcount.u4RipV1SimplePasswdFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV1SimplePasswdFailCount                : %u\r\n",
                       InFailcount.u4RipV1SimplePasswdFailCount);
        }
        if (InFailcount.u4RipV2UnAuthenticatedPktFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2UnAuthenticatedPktFailCount          : %u\r\n",
                       InFailcount.u4RipV2UnAuthenticatedPktFailCount);
        }
        if (InFailcount.u4RipV2SimplePasswdFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2SimplePasswdFailCount                : %u\r\n",
                       InFailcount.u4RipV2SimplePasswdFailCount);
        }
        if (InFailcount.u4RipV1Md5AuthenticatedPktFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV1Md5AuthenticatedPktFailCount         : %u\r\n",
                       InFailcount.u4RipV1Md5AuthenticatedPktFailCount);
        }
        if (InFailcount.u4RipV2Md5UnAuthenticatedPktFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2Md5UnAuthenticatedPktFailCount       : %u\r\n",
                       InFailcount.u4RipV2Md5UnAuthenticatedPktFailCount);
        }
        if (InFailcount.u4RipV2Md5InvalidLengthPktFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2Md5InvalidLengthPktFailCount         : %u\r\n",
                       InFailcount.u4RipV2Md5InvalidLengthPktFailCount);
        }
        if (InFailcount.u4RipV2Md5InvalidKeyFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2Md5InvalidKeyFailCount               : %u\r\n",
                       InFailcount.u4RipV2Md5InvalidKeyFailCount);
        }
        if (InFailcount.u4RipV2Md5InvalidDigestFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipV2Md5InvalidDigestFailCount            : %u\r\n",
                       InFailcount.u4RipV2Md5InvalidDigestFailCount);
        }
        if (InFailcount.u4RipBadUpdateHeaderFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipBadUpdateHeaderFailCount               : %u\r\n",
                       InFailcount.u4RipBadUpdateHeaderFailCount);
        }
        if (InFailcount.u4RipValidatePacketFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidatePacketFailCount                : %u\r\n",
                       InFailcount.u4RipValidatePacketFailCount);
        }
        if (InFailcount.u4RipTrailingJunkFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipTrailingJunkFailCount                  : %u\r\n",
                       InFailcount.u4RipTrailingJunkFailCount);
        }
        if (InFailcount.u4RipValidateHost1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateHost1FailCount                 : %u\r\n",
                       InFailcount.u4RipValidateHost1FailCount);
        }
        if (InFailcount.u4RipValidateHost2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateHost2FailCount                 : %u\r\n",
                       InFailcount.u4RipValidateHost2FailCount);
        }
        if (InFailcount.u4RipValidateHost3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateHost3FailCount                 : %u\r\n",
                       InFailcount.u4RipValidateHost3FailCount);
        }
        if (InFailcount.u4RipValidatePkt1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidatePkt1FailCount                  : %u\r\n",
                       InFailcount.u4RipValidatePkt1FailCount);
        }
        if (InFailcount.u4RipValidatePkt2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidatePkt2FailCount                  : %u\r\n",
                       InFailcount.u4RipValidatePkt2FailCount);
        }
        if (InFailcount.u4RipValidatePkt3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidatePkt3FailCount                  : %u\r\n",
                       InFailcount.u4RipValidatePkt3FailCount);
        }
        if (InFailcount.u4RipProcessRequest1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipProcessRequest1FailCount               : %u\r\n",
                       InFailcount.u4RipProcessRequest1FailCount);
        }
        if (InFailcount.u4RipProcessRequest2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipProcessRequest2FailCount               : %u\r\n",
                       InFailcount.u4RipProcessRequest2FailCount);
        }
        if (InFailcount.u4RipProcessRequest3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipProcessRequest3FailCount               : %u\r\n",
                       InFailcount.u4RipProcessRequest3FailCount);
        }
        if (InFailcount.u4RipProcessRequest4FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipProcessRequest4FailCount               : %u\r\n",
                       InFailcount.u4RipProcessRequest4FailCount);
        }
        if (InFailcount.u4RipFailUpdatePeerCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipFailUpdatePeerCount                    : %u\r\n",
                       InFailcount.u4RipFailUpdatePeerCount);
        }
        if (InFailcount.u4RipValidateRouteInfoFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfoFailCount             : %u\r\n",
                       InFailcount.u4RipValidateRouteInfoFailCount);
        }
        if (InFailcount.u4RipSendUpdateAndStartSpaceTmrFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSendUpdateAndStartSpaceTmrFailCount    : %u\r\n",
                       InFailcount.u4RipSendUpdateAndStartSpaceTmrFailCount);
        }
        if (InFailcount.u4RipAggTblInitFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipAggTblInitFailCount                      : %u\r\n",
                       InFailcount.u4RipAggTblInitFailCount);
        }
        if (InFailcount.u4RipDelIfAggNullEntryCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipDelIfAggNullEntryCount                     : %u\r\n",
                       InFailcount.u4RipDelIfAggNullEntryCount);
        }
        if (InFailcount.u4RipAggDelTrieFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipAggDelTrieFailCount                      : %u\r\n",
                       InFailcount.u4RipAggDelTrieFailCount);
        }
        if (InFailcount.u4RipAggDelTrieFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipAggDelTrieFailCount                     : %u\r\n",
                       InFailcount.u4RipAggDelTrieFailCount);
        }
        if (InFailcount.u4RipAggIntfSpecificDelTrieFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipAggIntfSpecificDelTrieFailCount         : %u\r\n",
                       InFailcount.u4RipAggIntfSpecificDelTrieFailCount);
        }
        if (InFailcount.u4RipAggChkLimitFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipAggChkLimitFailCount                      : %u\r\n",
                       InFailcount.u4RipAggChkLimitFailCount);
        }
        if (InFailcount.u4RipDelIfAggRouteFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipDelIfAggRouteFailCount                      : %u\r\n",
                       InFailcount.u4RipDelIfAggRouteFailCount);
        }
        if (InFailcount.u4RipDelIfAggTbl1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipDelIfAggTbl1FailCount                     : %u\r\n",
                       InFailcount.u4RipDelIfAggTbl1FailCount);
        }
        if (InFailcount.u4RipDelIfAggTbl2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipDelIfAggTbl2FailCount                      : %u\r\n",
                       InFailcount.u4RipDelIfAggTbl2FailCount);
        }
        if (InFailcount.u4RipSplitHorizonFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSplitHorizonFailCount                     : %u\r\n",
                       InFailcount.u4RipSplitHorizonFailCount);
        }
        if (InFailcount.u4RipIfAggRtNullEntryCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipIfAggRtNullEntryCount                      : %u\r\n",
                       InFailcount.u4RipIfAggRtNullEntryCount);
        }
        if (InFailcount.u4RipGenerateUpdate1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipGenerateUpdate1FailCount                      : %u\r\n",
                       InFailcount.u4RipGenerateUpdate1FailCount);
        }
        if (InFailcount.u4RipGenerateUpdate2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipGenerateUpdate2FailCount                      : %u\r\n",
                       InFailcount.u4RipGenerateUpdate2FailCount);
        }
        if (InFailcount.u4RipGenerateUpdate3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipGenerateUpdate3FailCount                      : %u\r\n",
                       InFailcount.u4RipGenerateUpdate3FailCount);
        }
        if (InFailcount.u4RipGenerateUpdate4FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipGenerateUpdate4FailCount                      : %u\r\n",
                       InFailcount.u4RipGenerateUpdate4FailCount);
        }
        if (InFailcount.u4RipSentUpdate1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSentUpdate1FailCount                      : %u\r\n",
                       InFailcount.u4RipSentUpdate1FailCount);
        }
        if (InFailcount.u4RipSentUpdate2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSentUpdate2FailCount                      : %u\r\n",
                       InFailcount.u4RipSentUpdate2FailCount);
        }
        if (InFailcount.u4RipSentUpdate3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSentUpdate3FailCount                      : %u\r\n",
                       InFailcount.u4RipSentUpdate3FailCount);
        }
        if (InFailcount.u4RipSentUpdate4FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSentUpdate4FailCount                      : %u\r\n",
                       InFailcount.u4RipSentUpdate4FailCount);
        }
        if (InFailcount.u4RipSentUpdate5FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSentUpdate5FailCount                      : %u\r\n",
                       InFailcount.u4RipSentUpdate5FailCount);
        }
        if (InFailcount.u4RipUpdateRipPktToSend1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipUpdateRipPktToSend1FailCount                      : %u\r\n",
                       InFailcount.u4RipUpdateRipPktToSend1FailCount);
        }
        if (InFailcount.u4RipUpdateRipPktToSend2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipUpdateRipPktToSend2FailCount                      : %u\r\n",
                       InFailcount.u4RipUpdateRipPktToSend2FailCount);
        }
        if (InFailcount.u4RipUpdateRipPktToSend3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipUpdateRipPktToSend3FailCount                      : %u\r\n",
                       InFailcount.u4RipUpdateRipPktToSend3FailCount);
        }
        if (InFailcount.u4RipUpdateRipPktToSend4FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipUpdateRipPktToSend4FailCount                      : %u\r\n",
                       InFailcount.u4RipUpdateRipPktToSend4FailCount);
        }
        if (InFailcount.u4RipSendUnicastDatagram1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSendUnicastDatagram1FailCount                      : %u\r\n",
                       InFailcount.u4RipSendUnicastDatagram1FailCount);
        }
        if (InFailcount.u4RipSendUnicastDatagram2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipSendUnicastDatagram2FailCount                      : %u\r\n",
                       InFailcount.u4RipSendUnicastDatagram2FailCount);
        }
        if (InFailcount.u4RipUpdateRequestFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipUpdateRequestFailCount                      : %u\r\n",
                       InFailcount.u4RipUpdateRequestFailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo1FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo1FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo1FailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo2FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo2FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo2FailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo3FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo3FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo3FailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo4FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo4FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo4FailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo5FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo5FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo5FailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo6FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo6FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo6FailCount);
        }
        if (InFailcount.u4RipValidateRouteInfo7FailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipValidateRouteInfo7FailCount                      : %u\r\n",
                       InFailcount.u4RipValidateRouteInfo7FailCount);
        }
        if (InFailcount.u4RipConstructAuthInfoFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipConstructAuthInfoFailCount                      : %u\r\n",
                       InFailcount.u4RipConstructAuthInfoFailCount);
        }
        if (InFailcount.u4RipGetOperStatusFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipGetOperStatusFailCount                      : %u\r\n",
                       InFailcount.u4RipGetOperStatusFailCount);
        }
        if (InFailcount.u4RipGetSendStatusFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipGetSendStatusFailCount                      : %u\r\n",
                       InFailcount.u4RipGetSendStatusFailCount);
        }
        if (InFailcount.u4RipFailRouteAddCount != 0)
        {
            CliPrintf (CliHandle,
                       "RipFailRouteAddCount                      : %u\r\n",
                       InFailcount.u4RipFailRouteAddCount);
        }
        if (InFailcount.u4RipFailCount != 0)
        {
            CliPrintf (CliHandle,
                       "Failed Count                              : %u\r\n",
                       InFailcount.u4RipFailCount);
        }
        CliPrintf (CliHandle,
                   "=================================================================\r\n");
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt */
            break;
        }
        i4CxtId = i4CurCxtId;
    }
    while (nmhGetNextIndexFsMIStdRip2IfStatTable
           (i4CxtId, &i4CurCxtId, u4RipIfAddr, &u4RipIfAddr) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/**************************************************************************/
/*   Function Name : RipShowDatabaseCxt                                   */
/*                                                                        */
/*   Description   :To display IP RIP route database                      */
/*                                                                        */
/*   Input(s)      : CliHandle - CLI context ID                           */
/*                   i4CxtId   - RIP Context Id.                          */
/*   Output(s)     : .If Success none else Error                          */
/*                                                                        */
/*   Return Values : .CLI_SUCCESS/CLI_FAILURE                             */
/* ************************************************************************/
INT4
RipShowDatabaseCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtInfo         *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tNetIpv4IfInfo      NetIpIfInfo;
    tRipRtEntry        *pRt = NULL;
    tRipRtEntry        *pAltRt = NULL;
    tRipRtEntry        *pBestRt = NULL;
    INT4                i4Tos = 0;
    INT4                i4count;
    INT4                i4TotalRtCount = 0;
    /* Key of Trie is made up of address and mask */
    UINT4               au4Indx[2];
    UINT4               u4Quit = FALSE;
    UINT4               u4MaskBits;
    UINT4               u4IpAddr = 0, u4Mask = 0, u4NextHop = 0;
    UINT4               u4Paging = CLI_SUCCESS;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4CurCxtId = 0, i4PrevCxtId = (INT4) RIP_INVALID_CXT_ID;
    INT4                i4AllCxt = RIP_FALSE;
    CHR1               *pu1IpAddr = NULL;
    UINT1               u1FoundFlag = FALSE;
    UINT1               u1SpaceFlag = FALSE;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    UINT4               u4AltNextHop = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4RetVal = 0;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    i4Tos = 0;
    u4IpAddr = u4Mask = u4NextHop = 0;
    if (i4CxtId == (INT4) RIP_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIRip2GlobalTable (&i4CxtId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n");
            return (CLI_SUCCESS);
        }
        i4AllCxt = RIP_TRUE;
    }

    /* Get the first route entry from fsRip2LocalRoutingTable */
    i4CurCxtId = i4CxtId;
    CliPrintf (CliHandle, "\r");
    do
    {
        u4AltNextHop = 0;

        if (i4CxtId != i4CurCxtId && i4AllCxt == RIP_FALSE)
        {
            break;
        }

        if (i4PrevCxtId != i4CurCxtId)
        {
            if ((RipGetVrfNameFrmCxtId (i4CurCxtId, au1RipCxtName)) ==
                RIP_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\r\nVrf %s\n", au1RipCxtName);

            nmhGetFsMIRipRtCount (i4CurCxtId, &i4TotalRtCount);
            CliPrintf (CliHandle, "Total Count : %d\r\n", i4TotalRtCount);
            i4PrevCxtId = i4CurCxtId;
        }

        /* Input key is formed of IP address and mask */
        au4Indx[0] = RIP_HTONL (u4IpAddr);
        au4Indx[1] = RIP_HTONL (u4Mask);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        pRipCxtEntry = RipGetCxtInfoRec (i4CurCxtId);
        if (pRipCxtEntry == NULL)
        {
            return SNMP_FAILURE;
        }

        InParams.pRoot = pRipCxtEntry->pRipRoot;
        OutParams.Key.pKey = NULL;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

        if (TrieSearchEntry (&(InParams), &(OutParams)) == TRIE_FAILURE)
        {
            continue;
        }
        /* Check whether we have the route for the given index */
        for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
        {
            if (apAppSpecInfo[i4count] == NULL)
            {
                continue;
            }
            pRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
            if (pRt == NULL)
            {
                continue;
            }
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, pRt->RtInfo.u4DestNet);
            u4MaskBits = CliGetMaskBits (pRt->RtInfo.u4DestMask);
            u4NextHop = pRt->RtInfo.u4NextHop;

            if (pRt != NULL && i4count == (OTHERS_ID - 1))
            {
                CliPrintf (CliHandle, "\r%15s/%-3u [%d]", pu1IpAddr, u4MaskBits,
                           pRt->RtInfo.i4Metric1);
                CliPrintf (CliHandle, "        auto-summary\r\n");
                pRt = NULL;
                continue;
            }

            CliPrintf (CliHandle, "\r%15s/%-3u [%d]", pu1IpAddr, u4MaskBits,
                       pRt->RtInfo.i4Metric1);
            if (i4count != (CIDR_LOCAL_ID - 1)
                && i4count != (RIP_ID - 1) && i4count != (OTHERS_ID - 1))
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, pRt->RtInfo.u4NextHop);
                CliPrintf (CliHandle, "        redistributed via %s\r\n",
                           pu1IpAddr);
            }
            else
            {
                /*Check whether the route is directly connected */
                if (u4NextHop == 0)
                {
                    if (NetIpv4GetIfInfo (pRt->RtInfo.u4RtIfIndx,
                                          &NetIpIfInfo) == NETIPV4_FAILURE)
                    {
                        u4Paging = (UINT4) CliPrintf (CliHandle,
                                                      "        directly connected\r\n");

                    }
                    else
                    {
                        i4RetVal =
                            NetIpv4GetCfaIfIndexFromPort (pRt->RtInfo.
                                                          u4RtIfIndx,
                                                          &u4CfaIfIndex);

                        /* Get the alias name for the interface */
                        CfaCliGetIfName (u4CfaIfIndex, (INT1 *) au1IfName);
                        u4Paging = (UINT4) CliPrintf (CliHandle,
                                                      "        directly connected, %s\r\n",
                                                      au1IfName);
                        if (u4Paging == CLI_FAILURE)
                        {
                            /* User pressed 'q' at more prompt */
                            u4Quit = TRUE;
                            break;
                        }
                    }
                }
                else
                {
                    /* Route is reachable via a next hop */
                    /* For ECMP routes, all the alternate paths should 
                     * be displyed. Otherwise display only the best path */
                    pAltRt = pRt->pNextAlternatepath;
                    if ((pAltRt == NULL)
                        || (pRt->RtInfo.i4Metric1 != pAltRt->RtInfo.i4Metric1))
                    {
                        /* Only one best route is available */
                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                   pRt->RtInfo.u4NextHop);
                        if (NetIpv4GetIfInfo (pRt->RtInfo.u4RtIfIndx,
                                              &NetIpIfInfo) == NETIPV4_FAILURE)
                        {
                            u4Paging =
                                (UINT4) CliPrintf (CliHandle,
                                                   "        via %s\r\n",
                                                   pu1IpAddr);

                        }
                        else
                        {
                            i4RetVal =
                                NetIpv4GetCfaIfIndexFromPort (pRt->RtInfo.
                                                              u4RtIfIndx,
                                                              &u4CfaIfIndex);
                            /* Get the alias name for the interface */
                            CfaCliGetIfName (u4CfaIfIndex, (INT1 *) au1IfName);
                            u4Paging =
                                (UINT4) CliPrintf (CliHandle,
                                                   "        via %s, %s\r\n",
                                                   pu1IpAddr, au1IfName);
                        }
                        while (pAltRt != NULL)
                        {
                            u4NextHop = pAltRt->RtInfo.u4NextHop;
                            pAltRt = pAltRt->pNextAlternatepath;
                        }

                        if (u4Paging == CLI_FAILURE)
                        {
                            /* User pressed 'q' at more prompt */
                            u4Quit = TRUE;
                            break;
                        }
                    }
                    else
                    {
                        /* More than one best route is available for the 
                         * destination - ECMP routes*/
                        u4NextHop = 0;
                        do
                        {
                            /* Get the route with the least nexthop (to be in 
                             * ascending order when displayed)from the 
                             * available alternate routes*/
                            u1FoundFlag = FALSE;
                            pAltRt = pRt;
                            do
                            {
                                if (u1FoundFlag == FALSE)
                                {
                                    if (pAltRt->RtInfo.u4NextHop > u4NextHop)
                                    {
                                        pBestRt = pAltRt;
                                        u1FoundFlag = TRUE;
                                    }
                                }
                                else
                                {
                                    if ((pAltRt->RtInfo.u4NextHop > u4NextHop)
                                        && (pAltRt->RtInfo.u4NextHop <
                                            pBestRt->RtInfo.u4NextHop))
                                    {
                                        pBestRt = pAltRt;
                                    }
                                }
                                pAltRt = pAltRt->pNextAlternatepath;
                            }
                            while ((pAltRt != NULL) &&
                                   (pAltRt->RtInfo.i4Metric1 ==
                                    pRt->RtInfo.i4Metric1));
                            /* print the route info */
                            if (u1FoundFlag == TRUE)
                            {
                                u4NextHop = pBestRt->RtInfo.u4NextHop;
                                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                           u4NextHop);
                                if (u1SpaceFlag == TRUE)
                                {
                                    CliPrintf (CliHandle,
                                               "                               ");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "        ");
                                }

                                MEMSET ((UINT1 *) &NetIpIfInfo, 0,
                                        sizeof (tNetIpv4IfInfo));
                                if (NetIpv4GetIfInfo
                                    (pBestRt->RtInfo.u4RtIfIndx,
                                     &NetIpIfInfo) == NETIPV4_FAILURE)
                                {
                                    u4Paging =
                                        (UINT4) CliPrintf (CliHandle,
                                                           "via %s\r\n",
                                                           pu1IpAddr);
                                }
                                else
                                {
                                    i4RetVal = NetIpv4GetCfaIfIndexFromPort
                                        (pBestRt->RtInfo.u4RtIfIndx,
                                         &u4CfaIfIndex);
                                    /* Get the alias name for the interface */
                                    CfaCliGetIfName (u4CfaIfIndex,
                                                     (INT1 *) au1IfName);
                                    u4Paging =
                                        (UINT4) CliPrintf (CliHandle,
                                                           "via %s, %s\r\n",
                                                           pu1IpAddr,
                                                           au1IfName);
                                }
                                u1SpaceFlag = TRUE;
                                if (u4Paging == CLI_FAILURE)
                                {
                                    /* User pressed 'q' at more prompt */
                                    u4Quit = TRUE;
                                    break;
                                }
                            }
                        }
                        while (u1FoundFlag == TRUE);
                        u4AltNextHop = u4NextHop;
                        u1SpaceFlag = FALSE;
                    }
                }
            }
        }
        if (u4Quit == TRUE)
        {
            break;
        }
        i4CxtId = i4CurCxtId;

        /* u4AltNextHop will be set in case of ECMP routes. The highest next hop
           will be strored in u4AltNextHop. In this case, get next should be called
           with the alternate next hop.
           e.g. If rip has a route to 20.0.0.0/8 through 11.0.0.1 and 12.0.0.1
           and through ospf has a route to same destination through 11.0.0.1,
           get next should be called using 12.0.0.1 since this would have been
           scanned and displayed */
        if (u4AltNextHop != 0)
        {
            u4NextHop = u4AltNextHop;
        }
    }
    while (nmhGetNextIndexFsMIRip2LocalRouteTable (i4CxtId, &i4CurCxtId,
                                                   u4IpAddr, &u4IpAddr,
                                                   u4Mask, &u4Mask, i4Tos,
                                                   &i4Tos, u4NextHop,
                                                   &u4NextHop) != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");
    UNUSED_PARAM (i4RetVal);
    return (CLI_SUCCESS);
}

/**************************************************************************/
/*   Function Name : RipShowIpAddrInCxt                                   */
/*                                                                        */
/*   Description   :To display IP RIP route database                      */
/*                                                                        */
/*   Input(s)      : CliHandle - CLI context ID                           */
/*                   u4DestIpAddr - Destination IP address                */
/*                   u4DestIpMask - Destination IP mask                   */
/*                   i4CxtId - RIP Context Id                             */
/*   Output(s)     : .If Success none else Error                          */
/*                                                                        */
/*   Return Values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RipShowIpAddrInCxt (tCliHandle CliHandle, UINT4 u4DestIpAddr,
                    UINT4 u4DestIpMask, INT4 i4CxtId)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtInfo         *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tNetIpv4IfInfo      NetIpIfInfo;
    tRipRtEntry        *pRt = NULL;
    tRipRtEntry        *pAltRt = NULL;
    tRipRtEntry        *pBestRt = NULL;
    INT4                i4Tos = 0;
    INT4                i4count;
    INT4                i4CurCxtId = RIP_DEFAULT_CXT;
    INT4                i4PrevCxtId = (INT4) RIP_INVALID_CXT_ID;
    /* Key of Trie is made up of address and mask */
    UINT4               au4Indx[2];
    UINT4               u4Quit = 0;
    UINT4               u4MaskBits;
    UINT4               u4IpAddr = 0, u4Mask = 0, u4NextHop = 0;
    UINT4               u4Paging = CLI_SUCCESS;
    UINT4               u4CfaIfIndex = 0;
    CHR1               *pu1IpAddr = NULL;
    UINT1               u1FoundFlag = FALSE;
    UINT1               u1SpaceFlag = FALSE;
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    tRipCxt            *pRipCxtEntry = NULL;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4RetVal = 0;

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    /* Get the first route entry from fsRip2LocalRoutingTable */
    if (i4CxtId == (INT4) RIP_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIRip2LocalRouteTable
            (&i4CurCxtId, &u4IpAddr, &u4Mask, &i4Tos,
             &u4NextHop) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n");
            return (CLI_SUCCESS);
        }
    }

    u4IpAddr = u4Mask = u4NextHop = 0;
    i4Tos = 0;
    if (i4CxtId != (INT4) RIP_INVALID_CXT_ID)
    {
        i4CurCxtId = i4CxtId;
    }
    CliPrintf (CliHandle, "\r");
    do
    {
        /* Check whether the route's destination IP address and mask matches
         * with the IP address and mask for whom we have to display RIP route
         * entries
         */
        if ((i4CxtId != (INT4) RIP_INVALID_CXT_ID) && (i4CxtId != i4CurCxtId))
        {
            break;
        }
        if ((i4CxtId == (INT4) RIP_INVALID_CXT_ID)
            && (i4PrevCxtId != i4CurCxtId))
        {
            if ((RipGetVrfNameFrmCxtId (i4CurCxtId, au1RipCxtName)) ==
                RIP_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\rVrf %s\n", au1RipCxtName);

        }

        if ((u4IpAddr == u4DestIpAddr) && (u4Mask == u4DestIpMask))
        {

            /* Input key is formed of IP address and mask */
            au4Indx[0] = RIP_HTONL (u4IpAddr);
            au4Indx[1] = RIP_HTONL (u4Mask);
            InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
            InParams.i1AppId = ALL_ROUTING_PROTOCOL;
            pRipCxtEntry = RipGetCxtInfoRec (i4CurCxtId);
            if (pRipCxtEntry == NULL)
            {
                return CLI_FAILURE;
            }

            InParams.pRoot = pRipCxtEntry->pRipRoot;
            OutParams.Key.pKey = NULL;
            MEMSET (apAppSpecInfo, 0,
                    MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
            OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

            if (TrieSearchEntry (&(InParams), &(OutParams)) == TRIE_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_ROUTE_NOT_PRESENT);
                return (CLI_FAILURE);
            }
            /* Check whether we have the route for the given index */
            for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
            {
                if (apAppSpecInfo[i4count] == NULL)
                {
                    continue;
                }
                pRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
                if (pRt == NULL)
                {
                    continue;
                }
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, pRt->RtInfo.u4DestNet);
                u4MaskBits = CliGetMaskBits (pRt->RtInfo.u4DestMask);
                u4NextHop = pRt->RtInfo.u4NextHop;

                if (pRt != NULL && i4count == (OTHERS_ID - 1))
                {
                    CliPrintf (CliHandle, "\r%s/%-2u [%d]", pu1IpAddr,
                               u4MaskBits, pRt->RtInfo.i4Metric1);
                    CliPrintf (CliHandle, "        auto-summary\r\n");
                    pRt = NULL;
                    continue;
                }

                CliPrintf (CliHandle, "\r%s/%-2u [%d]", pu1IpAddr, u4MaskBits,
                           pRt->RtInfo.i4Metric1);
                if (i4count != (CIDR_LOCAL_ID - 1)
                    && i4count != (RIP_ID - 1) && i4count != (OTHERS_ID - 1))
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                               pRt->RtInfo.u4NextHop);
                    CliPrintf (CliHandle, "        redistributed via %s\r\n",
                               pu1IpAddr);
                    return (CLI_SUCCESS);
                }
                else
                {
                    /*Check whether the route is directly connected */
                    if (u4NextHop == 0)
                    {
                        if (NetIpv4GetIfInfo (pRt->RtInfo.u4RtIfIndx,
                                              &NetIpIfInfo) == NETIPV4_FAILURE)
                        {
                            u4Paging = (UINT4) CliPrintf (CliHandle,
                                                          "        directly connected\r\n");
                        }
                        else
                        {
                            i4RetVal = NetIpv4GetCfaIfIndexFromPort
                                (pRt->RtInfo.u4RtIfIndx, &u4CfaIfIndex);
                            /* Get the alias name for the interface */
                            CfaCliGetIfName (u4CfaIfIndex, (INT1 *) au1IfName);
                            u4Paging = (UINT4) CliPrintf (CliHandle,
                                                          "        directly connected, %s\r\n",
                                                          au1IfName);
                        }
                        UNUSED_PARAM (i4RetVal);
                        return (CLI_SUCCESS);
                    }
                    else
                    {
                        /* Route is reachable via a next hop */
                        /* For ECMP routes, all the alternate paths should 
                         * be displyed. Otherwise display only the best path */
                        pAltRt = pRt->pNextAlternatepath;
                        if ((pAltRt == NULL)
                            || (pRt->RtInfo.i4Metric1 !=
                                pAltRt->RtInfo.i4Metric1))
                        {
                            /* Only one best route is available */
                            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                       pRt->RtInfo.u4NextHop);

                            if (NetIpv4GetIfInfo (pRt->RtInfo.u4RtIfIndx,
                                                  &NetIpIfInfo) ==
                                NETIPV4_FAILURE)
                            {
                                u4Paging = (UINT4) CliPrintf (CliHandle,
                                                              "        via %s\r\n",
                                                              pu1IpAddr);
                            }
                            else
                            {
                                i4RetVal = NetIpv4GetCfaIfIndexFromPort
                                    (pRt->RtInfo.u4RtIfIndx, &u4CfaIfIndex);
                                /* Get the alias name for the interface */
                                CfaCliGetIfName (u4CfaIfIndex,
                                                 (INT1 *) au1IfName);
                                u4Paging =
                                    (UINT4) CliPrintf (CliHandle,
                                                       "        via %s, %s\r\n",
                                                       pu1IpAddr, au1IfName);
                            }
                            UNUSED_PARAM (i4RetVal);
                            return (CLI_SUCCESS);
                        }
                        else
                        {
                            /* More than one best route is available for the 
                             * destination - ECMP routes*/
                            u4NextHop = 0;
                            do
                            {
                                /* Get the route with the least nexthop (to be in 
                                 * ascending order when displayed)from the 
                                 * available alternate routes*/
                                u1FoundFlag = FALSE;
                                pAltRt = pRt;
                                do
                                {
                                    if (u1FoundFlag == FALSE)
                                    {
                                        if (pAltRt->RtInfo.u4NextHop >
                                            u4NextHop)
                                        {
                                            pBestRt = pAltRt;
                                            u1FoundFlag = TRUE;
                                        }
                                    }
                                    else
                                    {
                                        if ((pAltRt->RtInfo.u4NextHop >
                                             u4NextHop)
                                            && (pAltRt->RtInfo.u4NextHop <
                                                pBestRt->RtInfo.u4NextHop))
                                        {
                                            pBestRt = pAltRt;
                                        }
                                    }
                                    pAltRt = pAltRt->pNextAlternatepath;
                                }
                                while ((pAltRt != NULL) &&
                                       (pAltRt->RtInfo.i4Metric1 ==
                                        pRt->RtInfo.i4Metric1));
                                /* print the route info */
                                if (u1FoundFlag == TRUE)
                                {
                                    u4NextHop = pBestRt->RtInfo.u4NextHop;
                                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                               u4NextHop);
                                    if (u1SpaceFlag == TRUE)
                                    {
                                        CliPrintf (CliHandle,
                                                   "                       ");
                                    }
                                    else
                                    {
                                        CliPrintf (CliHandle, "        ");
                                    }

                                    MEMSET ((UINT1 *) &NetIpIfInfo, 0,
                                            sizeof (tNetIpv4IfInfo));
                                    if (NetIpv4GetIfInfo
                                        (pBestRt->RtInfo.u4RtIfIndx,
                                         &NetIpIfInfo) == NETIPV4_FAILURE)
                                    {
                                        u4Paging =
                                            (UINT4) CliPrintf (CliHandle,
                                                               "via %s, %s\r\n",
                                                               pu1IpAddr);
                                    }
                                    else
                                    {
                                        i4RetVal = NetIpv4GetCfaIfIndexFromPort
                                            (pBestRt->RtInfo.u4RtIfIndx,
                                             &u4CfaIfIndex);
                                        /* Get the alias name for the interface */
                                        CfaCliGetIfName (u4CfaIfIndex,
                                                         (INT1 *) au1IfName);
                                        u4Paging =
                                            (UINT4) CliPrintf (CliHandle,
                                                               "via %s, %s\r\n",
                                                               pu1IpAddr,
                                                               au1IfName);
                                    }
                                    u1SpaceFlag = TRUE;
                                    if (u4Paging == CLI_FAILURE)
                                    {
                                        /* User pressed 'q' at more prompt */
                                        u4Quit = TRUE;
                                        break;
                                    }
                                }
                            }
                            while (u1FoundFlag == TRUE);
                            UNUSED_PARAM (u4Quit);
                            UNUSED_PARAM (i4RetVal);
                            return (CLI_SUCCESS);
                        }
                    }
                }
            }
            return CLI_SUCCESS;
        }
        i4PrevCxtId = i4CurCxtId;
    }
    while (nmhGetNextIndexFsMIRip2LocalRouteTable (i4CurCxtId, &i4CurCxtId,
                                                   u4IpAddr, &u4IpAddr,
                                                   u4Mask, &u4Mask, i4Tos,
                                                   &i4Tos, u4NextHop,
                                                   &u4NextHop) != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");
    CLI_SET_ERR (CLI_RIP_ROUTE_NOT_PRESENT);
    UNUSED_PARAM (i4RetVal);
    return (CLI_FAILURE);
}

/**************************************************************************/
/*   Function Name : RipShowPeerInformation                               */
/*                                                                        */
/*   Description   : To display IP RIP peer information                   */
/*                                                                        */
/*   Input(s)      : CliHandle - CliContext ID                            */
/*                   i4CxtId - RIP Context Id                             */
/*                                                                        */
/*   Output(s)     : If Success none else Error                           */
/*                                                                        */
/*   Return Values : CLI_SUCCESS/CLI_FAILURE                              */
/**************************************************************************/
INT4
RipShowPeerInformation (tCliHandle CliHandle, INT4 i4CxtId)
{
    tRipCxt            *pRipCxtEntry = NULL;
    tRipPeerRec        *pRipCurPeerRec = NULL;
    INT4                i4MaxCxt = 0;
    INT4                i4RetVal = 0;
    INT4                i4ContextId = 0;
    UINT4               u4Rip2PeerAddress = 0;
    UINT4               u4Rip2PrevPeerAddress = 0;
    UINT1              *pu1Domain = NULL;
    UINT1               au1PeerInfo[64];
    UINT1               au1PrevPeerInfo[64];
    CHR1               *pu1IpAddr = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT4                i4AuthType = 0;
    tSNMP_OCTET_STRING_TYPE Rip2PeerDomain;
    tSNMP_OCTET_STRING_TYPE Rip2PrevPeerDomain;

    UNUSED_PARAM (i4CxtId);
    MEMSET (au1PeerInfo, 0, sizeof (au1PeerInfo));
    MEMSET (au1PrevPeerInfo, 0, sizeof (au1PrevPeerInfo));
    Rip2PeerDomain.pu1_OctetList = au1PeerInfo;
    Rip2PrevPeerDomain.pu1_OctetList = au1PrevPeerInfo;
    Rip2PeerDomain.i4_Length = 0;
    Rip2PrevPeerDomain.i4_Length = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams
                        [MAX_RIP_CONTEXT_SIZING_ID].u4PreAllocatedUnits);

    CliPrintf (CliHandle, "\rRIP Peer Information :\r\n\r\n");
    CliPrintf (CliHandle, "\r%-14s %-17s %-14s %-11s %-7s\r\n",
               "PeerIfIndex", "PeerAddress", "PeerVersion",
               "RecvSeqNo", "KeyId");
    CliPrintf (CliHandle, "\r%-14s %-17s %-14s %-11s %-7s\r\n",
               "-----------", "-----------", "-----------",
               "---------", "-----");

    for (i4ContextId = 0; i4ContextId < i4MaxCxt; i4ContextId++)
    {
        if (gRipRtr.apRipCxt[i4ContextId] == NULL)
        {
            continue;
        }

        RIP_MGMT_CXT = gRipRtr.apRipCxt[i4ContextId];
        pRipCxtEntry = RIP_MGMT_CXT;

        if (nmhGetFirstIndexRip2PeerTable (&u4Rip2PeerAddress,
                                           &Rip2PeerDomain) != SNMP_FAILURE)
        {
            do
            {
                pu1Domain = Rip2PeerDomain.pu1_OctetList;
                if ((pRipCurPeerRec =
                     rip_retrieve_record_for_peer (u4Rip2PeerAddress, pu1Domain,
                                                   pRipCxtEntry)) ==
                    (tRipPeerRec *) NULL)
                {
                    return CLI_FAILURE;
                }
                pRipIfRec =
                    RipGetUtilIfRecForPrimSecAddr (pRipCurPeerRec->u4PeerAddr,
                                                   pRipCxtEntry);
                if (pRipIfRec != NULL)
                {
                    nmhGetFsMIStdRip2IfConfAuthType (i4ContextId,
                                                     pRipIfRec->RipIfaceCfg.
                                                     u4SrcAddr, &i4AuthType);
                }

                CliPrintf (CliHandle, "%-15d", pRipCurPeerRec->u2IfIndex);

                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                           pRipCurPeerRec->u4PeerAddr);
                CliPrintf (CliHandle, "%-18s", pu1IpAddr);

                CliPrintf (CliHandle, "%-15d", pRipCurPeerRec->u2PeerVersion);

                CliPrintf (CliHandle, "%-12d", pRipCurPeerRec->u4RecvSeqNo);

                CliPrintf (CliHandle, "%-7d", pRipCurPeerRec->u1KeyIdInUse);

                u4Rip2PrevPeerAddress = u4Rip2PeerAddress;
                Rip2PrevPeerDomain.i4_Length = Rip2PeerDomain.i4_Length;
                MEMCPY (Rip2PrevPeerDomain.pu1_OctetList,
                        Rip2PeerDomain.pu1_OctetList, Rip2PeerDomain.i4_Length);
                CliPrintf (CliHandle, "\r\n");

                i4RetVal = RipSetContext ((UINT4) i4ContextId);
            }
            while (nmhGetNextIndexRip2PeerTable (u4Rip2PrevPeerAddress,
                                                 &u4Rip2PeerAddress,
                                                 &Rip2PrevPeerDomain,
                                                 &Rip2PeerDomain) !=
                   SNMP_FAILURE);
            RipResetContext ();

        }
    }

    CliPrintf (CliHandle, "\r\n");
    UNUSED_PARAM (i4RetVal);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*   Function Name :RipShowRunningConfigCxt                               */
/*                                                                        */
/*   Description   :This function shows the current running configurations*/
/*                   of RIP module.                                       */
/*                                                                        */
/*   Input(s)      : CliHandle - CliContext ID                            */
/*                   u4Module - Specified module (rip/all), for           */
/*                              configuration                             */
/*                   u4ContextId - context identifier                     */
/*                                                                        */
/*   Output(s)     : None                                                 */
/*                                                                        */
/*   Return Values : CLI_SUCCESS                                          */
/**************************************************************************/
INT4
RipShowRunningConfigCxt (tCliHandle CliHandle, UINT4 u4Module,
                         UINT4 u4ContextId)
{
    tRipCxt            *pRipCxtEntry = NULL;

    CliRegisterLock (CliHandle, RipLock, RipUnLock);
    RipLock ();

    pRipCxtEntry = RipGetCxtInfoRec ((INT4) u4ContextId);
    if (pRipCxtEntry == NULL)
    {
        RipUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    if (RipShowRunningConfigScalarCxt (CliHandle, (INT4) u4ContextId, u4Module)
        == CLI_FAILURE)
    {
        RipUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    if (RipShowRunningConfigTableCxt (CliHandle, (INT4) u4ContextId) ==
        CLI_SUCCESS)
    {
        RipShowRunningConfigInterfaceCxt (CliHandle, (INT4) u4ContextId);
    }
    RipUnLock ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowRunningConfigScalarCxt                      */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        RIP scalar objects.                                */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4CxtId - RIP Context Id                           */
/*                        u4Module - Specified module (rip/all), for         */
/*                                   configuration                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT1
RipShowRunningConfigScalarCxt (tCliHandle CliHandle, INT4 i4CxtId,
                               UINT4 u4Module)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    INT4                i4Distance = 0;

    UINT1               au1RouteMapName[RMAP_MAX_NAME_LEN + 4] = { 0 };
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    INT4                i4Val = 0;
    CHR1                pc1Redist[32] = { 0 };
    INT1                i1RetVal = 0;

    MEMSET (pc1Redist, 0, 32);
    RouteMapName.pu1_OctetList = au1RouteMapName;
    RouteMapName.i4_Length = 0;

    i1RetVal = nmhGetFsMIRipAdminStatus (i4CxtId, &i4Val);
    if (i4Val != RIP_ADMIN_DISABLE)
    {
        nmhGetFsMIRip2LastAuthKeyLifetimeStatus (i4CxtId, &i4Val);
        if (i4Val != RIP_TRUE)
        {
            if ((i4CxtId != RIP_DEFAULT_CXT) &&
                (RipGetVrfNameFrmCxtId (i4CxtId, au1RipCxtName) == RIP_SUCCESS))
            {
                CliPrintf (CliHandle,
                           "rip vrf %s authentication last-key infinite lifetime false\r\n",
                           au1RipCxtName);
            }
            else
            {
                CliPrintf (CliHandle,
                           "rip authentication last-key infinite lifetime false\r\n");
            }
        }

        if (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
        {
            CliPrintf (CliHandle, "end\r\n");
            CliPrintf (CliHandle, "!\r\n");
        }

        if ((i4CxtId != RIP_DEFAULT_CXT) &&
            (VcmGetAliasName ((UINT4) i4CxtId, au1RipCxtName) == VCM_SUCCESS))
        {
            CliPrintf (CliHandle, "router rip\r\n");
            RipPrintCxtName (CliHandle, i4CxtId);
        }
        else
        {
            CliPrintf (CliHandle, "router rip\r\n");
        }
    }
    else
    {

        return CLI_FAILURE;
    }

    nmhGetFsMIRip2Security (i4CxtId, &i4Val);

    if (i4Val != CLI_RIP_SECURITY_MAX)
    {
        CliPrintf (CliHandle, "ip rip security minimum\r\n");
    }

    nmhGetFsMIRip2RetransTimeoutInt (i4CxtId, &i4Val);

    if (i4Val != RIP_DEF_RETRANSMISSION_INT)
    {
        CliPrintf (CliHandle, "ip rip retransmission interval %d\r\n", i4Val);
    }

    nmhGetFsMIRip2MaxRetransmissions (i4CxtId, &i4Val);

    if (i4Val != RIP_DEF_RETRANSMISSION_RETRIES)
    {
        CliPrintf (CliHandle, "ip rip retransmission retries %d\r\n", i4Val);
    }

    nmhGetFsMIRip2SpacingEnable (i4CxtId, &i4Val);

    if (i4Val != RIP_SPACING_DISABLE)
    {
        CliPrintf (CliHandle, "output-delay\r\n");
    }

    nmhGetFsMIRip2AutoSummaryStatus (i4CxtId, &i4Val);

    if (i4Val != RIP_AUTO_SUMMARY_ENABLE)
    {
        CliPrintf (CliHandle, "auto-summary disable\r\n");
    }

    nmhGetFsMIRipRRDRouteDefMetric (i4CxtId, &i4Val);

    if (i4Val != CLI_RIP_DEF_METRIC_VALUE)
    {
        CliPrintf (CliHandle, "default-metric %d\r\n", i4Val);
    }

    nmhGetFsMIRipRRDSrcProtoMaskEnable (i4CxtId, &i4Val);
    nmhGetFsMIRipRRDRouteMapEnable (i4CxtId, &RouteMapName);

    RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;
    if (RouteMapName.i4_Length != 0)
    {
        SNPRINTF (pc1Redist, 32, "route-map %s", RouteMapName.pu1_OctetList);
    }

    if ((i4Val & RIP_RRD_ALL_MASK) == RIP_RRD_ALL_MASK)
    {
        CliPrintf (CliHandle, "redistribute all %s\r\n", pc1Redist);
    }
    else
    {
        if ((i4Val & RIP_RRD_BGP_MASK) == RIP_RRD_BGP_MASK)
        {
            CliPrintf (CliHandle, "redistribute bgp %s\r\n", pc1Redist);
        }
        if ((i4Val & RIP_RRD_DIRECT_MASK) == RIP_RRD_DIRECT_MASK)
        {
            CliPrintf (CliHandle, "redistribute connected %s\r\n", pc1Redist);
        }
        if ((i4Val & RIP_RRD_OSPF_MASK) == RIP_RRD_OSPF_MASK)
        {
            CliPrintf (CliHandle, "redistribute ospf %s\r\n", pc1Redist);
        }
        if ((i4Val & RIP_RRD_STATIC_MASK) == RIP_RRD_STATIC_MASK)
        {
            CliPrintf (CliHandle, "redistribute static %s\r\n", pc1Redist);
        }
        if ((i4Val & RIP_RRD_ISISL1L2_MASK) == RIP_RRD_ISISL1_MASK)
        {
            CliPrintf (CliHandle, "redistribute isis level-1 %s\r\n",
                       pc1Redist);
        }
        else if ((i4Val & RIP_RRD_ISISL1L2_MASK) == RIP_RRD_ISISL2_MASK)
        {
            CliPrintf (CliHandle, "redistribute isis level-2 %s\r\n",
                       pc1Redist);
        }
        else if ((i4Val & RIP_RRD_ISISL1L2_MASK) == RIP_RRD_ISISL1L2_MASK)
        {
            CliPrintf (CliHandle, "redistribute isis level-1-2 %s\r\n",
                       pc1Redist);
        }
    }

    if ((RipGetVcmSystemModeExt (RIP_PROTOCOL_ID) == VCM_SI_MODE) ||
        (RipSetContext ((UINT4) i4CxtId) != SNMP_FAILURE))
    {
        if (nmhGetFsRipPreferenceValue (&i4Distance) == SNMP_SUCCESS)
        {
            if (i4Distance != RIP_DEFAULT_PREFERENCE)
            {
                CliPrintf (CliHandle, "distance  %d\r\n", i4Distance);
            }

        }

        if (RipGetVcmSystemModeExt (RIP_PROTOCOL_ID) != VCM_SI_MODE)
        {
            RipResetContext ();
        }
    }
    UNUSED_PARAM (u4Module);
    UNUSED_PARAM (i1RetVal);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowRunningConfigTableCxt                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current table           */
/*                        configurations of RIP Module of a particular Cxt   */
/*                                                                           */
/*     INPUT            : CliHandle, u1Flag, i4CxtId                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
RipShowRunningConfigTableCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    RipShowConfigNBRListInCxt (CliHandle, i4CxtId);
    RipShowConfigIfTableCxt (CliHandle, i4CxtId);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RipShowRunningConfigInterfaceCxt                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface configuration     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4CxtId  - RIP context Id.                         */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
RipShowRunningConfigInterfaceCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    INT4                i4Index = 0;
    INT4                i4PrevIfIndex = -1;
    INT4                i4NextCxtId = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4PrevRipIfAddr = 0;
    UINT4               u4RipIfAddr = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1HeaderPresent = OSIX_FALSE;
    UINT1               u1Flag = OSIX_FALSE;

    while (nmhGetNextIndexFsMIStdRip2IfConfTable
           (i4CxtId, &i4NextCxtId, u4PrevRipIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4CxtId != i4NextCxtId)
        {
            break;
        }

        if ((i4Index = RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NextCxtId,
                                                           u4RipIfAddr)) !=
            RIP_FAILURE)
        {
            if (i4Index != i4PrevIfIndex)
            {

                MEMSET (&au1IfName[0], 0, CFA_CLI_MAX_IF_NAME_LEN);

                RipShowRunningConfigInterfaceDetailsCxt (CliHandle, i4Index,
                                                         u4RipIfAddr,
                                                         &u1HeaderPresent);
                RipShowRunningConfigInterfaceAggDetailsCxt (CliHandle,
                                                            (UINT4) i4Index,
                                                            u4CfaIfIndex,
                                                            &u1HeaderPresent,
                                                            &u1Flag);

                if ((NetIpv4GetCfaIfIndexFromPort ((UINT4) i4Index,
                                                   &u4CfaIfIndex) ==
                     NETIPV4_SUCCESS) && (u1HeaderPresent == OSIX_TRUE))
                {
                    CfaCliConfGetIfName (u4CfaIfIndex, (INT1 *) &au1IfName[0]);
                    CliPrintf (CliHandle, "\r\n!\r\n");
                    CliPrintf (CliHandle, "interface %s\r\n", au1IfName);
                }
                u1Flag = OSIX_TRUE;

                RipShowRunningConfigInterfaceDetailsCxt (CliHandle, i4Index,
                                                         u4RipIfAddr,
                                                         &u1HeaderPresent);
                RipShowRunningConfigInterfaceAggDetailsCxt (CliHandle,
                                                            (UINT4) i4Index,
                                                            u4CfaIfIndex,
                                                            &u1HeaderPresent,
                                                            &u1Flag);
                u1HeaderPresent = OSIX_FALSE;
            }
            i4PrevIfIndex = i4Index;
        }
        u4PrevRipIfAddr = u4RipIfAddr;
        i4CxtId = i4NextCxtId;
    }
    CliPrintf (CliHandle, "!\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowRunningConfigIfaceAndAggDetailsCxt          */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current running           */
/*                        configuration of RIP Interface configuration &     */
/*                        RIP Interface Aggregations of the Context          */
/*                                                                           */
/*     INPUT            : CliHandle, u4CfaIfIndex                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
RipShowRunningConfigIfaceAndAggDetailsCxt (tCliHandle CliHandle,
                                           UINT4 u4CfaIfIndex)
{
    UINT4               u4Port = 0;
    tNetIpv4IfInfo      NetIpIfInfo;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               u1HeaderPresent = OSIX_FALSE;
    UINT1               u1Flag = OSIX_TRUE;
    CliRegisterLock (CliHandle, RipLock, RipUnLock);
    RipLock ();

    if (NetIpv4GetPortFromIfIndex (u4CfaIfIndex, &u4Port) == NETIPV4_FAILURE)
    {
        RipUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        RipUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }

    pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) u4CfaIfIndex);
    if (pRipCxtEntry == NULL)
    {
        RipUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }

    if (nmhValidateIndexInstanceFsMIStdRip2IfConfTable
        (pRipCxtEntry->i4CxtId, NetIpIfInfo.u4Addr) == SNMP_SUCCESS)
    {
        MEMSET (&au1IfName[0], 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliConfGetIfName (u4CfaIfIndex, (INT1 *) &au1IfName[0]);
        CliPrintf (CliHandle, "interface %s\r\n", au1IfName);
        RipShowRunningConfigInterfaceDetailsCxt (CliHandle, (INT4) u4Port,
                                                 NetIpIfInfo.u4Addr,
                                                 &u1HeaderPresent);
        RipShowRunningConfigInterfaceAggDetailsCxt (CliHandle, u4Port,
                                                    u4CfaIfIndex,
                                                    &u1HeaderPresent, &u1Flag);
        CliPrintf (CliHandle, "!\r\n");
    }

    RipUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowRunningConfigInterfaceDetailsCxt            */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current running           */
/*                                  configuration of                         */
/*                        RIP Interface configuration  of the Context        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4CxtId - RIP Context Id                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
RipShowRunningConfigInterfaceDetailsCxt (tCliHandle CliHandle, INT4 i4Index,
                                         UINT4 u4RipIfAddr,
                                         UINT1 *pu1HeaderPresent)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    INT4                i4RowStatus = 0;
    INT4                i4Val = 0;
    INT4                i4DefaultMetric = 0;
    INT4                i4DefRtInstall = 0;
    INT4                i4CxtId;
    UINT4               u4RipIfGarbgTmr = 0;
    UINT4               u4RipIfRouteAgeTmr = 0;
    UINT4               u4RipIfUpdateTmr = 0;
    UINT1               au1AuthKey[MAX_ADDR_BUFFER_SIZE];
    UINT1               u1Flag = 0;
    tRipCxt            *pRipCxtEntry = NULL;
    INT1                i1RetVal = 0;

    if (*pu1HeaderPresent == OSIX_FALSE)
    {
        u1Flag = 1;
    }

    pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) i4Index);

    if ((pRipCxtEntry == NULL) || (u4RipIfAddr == 0))
    {
        return CLI_SUCCESS;
    }
    i4CxtId = pRipCxtEntry->i4CxtId;

    MEMSET (&au1AuthKey[0], 0, MAX_ADDR_BUFFER_SIZE);

    if (nmhValidateIndexInstanceFsMIStdRip2IfConfTable (i4CxtId, u4RipIfAddr) ==
        SNMP_SUCCESS)
    {
        i1RetVal =
            nmhGetFsMIStdRip2IfConfStatus (i4CxtId, u4RipIfAddr, &i4RowStatus);

        if (i4RowStatus == ACTIVE)
        {
            nmhGetFsMIStdRip2IfConfSend (i4CxtId, u4RipIfAddr, &i4Val);

            switch (i4Val)
            {
                case RIPIF_DO_NOT_SEND:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {
                        CliPrintf (CliHandle, "ip rip send version none\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;

                case RIPIF_VERSION_1_SND:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {
                        CliPrintf (CliHandle, "ip rip send version 1\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;

                case RIPIF_VERSION_2_SND:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {
                        CliPrintf (CliHandle, "ip rip send version 2\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;
                default:
                    break;
            }

            nmhGetFsMIStdRip2IfConfReceive (i4CxtId, u4RipIfAddr, &i4Val);

            switch (i4Val)
            {
                case RIPIF_VERSION_1_RCV:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {
                        CliPrintf (CliHandle, "ip rip receive version 1\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;

                case RIPIF_VERSION_2_RCV:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {
                        CliPrintf (CliHandle, "ip rip receive version 2\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;

                case RIPIF_DO_NOT_RECV:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {
                        CliPrintf (CliHandle,
                                   "ip rip receive version none\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;
                default:
                    break;
            }

            nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr, &i4Val);

            /* Get the interface index from interface ip */
            pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);

            if (pRipIfRec != NULL)
            {
                switch (i4Val)
                {
                    case RIPIF_SIMPLE_PASSWORD:
                        if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                        {
                            CliPrintf (CliHandle,
                                       "ip rip authentication mode text key-chain \r\n");
                        }
                        *pu1HeaderPresent = OSIX_TRUE;
                        break;

                    case RIPIF_MD5_AUTHENTICATION:

                        if (RIP_SLL_Count (&(pRipIfRec->RipMd5KeyList)) != 0)
                        {
                            if ((*pu1HeaderPresent != OSIX_FALSE)
                                && (u1Flag == 0))
                            {
                                CliPrintf (CliHandle,
                                           "ip rip authentication mode md5 key-chain \r\n");
                            }
                            *pu1HeaderPresent = OSIX_TRUE;
                        }
                        else
                        {
                            if ((*pu1HeaderPresent != OSIX_FALSE)
                                && (u1Flag == 0))
                            {
                                CliPrintf (CliHandle,
                                           "ip rip auth-type md5\r\n");
                            }
                            *pu1HeaderPresent = OSIX_TRUE;
                        }
                        break;

                    case RIPIF_SHA1_AUTHENTICATION:
                        if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                        {
                            CliPrintf (CliHandle, "ip rip auth-type sha-1\r\n");
                        }
                        *pu1HeaderPresent = OSIX_TRUE;
                        break;

                    case RIPIF_SHA256_AUTHENTICATION:
                        if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                        {
                            CliPrintf (CliHandle,
                                       "ip rip auth-type sha-256\r\n");
                        }
                        *pu1HeaderPresent = OSIX_TRUE;
                        break;

                    case RIPIF_SHA384_AUTHENTICATION:
                        if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                        {
                            CliPrintf (CliHandle,
                                       "ip rip auth-type sha-384\r\n");
                        }
                        *pu1HeaderPresent = OSIX_TRUE;
                        break;

                    case RIPIF_SHA512_AUTHENTICATION:
                        if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                        {
                            CliPrintf (CliHandle,
                                       "ip rip auth-type sha-512\r\n");
                        }
                        *pu1HeaderPresent = OSIX_TRUE;
                        break;
                    default:
                        break;

                }

                if ((i4Val >= RIPIF_MD5_AUTHENTICATION) &&
                    (RIP_SLL_Count (&(pRipIfRec->RipCryptoAuthKeyList)) != 0))
                {
                    RipShowRunningConfigCryptoAuthDetails (CliHandle, i4CxtId,
                                                           i4Index, u4RipIfAddr,
                                                           pu1HeaderPresent,
                                                           &u1Flag);
                }
            }
            nmhGetFsMIRip2IfConfUpdateTmr (i4CxtId, u4RipIfAddr,
                                           (INT4 *) &u4RipIfUpdateTmr);

            nmhGetFsMIRip2IfConfRouteAgeTmr (i4CxtId, u4RipIfAddr,
                                             (INT4 *) &u4RipIfRouteAgeTmr);

            nmhGetFsMIRip2IfConfGarbgCollectTmr (i4CxtId, u4RipIfAddr,
                                                 (INT4 *) &u4RipIfGarbgTmr);

            if (u4RipIfUpdateTmr != CLI_RIP_DEF_UPDATE_TMR)
            {
                if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                {
                    CliPrintf (CliHandle, "timers basic %d", u4RipIfUpdateTmr);

                    if (u4RipIfRouteAgeTmr != CLI_RIP_DEF_HOLD_TMR)
                    {
                        CliPrintf (CliHandle, " %d", u4RipIfRouteAgeTmr);
                    }

                    if (u4RipIfGarbgTmr != CLI_RIP_DEF_GARBAGE_TMR)
                    {
                        CliPrintf (CliHandle, " %d\r\n", u4RipIfGarbgTmr);
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
                *pu1HeaderPresent = OSIX_TRUE;
            }

            nmhGetFsMIRip2IfSplitHorizonStatus (i4CxtId, u4RipIfAddr, &i4Val);

            switch (i4Val)
            {
                case RIP_SPLIT_HORIZON:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {

                        CliPrintf (CliHandle, "ip split-horizon\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;

                case RIP_NO_SPLIT_HORIZON:
                    if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                    {

                        CliPrintf (CliHandle, "no ip split-horizon\r\n");
                    }
                    *pu1HeaderPresent = OSIX_TRUE;
                    break;
                default:
                    break;
            }

            nmhGetFsMIRip2IfConfDefRtInstall (i4CxtId, u4RipIfAddr,
                                              &i4DefRtInstall);
            if (i4DefRtInstall == RIP_INSTALL_DEF_RT)
            {
                if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                {

                    CliPrintf (CliHandle, "ip rip default route install\r\n");
                }
                *pu1HeaderPresent = OSIX_TRUE;
            }

            nmhGetFsMIStdRip2IfConfDefaultMetric (i4CxtId, u4RipIfAddr,
                                                  &i4DefaultMetric);
            if (i4DefaultMetric != RIP_NO_DEF_RT_ORIGINATE)
            {
                if ((*pu1HeaderPresent != OSIX_FALSE) && (u1Flag == 0))
                {
                    CliPrintf (CliHandle,
                               "ip rip default route originate %d\r\n",
                               i4DefaultMetric);
                }
                *pu1HeaderPresent = OSIX_TRUE;
            }
        }
    }
    if ((*pu1HeaderPresent == OSIX_TRUE) && (u1Flag == 0))
    {
        CliPrintf (CliHandle, "\r\n");
    }
    UNUSED_PARAM (i1RetVal);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowRunningConfigCryptoAuthDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the configured authentication */
/*                        key information on the RIP interface               */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        pRipCxtEntry - RIP Context Pointer                 */
/*                        i4Index - Interface Index(Port number)             */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
RipShowRunningConfigCryptoAuthDetails (tCliHandle CliHandle,
                                       INT4 i4CxtId, INT4 i4Index,
                                       UINT4 u4RipIfAddr,
                                       UINT1 *pu1HeaderPresent, UINT1 *u1Flag)
{
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4IfIndex = 0;
    INT4                i4KeyId = 0;
    UINT4               u4AuthAddress = 0;
    tCryptoAuthKeyInfo *pNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tSNMP_OCTET_STRING_TYPE AuthInfo;
    UINT1               au1AuthInfo[RIP_DST_TIME_LEN + 1];

    MEMSET (au1AuthInfo, 0, sizeof (au1AuthInfo));
    AuthInfo.pu1_OctetList = au1AuthInfo;
    AuthInfo.i4_Length = 0;

    pRipCxtEntry = RipGetCxtInfoRec (i4CxtId);

    pRipIfRec = RipGetIfRec ((UINT4) i4Index, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        UNUSED_PARAM (u4RipIfAddr);
        return CLI_FAILURE;
    }

    RipGetIfIndexFromPort ((UINT4) i4Index, &u4IfIndex);

    /* Scan through the entries for this interface */
    RIP_SLL_Scan (&(pRipIfRec->RipCryptoAuthKeyList), pNode,
                  tCryptoAuthKeyInfo *)
    {
        i4KeyId = pNode->u1AuthKeyId;
        if ((*pu1HeaderPresent != OSIX_FALSE) && (*u1Flag == 0))
        {
            /* Get the Crypto Authentication key details */
            nmhGetFsMIRipCryptoAuthKey (i4CxtId, (INT4) u4IfIndex,
                                        u4AuthAddress, i4KeyId, &AuthInfo);
            CliPrintf (CliHandle, "ip rip authentication key-id %d key %s\r\n",
                       i4KeyId, AuthInfo.pu1_OctetList);

            nmhGetFsMIRipCryptoKeyStartAccept (i4CxtId, (INT4) u4IfIndex,
                                               u4AuthAddress, i4KeyId,
                                               &AuthInfo);
            CliPrintf (CliHandle, "ip rip key-id %d start-accept %s\r\n",
                       i4KeyId, AuthInfo.pu1_OctetList);

            nmhGetFsMIRipCryptoKeyStopAccept (i4CxtId, (INT4) u4IfIndex,
                                              u4AuthAddress, i4KeyId,
                                              &AuthInfo);
            CliPrintf (CliHandle, "ip rip key-id %d stop-accept %s\r\n",
                       i4KeyId, AuthInfo.pu1_OctetList);

            nmhGetFsMIRipCryptoKeyStartGenerate (i4CxtId, (INT4) u4IfIndex,
                                                 u4AuthAddress, i4KeyId,
                                                 &AuthInfo);
            CliPrintf (CliHandle, "ip rip key-id %d start-generate %s\r\n",
                       i4KeyId, AuthInfo.pu1_OctetList);

            nmhGetFsMIRipCryptoKeyStopGenerate (i4CxtId, (INT4) u4IfIndex,
                                                u4AuthAddress, i4KeyId,
                                                &AuthInfo);
            CliPrintf (CliHandle, "ip rip key-id %d stop-generate %s\r\n",
                       i4KeyId, AuthInfo.pu1_OctetList);
            *pu1HeaderPresent = OSIX_TRUE;
        }
    }
    UNUSED_PARAM (u4RipIfAddr);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RipShowRunningConfigInterfaceAggDetailsCxt         */
/*                                                                           */
/*     DESCRIPTION      : This function displays the currently operating     */
/*                        RIP interface aggregation configurations for a     */
/*                        particular interface.                              */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4CfaIfIndex  - The interface index                */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
INT4
RipShowRunningConfigInterfaceAggDetailsCxt (tCliHandle CliHandle,
                                            UINT4 u4Port, UINT4 u4CfaIfIndex,
                                            UINT1 *pu1HeaderPresent,
                                            UINT1 *u1Flag)
{
    UINT4               u4AggAddr = 0;
    UINT4               u4AggMask = 0;
    UINT4               u4NxtAddr = 0;
    UINT4               u4NxtMask = 0;
    INT4                i4NxtIndex = 0;
    INT4                i4CxtId, i4NextCxtId = 0;
    CHR1               *pc1AggAddr = NULL;
    CHR1               *pc1AggMask = NULL;
    tRipCxt            *pRipCxtEntry = NULL;

    pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) u4Port);

    if (pRipCxtEntry == NULL)
    {
        return CLI_SUCCESS;
    }

    i4CxtId = pRipCxtEntry->i4CxtId;

    while (nmhGetNextIndexFsMIRipAggTable
           (i4CxtId, &i4NextCxtId, (INT4) u4CfaIfIndex, &i4NxtIndex, u4AggAddr,
            &u4NxtAddr, u4AggMask, &u4NxtMask) != SNMP_FAILURE)
    {
        if ((i4CxtId != i4NextCxtId) || (i4NxtIndex != (INT4) u4CfaIfIndex))
        {
            return CLI_SUCCESS;
        }
        u4CfaIfIndex = (UINT4) i4NxtIndex;
        u4AggAddr = u4NxtAddr;
        u4AggMask = u4NxtMask;
        CLI_CONVERT_IPADDR_TO_STR (pc1AggAddr, u4AggAddr);
        if ((*pu1HeaderPresent != OSIX_FALSE) && (*u1Flag == OSIX_TRUE))
        {
            CliPrintf (CliHandle, "\r\n ip rip summary-address %s ",
                       pc1AggAddr);
        }

        *pu1HeaderPresent = OSIX_TRUE;
        CLI_CONVERT_IPADDR_TO_STR (pc1AggMask, u4AggMask);
        CliPrintf (CliHandle, " %s", pc1AggMask);

    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*   Function Name :CliRipEnableInterface                                 */
/*                                                                        */
/*   Description   :To Make Row Active                                    */
/*                                                                        */
/*   Input(s)      : u4RipIfAddr - Contains IP Addr                       */
/*   Output(s)     : .If Success none else Error                          */
/*   Return Values : .OSIX_SUCCESS/OSIX_FAILURE                           */
/* ************************************************************************/

INT4
CliRipEnableInterface (UINT4 u4RipIfAddr)
{

    if (nmhSetRip2IfConfStatus (u4RipIfAddr, ACTIVE) == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name : RipSetTraceValue                                          */
/* Description   : Sets the Trace Value                                      */
/* Input(s)      : i4TrapLevel - TrapLevel                                   */
/*                 u1LoggingCmd - Set or Reset                               */
/* Return Values : CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/
INT4
RipSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd)
{
    UINT4               u4Trace = 0;
    INT4                i4AdminStatus;
    tCliHandle          CliHandle = 0;
    RipLock ();
    if ((RipSetContext (RIP_DEFAULT_CXT) == SNMP_FAILURE))
    {
        CliPrintf (CliHandle, "\r %% Invalid RIP Context Id\n\r");
        RipUnLock ();
        return CLI_FAILURE;
    }
    if ((nmhGetFsRipAdminStatus (&i4AdminStatus) == SNMP_SUCCESS)
        && (i4AdminStatus == RIP_ADMIN_DISABLE))
    {
        RipUnLock ();
        return CLI_FAILURE;
    }
    RipUnLock ();

    if (i4TrapLevel == SYSLOG_CRITICAL_LEVEL)
    {
        u4Trace = (ALL_FAILURE_TRC | RIP_CRITICAL_TRC);
    }
    else if (i4TrapLevel == SYSLOG_INFO_LEVEL)
    {
        u4Trace = RIP_TRC_FLAG_MAX;
    }
    if (RipSetDebugLevel (1, u1LoggingCmd, (INT4) u4Trace) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : RipSetDebugLevel                                      */
/*                                                                         */
/*   Description   :To Set RIP Trace level                                 */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. CLI_ENABLE / CLI_DISABLE - to set / reset the     */
/*                       trace level                                       */
/*                    3. u4TraceLevel - trace level                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetDebugLevel (tCliHandle CliHandle, UINT4 u4Flag, INT4 i4TraceLevel)
{
    UINT4               u4ErrorCode;
    INT4                i4OriginalTrace;
    if (u4Flag == CLI_DISABLE)
    {
        if (nmhGetFsRipTrcFlag (&i4OriginalTrace) == SNMP_SUCCESS)
        {
            i4TraceLevel = i4OriginalTrace & (~i4TraceLevel);
        }
        else
        {
            return (CLI_FAILURE);
        }
    }
    if (u4Flag == CLI_ENABLE)
    {
        if (nmhGetFsRipTrcFlag (&i4OriginalTrace) == SNMP_SUCCESS)
        {
            i4TraceLevel |= i4OriginalTrace;
        }
        else
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2FsRipTrcFlag (&u4ErrorCode, i4TraceLevel) == SNMP_SUCCESS)
    {
        if (nmhSetFsRipTrcFlag (i4TraceLevel) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipEnableRouter                                       */
/*   Description   :To enter the router configuration mode                 */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipEnableRouter (tCliHandle CliHandle, INT4 i4CxtId)
{
    UINT1               au1IfName[MAX_PROMPT_LEN + 1];
    INT4                i4GetVal;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus;

    /* Check whether IP forwarding is enabled */
    nmhGetIpForwarding (&i4GetVal);

    if (i4GetVal != IP_FORW_ENABLE)
    {
        CliPrintf (CliHandle, "\r%% IP routing is disabled\r\n");
        return (CLI_FAILURE);
    }

    if (nmhGetFsMIRipRowStatus (i4CxtId, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsMIRipRowStatus (&u4ErrorCode, i4CxtId, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsMIRipRowStatus (i4CxtId, CREATE_AND_GO) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2FsMIRipAdminStatus (&u4ErrorCode, i4CxtId, RIP_ADMIN_ENABLE) ==
        SNMP_SUCCESS)
    {
        if (nmhSetFsMIRipAdminStatus (i4CxtId, RIP_ADMIN_ENABLE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    /* Enter the router configuration mode */

    SPRINTF ((CHR1 *) au1IfName, "%s%d", CLI_RIP_ROUTER_MODE, i4CxtId);

    /* Return the router configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipDisableRouter                                      */
/*                                                                         */
/*   Description   :To disable RIP on all the interfaces                   */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipDisableRouter (tCliHandle CliHandle, INT4 i4CxtId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMIRipAdminStatus (&u4ErrorCode, i4CxtId, RIP_ADMIN_DISABLE)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsMIRipAdminStatus (i4CxtId, RIP_ADMIN_DISABLE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsMIRipRowStatus (&u4ErrorCode, i4CxtId, DESTROY)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }
    if (nmhSetFsMIRipRowStatus (i4CxtId, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipCtrlLastAuthenticationKey                          */
/*                                                                         */
/*   Description   :To control the last authentication key lifetime status */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI Handle                            */
/*                    2. i4CxtId -  context ID                             */
/*                    3. i4LifeTimeStatus -  Lifetime status               */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipCtrlLastAuthenticationKey (tCliHandle CliHandle, INT4 i4CxtId,
                              INT4 i4LifeTimeStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4AdminStatus;

    if (nmhGetFsMIRipAdminStatus (i4CxtId, &i4AdminStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4AdminStatus == RIP_ADMIN_ENABLE)
    {
        if (nmhTestv2FsMIRip2LastAuthKeyLifetimeStatus
            (&u4ErrorCode, i4CxtId, i4LifeTimeStatus) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIRip2LastAuthKeyLifetimeStatus (i4CxtId, i4LifeTimeStatus)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/***************************************************************************/
/*   Function Name : RipSetSecurity                                        */
/*                                                                         */
/*   Description   : To set the security level which specifies whether     */
/*                   RIPv1 packets should be accepted or ignored when      */
/*                   authentication is in use.                             */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4SecurityLevel - RIP security level              */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetSecurity (tCliHandle CliHandle, INT4 i4SecurityLevel)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRip2Security (&u4ErrorCode, i4SecurityLevel) == SNMP_SUCCESS)
    {
        if (nmhSetFsRip2Security (i4SecurityLevel) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetRetransmissionInt                               */
/*                                                                         */
/*   Description   : To set the retransmission interval                    */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4RipRetransTmr - timeout value                   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetRetransmissionInt (tCliHandle CliHandle, INT4 i4RipRetransTmr)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRip2RetransTimeoutInt (&u4ErrorCode, i4RipRetransTmr)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRip2RetransTimeoutInt (i4RipRetransTmr) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetRetransmissionRetries                           */
/*                                                                         */
/*   Description   : To set the retransmission retries                     */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4RipRetransRetries - retransmission retries      */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetRetransmissionRetries (tCliHandle CliHandle, INT4 i4RipRetransRetries)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRip2MaxRetransmissions (&u4ErrorCode, i4RipRetransRetries)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRip2MaxRetransmissions (i4RipRetransRetries) ==
            SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetNetwork                                         */
/*                                                                         */
/*   Description   : To enable RIP on an IP network                        */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. u4RipIfConfSrcAddr - source ip address            */
/*                    3. u4RipIfConfAddr - ip address                      */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetNetwork (tCliHandle CliHandle, UINT4 u4RipIfConfSrcAddr,
               UINT4 u4RipIfConfAddr)
{
    INT4                i4RowStatus;
    INT4                i4AdminStat = 0;
    UINT4               u4ErrorCode;
    UINT4               u4IfAddr;

    if (u4RipIfConfAddr <= CLI_RIP_IPIF_NUMBER_OF_UNNUMBERED_INTERFACES)
    {
        if (RipCliGetIfAddrFromIfIndex
            (CliHandle, (INT4) u4RipIfConfAddr, &u4IfAddr) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }

        /* this check is to avoid enabling rip on a numbered interface
           assuming it to be an unnumbered interface */
        if (u4IfAddr != u4RipIfConfAddr)
        {
            CLI_SET_ERR (CLI_RIP_NOT_UNNUM_IF);
            return CLI_FAILURE;
        }
    }

    if (nmhGetRip2IfConfStatus (u4RipIfConfAddr, &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2Rip2IfConfStatus (&u4ErrorCode, u4RipIfConfAddr,
                                       RIPIF_ADMIN_CREATE_AND_WAIT) !=
            SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetRip2IfConfStatus
            (u4RipIfConfAddr, RIPIF_ADMIN_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhTestv2Rip2IfConfSrcAddress
            (&u4ErrorCode, u4RipIfConfAddr, u4RipIfConfSrcAddr) == SNMP_FAILURE)
        {
            nmhSetRip2IfConfStatus (u4RipIfConfAddr, DESTROY);
            return (CLI_FAILURE);
        }
        if (nmhSetRip2IfConfSrcAddress (u4RipIfConfAddr, u4RipIfConfSrcAddr) ==
            SNMP_FAILURE)
        {
            nmhSetRip2IfConfStatus (u4RipIfConfAddr, DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetRip2IfConfDefaultMetric
            (u4RipIfConfAddr, RIP_NO_DEFAULT_ROUTE_METRIC) == SNMP_FAILURE)
        {
            nmhSetRip2IfConfStatus (u4RipIfConfAddr, DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetRip2IfConfStatus (u4RipIfConfAddr, RIPIF_ADMIN_ACTIVE)
            == SNMP_FAILURE)
        {
            nmhSetRip2IfConfStatus (u4RipIfConfAddr, DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2FsRip2IfAdminStat (&u4ErrorCode, u4RipIfConfAddr,
                                    RIP_ADMIN_ENABLE) == SNMP_SUCCESS)
    {

        /* for Network command enabled for Secondary IP as the Index is same as the
         * Primary IP the ADMIN-FLAG properties will be copied while creating interface record.
         * If the Admin flag is passive need to check and need not enable again for Secondary 
         * IP while creating the Interface Record*/

        nmhGetFsRip2IfAdminStat (u4RipIfConfAddr, &i4AdminStat);

        if ((i4AdminStat != RIP_ZERO) && (i4AdminStat == RIP_ADMIN_PASSIVE))
        {
            return (CLI_SUCCESS);
        }

        if (nmhSetFsRip2IfAdminStat (u4RipIfConfAddr, RIP_ADMIN_ENABLE)
            == SNMP_FAILURE)
        {
            nmhSetRip2IfConfStatus (u4RipIfConfAddr, DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        return (CLI_SUCCESS);
    }
    else
    {
        return (CLI_FAILURE);
    }
}

/***************************************************************************/
/*   Function Name : RipSetNoNetwork                                       */
/*                                                                         */
/*   Description   : To disable RIP on an IP network                       */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. u4RipIfConfSrcAddr - source ip address            */
/*                    2. u4RipIfConfAddr - address on which rip is disabled*/
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetNoNetwork (tCliHandle CliHandle, UINT4 u4RipIfConfSrcAddr,
                 UINT4 u4RipIfConfAddr)
{
    INT4                i4RowStatus;

    if (u4RipIfConfAddr == 0)
    {
        u4RipIfConfAddr = u4RipIfConfSrcAddr;
    }

    /* Check RIP interface in the RIP table. */
    if (nmhGetRip2IfConfStatus (u4RipIfConfAddr, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_INVALID_NETWORK_ERR);
        return (CLI_FAILURE);
    }

    if (nmhSetRip2IfConfStatus (u4RipIfConfAddr, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetNeighbor                                        */
/*                                                                         */
/*   Description   :To enable the trusted neighbor feature and configure a */
/*                  trusted neighbor                                       */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. u4NeighborIpAddr - IP address of the neighbor     */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetNeighbor (tCliHandle CliHandle, UINT4 u4NbrIpAddr)
{
    UINT4               u4ErrorCode = 0;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return CLI_FAILURE;
    }
    /* Check if the trusted neighbor feature is disabled */
    if (pRipCxtEntry->RipNbrListCfg.u1RipNbrListStatus == RIP_NBR_LIST_DISABLE)
    {
        /* Enable the trusted neighbor feature */
        if (nmhTestv2FsRip2TrustNBRListEnable
            (&u4ErrorCode, RIP_NBR_LIST_ENABLE) == SNMP_SUCCESS)
        {
            if (nmhSetFsRip2TrustNBRListEnable (RIP_NBR_LIST_ENABLE) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
        {
            return (CLI_FAILURE);
        }
    }
    /* Configure the trusted neighbor */

    if (nmhTestv2FsRip2TrustNBRRowStatus (&u4ErrorCode,
                                          u4NbrIpAddr,
                                          CREATE_AND_GO) == SNMP_SUCCESS)
    {
        if (nmhSetFsRip2TrustNBRRowStatus (u4NbrIpAddr, CREATE_AND_GO)
            == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    else
    {
        switch (u4ErrorCode)
        {
            case SNMP_ERR_INCONSISTENT_VALUE:
                return CLI_SUCCESS;

            case SNMP_ERR_NO_CREATION:
                CLI_SET_ERR (CLI_RIP_NEIGHBOR_ERR);
                return CLI_FAILURE;

            case SNMP_ERR_INCONSISTENT_NAME:
                CLI_SET_ERR (CLI_RIP_OUROWN_ADDR_NBR_ERR);
                return CLI_FAILURE;

            case SNMP_ERR_WRONG_VALUE:
                CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
                return CLI_FAILURE;

            case SNMP_ERR_WRONG_TYPE:
                CLI_SET_ERR (CLI_RIP_INVALID_IP_ADDR);
                return CLI_FAILURE;

            default:
                break;
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetNoNeighbor                                      */
/*                                                                         */
/*   Description   :To delete the trusted neighbor and disable the trusted */
/*                  trusted neighbor feature if the trusted neighbor list  */
/*                  is empty                                               */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. u4NeighborIpAddr - IP address of the neighbor     */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetNoNeighbor (tCliHandle CliHandle, UINT4 u4NbrIpAddr)
{
    UINT4               u4ErrorCode;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsRip2TrustNBRRowStatus (&u4ErrorCode,
                                          u4NbrIpAddr, DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_NEIGHBOR_NOT_EXISTS_ERR);
        return (CLI_FAILURE);
    }
    else
    {
        if (nmhSetFsRip2TrustNBRRowStatus (u4NbrIpAddr, DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Disable the trusted neighbor feature if the trusted neighbor list is
     * empty
     */
    if (pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs == 0)
    {
        if (nmhTestv2FsRip2TrustNBRListEnable (&u4ErrorCode,
                                               RIP_NBR_LIST_DISABLE)
            == SNMP_SUCCESS)
        {
            if (nmhSetFsRip2TrustNBRListEnable (RIP_NBR_LIST_DISABLE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
        {
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetPassiveIntf                                     */
/*                                                                         */
/*   Description   :To suppress routing updates on an interface            */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                    3. i4AdminStatus - admin status                      */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetPassiveIntf (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4AdminStatus)
{
    UINT4               u4ErrorCode;
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* The interface index is same for the primary IP and the Secondary IP enabled
     * on that interface and hence get the index and loop through all the Primary 
     * and secondary IP to set the interface level properties*/
    do
    {

        /* Check for the configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            u4ErrorCode = 0;

            /*Configure The Admin Status For the RIP Interface */
            if (nmhTestv2FsRip2IfAdminStat (&u4ErrorCode, u4RipIfAddr,
                                            i4AdminStatus) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_WRONG_ADMIN_STAT);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRip2IfAdminStat (u4RipIfAddr, i4AdminStatus)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_ADMIN_STAT_NOT_SET);
                return (CLI_FAILURE);
            }

            i4RetVal = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetSpacing                                         */
/*                                                                         */
/*   Description   :To enable/disable spacing                              */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4SpacingStatus - RIP_SPACING_ENABLE /            */
/*                                         RIP_SPACING_DISABLE             */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetSpacing (tCliHandle CliHandle, INT4 i4SpacingStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRip2SpacingEnable (&u4ErrorCode, i4SpacingStatus)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRip2SpacingEnable (i4SpacingStatus) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetRouteRedistribute                               */
/*                                                                         */
/*   Description   :To enable redistribution of routes                     */
/*                                                                         */
/*   Input(s)      :  1. CliHandle  - CLI context ID                       */
/*                    2. i4Protocol - protocol identifier                  */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetRouteRedistribute (tCliHandle CliHandle, INT4 i4Protocol,
                         UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RouteMapName.i4_Length = 0;

    if (nmhSetFsRipRRDGlobalStatus (RIP_CLI_RRD_ENABLE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (pu1RMapName != NULL)
    {
        RouteMapName.i4_Length = (INT4) STRLEN (pu1RMapName);
        RouteMapName.pu1_OctetList = au1RMapName;
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);
        RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;

        if (nmhTestv2FsRipRRDRouteMapEnable (&u4ErrorCode, &RouteMapName)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsRipRRDRouteMapEnable (&RouteMapName) == SNMP_FAILURE)

        {
            CLI_SET_ERR (CLI_RIP_INV_ASSOC_ROUTE_MAP);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsRipRRDSrcProtoMaskEnable (&u4ErrorCode, i4Protocol)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRipRRDSrcProtoMaskEnable (i4Protocol) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if ((i4Protocol == RIP_RRD_STATIC_MASK) || (i4Protocol == RIP_RRD_ALL_MASK))
    {
        /* When RRD is not enabled,redistribute of static routes 
         * alone is allowed into RIP */
        if (nmhTestv2FsRip2Propagate (&u4ErrorCode, RIP_CLI_RRD_ENABLE)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsRip2Propagate (RIP_CLI_RRD_ENABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetNoRouteRedistribute                             */
/*                                                                         */
/*   Description   : To disable redistribution of routes                   */
/*                                                                         */
/*   Input(s)      : 1. CliHandle  - CLI context ID                        */
/*                   2. i4Protocol - protocol identifier                   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetNoRouteRedistribute (tCliHandle CliHandle, INT4 i4Protocol,
                           UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = 0;

    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 1];

    if (pu1RMapName != NULL)
    {
        RouteMapName.i4_Length = (INT4) STRLEN (pu1RMapName);
        RouteMapName.pu1_OctetList = au1RMapName;
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);
        RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;

        if (nmhTestv2FsRipRRDRouteMapEnable (&u4ErrorCode, &RouteMapName) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RIP_INV_DISASSOC_ROUTE_MAP);
            return (CLI_FAILURE);
        }

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        if (nmhSetFsRipRRDRouteMapEnable (&RouteMapName) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RIP_INV_DISASSOC_ROUTE_MAP);
            return (CLI_FAILURE);
        }

    }

    /* Reset the protocol disable mask */
    if (nmhTestv2FsRipRRDSrcProtoMaskDisable (&u4ErrorCode, i4Protocol)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRipRRDSrcProtoMaskDisable (i4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* If redistribution of routes for all protocols should be disabled,
     * then disable RRDGlobalStatus
     */

    if (i4Protocol == RIP_RRD_ALL_MASK)
    {
        if (nmhSetFsRipRRDGlobalStatus (RIP_CLI_RRD_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if ((i4Protocol == RIP_RRD_STATIC_MASK) || (i4Protocol == RIP_RRD_ALL_MASK))
    {
        /* When RRD is not enabled,redistribute of static routes 
           alone is allowed into RIP */
        if (nmhTestv2FsRip2Propagate (&u4ErrorCode, RIP_CLI_RRD_DISABLE)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsRip2Propagate (RIP_CLI_RRD_DISABLE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetSendAndRecvVersion                              */
/*                                                                         */
/*   Description   :To set the send version for an interface               */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4RecvVersion -  version number                   */
/*                    3. i4Version - send version number                   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetSendAndRecvVersion (tCliHandle CliHandle, INT4 i4SendVersion,
                          INT4 i4RecvVersion)
{

    INT4                i4ContextId = 0;
    UINT4               u4PrevRipIfAddr = 0;
    UINT4               u4RipIfAddr = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4NxtCxtId = 0;
    INT4                i4Port = -1;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4ErrorCode;
    INT4                i4RowStatus;
    INT4                i4RipSetContext;

    if (nmhGetFirstIndexFsMIRip2GlobalTable (&i4ContextId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    /* Walk through all interfaces on which RIP is enabled */

    while (nmhGetNextIndexFsMIRip2IfConfTable
           (i4ContextId, &i4NxtCxtId, u4PrevRipIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4ContextId != i4NxtCxtId)
        {
            break;
        }

        if ((i4Port = RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NxtCxtId,
                                                          u4RipIfAddr)) !=
            RIP_FAILURE)
        {
            pRipCxtEntry = RipGetCxtInfoRecFrmIface ((UINT2) i4Port);
            if (pRipCxtEntry == NULL)
            {
                continue;
            }
            else
            {
                i4RipSetContext = RipSetContext ((UINT4) pRipCxtEntry->i4CxtId);
                UNUSED_PARAM (i4RipSetContext);
                /* check for the Configured RIP interface in the RIP table */
                if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) ==
                    SNMP_FAILURE)
                {
                    RipReleaseContext ();
                    i4RetStatus = CLI_FAILURE;
                    CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
                    continue;
                }

                /*Configure The Send Type For the RIP Interface */
                if (nmhTestv2Rip2IfConfSend
                    (&u4ErrorCode, u4RipIfAddr, i4SendVersion) == SNMP_SUCCESS)
                {
                    if (nmhSetRip2IfConfSend (u4RipIfAddr, i4SendVersion) ==
                        SNMP_SUCCESS)
                    {
                        i4RetStatus = CLI_SUCCESS;
                    }
                    else
                    {
                        i4RetStatus = CLI_FAILURE;
                        CLI_FATAL_ERROR (CliHandle);
                    }
                }
                /*Configure The Recv  Type For the RIP Interface */

                if (nmhTestv2Rip2IfConfReceive
                    (&u4ErrorCode, u4RipIfAddr, i4RecvVersion) == SNMP_SUCCESS)
                {
                    if (nmhSetRip2IfConfReceive (u4RipIfAddr, i4RecvVersion) ==
                        SNMP_SUCCESS)
                    {
                        i4RetStatus = CLI_SUCCESS;
                    }
                    else
                    {
                        i4RetStatus = CLI_FAILURE;
                        CLI_FATAL_ERROR (CliHandle);
                    }
                }
                RipReleaseContext ();
            }

        }
        u4PrevRipIfAddr = u4RipIfAddr;

    }
    if (u4RipIfAddr == RIP_ZERO)
    {
        CliPrintf (CliHandle,
                   "\r%%RIP : version need to be configured after the interface configuration\r\n");
    }
    return i4RetStatus;

}

/***************************************************************************/
/*  Function Name  : RipCliSetDistribute                                   */
/*  Description    : This Routine Enable or Disable route map filtering.   */
/*  Input(s)       : 1. CliHandle        -  CLI context ID                 */
/*                   2. pu1RouteMapName  -  Route map name                 */
/*                   3. u4Seqno          -  Sequence number                */
/*                   4. u1Fltr           -  in/out.                        */
/*                   5. u1Status         -  Enable/Disable.                */
/*   Output(s)     : None.                                                 */
/*   Return Values : CLI_SUCCESS/CLI_FAILURE.                              */
/***************************************************************************/
INT4
RipCliSetDistribute (tCliHandle CliHandle, UINT1 *pu1RMapName,
                     UINT1 u1Fltr, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_RIP_RMAP_ASSOC_FAILED;
    UINT1               u1FilterType;
    INT4                i4RowStatus;
    UNUSED_PARAM (CliHandle);
    if (u1Fltr == CLI_RIP_IN_FILTER)
    {
        u1FilterType = FILTERING_TYPE_DISTRIB_IN;
    }
    else if (u1Fltr == CLI_RIP_OUT_FILTER)
    {
        u1FilterType = FILTERING_TYPE_DISTRIB_OUT;
    }
    else
    {
        CLI_SET_ERR (u4CliError);
        return (CLI_FAILURE);
    }

    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = (INT4) STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (CLI_ENABLE == u1Status)
        {
            if (nmhGetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                       u1FilterType,
                                                       &i4RowStatus) !=
                SNMP_SUCCESS)
            {
                if (nmhTestv2FsRipDistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, u1FilterType,
                     CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                               u1FilterType,
                                                               CREATE_AND_WAIT)
                        == SNMP_SUCCESS)
                    {
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }
                }
            }
            else
            {
                u4ErrorCode = SNMP_ERR_NO_ERROR;
            }
            u4ErrorCode = SNMP_ERR_NO_ERROR;

            if (SNMP_ERR_NO_ERROR == u4ErrorCode)
            {
                if (nmhTestv2FsRipDistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, u1FilterType,
                     ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                               u1FilterType,
                                                               ACTIVE) ==
                        SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
        else if (CLI_DISABLE == u1Status)
        {
            if (nmhGetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                       u1FilterType,
                                                       &i4RowStatus) ==
                SNMP_SUCCESS)
            {
                if (nmhTestv2FsRipDistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, u1FilterType,
                     DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                               u1FilterType,
                                                               DESTROY) ==
                        SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetSendVersion                                     */
/*                                                                         */
/*   Description   :To set the send version for an interface               */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                    3. i4Version - send version number                   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetSendVersion (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Version)
{
    UINT4               u4ErrorCode;
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */
    do
    {

        /* check for the Configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            /*Configure The Send Type For the RIP Interface */
            if (nmhTestv2Rip2IfConfSend (&u4ErrorCode, u4RipIfAddr, i4Version)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_WRONG_SEND_TYPE);
                return (CLI_FAILURE);
            }
            if (nmhSetRip2IfConfSend (u4RipIfAddr, i4Version) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_SEND_TYPE_NOT_SET);
                return (CLI_FAILURE);
            }

            i4RetVal = CLI_SUCCESS;
        }

    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetReceiveVersion                                  */
/*                                                                         */
/*   Description   :To set the receive version for an interface            */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                    3. i4Version - receive version number                */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetReceiveVersion (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Version)
{
    UINT4               u4ErrorCode;
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */
    do
    {
        /* check for the Configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {
            /*Configure The Send Type For the RIP Interface */

            if (nmhTestv2Rip2IfConfReceive
                (&u4ErrorCode, u4RipIfAddr, i4Version) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_WRONG_RECEIVE_TYPE);
                return (CLI_FAILURE);
            }
            if (nmhSetRip2IfConfReceive (u4RipIfAddr, i4Version) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_RECEIVE_TYPE_NOT_SET);
                return (CLI_FAILURE);
            }
            i4RetVal = CLI_SUCCESS;
        }

    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetAuthenticationKey                                  */
/*                                                                         */
/*   Description   :To set the authentication mode and key for an interface*/
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. i4IfIndex       - interface index                 */
/*                    3. i4AuthMode      - authentication mode             */
/*                    4. pu1RipIfAuthKey - pointer to authentication key   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetAuthenticationKey (tCliHandle CliHandle, INT4 i4IfIndex,
                         UINT1 *pu1RipIfAuthKey)
{
    INT4                i4ContextId = 0;
    UINT4               u4RipIfAddr = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RipIfAuthType = 0;
    INT4                i4RetVal = 0;

    u4IfIndex = (UINT4) CLI_GET_VLANID ();
    i4RetVal = nmhGetIfIpAddr ((INT4) u4IfIndex, &u4RipIfAddr);

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetFirstIndexFsMIRip2GlobalTable (&i4ContextId);

    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4ContextId, u4RipIfAddr,
                                                &i4RipIfAuthType);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;
    }

    if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Authentication mode is not configured \r\n");
        return CLI_FAILURE;
    }

    if (RipSetContext ((UINT4) i4ContextId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (CLI_FAILURE ==
        RipSetAuthentication (CliHandle, i4IfIndex, i4RipIfAuthType,
                              pu1RipIfAuthKey))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    RipResetContext ();

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : RipSetAuthentication                                  */
/*                                                                         */
/*   Description   :To set the authentication mode and key for an interface*/
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. i4IfIndex       - interface index                 */
/*                    3. i4AuthMode      - authentication mode             */
/*                    4. pu1RipIfAuthKey - pointer to authentication key   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetAuthentication (tCliHandle CliHandle, INT4 i4IfIndex,
                      INT4 i4RipIfAuthType, UINT1 *pu1RipIfAuthKey)
{

    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4Md5KeyId = 1;
    INT4                i4StartTime = 0;
    INT4                i4ExpiryTime = -1;
    tSNMP_OCTET_STRING_TYPE sOctetStrRipAuthKey;
    INT4                i4RowStatus;
    UINT1               au1AuthKey[MAX_AUTH_KEY_LENGTH + 1];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    MEMSET (&au1AuthKey[0], 0, MAX_AUTH_KEY_LENGTH + 1);

    if (pu1RipIfAuthKey != NULL)
    {
        sOctetStrRipAuthKey.i4_Length = (INT4) STRLEN (pu1RipIfAuthKey);
        sOctetStrRipAuthKey.pu1_OctetList = &au1AuthKey[0];

        STRNCPY (sOctetStrRipAuthKey.pu1_OctetList, pu1RipIfAuthKey,
                 STRLEN (pu1RipIfAuthKey));
        sOctetStrRipAuthKey.pu1_OctetList[STRLEN (pu1RipIfAuthKey)] = '\0';
    }
    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {

        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            /*If The Auth Type is Md5, Md5 Auth Table is Configured */
            /*Before setting AuthType for RipIntConf Table it expects
             * MD5 Table RowStatus to be Active*/
            if (i4RipIfAuthType == MD5AUTHTYPE)
            {
                /*Set the Entries for Md5 Auth Table and Make the Row Active */
                nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                            CREATE_AND_WAIT);

                nmhSetFsRipMd5KeyStartTime (u4RipIfAddr, i4Md5KeyId,
                                            i4StartTime);

                nmhSetFsRipMd5KeyExpiryTime (u4RipIfAddr, i4Md5KeyId,
                                             i4ExpiryTime);

                if (nmhTestv2FsRipMd5AuthKey (&u4ErrorCode, u4RipIfAddr,
                                              i4Md5KeyId, &sOctetStrRipAuthKey)
                    == SNMP_FAILURE)
                {
                    nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                DESTROY);
                    return (CLI_FAILURE);
                }

                if (nmhSetFsRipMd5AuthKey
                    (u4RipIfAddr, i4Md5KeyId,
                     &sOctetStrRipAuthKey) == SNMP_FAILURE)
                {
                    nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                DESTROY);
                    return (CLI_FAILURE);
                }

                nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId, ACTIVE);
            }

            /*Configure Authentication Mode for the Interface */
            if (nmhTestv2Rip2IfConfAuthType (&u4ErrorCode, u4RipIfAddr,
                                             i4RipIfAuthType) == SNMP_FAILURE)
            {
                /*Destroy the Md5 row if the type is Md5 */
                if (i4RipIfAuthType == MD5AUTHTYPE)
                {
                    nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                DESTROY);
                }
                return (CLI_FAILURE);
            }

            /* Set the interface configuration status as NOT_IN_SERVICE */

            if (nmhSetRip2IfConfStatus (u4RipIfAddr, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                /*If Row is Md5 DESTROY it */
                if (i4RipIfAuthType == MD5AUTHTYPE)
                {
                    nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                DESTROY);
                }
                CLI_SET_ERR (CLI_RIP_INT_CONF_STAT_NOT_SET);
                return (CLI_FAILURE);
            }

            if (nmhSetRip2IfConfAuthType (u4RipIfAddr, i4RipIfAuthType) ==
                SNMP_FAILURE)
            {

                /*If Row is Md5 DESTROY it */
                if (i4RipIfAuthType == MD5AUTHTYPE)
                {
                    nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                DESTROY);
                }
                CLI_SET_ERR (CLI_RIP_AUTH_TYPE_NOT_SET);
                return (CLI_FAILURE);
            }
            if (pu1RipIfAuthKey != NULL)
            {
                /* Set the authentication key */
                if (nmhTestv2Rip2IfConfAuthKey (&u4ErrorCode, u4RipIfAddr,
                                                &sOctetStrRipAuthKey) ==
                    SNMP_FAILURE)
                {
                    if (i4RipIfAuthType == MD5AUTHTYPE)
                    {
                        nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                    DESTROY);
                    }
                    CLI_SET_ERR (CLI_RIP_WRONG_AUTH_KEY);
                    return (CLI_FAILURE);
                }

                if (nmhSetRip2IfConfAuthKey (u4RipIfAddr, &sOctetStrRipAuthKey)
                    == SNMP_FAILURE)
                {
                    if (i4RipIfAuthType == MD5AUTHTYPE)
                    {
                        nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                    DESTROY);
                    }
                    CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                    return (CLI_FAILURE);
                }
            }
            /* Enable RIP by making the row status active */

            if (CliRipEnableInterface (u4RipIfAddr) == (INT4) OSIX_FAILURE)
            {
                if (i4RipIfAuthType == MD5AUTHTYPE)
                {
                    nmhSetFsRipMd5KeyRowStatus (u4RipIfAddr, i4Md5KeyId,
                                                DESTROY);
                }
                return (CLI_FAILURE);
            }
            i4RetVal = CLI_SUCCESS;

        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetCryptoAuthentication                            */
/*                                                                         */
/*   Description   :To set the authentication type for an interface        */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI Handle                      */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4Authtype      - authentication mode             */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetCryptoAuthentication (tCliHandle CliHandle, INT4 i4CxtId,
                            INT4 i4IfIndex, INT4 i4RipIfAuthType)
{

    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_NO_VALID_SOURCE);
        return (CLI_FAILURE);
    }
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }
    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {

        /* Check for the configured RIP interface in the RIP table. */
        if (nmhGetFsMIStdRip2IfConfStatus (i4CxtId, u4RipIfAddr, &i4RowStatus)
            == SNMP_SUCCESS)
        {

            /*Configure Authentication Mode for the Interface */
            if (nmhTestv2FsMIStdRip2IfConfAuthType (&u4ErrorCode,
                                                    i4CxtId,
                                                    u4RipIfAddr,
                                                    i4RipIfAuthType) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_WRONG_VALUE);
                return (CLI_FAILURE);
            }

            /* Set the interface configuration status as NOT_IN_SERVICE */

            if (nmhSetFsMIStdRip2IfConfStatus
                (i4CxtId, u4RipIfAddr, NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_INT_CONF_STAT_NOT_SET);
                return (CLI_FAILURE);
            }

            if (nmhSetFsMIStdRip2IfConfAuthType
                (i4CxtId, u4RipIfAddr, i4RipIfAuthType) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_TYPE_NOT_SET);
                return (CLI_FAILURE);
            }

            /* Enable RIP by making the row status active */

            if (nmhSetFsMIStdRip2IfConfStatus (i4CxtId, u4RipIfAddr, ACTIVE)
                == (INT4) SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_INT_CONF_STAT_NOT_SET);
                return (CLI_FAILURE);

            }

            i4RetVal = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetCryptoAuthenticationKey                         */
/*                                                                         */
/*   Description   :To set the authentication key for an interface         */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4CryptoAuthKeyID - authentication key ID         */
/*                    5. pu1RipIfAuthKey - pointer to authentication key   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetCryptoAuthenticationKey (tCliHandle CliHandle, INT4 i4CxtId,
                               INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                               UINT1 *pu1RipIfAuthKey)
{
    UINT4               u4RipIfAddr;
    UINT4               u4RipAuthAddr;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;
    INT4                i4RipIfAuthType = 0;
    tSNMP_OCTET_STRING_TYPE sOctetStrRipAuthKey;
    UINT1               au1AuthKey[MAX_AUTH_KEY_LENGTH + 1];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetValue = CLI_FAILURE;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&au1AuthKey[0], 0, MAX_AUTH_KEY_LENGTH + 1);

    sOctetStrRipAuthKey.i4_Length = (INT4) STRLEN (pu1RipIfAuthKey);
    sOctetStrRipAuthKey.pu1_OctetList = &au1AuthKey[0];

    STRNCPY (sOctetStrRipAuthKey.pu1_OctetList, pu1RipIfAuthKey,
             STRLEN (pu1RipIfAuthKey));
    sOctetStrRipAuthKey.pu1_OctetList[STRLEN (pu1RipIfAuthKey)] = '\0';

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP_NO_VALID_SOURCE);
        return (CLI_FAILURE);
    }
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */
    do
    {
        i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr,
                                                    &i4RipIfAuthType);

        if (i4RetVal == SNMP_SUCCESS)
        {
            if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle,
                           "\r%% RIP : Authentication mode is not configured"
                           "for address %s\r\n", pu1IpAddr);
                return CLI_FAILURE;
            }

            u4RipAuthAddr = u4RipIfAddr;

            if (nmhTestv2FsMIRipCryptoAuthKey
                (&u4ErrorCode, i4CxtId,
                 i4IfIndex, u4RipAuthAddr,
                 i4CryptoAuthKeyId, &sOctetStrRipAuthKey) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
                return CLI_FAILURE;
            }

            if (nmhSetFsMIRipCryptoAuthKey (i4CxtId, i4IfIndex,
                                            u4RipAuthAddr, i4CryptoAuthKeyId,
                                            &sOctetStrRipAuthKey) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return CLI_FAILURE;
            }

            /* Set the authentication key on the interface */
            if (nmhTestv2FsMIStdRip2IfConfAuthKey
                (&u4ErrorCode, i4CxtId, u4RipIfAddr,
                 &sOctetStrRipAuthKey) == SNMP_FAILURE)
            {
                nmhSetFsMIRipCryptoKeyStatus (i4CxtId, i4IfIndex,
                                              u4RipIfAddr, i4CryptoAuthKeyId,
                                              RIP_AUTHKEY_STATUS_DELETE);
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
                return (CLI_FAILURE);
            }

            if (nmhSetFsMIStdRip2IfConfAuthKey
                (i4CxtId, u4RipIfAddr, &sOctetStrRipAuthKey) == SNMP_FAILURE)
            {
                nmhSetFsMIRipCryptoKeyStatus (i4CxtId, i4IfIndex,
                                              u4RipIfAddr, i4CryptoAuthKeyId,
                                              RIP_AUTHKEY_STATUS_DELETE);
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return (CLI_FAILURE);
            }
            i4RetValue = CLI_SUCCESS;
        }

    }

    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetValue != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;

}

/***************************************************************************/
/*   Function Name : RipSetCryptoAuthKeyStartAccept                        */
/*                                                                         */
/*   Description   :To set the start accept time for the authentication    */
/*                          key for an interface                           */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4CryptoAuthKeyID - authentication key ID         */
/*                    5. pu1RipIfStartAccept - pointer to start accept     */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetCryptoAuthKeyStartAccept (tCliHandle CliHandle, INT4 i4CxtId,
                                INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                UINT1 *pu1RipIfStartAccept)
{
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RipAuthAddr;
    INT4                i4RetVal = 0;
    INT4                i4RipIfAuthType = 0;
    tSNMP_OCTET_STRING_TYPE sOctetStrRipStartAccept;
    UINT1               au1StartAccept[RIP_DST_TIME_LEN + 1];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetValue = CLI_FAILURE;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&au1StartAccept[0], 0, RIP_DST_TIME_LEN + 1);

    sOctetStrRipStartAccept.i4_Length = (INT4) STRLEN (pu1RipIfStartAccept);
    sOctetStrRipStartAccept.pu1_OctetList = &au1StartAccept[0];

    STRNCPY (sOctetStrRipStartAccept.pu1_OctetList, pu1RipIfStartAccept,
             STRLEN (pu1RipIfStartAccept));
    sOctetStrRipStartAccept.pu1_OctetList[STRLEN (pu1RipIfStartAccept)] = '\0';

    /* Check if date length exceeds format length */
    if (sOctetStrRipStartAccept.i4_Length >= RIP_DST_TIME_LEN)
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in YYYY-MM-DD,hh:mm:ss format \r\n");
        return CLI_FAILURE;
    }

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr,
                                                    &i4RipIfAuthType);

        if (i4RetVal == SNMP_SUCCESS)
        {

            if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle,
                           "\r%% RIP : Authentication mode is not configured"
                           "for address %s\r\n", pu1IpAddr);
                return CLI_FAILURE;
            }

            u4RipAuthAddr = 0;

            if (nmhTestv2FsMIRipCryptoKeyStartAccept
                (&u4ErrorCode, i4CxtId,
                 i4IfIndex, u4RipAuthAddr,
                 i4CryptoAuthKeyId, &sOctetStrRipStartAccept) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
                return CLI_FAILURE;
            }

            if (nmhSetFsMIRipCryptoKeyStartAccept (i4CxtId, i4IfIndex,
                                                   u4RipAuthAddr,
                                                   i4CryptoAuthKeyId,
                                                   &sOctetStrRipStartAccept) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return CLI_FAILURE;
            }
            i4RetValue = CLI_SUCCESS;
        }

    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);

    if (i4RetValue != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;

}

/***************************************************************************/
/*   Function Name : RipSetCryptoAuthKeyStopAccept                         */
/*                                                                         */
/*   Description   :To set the stop  accept time for the authentication    */
/*                          key for an interface                           */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4CryptoAuthKeyID - authentication key ID         */
/*                    5. pu1RipIfStopAccept - pointer to stop accept       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetCryptoAuthKeyStopAccept (tCliHandle CliHandle, INT4 i4CxtId,
                               INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                               UINT1 *pu1RipIfStopAccept)
{
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RipAuthAddr;
    INT4                i4RetVal = 0;
    INT4                i4RipIfAuthType = 0;
    tSNMP_OCTET_STRING_TYPE sOctetStrRipStopAccept;
    UINT1               au1StopAccept[RIP_DST_TIME_LEN + 1];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetValue = CLI_FAILURE;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&au1StopAccept[0], 0, RIP_DST_TIME_LEN + 1);

    sOctetStrRipStopAccept.i4_Length = (INT4) STRLEN (pu1RipIfStopAccept);
    sOctetStrRipStopAccept.pu1_OctetList = &au1StopAccept[0];

    STRNCPY (sOctetStrRipStopAccept.pu1_OctetList, pu1RipIfStopAccept,
             STRLEN (pu1RipIfStopAccept));
    sOctetStrRipStopAccept.pu1_OctetList[STRLEN (pu1RipIfStopAccept)] = '\0';

    /* Check if date length exceeds format length */
    if (sOctetStrRipStopAccept.i4_Length >= RIP_DST_TIME_LEN)
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in YYYY-MM-DD,hh:mm:ss format \r\n");
        return CLI_FAILURE;
    }

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr,
                                                    &i4RipIfAuthType);

        if (i4RetVal == SNMP_SUCCESS)
        {

            if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle,
                           "\r%% RIP : Authentication mode is not configured"
                           "for address %s\r\n", pu1IpAddr);

                return CLI_FAILURE;
            }

            u4RipAuthAddr = 0;

            if (nmhTestv2FsMIRipCryptoKeyStopAccept
                (&u4ErrorCode, i4CxtId,
                 i4IfIndex, u4RipAuthAddr,
                 i4CryptoAuthKeyId, &sOctetStrRipStopAccept) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
                return CLI_FAILURE;
            }

            if (nmhSetFsMIRipCryptoKeyStopAccept (i4CxtId, i4IfIndex,
                                                  u4RipAuthAddr,
                                                  i4CryptoAuthKeyId,
                                                  &sOctetStrRipStopAccept) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return CLI_FAILURE;
            }
            i4RetValue = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetValue != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : RipSetCryptoAuthKeyStartGenerate                      */
/*                                                                         */
/*   Description   :To set the start generate time for the authentication  */
/*                          key for an interface                           */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4CryptoAuthKeyID - authentication key ID         */
/*                    5. pu1RipIfStartGenerate - pointer to start generate */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetCryptoAuthKeyStartGenerate (tCliHandle CliHandle, INT4 i4CxtId,
                                  INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                  UINT1 *pu1RipIfStartGenerate)
{
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RipAuthAddr;
    INT4                i4RetVal = 0;
    INT4                i4RipIfAuthType = 0;
    tSNMP_OCTET_STRING_TYPE sOctetStrRipStartGen;
    UINT1               au1StartGenerate[RIP_DST_TIME_LEN + 1];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetValue = CLI_FAILURE;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&au1StartGenerate[0], 0, RIP_DST_TIME_LEN + 1);

    sOctetStrRipStartGen.i4_Length = (INT4) STRLEN (pu1RipIfStartGenerate);
    sOctetStrRipStartGen.pu1_OctetList = &au1StartGenerate[0];

    STRNCPY (sOctetStrRipStartGen.pu1_OctetList, pu1RipIfStartGenerate,
             STRLEN (pu1RipIfStartGenerate));
    sOctetStrRipStartGen.pu1_OctetList[STRLEN (pu1RipIfStartGenerate)] = '\0';

    /* Check if date length exceeds format length */
    if (sOctetStrRipStartGen.i4_Length >= RIP_DST_TIME_LEN)
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in YYYY-MM-DD,hh:mm:ss format \r\n");
        return CLI_FAILURE;
    }

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr,
                                                    &i4RipIfAuthType);

        if (i4RetVal == SNMP_SUCCESS)
        {

            if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
            {
                CliPrintf (CliHandle,
                           "\r%% RIP : Authentication mode is not configured"
                           "for address %s\r\n", pu1IpAddr);
                return CLI_FAILURE;
            }

            u4RipAuthAddr = 0;

            if (nmhTestv2FsMIRipCryptoKeyStartGenerate
                (&u4ErrorCode, i4CxtId,
                 i4IfIndex, u4RipAuthAddr,
                 i4CryptoAuthKeyId, &sOctetStrRipStartGen) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
                return CLI_FAILURE;
            }

            if (nmhSetFsMIRipCryptoKeyStartGenerate (i4CxtId, i4IfIndex,
                                                     u4RipAuthAddr,
                                                     i4CryptoAuthKeyId,
                                                     &sOctetStrRipStartGen) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return CLI_FAILURE;
            }
            i4RetValue = CLI_SUCCESS;
        }

    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetValue != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : RipSetCryptoAuthKeyStopGenerate                       */
/*                                                                         */
/*   Description   :To set the stop  generate time for the authentication  */
/*                          key for an interface                           */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI context ID                  */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4CryptoAuthKeyID - authentication key ID         */
/*                    5. pu1RipIfStopGenerate - pointer to stop generate   */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetCryptoAuthKeyStopGenerate (tCliHandle CliHandle, INT4 i4CxtId,
                                 INT4 i4IfIndex, INT4 i4CryptoAuthKeyId,
                                 UINT1 *pu1RipIfStopGenerate)
{
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4RipAuthAddr;
    INT4                i4RetVal = 0;
    INT4                i4RipIfAuthType = 0;
    tSNMP_OCTET_STRING_TYPE sOctetStrRipStopGen;
    UINT1               au1StopGenerate[RIP_DST_TIME_LEN + 1];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetValue = CLI_FAILURE;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&au1StopGenerate[0], 0, RIP_DST_TIME_LEN + 1);

    sOctetStrRipStopGen.i4_Length = (INT4) STRLEN (pu1RipIfStopGenerate);
    sOctetStrRipStopGen.pu1_OctetList = &au1StopGenerate[0];

    STRNCPY (sOctetStrRipStopGen.pu1_OctetList, pu1RipIfStopGenerate,
             STRLEN (pu1RipIfStopGenerate));
    sOctetStrRipStopGen.pu1_OctetList[STRLEN (pu1RipIfStopGenerate)] = '\0';

    /* Check if date length exceeds format length */
    if (sOctetStrRipStopGen.i4_Length >= RIP_DST_TIME_LEN)
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in YYYY-MM-DD,hh:mm:ss format \r\n");
        return CLI_FAILURE;
    }

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr,
                                                    &i4RipIfAuthType);

        if (i4RetVal == SNMP_SUCCESS)
        {

            if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle,
                           "\r%% RIP : Authentication mode is not configured"
                           "for address %s\r\n", pu1IpAddr);
                return CLI_FAILURE;

            }

            u4RipAuthAddr = 0;

            if (nmhTestv2FsMIRipCryptoKeyStopGenerate
                (&u4ErrorCode, i4CxtId,
                 i4IfIndex, u4RipAuthAddr,
                 i4CryptoAuthKeyId, &sOctetStrRipStopGen) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_TIME);
                return CLI_FAILURE;
            }

            if (nmhSetFsMIRipCryptoKeyStopGenerate (i4CxtId, i4IfIndex,
                                                    u4RipAuthAddr,
                                                    i4CryptoAuthKeyId,
                                                    &sOctetStrRipStopGen) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return CLI_FAILURE;
            }

            i4RetValue = CLI_SUCCESS;

        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);

    if (i4RetValue != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;

}

/***************************************************************************/
/*   Function Name : RipSetDeleteCryptoAuthKey                             */
/*                                                                         */
/*   Description   :To delete the authentication key for an interface      */
/*                                                                         */
/*   Input(s)      :  1. CliHandle       - CLI Handle                      */
/*                    2. Context ID      - Context ID                      */
/*                    3. i4IfIndex       - interface index                 */
/*                    4. i4CryptoAuthKeyID - authentication key ID         */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetDeleteCryptoAuthKey (tCliHandle CliHandle, INT4 i4CxtId,
                           INT4 i4IfIndex, INT4 i4CryptoAuthKeyId)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4RipIfAddr;
    UINT4               u4RipAuthAddr;
    INT4                i4RetVal = 0;
    INT4                i4RipIfAuthType = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetValue = CLI_FAILURE;
    CHR1               *pu1IpAddr = NULL;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        i4RetVal = nmhGetFsMIStdRip2IfConfAuthType (i4CxtId, u4RipIfAddr,
                                                    &i4RipIfAuthType);

        if (i4RetVal == SNMP_SUCCESS)
        {

            if (i4RipIfAuthType == CLI_RIP_AUTH_NONE)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle,
                           "\r%% RIP : Authentication mode is not configured"
                           "for address %s\r\n", pu1IpAddr);
                return CLI_FAILURE;
            }

            u4RipAuthAddr = u4RipIfAddr;

            if (nmhTestv2FsMIRipCryptoKeyStatus
                (&u4ErrorCode, i4CxtId, i4IfIndex, u4RipAuthAddr,
                 i4CryptoAuthKeyId, RIP_AUTHKEY_STATUS_DELETE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_INVALID_KEY);
                return (CLI_FAILURE);
            }

            if (nmhSetFsMIRipCryptoKeyStatus (i4CxtId, i4IfIndex,
                                              u4RipAuthAddr, i4CryptoAuthKeyId,
                                              RIP_AUTHKEY_STATUS_DELETE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_KEY_NOT_SET);
                return (CLI_FAILURE);
            }
            i4RetValue = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);

    if (i4RetValue != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RIP : Unable to get Authentication type\r\n");
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : RipSetNoAuthentication                                */
/*                                                                         */
/*   Description   : To disable authentication on an interface i.e. to set */
/*                   authentication type to NONE and to delete the key from*/
/*                   MD5 key table if the configured authentication type   */
/*                   is MD5                                                */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetNoAuthentication (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode;
    UINT4               u4RipIfAddr;
    UINT4               u4RipPrevIfAddr;
    UINT4               u4RipCurrIfAddr;
    UINT4               u4CryptoAuthAddr;
    UINT4               u4PrevCryptoAuthAddr;
    INT4                i4Index;
    INT4                i4NxtIndex;
    INT4                i4RowStatus;
    INT4                i4GetIfAuthType;
    INT4                i4PrevMD5Key;
    INT4                i4MD5Key;
    INT4                i4CryptoAuthKeyId;
    INT4                i4NxtCryptoAuthKeyId;
    tSNMP_OCTET_STRING_TYPE sOctetStrMD5AuthKey;
    tSNMP_OCTET_STRING_TYPE sOctetStrGetAuthKey;
    tSNMP_OCTET_STRING_TYPE sOctetStrCryptoAuthKey;
    UINT1               au1MD5AuthKey[MAX_AUTH_KEY_LENGTH];
    UINT1               au1CryptoAuthKey[MAX_AUTH_KEY_LENGTH];
    UINT1               au1GetAuthKey[MAX_AUTH_KEY_LENGTH];
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;
    INT1                i1RetVal = 0;

    MEMSET (&au1MD5AuthKey[0], 0, MAX_AUTH_KEY_LENGTH);
    MEMSET (&au1GetAuthKey[0], 0, MAX_AUTH_KEY_LENGTH);
    MEMSET (&au1CryptoAuthKey[0], 0, MAX_AUTH_KEY_LENGTH);

    sOctetStrMD5AuthKey.pu1_OctetList = &au1MD5AuthKey[0];
    sOctetStrGetAuthKey.pu1_OctetList = &au1GetAuthKey[0];
    sOctetStrCryptoAuthKey.pu1_OctetList = &au1CryptoAuthKey[0];

    /* Inoder to disable authentication on an interface set the authentication
     * type to CLI_RIP_AUTH_NONE
     */

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {
            /* Get the configured authentication type */
            if (nmhGetRip2IfConfAuthType (u4RipIfAddr, &i4GetIfAuthType)
                == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            /* Get the configured authentication key */
            nmhGetRip2IfConfAuthKey (u4RipIfAddr, &sOctetStrGetAuthKey);
            u4RipCurrIfAddr = u4RipIfAddr;

            /* Delete the key from the MD5 table */
            if (i4GetIfAuthType == CLI_RIP_AUTH_MD5)
            {
                i1RetVal =
                    nmhGetFirstIndexFsRipMd5AuthTable (&u4RipCurrIfAddr,
                                                       &i4MD5Key);
                do
                {
                    i1RetVal =
                        nmhGetFsRipMd5AuthKey (u4RipCurrIfAddr, i4MD5Key,
                                               &sOctetStrMD5AuthKey);

                    if (STRCMP (sOctetStrMD5AuthKey.pu1_OctetList,
                                sOctetStrGetAuthKey.pu1_OctetList) == 0)
                    {
                        nmhSetFsRipMd5KeyRowStatus (u4RipCurrIfAddr, i4MD5Key,
                                                    DESTROY);
                        break;
                    }
                    else
                    {
                        i4PrevMD5Key = i4MD5Key;
                        u4RipPrevIfAddr = u4RipCurrIfAddr;
                    }
                }
                while (nmhGetNextIndexFsRipMd5AuthTable
                       (u4RipPrevIfAddr, &u4RipCurrIfAddr, i4PrevMD5Key,
                        &i4MD5Key) == SNMP_SUCCESS);
            }

            /* Delete the key from the Crypto table */
            if (i4GetIfAuthType >= CLI_RIP_AUTH_MD5)
            {

                i1RetVal = nmhGetFirstIndexFsRipCryptoAuthTable (&i4Index,
                                                                 &u4CryptoAuthAddr,
                                                                 &i4CryptoAuthKeyId);
                do
                {
                    nmhGetFsRipCryptoAuthKey (i4Index,
                                              u4CryptoAuthAddr,
                                              i4CryptoAuthKeyId,
                                              &sOctetStrCryptoAuthKey);

                    if (STRCMP (sOctetStrCryptoAuthKey.pu1_OctetList,
                                sOctetStrGetAuthKey.pu1_OctetList) == 0)
                    {
                        nmhSetFsRipCryptoKeyStatus (i4IfIndex, u4CryptoAuthAddr,
                                                    i4CryptoAuthKeyId,
                                                    RIP_AUTHKEY_STATUS_DELETE);
                        break;
                    }
                    else
                    {
                        i4Index = i4NxtIndex;
                        i4CryptoAuthKeyId = i4NxtCryptoAuthKeyId;
                        u4PrevCryptoAuthAddr = u4CryptoAuthAddr;
                    }
                }

                while (nmhGetNextIndexFsRipCryptoAuthTable
                       (i4Index, &i4NxtIndex, u4PrevCryptoAuthAddr,
                        &u4CryptoAuthAddr, i4CryptoAuthKeyId,
                        &i4NxtCryptoAuthKeyId) == SNMP_SUCCESS);

            }

            if (nmhTestv2Rip2IfConfAuthType (&u4ErrorCode, u4RipIfAddr,
                                             CLI_RIP_AUTH_NONE) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            /* Disable authentication by setting authentication type to 
             * CLI_RIP_AUTH_NONE
             */
            if (nmhSetRip2IfConfAuthType (u4RipIfAddr, CLI_RIP_AUTH_NONE)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_AUTH_TYPE_NOT_SET);
                return (CLI_FAILURE);
            }
            i4RetVal = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);

    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    UNUSED_PARAM (i1RetVal);

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetSplitHorizon                                    */
/*                                                                         */
/*   Description   :To set the authentication mode for an interface        */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                    3. i4HorizonStatus - authentication mode             */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetSplitHorizon (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4HorizonStatus)
{
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }
    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        /* Check for the configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            if (nmhTestv2FsRip2IfSplitHorizonStatus (&u4ErrorCode, u4RipIfAddr,
                                                     i4HorizonStatus) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_WRONG_SPLIT_HORIZON_STAT);
                return (CLI_FAILURE);
            }
            if (nmhSetFsRip2IfSplitHorizonStatus (u4RipIfAddr,
                                                  i4HorizonStatus) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_SPLIT_HORIZON_STAT_NOT_SET);
                return (CLI_FAILURE);
            }
            i4RetVal = CLI_SUCCESS;
        }

    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetDefMetric                                       */
/*                                                                         */
/*   Description   :To set the default metric                              */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4Metric  - metric value                          */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetDefMetric (tCliHandle CliHandle, INT4 i4Metric)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsRipRRDRouteDefMetric (&u4ErrorCode, i4Metric)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRipRRDRouteDefMetric (i4Metric) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetTimers                                          */
/*                                                                         */
/*   Description   :To set the update, hold and garbage timers             */
/*                                                                         */
/*   Input(s)      :  1. CliHandle    - CLI context ID                     */
/*                    2. i4IfIndex    - interface index                    */
/*                    3. i4UpdateTmr  - update timer value                 */
/*                    4. i4HoldTmr    - hold timer value                   */
/*                    5. i4GarbageTmr - garbage timer value                */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetTimers (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4UpdateTmr,
              UINT4 u4HoldTmr, UINT4 u4GarbageTmr)
{
    UINT4               u4ErrorCode;
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        /* Check for the configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            /*Test the update, hold and garbage  timer values */
            if ((nmhTestv2FsRip2IfConfUpdateTmr (&u4ErrorCode, u4RipIfAddr,
                                                 (INT4) u4UpdateTmr) ==
                 SNMP_FAILURE)
                ||
                (nmhTestv2FsRip2IfConfRouteAgeTmr
                 (&u4ErrorCode, u4RipIfAddr, (INT4) u4HoldTmr) == SNMP_FAILURE)
                ||
                (nmhTestv2FsRip2IfConfGarbgCollectTmr
                 (&u4ErrorCode, u4RipIfAddr,
                  (INT4) u4GarbageTmr) == SNMP_FAILURE))
            {
                CLI_SET_ERR (CLI_RIP_WRONG_UPDATE_HOLD_GARBAGE_TIMER);
                return (CLI_FAILURE);
            }

            /* Set the interface configuration status as NOT_IN_SERVICE */

            if (nmhSetRip2IfConfStatus (u4RipIfAddr, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_INT_CONF_STAT_NOT_SET);
                return (CLI_FAILURE);
            }

            /*Set the update, hold and garbage  timer values */
            if ((nmhSetFsRip2IfConfUpdateTmr (u4RipIfAddr,
                                              (INT4) u4UpdateTmr) ==
                 SNMP_FAILURE)
                ||
                (nmhSetFsRip2IfConfRouteAgeTmr (u4RipIfAddr, (INT4) u4HoldTmr)
                 == SNMP_FAILURE)
                ||
                (nmhSetFsRip2IfConfGarbgCollectTmr
                 (u4RipIfAddr, (INT4) u4GarbageTmr) == SNMP_FAILURE))
            {
                CLI_SET_ERR (CLI_RIP_UPDATE_HOLD_GARBAGE_TIMER_NOT_SET);
                return (CLI_FAILURE);
            }

            /* Enable RIP by making the row status active */
            if (CliRipEnableInterface (u4RipIfAddr) == (INT4) OSIX_FAILURE)
            {
                CLI_FATAL_ERROR (CLI_FAILURE);
                return (CLI_FAILURE);
            }
            i4RetVal = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipGetRouterCfgPrompt                              */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the router configuration prompt in     */
/*                        pi1DispStr if valid                                */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
RipGetRouterCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4Index = RIP_DEFAULT_CXT;
    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-router" as the prompt
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        CLI_SET_CXT_ID (u4Index);
        STRNCPY (pi1DispStr, "config-router", STRLEN ("config-router"));
        pi1DispStr[STRLEN ("config-router")] = '\0';
        return TRUE;
    }

    u4Len = STRLEN (CLI_RIP_ROUTER_MODE);
    if (STRNCMP (pi1ModeName, CLI_RIP_ROUTER_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Index = (UINT4) CLI_ATOI (pi1ModeName);
    CLI_SET_CXT_ID (u4Index);

    STRNCPY (pi1DispStr, "(config-router)#", STRLEN ("(config-router)#"));
    pi1DispStr[STRLEN ("(config-router)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssRipShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the RIP  debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssRipShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;
    INT4                i4RipSetContext;

    i4RipSetContext = RipSetContext (RIP_DEFAULT_CXT);
    UNUSED_PARAM (i4RipSetContext);
    nmhGetFsRipTrcFlag (&i4DbgLevel);
    if (i4DbgLevel == 0)
    {
        RipReleaseContext ();
        return;
    }
    CliPrintf (CliHandle, "\rRIP :");

    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP init and shutdown debugging is on");
    }
    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP management debugging is on");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP data path debugging is on");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP control path debugging is on");
    }
    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP packet dump debugging is on");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP resources debugging is on");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP error debugging is on");
    }
    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  RIP buffer debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
    RipReleaseContext ();
    return;
}

/***************************************************************************/
/*   Function Name : RipDefaultRouteOriginate                              */
/*                                                                         */
/*   Description   :Set the deault route's metric,to be propagated in      */
/*                  updates with the specified value.                      */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                    3. i4Metric  - metric value to be set                */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipDefaultRouteOriginate (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Metric)
{
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }
    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {

        /* Check for the configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            if (nmhTestv2Rip2IfConfDefaultMetric (&u4ErrorCode, u4RipIfAddr,
                                                  i4Metric) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_WRONG_DEF_MET);
                return (CLI_FAILURE);
            }
            if (nmhSetRip2IfConfDefaultMetric (u4RipIfAddr, i4Metric) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_DEF_MET_NOT_SET);
                return (CLI_FAILURE);
            }

            i4RetVal = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetDefRtInstallStatus                              */
/*                                                                         */
/*   Description   : Set the defalt route install status.                  */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - interface index                       */
/*                    3. i4InstallStatus - the install status.             */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetDefRtInstallStatus (tCliHandle CliHandle, INT4 i4IfIndex,
                          INT4 i4InstallStatus)
{
    INT4                i4RowStatus;
    UINT4               u4RipIfAddr;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);;
    }

    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        /* Check for the configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {

            if (nmhTestv2FsRip2IfConfDefRtInstall (&u4ErrorCode, u4RipIfAddr,
                                                   i4InstallStatus) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_WRONG_DEF_ROUTE_INSTALL);
                return (CLI_FAILURE);
            }

            if (nmhSetFsRip2IfConfDefRtInstall (u4RipIfAddr,
                                                i4InstallStatus) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_DEF_ROUTE_INSTALL_NOT_SET);
                return (CLI_FAILURE);
            }

            i4RetVal = CLI_SUCCESS;
        }

    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSummaryAddress                                     */
/*                                                                         */
/*   Description   : Enable route aggregation                              */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. u4AggrAddr - ip address                           */
/*                    3. u4AggrMask - mask                                 */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSummaryAddress (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4AggAddr,
                   UINT4 u4AggMask)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4RipIfAddr = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4NetMask = 0;
    INT4                i4RetVal = CLI_FAILURE;

    if (RipCliGetIfAddrFromIfIndex (CliHandle, i4IfIndex, &u4RipIfAddr) ==
        CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /*The interface index is same for the primary IP and the Secondary IP enabled 
     *on that interface and hence get the index and loop through all the Primary 
     *and secondary IP to set the interface level properties */

    do
    {
        /* check for the Configured RIP interface in the RIP table. */
        if (nmhGetRip2IfConfStatus (u4RipIfAddr, &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhTestv2FsRipAggStatus (&u4ErrorCode, i4IfIndex, u4AggAddr,
                                         u4AggMask,
                                         CREATE_AND_GO) == SNMP_FAILURE)
            {

                switch (u4ErrorCode)
                {
                    case (SNMP_ERR_RESOURCE_UNAVAILABLE):
                        CLI_SET_ERR (CLI_RIP_IF_AGG_LIMIT_EXEEDED);
                        break;

                    case (SNMP_ERR_WRONG_VALUE):
                        CLI_SET_ERR (CLI_RIP_EXISTING_IF_AGGRN);
                        break;
                    default:
                        break;
                }
                return CLI_FAILURE;
            }
            if (nmhSetFsRipAggStatus (i4IfIndex, u4AggAddr, u4AggMask,
                                      CREATE_AND_GO) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP_EXISTING_IF_AGGRN);
                return CLI_FAILURE;
            }
            i4RetVal = CLI_SUCCESS;
        }
    }
    while (NetIpv4GetNextSecondaryAddress
           ((UINT2) u4IfIndex, u4RipIfAddr, &u4RipIfAddr,
            &u4NetMask) == NETIPV4_SUCCESS);
    if (i4RetVal != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_RIP_INTERFACE_DISABLED_ERR);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/***************************************************************************/
/*   Function Name : RipNoSummaryAddress                                   */
/*                                                                         */
/*   Description   : Disable route aggregation                             */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i4IfIndex - the interface index                   */
/*                    3. u4AggrAddr - ip address                           */
/*                    4. u4AggrMask - mask                                 */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipNoSummaryAddress (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4AggAddr,
                     UINT4 u4AggMask)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRipAggStatus (&u4ErrorCode, i4IfIndex, u4AggAddr,
                                 u4AggMask, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsRipAggStatus (i4IfIndex, u4AggAddr, u4AggMask,
                              DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : RipSetAutoSummary                                     */
/*                                                                         */
/*   Description   : Enable auto-summary aggregation                       */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. i2SummryStatus - value to be set                  */
/*                                        ( DISABLE / ENABLE )             */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
RipSetAutoSummary (tCliHandle CliHandle, INT4 i4SummryStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRip2AutoSummaryStatus (&u4ErrorCode, i4SummryStatus)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsRip2AutoSummaryStatus (i4SummryStatus) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;
}

/***************************************************************************
 *   Function Name : RipCliGetCxtIdFrmVrfName
 *                                                                         
 *   Description   : Gives Context Id value with given Context Name.                       
 *                                                                         
 *   Input(s)      :  1. pu1CxtName - Pointer to the context name.                       
 *                    2. pi4CxtId   - Pointer to the Context Id Value.        
 *                                                                         
 *   Output(s)     : None                                                  
 *                                                                         
 *   Return Values : CLI_SUCCESS / CLI_FAILURE                             
 ***************************************************************************/

INT4
RipCliGetCxtIdFrmVrfName (UINT1 *pu1CxtName, INT4 *pi4CxtId)
{
    if (VcmIsVrfExist (pu1CxtName, (UINT4 *) pi4CxtId) != VCM_TRUE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*   Function Name : RipCliGetContext                                        */
/*                                                                           */
/*   Description   : Gives the Current context Id.                           */
/*                                                                           */
/*   Input(s)      :  1. tCliHandle - CliHandle.                             */
/*                    2. u4Command - command identifier for RIP Coomands     */
/*                                                                           */
/*   Output(s)     : 1. pu2ShowCmdFlag - reurns RIP_TRUE, when u4Command is  */
/*                      a rip show command.                                  */
/*                    2. pi4RipCxtId - Pointer to the Context Id (returns    */
/*                      contextid for router mode & interface mode commands  */
/*                    3. pi4IfIndex  - Pointer to the IfIndex Value.(returns */
/*                      interface index for interface mode commands          */
/*                                                                           */
/*   Return Values : RIP_SUCCESS / RIP_FAILURE                               */
/*****************************************************************************/
INT4
RipCliGetContext (tCliHandle CliHandle, UINT4 u4Command,
                  INT4 *pi4RipCxtId, INT4 *pi4IfIndex, UINT2 *pu2ShowCmdFlag)
{
    INT4                i4IfIndex;
    tRipCxt            *pRipCxt = NULL;
    tNetIpv4IfInfo      pNetIpIfInfo;
    UINT4               u4Port;

    MEMSET ((UINT1 *) &pNetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    UNUSED_PARAM (CliHandle);
    *pi4RipCxtId = (INT4) CLI_GET_CXT_ID ();

    if (*pi4RipCxtId == (INT4) RIP_INVALID_CXT_ID)
    {
        i4IfIndex = CLI_GET_IFINDEX ();

        if (i4IfIndex != (INT4) RIP_INVALID_IFINDEX)
        {
            *pi4IfIndex = i4IfIndex;

            if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port) ==
                NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
                return (RIP_FAILURE);
            }
            if ((pRipCxt = RipGetCxtInfoRecFrmIface ((UINT2) u4Port)) == NULL)
            {
                return RIP_FAILURE;
            }
            *pi4RipCxtId = pRipCxt->i4CxtId;
        }
        else
        {
            /* This is the Global mode
             * Show flag is set for exec mode*/
            if ((u4Command == CLI_RIP_NO_ROUTER) ||
                (u4Command == CLI_RIP_ROUTER) || (u4Command == CLI_RIP_DEBUG))
            {
                *pu2ShowCmdFlag = RIP_FALSE;
            }
            else
            {
                *pu2ShowCmdFlag = RIP_TRUE;
            }

        }
    }

    return RIP_SUCCESS;
}

/***************************************************************************
 *   Function Name : RipGetVrfNameFrmCxtId
 *                                                                         
 *   Description   : Gives Context Name of the given Context Id Value.                       
 *                                                                         
 *   Input(s)      :  1. pu1CxtName - Pointer to the context name.                       
 *                    2. i4CxtId    - Context Id Value.        
 *                                                                         
 *   Output(s)     : None                                                  
 *                                                                         
 *   Return Values : RIP_SUCCESS / RIP_FAILURE                             
 ***************************************************************************/
INT4
RipGetVrfNameFrmCxtId (INT4 i4CxtId, UINT1 *pu1CxtName)
{
    if (VcmGetAliasName ((UINT4) i4CxtId, pu1CxtName) != VCM_FAILURE)
    {
        return RIP_SUCCESS;
    }
    return RIP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowConfigNBRListInCxt                          */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        RIP Neighbor List of the Context                   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4CxtId - RIP Context Id                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
UINT1
RipShowConfigNBRListInCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    INT4                i4RowStatus = 0;
    INT4                i4NextCxtId;
    UINT4               u4Index = 0;
    UINT4               u4PrevIndex = 0;
    CHR1               *pu1IpAddr = NULL;

    while (nmhGetNextIndexFsMIRip2NBRTrustListTable
           (i4CxtId, &i4NextCxtId, u4PrevIndex, &u4Index) == SNMP_SUCCESS)
    {
        if (i4CxtId != i4NextCxtId)
        {
            break;
        }
        nmhGetFsMIRip2TrustNBRRowStatus (i4CxtId, u4Index, &i4RowStatus);
        if (i4RowStatus == ACTIVE)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4Index);
            CliPrintf (CliHandle, " neighbor %s\r\n", pu1IpAddr);
        }

        u4PrevIndex = u4Index;
        i4CxtId = i4NextCxtId;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipShowConfigIfTableCxt                            */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        RIP Interface configuration  of the Context        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u1Flag                                             */
/*                        i4CxtId - RIP Context Id                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : u1Flag                                             */
/*                                                                           */
/*****************************************************************************/
UINT1
RipShowConfigIfTableCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    tSNMP_OCTET_STRING_TYPE NextRouteMapName;
    INT4                i4Val = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4NextCxtId;
    INT4                i4FilterType;
    INT4                i4NextFilterType;
    INT4                i4Value = 0;
    INT4                i4IfIndex;
    INT1                i1OutCome = 0;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4RipIfAddr = 0;
    UINT4               u4NextRipIfAddr = 0;
    UINT4               u4CfaIfIndex;
    UINT1               au1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1              *pu1IfName = NULL;
    CHR1               *pu1IpAddr = NULL;
    UINT4               u4RetValRip2IfConfSrcAddress;
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];

    MEMSET (au1RipCxtName, 0, RIP_MAX_CONTEXT_STR_LEN);
    pu1IfName = &au1InterfaceName[0];

    while (nmhGetNextIndexFsMIRip2IfConfTable
           (i4CxtId, &i4NextCxtId, u4NextRipIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4CxtId != i4NextCxtId)
        {
            break;
        }
        if ((i4IfIndex =
             RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NextCxtId,
                                                 u4RipIfAddr)) != RIP_FAILURE)
        {
            if (RipIfIsUnnumbered ((UINT2) i4IfIndex) == TRUE)
            {
                if (nmhGetRip2IfConfSrcAddress
                    (u4RipIfAddr,
                     &u4RetValRip2IfConfSrcAddress) == SNMP_SUCCESS)
                {
                    if (CfaCliConfGetIfName (u4RipIfAddr, (INT1 *) pu1IfName) !=
                        OSIX_FAILURE)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                   u4RetValRip2IfConfSrcAddress);
                        CliPrintf (CliHandle, "network %s unnum  %s \r\n",
                                   pu1IpAddr, pu1IfName);
                    }
                }
            }
            else
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RipIfAddr);
                CliPrintf (CliHandle, "network %s\r\n", pu1IpAddr);
            }
        }

        nmhGetFsMIRip2IfAdminStat (i4CxtId, u4RipIfAddr, &i4AdminStatus);

        if (i4AdminStatus == RIP_ADMIN_PASSIVE)
        {
            /* Get the 'if' table entry for this IP address. */
            if ((i4Val =
                 RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4CxtId,
                                                     u4RipIfAddr)) !=
                RIP_FAILURE)
            {
                /* Get the alias name */
                MEMSET (&au1InterfaceName[0], 0, CFA_CLI_MAX_IF_NAME_LEN);
                if (NetIpv4GetCfaIfIndexFromPort ((UINT4) i4Val,
                                                  &u4CfaIfIndex) ==
                    NETIPV4_SUCCESS)
                {

                    CfaCliConfGetIfName (u4CfaIfIndex,
                                         (INT1 *) &au1InterfaceName[0]);

                    CliPrintf (CliHandle, "passive-interface %s\r\n",
                               au1InterfaceName);
                }
            }
        }
        u4NextRipIfAddr = u4RipIfAddr;
        i4CxtId = i4NextCxtId;
    };

    /* Filters */
    if (RipSetContext ((UINT4) i4CxtId) != SNMP_FAILURE)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        i1OutCome = nmhGetFirstIndexFsRipDistInOutRouteMapTable (&RouteMapName,
                                                                 &i4FilterType);
        while (i1OutCome != SNMP_FAILURE)
        {
            switch (i4FilterType)
            {
                case FILTERING_TYPE_DISTANCE:
                    if (nmhGetFsRipDistInOutRouteMapValue
                        (&RouteMapName, i4FilterType, &i4Value) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "distance %d route-map %s \r\n",
                                   i4Value, RouteMapName.pu1_OctetList);
                    }
                    break;
                case FILTERING_TYPE_DISTRIB_IN:
                    CliPrintf (CliHandle,
                               "distribute-list route-map %s in\r\n",
                               RouteMapName.pu1_OctetList);
                    break;
                case FILTERING_TYPE_DISTRIB_OUT:
                    CliPrintf (CliHandle,
                               "distribute-list route-map %s out\r\n",
                               RouteMapName.pu1_OctetList);
                    break;
                default:
                    break;
            }

            MEMSET (&NextRouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            MEMSET (au1NextRMapName, 0, RMAP_MAX_NAME_LEN + 4);
            NextRouteMapName.pu1_OctetList = au1NextRMapName;

            i1OutCome =
                nmhGetNextIndexFsRipDistInOutRouteMapTable (&RouteMapName,
                                                            &NextRouteMapName,
                                                            i4FilterType,
                                                            &i4NextFilterType);

            i4FilterType = i4NextFilterType;

            MEMCPY (RouteMapName.pu1_OctetList, NextRouteMapName.pu1_OctetList,
                    RMAP_MAX_NAME_LEN + 4);
            RouteMapName.i4_Length = NextRouteMapName.i4_Length;
        }
    }

    CliPrintf (CliHandle, "!\n");

    nmhGetFsMIRip2LastAuthKeyLifetimeStatus (i4CxtId, &i4Val);
    if (i4Val != RIP_TRUE)
    {
        if ((i4CxtId != RIP_DEFAULT_CXT) &&
            (RipGetVrfNameFrmCxtId (i4CxtId, au1RipCxtName) == RIP_SUCCESS))
        {
            CliPrintf (CliHandle,
                       "rip vrf %s authentication last-key infinite lifetime false\r\n",
                       au1RipCxtName);
        }
        else
        {
            CliPrintf (CliHandle,
                       "rip authentication last-key infinite lifetime false\r\n");
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipPrintCxtName                                    */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current context name      */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4CxtId - RIP Context Id                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RipPrintCxtName (tCliHandle CliHandle, INT4 i4CxtId)
{
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    if ((i4CxtId != RIP_DEFAULT_CXT) &&
        (VcmGetAliasName ((UINT4) i4CxtId, au1RipCxtName) == VCM_SUCCESS))
    {
        CliPrintf (CliHandle, " vrf %s\r\n", au1RipCxtName);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RipCliGetIfAddrFromIfIndex                         */
/*                                                                           */
/*     DESCRIPTION      : This function provides the address to be used for  */
/*                        rip interface table                                */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4IfIndex - Cfaindex                               */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RipCliGetIfAddrFromIfIndex (tCliHandle CliHandle, INT4 i4IfIndex,
                            UINT4 *pu4RipIfAddr)
{
    UINT4               u4Port;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
        return (CLI_FAILURE);
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid interface\r\n");
        return (CLI_FAILURE);
    }
    if (NetIpIfInfo.u4Addr == 0)
    {
        *pu4RipIfAddr = (UINT4) i4IfIndex;
    }
    else
    {
        *pu4RipIfAddr = NetIpIfInfo.u4Addr;
    }
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : RipSetRouteDistance                                   */
/*                                                                         */
/*   Description   :                                                       */
/*                                                                         */
/*   Input(s)      : 1. CliHandle                                          */
/*                   2. i4Distance  - Distance value                       */
/*                   3. pu1RMapName - route map name                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetRouteDistance (tCliHandle CliHandle, INT4 i4Distance, UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_RIP_INV_ASSOC_ROUTE_MAP;
    INT4                i4RowStatus;
    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = (INT4) STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (nmhGetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                   FILTERING_TYPE_DISTANCE,
                                                   &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            if (nmhTestv2FsRipDistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhSetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                           FILTERING_TYPE_DISTANCE,
                                                           CREATE_AND_WAIT) ==
                    SNMP_SUCCESS)
                {
                    u4ErrorCode = SNMP_ERR_NO_ERROR;
                }
            }
            else
            {
                u4CliError = CLI_RIP_RMAP_ASSOC_FAILED;
            }
        }
        else
        {
            u4ErrorCode = SNMP_ERR_NO_ERROR;
        }

        if (SNMP_ERR_NO_ERROR == u4ErrorCode)
        {
            if (nmhTestv2FsRipDistInOutRouteMapValue
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 i4Distance) == SNMP_SUCCESS)
            {
                if (nmhSetFsRipDistInOutRouteMapValue (&RouteMapName,
                                                       FILTERING_TYPE_DISTANCE,
                                                       i4Distance) ==
                    SNMP_SUCCESS)
                {
                    if (nmhTestv2FsRipDistInOutRouteMapRowStatus
                        (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         ACTIVE) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsRipDistInOutRouteMapRowStatus
                            (&RouteMapName, FILTERING_TYPE_DISTANCE,
                             ACTIVE) == SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FsRipPreferenceValue (&u4ErrorCode, i4Distance) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsRipPreferenceValue (i4Distance) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4ErrorCode = CLI_RIP_WRONG_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : RipSetNoRouteDistance                                 */
/*                                                                         */
/*   Description   :                                                       */
/*                                                                         */
/*   Input(s)      : 1. CliHandle                                          */
/*                   2. pu1RMapName - route map name                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
RipSetNoRouteDistance (tCliHandle CliHandle, UINT1 *pu1RMapName)
{
    INT4                i4RowStatus;
    UINT4               u4CliError = (UINT4) CLI_RIP_INV_DISASSOC_ROUTE_MAP;
    UINT4               u4ErrorCode = (UINT4) SNMP_ERR_NO_CREATION;
    INT4                i4Distance = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = (INT4) STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);
        if (nmhGetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                   FILTERING_TYPE_DISTANCE,
                                                   &i4RowStatus) ==
            SNMP_SUCCESS)
        {
            if (nmhTestv2FsRipDistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 DESTROY) == SNMP_SUCCESS)
            {
                if (nmhSetFsRipDistInOutRouteMapRowStatus (&RouteMapName,
                                                           FILTERING_TYPE_DISTANCE,
                                                           DESTROY) ==
                    SNMP_SUCCESS)
                {
                    i4RetVal = nmhGetFsRipPreferenceValue (&i4Distance);
                    UNUSED_PARAM (i4RetVal);
                    if (nmhTestv2FsRipPreferenceValue (&u4ErrorCode, i4Distance)
                        == SNMP_SUCCESS)
                    {
                        if (nmhSetFsRipPreferenceValue (i4Distance) ==
                            SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
        else
        {
            u4CliError = CLI_RIP_INV_ASSOC_ROUTE_MAP;
        }
    }
    else
    {                            /* 0 distance will set default value */
        if (nmhTestv2FsRipPreferenceValue (&u4ErrorCode, 0) == SNMP_SUCCESS)
        {
            if (nmhSetFsRipPreferenceValue (0) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4ErrorCode = CLI_RIP_WRONG_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/*****************************************************************************/
/* Function Name      : RipGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RIP_SUCESS/RIP_FAILURE                               */
/*****************************************************************************/
INT4
RipGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (RipCliGetShowCmdOutputToFile ((UINT1 *) RIP_AUDIT_FILE_ACTIVE) !=
            RIP_SUCCESS)
        {
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_GLOBAL_FLAG,
                     RIP_INVALID_CXT_ID,
                     ALL_FAILURE_TRC, RIP_NAME, "GetShRunFile Failed\n");
            return RIP_FAILURE;
        }
        if (RipCliCalcSwAudCheckSum
            ((UINT1 *) RIP_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != RIP_SUCCESS)
        {
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_GLOBAL_FLAG,
                     RIP_INVALID_CXT_ID,
                     ALL_FAILURE_TRC, RIP_NAME, "CalcSwAudChkSum Failed\n");
            return RIP_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (RipCliGetShowCmdOutputToFile ((UINT1 *) RIP_AUDIT_FILE_STDBY) !=
            RIP_SUCCESS)
        {
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_GLOBAL_FLAG,
                     RIP_INVALID_CXT_ID,
                     ALL_FAILURE_TRC, RIP_NAME, "GetShRunFile Failed\n");
            return RIP_FAILURE;
        }
        if (RipCliCalcSwAudCheckSum
            ((UINT1 *) RIP_AUDIT_FILE_STDBY, pu2SwAudChkSum) != RIP_SUCCESS)
        {
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_GLOBAL_FLAG,
                     RIP_INVALID_CXT_ID,
                     ALL_FAILURE_TRC, RIP_NAME, "CalcSwAudChkSum Failed\n");
            return RIP_FAILURE;
        }
    }
    else
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC,
                 RIP_NAME, "Node is neither Active nor Standby\n");
        return RIP_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return RIP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RipCliGetShowCmdOutputToFile                        */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RIP_SUCCESS/RIP_FAILURE                            */
/*****************************************************************************/
INT4
RipCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    INT4                i4Cmd = 0;
    CHR1               *au1RipShowCmdList[RIP_DYN_MAX_CMDS]
        = { "show ip rip database > ",
        "show ip rip peerinfo >> "
    };
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return RIP_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < RIP_DYN_MAX_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile ((UINT1 *) pu1FileName,
                                       (UINT1 *) au1RipShowCmdList[i4Cmd]) ==
            CLI_FAILURE)
        {
            return RIP_FAILURE;
        }
    }
    return RIP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RipCliCalcSwAudCheckSum                             */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RIP_SUCCESS/RIP_FAILURE                            */
/*****************************************************************************/
INT4
RipCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[RIP_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, RIP_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return RIP_FAILURE;
    }
    MEMSET (ai1Buf, 0, RIP_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (RipCliReadLineFromFile (i4Fd, ai1Buf, RIP_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != RIP_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', RIP_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RipCliReadLineFromFile                              */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RIP_SUCCESS/RIP_FAILURE                            */
/*****************************************************************************/
INT1
RipCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (RIP_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (RIP_CLI_EOF);
}

/**************************************************************************/
/*   Function Name : RipShowSimpleAuthenticationInCxt                     */
/*                                                                        */
/*   Description   :To display IP RIP protocol simple authentication info */
/*                                                                        */
/*   Input(s)      : CliHandle - CLI Context Id.                          */
/*                   i4CxtId - RIP Context Id                             */
/*                                                                        */
/*   Return Values : NONE                                                 */
/* ************************************************************************/

VOID
RipShowSimpleAuthenticationInCxt (tCliHandle CliHandle, INT4 i4CxtId)
{
    INT4                i4Index = -1;
    INT4                i4PrevIfIndex = -1;

    INT4                i4NextCxtId = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4PrevRipIfAddr = 0;
    UINT4               u4RipIfAddr = 0;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1RipCxtName[RIP_MAX_CONTEXT_STR_LEN];
    UINT4               u4CurrIndex = 0;
    UINT4               u4IfAddr = 0;
    INT4                i4AuthType = 0;
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4InUseKeyId = 0;
    INT4                i4LastKeyStatus = 0;
    INT4                i4NetIpv4GetIfInfo = 0;

    if ((nmhGetFirstIndexFsMIRip2IfConfTable
         (&i4CxtId, &u4CurrIndex)) == SNMP_FAILURE)
    {
        return;
    }

    while (nmhGetNextIndexFsMIStdRip2IfConfTable
           (i4CxtId, &i4NextCxtId, u4PrevRipIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4CxtId != i4NextCxtId)
        {
            break;
        }

        if ((i4Index = RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) i4NextCxtId,
                                                           u4RipIfAddr)) !=
            RIP_FAILURE)
        {
            if (i4PrevIfIndex != i4Index)
            {

                MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
                MEMSET (au1RipCxtName, 0, RIP_MAX_CONTEXT_STR_LEN);
                MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

                RipGetVrfNameFrmCxtId (i4NextCxtId, au1RipCxtName);

                if ((NetIpv4GetIfInfo ((UINT4) i4Index, &NetIpIfInfo) ==
                     NETIPV4_FAILURE) ||
                    (NetIpv4GetCfaIfIndexFromPort
                     ((UINT4) i4Index, &u4CfaIfIndex) == NETIPV4_FAILURE))

                {
                    return;
                }

                CfaGetIfAlias (u4CfaIfIndex, au1IfName);

                nmhGetFsMIStdRip2IfConfAuthType (i4NextCxtId, u4RipIfAddr,
                                                 &i4AuthType);

                /* Display the authentication type */

                switch (i4AuthType)
                {
                    case RIPIF_NO_AUTHENTICATION:
                        CliPrintf (CliHandle, "\n\rVrf %s\n", au1RipCxtName);
                        CliPrintf (CliHandle,
                                   "Interface Name                   %s\r\n",
                                   au1IfName);
                        CliPrintf (CliHandle,
                                   "Authentication type is           none\r\n");
                        break;

                    case RIPIF_SIMPLE_PASSWORD:
                        CliPrintf (CliHandle, "\n\rVrf %s\n", au1RipCxtName);
                        CliPrintf (CliHandle,
                                   "Interface Name                   %s\r\n",
                                   au1IfName);
                        CliPrintf (CliHandle,
                                   "Authentication type is           simple text\r\n");
                        break;

                    default:
                        break;

                }

                if (i4AuthType == RIPIF_SIMPLE_PASSWORD)
                {
                    nmhGetFsMIRip2IfConfInUseKey (i4NextCxtId, u4RipIfAddr,
                                                  &i4InUseKeyId);
                    CliPrintf (CliHandle,
                               "Authentication KeyId in use:     %d\r\n",
                               i4InUseKeyId);
                    nmhGetFsMIRip2IfConfAuthLastKeyStatus (i4NextCxtId,
                                                           u4RipIfAddr,
                                                           &i4LastKeyStatus);
                    if (i4LastKeyStatus == 1)
                    {
                        CliPrintf (CliHandle,
                                   "Authentication Last key status:  true\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "Authentication Last key status:  false\r\n");
                    }
                }
            }
        }
        i4PrevIfIndex = i4Index;
        u4PrevRipIfAddr = u4RipIfAddr;
        i4CxtId = i4NextCxtId;
    }
    UNUSED_PARAM (u4IfAddr);
    UNUSED_PARAM (i4NetIpv4GetIfInfo);
    return;
}

#endif
