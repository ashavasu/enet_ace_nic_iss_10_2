#include "lr.h"
#include "fssnmp.h"
#include "stdripwr.h"
#include "stdrilow.h"
#include "stdripdb.h"
#include "rip.h"

VOID
RegisterSTDRIP ()
{
    SNMPRegisterMibWithContextIdAndLock (&stdripOID, &stdripEntry,
                                         RipLock, RipUnLock, RipSetContext,
                                         RipResetContext, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdripOID, (const UINT1 *) "stdrip");
}

INT4
Rip2GlobalRouteChangesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRip2GlobalRouteChanges (&pMultiData->u4_ULongValue));
}

INT4
Rip2GlobalQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetRip2GlobalQueries (&pMultiData->u4_ULongValue));
}

INT4
Rip2IfStatAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfStatTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
Rip2IfStatRcvBadPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfStatTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfStatRcvBadPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &pMultiData->u4_ULongValue));
}

INT4
Rip2IfStatRcvBadRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfStatTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfStatRcvBadRoutes (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
Rip2IfStatSentUpdatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfStatTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfStatSentUpdates (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Rip2IfStatStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfStatStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));
}

INT4
Rip2IfStatTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Rip2IfStatTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Rip2IfStatStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfStatStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));
}

INT4
Rip2IfStatStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfStatTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfStatStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
Rip2IfConfDomainTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfDomain (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->pOctetStrValue));
}

INT4
Rip2IfConfDomainSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfDomain (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
Rip2IfConfDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfDomain (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
Rip2IfConfAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfAuthType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfAuthType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfAuthType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfAuthKey (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));
}

INT4
Rip2IfConfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfAuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));
}

INT4
Rip2IfConfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfAuthKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));
}

INT4
Rip2IfConfSendTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfSend (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfSendSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfSend (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfSendGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfSend (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfReceiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfReceive (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfReceiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfReceive (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfReceiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfReceive (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfDefaultMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfDefaultMetric (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfDefaultMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfDefaultMetric (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfDefaultMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfDefaultMetric (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &pMultiData->i4_SLongValue));
}

INT4
Rip2IfConfSrcAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{

    return (nmhTestv2Rip2IfConfSrcAddress (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));
}

INT4
Rip2IfConfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Rip2IfConfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Rip2IfConfSrcAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetRip2IfConfSrcAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));
}

INT4
Rip2IfConfSrcAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2IfConfTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2IfConfSrcAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &pMultiData->u4_ULongValue));
}

INT4
Rip2PeerAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
Rip2PeerDomainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;
    return SNMP_SUCCESS;
}

INT4
Rip2PeerLastUpdateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2PeerLastUpdate (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
Rip2PeerVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2PeerVersion (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   &pMultiData->i4_SLongValue));
}

INT4
Rip2PeerRcvBadPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2PeerRcvBadPackets (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
Rip2PeerRcvBadRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceRip2PeerTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetRip2PeerRcvBadRoutes (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        &pMultiData->u4_ULongValue));
}

INT4
GetNextIndexRip2PeerTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4rip2PeerAddress;
    tSNMP_OCTET_STRING_TYPE *p1rip2PeerDomain;
    p1rip2PeerDomain = pNextMultiIndex->pIndex[1].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRip2PeerTable (&u4rip2PeerAddress,
                                           p1rip2PeerDomain) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRip2PeerTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4rip2PeerAddress,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             p1rip2PeerDomain) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4rip2PeerAddress;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexRip2IfConfTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4rip2IfConfAddress;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRip2IfConfTable (&u4rip2IfConfAddress)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRip2IfConfTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4rip2IfConfAddress) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4rip2IfConfAddress;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexRip2IfStatTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4rip2IfStatAddress;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexRip2IfStatTable (&u4rip2IfStatAddress)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexRip2IfStatTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4rip2IfStatAddress) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4rip2IfStatAddress;
    return SNMP_SUCCESS;
}
