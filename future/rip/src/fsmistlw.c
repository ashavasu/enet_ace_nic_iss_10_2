/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmistlw.c,v 1.9 2013/08/05 12:30:55 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "fssnmp.h"
# include  "ripinc.h"

/* LOW LEVEL Routines for Table : FsMIStdRip2GlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdRip2GlobalTable (INT4 i4FsMIRipContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);
    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT)
        || (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdRip2GlobalTable (INT4 *pi4FsMIRipContextId)
{
    if (RipGetFirstCxtId (pi4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdRip2GlobalTable (INT4 i4FsMIRipContextId,
                                       INT4 *pi4NextFsMIRipContextId)
{
    if (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) !=
        RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2GlobalRouteChanges
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIStdRip2GlobalRouteChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2GlobalRouteChanges (INT4 i4FsMIRipContextId,
                                     UINT4
                                     *pu4RetValFsMIStdRip2GlobalRouteChanges)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2GlobalRouteChanges (pu4RetValFsMIStdRip2GlobalRouteChanges);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2GlobalQueries
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIStdRip2GlobalQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2GlobalQueries (INT4 i4FsMIRipContextId,
                                UINT4 *pu4RetValFsMIStdRip2GlobalQueries)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetRip2GlobalQueries (pu4RetValFsMIStdRip2GlobalQueries);

    RipResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdRip2IfStatTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdRip2IfStatTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdRip2IfStatTable (INT4 i4FsMIRipContextId,
                                                UINT4
                                                u4FsMIStdRip2IfStatAddress)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceRip2IfStatTable (u4FsMIStdRip2IfStatAddress);

    RipResetContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdRip2IfStatTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdRip2IfStatTable (INT4 *pi4FsMIRipContextId,
                                        UINT4 *pu4FsMIStdRip2IfStatAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexRip2IfStatTable (pu4FsMIStdRip2IfStatAddress);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdRip2IfStatTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIStdRip2IfStatAddress
                nextFsMIStdRip2IfStatAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdRip2IfStatTable (INT4 i4FsMIRipContextId,
                                       INT4 *pi4NextFsMIRipContextId,
                                       UINT4 u4FsMIStdRip2IfStatAddress,
                                       UINT4 *pu4NextFsMIStdRip2IfStatAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexRip2IfStatTable (u4FsMIStdRip2IfStatAddress,
                                                   pu4NextFsMIStdRip2IfStatAddress);

        RipResetContext ();
    }
    *pi4NextFsMIRipContextId = i4FsMIRipContextId;

    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }
    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexRip2IfStatTable (pu4NextFsMIStdRip2IfStatAddress);

        RipResetContext ();
        i4FsMIRipContextId = *pi4NextFsMIRipContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfStatRcvBadPackets
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                retValFsMIStdRip2IfStatRcvBadPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfStatRcvBadPackets (INT4 i4FsMIRipContextId,
                                      UINT4 u4FsMIStdRip2IfStatAddress,
                                      UINT4
                                      *pu4RetValFsMIStdRip2IfStatRcvBadPackets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfStatRcvBadPackets (u4FsMIStdRip2IfStatAddress,
                                       pu4RetValFsMIStdRip2IfStatRcvBadPackets);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfStatRcvBadRoutes
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                retValFsMIStdRip2IfStatRcvBadRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfStatRcvBadRoutes (INT4 i4FsMIRipContextId,
                                     UINT4 u4FsMIStdRip2IfStatAddress,
                                     UINT4
                                     *pu4RetValFsMIStdRip2IfStatRcvBadRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfStatRcvBadRoutes (u4FsMIStdRip2IfStatAddress,
                                      pu4RetValFsMIStdRip2IfStatRcvBadRoutes);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfStatSentUpdates
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                retValFsMIStdRip2IfStatSentUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfStatSentUpdates (INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIStdRip2IfStatAddress,
                                    UINT4
                                    *pu4RetValFsMIStdRip2IfStatSentUpdates)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfStatSentUpdates (u4FsMIStdRip2IfStatAddress,
                                     pu4RetValFsMIStdRip2IfStatSentUpdates);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfStatPeriodicUpdates
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                retValFsMIStdRip2IfStatPeriodicUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfStatPeriodicUpdates (INT4 i4FsMIRipContextId,
                                        UINT4 u4FsMIStdRip2IfStatAddress,
                                        UINT4
                                        *pu4RetValFsMIStdRip2IfStatPeriodicUpdates)
{
    tRipCxt            *pRipCxtEntry = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    if ((pRipCxtEntry = RipGetCxtInfoRec (i4FsMIRipContextId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pRipIfRec = RipGetIfRecFromAddr (u4FsMIStdRip2IfStatAddress, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMIStdRip2IfStatPeriodicUpdates =
        (pRipIfRec->RipIfaceStats.u4SentPeriodicUpdates);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfStatStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                retValFsMIStdRip2IfStatStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfStatStatus (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2IfStatAddress,
                               INT4 *pi4RetValFsMIStdRip2IfStatStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfStatStatus (u4FsMIStdRip2IfStatAddress,
                                pi4RetValFsMIStdRip2IfStatStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfStatRcvBadAuthPackets
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                retValFsMIStdRip2IfStatRcvBadAuthPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfStatRcvBadAuthPackets (INT4 i4FsMIRipContextId,
                                          UINT4 u4FsMIStdRip2IfStatAddress,
                                          UINT4
                                          *pu4RetValFsMIStdRip2IfStatRcvBadAuthPackets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfStatRcvBadAuthPackets
        (u4FsMIStdRip2IfStatAddress,
         pu4RetValFsMIStdRip2IfStatRcvBadAuthPackets);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfStatStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                setValFsMIStdRip2IfStatStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfStatStatus (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2IfStatAddress,
                               INT4 i4SetValFsMIStdRip2IfStatStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfStatStatus (u4FsMIStdRip2IfStatAddress,
                                i4SetValFsMIStdRip2IfStatStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfStatStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress

                The Object 
                testValFsMIStdRip2IfStatStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfStatStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIStdRip2IfStatAddress,
                                  INT4 i4TestValFsMIStdRip2IfStatStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfStatStatus (pu4ErrorCode, u4FsMIStdRip2IfStatAddress,
                                   i4TestValFsMIStdRip2IfStatStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdRip2IfStatTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfStatAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdRip2IfStatTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdRip2IfConfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdRip2IfConfTable (INT4 i4FsMIRipContextId,
                                                UINT4
                                                u4FsMIStdRip2IfConfAddress)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceRip2IfConfTable (u4FsMIStdRip2IfConfAddress);

    RipResetContext ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdRip2IfConfTable (INT4 *pi4FsMIRipContextId,
                                        UINT4 *pu4FsMIStdRip2IfConfAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexRip2IfConfTable (pu4FsMIStdRip2IfConfAddress);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIStdRip2IfConfAddress
                nextFsMIStdRip2IfConfAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdRip2IfConfTable (INT4 i4FsMIRipContextId,
                                       INT4 *pi4NextFsMIRipContextId,
                                       UINT4 u4FsMIStdRip2IfConfAddress,
                                       UINT4 *pu4NextFsMIStdRip2IfConfAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexRip2IfConfTable (u4FsMIStdRip2IfConfAddress,
                                                   pu4NextFsMIStdRip2IfConfAddress);

        RipResetContext ();
    }
    *pi4NextFsMIRipContextId = i4FsMIRipContextId;

    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }

    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexRip2IfConfTable (pu4NextFsMIStdRip2IfConfAddress);

        RipResetContext ();
        i4FsMIRipContextId = *pi4NextFsMIRipContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfDomain
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfDomain (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2IfConfAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIStdRip2IfConfDomain)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfDomain (u4FsMIStdRip2IfConfAddress,
                                pRetValFsMIStdRip2IfConfDomain);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfAuthType
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfAuthType (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIStdRip2IfConfAddress,
                                 INT4 *pi4RetValFsMIStdRip2IfConfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfAuthType (u4FsMIStdRip2IfConfAddress,
                                  pi4RetValFsMIStdRip2IfConfAuthType);

    /* Since stdrip does not support crypto authentication
     * fsrip mib object is invoked to retrieve the crypto
     * authentication type. */

    if (*pi4RetValFsMIStdRip2IfConfAuthType == RIPIF_MD5_AUTHENTICATION)
    {
        i1Return =
            nmhGetFsRip2IfConfAuthType (u4FsMIStdRip2IfConfAddress,
                                        pi4RetValFsMIStdRip2IfConfAuthType);

        /* In fsmirip mib the IfConfAuthType object supports all type of
         * authentication including simple text, MD5 & SHA.
         * But in fsrip.mib the IfConfAuthType object supports only
         * crypto authentication so the return value of auth type is
         * incremented by 2 & displayed */
        *pi4RetValFsMIStdRip2IfConfAuthType += 2;
    }

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfAuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfAuthKey (INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIStdRip2IfConfAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMIStdRip2IfConfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfAuthKey (u4FsMIStdRip2IfConfAddress,
                                 pRetValFsMIStdRip2IfConfAuthKey);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfSend
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfSend
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfSend (INT4 i4FsMIRipContextId,
                             UINT4 u4FsMIStdRip2IfConfAddress,
                             INT4 *pi4RetValFsMIStdRip2IfConfSend)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfSend (u4FsMIStdRip2IfConfAddress,
                              pi4RetValFsMIStdRip2IfConfSend);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfReceive
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfReceive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfReceive (INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIStdRip2IfConfAddress,
                                INT4 *pi4RetValFsMIStdRip2IfConfReceive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfReceive (u4FsMIStdRip2IfConfAddress,
                                 pi4RetValFsMIStdRip2IfConfReceive);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfDefaultMetric
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfDefaultMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfDefaultMetric (INT4 i4FsMIRipContextId,
                                      UINT4 u4FsMIStdRip2IfConfAddress,
                                      INT4
                                      *pi4RetValFsMIStdRip2IfConfDefaultMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfDefaultMetric (u4FsMIStdRip2IfConfAddress,
                                       pi4RetValFsMIStdRip2IfConfDefaultMetric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfStatus (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2IfConfAddress,
                               INT4 *pi4RetValFsMIStdRip2IfConfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfStatus (u4FsMIStdRip2IfConfAddress,
                                pi4RetValFsMIStdRip2IfConfStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2IfConfSrcAddress
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                retValFsMIStdRip2IfConfSrcAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2IfConfSrcAddress (INT4 i4FsMIRipContextId,
                                   UINT4 u4FsMIStdRip2IfConfAddress,
                                   UINT4 *pu4RetValFsMIStdRip2IfConfSrcAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2IfConfSrcAddress (u4FsMIStdRip2IfConfAddress,
                                    pu4RetValFsMIStdRip2IfConfSrcAddress);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfDomain
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfDomain (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2IfConfAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIStdRip2IfConfDomain)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfDomain (u4FsMIStdRip2IfConfAddress,
                                pSetValFsMIStdRip2IfConfDomain);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfAuthType
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfAuthType (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIStdRip2IfConfAddress,
                                 INT4 i4SetValFsMIStdRip2IfConfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    if (i4SetValFsMIStdRip2IfConfAuthType <= RIPIF_MD5_AUTHENTICATION)
    {
        i1Return =
            nmhSetRip2IfConfAuthType (u4FsMIStdRip2IfConfAddress,
                                      i4SetValFsMIStdRip2IfConfAuthType);
    }
    else
    {
        i4SetValFsMIStdRip2IfConfAuthType =
            i4SetValFsMIStdRip2IfConfAuthType - 2;
        i1Return =
            nmhSetFsRip2IfConfAuthType (u4FsMIStdRip2IfConfAddress,
                                        i4SetValFsMIStdRip2IfConfAuthType);
    }
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfAuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfAuthKey (INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIStdRip2IfConfAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMIStdRip2IfConfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfAuthKey (u4FsMIStdRip2IfConfAddress,
                                 pSetValFsMIStdRip2IfConfAuthKey);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfSend
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfSend
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfSend (INT4 i4FsMIRipContextId,
                             UINT4 u4FsMIStdRip2IfConfAddress,
                             INT4 i4SetValFsMIStdRip2IfConfSend)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfSend (u4FsMIStdRip2IfConfAddress,
                              i4SetValFsMIStdRip2IfConfSend);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfReceive
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfReceive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfReceive (INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIStdRip2IfConfAddress,
                                INT4 i4SetValFsMIStdRip2IfConfReceive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfReceive (u4FsMIStdRip2IfConfAddress,
                                 i4SetValFsMIStdRip2IfConfReceive);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfDefaultMetric
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfDefaultMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfDefaultMetric (INT4 i4FsMIRipContextId,
                                      UINT4 u4FsMIStdRip2IfConfAddress,
                                      INT4
                                      i4SetValFsMIStdRip2IfConfDefaultMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfDefaultMetric (u4FsMIStdRip2IfConfAddress,
                                       i4SetValFsMIStdRip2IfConfDefaultMetric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfStatus (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2IfConfAddress,
                               INT4 i4SetValFsMIStdRip2IfConfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfStatus (u4FsMIStdRip2IfConfAddress,
                                i4SetValFsMIStdRip2IfConfStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdRip2IfConfSrcAddress
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                setValFsMIStdRip2IfConfSrcAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdRip2IfConfSrcAddress (INT4 i4FsMIRipContextId,
                                   UINT4 u4FsMIStdRip2IfConfAddress,
                                   UINT4 u4SetValFsMIStdRip2IfConfSrcAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetRip2IfConfSrcAddress (u4FsMIStdRip2IfConfAddress,
                                    u4SetValFsMIStdRip2IfConfSrcAddress);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfDomain
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfDomain (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIStdRip2IfConfAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIStdRip2IfConfDomain)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfDomain (pu4ErrorCode, u4FsMIStdRip2IfConfAddress,
                                   pTestValFsMIStdRip2IfConfDomain);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfAuthType
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfAuthType (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIStdRip2IfConfAddress,
                                    INT4 i4TestValFsMIStdRip2IfConfAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    if (i4TestValFsMIStdRip2IfConfAuthType < RIPIF_MD5_AUTHENTICATION)
    {
        i1Return =
            nmhTestv2Rip2IfConfAuthType (pu4ErrorCode,
                                         u4FsMIStdRip2IfConfAddress,
                                         i4TestValFsMIStdRip2IfConfAuthType);
    }
    else
    {
        i4TestValFsMIStdRip2IfConfAuthType =
            i4TestValFsMIStdRip2IfConfAuthType - 2;
        i1Return =
            nmhTestv2FsRip2IfConfAuthType (pu4ErrorCode,
                                           u4FsMIStdRip2IfConfAddress,
                                           i4TestValFsMIStdRip2IfConfAuthType);
    }

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfAuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfAuthKey (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                   UINT4 u4FsMIStdRip2IfConfAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMIStdRip2IfConfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfAuthKey (pu4ErrorCode, u4FsMIStdRip2IfConfAddress,
                                    pTestValFsMIStdRip2IfConfAuthKey);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfSend
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfSend
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfSend (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIStdRip2IfConfAddress,
                                INT4 i4TestValFsMIStdRip2IfConfSend)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfSend (pu4ErrorCode, u4FsMIStdRip2IfConfAddress,
                                 i4TestValFsMIStdRip2IfConfSend);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfReceive
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfReceive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfReceive (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                   UINT4 u4FsMIStdRip2IfConfAddress,
                                   INT4 i4TestValFsMIStdRip2IfConfReceive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfReceive (pu4ErrorCode, u4FsMIStdRip2IfConfAddress,
                                    i4TestValFsMIStdRip2IfConfReceive);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfDefaultMetric
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfDefaultMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfDefaultMetric (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIRipContextId,
                                         UINT4 u4FsMIStdRip2IfConfAddress,
                                         INT4
                                         i4TestValFsMIStdRip2IfConfDefaultMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfDefaultMetric (pu4ErrorCode,
                                          u4FsMIStdRip2IfConfAddress,
                                          i4TestValFsMIStdRip2IfConfDefaultMetric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIStdRip2IfConfAddress,
                                  INT4 i4TestValFsMIStdRip2IfConfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfStatus (pu4ErrorCode, u4FsMIStdRip2IfConfAddress,
                                   i4TestValFsMIStdRip2IfConfStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdRip2IfConfSrcAddress
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress

                The Object 
                testValFsMIStdRip2IfConfSrcAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdRip2IfConfSrcAddress (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIRipContextId,
                                      UINT4 u4FsMIStdRip2IfConfAddress,
                                      UINT4
                                      u4TestValFsMIStdRip2IfConfSrcAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2Rip2IfConfSrcAddress (pu4ErrorCode, u4FsMIStdRip2IfConfAddress,
                                       u4TestValFsMIStdRip2IfConfSrcAddress);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIStdRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2IfConfAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIStdRip2IfConfTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIStdRip2PeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdRip2PeerTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdRip2PeerTable (INT4 i4FsMIRipContextId,
                                              UINT4 u4FsMIStdRip2PeerAddress,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMIStdRip2PeerDomain)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceRip2PeerTable (u4FsMIStdRip2PeerAddress,
                                               pFsMIStdRip2PeerDomain);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdRip2PeerTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdRip2PeerTable (INT4 *pi4FsMIRipContextId,
                                      UINT4 *pu4FsMIStdRip2PeerAddress,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIStdRip2PeerDomain)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexRip2PeerTable (pu4FsMIStdRip2PeerAddress,
                                                  pFsMIStdRip2PeerDomain);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdRip2PeerTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIStdRip2PeerAddress
                nextFsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain
                nextFsMIStdRip2PeerDomain
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdRip2PeerTable (INT4 i4FsMIRipContextId,
                                     INT4 *pi4NextFsMIRipContextId,
                                     UINT4 u4FsMIStdRip2PeerAddress,
                                     UINT4 *pu4NextFsMIStdRip2PeerAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdRip2PeerDomain,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsMIStdRip2PeerDomain)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexRip2PeerTable (u4FsMIStdRip2PeerAddress,
                                                 pu4NextFsMIStdRip2PeerAddress,
                                                 pFsMIStdRip2PeerDomain,
                                                 pNextFsMIStdRip2PeerDomain);

        RipResetContext ();
    }
    *pi4NextFsMIRipContextId = i4FsMIRipContextId;

    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }

    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) ==
            RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexRip2PeerTable (pu4NextFsMIStdRip2PeerAddress,
                                                  pNextFsMIStdRip2PeerDomain);

        RipResetContext ();
        i4FsMIRipContextId = *pi4NextFsMIRipContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2PeerLastUpdate
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain

                The Object 
                retValFsMIStdRip2PeerLastUpdate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2PeerLastUpdate (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIStdRip2PeerAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdRip2PeerDomain,
                                 UINT4 *pu4RetValFsMIStdRip2PeerLastUpdate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2PeerLastUpdate (u4FsMIStdRip2PeerAddress,
                                  pFsMIStdRip2PeerDomain,
                                  pu4RetValFsMIStdRip2PeerLastUpdate);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2PeerVersion
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain

                The Object 
                retValFsMIStdRip2PeerVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2PeerVersion (INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIStdRip2PeerAddress,
                              tSNMP_OCTET_STRING_TYPE * pFsMIStdRip2PeerDomain,
                              INT4 *pi4RetValFsMIStdRip2PeerVersion)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2PeerVersion (u4FsMIStdRip2PeerAddress, pFsMIStdRip2PeerDomain,
                               pi4RetValFsMIStdRip2PeerVersion);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2PeerRcvBadPackets
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain

                The Object 
                retValFsMIStdRip2PeerRcvBadPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2PeerRcvBadPackets (INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIStdRip2PeerAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdRip2PeerDomain,
                                    UINT4
                                    *pu4RetValFsMIStdRip2PeerRcvBadPackets)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2PeerRcvBadPackets (u4FsMIStdRip2PeerAddress,
                                     pFsMIStdRip2PeerDomain,
                                     pu4RetValFsMIStdRip2PeerRcvBadPackets);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2PeerRcvBadRoutes
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain

                The Object 
                retValFsMIStdRip2PeerRcvBadRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2PeerRcvBadRoutes (INT4 i4FsMIRipContextId,
                                   UINT4 u4FsMIStdRip2PeerAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIStdRip2PeerDomain,
                                   UINT4 *pu4RetValFsMIStdRip2PeerRcvBadRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetRip2PeerRcvBadRoutes (u4FsMIStdRip2PeerAddress,
                                    pFsMIStdRip2PeerDomain,
                                    pu4RetValFsMIStdRip2PeerRcvBadRoutes);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdRip2PeerInUseKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIStdRip2PeerAddress
                FsMIStdRip2PeerDomain

                The Object 
                retValFsMIStdRip2PeerInUseKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdRip2PeerInUseKey (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIStdRip2PeerAddress,
                               tSNMP_OCTET_STRING_TYPE * pFsMIStdRip2PeerDomain,
                               INT4 *pi4RetValFsMIStdRip2PeerInUseKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return =
        nmhGetFsRip2PeerInUseKey (u4FsMIStdRip2PeerAddress,
                                  pFsMIStdRip2PeerDomain,
                                  pi4RetValFsMIStdRip2PeerInUseKey);
    RipResetContext ();

    return i1Return;
}
