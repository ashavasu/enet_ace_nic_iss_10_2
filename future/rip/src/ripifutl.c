/*********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripifutl.c,v 1.64 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains interface record utility  
 *             routines which operate on the interface      
 *             record configuration structures.             
 *
 *******************************************************************/
#include "ripinc.h"
#include "fsmirip.h"
#include "snmputil.h"
#include "riptdfs.h"
#include   "arMD5_api.h"
UINT4               gaRIP_TRAP_OID[] = { 1, 3, 6, 1, 4, 1, 2076, 151, 6, 0 };
UINT4               gaRIP_SNMP_TRAP_OID[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

PRIVATE tRipPeerRec *rip_get_peer_record ARG_LIST ((UINT2, UINT4, tRipCxt *));
PRIVATE UINT2 rip_get_total_no_peers ARG_LIST ((tRipCxt *));

/*******************************************************************************

    Function            :   rip_initialize_if_to_defaults.

    Description         :   This routine initializes the interface record to the
                            default values given an index to the interface
                            record. It's main usage would be at the start-up and
                            during the delete operation for an interface.
    
    Input parameters    :   u2IfInd, an index to the iface record.
    
    Output parameters   :   None.
    
    Global variables 
    Affected            :   1. RipIfGblRec[u2IfIndex], which is of type
                               t_RIP_IFACE_REC is initialized to their default
                               values.
    
    Return value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_initialize_if_to_defaults
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_initialize_if_to_defaults (tRipIfaceRec * pRipIfRec)
{
    MEMSET ((&pRipIfRec->RipIfaceCfg), 0, sizeof (tRipIfaceCfg));
    pRipIfRec->RipIfaceCfg.u2AdminStatus = RIPIF_ADMIN_NOT_EXIST;
    pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_DISABLE;
    pRipIfRec->RipIfaceCfg.u1IfCurMask = RIP_CONF_DEF_BIT;
    pRipIfRec->RipIfaceCfg.u1RipAdminFlag = RIP_ADMIN_ENABLE;
    pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus =
        RIP_SPLIT_HORZ_WITH_POIS_REV;
    pRipIfRec->i2ResetCount = 0;
    MEMSET ((VOID *) &pRipIfRec->RipIfaceStats, 0, sizeof (tRipIfaceStats));
}

/*******************************************************************************

    Function            :   rip_check_recv_status().
    
    Description         :   This routine checks the configured receive status as
                            to whether this particular packet can be acceptable
                            or not and also checks for the validity of the
                            received address, basically a first level of
                            filtering.
    
    Input Parameters    :   1. u1Version Of Type UINT1.
                            2. u2IfIndex Of Type UINT2.
                            3. u4DestAddr Of Type UINT4.
                            4. u4SrcAddr Of Type UINT4.
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   None.
    
    Return Value        :   SUCCESS | FAILURE.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_check_recv_status
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

EXPORT INT4
rip_check_recv_status (UINT1 u1Version, UINT2 u2IfIndex, UINT4 u4DestAddr,
                       UINT4 u4SrcAddr, tRipCxt * pRipCxtEntry)
{
    INT4                i4Status = RIP_SUCCESS;
    UINT4               u4NetBroadcastAddress;
    UINT4               u4Addr;
    UINT4               u4Mask;
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Get the Interface Record from Hash Table */

    /*gets the Rip Interface record from the given Primary/Secondary 
     *IP address for which the network command is enabled*/
    pRipIfRec = RipGetUtilIfRecForPrimSecAddr (u4SrcAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        i4Status = RIP_FAILURE;
        /*Interface Record Not Found */
        return i4Status;
    }

    switch (u1Version)
    {

        case RIP_VERSION_0:

            i4Status = RIP_FAILURE;
            break;

        case RIP_VERSION_1:

            switch (pRipIfRec->RipIfaceCfg.u2RipRecvStatus)
            {

                case RIPIF_VERSION_1_RCV:
                case RIPIF_1_OR_2:
                    /* In this configuration, we can receive rip1 Or rip2
                       packets in broadcast mode */

                    /* ANVL -- Problem: No response for unicast request.
                     * Version 1 packet can be unicast or broadcast 
                     */
                    if (u4DestAddr == RIP_MULTICAST_ADDRESS)
                    {
                        i4Status = RIP_FAILURE;
                    }
                    break;

                case RIPIF_VERSION_2_RCV:
                default:
                    i4Status = RIP_FAILURE;    /* We are not permitted */
                    break;

            }                    /*  End of switch (recv status)  */
            break;

        case RIP_VERSION_2:

            switch (pRipIfRec->RipIfaceCfg.u2RipRecvStatus)
            {

                case RIPIF_1_OR_2:

                    /*
                     * Note that we can either receive rip2 packets in broadcast
                     * or multicast mode.
                     */
                    /* ANVL -- Problem: No response for unicast request.
                     * Version 2 pkts can be unicast/broadcast/multicast packets
                     * Nothing to check for this case.
                     * deleted the checks that were here
                     */
                    break;

                case RIPIF_VERSION_2_RCV:

                    u4Addr = RIPIF_GLBTAB_ADDR_OF (u2IfIndex);
                    u4Mask = RIPIF_GLBTAB_MASK_OF (u2IfIndex);
                    if (RIPIF_IS_UNNUMBERED (u2IfIndex) == FALSE)
                    {
                        if (u4Addr == (UINT4) RIP_FAILURE
                            || u4Mask == (UINT4) 0)
                        {
                            i4Status = RIP_FAILURE;
                            break;
                        }
                    }

                    u4NetBroadcastAddress = u4Addr | ~u4Mask;

                    /*
                     * RIP2 Packets can be generated from either RIP1Compatible
                     * interface or RIP2 interface; and so it can be received
                     * by multicast or broadcast packet or unicast packet
                     */
                    if ((u4DestAddr != RIP_MULTICAST_ADDRESS) &&
                        (u4DestAddr != RIP_BROADCAST_ADDRESS) &&
                        ((u4DestAddr | ~u4Mask) != u4NetBroadcastAddress))
                    {
                        i4Status = RIP_FAILURE;
                    }
                    break;

                case RIPIF_VERSION_1_RCV:
                default:
                    i4Status = RIP_FAILURE;
                    break;

            }                    /* End of switch ( recv status ) */
            break;

        default:

            /* For higher versions */

            if (pRipIfRec->RipIfaceCfg.u2RipRecvStatus == RIPIF_DO_NOT_RECV)
                i4Status = RIP_FAILURE;

            /*
             * Ideally, in the above case, we should not be receiving any
             * packets even we tend to be upward compatible as far as the other
             * versions are concerned.
             */

            break;

    }                            /* End of switch ( u1Version ) */

    return (i4Status);
}

/*******************************************************************************

    Function            :   rip_check_authentication.
    
    Description         :   This routine is called to check the incoming packet
                            for authentication. It works according to the
                            algorithm mentioned in rfc1723 "COMPATIBILITY"
                            section.
    
                            The algorithm reads as,
    
                            "If the router is not configured to authenticate
                            RIP-2 messages, then RIP-1 and unauthenticated RIP-2
                            messages will be accepted. If the router is
                            configured to authenticate RIP-2 messages, then
                            RIP-1 messages and RIP-2 messages which pass
                            authentication testing shall be accepted.
                            Unauthenticated and failed authentication RIP-2
                            messages shall be discarded. For maximum security,
                            RIP-1 messages must be ignored when authentication
                            is in use."
    
    Input Parameters    :   1. u1Version Of Type UINT1.
                            2. u2IfIndex Of Type UINT2.
                            3. pRipInfo Of Type tRipInfo *.
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   None.
    
    Return Value        :   Success | Failure.
    
*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_check_authentication
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

EXPORT INT4
rip_check_authentication (tRipInfo * pRipInfo,
                          UINT1 *pu1RipPkt, UINT4 u4SrcAddr, UINT2 u2IfIndex,
                          UINT2 *pu2Len, UINT4 *pu4SeqNo, UINT1 *pu1Key,
                          tRipCxt * pRipCxtEntry)
{
    INT4                i4Status = RIP_SUCCESS;
    UINT1               u1Version;
    tCryptoAuthInfo    *pCryptoInfo = NULL;
    tRip               *pRipHdr = NULL;
    tCryptoAuthKeyInfo *pAuthInfo = NULL;
    tRipPeerRec        *pPeerRec = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipTrapInfo        RipTrapInfo;
    tRipCxt            *pRipCxt = NULL;
    UINT4               u4CurrentTime = 0;
    tUtlTm              tm;
    INT4                i4DigestLen = 0;

    MEMSET (&RipTrapInfo, 0, sizeof (tRipTrapInfo));

    /*gets the Rip Interface record from the given Primary/Secondary 
     *IP address for which the network command is enabled*/
    pRipIfRec = RipGetUtilIfRecForPrimSecAddr (u4SrcAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        i4Status = RIP_FAILURE;
        /*Interface Record Not Found */
        return i4Status;
    }

    pRipHdr = (tRip *) (VOID *) pu1RipPkt;

    u1Version = pRipHdr->u1Version;
    switch (pRipIfRec->RipIfaceCfg.u2AuthType)
    {

        case RIPIF_NO_AUTHENTICATION:

            switch (u1Version)
            {

                case RIP_VERSION_1:
                    i4Status = RIP_SUCCESS;
                    break;

                case RIP_VERSION_2:

                    /*
                     * If we have authentication family identifier in the entry,
                     * we should not accept the packet.
                     */

                    if (RIP_NTOHS (RIP_ADDRESS_FAMILY (pRipInfo)) !=
                        (UINT2) RIP_AUTH_ADDRESS_FAMILY)
                    {
                        i4Status = RIP_SUCCESS;
                    }
                    else
                    {
                        (pRipIfRec->RipIfaceStats.u4RipV2NoAuthFailCount)++;
                        i4Status = RIP_FAILURE;
                    }
                    break;

                default:

                    /*
                     * In this case, we are sure that only the higher versions
                     * packets are coming and we can accept it.
                     */

                    i4Status = RIP_SUCCESS;
                    break;

            }                    /* End of switch (u1Version) */

            break;

        case RIPIF_SIMPLE_PASSWORD:

            switch (u1Version)
            {

                case RIP_VERSION_1:

                    /*
                     * We refer the MIB variable, rip2MaximumSecurity to see
                     * whether we can accept RIP version 1 packets or not.
                     */

                    if (pRipCxtEntry->RipGblCfg.u1MaximumSecurity ==
                        RIP_MAXIMUM_SECURITY)
                    {
                        (pRipIfRec->RipIfaceStats.
                         u4RipV1SimplePasswdFailCount)++;
                        i4Status = RIP_FAILURE;
                    }
                    else
                    {
                        i4Status = RIP_SUCCESS;
                    }
                    break;

                case RIP_VERSION_2:
                {

                    UINT1              *pu1AuthKey = NULL;

                    /*
                     * Packet discarding conditions.
                     * 1. Unauthenticated packets.
                     * 2. Authentication type mismatch.
                     * 3. Authentication failure.
                     */

                    if ((RIP_NTOHS (RIP_ADDRESS_FAMILY (pRipInfo)) !=
                         (UINT2) RIP_AUTH_ADDRESS_FAMILY) ||
                        (RIP_NTOHS (RIP_AUTH_TYPE (pRipInfo)) !=
                         (UINT2) RIPIF_SIMPLE_PASSWORD))
                    {

                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received an unauthenticated RIP packet "
                                      "on interface %d \n", (u2IfIndex));

                        (pRipIfRec->RipIfaceStats.
                         u4RipV2UnAuthenticatedPktFailCount)++;
                        (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                        i4Status = RIP_FAILURE;
                        break;
                    }

                    /*
                     * The packet is authenticated, Here, we need to compare the
                     * two string values and if they match, we will accept the
                     * current packet.
                     */

                    else
                    {
                        pu1AuthKey = &(RIP_AUTH_KEY (pRipInfo)[0]);

                        if (MEMCMP ((VOID *) pu1AuthKey,
                                    (VOID *) pRipIfRec->RipIfaceCfg.au1AuthKey,
                                    MAX_AUTH_KEY_LENGTH) != 0)
                        {

                            RIP_TRC_ARG1 (RIP_MOD_TRC,
                                          pRipCxtEntry->u4RipTrcFlag,
                                          pRipCxtEntry->i4CxtId,
                                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                          RIP_NAME,
                                          "Received a RIP packet on interface %d "
                                          "with a wrong text password \n",
                                          (u2IfIndex));
                            (pRipIfRec->RipIfaceStats.
                             u4RipV2SimplePasswdFailCount)++;
                            (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                            i4Status = RIP_FAILURE;
                        }
                    }
                }                /* End of case RIP_VERSION_2 */

                    break;

                default:
                    i4Status = RIP_SUCCESS;
                    break;

            }                    /* End of switch ( u1Version ) */

            break;

        case RIPIF_MD5_AUTHENTICATION:
            /* fall through for MD5 and SHA authentication */
        case RIPIF_SHA1_AUTHENTICATION:
            /* fall through for all SHA authentication */
        case RIPIF_SHA256_AUTHENTICATION:
            /* fall through for all SHA authentication */
        case RIPIF_SHA384_AUTHENTICATION:
            /* fall through for all SHA authentication */
        case RIPIF_SHA512_AUTHENTICATION:
            switch (u1Version)
            {

                case RIP_VERSION_1:
                    /*
                     * We refer the MIB variable, rip2MaximumSecurity to see
                     * whether we can accept RIP version 1 packets or not.
                     */
                    if (pRipCxtEntry->RipGblCfg.u1MaximumSecurity ==
                        RIP_MAXIMUM_SECURITY)
                    {
                        (pRipIfRec->RipIfaceStats.
                         u4RipV1Md5AuthenticatedPktFailCount)++;
                        i4Status = RIP_FAILURE;
                    }
                    else
                    {
                        i4Status = RIP_SUCCESS;
                    }
                    break;

                case RIP_VERSION_2:
                    /*
                     * If we dont have authentication family identifier 
                     * in the entry, we should not accept the packet */
                    if ((OSIX_NTOHS (RIP_ADDRESS_FAMILY (pRipInfo)) != (UINT2)
                         RIP_AUTH_ADDRESS_FAMILY) &&
                        (RIP_AUTH_TYPE (pRipInfo) <= RIPIF_MD5_AUTHENTICATION))
                    {

                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a unauthenticated "
                                      "RIP packet on interface %d \n",
                                      (u2IfIndex));
                        (pRipIfRec->RipIfaceStats.
                         u4RipV2Md5UnAuthenticatedPktFailCount)++;
                        i4Status = RIP_FAILURE;
                        break;
                    }

                    switch (pRipIfRec->RipIfaceCfg.u2AuthType)
                    {
                        case RIPIF_MD5_AUTHENTICATION:
                            i4DigestLen = RIP_MD5_DIGEST_LEN;
                            break;

                        case RIPIF_SHA1_AUTHENTICATION:
                            i4DigestLen = RIP_SHA1_DIGEST_LEN;
                            break;

                        case RIPIF_SHA256_AUTHENTICATION:
                            i4DigestLen = RIP_SHA256_DIGEST_LEN;
                            break;

                        case RIPIF_SHA384_AUTHENTICATION:
                            i4DigestLen = RIP_SHA384_DIGEST_LEN;
                            break;

                        case RIPIF_SHA512_AUTHENTICATION:
                            i4DigestLen = RIP_SHA512_DIGEST_LEN;
                            break;
                        default:
                            break;
                    }

                    pCryptoInfo =
                        (tCryptoAuthInfo *) (VOID *) (RIP_AUTH_KEY (pRipInfo));

                    /* Filling the info to be sent in trap */
                    pRipCxt = RipGetCxtInfoRecFrmIface (u2IfIndex);
                    if (pRipCxt != NULL)
                    {
                        RipTrapInfo.i4CxtId = pRipCxt->i4CxtId;
                        RipTrapInfo.u4PeerAddr = u4SrcAddr;
                        RipTrapInfo.u4IfIndex = (UINT4) u2IfIndex;
                        RipTrapInfo.i4AuthKeyId = (INT4) pCryptoInfo->u1KeyId;
                    }
                    if ((RIP_NTOHS (pCryptoInfo->u2RipPktLen)) !=
                        (*pu2Len - (i4DigestLen + RIP_AUTH_HDR_LEN)))
                    {
                        /* RIP2 packet length is invalid */
                        /* 
                         * RIP packet length should be the offset for the 
                         * authentication (message digest) at the last.
                         */

                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a authenticated RIP packet "
                                      "with invalid length on interface %d \n",
                                      (u2IfIndex));
                        (pRipIfRec->RipIfaceStats.
                         u4RipV2Md5InvalidLengthPktFailCount)++;
                        RipUtilSnmpIfSendTrap (RIP_AUTH_FAILURE_TRAP_ID,
                                               &RipTrapInfo);
                        (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                        return RIP_FAILURE;

                    }

                    pAuthInfo =
                        RipCryptoGetKeyToUseForReceive (u2IfIndex,
                                                        pCryptoInfo->u1KeyId,
                                                        pRipCxtEntry);

                    if (pAuthInfo == NULL)
                    {
                        /* No key with the received key Identifier */

                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a authenticated RIP packet "
                                      "with invalid key identifier on "
                                      "interface %d \n", (u2IfIndex));
                        (pRipIfRec->RipIfaceStats.
                         u4RipV2Md5InvalidKeyFailCount)++;
                        (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                        RipUtilSnmpIfSendTrap (RIP_AUTH_FAILURE_TRAP_ID,
                                               &RipTrapInfo);
                        return RIP_FAILURE;
                    }

                    /* Getting the current system time */
                    UtlGetTime (&tm);
                    u4CurrentTime = RipUtilGetSecondsSinceBase (tm);

                    if (RIP_CRYPTO_START_ACCEPT_TIME (pAuthInfo) >
                        u4CurrentTime)
                    {
                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a authenticated RIP packet "
                                      "with key start accept > current time "
                                      "on interface %d \n", (u2IfIndex));

                        (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                        RipUtilSnmpIfSendTrap (RIP_AUTH_FAILURE_TRAP_ID,
                                               &RipTrapInfo);
                        return RIP_FAILURE;
                    }

                    if (RIP_CRYPTO_STOP_ACCEPT_TIME (pAuthInfo) <=
                        u4CurrentTime)
                    {
                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a authenticated RIP packet "
                                      "with key start accept <= current time "
                                      "on interface %d \n", (u2IfIndex));

                        (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                        RipUtilSnmpIfSendTrap (RIP_AUTH_FAILURE_TRAP_ID,
                                               &RipTrapInfo);
                        return RIP_FAILURE;
                    }

                    /* Check the sequence number in the packet */

                    pPeerRec =
                        rip_get_peer_record (u2IfIndex, u4SrcAddr,
                                             pRipCxtEntry);

                    if (pPeerRec != NULL)
                    {
                        /* Peer already present.
                         * So the sequence number in the received packet should be 
                         * greater than the last received one.
                         */
                        if ((pCryptoInfo->u1KeyId ==
                             RIP_PEER_KEY_IN_USE (pPeerRec))
                            && ((RIP_NTOHL (pCryptoInfo->u4SeqNo)) >=
                                RIP_MD5_ALLOW_SEQUENCE_NO))
                        {
                            if ((RIP_NTOHL (pCryptoInfo->u4SeqNo)) <
                                RIP_PEER_LAST_SEQ_NO (pPeerRec))
                            {
                                RIP_TRC_ARG1 (RIP_MOD_TRC,
                                              pRipCxtEntry->u4RipTrcFlag,
                                              pRipCxtEntry->i4CxtId,
                                              ALL_FAILURE_TRC |
                                              CONTROL_PLANE_TRC, RIP_NAME,
                                              "Received a authenticated "
                                              "RIP packet with invalid sequence "
                                              "number on interface %d \n",
                                              (u2IfIndex));

                                (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                                RipUtilSnmpIfSendTrap (RIP_AUTH_FAILURE_TRAP_ID,
                                                       &RipTrapInfo);
                                return RIP_FAILURE;
                            }
                        }

                        /* received seq no greater than last one recvd */
                        /* Update the sequence number and key ID */

                        RIP_PEER_LAST_SEQ_NO (pPeerRec) =
                            RIP_NTOHL (pCryptoInfo->u4SeqNo);
                        RIP_PEER_KEY_IN_USE (pPeerRec) = pCryptoInfo->u1KeyId;
                    }
                    else
                    {
                        /* this is the first packet */
                        /* In case of RIP_RESPONSE this will bw updated by the 
                         * calling module.
                         */

                        *pu4SeqNo = RIP_NTOHL (pCryptoInfo->u4SeqNo);
                        *pu1Key = pCryptoInfo->u1KeyId;
                    }

                    i4Status =
                        RipUtilValidateAuthentication (pu1RipPkt, pu2Len,
                                                       pAuthInfo,
                                                       pRipIfRec->RipIfaceCfg.
                                                       u2AuthType);

                    if (i4Status == RIP_SUCCESS)
                    {
                        /* Received digest matches the calculated digest */

                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a valid authenticated "
                                      "RIP packet on interface %d \n",
                                      (u2IfIndex));
                    }
                    else
                    {
                        RIP_TRC_ARG1 (RIP_MOD_TRC,
                                      pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                      RIP_NAME,
                                      "Received a authenticated RIP packet "
                                      "with invalid digest "
                                      "on interface %d \n", (u2IfIndex));
                        (pRipIfRec->RipIfaceStats.u4InBadAuthPackets)++;
                        (pRipIfRec->RipIfaceStats.
                         u4RipV2Md5InvalidDigestFailCount)++;
                        RipUtilSnmpIfSendTrap (RIP_AUTH_FAILURE_TRAP_ID,
                                               &RipTrapInfo);
                    }

                    break;

                default:
                    return RIP_SUCCESS;

            }                    /* End of switch for MD5 */

            break;

        default:

            /*
             * Authentication unknown , we don't support any other type of
             * authentication.
             */

            i4Status = RIP_FAILURE;
            break;

    }                            /* End Of switch (RIPIF_CFG_AUTH_TYPE(u2IfIndex)) */

    return (i4Status);
}

/*******************************************************************************

    Function            :   rip_check_for_our_own_address.
                                             
    Description         :   This routine is called to check whether the given
                            address matches with any of our own interface
                            addresses. It goes from a valid interface till it
                            reaches invalid interface in the interface control
                            block and checks for the address match. If any of 
                            the addresses match, it returns TRUE message or
                            a FALSE message.
                                             
    Input Parameters    :   u4SrcAddr, the source address of the host which has
                            sent the RIP packet, of type UINT4.
                                             
    Output Parameters   :   None.
                                             
    Global Variables                         
    Affected            :   None.            
                                             
    Return Value        :   TRUE | FALSE.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_check_for_our_own_address
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/
EXPORT BOOLEAN
rip_check_for_our_own_address (UINT4 u4SrcAddr, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;

    /*
     * Scan through the list of valid interfaces.
     */

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if ((pRipIfRec->pRipCxt == pRipCxtEntry) &&
                (pRipIfRec->RipIfaceCfg.u4SrcAddr == u4SrcAddr))
            {
                RIP_TRC_ARG1 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Matched with address of iface - %d\n",
                              RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId));

                return TRUE;
            }
        }
    }                            /* End of IFACES_SCAN */

    return FALSE;
}

/*******************************************************************************

    Function            :   rip_check_send_status.
    
    Description         :   This procedure checks whether we have permission to
                            send the RIP packets with the given version. The
                            configured permission will be checked against the
                            version and if they agree, success will be returned
                            otherwise failure will be returned.
    
    Input Parameters    :   1. u1Version Of Type UINT1.
                            2. u2If, the interface index Of Type UINT2.
    
    Output Parameters   :   None.
    
    Global Variables 
    Affected            :   None.
    
    Return Value        :   success/failure Of Type INT4.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_check_send_status
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT INT4
rip_check_send_status (UINT1 u1Version, UINT2 u2If, tRipCxt * pRipCxtEntry)
{
    INT4                i4Status = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        i4Status = RIP_FAILURE;
        /*Interface Record Not Found */
        return i4Status;
    }

    switch (pRipIfRec->RipIfaceCfg.u2RipSendStatus)
    {

        case RIPIF_DO_NOT_SEND:
            i4Status = RIP_FAILURE;
            break;

        case RIPIF_VERSION_1_SND:
            if (u1Version != RIP_VERSION_1)
            {
                /* can't send the response */
                i4Status = RIP_FAILURE;
            }
            break;

        case RIPIF_RIP1_COMPATIBLE:
            i4Status = RIP_SUCCESS;
            break;

        case RIPIF_VERSION_2_SND:
            if (u1Version == RIP_VERSION_1)
            {
                i4Status = RIP_FAILURE;
            }
            break;

        default:
            i4Status = RIP_FAILURE;
            break;

    }
    /* End of switch (RIPIF_CFG_SEND_STATUS(u2If)) */

    return (i4Status);
}

/*******************************************************************************

    Function            :   rip_get_send_status.
    
    Description         :   This procedure does two primary things.
                            1. Validates the interface for its status.
                            2. Checks the send status for an interface and
                               (rip2IfConfSend parameter) then returns the
                               version that the interface is configured for, the
                               mode of address as to whether broadcast,
                               multicast that the RIP messages need to be sent.
    
    Input Parameters    :   1. u2If, the interface through which the RIP
                               information need to be sent.
                            2. pu1Version, the pointer to the version
                               variable at which the version will be filled in.
                            3. pu1MCastFlag, the pointer to the multicast flag
                               variable which will be filled in with multicast
                               enabled/disabled value.
                            4. pu1NeedUnicastUpdate - Added for Unicast update
                            feature. When RIPv1Compatible is enabled, we need to send
                            unicast packets to the configured Neighbors.
                    
    
    Output Parameters   :   pu1Version, pu1MCastFlag .
    
    Global Variables 
    Affected            :   None.
    
    Return Value        :   Success / Failure.
    
*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_send_status
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT INT4
rip_get_send_status (UINT2 u2If, UINT1 *pu1Version, UINT1 *pu1MCastFlag,
                     tRipCxt * pRipCxtEntry)
{
    INT4                i4Status = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        i4Status = RIP_FAILURE;
        /*Interface Record Not Found */
        return i4Status;
    }

    /* Validate the interface for admin & operational status. */
    if (pRipIfRec->RipIfaceCfg.u2AdminStatus != RIPIF_ADMIN_ACTIVE ||
        pRipIfRec->RipIfaceCfg.u2OperStatus != RIPIF_OPER_ENABLE)
    {

        i4Status = RIP_FAILURE;
        return i4Status;
    }

    switch (pRipIfRec->RipIfaceCfg.u2RipSendStatus)
    {
        case RIPIF_VERSION_1_SND:

            *pu1Version = RIP_VERSION_1;
            *pu1MCastFlag = RIP_MCAST_OFF;
            break;

        case RIPIF_RIP1_COMPATIBLE:

            *pu1Version = RIP_VERSION_2;
            *pu1MCastFlag = RIP_MCAST_OFF;
            break;

        case RIPIF_VERSION_2_SND:

            *pu1Version = RIP_VERSION_2;
            *pu1MCastFlag = RIP_MCAST_ON;
            break;

        default:

            i4Status = RIP_FAILURE;    /* No support */
            break;
    }
    return (i4Status);
}

/*******************************************************************************

    Function            :   rip_construct_auth_info.
    
    Description         :   This fills in the given authentication structure
                            with the configured information for this port.
                            If the type is a simple password, then the
                            appropriate key is filled in. Otherwise RIP_FAILURE
                            message is returned.
    
    Input Parameters    :   1. u2If, the interface for which authentication is
                               referred.
                            2. pRipAuthInfo, a pointer to authentication
                               structure, where the authentication info is to be
                               filled.
    
    Output Parameters   :   pRipAuthInfo is filled in with necessary information
                            and then returned.
    
    Global Variables 
    Affected            :   None.
    
    Return Value        :   success/failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_construct_auth_info
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

EXPORT INT4
rip_construct_auth_info (UINT1 *pu1RipPkt, UINT2 *pu2LenToSend, UINT2 u2If,
                         tRipCxt * pRipCxtEntry)
{
    tCryptoAuthInfo    *pCryptoInfo = NULL;
    tRipInfo           *pRipAuthInfo = NULL;
    tCryptoAuthKeyInfo *pCryptoKeyInfo = NULL;
    INT4                i4RetVal = RIP_SUCCESS;
    INT4                i4DigestLen = 0;
    tRipIfaceRec       *pTempRipIfRec = NULL;
    UINT4               u4HashIndex = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT1                i1IsDC = RIP_SUCCESS;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        i4RetVal = RIP_FAILURE;
        /*Interface Record Not Found */
        return i4RetVal;
    }

    switch (pRipIfRec->RipIfaceCfg.u2AuthType)
    {
        case RIPIF_SIMPLE_PASSWORD:
            /* Fall through */
        case RIPIF_MD5_AUTHENTICATION:
            i4DigestLen = RIP_MD5_DIGEST_LEN;
            break;

        case RIPIF_SHA1_AUTHENTICATION:
            i4DigestLen = RIP_SHA1_DIGEST_LEN;
            break;

        case RIPIF_SHA256_AUTHENTICATION:
            i4DigestLen = RIP_SHA256_DIGEST_LEN;
            break;

        case RIPIF_SHA384_AUTHENTICATION:
            i4DigestLen = RIP_SHA384_DIGEST_LEN;
            break;

        case RIPIF_SHA512_AUTHENTICATION:
            i4DigestLen = RIP_SHA512_DIGEST_LEN;
            break;

        default:
            break;
    }

    *pu2LenToSend = (UINT2) (*pu2LenToSend + (i4DigestLen + RIP_AUTH_HDR_LEN));

    /* interface is configured for MD5 or clear text password authentication */

    if (pRipIfRec->u1Persistence == RIP_WAN_TYPE)
        i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE) && (i1IsDC != RIP_SUCCESS))
    {
        pRipAuthInfo =
            (tRipInfo *) (VOID *) &(pu1RipPkt[sizeof (tUpdHdr) +
                                              sizeof (tRip)]);
    }
    else
    {
        /* we got the authentication tuple */

        pRipAuthInfo = (tRipInfo *) (VOID *) &(pu1RipPkt[sizeof (tRip)]);
    }

    /*
     * Fill in the necessary items in the tAuth structure.
     */

    RIP_ADDRESS_FAMILY (pRipAuthInfo) =
        RIP_HTONS ((UINT2) RIP_AUTH_ADDRESS_FAMILY);

    RIP_AUTH_TYPE (pRipAuthInfo) =
        (UINT2) (RIP_HTONS ((UINT2) pRipIfRec->RipIfaceCfg.u2AuthType));

    switch (pRipIfRec->RipIfaceCfg.u2AuthType)
    {                            /* For Scalability */

        case RIPIF_SIMPLE_PASSWORD:

            /*
             * Copy the authentication key value from the configured value to
             * the structure field.
             */

            MEMCPY ((VOID *) RIP_AUTH_KEY (pRipAuthInfo),
                    (const VOID *) pRipIfRec->RipIfaceCfg.au1AuthKey,
                    MAX_AUTH_KEY_LENGTH);
            break;

        case RIPIF_MD5_AUTHENTICATION:
            /* fall through for MD5 and SHA authentication */
        case RIPIF_SHA1_AUTHENTICATION:
            /* fall through for all SHA authentication */
        case RIPIF_SHA256_AUTHENTICATION:
            /* fall through for all SHA authentication */
        case RIPIF_SHA384_AUTHENTICATION:
            /* fall through for all SHA authentication */
        case RIPIF_SHA512_AUTHENTICATION:

            pCryptoInfo =
                (tCryptoAuthInfo *) (VOID *) RIP_AUTH_KEY (pRipAuthInfo);

            pCryptoInfo->u2RipPktLen = (UINT2) RIP_HTONS (*pu2LenToSend);

            pCryptoKeyInfo = RipCryptoGetKeyToUseForSend (u2If, pRipCxtEntry);

            if (pCryptoKeyInfo != NULL)
            {
                RIP_TRC_ARG2 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              CONTROL_PLANE_TRC, RIP_NAME,
                              "Key ID = %d "
                              "is used for sending on interface = %d \n ",
                              pCryptoKeyInfo->u1AuthKeyId, u2If);

                /* Authentication key found for this interface */
                pCryptoInfo->u1KeyId = RIP_CRYPTO_AUTH_KEY_ID (pCryptoKeyInfo);

                TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
                {
                    TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                          pTempRipIfRec, tRipIfaceRec *)
                    {
                        if (RIP_GET_IFACE_INDEX (pTempRipIfRec->IfaceId) ==
                            u2If)
                        {
                            pTempRipIfRec->RipIfaceCfg.u1KeyIdInUse =
                                RIP_CRYPTO_AUTH_KEY_ID (pCryptoKeyInfo);
                        }
                    }

                }

                pCryptoInfo->u1AuthDataLen =
                    (UINT1) (i4DigestLen + RIP_AUTH_HDR_LEN);

                pCryptoInfo->u4SeqNo =
                    RIP_HTONL (RIP_CRYPTO_SEQUENCE_NO (pCryptoKeyInfo));
                /* increment the sequence number */

                RIP_CRYPTO_SEQUENCE_NO (pCryptoKeyInfo)++;

                pCryptoInfo->u4Reserved1 = pCryptoInfo->u4Reserved2 = 0;

                RipUtilHandleAuthentication (pu1RipPkt,
                                             pRipIfRec->RipIfaceCfg.u2AuthType,
                                             pu2LenToSend, pCryptoKeyInfo);
            }
            else
            {
                /* Authentication key is not available */
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                         RIP_NAME, "No Authentication key is available. \n");

                *pu2LenToSend =
                    (UINT2) (*pu2LenToSend - (i4DigestLen + RIP_AUTH_HDR_LEN));
                i4RetVal = RIP_SUCCESS;
            }
            break;
        default:
            break;

    }                            /* End of switch() */
    return i4RetVal;
}

            /******************************************************
            * The following routines relates to the peer record   *
            * manipulation on a per interface basis.              *
            ******************************************************/

/*******************************************************************************

    Function            :   rip_update_peer_record.
    
    Description         :   This function updates the peer record corresponding
                            to the given peer address for the interface. If the
                            entry already exists, it just updates the relevent
                            fields. Otherwise, it allocates a new structure of
                            the peer address type, fills in appropriate values
                            and then adds the node to the linked list which is
                            being maintained in the sorted order. (Descending)
    
    Input Parameters    :   1. u2IfIndex, the port through which the RIP
                               response packet has arrived.
                            2. u4PeerAddr, the remote peer address from which
                               the RIP packet was received.
                            3. pu1RouteTag, the domain value that are present in
                               the RIP packet. (Always 0, Acc, rfc1723).
                            4. u1Version, the version of the received RIP
                               packet.
                            5. u2BadPkts, the number of bad packets received
                               which needs to be updated.
                            6. u2BadRoutes, the number of bad routes received
                               which needs to be updated.
    
    Output Parameters   :   None.
                           
    Global Variables 
    Affected            :   RIP peer record corresponding to the peer address
                            u4PeerAddr and the port u2IfIndex is updated.
    
    Return Value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_update_peer_record
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT INT4
rip_update_peer_record (UINT2 u2IfIndex, UINT4 u4PeerAddr,
                        UINT1 *pu1RouteTag, UINT1 u1Version,
                        UINT2 u2BadPkts, UINT2 u2BadRoutes,
                        UINT4 u4SeqNo, UINT1 u1KeyId, tRipCxt * pRipCxtEntry)
{
    UINT2               u2IndFlag = RIP_PEER_RECORD_OLD;
    tRipPeerRec        *pRipPeerRec = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return RIP_FAILURE;
    }

    if ((pRipPeerRec =
         rip_get_peer_record (u2IfIndex, u4PeerAddr, pRipCxtEntry)) == NULL)
    {

        /*
         * The peer is not available and may be a new peer, we will have a
         * preliminary check for MAXIMUM_NUMBER_OF_RIP_PEERS and then
         * accordingly make a decision.
         */

        if (rip_get_total_no_peers (pRipCxtEntry) >=
            pRipCxtEntry->RipGblCfg.u2TotalNoOfPeers)
        {

            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "Addition of a new RIP peer is not possible, "
                     "since limit for existing RIP peers "
                     "exceeds maximum limit \n");

            return RIP_SUCCESS;
        }

        /*
         * Looks like we can add a new peer entry and we will allocate the
         * necessary memory for that.
         */

        RIP_ALLOC_PEER_REC_MEM (pRipCxtEntry, pRipPeerRec);

        if (pRipPeerRec == NULL)
        {

            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC,
                     RIP_NAME,
                     "Memory allocation failure for a new RIP peer record \n");

            /*
             * Send traps the the net manager for further actions, if required.
             */

            RIP_SEND_TRAP_FOR_MEM_ALLOC_FAIL (RIP_MEMORY_ALLOCATION_FAILED);
            return RIP_FAILURE;
        }

        u2IndFlag = RIP_PEER_RECORD_NEW;

    }                            /* End of if (rip_get_peer_record ()) */

    /*
     * At this point,
     *      - We have a record of type tRipPeerRec*, which may either be a new
     *        entry or an existing one.
     *      - We can proceed with updation of necessary entries in the record.
     */

    RipRedDbNodeInit (&(pRipPeerRec->PeerDbNode), RIP_RED_DYN_PEER_INFO);
    pRipPeerRec->au1PeerRouteTag[0] = pu1RouteTag[0];
    pRipPeerRec->au1PeerRouteTag[1] = pu1RouteTag[1];
    pRipPeerRec->u2PeerVersion = (UINT2) u1Version;
    OsixGetSysTime (&pRipPeerRec->u4PeerLastUpdateTime);
    pRipPeerRec->u4PeerRcvBadPackets += u2BadPkts;
    pRipPeerRec->u4PeerRcvBadRoutes += u2BadRoutes;
    pRipPeerRec->u2IfIndex = u2IfIndex;

    if (u2IndFlag == RIP_PEER_RECORD_NEW)
    {

        /*
         * A new peer record and need to be added to the interface peer record
         * list. Had it been an existing peer, we won't be in this block because
         * it is not necessary to add that one.
         */

        RIP_PEER_LAST_SEQ_NO (pRipPeerRec) = u4SeqNo;
        RIP_PEER_KEY_IN_USE (pRipPeerRec) = u1KeyId;
        pRipPeerRec->u4PeerAddr = u4PeerAddr;
        RIP_SLL_Add (&pRipIfRec->RipPeerList, &(pRipPeerRec->Link));
        RipRedDbUtilAddTblNode (&gRipDynInfoList, &(pRipPeerRec->PeerDbNode));
        RipRedSyncDynInfo ();

    }

    return RIP_SUCCESS;
}

/*******************************************************************************

    Function            :   rip_retrieve_record_for_peer.
    
    Description         :   This function returns the peer record which
                            corresponds to the given peer address and the domain
                            tag. This scans through the list of valid
                            interfaces and also through the list of peer
                            records to fetch on.
    
    Input Parameters    :   1. u4PeerAddr, the address of the peer.
                            2. pu1Domain, the domain tag.
    
    Output Parameters   :   None.
    
    Global Variables 
    Affected            :   None.
    
    Return Value        :   Either the record if one is available or NULL Of
                            type tRipPeerRec*.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_retrieve_record_for_peer
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT tRipPeerRec *
rip_retrieve_record_for_peer (UINT4 u4PeerAddr, UINT1 *pu1Domain,
                              tRipCxt * pRipCxtEntry)
{
    tRipPeerRec        *pRipCurPeerRec = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipIfRec->pRipCxt != pRipCxtEntry)
            {
                continue;
            }

            /* Scan the list of peer list for the specified interface */
            RIP_SLL_Scan (&(pRipIfRec->RipPeerList), pRipCurPeerRec,
                          tRipPeerRec *)
            {

                if ((pRipCurPeerRec->u4PeerAddr == u4PeerAddr) &&
                    (MEMCMP (pRipCurPeerRec->au1PeerRouteTag, pu1Domain, sizeof
                             (pRipCurPeerRec->au1PeerRouteTag)) == 0))
                {

                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC,
                             RIP_NAME,
                             "rip_retrieve_record_for_peer, "
                             "Found a matching record. \n");

                    return pRipCurPeerRec;
                }
            }
        }
    }                            /* End of Logical Iface Scanning */

    return (NULL);
}

/*******************************************************************************

    Function            :   rip_if_update_periodic_timers.
    
    Description         :   This function goes through the list of interfaces
                            and then depending upon the function that is
                            specified, it links or unlinks the periodic timer
                            blocks for all the configured, enabled interfaces to
                            the global rip timer list for periodic operation of
                            RIP.
    
    Input Parameters    :   u1Function, denotes the actual operation to be done
                            on the timers, whether the timers need to be linked
                            or unlinked.
    
    Output Parameters   :   None.
    
    Global Variables 
    Affected            :   RipTimerList, the global list of timers, is updated
                            with either the addition or deletion of timers for
                            each node depending upon the function parameter.
    
    Return Value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_if_update_periodic_timers
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_if_update_periodic_timer (UINT1 u1Function, UINT2 u2If,
                              tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4Interval;
    UINT4               u4RemainingTime = 0;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return;
    }

    switch (u1Function)
    {

        case RIP_LINK_TIMERS:

            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          CONTROL_PLANE_TRC, RIP_NAME,
                          "Linking Update timer for the interface %d. \n",
                          (u2If));
            u4Interval =
                RipGetJitterVal (pRipIfRec->RipIfaceCfg.u2UpdateInterval,
                                 RIP_JITTER);
            RIP_START_TIMER (RIP_TIMER_ID,
                             &(pRipIfRec->RipUpdateTimer.TimerNode),
                             u4Interval);

            break;

        case RIP_UNLINK_TIMERS:

            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          CONTROL_PLANE_TRC, RIP_NAME,
                          "Unlinking Update timer for the interface %d. \n",
                          (u2If));
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &(pRipIfRec->RipUpdateTimer.TimerNode));

/* IF possible, we need to check whether the spacing timer is running or not
 * and stop this timer. As stop timer will not give a problem even if that timer is not
 * in the list, the following line is not giving any problem.
 */
            if ((RIP_GET_REMAINING_TIME (RIP_TIMER_ID,
                                         &(pRipIfRec->RipSpacingTimer.
                                           TimerNode),
                                         &u4RemainingTime)) == TMR_SUCCESS)
            {
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(pRipIfRec->RipSpacingTimer.TimerNode));
                TMO_DLL_Delete (&(pRipCxtEntry->RipIfSpaceLst),
                                &pRipIfRec->RipIfRecNode);
            }
            break;

        default:

            break;

    }                            /* End of switch (u1Function) */

}

/*******************************************************************************

    Function            :   rip_get_routes_to_compose.
    
    Description         :   This procedure is meant for getting the number of
                            routes that need to be composed per RIP packet. It
                            is based on the interface index on whether it
                            represents a single interface and if so, it returns
                            the routes to compose value based on authentication
                            configuration parameter. Otherwise, it returns the
                            routes to compose value as MAXIMUM VALUE minus 1 for
                            obvious reasons.
    
    Input Parameters    :   u2If, the interface through which the RIP
                            information need to be sent.
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   None.
    
    Return Value        :   Number Of Routes to compose as an UINT2 type value.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_routes_to_compose
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT UINT2
rip_get_routes_to_compose (UINT2 u2If, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Get the Interface Record from Hash Table */

    if (u2If != (UINT2) RIPIF_INVALID_INDEX)
    {
        if ((pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry)) != NULL)
        {
            /*
             * Update is for a specific interface. We need to check for whether
             * authentication is needed for this interface and if not, number of
             * packets to compose can be MAXIMUM NUMBER.
             */

            if (pRipIfRec->RipIfaceCfg.u2AuthType ==
                (UINT2) RIPIF_NO_AUTHENTICATION)
            {

                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         CONTROL_PLANE_TRC,
                         RIP_NAME,
                         "Authentication not needed for this interface, "
                         "So 25 routes can be composed \n");

                return ((UINT2) RIP_MAX_ROUTES_PER_PKT);
            }
        }

    }                            /* End of if (u2If != RIPIF_INVALID_INDEX) */

    RIP_TRC (RIP_MOD_TRC,
             pRipCxtEntry->u4RipTrcFlag,
             pRipCxtEntry->i4CxtId,
             CONTROL_PLANE_TRC,
             RIP_NAME,
             "Authentication needed for this interface, "
             "So 24 routes can be composed \n");

    return ((UINT2) (RIP_MAX_ROUTES_PER_PKT - 1));
}

/*******************************************************************************

    Function            :   rip_if_create.

    Description         :   This function alocates memory for the interface 
                            and adds the node to the (Interface)Hash Table.
                            This function is called from rip_create_if_instance
                             

    Input parameters    :   u2IfInd, the interface index.

    Output parameters   :   None.

    Global variables
    Affected            :   RipRtTabList, RipIfGblRec, RipGblStats.

    Return value        :   pointer to the created interface, if successfully 
                            created  
                            else returns NULL

*******************************************************************************/

tRipIfaceRec       *
rip_if_create (tRIP_INTERFACE IfId, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Allocate buffer for interface data structure */
    if (RIP_ALLOC_IF_REC_MEM (pRipIfRec) == NULL)
    {

        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC | INIT_SHUT_TRC,
                 RIP_NAME,
                 "Failure in the creation of MEM POOL for "
                 "RIP Interface  record. \n");
        return NULL;
    }

    MEMSET (pRipIfRec, 0, sizeof (tRipIfaceRec));
    TMO_HASH_INIT_NODE (&(pRipIfRec->nextIfNode));
    TMO_SLL_Init (&(pRipIfRec->RipPeerList));
    TMO_SLL_Init (&(pRipIfRec->RipMd5KeyList));
    TMO_SLL_Init (&(pRipIfRec->RipCryptoAuthKeyList));
    TMO_SLL_Init (&(pRipIfRec->RipUnicastNBRS));
    TMO_DLL_Init (&(pRipIfRec->RipIfRtList));

    pRipIfRec->IfaceId = IfId;
    pRipIfRec->pRipCxt = pRipCxtEntry;

    TMO_HASH_Add_Node (gRipRtr.pGIfHashTbl,
                       &(pRipIfRec->nextIfNode),
                       RipGetIfHashIndex (RIP_GET_IFACE_INDEX
                                          (pRipIfRec->IfaceId)), NULL);
    return pRipIfRec;
}

/*******************************************************************************

    Function            :   rip_if_enable.

    Description         :   This function enables the interface and is called
                            when either the Admin State | the Oper State of an
                            interface is set to ENABLE state.
                            It adds the direct route for this interface, links
                            the iface update timer.

    Input parameters    :   u2IfInd, the interface index.

    Output parameters   :   None.

    Global variables
    Affected            :   RipRtTabList, RipIfGblRec, RipGblStats.

    Return value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_if_enable
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_if_enable (tRipIfaceRec * pRipIfRec)
{
    UINT4               u4RemainingTime;
    UINT4               u4Interval;
    UINT2               u2IfInd;
    INT1                i1IsDC = RIP_SUCCESS;

    u2IfInd = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

    /*
     * Let us also set & link the update timer for this interface to the
     * timer list.
     */
    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE) || (i1IsDC == RIP_SUCCESS))
    {
        pRipIfRec->RipUpdateTimer.u1Id = RIP_UPDATE_TIMER_ID;
        pRipIfRec->RipUpdateTimer.u2If = u2IfInd;
        pRipIfRec->RipSpacingTimer.u1Id = RIP_SPACE_TIMER_ID;
        pRipIfRec->RipSpacingTimer.u2If = u2IfInd;
        TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pRipIfRec->RipIfRecNode);
    }

    if ((pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE) ||
        (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_PASSIVE))
    {
        /* Added to update local route when rip is enabled
           or interface transitioned to operationally up */
        RipAddLocalRoute (pRipIfRec);
    }

    /* 
     *  Interface route need not be added by RIP as it is done by ipif module. 
     */
    if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE)
    {
        if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE)
            || (i1IsDC == RIP_SUCCESS))
        {
            /*If Admin Status was set earlier,the Update timer might 
             * be running.check whether its running,by calling 
             * GET_REMAINING_TIME.If its already running ,stop it now.
             * */
            if ((RIP_GET_REMAINING_TIME (RIP_TIMER_ID,
                                         &(pRipIfRec->RipUpdateTimer.TimerNode),
                                         &u4RemainingTime)) == TMR_SUCCESS)
            {
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(pRipIfRec->RipUpdateTimer.TimerNode));
            }
            u4Interval =
                RipGetJitterVal (pRipIfRec->RipIfaceCfg.u2UpdateInterval,
                                 RIP_JITTER);
            RIP_START_TIMER (RIP_TIMER_ID,
                             &(pRipIfRec->RipUpdateTimer.TimerNode),
                             u4Interval);

        }
        else if (pRipIfRec->RipIfaceCfg.u2RipSendStatus != RIPIF_DO_NOT_SEND)
        {
            RipInitWanInterface (pRipIfRec);
        }
    }

}

/*******************************************************************************

    Function            :   rip_if_disable.

    Description         :   This function disables the interface and is called
                            when either the Admin State | the Oper State of an
                            interface is set to DISABLE state.
                            It puts all the corresponding routes for this
                            interface into the Garbage Collect Interval and then
                            unlinks the the iface update timer.

    Input parameters    :   tRipIfaceRec *pRipIfRec - Interface Record.

    Output parameters   :   None.

    Global variables
    Affected            :   RipRtTabList, RipIfGblRec, RipGblStats.

    Return value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_if_disable
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_if_disable (tRipIfaceRec * pRipIfRec)
{
    UINT2               u2IfInd;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4RemainingTime = 0;
    UINT4               u4CfaIfIndex = 0;
    tRipRtEntry        *pSummaryRt = NULL;
    INT4                i4RetVal = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    IfInfo.u1IfOperStatus = CFA_IF_DOWN;
    pRipCxtEntry = pRipIfRec->pRipCxt;

    u2IfInd = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

/* Change added to set localroute metric to 16 and
   initiate garbage collection for the route 
   this will be done here only if direct routes are not
   being redistributed
 */

    /* Get the CFA Interface Index from Ip Port No */
    if (NetIpv4GetCfaIfIndexFromPort (u2IfInd,
                                      &u4CfaIfIndex) == NETIPV4_SUCCESS)
    {
        i4RetVal = CfaGetIfInfo (u4CfaIfIndex, &IfInfo);
    }

    if (((IfInfo.u1IfOperStatus == CFA_IF_DOWN)) ||
        (!(pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable & RTM_DIRECT_MASK)))
    {
        RipDeleteLocalRoute (pRipIfRec);
        /*check if this route has auto summary routes */
        pSummaryRt = RipCheckSummaryAgg (pRipIfRec->u4Addr,
                                         pRipIfRec->u4NetMask, pRipCxtEntry);
        if (pSummaryRt != NULL)
        {
            if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) ==
                RIP_AUTO_SUMMARY_DISABLE)
            {

                RipDeleteSummarySinkRt (pSummaryRt, pRipCxtEntry);
                RipDeleteSummaryRt (pSummaryRt, pRipCxtEntry);
            }
        }

    }

    /*
     * When we are not operational, we should ideally be deleting all the
     * corresponding routes for this interface.
     */

    if (RipUpdateRoutesThroIf
        (u2IfInd, RIP_PURGE_ROUTES, pRipIfRec->RipIfaceCfg.u4SrcAddr,
         pRipCxtEntry) == RIP_DATABASE_CHANGED)
    {

        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME, "Generating Triggered update \n");

        if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE)
        {
/* 2.0.1.1 Fix 1 15-09-99 --START-- */
            if (rip_generate_update_message (RIP_VERSION_ANY,
                                             RIP_TRIG_UPDATE,
                                             IP_GEN_BCAST_ADDR,
                                             UDP_RIP_PORT,
                                             RIPIF_INVALID_INDEX,
                                             pRipCxtEntry) == RIP_FAILURE)
            {

                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC,
                         RIP_NAME,
                         "Fail in the generation of update message \n");

            }

/* 2.0.1.1 Fix 1 15-09-99 --END-- */

        }                        /* End of if (RipGblCfg.u1RipAdminFlag) */
    }

    /*
     * Unlink the timer associated with this interface, if the iface
     * is already active. Even for WAN circuits timers needs to be 
     * unlinked. 
     */
    if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE)
    {
        RIP_STOP_TIMER (RIP_TIMER_ID, &(pRipIfRec->RipUpdateTimer.TimerNode));
        if ((RIP_GET_REMAINING_TIME (RIP_TIMER_ID,
                                     &(pRipIfRec->RipSpacingTimer.TimerNode),
                                     &u4RemainingTime)) == TMR_SUCCESS)
        {
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &(pRipIfRec->RipSpacingTimer.TimerNode));
            TMO_DLL_Delete (&pRipCxtEntry->RipIfSpaceLst,
                            &pRipIfRec->RipIfRecNode);
        }
        /*
         *Delete the interface record from Space Interface list
         * */
        RIP_STOP_TIMER (RIP_TIMER_ID, &(pRipIfRec->RipSubscrTimer.TimerNode));

        pRipIfRec->RipUpdateTimer.u1Data = 0;
        pRipIfRec->RipSpacingTimer.u1Data = 0;
        pRipIfRec->RipSubscrTimer.u1Data = 0;
    }
    UNUSED_PARAM (i4RetVal);
}

/*******************************************************************************

    Function            :   rip_if_destroy.

    Description         :   This function deletes the interface instance and the
                            necessary resources.
                            It deletes the direct route for this interface,
                            unlinks the iface update timer, Initializes the
                            interface to default values and manipulates the
                            iface list.
                          
    Input parameters    :   u2IfInd, the interface index.

    Output parameters   :   None.

    Global variables
    Affected            :   RipRtTabList, RipIfGblRec, u2RipStartIndex.

    Return value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_if_destroy
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_if_destroy (tRipIfaceRec * pRipIfRec)
{

    /*
     * Before we proceed to do the `destroy` operation, we must delete the
     * direct route, all the routes that are learned through this interface from
     * the route database and also unlink the update timer for this iface.
     */
    rip_if_disable (pRipIfRec);
    RipClearIfInfo (pRipIfRec);
}

        /***************************************************************
        >>>>>>>>> PRIVATE Routines of this Module Start Here <<<<<<<<<<
        ***************************************************************/

/*******************************************************************************

    Function            :   rip_get_peer_record.
    
    Description         :   This function goes thro a singly linked list of peer
                            records which are maintained on a port basis, and
                            then extracts out the matching record for the peer
                            address, if it exists. If there is no matching
                            record, it returns a NULL record.
    
    Input Parameters    :   u2IfIndex, the port through which the peer is
                            sending update.
                            u4PeerAddr, the peer address which has sent the RIP
                            broadcast.
    
    Output Parameters   :   None.
    
    Global Variables 
    Affected            :   None.
    
    Return Value        :   A record of type t_RIP_PEER_REC* os returned. NULL
                            is returned if no matching record exists.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_peer_record
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE tRipPeerRec *
rip_get_peer_record (UINT2 u2IfIndex, UINT4 u4PeerAddr, tRipCxt * pRipCxtEntry)
{
    tRipPeerRec        *pRipPeerRec = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    /* Get the Interface Record from Hash Table */
    pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);

    if (pRipIfRec == NULL)
    {
        /*Interface Record Not Found */
        return NULL;
    }
    /*
     * Let us try and get the peer record corresponding to the port and the
     * source peer address. (If available) Peer record is maintained as a singly
     * linked list on a per interface basis.
     */

    RIP_SLL_Scan (&pRipIfRec->RipPeerList, pRipPeerRec, tRipPeerRec *)
    {

        /*
         * See if there is a match in the peer addresses.
         */

        if (pRipPeerRec->u4PeerAddr == u4PeerAddr)
        {
            break;
        }
    }

    return (pRipPeerRec);
}

/*******************************************************************************

    Function            :   rip_get_total_no_peers.
    
    Description         :   This procedure returns the total number of RIP peers
                            that are connected with the system.
    
    Input Parameters    :   None.
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   None.
    
    Return Value        :   The value of total no of RIP peers as a UINT2 type
                            value.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_get_total_no_peers
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE UINT2
rip_get_total_no_peers (tRipCxt * pRipCxtEntry)
{
    UINT2               u2PeerCount = 0;
    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipIfRec->pRipCxt == pRipCxtEntry)
            {
                u2PeerCount =
                    (UINT2) (u2PeerCount +
                             RIP_SLL_Count (&(pRipIfRec->RipPeerList)));
            }
        }
    }                            /* End of IFACES_SCAN */

    return (u2PeerCount);
}

EXPORT INT4
RipIsActiveInAnyInterface (tRipCxt * pRipCxtEntry)
{

    UINT4               u4HashIndex;
    tRipIfaceRec       *pRipIfRec = NULL;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if ((pRipIfRec->pRipCxt == pRipCxtEntry) &&
                (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE))
            {
                return RIP_SUCCESS;
            }
        }
    }                            /* End of IFACES_SCAN */

    return RIP_FAILURE;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipInitWanInterface                                 |
*|                                                                     |
*| Description   : Sends Update Request and Update Response with Flush |  
*|                 flag set. Called when                               |  
*|                  -power on                                          |  
*|                  -circuit down to circuit up                        |  
*|                                                                     |  
*| Input         : u2IfIndex                                           |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS / RIP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
RipInitWanInterface (tRipIfaceRec * pRipIfRec)
{
    tTrigRipPkt         RipPkt;
    tRoute             *pRouteEntry = &(RipPkt.aRipInfo[0].RipMesg.Route);
    UINT2               u2Len;
    UINT2               u2IfIndex;
    tRipCxt            *pRipCxtEntry = NULL;

    if (pRipIfRec == NULL)
    {
        return (RIP_FAILURE);
    }

    pRipCxtEntry = pRipIfRec->pRipCxt;

    u2IfIndex = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

    /* sending update request */
    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  pRipCxtEntry->u4RipTrcFlag,
                  pRipCxtEntry->i4CxtId,
                  CONTROL_PLANE_TRC, RIP_NAME,
                  "Sending Update Request on Interface %d.\n", (u2IfIndex));
    MEMSET (&RipPkt, 0, sizeof (tTrigRipPkt));
    RIP_GENERATE_REQUEST_HDR (RipPkt, pRipIfRec);
    u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));
    if (pRipIfRec->RipIfaceCfg.u2AuthType != (UINT2) RIPIF_NO_AUTHENTICATION)
    {
        u2Len = (UINT2) (u2Len + (UINT2) (sizeof (tRoute)));
        pRouteEntry = &(RipPkt.aRipInfo[1].RipMesg.Route);
        DEMAND_RIP_GENERATE_REQUEST_HDR (pRouteEntry);
        if (rip_construct_auth_info
            ((UINT1 *) &RipPkt, &u2Len, u2IfIndex, pRipCxtEntry) != RIP_SUCCESS)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          ALL_FAILURE_TRC, RIP_NAME,
                          "Failed while filling authentication structure other than simple password for an index %d.\n",
                          (u2IfIndex));
            (pRipIfRec->RipIfaceStats.u4RipConstructAuthInfoFailCount)++;
            return RIP_FAILURE;
        }
    }
    else
    {
        DEMAND_RIP_GENERATE_REQUEST_HDR (pRouteEntry);
        u2Len = (UINT2) (u2Len + (UINT2) (sizeof (tRoute)));

    }
    if (RipIfGetValidPeerIpAddress (pRipIfRec) == RIP_SUCCESS)
    {

        rip_task_udp_send ((UINT1 *) &RipPkt, UDP_RIP_PORT,
                           pRipIfRec->u4PeerAddress, UDP_RIP_PORT, u2Len,
                           RIP_INITIAL_TTL_VALUE, u2IfIndex,
                           RIP_INITIAL_BROADCAST_VALUE, 0, pRipCxtEntry);
    }
    /* starting request retx timer */
    RIPIF_FILL_REQ_RETX_TIMER (pRipIfRec,
                               (UINT1) RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry));
    RIP_START_TIMER (RIP_TIMER_ID, &pRipIfRec->RipSpacingTimer.TimerNode,
                     (UINT4) RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry));

    MEMSET (&RipPkt, 0, sizeof (tTrigRipPkt));
    /* sending update response with flush flag set */
    pRipIfRec->RipIfaceStats.u4SentTrigUpdates += 1;

    RIP_GENERATE_RESPONSE_HDR (RipPkt, pRipIfRec, RIP_FLUSH_FLAG);
    u2Len = (UINT2) (sizeof (tRip) + sizeof (tUpdHdr));

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  pRipCxtEntry->u4RipTrcFlag,
                  pRipCxtEntry->i4CxtId,
                  CONTROL_PLANE_TRC, RIP_NAME,
                  "Sending Update Response with flush flag on Interface %d.\n",
                  (u2IfIndex));
    if (pRipIfRec->RipIfaceCfg.u2AuthType != RIPIF_NO_AUTHENTICATION)
    {
        if (rip_construct_auth_info
            ((UINT1 *) &RipPkt, &u2Len, u2IfIndex, pRipCxtEntry) != RIP_SUCCESS)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          CONTROL_PLANE_TRC, RIP_NAME,
                          "Failed while filling authentication structure other than simple password for an index %d.\n",
                          (u2IfIndex));
            (pRipIfRec->RipIfaceStats.u4RipConstructAuthInfoFailCount)++;
            return RIP_FAILURE;
        }
    }

    pRipIfRec->u4PeerAddress = RipIfGetBaddrFromPort (u2IfIndex);

    if ((pRipIfRec->u4PeerAddress != 0) ||
        (RipIfGetValidPeerIpAddress (pRipIfRec) == RIP_SUCCESS))
    {

        rip_task_udp_send ((UINT1 *) &RipPkt, UDP_RIP_PORT,
                           pRipIfRec->u4PeerAddress, UDP_RIP_PORT, u2Len,
                           RIP_INITIAL_TTL_VALUE, u2IfIndex,
                           RIP_INITIAL_BROADCAST_VALUE, 0, pRipCxtEntry);
    }
    /* starting response retx timer */
    RIPIF_FILL_RES_RETX_TIMER (pRipIfRec, RIP_FLUSH_FLAG);
    RIP_START_TIMER (RIP_TIMER_ID, &pRipIfRec->RipUpdateTimer.TimerNode,
                     (UINT4) RIP_CFG_GET_RETX_TIMEOUT (pRipCxtEntry));

    pRipIfRec->u1ReTxCounter = 0;
    pRipIfRec->pReTxFirstRoute =
        (tRipRtNode *) TMO_DLL_First (&pRipCxtEntry->RipRtList);
    pRipIfRec->pReTxLastRoute = NULL;
    return RIP_SUCCESS;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RIP Interface functions to get the Interface info.  |
*|                                                                     |
*| Description   : All the below functions are the interface functions |  
*|                 that retrieve the information about the interface   |
*|                                                                     |  
*| Input         : u4IfIndex/u4Port                                    |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS / RIP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
UINT4
RipIfGetAddrFromPort (UINT4 u4Port)
{
    tNetIpv4IfInfo      NetIfInfo;
    UINT4               u4Index = u4Port;
    UINT4               u4RetVal = (UINT4) RIP_FAILURE;

    MEMSET ((UINT1 *) &NetIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4Index, &NetIfInfo) == NETIPV4_SUCCESS)
    {
        u4RetVal = NetIfInfo.u4Addr;
    }
    return (u4RetVal);
}

UINT4
RipIfGetMaskFromPort (UINT4 u4Port)
{
    tNetIpv4IfInfo      NetIfInfo;
    UINT4               u4RetVal;
    UINT4               u4Index = 0;
    MEMSET ((UINT1 *) &NetIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4Port, &NetIfInfo) == NETIPV4_FAILURE)
    {
        u4RetVal = (UINT4) RIP_FAILURE;
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_GLOBAL_FLAG,
                      RIP_INVALID_CXT_ID,
                      ALL_FAILURE_TRC, RIP_NAME,
                      "Failed while getting Interface Related configurations for an index %d.\n",
                      (u4Index));
        return (u4RetVal);
    }
    u4RetVal = NetIfInfo.u4NetMask;
    return (u4RetVal);
}

UINT4
RipIfGetBaddrFromPort (UINT4 u4Index)
{
    tNetIpv4IfInfo      NetIfInfo;
    UINT4               u4RetVal = (UINT4) RIP_FAILURE;

    MEMSET ((UINT1 *) &NetIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4Index, &NetIfInfo) == NETIPV4_FAILURE)
    {
        return (u4RetVal);
    }
    u4RetVal = NetIfInfo.u4BcastAddr;
    return (u4RetVal);
}

INT4
RipIfGetIndexFromAddrInCxt (UINT4 u4CxtId, UINT4 u4Addr)
{
    UINT4               u4IfIndex = 0;

    /*if the interface is unnumbered , the index itself is  */
    /* address - so we directly get the port number for    */
    /* that interface index */
    if ((u4Addr <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES) && (u4Addr != 0))
    {
        if (NetIpv4GetPortFromIfIndex (u4Addr, &u4IfIndex) == NETIPV4_FAILURE)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          RIP_TRC_GLOBAL_FLAG,
                          RIP_INVALID_CXT_ID,
                          ALL_FAILURE_TRC, RIP_NAME,
                          "Failed while getting IP PORT NUMBER for an index %d.\n",
                          (u4IfIndex));
            return RIP_FAILURE;
        }
    }
    else
    {
        if (NetIpv4GetIfIndexFromAddrInCxt (u4CxtId, u4Addr, &u4IfIndex)
            == NETIPV4_FAILURE)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          RIP_TRC_GLOBAL_FLAG,
                          RIP_INVALID_CXT_ID,
                          ALL_FAILURE_TRC, RIP_NAME,
                          "Failed while getting IP PORT NUMBER for an index %d.\n",
                          (u4IfIndex));
            return RIP_FAILURE;
        }
    }
    return (INT4) u4IfIndex;
}

UINT1
RipIfGetOperStatusFromPort (UINT4 u4Port)
{
    tNetIpv4IfInfo      NetIfInfo;
    UINT1               u1RetVal = (UINT1) RIP_FAILURE;

    MEMSET ((UINT1 *) &NetIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4Port, &NetIfInfo) == NETIPV4_FAILURE)
    {
        return (u1RetVal);
    }
    u1RetVal = (UINT1) NetIfInfo.u4Oper;
    return (u1RetVal);
}

UINT4
RipIfGetDflNetMask (UINT4 u4Addr)
{
    UINT4               u4Mask;

    if (IP_IS_ADDR_CLASS_A (u4Addr))
    {
        u4Mask = 0xff000000;
    }
    else if (IP_IS_ADDR_CLASS_B (u4Addr))
    {

        u4Mask = 0xffff0000;
    }
    else if (IP_IS_ADDR_CLASS_C (u4Addr))
    {
     /**** $$TRACE_LOG (EXIT, "Class C\n");****/
        u4Mask = 0xffffff00;
    }
    else
    {
     /**** $$TRACE_LOG (EXIT, "Class NO\n");****/
        u4Mask = 0xffffffff;
    }
    return u4Mask;
}

INT4
RipIfGetIfIdFromIfIndex (UINT4 u4Index, tIP_INTERFACE * pIfId)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4NetIpv4GetIfInfo;

    i4NetIpv4GetIfInfo = NetIpv4GetIfInfo (u4Index, &NetIpIfInfo);
    UNUSED_PARAM (i4NetIpv4GetIfInfo);

    pIfId->u4IfIndex = NetIpIfInfo.u4IfIndex;
    pIfId->u2_SubReferenceNum = 0;
    pIfId->u1_InterfaceType = (UINT1) NetIpIfInfo.u4IfType;
    pIfId->u1_InterfaceNum = (UINT1) NetIpIfInfo.u4IfIndex;
    return RIP_SUCCESS;
}

UINT1
RipIfIsUnnumbered (UINT2 u2Port)
{
    if (RipIfGetAddrFromPort (u2Port) == 0)
    {
        return TRUE;
    }

    return FALSE;
}

tRipIfaceRec       *
RipGetIfRec (UINT4 u4Port, tRipCxt * pRipCxtEntry)
{
    UINT4               u4HashIndex = RipGetIfHashIndex (u4Port);
    tRipIfaceRec       *pRipIfRec = NULL;

    TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                          pRipIfRec, tRipIfaceRec *)
    {
        if ((pRipIfRec->pRipCxt == pRipCxtEntry) &&
            (RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId) == u4Port))
        {
            return pRipIfRec;
        }

    }

    return NULL;
}

tRipIfaceRec       *
RipGetIfRecFromIndex (UINT4 u4Port)
{
    UINT4               u4HashIndex = RipGetIfHashIndex (u4Port);
    tRipIfaceRec       *pRipIfRec = NULL;

    TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                          pRipIfRec, tRipIfaceRec *)
    {
        if (RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId) == u4Port)
        {
            return pRipIfRec;
        }
    }
    return NULL;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipGetIfRecFromAddr                                 |
*|                                                                     |
*| Description   : Retrieves Interface Record Entry                    |
*|                   from Interface Hash Table                         |
*|                                                                     |  
*| Input         : u4IfAddr - Interface Address                        |
*|                 pRipCxtEntry- Context Pointer                       |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : Pointer to the (RipIfaceRec) Interface Record Entry |
*|                 if entry is found                                   |
*|                 else  NULL.                                         |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/

tRipIfaceRec       *
RipGetIfRecFromAddr (UINT4 u4IfAddr, tRipCxt * pRipCxtEntry)
{
    UINT4               u4HashIndex = 0;
    UINT4               u4IfIndex = 0;
    tRipIfaceRec       *pRipIfRec = NULL;

    /*if the interface is unnumbered , the interface index and
     * address are the same - so we fetch the interface record
     * from the port no. of that interface index*/
    if (u4IfAddr <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES)
    {
        if (NetIpv4GetPortFromIfIndex (u4IfAddr, &u4IfIndex) == NETIPV4_FAILURE)
        {
            return NULL;
        }
    }
    else
    {
        if (NetIpv4GetIfIndexFromAddrInCxt ((UINT4) pRipCxtEntry->i4CxtId,
                                            u4IfAddr,
                                            &u4IfIndex) == NETIPV4_FAILURE)
        {
            return NULL;
        }
    }

    u4HashIndex = RipGetIfHashIndex (u4IfIndex);
    TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                          pRipIfRec, tRipIfaceRec *)
    {
        if ((pRipIfRec->pRipCxt == pRipCxtEntry) &&
            (pRipIfRec->u4Addr == u4IfAddr))
        {
            return pRipIfRec;
        }
    }
    return NULL;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipGetIfHashIndex                                   |
*|                                                                     |
*| Description   : Generates HashIndex From IpPortNumber               |
*|                                                                     |  
*| Input         : u4Port --IP Port Number                             |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : u4HashIndex                                         |
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
UINT4
RipGetIfHashIndex (UINT4 u4Port)
{
    return (u4Port % ((gRipRtr.pGIfHashTbl)->u4_HashSize));
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipGetPortFromIfIndex                               |
*|                                                                     |
*| Description   : Retrieves the IP port Number From Interface Index   |
*|                                                                     |  
*| Input         : i4IfIndex  --> Interface Index                      |
*|                                                                     |  
*| Output        : pu4Port --> Port no                                 |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS/RIP_FAILURE                             |
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
RipGetPortFromIfIndex (INT4 i4IfIndex, UINT4 *pu4Port)
{
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex,
                                   pu4Port) == NETIPV4_FAILURE)
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipGetIfIndexFromPort                               |
*|                                                                     |
*| Description   : Retrieves the corresponding Interface Index for the |
*|                 port number                                         |  
*|                                                                     |  
*| Input         : u4Port  --> Port                                    |
*|                                                                     |  
*| Output        : pu4IfIndex --> Interface Index                      |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS/RIP_FAILURE                             |
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
RipGetIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, pu4IfIndex) == NETIPV4_FAILURE)
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/*******************************************************************************

    Function            :   RipClearInfo.

    Description         :   This function deletes the interface instance and the
                            necessary resources.
                            It deletes the direct route for this interface,
                            deletes the interface from the iface list.
    Input parameters    :   u2IfInd, the interface index.

    Output parameters   :   None.

    Global variables
    Affected            :   RipRtTabList, RipIfGblRec, u2RipStartIndex.

    Return value        :   None.
*************************************************************************/
VOID
RipClearIfInfo (tRipIfaceRec * pRipIfRec)
{
    UINT4               u4HashIndex = 0;
    tRipPeerRec        *pRipPeerRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    tMd5AuthKeyInfo    *pMd5AuthRec = NULL;
    tCryptoAuthKeyInfo *pCryptoAuthRec = NULL;
    tRipUnicastNBRS    *pRipUnicastNBRS = NULL;
    UINT2               u2IfInd;
    UINT4               u4RemainingTime = 0;

    pRipCxtEntry = pRipIfRec->pRipCxt;

    u2IfInd = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

    /* Delete all the interface specific aggregations configured over this 
     * interface.Dlete the trie created for this interface.ALso delete the 
     * entry from the SLL that holds the interface to aggregation trie
     * mapping.*/

    RipDeleteIfAgg (pRipIfRec);
    /*
     * Stop all the timers
     * */
    if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE)
    {
        RIP_STOP_TIMER (RIP_TIMER_ID, &(pRipIfRec->RipUpdateTimer.TimerNode));
        if ((RIP_GET_REMAINING_TIME (RIP_TIMER_ID,
                                     &(pRipIfRec->RipSpacingTimer.TimerNode),
                                     &u4RemainingTime)) == TMR_SUCCESS)
        {
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &(pRipIfRec->RipSpacingTimer.TimerNode));
            TMO_DLL_Delete (&pRipCxtEntry->RipIfSpaceLst,
                            &pRipIfRec->RipIfRecNode);
        }
        RIP_STOP_TIMER (RIP_TIMER_ID, &(pRipIfRec->RipSubscrTimer.TimerNode));

        pRipIfRec->RipUpdateTimer.u1Data = 0;
        pRipIfRec->RipSpacingTimer.u1Data = 0;
        pRipIfRec->RipSubscrTimer.u1Data = 0;
    }

    /* We need to unregister multicast registry for this interface */

    if ((pRipIfRec->RipIfaceCfg.u2RipRecvStatus == RIPIF_VERSION_2_RCV) ||
        (pRipIfRec->RipIfaceCfg.u2RipRecvStatus == RIPIF_1_OR_2))
    {

        /* Leave the mcast registry group */
        rip_ip_leave_mcast_group (u2IfInd);
    }

    /*
     * Then, the RipPeerList is cleared off. The following call removes & frees
     * all the elements in the given list and initializes the list.
     */

    while ((pRipPeerRec =
            (tRipPeerRec *) TMO_SLL_Get (&(pRipIfRec->RipPeerList))) != NULL)
    {
        RIP_FREE_PEER_REC_MEM (pRipCxtEntry, pRipPeerRec);
    }
    /*
     *Deleting the Md5auth List.
     *
     * */
    while ((pMd5AuthRec =
            (tMd5AuthKeyInfo *) TMO_SLL_Get (&(pRipIfRec->RipMd5KeyList))) !=
           NULL)
    {
        RIP_FREE_AUTH_KEY_NODE (pRipIfRec, pMd5AuthRec);
    }
    /*
     *Deleting the Cryptoauth List.
     *
     * */
    while ((pCryptoAuthRec =
            (tCryptoAuthKeyInfo *)
            TMO_SLL_Get (&(pRipIfRec->RipCryptoAuthKeyList))) != NULL)
    {
        RIP_FREE_CRYPTO_AUTH_KEY_NODE (pRipIfRec, pCryptoAuthRec);
    }
    /*
     * Delete the Unicat Neighbor List.
     * */
    while ((pRipUnicastNBRS =
            (tRipUnicastNBRS *) TMO_SLL_Get (&(pRipIfRec->RipUnicastNBRS))) !=
           NULL)
    {
        RIP_FREE_NBR_NODE (pRipIfRec, pRipUnicastNBRS);
    }

    u4HashIndex = RipGetIfHashIndex (u2IfInd);
    /* Delete the node from Hash Table */
    TMO_HASH_Delete_Node (gRipRtr.pGIfHashTbl,
                          &(pRipIfRec->nextIfNode), u4HashIndex);

    /* Release the Memory Allocated To the Interface Record */
    RIP_RELEASE_IF_REC_MEM (pRipIfRec);
}

/**************************************************************************
 *Function Name     :  RipUpdateIfaceNextRtSend.            
 *Description       :  This function Updates the interface nextsend route
                       of the interface records in RipSpaceIfList.
                       with valid route info.
 *Input(s)          :  pRipCxtEntry - Pointer to the context.
                       pRipRtEntry - Pointer to the route entry. 
 *Output(s)         :  None.                                     
 *Returns           :  None.
**************************************************************************/
VOID
RipUpdateIfaceNextRtSend (tRipCxt * pRipCxtEntry, tRipRtEntry * pRipRtEntry)
{
    tTMO_DLL_NODE      *pListNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipRtEntry        *pIfRt = NULL;
    INT2                i2OffSet =
        (INT2) RIP_OFFSET (tRipIfaceRec, RipIfRecNode);
    INT2                i2RtOffSet =
        (INT2) RIP_OFFSET (tRipRtEntry, RipCxtRtNode);

    TMO_DLL_Scan (&(pRipCxtEntry->RipIfSpaceLst), pListNode, tTMO_DLL_NODE *)
    {
        pRipIfRec =
            (tRipIfaceRec *) (VOID *) (((UINT1 *) pListNode) - i2OffSet);
        pIfRt = (tRipRtEntry *) (pRipIfRec->pRipNextSendRt);
        pIfRt = (tRipRtEntry *) ((FS_ULONG) pIfRt - (UINT2) i2RtOffSet);
        if (pIfRt == pRipRtEntry)
        {
            if (pRipRtEntry->RtInfo.u2RtProto == OTHERS_ID)
            {
                pRipIfRec->pRipNextSendRt =
                    TMO_DLL_Next (&(pRipCxtEntry->RipCxtSummaryList),
                                  (tTMO_DLL_NODE *) & pRipRtEntry->
                                  RipCxtRtNode);
            }
            else
            {
                pRipIfRec->pRipNextSendRt =
                    TMO_DLL_Next (&(pRipCxtEntry->RipCxtRtList),
                                  (tTMO_DLL_NODE *) & pRipRtEntry->
                                  RipCxtRtNode);
            }
        }
    }
}

/**************************************************************************
* Function Name     :  RipIfGetIpForUnnumIf  
* Description       :  Fetches some valid ip for the unnumbered interface
                       from the current context. 
* Input(s)          :  pRipCxtEntry - Pointer to the context entry.
* Output(s)         :  A valid ip address from current context.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/

INT4
RipIfGetIpForUnnumIf (tRipCxt * pRipCxtEntry, UINT4 *pu4InterfaceIp)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4NetCxtId;
    *pu4InterfaceIp = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetFirstIfInfo (&NetIpIfInfo) != NETIPV4_SUCCESS)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Failed while getting First entry in If config Record");
        return RIP_FAILURE;
    }
    do
    {
        if (NetIpv4GetCxtId (NetIpIfInfo.u4IfIndex, &u4NetCxtId)
            == NETIPV4_SUCCESS)
        {
            if ((UINT4) (pRipCxtEntry->i4CxtId) == (u4NetCxtId))
            {
                if ((NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE) &&
                    (NetIpIfInfo.u4Addr != 0))
                {
                    *pu4InterfaceIp = NetIpIfInfo.u4Addr;
                    return RIP_SUCCESS;
                }
            }
        }
    }
    while (NetIpv4GetNextIfInfo (NetIpIfInfo.u4IfIndex, &NetIpIfInfo)
           != NETIPV4_FAILURE);
    return RIP_FAILURE;
}

/**************************************************************************
* Function Name     :  RipIfGetConfAuthKey  
* Description       :  Fetches value of rip2IfConfAuthKey  
* Input(s)          :  u4IfAddr
* Output(s)         :  pOctetAuthKeyValue                                    
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/

INT4
RipIfGetConfAuthKey (UINT4 u4IfAddr,
                     tSNMP_OCTET_STRING_TYPE * pOctetAuthKeyValue)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }
    pRipIfRec = RipGetIfRecFromAddr (u4IfAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return (RIP_FAILURE);
    }

    pOctetAuthKeyValue->i4_Length =
        (INT4) STRLEN (pRipIfRec->RipIfaceCfg.au1AuthKey);
    MEMCPY (pOctetAuthKeyValue->pu1_OctetList,
            pRipIfRec->RipIfaceCfg.au1AuthKey, pOctetAuthKeyValue->i4_Length);
    return RIP_SUCCESS;

}

/**************************************************************************
* Function Name     :  RipIfGetValidPeerIpAddress  
* Description       :  select valid neighbor IP Address  
* Input(s)          :  pRipIfRec
* Output(s)         :  pRipIfRec->u4PeerAddress                            
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipIfGetValidPeerIpAddress (tRipIfaceRec * pRipIfRec)
{

    UINT1               u1Index;
    UINT4               u4NeighborAddr = 0;
    UINT4               u4FsRip2TrustNBRIpAddr = 0;
    UINT4               u4NumAuthorizedNbrs;
    INT4                i4RetValFsRip2TrustNBRRowStatus = 0;
    INT1                i1RetStatus;
    UINT1               u1NumNeighborIndex;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u4NumAuthorizedNbrs = pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs;
    if (u4NumAuthorizedNbrs != 0)
    {
        for (u1Index = 0; u1Index < u4NumAuthorizedNbrs; u1Index++)
        {
            u4NeighborAddr =
                ((pRipCxtEntry->RipNbrListCfg.aNbrList[u1Index].
                  u4FsRip2TrustNBRIpAddr > u4FsRip2TrustNBRIpAddr)
                 && ((u4NeighborAddr == u4FsRip2TrustNBRIpAddr)
                     || (pRipCxtEntry->RipNbrListCfg.aNbrList[u1Index].
                         u4FsRip2TrustNBRIpAddr <
                         u4NeighborAddr))) ? pRipCxtEntry->RipNbrListCfg.
                aNbrList[u1Index].u4FsRip2TrustNBRIpAddr : u4NeighborAddr;

            if (u4NeighborAddr != u4FsRip2TrustNBRIpAddr)
            {
                i1RetStatus = RipGetNeighborPosition ((INT4) u4NeighborAddr,
                                                      pRipCxtEntry);
                if (i1RetStatus != RIP_NBR_NOT_FOUND)
                {

                    /* Checks whether the given neighbor 
                     * is available in the List */
                    for (u1NumNeighborIndex = 0; u1NumNeighborIndex <
                         pRipCxtEntry->RipNbrListCfg.u4NumAuthorizedNbrs;
                         u1NumNeighborIndex++)
                    {
                        if (pRipCxtEntry->RipNbrListCfg.
                            aNbrList[u1NumNeighborIndex].u4FsRip2TrustNBRIpAddr
                            == (UINT4) u4NeighborAddr)
                            break;
                    }

                    if (u1NumNeighborIndex >= RIP_MAX_NUM_NBRS)
                    {
                        return SNMP_FAILURE;
                    }
                    i4RetValFsRip2TrustNBRRowStatus =
                        (INT4) pRipCxtEntry->RipNbrListCfg.
                        aNbrList[u1NumNeighborIndex].u4FsRip2TrustNBRRowStatus;
                    /*Checks rowstatus of neighbor is Active or not */
                    if ((i4RetValFsRip2TrustNBRRowStatus == ACTIVE) &&
                        ((pRipIfRec->u4Addr & pRipIfRec->u4NetMask) ==
                         (u4NeighborAddr & pRipIfRec->u4NetMask)))
                    {
                        pRipIfRec->u4PeerAddress = u4NeighborAddr;
                        return RIP_SUCCESS;
                    }
                }
            }
            u4FsRip2TrustNBRIpAddr = u4NeighborAddr;
        }
    }
    pRipIfRec->u4PeerAddress = 0;
    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RipUtilGetSecondsSinceBase                                 */
/*                                                                           */
/* Description  : This routine calculates the seconds from the obtained      */
/*                time. The seconds is calculated from the BASE year (2000)  */
/*                                                                           */
/* Input        : tUtlTm   -   Time format structure                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : The calculated time in seconds                             */
/*                                                                           */
/*****************************************************************************/
UINT4
RipUtilGetSecondsSinceBase (tUtlTm utlTm)
{
    UINT4               u4year = utlTm.tm_year;
    UINT4               u4Secs = 0;

    --u4year;
    while (u4year >= TM_BASE_YEAR)
    {
        u4Secs += SECS_IN_YEAR (u4year);
        --u4year;
    }
    u4Secs += (utlTm.tm_yday * 86400);
    u4Secs += (utlTm.tm_hour * 3600);
    u4Secs += (utlTm.tm_min * 60);
    u4Secs += (utlTm.tm_sec);

    return u4Secs;
}

/************************************************************************
 *  Function Name   : RipUtilConvertTime
 *  Description     : This function will validate the time
                      by parsing through the string against 
                      YEAR-MON-DD,HH:MM:SS.DS format and convert the time 
                      in string to structure.
 *  Input           : pu1TimeStr -
 *                    tm - Time structure to set or get time of key constants.
 *  Output          : None
 *  Returns         : RIP_SUCCESS/RIP_FAILURE
 ************************************************************************/
INT4
RipUtilConvertTime (UINT1 *pu1TimeStr, tUtlTm * tm)
{
    UINT4               u4Month = 0;
    UINT1               au1Temp[RIP_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;
    UINT4               au4YearDays[] = { 0, 31, 31, 62, 92, 123,
        153, 184, 215, 245, 276, 306
    };
    UINT1               au1MonthDays[] = { 31, 29, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31
    };

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);

    /* Parsing through the function  for year */
    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[0]))    /* Check if year is integer or not */
            && (ISDIGIT (pu1CurrPtr[1]))
            && (ISDIGIT (pu1CurrPtr[2])) && (ISDIGIT (pu1CurrPtr[3])))
        {
            MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
            MEMCPY (au1Temp, pu1CurrPtr, 4);
            tm->tm_year = (UINT4) ATOI (au1Temp);

            /* Checking for the validity of Year */
            if (tm->tm_year < TM_BASE_YEAR)
            {
                return RIP_INVALID_YEAR;
            }
            MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        }
        else
        {
            return RIP_INVALID_YEAR;
        }
    }
    else
    {
        return RIP_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-MM-DD,hh:mm:ss" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "MM-DD,hh:mm:ss" */
        MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        MEMCPY (au1Temp, pu1BasePtr, 2);
        /* Month Validations  and check if date is valid for respective month */
        if ((ISDIGIT (pu1BasePtr[0]))    /* Check if month is an integer */
            && (ISDIGIT (pu1BasePtr[1])))
        {
            u4Month = (UINT4) ATOI (au1Temp);

            if ((u4Month > 0) && (u4Month <= RIP_MAX_MONTH))
            {
                tm->tm_mon = u4Month - 1;
                tm->tm_yday += au4YearDays[tm->tm_mon];
            }
            else
            {
                return RIP_INVALID_MONTH;
            }
        }
        else
        {
            return RIP_INVALID_MONTH;
        }
        /* End of Month Validation */
    }
    else
    {
        return RIP_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-DD,hh:mm:ss" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "DD,hh:mm:ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        /* Checking for date */
        MEMCPY (au1Temp, pu1BasePtr, 2);
        if ((ISDIGIT (pu1BasePtr[0]))    /* Check if date is an integer */
            && (ISDIGIT (pu1BasePtr[1])))
        {
            tm->tm_mday = (UINT4) ATOI (au1Temp);
            tm->tm_yday += tm->tm_mday;
            /* Checking for the validity of Date */
            if ((tm->tm_mday < 1) || (tm->tm_mday >= RIP_MAX_MONTH_DAYS + 1))
            {
                return RIP_INVALID_DATE;
            }
            if (tm->tm_mday > au1MonthDays[tm->tm_mon])
            {
                return RIP_INVALID_DATE;
            }
            if ((tm->tm_mon == 1) && (!(IS_LEAP (tm->tm_year)))
                && (tm->tm_mday == RIP_MAX_LEAP_FEB_DAYS))
            {
                return RIP_INVALID_DATE;
            }
            if (tm->tm_mon >= 2)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    tm->tm_yday += RIP_MAX_LEAP_FEB_DAYS;

                }
                else
                {
                    tm->tm_yday += RIP_MIN_LEAP_FEB_DAYS;
                }
            }
            MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        }
        else
        {
            return RIP_INVALID_DATE;
        }
    }
    else
    {
        return RIP_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');    /*pu1CurrPtr postioned at ",hh:mm:ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /*pu1CurrPtr postioned at "hh:mm:ss" */
        if ((ISDIGIT (pu1CurrPtr[0]))    /* check if hour is integer */
            && (ISDIGIT (pu1CurrPtr[1])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_hour = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of Hour */
            if (tm->tm_hour > RIP_MAX_HOUR)
            {
                return RIP_INVALID_HOUR;
            }
            MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        }
        else
        {
            return RIP_INVALID_HOUR;
        }
    }
    else
    {
        return RIP_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":mm:ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;            /* pu1CurrPtr postioned at "mm:ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[0]))    /* Check if min is integer or not */
            && (ISDIGIT (pu1CurrPtr[1])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_min = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of min */
            if (tm->tm_min > RIP_MAX_MIN)
            {
                return RIP_INVALID_MIN;
            }
            MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        }
        else
        {
            return RIP_INVALID_MIN;
        }
    }
    else
    {
        return RIP_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;            /* pu1CurrPtr postioned at "ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        if ((ISDIGIT (pu1CurrPtr[0]))    /* Check if sec is integer or not */
            && (ISDIGIT (pu1CurrPtr[1])))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_sec = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of sec */
            if (tm->tm_sec > RIP_MAX_SEC)
            {
                return RIP_INVALID_SEC;
            }
            MEMSET (au1Temp, 0, RIP_TMP_NUM_STR);
        }
        else
        {
            return RIP_INVALID_SEC;
        }
    }
    else
    {
        return RIP_FAILURE;
    }
    return RIP_SUCCESS;
}

/************************************************************************
 *  Function Name   : RipUtilGetKeyTime
 *  Description     : This function will convert the time in structure to
                      string.
 *  Input           : u4Secs -Seconds
 *  Output          : pAuthKeyTime- Returns the Value in array
 *  Returns         : NONE
 ************************************************************************/
VOID
RipUtilGetKeyTime (UINT4 u4Secs, UINT1 *pAuthKeyTime)
{
    tUtlTm              tm;
    UINT4               au4YearDays[] = { 0, 31, 31, 62, 92, 123,
        153, 184, 215, 245, 276, 306
    };

    MEMSET (&tm, 0, sizeof (tUtlTm));

    UtlGetTimeForSeconds (u4Secs, &tm);    /* Tm structure is filled for secs */

    if (tm.tm_mon >= 2)
    {
        if (IS_LEAP (tm.tm_year))
        {
            tm.tm_yday = tm.tm_yday - RIP_MAX_LEAP_FEB_DAYS;
        }
        else
        {
            tm.tm_yday = tm.tm_yday - RIP_MIN_LEAP_FEB_DAYS;
        }
    }
    if (tm.tm_mon < RIP_MAX_MONTH)
    {
        tm.tm_yday = tm.tm_yday - au4YearDays[tm.tm_mon];
        tm.tm_mon = tm.tm_mon + 1;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_yday = tm.tm_yday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;
    /* Values are printed in the structure */
    SPRINTF ((CHR1 *) pAuthKeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon, tm.tm_yday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);
}

VOID
RipSortInsertAuthKey (tTMO_SLL * pCryptoAuthkeyList,
                      tCryptoAuthKeyInfo * pCryptoKeyInfo)
{
    UINT1               u1AuthkeyId;
    tTMO_SLL_NODE      *pPrev = NULL;
    tCryptoAuthKeyInfo *pTmpAuthkeyInfo;
    tTMO_SLL_NODE      *pLstNode;

    u1AuthkeyId = RIP_CRYPTO_AUTH_KEY_ID (pCryptoKeyInfo);
    TMO_SLL_Scan (pCryptoAuthkeyList, pLstNode, tTMO_SLL_NODE *)
    {
        pTmpAuthkeyInfo = (tCryptoAuthKeyInfo *) pLstNode;
        if (RIP_CRYPTO_AUTH_KEY_ID (pTmpAuthkeyInfo) < u1AuthkeyId)
            pPrev = pLstNode;
        if (RIP_CRYPTO_AUTH_KEY_ID (pTmpAuthkeyInfo) > u1AuthkeyId)
            break;
    }
    TMO_SLL_Insert (pCryptoAuthkeyList, pPrev, &(pCryptoKeyInfo->NextNode));

}

/************************************************************************
 *  Function Name   : RipUtilGetShaDigest
 *  Description     : This function will invoke the HMAC SHA utilities
 *                    based on the SHA version and calculates the 
 *                    message digest.
 *  Input           : pu1RipPkt - RIP packet
 *                    u2LenToSend - Length
 *                    u2AuthType - Authentication type
 *                    pCryptoKeyInfo - Authentication key info
 *  Output          : pdigest - Key Digest
 *  Returns         : NONE
 ************************************************************************/
VOID
RipUtilGetShaDigest (UINT1 *pu1RipPkt, UINT2 u2LenToSend, UINT2 u2AuthType,
                     tCryptoAuthKeyInfo * pCryptoKeyInfo, UINT1 *pdigest)
{
    UINT2               u2Len = 0;

    u2Len = RIP_CRYPTO_AUTH_KEY_LEN (pCryptoKeyInfo);

    switch (u2AuthType)
    {
        case RIPIF_SHA1_AUTHENTICATION:
            /* SHA1 Type */
            arHmac_Sha1 ((UINT1 *) pCryptoKeyInfo->au1AuthKey, u2Len,
                         pu1RipPkt, u2LenToSend, pdigest);
            break;

        case RIPIF_SHA256_AUTHENTICATION:
            /* SHA2 Type */
            arHmacSha2 (RIP_AR_SHA256_ALGO,
                        (UINT1 *) pCryptoKeyInfo->au1AuthKey, u2Len, pu1RipPkt,
                        u2LenToSend, pdigest);
            break;

        case RIPIF_SHA384_AUTHENTICATION:
            /* SHA2 Type */
            arHmacSha2 (RIP_AR_SHA384_ALGO,
                        (UINT1 *) pCryptoKeyInfo->au1AuthKey, u2Len, pu1RipPkt,
                        u2LenToSend, pdigest);
            break;

        case RIPIF_SHA512_AUTHENTICATION:
            /* SHA2 Type */
            arHmacSha2 (RIP_AR_SHA512_ALGO,
                        (UINT1 *) pCryptoKeyInfo->au1AuthKey, u2Len, pu1RipPkt,
                        u2LenToSend, pdigest);
            break;
        default:
            break;
    }
}

/************************************************************************
 *  Function Name   : RipUtilHandleAuthentication
 *  Description     : This function will invoke the routines to  
 *                    calculate the message digest based on the 
 *                    Authentication type and sends the calculated
 *                    message digest in the RIP update. 
 *  Input           : pu1RipPkt - RIP packet
 *                    u2LenToSend - Length
 *                    u2AuthType - Authentication type
 *                    pCryptoKeyInfo - Authentication key info
 *  Output          : pdigest - Key Digest
 *  Returns         : NONE
 ************************************************************************/
VOID
RipUtilHandleAuthentication (UINT1 *pu1RipPkt, UINT2 u2AuthType,
                             UINT2 *pu2LenToSend,
                             tCryptoAuthKeyInfo * pCryptoKeyInfo)
{
    tRipInfo           *pRipAuthInfo = NULL;
    INT4                i4DigestLen = 0;
    UINT2               u2KeyOffset;
    UINT1               au1CalcDigest[MAX_DIGEST_LENGTH];

    MEMSET (au1CalcDigest, 0, MAX_DIGEST_LENGTH);

    switch (u2AuthType)
    {
        case RIPIF_MD5_AUTHENTICATION:
            i4DigestLen = RIP_MD5_DIGEST_LEN;
            break;

        case RIPIF_SHA1_AUTHENTICATION:
            i4DigestLen = RIP_SHA1_DIGEST_LEN;
            break;

        case RIPIF_SHA256_AUTHENTICATION:
            i4DigestLen = RIP_SHA256_DIGEST_LEN;
            break;

        case RIPIF_SHA384_AUTHENTICATION:
            i4DigestLen = RIP_SHA384_DIGEST_LEN;
            break;

        case RIPIF_SHA512_AUTHENTICATION:
            i4DigestLen = RIP_SHA512_DIGEST_LEN;
            break;
        default:
            break;
    }

    MEMSET (&(pu1RipPkt[*pu2LenToSend]), 0, (i4DigestLen + RIP_AUTH_HDR_LEN));
    /* Add the trailer to the message */
    pRipAuthInfo = (tRipInfo *) (VOID *) &(pu1RipPkt[*pu2LenToSend]);

    RIP_ADDRESS_FAMILY (pRipAuthInfo) =
        RIP_HTONS ((UINT2) RIP_AUTH_ADDRESS_FAMILY);

    RIP_AUTH_TYPE (pRipAuthInfo) = RIP_HTONS ((UINT2) RIP_TRAILER_AUTH_TYPE);

    MEMCPY ((VOID *) RIP_AUTH_KEY (pRipAuthInfo),
            (const VOID *) RIP_CRYPTO_AUTH_KEY (pCryptoKeyInfo),
            MAX_AUTH_KEY_LENGTH);

    u2KeyOffset = (UINT2) (*pu2LenToSend + RIP_OFFSET_TO_KEY);

    *pu2LenToSend = (UINT2) (*pu2LenToSend + i4DigestLen + RIP_AUTH_HDR_LEN);

    if (u2AuthType == RIPIF_MD5_AUTHENTICATION)
    {
        Md5GetKeyDigest (pu1RipPkt, *pu2LenToSend, au1CalcDigest);
    }
    else
    {
        RipUtilGetShaDigest (pu1RipPkt, *pu2LenToSend, u2AuthType,
                             pCryptoKeyInfo, au1CalcDigest);
    }

    MEMCPY (&(pu1RipPkt[u2KeyOffset]), au1CalcDigest, i4DigestLen);

}

/************************************************************************
 *  Function Name   : RipUtilValidateAuthentication
 *  Description     : This function will invoke the routines to  
 *                    calculate the message digest and compares the
 *                    calculated digest with the digest received
 *                    in the RIP packet. 
 *  Input           : pu1RipPkt - RIP packet
 *                    u2Len - Length
 *                    u2AuthType - Authentication type
 *                    pCryptoKeyInfo - Authentication key info
 *  Output          : NONE
 *  Returns         : SUCCESS / FAILURE
 ************************************************************************/
INT4
RipUtilValidateAuthentication (UINT1 *pu1RipPkt, UINT2 *pu2Len,
                               tCryptoAuthKeyInfo * pAuthInfo, UINT2 u2AuthType)
{
    INT4                i4DigestLen = 0;
    INT4                i4Status = 0;
    UINT1               au1CalcDigest[MAX_DIGEST_LENGTH];
    UINT1               au1RecvDigest[MAX_DIGEST_LENGTH];

    MEMSET (au1CalcDigest, 0, MAX_DIGEST_LENGTH);
    MEMSET (au1RecvDigest, 0, MAX_DIGEST_LENGTH);

    switch (u2AuthType)
    {
        case RIPIF_MD5_AUTHENTICATION:
            i4DigestLen = RIP_MD5_DIGEST_LEN;
            break;

        case RIPIF_SHA1_AUTHENTICATION:
            i4DigestLen = RIP_SHA1_DIGEST_LEN;
            break;

        case RIPIF_SHA256_AUTHENTICATION:
            i4DigestLen = RIP_SHA256_DIGEST_LEN;
            break;

        case RIPIF_SHA384_AUTHENTICATION:
            i4DigestLen = RIP_SHA384_DIGEST_LEN;
            break;

        case RIPIF_SHA512_AUTHENTICATION:
            i4DigestLen = RIP_SHA512_DIGEST_LEN;
            break;
        default:
            break;
    }

    *pu2Len = (UINT2) (*pu2Len - (i4DigestLen + RIP_AUTH_HDR_LEN));

    /* Copy the digest to the message */

    MEMCPY (au1RecvDigest,
            &(pu1RipPkt[*pu2Len + RIP_OFFSET_TO_KEY]), i4DigestLen);

    MEMSET (&(pu1RipPkt[*pu2Len + RIP_OFFSET_TO_KEY]), 0, i4DigestLen);
    MEMCPY (&(pu1RipPkt[*pu2Len + RIP_OFFSET_TO_KEY]),
            RIP_CRYPTO_AUTH_KEY (pAuthInfo), MAX_AUTH_KEY_LENGTH);

    if (u2AuthType == RIPIF_MD5_AUTHENTICATION)
    {
        Md5GetKeyDigest (pu1RipPkt,
                         (UINT2) (*pu2Len + i4DigestLen + RIP_AUTH_HDR_LEN),
                         au1CalcDigest);
    }
    else
    {
        RipUtilGetShaDigest
            (pu1RipPkt, (UINT2) (*pu2Len + i4DigestLen + RIP_AUTH_HDR_LEN),
             u2AuthType, pAuthInfo, au1CalcDigest);
    }

    if (!MEMCMP (au1RecvDigest, au1CalcDigest, i4DigestLen))
    {
        /* Received digest matches the calculated digest */
        i4Status = RIP_SUCCESS;
    }
    else
    {
        i4Status = RIP_FAILURE;
    }
    return i4Status;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : RipUtilSnmpIfSendTrap                                   */
/*                                                                            */
/*  Description     : Generates trap and sends to SNMP manager.               */
/*                                                                            */
/*  Input(s)        : u1TrapId:  Trap ID representing the type of trap.       */
/*                    pRipTrapInfo: structure containing the trap information.*/
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : VOID                                                    */
/*                                                                            */
/******************************************************************************/
VOID
RipUtilSnmpIfSendTrap (UINT1 u1TrapId, tRipTrapInfo * pRipTrapInfo)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) || \
    defined (SNMPV3_WANTED)

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { RIP_ZERO, RIP_ZERO };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[RIP_MAX_BUFFER];

    MEMSET (&au1Buf, 0, RIP_MAX_BUFFER);

    pEnterpriseOid = alloc_oid ((sizeof (gaRIP_TRAP_OID) /
                                 sizeof (UINT4)) + RIP_TWO);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gaRIP_TRAP_OID,
            sizeof (gaRIP_TRAP_OID));

    pEnterpriseOid->u4_Length = sizeof (gaRIP_TRAP_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, gaRIP_SNMP_TRAP_OID,
            sizeof (gaRIP_SNMP_TRAP_OID));

    pSnmpTrapOid->u4_Length = sizeof (gaRIP_SNMP_TRAP_OID) / sizeof (UINT4);

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "fsMIRip2ContextId");
    if ((pOid = RipUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32, RIP_ZERO,
         pRipTrapInfo->i4CxtId, NULL, NULL, u8CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    switch (u1TrapId)
    {
        case RIP_AUTH_FAILURE_TRAP_ID:
            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMIRipPeerAddress");
            if ((pOid = RipUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, RIP_ZERO,
                 (INT4) pRipTrapInfo->u4PeerAddr, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMIRipAuthIfIndex");
            if ((pOid = RipUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, RIP_ZERO,
                 (INT4) pRipTrapInfo->u4IfIndex, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMIRipAuthKeyId");
            if ((pOid = RipUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, RIP_ZERO,
                 pRipTrapInfo->i4AuthKeyId, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case RIP_AUTH_LAST_KEY_TRAP_ID:

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMIRipAuthIfIndex");
            if ((pOid = RipUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, RIP_ZERO,
                 (INT4) pRipTrapInfo->u4IfIndex, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            SPRINTF ((CHR1 *) au1Buf, "fsMIRipAuthKeyId");
            if ((pOid = RipUtilMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid, SNMP_DATA_TYPE_INTEGER32, RIP_ZERO,
                 pRipTrapInfo->i4AuthKeyId, NULL, NULL, u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        default:

            SNMP_free_snmp_vb_list (pStartVb);
            return;
    }
    /* The following API sends the Trap info to the FutureSNMP Agent */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

#else
    UNUSED_PARAM (pRipTrapInfo);
    UNUSED_PARAM (u1TrapId);
#endif
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : RipUtilParseSubIdNew                                   */
/*                                                                            */
/*  Description     : returns the numeric value of ppu1TempPtr                */
/*                                                                            */
/*  Input(s)        :  **ppu1TempPtr                                          */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Returns         : value of ppu1TempPtr or RIP_INVALID                  */
/*                                                                            */
/******************************************************************************/
INT4
RipUtilParseSubIdNew (UINT1 **ppu1TempPtr)
{
    INT4                i4Value = RIP_ZERO;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        i4Value = (i4Value * RIP_NUMBER_TEN) +
            (*pu1Tmp & RIP_MAX_HEX_SINGLE_DIGIT);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4Value = RIP_INVALID;
    }
    *ppu1TempPtr = pu1Tmp;
    return (i4Value);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : RipUtilMakeObjIdFromDotNew                             */
/*                                                                            */
/*  Description     : Gives Oid from the input string.                        */
/*                                                                            */
/*  Input(s)        :  pi1TxtStr                                              */
/*                                                                            */
/*  Output(s)       :  None                                                   */
/*                                                                            */
/*  Returns         :  pOidPtr or NULL                                        */
/*                                                                            */
/******************************************************************************/
#define   RIP_SNMP_TEMP_BUFF   280
INT1                gai4RipTempBuffer[RIP_SNMP_TEMP_BUFF];
tSNMP_OID_TYPE     *
RipUtilMakeObjIdFromDotNew (INT1 *pi1TxtStr)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1TempPtr = NULL, *pi1DotPtr = NULL;
    UINT2               u2Cnt = RIP_ZERO;
    UINT2               u2DotCount = RIP_ZERO;
    UINT1              *pu1TmpPtr = NULL;
    UINT1               u1BufferLen = 0;
    UINT1               u1Len = 0;

    MEMSET (gai4RipTempBuffer, 0, RIP_SNMP_TEMP_BUFF);
    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TxtStr) != RIP_ZERO)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TxtStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TxtStr + STRLEN ((INT1 *) pi1TxtStr);
        }
        pi1TempPtr = pi1TxtStr;

        for (u2Cnt = RIP_ZERO;
             ((pi1TempPtr < pi1DotPtr) &&
              (u2Cnt < RIP_SNMP_TEMP_BUFF)); u2Cnt++)
        {
            gai4RipTempBuffer[u2Cnt] = *pi1TempPtr++;
        }
        if (u2Cnt != RIP_SNMP_TEMP_BUFF)
            gai4RipTempBuffer[u2Cnt] = '\0';
        else
        {
            return (NULL);
        }

        for (u2Cnt = RIP_ZERO;
             (u2Cnt < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))
              && (orig_mib_oid_table[u2Cnt].pName != NULL)); u2Cnt++)
        {
            if ((STRCMP (orig_mib_oid_table[u2Cnt].pName,
                         (INT1 *) gai4RipTempBuffer) == RIP_ZERO)
                && (STRLEN ((INT1 *) gai4RipTempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Cnt].pName)))
            {
                u1Len = (UINT1) ((STRLEN (orig_mib_oid_table[u2Cnt].pNumber) <
                                  sizeof (gai4RipTempBuffer)) ?
                                 STRLEN (orig_mib_oid_table[u2Cnt].
                                         pNumber) : sizeof (gai4RipTempBuffer) -
                                 1);
                STRNCPY ((INT1 *) gai4RipTempBuffer,
                         orig_mib_oid_table[u2Cnt].pNumber, u1Len);
                gai4RipTempBuffer[u1Len] = '\0';
                break;
            }
        }
        if ((u2Cnt < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
            && (orig_mib_oid_table[u2Cnt].pName == NULL))
        {
            return (NULL);
        }

        if (u2Cnt == (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u1BufferLen =
            (UINT1) (sizeof (gai4RipTempBuffer) - (STRLEN (gai4RipTempBuffer)));
        if (u1BufferLen > 0)
        {
            u1BufferLen = (UINT1) ((STRLEN (pi1DotPtr) < u1BufferLen) ?
                                   (UINT1) (STRLEN (pi1DotPtr)) : u1BufferLen -
                                   1);
            STRNCAT ((INT1 *) gai4RipTempBuffer, (INT1 *) pi1DotPtr,
                     u1BufferLen);
            gai4RipTempBuffer[u1Len + u1BufferLen] = '\0';
        }
    }
    else
    {                            /* is not alpha, so just copy into gai4RipTempBuffer */
        u1Len =
            (UINT1) ((STRLEN (pi1TxtStr) <
                      sizeof (gai4RipTempBuffer)) ? STRLEN (pi1TxtStr) :
                     sizeof (gai4RipTempBuffer) - 1);
        STRNCPY ((INT1 *) gai4RipTempBuffer, (INT1 *) pi1TxtStr, u1Len);
        gai4RipTempBuffer[u1Len] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = RIP_ZERO;
    for (u2Cnt = RIP_ZERO;
         ((u2Cnt < RIP_SNMP_TEMP_BUFF) && (gai4RipTempBuffer[u2Cnt] != '\0'));
         u2Cnt++)
    {
        if (gai4RipTempBuffer[u2Cnt] == '.')
        {
            u2DotCount++;
        }
    }
    if ((pOidPtr = alloc_oid ((INT4) (u2DotCount + RIP_ONE))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) gai4RipTempBuffer;
    for (u2Cnt = RIP_ZERO; u2Cnt < u2DotCount + RIP_ONE; u2Cnt++)
    {
        if ((pOidPtr->pu4_OidList[u2Cnt] =
             ((UINT4) (RipUtilParseSubIdNew (&pu1TmpPtr)))) ==
            (UINT4) RIP_INVALID)
        {
            free_oid (pOidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
        {
            pu1TmpPtr++;        /* to skip over dot */
        }
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */
    pOidPtr->u4_Length = (UINT4) (u2DotCount + RIP_ONE);

    return (pOidPtr);
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipGetUtilIfRecForPrimSecAddr                       |
*|                                                                     |
*| Description   : Retrieves Interface Record Entry                    |
*|                 from Interface Hash Table according to the primary  |
*|                 or secondary source address received.               |
*|                                                                     |  
*| Input         : u4SrcAddr    - Source Address                       |
*|                 pRipCxtEntry - Context Pointer                      |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : Pointer to the (RipIfaceRec) Interface Record Entry |
*|                 if entry is found                                   |
*|                 else  NULL.                                         |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/

tRipIfaceRec       *
RipGetUtilIfRecForPrimSecAddr (UINT4 u4SrcAddr, tRipCxt * pRipCxtEntry)
{
    UINT4               u4HashIndex = 0;
    UINT4               u4IfSrcAddress = 0;
    UINT4               u4IfRecSrcAddress = 0;
    UINT1               u1RecFoundFlag = 0;
    tRipIfaceRec       *pRipIfRec = NULL;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {

            if (pRipIfRec->pRipCxt != pRipCxtEntry)
            {
                continue;
            }

            u4IfRecSrcAddress = (pRipIfRec->RipIfaceCfg.u4SrcAddr) &
                (pRipIfRec->u4NetMask);
            u4IfSrcAddress = u4SrcAddr & (pRipIfRec->u4NetMask);

            if ((pRipIfRec != NULL) && (u4IfSrcAddress != u4IfRecSrcAddress))
            {
                continue;
            }
            else if ((pRipIfRec != NULL)
                     && (u4IfSrcAddress == u4IfRecSrcAddress))
            {
                u1RecFoundFlag = RIP_FLAG_SET;
                break;
            }

        }
        if (u1RecFoundFlag == RIP_FLAG_SET)
        {
            return pRipIfRec;
        }
    }
    return NULL;

}

/*
 * *+---------------------------------------------------------------------+
 * *| Function Name : RipGetUtilNoOfRoutes                                |
 * *|                                                                     |
 * *| Description   : This fucntion calculates the no of valid routes     |
 * *|                 to be sent out from message length, rip header      |
 * *|                 rip info length                                     |
 * *| Input         : u2Len    - Length of the message.                   |
 * *|                 u2If     - Interface index.                         |
 * *|                 pRipCxtEntry - Context Pointer                      |
 * *| Output        : None.                                               |
 * *|                                                                     |
 * *| Returns       : Valid No of Routes to be sent out                   |
 * *|                                                                     |
 * *+---------------------------------------------------------------------+
 * */

UINT2
RipGetUtilNoOfRoutes (UINT2 u2Len, UINT2 u2If, tRipCxt * pRipCxtEntry)
{

    tRipIfaceRec       *pRipIfRec = NULL;
    tCryptoAuthKeyInfo *pCryptoKeyInfo = NULL;
    UINT2               u2NoOfValidRoutes = 0;
    UINT2               u2LenToSend = 0;

    if (u2If != (UINT2) RIPIF_INVALID_INDEX)
    {
        if ((pRipIfRec = RipGetIfRec (u2If, pRipCxtEntry)) != NULL)
        {
            if (pRipIfRec->RipIfaceCfg.u2AuthType ==
                (UINT2) RIPIF_NO_AUTHENTICATION)
            {

                u2NoOfValidRoutes =
                    (UINT2) ((u2Len - RIP_HDR_LEN) / RIP_INFO_LEN);
            }
            else
            {
                pCryptoKeyInfo =
                    RipCryptoGetKeyToUseForSend (u2If, pRipCxtEntry);

                if (pCryptoKeyInfo != NULL)
                {
                    switch (pRipIfRec->RipIfaceCfg.u2AuthType)
                    {

                        case RIPIF_MD5_AUTHENTICATION:

                            u2LenToSend =
                                (UINT2) (2 *
                                         (RIP_MD5_DIGEST_LEN +
                                          RIP_AUTH_HDR_LEN));
                            break;

                        case RIPIF_SHA1_AUTHENTICATION:
                            u2LenToSend =
                                (UINT2) (2 *
                                         (RIP_SHA1_DIGEST_LEN +
                                          RIP_AUTH_HDR_LEN));
                            break;

                        case RIPIF_SHA256_AUTHENTICATION:
                            u2LenToSend = (UINT2) (2 *
                                                   (RIP_SHA256_DIGEST_LEN +
                                                    RIP_AUTH_HDR_LEN));
                            break;

                        case RIPIF_SHA384_AUTHENTICATION:
                            u2LenToSend = (UINT2) (2 *
                                                   (RIP_SHA384_DIGEST_LEN +
                                                    RIP_AUTH_HDR_LEN));
                            break;

                        case RIPIF_SHA512_AUTHENTICATION:
                            u2LenToSend = (UINT2) (2 *
                                                   (RIP_SHA512_DIGEST_LEN +
                                                    RIP_AUTH_HDR_LEN));
                            break;

                        default:
                            break;
                    }
                }
                if (pRipIfRec->RipIfaceCfg.u2AuthType == RIPIF_SIMPLE_PASSWORD)
                {
                    u2LenToSend =
                        (UINT2) (RIP_MD5_DIGEST_LEN + RIP_AUTH_HDR_LEN);
                }
                u2NoOfValidRoutes = (UINT2) (u2Len - u2LenToSend);

                u2NoOfValidRoutes =
                    (UINT2) ((u2NoOfValidRoutes - RIP_HDR_LEN) / RIP_INFO_LEN);

            }

        }
    }

    return u2NoOfValidRoutes;
}

/************************************************************************
 *  *  Function Name   : RipUtilRecalculateDigest
 *  *  Description     : This function will invoke the routines to
 *  *                    calculate the message digest based on the
 *  *                    Authentication type and sends the calculated
 *  *                    message digest in the RIP update.
 *  *  Input           : pu1RipPkt - RIP packet
 *  *                    u2LenToSend - Length
 *  *                    u2AuthType - Authentication type
 *  *                    pCryptoKeyInfo - Authentication key info
 *  *  Output          : pdigest - Key Digest
 *  *  Returns         : NONE
 * ************************************************************************/
VOID
RipUtilRecalculateDigest (UINT1 *pu1RipPkt, UINT2 u2AuthType,
                          UINT2 u2LenToSend,
                          tCryptoAuthKeyInfo * pCryptoKeyInfo)
{
    INT4                i4DigestLen = 0;
    UINT2               u2KeyOffset;
    UINT1               au1CalcDigest[MAX_DIGEST_LENGTH];
    UINT2               u2TempLength = 0;
    MEMSET (au1CalcDigest, 0, MAX_DIGEST_LENGTH);

    switch (u2AuthType)
    {
        case RIPIF_MD5_AUTHENTICATION:
            i4DigestLen = RIP_MD5_DIGEST_LEN;
            break;

        case RIPIF_SHA1_AUTHENTICATION:
            i4DigestLen = RIP_SHA1_DIGEST_LEN;
            break;

        case RIPIF_SHA256_AUTHENTICATION:
            i4DigestLen = RIP_SHA256_DIGEST_LEN;
            break;

        case RIPIF_SHA384_AUTHENTICATION:
            i4DigestLen = RIP_SHA384_DIGEST_LEN;
            break;

        case RIPIF_SHA512_AUTHENTICATION:
            i4DigestLen = RIP_SHA512_DIGEST_LEN;
            break;
        default:
            break;
    }

    u2TempLength = (UINT2) (u2LenToSend - (i4DigestLen + RIP_AUTH_HDR_LEN));
    u2KeyOffset = (UINT2) (u2TempLength + RIP_OFFSET_TO_KEY);

    MEMSET (&(pu1RipPkt[u2KeyOffset]), 0, i4DigestLen);
    MEMCPY (&(pu1RipPkt[u2KeyOffset]),
            RIP_CRYPTO_AUTH_KEY (pCryptoKeyInfo), MAX_AUTH_KEY_LENGTH);

    if (u2AuthType == RIPIF_MD5_AUTHENTICATION)
    {
        Md5GetKeyDigest (pu1RipPkt, u2LenToSend, au1CalcDigest);
    }
    else
    {
        RipUtilGetShaDigest (pu1RipPkt, u2LenToSend, u2AuthType,
                             pCryptoKeyInfo, au1CalcDigest);
    }

    MEMCPY (&(pu1RipPkt[u2KeyOffset]), au1CalcDigest, i4DigestLen);
    return;
}

/************************************************************************
 *  *  Function Name   : RipDeleteIfRecord
 *  *  Description     : Deletes Interface Record Entry
 *  *                    for the  address received.
 *  *  Input           : u4IfIndex    -  Interface index
 *  *                     u4IpAddr     - Secondary IP Address.
 *  *  Output          : None
 *  *  Returns         : Returns RIP_FAILURE or RIP_SUCCESS
 * ************************************************************************/
INT4
RipDeleteIfRecord (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT4 u4Flag)
{
    /* 1. create mempool or pbuf
     * 2. check q msg struct
     * 3. fill the data
     * 4. que send
     * 5. event send*/
    tOsixMsg           *pBuf = NULL;
    UINT4               u4Size =
        sizeof (u4IfIndex) + sizeof (u4IpAddr) + sizeof (u4Flag);;
    INT4                i4OutCome = 0;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, 0);
    if (pBuf == NULL)
    {
        return RIP_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "RipDelIfRec");

    /*** copy status to offset 0 ***/
    if ((i4OutCome = CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4IfIndex, 0,
                                                sizeof (u4IfIndex))) !=
        CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }

    /*** copy map name to offset 4 ***/
    if ((i4OutCome =
         CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4IpAddr,
                                    sizeof (u4IfIndex),
                                    sizeof (u4IpAddr))) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }
  /*** copy flag  to offset 5***/
    if ((i4OutCome =
         CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Flag,
                                    (sizeof (u4IfIndex) + sizeof (u4IpAddr)),
                                    sizeof (u4Flag))) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }

    if (RipSendToQ ((void *) pBuf, RIP_DELETE_IFREC_MSG) != (UINT4) RIP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }

    return RIP_SUCCESS;
}

/************************************************************************
 *  *  Function Name   : RipDeleteIfRecord
 *  *  Description     : Deletes Interface Record Entry
 *  *                    for the  address received.
 *  *  Input           : u4IfIndex    -  Interface index
 *  *                     u4IpAddr     - Secondary IP Address.
 *  *  Output          : None
 *  *  Returns         : Returns RIP_FAILURE or RIP_SUCCESS
 * ************************************************************************/
VOID
RipHandleDeleteIfRecEvent (tProtoQMsg * pMsg)
{

    tOsixMsg           *pBuf = NULL;
    UINT4               u4IpAddr = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Flag = 0;
    UINT4               u4Network = 0;
    UINT4               u4HashIndex = 0;
    UINT1               u1RecFound = 0;
    UINT4               u4Port = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipNextIfRec = NULL;
    INT1                i1RetVal = 0;

    if (pMsg == NULL)
    {
        return;
    }

    pBuf = pMsg->msg.IfDeleteParms;
    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) &u4IfIndex, 0,
         sizeof (u4IfIndex)) != sizeof (u4IfIndex))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) &u4IpAddr, sizeof (u4IfIndex),
         sizeof (u4IpAddr)) != sizeof (u4IpAddr))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4Flag,
                                   (sizeof (u4IfIndex) + sizeof (u4IpAddr)),
                                   sizeof (u4Flag)) != sizeof (u4Flag))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    if (NetIpv4GetPortFromIfIndex ((UINT4) u4IfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        return;
    }
    if (u4Flag == RIP_DEL_ALL)
    {

        TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
        {
            TMO_HASH_DYN_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                      pRipIfRec, pRipNextIfRec, tRipIfaceRec *)
            {
                if (u4Port == (UINT2) pRipIfRec->IfaceId.u4IfIndex)
                {
                    u4Network = pRipIfRec->u4Addr;
                    rip_if_destroy (pRipIfRec);
                    i1RetVal = RipSetIfConfStatus (u4Network, DESTROY);
                }
            }
        }
        UNUSED_PARAM (i1RetVal);
        return;
    }

    else if (u4Flag == RIP_DEL)
    {
        TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
        {
            TMO_HASH_DYN_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                      pRipIfRec, pRipNextIfRec, tRipIfaceRec *)
            {
                if ((u4Port == pRipIfRec->IfaceId.u4IfIndex) &&
                    (u4IpAddr == pRipIfRec->RipIfaceCfg.u4SrcAddr))
                {
                    u1RecFound = RIP_FLAG_SET;
                    break;
                }
            }
            if (u1RecFound == RIP_FLAG_SET)
            {
                break;
            }
        }

        if (u1RecFound == RIP_FLAG_SET)
        {
            u4Network = pRipIfRec->u4Addr;
            rip_if_destroy (pRipIfRec);
            i1RetVal = RipSetIfConfStatus (u4Network, DESTROY);
            UNUSED_PARAM (i1RetVal);
            return;
        }
        return;
    }
}

/*PopulateRipCounters is test code.This function is invoked from ISS.
 *It populates the RIP counters/statistics for verifying the RIP clear counters*/

/*****************************************************************************/
/*                                                                           */
/* Function     : PopulateRipCounters                                        */
/*                                                                           */
/* Description    This function Populates the rip peerRec counters.          */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PopulateRipCounters ()
{

    UINT4               u4PeerAddr;
    UINT4               u4SeqNo = 0;
    UINT2               u2IfIndex = 0;
    UINT1               u1KeyId = 1;
    UINT1               u1Version = 0;
    UINT1               au1RtTag[2];
    tRipCxt            *pRipCxtEntry = NULL;
    tRipPeerRec        *pRipPeerRec = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT4                i4RipSetContext;

    i4RipSetContext = RipSetContext (0);
    UNUSED_PARAM (i4RipSetContext);
    pRipCxtEntry = RIP_MGMT_CXT;
    MEMSET (au1RtTag, 0, sizeof (au1RtTag));
    u2IfIndex = 0;
    u1Version = 2;
    u4PeerAddr = 0xb000001;
    RipLock ();
    pRipCxtEntry->RipNbrListCfg.u4NumPktsDropped = 5;
    pRipCxtEntry->RipGblStats.u4GlobalRouteChanges = 5;
    pRipCxtEntry->RipGblStats.u4GlobalQueries = 5;

    pRipIfRec = RipGetIfRecFromAddr (u4PeerAddr, pRipCxtEntry);
    if (NULL != pRipIfRec)
    {

        pRipIfRec->RipIfaceStats.u4InBadPackets = 5;
        pRipIfRec->RipIfaceStats.u4InBadRoutes = 5;
        pRipIfRec->RipIfaceStats.u4SentTrigUpdates = 5;
    }

    rip_update_peer_record (u2IfIndex, u4PeerAddr, au1RtTag,
                            u1Version, RIP_INCR_BAD_PACKETS, 0,
                            u4SeqNo, u1KeyId, pRipCxtEntry);

    pRipPeerRec = rip_get_peer_record (u2IfIndex, u4PeerAddr, pRipCxtEntry);
    if (NULL != pRipPeerRec)
    {
        pRipPeerRec->u4PeerRcvBadPackets = 5;
        pRipPeerRec->u4PeerRcvBadRoutes = 5;
    }
    RipUnLock ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RipCheckNetworkIsPresent                                   */
/*                                                                           */
/* Description    This function check whether the given address already      */
/*                  covered by a existing rip network                        */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RIP_SUCCESS/RIP_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/

INT4
RipCheckNetworkIsPresent (UINT4 u4IpAddr, UINT4 u4SubNet)
{
    UINT4               u4HashIndex = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    if (u4IpAddr == 0)
    {
        return RIP_FAILURE;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if ((pRipIfRec->u4Addr & u4SubNet) == u4IpAddr)
            {
                return RIP_SUCCESS;
            }
        }

    }                            /* End Of If Hash Table Scan */

    return RIP_FAILURE;
}
