/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripprrt.c,v 1.90 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains the RIP route processing  
 *             functions which manipulate the dynamic route 
 *             table maintained by RIP.                    
 *
 *******************************************************************/
#include "ripinc.h"

        /*|**********************************************************|**
         *** <<<<      local prototypes are delared here         >>>> ***
         **|**********************************************************|* */

PRIVATE UINT2       rip_stale_route_ifcounter (tRipRtEntry *, tRipCxt *);

/* Makefile Changes - no previous prototype for function */
PRIVATE INT4        RipPurgeRoutes (tRipRtEntry *, tRipIfaceRec *);
PRIVATE INT4        RipDecCount (tScanOutParams *);

        /*|**********************************************************|**
         *** <<<<     Global Variable Declarations Start Here    >>>> ***
         **|**********************************************************|* */

        /***************************************************************
        >>>>>>>>> PUBLIC Routines of this Module Start Here <<<<<<<<<<
        ***************************************************************/

/*******************************************************************************

    Function            :   rip_rt_init().

    Description         :   This initializes the singly list of the hash route
                            tables.

    Input Parameters    :   None.

    Output Parameters   :   None.

    Global Variables 
    Affected            :   gRipRtr.i1RipId, gpRipRoot, u4TotalEntryCount.
                            Both variables are initialised.

    Return Value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_rt_init
    $$TRACE_PROCEDURE_LEVEL = MAIN
*******************************************************************************/

EXPORT INT4
rip_rt_init_in_cxt (tRipCxt * pRipCxtEntry)
{
    tCreateParams       RipCreate;

    pRipCxtEntry->pRipRoot = NULL;
    RipCreate.u1KeySize = 2 * sizeof (UINT4);    /* key is address + mask */

    RipCreate.u4Type = RIP_RT_TYPE_IN_CXT (pRipCxtEntry);

    RipCreate.u1AppId = RIP_ID;
    pRipCxtEntry->i1RipId =
        (INT1) TrieCreate (&(RipCreate), &(pRipCxtEntry->pRipRoot));

    if (pRipCxtEntry->i1RipId < 0)
    {
        pRipCxtEntry->u1RipRouteDBInitialized = 0;    /* Set it */
        return (RIP_FAILURE);
    }
    else
    {
        pRipCxtEntry->u1RipRouteDBInitialized = 1;    /* Set it */
        return (RIP_SUCCESS);
    }
}

/*******************************************************************************

    Function            :   rip_add_route.

    Description         :   This function,
                            Adds an entry to ip-rip route table.
                            Finds if there is any table existing for current
                            mask group. If not a table entry is created. After
                            this the hash bucket is scanned for matching entry
                            for destination. If one is found, the modification
                            operation is performed.
                            Destination and masks are treated as indices to this
                            table.

    Input Parameters    :   1. u4DestAddr, the destination network.
                            2. pu1RouteTag, the route tag information in the
                               PDU.
                            3. u4DestMask, the (sub)net mask for the
                               destination.
                            4. u2If, the interface through which the packet has
                               arrived.
                            5. u4GwAddr, the gateway address from which the
                               destination can be reached.
                            6. u4NextHop, the next hop address through which the
                               destination can be reached with shorter cost.
                            7. u4Metric, the cost for reaching the destination.
                            8. i1RtStatus, the route status as to whether
                               direct, indirect, pending, invalid.
                            9. u1Distance, distance value for the route
                           10. pRipCxtEntry, rip context

    Output Parameters   :   None.

    Global Variables 
    Affected            :   None.

    Return Value        :   Success | Failure | Rip_database_changed |
                            Rip_database_not_changed.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_add_route
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/
EXPORT INT4
rip_add_route (UINT4 u4DestAddr, UINT1 *pu1RouteTag,
               UINT4 u4DestMask, UINT2 u2If, UINT4 u4GwAddr,
               UINT4 u4NextHop, UINT4 u4Metric, INT1 i1RtStatus,
               UINT1 u1Distance, UINT4 u4Src, tRipCxt * pRipCxtEntry)
{
    UINT1               u1Changed = FALSE;
    INT4                i4OutCome;
    INT4                i4LowMetric = 0xFFFF;
    INT4                i4count;
    INT4                i4RtNotExists = FALSE;
    UINT4               u4PrevMetric = 0;
    UINT4               u4CurBstRtMetric = 0;
    UINT4               au4Indx[2];    /* Index for Trie is madeup of 2 members */
    UINT4               u4BitMask = 0;
    UINT4               u4TrueNextHop;
    UINT4               u4CurBstRtNxtHop = 0;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    tRipRtEntry        *pTmpRt = NULL;
    tRipRtEntry        *pRt = NULL;
    tRipRtEntry        *pAvlRt = NULL;
    tRipRtEntry        *pNewBestRt = NULL;
    tRipRtEntry        *pPrevRt = NULL;
    tRipRtEntry         bestRt;
    UINT1               u1NewRoute = FALSE;
    tRipRtEntry        *pBestRt = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT1               u1DistanceChanged = FALSE;
    INT1                i1IsDC = RIP_SUCCESS;
    INT4                i4Metric1 = 0;
    CHR1               *pu1DestNetString = NULL;
    CHR1               *pu1NextHopString = NULL;

    pRipIfRec = RipGetIfRecFromAddr (u4Src, pRipCxtEntry);
    /* Get the old best route */
    pPrevRt =
        RipGetBestRouteForUpdateToRtm (u4DestAddr, u4DestMask, pRipCxtEntry);
    MEMSET (&bestRt, 0, sizeof (tRipRtEntry));
    if (pPrevRt != NULL)
    {
        MEMCPY (&bestRt, pPrevRt, sizeof (tRipRtEntry));
    }

    /* becos of ANVL FIX */
    UNUSED_PARAM (i1RtStatus);
    /* find out if entry is there in the Trie */

    /* this HTONL is required so that longest match is done correctly   
     * when addres is ANDED with mask it should be equal to the original
     * address. refer 2096 for details.
     */
    au4Indx[0] = RIP_HTONL (u4DestAddr & u4DestMask);
    au4Indx[1] = RIP_HTONL (u4DestMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    /* so that our own local, aggregated  or static routes are not added */
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    u4TrueNextHop = (u4NextHop == 0) ? u4GwAddr : u4NextHop;

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* route is present */

        for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
        {
            if ((apAppSpecInfo[i4count] != NULL) && (i4count != OTHERS_ID - 1))
            {
                break;
            }
        }
        if ((i4count == CIDR_LOCAL_ID - 1) || (i4count == CIDR_STATIC_ID - 1)
            || (i4count == MAX_ROUTING_PROTOCOLS))
        {
            return RIP_SUCCESS;
        }
        pTmpRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];

        u4CurBstRtMetric = (UINT4) pTmpRt->RtInfo.i4Metric1;
        u4CurBstRtNxtHop = pTmpRt->RtInfo.u4NextHop;

        CLI_CONVERT_IPADDR_TO_STR (pu1DestNetString, pTmpRt->RtInfo.u4DestNet);
        CLI_CONVERT_IPADDR_TO_STR (pu1NextHopString, pTmpRt->RtInfo.u4NextHop);

        RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC, RIP_NAME,
                      "Current Best Route is %s with mask %d and next hop is %s with metric %d on Interface %d\n",
                      pu1DestNetString,
                      CliGetMaskBits (pTmpRt->RtInfo.u4DestMask),
                      pu1NextHopString, pTmpRt->RtInfo.i4Metric1,
                      pTmpRt->RtInfo.u4RtIfIndx);

        /*
         * Find out if we have this route already in the database.
         * Next Hop uniquely identifies a route.
         */
        while (pTmpRt != NULL)
        {
            RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC, RIP_NAME,
                          "Alternate Route is %s with mask %d and next hop is %s with metric %d on Interface %d\n",
                          pu1DestNetString,
                          CliGetMaskBits (pTmpRt->RtInfo.u4DestMask),
                          pu1NextHopString, pTmpRt->RtInfo.i4Metric1,
                          pTmpRt->RtInfo.u4RtIfIndx);

            /* Validation for Route from the same source */
            if ((pTmpRt->RtInfo.u4NextHop == u4TrueNextHop) &&
                (pTmpRt->RtInfo.u4Gw == u4GwAddr))
            {
                i4RtNotExists = TRUE;
                /* Found the route. */
                break;
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }                        /* while */

        if (i4RtNotExists == TRUE && pTmpRt->u1Preference != u1Distance)
        {
            pTmpRt->u1Preference = u1Distance;
            pTmpRt->RtInfo.u1Distance = u1Distance;
            u1DistanceChanged = TRUE;
        }

        if ((i4RtNotExists == FALSE) && (pTmpRt != NULL) &&
            (UINT4) pTmpRt->RtInfo.i4Metric1 < u4Metric)
        {
            return RIP_SUCCESS;
        }
    }

    if (!pTmpRt)
    {

        /*
         * If it is a new node and the metric is INFINITY, we will discard this
         * entry.
         */
        if (u4Metric == RIP_INFINITY)
        {
            return (RIP_DATABASE_NOT_CHANGED);
        }

        /*
         * We need to add the route in the database, if it is new.
         * The route is new and need to be added.
         */
        RIP_ALLOC_ROUTE_ENTRY (pAvlRt);
        if (pAvlRt == NULL)
        {

            /*
             * Send traps to the net manager for further actions, if required.
             */

            RIP_SEND_TRAP_FOR_MEM_ALLOC_FAIL (RIP_MEMORY_ALLOCATION_FAILED);
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_GLOBAL_FLAG,
                     RIP_INVALID_CXT_ID,
                     ALL_FAILURE_TRC, RIP_NAME, "Error in Memory Allocation");
            return RIP_FAILURE;
        }

        /*
         * We need to initialise the default values for some of the 
         * elements in the route structure. 
         */
        RIP_INITIALIZE_ROUTE_DEFAULTS (pAvlRt);
        /* this is done so only correct route as per RFC 2096 are 
         * added 
         */
        RipRedDbNodeInit (&(pAvlRt->RtInfo.RouteDbNode), RIP_RED_DYN_RT_INFO);
        pAvlRt->RtInfo.u4DestMask = u4DestMask;
        pAvlRt->RtInfo.u4DestNet = (u4DestAddr & u4DestMask);
        pAvlRt->RtInfo.u4NextHop = u4TrueNextHop;
        pAvlRt->RtInfo.i4Metric1 = (INT4) u4Metric;
        pAvlRt->RtInfo.i4CxtId = pRipCxtEntry->i4CxtId;
        pAvlRt->RtInfo.u4RtIfIndx = u2If;
        pAvlRt->RtInfo.u2RtProto = (UINT2) RIP_ID;
        pAvlRt->RtInfo.u4TempDestMask = pAvlRt->RtInfo.u4DestMask;
        pAvlRt->RtInfo.u4TempDestNet = pAvlRt->RtInfo.u4DestNet;
        pAvlRt->RtInfo.u1Distance = u1Distance;
        if (pRipIfRec != NULL)
        {
            pAvlRt->RtInfo.u4SrcAddr = pRipIfRec->RipIfaceCfg.u4SrcAddr;
        }
        pAvlRt->u1Preference = u1Distance;

        if (u4Metric != RIP_INFINITY)
        {
            pAvlRt->RtInfo.u4RowStatus = IPFWD_ACTIVE;
        }

        if (rip_add_new_route (pAvlRt, pRipCxtEntry) == RIP_FAILURE)
        {
            RIP_TRC_ARG1 (RIP_MOD_TRC,
                          pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          ALL_FAILURE_TRC, RIP_NAME,
                          "FAILED while adding new route to route database for an index %d.\n",
                          (pAvlRt->RtInfo.u4RtIfIndx));
            RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pAvlRt)));
            RIP_ROUTE_FREE (pAvlRt);
            return RIP_FAILURE;
        }
#ifndef RRD_WANTED
        else
        {
            u1NewRoute = TRUE;
        }
#endif /* RRD_WANTED */
        /* ANVL  FIX */
        u1Changed = TRUE;

        if (RipIsActiveInAnyInterface (pRipCxtEntry) == RIP_SUCCESS)
        {
            RIP_RT_SET_CHANGED_STATUS (pAvlRt);
        }
        /* ANVL FIX */

    }                            /* End of if (!pTmpRt) */
    else if (u4Metric == RIP_INFINITY &&
             pTmpRt->RtInfo.i4Metric1 == RIP_INFINITY)
    {
        /* ignore the update if already the route is marked for deletion 
         * and the update is with metric INFINITY for the same route 
         */
        return (RIP_DATABASE_NOT_CHANGED);
    }
    else
    {
        pAvlRt = pTmpRt;
        u4PrevMetric = (UINT4) pAvlRt->RtInfo.i4Metric1;
    }

    /* updating the bit mask to inform other protocols that some important 
     * parameters have changed.
     */

    if (!pTmpRt)
    {
        /* set all the bit as its a new route */
        u4BitMask = IP_BIT_ALL;
    }
    else
    {
        /*
         * We gotta modify the existing node as seems to be fit enough and
         * we might as well delink the age timer for this route to reinstall
         * it again. This should be done here as we need the information of how
         * long the route has been alive for the purpose of ifCounter
         * calculations.
         */
        if (pRipIfRec != NULL)
        {
            i1IsDC =
                RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
            if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE)
                || (i1IsDC == RIP_SUCCESS))
            {
                RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pTmpRt)));
            }
        }
        if ((UINT4) u2If != pAvlRt->RtInfo.u4RtIfIndx)
        {
            /* interface is different */
            SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_INTRFAC);
        }

        if ((UINT4) pAvlRt->RtInfo.i4Metric1 != u4Metric)
        {
            /* metric is not matching */
            SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_METRIC);
            /* metric is not matching and we are changing the state */
            if ((pAvlRt->RtInfo.i4Metric1 == RIP_INFINITY) ||
                (u4Metric == RIP_INFINITY))
            {
                SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_STATUS);
            }
        }
    }

    pAvlRt->RtInfo.u4RtIfIndx = (UINT4) u2If;
    COPY_RT_TAG (pAvlRt, pu1RouteTag);
    pAvlRt->RtInfo.u4RtNxtHopAS =
        (UINT4) RIP_NTOHS (pAvlRt->RtInfo.u4RtNxtHopAS);

    OsixGetSysTime (&pAvlRt->RtInfo.u4ChgTime);

    pAvlRt->RtInfo.u4Gw = u4GwAddr;
    pAvlRt->RtInfo.u4NextHop = u4TrueNextHop;

    /*
     * Calculate for ifcounter value for the route. This is done here, since we
     * will be changing the values of the variables for the
     * existing/non-existing route hereafter.
     */
    if (pRipIfRec != NULL)
    {
        i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
        if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE)
            || (i1IsDC == RIP_SUCCESS))
        {
            /* for wan circuits SHwPR should always be applied */
            rip_calculate_ifcounter_for_route (u2If, pTmpRt, pAvlRt, u4Metric);
        }
    }
    /*
     * The timer structure for RouteAge calculation that need to be initialised.
     */

    RIP_ROUTE_TIMER_ROUTE (pAvlRt) = (VOID *) pAvlRt;
    RIP_ROUTE_TIMER_ID (pAvlRt) = (UINT1) RIP_RT_TIMER_ID;

    /*
     * Updating RIP route, take care of timers associated with it
     */

    if (u4Metric == RIP_INFINITY)
    {

        /* If this is the case, the node would have been already existing and
         * it's metric is getting changed */

        /*
         * Deletions can occur for one of two reasons:
         * (1) the timer expires, OR
         * (2) the metric is set to 16 because of an update received from the
         *     current gateway.
         * The deletion process starts by setting route timer to garbage
         * collection interval.
         */

        pAvlRt->RtInfo.u1Status = (UINT1) RIP_RT_PENDING;
        if (pRipIfRec != NULL)
        {
            if ((pAvlRt->RtInfo.u2RtProto != CIDR_LOCAL_ID) &&
                (pAvlRt->RtInfo.u2RtProto != OTHERS_ID))
            {
                if (TMO_DLL_Is_Node_In_List
                    ((tTMO_DLL_NODE *) & pAvlRt->RipIfNode) == TRUE)
                {
                    TMO_DLL_Delete (&(pRipIfRec->RipIfRtList),
                                    (tTMO_DLL_NODE *) & pAvlRt->RipIfNode);
                }

                RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC,
                              RIP_NAME,
                              "Route %x %x %x %d is deleted from IfRtList %d\n",
                              pAvlRt->RtInfo.u4DestNet,
                              pAvlRt->RtInfo.u4DestMask,
                              pAvlRt->RtInfo.u4NextHop,
                              pAvlRt->RtInfo.i4Metric1, u2If);
            }
            RIP_START_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pAvlRt)),
                             pRipIfRec->RipIfaceCfg.
                             u2GarbageCollectionInterval);
        }

        pAvlRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;
    }
    else
    {

        /*
         * The timeout is initialized when a route is established, and any time
         * an update message is received for the route.
         *
         * Should a new route to this network be established while the garbage-
         * collection timer is running, the new route will replace the one that
         * is about to be deleted.  In this case the garbage-collection timer
         * must be cleared.
         * In both the cases we will come here.
         */

      /**** $$TRACE_LOG (INTMD, "Starting Route age timer - %x \n",
        pAvlRt); ****/
        if (pRipIfRec != NULL)
        {
            i1IsDC =
                RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
            if ((pRipIfRec->u1Persistence != RIP_WAN_TYPE)
                || (i1IsDC == RIP_SUCCESS))

            {
                RIP_START_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pAvlRt)),
                                 pRipIfRec->RipIfaceCfg.u2RouteAgeInterval);
            }
        }

        /* ok, now i can make the row status active for new routes */
        /* old routes already had row status active */
        pAvlRt->RtInfo.u4RowStatus = IPFWD_ACTIVE;

    }

    /* Makefile Changes - comparison btwn signed and unsigned */
    if ((UINT4) pAvlRt->RtInfo.i4Metric1 != u4Metric)
    {

        pAvlRt->RtInfo.i4Metric1 = (INT4) u4Metric;

        if (pTmpRt != NULL)
        {
            InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
            InParams.i1AppId = (INT1) (pTmpRt->RtInfo.u2RtProto - 1);
            InParams.pRoot = pRipCxtEntry->pRipRoot;

            i4OutCome = TrieUpdate (&(InParams), pTmpRt, u4Metric, &OutParams);

            if (i4OutCome == TRIE_SUCCESS)
            {
                if (pTmpRt->RtInfo.u2RtProto == RIP_ID)
                {
                    pTmpRt->RtInfo.u1Operation = RIPHA_ADD_ROUTE;
                    RipRedDbUtilAddRtInfo (pTmpRt);
                    RipRedSyncDynInfo ();
                }
                au4Indx[0] = RIP_HTONL (u4DestAddr & u4DestMask);
                au4Indx[1] = RIP_HTONL (u4DestMask);
                InParams.pRoot = pRipCxtEntry->pRipRoot;
                InParams.i1AppId = ALL_ROUTING_PROTOCOL;
                InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

                OutParams.Key.pKey = NULL;
                MEMSET (apAppSpecInfo, 0,
                        MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
                OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
                OutParams.pObject1 = NULL;
                OutParams.pObject2 = NULL;

                i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

                if (i4OutCome == TRIE_SUCCESS)
                {
                    for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS;
                         i4count++)
                    {
                        if ((apAppSpecInfo[i4count] != NULL)
                            && (i4count != OTHERS_ID - 1))
                        {
                            if ((i4count == CIDR_LOCAL_ID - 1) ||
                                (i4count == CIDR_STATIC_ID - 1))
                            {
                                return RIP_SUCCESS;
                            }
                            pBestRt =
                                ((tRipRtEntry **) OutParams.
                                 pAppSpecInfo)[i4count];
                            if (i4LowMetric > pBestRt->RtInfo.i4Metric1)
                            {
                                i4LowMetric = pBestRt->RtInfo.i4Metric1;
                                pNewBestRt = pBestRt;
                            }
                        }
                    }

                    if (pNewBestRt == NULL)
                    {
                        return RIP_FAILURE;
                    }

                    /* DLL needs to be updated and Trigger update needs to be sent
                     * only for changes in the best route.
                     * Best route changes occurs when
                     *   (i) when  a new route becomes the best route. It is handled in rip_add_new_route.
                     *  (ii) when a non best route becomes a new best route.
                     * (iii) when a best route metric changes.
                     * */
                    if (u4CurBstRtNxtHop != pNewBestRt->RtInfo.u4NextHop ||
                        (u4CurBstRtNxtHop == pNewBestRt->RtInfo.u4NextHop &&
                         u4CurBstRtMetric !=
                         (UINT4) pNewBestRt->RtInfo.i4Metric1))
                    {
                        RipUpdateRouteEntry (pNewBestRt, pRipCxtEntry);
                        u1Changed = TRUE;

                        RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                                      pRipCxtEntry->i4CxtId,
                                      CONTROL_PLANE_TRC, RIP_NAME,
                                      "New Best Route is %x %x %x %d on"
                                      "Interface %d\n",
                                      pNewBestRt->RtInfo.u4DestNet,
                                      pNewBestRt->RtInfo.u4DestMask,
                                      pNewBestRt->RtInfo.u4NextHop,
                                      pNewBestRt->RtInfo.i4Metric1,
                                      pNewBestRt->RtInfo.u4RtIfIndx);
                    }

                    /* a check needed before updating DLL, Because trie
                     * may update an alternate path
                     * Such a Trie alternate path update need not update the
                     * Context DLL*/

                    if (pRipIfRec != NULL)
                    {
                        if ((pNewBestRt->RtInfo.u2RtProto != CIDR_LOCAL_ID) &&
                            (pNewBestRt->RtInfo.u2RtProto != OTHERS_ID))
                        {
                            /* If Previous metric of route in TRIE is 16,
                             * the route needs to be added in interface DLL.
                             * Else the route will be present in interface DLL
                             * The route is deleted form DLL only when its metric
                             * changes to 16 */
                            if (u4PrevMetric == RIP_INFINITY)
                            {
                                TMO_DLL_Init_Node ((tTMO_DLL_NODE *)
                                                   & pAvlRt->RipIfNode);
                                if (TMO_DLL_Is_Node_In_List
                                    ((tTMO_DLL_NODE *) & pAvlRt->RipIfNode) ==
                                    TRUE)
                                {
                                    return RIP_FAILURE;
                                }

                                TMO_DLL_Add (&(pRipIfRec->RipIfRtList),
                                             (tTMO_DLL_NODE *)
                                             & pAvlRt->RipIfNode);

                                RIP_TRC_ARG5 (RIP_MOD_TRC,
                                              pRipCxtEntry->u4RipTrcFlag,
                                              pRipCxtEntry->i4CxtId,
                                              CONTROL_PLANE_TRC, RIP_NAME,
                                              "Route %x %x %x %d is added to "
                                              "IfRtList %d\n",
                                              pAvlRt->RtInfo.u4DestNet,
                                              pAvlRt->RtInfo.u4DestMask,
                                              pAvlRt->RtInfo.u4NextHop,
                                              pAvlRt->RtInfo.i4Metric1, u2If);
                            }
                        }

                        if (OutParams.pObject1 != NULL)
                        {
                            RipDeleteLeafFromList (OutParams.pObject1,
                                                   (BOOLEAN) OutParams.
                                                   u1LeafFlag, pRipIfRec,
                                                   pRipCxtEntry);
                        }
                        else if (OutParams.pObject2 != NULL)
                        {
                            RipMoveLeafToListEnd (OutParams.pObject2, pRipIfRec,
                                                  pRipCxtEntry);
                        }
                    }
                    OutParams.pObject1 = NULL;
                    OutParams.pObject2 = NULL;
                }
            }
        }

        /*
         * We should set the Changed flag only when it is possible to send
         * triggered updates. But updates can be sent only when RIP is enabled.
         * Therefore should set the status only when RIP is enabled.
         */
        if ((RipIsActiveInAnyInterface (pRipCxtEntry) == RIP_SUCCESS) &&
            (pNewBestRt != NULL))
        {
            RIP_RT_SET_CHANGED_STATUS (pNewBestRt);
        }
    }

    pNewBestRt =
        RipGetBestRouteForUpdateToRtm (u4DestAddr, u4DestMask, pRipCxtEntry);

    /* better inform the other protocols first */
    if (u4BitMask != 0 || u1DistanceChanged == TRUE)
    {
        if (u1NewRoute == FALSE)
        {
            /* BitMask must be AND'ed with the IP_BIT_STATUS rather than 
             * a direct check.The IP_BIT_STATUS will also be set if the
             * route in the IP forwarding table had the same route but
             * the metric set to 16 (RIP_INFINITY) but a new update
             * arrived with a lower metric (Valid Rip route). Hence
             * a check need to added to see if the IP_BIT_STATUS is set and the
             * new metric is other than RIP_INFINITY, in this case modify 
             * the route in the IP forwarding table.*/
            if ((u4BitMask & IP_BIT_STATUS) && (u4Metric == RIP_INFINITY))
            {
                if (pAvlRt->u1RouteStatusInRtm == OSIX_TRUE)
                {
                    RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry,
                                                            pAvlRt);
                    RipAddNewBestRoutesToRtm (pAvlRt, pRipCxtEntry);
                }
            }
            else
            {
                if (pPrevRt == NULL)
                {
                    RipAddRouteToForwardingTableInCxt (pRipCxtEntry, pAvlRt);
                    pAvlRt->u1RouteStatusInRtm = OSIX_TRUE;
                }
                else
                {
                    if (bestRt.RtInfo.u4NextHop == pAvlRt->RtInfo.u4NextHop)
                    {
                        if ((bestRt.RtInfo.i4Metric1 ==
                             pAvlRt->RtInfo.i4Metric1)
                            && (u1DistanceChanged == OSIX_TRUE))
                        {
                            /* If the preferance is changed in RIP, it has to 
                               be notified to RTM */
                            RipModifyRouteToForwardingTableInCxt (pRipCxtEntry,
                                                                  pAvlRt);
                        }
                        else if (bestRt.RtInfo.i4Metric1 >
                                 pAvlRt->RtInfo.i4Metric1)
                        {
                            /* Modifying the best route which is a new route - START */
                            RipModifyRouteToForwardingTableInCxt (pRipCxtEntry,
                                                                  pAvlRt);
                            pAvlRt->u1RouteStatusInRtm = OSIX_TRUE;
                            /* Adding the best route which is a new route - END */

                            /* Delete all the ECFM routes of the old metric - 
                             * bestRt.RtInfo.i4Metric1
                             */
                            /* START */
                            i4Metric1 = bestRt.RtInfo.i4Metric1;
                            pRt = pNewBestRt;
                            while (pRt != NULL)
                            {
                                pTmpRt = pRt->pNextAlternatepath;

                                if (pRt->RtInfo.i4Metric1 == i4Metric1)
                                {
                                    RipDeleteRouteFromForwardingTableInCxt
                                        (pRipCxtEntry, pRt);
                                    pRt->u1RouteStatusInRtm = OSIX_FALSE;

                                }
                                pRt = pTmpRt;

                            }
                            /* END */
                        }
                        else if ((pAvlRt != NULL) && (pAvlRt == pPrevRt)
                                 && (pAvlRt->u1RouteStatusInRtm == OSIX_TRUE))
                        {
                            /* Modifying the existing route */
                            RipModifyRouteToForwardingTableInCxt (pRipCxtEntry,
                                                                  pAvlRt);
                        }
                        else    /* bestRt.RtInfo.i4Metric1 < pAvlRt->RtInfo.i4Metric */
                        {
                            /*If metric value of one of the ECMP route is greater than 
                               best route, delete it from RTM */
                            if (pAvlRt->u1RouteStatusInRtm == OSIX_TRUE)
                            {
                                RipDeleteRouteFromForwardingTableInCxt
                                    (pRipCxtEntry, pAvlRt);
                                pAvlRt->u1RouteStatusInRtm = OSIX_FALSE;
                            }
                        }
                    }
                    else        /* bestRt.RtInfo.u4NextHop != pAvlRt->RtInfo.u4NextHop */
                    {
                        if (bestRt.RtInfo.i4Metric1 == pAvlRt->RtInfo.i4Metric1)
                        {
                            /* This should be for ECMP */
                            RipAddRouteToForwardingTableInCxt (pRipCxtEntry,
                                                               pAvlRt);
                            pAvlRt->u1RouteStatusInRtm = OSIX_TRUE;
                        }
                        else if (bestRt.RtInfo.i4Metric1 >
                                 pAvlRt->RtInfo.i4Metric1)
                        {

                            /* Delete all the ECFM routes of the old metric - 
                             * bestRt.RtInfo.i4Metric1
                             */
                            /* START */
                            i4Metric1 = bestRt.RtInfo.i4Metric1;
                            pRt = pNewBestRt;
                            while (pRt != NULL)
                            {
                                pTmpRt = pRt->pNextAlternatepath;

                                if (pRt->RtInfo.i4Metric1 == i4Metric1)
                                {

                                    RipDeleteRouteFromForwardingTableInCxt
                                        (pRipCxtEntry, pRt);
                                    pRt->u1RouteStatusInRtm = OSIX_FALSE;

                                }
                                pRt = pTmpRt;
                            }
                            /* END */

                            /* Adding the best route which is a new route - START */
                            RipModifyRouteToForwardingTableInCxt (pRipCxtEntry,
                                                                  pAvlRt);
                            pAvlRt->u1RouteStatusInRtm = OSIX_TRUE;
                            /* Deleting the best route which is a new route - END */
                        }
                        else    /* bestRt.RtInfo.i4Metric1 < pAvlRt->RtInfo.i4Metric */
                        {
                            /*If metric value of one of the ECMP route is greater than 
                               best route, delete it from RTM */
                            if (pAvlRt->u1RouteStatusInRtm == OSIX_TRUE)
                            {
                                RipDeleteRouteFromForwardingTableInCxt
                                    (pRipCxtEntry, pAvlRt);
                                pAvlRt->u1RouteStatusInRtm = OSIX_FALSE;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (RipAddRouteToForwardingTableInCxt (pRipCxtEntry, pAvlRt) ==
                RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC,
                         RIP_NAME,
                         "rip_add_route: FATAL_ERROR : Failed to add route"
                         "in Forwarding Table \n");
            }
            pAvlRt->u1RouteStatusInRtm = OSIX_TRUE;
        }
    }

    if (u1Changed == TRUE)
    {
        return (RIP_TRIGGER_UPDATE);
    }
    else
    {
#ifdef L2RED_WANTED
        pAvlRt->u1RouteStatusInRtm = OSIX_TRUE;
        pAvlRt->RtInfo.u1Operation = RIPHA_ADD_ROUTE;
        RipRedDbUtilAddRtInfo (pAvlRt);
        RipRedSyncDynInfo ();

#endif
        return (RIP_SUCCESS);
    }
}

/*******************************************************************************

    Function            :   rip_calculate_ifcounter_for_route.

    Description         :   This function,
                            calculates the ifcounter which is associated with
                            each destination, according to the rules mentioned
                            in rfc1812 for limiting poisoned reverse. During
                            calculation, it refers the configured variable to
                            see whether which split horizon operation is in
                            effect.

    Input Parameters    :   1. u2If, the interface through which the new route
                               has been learnt. 
                            2. pAvlRt, the already existing route, will be a
                               valid one, if available.
                            3. pNewRt, the newly added route.
                            4. u4NewMetric, the metric value for the new route.

    Output Parameters   :   Both, pAvlRt, pNewRt are affected with their
                            ifcounter value change.

    Global Variables 
    Affected            :   u2IfCounter, value for the route.

    Return Value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_calculate_ifcounter_for_route
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_calculate_ifcounter_for_route (UINT2 u2If, tRipRtEntry * pAvlRt,
                                   tRipRtEntry * pNewRt, UINT4 u4NewMetric)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;

    pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2If);
    if (pRipCxtEntry == NULL)
    {
        return;
    }

    pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);

    if (pAvlRt)
    {                            /* Refer rfc1812      */
        if ((pRipIfRec != NULL)
            && (pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus ==
                RIP_SPLIT_HORZ_WITH_POIS_REV))
        {

            /*
             * If the new route is superseding a valid route, and the old route
             * used a different output interface, then the ifcounter is set to
             * RIP_UPDATE_LOSS.
             */
            /* Makefile Changes - comparison btwn signed and unsigned */
            if ((UINT4) pAvlRt->RtInfo.i4Metric1 >= u4NewMetric &&
                (pAvlRt->RtInfo.u1Status != ((UINT1) RIP_RT_PENDING)) &&
                ((UINT2) (pAvlRt->RtInfo.u4RtIfIndx) != u2If))
            {

                pAvlRt->RtInfo.u2IfCounter = RIP_UPDATE_LOSS;
            }

            /*
             * If the new route is superseding a stale route, and the old route
             * used a different output interface, then the ifcounter to set to
             * MAX (0, RIP_UPDATE_LOSS - INT(seconds that the route has been
             * stale/update timer)
             */
            /* Makefile Changes - comparison btwn signed and unsigned */
            else if ((UINT4) pAvlRt->RtInfo.i4Metric1 >= u4NewMetric &&
                     (pAvlRt->RtInfo.u1Status == ((UINT1) RIP_RT_PENDING)) &&
                     ((UINT2) (pAvlRt->RtInfo.u4RtIfIndx) != u2If))
            {

                pAvlRt->RtInfo.u2IfCounter =
                    rip_stale_route_ifcounter (pAvlRt, pRipCxtEntry);
            }

            else
            {                    /* Otherwise, it is 0 */

                pAvlRt->RtInfo.u2IfCounter = 0;
            }
        }

        else
        {

            pAvlRt->RtInfo.u2IfCounter = 0;
        }

    }                            /* End of if (pAvlRt) */

    else
    {
        if ((pRipIfRec != NULL)
            && (pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus ==
                RIP_SPLIT_HORZ_WITH_POIS_REV))
        {
            /* This is going to be a new route */

            pNewRt->RtInfo.u2IfCounter = RIP_NEW_DESTN_IFCOUNTER;
        }

        else
        {

            pNewRt->RtInfo.u2IfCounter = 0;
        }
    }

}

/*******************************************************************************

    Function            :   rip_rt_timeout_handler.
    
    Description         :   This is called after an expiry of route timer. It
                            may pertain to any case, when an active route
                            expires out or the route has been in the garbage
                            collection interval.
                            For age timer expiry,
                               The garbage-collection timer is set for 120
                               seconds.
                               The metric for the route is set to INFINITY.
                               A triggered update is sent over all the
                               interfaces.
                            For garbage-collection timer expiry,
                               The route is removed from the database.
    
    Input parameters    :   1. pCurRt, a pointer to route information structure.
                            2. pRipCxtEntry: Pointer to the context Info         
    
    Output parameters   :   None.
    
    Global variables
    Affected            :   Route information structure, and it's timer nodes
                            are getting affected.

    Return value        :   Success/Rip_trigger_update.

*******************************************************************************/
EXPORT INT4
rip_rt_timeout_handler (VOID *pRt, tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome;
    INT4                i4Status = RIP_SUCCESS;
    UINT4               au4Indx[2];    /* Index of Trie is made-up of 
                                       address and mask */
    UINT4               u4BitMask = 0;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtEntry        *pCurRt = (tRipRtEntry *) pRt;
    UINT2               u2GarbageCollectTimer;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipRtEntry        *pTempRipRtEntry = NULL;
    UINT1               u1RipNetworkEnabled = OSIX_FALSE;
    CHR1               *pu1NetAddress = NULL;
    CHR1               *pu1NxtHopAddress = NULL;
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    UINT1               au1NxtHopAddress[MAX_ADDR_LEN];

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (au1NxtHopAddress, 0, MAX_ADDR_LEN);

    pu1NetAddress = (CHR1 *) & au1NetAddress[0];
    pu1NxtHopAddress = (CHR1 *) & au1NxtHopAddress[0];

    if (pCurRt->RtInfo.u4SrcAddr != RIP_ZERO)
    {
        pRipIfRec =
            RipGetIfRecFromAddr (pCurRt->RtInfo.u4SrcAddr, pRipCxtEntry);
        u1RipNetworkEnabled = OSIX_TRUE;
    }
    else
    {
        pRipIfRec = RipGetIfRec (pCurRt->RtInfo.u4RtIfIndx, pRipCxtEntry);
    }

/*    pRipIfRec = RipGetIfRec (pCurRt->RtInfo.u4RtIfIndx, pRipCxtEntry);*/
    if (pCurRt->RtInfo.i4Metric1 != RIP_INFINITY)
    {
        /*
         * Active route aging out:
         * The timeout expires,
         * The deletion process starts by setting route timer to garbage
         * collection interval.
         */

        CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress, pCurRt->RtInfo.u4DestNet);
        MEMCPY (au1NetAddress, pu1NetAddress, MAX_ADDR_LEN);

        RIP_TRC_ARG2 (RIP_MOD_TRC,
                      pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId,
                      CONTROL_PLANE_TRC, RIP_NAME,
                      "Putting the route Dest = %s and GW = %x "
                      "in Garbage collection interval \n",
                      au1NetAddress, pCurRt->RtInfo.u4Gw);
        MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);

        pCurRt->RtInfo.u1Status = (UINT1) RIP_RT_PENDING;
        pCurRt->RtInfo.i4Metric1 = RIP_INFINITY;

        au4Indx[0] = RIP_HTONL (pCurRt->RtInfo.u4DestNet);
        au4Indx[1] = RIP_HTONL (pCurRt->RtInfo.u4DestMask);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = (INT1) (pCurRt->RtInfo.u2RtProto - 1);

        OutParams.pObject1 = NULL;
        OutParams.pObject2 = NULL;

        i4OutCome = TrieUpdate (&(InParams), pCurRt, RIP_INFINITY, &OutParams);

        if (i4OutCome == TRIE_SUCCESS)
        {
            if (pCurRt->RtInfo.u2RtProto == RIP_ID)
            {
                pCurRt->RtInfo.u1Operation = RIPHA_ADD_ROUTE;
                RipRedDbUtilAddRtInfo (pCurRt);
                RipRedSyncDynInfo ();
            }
            RipUpdateRouteEntry (pCurRt, pRipCxtEntry);
            if ((pRipIfRec != NULL) &&
                (pCurRt->RtInfo.u2RtProto != CIDR_LOCAL_ID) &&
                (pCurRt->RtInfo.u2RtProto != OTHERS_ID))
            {
                if (TMO_DLL_Is_Node_In_List
                    ((tTMO_DLL_NODE *) & pCurRt->RipIfNode) == TRUE)
                {
                    TMO_DLL_Delete (&(pRipIfRec->RipIfRtList),
                                    (tTMO_DLL_NODE *) & pCurRt->RipIfNode);
                }

                CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                           pCurRt->RtInfo.u4DestNet);
                MEMCPY (au1NetAddress, pu1NetAddress, MAX_ADDR_LEN);
                CLI_CONVERT_IPADDR_TO_STR (pu1NxtHopAddress,
                                           pCurRt->RtInfo.u4NextHop);
                MEMCPY (au1NxtHopAddress, pu1NxtHopAddress, MAX_ADDR_LEN);
                RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              RIP_CRITICAL_TRC, RIP_NAME,
                              "Route %s %d %s %d is deleted from IfRtList %d\n",
                              au1NetAddress,
                              CliGetMaskBits (pCurRt->RtInfo.u4DestMask),
                              au1NxtHopAddress,
                              pCurRt->RtInfo.i4Metric1,
                              pCurRt->RtInfo.u4RtIfIndx);

                MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
                MEMSET (au1NxtHopAddress, 0, MAX_ADDR_LEN);

            }
            if (OutParams.pObject1 != NULL)
            {
                RipDeleteLeafFromList (OutParams.pObject1,
                                       (BOOLEAN) OutParams.u1LeafFlag,
                                       pRipIfRec, pRipCxtEntry);
            }
            else if (OutParams.pObject2 != NULL)
            {
                RipMoveLeafToListEnd (OutParams.pObject2, pRipIfRec,
                                      pRipCxtEntry);
            }
            OutParams.pObject1 = NULL;
            OutParams.pObject2 = NULL;

        }
        if (RipIsActiveInAnyInterface (pRipCxtEntry) == RIP_SUCCESS)
        {

            RIP_RT_SET_CHANGED_STATUS (pCurRt);
            pCurRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;
            i4Status = RIP_TRIGGER_UPDATE;
        }
        /* Get the Interface Record from Hash Table */

        if (pRipIfRec == NULL)
        {
            /*Interface Record Not Found */
            i4OutCome = RIP_FAILURE;
            return i4OutCome;
        }
        u2GarbageCollectTimer =
            pRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval;
        RIP_START_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pCurRt)),
                         u2GarbageCollectTimer);
        SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_METRIC);
        SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_STATUS);

        /* Delete it from forwarding table */
        if (pCurRt->u1RouteStatusInRtm == OSIX_TRUE)
        {
            RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry, pCurRt);
            RipAddNewBestRoutesToRtm (pCurRt, pRipCxtEntry);
        }
    }
    else
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress, pCurRt->RtInfo.u4DestNet);
        MEMCPY (au1NetAddress, pu1NetAddress, MAX_ADDR_LEN);

        RIP_TRC_ARG2 (RIP_MOD_TRC,
                      pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId,
                      CONTROL_PLANE_TRC, RIP_NAME,
                      "Deleting the route Dest = %s and GW = %x \n",
                      au1NetAddress, pCurRt->RtInfo.u4Gw);

        MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
        /*
         * Already marked as deleted node, We must have got garbage collection
         * timeout.
         */

        /* Stoping the timer which is already fired is not necessary. 
         * Can be removed Later 
         */

        RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pCurRt)));

        /* Delete it from forwarding table */
        RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry, pCurRt);
        RipUpdateIfaceNextRtSend (pRipCxtEntry, pCurRt);
        /* Delete the route now */
        au4Indx[0] = RIP_HTONL (pCurRt->RtInfo.u4DestNet);
        au4Indx[1] = RIP_HTONL (pCurRt->RtInfo.u4DestMask);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = (INT1) (pCurRt->RtInfo.u2RtProto - 1);

        OutParams.pAppSpecInfo = NULL;
        OutParams.Key.pKey = NULL;
        OutParams.pObject1 = NULL;
        OutParams.pObject2 = NULL;

        i4OutCome = TrieDeleteEntry (&(InParams), &(OutParams),
                                     pCurRt->RtInfo.u4NextHop);

        if (i4OutCome == TRIE_SUCCESS)
        {
            if ((pCurRt->RtInfo.u2RtProto == RIP_ID) ||
                (pCurRt->RtInfo.u2RtProto == CIDR_LOCAL_ID))
            {
                pCurRt->RtInfo.u1Operation = RIPHA_DEL_ROUTE;
                RipRedDbUtilAddRtInfo (pCurRt);
                RipRedSyncDynInfo ();
            }
            if (OutParams.pObject1 != NULL)
            {
                RipMoveLeafToListEnd (OutParams.pObject1, pRipIfRec,
                                      pRipCxtEntry);
            }
            else if (OutParams.pObject2 != NULL)
            {
                RipDeleteLeafFromList (OutParams.pObject2,
                                       (BOOLEAN) OutParams.u1LeafFlag,
                                       pRipIfRec, pRipCxtEntry);
            }
            TMO_DLL_Delete (&(pRipCxtEntry->RipCxtRtList),
                            (tTMO_DLL_NODE *) & pCurRt->RipCxtRtNode);

            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress, pCurRt->RtInfo.u4DestNet);
            MEMCPY (au1NetAddress, pu1NetAddress, MAX_ADDR_LEN);
            CLI_CONVERT_IPADDR_TO_STR (pu1NxtHopAddress,
                                       pCurRt->RtInfo.u4NextHop);
            MEMCPY (au1NxtHopAddress, pu1NxtHopAddress, MAX_ADDR_LEN);

            RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId,
                          RIP_CRITICAL_TRC, RIP_NAME,
                          "Route %s %d %s %d is deleted from RipCxtRtList\n",
                          au1NetAddress,
                          CliGetMaskBits (pCurRt->RtInfo.u4DestMask),
                          au1NxtHopAddress, pCurRt->RtInfo.i4Metric1);

            MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
            MEMSET (au1NxtHopAddress, 0, MAX_ADDR_LEN);

            if (pCurRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                pRipCxtEntry->u4TotalSummaryCount--;
                pTempRipRtEntry = (tRipRtEntry *) (OutParams.pAppSpecInfo);
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(RIP_ROUTE_TIMER_NODE (pTempRipRtEntry)));
                RIP_ROUTE_FREE (OutParams.pAppSpecInfo);
            }
            else if ((u1RipNetworkEnabled == OSIX_TRUE)
                     && (pCurRt->RtInfo.u2RtProto == CIDR_LOCAL_ID))
            {
                pRipCxtEntry->u4TotalEntryCount--;
                pTempRipRtEntry = (tRipRtEntry *) (OutParams.pAppSpecInfo);
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(RIP_ROUTE_TIMER_NODE (pTempRipRtEntry)));
                RIP_FREE_LOCAL_ROUTE_ENTRY (pCurRt);
                return RIP_SUCCESS;
            }
            else
            {
                pRipCxtEntry->u4TotalEntryCount--;
                pTempRipRtEntry = (tRipRtEntry *) (OutParams.pAppSpecInfo);
                RIP_STOP_TIMER (RIP_TIMER_ID,
                                &(RIP_ROUTE_TIMER_NODE (pTempRipRtEntry)));
                RIP_ROUTE_FREE (OutParams.pAppSpecInfo);
            }

        }
    }

    return (i4Status);
}

    /*******************************************************************************

        Function            : RipDecCount
        
        Description         : This function decrements the u2IfCounter for each
                  and every route inside the routing table.    
        
        Input parameters    : Pointer to the tScanOutParams structure.
        
        Output parameters   : None.           
        
        Global variables
        Affected            : Route information structure in the route database,
                  and it's non-zero ifcounters are affected.

        Return value        : RIP_SUCCESS.                    
        
    *******************************************************************************/
    /*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = RipDecCount
        $$TRACE_PROCEDURE_LEVEL = LOW
    *******************************************************************************/

INT4
RipDecCount (pOut)
     tScanOutParams     *pOut;
{
    UINT4               u4Entry;
    tRipRtEntry        *pRt = NULL;

    for (u4Entry = 0; u4Entry < pOut->u4NumEntries; u4Entry++)
    {
        pRt = (((tRipRtEntry **) pOut->pAppSpecInfo)[u4Entry]);
        if (pRt->RtInfo.u2IfCounter != 0)
        {

            pRt->RtInfo.u2IfCounter--;
        }
    }
    return (RIP_SUCCESS);
}

    /*******************************************************************************

        Function            :   rip_decrement_ifcounter_for_routes.
        
        Description         :   This procedure is called after sending regular
                    update successfully. This scans through the entire
                    route table and decrements the ifCounter for all the
                    routes, if the already existing ifCounter is not
                    zero.
                    Only, Dynamic route table is scanned and static
                    route table is not scanned because we already know
                    it's ifcounters are always zero.
        
        Input parameters    :   pRt, a pointer to route information structure.
        
        Output parameters   :   None.
        
        Global variables
        Affected            :   None. 

        Return value        :   None.
        
    *******************************************************************************/

    /*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = rip_decrement_ifcounter_for_routes
        $$TRACE_PROCEDURE_LEVEL = LOW
    *******************************************************************************/

EXPORT VOID
rip_decrement_ifcounter_for_routes (tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tScanOutParams      ScanOutParams;
    tRipRtEntry        *apAppSpecInfo[RIP_TRIE_NUM_ENTRIES];

    /*  Scan through the entrie table */

    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = pRipCxtEntry->i1RipId;

    ScanOutParams.pKey = NULL;
    ScanOutParams.pAppSpecInfo = &(apAppSpecInfo[0]);
    ScanOutParams.u4NumEntries = RIP_TRIE_NUM_ENTRIES;
    TrieScan (&(InParams), RipDecCount, &(ScanOutParams));

}

    /*******************************************************************************

        Function            :   rip_rt_get_info ().
        
        Description         :   This function scans the route table maintained by
                    the RIP module and then checks whether there is any
                    reachable route exist for the given destination and
                    mask pair, accordingly returns the route 
                    information structure or NULL.
        
        Input Parameters    :   u4DestAddr, the destination address for which a
                    route is needed.

        Output Parameters   :   None.

        Global Variables 
        Affected            :   None.

        Return Value        :   The destination route information structure, of type
                    void *.

    *******************************************************************************/

    /*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = rip_rt_get_info
        $$TRACE_PROCEDURE_LEVEL = LOW
    *******************************************************************************/

EXPORT VOID        *
rip_rt_get_info (UINT4 u4DestAddr, UINT4 u4NetMask, tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome;
    INT4                i4AppEntry;
    UINT4               au4Indx[2];    /*The Key of Trie is made-up of address and mask */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pRipRtEntry = NULL;
    INT4                i4LowMetric = 0xFFFF;

    /*
     * Search in side the trie
     */

    au4Indx[0] = RIP_HTONL (u4DestAddr & u4NetMask);
    au4Indx[1] = RIP_HTONL (u4NetMask);

    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    /* this is because interfaces are handled by 
     * another protocol.
     */
    InParams.pRoot = pRipCxtEntry->pRipRoot;

    MEMSET (apAppSpecInfo, 0, 16 * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
    if (i4OutCome == TRIE_SUCCESS)
    {
        for (i4AppEntry = 0; i4AppEntry < 16; i4AppEntry++)
        {
            if (apAppSpecInfo[i4AppEntry] != NULL)
            {
                /* Dont return if it is the summary route */
                if (i4AppEntry != (OTHERS_ID - 1))
                {
                    pBestRt = ((tRtInfo **) OutParams.pAppSpecInfo)[i4AppEntry];
                    if (i4LowMetric > pBestRt->i4Metric1)
                    {
                        i4LowMetric = pBestRt->i4Metric1;
                        pRipRtEntry = pBestRt;
                    }
                }
            }
        }
        return ((VOID *) pRipRtEntry);
    }
    return ((VOID *) NULL);

}

    /*******************************************************************************

        Function            : rip_copy_valid_entries
        
        Description         :  This function  copies the selected entry to 
                   the packet created for sending updates. 
                   This copy is based on the protocol type.
        
        Input parameters    : 
                    1. pRipInfoBlk, a pointer to the route information
                       block which has route structure, interface 
                       through which the route has been learnt,
                       Ifcounter value for that route.
                    2. pu2NoOfRoutes, the address of the variable which
                       is holding the current number of routes in the
                       packet.
                    3. u2If, Interface over which update will be sent.
                    4. pRoot, the route entry which needs to be copied.
        
        Output parameters   : None.      
        
        Global variables
        Affected            : None.                                            

        Return value        : RIP_SUCCESS/RIP_FAILURE.
        
    *******************************************************************************/
    /*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = rip_copy_valid_entries 
        $$TRACE_PROCEDURE_LEVEL = INTMD
    *******************************************************************************/

VOID
rip_copy_valid_entries (tRipInfoBlk * pRipInfoBlk,
                        UINT2 *pu2NoOfRoutes, int u2If, VOID *pRoot,
                        tRipCxt * pRipCxtEntry)
{
    tRipRtEntry        *pRt = NULL;
    tRipRtInfo          TempRt;
    UINT2               u2RtTag = 0;

    MEMSET (&TempRt, 0, sizeof (tRipRtInfo));

    RipGetIfRec ((UINT4) u2If, pRipCxtEntry);

    pRt = (tRipRtEntry *) pRoot;

    TempRt.u4DestNet = pRt->RtInfo.u4DestNet;
    TempRt.u4DestMask = pRt->RtInfo.u4DestMask;
    TempRt.u2RtType = pRt->RtInfo.u2RtType;
    TempRt.u4NextHop = pRt->RtInfo.u4NextHop;
    TempRt.u4RtIfIndx = pRt->RtInfo.u4RtIfIndx;
    TempRt.i4Metric1 = pRt->RtInfo.i4Metric1;
    TempRt.u4RtNxtHopAS = pRt->RtInfo.u4RtNxtHopAS;
    TempRt.u4NextHop = pRt->RtInfo.u4NextHop;

    if (RipApplyInOutFilter (pRipCxtEntry->pDistributeOutFilterRMap,
                             &TempRt, 0) != RIP_SUCCESS)
    {
        /* Stop processing this route. */
        return;
    }

    u2RtTag = (UINT2) ((TempRt.u4RtNxtHopAS) & 0x0000ffff);
    u2RtTag = RIP_HTONS (u2RtTag);
    MEMCPY (pRipInfoBlk[*pu2NoOfRoutes].Route.au1RouteTag, &u2RtTag,
            sizeof (UINT2));

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4DestNet
        = RIP_HTONL (TempRt.u4DestNet);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4SubnetMask
        = RIP_HTONL (TempRt.u4DestMask);

    (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric
        = RIP_HTONL ((UINT4) (TempRt.i4Metric1));

    /*
     * We need to store the interface index also for the
     * application of split horizon while sending.
     */

    (pRipInfoBlk[*pu2NoOfRoutes]).u2If = (UINT2) (TempRt.u4RtIfIndx);

    switch (pRt->RtInfo.u2RtProto)
    {
        case OTHERS_ID:
            if (u2If != RIPIF_INVALID_INDEX)
            {
                (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric =
                    RIP_HTONL (RIP_DEFAULT_METRIC);
            }
            else
            {
                /* we are sending normal updates on all interfaces
                 * only when rip is shutting down itself.
                 */
                (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric =
                    RIP_HTONL ((RIP_INFINITY - 1));
            }

            /*  no reverse poisoning */
            (pRipInfoBlk[*pu2NoOfRoutes]).u2IfCounter = 0;

            /* except rip every other route has zero next hop */
            (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4NextHop = 0;
            break;

        case CIDR_STATIC_ID:
            /* preference and metric fields in case of static
             * routes are same and it can take a value between
             * 0 - 255.*/

            if (TempRt.i4Metric1 != RIP_INFINITY)
            {
                (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric =
                    (UINT4) RIP_HTONL ((UINT4) TempRt.i4Metric1);
            }
            /* fall thro */
        case CIDR_LOCAL_ID:
            /*  no reverse poisoning */
            (pRipInfoBlk[*pu2NoOfRoutes]).u2IfCounter = 0;

            /* except rip every other route has zero next hop */
            (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4NextHop = 0;
            break;

        case RIP_ID:
            /*
             * For split horizon operation, we need the ifCounter value.
             */

            (pRipInfoBlk[*pu2NoOfRoutes]).u2IfCounter = pRt->RtInfo.u2IfCounter;

            /* rip might have non-zero next hop which should be copied */
            (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4NextHop =
                RIP_HTONL (TempRt.u4NextHop);

            break;

        case OSPF_ID:
        case BGP_ID:
            /*
             * For split horizon operation, we need the ifCounter value.
             */

            (pRipInfoBlk[*pu2NoOfRoutes]).u2IfCounter = pRt->RtInfo.u2IfCounter;

            /* rip might have non-zero next hop which should be copied */
            (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4NextHop =
                (TempRt.u4NextHop == pRt->RtInfo.u4Gw) ?
                0 : RIP_HTONL (TempRt.u4NextHop);
            if (TempRt.i4Metric1 != RIP_INFINITY)
            {
                (pRipInfoBlk[*pu2NoOfRoutes]).Route.u4Metric =
                    (UINT4) RIP_HTONL ((UINT4) TempRt.i4Metric1);
            }

            break;
        default:
            break;
    }
}

    /*******************************************************************************

        Function            : rip_select_entries_for_update
        
        Description         :  This function, based on the broadcast flag
                  u1BcstFlag and i1StaticFlag selects the correct 
                  route and returns to the calling function.      
                  in case, no entry is useful for the updates, it
                  returns NULL.
                   For normal broacast, every route is taken
                  on the consideration. for triggered update boradcast,
                  only those routes which have changed are taken in the
                  consideration.
                  The important assumption here is  a unique route
                  is associated with unique indices.
        
        Input parameters    : 1. u1BcstFlag. This helps in selecting entries based
                     on the type of update.
                  2. i1StaticRtEnable. This helps in deciding static
                     routes should be sent in updates or not.
                  3. pRt. The  pointer to the array of pointers  
                     which points to the routes of the different
                     application.
        
        Output parameters   : 1. pOutRt. The pointer to the non-zero entry.      
                     ( this non-zero entry might not turn out to be
                       valid entry)
        
        Global variables
        Affected            : None.                                            

        Return value        : Pointer to the valid and successful entry/NULL.
        
 *******************************************************************************/
 /*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = rip_select_entries_for update
        $$TRACE_PROCEDURE_LEVEL = INTMD
 *******************************************************************************/
static tRtInfo     *
rip_select_entries_for_update (UINT1 u1BcstFlag,
                               INT1 i1StaticRtEnable,
                               VOID *pRt, tRtInfo ** ppOutRt)
{
    tRtInfo           **pBase;
    INT4                i4AppEntry;

    pBase = (tRtInfo **) pRt;

    for (i4AppEntry = MAX_ROUTING_PROTOCOLS - 1; i4AppEntry >= 0; i4AppEntry--)
    {
        if (pBase[i4AppEntry] != NULL)
        {
            (*ppOutRt) = pBase[i4AppEntry];
            break;
        }
    }

    /* The aggregated/local if/static routes are not sent 
     *  in triggered updates.*/
    switch (i4AppEntry + 1)
    {
        case OTHERS_ID:
            return ((pBase[i4AppEntry]));

        case CIDR_LOCAL_ID:
            /* the entry is a local interface */
            if ((pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE) ||
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_NOT_IN_SERVICE))
            {
                if (u1BcstFlag == RIP_TRIG_UPDATE)
                {
                    /* triggered update */
                    if (RIP_RT_CHANGED_STATUS
                        (((tRipRtEntry *) pBase[i4AppEntry])) == 0)
                    {
                        return (NULL);
                    }
                    RIP_RT_RESET_CHANGED_STATUS
                        (((tRipRtEntry *) pBase[i4AppEntry]));
                }
                return ((pBase[i4AppEntry]));
            }
            break;

        case CIDR_STATIC_ID:
            /* the entry is a static entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
                RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *)
                                              pBase[i4AppEntry]));
                return ((pBase[i4AppEntry]));
            }

            if ((i1StaticRtEnable == RIP_PROP_STATIC_ENABLE) &&
                (u1BcstFlag != RIP_TRIG_UPDATE) &&
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE) &&
                (pBase[i4AppEntry]->i4Metric1 > 0))
            {
                /* the static route needs to be propagated */
                return (pBase[i4AppEntry]);
            }
            break;

        case RIP_ID:
            /* the entry is a RIP entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
            }
            RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]));
            return (pBase[i4AppEntry]);

        case BGP_ID:
            /* the entry is a bgp route */
            /* For trig upd. reset the changed status and return the entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
                RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *)
                                              pBase[i4AppEntry]));
                return (pBase[i4AppEntry]);
            }
            if ((u1BcstFlag != RIP_TRIG_UPDATE) &&
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE))
            {
                /* the bgp route needs to be propagated */
                return (pBase[i4AppEntry]);
            }
            break;

        case OSPF_ID:
            /* the entry is a ospf route */
            /* For trig upd. reset the changed status and return the entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
                RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *)
                                              pBase[i4AppEntry]));
                return (pBase[i4AppEntry]);
            }
            if ((u1BcstFlag != RIP_TRIG_UPDATE) &&
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE))
            {

                /* the ospf route needs to be propagated */
                return (pBase[i4AppEntry]);
            }
            break;
        default:
            break;
    }
    return (NULL);

}

/*******************************************************************************
Function            : rip_select_entries_for_wan_update
Description         : This function, based on the broadcast flag
                      u1BcstFlag and i1StaticFlag selects the correct
                      route and returns to the calling function.
                      in case, no entry is useful for the updates, it
                      returns NULL.
                      For normal broacast, every route is taken
                      on the consideration. for triggered update boradcast,
                      only those routes which have changed are taken in the
                      consideration.
                      The important assumption here is  a unique route
                      is associated with unique indices.
Input parameters    : 1. u1BcstFlag. This helps in selecting entries based
                      on the type of update.
                      2. i1StaticRtEnable. This helps in deciding static
                      routes should be sent in updates or not.
                      3. pRt. The  pointer to the array of pointers
                      which points to the routes of the different
                      application.
Output parameters   : 1. pOutRt. The pointer to the non-zero entry.
                      ( this non-zero entry might not turn out to be
                      valid entry)
Return value        : Pointer to the valid and successful entry/NULL.
*******************************************************************************/
static tRtInfo     *
rip_select_entries_for_wan_update (UINT1 u1BcstFlag,
                                   INT1 i1StaticRtEnable,
                                   VOID *pRt, tRtInfo ** ppOutRt)
{
    tRtInfo           **pBase;
    INT4                i4AppEntry;

    pBase = (tRtInfo **) pRt;

    for (i4AppEntry = MAX_ROUTING_PROTOCOLS - 1; i4AppEntry >= 0; i4AppEntry--)
    {
        if (pBase[i4AppEntry] != NULL)
        {
            (*ppOutRt) = pBase[i4AppEntry];
            break;
        }
    }
    /* The aggregated/local if/static routes are not sent
     *  in triggered updates.*/
    switch (i4AppEntry + 1)
    {
        case OTHERS_ID:
        {
            if (RIP_RT_CHANGED_STATUS
                (((tRipRtEntry *) pBase[i4AppEntry])) != 0)
            {
                RIP_RT_RESET_CHANGED_STATUS
                    (((tRipRtEntry *) pBase[i4AppEntry]));
            }
            return (NULL);

        }

        case CIDR_LOCAL_ID:
            /* the entry is a local interface */
            if ((pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE) ||
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_NOT_IN_SERVICE))
            {
                if (u1BcstFlag == RIP_TRIG_UPDATE)
                {
                    /* triggered update */
                    if (RIP_RT_CHANGED_STATUS
                        (((tRipRtEntry *) pBase[i4AppEntry])) == 0)
                    {
                        return (NULL);
                    }
                    RIP_RT_RESET_CHANGED_STATUS
                        (((tRipRtEntry *) pBase[i4AppEntry]));
                }
                return ((pBase[i4AppEntry]));
            }
            break;

        case CIDR_STATIC_ID:
            /* the entry is a static entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
                RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *)
                                              pBase[i4AppEntry]));
                return ((pBase[i4AppEntry]));
            }
            if ((i1StaticRtEnable == RIP_PROP_STATIC_ENABLE) &&
                (u1BcstFlag != RIP_TRIG_UPDATE) &&
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE) &&
                (pBase[i4AppEntry]->i4Metric1 > 0))
            {
                /* the static route needs to be propagated */
                return (pBase[i4AppEntry]);
            }
            break;

        case RIP_ID:
            /* the entry is a RIP entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
            }
            RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]));
            return (pBase[i4AppEntry]);

        case BGP_ID:
            /* the entry is a bgp route */
            /* For trig upd. reset the changed status and return the entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
                RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *)
                                              pBase[i4AppEntry]));
                return (pBase[i4AppEntry]);
            }
            if ((u1BcstFlag != RIP_TRIG_UPDATE) &&
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE))
            {
                /* the bgp route needs to be propagated */
                return (pBase[i4AppEntry]);
            }
            break;

        case OSPF_ID:
            /* the entry is a ospf route */
            /* For trig upd. reset the changed status and return the entry */
            if (u1BcstFlag == RIP_TRIG_UPDATE)
            {
                /* triggered update */

                if (RIP_RT_CHANGED_STATUS (((tRipRtEntry *) pBase[i4AppEntry]))
                    == 0)
                {
                    return (NULL);
                }
                RIP_RT_RESET_CHANGED_STATUS (((tRipRtEntry *)
                                              pBase[i4AppEntry]));
                return (pBase[i4AppEntry]);
            }
            if ((u1BcstFlag != RIP_TRIG_UPDATE) &&
                (pBase[i4AppEntry]->u4RowStatus == IPFWD_ACTIVE))
            {

                /* the ospf route needs to be propagated */
                return (pBase[i4AppEntry]);
            }
            break;
        default:
            break;
    }
    return (NULL);
}

/*******************************************************************************

        Function            :   rip_send_rip_rt_info.
        
        Description         :   This procedure is meant for sending RIP updates from
                    the RIP dynamic database. When it is a normal update,
                    all the routes that exist in the routing database
                    are sent and when it is a triggered update, only the
                    routes which have been changed are sent. For direct 
                    routes (and static), the information is not sent as
                    those would be covered by the static route
                    information broadcast. It also takes care of how
                    many routes can be composed by looking at the
                    interface index configuration parameters.
        
        Input Parameters    :   1. u1BcstFlag, flag indicating thr type of
                       broadcast.
                    2. u2If, the interface through which the RIP
                       information need to be sent.
                    3. pRipInfoBlk, a pointer to the route information
                       block which has route structure, interface 
                       through which the route has been learnt,
                       Ifcounter value for that route.
                    4. pu2NoOfRoutes, the address of the variable which
                       is holding the current number of routes in the
                       packet.
                    5. u4DestNet, the destination network.
                    6. u2DestUdpPort.
                    7. i1StaticEnable. Determines static routes 
                       should be sent or not.
        
        Output Parameters   :   pRouteBlk, pi4CurRt, au2PortArray both of these
                    variables are filled and modified.
        
        Global Variables
        Affected            :   None.
        
        Return Value        :   success | failure.
        
*******************************************************************************/

/*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = rip_send_rip_rt_info
        $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/
    /* 2.0.1.1 Fix 1 15-09-99 --START-- */

EXPORT INT4
rip_send_rip_rt_info (UINT1 u1Version,
                      UINT1 u1BcstFlag, UINT2 u2If,
                      tRipInfoBlk * pRipInfoBlk,
                      UINT2 *pu2NoOfRoutes, UINT4 u4DestNet,
                      UINT2 u2DestUdpPort, INT1 i1StaticRtEnable,
                      tRipCxt * pRipCxtEntry)
    /* 2.0.1.1 Fix 1 15-09-99 --END-- */
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    UINT2               u2RtsToCompose;
    INT4                i4OutCome;
    UINT4               au4Indx[2];
    VOID               *apAppSpecInfo[16];

    /*
     * Before we proceed further, we have to calculate (if possible) how many
     * routes that need to be composed per RIP packet. It is again based on the
     * interface configuration paramaters.
     */

    u2RtsToCompose = rip_get_routes_to_compose (u2If, pRipCxtEntry);

    /* IP code starts from here */
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    MEMSET (apAppSpecInfo, 0, 16 * sizeof (VOID *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = NULL;
    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
    if (i4OutCome == TRIE_SUCCESS)
    {
        pRt = rip_select_entries_for_update (u1BcstFlag, i1StaticRtEnable,
                                             OutParams.pAppSpecInfo, &(pTmpRt));
        if (pRt != NULL)
        {
            rip_copy_valid_entries (pRipInfoBlk, pu2NoOfRoutes, u2If, pRt,
                                    pRipCxtEntry);
            (*pu2NoOfRoutes)++;
        }
        else
        {
            return (RIP_FAILURE);
        }
    }
    MEMSET (apAppSpecInfo, 0, 16 * sizeof (VOID *));
    i4OutCome = RIP_SUCCESS;
    while (i4OutCome == RIP_SUCCESS)
    {
        /* this operation should be stopped when 
         * get next fails as get next failure represents
         * normal termination. 
         */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_SUCCESS)
        {
            pRt = rip_select_entries_for_update (u1BcstFlag, i1StaticRtEnable,
                                                 (tRtInfo *)
                                                 OutParams.pAppSpecInfo,
                                                 &(pTmpRt));
            if (pRt != NULL)
            {
                if (pRt->u2RtProto == OTHERS_ID)
                {
                    /* Do not add the route.Move to next entry in the trie */
                    au4Indx[0] = RIP_HTONL (pRt->u4DestNet);
                    au4Indx[1] = RIP_HTONL (pRt->u4DestMask);
                    MEMSET (apAppSpecInfo, 0, 16 * sizeof (VOID *));
                    continue;
                }
                rip_copy_valid_entries (pRipInfoBlk, pu2NoOfRoutes, u2If,
                                        pRt, pRipCxtEntry);

                if (*pu2NoOfRoutes == (u2RtsToCompose - 1))
                {
                    /*
                     * One datagram is full,
                     * Send it over the specified interface(s).
                     */

                    rip_send_update_datagram (u1Version, u1BcstFlag,
                                              u4DestNet, u2DestUdpPort,
                                              pRipInfoBlk, u2If,
                                              u2RtsToCompose, pRipCxtEntry);

                    *pu2NoOfRoutes = 0;
                }
                (*pu2NoOfRoutes)++;
            }
            au4Indx[0] = RIP_HTONL (pTmpRt->u4DestNet);
            au4Indx[1] = RIP_HTONL (pTmpRt->u4DestMask);
            MEMSET (apAppSpecInfo, 0, 16 * sizeof (VOID *));
        }
    }
    return RIP_SUCCESS;
}

/*******************************************************************************

         Function            : RipPurgeRoutes
        
        Description         : for each route in the route table, this function
                  checks if the interface for the route matches with
                  given interface. if the interface matches, then
                  that route is declared inaccessible by setting its
                  metric to infinity.                          
                   for every route that is changed, a flag is set
                  to indicate this change.
        
        Input parameters    : Pointer to the tScanOutParams structure.          
        
        Output parameters   : None.           
        
        Global variables
        Affected            : the global route database.

        Return value        : RIP_SUCCESS.                     
        
    *******************************************************************************/

    /*******************************************************************************
        $$TRACE_PROCEDURE_NAME  = RipPurgeRoutes
        $$TRACE_PROCEDURE_LEVEL = LOW
    *******************************************************************************/

INT4
RipPurgeRoutes (tRipRtEntry * pRt, tRipIfaceRec * pRipIfRec)
{
    UINT4               u4BitMask = 0;
    UINT4               u4PrevMetric;
    UINT4               u4PrevStatus;
    UINT4               au4Indx[2];    /*The Key of Trie is made-up of address and mask */
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4RemainingTime;

    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }
    pRipCxtEntry = pRipIfRec->pRipCxt;
    /* Put the route in Garbage Collect Interval. */

    u4PrevMetric = (UINT4) pRt->RtInfo.i4Metric1;
    u4PrevStatus = pRt->RtInfo.u4RowStatus;
    pRt->RtInfo.i4Metric1 = RIP_INFINITY;

    if (u4PrevMetric != RIP_INFINITY)
    {
        au4Indx[0] = RIP_HTONL (pRt->RtInfo.u4DestNet);
        au4Indx[1] = RIP_HTONL (pRt->RtInfo.u4DestMask);
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        InParams.i1AppId = (INT1) (pRt->RtInfo.u2RtProto - 1);
        InParams.pRoot = pRipCxtEntry->pRipRoot;

        OutParams.pObject1 = NULL;
        OutParams.pObject2 = NULL;

        /* Added the following line to avoid crash when 'no rip' 
         * is given on a particular interface, with pthreads.
         */
        OsixGiveSem (TRIE_SEM_NODE_ID, TRIE_SEM_NAME);

        i4OutCome = TrieUpdate (&(InParams), pRt, RIP_INFINITY, &OutParams);

        /* Added the following line to avoid crash when 'no rip' 
         * is given on a particular interface, with pthreads. 
         */
        OsixTakeSem (TRIE_SEM_NODE_ID, TRIE_SEM_NAME, OSIX_WAIT, 0);

        if (i4OutCome == TRIE_SUCCESS)
        {
            if (pRt->RtInfo.u2RtProto == RIP_ID)
            {
                pRt->RtInfo.u1Operation = RIPHA_ADD_ROUTE;
                RipRedDbUtilAddRtInfo (pRt);
                RipRedSyncDynInfo ();
            }
            RipUpdateRouteEntry (pRt, pRipCxtEntry);
            if (OutParams.pObject1 != NULL)
            {
                RipDeleteLeafFromList (OutParams.pObject1,
                                       (BOOLEAN) OutParams.u1LeafFlag,
                                       pRipIfRec, pRipCxtEntry);
            }
            else if (OutParams.pObject2 != NULL)
            {
                RipMoveLeafToListEnd (OutParams.pObject2, pRipIfRec,
                                      pRipCxtEntry);
            }
            OutParams.pObject1 = NULL;
            OutParams.pObject2 = NULL;
        }

    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  pRipCxtEntry->u4RipTrcFlag,
                  pRipCxtEntry->i4CxtId,
                  CONTROL_PLANE_TRC, RIP_NAME,
                  "Deleting Rt: %x \n", pRt->RtInfo.u4DestNet);

    pRt->RtInfo.u1Status = (UINT1) RIP_RT_PENDING;
    pRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;

    if (u4PrevMetric != RIP_INFINITY)
    {

        SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_METRIC);

        if ((RIP_GET_REMAINING_TIME (RIP_TIMER_ID,
                                     &(RIP_ROUTE_TIMER_NODE (pRt)),
                                     &u4RemainingTime)) == TMR_SUCCESS)
        {
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &(pRipIfRec->RipUpdateTimer.TimerNode));
        }

        RIP_ROUTE_TIMER_ROUTE (pRt) = (void *) pRt;
        RIP_ROUTE_TIMER_ID (pRt) = (UINT1) RIP_RT_TIMER_ID;
        RIP_START_TIMER (RIP_TIMER_ID,
                         &(RIP_ROUTE_TIMER_NODE (pRt)),
                         pRipIfRec->RipIfaceCfg.u2GarbageCollectionInterval);
        if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag == RIP_ADMIN_ENABLE)
        {
            RIP_RT_SET_CHANGED_STATUS (pRt);
        }
    }
    RIP_RT_SET_CHANGED_STATUS (pRt);

    if (u4PrevStatus != IPFWD_NOT_IN_SERVICE)
    {
        SET_BIT_FOR_CHANGED_PARAM (u4BitMask, IP_BIT_STATUS);
    }
    if (u4BitMask != 0)
    {
        OsixGiveSem (TRIE_SEM_NODE_ID, TRIE_SEM_NAME);

        if (pRt->u1RouteStatusInRtm == OSIX_TRUE)
        {
            RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry, pRt);
            RipAddNewBestRoutesToRtm (pRt, pRipCxtEntry);
        }

        OsixTakeSem (TRIE_SEM_NODE_ID, TRIE_SEM_NAME, OSIX_WAIT, 0);
    }

    return (RIP_SUCCESS);
}

/*******************************************************************************

    Function            :   RipUpdateRoutesThroIf    
    
    Description         :   This procedure either deletes the routes or starts
                            database timer for routes learned on this interface.
    
    Input Parameters    :   u2If, the interface index.
                            u1Flag, RIP_PURGE_ROUTES or 
                                    RIP_START_DBASE_TIMER
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   the global route database.
    
    Return Value        :   Rip_database_not_changed | Rip_database_changed.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipUpdateRoutesThroIf
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT UINT1
RipUpdateRoutesThroIf (UINT2 u2If, UINT1 u1Flag, UINT4 u4SrcAddr,
                       tRipCxt * pRipCxtEntry)
{
    tRipRtEntry        *pRtEntry = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT2                i2OffSet = (INT2) RIP_OFFSET (tRipRtEntry, RipIfNode);
    UINT1               u1RetVal = RIP_DATABASE_NOT_CHANGED;
    tRipRtEntry        *pSummaryRt = NULL;

    /*gets the Rip Interface record from the given Primary/Secondary
     *IP address for which the network command is enabled*/
    pRipIfRec = RipGetUtilIfRecForPrimSecAddr (u4SrcAddr, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_DATABASE_NOT_CHANGED;
    }

    while ((pRtEntry =
            (tRipRtEntry *) TMO_DLL_First (&(pRipIfRec->RipIfRtList))) != NULL)
    {
        pRtEntry = (tRipRtEntry *) ((FS_ULONG) pRtEntry - (UINT2) i2OffSet);
        TMO_DLL_Delete (&(pRipIfRec->RipIfRtList), &(pRtEntry->RipIfNode));

        RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC, RIP_NAME,
                      "Route %x %x %x %d is deleted from IfRtList %d\n",
                      pRtEntry->RtInfo.u4DestNet, pRtEntry->RtInfo.u4DestMask,
                      pRtEntry->RtInfo.u4NextHop,
                      pRtEntry->RtInfo.i4Metric1, pRtEntry->RtInfo.u4RtIfIndx);

        if (u1Flag == RIP_PURGE_ROUTES)
        {
            if (RipPurgeRoutes (pRtEntry, pRipIfRec) == RIP_SUCCESS)
            {
                /*check if this route has auto summary routes */
                pSummaryRt = RipCheckSummaryAgg (pRtEntry->RtInfo.u4DestNet,
                                                 pRtEntry->RtInfo.u4DestMask,
                                                 pRipCxtEntry);
                if (pSummaryRt != NULL)
                {
                    if (RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) ==
                        RIP_AUTO_SUMMARY_DISABLE)
                    {

                        RipDeleteSummarySinkRt (pSummaryRt, pRipCxtEntry);

                        RipDeleteSummaryRt (pSummaryRt, pRipCxtEntry);

                    }
                }

                if ((pRtEntry->RtInfo.u1Status & RIP_RT_CHANGED_VAL)
                    == RIP_RT_CHANGED_VAL)
                {
                    u1RetVal = RIP_DATABASE_CHANGED;
                }
            }
        }
        else
        {
            RIP_START_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRtEntry)),
                             pRipIfRec->RipIfaceCfg.u2RouteAgeInterval);
        }
    }

    UNUSED_PARAM (u2If);
    return u1RetVal;
}

        /***************************************************************
        >>>>>>>>> PRIVATE Routines of this Module Start Here <<<<<<<<<<
        ***************************************************************/

/*******************************************************************************

    Function            :   rip_add_new_route.
    
    Description         :   This procedure adds a new route to the route 
                            database. Sends the traps to memory manger if
                            necessary.
    
    Input Parameters    :   1. pRt, the route information structure which needs
                               to be added.
                            2. pRipCxtEntry: Pointer to the context Info         
    
    Output Parameters   :   pAvlRtTab is modified with the addition of the route
                            information strcuture.
    
    Global Variables
    Affected            :   u2RtEntryCount (Total no of entries in
                            the Route Database), route table of routes are
                            affected.
    
    Return Value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_add_new_route
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT INT4
rip_add_new_route (tRipRtEntry * pRt, tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome;
    UINT1               u1Flag = 0;
    INT4                i4count;
    INT4                i4LowMetric = 0xFFFF;
    UINT4               au4Indx[2];    /*The Key of Trie is made-up of address and mask */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipRtEntry        *pRipRtEntry = NULL;    /* Stores the current enrty in DLL */
    UINT1               u1NodePresent = FALSE;
    tRipRtEntry        *pBestRt = NULL;
    VOID               *apAppSpecInfo[16];
    CHR1               *pu1DestNetString = NULL;
    CHR1               *pu1NextHopString = NULL;
    UINT1               au1DestNetAddress[MAX_ADDR_LEN];

    MEMSET (au1DestNetAddress, 0, MAX_ADDR_LEN);

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }

    RIP_TRC_ARG1 (RIP_MOD_TRC,
                  pRipCxtEntry->u4RipTrcFlag,
                  pRipCxtEntry->i4CxtId,
                  CONTROL_PLANE_TRC, RIP_NAME,
                  "Total no of existing routes = %d \n",
                  pRipCxtEntry->u4TotalEntryCount);

    if (pRipCxtEntry->u4TotalEntryCount >=
        (FsRIPSizingParams[MAX_RIP_ROUTE_ENTRIES_SIZING_ID].u4PreAllocatedUnits
         +
         FsRIPSizingParams[MAX_RIP_SUMMARY_ENTRIES_SIZING_ID].
         u4PreAllocatedUnits))
    {
        RIP_TRC_ARG4 (RIP_MOD_TRC,
                      pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId,
                      RIP_CRITICAL_TRC, RIP_NAME,
                      "MAX RIP ROUTES ADDED TO RIP DB: DATABASE OVERFLOW"
                      "Route not added to the database - DEST - %x NextHop - %x METRICS - %d INT - %d \n",
                      pRt->RtInfo.u4DestNet, pRt->RtInfo.u4NextHop,
                      pRt->RtInfo.i4Metric1, pRt->RtInfo.u4RtIfIndx);
        /*
         * Send traps to the net manager for further actions, if required.
         */

        RIP_SEND_TRAP_FOR_TABLE_OVFLOW (RIP_TRAP_TABLE);    /* 0-> SubRef for trap */
        return (RIP_FAILURE);
    }

    RIP_TRC_ARG4 (RIP_MOD_TRC,
                  pRipCxtEntry->u4RipTrcFlag,
                  pRipCxtEntry->i4CxtId,
                  CONTROL_PLANE_TRC, RIP_NAME,
                  "Route to be added to the database - DEST - %x , "
                  "GW - %x, METRICS - %d, INT - %d \n",
                  pRt->RtInfo.u4DestNet, pRt->RtInfo.u4NextHop,
                  pRt->RtInfo.i4Metric1, pRt->RtInfo.u4RtIfIndx);

/* Search trie for any entry.
 * Case 1:
 * If Trie search is successful, validate existing route with the new one.
 * If new route is better, delete old one and add new route to DLL.
 * Else no update to be done in DLL.
 * Case 2. If not found, add route to DLL after TrieAddEntry. Because DLL contains the routes that 
 * are in TRIE. */
    au4Indx[0] = RIP_HTONL (pRt->RtInfo.u4DestNet & pRt->RtInfo.u4DestMask);
    au4Indx[1] = RIP_HTONL (pRt->RtInfo.u4DestMask);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    /* so that our own local, aggregated  or static routes are not added */
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    CLI_CONVERT_IPADDR_TO_STR (pu1DestNetString, pRt->RtInfo.u4DestNet);
    MEMCPY (au1DestNetAddress, pu1DestNetString, MAX_ADDR_LEN);
    CLI_CONVERT_IPADDR_TO_STR (pu1NextHopString, pRt->RtInfo.u4NextHop);

    if (i4OutCome == TRIE_SUCCESS)
    {
        for (i4count = 0; i4count < 16; i4count++)
        {
            if ((apAppSpecInfo[i4count] != NULL) &&
                (i4count != (OTHERS_ID - 1)) &&
                (i4count != (CIDR_STATIC_ID - 1)))
            {
                pBestRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
                if (i4LowMetric > pBestRt->RtInfo.i4Metric1)
                {
                    i4LowMetric = pBestRt->RtInfo.i4Metric1;
                    pRipRtEntry = pBestRt;
                }
            }
        }
        if (pRipRtEntry != NULL)
        {
            if ((pRipRtEntry->RtInfo.u4DestNet == pRt->RtInfo.u4DestNet) &&
                (pRipRtEntry->RtInfo.u4DestMask == pRt->RtInfo.u4DestMask) &&
                (pRipRtEntry->RtInfo.i4Metric1) <= (pRt->RtInfo.i4Metric1))
            {
                u1NodePresent = TRUE;
            }
            else
            {
                TMO_DLL_Delete (&(pRipCxtEntry->RipCxtRtList),
                                (tTMO_DLL_NODE *) & pRipRtEntry->RipCxtRtNode);

                RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId, RIP_CRITICAL_TRC,
                              RIP_NAME,
                              "Route for Dest IP Network = %s Dest Mask = %d Next Hop = %s Metric = %d deleted from RipCxtRtList\n",
                              au1DestNetAddress,
                              CliGetMaskBits (pRipRtEntry->RtInfo.u4DestMask),
                              pu1NextHopString, pRipRtEntry->RtInfo.i4Metric1);

                u1NodePresent = FALSE;
            }
        }
    }
    else
    {
        u1NodePresent = FALSE;
    }

    pRt->u4RipCxtId = (UINT4) pRipCxtEntry->i4CxtId;
    InParams.i1AppId = (INT1) ((pRt->RtInfo.u2RtProto) - 1);
    InParams.pRoot = pRipCxtEntry->pRipRoot;

    au4Indx[0] = RIP_HTONL (pRt->RtInfo.u4DestNet);
    au4Indx[1] = RIP_HTONL (pRt->RtInfo.u4DestMask);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.pAppSpecInfo = NULL;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieAddEntry (&(InParams), pRt, &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        /*Summary Route added to summarylist. */
        if (pRt->RtInfo.u2RtProto == RIP_ID)
        {
            pRt->RtInfo.u1Operation = RIPHA_ADD_ROUTE;
            RipRedDbUtilAddRtInfo (pRt);
            RipRedSyncDynInfo ();
        }
        if (pRt->RtInfo.u2RtProto == OTHERS_ID)
        {
            TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pRt->RipCxtRtNode);
            TMO_DLL_Add (&(pRipCxtEntry->RipCxtSummaryList),
                         (tTMO_DLL_NODE *) & pRt->RipCxtRtNode);
        }
        else
        {
            TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pRt->RipCxtRtNode);
            if (pRt->RtInfo.u4DestNet == 0)
            {
                /*
                 *Default Route added as first entry of the List
                 *
                 * */
                TMO_DLL_Insert (&(pRipCxtEntry->RipCxtRtList), NULL,
                                (tTMO_DLL_NODE *) & pRt->RipCxtRtNode);
            }
            else
            {
                if (u1NodePresent != TRUE)
                {
                    RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                                  pRipCxtEntry->i4CxtId, RIP_CRITICAL_TRC,
                                  RIP_NAME,
                                  "Route for Dest IP Network = %s Dest Mask = %d Next Hop = %s Metric = %d added to RipCxtRtList\n",
                                  au1DestNetAddress,
                                  CliGetMaskBits (pRt->RtInfo.u4DestMask),
                                  pu1NextHopString, pRt->RtInfo.i4Metric1);

                    TMO_DLL_Add (&(pRipCxtEntry->RipCxtRtList),
                                 (tTMO_DLL_NODE *) & pRt->RipCxtRtNode);
                }
            }
        }

        /*    pRipIfRec = RipGetIfRec (pRt->RtInfo.u4RtIfIndx, pRipCxtEntry); */

        if (pRt->RtInfo.u4SrcAddr != RIP_ZERO)
        {
            pRipIfRec =
                RipGetIfRecFromAddr (pRt->RtInfo.u4SrcAddr, pRipCxtEntry);
        }
        else
        {
            pRipIfRec = RipGetIfRec (pRt->RtInfo.u4RtIfIndx, pRipCxtEntry);
        }
        if ((pRipIfRec != NULL) &&
            (pRt->RtInfo.u2RtProto != CIDR_LOCAL_ID) &&
            (pRt->RtInfo.u2RtProto != OTHERS_ID))
        {
            RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId, RIP_CRITICAL_TRC,
                          RIP_NAME,
                          "Route for Dest IP Network = %s Dest Mask = %d Next Hop = %s Metric = %d is Added to IfRtList %d\n",
                          au1DestNetAddress,
                          CliGetMaskBits (pRt->RtInfo.u4DestMask),
                          pu1NextHopString, pRt->RtInfo.i4Metric1,
                          pRt->RtInfo.u4RtIfIndx);

            TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pRt->RipIfNode);
            TMO_DLL_Add (&(pRipIfRec->RipIfRtList),
                         (tTMO_DLL_NODE *) & pRt->RipIfNode);
        }
        if (OutParams.pObject1 != NULL)
        {
            RipAddLeafToList (OutParams.pObject1, pRipCxtEntry);
        }
        else if (OutParams.pObject2 != NULL)
        {
            RipMoveLeafToListEnd (OutParams.pObject2, pRipIfRec, pRipCxtEntry);
        }
        OutParams.pObject1 = NULL;
        OutParams.pObject2 = NULL;
    }

    /* linking/unlinking of timer and memory freeing will be done in 
     * rip_add_route 
     */

    if (i4OutCome == TRIE_SUCCESS)
    {
        if (OutParams.pAppSpecInfo == NULL)
        {
            if (pRt->RtInfo.u2RtProto == OTHERS_ID)
            {
                pRipCxtEntry->u4TotalSummaryCount++;
            }
            else
            {
                pRipCxtEntry->u4TotalEntryCount++;
            }
            u1Flag = 1;
        }
        if ((pRt->RtInfo.u2RtProto != RIP_ID)
            && (pRt->RtInfo.u2RtProto != OTHERS_ID))
        {
            RipUpdateImportTbl (pRt, (UINT1) pRt->RtInfo.u2RtProto, u1Flag,
                                pRipCxtEntry);
        }
        return (RIP_SUCCESS);
    }
    else
    {
        return (RIP_FAILURE);
    }
}

/*******************************************************************************

    Function            :   rip_stale_route_ifcounter.
    
    Description         :   This procedure is called in the context of assigning
                            a value for IfCounter for a route when it is added
                            and the route if superseding a stale route.
    
    Input Parameters    :   pAvlRt, the route structure.
                            pRipCxtEntry: Pointer to the context Info         
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   None
    
    Return Value        :   IfCounter value.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_stale_route_ifcounter
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

PRIVATE UINT2
rip_stale_route_ifcounter (tRipRtEntry * pAvlRt, tRipCxt * pRipCxtEntry)
{
    UINT2               u2Counter = 0;
    UINT4               u4RemainingTime = 0;
    tRipIfaceRec       *pRipIfRec = NULL;

    if ((pRipIfRec =
         RipGetIfRec (pAvlRt->RtInfo.u4RtIfIndx, pRipCxtEntry)) != NULL)
    {

        RIP_GET_REMAINING_TIME (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pAvlRt)),
                                &u4RemainingTime);
        u2Counter = (UINT2) (RIP_UPDATE_LOSS -
                             (UINT4) ((pRipIfRec->RipIfaceCfg.
                                       u2GarbageCollectionInterval) -
                                      u4RemainingTime /
                                      (pRipIfRec->RipIfaceCfg.
                                       u2UpdateInterval)));
        if (u2Counter > 0)
        {

            return u2Counter;
        }
    }

    return (0);

}

/*******************************************************************************

    Function            :   RipFindNumOfPktsPerSpace.
    
    Description         :   This procedure is called to find the number of 
                            packets per space if spacing is enabled. 

            NOTE        :  This function should be modified, if RIP 
                       redistributes the other dynamic protocols routes 
               also. 
    
    Input Parameters    :  u2IfId - The interface for which the number of 
                           packets to be send (per space) has to be calculated.
               u2NumRoutesPerPkt - Number of routes per packet.
    
    Output Parameters   :  None.
    
    Global Variables
    Affected            :  u2NoOfPktsPerSpace for a interface.
    
    Return Value        :  None. 

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipFindNumOfPktsPerSpace 
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

VOID
RipFindNumOfPktsPerSpace (UINT2 u2IfId, UINT2 u2NumRoutesPerPkt,
                          tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4TotalNumOfRoutes =
        pRipCxtEntry->u4TotalEntryCount + pRipCxtEntry->u4TotalSummaryCount;

    UNUSED_PARAM (u2NumRoutesPerPkt);
    pRipIfRec = RipGetIfRec ((UINT4) u2IfId, pRipCxtEntry);

    if (pRipIfRec != NULL)
    {
        /*
         *If the Total number of packets are > 25
         *No need to run spacing timer.
         * */

        if (((pRipIfRec->RipIfaceCfg.u2AuthType ==
              (UINT2) RIPIF_NO_AUTHENTICATION) &&
             (u4TotalNumOfRoutes <= RIP_MAX_ROUTES_PER_PKT)) ||
            ((pRipIfRec->RipIfaceCfg.u2AuthType !=
              (UINT2) RIPIF_NO_AUTHENTICATION) &&
             (u4TotalNumOfRoutes <= RIP_MAX_ROUTES_PER_PKT - 1)))
        {
            pRipIfRec->u2NoOfPktsPerSpace = 1;
            return;
        }

        /* Find the Number of packets per Space 
         * = Total number of RIP routes/25)/(10% of regular update interval)
         *  */
        pRipIfRec->u2NoOfPktsPerSpace = RIP_NUM_PACKETS_PER_SPACE;

    }
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipGenerateUpdateResponse                           |
*|                                                                     |
*| Description   : Generates Update Response.                          |  
*|                                                                     |  
*|                                                                     |  
*| Input         : pRipInfo, u2IfIndex, pu2Len.                        |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS / RIP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
RipGenerateUpdateResponse (tRipInfo * pRipInfo, UINT2 u2If, UINT2 *pu2Len,
                           tRipCxt * pRipCxtEntry, UINT1 u1ResType)
{
    tRipInfoBlk         aRipInfoBlk[RIP_MAX_ROUTES_PER_PKT];
    tRipInfoBlk        *pRipInfoBlk = &(aRipInfoBlk[0]);
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRipRtNode         *pListNode = NULL;
    VOID               *apAppSpecInfo[16];
    INT4                i4OutCome;
    UINT2               u2RtsToCompose = RIP_MAX_ROUTES_PER_PKT;
    UINT2               u2RouteInd;
    UINT2               u2NoOfRoutes = 0;
    INT4                i4RetVal = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;

    pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        return RIP_FAILURE;
    }

    if (u1ResType == RIP_TRIG_UPDATE)
    {
        pListNode = (tRipRtNode *) TMO_DLL_First (&(pRipCxtEntry->RipRtList));
    }
    else
    {
        pListNode = pRipIfRec->pReTxFirstRoute;
    }

    if (pListNode == NULL)
    {
        return RIP_FAILURE;
    }
    if (pRipIfRec->RipIfaceCfg.u2AuthType != (UINT2) RIPIF_NO_AUTHENTICATION)
    {
        /* only 24 routes can be sent */
        u2RtsToCompose--;
        pRipInfo++;
        *pu2Len = (UINT2) (*pu2Len + sizeof (tRoute));
    }

    InParams.pRoot = pListNode->pLeafNode;
    MEMSET (apAppSpecInfo, 0, 16 * sizeof (VOID *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = NULL;
    i4OutCome = TrieGetLeafInfo (&(InParams), &(OutParams));

    while (i4OutCome == TRIE_SUCCESS)
    {
        pRt = rip_select_entries_for_wan_update (u1ResType,
                                                 pRipCxtEntry->RipGblCfg.
                                                 i1PropagateStatic,
                                                 OutParams.pAppSpecInfo,
                                                 &(pTmpRt));
        if (pRt != NULL)
        {
            if (pRt->u4DestNet != 0)
            {
                rip_copy_valid_entries (pRipInfoBlk, &u2NoOfRoutes, u2If, pRt,
                                        pRipCxtEntry);
                u2NoOfRoutes++;
            }
        }
        pListNode = (tRipRtNode *) TMO_DLL_Next (&pRipCxtEntry->RipRtList,
                                                 (tTMO_DLL_NODE *) &
                                                 (pListNode->DllLink));
        if ((pListNode != NULL) && (u2NoOfRoutes < u2RtsToCompose))
        {
            InParams.pRoot = pListNode->pLeafNode;
            MEMSET (apAppSpecInfo, 0, 16 * sizeof (VOID *));
            OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
            OutParams.Key.pKey = NULL;
            i4OutCome = TrieGetLeafInfo (&(InParams), &(OutParams));
        }
        else
        {
            i4OutCome = RIP_FAILURE;
        }
    }
    for (u2RouteInd = 0; u2RouteInd < u2NoOfRoutes; u2RouteInd++)
    {

        /* Copy the route information into the outgoing RIP packet */

        MEMCPY ((VOID *) &(pRipInfo->RipMesg.Route),
                (const VOID *) &((pRipInfoBlk[u2RouteInd]).Route),
                sizeof (tRoute));

        RIP_ADDRESS_FAMILY (pRipInfo) = RIP_HTONS (INET_ADDRESS_FAMILY);
        if ((pRipInfoBlk[u2RouteInd]).u2If == u2If)
        {
            /* apply SHwPR - as per RFC 2091 Sec 3.3 */
            RIP_METRIC (pRipInfo) = RIP_HTONL (RIP_INFINITY);
        }
        if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_V1_DEMAND)
        {
            pRipInfo->RipMesg.Route.au1RouteTag[0] = 0;
            pRipInfo->RipMesg.Route.au1RouteTag[1] = 0;
            /* RIP_V1 MBZ feild */
            RIP_SUBNET_MASK (pRipInfo) = 0;
        }
        RIP_NEXT_HOP (pRipInfo) = 0;
        pRipInfo++;
    }

    *pu2Len = (UINT2) (*pu2Len + (u2NoOfRoutes * sizeof (tRoute)));

    /* update the retx last node of the interface */
    if (pListNode == NULL)
    {
        pRipIfRec->pReTxLastRoute =
            (tRipRtNode *) TMO_DLL_Last (&pRipCxtEntry->RipRtList);

    }
    else
    {
        pRipIfRec->pReTxLastRoute =
            (tRipRtNode *) TMO_DLL_Previous (&pRipCxtEntry->RipRtList,
                                             (tTMO_DLL_NODE *) &
                                             (pListNode->DllLink));
    }

    if (u2NoOfRoutes == 0)
    {
        pRipIfRec->pReTxFirstRoute = NULL;

        i4RetVal = RIP_FAILURE;
    }
    return i4RetVal;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipAddRouteToForwardingTableInCxt                   |
*|                                                                     |
*| Description   : Adds a route to IP forwarding Table                 |  
*|                                                                     |  
*|                                                                     |  
*| Input         : pRipRoute - carries RIP route                       |
*|               : pRipCxt - RIP Cxt pointer                           |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS / RIP_FAILURE / RIP_DUPLICATE_ROUTE     |
*|                 / RIP_NO_ROOM / RIP_UNKNOWN_VALUE                   |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT1
RipAddRouteToForwardingTableInCxt (tRipCxt * pRipCxt, tRipRtEntry * pRipRoute)
{
    INT4                i4ReturnValue = RIP_SUCCESS;
    tNetIpv4RtInfo      NetRtInfo;
    CHR1               *pu1DestNetString = NULL;
    CHR1               *pu1NextHopString = NULL;
    UINT1               au1DestNetAddress[MAX_ADDR_LEN];

    MEMSET (au1DestNetAddress, 0, MAX_ADDR_LEN);

    /* Forwarding  Tableshould be updated for RIP Routes.For Exported
       Routes, RIP should not update FIB */
    if ((pRipRoute == NULL) || (pRipRoute->RtInfo.u2RtProto != RIP_ID))
    {
        return RIP_SUCCESS;
    }
    else
    {
        MEMSET ((UINT1 *) &NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

        NetRtInfo.u4DestNet = pRipRoute->RtInfo.u4DestNet;
        NetRtInfo.u4DestMask = pRipRoute->RtInfo.u4DestMask;
        NetRtInfo.u4Tos = pRipRoute->RtInfo.u4Tos;
        NetRtInfo.u4NextHop = pRipRoute->RtInfo.u4NextHop;
        NetRtInfo.u4RtIfIndx = pRipRoute->RtInfo.u4RtIfIndx;
        NetRtInfo.u4RtNxtHopAs = pRipRoute->RtInfo.u4RtNxtHopAS;
        NetRtInfo.u4RtAge = 0;
        NetRtInfo.u4RouteTag =
            RIP_HTONS (pRipRoute->RtInfo.u4RtNxtHopAS & 0x0000ffff);
        NetRtInfo.i4Metric1 = pRipRoute->RtInfo.i4Metric1;
        NetRtInfo.u2RtProto = pRipRoute->RtInfo.u2RtProto;
        NetRtInfo.u2RtType = REMOTE;
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
        NetRtInfo.u1Preference = pRipRoute->u1Preference;
        NetRtInfo.u2ChgBit = (UINT2) IP_BIT_ALL;
        NetRtInfo.u4ContextId = (UINT4) pRipCxt->i4CxtId;
        /* Some change in the route parameter. */
        i4ReturnValue = NetIpv4LeakRoute (NETIPV4_ADD_ROUTE, &NetRtInfo);
        if (i4ReturnValue == NETIPV4_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1DestNetString, NetRtInfo.u4DestNet);
            MEMCPY (au1DestNetAddress, pu1DestNetString, MAX_ADDR_LEN);
            CLI_CONVERT_IPADDR_TO_STR (pu1NextHopString,
                                       pRipRoute->RtInfo.u4NextHop);
            RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxt->u4RipTrcFlag, pRipCxt->i4CxtId,
                          RIP_CRITICAL_TRC, RIP_NAME,
                          "Route for Dest IP Network = %s Dest Mask = %d Next Hop = %s Metric = %d Added in IP forwarding Table\n\r",
                          au1DestNetAddress,
                          CliGetMaskBits (NetRtInfo.u4DestMask),
                          pu1NextHopString, NetRtInfo.i4Metric1);
            i4ReturnValue = RIP_SUCCESS;
        }
        else
        {
            i4ReturnValue = RIP_FAILURE;
        }
        return (INT1) i4ReturnValue;
    }
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipModifyRouteToForwardingTableInCxt                |
*|                                                                     |
*| Description   : Modify a route in IP forwarding Table               |  
*|                                                                     |  
*|                                                                     |  
*| Input         : pRipRoute - carries RIP route to  modify            |
*|               : pRipCxt - RIP Cxt pointer                           |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS / RIP_ROUTE_NOT_FOUND                   |
*|                 / RIP_UNKNOWN_VALUE                                 |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT1
RipModifyRouteToForwardingTableInCxt (tRipCxt * pRipCxt,
                                      tRipRtEntry * pRipRoute)
{
    INT4                i4ReturnValue = RIP_SUCCESS;
    tNetIpv4RtInfo      NetRtInfo;
    CHR1               *pu1DestNetString = NULL;
    CHR1               *pu1NextHopString = NULL;
    UINT1               au1DestNetAddress[MAX_ADDR_LEN];

    MEMSET (au1DestNetAddress, 0, MAX_ADDR_LEN);

    /* Forwarding  Tableshould be updated for RIP Routes.For Exported
       Routes, RIP should not update FIB */
    if ((pRipRoute == NULL) || (pRipRoute->RtInfo.u2RtProto != RIP_ID))
    {
        return RIP_SUCCESS;
    }
    else
    {
        MEMSET ((UINT1 *) &NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
        NetRtInfo.u4DestNet = pRipRoute->RtInfo.u4DestNet;
        NetRtInfo.u4DestMask = pRipRoute->RtInfo.u4DestMask;
        NetRtInfo.u4Tos = pRipRoute->RtInfo.u4Tos;
        NetRtInfo.u4NextHop = pRipRoute->RtInfo.u4NextHop;
        NetRtInfo.u4RtIfIndx = pRipRoute->RtInfo.u4RtIfIndx;
        NetRtInfo.u4RtNxtHopAs = pRipRoute->RtInfo.u4RtNxtHopAS;
        NetRtInfo.u4RtAge = 0;
        NetRtInfo.u4RouteTag =
            RIP_HTONS (pRipRoute->RtInfo.u4RtNxtHopAS & 0x0000ffff);
        NetRtInfo.i4Metric1 = pRipRoute->RtInfo.i4Metric1;
        NetRtInfo.u2RtProto = pRipRoute->RtInfo.u2RtProto;
        NetRtInfo.u2RtType = REMOTE;
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
        NetRtInfo.u1Preference = pRipRoute->u1Preference;
        NetRtInfo.u2ChgBit = (UINT2) IP_BIT_ALL;
        NetRtInfo.u4ContextId = (UINT4) pRipCxt->i4CxtId;
        /* Some change in the route parameter. */
        i4ReturnValue = NetIpv4LeakRoute (NETIPV4_MODIFY_ROUTE, &NetRtInfo);
        if (i4ReturnValue == NETIPV4_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1DestNetString, NetRtInfo.u4DestNet);
            MEMCPY (au1DestNetAddress, pu1DestNetString, MAX_ADDR_LEN);
            CLI_CONVERT_IPADDR_TO_STR (pu1NextHopString,
                                       pRipRoute->RtInfo.u4NextHop);
            RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxt->u4RipTrcFlag, pRipCxt->i4CxtId,
                          RIP_CRITICAL_TRC, RIP_NAME,
                          "Route for Dest IP Network = %s Dest Mask = %d Next Hop = %s Metric = %d Modified in IP forwarding Table\n\r",
                          au1DestNetAddress,
                          CliGetMaskBits (NetRtInfo.u4DestMask),
                          pu1NextHopString, NetRtInfo.i4Metric1);
            i4ReturnValue = RIP_SUCCESS;
        }
        else
        {
            i4ReturnValue = RIP_FAILURE;
        }
    }
    return ((INT1) i4ReturnValue);
}

/*
*+---------------------------------------------------------------------+
*| Function Name : RipDeleteRouteFromForwardingTableInCxt              |
*|                                                                     |
*| Description   : Modify a route in IP forwarding Table               |  
*|                                                                     |  
*|                                                                     |  
*| Input         : pRipRoute - carries RIP route to  modify            |
*|               : pRipCxt - RIP Cxt pointer                           |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : RIP_SUCCESS / RIP_ROUTE_NOT_FOUND                   |
*|                 / RIP_UNKNOWN_VALUE                                 |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT1
RipDeleteRouteFromForwardingTableInCxt (tRipCxt * pRipCxt,
                                        tRipRtEntry * pRipRoute)
{
    INT4                i4ReturnValue = RIP_SUCCESS;
    tNetIpv4RtInfo      NetRtInfo;
    CHR1               *pu1NetAddress = NULL;
    CHR1               *pu1NxtHopAddress = NULL;
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    UINT1               au1NxtHopAddress[MAX_ADDR_LEN];

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (au1NxtHopAddress, 0, MAX_ADDR_LEN);

    pu1NetAddress = (CHR1 *) & au1NetAddress[0];
    pu1NxtHopAddress = (CHR1 *) & au1NxtHopAddress[0];

    /* Forwarding  Tableshould be updated for RIP Routes.For Exported
       Routes, RIP should not update FIB */
    if ((pRipRoute == NULL) || (pRipRoute->RtInfo.u2RtProto != RIP_ID))
    {
        return RIP_SUCCESS;
    }
    else
    {
        MEMSET ((UINT1 *) &NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
        NetRtInfo.u4DestNet = pRipRoute->RtInfo.u4DestNet;
        NetRtInfo.u4DestMask = pRipRoute->RtInfo.u4DestMask;
        NetRtInfo.u4Tos = pRipRoute->RtInfo.u4Tos;
        NetRtInfo.u4NextHop = pRipRoute->RtInfo.u4NextHop;
        NetRtInfo.u4RtIfIndx = pRipRoute->RtInfo.u4RtIfIndx;
        NetRtInfo.u4RtNxtHopAs = pRipRoute->RtInfo.u4RtNxtHopAS;
        NetRtInfo.u4RtAge = 0;
        NetRtInfo.u4RouteTag =
            RIP_HTONS (pRipRoute->RtInfo.u4RtNxtHopAS & 0x0000ffff);
        NetRtInfo.i4Metric1 = pRipRoute->RtInfo.i4Metric1;
        NetRtInfo.u2RtProto = pRipRoute->RtInfo.u2RtProto;
        NetRtInfo.u2RtType = REMOTE;
        NetRtInfo.u4RowStatus = (UINT4) IPFWD_DESTROY;
        NetRtInfo.u2ChgBit = (UINT2) IP_BIT_ALL;
        NetRtInfo.u4ContextId = (UINT4) pRipCxt->i4CxtId;
        /* Some change in the route parameter. */
        i4ReturnValue = NetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetRtInfo);
        CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress, NetRtInfo.u4DestNet);
        MEMCPY (au1NetAddress, pu1NetAddress, MAX_ADDR_LEN);
        CLI_CONVERT_IPADDR_TO_STR (pu1NxtHopAddress, NetRtInfo.u4NextHop);
        if (i4ReturnValue == NETIPV4_SUCCESS)
        {
            RIP_TRC_ARG4 (RIP_MOD_TRC,
                          pRipCxt->u4RipTrcFlag,
                          pRipCxt->i4CxtId,
                          RIP_CRITICAL_TRC,
                          RIP_NAME,
                          "Route deleted in IP forwarding Table : Dest IP Network = %s Dest Mask = %d Next Hop = %s Metric = %d\n\r",
                          au1NetAddress,
                          CliGetMaskBits (pRipRoute->RtInfo.u4DestMask),
                          pu1NxtHopAddress, pRipRoute->RtInfo.i4Metric1);
            i4ReturnValue = RIP_SUCCESS;
        }
        else
        {
            i4ReturnValue = RIP_FAILURE;
        }
        return (INT1) i4ReturnValue;
    }
}

/**************************************************************************
* Function Name     :  RipDeleteRtEntry.            
* Description       :  Deletes the Route Entry from RIP Route Table. 
* Input(s)          :  pRtEntry - Pointer to the RouteEntry.
*                      pRipCxtEntry - Pointer to the Context.
* Output(s)         :  Deletes the Route entry in RIP Route Table By calling
*                      TrieDeleteEntry.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
PUBLIC INT4
RipDeleteRtEntry (tRipRtEntry * pRtEntry, tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[2];    /* Index of Trie is made-up of 
                                       address and mask */
    tRipRtNode         *pListNode = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT1               u1SummaryFlag = 0;
    tRipRtEntry        *pTempRipRtEntry = NULL;
    UINT1               u1RipNetworkEnabled = OSIX_FALSE;

    if (pRtEntry->RtInfo.u4SrcAddr != RIP_ZERO)
    {
        pRipIfRec =
            RipGetIfRecFromAddr (pRtEntry->RtInfo.u4SrcAddr, pRipCxtEntry);
        u1RipNetworkEnabled = OSIX_TRUE;
    }
    else
    {
        pRipIfRec = RipGetIfRec (pRtEntry->RtInfo.u4RtIfIndx, pRipCxtEntry);
    }

/*    pRipIfRec = RipGetIfRec (pRtEntry->RtInfo.u4RtIfIndx, pRipCxtEntry);*/
    /* Stop timer before delete node from DLL or before free */
    RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRtEntry)));
    au4Indx[0] = RIP_HTONL (pRtEntry->RtInfo.u4DestNet);
    au4Indx[1] = RIP_HTONL (pRtEntry->RtInfo.u4DestMask);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = (INT1) (pRtEntry->RtInfo.u2RtProto - 1);

    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieDeleteEntry (&(InParams), &(OutParams),
                                 pRtEntry->RtInfo.u4NextHop);
    if (i4OutCome == TRIE_SUCCESS)
    {
        if (pRtEntry->RtInfo.u2RtProto == OTHERS_ID)
        {
            TMO_DLL_Delete (&(pRipCxtEntry->RipCxtSummaryList),
                            (tTMO_DLL_NODE *) & pRtEntry->RipCxtRtNode);
            if (pRtEntry->RtInfo.u4DestNet ==
                RIP_GET_SUMMARY_ADDRESS (pRtEntry->RtInfo.u4DestNet))
            {
                u1SummaryFlag = 1;
            }

        }
        else
        {
            RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                          pRipCxtEntry->i4CxtId, RIP_CRITICAL_TRC, RIP_NAME,
                          "Route %x %x %x %d deleted from RipCxtRtList\n",
                          pRtEntry->RtInfo.u4DestNet,
                          pRtEntry->RtInfo.u4DestMask,
                          pRtEntry->RtInfo.u4NextHop,
                          pRtEntry->RtInfo.i4Metric1);

            TMO_DLL_Delete (&(pRipCxtEntry->RipCxtRtList),
                            (tTMO_DLL_NODE *) & pRtEntry->RipCxtRtNode);
        }
        if (OutParams.pObject2 != NULL)
        {
            pListNode = (tRipRtNode *) OutParams.pObject2;
        }
        if (OutParams.pObject1 != NULL)
        {
            pListNode = (tRipRtNode *) OutParams.pObject1;
        }
        /*
         *TrieDeleEntry assigns the Node to 
         *PObject2 & deletes from Trie.
         *Updates trie & assigns node to
         *pObject1.
         *
         * */
        if (OutParams.pObject1 != NULL)
        {
            if (TMO_DLL_Is_Node_In_List ((tTMO_DLL_NODE *) & pListNode->DllLink)
                != 0)
            {

                TMO_DLL_Delete (&(pRipCxtEntry->RipRtList),
                                (tTMO_DLL_NODE *) & pListNode->DllLink);
                TMO_DLL_Add (&(pRipCxtEntry->RipRtList),
                             (tTMO_DLL_NODE *) & pListNode->DllLink);
            }
        }
        else if (OutParams.pObject2 != NULL)
        {
            if (OutParams.u1LeafFlag == TRUE)
            {
                pListNode->pLeafNode->pListNode = NULL;
            }
            if (TMO_DLL_Is_Node_In_List ((tTMO_DLL_NODE *) & pListNode->DllLink)
                != 0)
            {

                TMO_DLL_Delete (&(pRipCxtEntry->RipRtList),
                                (tTMO_DLL_NODE *) & pListNode->DllLink);
            }
            if ((pRipIfRec != NULL)
                && (pRtEntry->RtInfo.u2RtProto != CIDR_LOCAL_ID)
                && (pRtEntry->RtInfo.u2RtProto != OTHERS_ID))
            {
                if (TMO_DLL_Is_Node_In_List
                    ((tTMO_DLL_NODE *) & pRtEntry->RipIfNode) == TRUE)
                {
                    TMO_DLL_Delete (&(pRipIfRec->RipIfRtList),
                                    (tTMO_DLL_NODE *) & pRtEntry->RipIfNode);
                }

                RIP_TRC_ARG5 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId, RIP_CRITICAL_TRC,
                              RIP_NAME,
                              "Route %x %x %x %d is deleted from IfRtList %d\n",
                              pRtEntry->RtInfo.u4DestNet,
                              pRtEntry->RtInfo.u4DestMask,
                              pRtEntry->RtInfo.u4NextHop,
                              pRtEntry->RtInfo.i4Metric1,
                              pRtEntry->RtInfo.u4RtIfIndx);
            }
            RIP_FREE_LIST_ENTRY (pListNode);
        }
        if (u1SummaryFlag)
        {
            pRipCxtEntry->u4TotalSummaryCount--;
            RIP_SUMMARY_FREE (OutParams.pAppSpecInfo);
        }
        else if ((u1RipNetworkEnabled == OSIX_TRUE)
                 && (pRtEntry->RtInfo.u2RtProto == CIDR_LOCAL_ID))
        {
            pRipCxtEntry->u4TotalEntryCount--;
            pTempRipRtEntry = (tRipRtEntry *) (OutParams.pAppSpecInfo);
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &(RIP_ROUTE_TIMER_NODE (pTempRipRtEntry)));
            RIP_FREE_LOCAL_ROUTE_ENTRY (OutParams.pAppSpecInfo);
        }
        else
        {
            pRipCxtEntry->u4TotalEntryCount--;
            pTempRipRtEntry = (tRipRtEntry *) (OutParams.pAppSpecInfo);
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &(RIP_ROUTE_TIMER_NODE (pTempRipRtEntry)));
            RIP_ROUTE_FREE (OutParams.pAppSpecInfo);
        }
        return RIP_SUCCESS;
    }
    RIP_TRC (RIP_MOD_TRC,
             pRipCxtEntry->u4RipTrcFlag,
             pRipCxtEntry->i4CxtId,
             ALL_FAILURE_TRC, RIP_NAME,
             "Error while deleting RIP route entry from the route table");
    return RIP_FAILURE;
}

/**************************************************************************
* Function Name     :  RipDeleteRtTbl.            
* Description       :  Deletes the Route Entry from RIP Route Table. 
* Input(s)          :  pRipIfRec - Pointer to the IfRec Entry.
* Output(s)         :  Deletes the all the Route entries from RIP Route Table.
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
INT4
RipDeleteRtTbl (tRipCxt * pRipCxtEntry)
{

    tInputParams        InParams;
    tOutputParams       OutParams;
    tRipRtInfo         *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRipImportList     *pRipImportRt = NULL;
    tRipRtEntry        *pRt = NULL;
    UINT4               au4Indx[2];    /* Index of Trie is made-up of 
                                       address and mask */
    INT4                i4count;
    INT2                i2SrcProto;

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;

    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = NULL;

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    /*
     * Deleting All Route entries(Local, redistibuted & Aggreate Route) 
     * from the Context Routing Table.
     * 
     * */
    while (TrieGetNextEntry (&InParams, &OutParams) == TRIE_SUCCESS)
    {
        for (i4count = 0; i4count < MAX_ROUTING_PROTOCOLS; i4count++)
        {
            pRt = ((tRipRtEntry **) OutParams.pAppSpecInfo)[i4count];
            if (pRt == NULL)
            {
                continue;
            }
            /*
             * Deleting the Entry from forwading Table.
             * */
            RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRt)));
            RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry, pRt);
            if (RipDeleteRtEntry (pRt, pRipCxtEntry) == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         ALL_FAILURE_TRC,
                         RIP_NAME, "RIP Route Entry Deletion Failed\n\r");
                continue;
            }
        }
    }

    /*
     *      *Deleting Redistributed Routes From ImportList.
     *           *
     *                * */
    for (i2SrcProto = 0; i2SrcProto < MAX_ROUTING_PROTOCOLS; i2SrcProto++)
    {
        while ((pRipImportRt = (tRipImportList *)
                TMO_SLL_Get (&pRipCxtEntry->aRipImportList[i2SrcProto])) !=
               NULL)
        {
            RIP_FREE_IMPORT_ROUTE (pRipCxtEntry, pRipImportRt);
        }
    }
    /* Deleting the Default route created */

    pRt = rip_rt_get_info (0, 0, pRipCxtEntry);
    if (pRt != NULL)
    {
        RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRt)));
        RipDeleteRouteFromForwardingTableInCxt (pRipCxtEntry, pRt);
        RipDeleteRtEntry (pRt, pRipCxtEntry);
    }
    return RIP_SUCCESS;
}

/**************************************************************************
* Function Name     :  RipDelTrieRootEntry.
* Description       :  Deletes the Rip TrieRoot  Entry . 
* Input(s)          :  pRipIfRec - Pointer to the IfRec Entry.
* Output(s)         :  Deletes the all the Route entries from RIP Route Table.
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
***************************************************************************/
INT4
RipDelTrieRootEntry (tRipCxt * pRipCxtEntry)
{
    tInputParams        InParams;
    tDeleteOutParams    DeleteOutParams;
    if (pRipCxtEntry->pRipRoot != NULL)
    {
        InParams.pRoot = pRipCxtEntry->pRipRoot;
        InParams.i1AppId = pRipCxtEntry->i1RipId;

        if (TrieDelete (&InParams, RipTrieRootDel, (VOID *) &DeleteOutParams) ==
            TRIE_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME,
                     "Error while deleting Rip TrieRoot  Entry");
            return RIP_FAILURE;
        }
    }
    return RIP_SUCCESS;
}

/**************************************************************************
* Function Name     :  RipUpdateRouteEntry.
* Description       :  Updates the metric value of the route in the DLL . 
* Input(s)          :  pRt - Pointer to Route Entry.
* Output(s)         :  None.
* Returns           :  none.
***************************************************************************/
VOID
RipUpdateRouteEntry (tRipRtEntry * pRt, tRipCxt * pRipCxtEntry)
{
    tRipRtEntry        *pRtTemp = NULL;
    tRipRtEntry        *pNewBestRt = NULL;
    tTMO_DLL_NODE      *pListNode = NULL;
    tTMO_DLL           *pList = NULL;
    INT2                i2OffSet = 0;
    /* Get Best route for update in DLL */
    if (pRt->RtInfo.u2RtProto != OTHERS_ID)
    {
        pNewBestRt =
            RipGetBestRoute (pRt->RtInfo.u4DestNet,
                             pRt->RtInfo.u4DestMask, pRipCxtEntry);
        if (pNewBestRt == NULL)
        {
            pNewBestRt = pRt;
        }
    }
    else
    {
        pNewBestRt = pRt;
    }

    pList = (pRt->RtInfo.u2RtProto == OTHERS_ID) ?
        (&(pRipCxtEntry->RipCxtSummaryList)) : (&(pRipCxtEntry->RipCxtRtList));

    TMO_DLL_Scan (pList, pListNode, tTMO_DLL_NODE *)
    {
        i2OffSet = (INT2) RIP_OFFSET (tRipRtEntry, RipCxtRtNode);
        pRtTemp = RIP_ROUTE_INFO_PTR_FROM_RTLST (pListNode, i2OffSet);
        while (pRtTemp != NULL)
        {
            /* Validation for Route from the same source */
            if ((pRtTemp->RtInfo.u4DestNet == pNewBestRt->RtInfo.u4DestNet) &&
                (pRtTemp->RtInfo.u4DestMask == pNewBestRt->RtInfo.u4DestMask) &&
                (pRtTemp->RtInfo.u2RtProto == pNewBestRt->RtInfo.u2RtProto) &&
                (pRtTemp->RtInfo.i4Metric1 > pNewBestRt->RtInfo.i4Metric1))
            {
                RIP_TRC_ARG4 (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId, CONTROL_PLANE_TRC,
                              RIP_NAME,
                              "Route %x %x %x %d deleted from RipCxtRtList\n",
                              pRtTemp->RtInfo.u4DestNet,
                              pRtTemp->RtInfo.u4DestMask,
                              pRtTemp->RtInfo.u4NextHop,
                              pRtTemp->RtInfo.i4Metric1);

                TMO_DLL_Delete (pList, (tTMO_DLL_NODE *) pListNode);
                TMO_DLL_Delete (pList,
                                (tTMO_DLL_NODE *) & pNewBestRt->RipCxtRtNode);
                TMO_DLL_Init_Node (&(pNewBestRt->RipCxtRtNode));
                TMO_DLL_Add (pList,
                             (tTMO_DLL_NODE *) & pNewBestRt->RipCxtRtNode);

                return;
            }
            pRtTemp = pRtTemp->pNextAlternatepath;
        }
    }
}

/**************************************************************************/
/*   Function Name   : RipApplyInOutFilter                                */
/*   Description     : This function will check whether the route         */
/*                     can be added or dropped.                           */
/*                     If route is permitted then returned                */
/*                     RIP_SUCCESS else return RIP_FAILURE.               */
/*                                                                        */
/*   Input(s)        : pFilterRMap      - route map data                  */
/*                     pRtInfo          - ptr to the routing update       */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : RIP_FAILURE / RIP_SUCCESS                          */
/**************************************************************************/
INT1
RipApplyInOutFilter (tFilteringRMap * pFilterRMap, tRipRtInfo * pRtInfo,
                     UINT4 u4Src)
{
#ifdef ROUTEMAP_WANTED
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;

    if ((pFilterRMap == NULL) || (pRtInfo == NULL))
    {
        return RIP_SUCCESS;
    }

/*  If status of route map is disable then this route should not be droped */
    if (pFilterRMap->u1Status == FILTERNIG_STAT_DISABLE)
    {
        return RIP_SUCCESS;
    }

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));

    IPVX_ADDR_INIT_FROMV4 (RtInfoIn.DstXAddr, pRtInfo->u4DestNet);
    IPV4_MASK_TO_MASKLEN (RtInfoIn.u2DstPrefixLen, pRtInfo->u4DestMask);
    RtInfoIn.i2RouteType = (INT2) pRtInfo->u2RtType;
    IPVX_ADDR_INIT_FROMV4 (RtInfoIn.NextHopXAddr, pRtInfo->u4NextHop);
    RtInfoIn.u4IfIndex = pRtInfo->u4RtIfIndx;
    RtInfoIn.i4Metric = pRtInfo->i4Metric1;
    RtInfoIn.u4RouteTag = pRtInfo->u4RtNxtHopAS;
    IPVX_ADDR_INIT_FROMV4 (RtInfoIn.SrcXAddr, u4Src);

/*  Ignore modification of route attributes by route map */

    if (RMAP_ROUTE_DENY != RMapApplyRule (&RtInfoIn, &RtInfoOut,
                                          pFilterRMap->
                                          au1DistInOutFilterRMapName))
    {
        RipUpdateRtInfo (&RtInfoOut, pRtInfo);
        return RIP_SUCCESS;
    }
    else
    {
        return RIP_FAILURE;
    }
#else
    UNUSED_PARAM (pFilterRMap);
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (u4Src);
    return RIP_SUCCESS;
#endif
}

#ifdef ROUTEMAP_WANTED
/**************************************************************************/
/*   Function Name   : RipUpdateRtInfo                                    */
/*   Description     : This function will update the routeinfo            */
/*                      if the Set applied.                               */
/*   Input(s)        : pInfo         - ptr to RouteMapinfo after applying */
/*                                        Set rules                       */
/*                     pRtInfo    - ptr to the route need to update       */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : NONE                                               */
/**************************************************************************/

VOID
RipUpdateRtInfo (tRtMapInfo * pInfo, tRipRtInfo * pRtInfo)
{
    MEMCPY (&(pRtInfo->u4DestNet), pInfo->DstXAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
    IPV4_MASKLEN_TO_MASK (pRtInfo->u4DestMask, pInfo->u2DstPrefixLen);
    pRtInfo->u2RtType = (UINT2) pInfo->i2RouteType;
    MEMCPY (&(pRtInfo->u4NextHop), pInfo->NextHopXAddr.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    pRtInfo->u4RtIfIndx = pInfo->u4IfIndex;
    pRtInfo->i4Metric1 = pInfo->i4Metric;
    pRtInfo->u4RtNxtHopAS = pInfo->u4RouteTag;
    return;
}
#endif
