/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rrdlow.c,v 1.37 2016/03/26 09:59:47 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

#include "ripinc.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"
#include "ripcli.h"

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsRipRRDGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsRipRRDGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipRRDGlobalStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipRRDGlobalStatus (INT4 *pi4RetValFsRipRRDGlobalStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRipRRDGlobalStatus =
        pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipRRDSrcProtoMaskEnable
 Input       :  The Indices

                The Object 
                retValFsRipRRDSrcProtoMaskEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipRRDSrcProtoMaskEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipRRDSrcProtoMaskEnable (INT4 *pi4RetValFsRipRRDSrcProtoMaskEnable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipRRDSrcProtoMaskEnable =
        pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipRRDSrcProtoMaskDisable
 Input       :  The Indices

                The Object 
                retValFsRipRRDSrcProtoMaskDisable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipRRDSrcProtoMaskDisable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipRRDSrcProtoMaskDisable (INT4 *pi4RetValFsRipRRDSrcProtoMaskDisable)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRipRRDSrcProtoMaskDisable =
        pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipRRDRouteTagType
 Input       :  The Indices

                The Object 
                retValFsRipRRDRouteTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipRRDRouteTagType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipRRDRouteTagType (INT4 *pi4RetValFsRipRRDRouteTagType)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipRRDRouteTagType =
        pRipCxtEntry->RipRRDGblCfg.u1RipRRDRouteTagType;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipRRDRouteTag
 Input       :  The Indices

                The Object 
                retValFsRipRRDRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipRRDRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipRRDRouteTag (INT4 *pi4RetValFsRipRRDRouteTag)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipRRDRouteTag = pRipCxtEntry->RipRRDGblCfg.u2RipRRDRtTag;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRipRRDRouteDefMetric
 Input       :  The Indices

                The Object 
                retValFsRipRRDRouteDefMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRipRRDRouteDefMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsRipRRDRouteDefMetric (INT4 *pi4RetValFsRipRRDRouteDefMetric)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsRipRRDRouteDefMetric =
        pRipCxtEntry->RipRRDGblCfg.u2RipRRDDefMetric;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRipRRDGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsRipRRDGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipRRDGlobalStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipRRDGlobalStatus (INT4 i4SetValFsRipRRDGlobalStatus)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT4                i4CxtId = 0;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4CxtId = pRipCxtEntry->i4CxtId;

    switch (i4SetValFsRipRRDGlobalStatus)
    {
        case RIP_RRD_GBL_STAT_ENABLE:
            i1RetVal = (INT1) RipSetRrdGblStatusEnable (i4CxtId);
            break;

        case RIP_RRD_GBL_STAT_DISABLE:
            i1RetVal = (INT1) RipSetRrdGblStatusDisable (i4CxtId);
            break;

        default:
            i1RetVal = SNMP_FAILURE;

    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDGlobalStatus = %d\n", i4SetValFsRipRRDGlobalStatus); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDGlobalStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipRRDGlobalStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRipRRDGlobalStatus));
#endif
    return i1RetVal;
}

INT4
RipSetRrdGblStatusEnable (INT4 i4CxtId)
{
    tRIP_BUF_CHAIN_HEADER *pBuf = NULL;
    tIpParms           *pParms = NULL;
    tIpParms            IpParms;
    UINT4               u4BitMask;
    UINT4               u4RrdStatus;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    pParms = &IpParms;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

   /*** check if it is already enabled ***/
    if (pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus == RIP_RRD_GBL_STAT_ENABLE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        u4RrdStatus = RIP_RRD_GBL_STAT_ENABLE;
        u4BitMask = SNMP_BIT_RRD_STATUS;
        pBuf = RIP_BUF_ALLOCATE_CHAIN (sizeof (u4BitMask) +
                                       sizeof (u4RrdStatus) +
                                       sizeof (i4CxtId), 0);
        if (pBuf == NULL)
        {
            return SNMP_FAILURE;
        }

        pParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pBuf);
        if (pParms == NULL)
        {
            RIP_RELEASE_BUF (pBuf, FALSE);
            return SNMP_FAILURE;
        }
        pParms->u1Cmd = FROM_SNMP_TO_RIP;

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &u4BitMask, 0, sizeof (u4BitMask));

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &u4RrdStatus, sizeof (u4BitMask),
                                 sizeof (u4RrdStatus));

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &i4CxtId,
                                 sizeof (u4BitMask) + sizeof (u4RrdStatus),
                                 sizeof (i4CxtId));

        if (RipSendToQ (pBuf, RIP_SNMP_MSG) != RIP_SUCCESS)
        {
            RIP_RELEASE_BUF (pBuf, FALSE);
        }

        return SNMP_SUCCESS;
    }
}

INT4
RipSetRrdGblStatusDisable (INT4 i4CxtId)
{
    tRIP_BUF_CHAIN_HEADER *pBuf = NULL;
    tIpParms           *pParms = NULL;
    tIpParms            IpParms;
    UINT4               u4BitMask;
    UINT4               u4RrdStatus;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    pParms = &IpParms;
   /*** check if it is already disabled ***/
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus !=
        RIP_RRD_GBL_STAT_DISABLE)
    {
        u4BitMask = SNMP_BIT_RRD_STATUS;
        pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus = RIP_RRD_GBL_STAT_DISABLE;
        u4RrdStatus = RIP_RRD_GBL_STAT_DISABLE;

        pBuf =
            (tRIP_BUF_CHAIN_HEADER *)
            RIP_BUF_ALLOCATE_CHAIN (sizeof (u4BitMask) + sizeof (i4CxtId) +
                                    sizeof (u4RrdStatus), 0);
        if (pBuf == NULL)
        {
            return SNMP_FAILURE;
        }
        pParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pBuf);
        pParms->u1Cmd = FROM_SNMP_TO_RIP;

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &u4BitMask, 0, sizeof (u4BitMask));

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &u4RrdStatus,
                                 sizeof (u4BitMask), sizeof (u4RrdStatus));
        RIP_BUF_COPY_OVER_CHAIN (pBuf, &i4CxtId,
                                 sizeof (u4BitMask) + sizeof (u4RrdStatus),
                                 sizeof (i4CxtId));

        if (RipSendToQ (pBuf, RIP_SNMP_MSG) != RIP_SUCCESS)
        {
            RIP_RELEASE_BUF (pBuf, FALSE);
        }

    }                            /* end of else */
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRipRRDSrcProtoMaskEnable
 Input       :  The Indices

                The Object 
                setValFsRipRRDSrcProtoMaskEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipRRDSrcProtoMaskEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipRRDSrcProtoMaskEnable (INT4 i4SetValFsRipRRDSrcProtoMaskEnable)
{
    tRIP_BUF_CHAIN_HEADER *pBuf = NULL;
    tIpParms           *pParms = NULL;
    UINT4               u4BitMask;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT2               u2AllowedProtocols = RTM_BGP_MASK | RTM_OSPF_MASK |
        RTM_STATIC_MASK | RTM_DIRECT_MASK | RTM_AGGR_MASK | RTM_ISISL1L2_MASK;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
    INT4                i4CxtId = 0;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    i4CxtId = pRipCxtEntry->i4CxtId;

   /*** $$TRACE_LOG (ENTRY, "FsRipRRDSrcProtoMaskEnable = %d\n", i4SetValFsRipRRDSrcProtoMaskEnable); ***/

    if ((i4SetValFsRipRRDSrcProtoMaskEnable) & (~u2AllowedProtocols))
    {
        return SNMP_FAILURE;
    }
    else
    {
        pBuf =
            (tRIP_BUF_CHAIN_HEADER *) RIP_BUF_ALLOCATE_CHAIN (sizeof (u4BitMask)
                                                              +
                                                              sizeof (i4CxtId),
                                                              0);
        if (pBuf == NULL)
        {
            return SNMP_FAILURE;
        }
        pParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pBuf);
        pParms->u1Cmd = FROM_SNMP_TO_RIP;
        pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable |=
            (UINT2) i4SetValFsRipRRDSrcProtoMaskEnable;
        pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable &=
            (UINT2) ~(i4SetValFsRipRRDSrcProtoMaskEnable);
        u4BitMask = SNMP_BIT_SRC_PROTO_MSK_ENA;

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &u4BitMask, 0, sizeof (u4BitMask));
        RIP_BUF_COPY_OVER_CHAIN (pBuf, &i4CxtId, sizeof (u4BitMask),
                                 sizeof (i4CxtId));

        if (RipSendToQ (pBuf, RIP_SNMP_MSG) != RIP_SUCCESS)
        {
            RIP_RELEASE_BUF (pBuf, FALSE);
        }

    }
    /*** end of else ***/

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDSrcProtoMaskEnable;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipRRDSrcProtoMaskEnable) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable));
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDSrcProtoMaskDisable;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDRouteMapEnable
 Input       :  The Indices

                The Object
                testValFsRipRRDRouteMapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRipRRDRouteMapEnable (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsRipRRDRouteMapName)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDSrcProtoMaskEnable = %d\n", i4TestValFsRipRRDSrcProtoMaskEnable); ***/

    if (pTestValFsRipRRDRouteMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsRipRRDRouteMapName->i4_Length != 0)
    {
        if ((pRipCxtEntry == NULL) ||
            (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_GEN_ERR;
            CLI_SET_ERR (CLI_RIP_INV_ASSOC_ROUTE_MAP);
            return SNMP_FAILURE;
        }

        if (STRLEN (pRipCxtEntry->RipRRDGblCfg.au1RMapName) != 0)
        {
            /* Check for existence of Route Map with different Name.
             * If So, return failure, */
            if (STRLEN (pRipCxtEntry->RipRRDGblCfg.au1RMapName) !=
                (UINT4) pTestValFsRipRRDRouteMapName->i4_Length)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_RIP_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
            if (MEMCMP (pRipCxtEntry->RipRRDGblCfg.au1RMapName,
                        pTestValFsRipRRDRouteMapName->pu1_OctetList,
                        pTestValFsRipRRDRouteMapName->i4_Length) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_RIP_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDGlobalStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDSrcProtoMaskEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDSrcProtoMaskEnable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDSrcProtoMaskDisable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDSrcProtoMaskDisable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDRouteTagType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDRouteTagType (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDRouteTag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDRouteTag (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDRouteDefMetric
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDRouteDefMetric (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRipRRDRouteMapEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRipRRDRouteMapEnable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRipRRDRouteMapEnable
 Input       :  The Indices

                The Object
                setValFsRipRRDRouteMapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRipRRDRouteMapEnable (tSNMP_OCTET_STRING_TYPE *
                              pSetValFsRipRRDRouteMapName)
{

    INT1                i1RetVal = SNMP_FAILURE;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "FsRipRRDSrcProtoMaskEnable = %d\n", i4SetValFsRipRRDSrcProtoMaskEnable); ***/

    if (pSetValFsRipRRDRouteMapName->i4_Length == 0)
    {
        /* Resetting the Route Map Associated */
        MEMSET (pRipCxtEntry->RipRRDGblCfg.au1RMapName, 0,
                RMAP_MAX_NAME_LEN + 4);
        i1RetVal = SNMP_SUCCESS;
    }
    else if (STRLEN (pRipCxtEntry->RipRRDGblCfg.au1RMapName) == 0)
    {
        MEMCPY (pRipCxtEntry->RipRRDGblCfg.au1RMapName,
                pSetValFsRipRRDRouteMapName->pu1_OctetList,
                pSetValFsRipRRDRouteMapName->i4_Length);
        i1RetVal = SNMP_SUCCESS;
    }
    else if (MEMCMP (pRipCxtEntry->RipRRDGblCfg.au1RMapName,
                     pSetValFsRipRRDRouteMapName->pu1_OctetList,
                     pSetValFsRipRRDRouteMapName->i4_Length) == 0)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDRouteMapEnable;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipRRDRouteMapEnable) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", pRipCxtEntry->i4CxtId,
                      pSetValFsRipRRDRouteMapName));
#endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsRipRRDRouteMapEnable
 Input       :  The Indices

                The Object
                retValFsRipRRDRouteMapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRipRRDRouteMapEnable (tSNMP_OCTET_STRING_TYPE
                              * pRetValFsRipRRDRouteMapName)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsRipRRDRouteMapName->i4_Length = 0;
    MEMSET (pRetValFsRipRRDRouteMapName->pu1_OctetList, 0,
            RMAP_MAX_NAME_LEN + 4);

    if (STRLEN (pRipCxtEntry->RipRRDGblCfg.au1RMapName) != 0)
    {
        pRetValFsRipRRDRouteMapName->i4_Length =
            (INT4) STRLEN (pRipCxtEntry->RipRRDGblCfg.au1RMapName);
        MEMCPY (pRetValFsRipRRDRouteMapName->pu1_OctetList,
                pRipCxtEntry->RipRRDGblCfg.au1RMapName,
                pRetValFsRipRRDRouteMapName->i4_Length);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRipRRDSrcProtoMaskDisable
 Input       :  The Indices

                The Object 
                setValFsRipRRDSrcProtoMaskDisable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipRRDSrcProtoMaskDisable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipRRDSrcProtoMaskDisable (INT4 i4SetValFsRipRRDSrcProtoMaskDisable)
{
    tRIP_BUF_CHAIN_HEADER *pBuf = NULL;
    tIpParms           *pParms = NULL;
    UINT4               u4BitMask;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT2               u2AllowedProtocols = RTM_BGP_MASK | RTM_OSPF_MASK |
        RTM_STATIC_MASK | RTM_DIRECT_MASK | RTM_AGGR_MASK | RTM_ISISL1L2_MASK;
    tRipCxt            *pRipCxtEntry = NULL;
    INT4                i4CxtId;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    pRipCxtEntry = RIP_MGMT_CXT;
    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    i4CxtId = pRipCxtEntry->i4CxtId;
    pBuf =
        (tRIP_BUF_CHAIN_HEADER *) RIP_BUF_ALLOCATE_CHAIN (sizeof (u4BitMask) +
                                                          sizeof (i4CxtId), 0);
    if (pBuf == NULL)
    {
        return SNMP_FAILURE;
    }
    pParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pBuf);
    pParms->u1Cmd = FROM_SNMP_TO_RIP;

   /*** $$TRACE_LOG (ENTRY, "FsRipRRDSrcProtoMaskDisable = %d\n", i4SetValFsRipRRDSrcProtoMaskDisable); ***/

    if ((i4SetValFsRipRRDSrcProtoMaskDisable) & (~u2AllowedProtocols))
    {
        RIP_RELEASE_BUF (pBuf, FALSE);
        return SNMP_FAILURE;
    }
    else
    {
        u4BitMask = SNMP_BIT_SRC_PROTO_MSK_DIS;
        pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable |=
            (UINT2) i4SetValFsRipRRDSrcProtoMaskDisable;
        pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable &=
            (UINT2) ~(i4SetValFsRipRRDSrcProtoMaskDisable);

        RIP_BUF_COPY_OVER_CHAIN (pBuf, &u4BitMask, 0, sizeof (u4BitMask));
        RIP_BUF_COPY_OVER_CHAIN (pBuf, &i4CxtId, sizeof (u4BitMask),
                                 sizeof (i4CxtId));

        if (RipSendToQ (pBuf, RIP_SNMP_MSG) != RIP_SUCCESS)
        {
            RIP_RELEASE_BUF (pBuf, FALSE);
        }

    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDSrcProtoMaskDisable;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipRRDSrcProtoMaskDisable) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable));
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDSrcProtoMaskEnable;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable));
#endif
    /*** end of else ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsRipRRDRouteTagType
 Input       :  The Indices

                The Object 
                setValFsRipRRDRouteTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipRRDRouteTagType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipRRDRouteTagType (INT4 i4SetValFsRipRRDRouteTagType)
{
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDRouteTagType = %d\n", i4SetValFsRipRRDRouteTagType); ***/
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsRipRRDRouteTagType)
    {
        case MANUAL:
        case AUTOMATIC:
            pRipCxtEntry->RipRRDGblCfg.u1RipRRDRouteTagType =
                (UINT1) i4SetValFsRipRRDRouteTagType;
         /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
            RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
            SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDRouteTagType;
            SnmpNotifyInfo.u4OidLen =
                sizeof (FsMIRipRRDRouteTagType) / sizeof (UINT4);
            SnmpNotifyInfo.u1RowStatus = FALSE;
            SnmpNotifyInfo.pLockPointer = RipLock;
            SnmpNotifyInfo.pUnLockPointer = RipUnLock;
            SnmpNotifyInfo.u4Indices = 1;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SnmpNotifyInfo.u4SeqNum = u4SeqNum;

            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                              i4SetValFsRipRRDRouteTagType));
#endif
            return SNMP_SUCCESS;
        default:
            return SNMP_FAILURE;
    }
    /*** end of switch ***/
}

/****************************************************************************
 Function    :  nmhSetFsRipRRDRouteTag
 Input       :  The Indices

                The Object 
                setValFsRipRRDRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipRRDRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipRRDRouteTag (INT4 i4SetValFsRipRRDRouteTag)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDRouteTag = %d\n", i4SetValFsRipRRDRouteTag); ***/

    pRipCxtEntry->RipRRDGblCfg.u2RipRRDRtTag = (UINT2) i4SetValFsRipRRDRouteTag;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDRouteTag;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipRRDRouteTag) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRipRRDRouteTag));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRipRRDRouteDefMetric
 Input       :  The Indices

                The Object 
                setValFsRipRRDRouteDefMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRipRRDRouteDefMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsRipRRDRouteDefMetric (INT4 i4SetValFsRipRRDRouteDefMetric)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDRouteDefMetric = %d\n", i4SetValFsRipRRDRouteDefMetric); ***/

    pRipCxtEntry->RipRRDGblCfg.u2RipRRDDefMetric =
        (UINT2) i4SetValFsRipRRDRouteDefMetric;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRRDRouteDefMetric;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIRipRRDRouteDefMetric) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", pRipCxtEntry->i4CxtId,
                      i4SetValFsRipRRDRouteDefMetric));
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsRipRRDGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipRRDGlobalStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipRRDGlobalStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRipRRDGlobalStatus)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDGlobalStatus = %d\n", i4TestValFsRipRRDGlobalStatus); ***/
    switch (i4TestValFsRipRRDGlobalStatus)
    {
        case RIP_RRD_GBL_STAT_ENABLE:
        case RIP_RRD_GBL_STAT_DISABLE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    /** end of switch ***/

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDSrcProtoMaskEnable
 Input       :  The Indices

                The Object 
                testValFsRipRRDSrcProtoMaskEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipRRDSrcProtoMaskEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipRRDSrcProtoMaskEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsRipRRDSrcProtoMaskEnable)
{
    UINT2               u2AllowedProtocols = RTM_BGP_MASK | RTM_OSPF_MASK |
        RTM_STATIC_MASK | RTM_DIRECT_MASK | RTM_AGGR_MASK | RTM_ISISL1L2_MASK;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDSrcProtoMaskEnable = %d\n", i4TestValFsRipRRDSrcProtoMaskEnable); ***/

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValFsRipRRDSrcProtoMaskEnable & (~u2AllowedProtocols))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDSrcProtoMaskDisable
 Input       :  The Indices

                The Object 
                testValFsRipRRDSrcProtoMaskDisable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipRRDSrcProtoMaskDisable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipRRDSrcProtoMaskDisable (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsRipRRDSrcProtoMaskDisable)
{
    UINT2               u2AllowedProtocols = RTM_BGP_MASK | RTM_OSPF_MASK |
        RTM_STATIC_MASK | RTM_DIRECT_MASK | RTM_AGGR_MASK | RTM_ISISL1L2_MASK;
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDSrcProtoMaskDisable = %d\n", i4TestValFsRipRRDSrcProtoMaskDisable); ***/

    if (i4TestValFsRipRRDSrcProtoMaskDisable & (~u2AllowedProtocols))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDRouteTagType
 Input       :  The Indices

                The Object 
                testValFsRipRRDRouteTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipRRDRouteTagType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipRRDRouteTagType (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRipRRDRouteTagType)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDRouteTagType = %d\n", i4TestValFsRipRRDRouteTagType); ***/

    switch (i4TestValFsRipRRDRouteTagType)
    {
        case MANUAL:
        case AUTOMATIC:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    }/*** end of switch ***/

}

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDRouteTag
 Input       :  The Indices

                The Object 
                testValFsRipRRDRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipRRDRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipRRDRouteTag (UINT4 *pu4ErrorCode, INT4 i4TestValFsRipRRDRouteTag)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDRouteTag = %d\n", i4TestValFsRipRRDRouteTag); ***/
   /*** to avoid warning : unused parameter ***/
    if (pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus != RIP_RRD_GBL_STAT_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i4TestValFsRipRRDRouteTag = 0;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRipRRDRouteTag < 0) || (i4TestValFsRipRRDRouteTag > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsRipRRDRouteDefMetric
 Input       :  The Indices

                The Object 
                testValFsRipRRDRouteDefMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRipRRDRouteDefMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsRipRRDRouteDefMetric (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsRipRRDRouteDefMetric)
{
    tRipCxt            *pRipCxtEntry = RIP_MGMT_CXT;

    if ((pRipCxtEntry == NULL) ||
        (pRipCxtEntry->u1AdminStatus == RIP_ADMIN_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "FsRipRRDRouteDefMetric = %d\n", i4TestValFsRipRRDRouteDefMetric); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if ((i4TestValFsRipRRDRouteDefMetric > RIP_INFINITY) ||
        (i4TestValFsRipRRDRouteDefMetric < 1))
    {
        /*IP_TRC(RIP_MOD_TRC, CONTROL_PLANE_TRC ,RIP_NAME ,"wrong value for default metric"); */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
