/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: riptskmg.c,v 1.109 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains the routine for RIP task  
 *             and event management.
 *
 *******************************************************************/

#include "ripinc.h"
#include "ripcli.h"

#ifdef SNMP_2_WANTED
#include "fsripwr.h"
#include "stdripwr.h"
#include "fsmiriwr.h"
#include "fsmistwr.h"
#endif

extern INT4         CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName);

        /*|**********************************************************|**
         *** <<<<     Global Variable Declarations Start Here    >>>> ***
         **|**********************************************************|* */

               /* Semaphore identifier used for Mutual
                *  Exclusion between SNMP & RIP  
                */

/* tRipUpdateTimer     gTripTrigTmr; */
        /***************************************************************
        >>>>>>>>> Private Function Prototypes <<<<<<<<<<
        ***************************************************************/

tRipRtr             gRipRtr;
INT4                RipAttachRtmReceivedMessage (tRtmRespInfo * pRespInfo,
                                                 tRtmMsgHdr * pRtmHeader);

PRIVATE void        RipHandleRtmMessageEvent (tProtoQMsg * pParms,
                                              BOOLEAN *pbTriggerIndicator);
PRIVATE VOID        RipProcessRMapHandler (tProtoQMsg * pMsg);
PRIVATE INT1        RipHandleSnmpToRipMesg (tRIP_BUF_CHAIN_HEADER * pBuf,
                                            BOOLEAN *pbTriggerIndicator,
                                            tRipCxt **);

PRIVATE void        RipHandleControlMessageEvent (tProtoQMsg * pParms,
                                                  BOOLEAN *pbTrigInd,
                                                  tRipCxt **);

PRIVATE void        RipHandleTimerExpiryEvent (VOID);
PRIVATE void        RipReceiveAndProcessRipPkts (VOID);
PRIVATE INT1        RipHandleInterfaceUpMesg (tIpParms * pIpParms,
                                              tRipCxt ** pRipCxtEntry);
PRIVATE INT1        RipHandleInterfaceDownMesg (tIpParms * pIpParms,
                                                tRipCxt ** pRipCxtEntry);
PRIVATE INT1        RipHandleInterfaceDeleteMesg (tIpParms * pIpParms,
                                                  tRipCxt ** pRipCxtEntry);
PRIVATE INT1        RipHandleInterfaceIpAddrChg (tIpParms * pIpParms,
                                                 tRipCxt ** pRipCxtEntry);
PRIVATE INT1        RipIfDownHandleTimers (tRipIfaceRec * pRipIfRec);
PRIVATE INT1        RipIfUpHandleTimers (tRipIfaceRec * pRipIfRec);
PRIVATE VOID        RipIfUpUnnumHandler (UINT2, tRipCxt *);
PRIVATE VOID        RipIfDownUnnumHandler (UINT2, tRipCxt *);
PRIVATE VOID        RipIfDeleteUnnumHandler (tRipCxt *);
PRIVATE VOID        RipIfUnnumHandlers (tIpParms *, tRipCxt **);

PRIVATE UINT4       RipCreateTimerLists (void);
PRIVATE void        RipBindWithTransportLayer (void);
PRIVATE void        RipProcessMessages (void);
INT4                RipDeleteStaticRoute (UINT4 u4DestNet, UINT4 u4DestMask,
                                          UINT4 u4Tos, UINT4 u4NextHop,
                                          UINT2 u2RtType, INT4 i4Metric1,
                                          UINT4 u4RtIfIndx);
INT4                RipAddStaticRoute (UINT4 u4DestNet, UINT4 u4DestMask,
                                       UINT4 u4Tos, UINT4 u4NextHop,
                                       UINT2 u2RtType, INT4 i4Metric1,
                                       UINT4 u4RtIfIndx);
void                RipStaticRouteHandler (tRtInfo * pRtEntry, UINT4 u4BitMap);

PRIVATE INT4        RipMemInit (void);
PRIVATE INT4        RipCreateSemaphore (void);
PRIVATE INT4        RipCreateQ (void);
PRIVATE INT4        RipRegisterWithOthers (tRipCxt * pRipCxt);
PRIVATE INT4        RipInitializeToDefaultValues (tRipCxt *);
INT4                RipHandleRouteModification (tRipRtEntry * pRt, UINT4 u4Tos,
                                                UINT4 u4NextHop, UINT2 u2RtType,
                                                INT4 i4Metric1,
                                                UINT4 u4RtIfIndx,
                                                tOutputParams * pOutParams);
INT4                RipHandleLocalRouteModification (tRipRtEntry *, UINT2, INT4,
                                                     UINT4, tOutputParams *,
                                                     tRipCxt *);
PRIVATE VOID        RipHandleVcmMessageEvent (tProtoQMsg * pParms);

/**************************************************************************/
/*   Function Name   : RipMemInit                       */
/*   Description     : This function Intialize the memory and create      */
/*               Memory Pool.                                       */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/

INT4
RipMemInit (void)
{
    INT4                i4ReturnValue = RIP_FAILURE;
    RipSizingMemCreateMemPools ();
    IpRipMemory.RtEntryQId = RIPMemPoolIds[MAX_RIP_ROUTE_ENTRIES_SIZING_ID];
    IpRipMemory.LocalRtEntryQId =
        RIPMemPoolIds[MAX_RIP_LOCAL_ROUTE_ENTRIES_SIZING_ID];
    IpRipMemory.SumEntryQId = RIPMemPoolIds[MAX_RIP_SUMMARY_ENTRIES_SIZING_ID];
    IpRipMemory.RtNodeQId = RIPMemPoolIds[MAX_RIP_ROUTE_NODES_SIZING_ID];
    IpRipMemory.RtQMsgId = RIPMemPoolIds[MAX_RIP_QUE_DEPTH_SIZING_ID];
    IpRipMemory.IfQId = RIPMemPoolIds[MAX_RIP_INTERFACES_SIZING_ID];
    IpRipMemory.IfAggTblRootId =
        RIPMemPoolIds[MAX_RIP_IPIF_LOGICAL_IFACES_SIZING_ID];
    IpRipMemory.IfAggRtRecId = RIPMemPoolIds[MAX_RIP_AGG_RT_ENTRIES_SIZING_ID];
    IpRipMemory.CxtPoolId = RIPMemPoolIds[MAX_RIP_CONTEXT_SIZING_ID];
    IpRipMemory.RespPoolId = RIPMemPoolIds[MAX_RIP_RESP_MSG_NUM_SIZING_ID];
    IpRipMemory.ImportRtPoolId =
        RIPMemPoolIds[MAX_RIP_REDISTRIBUTE_ROUTES_SIZING_ID];
    IpRipMemory.UnicastNbrPoolId = RIPMemPoolIds[MAX_RIP_NBRS_SIZING_ID];
    IpRipMemory.AuthKeyPoolId = RIPMemPoolIds[MAX_RIP_MD5_AUTH_KEYS_SIZING_ID];
    IpRipMemory.CryptoAuthKeyPoolId =
        RIPMemPoolIds[MAX_RIP_CRYPTO_AUTH_KEYS_SIZING_ID];
    IpRipMemory.RMapFilterPoolId =
        RIPMemPoolIds[MAX_RIP_RMAP_FILTERS_SIZING_ID];
    IpRipMemory.RMMemPoolId = RIPMemPoolIds[MAX_RIP_RM_QUE_DEPTH_SIZING_ID];
    i4ReturnValue = RIP_SUCCESS;

    return i4ReturnValue;
}

/**************************************************************************/
/*   Function Name   : RipCreateSemaphore                          */
/*   Description     : This function create the semaphore                 */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/

INT4
RipCreateSemaphore (void)
{
    /* Create the semaphore identifier for the RIP task */
    if (OsixSemCrt (RIP_SEM_NAME, &gRipRtr.RipSemId) != OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Creation of RIP Semaphore failed. \n");

        return RIP_FAILURE;
    }
    OsixSemGive (gRipRtr.RipSemId);

    /* Create the Semaphore for RTM RouteList */
    if (OsixSemCrt (RIP_RTMRT_LIST_SEM, &gRipRtr.RipRtmLstSemId) !=
        OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Creation of RTM ROUTE Semaphore failed. \n");
        return RIP_FAILURE;
    }
    OsixSemGive (gRipRtr.RipRtmLstSemId);

    return RIP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipLock                                            */
/*   Description     : This function takes the RIP protocol semaphore     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : SNMP_SUCCESS or SNMP_FAILURE                       */
/**************************************************************************/
INT4
RipLock (VOID)
{
    if (OsixSemTake (gRipRtr.RipSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipUnLock                                          */
/*   Description     : This function releases the RIP protocol semaphore  */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : SNMP_SUCCESS or SNMP_FAILURE                       */
/**************************************************************************/
INT4
RipUnLock (VOID)
{
    OsixSemGive (gRipRtr.RipSemId);
    return SNMP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipCreateQ                                         */
/*   Description     : This function create the queue                     */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/

INT4
RipCreateQ (void)
{
    tOsixQId            RipInputQId;
    tOsixQId            RipRespQId;

    /* Create the Q for messages to RIP task */
    if (OsixCreateQ (RIP_INPUT_Q_NAME,
                     RIP_INPUT_Q_DEPTH,
                     RIP_INPUT_Q_MODE, &RipInputQId) != OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Creation Of RIP Input Queue Failed. \n");
        return RIP_FAILURE;
    }
    if (OsixCreateQ (RIP_RESP_Q_NAME,
                     RIP_RESP_MSG_NUM,
                     OSIX_GLOBAL, &RipRespQId) != OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Creation Of RIP Response Queue Failed. \n");
        OsixDeleteQ (RIP_INPUT_Q_NODE_ID, RIP_INPUT_Q_NAME);
        return RIP_FAILURE;
    }

    return RIP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipRegisterWithOthers                              */
/*   Description     : This function register RIP with other protocols    */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT4
RipRegisterWithOthers (tRipCxt * pRipCxt)
{
    tNetIpRegInfo       NetRegInfo;
    tRtmRegnId          RegnId;
    INT4                i4ReturnValue = NETIPV4_SUCCESS;
    INT4                i4ReturnVal = RIP_FAILURE;
    /* Register with IP and RTM */
    MEMSET ((UINT1 *) &NetRegInfo, 0, sizeof (tNetIpRegInfo));
    NetRegInfo.pIfStChng = RipIfStateChgHdlr;
    NetRegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    NetRegInfo.u1ProtoId = RIP_ID;
    NetRegInfo.u4ContextId = (UINT4) pRipCxt->i4CxtId;
    i4ReturnValue = NetIpv4RegisterHigherLayerProtocol (&NetRegInfo);

    if (i4ReturnValue == NETIPV4_SUCCESS)
    {

        RegnId.u2ProtoId = RIP_ID;
        RegnId.u4ContextId = (UINT4) pRipCxt->i4CxtId;

        i4ReturnValue = RtmRegister (&RegnId, RIP_RT_CHANGED_VAL,
                                     RipAttachRtmReceivedMessage);
        if (i4ReturnValue == RTM_SUCCESS)
        {
            i4ReturnVal = RIP_SUCCESS;
        }

    }

    return i4ReturnVal;

}

/**************************************************************************/
/*   Function Name   : RipDeRegisterWithOthersInCxt                       */
/*   Description     : This function Deregister RIP with other protocols  */
/*   Input(s)        : i4CxtId - RIP Cxt Id                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT4
RipDeRegisterWithOthersInCxt (INT4 i4CxtId)
{
    tRtmRegnId          RegnId;

    RegnId.u2ProtoId = RIP_ID;
    RegnId.u4ContextId = (UINT4) i4CxtId;
    RtmDeregister (&RegnId);
    NetIpv4DeRegisterHigherLayerProtocolInCxt ((UINT4) i4CxtId, RIP_ID);
    return RIP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipInitializeToDefaultValues                       */
/*   Description     : This function Initialize all rip prorocol          */
/*                     paramaters to default values.                      */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/

INT4
RipInitializeToDefaultValues (tRipCxt * pRipCxtEntry)
{
    INT4                i4OutCome;

    /*
     * Initialize the global rip configuration structure
     * to the default values.
     */
    MEMSET ((VOID *) &(pRipCxtEntry->RipGblCfg), 0, sizeof (tRipGblCfg));
    pRipCxtEntry->RipGblCfg.i1PropagateStatic = RIP_PROP_STATIC_DISABLE;
    pRipCxtEntry->RipGblCfg.u2RetxTimeout = RIP_DEF_RETX_TIMEOUT;
    pRipCxtEntry->RipGblCfg.u2MaxRetxCount = RIP_DEF_RETX_COUNT;
    pRipCxtEntry->u4RipTrcFlag = 0x0;
    pRipCxtEntry->u1RipRouteDBInitialized = 0;
    pRipCxtEntry->u4TotalEntryCount = 0;
    pRipCxtEntry->u4TotalSummaryCount = 0;
    pRipCxtEntry->u4NumOfPassNeighbors = 0;
    pRipCxtEntry->u1AdminStatus = RIP_ADMIN_DISABLE;
    pRipCxtEntry->b1LastKeyLifeTimeStatus = TRUE;

    pRipCxtEntry->TripTrigTmr.u1Data = RIP_TRIG_TIMER_INACTIVE;
    TMO_DLL_Init (&pRipCxtEntry->RipRtList);
    TMO_DLL_Init (&pRipCxtEntry->RipCxtRtList);
    TMO_DLL_Init (&pRipCxtEntry->RipCxtSummaryList);
    TMO_DLL_Init (&pRipCxtEntry->RipIfSpaceLst);
    RIP_CFG_SUBSCRIP_TIME (pRipCxtEntry) = RIP_DEF_SUBSCRIP_TIME;

    /* Initialize the global rip statistics structure. */
    MEMSET ((VOID *) &pRipCxtEntry->RipGblStats, 0, sizeof (tRipGblStats));

    /*
     * Initialize the Global Configuration structure for Neighbor List
     */
    RipInitNeighborList (pRipCxtEntry);

/* Spacing Status initialization */
    RIP_SPACING_STATUS (pRipCxtEntry) = RIP_SPACING_DISABLE;
    RIP_AUTO_SUMMARY_STATUS (pRipCxtEntry) = RIP_AUTO_SUMMARY_ENABLE;
    /*
     * Initialize the route table. Here, we refer the global indicator before
     * initializing. This is done with the view that we don't end up
     * reinitializing it again during Reset() operation of RIP, during which the     * route table pointer should not be lost.
     */
    if (!pRipCxtEntry->u1RipRouteDBInitialized)
    {
        i4OutCome = rip_rt_init_in_cxt (pRipCxtEntry);
        if (i4OutCome == RIP_FAILURE)
        {
            return (RIP_FAILURE);
        }
    }

    /*
     * Initialize the split horizon oper status variable to it's default value.
     */

    /*
     * Initialize the triggered timer limiting variables to their startup
     * values.
     */
    pRipCxtEntry->RipGblCfg.u1TrigUpdFlag = RIP_TRIG_TIMER_INACTIVE;
    pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_NOT_DESIRED;

    /* Initialize the Maximum security to its default value */
    pRipCxtEntry->RipGblCfg.u1MaximumSecurity = RIP_MAXIMUM_SECURITY;

    pRipCxtEntry->RipGblCfg.u2TotalNoOfPeers =
        (UINT2) FsRIPSizingParams[MAX_RIP_PEERS_SIZING_ID].u4PreAllocatedUnits;

    MEMSET ((VOID *) &pRipCxtEntry->RipRRDGblCfg, 0, sizeof (tRipRRDGblCfg));
    pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus = RIP_RRD_GBL_STAT_DISABLE;
    pRipCxtEntry->RipRRDGblCfg.u1RipRRDRouteTagType = MANUAL;
    pRipCxtEntry->RipRRDGblCfg.u2RipRRDRtTag = RIP_RRD_DEF_RT_TAG;
    pRipCxtEntry->RipRRDGblCfg.u2RipRRDDefMetric = RIP_RRD_DEF_RT_METRIC;
    pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable = RIP_RRD_DEF_SRC_PRO_ENA_MSK;
    pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable =
        RIP_RRD_DEF_SRC_PRO_DIS_MSK;

    pRipCxtEntry->pDistributeInFilterRMap = NULL;
    pRipCxtEntry->pDistributeOutFilterRMap = NULL;
    pRipCxtEntry->pDistanceFilterRMap = NULL;
    pRipCxtEntry->u1Distance = RIP_DEFAULT_PREFERENCE;

    UTL_SLL_Init (&(pRipCxtEntry->RipIfAggTblRootLst), 0);

    RipInitImportList (pRipCxtEntry);

    return RIP_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipPktRcvdOnSocket                                 */
/*   Description     : When the RIP Receives the Packet calls this        */
/*                     function                                           */
/*   Input(s)        : Socket Fd.                                         */
/*   Output(s)       : Sends the Message Received Event to RipTask.       */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RipPktRcvdOnSocket (INT4 i4SocketFd)
{
    UNUSED_PARAM (i4SocketFd);
    OsixSendEvent (RIP_TASK_NODE_ID, RIP_TASK_NAME, RIP_PKT_ARRIVAL_EVENT);
    return;
}

/*******************************************************************************

    Function        :   RipProtoInit.

    Description        :   This function is the start-up function for RIP
                protocol and is called from a root task. (e.g., IP
                module) It initializes the necessary data structures
                and enables the RIP protocol to active state and
                then spawns the RIP task for operation.
                First it initializes the RIP GLOBAL configuration
                elements to their default values. (Like, Preference
                and Propagate Static Flags).
                Then, it initializes the RIP GLOBAL statistics
                elements to 0.
                It then calls procedures to initialize the interface
                records for all the logical interfaces, to
                initialize the RIP route database,
                It creates the semaphore identifier for future use.
                Then enables the protocol to active state.

    Input parameters    :   None.

    Output parameters    :   None.

    Global variables
    Affected        :   1. Structure RipGblCfg Of Type tRipGblCfg.
                2. Structure RipGblStats Of Type tRipGblStats.
                3. Structure RipIfaceRec Of Type
                   tRipIfaceRecord.
                4. u2RipStartIndex Of Type UINT2, which is the
                   first valid interface.
                5. RipRtTabList Of Type tRIP_SLL, which is a
                   singly linked list of hash route tables.
                6. RipSemId Of Type SEM_ID.
                7. RipGblCfg.u1RipAdminFlag, the global admin status
                   for RIP protocol operation.
                8. RipGblCfg.u2SplitHorizonOperStatus, the flag
                   indicating what is the split horizon operation
                   to be applied.
                9. RipGblCfg.u1TrigUpdFlag, the flag indicating
                   whether any triggered timer is active.
               10. RipGblCfg.u1TrigDesired, the flag indicating
                   whether any triggered update is desired.

    Return value    :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipProtoInit 
    $$TRACE_PROCEDURE_LEVEL = INTMD
*******************************************************************************/

EXPORT INT4
RipProtoInit ()
{
    INT4                i4ReturnValue = RIP_SUCCESS;

    MEMSET ((void *) &IpRipMemory, 0, sizeof (tRipMemory));

    /* Bind with UDP */
    RipBindWithTransportLayer ();

    TMO_SLL_Init (&gRipRtr.RtmRtLst);
    gRipRtr.i4RtCal = RIP_RT_COMPLETE;

    /* Rip Memory initialisation */
    i4ReturnValue = RipMemInit ();
    if (i4ReturnValue == RIP_FAILURE)
    {
        i4ReturnValue = RIP_NO_MEMORY;
    }

    /* Rip Semaphore creation */
    i4ReturnValue = RipCreateSemaphore ();
    if (i4ReturnValue == RIP_FAILURE)
    {
        i4ReturnValue = RIP_NO_SEMAPHORE;

    }
    else
    {
        /* Rip Queue creation */
        i4ReturnValue = RipCreateQ ();
        if (i4ReturnValue == RIP_FAILURE)
        {
            i4ReturnValue = RIP_NO_QUEUE;
        }
        if ((i4ReturnValue == RIP_SUCCESS) &&
            (RipCreateTimerLists () == TMR_FAILURE))
        {
            i4ReturnValue = (INT4) TMR_FAILURE;
        }
    }

    /* Create Hash Table for Interface Record */
    gRipRtr.pGIfHashTbl = TMO_HASH_Create_Table (RIPIF_MAX_LOGICAL_IFACES,
                                                 NULL, FALSE);
    if (gRipRtr.pGIfHashTbl == NULL)
    {
        i4ReturnValue = RIP_NO_MEMORY;
    }
    return i4ReturnValue;
}

/**************************************************************************/
/*   Function Name   : RipProcessMessages                                 */
/*   Description     : This function dequeue messages from queue and      */
/*               called the appropriate fuctions to process the     */
/*               message.                                           */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE void
RipProcessMessages (void)
{
    tProtoQMsg         *pParms = NULL;
    BOOLEAN             bTrigInd = FALSE;
    INT1                i1ReturnValue;
    tRipCxt            *pRipCxtEntry = NULL;
    tOsixMsg           *pOsixMsg = NULL;

    while (OsixReceiveFromQ (RIP_INPUT_Q_NODE_ID,
                             RIP_INPUT_Q_NAME,
                             OSIX_NO_WAIT, 0, &pOsixMsg) == OSIX_SUCCESS)
    {
        /*
         * We have the mutual exclusion principle with SNMP mainly.
         */
        pParms = (tProtoQMsg *) pOsixMsg;
        switch (pParms->u4MsgType)
        {

            case RIP_SNMP_MSG:

                RipHandleControlMessageEvent (pParms, &bTrigInd, &pRipCxtEntry);
                break;

            case RIP_RRD_MSG:

                RipHandleRtmMessageEvent (pParms, &bTrigInd);
                break;

            case RIP_ROUTEMAP_UPDATE_MSG:

                RipProcessRMapHandler (pParms);
                break;
            case RIP_VCM_MSG:
                RipHandleVcmMessageEvent (pParms);
                break;
            case RIP_DELETE_IFREC_MSG:
                RipHandleDeleteIfRecEvent (pParms);
                break;

            default:
                break;
        }
        if (bTrigInd == TRUE)
        {
            bTrigInd = FALSE;
            if (pRipCxtEntry != NULL)
            {
                i1ReturnValue = RipSendTriggeredUpdate (pRipCxtEntry);
                if (i1ReturnValue == RIP_CONTINUE_LOOP)
                {
                    /* continue; */
                }
                else if (i1ReturnValue == RIP_FAILURE)
                {
                    RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                        RIP_TRC_GLOBAL_FLAG,
                                        RIP_INVALID_CXT_ID,
                                        ALL_FAILURE_TRC,
                                        RIP_NAME,
                                        "Failure in RipSendTriggeredUpdate.......\n");
                }
            }
        }                        /* bTrigInd */

        RIP_FREE_QMSG_ENTRY (pParms);
    }                            /*while */

}

/**************************************************************************/
/*   Function Name   : RipSendToQ                       */
/*   Description     :                                                    */
/*                                                                        */
/*   Input(s)        : PBuf                                               */
/*               MsgType - flag to indicate the message type        */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/

UINT4
RipSendToQ (void *pBuf, UINT4 MsgType)
{
    tProtoQMsg         *pParms = NULL;

    RIP_ALLOC_QMSG_ENTRY (pParms);

    if (pParms == NULL)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC, RIP_NAME,
                            "RipIfStateChgHdlr: Buffer allocation failed. \n");
        return (UINT4) RIP_NO_ROOM;
    }

    MEMSET (pParms, 0, sizeof (tProtoQMsg));

    switch (MsgType)
    {
        case RIP_SNMP_MSG:

            pParms->u4MsgType = RIP_SNMP_MSG;
            pParms->msg.SnmpParms = (tCRU_BUF_CHAIN_HEADER *) pBuf;
            break;

        case RIP_RRD_MSG:

            pParms->u4MsgType = RIP_RRD_MSG;
            pParms->msg.RtmParms = (tCRU_BUF_CHAIN_HEADER *) pBuf;
            break;
        case RIP_VCM_MSG:
            pParms->u4MsgType = RIP_VCM_MSG;
            pParms->msg.RipParms = (tCRU_BUF_CHAIN_HEADER *) pBuf;
            break;

        case RIP_ROUTEMAP_UPDATE_MSG:
            pParms->u4MsgType = RIP_ROUTEMAP_UPDATE_MSG;
            pParms->msg.RMapParms = (tCRU_BUF_CHAIN_HEADER *) pBuf;
            break;
        case RIP_DELETE_IFREC_MSG:
            pParms->u4MsgType = RIP_DELETE_IFREC_MSG;
            pParms->msg.IfDeleteParms = (tCRU_BUF_CHAIN_HEADER *) pBuf;
            break;

        default:
            break;

    }

    if (OsixSendToQ (RIP_INPUT_Q_NODE_ID,
                     RIP_INPUT_Q_NAME, (tOsixMsg *) (void *) pParms,
                     OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC, RIP_NAME,
                            "RipIfStateChgHdlr: OsixSendTo failed to"
                            "send Interface status. \n");
        RIP_FREE_QMSG_ENTRY (pParms);
        /*And pBuf should be freed by the caller */
        return ((UINT4) RIP_FAILURE);

    }

    OsixSendEvent (RIP_TASK_NODE_ID, RIP_TASK_NAME, RIP_INPUT_QUEUE_EVENT);
    return RIP_SUCCESS;
}

/*******************************************************************************

    Function            :   RIPTaskMain().
    
    Description         :   This is the entry function for the RIP module and
                            is to be spawned at the startup from an
                            initialization routine which will be called from a
                            root module (e.g., IP module), and runs as an
                            independent task performing RIP2 operation. It
                            does the following things.
                            1. since RIP runs as an application over UDP
                               module, this procedure also issues a call to
                               UDP to register it's presence.
                            3. It associates a global timer event
                               corresponding to RIP for further operation.
                            4. It enters a vicious loop and waits for 7 events,
                               namely,
                               RIP Interface Up/Down Events,
                               RIP Reset Event,
                               RIP Timer Event,
                               RIP Packet Input Event,
                               RIP Trigger Timer Event,
                               RIP Reset Timer Event.
                            5. If the event mask corresponds to the interface
                               operational status, it activates/deactivates the
                               corresponding interface.
                            6. If the event mask corresponds to the RESET event,
                               rip gets reset.
                            7. If the event mask corresponds to RIP Packet
                               Input Event, then,
                               It dequeues the packet and starts processing.
                            8. If the event mask corresponds to any of the timer
                               events, then,
                               It handles the expired timers and then proceeds
                               with processing them.
    
    Input parameters    :   None.
    
    Output parameters   :   None.
    
    Global variables  
    Affected            :   1. Structure RipGblCfg Of Type t_RIP_GBL_CFG.
                            2. Structure RipIfaceRec Of Type
                               tRipIfaceRecORD.
                            3. u2RipStartIndex Of Type UINT2, which is the
                               first valid interface.
                            4. RipRtTabList Of Type tRIP_SLL, which is a
                               singly linked list of hash route tables.
    
    Return value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RIPTaskMain
    $$TRACE_PROCEDURE_LEVEL = TOP
*******************************************************************************/

EXPORT VOID
RIPTaskMain (INT1 *pi1TaskParam)
{
    UINT4               u4EventMask;
    tRipCxt            *pRipCxtEntry = NULL;
    INT4                i4ReturnValue;
    UINT4               u4Flags = OSIX_NO_WAIT;
    tVcmRegInfo         VcmRegInfo;

    if (RipProtoInit () != RIP_SUCCESS)
    {
        RipDelMemInit ();
        RIP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    gu4RipSysLogId =
        (UINT4) SYS_LOG_REGISTER ((CONST UINT1 *) "RIP", SYSLOG_ERROR_LEVEL);

    if (gu4RipSysLogId <= 0)
    {
        RipDelMemInit ();
        SYS_LOG_DEREGISTER (gu4RipSysLogId);
        RIP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (RipRedInitGlobalInfo () == OSIX_FAILURE)
    {
        RipRedDeInitGlobalInfo ();
        RipDelMemInit ();
        SYS_LOG_DEREGISTER (gu4RipSysLogId);
        RIP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    RipRedDynDataDescInit ();

    RipRedDescrTblInit ();

    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (VcmRegInfo));
    VcmRegInfo.u1InfoMask |= VCM_IF_MAP_CHG_REQ;
    VcmRegInfo.u1InfoMask |= VCM_CXT_STATUS_CHG_REQ;
    VcmRegInfo.u1ProtoId = RIP_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = RipIfMapChngAndCxtChngHdlr;

    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        RipDelMemInit ();
        SYS_LOG_DEREGISTER (gu4RipSysLogId);
        RIP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (RIP_ALLOC_CXT_ENTRY (pRipCxtEntry) == NULL)
    {
        RipDelMemInit ();
        SYS_LOG_DEREGISTER (gu4RipSysLogId);
        RIP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    pRipCxtEntry->i4CxtId = RIP_DEFAULT_CXT;
    if (RipCreateCxtEntry (pRipCxtEntry) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 CONTROL_PLANE_TRC, RIP_NAME,
                 "Failed while Creating context entry");
        RipDelMemInit ();
        SYS_LOG_DEREGISTER (gu4RipSysLogId);
        RIP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    pRipCxtEntry->u1CxtStatus = ACTIVE;
    if (RipGetVcmSystemModeExt (RIP_PROTOCOL_ID) == VCM_SI_MODE)
    {
        RIP_MGMT_CXT = gRipRtr.apRipCxt[RIP_DEFAULT_CXT];
    }

    /* Indicate the status of initialization to the main routine */
    RIP_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterFSRIP ();
    RegisterSTDRIP ();
    RegisterFSMISTRIP ();
    RegisterFSMIRIP ();
#endif

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_RIP, RipSendRouteMapUpdateMsg);
#endif

    UNUSED_PARAM (pi1TaskParam);
    /* Initialise RIP related variables and activities */

    while (1)
    {
        while (OsixReceiveEvent ((RIP_TIMER_EVENT | RIP_PKT_ARRIVAL_EVENT |
                                  RIP_CONTROL_MSG_EVENT | RIP_RTM_RTMSG_EVENT |
                                  RIP_INPUT_QUEUE_EVENT | RIP_RM_PKT_EVENT |
                                  RIP_RM_PEND_RT_SYNC_EVENT |
                                  RIP_RM_START_TIMER_EVENT),
                                 u4Flags,
                                 RIP_EVENT_WAIT_TIMEOUT,
                                 &u4EventMask) == OSIX_FAILURE)
        {
            RipLock ();
            i4ReturnValue = RipProcessRespMessages ();
            if (i4ReturnValue == RIP_FAILURE)
            {
                u4Flags = OSIX_WAIT;
            }
            RipUnLock ();
        }
        u4Flags = OSIX_NO_WAIT;
        if (u4EventMask & (UINT4) RIP_TIMER_EVENT)
        {
            RipLock ();
            RipHandleTimerExpiryEvent ();

            /* Send triggered update if the flag for triggered update is set */
            RipUnLock ();
        }

        if (u4EventMask & RIP_RTM_RTMSG_EVENT)
        {
            RipLock ();
            RipProcessRtmRts ();
            RipUnLock ();
        }

        if (u4EventMask & (UINT4) RIP_PKT_ARRIVAL_EVENT)
        {
            RipLock ();
            RipReceiveAndProcessRipPkts ();

            if (SelAddFd (gRipRtr.i4RipSd, RipPktRcvdOnSocket) != OSIX_SUCCESS)
            {
                RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                    RIP_TRC_GLOBAL_FLAG,
                                    RIP_INVALID_CXT_ID,
                                    ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                                    "RIP Socket Select called Failed. \n");
            }
            RipUnLock ();
        }

        if (u4EventMask & (UINT4) RIP_INPUT_QUEUE_EVENT)
        {
            RipLock ();
            RipProcessMessages ();
            RipUnLock ();
        }

        if (u4EventMask & (UINT4) RIP_RM_PKT_EVENT)
        {
            RipLock ();
            RipRedHandleRmEvents (RIP_PROCESS_ALL_MSG);
            RipUnLock ();
        }

        if (u4EventMask & (UINT4) RIP_RM_START_TIMER_EVENT)
        {
            RipLock ();
            RipRedStartTimers ();
            RipUnLock ();
        }

        if (u4EventMask & (UINT4) RIP_RM_PEND_RT_SYNC_EVENT)
        {
            RipLock ();
            RipRedAddAllRouteNodeInDbTbl ();
            RipUnLock ();
        }
    }
}

/**************************************************************************/
/*   Function Name   : RipCreateTimerLists                                */
/*   Description     : This creates the timer lists required for RIP      */
/*                     process. The created timer list pointers are       */
/*                     captured at global variables.                      */
/*   Input(s)        : None.                                              */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE UINT4
RipCreateTimerLists (void)
{
    UINT1               au1RipTskName[4];
    UINT4               u4ReturnValue;

    /*
     * We define a global timer list for RIP operation and associate it with an
     * event. This is common for all the RIP related timers, such as update,
     * age, garbage collection.
     */

    STRNCPY (au1RipTskName, RIP_TASK_NAME, STRLEN (RIP_TASK_NAME));
    au1RipTskName[STRLEN (RIP_TASK_NAME)] = '\0';
    u4ReturnValue = RIP_CREATE_TMR_LIST (au1RipTskName,
                                         RIP_TIMER_EVENT, &RIP_TIMER_ID);
    if (u4ReturnValue == (UINT4) TMR_FAILURE)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "RipCreateTimerLists : "
                            "RIP Timer List creation Failed\n");
    }
    return u4ReturnValue;
}                                /* RipCreateTimerLists */

/**************************************************************************/
/*   Function Name   : RipBindWithTransportLayer                          */
/*   Description     : This binds with UDP and sets the socket options    */
/*                     The socket descriptor is captured in a global var. */
/*   Input(s)        : None.                                              */
/*   Output(s)       : None.                                              */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE void
RipBindWithTransportLayer ()
{
    struct sockaddr_in  RipLocalAddr;
    INT4                i4OpnVal = RIP_ENABLE;
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4MCloopVal = 0;    /*disable receiving mcast packets
                                             *sent by us*/
#endif
    INT4                i4Flags;

    /*
     * Since RIP operates over UDP, we need to open the standard UDP port for
     * RIP.
     */
    gRipRtr.i4RipSd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gRipRtr.i4RipSd < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Failure in Opening the UDP Port at 520 \n");

        return;
    }

    RipLocalAddr.sin_family = AF_INET;
    RipLocalAddr.sin_addr.s_addr = 0;
    RipLocalAddr.sin_port = RIP_HTONS (UDP_RIP_PORT);

    if (bind (gRipRtr.i4RipSd, (struct sockaddr *) &RipLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Binding of RIP Socket failed. \n");

        return;
    }

    if ((i4Flags = fcntl (gRipRtr.i4RipSd, F_GETFL, 0)) < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "Fcntl Get Failed in RIP. \n");
        return;
    }

    i4Flags |= O_NONBLOCK;
    if (fcntl (gRipRtr.i4RipSd, F_SETFL, i4Flags) < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "fcntl Failed in RIP. \n");
        return;
    }

    if (setsockopt
        (gRipRtr.i4RipSd, SOL_SOCKET, SO_BROADCAST, &i4OpnVal,
         sizeof (INT4)) < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "setsockopt Failed for SO_BROADCAST in RIP. \n");
        return;
    }
    if (setsockopt
        (gRipRtr.i4RipSd, IPPROTO_IP, IP_PKTINFO, &i4OpnVal, sizeof (INT4)) < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "setsockopt Failed for IP_PKTINFO in RIP. \n");
        return;
    }
#ifdef BSDCOMP_SLI_WANTED
    /*Set the option not to receive packet on the interface on which
     * it is sent*/
    if (setsockopt (gRipRtr.i4RipSd, IPPROTO_IP, IP_MULTICAST_LOOP,
                    &i4MCloopVal, sizeof (INT4)) < 0)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "setsockopt Failed for IP_IP_MULTICAST_LOOP in RIP. \n");
        return;
    }
#endif
    if (SelAddFd (gRipRtr.i4RipSd, RipPktRcvdOnSocket) != OSIX_SUCCESS)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                            "RIP Socket Select called Failed. \n");
        return;
    }

}                                /* RipBindWithTransportLayer */

/**************************************************************************/
/*   Function Name   : RipSendTriggeredUpdate                             */
/*   Description     : This sends the update message as triggered         */
/*   Input(s)        : None.                                              */
/*   Output(s)       : None.                                              */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/

INT1
RipSendTriggeredUpdate (tRipCxt * pRipCxtEntry)
{
    INT1                i1ReturnValue = RIP_SUCCESS;

    i1ReturnValue = (INT1) rip_generate_update_message (RIP_VERSION_ANY,
                                                        RIP_TRIG_UPDATE,
                                                        IP_GEN_BCAST_ADDR,
                                                        UDP_RIP_PORT,
                                                        RIPIF_INVALID_INDEX,
                                                        pRipCxtEntry);

    /*start trigger update timer for wan interface */
    if ((pRipCxtEntry->TripTrigTmr.u1Data == RIP_TRIG_TIMER_INACTIVE) &&
        (RIP_TIMER_ID != 0))
    {
        /* database changed start timer so that we dont sent
         * a update for every route added
         */
        pRipCxtEntry->TripTrigTmr.u1Data = RIP_TRIG_TIMER_ACTIVE;
        pRipCxtEntry->TripTrigTmr.u1Id = RIP_TRIP_TRIG_TMR;
        RIP_START_TIMER (RIP_TIMER_ID,
                         &((pRipCxtEntry->TripTrigTmr).TimerNode),
                         RIP_TRIP_TRIG_INTVL);
    }

    return i1ReturnValue;
}                                /* RipSendTriggeredUpdate */

/**************************************************************************/
/*   Function Name   : RipHandleControlMessageEvent                       */
/*   Description     : This receives the RIP Control messages and process */
/*                     them by invoking repective functions.              */
/*   Input(s)        : pbTriggerIndicator -Flag to indicate trigger update*/
/*   Output(s)       : pu1FirstIfToComeUp - Flag to indicate whether this */
/*                     is the first interface comes up                    */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE void
RipHandleControlMessageEvent (tProtoQMsg * pParms,
                              BOOLEAN *pbTriggerIndicator,
                              tRipCxt ** ppRipCxtEntry)
{
    tIpParms           *pIpParms = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtInfo = NULL;
    UINT4               u4Addr;
    INT4                i4RetVal = 0;
    BOOLEAN             bSecondaryFound = FALSE;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4NetMask = 0;

    *pbTriggerIndicator = FALSE;

    pIpParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pParms->msg.SnmpParms);

    if (pIpParms->u1Cmd != FROM_SNMP_TO_RIP)
    {
        /*
         * The communication from UDP to RIP consists of a buffer of
         * data and UDP to APPLICATION params structure which is used to
         * identify the port from which the packet comes.
         */

        pRipCxtInfo = RipGetCxtInfoRec ((INT4) pIpParms->u4ContextId);
        if (pRipCxtInfo == NULL)
        {
            RIP_RELEASE_BUF (pParms->msg.SnmpParms, FALSE);
            return;
        }

        /* Rip enabled unnmbered interfaces are handled on 
           any interface state change whether rip is enabled
           or not on that interface */
        RipIfUnnumHandlers (pIpParms, ppRipCxtEntry);
        pRipIfRec = RipGetIfRec (pIpParms->u2Port, pRipCxtInfo);
        if ((pRipIfRec == NULL) || (RIPIF_IS_INVALID_ID (pRipIfRec->IfaceId)))
        {
            RIP_RELEASE_BUF (pParms->msg.SnmpParms, FALSE);
            return;
        }

        /* For interface deletion, address change need not be checked */
        if (pIpParms->u1Cmd != IP_INTERFACE_DELETE)
        {
            if ((i4RetVal = (INT4) RipIfGetAddrFromPort (pIpParms->u2Port))
                == RIP_FAILURE)
            {
                RIP_RELEASE_BUF (pParms->msg.SnmpParms, FALSE);
                return;
            }
            u4Addr = (UINT4) i4RetVal;
            /* check for secodnary address also as network command can be 
               enabled on secondary */
            NetIpv4GetCfaIfIndexFromPort (((UINT4) pIpParms->u2Port),
                                          &u4CfaIfIndex);
            if (CfaIpIfIsSecLocalNetOnInterface
                (u4CfaIfIndex, (pRipIfRec->u4Addr), &u4NetMask) == CFA_SUCCESS)
            {
                bSecondaryFound = TRUE;
            }

            /*check whether there is an address change on a numbered 
               interface or on an unnumbered interface when rip is enabled
               on that interface */
            if (((pRipIfRec->u4Addr > RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES) &&
                 ((u4Addr != pRipIfRec->u4Addr) && (bSecondaryFound == FALSE)))
                || ((pRipIfRec->u4Addr <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES)
                    && (u4Addr != 0)))
            {
                pIpParms->u1Cmd = RIP_IF_IP_ADDR_CHG;
            }
        }
    }
    switch (pIpParms->u1Cmd)
    {
        case IPIF_OPER_ENABLE:
            RipHandleInterfaceUpMesg (pIpParms, ppRipCxtEntry);
            break;

        case IPIF_OPER_DISABLE:
            RipHandleInterfaceDownMesg (pIpParms, ppRipCxtEntry);
            break;

        case IP_INTERFACE_DELETE:
            RipHandleInterfaceDeleteMesg (pIpParms, ppRipCxtEntry);
            break;
        case RIP_IF_IP_ADDR_CHG:
            RipHandleInterfaceIpAddrChg (pIpParms, ppRipCxtEntry);
            break;

        case FROM_SNMP_TO_RIP:
            RipHandleSnmpToRipMesg (pParms->msg.SnmpParms,
                                    pbTriggerIndicator, ppRipCxtEntry);
            break;
        default:
            break;
    }                            /* switch */

    RIP_RELEASE_BUF (pParms->msg.SnmpParms, FALSE);
}                                /* RipHandleControlMessageEvent */

/**************************************************************************/
/*   Function Name   : RipHandleRtmMessageEvent                           */
/*   Description     : This receives the Rtm messages and process them by */
/*                     invoking the respective function.                  */
/*   Input(s)        : None                                               */
/*   Output(s)       :    */
/*                     pbTriggerIndicator - Flag to send the triggered    */
/*                     update                                             */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE void
RipHandleRtmMessageEvent (tProtoQMsg * pParms, BOOLEAN *pbTriggerIndicator)
{
    tRtmMsgHdr          RtmHdr;
    INT4                i4RetStat;

    UNUSED_PARAM (pbTriggerIndicator);
    RIP_BUF_COPY_FROM_CHAIN (pParms->msg.RtmParms, &RtmHdr, 0,
                             sizeof (tRtmMsgHdr));
    i4RetStat = RipProcessPktFromRtm (pParms->msg.RtmParms);
    if (i4RetStat == RIP_FAILURE)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME,
                            "Failure in RipProcessPktFromRtm.......\n");
    }
}                                /* RipHandleRtmMessageEvent */

/**************************************************************************/
/*   Function Name   : RipHandleTimerExpiryEvent                          */
/*   Description     : This receives the RIP packet from the socket,      */
/*                     if it is ready, and process them.                  */
/*   Input(s)        : None                                               */
/*                                                                        */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE void
RipHandleTimerExpiryEvent ()
{
    rip_timer_expiry_handler ();

}                                /* RipHandleTimerEvent */

/**************************************************************************/
/*   Function Name   : RipReceiveAndProcessRipPkts                        */
/*   Description     : This receives the RIP packet from the socket,      */
/*                     if it is ready, and process them.                  */
/*   Input(s)        : None                                               */
/*                                                                        */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE void
RipReceiveAndProcessRipPkts ()
{
    INT4                i4RetStat = IP_SUCCESS;
    UINT1               au1Data[RIP_MAX_LEN];
    INT2                i2Len;
    UINT2               u2IfIndex;
    struct sockaddr_in  RipPeerAddr;
    struct msghdr       PktInfo;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1Cmsg[24];    /* For storing Auxillary Data - IP Packet INFO. */
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo;
#else
    INT4                i4AddrLen = sizeof (RipPeerAddr);
    struct cmsghdr      CmsgInfo;
    tInPktinfo         *pIpPktInfo = NULL;
#endif
    UINT4               u4PeerSAddress;
    UINT4               u4IpPktSAddress;
    UINT2               u2PeerPortAddress;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    INT1                i1TrigInd = FALSE;
    INT1                i1ReturnValue;

    /*** 
     Since with out RTM there is no rip. Till rip gets acknowledgement 
     from all protocol related activities are stoped 
     ***/

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&RipPeerAddr, 0, sizeof (struct sockaddr_in));
    /* Intialising for receiving from any address */

#ifndef BSDCOMP_SLI_WANTED
    PktInfo.msg_control = (VOID *) &CmsgInfo;

    while ((i2Len = (INT2) recvfrom (gRipRtr.i4RipSd,
                                     (UINT1 *) &au1Data,
                                     RIP_MAX_LEN,
                                     0,
                                     (struct sockaddr *) &RipPeerAddr,
                                     &i4AddrLen)) > 0)
    {
        /*
         * Here, we check for the RIP operation flag as to whether it is
         * enabled or disabled and if it is disabled, we will abstain
         * ourself from the input processing.
         */

        i4RetStat = recvmsg (gRipRtr.i4RipSd, &PktInfo, 0);
        if (i4RetStat < 0)
        {
            MEMSET (&RipPeerAddr, 0, sizeof (struct sockaddr_in));
            /* Intialising for receiving from any address */
            continue;
        }
        pIpPktInfo =
            (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u2IfIndex = (UINT2) pIpPktInfo->ipi_ifindex;
        u4IpPktSAddress = pIpPktInfo->ipi_addr.s_addr;
#else /* BSDCOMP_SLI_WANTED */
    MEMSET (au1Data, 0, sizeof (au1Data));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &RipPeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = au1Data;
    Iov.iov_len = sizeof (au1Data);
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    while ((i2Len = recvmsg (gRipRtr.i4RipSd, &PktInfo, 0)) > 0)
    {
        if (i2Len < 0)
        {
            MEMSET (&RipPeerAddr, 0, sizeof (struct sockaddr_in));
            /* Intialising for receiving from any address */
            continue;
        }
        pIpPktInfo =
            (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u2IfIndex = (UINT2) pIpPktInfo->ipi_ifindex;
        u4IpPktSAddress = RIP_NTOHL (pIpPktInfo->ipi_addr.s_addr);
#endif /* SLI_WANTED */

        /* Get the Interface Record from Hash Table */
        if ((pRipCxtEntry = RipGetCxtInfoRecFrmIface (u2IfIndex)) == NULL)
        {
            continue;
        }

        pRipIfRec = RipGetIfRec (u2IfIndex, pRipCxtEntry);

        if (pRipIfRec == NULL)
        {
            /*Interface Record Not Found */
            continue;
        }

        if (pRipIfRec->RipIfaceCfg.u1RipAdminFlag != (UINT1) RIP_ADMIN_DISABLE)
        {
            if (RIPIF_IS_INVALID_ID (pRipIfRec->IfaceId) == TRUE
                || pRipIfRec->RipIfaceCfg.u2AdminStatus != RIPIF_ADMIN_ACTIVE
                || pRipIfRec->RipIfaceCfg.u2OperStatus != RIPIF_OPER_ENABLE)
            {
                /*
                 * Received a packet for an unconfigured interface.
                 */
            }
            else
            {
                u4PeerSAddress = RipPeerAddr.sin_addr.s_addr;
                u4PeerSAddress = RIP_NTOHL (u4PeerSAddress);

                /* If the packet sent by us is received,Dont Process it */
                if (pRipIfRec->u4Addr == u4PeerSAddress)
                {
                    RIP_TRC (RIP_MOD_TRC, pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC,
                             RIP_NAME,
                             "Packet Sent by us, Don't Process it\r\n");
                    MEMSET (&RipPeerAddr, 0, sizeof (struct sockaddr_in));
                    continue;
                }

                u2PeerPortAddress = RipPeerAddr.sin_port;
                i4RetStat = rip_process_input (u2IfIndex,
                                               au1Data,
                                               (UINT2) i2Len,
                                               u4PeerSAddress,
                                               u4IpPktSAddress,
                                               (UINT2) (RIP_NTOHS
                                                        (u2PeerPortAddress)),
                                               pRipCxtEntry);

                if (i4RetStat == IP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             CONTROL_PLANE_TRC,
                             RIP_NAME,
                             "RIP packet processing fails, "
                             "may be due to validation, "
                             "configuration error, "
                             "Memory alloc fail, etc.. \n");
                }
                else if (i4RetStat == RIP_TRIGGER_UPDATE)
                {
                    /*
                     * Set a flag indicating a triggered update is
                     * desired, so that will send once we finish
                     * servicing all the events and this in one way
                     * limits the number of triggered updates.
                     */

                    i1TrigInd = TRUE;

                }                /* End of else of Inteface Configured Check */

            }                    /* End of if part of u1RipAdminFlag check */

            MEMSET (&RipPeerAddr, 0, sizeof (struct sockaddr_in));
            /* Intialising for receiving from any address */
            if (i1TrigInd == TRUE)
            {
                i1ReturnValue = RipSendTriggeredUpdate (pRipCxtEntry);
                if (i1ReturnValue == RIP_FAILURE)
                {
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC,
                             RIP_NAME,
                             "Failure in RipSendTriggeredUpdate.......\n");
                }
                i1TrigInd = FALSE;
            }

        }
    }
}                                /* RipReceiveAndProcessRipPkts */

/**************************************************************************/
/*   Function Name   : RipHandleInterfaceUpMesg                           */
/*   Description     : This receives the interface-up message and does    */
/*                     the necessary action to process it.                */
/*   Input(s)        : pIpParms - To Carry the IP/UDP parameters          */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE INT1
RipHandleInterfaceUpMesg (tIpParms * pIpParms, tRipCxt ** ppRipCxtInfo)
{
    UINT2               u2IfIndex = pIpParms->u2Port;
    INT1                i1ReturnValue = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT1               u1NetType = 0;
    UINT4               u4Index = 0;
    UINT4               u4Addr = 0;
    UINT4               u4NetMask = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4CfaIfIndex = 0;

    if ((pRipCxtEntry =
         RipGetCxtInfoRec ((INT4) pIpParms->u4ContextId)) == NULL)
    {
        i1ReturnValue = (INT1) RIP_FAILURE;
        return i1ReturnValue;
    }
    *ppRipCxtInfo = pRipCxtEntry;
    /* Get the Interface Record from Hash Table */
    u4Addr = RipIfGetAddrFromPort (u2IfIndex);

    /* The interface index is same for the primary IP and the Secondary IP 
     *  enabled on that interface and hence get the index and loop through 
     *  all the Primary and secondary IP to set the interface level properties*/

    do
    {
        pRipIfRec = RipGetIfRecFromAddr (u4Addr, pRipCxtEntry);
        if (pRipIfRec != NULL)
        {
            pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_ENABLE;
            /*Get Interface Index from port number */
            if (NetIpv4GetCfaIfIndexFromPort ((UINT4) u2IfIndex, &u4Index) ==
                NETIPV4_FAILURE)
            {
                return RIP_FAILURE;
            }
            /*Get Network Type from Index number */
            if (CfaGetIfNwType (u4Index, &u1NetType) == CFA_FAILURE)
            {
                return RIP_FAILURE;
            }
            pRipIfRec->u1Persistence = u1NetType;

            if (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_ACTIVE)
            {
                i1ReturnValue = RipIfUpHandleTimers (pRipIfRec);
                if (i1ReturnValue == RIP_EXIT_LOOP)
                {
                    return (INT1) RIP_EXIT_LOOP;
                }
                rip_if_enable (pRipIfRec);
            }
            CLI_CONVERT_IPADDR_TO_STR (pu1String, pRipIfRec->u4Addr);
            NetIpv4GetCfaIfIndexFromPort (((UINT4) pIpParms->u2Port),
                                          &u4CfaIfIndex);
            CfaGetIfName (u4CfaIfIndex, au1IfName);
        }
    }
    while (NetIpv4GetNextSecondaryAddress (u2IfIndex, u4Addr, &u4Addr,
                                           &u4NetMask) == NETIPV4_SUCCESS);

    UNUSED_PARAM (pu1String);
    return i1ReturnValue;
}                                /* RipHandleInterfaceUpMesg */

/**************************************************************************/
/*   Function Name   : RipHandleInterfaceDownMesg                         */
/*   Description     : This receives the interface-up message and does    */
/*                     the necessary action to process it.                */
/*   Input(s)        :          */
/*                              */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE INT1
RipHandleInterfaceDownMesg (tIpParms * pIpParms, tRipCxt ** ppRipCxtInfo)
{
    UINT2               u2IfIndex = pIpParms->u2Port;
    INT1                i1ReturnValue = RIP_SUCCESS;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4Addr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4HashIndex = 0;
    CHR1               *pu1String = NULL;
    UINT4               u4CfaIfIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    if ((pRipCxtEntry = RipGetCxtInfoRecFrmIface (pIpParms->u2Port)) == NULL)
    {
        i1ReturnValue = (INT1) RIP_FAILURE;
        return i1ReturnValue;
    }

    *ppRipCxtInfo = pRipCxtEntry;
    /* Get the Interface Record from Hash Table */
    u4Addr = RipIfGetAddrFromPort (((UINT4) u2IfIndex));

    if (u4Addr == 0xffffffff)
    {
        TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                  pRipIfRec, tRipIfaceRec *)
            {
                if (u2IfIndex == (UINT2) pRipIfRec->IfaceId.u4IfIndex)
                {

                    pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_DISABLE;

                    if (pRipIfRec->RipIfaceCfg.u2AdminStatus ==
                        RIPIF_ADMIN_ACTIVE)
                    {
                        i1ReturnValue = RipIfDownHandleTimers (pRipIfRec);
                        if (i1ReturnValue == RIP_EXIT_LOOP)
                        {
                            return (INT1) RIP_EXIT_LOOP;
                        }
                        rip_if_disable (pRipIfRec);
                    }
                }
            }
        }
    }

    /* The interface index is same for the primary IP and the Secondary IP 
     *  enabled on that interface and hence get the index and loop through 
     *  all the Primary and secondary IP to set the interface level properties*/
    do
    {
        pRipIfRec = RipGetIfRecFromAddr (u4Addr, pRipCxtEntry);
        if (pRipIfRec != NULL)
        {

            pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_DISABLE;

            if (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_ACTIVE)
            {
                i1ReturnValue = RipIfDownHandleTimers (pRipIfRec);
                if (i1ReturnValue == RIP_EXIT_LOOP)
                {
                    return (INT1) RIP_EXIT_LOOP;
                }
                rip_if_disable (pRipIfRec);
            }
            CLI_CONVERT_IPADDR_TO_STR (pu1String, pRipIfRec->u4Addr);
            NetIpv4GetCfaIfIndexFromPort (((UINT4) pIpParms->u2Port),
                                          &u4CfaIfIndex);
            CfaGetIfName (u4CfaIfIndex, au1IfName);
        }
    }
    while (NetIpv4GetNextSecondaryAddress (u2IfIndex, u4Addr, &u4Addr,
                                           &u4NetMask) == NETIPV4_SUCCESS);

    UNUSED_PARAM (pu1String);
    return i1ReturnValue;
}                                /* RipHandleInterfaceDownMesg */

/**************************************************************************/
/*   Function Name   : RipHandleSnmpToRipMesg                             */
/*   Description     : This receives the interface-up message and does    */
/*                     the necessary action to process it.                */
/*   Input(s)        : pbTriggerIndicator -Flag to indicate trigger update*/
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

PRIVATE INT1
RipHandleSnmpToRipMesg (tRIP_BUF_CHAIN_HEADER * pBuf,
                        BOOLEAN *pbTriggerIndicator, tRipCxt ** ppRipCxtInfo)
{
    UINT4               u4BitMask = 0;
    UINT4               u4RrdStatus;
    INT1                i1ReturnValue = RIP_SUCCESS;
    tRipCxt            *pRipCxtEntry = NULL;
    INT4                i4CxtId;

    *pbTriggerIndicator = FALSE;

    if (RIP_BUF_COPY_FROM_CHAIN (pBuf, &u4BitMask, 0, sizeof (u4BitMask))
        != sizeof (u4BitMask))
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            BUFFER_TRC, RIP_NAME,
                            "RipHandleSnmpToRipMesg : "
                            "Error while extracting from buffer \n");
        i1ReturnValue = (INT1) RIP_BUF_COPY_FAILURE;
        return i1ReturnValue;

    }

    if (u4BitMask & SNMP_BIT_RRD_STATUS)
    {
        if (RIP_BUF_COPY_FROM_CHAIN
            (pBuf, &u4RrdStatus, sizeof (u4BitMask),
             sizeof (u4RrdStatus)) != sizeof (u4RrdStatus))
        {
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                BUFFER_TRC, RIP_NAME,
                                "RipHandleSnmpToRipMesg : "
                                "Error while extracting from buffer \n");
            i1ReturnValue = (INT1) RIP_BUF_COPY_FAILURE;
            return i1ReturnValue;

        }
        if (RIP_BUF_COPY_FROM_CHAIN
            (pBuf, &i4CxtId, sizeof (u4BitMask) + sizeof (u4RrdStatus),
             sizeof (i4CxtId)) != sizeof (i4CxtId))
        {
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                BUFFER_TRC, RIP_NAME,
                                "RipHandleSnmpToRipMesg : "
                                "Error while extracting from buffer \n");
            i1ReturnValue = (INT1) RIP_BUF_COPY_FAILURE;
            return i1ReturnValue;
        }
        if ((pRipCxtEntry = RipGetCxtInfoRec (i4CxtId)) == NULL)
        {
            return SNMP_FAILURE;
        }
        *ppRipCxtInfo = pRipCxtEntry;

        if (u4RrdStatus == RIP_RRD_GBL_STAT_ENABLE)
        {
            pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus =
                RIP_RRD_GBL_STAT_ENABLE;

            i1ReturnValue =
                (INT1) RipSendRtmRedistEnableMsgInCxt
                (pRipCxtEntry,
                 (INT2) pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable,
                 pRipCxtEntry->RipRRDGblCfg.au1RMapName);
            if (i1ReturnValue == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         BUFFER_TRC, RIP_NAME,
                         "RipHandleSnmpToRipMesg : "
                         "RipSendRtmRedistEnableMsgInCxt FAILED\n");
            }
        }
        else
        {
            pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus =
                RIP_RRD_GBL_STAT_DISABLE;

            i1ReturnValue =
                (INT1) RipSendRtmRedistDisableMsg ((INT2) RIP_RRD_ALL_MASK,
                                                   pRipCxtEntry);
            if (i1ReturnValue == RIP_TRIGGER_UPDATE)
            {
                *pbTriggerIndicator = TRUE;
            }
            else if (i1ReturnValue == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         BUFFER_TRC, RIP_NAME,
                         "RipHandleSnmpToRipMesg : "
                         "RipSendRtmRedistDisableMsg FAILED \n");
            }

            /* Resetting the RRD Scalar Values */
            pRipCxtEntry->RipRRDGblCfg.u1RipRRDRouteTagType = MANUAL;
            pRipCxtEntry->RipRRDGblCfg.u2RipRRDRtTag = 0;
            pRipCxtEntry->RipRRDGblCfg.u2RipRRDDefMetric
                = RIP_RRD_DEF_RT_METRIC;
            pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable = 0;
            pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable = 0;

            MEMSET (pRipCxtEntry->RipRRDGblCfg.au1RMapName, 0,
                    RMAP_MAX_NAME_LEN);
        }
    }

    if (u4BitMask & SNMP_BIT_SRC_PROTO_MSK_ENA)
    {
        if (RIP_BUF_COPY_FROM_CHAIN
            (pBuf, &i4CxtId, sizeof (u4BitMask),
             sizeof (i4CxtId)) != sizeof (i4CxtId))
        {
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                BUFFER_TRC, RIP_NAME,
                                "RipHandleSnmpToRipMesg : "
                                "Error while extracting from buffer \n");

            i1ReturnValue = (INT1) RIP_BUF_COPY_FAILURE;
            return i1ReturnValue;
        }
        if ((pRipCxtEntry = RipGetCxtInfoRec (i4CxtId)) == NULL)
        {
            return SNMP_FAILURE;
        }
        *ppRipCxtInfo = pRipCxtEntry;
        if (pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus ==
            RIP_RRD_GBL_STAT_ENABLE)
        {
            i1ReturnValue =
                (INT1) RipSendRtmRedistEnableMsgInCxt
                (pRipCxtEntry,
                 (INT2) pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable,
                 pRipCxtEntry->RipRRDGblCfg.au1RMapName);
            if (i1ReturnValue == (INT1) RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         BUFFER_TRC, RIP_NAME,
                         "RipHandleSnmpToRipMesg : "
                         "RipSendRtmRedistEnableMsgInCxt FAILED\n");
            }
        }
        else
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "RipHandleSnmpToRipMesg : "
                     "RipRRDGblStatus is disabled \n");
        }

    }
    if (u4BitMask & SNMP_BIT_SRC_PROTO_MSK_DIS)
    {
        if (RIP_BUF_COPY_FROM_CHAIN
            (pBuf, &i4CxtId, sizeof (u4BitMask),
             sizeof (i4CxtId)) != sizeof (i4CxtId))
        {
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                BUFFER_TRC, RIP_NAME,
                                "RipHandleSnmpToRipMesg : "
                                "Error while extracting from buffer \n");
            i1ReturnValue = (INT1) RIP_BUF_COPY_FAILURE;
            return i1ReturnValue;
        }
        if ((pRipCxtEntry = RipGetCxtInfoRec (i4CxtId)) == NULL)
        {
            return SNMP_FAILURE;
        }
        *ppRipCxtInfo = pRipCxtEntry;

        if (pRipCxtEntry->RipRRDGblCfg.u1RipRRDGblStatus ==
            RIP_RRD_GBL_STAT_ENABLE)
        {
            i1ReturnValue =
                (INT1) RipSendRtmRedistDisableMsg
                ((INT2) pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskDisable,
                 pRipCxtEntry);
            if (i1ReturnValue == RIP_TRIGGER_UPDATE)
            {
                *pbTriggerIndicator = TRUE;
            }
            else if (i1ReturnValue == RIP_FAILURE)
            {
                RIP_TRC (RIP_MOD_TRC,
                         pRipCxtEntry->u4RipTrcFlag,
                         pRipCxtEntry->i4CxtId,
                         BUFFER_TRC, RIP_NAME,
                         "RipHandleSnmpToRipMesg : "
                         "RipSendRtmRedistDisableMsg FAILED \n");
            }
        }
        else
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "RipRRDGblStatus is already " "disabled\n");
        }

    }

    return i1ReturnValue;
}                                /* RipHandleSnmpToRipMesg */

/**************************************************************************/
/*   Function Name   : RipAddLocalRoute                                   */
/*   Description     : This adds a local route to the RIP routing table   */
/*   Input(s)        : u2IfIndex - Interface Index whose direct route is  */
/*                                 to be added                            */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

EXPORT INT1
RipAddLocalRoute (tRipIfaceRec * pRipIfRec)
{
    tRipRtEntry        *pRt = NULL;
    INT4                i4ReturnValue = RIP_SUCCESS;
    UINT4               au4Indx[2];
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    UINT2               u2RtType;
    UINT2               u2IfIndex;
    INT4                i4Metric;
    UINT4               u4Address;
    UINT4               u4Netmask;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4IfAddr = 0;
    UINT4               u4NextIfAddr = 0;
    UINT4               u4netMask = 0;

    u2IfIndex = (UINT2) RIP_GET_IFACE_INDEX (pRipIfRec->IfaceId);

    pRipCxtEntry = pRipIfRec->pRipCxt;
    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }

    u4Address = RIPIF_GLBTAB_ADDR_OF (((UINT4) u2IfIndex));
    u4Netmask = RIPIF_GLBTAB_MASK_OF (u2IfIndex);
    if (u4Address != pRipIfRec->u4Addr)
    {
        /* Need to get the secondary IPAddr */
        while (NetIpv4GetNextSecondaryAddress (u2IfIndex, u4IfAddr,
                                               &u4NextIfAddr,
                                               &u4netMask) == NETIPV4_SUCCESS)
        {
            if ((u4NextIfAddr == pRipIfRec->u4Addr)
                && (u4netMask == pRipIfRec->u4NetMask))
            {
                u4Address = u4NextIfAddr;
                u4Netmask = u4netMask;
                break;
            }
            u4IfAddr = u4NextIfAddr;
        }
    }
/*  for unnumbered interfaces there will not be any network ,
 *  so no need to add local route for these kind of interfaces */
    if ((u4Address == 0) && (u4Netmask == 0))
    {
        return RIP_SUCCESS;
    }

    au4Indx[0] = RIP_HTONL (u4Address & u4Netmask);
    au4Indx[1] = RIP_HTONL (u4Netmask);

    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = CIDR_LOCAL_ID - 1;
    InParams.Key.pKey = (UINT1 *) au4Indx;

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, 16 * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    i4ReturnValue = TrieSearchEntry (&InParams, &OutParams);
    u2RtType = FSIP_LOCAL;
    i4Metric = (INT4) RIP_DEFAULT_METRIC;

    if (i4ReturnValue == TRIE_SUCCESS)
    {
        return (INT1) (RipHandleLocalRouteModification (pRt, u2RtType, i4Metric,
                                                        (UINT4) u2IfIndex,
                                                        &OutParams,
                                                        pRipCxtEntry));
    }
    RIP_ALLOC_LOCAL_ROUTE_ENTRY (pRt);
    if (pRt == NULL)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 CONTROL_PLANE_TRC, RIP_NAME,
                 "RipAddLocalRoute: RIP_ALLOC_ROUTE_ENTRY FAILURE");
        return (INT1) RIP_NO_ROOM;
    }
    MEMSET (pRt, 0, sizeof (tRtInfo));
    pRt->RtInfo.u2RtType = FSIP_LOCAL;
    pRt->RtInfo.u2RtProto = (UINT2) CIDR_LOCAL_ID;
    pRt->RtInfo.i4Metric1 = (INT4) RIP_DEFAULT_METRIC;
    pRt->RtInfo.u4RtNxtHopAS = 0;    /* 0 For Local Routes */
    pRt->RtInfo.u4DestNet = u4Address & u4Netmask;
    pRt->RtInfo.u4DestMask = u4Netmask;
    pRt->RtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
    pRt->RtInfo.u4RtIfIndx = u2IfIndex;
    pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
    pRt->RtInfo.u4SrcAddr = pRipIfRec->RipIfaceCfg.u4SrcAddr;

    i4ReturnValue = rip_add_new_route (pRt, pRipCxtEntry);

    if (i4ReturnValue == (INT4) RIP_FAILURE)
    {
        RIP_STOP_TIMER (RIP_TIMER_ID, &(RIP_ROUTE_TIMER_NODE (pRt)));
        RIP_FREE_LOCAL_ROUTE_ENTRY (pRt);
    }
    else
    {

        rip_generate_update_message (RIP_VERSION_ANY,
                                     RIP_TRIG_UPDATE,
                                     IP_GEN_BCAST_ADDR,
                                     UDP_RIP_PORT,
                                     RIPIF_INVALID_INDEX, pRipCxtEntry);
    }

    UNUSED_PARAM (u2RtType);
    UNUSED_PARAM (i4Metric);
    return (INT1) i4ReturnValue;
}

/**************************************************************************/
/*   Function Name   : RipDeleteLocalRoute                                */
/*   Description     : This deletes a direct route from the RIP local     */
/*                     routing table                                      */
/*   Input(s)        : u2IfIndex - Interface index whose route is to be   */
/*                                 deleted                                */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
EXPORT INT1
RipDeleteLocalRoute (tRipIfaceRec * pRipIfRec)
{
    tRipRtEntry        *pRt = NULL;
    INT4                i4ReturnValue = RIP_SUCCESS;
    UINT4               au4Indx[2];
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *apAppSpecInfo[16];
    UINT4               u4Address;
    UINT4               u4Netmask;
    tRipCxt            *pRipCxtEntry = NULL;

    u4Address = pRipIfRec->u4Addr;
    u4Netmask = pRipIfRec->u4NetMask;
    pRipCxtEntry = pRipIfRec->pRipCxt;

    if (pRipCxtEntry == NULL)
    {
        return RIP_FAILURE;
    }
    au4Indx[0] = RIP_HTONL (u4Address & u4Netmask);
    au4Indx[1] = RIP_HTONL (u4Netmask);

    InParams.pRoot = pRipCxtEntry->pRipRoot;
    InParams.i1AppId = CIDR_LOCAL_ID - 1;
    InParams.Key.pKey = (UINT1 *) au4Indx;

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, 16 * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    i4ReturnValue = TrieSearchEntry (&InParams, &OutParams);

    if (i4ReturnValue == TRIE_SUCCESS)
    {
        pRt = (tRipRtEntry *) OutParams.pAppSpecInfo;
        /* if the metric is already set to infinity, assumed
         * that the garbage collection has already been initiated
         * this can happen, when the interface goes down for a rip
         * interface that is already in disabled state.
         * or if we invoke 'no rip' for an operationally down interface
         */
        if ((pRt != NULL) && (pRt->RtInfo.i4Metric1 != RIP_INFINITY))
        {
            pRt->RtInfo.i4Metric1 = (INT4) RIP_INFINITY;
            pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
            pRt->RtInfo.u4RowStatus = IPFWD_NOT_IN_SERVICE;
            RipUpdateRouteEntry (pRt, pRipCxtEntry);

            /*
             * The timer structure for RouteAge calculation that
             * needs to be initialised to garbage collect interval
             */
            RIP_ROUTE_TIMER_ROUTE (pRt) = (void *) pRt;
            RIP_ROUTE_TIMER_ID (pRt) = (UINT1) RIP_RT_TIMER_ID;
            RIP_START_TIMER (RIP_TIMER_ID,
                             &(RIP_ROUTE_TIMER_NODE (pRt)),
                             RIP_DEF_GARBAGE_COLCT_INTERVAL);

            i4ReturnValue = rip_generate_update_message (RIP_VERSION_ANY,
                                                         RIP_TRIG_UPDATE,
                                                         IP_GEN_BCAST_ADDR,
                                                         UDP_RIP_PORT,
                                                         RIPIF_INVALID_INDEX,
                                                         pRipCxtEntry);
        }
    }
    return (INT1) i4ReturnValue;
}

INT4
RipAttachRtmReceivedMessage (tRtmRespInfo * pRespInfo, tRtmMsgHdr * pRtmHdr)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tExpRtNode         *pRtmRouteMsg = NULL;
    tRipCxt            *pRipCxtEntry = NULL;

    if (pRtmHdr->u1MessageType == RTM_REGISTRATION_ACK_MESSAGE)
    {
        pBuf = RIP_ALLOCATE_BUF ((UINT4) sizeof (tRtmRegnAckMsg), 0);
        if (pBuf == NULL)
        {
            return RTM_FAILURE;
        }

        if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pRespInfo->pRegnAck, 0,
                                       (UINT4) sizeof (tRtmRegnAckMsg)) ==
            CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return RTM_FAILURE;
        }

    }
    else
    {
        if (RIP_ALLOC_RTM_ROUTE (pRtmRouteMsg) == NULL)
        {
            return (RTM_FAILURE);
        }

        MEMCPY (&(pRtmRouteMsg->RtInfo), pRespInfo->pRtInfo,
                sizeof (tNetIpv4RtInfo));
        /* Take the DataLock for accessing RTM Route List */
#ifdef TEST_RRD_WANTED
        if (NetIpv4GetCxtId (pRtmRouteMsg->RtInfo.u4RtIfIndx,
                             &pRtmRouteMsg->RtInfo.u4ContextId) ==
            NETIPV4_FAILURE)
        {
            RIP_FREE_RTM_ROUTE (pRtmRouteMsg);
            return RTM_FAILURE;
        }
#endif
        pRipCxtEntry =
            RipGetCxtInfoRec ((INT4) pRtmRouteMsg->RtInfo.u4ContextId);
        if (pRipCxtEntry == NULL)
        {
            RIP_FREE_RTM_ROUTE (pRtmRouteMsg);
            return RTM_FAILURE;
        }
        OsixSemTake (gRipRtr.RipRtmLstSemId);
        TMO_SLL_Add (&gRipRtr.RtmRtLst, &(pRtmRouteMsg->NextRt));
        /* Release the DataLock for accessing RTM Route List */
        OsixSemGive (gRipRtr.RipRtmLstSemId);

        OsixSendEvent (RIP_TASK_NODE_ID, RIP_TASK_NAME, RIP_RTM_RTMSG_EVENT);

        return RTM_SUCCESS;
    }

    MEMCPY (IP_GET_MODULE_DATA_PTR (pBuf), pRtmHdr, sizeof (tRtmMsgHdr));

    if (RipSendToQ ((void *) pBuf, RIP_RRD_MSG) != (UINT4) RIP_SUCCESS)
    {
        RIP_RELEASE_BUF (pBuf, FALSE);
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/**************************************************************************/
/*   Function Name   : RipHandleRouteModification                         */
/*   Description     : This function checks whether there is any change   */
/*                     in the route. If there is any change, check the    */
/*                     possibility of valid route becoming invalid,       */
/*                     invalid becoming valid, valid remaining as valid,  */
/*                     invalid remaining as invalid. Generate trig update */
/*                     accordingly. Modify the route entry appropriately. */
/*   Input(s)        : Route entry & route parameters                     */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT4
RipHandleRouteModification (tRipRtEntry * pRt, UINT4 u4Tos, UINT4 u4NextHop,
                            UINT2 u2RtType, INT4 i4Metric1, UINT4 u4RtIfIndx,
                            tOutputParams * pOutParams)
{
    INT4                i4ReturnValue = RIP_SUCCESS;
    tRipCxt            *pRipCxtEntry = NULL;
    pRt = (tRipRtEntry *) pOutParams->pAppSpecInfo;

    for (; pRt; pRt = pRt->pNextAlternatepath)
    {
        if (pRt->RtInfo.u4NextHop == u4NextHop && pRt->RtInfo.u4Tos == u4Tos)
        {
            break;
        }
    }
    if (pRt != NULL)
    {
        pRipCxtEntry =
            RipGetCxtInfoRecFrmIface ((UINT2) (pRt->RtInfo.u4RtIfIndx));
        if (pRipCxtEntry == NULL)
        {
            return RTM_FAILURE;
        }
        /* Static route with same nexthop and tos exists. No need
         * to add this entry. Check whether we need to modify the entry */
        if ((pRt->RtInfo.u2RtType == u2RtType) &&
            (pRt->RtInfo.i4Metric1 == i4Metric1) &&
            (pRt->RtInfo.u4RtIfIndx == u4RtIfIndx))
        {
            /*All params are same. No change is route. Return failure since
             * the route is not added */
            i4ReturnValue = RIP_FAILURE;
        }
        else
        {
            i4ReturnValue = RIP_SUCCESS;
            /* Overwrite the parameters, assuming that the caller had
             * already validated the parameters */
            pRt->RtInfo.u2RtType = u2RtType;
            pRt->RtInfo.u4RtIfIndx = u4RtIfIndx;
            if (pRt->RtInfo.i4Metric1 == RIP_INFINITY)
            {
                /* Old route is invalid */
                if (i4Metric1 < RIP_INFINITY)
                {
                    /* Invalid route has become valid now. */
                    /* Set new metric, stop garbage timer, send trig upd */
                    pRt->RtInfo.i4Metric1 = i4Metric1;
                    pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
                    RIP_STOP_TIMER (RIP_TIMER_ID,
                                    &(RIP_ROUTE_TIMER_NODE (pRt)));
                    i4ReturnValue =
                        rip_generate_update_message (RIP_VERSION_ANY,
                                                     RIP_TRIG_UPDATE,
                                                     IP_GEN_BCAST_ADDR,
                                                     UDP_RIP_PORT,
                                                     RIPIF_INVALID_INDEX,
                                                     pRipCxtEntry);
                }
                /* Else, invalid Rt is still invalid,No change, no action */
            }
            else
            {
                /* Old route is valid */
                if (i4Metric1 == RIP_INFINITY)
                {
                    /* Valid route has become invalid now. */
                    /* Set new metric, start garbage timer, send trig upd */
                    pRt->RtInfo.i4Metric1 = i4Metric1;
                    pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &(RIP_ROUTE_TIMER_NODE (pRt)),
                                     RIP_DEF_GARBAGE_COLCT_INTERVAL);
                    i4ReturnValue =
                        rip_generate_update_message (RIP_VERSION_ANY,
                                                     RIP_TRIG_UPDATE,
                                                     IP_GEN_BCAST_ADDR,
                                                     UDP_RIP_PORT,
                                                     RIPIF_INVALID_INDEX,
                                                     pRipCxtEntry);
                }
                else
                {
                    /* Valid route is still valid. */
                    if ((pRt->RtInfo.i4Metric1 != i4Metric1) &&
                        (i4Metric1 < RIP_INFINITY))
                    {
                        /* Set new metric, send trig upd */
                        pRt->RtInfo.i4Metric1 = i4Metric1;
                        pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
                        i4ReturnValue =
                            rip_generate_update_message (RIP_VERSION_ANY,
                                                         RIP_TRIG_UPDATE,
                                                         IP_GEN_BCAST_ADDR,
                                                         UDP_RIP_PORT,
                                                         RIPIF_INVALID_INDEX,
                                                         pRipCxtEntry);
                    }
                }
            }
        }
    }
    return i4ReturnValue;
}

/**************************************************************************/
/*   Function Name   : RipHandleLocalRouteModification                         */
/*   Description     : This function checks whether there is any change   */
/*                     in the route. If there is any change, check the    */
/*                     possibility of valid route becoming invalid,       */
/*                     invalid becoming valid, valid remaining as valid,  */
/*                     invalid remaining as invalid. Generate trig update */
/*                     accordingly. Modify the route entry appropriately. */
/*   Input(s)        : Route entry & route parameters                     */
/*   Output(s)       : None                                               */
/*   Return Value    : RIP_SUCCESS or RIP_FAILURE                         */
/**************************************************************************/
INT4
RipHandleLocalRouteModification (tRipRtEntry * pRt, UINT2 u2RtType,
                                 INT4 i4Metric1, UINT4 u4RtIfIndx,
                                 tOutputParams * pOutParams,
                                 tRipCxt * pRipCxtEntry)
{
    INT4                i4ReturnValue = RIP_SUCCESS;

    pRt = (tRipRtEntry *) pOutParams->pAppSpecInfo;

    if (pRt != NULL)
    {

        /* Check whether we need to modify the entry */
        if ((pRt->RtInfo.u2RtType == u2RtType) &&
            (pRt->RtInfo.i4Metric1 == i4Metric1) &&
            (pRt->RtInfo.u4RtIfIndx == u4RtIfIndx))
        {
            /*All params are same. No change in route. Return failure since
             * the route is not added */
            i4ReturnValue = RIP_FAILURE;
        }
        else
        {
            i4ReturnValue = RIP_SUCCESS;
            /* Overwrite the parameters, assuming that the caller had
             * already validated the parameters */
            pRt->RtInfo.u2RtType = u2RtType;
            pRt->RtInfo.u4RtIfIndx = u4RtIfIndx;
            if (pRt->RtInfo.i4Metric1 == RIP_INFINITY)
            {
                /* Old route is invalid */
                if (i4Metric1 < RIP_INFINITY)
                {
                    /* Invalid route has become valid now. */
                    /* Set new metric, stop garbage timer, send trig upd */
                    pRt->RtInfo.i4Metric1 = i4Metric1;
                    pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
                    pRt->RtInfo.u4RowStatus = IPFWD_ACTIVE;
                    RIP_STOP_TIMER (RIP_TIMER_ID,
                                    &(RIP_ROUTE_TIMER_NODE (pRt)));
                    i4ReturnValue =
                        rip_generate_update_message (RIP_VERSION_ANY,
                                                     RIP_TRIG_UPDATE,
                                                     IP_GEN_BCAST_ADDR,
                                                     UDP_RIP_PORT,
                                                     RIPIF_INVALID_INDEX,
                                                     pRipCxtEntry);
                }
                /* Else, invalid Rt is still invalid,No change, no action */
            }
            else
            {
                /* Old route is valid */
                if (i4Metric1 == RIP_INFINITY)
                {
                    /* Valid route has become invalid now. */
                    /* Set new metric, start garbage timer, send trig upd */
                    pRt->RtInfo.i4Metric1 = i4Metric1;
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &(RIP_ROUTE_TIMER_NODE (pRt)),
                                     RIP_DEF_GARBAGE_COLCT_INTERVAL);
                    pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
                    i4ReturnValue =
                        rip_generate_update_message (RIP_VERSION_ANY,
                                                     RIP_TRIG_UPDATE,
                                                     IP_GEN_BCAST_ADDR,
                                                     UDP_RIP_PORT,
                                                     RIPIF_INVALID_INDEX,
                                                     pRipCxtEntry);
                }
                else
                {
                    /* Valid route is still valid. */
                    if ((pRt->RtInfo.i4Metric1 != i4Metric1) &&
                        (i4Metric1 < RIP_INFINITY))
                    {
                        /* Set new metric, send trig upd */
                        pRt->RtInfo.i4Metric1 = i4Metric1;
                        pRt->RtInfo.u1Status = (UINT1) RIP_RT_CHANGED_VAL;
                        i4ReturnValue =
                            rip_generate_update_message (RIP_VERSION_ANY,
                                                         RIP_TRIG_UPDATE,
                                                         IP_GEN_BCAST_ADDR,
                                                         UDP_RIP_PORT,
                                                         RIPIF_INVALID_INDEX,
                                                         pRipCxtEntry);
                    }
                }
            }
        }
    }
    return i4ReturnValue;
}

/****************************************************************************
 Function    :  RipHandleInterfaceDeleteMesg 
 Input       :  u2Port 
                
 Output      : The RIP interface corresponding to the port received is deleted.
                
 Returns     :  RIP_SUCCESS 
****************************************************************************/
PRIVATE INT1
RipHandleInterfaceDeleteMesg (tIpParms * pIpParms, tRipCxt ** ppRipCxtInfo)
{

    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pRipNextIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4Network = 0;
    INT1                i1RetVal = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4CfaIfIndex = 0;
    pRipCxtEntry = RipGetCxtInfoRec ((INT4) pIpParms->u4ContextId);
    if (NULL == pRipCxtEntry)
    {
        return RIP_FAILURE;
    }
    *ppRipCxtInfo = pRipCxtEntry;

    /* Get the Interface Record from Hash Table */
    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_DYN_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                  pRipIfRec, pRipNextIfRec, tRipIfaceRec *)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1String, pRipIfRec->u4Addr);
            NetIpv4GetCfaIfIndexFromPort (((UINT4) pIpParms->u2Port),
                                          &u4CfaIfIndex);
            CfaGetIfName (u4CfaIfIndex, au1IfName);
            RIP_TRC_ARG3 (RIP_MOD_TRC,
                          (*ppRipCxtInfo)->u4RipTrcFlag,
                          (*ppRipCxtInfo)->i4CxtId,
                          RIP_CRITICAL_TRC,
                          RIP_NAME,
                          "Handled interface-Delete message recieved for Route=%s NetMask=%d InterfaceIndex=%s\n\r",
                          pu1String, CliGetMaskBits (pRipIfRec->u4NetMask),
                          au1IfName);
            if (pIpParms->u2Port == (UINT2) pRipIfRec->IfaceId.u4IfIndex)
            {
                u4Network = pRipIfRec->u4Addr;
                rip_if_destroy (pRipIfRec);
                i1RetVal = RipSetIfConfStatus (u4Network, DESTROY);
            }
        }
    }
    UNUSED_PARAM (i1RetVal);
    return RIP_SUCCESS;

}

/****************************************************************************
 Function    :  RipHandleInterfaceIpAddrChg 
 Input       :  u2Port 
                
 Output      : The RIP interface corresponding to the port received is deleted.
                
 Returns     :  RIP_SUCCESS 
****************************************************************************/

PRIVATE INT1
RipHandleInterfaceIpAddrChg (tIpParms * pIpParms, tRipCxt ** ppRipCxtInfo)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipCxt            *pRipCxtEntry = NULL;

    if (pIpParms == NULL)
    {
        RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                            RIP_TRC_GLOBAL_FLAG,
                            RIP_INVALID_CXT_ID,
                            ALL_FAILURE_TRC,
                            RIP_NAME, "Received NULL Parameters\n");
        return RIP_FAILURE;
    }
    if ((pRipCxtEntry = RipGetCxtInfoRecFrmIface (pIpParms->u2Port)) == NULL)
    {
        return RIP_FAILURE;
    }
    *ppRipCxtInfo = pRipCxtEntry;
    pRipIfRec = RipGetIfRec (pIpParms->u2Port, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      pRipCxtEntry->u4RipTrcFlag,
                      pRipCxtEntry->i4CxtId,
                      RIP_CRITICAL_TRC,
                      RIP_NAME,
                      "Received a RIP packet on interface %d "
                      "Not Exists\n", (pIpParms->u2Port));
        return RIP_FAILURE;
    }
    RipClearIfInfo (pRipIfRec);

    return RIP_SUCCESS;
}

/****************************************************************************
 Function    :  RipCreatCxtEntry
 Description :  Creates context entry and initializes with default values.
 Input       :  None.                
 Output      :  None.                
 Returns     :  RIP_SUCCESS on success otherwise RIP_FAILURE.
****************************************************************************/
INT4
RipCreateCxtEntry (tRipCxt * pRipCxtEntry)
{

    if (RipInitializeToDefaultValues (pRipCxtEntry) == RIP_FAILURE)
    {
        return RIP_FAILURE;
    }
    gRipRtr.apRipCxt[pRipCxtEntry->i4CxtId] = pRipCxtEntry;
    return RIP_SUCCESS;
}

/****************************************************************************
 Function    :  RipDelCxtEntry
 Description :  Deletes context entry and releases the memory used.
 Input       :  None.                
 Output      :  None.                
 Returns     :  RIP_SUCCESS on success otherwise RIP_FAILURE.
****************************************************************************/
INT4
RipDelCxtEntry (tRipCxt * pRipCxtEntry)
{

    UINT4               u4RipIfAddr = 0;
    UINT4               u4RipPrevIfAddr = 0;
    INT4                i4NextCxtId = pRipCxtEntry->i4CxtId;
    INT4                i4CxtId = pRipCxtEntry->i4CxtId;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRIP_INTERFACE      IfId;
    INT4                i4RetIf = 0;

    while (nmhGetNextIndexFsMIStdRip2IfConfTable
           (i4NextCxtId, &i4NextCxtId, u4RipPrevIfAddr,
            &u4RipIfAddr) == SNMP_SUCCESS)
    {
        if (i4CxtId != i4NextCxtId)
        {
            break;
        }
        if ((i4RetIf =
             RIPIF_GET_IFINDEX_FROM_ADDR_IN_CXT ((UINT4) pRipCxtEntry->i4CxtId,
                                                 u4RipIfAddr)) == RIP_FAILURE)
        {
            /* the L3 interface itself is not configured */
            RIP_TRC (RIP_MOD_TRC,
                     RIP_TRC_GLOBAL_FLAG,
                     RIP_INVALID_CXT_ID,
                     CONTROL_PLANE_TRC, RIP_NAME,
                     "EXITED : L3 interface itself is not configured");
            return RIP_FAILURE;
        }
        RIPIF_GET_IFID_FROM_INDEX ((UINT4) i4RetIf, &IfId);
        /* Get the Interface Record from Hash Table */
        pRipIfRec = RipGetIfRecFromAddr (u4RipIfAddr, pRipCxtEntry);

        if (pRipIfRec != NULL)
        {
            rip_if_destroy (pRipIfRec);
        }
        u4RipPrevIfAddr = u4RipIfAddr;
    }
    pRipCxtEntry->u1AdminStatus = RIP_ADMIN_DISABLE;
    RipDeleteRtTbl (pRipCxtEntry);

    if (RipClearCxtInfo (pRipCxtEntry) == RIP_FAILURE)
    {
        return (RIP_FAILURE);
    }

    if (RipDeRegisterWithOthersInCxt (i4CxtId) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "FAILED while deregestering RIP protocol from others");
        return (RIP_FAILURE);
    }
    gRipRtr.apRipCxt[i4CxtId] = NULL;
    RIP_FREE_CXT_ENTRY (pRipCxtEntry);

    return RIP_SUCCESS;
}

/****************************************************************************
 Function    :  RipHandleVcmMessageEvent. 
 Description :  Receives the event from Vcm and calls correspoding
                functions..
 Input       :  None.                
 Output      :  None.                
 Returns     :  RIP_SUCCESS on success otherwise RIP_FAILURE.
****************************************************************************/
PRIVATE VOID
RipHandleVcmMessageEvent (tProtoQMsg * pParms)
{
    tRipParams         *pRipParms = NULL;
    tRipCxt            *pRipCxtEntry = NULL;

    pRipParms = (tRipParams *) RIP_GET_MODULE_DATA_PTR (pParms->msg.RipParms);

    if ((pRipCxtEntry = RipGetCxtInfoRec (pRipParms->i4CxtId)) == NULL)
    {
        RIP_RELEASE_BUF (pParms->msg.RipParms, FALSE);
        return;
    }
    UNUSED_PARAM (pRipCxtEntry);

    if (pRipParms->u1Chg != VCM_CXT_STATUS_CHG_REQ)
    {
        RIP_RELEASE_BUF (pParms->msg.RipParms, FALSE);
        return;
    }

    /* Received context delete indication from VCM. So delete the rip
     * context.
     */
    RipDelCxt (pRipParms->i4CxtId);
    RIP_RELEASE_BUF (pParms->msg.RipParms, FALSE);
}

/****************************************************************************
 Function    :  RipSendRespPktToQ. 
 Description :  Sends the response packet to the response Queue.
 Input       :  None.                
 Output      :  None.                
 Returns     :  RIP_SUCCESS on success otherwise RIP_FAILURE.
****************************************************************************/
INT4
RipSendRespPktToQ (tRip * pRipHdr, tRipInfo * pRipInfo,
                   UINT2 u2Len, UINT4 u4Src, UINT2 u2If,
                   UINT4 u4SeqNo, UINT1 u1KeyId, tRipCxt * pRipCxtEntry)
{
    tRipRespQMsg       *pRespMsg = NULL;
    UINT2               u2NoOfRoutes =
        (UINT2) ((u2Len - RIP_HDR_LEN) / RIP_INFO_LEN);
    if (u2NoOfRoutes > RIP_MAX_ROUTES_PER_PKT)
    {
        return RIP_FAILURE;
    }

    if (RIP_ALLOC_RESP_ENTRY (pRespMsg) != NULL)
    {
        pRespMsg->u1Version = pRipHdr->u1Version;
        pRespMsg->u2Len = u2Len;
        MEMCPY (pRespMsg->aRipRtInfo, pRipInfo,
                sizeof (tRipInfo) * u2NoOfRoutes);
        pRespMsg->u4SrcAddr = u4Src;
        pRespMsg->u2IfIndex = u2If;
        pRespMsg->u4SeqNo = u4SeqNo;
        pRespMsg->u1KeyId = u1KeyId;
        pRespMsg->u4CxtId = (UINT4) pRipCxtEntry->i4CxtId;

        if (OsixSendToQ ((UINT4) 0,
                         RIP_RESP_Q_NAME, (tOsixMsg *) (VOID *) pRespMsg,
                         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
        {
            RIP_TRC_INV_CXT_ID (RIP_MOD_TRC,
                                RIP_TRC_GLOBAL_FLAG,
                                RIP_INVALID_CXT_ID,
                                ALL_FAILURE_TRC, RIP_NAME,
                                "RipSendRespPktToQ: OsixSendTo failed to"
                                "send Response Message. \n");
            RIP_FREE_RESP_ENTRY (pRespMsg);
            return RIP_FAILURE;

        }

    }
    return RIP_SUCCESS;
}

/****************************************************************************
 Function    :  RipDisableAdminStat
 Description :  Disable the context..
 Input       :  pRipCxtEntry - Context entry Info.                
 Output      :  Deletes the all the routes in the context.                
 Returns     :  RIP_SUCCESS on success otherwise RIP_FAILURE.
****************************************************************************/
INT4
RipDisableAdminStat (tRipCxt * pRipCxtEntry)
{
    UINT4               u4HashIndex = 0;
    tRipIfaceRec       *pRipIfRec = NULL;
    INT1                i1IsDC = RIP_SUCCESS;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_DISABLE;

            if (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_ACTIVE)
            {
                i1IsDC =
                    RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
                if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                    && (i1IsDC != RIP_SUCCESS))
                {
                    RIPIF_FILL_SUBSCRIP_TIMER (pRipIfRec,
                                               (UINT1) RIP_CFG_SUBSCRIP_TIME
                                               (pRipCxtEntry));
                    RIP_START_TIMER (RIP_TIMER_ID,
                                     &pRipIfRec->RipSubscrTimer.TimerNode,
                                     RIP_CFG_SUBSCRIP_TIME (pRipCxtEntry));

                    if (RIP_STOP_TIMER (RIP_TIMER_ID,
                                        &pRipIfRec->RipUpdateTimer.TimerNode)
                        == TMR_SUCCESS)
                    {
                        pRipIfRec->RipUpdateTimer.u1Data &= RIP_RUNIN_TMR;
                    }

                    if (pRipIfRec->RipSpacingTimer.u1Data != 0)
                    {
                        RIP_STOP_TIMER (RIP_TIMER_ID,
                                        &pRipIfRec->RipSpacingTimer.TimerNode);
                    }
                }
                else
                {
                    rip_if_disable (pRipIfRec);
                }
            }
        }
    }
    /* Stop the timers before clearing the context */
    RIP_STOP_TIMER (RIP_TIMER_ID,
                    &((pRipCxtEntry->RipPassiveUpdTimer).Timer_node));
    /* Initialize the triggered timer limiting variables to their startup
     * values.
     */
    pRipCxtEntry->RipGblCfg.u1TrigUpdFlag = RIP_TRIG_TIMER_INACTIVE;
    pRipCxtEntry->RipGblCfg.u1TrigDesired = RIP_TRIG_UPD_NOT_DESIRED;
    RIP_STOP_TIMER (RIP_TIMER_ID,
                    &((pRipCxtEntry->RipTrigUpdTimer).Timer_node));
    pRipCxtEntry->TripTrigTmr.u1Data = RIP_TRIG_TIMER_INACTIVE;
    RIP_STOP_TIMER (RIP_TIMER_ID, &((pRipCxtEntry->TripTrigTmr).TimerNode));
    RipDeleteRtTbl (pRipCxtEntry);

    if (RipDeRegisterWithOthersInCxt (pRipCxtEntry->i4CxtId) == RIP_FAILURE)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "FAILED while deregestering RIP protocol from others");
        return (RIP_FAILURE);
    }

    pRipCxtEntry->u1AdminStatus = RIP_ADMIN_DISABLE;
    return RIP_SUCCESS;
}

/****************************************************************************
 Function    :  RipEnableAdminStat
 Description :  Enables the context..
 Input       :  pRipCxtEntry - Context entry Info.                
 Output      :  Enables RIP on the context interfaces.                
 Returns     :  RIP_SUCCESS on success otherwise RIP_FAILURE.
****************************************************************************/
INT4
RipEnableAdminStat (tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;
    INT1                i1IsDC = RIP_SUCCESS;

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (pRipCxtEntry != pRipIfRec->pRipCxt)
            {
                continue;
            }
            pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_ENABLE;

            if (pRipIfRec->RipIfaceCfg.u2AdminStatus == RIPIF_ADMIN_ACTIVE)
            {
                i1IsDC =
                    RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);
                if (((pRipIfRec->u1Persistence == RIP_WAN_TYPE)
                     && (i1IsDC != RIP_SUCCESS))
                    && (pRipIfRec->RipSubscrTimer.u1Data != 0))
                {
                    RIP_STOP_TIMER (RIP_TIMER_ID,
                                    &pRipIfRec->RipSubscrTimer.TimerNode);
                    pRipIfRec->RipSubscrTimer.u1Data = 0;

                    if (pRipIfRec->RipUpdateTimer.u1Data & RIP_RUNIN_TMR)
                    {
                        pRipIfRec->RipUpdateTimer.u1Data &=
                            (UINT1) ~RIP_RUNIN_TMR;
                        RIP_START_TIMER (RIP_TIMER_ID,
                                         &pRipIfRec->RipUpdateTimer.TimerNode,
                                         RIP_CFG_GET_RETX_TIMEOUT
                                         (pRipCxtEntry));
                    }

                    if (pRipIfRec->RipSpacingTimer.u1Data != 0)
                    {

                        RIP_START_TIMER (RIP_TIMER_ID,
                                         &pRipIfRec->RipSpacingTimer.
                                         TimerNode,
                                         RIP_CFG_GET_RETX_TIMEOUT
                                         (pRipCxtEntry));
                    }
                }
                else
                {
                    rip_if_enable (pRipIfRec);
                }
            }

        }
    }
    if (RipRegisterWithOthers (pRipCxtEntry) != RTM_SUCCESS)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "FAILED while Registering RIp with OTHER PROTOCOL");
        return RIP_FAILURE;
    }
    if (pRipCxtEntry->RipRRDGblCfg.u2SrcProtMskEnable != 0)
    {
        if (RipSendRtmRedistEnableMsgInCxt (pRipCxtEntry,
                                            (INT2) pRipCxtEntry->RipRRDGblCfg.
                                            u2SrcProtMskEnable,
                                            pRipCxtEntry->RipRRDGblCfg.
                                            au1RMapName) == RIP_FAILURE)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC, RIP_NAME,
                     "ERROR in redistribution of particular protocol route learnt");
            return RIP_FAILURE;
        }

    }
    pRipCxtEntry->u1AdminStatus = RIP_ADMIN_ENABLE;
    return RIP_SUCCESS;
}

/****************************************************************************
 Function    :  RipDelMemInit
 Description :  Deletes the Allocated memory pools.
 Input       :  None.               
 Output      :  Deletes the Allocates Memory Pools.                
 Returns     :  None.
****************************************************************************/
VOID
RipDelMemInit ()
{
    tOsixQId            RipInputQId;
    tOsixQId            RipRespQId;

    RipSizingMemDeleteMemPools ();

    if (gRipRtr.RipSemId != 0)
    {
        OsixSemDel (gRipRtr.RipSemId);
    }
    if (gRipRtr.RipRtmLstSemId != 0)
    {
        OsixSemDel (gRipRtr.RipRtmLstSemId);
    }
    if (OsixGetQId (RIP_INPUT_Q_NODE_ID,
                    RIP_INPUT_Q_NAME, &RipInputQId) == OSIX_SUCCESS)
    {
        OsixDeleteQ (RIP_INPUT_Q_NODE_ID, RIP_INPUT_Q_NAME);
    }
    if (OsixGetQId ((UINT4) 0, RIP_RESP_Q_NAME, &RipRespQId) == OSIX_SUCCESS)
    {
        OsixDeleteQ ((UINT4) 0, RIP_RESP_Q_NAME);
    }
    if (RIP_TIMER_ID != 0)
    {
        TmrDeleteTimerList (RIP_TIMER_ID);
    }

    /* Delete Interface Hash Table */
    if (gRipRtr.pGIfHashTbl != NULL)
    {
        TMO_HASH_Delete_Table (gRipRtr.pGIfHashTbl, NULL);
        gRipRtr.pGIfHashTbl = NULL;
    }
}

/*******************************************************************************

  Function    :  RipIfUpHandleTimers

  Description :  Handles the subscription timers when an interface comes up

  Input       :  tRipIfaceRec* - Rip Interface Record
    
  Output      :  None

  Returns     :  RIP_SUCCESS or RIP_EXIT_LOOP
********************************************************************************/

PRIVATE INT1
RipIfUpHandleTimers (tRipIfaceRec * pRipIfRec)
{
    INT1                i1IsDC = RIP_SUCCESS;

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE) && (i1IsDC != RIP_SUCCESS))
    {
        if (pRipIfRec->RipSubscrTimer.u1Data != 0)
        {
            /*
             * subscription timer is still running, so ignore
             * iface up and restore all other timers.
             */
            RIP_STOP_TIMER (RIP_TIMER_ID, &pRipIfRec->RipSubscrTimer.TimerNode);
            pRipIfRec->RipSubscrTimer.u1Data = 0;

            /* start retx timer for update response */
            if (pRipIfRec->RipUpdateTimer.u1Data & RIP_RUNIN_TMR)
            {
                pRipIfRec->RipUpdateTimer.u1Data &= (UINT1) ~RIP_RUNIN_TMR;
                RIP_START_TIMER (RIP_TIMER_ID,
                                 &pRipIfRec->RipUpdateTimer.TimerNode,
                                 RIP_CFG_GET_RETX_TIMEOUT (pRipIfRec->pRipCxt));
            }

            /* start retx timer for update request */
            if (pRipIfRec->RipSpacingTimer.u1Data != 0)
            {

                RIP_START_TIMER (RIP_TIMER_ID,
                                 &pRipIfRec->RipSpacingTimer.TimerNode,
                                 RIP_CFG_GET_RETX_TIMEOUT (pRipIfRec->pRipCxt));
            }
            return (INT1) RIP_EXIT_LOOP;
        }
    }
    return RIP_SUCCESS;
}

/*******************************************************************************

  Function    :  RipIfDownHandleTimers
 
  Description :  Handles the subscription timers when an interface goes down

  Input       :  tRipIfaceRec* - Rip interface record

  Output      :  None

  Returns     :  RIP_SUCCESS or RIP_EXIT_LOOP
********************************************************************************/

PRIVATE INT1
RipIfDownHandleTimers (tRipIfaceRec * pRipIfRec)
{
    INT1                i1IsDC = RIP_SUCCESS;

    i1IsDC = RipIsDemandCircuit (pRipIfRec->RipIfaceCfg.u2RipSendStatus);

    if ((pRipIfRec->u1Persistence == RIP_WAN_TYPE) && (i1IsDC != RIP_SUCCESS))
    {
        /* starting Over-subscription timer */
        RIPIF_FILL_SUBSCRIP_TIMER (pRipIfRec,
                                   (UINT1)
                                   RIP_CFG_SUBSCRIP_TIME (pRipIfRec->pRipCxt));
        RIP_START_TIMER (RIP_TIMER_ID,
                         &pRipIfRec->RipSubscrTimer.TimerNode,
                         RIP_CFG_SUBSCRIP_TIME (pRipIfRec->pRipCxt));

        /* stop retx timer for update response if it is
         * running not setting timer data to zero, coz have
         * to restore all the state, when IFACE_UP is received
         *  before subscription timer fires.
         */
        if (RIP_STOP_TIMER (RIP_TIMER_ID,
                            &pRipIfRec->RipUpdateTimer.TimerNode)
            == TMR_SUCCESS)
        {
            pRipIfRec->RipUpdateTimer.u1Data &= RIP_RUNIN_TMR;
        }

        /* stop retx timer for update request if it is running
         */
        if (pRipIfRec->RipSpacingTimer.u1Data != 0)
        {
            RIP_STOP_TIMER (RIP_TIMER_ID,
                            &pRipIfRec->RipSpacingTimer.TimerNode);
        }
        return RIP_EXIT_LOOP;
    }
    return RIP_SUCCESS;
}

/*******************************************************************************

  Function    :  RipIfUpUnnumHandler

  Description :  When a new interface comes up with valid ip address, the
                 unnumbered interfaces on which rip is disabled due to the 
                 non-availability of valid ip address are now enabled  
                 using this as the source addr

  Input       :  u2Port
                 pRipCxtEntry - pointer to tRipCxt

  Output      :  None 

  Returns     :  None
********************************************************************************/
PRIVATE VOID
RipIfUpUnnumHandler (UINT2 u2Port, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4Addr;
    INT1                i1ReturnValue;

    u4Addr = RipIfGetAddrFromPort (u2Port);

    if (u4Addr == 0)
    {
        return;
    }

    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (((pRipIfRec->u4Addr) <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES) &&
                (pRipIfRec->pRipCxt == pRipCxtEntry))
            {
                if (pRipIfRec->RipIfaceCfg.u2OperStatus == RIPIF_OPER_DISABLE)
                {
                    pRipIfRec->RipIfaceCfg.u4SrcAddr = u4Addr;
                    if (pRipIfRec->RipIfaceCfg.u2AdminStatus
                        == RIPIF_ADMIN_ACTIVE)
                    {
                        i1ReturnValue = RipIfUpHandleTimers (pRipIfRec);
                        if (i1ReturnValue != RIP_EXIT_LOOP)
                        {
                            rip_if_enable (pRipIfRec);
                            pRipIfRec->RipIfaceCfg.u2OperStatus =
                                RIPIF_OPER_ENABLE;
                        }
                    }
                }
            }
        }
    }
}

/*******************************************************************************

 Function    :  RipIfDownUnnumHandler

 Description :  All the RIP unnumbered interfaces using the source addr of this
                port are handled to take another interfaces ip address if
                a valid one exists else rip operation is disabled on that 
                unnumbered interface.
 
 Input       :  u2Port

 Output      :  None 

 Returns     :  None
********************************************************************************/
PRIVATE VOID
RipIfDownUnnumHandler (UINT2 u2Port, tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4Addr;
    UINT4               u4NewSrcAddr = 0;
    INT1                i1ReturnValue;

    u4Addr = RipIfGetAddrFromPort (u2Port);
    if (u4Addr == 0)
    {
        return;
    }

    RipIfGetIpForUnnumIf (pRipCxtEntry, &u4NewSrcAddr);
    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (((pRipIfRec->u4Addr) <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES) &&
                (pRipIfRec->pRipCxt == pRipCxtEntry))
            {
                if ((pRipIfRec->RipIfaceCfg.u4SrcAddr) == u4Addr)
                {
                    if (u4NewSrcAddr == 0)
                    {
                        pRipIfRec->RipIfaceCfg.u4SrcAddr = u4NewSrcAddr;
                        if (pRipIfRec->RipIfaceCfg.u2AdminStatus
                            == RIPIF_ADMIN_ACTIVE)
                        {
                            i1ReturnValue = RipIfDownHandleTimers (pRipIfRec);
                            if (i1ReturnValue != RIP_EXIT_LOOP)
                            {
                                pRipIfRec->RipIfaceCfg.u2OperStatus =
                                    RIPIF_OPER_DISABLE;
                                rip_if_disable (pRipIfRec);
                            }
                        }
                    }
                    else
                    {
                        pRipIfRec->RipIfaceCfg.u4SrcAddr = u4NewSrcAddr;
                    }
                }
            }
        }
    }
}

/*******************************************************************************
 Function    :  RipIfDeleteUnnumHandler

 Description :  All the RIP unnumbered interfaces are handled whether they are 
                using the address on that interface or not since we cannot 
                retrieve the address of a deleted interface.The unnumbered
                interfaces take up an interface's ip address if a valid one 
                exists else rip operation is disabled on those unnnumbered 
                interfaces
 
 Input       :  None 

 Output      :  None

 Returns     :  None
*******************************************************************************/
PRIVATE VOID
RipIfDeleteUnnumHandler (tRipCxt * pRipCxtEntry)
{
    tRipIfaceRec       *pRipIfRec = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4NewSrcAddr = 0;
    INT1                i1ReturnValue;

    RipIfGetIpForUnnumIf (pRipCxtEntry, &u4NewSrcAddr);
    TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                              pRipIfRec, tRipIfaceRec *)
        {
            if (((pRipIfRec->u4Addr) <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES) &&
                (pRipIfRec->pRipCxt == pRipCxtEntry))
            {
                if (u4NewSrcAddr == 0)
                {
                    pRipIfRec->RipIfaceCfg.u4SrcAddr = u4NewSrcAddr;
                    pRipIfRec->RipIfaceCfg.u2OperStatus = RIPIF_OPER_DISABLE;
                    if (pRipIfRec->RipIfaceCfg.u2AdminStatus
                        == RIPIF_ADMIN_ACTIVE)
                    {
                        i1ReturnValue = RipIfDownHandleTimers (pRipIfRec);
                        if (i1ReturnValue != RIP_EXIT_LOOP)
                        {
                            rip_if_disable (pRipIfRec);
                        }
                    }
                }
                else
                {
                    pRipIfRec->RipIfaceCfg.u4SrcAddr = u4NewSrcAddr;
                }
            }
        }
    }
}

/*******************************************************************************
                                                                                                                            
 Function    :  RipIfUnnumHandlers

 Description :  All the RIP unnumbered interfaces are handled.

 Input       :  pIpParms- points to tIpParms 

 Output      :  All the RIP unnumbered interfaces are handled.

 Returns     :  none
********************************************************************************/

PRIVATE VOID
RipIfUnnumHandlers (tIpParms * pIpParms, tRipCxt ** ppRipCxtEntry)
{
    UINT2               u2RipIf = pIpParms->u2Port;
    tRipCxt            *pRipCxtInfo = NULL;

    if ((pRipCxtInfo = RipGetCxtInfoRecFrmIface (u2RipIf)) == NULL)
    {
        return;
    }

    *ppRipCxtEntry = pRipCxtInfo;

    switch (pIpParms->u1Cmd)
    {
        case IP_INTERFACE_DELETE:
            RipIfDeleteUnnumHandler (pRipCxtInfo);
            break;
        case IPIF_OPER_ENABLE:
            RipIfUpUnnumHandler (u2RipIf, pRipCxtInfo);
            break;
        case IPIF_OPER_DISABLE:
            RipIfDownUnnumHandler (u2RipIf, pRipCxtInfo);
            break;
        default:
            break;
    }
}

/*****************************************************************************/
/* Function     : RipSendRouteMapUpdateMsg                                   */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module, this function posts an event to Rip      */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RIP_SUCCESS if Route Map update Msg Sent to RIP            */
/*                successfully                                               */
/*                RIP_FAILURE otherwise                                      */
/*****************************************************************************/
INT4
RipSendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    tOsixMsg           *pBuf = NULL;
    INT4                i4OutCome;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + 4];
    UINT1               u1Len = 0;

    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status);    /* message size is hdr+ size of
                                                     * RouteMap Name+Status */

    if (pu1RMapName == NULL)
    {
        return RIP_FAILURE;
    }

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    u1Len =
        (UINT1) ((STRLEN (pu1RMapName) <
                  sizeof (au1NameBuf)) ? STRLEN (pu1RMapName) :
                 sizeof (au1NameBuf) - 1);
    STRNCPY (au1NameBuf, pu1RMapName, u1Len);
    au1NameBuf[u1Len] = '\0';

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, 0);
    if (pBuf == NULL)
    {
        return RIP_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "RipSndRteMsg");
    /*** copy status to offset 0 ***/
    if ((i4OutCome = CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Status, 0,
                                                sizeof (u4Status))) !=
        CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }

    /*** copy map name to offset 4 ***/
    if ((i4OutCome =
         CRU_BUF_Copy_OverBufChain (pBuf, au1NameBuf, sizeof (u4Status),
                                    RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }

    /*** post to Rip queue ***/

    if (RipSendToQ ((void *) pBuf, RIP_ROUTEMAP_UPDATE_MSG) !=
        (UINT4) RIP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP_FAILURE;
    }

    return i4OutCome;
}

/*****************************************************************************/
/* Function     : RipProcessRMapHandler                                      */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module.                                          */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
RipProcessRMapHandler (tProtoQMsg * pMsg)
{
    tOsixMsg           *pBuf = NULL;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status = 0;
    UINT4               u4ContextId = 0;
    tRipCxt            *pRipCxt = NULL;
    UINT4               u4MaxCxt = 0;
    UINT1               u1Status = FILTERNIG_STAT_DEFAULT;

    u4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        FsRIPSizingParams
                        [MAX_RIP_CONTEXT_SIZING_ID].u4PreAllocatedUnits);

    if (pMsg == NULL)
    {
        return;
    }

    pBuf = pMsg->msg.RMapParms;

    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) &u4Status, 0, sizeof (u4Status)) != sizeof (u4Status))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN + 4));
    if (CRU_BUF_Copy_FromBufChain (pBuf, au1RMapName, sizeof (u4Status),
                                   RMAP_MAX_NAME_LEN) != RMAP_MAX_NAME_LEN)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (u4Status != 0)
    {
        u1Status = FILTERNIG_STAT_ENABLE;
    }
    else
    {
        u1Status = FILTERNIG_STAT_DISABLE;
    }
    /* Scan through all the context and change the status */
    for (u4ContextId = 0; u4ContextId < u4MaxCxt; u4ContextId++)
    {
        if (gRipRtr.apRipCxt[u4ContextId] == NULL)
        {
            continue;
        }
        pRipCxt = gRipRtr.apRipCxt[u4ContextId];
        if (pRipCxt->pDistributeInFilterRMap != NULL)
        {
            if (STRCMP
                (pRipCxt->pDistributeInFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pRipCxt->pDistributeInFilterRMap->u1Status = u1Status;
            }
        }

        if (pRipCxt->pDistributeOutFilterRMap != NULL)
        {
            if (STRCMP
                (pRipCxt->pDistributeOutFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pRipCxt->pDistributeOutFilterRMap->u1Status = u1Status;
            }
        }

        if (pRipCxt->pDistanceFilterRMap != NULL)
        {
            if (STRCMP
                (pRipCxt->pDistanceFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pRipCxt->pDistanceFilterRMap->u1Status = u1Status;
            }
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
}
