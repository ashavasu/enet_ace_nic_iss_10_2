/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirilw.c,v 1.27 2016/03/26 09:59:47 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "ripinc.h"
#include "stdripwr.h"
#include "fsripwr.h"
#include "fsmiricli.h"
#include "fsmistdripcli.h"

/* LOW LEVEL Routines for Table : FsMIRip2GlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRip2GlobalTable (INT4 i4FsMIRipContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }
    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRip2GlobalTable (INT4 *pi4FsMIRipContextId)
{
    if (RipGetFirstCxtId (pi4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRip2GlobalTable (INT4 i4FsMIRipContextId,
                                    INT4 *pi4NextFsMIRipContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);
    if (i4FsMIRipContextId > i4MaxCxt)
    {
        return SNMP_FAILURE;
    }
    if (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) !=
        RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRip2Security
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2Security
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2Security (INT4 i4FsMIRipContextId,
                        INT4 *pi4RetValFsMIRip2Security)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2Security (pi4RetValFsMIRip2Security);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2Peers
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2Peers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2Peers (INT4 i4FsMIRipContextId, INT4 *pi4RetValFsMIRip2Peers)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2Peers (pi4RetValFsMIRip2Peers);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2TrustNBRListEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2TrustNBRListEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2TrustNBRListEnable (INT4 i4FsMIRipContextId,
                                  INT4 *pi4RetValFsMIRip2TrustNBRListEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRip2TrustNBRListEnable (pi4RetValFsMIRip2TrustNBRListEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2NumberOfDroppedPkts
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2NumberOfDroppedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2NumberOfDroppedPkts (INT4 i4FsMIRipContextId,
                                   UINT4 *pu4RetValFsMIRip2NumberOfDroppedPkts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRip2NumberOfDroppedPkts (pu4RetValFsMIRip2NumberOfDroppedPkts);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2SpacingEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2SpacingEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2SpacingEnable (INT4 i4FsMIRipContextId,
                             INT4 *pi4RetValFsMIRip2SpacingEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2SpacingEnable (pi4RetValFsMIRip2SpacingEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2AutoSummaryStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2AutoSummaryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2AutoSummaryStatus (INT4 i4FsMIRipContextId,
                                 INT4 *pi4RetValFsMIRip2AutoSummaryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRip2AutoSummaryStatus (pi4RetValFsMIRip2AutoSummaryStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2RetransTimeoutInt
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2RetransTimeoutInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2RetransTimeoutInt (INT4 i4FsMIRipContextId,
                                 INT4 *pi4RetValFsMIRip2RetransTimeoutInt)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRip2RetransTimeoutInt (pi4RetValFsMIRip2RetransTimeoutInt);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2MaxRetransmissions
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2MaxRetransmissions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2MaxRetransmissions (INT4 i4FsMIRipContextId,
                                  INT4 *pi4RetValFsMIRip2MaxRetransmissions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRip2MaxRetransmissions (pi4RetValFsMIRip2MaxRetransmissions);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2OverSubscriptionTimeout
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2OverSubscriptionTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2OverSubscriptionTimeout (INT4 i4FsMIRipContextId,
                                       INT4
                                       *pi4RetValFsMIRip2OverSubscriptionTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRip2OverSubscriptionTimeout
        (pi4RetValFsMIRip2OverSubscriptionTimeout);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2Propagate
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2Propagate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2Propagate (INT4 i4FsMIRipContextId,
                         INT4 *pi4RetValFsMIRip2Propagate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2Propagate (pi4RetValFsMIRip2Propagate);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipTrcFlag
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipTrcFlag (INT4 i4FsMIRipContextId, INT4 *pi4RetValFsMIRipTrcFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipTrcFlag (pi4RetValFsMIRipTrcFlag);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRowStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRowStatus (INT4 i4FsMIRipContextId,
                        INT4 *pi4RetValFsMIRipRowStatus)
{
    tRipCxt            *pRipCxtEntry = NULL;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRipCxtEntry = RipGetCxtInfoRec (i4FsMIRipContextId);

    if (pRipCxtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIRipRowStatus = pRipCxtEntry->u1CxtStatus;

    RipResetContext ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipAdminStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object
                retValFsMIRipAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipAdminStatus (INT4 i4FsMIRipContextId,
                          INT4 *pi4RetValFsMIRipAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipAdminStatus (pi4RetValFsMIRipAdminStatus);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2LastAuthKeyLifetimeStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRip2LastAuthKeyLifetimeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsMIRip2LastAuthKeyLifetimeStatus
    (INT4 i4FsMIRipContextId, INT4 *pi4RetValFsMIRip2LastAuthKeyLifetimeStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2LastAuthKeyLifetimeStatus
        (pi4RetValFsMIRip2LastAuthKeyLifetimeStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRtCount
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRtCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRtCount (INT4 i4FsMIRipContextId, INT4 *pi4RetValFsMIRipRtCount)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipRtCount (pi4RetValFsMIRipRtCount);
    RipResetContext ();
    return i1Return;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRip2Security
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2Security
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2Security (INT4 i4FsMIRipContextId, INT4 i4SetValFsMIRip2Security)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2Security (i4SetValFsMIRip2Security);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2Peers
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2Peers
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2Peers (INT4 i4FsMIRipContextId, INT4 i4SetValFsMIRip2Peers)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2Peers (i4SetValFsMIRip2Peers);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2TrustNBRListEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2TrustNBRListEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2TrustNBRListEnable (INT4 i4FsMIRipContextId,
                                  INT4 i4SetValFsMIRip2TrustNBRListEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRip2TrustNBRListEnable (i4SetValFsMIRip2TrustNBRListEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2SpacingEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2SpacingEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2SpacingEnable (INT4 i4FsMIRipContextId,
                             INT4 i4SetValFsMIRip2SpacingEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2SpacingEnable (i4SetValFsMIRip2SpacingEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2AutoSummaryStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2AutoSummaryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2AutoSummaryStatus (INT4 i4FsMIRipContextId,
                                 INT4 i4SetValFsMIRip2AutoSummaryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRip2AutoSummaryStatus (i4SetValFsMIRip2AutoSummaryStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2RetransTimeoutInt
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2RetransTimeoutInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2RetransTimeoutInt (INT4 i4FsMIRipContextId,
                                 INT4 i4SetValFsMIRip2RetransTimeoutInt)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRip2RetransTimeoutInt (i4SetValFsMIRip2RetransTimeoutInt);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2MaxRetransmissions
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2MaxRetransmissions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2MaxRetransmissions (INT4 i4FsMIRipContextId,
                                  INT4 i4SetValFsMIRip2MaxRetransmissions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRip2MaxRetransmissions (i4SetValFsMIRip2MaxRetransmissions);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2OverSubscriptionTimeout
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2OverSubscriptionTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2OverSubscriptionTimeout (INT4 i4FsMIRipContextId,
                                       INT4
                                       i4SetValFsMIRip2OverSubscriptionTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRip2OverSubscriptionTimeout
        (i4SetValFsMIRip2OverSubscriptionTimeout);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2Propagate
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2Propagate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2Propagate (INT4 i4FsMIRipContextId,
                         INT4 i4SetValFsMIRip2Propagate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2Propagate (i4SetValFsMIRip2Propagate);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipTrcFlag
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipTrcFlag (INT4 i4FsMIRipContextId, INT4 i4SetValFsMIRipTrcFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipTrcFlag (i4SetValFsMIRipTrcFlag);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRowStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRowStatus (INT4 i4FsMIRipContextId, INT4 i4SetValFsMIRipRowStatus)
{
    tRipCxt            *pRipCxtEntry = NULL;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    pRipCxtEntry = RipGetCxtInfoRec (i4FsMIRipContextId);

    if (i4SetValFsMIRipRowStatus == CREATE_AND_WAIT ||
        i4SetValFsMIRipRowStatus == CREATE_AND_GO)
    {
        if (pRipCxtEntry != NULL)
        {
            return SNMP_FAILURE;
        }
        if (RIP_ALLOC_CXT_ENTRY (pRipCxtEntry) == NULL)
        {
            return SNMP_FAILURE;
        }

        pRipCxtEntry->i4CxtId = i4FsMIRipContextId;

        if (RipCreateCxtEntry (pRipCxtEntry) != RIP_SUCCESS)
        {
            RIP_FREE_CXT_ENTRY (pRipCxtEntry);
            return SNMP_FAILURE;
        }

        if (i4SetValFsMIRipRowStatus == CREATE_AND_GO)
        {
            pRipCxtEntry->u1CxtStatus = ACTIVE;
            RIP_MGMT_CXT = gRipRtr.apRipCxt[i4FsMIRipContextId];
        }
        else
        {
            pRipCxtEntry->u1CxtStatus = NOT_IN_SERVICE;
        }
    }
    else if (i4SetValFsMIRipRowStatus == DESTROY)
    {
        if (pRipCxtEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        RipDelCxtEntry (pRipCxtEntry);
    }
    else if (i4SetValFsMIRipRowStatus == ACTIVE ||
             i4SetValFsMIRipRowStatus == NOT_IN_SERVICE)
    {
        if (pRipCxtEntry == NULL)
        {
            return SNMP_FAILURE;
        }
        pRipCxtEntry->u1CxtStatus = (UINT1) i4SetValFsMIRipRowStatus;

        if (i4SetValFsMIRipRowStatus == ACTIVE)
        {
            RIP_MGMT_CXT = gRipRtr.apRipCxt[i4FsMIRipContextId];
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipRowStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipRowStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FsMIRipContextId,
                      i4SetValFsMIRipRowStatus));
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipAdminStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object
                setValFsMIRipAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipAdminStatus (INT4 i4FsMIRipContextId,
                          INT4 i4SetValFsMIRipAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipAdminStatus (i4SetValFsMIRipAdminStatus);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2LastAuthKeyLifetimeStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRip2LastAuthKeyLifetimeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsMIRip2LastAuthKeyLifetimeStatus
    (INT4 i4FsMIRipContextId, INT4 i4SetValFsMIRip2LastAuthKeyLifetimeStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2LastAuthKeyLifetimeStatus
        (i4SetValFsMIRip2LastAuthKeyLifetimeStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2Security
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2Security
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2Security (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                           INT4 i4TestValFsMIRip2Security)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2Security (pu4ErrorCode,
                                        i4TestValFsMIRip2Security);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2Peers
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2Peers
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2Peers (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                        INT4 i4TestValFsMIRip2Peers)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2Peers (pu4ErrorCode, i4TestValFsMIRip2Peers);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2TrustNBRListEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2TrustNBRListEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2TrustNBRListEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRipContextId,
                                     INT4 i4TestValFsMIRip2TrustNBRListEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2TrustNBRListEnable (pu4ErrorCode,
                                                  i4TestValFsMIRip2TrustNBRListEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2SpacingEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2SpacingEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2SpacingEnable (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                INT4 i4TestValFsMIRip2SpacingEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2SpacingEnable (pu4ErrorCode,
                                             i4TestValFsMIRip2SpacingEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2AutoSummaryStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2AutoSummaryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2AutoSummaryStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRipContextId,
                                    INT4 i4TestValFsMIRip2AutoSummaryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2AutoSummaryStatus (pu4ErrorCode,
                                                 i4TestValFsMIRip2AutoSummaryStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2RetransTimeoutInt
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2RetransTimeoutInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2RetransTimeoutInt (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRipContextId,
                                    INT4 i4TestValFsMIRip2RetransTimeoutInt)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2RetransTimeoutInt (pu4ErrorCode,
                                                 i4TestValFsMIRip2RetransTimeoutInt);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2MaxRetransmissions
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2MaxRetransmissions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2MaxRetransmissions (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRipContextId,
                                     INT4 i4TestValFsMIRip2MaxRetransmissions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2MaxRetransmissions (pu4ErrorCode,
                                                  i4TestValFsMIRip2MaxRetransmissions);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2OverSubscriptionTimeout
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2OverSubscriptionTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2OverSubscriptionTimeout (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIRipContextId,
                                          INT4
                                          i4TestValFsMIRip2OverSubscriptionTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2OverSubscriptionTimeout (pu4ErrorCode,
                                                       i4TestValFsMIRip2OverSubscriptionTimeout);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2Propagate
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2Propagate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2Propagate (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                            INT4 i4TestValFsMIRip2Propagate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2Propagate (pu4ErrorCode,
                                         i4TestValFsMIRip2Propagate);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipTrcFlag
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipTrcFlag (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                         INT4 i4TestValFsMIRipTrcFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipTrcFlag (pu4ErrorCode, i4TestValFsMIRipTrcFlag);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRowStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                           INT4 i4TestValFsMIRipRowStatus)
{

    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);
    /* Validating the context Id */
    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMIRipRowStatus == CREATE_AND_WAIT ||
        i4TestValFsMIRipRowStatus == CREATE_AND_GO)
    {
        /* Checking if the context exists in vcm */
        if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
        if (RipCxtExists (i4FsMIRipContextId) == RIP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsMIRipRowStatus == DESTROY)
    {
        if (RipCxtExists (i4FsMIRipContextId) != RIP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsMIRipRowStatus == ACTIVE)
    {
        if (RipCxtExists (i4FsMIRipContextId) != RIP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    else if (i4TestValFsMIRipRowStatus == NOT_IN_SERVICE)
    {
        if (RipCxtExists (i4FsMIRipContextId) != RIP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsMIRipRowStatus == NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipAdminStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object
                testValFsMIRipAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipAdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                             INT4 i4TestValFsMIRipAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRipAdminStatus (pu4ErrorCode, i4TestValFsMIRipAdminStatus);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2LastAuthKeyLifetimeStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRip2LastAuthKeyLifetimeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsMIRip2LastAuthKeyLifetimeStatus
    (UINT4 *pu4ErrorCode,
     INT4 i4FsMIRipContextId, INT4 i4TestValFsMIRip2LastAuthKeyLifetimeStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2LastAuthKeyLifetimeStatus
        (pu4ErrorCode, i4TestValFsMIRip2LastAuthKeyLifetimeStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRip2GlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRip2GlobalTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipGlobalTrcFlag
 Input       :  The Indices

                The Object 
                retValFsMIRipGlobalTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipGlobalTrcFlag (INT4 *pi4RetValFsMIRipGlobalTrcFlag)
{
    *pi4RetValFsMIRipGlobalTrcFlag = (INT4) RIP_TRC_GLOBAL_FLAG;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipGlobalTrcFlag
 Input       :  The Indices

                The Object 
                setValFsMIRipGlobalTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipGlobalTrcFlag (INT4 i4SetValFsMIRipGlobalTrcFlag)
{
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
#endif
    UINT4               u4SeqNum = 0;

    RIP_TRC_GLOBAL_FLAG = (UINT4) i4SetValFsMIRipGlobalTrcFlag;

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIRipGlobalTrcFlag;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIRipGlobalTrcFlag) / sizeof (UINT4);
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RipLock;
    SnmpNotifyInfo.pUnLockPointer = RipUnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsMIRipGlobalTrcFlag));
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipGlobalTrcFlag
 Input       :  The Indices

                The Object 
                testValFsMIRipGlobalTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipGlobalTrcFlag (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMIRipGlobalTrcFlag)
{
    if (i4TestValFsMIRipGlobalTrcFlag < RIP_TRC_FLAG_MIN ||
        i4TestValFsMIRipGlobalTrcFlag > RIP_TRC_FLAG_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipGlobalTrcFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipGlobalTrcFlag (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRip2NBRTrustListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRip2NBRTrustListTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2TrustNBRIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRip2NBRTrustListTable (INT4 i4FsMIRipContextId,
                                                   UINT4
                                                   u4FsMIRip2TrustNBRIpAddr)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsRip2NBRTrustListTable
        (u4FsMIRip2TrustNBRIpAddr);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRip2NBRTrustListTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2TrustNBRIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRip2NBRTrustListTable (INT4 *pi4FsMIRipContextId,
                                           UINT4 *pu4FsMIRip2TrustNBRIpAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRip2NBRTrustListTable (pu4FsMIRip2TrustNBRIpAddr);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRip2NBRTrustListTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRip2TrustNBRIpAddr
                nextFsMIRip2TrustNBRIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRip2NBRTrustListTable (INT4 i4FsMIRipContextId,
                                          INT4 *pi4NextFsMIRipContextId,
                                          UINT4 u4FsMIRip2TrustNBRIpAddr,
                                          UINT4 *pu4NextFsMIRip2TrustNBRIpAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexFsRip2NBRTrustListTable
            (u4FsMIRip2TrustNBRIpAddr, pu4NextFsMIRip2TrustNBRIpAddr);

        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }
    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (*pi4NextFsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRip2NBRTrustListTable
            (pu4NextFsMIRip2TrustNBRIpAddr);

        RipResetContext ();
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRip2TrustNBRRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2TrustNBRIpAddr

                The Object 
                retValFsMIRip2TrustNBRRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2TrustNBRRowStatus (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIRip2TrustNBRIpAddr,
                                 INT4 *pi4RetValFsMIRip2TrustNBRRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2TrustNBRRowStatus (u4FsMIRip2TrustNBRIpAddr,
                                              pi4RetValFsMIRip2TrustNBRRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRip2TrustNBRRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2TrustNBRIpAddr

                The Object 
                setValFsMIRip2TrustNBRRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2TrustNBRRowStatus (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIRip2TrustNBRIpAddr,
                                 INT4 i4SetValFsMIRip2TrustNBRRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRip2TrustNBRRowStatus (u4FsMIRip2TrustNBRIpAddr,
                                              i4SetValFsMIRip2TrustNBRRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2TrustNBRRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2TrustNBRIpAddr

                The Object 
                testValFsMIRip2TrustNBRRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2TrustNBRRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIRip2TrustNBRIpAddr,
                                    INT4 i4TestValFsMIRip2TrustNBRRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2TrustNBRRowStatus (pu4ErrorCode,
                                                 u4FsMIRip2TrustNBRIpAddr,
                                                 i4TestValFsMIRip2TrustNBRRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRip2NBRTrustListTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2TrustNBRIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRip2NBRTrustListTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRip2IfConfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRip2IfConfTable (INT4 i4FsMIRipContextId,
                                             UINT4 u4FsMIRip2IfConfAddress)
{

    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsRip2IfConfTable (u4FsMIRip2IfConfAddress);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRip2IfConfTable (INT4 *pi4FsMIRipContextId,
                                     UINT4 *pu4FsMIRip2IfConfAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRip2IfConfTable (pu4FsMIRip2IfConfAddress);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRip2IfConfAddress
                nextFsMIRip2IfConfAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRip2IfConfTable (INT4 i4FsMIRipContextId,
                                    INT4 *pi4NextFsMIRipContextId,
                                    UINT4 u4FsMIRip2IfConfAddress,
                                    UINT4 *pu4NextFsMIRip2IfConfAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal =
            nmhGetNextIndexFsRip2IfConfTable (u4FsMIRip2IfConfAddress,
                                              pu4NextFsMIRip2IfConfAddress);

        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }
    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (*pi4NextFsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRip2IfConfTable (pu4NextFsMIRip2IfConfAddress);

        RipResetContext ();
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfAdminStat
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfAdminStat (INT4 i4FsMIRipContextId,
                           UINT4 u4FsMIRip2IfConfAddress,
                           INT4 *pi4RetValFsMIRip2IfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfAdminStat (u4FsMIRip2IfConfAddress,
                                        pi4RetValFsMIRip2IfAdminStat);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfOperState
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfOperState (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIRip2IfConfAddress,
                               INT4 *pi4RetValFsMIRip2IfConfOperState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfOperState (u4FsMIRip2IfConfAddress,
                                            pi4RetValFsMIRip2IfConfOperState);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfUpdateTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfUpdateTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfUpdateTmr (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIRip2IfConfAddress,
                               INT4 *pi4RetValFsMIRip2IfConfUpdateTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfUpdateTmr (u4FsMIRip2IfConfAddress,
                                            pi4RetValFsMIRip2IfConfUpdateTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfGarbgCollectTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfGarbgCollectTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfGarbgCollectTmr (INT4 i4FsMIRipContextId,
                                     UINT4 u4FsMIRip2IfConfAddress,
                                     INT4
                                     *pi4RetValFsMIRip2IfConfGarbgCollectTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfGarbgCollectTmr (u4FsMIRip2IfConfAddress,
                                                  pi4RetValFsMIRip2IfConfGarbgCollectTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfRouteAgeTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfRouteAgeTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfRouteAgeTmr (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIRip2IfConfAddress,
                                 INT4 *pi4RetValFsMIRip2IfConfRouteAgeTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfRouteAgeTmr (u4FsMIRip2IfConfAddress,
                                              pi4RetValFsMIRip2IfConfRouteAgeTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfSplitHorizonStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfSplitHorizonStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfSplitHorizonStatus (INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIRip2IfConfAddress,
                                    INT4 *pi4RetValFsMIRip2IfSplitHorizonStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfSplitHorizonStatus (u4FsMIRip2IfConfAddress,
                                                 pi4RetValFsMIRip2IfSplitHorizonStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfDefRtInstall
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfDefRtInstall
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfDefRtInstall (INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIRip2IfConfAddress,
                                  INT4 *pi4RetValFsMIRip2IfConfDefRtInstall)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfDefRtInstall (u4FsMIRip2IfConfAddress,
                                               pi4RetValFsMIRip2IfConfDefRtInstall);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfSpacingTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfSpacingTmr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfSpacingTmr (INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIRip2IfConfAddress,
                                INT4 *pi4RetValFsMIRip2IfConfSpacingTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfSpacingTmr (u4FsMIRip2IfConfAddress,
                                             pi4RetValFsMIRip2IfConfSpacingTmr);
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfInUseKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfInUseKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfInUseKey (INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIRip2IfConfAddress,
                              INT4 *pi4RetValFsMIRip2IfConfInUseKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfInUseKey (u4FsMIRip2IfConfAddress,
                                           pi4RetValFsMIRip2IfConfInUseKey);
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2IfConfAuthLastKeyStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                retValFsMIRip2IfConfAuthLastKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2IfConfAuthLastKeyStatus (INT4 i4FsMIRipContextId,
                                       UINT4 u4FsMIRip2IfConfAddress,
                                       INT4
                                       *pi4RetValFsMIRip2IfConfAuthLastKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2IfConfAuthLastKeyStatus
        (u4FsMIRip2IfConfAddress, pi4RetValFsMIRip2IfConfAuthLastKeyStatus);
    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfAdminStat
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfAdminStat (INT4 i4FsMIRipContextId,
                           UINT4 u4FsMIRip2IfConfAddress,
                           INT4 i4SetValFsMIRip2IfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2IfAdminStat (u4FsMIRip2IfConfAddress,
                                        i4SetValFsMIRip2IfAdminStat);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfConfUpdateTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfConfUpdateTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfConfUpdateTmr (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIRip2IfConfAddress,
                               INT4 i4SetValFsMIRip2IfConfUpdateTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2IfConfUpdateTmr (u4FsMIRip2IfConfAddress,
                                            i4SetValFsMIRip2IfConfUpdateTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfConfGarbgCollectTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfConfGarbgCollectTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfConfGarbgCollectTmr (INT4 i4FsMIRipContextId,
                                     UINT4 u4FsMIRip2IfConfAddress,
                                     INT4 i4SetValFsMIRip2IfConfGarbgCollectTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2IfConfGarbgCollectTmr (u4FsMIRip2IfConfAddress,
                                                  i4SetValFsMIRip2IfConfGarbgCollectTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfConfRouteAgeTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfConfRouteAgeTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfConfRouteAgeTmr (INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIRip2IfConfAddress,
                                 INT4 i4SetValFsMIRip2IfConfRouteAgeTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2IfConfRouteAgeTmr (u4FsMIRip2IfConfAddress,
                                              i4SetValFsMIRip2IfConfRouteAgeTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfSplitHorizonStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfSplitHorizonStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfSplitHorizonStatus (INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIRip2IfConfAddress,
                                    INT4 i4SetValFsMIRip2IfSplitHorizonStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2IfSplitHorizonStatus (u4FsMIRip2IfConfAddress,
                                                 i4SetValFsMIRip2IfSplitHorizonStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfConfDefRtInstall
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfConfDefRtInstall
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfConfDefRtInstall (INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIRip2IfConfAddress,
                                  INT4 i4SetValFsMIRip2IfConfDefRtInstall)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2IfConfDefRtInstall (u4FsMIRip2IfConfAddress,
                                               i4SetValFsMIRip2IfConfDefRtInstall);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRip2IfConfSpacingTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                setValFsMIRip2IfConfSpacingTmr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2IfConfSpacingTmr (INT4 i4FsMIRipContextId,
                                UINT4 u4FsMIRip2IfConfAddress,
                                INT4 i4SetValFsMIRip2IfConfSpacingTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRip2IfConfSpacingTmr (u4FsMIRip2IfConfAddress,
                                             i4SetValFsMIRip2IfConfSpacingTmr);
    RipResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfAdminStat
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfAdminStat (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIRip2IfConfAddress,
                              INT4 i4TestValFsMIRip2IfAdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfAdminStat (pu4ErrorCode,
                                           u4FsMIRip2IfConfAddress,
                                           i4TestValFsMIRip2IfAdminStat);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfConfUpdateTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfConfUpdateTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfConfUpdateTmr (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIRip2IfConfAddress,
                                  INT4 i4TestValFsMIRip2IfConfUpdateTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfConfUpdateTmr (pu4ErrorCode,
                                               u4FsMIRip2IfConfAddress,
                                               i4TestValFsMIRip2IfConfUpdateTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfConfGarbgCollectTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfConfGarbgCollectTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfConfGarbgCollectTmr (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIRipContextId,
                                        UINT4 u4FsMIRip2IfConfAddress,
                                        INT4
                                        i4TestValFsMIRip2IfConfGarbgCollectTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfConfGarbgCollectTmr (pu4ErrorCode,
                                                     u4FsMIRip2IfConfAddress,
                                                     i4TestValFsMIRip2IfConfGarbgCollectTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfConfRouteAgeTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfConfRouteAgeTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfConfRouteAgeTmr (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRipContextId,
                                    UINT4 u4FsMIRip2IfConfAddress,
                                    INT4 i4TestValFsMIRip2IfConfRouteAgeTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfConfRouteAgeTmr (pu4ErrorCode,
                                                 u4FsMIRip2IfConfAddress,
                                                 i4TestValFsMIRip2IfConfRouteAgeTmr);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfSplitHorizonStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfSplitHorizonStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfSplitHorizonStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIRipContextId,
                                       UINT4 u4FsMIRip2IfConfAddress,
                                       INT4
                                       i4TestValFsMIRip2IfSplitHorizonStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfSplitHorizonStatus (pu4ErrorCode,
                                                    u4FsMIRip2IfConfAddress,
                                                    i4TestValFsMIRip2IfSplitHorizonStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfConfDefRtInstall
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfConfDefRtInstall
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfConfDefRtInstall (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRipContextId,
                                     UINT4 u4FsMIRip2IfConfAddress,
                                     INT4 i4TestValFsMIRip2IfConfDefRtInstall)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfConfDefRtInstall (pu4ErrorCode,
                                                  u4FsMIRip2IfConfAddress,
                                                  i4TestValFsMIRip2IfConfDefRtInstall);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2IfConfSpacingTmr
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress

                The Object 
                testValFsMIRip2IfConfSpacingTmr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2IfConfSpacingTmr (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIRipContextId,
                                   UINT4 u4FsMIRip2IfConfAddress,
                                   INT4 i4TestValFsMIRip2IfConfSpacingTmr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2IfConfSpacingTmr (pu4ErrorCode,
                                                u4FsMIRip2IfConfAddress,
                                                i4TestValFsMIRip2IfConfSpacingTmr);
    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRip2IfConfTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2IfConfAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRip2IfConfTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRipMd5AuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRipMd5AuthTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRipMd5AuthTable (INT4 i4FsMIRipContextId,
                                             UINT4 u4FsMIRipMd5AuthAddress,
                                             INT4 i4FsMIRipMd5AuthKeyId)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsRipMd5AuthTable (u4FsMIRipMd5AuthAddress,
                                                   i4FsMIRipMd5AuthKeyId);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRipMd5AuthTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRipMd5AuthTable (INT4 *pi4FsMIRipContextId,
                                     UINT4 *pu4FsMIRipMd5AuthAddress,
                                     INT4 *pi4FsMIRipMd5AuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRipMd5AuthTable (pu4FsMIRipMd5AuthAddress,
                                               pi4FsMIRipMd5AuthKeyId);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRipMd5AuthTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRipMd5AuthAddress
                nextFsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId
                nextFsMIRipMd5AuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRipMd5AuthTable (INT4 i4FsMIRipContextId,
                                    INT4 *pi4NextFsMIRipContextId,
                                    UINT4 u4FsMIRipMd5AuthAddress,
                                    UINT4 *pu4NextFsMIRipMd5AuthAddress,
                                    INT4 i4FsMIRipMd5AuthKeyId,
                                    INT4 *pi4NextFsMIRipMd5AuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal =
            nmhGetNextIndexFsRipMd5AuthTable (u4FsMIRipMd5AuthAddress,
                                              pu4NextFsMIRipMd5AuthAddress,
                                              i4FsMIRipMd5AuthKeyId,
                                              pi4NextFsMIRipMd5AuthKeyId);

        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }

    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) ==
            RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRipMd5AuthTable (pu4NextFsMIRipMd5AuthAddress,
                                               pi4NextFsMIRipMd5AuthKeyId);

        RipResetContext ();
        i4FsMIRipContextId = *pi4NextFsMIRipContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipMd5AuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                retValFsMIRipMd5AuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipMd5AuthKey (INT4 i4FsMIRipContextId, UINT4 u4FsMIRipMd5AuthAddress,
                         INT4 i4FsMIRipMd5AuthKeyId,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsMIRipMd5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipMd5AuthKey (u4FsMIRipMd5AuthAddress,
                                      i4FsMIRipMd5AuthKeyId,
                                      pRetValFsMIRipMd5AuthKey);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipMd5KeyStartTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                retValFsMIRipMd5KeyStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipMd5KeyStartTime (INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIRipMd5AuthAddress,
                              INT4 i4FsMIRipMd5AuthKeyId,
                              INT4 *pi4RetValFsMIRipMd5KeyStartTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipMd5KeyStartTime (u4FsMIRipMd5AuthAddress,
                                           i4FsMIRipMd5AuthKeyId,
                                           pi4RetValFsMIRipMd5KeyStartTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipMd5KeyExpiryTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                retValFsMIRipMd5KeyExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipMd5KeyExpiryTime (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIRipMd5AuthAddress,
                               INT4 i4FsMIRipMd5AuthKeyId,
                               INT4 *pi4RetValFsMIRipMd5KeyExpiryTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipMd5KeyExpiryTime (u4FsMIRipMd5AuthAddress,
                                            i4FsMIRipMd5AuthKeyId,
                                            pi4RetValFsMIRipMd5KeyExpiryTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipMd5KeyRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                retValFsMIRipMd5KeyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipMd5KeyRowStatus (INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIRipMd5AuthAddress,
                              INT4 i4FsMIRipMd5AuthKeyId,
                              INT4 *pi4RetValFsMIRipMd5KeyRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipMd5KeyRowStatus (u4FsMIRipMd5AuthAddress,
                                           i4FsMIRipMd5AuthKeyId,
                                           pi4RetValFsMIRipMd5KeyRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipMd5AuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                setValFsMIRipMd5AuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipMd5AuthKey (INT4 i4FsMIRipContextId, UINT4 u4FsMIRipMd5AuthAddress,
                         INT4 i4FsMIRipMd5AuthKeyId,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsMIRipMd5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipMd5AuthKey (u4FsMIRipMd5AuthAddress,
                                      i4FsMIRipMd5AuthKeyId,
                                      pSetValFsMIRipMd5AuthKey);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipMd5KeyStartTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                setValFsMIRipMd5KeyStartTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipMd5KeyStartTime (INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIRipMd5AuthAddress,
                              INT4 i4FsMIRipMd5AuthKeyId,
                              INT4 i4SetValFsMIRipMd5KeyStartTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipMd5KeyStartTime (u4FsMIRipMd5AuthAddress,
                                           i4FsMIRipMd5AuthKeyId,
                                           i4SetValFsMIRipMd5KeyStartTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipMd5KeyExpiryTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                setValFsMIRipMd5KeyExpiryTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipMd5KeyExpiryTime (INT4 i4FsMIRipContextId,
                               UINT4 u4FsMIRipMd5AuthAddress,
                               INT4 i4FsMIRipMd5AuthKeyId,
                               INT4 i4SetValFsMIRipMd5KeyExpiryTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipMd5KeyExpiryTime (u4FsMIRipMd5AuthAddress,
                                            i4FsMIRipMd5AuthKeyId,
                                            i4SetValFsMIRipMd5KeyExpiryTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipMd5KeyRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                setValFsMIRipMd5KeyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipMd5KeyRowStatus (INT4 i4FsMIRipContextId,
                              UINT4 u4FsMIRipMd5AuthAddress,
                              INT4 i4FsMIRipMd5AuthKeyId,
                              INT4 i4SetValFsMIRipMd5KeyRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipMd5KeyRowStatus (u4FsMIRipMd5AuthAddress,
                                           i4FsMIRipMd5AuthKeyId,
                                           i4SetValFsMIRipMd5KeyRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipMd5AuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                testValFsMIRipMd5AuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipMd5AuthKey (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                            UINT4 u4FsMIRipMd5AuthAddress,
                            INT4 i4FsMIRipMd5AuthKeyId,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsMIRipMd5AuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipMd5AuthKey (pu4ErrorCode,
                                         u4FsMIRipMd5AuthAddress,
                                         i4FsMIRipMd5AuthKeyId,
                                         pTestValFsMIRipMd5AuthKey);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipMd5KeyStartTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                testValFsMIRipMd5KeyStartTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipMd5KeyStartTime (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIRipMd5AuthAddress,
                                 INT4 i4FsMIRipMd5AuthKeyId,
                                 INT4 i4TestValFsMIRipMd5KeyStartTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipMd5KeyStartTime (pu4ErrorCode,
                                              u4FsMIRipMd5AuthAddress,
                                              i4FsMIRipMd5AuthKeyId,
                                              i4TestValFsMIRipMd5KeyStartTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipMd5KeyExpiryTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                testValFsMIRipMd5KeyExpiryTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipMd5KeyExpiryTime (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                  UINT4 u4FsMIRipMd5AuthAddress,
                                  INT4 i4FsMIRipMd5AuthKeyId,
                                  INT4 i4TestValFsMIRipMd5KeyExpiryTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipMd5KeyExpiryTime (pu4ErrorCode,
                                               u4FsMIRipMd5AuthAddress,
                                               i4FsMIRipMd5AuthKeyId,
                                               i4TestValFsMIRipMd5KeyExpiryTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipMd5KeyRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId

                The Object 
                testValFsMIRipMd5KeyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipMd5KeyRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                 UINT4 u4FsMIRipMd5AuthAddress,
                                 INT4 i4FsMIRipMd5AuthKeyId,
                                 INT4 i4TestValFsMIRipMd5KeyRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipMd5KeyRowStatus (pu4ErrorCode,
                                              u4FsMIRipMd5AuthAddress,
                                              i4FsMIRipMd5AuthKeyId,
                                              i4TestValFsMIRipMd5KeyRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipMd5AuthTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipMd5AuthAddress
                FsMIRipMd5AuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipMd5AuthTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRipCryptoAuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRipCryptoAuthTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMIRipCryptoAuthTable
    (INT4 i4FsMIRipContextId,
     INT4 i4FsMIRipCryptoAuthIfIndex,
     UINT4 u4FsMIRipCryptoAuthAddress, INT4 i4FsMIRipCryptoAuthKeyId)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhValidateIndexInstanceFsRipCryptoAuthTable
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress, i4FsMIRipCryptoAuthKeyId);
    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRipCryptoAuthTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRipCryptoAuthTable (INT4 *pi4FsMIRipContextId,
                                        INT4 *pi4FsMIRipCryptoAuthIfIndex,
                                        UINT4 *pu4FsMIRipCryptoAuthAddress,
                                        INT4 *pi4FsMIRipCryptoAuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRipCryptoAuthTable
            (pi4FsMIRipCryptoAuthIfIndex,
             pu4FsMIRipCryptoAuthAddress, pi4FsMIRipCryptoAuthKeyId);
        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRipCryptoAuthTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                nextFsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                nextFsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId
                nextFsMIRipCryptoAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRipCryptoAuthTable (INT4 i4FsMIRipContextId,
                                       INT4 *pi4NextFsMIRipContextId,
                                       INT4 i4FsMIRipCryptoAuthIfIndex,
                                       INT4 *pi4NextFsMIRipCryptoAuthIfIndex,
                                       UINT4 u4FsMIRipCryptoAuthAddress,
                                       UINT4 *pu4NextFsMIRipCryptoAuthAddress,
                                       INT4 i4FsMIRipCryptoAuthKeyId,
                                       INT4 *pi4NextFsMIRipCryptoAuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = (INT1) RipSetContext ((UINT4) i4FsMIRipContextId);
    if (i1RetVal == SNMP_SUCCESS)
    {
        i1RetVal =
            nmhGetNextIndexFsRipCryptoAuthTable (i4FsMIRipCryptoAuthIfIndex,
                                                 pi4NextFsMIRipCryptoAuthIfIndex,
                                                 u4FsMIRipCryptoAuthAddress,
                                                 pu4NextFsMIRipCryptoAuthAddress,
                                                 i4FsMIRipCryptoAuthKeyId,
                                                 pi4NextFsMIRipCryptoAuthKeyId);
        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }

    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) ==
            RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRipCryptoAuthTable
            (pi4NextFsMIRipCryptoAuthIfIndex,
             pu4NextFsMIRipCryptoAuthAddress, pi4NextFsMIRipCryptoAuthKeyId);

        RipResetContext ();
        i4FsMIRipContextId = *pi4NextFsMIRipContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipCryptoAuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                retValFsMIRipCryptoAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipCryptoAuthKey (INT4 i4FsMIRipContextId,
                            INT4 i4FsMIRipCryptoAuthIfIndex,
                            UINT4 u4FsMIRipCryptoAuthAddress,
                            INT4 i4FsMIRipCryptoAuthKeyId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIRipCryptoAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipCryptoAuthKey (i4FsMIRipCryptoAuthIfIndex,
                                         u4FsMIRipCryptoAuthAddress,
                                         i4FsMIRipCryptoAuthKeyId,
                                         pRetValFsMIRipCryptoAuthKey);
    RipResetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIRipCryptoKeyStartAccept
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                retValFsMIRipCryptoKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipCryptoKeyStartAccept (INT4 i4FsMIRipContextId,
                                   INT4 i4FsMIRipCryptoAuthIfIndex,
                                   UINT4 u4FsMIRipCryptoAuthAddress,
                                   INT4 i4FsMIRipCryptoAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIRipCryptoKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsRipCryptoKeyStartAccept
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pRetValFsMIRipCryptoKeyStartAccept);
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipCryptoKeyStartGenerate
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                retValFsMIRipCryptoKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipCryptoKeyStartGenerate (INT4 i4FsMIRipContextId,
                                     INT4 i4FsMIRipCryptoAuthIfIndex,
                                     UINT4 u4FsMIRipCryptoAuthAddress,
                                     INT4 i4FsMIRipCryptoAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIRipCryptoKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsRipCryptoKeyStartGenerate
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pRetValFsMIRipCryptoKeyStartGenerate);
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipCryptoKeyStopGenerate
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                retValFsMIRipCryptoKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipCryptoKeyStopGenerate (INT4 i4FsMIRipContextId,
                                    INT4 i4FsMIRipCryptoAuthIfIndex,
                                    UINT4 u4FsMIRipCryptoAuthAddress,
                                    INT4 i4FsMIRipCryptoAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMIRipCryptoKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsRipCryptoKeyStopGenerate
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pRetValFsMIRipCryptoKeyStopGenerate);
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipCryptoKeyStopAccept
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                retValFsMIRipCryptoKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipCryptoKeyStopAccept (INT4 i4FsMIRipContextId,
                                  INT4 i4FsMIRipCryptoAuthIfIndex,
                                  UINT4 u4FsMIRipCryptoAuthAddress,
                                  INT4 i4FsMIRipCryptoAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsMIRipCryptoKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsRipCryptoKeyStopAccept
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pRetValFsMIRipCryptoKeyStopAccept);
    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipCryptoKeyStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                retValFsMIRipCryptoKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipCryptoKeyStatus (INT4 i4FsMIRipContextId,
                              INT4 i4FsMIRipCryptoAuthIfIndex,
                              UINT4 u4FsMIRipCryptoAuthAddress,
                              INT4 i4FsMIRipCryptoAuthKeyId,
                              INT4 *pi4RetValFsMIRipCryptoKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsRipCryptoKeyStatus
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pi4RetValFsMIRipCryptoKeyStatus);
    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipCryptoAuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                setValFsMIRipCryptoAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipCryptoAuthKey (INT4 i4FsMIRipContextId,
                            INT4 i4FsMIRipCryptoAuthIfIndex,
                            UINT4 u4FsMIRipCryptoAuthAddress,
                            INT4 i4FsMIRipCryptoAuthKeyId,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsMIRipCryptoAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipCryptoAuthKey (i4FsMIRipCryptoAuthIfIndex,
                                         u4FsMIRipCryptoAuthAddress,
                                         i4FsMIRipCryptoAuthKeyId,
                                         pSetValFsMIRipCryptoAuthKey);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipCryptoKeyStartAccept
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                setValFsMIRipCryptoKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipCryptoKeyStartAccept (INT4 i4FsMIRipContextId,
                                   INT4 i4FsMIRipCryptoAuthIfIndex,
                                   UINT4 u4FsMIRipCryptoAuthAddress,
                                   INT4 i4FsMIRipCryptoAuthKeyId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMIRipCryptoKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipCryptoKeyStartAccept
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pSetValFsMIRipCryptoKeyStartAccept);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipCryptoKeyStartGenerate
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                setValFsMIRipCryptoKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipCryptoKeyStartGenerate (INT4 i4FsMIRipContextId,
                                     INT4 i4FsMIRipCryptoAuthIfIndex,
                                     UINT4 u4FsMIRipCryptoAuthAddress,
                                     INT4 i4FsMIRipCryptoAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsMIRipCryptoKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipCryptoKeyStartGenerate
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pSetValFsMIRipCryptoKeyStartGenerate);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipCryptoKeyStopGenerate
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                setValFsMIRipCryptoKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipCryptoKeyStopGenerate (INT4 i4FsMIRipContextId,
                                    INT4 i4FsMIRipCryptoAuthIfIndex,
                                    UINT4 u4FsMIRipCryptoAuthAddress,
                                    INT4 i4FsMIRipCryptoAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsMIRipCryptoKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipCryptoKeyStopGenerate
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pSetValFsMIRipCryptoKeyStopGenerate);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipCryptoKeyStopAccept
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                setValFsMIRipCryptoKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipCryptoKeyStopAccept (INT4 i4FsMIRipContextId,
                                  INT4 i4FsMIRipCryptoAuthIfIndex,
                                  UINT4 u4FsMIRipCryptoAuthAddress,
                                  INT4 i4FsMIRipCryptoAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsMIRipCryptoKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipCryptoKeyStopAccept
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pSetValFsMIRipCryptoKeyStopAccept);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipCryptoKeyStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                setValFsMIRipCryptoKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipCryptoKeyStatus (INT4 i4FsMIRipContextId,
                              INT4 i4FsMIRipCryptoAuthIfIndex,
                              UINT4 u4FsMIRipCryptoAuthAddress,
                              INT4 i4FsMIRipCryptoAuthKeyId,
                              INT4 i4SetValFsMIRipCryptoKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFsRipCryptoKeyStatus
        (i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, i4SetValFsMIRipCryptoKeyStatus);
    RipResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipCryptoAuthKey
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                testValFsMIRipCryptoAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipCryptoAuthKey (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIRipContextId,
                               INT4 i4FsMIRipCryptoAuthIfIndex,
                               UINT4 u4FsMIRipCryptoAuthAddress,
                               INT4 i4FsMIRipCryptoAuthKeyId,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsMIRipCryptoAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2FsRipCryptoAuthKey (pu4ErrorCode,
                                            i4FsMIRipCryptoAuthIfIndex,
                                            u4FsMIRipCryptoAuthAddress,
                                            i4FsMIRipCryptoAuthKeyId,
                                            pTestValFsMIRipCryptoAuthKey);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipCryptoKeyStartAccept
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                testValFsMIRipCryptoKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipCryptoKeyStartAccept (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIRipContextId,
                                      INT4 i4FsMIRipCryptoAuthIfIndex,
                                      UINT4 u4FsMIRipCryptoAuthAddress,
                                      INT4 i4FsMIRipCryptoAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMIRipCryptoKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2FsRipCryptoKeyStartAccept
        (pu4ErrorCode,
         i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pTestValFsMIRipCryptoKeyStartAccept);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipCryptoKeyStartGenerate
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                testValFsMIRipCryptoKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipCryptoKeyStartGenerate (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIRipContextId,
                                        INT4 i4FsMIRipCryptoAuthIfIndex,
                                        UINT4 u4FsMIRipCryptoAuthAddress,
                                        INT4 i4FsMIRipCryptoAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsMIRipCryptoKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2FsRipCryptoKeyStartGenerate
        (pu4ErrorCode,
         i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pTestValFsMIRipCryptoKeyStartGenerate);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipCryptoKeyStopGenerate
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                testValFsMIRipCryptoKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipCryptoKeyStopGenerate (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIRipContextId,
                                       INT4 i4FsMIRipCryptoAuthIfIndex,
                                       UINT4 u4FsMIRipCryptoAuthAddress,
                                       INT4 i4FsMIRipCryptoAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsMIRipCryptoKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2FsRipCryptoKeyStopGenerate
        (pu4ErrorCode,
         i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pTestValFsMIRipCryptoKeyStopGenerate);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipCryptoKeyStopAccept
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                testValFsMIRipCryptoKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipCryptoKeyStopAccept (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRipContextId,
                                     INT4 i4FsMIRipCryptoAuthIfIndex,
                                     UINT4 u4FsMIRipCryptoAuthAddress,
                                     INT4 i4FsMIRipCryptoAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsMIRipCryptoKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2FsRipCryptoKeyStopAccept
        (pu4ErrorCode,
         i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, pTestValFsMIRipCryptoKeyStopAccept);
    RipResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipCryptoKeyStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId

                The Object 
                testValFsMIRipCryptoKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipCryptoKeyStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIRipContextId,
                                 INT4 i4FsMIRipCryptoAuthIfIndex,
                                 UINT4 u4FsMIRipCryptoAuthAddress,
                                 INT4 i4FsMIRipCryptoAuthKeyId,
                                 INT4 i4TestValFsMIRipCryptoKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhTestv2FsRipCryptoKeyStatus
        (pu4ErrorCode,
         i4FsMIRipCryptoAuthIfIndex,
         u4FsMIRipCryptoAuthAddress,
         i4FsMIRipCryptoAuthKeyId, i4TestValFsMIRipCryptoKeyStatus);
    RipResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipCryptoAuthTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipCryptoAuthIfIndex
                FsMIRipCryptoAuthAddress
                FsMIRipCryptoAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipCryptoAuthTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRip2NBRUnicastListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRip2NBRUnicastListTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2NBRUnicastIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRip2NBRUnicastListTable (INT4 i4FsMIRipContextId,
                                                     UINT4
                                                     u4FsMIRip2NBRUnicastIpAddr)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm ((INT4) i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsRip2NBRUnicastListTable
        (u4FsMIRip2NBRUnicastIpAddr);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRip2NBRUnicastListTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2NBRUnicastIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRip2NBRUnicastListTable (INT4 *pi4FsMIRipContextId,
                                             UINT4 *pu4FsMIRip2NBRUnicastIpAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRip2NBRUnicastListTable
            (pu4FsMIRip2NBRUnicastIpAddr);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRip2NBRUnicastListTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRip2NBRUnicastIpAddr
                nextFsMIRip2NBRUnicastIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRip2NBRUnicastListTable (INT4 i4FsMIRipContextId,
                                            INT4 *pi4NextFsMIRipContextId,
                                            UINT4 u4FsMIRip2NBRUnicastIpAddr,
                                            UINT4
                                            *pu4NextFsMIRip2NBRUnicastIpAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal =
            nmhGetNextIndexFsRip2NBRUnicastListTable
            (u4FsMIRip2NBRUnicastIpAddr, pu4NextFsMIRip2NBRUnicastIpAddr);

        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }
    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (*pi4NextFsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRip2NBRUnicastListTable
            (pu4NextFsMIRip2NBRUnicastIpAddr);

        RipResetContext ();
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRip2NBRUnicastNBRRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2NBRUnicastIpAddr

                The Object 
                retValFsMIRip2NBRUnicastNBRRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2NBRUnicastNBRRowStatus (INT4 i4FsMIRipContextId,
                                      UINT4 u4FsMIRip2NBRUnicastIpAddr,
                                      INT4
                                      *pi4RetValFsMIRip2NBRUnicastNBRRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2NBRUnicastNBRRowStatus (u4FsMIRip2NBRUnicastIpAddr,
                                                   pi4RetValFsMIRip2NBRUnicastNBRRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRip2NBRUnicastNBRRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2NBRUnicastIpAddr

                The Object 
                setValFsMIRip2NBRUnicastNBRRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRip2NBRUnicastNBRRowStatus (INT4 i4FsMIRipContextId,
                                      UINT4 u4FsMIRip2NBRUnicastIpAddr,
                                      INT4
                                      i4SetValFsMIRip2NBRUnicastNBRRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRip2NBRUnicastNBRRowStatus (u4FsMIRip2NBRUnicastIpAddr,
                                                   i4SetValFsMIRip2NBRUnicastNBRRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRip2NBRUnicastNBRRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2NBRUnicastIpAddr

                The Object 
                testValFsMIRip2NBRUnicastNBRRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRip2NBRUnicastNBRRowStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIRipContextId,
                                         UINT4 u4FsMIRip2NBRUnicastIpAddr,
                                         INT4
                                         i4TestValFsMIRip2NBRUnicastNBRRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRip2NBRUnicastNBRRowStatus (pu4ErrorCode,
                                                      u4FsMIRip2NBRUnicastIpAddr,
                                                      i4TestValFsMIRip2NBRUnicastNBRRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRip2NBRUnicastListTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2NBRUnicastIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRip2NBRUnicastListTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRip2LocalRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRip2LocalRouteTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRip2LocalRouteTable (INT4 i4FsMIRipContextId,
                                                 UINT4 u4FsMIRip2DestNet,
                                                 UINT4 u4FsMIRip2DestMask,
                                                 INT4 i4FsMIRip2Tos,
                                                 UINT4 u4FsMIRip2NextHop)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsRip2LocalRoutingTable (u4FsMIRip2DestNet,
                                                         u4FsMIRip2DestMask,
                                                         i4FsMIRip2Tos,
                                                         u4FsMIRip2NextHop);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRip2LocalRouteTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRip2LocalRouteTable (INT4 *pi4FsMIRipContextId,
                                         UINT4 *pu4FsMIRip2DestNet,
                                         UINT4 *pu4FsMIRip2DestMask,
                                         INT4 *pi4FsMIRip2Tos,
                                         UINT4 *pu4FsMIRip2NextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRip2LocalRoutingTable (pu4FsMIRip2DestNet,
                                                     pu4FsMIRip2DestMask,
                                                     pi4FsMIRip2Tos,
                                                     pu4FsMIRip2NextHop);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRip2LocalRouteTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRip2DestNet
                nextFsMIRip2DestNet
                FsMIRip2DestMask
                nextFsMIRip2DestMask
                FsMIRip2Tos
                nextFsMIRip2Tos
                FsMIRip2NextHop
                nextFsMIRip2NextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRip2LocalRouteTable (INT4 i4FsMIRipContextId,
                                        INT4 *pi4NextFsMIRipContextId,
                                        UINT4 u4FsMIRip2DestNet,
                                        UINT4 *pu4NextFsMIRip2DestNet,
                                        UINT4 u4FsMIRip2DestMask,
                                        UINT4 *pu4NextFsMIRip2DestMask,
                                        INT4 i4FsMIRip2Tos,
                                        INT4 *pi4NextFsMIRip2Tos,
                                        UINT4 u4FsMIRip2NextHop,
                                        UINT4 *pu4NextFsMIRip2NextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexFsRip2LocalRoutingTable (u4FsMIRip2DestNet,
                                                           pu4NextFsMIRip2DestNet,
                                                           u4FsMIRip2DestMask,
                                                           pu4NextFsMIRip2DestMask,
                                                           i4FsMIRip2Tos,
                                                           pi4NextFsMIRip2Tos,
                                                           u4FsMIRip2NextHop,
                                                           pu4NextFsMIRip2NextHop);

        RipResetContext ();
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        *pi4NextFsMIRipContextId = i4FsMIRipContextId;
        return i1RetVal;
    }
    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (*pi4NextFsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRip2LocalRoutingTable (pu4NextFsMIRip2DestNet,
                                                     pu4NextFsMIRip2DestMask,
                                                     pi4NextFsMIRip2Tos,
                                                     pu4NextFsMIRip2NextHop);

        RipResetContext ();
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRip2RtIfIndex
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2RtIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2RtIfIndex (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                         UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                         UINT4 u4FsMIRip2NextHop,
                         INT4 *pi4RetValFsMIRip2RtIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2RtIfIndex (u4FsMIRip2DestNet,
                                      u4FsMIRip2DestMask, i4FsMIRip2Tos,
                                      u4FsMIRip2NextHop,
                                      pi4RetValFsMIRip2RtIfIndex);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2RtType
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2RtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2RtType (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                      UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                      UINT4 u4FsMIRip2NextHop, INT4 *pi4RetValFsMIRip2RtType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2RtType (u4FsMIRip2DestNet,
                                   u4FsMIRip2DestMask, i4FsMIRip2Tos,
                                   u4FsMIRip2NextHop, pi4RetValFsMIRip2RtType);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2Proto
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2Proto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2Proto (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                     UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                     UINT4 u4FsMIRip2NextHop, INT4 *pi4RetValFsMIRip2Proto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2Proto (u4FsMIRip2DestNet, u4FsMIRip2DestMask,
                                  i4FsMIRip2Tos, u4FsMIRip2NextHop,
                                  pi4RetValFsMIRip2Proto);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2ChgTime
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2ChgTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2ChgTime (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                       UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                       UINT4 u4FsMIRip2NextHop, INT4 *pi4RetValFsMIRip2ChgTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2ChgTime (u4FsMIRip2DestNet, u4FsMIRip2DestMask,
                                    i4FsMIRip2Tos, u4FsMIRip2NextHop,
                                    pi4RetValFsMIRip2ChgTime);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2Metric
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2Metric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2Metric (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                      UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                      UINT4 u4FsMIRip2NextHop, INT4 *pi4RetValFsMIRip2Metric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2Metric (u4FsMIRip2DestNet, u4FsMIRip2DestMask,
                                   i4FsMIRip2Tos, u4FsMIRip2NextHop,
                                   pi4RetValFsMIRip2Metric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2RowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2RowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2RowStatus (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                         UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                         UINT4 u4FsMIRip2NextHop,
                         INT4 *pi4RetValFsMIRip2RowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2RowStatus (u4FsMIRip2DestNet, u4FsMIRip2DestMask,
                                      i4FsMIRip2Tos, u4FsMIRip2NextHop,
                                      pi4RetValFsMIRip2RowStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRip2Gateway
 Input       :  The Indices
                FsMIRipContextId
                FsMIRip2DestNet
                FsMIRip2DestMask
                FsMIRip2Tos
                FsMIRip2NextHop

                The Object 
                retValFsMIRip2Gateway
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRip2Gateway (INT4 i4FsMIRipContextId, UINT4 u4FsMIRip2DestNet,
                       UINT4 u4FsMIRip2DestMask, INT4 i4FsMIRip2Tos,
                       UINT4 u4FsMIRip2NextHop, UINT4 *pu4RetValFsMIRip2Gateway)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRip2Gateway (u4FsMIRip2DestNet, u4FsMIRip2DestMask,
                                    i4FsMIRip2Tos, u4FsMIRip2NextHop,
                                    pu4RetValFsMIRip2Gateway);

    RipResetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIRipAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRipAggTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipIfIndex
                FsMIRipAggAddress
                FsMIRipAggAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRipAggTable (INT4 i4FsMIRipContextId,
                                         INT4 i4FsMIRipIfIndex,
                                         UINT4 u4FsMIRipAggAddress,
                                         UINT4 u4FsMIRipAggAddressMask)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal = nmhValidateIndexInstanceFsRipAggTable (i4FsMIRipIfIndex,
                                                      u4FsMIRipAggAddress,
                                                      u4FsMIRipAggAddressMask);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRipAggTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipIfIndex
                FsMIRipAggAddress
                FsMIRipAggAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRipAggTable (INT4 *pi4FsMIRipContextId,
                                 INT4 *pi4FsMIRipIfIndex,
                                 UINT4 *pu4FsMIRipAggAddress,
                                 UINT4 *pu4FsMIRipAggAddressMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRipAggTable (pi4FsMIRipIfIndex,
                                                  pu4FsMIRipAggAddress,
                                                  pu4FsMIRipAggAddressMask);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRipAggTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRipIfIndex
                nextFsMIRipIfIndex
                FsMIRipAggAddress
                nextFsMIRipAggAddress
                FsMIRipAggAddressMask
                nextFsMIRipAggAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRipAggTable (INT4 i4FsMIRipContextId,
                                INT4 *pi4NextFsMIRipContextId,
                                INT4 i4FsMIRipIfIndex,
                                INT4 *pi4NextFsMIRipIfIndex,
                                UINT4 u4FsMIRipAggAddress,
                                UINT4 *pu4NextFsMIRipAggAddress,
                                UINT4 u4FsMIRipAggAddressMask,
                                UINT4 *pu4NextFsMIRipAggAddressMask)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexFsRipAggTable (i4FsMIRipIfIndex,
                                                 pi4NextFsMIRipIfIndex,
                                                 u4FsMIRipAggAddress,
                                                 pu4NextFsMIRipAggAddress,
                                                 u4FsMIRipAggAddressMask,
                                                 pu4NextFsMIRipAggAddressMask);

        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }

    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) ==
            RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRipAggTable (pi4NextFsMIRipIfIndex,
                                                  pu4NextFsMIRipAggAddress,
                                                  pu4NextFsMIRipAggAddressMask);

        RipResetContext ();
        i4FsMIRipContextId = *pi4NextFsMIRipContextId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipAggStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipIfIndex
                FsMIRipAggAddress
                FsMIRipAggAddressMask

                The Object 
                retValFsMIRipAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipAggStatus (INT4 i4FsMIRipContextId, INT4 i4FsMIRipIfIndex,
                        UINT4 u4FsMIRipAggAddress,
                        UINT4 u4FsMIRipAggAddressMask,
                        INT4 *pi4RetValFsMIRipAggStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipAggStatus (i4FsMIRipIfIndex, u4FsMIRipAggAddress,
                                     u4FsMIRipAggAddressMask,
                                     pi4RetValFsMIRipAggStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipAggStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipIfIndex
                FsMIRipAggAddress
                FsMIRipAggAddressMask

                The Object 
                setValFsMIRipAggStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipAggStatus (INT4 i4FsMIRipContextId, INT4 i4FsMIRipIfIndex,
                        UINT4 u4FsMIRipAggAddress,
                        UINT4 u4FsMIRipAggAddressMask,
                        INT4 i4SetValFsMIRipAggStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipAggStatus (i4FsMIRipIfIndex, u4FsMIRipAggAddress,
                                     u4FsMIRipAggAddressMask,
                                     i4SetValFsMIRipAggStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipAggStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipIfIndex
                FsMIRipAggAddress
                FsMIRipAggAddressMask

                The Object 
                testValFsMIRipAggStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipAggStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                           INT4 i4FsMIRipIfIndex, UINT4 u4FsMIRipAggAddress,
                           UINT4 u4FsMIRipAggAddressMask,
                           INT4 i4TestValFsMIRipAggStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipAggStatus (pu4ErrorCode, i4FsMIRipIfIndex,
                                        u4FsMIRipAggAddress,
                                        u4FsMIRipAggAddressMask,
                                        i4TestValFsMIRipAggStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipAggTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipIfIndex
                FsMIRipAggAddress
                FsMIRipAggAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipAggTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRipRRDGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRipRRDGlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRipRRDGlobalTable (INT4 i4FsMIRipContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRipRRDGlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRipRRDGlobalTable (INT4 *pi4FsMIRipContextId)
{
    if (RipGetFirstCxtId (pi4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRipRRDGlobalTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRipRRDGlobalTable (INT4 i4FsMIRipContextId,
                                      INT4 *pi4NextFsMIRipContextId)
{
    if (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) !=
        RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDGlobalStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDGlobalStatus (INT4 i4FsMIRipContextId,
                              INT4 *pi4RetValFsMIRipRRDGlobalStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipRRDGlobalStatus (pi4RetValFsMIRipRRDGlobalStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDSrcProtoMaskEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDSrcProtoMaskEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDSrcProtoMaskEnable (INT4 i4FsMIRipContextId,
                                    INT4 *pi4RetValFsMIRipRRDSrcProtoMaskEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRipRRDSrcProtoMaskEnable
        (pi4RetValFsMIRipRRDSrcProtoMaskEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDSrcProtoMaskDisable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDSrcProtoMaskDisable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDSrcProtoMaskDisable (INT4 i4FsMIRipContextId,
                                     INT4
                                     *pi4RetValFsMIRipRRDSrcProtoMaskDisable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRipRRDSrcProtoMaskDisable
        (pi4RetValFsMIRipRRDSrcProtoMaskDisable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDRouteTagType
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDRouteTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDRouteTagType (INT4 i4FsMIRipContextId,
                              INT4 *pi4RetValFsMIRipRRDRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipRRDRouteTagType (pi4RetValFsMIRipRRDRouteTagType);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDRouteTag
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDRouteTag (INT4 i4FsMIRipContextId,
                          INT4 *pi4RetValFsMIRipRRDRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipRRDRouteTag (pi4RetValFsMIRipRRDRouteTag);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDRouteDefMetric
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDRouteDefMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDRouteDefMetric (INT4 i4FsMIRipContextId,
                                INT4 *pi4RetValFsMIRipRRDRouteDefMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipRRDRouteDefMetric (pi4RetValFsMIRipRRDRouteDefMetric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipRRDRouteMapEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipRRDRouteMapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipRRDRouteMapEnable (INT4 i4FsMIRipContextId,
                                tSNMP_OCTET_STRING_TYPE
                                * pRetValFsMIRipRRDRouteMapEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipRRDRouteMapEnable (pRetValFsMIRipRRDRouteMapEnable);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDGlobalStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDGlobalStatus (INT4 i4FsMIRipContextId,
                              INT4 i4SetValFsMIRipRRDGlobalStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipRRDGlobalStatus (i4SetValFsMIRipRRDGlobalStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDSrcProtoMaskEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDSrcProtoMaskEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDSrcProtoMaskEnable (INT4 i4FsMIRipContextId,
                                    INT4 i4SetValFsMIRipRRDSrcProtoMaskEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRipRRDSrcProtoMaskEnable (i4SetValFsMIRipRRDSrcProtoMaskEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDSrcProtoMaskDisable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDSrcProtoMaskDisable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDSrcProtoMaskDisable (INT4 i4FsMIRipContextId,
                                     INT4 i4SetValFsMIRipRRDSrcProtoMaskDisable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRipRRDSrcProtoMaskDisable
        (i4SetValFsMIRipRRDSrcProtoMaskDisable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDRouteTagType
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDRouteTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDRouteTagType (INT4 i4FsMIRipContextId,
                              INT4 i4SetValFsMIRipRRDRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipRRDRouteTagType (i4SetValFsMIRipRRDRouteTagType);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDRouteTag
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDRouteTag (INT4 i4FsMIRipContextId,
                          INT4 i4SetValFsMIRipRRDRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipRRDRouteTag (i4SetValFsMIRipRRDRouteTag);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDRouteDefMetric
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDRouteDefMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDRouteDefMetric (INT4 i4FsMIRipContextId,
                                INT4 i4SetValFsMIRipRRDRouteDefMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipRRDRouteDefMetric (i4SetValFsMIRipRRDRouteDefMetric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipRRDRouteMapEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipRRDRouteMapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipRRDRouteMapEnable (INT4 i4FsMIRipContextId,
                                tSNMP_OCTET_STRING_TYPE
                                * pSetValFsMIRipRRDRouteMapEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipRRDRouteMapEnable (pSetValFsMIRipRRDRouteMapEnable);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDGlobalStatus
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDGlobalStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                 INT4 i4TestValFsMIRipRRDGlobalStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDGlobalStatus (pu4ErrorCode,
                                              i4TestValFsMIRipRRDGlobalStatus);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDSrcProtoMaskEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDSrcProtoMaskEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDSrcProtoMaskEnable (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIRipContextId,
                                       INT4
                                       i4TestValFsMIRipRRDSrcProtoMaskEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDSrcProtoMaskEnable (pu4ErrorCode,
                                                    i4TestValFsMIRipRRDSrcProtoMaskEnable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDSrcProtoMaskDisable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDSrcProtoMaskDisable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDSrcProtoMaskDisable (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIRipContextId,
                                        INT4
                                        i4TestValFsMIRipRRDSrcProtoMaskDisable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDSrcProtoMaskDisable (pu4ErrorCode,
                                                     i4TestValFsMIRipRRDSrcProtoMaskDisable);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDRouteTagType
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDRouteTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDRouteTagType (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                 INT4 i4TestValFsMIRipRRDRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDRouteTagType (pu4ErrorCode,
                                              i4TestValFsMIRipRRDRouteTagType);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDRouteTag
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDRouteTag (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                             INT4 i4TestValFsMIRipRRDRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDRouteTag (pu4ErrorCode,
                                          i4TestValFsMIRipRRDRouteTag);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDRouteDefMetric
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDRouteDefMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDRouteDefMetric (UINT4 *pu4ErrorCode, INT4 i4FsMIRipContextId,
                                   INT4 i4TestValFsMIRipRRDRouteDefMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDRouteDefMetric (pu4ErrorCode,
                                                i4TestValFsMIRipRRDRouteDefMetric);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipRRDRouteMapEnable
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipRRDRouteMapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipRRDRouteMapEnable (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIRipContextId,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTestValFsMIRipRRDRouteMapEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipRRDRouteMapEnable (pu4ErrorCode,
                                                pTestValFsMIRipRRDRouteMapEnable);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipRRDGlobalTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipRRDGlobalTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRipDistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRipDistInOutRouteMapTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRipDistInOutRouteMapTable (INT4 i4FsMIRipContextId,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsMIRipDistInOutRouteMapName,
                                                       INT4
                                                       i4FsMIRipDistInOutRouteMapType)
{
    INT4                i4MaxCxt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);

    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT) ||
        (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }

    i1RetVal =
        nmhValidateIndexInstanceFsRipDistInOutRouteMapTable
        (pFsMIRipDistInOutRouteMapName, i4FsMIRipDistInOutRouteMapType);

    RipResetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRipDistInOutRouteMapTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRipDistInOutRouteMapTable (INT4 *pi4FsMIRipContextId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsMIRipDistInOutRouteMapName,
                                               INT4
                                               *pi4FsMIRipDistInOutRouteMapType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRipContextId;

    if (RipGetFirstCxtId (pi4FsMIRipContextId) == RIP_FAILURE)
    {
        return i1RetVal;
    }

    do
    {
        i4FsMIRipContextId = *pi4FsMIRipContextId;

        if (RipSetContext ((UINT4) *pi4FsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRipDistInOutRouteMapTable
            (pFsMIRipDistInOutRouteMapName, pi4FsMIRipDistInOutRouteMapType);

        RipResetContext ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (RipGetNextCxtId (i4FsMIRipContextId, pi4FsMIRipContextId)
           != RIP_FAILURE);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRipDistInOutRouteMapTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
                FsMIRipDistInOutRouteMapName
                nextFsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType
                nextFsMIRipDistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRipDistInOutRouteMapTable (INT4 i4FsMIRipContextId,
                                              INT4 *pi4NextFsMIRipContextId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMIRipDistInOutRouteMapName,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsMIRipDistInOutRouteMapName,
                                              INT4
                                              i4FsMIRipDistInOutRouteMapType,
                                              INT4
                                              *pi4NextFsMIRipDistInOutRouteMapType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i1RetVal =
         (INT1) RipSetContext ((UINT4) i4FsMIRipContextId)) == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetNextIndexFsRipDistInOutRouteMapTable
            (pFsMIRipDistInOutRouteMapName,
             pNextFsMIRipDistInOutRouteMapName,
             i4FsMIRipDistInOutRouteMapType,
             pi4NextFsMIRipDistInOutRouteMapType);

        RipResetContext ();
    }

    *pi4NextFsMIRipContextId = i4FsMIRipContextId;
    if (i1RetVal == SNMP_SUCCESS)
    {
        return i1RetVal;
    }
    while ((i1RetVal == SNMP_FAILURE) &&
           (RipGetNextCxtId (*pi4NextFsMIRipContextId, pi4NextFsMIRipContextId)
            == RIP_SUCCESS))
    {
        if (RipSetContext ((UINT4) *pi4NextFsMIRipContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetFirstIndexFsRipDistInOutRouteMapTable
            (pNextFsMIRipDistInOutRouteMapName,
             pi4NextFsMIRipDistInOutRouteMapType);

        RipResetContext ();
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipDistInOutRouteMapValue
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType

                The Object 
                retValFsMIRipDistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipDistInOutRouteMapValue (INT4 i4FsMIRipContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIRipDistInOutRouteMapName,
                                     INT4 i4FsMIRipDistInOutRouteMapType,
                                     INT4
                                     *pi4RetValFsMIRipDistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipDistInOutRouteMapValue
        (pFsMIRipDistInOutRouteMapName,
         i4FsMIRipDistInOutRouteMapType,
         pi4RetValFsMIRipDistInOutRouteMapValue);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRipDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType

                The Object 
                retValFsMIRipDistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipDistInOutRouteMapRowStatus (INT4 i4FsMIRipContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIRipDistInOutRouteMapName,
                                         INT4 i4FsMIRipDistInOutRouteMapType,
                                         INT4
                                         *pi4RetValFsMIRipDistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipDistInOutRouteMapRowStatus
        (pFsMIRipDistInOutRouteMapName,
         i4FsMIRipDistInOutRouteMapType,
         pi4RetValFsMIRipDistInOutRouteMapRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipDistInOutRouteMapValue
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType

                The Object 
                setValFsMIRipDistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipDistInOutRouteMapValue (INT4 i4FsMIRipContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIRipDistInOutRouteMapName,
                                     INT4 i4FsMIRipDistInOutRouteMapType,
                                     INT4 i4SetValFsMIRipDistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipDistInOutRouteMapValue
        (pFsMIRipDistInOutRouteMapName,
         i4FsMIRipDistInOutRouteMapType, i4SetValFsMIRipDistInOutRouteMapValue);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRipDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType

                The Object 
                setValFsMIRipDistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipDistInOutRouteMapRowStatus (INT4 i4FsMIRipContextId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIRipDistInOutRouteMapName,
                                         INT4 i4FsMIRipDistInOutRouteMapType,
                                         INT4
                                         i4SetValFsMIRipDistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipDistInOutRouteMapRowStatus
        (pFsMIRipDistInOutRouteMapName,
         i4FsMIRipDistInOutRouteMapType,
         i4SetValFsMIRipDistInOutRouteMapRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipDistInOutRouteMapValue
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType

                The Object 
                testValFsMIRipDistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipDistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIRipContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIRipDistInOutRouteMapName,
                                        INT4 i4FsMIRipDistInOutRouteMapType,
                                        INT4
                                        i4TestValFsMIRipDistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipDistInOutRouteMapValue (pu4ErrorCode,
                                                     pFsMIRipDistInOutRouteMapName,
                                                     i4FsMIRipDistInOutRouteMapType,
                                                     i4TestValFsMIRipDistInOutRouteMapValue);

    RipResetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRipDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType

                The Object 
                testValFsMIRipDistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipDistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIRipContextId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIRipDistInOutRouteMapName,
                                            INT4 i4FsMIRipDistInOutRouteMapType,
                                            INT4
                                            i4TestValFsMIRipDistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhTestv2FsRipDistInOutRouteMapRowStatus (pu4ErrorCode,
                                                         pFsMIRipDistInOutRouteMapName,
                                                         i4FsMIRipDistInOutRouteMapType,
                                                         i4TestValFsMIRipDistInOutRouteMapRowStatus);

    RipResetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipDistInOutRouteMapTable
 Input       :  The Indices
                FsMIRipContextId
                FsMIRipDistInOutRouteMapName
                FsMIRipDistInOutRouteMapType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipDistInOutRouteMapTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRipPreferenceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRipPreferenceTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRipPreferenceTable (INT4 i4FsMIRipContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);
    if ((i4FsMIRipContextId < RIP_DEFAULT_CXT)
        || (i4FsMIRipContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }
    if (RipCxtIsExistInVcm (i4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (RipCxtExists (i4FsMIRipContextId) == RIP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRipPreferenceTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRipPreferenceTable (INT4 *pi4FsMIRipContextId)
{
    if (RipGetFirstCxtId (pi4FsMIRipContextId) != RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRipPreferenceTable
 Input       :  The Indices
                FsMIRipContextId
                nextFsMIRipContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRipPreferenceTable (INT4 i4FsMIRipContextId,
                                       INT4 *pi4NextFsMIRipContextId)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP_MIN (MAX_RIP_CONTEXT_LIMIT,
                        (INT4) FsRIPSizingParams[MAX_RIP_CONTEXT_SIZING_ID].
                        u4PreAllocatedUnits);
    if (i4FsMIRipContextId > i4MaxCxt)
    {
        return SNMP_FAILURE;
    }
    if (RipGetNextCxtId (i4FsMIRipContextId, pi4NextFsMIRipContextId) !=
        RIP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRipPreferenceValue
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                retValFsMIRipPreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRipPreferenceValue (INT4 i4FsMIRipContextId,
                              INT4 *pi4RetValFsMIRipPreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRipPreferenceValue (pi4RetValFsMIRipPreferenceValue);
    RipResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRipPreferenceValue
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                setValFsMIRipPreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRipPreferenceValue (INT4 i4FsMIRipContextId,
                              INT4 i4SetValFsMIRipPreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRipPreferenceValue (i4SetValFsMIRipPreferenceValue);
    RipResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRipPreferenceValue
 Input       :  The Indices
                FsMIRipContextId

                The Object 
                testValFsMIRipPreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRipPreferenceValue (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIRipContextId,
                                 INT4 i4TestValFsMIRipPreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (RipSetContext ((UINT4) i4FsMIRipContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FsRipPreferenceValue (pu4ErrorCode,
                                              i4TestValFsMIRipPreferenceValue);
    RipResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRipPreferenceTable
 Input       :  The Indices
                FsMIRipContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRipPreferenceTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
