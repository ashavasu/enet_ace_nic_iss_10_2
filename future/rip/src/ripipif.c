/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripipif.c,v 1.32 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains routines that act as an   
 *             interface between RIP and IP module. Route   
 *             database lookup from IP module, Static route 
 *             lookup from RIP module form some of the main 
 *             components of this file.                   
 *
 *******************************************************************/
#include "ripinc.h"

        /***************************************************************
        * Functions that are called from RIP module to IP module are   *
        * present here.                                                *
        ***************************************************************/
/*******************************************************************************

    Function            :   rip_task_ip_get_net_mask.
    
    Description         :   RIP derives the network mask for a destination from
                            the interface over which that destination network is
                            connected. If the destination is on the same natural
                            network as the interface and the interface mask
                            is more specific, interface mask is used. However,
                            if the destination is on the same natural network
                            with interface mask less than or equals the natural
                            mask, natural mask of the interface is used.
                            if destination is a remote address (not directly
                            connected), natural mask is used.
 
    Input Parameters    :   1. u2If, the interface through which the RIP
                               information need to be sent.
                            2. u4DestNet, the destination network.
                            3. i1NaturalMaskFlag. Controls the flow.
                               when it is -ve, natural mask of dest is returned.
                               when it is non-negative, the decision is taken
                               based as described above.
    
    Output Parameters   :   None.
    
    Global Variables
    Affected            :   None.
    
    Return Value        :   The mask for the destination is returned and the
                            type is UINT4.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_task_ip_get_net_mask
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/
extern INT4         CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName);
EXPORT UINT4
rip_task_ip_get_net_mask (UINT2 u2If, UINT4 u4DestNet, INT1 i1NaturalMaskFlag)
{
    UINT4               u4DestMask;
    UINT4               u4IfMask;

    u4DestMask = RIP_DEFAULT_NET_MASK (u4DestNet);

    if (i1NaturalMaskFlag < 0)
    {
        return (u4DestMask);
    }

    u4IfMask = RIP_DEFAULT_NET_MASK (RIPIF_GLBTAB_ADDR_OF (((UINT4) u2If)));
    if ((u4IfMask & RIPIF_GLBTAB_ADDR_OF (u2If)) == (u4DestMask & u4DestNet))
    {
        if ((u4IfMask > RIPIF_GLBTAB_MASK_OF (u2If)) &&
            (RIPIF_GLBTAB_MASK_OF (u2If) != RIP_BROADCAST_MASK))
        {
            return (RIPIF_GLBTAB_MASK_OF (u2If));
        }
        else
        {
            return (u4IfMask);
        }
    }
    else
    {
        return (u4DestMask);
    }
}

/*******************************************************************************

    Function            :   rip_ip_get_oper_status.

    Description         :   RIP queries IP module to get the operational status
                            for an interface. IP module the operational status
                            either "enabled" or "disabled".

    Input Parameters    :   u2If, the interface through which the RIP
                            information need to be sent.

    Output Parameters   :   None.

    Global Variables
    Affected            :   None.

    Return Value        :   The operational status is returned with type UINT2.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_ip_get_oepr_status
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT INT4
rip_ip_get_oper_status (UINT2 u2If)
{
    INT4                i4OperState;

    i4OperState = RIPIF_GLBTAB_OPER_OF (u2If);
    return (i4OperState);
}

/*******************************************************************************

    Function            :   rip_ip_join_mcast_group.

    Description         :   This function calls an IP module routine to join the
                            RIP specific multicast group address in the specified
                            interface.

    Input Parameters    :   u2If, the interface index.

    Output Parameters   :   None.

    Global Variables
    Affected            :   None.

    Return Value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_ip_join_mcast_group
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/
EXPORT VOID
rip_ip_join_mcast_group (UINT2 u2If)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ip_mreqn     mreqn;
    UINT1               RipMcastGroupAddr[4] = { 224, 0, 0, 9 };

    /* Add membership is done to receive the rip pkts addressed to 224.0.0.9
     * This SLI call is not supported by FSIP. By default FSIP receives all
     * mutilcast packets */
    memset (&mreqn, 0, sizeof (mreqn));
    memcpy (&mreqn.imr_multiaddr.s_addr, RipMcastGroupAddr,
            sizeof (RipMcastGroupAddr));

    mreqn.imr_ifindex = u2If;

    if (setsockopt (gRipRtr.i4RipSd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                    (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                 "Setsockopt Failed for IP_ADD_MEMBERSHIP in RIP. \n");
    }
#else
    UNUSED_PARAM (u2If);
#endif /* BSDCOMP_SLI_WANTED */
}

/*******************************************************************************

    Function            :   rip_ip_leave_mcast_group.

    Description         :   This function calls an IP module routine to leave the
                            RIP specific multicast group address in the specified
                            interface.

    Input Parameters    :   u2If, the interface index.

    Output Parameters   :   None.

    Global Variables
    Affected            :   None.

    Return Value        :   None.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_ip_leave_mcast_group
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/

EXPORT VOID
rip_ip_leave_mcast_group (UINT2 u2If)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ip_mreqn     mreqn;
    UINT1               RipMcastGroupAddr[4] = { 224, 0, 0, 9 };

    /* Add membership is done to receive the rip pkts addressed to 224.0.0.9
     * This SLI call is not supported by FSIP. By default FSIP receives all
     * mutilcast packets */
    memset (&mreqn, 0, sizeof (mreqn));
    memcpy (&mreqn.imr_multiaddr.s_addr, RipMcastGroupAddr,
            sizeof (RipMcastGroupAddr));

    mreqn.imr_ifindex = u2If;

    if (setsockopt (gRipRtr.i4RipSd, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                    (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                 "Setsockopt Failed for IP_DROP_MEMBERSHIP in RIP. \n");
    }
#else
    UNUSED_PARAM (u2If);
#endif /* BSDCOMP_SLI_WANTED */
}

        /***************************************************************
        * Functions that are called from IP module to RIP module are   *
        * present here.                                                *
        ***************************************************************/
/*******************************************************************************

    Function            : RipIfStateChgHdlr.
    
    Description         : This procedure is called to inform the change in the
                          operational status of a logical interface and is
                          indicated by the IP module. The value could be
                          IFACE_DELETED or OPER_STATE. Depending on the current
                          status of the iface the different actions are taken. 
                          
    Input parameters    : 1. pNetIfInfo, the logical interface.
                          2. u4BitMap, the event that happened.
    
    Output parameters   : None.
    
    Global variables
    Affected            : OperStatus for this interface in the record,
                          RipIfGblRec, indexed by the iface index.
    
    Return value        : Success is returned.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = RipIfStateChgHdlr
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/
EXPORT VOID
RipIfStateChgHdlr (tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap)
{
    tIpBuf             *pBuf = NULL;
    tIpParms           *pParms = NULL;
    UINT1               u1Cmd = 0;
    UINT2               u2RipIf;
    UINT4               u4CxtId = 0;
    tRipCxt            *pRipCxtEntry = NULL;
    UINT4               u4Addr = 0;
    CHR1               *pu1String = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    u2RipIf = (UINT2) pNetIfInfo->u4IfIndex;
    u4CxtId = pNetIfInfo->u4ContextId;
    u4Addr = pNetIfInfo->u4Addr;

    pRipCxtEntry = RipGetCxtInfoRec ((INT4) u4CxtId);
    if (pRipCxtEntry == NULL)
    {
        return;
    }

    /* Set the u1Cmd based on the BitMap and  Iface oper status */
    if (u4BitMap == IFACE_DELETED)
    {
        /* BitMap is IFACE_DELETED */
        u1Cmd = IP_INTERFACE_DELETE;
    }
    else
    {
        /* OPER_STATE indication has arrived.  */
        u1Cmd = (UINT1) pNetIfInfo->u4Oper;
    }
    pBuf = RIP_ALLOCATE_BUF (sizeof (UINT4), 0);
    if (pBuf == NULL)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "RipIfStateChgHdlr: Buffer allocation failed. \n");
        return;
    }

    pParms = (tIpParms *) RIP_GET_MODULE_DATA_PTR (pBuf);
    pParms->u2Port = (UINT2) u2RipIf;

    pParms->u1Cmd = u1Cmd;
    pParms->u4ContextId = u4CxtId;
    if (u4BitMap == 1)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Addr);
        CfaGetIfName (pNetIfInfo->u4CfaIfIndex, au1IfName);
        switch (pParms->u1Cmd)
        {
            case IPIF_OPER_ENABLE:
                RIP_TRC_ARG3 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              RIP_CRITICAL_TRC,
                              RIP_NAME,
                              "Handled interface-up message recieved for Route=%s NetMask=%d InterfaceIndex=%s\n\r",
                              pu1String, CliGetMaskBits (pNetIfInfo->u4NetMask),
                              au1IfName);

                break;

            case IPIF_OPER_DISABLE:
                RIP_TRC_ARG3 (RIP_MOD_TRC,
                              pRipCxtEntry->u4RipTrcFlag,
                              pRipCxtEntry->i4CxtId,
                              RIP_CRITICAL_TRC,
                              RIP_NAME,
                              "Handled interface-Down message recieved for Route=%s NetMask=%d InterfaceIndex=%s\n\r",
                              pu1String, CliGetMaskBits (pNetIfInfo->u4NetMask),
                              au1IfName);

                break;
            default:
                break;
        }
    }
    if (RipSendToQ (pBuf, RIP_SNMP_MSG) != RIP_SUCCESS)
    {
        RIP_RELEASE_BUF (pBuf, FALSE);
    }
}

/**************************************************************************
* Function Name     :  RipIfMapChngAndCxtChngHdlr.            
* Description       :  This function is called when there is a change
*                      interface context mapping and context deletion. 
* Input(s)          :  Pinter to the context entry.
* Output(s)         :  Deletes all the context related inforamtion.                                     
* Returns           :  RIP_SUCCESS on success otherwise RIP_FAILURE.
**************************************************************************/
EXPORT VOID
RipIfMapChngAndCxtChngHdlr (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId,
                            UINT1 u1RegFlag)
{
    tIpBuf             *pBuf = NULL;
    tRipParams         *pRipParams = NULL;
    tRipCxt            *pRipCxtEntry = NULL;
    tRipIfaceRec       *pRipIfRec = NULL;

    if ((pRipCxtEntry = RipGetCxtInfoRec ((INT4) u4VcmCxtId)) == NULL)
    {
        return;
    }
    if ((u1RegFlag == VCM_IF_MAP_CHG_REQ) &&
        ((pRipIfRec = RipGetIfRec (u4IpIfIndex, pRipCxtEntry)) == NULL))
    {
        return;
    }
    UNUSED_PARAM (pRipIfRec);
    pBuf = RIP_ALLOCATE_BUF (sizeof (UINT4), 0);
    if (pBuf == NULL)
    {
        RIP_TRC (RIP_MOD_TRC,
                 pRipCxtEntry->u4RipTrcFlag,
                 pRipCxtEntry->i4CxtId,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "RipIfStateChgHdlr: Buffer allocation failed. \n");
        return;
    }
    pRipParams = (tRipParams *) RIP_GET_MODULE_DATA_PTR (pBuf);
    pRipParams->u4IfIdx = u4IpIfIndex;
    pRipParams->i4CxtId = (INT4) u4VcmCxtId;
    pRipParams->u1Chg = u1RegFlag;
    if (RipSendToQ (pBuf, RIP_VCM_MSG) != RIP_SUCCESS)
    {
        RIP_RELEASE_BUF (pBuf, FALSE);
    }

}
