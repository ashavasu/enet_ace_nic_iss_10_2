/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ripudpif.c,v 1.36 2017/09/20 13:08:00 siva Exp $
 *
 * Description:This file contains routines that act as an   
 *             interface between RIP and UDP module. UDP    
 *             port acquirement, sending to UDP module etc  
 *             form the main components of this file.      
 *
 *******************************************************************/
#include "ripinc.h"

#ifdef BSDCOMP_SLI_WANTED

#define RIP_UDP_HDR_LEN 8
#define RIP_UDP_PROTO 17
#define RIP_MTU           1500

PRIVATE INT1        gai1RipPacket[RIP_MTU];
PRIVATE INT4        RipSendMsgwithRawInfo (UINT1 *, UINT2, UINT4, UINT4);
#endif
   /***************************************************************
        * Functions that are called from RIP module to UDP module are  *
        * present here.                                                *
        ***************************************************************/
/*******************************************************************************

    Function            :   rip_task_udp_send.
    
    Description         :   This procedure allocates the "params" structure for
                            udp send operation and then fills in the necessary
                            entities.  Then it prepares the buffer for
                            intermodule communication (UDP-RIP) and then
                            enqueues it to UDP task in the application queue.
    
    Input Parameters    :   1. u2SrcUdpPort,    Local UDP socket.
                            2. u4DestAddr,      Destination IP address.
                            3. u2DestUdpPort,   Remote UDP port.
                            4. *pRipPkt,        a pointer of type tRipPkt*.
                            5. u2Len,           Length of data passed.
                            6. u1Ttl,           Time to live field.
                            7. u2If,            Interface index.
                            8. u1BcastFlag,     Broadcast flag
                            7. u4SrcAddr,       Source Address
    
    Output Parameters   :   None.
    
    Global Variables 
    Affected            :   None.
    
    Return Value        :   Success | Failure.

*******************************************************************************/

/*******************************************************************************
    $$TRACE_PROCEDURE_NAME  = rip_task_udp_send
    $$TRACE_PROCEDURE_LEVEL = LOW
*******************************************************************************/
EXPORT INT4
rip_task_udp_send (UINT1 *au1Data,
                   UINT2 u2SrcUdpPort, UINT4 u4DestAddr,
                   UINT2 u2DestUdpPort, UINT2 u2Len, UINT1 u1Ttl, UINT2 u2If,
                   UINT1 u1BcastFlag, UINT4 u4SrcAddr, tRipCxt * pRipCxtEntry)
{
    struct sockaddr_in  PeerAddr;
    struct msghdr       PktInfo;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[24];    /* For storing Auxillary Data - IP Packet INFO. */
    struct in_pktinfo  *pIpPktInfo = NULL;
    INT4                i4OpnVal = 1;
    struct in_addr      IfAddr;
    struct in_addr      MultiCastIfAddr;
    UINT4               u4SrcSourceAddr;
    UINT4               u4SrcNetMask;
    INT4                i4RetVal;
    tRipIfaceRec       *pRipIfRec = NULL;
    tRipIfaceRec       *pTempRipIfRec = NULL;
    UINT4               u4HashIndex = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4IfPrimaryIpAddress = 0;
    UINT4               u4NegatePrimaryIpAddress = 0;
    UINT4               u4IfSrcAddress = 0;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4GetPortIfIn;
    INT4                i4Route;
    tRipPkt            *pRipPkt = NULL;
    tRip               *pRipHdr = NULL;
    tCryptoAuthKeyInfo *pCryptoKeyInfo = NULL;
    UINT1               u1ClassAFlag = 0;
    UINT1               u1ClassBFlag = 0;
    UINT1               u1ClassCFlag = 0;
    UINT1               u1TrustedNeighbourFound = 0;
    UINT2               u2NoofRoutes = 0;

    struct ip_mreqn     Mreqn;

    MEMSET (&Mreqn, 0, sizeof (struct ip_mreqn));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));

    pRipIfRec = RipGetIfRec ((UINT4) u2If, pRipCxtEntry);
    if (pRipIfRec == NULL)
    {
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_GLOBAL_FLAG,
                      RIP_INVALID_CXT_ID,
                      ALL_FAILURE_TRC, RIP_NAME,
                      "ERROR in filling pRipIfRec for an index %d.\n", (u2If));
        return RIP_FAILURE;
    }
    if (RIP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return RIP_SUCCESS;
    }

#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif

    i4OpnVal = RIP_ENABLE;
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = au1Data;
    Iov.iov_len = u2Len;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo = (struct in_pktinfo *) (VOID *) CMSG_DATA
        (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = u2If;
    pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
    u4SrcSourceAddr = pRipIfRec->RipIfaceCfg.u4SrcAddr;
    IfAddr.s_addr = RIP_NTOHL (u4SrcSourceAddr);
    if (u4DestAddr == MUL_MASK)
    {
        MultiCastIfAddr.s_addr = MUL_MASK;
        Mreqn.imr_multiaddr = MultiCastIfAddr;
        Mreqn.imr_address = IfAddr;
        Mreqn.imr_ifindex = u2If;

        i4RetVal = setsockopt (gRipRtr.i4RipSd, IPPROTO_IP, IP_MULTICAST_IF,
                               (char *) &(Mreqn), sizeof (Mreqn));
        if (i4RetVal < 0)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                     "Setsockopt Failed for IP_MULTICAST_IF in RIP. \n");
        }
    }
    else
    {
        /* set the IP Level option */
        i4RetVal =
            setsockopt (gRipRtr.i4RipSd, IPPROTO_IP, IP_PKTINFO, &i4OpnVal,
                        sizeof (INT4));
        if (i4RetVal < 0)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                     "Setsockopt Failed for IP_PKTINFO in RIP. \n");
        }

        /* All RIP Packets should go with a TTL of 1 */
        i4RetVal = setsockopt (gRipRtr.i4RipSd, IPPROTO_IP, IP_TTL, &i4OpnVal,
                               sizeof (INT4));
        if (i4RetVal < 0)
        {
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                     "Setsockopt Failed for IP_TTL in RIP. \n");
        }

    }

    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_addr.s_addr = RIP_HTONL (u4DestAddr);
    PeerAddr.sin_port = (UINT2) RIP_HTONS (u2DestUdpPort);
#ifdef BSDCOMP_SLI_WANTED
    if (pRipIfRec->u4Addr <= RIP_IPIF_NUMBER_OF_UNNUMBERED_IFACES)
    {
        if (RipSendMsgwithRawInfo (au1Data, u2Len, u2If, u4SrcSourceAddr) ==
            RIP_SUCCESS)
        {
            return RIP_SUCCESS;
        }
        RIP_TRC_ARG1 (RIP_MOD_TRC,
                      RIP_TRC_GLOBAL_FLAG,
                      RIP_INVALID_CXT_ID,
                      ALL_FAILURE_TRC, RIP_NAME,
                      "Failed while filling IP address and UDP address over an interface index %d.\n",
                      (u2If));
        return RIP_FAILURE;
    }
#endif
    /*To get the MessageTyoe from the RIP HEADER */
    pRipPkt = (tRipPkt *) (VOID *) au1Data;
    pRipHdr = (tRip *) (VOID *) pRipPkt;

    /* While sending the packet loop through the rip enabled interfaces and 
       send for both Primay and Secondary IP Address on that interface. 
       If the Primary and Secondary are on the same major network then send 
       only one packet with the Primary IP Address as the destination IP address.
       If Primary and Seondary IP are on different Major Networks then send two packets 
       for each network with the destination IP as the Source IP address of those respectively */

    if (u4SrcAddr != RIP_ZERO)
    {
        u4SrcSourceAddr = u4SrcAddr;
        pIpPktInfo->ipi_spec_dst.s_addr = u4SrcSourceAddr;

        pRipIfRec = RipGetIfRecFromAddr (u4SrcSourceAddr, pRipCxtEntry);
        if (pRipIfRec == NULL)
        {
            return RIP_FAILURE;
        }

        if ((pRipHdr->u1Command == RIP_REQUEST) ||
            (pRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus <=
             RIP_SPLIT_HORZ_WITH_POIS_REV))
        {

            /*when Primary and Seondary IP are rip enabled networks and are 
               in the Same major network or the same class then only one request 
               packet needs to be send out of the interface */
            i4GetPortIfIn = NetIpv4GetCfaIfIndexFromPort (u2If, &u4CfaIfIndex);
            CfaIfGetIpAddress (u4CfaIfIndex, &u4IfPrimaryIpAddress);
            if (u4SrcSourceAddr == u4IfPrimaryIpAddress)
            {
                if ((RIP_SLL_Count (&(pRipIfRec->RipUnicastNBRS)) != 0))
                {
                    u1TrustedNeighbourFound = RIP_FLAG_SET;
                }
            }

            if (u4IfPrimaryIpAddress != u4SrcSourceAddr)
            {
                if ((CFA_IS_ADDR_CLASS_A (u4SrcSourceAddr)) &&
                    (CFA_IS_ADDR_CLASS_A (u4IfPrimaryIpAddress)) &&
                    (u1ClassAFlag < RIP_FLAG_SET))
                {
                    u4IfSrcAddress = u4SrcSourceAddr & CLASSA_MASK;
                    u4NegatePrimaryIpAddress = u4IfPrimaryIpAddress &
                        CLASSA_MASK;

                    if (u4IfSrcAddress == u4NegatePrimaryIpAddress)
                    {
                        u4SrcSourceAddr = u4IfPrimaryIpAddress;
                        pIpPktInfo->ipi_spec_dst.s_addr = u4SrcSourceAddr;
                        u1ClassAFlag++;
                    }

                }
                else if ((CFA_IS_ADDR_CLASS_B (u4SrcSourceAddr)) &&
                         (CFA_IS_ADDR_CLASS_B (u4IfPrimaryIpAddress)) &&
                         (u1ClassBFlag < RIP_FLAG_SET))
                {
                    u4IfSrcAddress = u4SrcSourceAddr & CLASSB_MASK;
                    u4NegatePrimaryIpAddress = u4IfPrimaryIpAddress &
                        CLASSB_MASK;
                    if (u4IfSrcAddress == u4NegatePrimaryIpAddress)
                    {
                        u4SrcSourceAddr = u4IfPrimaryIpAddress;
                        pIpPktInfo->ipi_spec_dst.s_addr = u4SrcSourceAddr;
                        u1ClassBFlag++;
                    }
                }
                else if ((CFA_IS_ADDR_CLASS_C (u4SrcSourceAddr)) &&
                         (CFA_IS_ADDR_CLASS_C (u4IfPrimaryIpAddress)) &&
                         (u1ClassCFlag < RIP_FLAG_SET))
                {
                    u4IfSrcAddress = u4SrcSourceAddr & CLASSC_MASK;
                    u4NegatePrimaryIpAddress = u4IfPrimaryIpAddress &
                        CLASSC_MASK;
                    if (u4IfSrcAddress == u4NegatePrimaryIpAddress)
                    {
                        u4SrcSourceAddr = u4IfPrimaryIpAddress;
                        pIpPktInfo->ipi_spec_dst.s_addr = u4SrcSourceAddr;
                        u1ClassCFlag++;
                    }
                }
            }

        }
        if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_VERSION_2_SND)
        {
            if ((u1TrustedNeighbourFound == RIP_FLAG_SET)
                || (u2DestUdpPort != UDP_RIP_PORT))
            {
                PeerAddr.sin_addr.s_addr = RIP_HTONL (u4DestAddr);
                pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
            }
            else
            {

                /* if rip version 2 is configured then the destination address 
                   is multicast address (224.0.0.9) else it is treated as a 
                   version 1 packet (255.255.255.255) according to RFC2453 during 
                   multicast/broadcast updates */
                PeerAddr.sin_addr.s_addr = RIP_HTONL (RIP_MULTICAST_ADDRESS);
                pIpPktInfo->ipi_addr.s_addr = RIP_MULTICAST_ADDRESS;
                /* calling setscokopt for secondary ip address */
                IfAddr.s_addr = RIP_NTOHL (u4SrcSourceAddr);
                if (u4DestAddr == MUL_MASK)
                {
                    MultiCastIfAddr.s_addr = MUL_MASK;
                    Mreqn.imr_multiaddr = MultiCastIfAddr;
                    Mreqn.imr_address = IfAddr;
                    Mreqn.imr_ifindex = u2If;

                    i4RetVal =
                        setsockopt (gRipRtr.i4RipSd, IPPROTO_IP,
                                    IP_MULTICAST_IF, (char *) &(Mreqn),
                                    sizeof (Mreqn));
                    if (i4RetVal < 0)
                    {
                        RIP_TRC (RIP_MOD_TRC,
                                 pRipCxtEntry->u4RipTrcFlag,
                                 pRipCxtEntry->i4CxtId,
                                 ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                                 "Setsockopt Failed for IP_MULTICAST_IF in RIP. \n");
                    }
                }
            }

        }
        else
        {
            if ((u1TrustedNeighbourFound == RIP_FLAG_SET)
                || (u2DestUdpPort != UDP_RIP_PORT))
            {
                PeerAddr.sin_addr.s_addr = RIP_HTONL (u4DestAddr);
                pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
            }
            else
            {
                PeerAddr.sin_addr.s_addr = RIP_HTONL (RIP_BROADCAST_ADDRESS);
                pIpPktInfo->ipi_addr.s_addr = RIP_BROADCAST_ADDRESS;
            }
        }

        if (pRipIfRec->RipIfaceCfg.u2RipSendStatus == RIPIF_VERSION_2_SND)
        {
            if ((pRipHdr->u1Command != RIP_REQUEST))
            {
                u2NoofRoutes = RipGetUtilNoOfRoutes (u2Len, u2If, pRipCxtEntry);
                if (pRipIfRec->RipIfaceCfg.u2AuthType ==
                    (UINT2) RIPIF_NO_AUTHENTICATION)
                {
                    i4Route = 0;
                }
                else
                {

                    i4Route = 1;
                }
                for (; i4Route <= u2NoofRoutes; i4Route++)
                {
                    pRipPkt->aRipInfo[i4Route].RipMesg.Route.u4NextHop =
                        RIP_HTONL (u4SrcSourceAddr);
                }

                /*recalculate the digest because next-hop field
                   is changed in packet */
                if ((pRipIfRec->RipIfaceCfg.u2AuthType != RIPIF_SIMPLE_PASSWORD)
                    && (pRipIfRec->RipIfaceCfg.u2AuthType !=
                        RIPIF_NO_AUTHENTICATION))
                {

                    pCryptoKeyInfo =
                        RipCryptoGetKeyToUseForSend (u2If, pRipCxtEntry);
                    if (pCryptoKeyInfo != NULL)
                    {
                        RipUtilRecalculateDigest ((UINT1 *) pRipPkt,
                                                  pRipIfRec->RipIfaceCfg.
                                                  u2AuthType, u2Len,
                                                  pCryptoKeyInfo);
                    }
                }

                au1Data = (UINT1 *) (VOID *) pRipPkt;
            }
        }

        /* In SendMsg IP_PKT_TX_CXTID will be updated for
         *                *      * outgoing packet from port number*/
        if (sendmsg (gRipRtr.i4RipSd, &PktInfo, 0) < 0)
        {
            perror ("Error in sendmsg\n");
            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                     "sendmsg failed in RIP. \n");
        }
#ifndef BSDCOMP_SLI_WANTED
        if (sendto
            (gRipRtr.i4RipSd, au1Data, u2Len, 0, (struct sockaddr *) &PeerAddr,
             sizeof (struct sockaddr_in)) < 0)
        {

            RIP_TRC (RIP_MOD_TRC,
                     pRipCxtEntry->u4RipTrcFlag,
                     pRipCxtEntry->i4CxtId,
                     ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                     "SendTo failed in RIP. \n");
            return RIP_FAILURE;
        }
#endif /* BSDCOMP_SLI_WANTED */
        /*Interface statistics to be incremmented for both rip enabled Primary 
           and Secondary IP addresss done here */
        if (pRipHdr->u1Command != RIP_REQUEST)
        {
            if (u1BcastFlag == RIP_TRIG_UPDATE)
            {
                (pRipIfRec->RipIfaceStats.u4SentTrigUpdates)++;
            }
            else
            {
                if (u1BcastFlag == RIP_RES_FOR_REQ)
                {
                    pRipCxtEntry->RipGblStats.u4GlobalQueries++;
                }

                /* Incrementing number of periodic updates sent on the
                 * interface */
                if ((u1BcastFlag == RIP_REG_UPDATE) ||
                    (u1BcastFlag == RIP_LAST_SPACE_UPDATE))
                {
                    pRipCxtEntry->u4RipPeriodicUpdates++;
                    (pRipIfRec->RipIfaceStats.u4SentPeriodicUpdates)++;
                }
            }
        }
    }
    else
    {
        TMO_HASH_Scan_Table (gRipRtr.pGIfHashTbl, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (gRipRtr.pGIfHashTbl, u4HashIndex,
                                  pTempRipIfRec, tRipIfaceRec *)
            {
                if ((u2If != pTempRipIfRec->IfaceId.u4IfIndex) ||
                    (pTempRipIfRec->RipIfaceCfg.u2OperStatus ==
                     RIPIF_OPER_DISABLE))
                {
                    continue;
                }

                u4SrcSourceAddr = pTempRipIfRec->RipIfaceCfg.u4SrcAddr;
                pIpPktInfo->ipi_spec_dst.s_addr = u4SrcSourceAddr;

                if ((pRipHdr->u1Command == RIP_REQUEST) ||
                    (pTempRipIfRec->RipIfaceCfg.u2SplitHorizonOperStatus <=
                     RIP_SPLIT_HORZ_WITH_POIS_REV))
                {

                    i4GetPortIfIn =
                        NetIpv4GetCfaIfIndexFromPort (u2If, &u4CfaIfIndex);
                    CfaIfGetIpAddress (u4CfaIfIndex, &u4IfPrimaryIpAddress);
                    if (u4SrcSourceAddr == u4IfPrimaryIpAddress)
                    {
                        if ((RIP_SLL_Count (&(pTempRipIfRec->RipUnicastNBRS)) !=
                             0))
                        {
                            u1TrustedNeighbourFound = RIP_FLAG_SET;
                        }
                    }
                    if (u4IfPrimaryIpAddress != u4SrcSourceAddr)
                    {
                        if ((CFA_IS_ADDR_CLASS_A (u4SrcSourceAddr))
                            && (CFA_IS_ADDR_CLASS_A (u4IfPrimaryIpAddress)) &&
                            (u1ClassAFlag < RIP_FLAG_SET))
                        {
                            u4IfSrcAddress = u4SrcSourceAddr & CLASSA_MASK;
                            u4NegatePrimaryIpAddress =
                                u4IfPrimaryIpAddress & CLASSA_MASK;
                            if (u4IfSrcAddress == u4NegatePrimaryIpAddress)
                            {
                                u4SrcSourceAddr = u4IfPrimaryIpAddress;
                                pIpPktInfo->ipi_spec_dst.s_addr =
                                    u4SrcSourceAddr;
                                u1ClassAFlag++;
                            }

                        }
                        else if ((CFA_IS_ADDR_CLASS_B (u4SrcSourceAddr))
                                 && (CFA_IS_ADDR_CLASS_B (u4IfPrimaryIpAddress))
                                 && (u1ClassBFlag < RIP_FLAG_SET))
                        {
                            u4IfSrcAddress = u4SrcSourceAddr & CLASSB_MASK;
                            u4NegatePrimaryIpAddress =
                                u4IfPrimaryIpAddress & CLASSB_MASK;
                            if (u4IfSrcAddress == u4NegatePrimaryIpAddress)
                            {
                                u4SrcSourceAddr = u4IfPrimaryIpAddress;
                                pIpPktInfo->ipi_spec_dst.s_addr =
                                    u4SrcSourceAddr;
                                u1ClassBFlag++;
                            }
                        }
                        else if ((CFA_IS_ADDR_CLASS_C (u4SrcSourceAddr))
                                 && (CFA_IS_ADDR_CLASS_C (u4IfPrimaryIpAddress))
                                 && (u1ClassCFlag < RIP_FLAG_SET))
                        {
                            u4IfSrcAddress = u4SrcSourceAddr & CLASSC_MASK;
                            u4NegatePrimaryIpAddress =
                                u4IfPrimaryIpAddress & CLASSC_MASK;
                            if (u4IfSrcAddress == u4NegatePrimaryIpAddress)
                            {
                                u4SrcSourceAddr = u4IfPrimaryIpAddress;
                                pIpPktInfo->ipi_spec_dst.s_addr =
                                    u4SrcSourceAddr;
                                u1ClassCFlag++;
                            }
                        }
                        else
                        {
                            if ((CFA_IS_ADDR_CLASS_A (u4SrcSourceAddr)
                                 && (u1ClassAFlag > RIP_FLAG_SET)))
                            {
                                /* denot do anything skip the iteration for the same class */
                                continue;
                            }
                            else if ((CFA_IS_ADDR_CLASS_B (u4SrcSourceAddr)
                                      && (u1ClassBFlag > RIP_FLAG_SET)))
                            {
                                continue;
                            }
                            else if ((CFA_IS_ADDR_CLASS_C (u4SrcSourceAddr)
                                      && (u1ClassCFlag > RIP_FLAG_SET)))
                            {
                                continue;
                            }

                        }
                    }

                }

                if (pTempRipIfRec->RipIfaceCfg.u2RipSendStatus ==
                    RIPIF_VERSION_2_SND)
                {
                    if ((u1TrustedNeighbourFound == RIP_FLAG_SET)
                        || (u2DestUdpPort != UDP_RIP_PORT))
                    {
                        PeerAddr.sin_addr.s_addr = RIP_HTONL (u4DestAddr);
                        pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
                    }
                    else
                    {
                        PeerAddr.sin_addr.s_addr =
                            RIP_HTONL (RIP_MULTICAST_ADDRESS);
                        pIpPktInfo->ipi_addr.s_addr = RIP_MULTICAST_ADDRESS;
                        /* calling setscokopt for secondary ip address */
                        IfAddr.s_addr = RIP_NTOHL (u4SrcSourceAddr);
                        if (u4DestAddr == MUL_MASK)
                        {
                            MultiCastIfAddr.s_addr = MUL_MASK;
                            Mreqn.imr_multiaddr = MultiCastIfAddr;
                            Mreqn.imr_address = IfAddr;
                            Mreqn.imr_ifindex = u2If;

                            i4RetVal =
                                setsockopt (gRipRtr.i4RipSd, IPPROTO_IP,
                                            IP_MULTICAST_IF, (char *) &(Mreqn),
                                            sizeof (Mreqn));
                            if (i4RetVal < 0)
                            {
                                RIP_TRC (RIP_MOD_TRC,
                                         pRipCxtEntry->u4RipTrcFlag,
                                         pRipCxtEntry->i4CxtId,
                                         ALL_FAILURE_TRC | INIT_SHUT_TRC,
                                         RIP_NAME,
                                         "Setsockopt Failed for IP_MULTICAST_IF in RIP. \n");
                            }
                        }
                    }

                }
                else
                {
                    if ((u1TrustedNeighbourFound == RIP_FLAG_SET)
                        || (u2DestUdpPort != UDP_RIP_PORT))
                    {
                        PeerAddr.sin_addr.s_addr = RIP_HTONL (u4DestAddr);
                        pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
                    }
                    else
                    {
                        PeerAddr.sin_addr.s_addr =
                            RIP_HTONL (RIP_BROADCAST_ADDRESS);
                        pIpPktInfo->ipi_addr.s_addr = RIP_BROADCAST_ADDRESS;
                    }
                }

                if (pTempRipIfRec->RipIfaceCfg.u2RipSendStatus ==
                    RIPIF_VERSION_2_SND)
                {

                    if ((pRipHdr->u1Command != RIP_REQUEST))
                    {
                        u2NoofRoutes =
                            RipGetUtilNoOfRoutes (u2Len, u2If, pRipCxtEntry);
                        if (pTempRipIfRec->RipIfaceCfg.u2AuthType ==
                            (UINT2) RIPIF_NO_AUTHENTICATION)
                        {
                            i4Route = 0;
                        }
                        else
                        {

                            i4Route = 1;
                        }
                        for (; i4Route <= u2NoofRoutes; i4Route++)
                        {
                            pRipPkt->aRipInfo[i4Route].RipMesg.Route.u4NextHop =
                                RIP_HTONL (u4SrcSourceAddr);
                        }

                        if ((pTempRipIfRec->RipIfaceCfg.u2AuthType !=
                             RIPIF_SIMPLE_PASSWORD)
                            && (pTempRipIfRec->RipIfaceCfg.u2AuthType !=
                                RIPIF_NO_AUTHENTICATION))
                        {
                            pCryptoKeyInfo =
                                RipCryptoGetKeyToUseForSend (u2If,
                                                             pRipCxtEntry);
                            if (pCryptoKeyInfo != NULL)
                            {

                                RipUtilRecalculateDigest ((UINT1 *) pRipPkt,
                                                          pTempRipIfRec->
                                                          RipIfaceCfg.
                                                          u2AuthType, u2Len,
                                                          pCryptoKeyInfo);
                            }
                        }

                        au1Data = (UINT1 *) (VOID *) pRipPkt;
                    }

                }

                /* In SendMsg IP_PKT_TX_CXTID will be updated for
                 *      * outgoing packet from port number*/
                if (sendmsg (gRipRtr.i4RipSd, &PktInfo, 0) < 0)
                {
                    perror ("Error in sendmsg\n");
                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                             "sendmsg failed in RIP. \n");
                }
#ifndef BSDCOMP_SLI_WANTED
                if (sendto
                    (gRipRtr.i4RipSd, au1Data, u2Len, 0,
                     (struct sockaddr *) &PeerAddr,
                     sizeof (struct sockaddr_in)) < 0)
                {

                    RIP_TRC (RIP_MOD_TRC,
                             pRipCxtEntry->u4RipTrcFlag,
                             pRipCxtEntry->i4CxtId,
                             ALL_FAILURE_TRC | INIT_SHUT_TRC, RIP_NAME,
                             "SendTo failed in RIP. \n");
                    return RIP_FAILURE;
                }
#endif /* BSDCOMP_SLI_WANTED */

                /*Interface statistics to be incremmented for both rip enabled Primary 
                   and Secondary IP addresss done here */
                if (pRipHdr->u1Command != RIP_REQUEST)
                {
                    if (u1BcastFlag == RIP_TRIG_UPDATE)
                    {
                        (pTempRipIfRec->RipIfaceStats.u4SentTrigUpdates)++;
                    }
                    else
                    {
                        if (u1BcastFlag == RIP_RES_FOR_REQ)
                        {
                            pRipCxtEntry->RipGblStats.u4GlobalQueries++;
                        }

                        /* Incrementing number of periodic updates sent on the
                         *                  * interface */
                        if ((u1BcastFlag == RIP_REG_UPDATE) ||
                            (u1BcastFlag == RIP_LAST_SPACE_UPDATE))
                        {
                            pRipCxtEntry->u4RipPeriodicUpdates++;
                            (pTempRipIfRec->RipIfaceStats.
                             u4SentPeriodicUpdates)++;
                        }
                    }
                }
            }

        }
    }

    UNUSED_PARAM (u2SrcUdpPort);
    UNUSED_PARAM (u1Ttl);
    UNUSED_PARAM (u4BcastAddr);
    UNUSED_PARAM (u4NetMask);
    UNUSED_PARAM (u4SrcNetMask);
    UNUSED_PARAM (i4GetPortIfIn);
    return RIP_SUCCESS;
}

#ifdef BSDCOMP_SLI_WANTED
/**************************************************************************/
/*  Function Name   : RipSendMsgwithRawInfo                              */
/*  Description     : Fills the IP Header and UDP Header along with       */
/*                    RIP Message payload and sent the Message over       */
/*                    Ethernet.As the LinuxIP dont allow the transmission */
/*                    over a Interface without any valid IP Address,this  */
/*                    is required in Linux IP                             */
/*  Input(s)        : pu1Data   - RIP Message PayLoad                     */
/*                    u2Len     - Size of the RIP Message                 */
/*                    u2IfIndex - IP Interface Index                      */
/*  Output(s)       : None                                                */
/*  Returns         : RIP_SUCCESS/RIP_FAILURE                             */
/**************************************************************************/

PRIVATE INT4
RipSendMsgwithRawInfo (UINT1 *pu1Data, UINT2 u2Len, UINT4 u2IfIndex,
                       UINT4 u4SrcSourceAddr)
{
    UINT2               u2Val;
    UINT2               u2Bufflen;
    struct sockaddr_ll  DestAddr;
    t_IP_HEADER         IpHdr;
    tMacAddr            ripRtrAddr = { 0x01, 0x00, 0x5e, 0x00, 0x00, 0x09 };
    INT4                RipRawSockId;
    INT1               *pi1RipPacket = NULL;

    u2Bufflen = RIP_UDP_HDR_LEN + IP_HDR_LEN + u2Len;
    if (u2Bufflen > RIP_MTU)
    {
        return RIP_FAILURE;
    }
    RipRawSockId = socket (PF_PACKET, SOCK_DGRAM, 0);

    if (RipRawSockId < 0)
    {
        perror ("Opening RIP PF_PACKET Socket Failed !!!\n");
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC, RIP_NAME,
                 "Opening RIP PF_PACKET Socket Failed !!!");
        return RIP_FAILURE;
    }

    MEMSET (&gai1RipPacket, 0, sizeof (gai1RipPacket));
    pi1RipPacket = (VOID *) &gai1RipPacket;

    /* Fill the Ip Header */
    MEMSET (&IpHdr, 0, IP_HDR_LEN);
    IpHdr.u1Ver_hdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    IpHdr.u1Tos = 0;
    IpHdr.u2Totlen = u2Bufflen;
    IpHdr.u2Id = 0;
    IpHdr.u2Fl_offs = 0;
    IpHdr.u1Ttl = RIP_IP_DEF_TTL;
    IpHdr.u1Proto = RIP_UDP_PROTO;
    IpHdr.u2Cksum = 0;
    IpHdr.u4Src = u4SrcSourceAddr;
    IpHdr.u4Dest = MUL_MASK;
    IpHdr.u2Cksum = UtlIpCSumLinBuf ((const INT1 *) &IpHdr, IP_HDR_LEN);
    MEMCPY (pi1RipPacket, &IpHdr, IP_HDR_LEN);

    /* Fill the UDP Header */
    u2Val = IP_HTONS (UDP_RIP_PORT);
    MEMCPY (pi1RipPacket + IP_HDR_LEN, &u2Val, sizeof (UINT2));
    u2Val = IP_HTONS (UDP_RIP_PORT);
    MEMCPY (pi1RipPacket + IP_HDR_LEN + 2, &u2Val, sizeof (UINT2));
    u2Val = IP_HTONS (RIP_UDP_HDR_LEN + u2Len);
    MEMCPY (pi1RipPacket + IP_HDR_LEN + 4, &u2Val, sizeof (UINT2));
    u2Val = 0;
    MEMCPY (pi1RipPacket + IP_HDR_LEN + 6, &u2Val, sizeof (UINT2));

    /* Copy the Rip packet */
    MEMCPY (pi1RipPacket + IP_HDR_LEN + RIP_UDP_HDR_LEN, pu1Data, u2Len);

    /* Fill Destination Mac as Broadcast Mac and transmit the packet */
    MEMSET (&DestAddr, 0, sizeof (struct sockaddr_ll));
    DestAddr.sll_family = AF_PACKET;
    DestAddr.sll_ifindex = (UINT2) u2IfIndex;
    DestAddr.sll_protocol = IP_HTONS (ETH_P_IP);
    DestAddr.sll_halen = CFA_ENET_ADDR_LEN;
    MEMCPY (DestAddr.sll_addr, ripRtrAddr, CFA_ENET_ADDR_LEN);

    if (sendto (RipRawSockId, &gai1RipPacket, u2Bufflen, 0,
                (struct sockaddr *) &DestAddr, sizeof (struct sockaddr_ll)) < 0)
    {
        perror ("Sendto Failed due to !!!\n");
        RIP_TRC (RIP_MOD_TRC,
                 RIP_TRC_GLOBAL_FLAG,
                 RIP_INVALID_CXT_ID,
                 ALL_FAILURE_TRC, RIP_NAME, "Send to failed in RIP");
        pi1RipPacket = NULL;
        close (RipRawSockId);
        return RIP_FAILURE;
    }

    pi1RipPacket = NULL;
    close (RipRawSockId);
    return RIP_SUCCESS;
}
#endif /* BSDCOMP_SLI_WANTED */
